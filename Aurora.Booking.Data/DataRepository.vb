'' Change Log!
'' 28.8.8 - Shoel - Modified for THRIFTY HISTORY
'-- RKS : 18-Nov-2010
'-- Call 31092: Complaint tab enhancement
'--rev:mia july 4 2012 - start-- addition of 3rd party field

Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Common
Imports System.Data.SqlClient

Public Class DataRepository

    Public Shared Function GetBookingStatusList() As DataTable
        Return Aurora.Common.Data.ExecuteTableSP("sp_getBookingStatus", New DataTable(), 0)
    End Function




    ''rev:mia july 4 2012 - start-- addition of 3rd party field
    Public Shared Function SearchBookingRental(ByVal bookingStatus As String, ByVal oldBookingNumber As String, _
       ByVal rentalStatus As String, ByVal oldRentalNumber As String, ByVal brandCode As String, ByVal packageId As String, _
       ByVal bookingCity As String, ByVal pickupFromDate As DateTime, ByVal pickupToDate As DateTime, ByVal vehicleClass As String, _
       ByVal checkOutLocationCode As String, ByVal unitNumber As String, ByVal dropOffFromDate As DateTime, _
       ByVal dropOffToDate As DateTime, ByVal regoNumber As String, ByVal checkInLocationCode As String, _
       ByVal arrivalConnectionRef As String, ByVal departureConnectionRef As String, ByVal hirerLastName As String, _
       ByVal agentCode As String, ByVal hirerAddress As String, ByVal agentRef As String, ByVal hirerState As String, _
       ByVal voucherNumber As String, ByVal hirerTown As String, ByVal hirerCity As String, ByVal bookedFromDate As DateTime, _
       ByVal bookedToDate As DateTime, ByVal customerLastName As String, ByVal customerFirstName As String, _
       ByVal inError As String, ByVal agentType As String, ByVal debtorStatus As String, ByVal productShortName As String, _
       ByVal fleetModel As String, ByVal userCode As String, Optional ByVal thirdParty As String = "") As DataSet

        Dim pickupFromDateString As String
        Dim pickupToDateString As String
        Dim dropOffFromDateString As String
        Dim dropOffToDateString As String
        Dim bookedFromDateString As String
        Dim bookedToDateString As String
        'Dim culture As New CultureInfo("en-nz")

        If pickupFromDate = Date.MinValue Then
            pickupFromDateString = ""
        Else
            'pickupFromDateString = Utility.DateTimeToString(pickupFromDate, Utility.SystemCulture)
            pickupFromDateString = Utility.DateDBToString(pickupFromDate)
        End If
        If pickupToDate = Date.MinValue Then
            pickupToDateString = ""
        Else
            'pickupToDateString = Utility.DateTimeToString(pickupToDate, Utility.SystemCulture)
            pickupToDateString = Utility.DateDBToString(pickupToDate)
        End If
        If dropOffFromDate = Date.MinValue Then
            dropOffFromDateString = ""
        Else
            'dropOffFromDateString = Utility.DateTimeToString(dropOffFromDate, Utility.SystemCulture)
            dropOffFromDateString = Utility.DateDBToString(dropOffFromDate)
        End If
        If dropOffToDate = Date.MinValue Then
            dropOffToDateString = ""
        Else
            'dropOffToDateString = Utility.DateTimeToString(dropOffToDate, Utility.SystemCulture)
            dropOffToDateString = Utility.DateDBToString(dropOffToDate)
        End If
        If bookedFromDate = Date.MinValue Then
            bookedFromDateString = ""
        Else
            'bookedFromDateString = Utility.DateTimeToString(bookedFromDate, Utility.SystemCulture)
            bookedFromDateString = Utility.DateDBToString(bookedFromDate)
        End If
        If bookedToDate = Date.MinValue Then
            bookedToDateString = ""
        Else
            'bookedToDateString = Utility.DateTimeToString(bookedToDate, Utility.SystemCulture)
            bookedToDateString = Utility.DateDBToString(bookedToDate)
        End If

        ''rev:mia Oct.20 2013 - catch surnames with apostrophe
        customerLastName = Utility.EncodeTextForSqlPassing(customerLastName)
        hirerLastName = Utility.EncodeTextForSqlPassing(hirerLastName)
        customerFirstName = Utility.EncodeTextForSqlPassing(customerFirstName)

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("Boo_SearchBookingRental", _
           bookingStatus, oldBookingNumber, rentalStatus, oldRentalNumber, brandCode, packageId, bookingCity, _
           pickupFromDateString, pickupToDateString, vehicleClass, checkOutLocationCode, unitNumber, dropOffFromDateString, _
           dropOffToDateString, regoNumber, checkInLocationCode, arrivalConnectionRef, departureConnectionRef, _
           hirerLastName, agentCode, hirerAddress, agentRef, hirerState, voucherNumber, hirerTown, hirerCity, _
           bookedFromDateString, bookedToDateString, customerLastName, customerFirstName, inError, agentType, debtorStatus, _
           productShortName, fleetModel, userCode, thirdParty) ''rev:mia july 4 2012 - start-- addition of 3rd party field

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds
    End Function

    Public Shared Function GetBookingDetail(ByVal bookingId As String, ByVal condition As String, ByVal rentalId As String, _
        ByVal bookingNumber As String, ByVal userCode As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_GetBookingDetailNew", bookingId, condition, rentalId, bookingNumber, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

#Region "Booking Note"
    Public Shared Function GetBookingNotes(ByVal bookingId As String, ByVal rentalId As String, ByVal isAllNote As Boolean) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_getBookingNotesNew", bookingId, rentalId, isAllNote)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function GetBookingNote(ByVal noteId As String, ByVal bookingId As String, ByVal rentalId As String) As DataSet
        'exec(get_BookingNote) '','3285499','3285499-2'
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_BookingNote", noteId, bookingId, rentalId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function ManageNote(ByVal noteId As String, ByVal bookingId As String, ByVal rentalNo As String, _
        ByVal codTypId As String, ByVal noteDesc As String, ByVal codAudTypId As String, ByVal priority As Integer, _
        ByVal isActive As Boolean, ByVal integrityNo As Integer, ByVal userName As String, ByVal addProgramName As String) As String

        Return Aurora.Common.Data.ExecuteScalarSP("ManageNote", noteId, bookingId, bookingId, rentalNo, codTypId, noteDesc, _
            codAudTypId, priority, isActive, integrityNo, userName, addProgramName)

    End Function

    Public Shared Function SaveFile(ByVal rentalId As String, ByVal fileType As String, ByVal description As String,
                                     ByVal fileName As String, ByVal filePath As String, _
                                     ByVal integrityNo As Integer, ByVal userName As String, ByVal addProgramName As String) As String

        Return Aurora.Common.Data.ExecuteScalarSP("SaveRentalFile", rentalId, fileType, description, fileName, filePath, integrityNo, userName, addProgramName)

    End Function

    Public Shared Function DeleteFile(ByVal rentalFileId As String) As String

        Return Aurora.Common.Data.ExecuteScalarSP("DeleteRentalFiles", rentalFileId)

    End Function

    Public Shared Function GetNoteType(ByVal codcdtnum As String, ByVal code As String) As DataSet
        'exec(spgetNoteType) '13',''
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("spgetNoteType", codcdtnum, code)
        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds
    End Function

    Public Shared Function SearchNotes(ByVal bookingId As String, ByVal rentalNoFrom As String, _
       ByVal rentalNoTo As String, ByVal type As String, ByVal addedBy As String, ByVal fromDate As DateTime, _
       ByVal toDate As DateTime, ByVal audience As String, ByVal active As String, ByVal containingText As String, _
       ByVal userCode As String) As DataSet

        Dim fromDateString As String
        Dim toDateString As String

        If fromDate = "12:00:00am" Then
            fromDateString = ""
        Else
            fromDateString = fromDate.ToShortDateString()
        End If
        If toDate = "12:00:00am" Then
            toDateString = ""
        Else
            toDateString = toDate.ToShortDateString()
        End If

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_SearchNotes", bookingId, rentalNoFrom, _
        rentalNoTo, type, addedBy, fromDateString, toDateString, audience, active, containingText, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

#End Region

    Public Shared Function GetFileTypes() As XmlDocument

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GetRentalFileTypes")
        Return xmlDoc

    End Function

    Public Shared Function GetFiles(ByVal rentalId As String) As XmlDocument

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("tab_getRentalFiles", rentalId)
        Return xmlDoc

    End Function

    Public Shared Function GetHistory(ByVal rentalId As String) As XmlDocument

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("tab_getHistory", rentalId)
        Return xmlDoc


    End Function

    Public Shared Function GetMaxRentalNumber(ByVal bookingId As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_maxRentalNumber", bookingId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function GetBookingDetail2(ByVal bookingId As String, ByVal userCode As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("REs_getBookingDetail", bookingId, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Sub ManageBookedProductModBoo(ByVal bookingId As String, ByVal agentId As String, ByVal userCode As String, _
        ByVal programName As String, ByRef returnError As String, ByRef returnMessage As String, ByRef updateFleet As String, _
        ByRef blrIdList As String)

        Dim params(7) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("psBooID", DbType.String, bookingId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("psAgnId", DbType.String, agentId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("psUserId", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("psProgramName", DbType.String, programName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("pReturnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(5) = New Aurora.Common.Data.Parameter("pReturnMessage", DbType.String, 4000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(6) = New Aurora.Common.Data.Parameter("pbUpdateFleet", DbType.Binary, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(7) = New Aurora.Common.Data.Parameter("psBlrIdList", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("RES_ManageBookedProductModBoo", params)
        returnError = params(4).Value
        returnMessage = params(5).Value
        updateFleet = params(6).Value
        blrIdList = params(7).Value

    End Sub

    Public Shared Function ShowBookingRequestAgent(ByVal agentCode As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_showBookingRequestAgent", agentCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function


    Public Shared Function GetBookingRequestAgent(ByVal bookingRequestId As String, ByVal bookingId As String) As XmlDocument

        Dim xmlDoc As New XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getBookingRequestAgent", bookingRequestId, bookingId)
        Return xmlDoc

    End Function

    Public Shared Function UpdateBookingRequestAgent(ByVal xmlDoc As XmlDocument, ByVal parentTable As String) As String

        Dim sRetString As String
        sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("RES_UpdateBookingRequestAgent", xmlDoc.InnerXml, parentTable)
        Return sRetString

    End Function

    ''rev:mia 15jan2014-addition of OPTIONALS for  supervisor override password
    Public Shared Function ManageBooking(ByVal bookingId As String, ByVal firstName As String, ByVal lastName As String, _
      ByVal title As String, ByVal refType As String, ByVal agentId As String, ByVal oldAgentId As String, _
      ByVal intNo As String, ByVal userCode As String, ByVal programName As String, _
      Optional SlotId As String = "", _
      Optional SlotDescription As String = "", _
      Optional RentalId As String = "") As String

        Dim xmlString As StringBuilder = New StringBuilder

        xmlString.Append("<Data><Booking>")

        xmlString.Append("<BooId>" & bookingId & "</BooId>")
        xmlString.Append("<FName>" & firstName & "</FName>")
        xmlString.Append("<LName>" & lastName & "</LName>")
        xmlString.Append("<Title>" & title & "</Title>")
        xmlString.Append("<AgnId>" & agentId & "</AgnId>")
        xmlString.Append("<OLD_AgnId>" & oldAgentId & "</OLD_AgnId>")
        xmlString.Append("<RefType>" & refType & "</RefType>")
        xmlString.Append("<IntNo>" & intNo & "</IntNo>")

        ''rev:mia 15jan2014-addition of SlotId, SlotDescription, RentalId
        xmlString.Append("<SlotId>" & SlotId & "</SlotId>")
        xmlString.Append("<SlotDescription>" & SlotDescription & "</SlotDescription>")
        xmlString.Append("<RentalId>" & RentalId & "</RentalId>")

        xmlString.Append("</Booking></Data>")

        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("RES_manageBooking", xmlString.ToString(), userCode, programName), "")

    End Function

    Public Shared Function GetBookingummary(ByVal bookingId As String, ByVal rentalId As String, ByVal isAll As Boolean) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("Boo_getSummary", bookingId, rentalId, isAll)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function GetForModifyRental(ByVal bookingId As String, ByVal rentalId As String, ByVal userCode As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getForModifyRental", bookingId, rentalId, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function


    ' Extension stuff

    Public Shared Function GetRentalForExtension(ByVal BookingId As String, ByVal RentalId As String) As XmlDocument  'As DataSet
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getRentalForExtension", BookingId, RentalId)
        Return xmlDoc

    End Function


    Public Shared Function GetHirePeriodForExtension(ByVal BPDId As String, ByVal CheckOutDateTime As DateTime, ByVal CheckInDateTime As DateTime, ByVal UOMId As String)

        Dim sCkoWhen, sCkiWhen As String

        sCkoWhen = CheckOutDateTime.ToShortDateString() + " " + CheckOutDateTime.TimeOfDay.ToString()
        sCkiWhen = CheckInDateTime.ToShortDateString() + " " + CheckInDateTime.TimeOfDay.ToString()

        Dim xmlDoc As XmlDocument
        'xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getHirePeriod", "CE41BC9458B840DCB4F034176554A5A4", "17/03/2007 15:00", "26/03/2007 15:00", "81C0738A-0BA1-4E94-8FEC-B7A31824393F")
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getHirePeriod", BPDId, sCkoWhen, sCkiWhen, UOMId)

        Return xmlDoc.SelectSingleNode("//data").InnerXml

    End Function


    Public Shared Function DeleteTProcessedBpds(ByVal RentalId As String) As Boolean
        Try
            'Call oAuroraDAL.appendParameter("@sRntId", 200, 64, sRntId)
            'strQueryString = "RES_DeleteTProcessedBpds"
            Dim xmlDoc As XmlDocument
            xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_DeleteTProcessedBpds", RentalId)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function


    Public Shared Function SSTransToFinance(ByVal BookingId As String, ByVal RentalId As String, ByVal SapId As String, ByVal BookedProductId As String) As Boolean
        Try

            Dim xmlDoc As XmlDocument
            xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_ssTransToFinance", BookingId, RentalId, SapId, BookedProductId)
            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function


    'functions for save functionality


    ' Extension stuff


    'integrity

    Public Shared Sub GetIntegrityNumber(ByRef RentalIntegrityNo As Integer, ByRef BookingIntegrityNo As Integer, Optional ByVal RentalId As String = "", Optional ByVal BPDId As String = "", Optional ByVal BooID As String = "")

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetRentalIntegrity", RentalId, BPDId, BooID)

        If Not xmlDoc.SelectSingleNode("//data/Rental") Is Nothing Then
            RentalIntegrityNo = CInt(xmlDoc.SelectSingleNode("//data/Rental/IntegrityNo").InnerText)
            'If Not xmlDoc.SelectSingleNode("//data/Rental/Booking/BooIntNo") Is Nothing Then
            BookingIntegrityNo = CInt(xmlDoc.SelectSingleNode("//data/Rental/Booking/BooIntNo").InnerText)
            'Else
            '    'another place the booking number can be inside is
            '    If Not xmlDoc.SelectSingleNode("//data/Booking/BooIntNo") Is Nothing Then
            '        BookingIntegrityNo = CInt(xmlDoc.SelectSingleNode("//data/Booking/BooIntNo").InnerText)
            '    End If
            'End If
        Else
            ' Booking is outer node
            RentalIntegrityNo = CInt(xmlDoc.SelectSingleNode("//data/Booking/IntegrityNo").InnerText)
            BookingIntegrityNo = CInt(xmlDoc.SelectSingleNode("//data/Booking/BooIntNo").InnerText)
        End If




    End Sub

    Public Shared Function UpdateRentalIntegrityNumber(Optional ByVal RentalId As String = "", Optional ByVal BPDId As String = "") As XmlDocument
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_UpdateRentalIntegrity", RentalId, BPDId)
        Return xmlDoc
    End Function

    Public Shared Function UpdateBookingIntegrityNumber(ByVal BookingId As String) As XmlDocument
        'ALTER	PROCEDURE [dbo].[RES_UpdateRentalIntegrityForBooking] 
        '	@sBooId		VARCHAR(64) = NULL
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_UpdateRentalIntegrityForBooking", BookingId)
        Return xmlDoc
    End Function


    Public Shared Function ManageRental(ByVal rentalId As String, ByVal arrRef As String, ByVal arrDate As DateTime, _
        ByVal deptRef As String, ByVal deptDate As DateTime, ByVal noOfAdult As Integer, ByVal noOfChildren As Integer, _
        ByVal noOfInfants As Integer, ByVal vipRental As Boolean, ByVal refCustomer As Boolean, ByVal partOfConvoy As Boolean, _
        ByVal agentRef As String, ByVal agentVoucherNo As String, ByVal bookedDate As DateTime, ByVal followUpDate As DateTime, _
        ByVal rentalSource As String, ByVal knockBack As String, ByVal productId As String, ByVal userCode As String, _
        ByVal programName As String, Optional ByVal thirdparty As String = "") As DataTable


        Return ManageRental(rentalId, arrRef, arrDate, _
                           deptRef, deptDate, noOfAdult, noOfChildren, _
                           noOfInfants, vipRental, refCustomer, partOfConvoy, _
                           agentRef, agentVoucherNo, bookedDate, followUpDate, _
                           rentalSource, knockBack, productId, userCode, _
                           programName, thirdparty, Nothing, Nothing, False)

    End Function

    'integrity
    ''rev:mia july 4 2012 - start-- addition of 3rd party field
    ''rev:mia Oct 16 2014 - addition of PromoCode
    ''rev:mia 10nov2014   - slot management, addition of RntSelectedSlotId ,RntSelectedSlot   
    Public Shared Function ManageRental(ByVal rentalId As String, ByVal arrRef As String, ByVal arrDate As DateTime, _
        ByVal deptRef As String, ByVal deptDate As DateTime, ByVal noOfAdult As Integer, ByVal noOfChildren As Integer, _
        ByVal noOfInfants As Integer, ByVal vipRental As Boolean, ByVal refCustomer As Boolean, ByVal partOfConvoy As Boolean, _
        ByVal agentRef As String, ByVal agentVoucherNo As String, ByVal bookedDate As DateTime, ByVal followUpDate As DateTime, _
        ByVal rentalSource As String, ByVal knockBack As String, ByVal productId As String, ByVal userCode As String, _
        ByVal programName As String, ByVal thirdparty As String, _
        ByVal estimatedArrivalDateTime As Nullable(Of DateTime), _
        ByVal estimatedDepartureTime As Nullable(Of DateTime), _
        ByVal canUpdateArrivalDepartureTime As Boolean, _
        Optional ByVal PromoCode As String = "", _
        Optional ByVal RntSelectedSlotId As String = "", _
        Optional ByVal RntSelectedSlot As String = "" _
        ) As DataTable

        Dim arrDateObject As Object
        Dim deptDateObject As Object
        Dim bookedDateObject As Object
        Dim followUpDateObject As Object

        If arrDate = "12:00:00am" Then
            arrDateObject = System.DBNull.Value
        Else
            arrDateObject = Utility.DateTimeToString(arrDate, Utility.SystemCulture)
        End If
        If deptDate = "12:00:00am" Then
            deptDateObject = System.DBNull.Value
        Else
            deptDateObject = Utility.DateTimeToString(deptDate, Utility.SystemCulture)
        End If
        If bookedDate = Date.MinValue Then
            bookedDateObject = System.DBNull.Value
        Else
            bookedDateObject = Utility.DateTimeToString(bookedDate, Utility.SystemCulture)
        End If
        If followUpDate = Date.MinValue Then
            followUpDateObject = System.DBNull.Value
        Else
            followUpDateObject = Utility.DateTimeToString(followUpDate, Utility.SystemCulture)
        End If

        Dim dt As DataTable = New DataTable

         Aurora.Common.Data.ExecuteTableSP("RES_manageRental", dt, rentalId, arrRef, arrDateObject, deptRef, deptDateObject, _
           noOfAdult, noOfChildren, noOfInfants, vipRental, refCustomer, partOfConvoy, agentRef, agentVoucherNo, _
           bookedDateObject, followUpDateObject, rentalSource, knockBack, productId, userCode, programName, thirdparty, _
           estimatedArrivalDateTime, estimatedDepartureTime, canUpdateArrivalDepartureTime, _
           PromoCode, IIf(String.IsNullOrEmpty(RntSelectedSlotId), DBNull.Value, RntSelectedSlotId), RntSelectedSlot)

        Return dt


    End Function


#Region "Check In / Check Out"

    Public Shared Function GetDataForCheckInCheckOut( _
                                ByVal BookingId As String, _
                                ByVal FunctionName As String, _
                                ByVal RentalId As String, _
                                Optional ByVal BookingNumber As String = "", _
                                Optional ByVal UserCode As String = "") As Xml.XmlDocument

        'cus_GetBookingDetail  vBooId & "', 'CKOCKIGET', '" & vRntId & "'"

        '@BooId   VARCHAR  (64) = '',  
        '@Condition  VARCHAR  (8000) = '',  
        '@RntId   VARCHAR  (64) = '',  
        '@BooNum   VARCHAR  (64) = '',  
        '-- KX Changes :RKS  
        '@sUsrCode  VARCHAR(64)  =   ''  

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_GetBookingDetail", BookingId, FunctionName, RentalId, BookingNumber, UserCode)
        Return xmlDoc

    End Function

    Public Shared Function GetLocationStatusForCheckInCheckOut( _
                                ByVal BookingId As String, _
                                ByVal RentalId As String, _
                                ByVal UserCode As String, _
                                ByVal ScreenValue As Integer, _
                                ByVal UnitNumber As String) As XmlDocument
        'RES_getLocStatus   vBooId & "', '" & vRntId & "', '" & vUserCode & "','" & sScreenValue & "','" & sUnitNumber & "'"

        '@sBooId   VARCHAR  (64) = '' ,  
        '@sRntId   VARCHAR  (64) = '' ,  
        '@DefUsr   VARCHAR  (64) = '' ,  
        '@sScreenValue int = 0,  
        '@sDVASSUnitNum varchar(64) = ''   

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getLocStatus", BookingId, RentalId, UserCode, ScreenValue, UnitNumber)
        Return xmlDoc
    End Function

    Public Shared Function GetDataRentalAddForCheckInCheckOut(ByVal BooRnt As String) As XmlDocument
        'GEN_GetPopUpData
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", BooRnt)
        Return xmlDoc
    End Function

    Public Shared Function ManagePreCheckOut(ByVal ScreenData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String)
        'oAuroraDAL.appendParameter("sXmlData", 200, Len(oSelVehXmlDom.xml) + 100, oSelVehXmlDom.xml)
        'oAuroraDAL.appendParameter("sUserCode", 200, 64, sUsercode)
        'oAuroraDAL.appendParameter("sPrgName", 200, 64, sPrgName)
        'sXmlString = oAuroraDAL.UpdateRecords("RES_managePreCheckOut")

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_managePreCheckOut", ScreenData.OuterXml, UserCode, ProgramName)
        Return xmlDoc

    End Function

    Public Shared Function ManageCheckOut(ByVal XmlData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String, ByVal ScreenValue As String) As XmlDocument

        'CREATE                      PROCEDURE [dbo].[RES_manageCheckOut]
        '	@XMLData			TEXT		=	DEFAULT ,
        '	@sUserCode			VARCHAR	 (64)	=	'',
        '	@sPrgmName			VARCHAR	 (64)	=	'',
        '	@ScreenValue		varchar	 (2)	=   ''
        Dim xmlDoc As New XmlDocument
        Dim sRetString As String
        sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("RES_manageCheckOut", XmlData.OuterXml, UserCode, ProgramName, ScreenValue)
        xmlDoc.LoadXml(sRetString)
        Return xmlDoc
    End Function

    Public Shared Function ManageCheckIn(ByVal XmlData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String, ByVal ScreenValue As String)
        'CREATE	PROCEDURE RES_manageCheckIn
        '	@XMLData			TEXT			=	DEFAULT,
        '	@sUserCode			VARCHAR	 (64)	=	'',
        '	@sPrgmName			VARCHAR	 (64)	=	'',
        '	@ScreenValue		varchar	 (2)	=   ''

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_manageCheckIn", XmlData.OuterXml, UserCode, ProgramName, ScreenValue)
        Return xmlDoc


    End Function

    Public Shared Function GetCrossHire(ByVal CountryCrossHire As String, ByVal UnitNumber As String) As XmlDocument
        'CREATE PROCEDURE RES_getCrossHire
        '@sCtyCrossHr				VARCHAR	 (64)	=	'' ,
        '@sUnitNum				VARCHAR	 (64)	=	''

        Dim xmlDoc As New XmlDocument
        Dim sRetString As String
        sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("RES_getCrossHire", CountryCrossHire, UnitNumber)
        xmlDoc.LoadXml(sRetString)
        Return xmlDoc

    End Function

    Public Shared Function MaintainContractHistory(ByVal BookingId As String, ByVal RentalId As String, ByVal UserCode As String, ByVal ProgramName As String)
        'RES_maitainContractHistory
        '       CREATE     PROCEDURE RES_maitainContractHistory 
        '@sBooId			VARCHAR	 (64)	=	'' ,
        '@sRntId			VARCHAR	 (64)	=	'' ,
        '@sUserCode			VARCHAR	 (64)	=	'' ,
        '@sAddPrgmName		VARCHAR	 (256)	=	''
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_maitainContractHistory", BookingId, RentalId, UserCode, ProgramName)
        Return xmlDoc


    End Function

    Public Shared Function GetDataForRentalAgreementPrinting(ByVal RentalId As String) As XmlDocument

        'obj = server.CreateObject("AuroraFindCom.GenericFindRecord")
        'sfunctionName = "RES_Generate_Rental_Agreement '" & param2 & "'"
        'resRecord = obj.getInformation(sfunctionName)
        'Response.Write(resRecord)
        'obj = Nothing

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_Generate_Rental_Agreement", RentalId)
        Return xmlDoc

    End Function

    Public Shared Function GetAIMSVehicleName(ByVal RegNo As String, ByVal UnitNo As String, ByVal RentalId As String, ByVal RequestId As String, ByVal UserCode As String) As XmlDocument
        'CREATE         PROCEDURE [dbo].[RES_getAIMSVehicleName] 
        '	@sRegNo				VARCHAR	 (64)	=	'',
        '	@sUnitNumber		VARCHAR	 (64)	=	'', 
        '	@sRntid				varchar  (64)   = null,
        '	@sRequestType		varchar	 (64)   = null,
        '	-- KX Changes :RKS
        '	@sUserCode			varchar	 (64)	= ''
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getAIMSVehicleName", RegNo, UnitNo, RentalId, RequestId, UserCode)
        Return xmlDoc
    End Function

    ''rev:MIA oct.10
    Public Shared Function GetAIMSVehicleNameExchangeTab(ByVal RegNo As String, ByVal UnitNo As String, ByVal RentalId As String, ByVal RequestId As String, ByVal UserCode As String) As XmlDocument
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getAIMSVehicleName_OSLO_ExchangeTab", RegNo, UnitNo, RentalId, RequestId, UserCode)
        Return xmlDoc
    End Function

    Public Shared Function GetUVIData(ByVal UserCode As String, ByVal roleCode As String) As XmlDocument

        '  GEN_GetPopUpData @case = 'GETUVI', @param1 = 'sp7', @param2='ORID_AIMS'


        'CREATE PROCEDURE [dbo].[GEN_GetPopUpData] @case VARCHAR(30) = '',  
        ' @param1 VARCHAR(64) = '',  
        ' @param2 VARCHAR(64) = '',  
        ' @param3 VARCHAR(500) = '',  
        ' @param4 VARCHAR(64) = '' ,  
        ' @param5 VARCHAR(64)  = '',  
        ' -- KX Changes :RKS  
        ' @sUsrCode varchar(64) = ''  

        Dim xmlDoc As XmlDocument
        'xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", "GETUVI", UserCode, "ORID_AIMS", "", "", "", UserCode)
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", "GETUVI", UserCode, roleCode, "", "", "", UserCode)
        Return xmlDoc
    End Function

    ' Serial Number business here!

    Public Shared Function GetProductsRequiringSerialNumber(ByVal RentalId As String, ByVal ShowAll As Boolean) As XmlDocument
        'CREATE PROC RES_GetProductsRequiringSerialNumber-- '3294372/1' , 1  
        '@RentalId AS VARCHAR(64) ,  
        '@ShowAll AS BIT  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetProductsRequiringSerialNumber", RentalId, ShowAll)
        Return xmlDoc
    End Function

    Public Shared Function SaveProductSerialNumber(ByVal BookedProductId As String, ByVal SerialNumber As String, ByVal UserCode As String, ByVal ProductName As String, ByVal ProgramName As String) As XmlDocument
        ' CREATE PROC dbo.RES_UpdateBPDSerialNumbers  
        ' @BPDId VARCHAR(64) = '',  
        ' @SerialNumber VARCHAR(256) = '',  
        ' @sModUser VARCHAR(64) = '',  
        ' @ProductName VARCHAR(64) = '',  
        ' @sProgramName VARCHAR(64) = ''  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_UpdateBPDSerialNumbers", BookedProductId, SerialNumber, UserCode, ProductName, ProgramName)
        Return xmlDoc
    End Function

    ' End Serial Number Business! :)

    ''rev:mia nov 30 2011 - bonifacio day - 25084 Issue with Mandatory Fields in Customer Maintenance Screen
    Public Shared Function ValidateCustomerFieldsBeforeCheckOut(ByVal BooNum As String, ByVal rentalNo As String) As String
        Dim result As String = ""
        'Return result

        Dim params(2) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("rntBooId", DbType.String, BooNum, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("rntNum", DbType.String, rentalNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("AddId", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)


        Try

            Aurora.Common.Data.ExecuteOutputSP("CheckOut_ValidateCustomerBeforeCheckOut", params)
            result = params(2).Value

        Catch ex As Exception
            Return result
        End Try
        Return result
    End Function

#Region "Enhancement #28 DTR (or DRT or whatever!)"

    'Public Shared Function GetProductsRequiringDieselRecoveryTax(ByVal RentalId As String) As XmlDocument
    '    Dim xmlDoc As XmlDocument
    '    xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetProductsRequiringDieselRecoveryTax", RentalId)
    '    Return xmlDoc
    'End Function

    'Public Shared Function IsDieselRecoveryTaxAlreadyApplied(ByVal RentalId As String) As Boolean
    '    Dim oObj As Object
    '    Dim bResult As Boolean = False
    '    oObj = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.RES_IsDieselRecoveryTaxApplied('" & RentalId & "')")
    '    If oObj.GetType().Equals(GetType(Boolean)) Then
    '        ' It's a boolean return :)
    '        bResult = CBool(oObj)
    '    Else
    '        Throw New Exception("Function 'dbo.RES_IsDieselRecoveryTaxApplied' returned a non-boolean result - " & oObj.ToString())
    '    End If

    '    Return bResult

    'End Function

    'Public Shared Function SaveProduct(ByVal RentalId As String, ByVal SaleableProductId As String, ByVal CheckOutDate As DateTime, _
    'ByVal CheckOutLocation As String, ByVal CheckInDate As DateTime, ByVal CheckInLocation As String, _
    'ByVal Rate As Double, ByVal Quantity As Integer, ByVal UserCode As String, ByVal ProgramName As String, _
    'ByRef ReturnError As String) As String
    '    ' PROCEDURE [dbo].[RES_ManageBookedProductAddNonVehicleRequest] 
    '    '	@sRntID						VARCHAR(64),
    '    '	@sSapId						VARCHAR(64),
    '    '	@bApplyOriginalRates		BIT					= 0,
    '    '	@dCkoWhen					DATETIME			= NULL,
    '    '	@sCkoLocCode				VARCHAR(12)		= NULL,

    '    '	@dCkiWhen					DATETIME			= NULL,
    '    '	@sCkiLocCode				VARCHAR(12)		= NULL,
    '    '	@cRate						MONEY				= NULL,
    '    '	@iQuantity					INTEGER				= NULL,
    '    '	@sPrrIdList					VARCHAR(500)		= NULL,

    '    '	@sPrrQtyList					VARCHAR(500)		= NULL,
    '    '	@sPrrRateList					VARCHAR(500)		= NULL,
    '    '	@sPtdId						VARCHAR(64)		= NULL,
    '    '	@sCodTypId					VARCHAR(64)		= NULL,
    '    '	@sBpdIDParent				VARCHAR(64)		= NULL,

    '    '	@dChargeFrom				datetime 				= null,
    '    '	@dChargeTo					datetime 				= null,
    '    '	@psUnitNo					VARCHAR(64) 		= NULL ,			
    '    '	@psBpdRego					VARCHAR	 (64)	= NULL ,			
    '    '	@iFromOdo 					MONEY				= NULL ,		

    '    '	@iToOdo						MONEY				= NULL ,		
    '    '	@psExtRef						VARCHAR(64)		= NULL ,		
    '    '	@bIsCusCharge					bit				= NULL,			
    '    '	@sUserId						VARCHAR(64)		= NULL,
    '    '	@sProgramName				VARCHAR(256)		= NULL,

    '    '	@bCreateHistory				BIT					= 1 ,
    '    '	@psEvent					VARCHAR	 (64)	=	NULL ,	
    '    '	@sNewBpdIdsList			VARCHAR(2000)	 	OUTPUT ,	
    '    '	@ReturnError					VARCHAR(500)		OUTPUT,
    '    '	@ReturnMessage				VARCHAR(500)		OUTPUT

    '    Dim NewBPDList, RetError, RetMsg As String
    '    NewBPDList = ""
    '    RetError = ""
    '    RetMsg = ""
    '    Dim xmlDoc As XmlDocument

    '    xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_ManageBookedProductAddNonVehicleRequest", _
    '    RentalId, SaleableProductId, False, CheckOutDate, CheckOutLocation, _
    '    CheckInDate, CheckInLocation, Rate, Quantity, System.DBNull.Value, _
    '    System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, _
    '    System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, _
    '    System.DBNull.Value, System.DBNull.Value, True, UserCode, ProgramName, _
    '    True, System.DBNull.Value, NewBPDList, RetError, RetMsg)


    '    Return xmlDoc.OuterXml

    '    'Try

    '    '    Dim params(29) As Aurora.Common.Data.Parameter

    '    '    params(0) = New Aurora.Common.Data.Parameter("sRntID", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(1) = New Aurora.Common.Data.Parameter("sSapId", DbType.String, SaleableProductId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(2) = New Aurora.Common.Data.Parameter("bApplyOriginalRates", DbType.Boolean, False, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(3) = New Aurora.Common.Data.Parameter("dCkoWhen", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(4) = New Aurora.Common.Data.Parameter("sCkoLocCode", DbType.String, CheckOutLocation, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '    '    params(5) = New Aurora.Common.Data.Parameter("dCkiWhen", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(6) = New Aurora.Common.Data.Parameter("sCkiLocCode", DbType.String, CheckInLocation, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(7) = New Aurora.Common.Data.Parameter("cRate", DbType.Currency, Rate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(8) = New Aurora.Common.Data.Parameter("iQuantity", DbType.Int32, Quantity, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(9) = New Aurora.Common.Data.Parameter("sPrrIdList", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '    '    params(10) = New Aurora.Common.Data.Parameter("sPrrQtyList", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(11) = New Aurora.Common.Data.Parameter("sPrrRateList", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(12) = New Aurora.Common.Data.Parameter("sPtdId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(13) = New Aurora.Common.Data.Parameter("sCodTypId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(14) = New Aurora.Common.Data.Parameter("sBpdIDParent", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '    '    params(15) = New Aurora.Common.Data.Parameter("dChargeFrom", DbType.DateTime, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(16) = New Aurora.Common.Data.Parameter("dChargeTo", DbType.DateTime, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(17) = New Aurora.Common.Data.Parameter("psUnitNo", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(18) = New Aurora.Common.Data.Parameter("psBpdRego", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(19) = New Aurora.Common.Data.Parameter("iFromOdo", DbType.Currency, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '    '    params(20) = New Aurora.Common.Data.Parameter("iToOdo", DbType.Currency, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(21) = New Aurora.Common.Data.Parameter("psExtRef", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(22) = New Aurora.Common.Data.Parameter("bIsCusCharge", DbType.Boolean, True, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(23) = New Aurora.Common.Data.Parameter("sUserId", DbType.String, UserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(24) = New Aurora.Common.Data.Parameter("sProgramName", DbType.String, ProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '    '    params(25) = New Aurora.Common.Data.Parameter("bCreateHistory", DbType.Boolean, True, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(26) = New Aurora.Common.Data.Parameter("psEvent", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    '    params(27) = New Aurora.Common.Data.Parameter("sNewBpdIdsList", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '    '    params(28) = New Aurora.Common.Data.Parameter("ReturnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '    '    params(29) = New Aurora.Common.Data.Parameter("ReturnMessage", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

    '    '    Aurora.Common.Data.ExecuteOutputSP("RES_ManageBookedProductAddNonVehicleRequest", params)

    '    '    ReturnError = params(28).Value
    '    '    Return params(29).Value

    '    'Catch ex As Exception
    '    '    Return "ERR/" & ex.Message
    '    'End Try
    'End Function

    'Public Shared Function GetDieselRecovertTaxSaleableProductId(ByVal PackageId As String, ByVal CountryCode As String, ByVal ProductId As String, _
    'ByVal CheckOutLocation As String, ByVal CheckInLocation As String, ByVal CheckOutDate As DateTime, ByVal CheckInDate As DateTime, _
    'ByRef ReturnError As String) As String
    '    'PROCEDURE [dbo].[RES_getSaleableProductId] 

    '    '@psPkgID		VARCHAR(64),
    '    '@psCtyCode		VARCHAR(3)	= NULL,
    '    '@psPrdID		VARCHAR(64),
    '    '@psAgnId		VARCHAR(64),
    '    '@psCkoLocCode		VARCHAR(12)	= NULL,

    '    '@psCkiLocCode		VARCHAR(12)	= NULL,
    '    '@pdCkoWhen		DATETIME	= NULL,
    '    '@pdCkiWhen		DATETIME	= NULL,	
    '    '@psSapId		VARCHAR(64)	= NULL,
    '    '@pdAdjBookedWhen	DATETIME	= NULL,

    '    '@piNumOfAdults		INTEGER		= NULL,
    '    '@piNumOfChildren	INTEGER		= NULL,
    '    '@piNumOfInfants		INTEGER		= NULL,
    '    '@psEvent		varchar(12)	= null,
    '    '@pbProduceDebugTrace	BIT		= 0,

    '    '@ReturnSapId		VARCHAR(64)	OUTPUT,
    '    '@psReturnCodCurrId	VARCHAR(64)	OUTPUT,
    '    '@pbReturnPkgPrd		BIT		OUTPUT,
    '    '@pbReturnIsBase		BIT		OUTPUT,
    '    '@psReturnSapStatus	VARCHAR(12)	OUTPUT ,	

    '    '@psError			VARCHAR	 (8000) OUTPUT	
    '    Try

    '        'Dim xmlDoc As XmlDocument
    '        'Dim s1, s2, s3, s4 As String
    '        'Dim b1, b2 As Boolean

    '        'xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getSaleableProductId", _
    '        'PackageId, CountryCode, ProductId, System.DBNull.Value, CheckOutLocation, _
    '        'CheckInLocation, CheckOutDate, CheckInDate, System.DBNull.Value, System.DBNull.Value, _
    '        'System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, System.DBNull.Value, False, _
    '        's1, s2, b1, b2, s3, s4)



    '        Dim params(20) As Aurora.Common.Data.Parameter

    '        params(0) = New Aurora.Common.Data.Parameter("psPkgID", DbType.String, PackageId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(1) = New Aurora.Common.Data.Parameter("psCtyCode", DbType.String, CountryCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(2) = New Aurora.Common.Data.Parameter("psPrdID", DbType.String, ProductId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(3) = New Aurora.Common.Data.Parameter("psAgnId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(4) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, CheckOutLocation, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '        params(5) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, CheckInLocation, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(6) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.DateTime, CheckOutDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(7) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.DateTime, CheckInDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(8) = New Aurora.Common.Data.Parameter("psSapId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(9) = New Aurora.Common.Data.Parameter("pdAdjBookedWhen", DbType.DateTime, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '        params(10) = New Aurora.Common.Data.Parameter("piNumOfAdults", DbType.Int32, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(11) = New Aurora.Common.Data.Parameter("piNumOfChildren", DbType.Int32, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(12) = New Aurora.Common.Data.Parameter("piNumOfInfants", DbType.Int32, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(13) = New Aurora.Common.Data.Parameter("psEvent", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(14) = New Aurora.Common.Data.Parameter("pbProduceDebugTrace", DbType.Boolean, False, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '        params(15) = New Aurora.Common.Data.Parameter("ReturnSapId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(16) = New Aurora.Common.Data.Parameter("psReturnCodCurrId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(17) = New Aurora.Common.Data.Parameter("psReturnSapStatus", DbType.Boolean, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(18) = New Aurora.Common.Data.Parameter("pbReturnPkgPrd", DbType.Boolean, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(19) = New Aurora.Common.Data.Parameter("pbReturnIsBase", DbType.String, 12, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

    '        params(20) = New Aurora.Common.Data.Parameter("psError", DbType.String, 8000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

    '        Aurora.Common.Data.ExecuteOutputSP("RES_getSaleableProductId", params)

    '        ReturnError = params(20).Value

    '        Return params(15).Value

    '    Catch ex As Exception
    '        Return "ERR/" & ex.Message
    '    End Try
    'End Function

    '''' <summary>
    '''' Get the Package Id of the rental, and also the DTR Package Id
    '''' </summary>
    '''' <param name="RentalId">The Rental Id</param>
    '''' <param name="PackageId">The Rental's Package Id - Returned</param>
    '''' <returns>The DTR Product Id :)</returns>
    '''' <remarks></remarks>
    'Public Shared Function GetDieselRecoveryTaxProductAndPackageId(ByVal RentalId As String, ByRef PackageId As String) As String

    '    'CREATE PROC RES_GetDieselRecoveryTaxProductAndPackageId --'3197261-2'  
    '    '@RentalId AS VARCHAR(64),  
    '    '@PackageId AS VARCHAR(64) OUTPUT,
    '    '@ProductId AS VARCHAR(64) OUTPUT

    '    Dim params(2) As Aurora.Common.Data.Parameter
    '    params(0) = New Aurora.Common.Data.Parameter("RentalId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '    params(1) = New Aurora.Common.Data.Parameter("PackageId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '    params(2) = New Aurora.Common.Data.Parameter("ProductId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

    '    Aurora.Common.Data.ExecuteOutputSP("RES_GetDieselRecoveryTaxProductAndPackageId", params)

    '    PackageId = params(1).Value
    '    Return params(2).Value

    'End Function

#End Region


#End Region

#Region "Agent contact"
    Public Shared Function GetAgentContact(ByVal agentId As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getAgentContact", agentId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Sub ValidatePhoneNumber(ByVal action As String, ByVal tableName As String, ByVal agentId As String, _
        ByVal typeId As String, ByVal phoneNumber As String, ByVal email As String, ByRef returnError As String)

        Dim params(6) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sAction", DbType.String, action, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sphnprntTableName", DbType.String, tableName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sPhnPrntId", DbType.String, agentId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sPhnCodTypeId", DbType.String, typeId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sPhnNumber", DbType.String, phoneNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sPhnEmailAddress", DbType.String, email, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_validatePhoneNumber", params)
        returnError = params(6).Value

    End Sub


    Public Shared Sub UpdateContact(ByVal queryAction As String, ByVal contactId As String, ByVal tableName As String, _
        ByVal agentId As String, ByVal typeId As String, ByVal title As String, ByVal fName As String, ByVal lName As String, _
        ByVal position As String, ByVal comment As String, ByVal integrityNo As Integer, ByVal userCode As String, _
        ByVal programName As String, ByRef returnError As String)

        Dim params(14) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sQueryAction", DbType.String, queryAction, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sConId", DbType.String, contactId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sConPrntTableName", DbType.String, tableName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sConPrntId", DbType.String, agentId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sConCodTypId", DbType.String, typeId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sConName", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sConTitle", DbType.String, title, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sConFirstName", DbType.String, fName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("sConLastName", DbType.String, lName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sConPosition", DbType.String, position, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("sConComments", DbType.String, comment, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int32, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("sAddModUsrId", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, programName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(14) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_updateContact", params)
        returnError = params(14).Value

    End Sub

    Public Shared Sub UpdatePhoneNumber(ByVal queryAction As String, ByVal phnId As String, ByVal tableName As String, _
        ByVal phnPrnId As String, ByVal typeId As String, ByVal areaCode As String, ByVal countryCode As String, ByVal number As String, ByVal integrityNo As Integer, _
        ByVal email As String, ByVal userCode As String, ByVal programName As String, ByRef returnError As String)

        Dim params(12) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sQueryAction", DbType.String, queryAction, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sPhnId", DbType.String, phnId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sPhnPrntTableName", DbType.String, tableName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sPhnPrntId", DbType.String, phnPrnId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sPhnCodTypeId", DbType.String, typeId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sPhnAreaCode", DbType.String, areaCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sPhnCountryCode", DbType.String, countryCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sPhnNumber", DbType.String, number, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int32, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sPhnEmailAddress", DbType.String, email, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("AddModUsrId", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, programName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_updatePhoneNumber", params)
        returnError = params(12).Value

    End Sub

#End Region

#Region "Booking Customer"

    Public Shared Function GetCustomerSummary(ByVal thisRental As Boolean, ByVal bookingId As String, ByVal rentalId As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cust_get_CustomerSummary", thisRental, bookingId, rentalId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    ''rev:mia jan 21 2009 bypass bp2bb
    Public Shared Function UpdateCustomerSummaryAndBypassBP2BB(ByVal bookingId As String, ByVal rentalId As String, ByVal xmlDoc As XmlDocument) As String

        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("cus_UpdateCustomerSummaryBypassAgeValidation", bookingId, rentalId, xmlDoc.InnerXml))

    End Function

    Public Shared Function UpdateCustomerSummary(ByVal bookingId As String, ByVal rentalId As String, ByVal xmlDoc As XmlDocument) As String

        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("cus_UpdateCustomerSummary", bookingId, rentalId, xmlDoc.InnerXml))

    End Function

    Public Shared Function GetDrivers(ByVal rentalId As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cust_getDrivers", rentalId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function FindDriverByLicence(ByVal rentalId As String, ByVal licence As String, ByVal dob As Date) As DataSet
        Dim dobString As String = ""

        If dobString = "12:00:00am" Then
            dobString = ""
        Else
            dobString = dob.ToShortDateString()
        End If

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cust_FindDriverByLicence", rentalId, licence, dobString)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Sub DeleteDriver(ByVal customerId As String, ByVal rentalId As String, ByRef errorMessage As String)

        Dim params(2) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("CusId", System.Data.DbType.String, customerId, Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("RntID", System.Data.DbType.String, rentalId, Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        Aurora.Common.Data.ExecuteOutputSP("cust_deleteDriver", params)

        errorMessage = params(2).Value

    End Sub

    Public Shared Sub ValidateDriver(ByVal rentalId As String, ByVal dob As Date, ByVal expDate As Date, ByRef errorMessage As String)

        Dim dobObject As Object
        Dim expDateObject As Object

        If dob = "12:00:00am" Then
            dobObject = System.DBNull.Value
        Else
            dobObject = dob.ToShortDateString
        End If
        If expDate = "12:00:00am" Then
            expDateObject = System.DBNull.Value
        Else
            expDateObject = expDate.ToShortDateString
        End If


        Dim params(3) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("RntId", System.Data.DbType.String, rentalId, Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("CusDob", System.Data.DbType.String, dobObject, Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("ExpDate", System.Data.DbType.String, expDateObject, Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        Aurora.Common.Data.ExecuteOutputSP("cust_ValidateDriver", params)

        errorMessage = params(3).Value

    End Sub

    Public Shared Function UpdateCustomer(ByVal customerId As String, ByVal firstName As String, _
        ByVal lastName As String, ByVal isVip As Boolean, ByVal fleetDriver As Boolean, _
        ByVal personType As String, ByVal title As String, ByVal dob As Date, ByVal licence As String, _
        ByVal expiryDate As Date, ByVal issuingAuthority As String, ByVal gender As String, _
        ByVal occupation As String, ByVal codPrefModOfCommId As String, ByVal receiveMailLists As Boolean, _
        ByVal receiveSurveys As Boolean, ByVal integrityNo As Integer, ByVal passportNo As String, _
        ByVal passportCountry As String, ByVal rentalId As String, ByVal addUserId As String, _
        ByVal modUserId As String, ByVal programName As String, ByVal loyaltyCardNumber As String) As String

        Dim dobObject As Object
        Dim expDateObject As Object

        If dob = "12:00:00am" Then
            dobObject = System.DBNull.Value
        Else
            dobObject = dob.ToShortDateString
        End If
        If expiryDate = "12:00:00am" Then
            expDateObject = System.DBNull.Value
        Else
            expDateObject = Utility.DateTimeToString(expiryDate, Utility.SystemCulture)
        End If

        'exec(cust_updateCustomer) 'F2E3B894-B178-41D8-A18B-983E74219E37', 'A', 'A', 0, 0, default, '', 
        '20/10/1980', '1', 'Dec 20 2010 12:00:00:000AM', 'a', default, default, default, 0, 
        '0, 1, 'NA', 'NA', '', 'jl3', 'jl3', 'customerlistener.asp'



        'personType, gender, occupation, codPrefModOfCommId


        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("cust_updateCustomer", _
            customerId, firstName, lastName, isVip, fleetDriver, personType, title, dobObject, licence, _
            expDateObject, issuingAuthority, gender, occupation, codPrefModOfCommId, receiveMailLists, _
            receiveSurveys, integrityNo, passportNo, passportCountry, rentalId, addUserId, _
            modUserId, programName, loyaltyCardNumber))

    End Function

    Public Shared Function UpdateDriver(ByVal customerId As String, ByVal firstName As String, ByVal lastName As String, _
        ByVal title As String, ByVal dob As Date, ByVal licence As String, ByVal expiryDate As Date, _
        ByVal issuingAuthority As String, ByVal integrityNo As Integer, ByVal addUserId As String, _
        ByVal modUserId As String, ByVal programName As String) As String

        Dim dobObject As Object
        Dim expDateObject As Object

        If dob = "12:00:00am" Then
            dobObject = System.DBNull.Value
        Else
            dobObject = dob.ToShortDateString
        End If
        If expiryDate = "12:00:00am" Then
            expDateObject = System.DBNull.Value
        Else
            expDateObject = Utility.DateTimeToString(expiryDate, Utility.SystemCulture)
        End If
        '@CusId	varchar
        '@CusFirstName	varchar
        '@CusLastName	varchar
        '@CusTitle	varchar
        '@CusDob	varchar
        '@CusDriversLicenceNum	varchar
        '@CusLicenceExpiryDate	datetime
        '@CusIssuingAuthority	varchar
        '@IntegrityNo	int
        '@AddUsrId	varchar
        '@ModUsrId	varchar
        '@AddPrgmName	varchar

        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("cust_updateDriver", _
            customerId, firstName, lastName, title, dobObject, licence, expDateObject, issuingAuthority, _
            integrityNo, addUserId, modUserId, programName))

    End Function

    Public Shared Function UpdateTraveller(ByVal customerId As String, ByVal rentalId As String, ByVal isDriver As Boolean, _
           ByVal isHirer As Boolean, ByVal isPrimaryHirer As Boolean, ByVal addUserId As String, ByVal modUserId As String, ByVal programName As String) As String

        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("cust_updateTraveller", _
            customerId, rentalId, isDriver, isHirer, addUserId, modUserId, programName, isPrimaryHirer))

    End Function

    Public Shared Function FindCustomer(ByVal search As Integer, ByVal lastName As String, _
        ByVal firstName As String, ByVal dob As Date, ByVal isVip As Boolean, ByVal street As String, _
        ByVal city As String, ByVal state As String, ByVal country As String, ByVal licenceNo As String, _
        ByVal isFleet As Boolean, ByVal age As Integer, ByVal userCode As String) As DataSet

        '@bSearch		int,	     	 
        '@sCusLastName	varchar(128) 	= '',
        '@sCusFirstName	varchar(128) 	= '',
        '@sCusDOB		varchar(12)  	= '',
        '@bCusVip		bit	     		= 0 ,
        '@sStreet		varchar(100) 	= '',
        '@sCityTown		varchar(100) 	= '',
        '@sState			varchar(100) 	= '',
        '@sCountry		varchar(100) 	= '',
        '@sLicenceNo		varchar(100) 	= '',
        '@bFleet			bit	     		= '0',
        '@iAge				VARCHAR(10)	= '',
        '@sUserCode		VARCHAR(64)		

        Dim dobObject As String
        If dob = Date.MinValue Then
            dobObject = "" 'DBNull.Value
        Else
            dobObject = dob.ToShortDateString
        End If

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_findCustomer", search, lastName, _
            firstName, dobObject, isVip, street, city, state, country, licenceNo, isFleet, age, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function FleetDriverCheckBoxEnable(ByVal userCode As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_fleetDriverCheckBox_Enable", userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function GetCustomerDetails(ByVal customerId As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cust_get_CustomerDetails", customerId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Sub ValidateAddress(ByVal action As String, ByVal tableName As String, ByVal agentId As String, _
           ByVal typeId As String, ByVal city As String, ByVal state As String, ByRef returnError As String)

        Dim params(6) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sAction", DbType.String, action, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sAddPrntTableName", DbType.String, tableName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sAddPrntId", DbType.String, agentId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sAddCodTypeId", DbType.String, typeId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, city, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sStaCode", DbType.String, state, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_validateAddress", params)
        returnError = params(6).Value

    End Sub

    Public Shared Sub UpdateAddress(ByVal queryAction As String, ByVal id As String, ByVal tableName As String, _
           ByVal address1 As String, ByVal typeId As String, ByVal prntId As String, ByVal address2 As String, _
           ByVal postCode As String, ByVal cityCode As String, ByVal address3 As String, ByVal stateCode As String, _
           ByVal integrityNo As Integer, ByVal userCode As String, ByVal programName As String, ByRef returnError As String)

        Dim params(14) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sQueryAction", DbType.String, queryAction, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sAddId", DbType.String, id, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sAddPrntTableName", DbType.String, tableName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sAddAddress1", DbType.String, address1, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sAddCodTypeId", DbType.String, typeId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sAddPrntID", DbType.String, prntId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sAddAddress2", DbType.String, address2, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sAddPostcode", DbType.String, postCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("sAddCtyCode", DbType.String, cityCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sAddAddress3", DbType.String, address3, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("sAddStaCode", DbType.String, stateCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int32, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("sAddModUsrId", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, programName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(14) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_updateAddress", params)
        returnError = params(14).Value

    End Sub

    Public Shared Function UpdateCustomerBrochure(ByVal brochureId As String, ByVal customerId As String, ByVal qty As Integer, _
           ByVal addUserId As String, ByVal modUserId As String, ByVal programName As String) As String

        Return Common.Utility.ParseString(Aurora.Common.Data.ExecuteScalarSP("cust_UpdateCustomerBrochure", _
            brochureId, customerId, qty, addUserId, modUserId, programName))

    End Function

    Public Shared Sub ValidateDeletion(ByVal customerId As String, ByVal rentalId As String, ByRef returnError As String)

        Dim params(2) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("CusId", DbType.String, customerId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("CurrentRntId", DbType.String, rentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("cust_ValidateDeletion", params)
        returnError = params(2).Value

    End Sub

    Public Shared Sub DeleteCustomer(ByVal customerId As String, ByRef returnError As String)

        Dim params(1) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("CusId", DbType.String, customerId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sRetirnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("cust_deleteCustomer", params)
        returnError = params(1).Value

    End Sub

#End Region

#Region "Booking exchange"

    Public Shared Function GetExchangeData(ByVal bookingId As String, ByVal rentalId As String, _
        ByVal userCode As String) As DataSet

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getExchangeData", bookingId, rentalId, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    Public Shared Function GetExchangeDataXmlDoc(ByVal bookingId As String, ByVal rentalId As String, _
       ByVal userCode As String) As XmlDocument

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getExchangeData", bookingId, rentalId, userCode)
        Return xmlDoc

    End Function

    Public Shared Sub GetSapIdForShortName(ByVal rentalId As String, ByVal shortName As String, _
        ByVal userCode As String, ByVal startDate As Date, _
        ByRef amPm As String, ByRef dVassSeqNum As String, ByRef sapId As String, _
        ByRef returnError As String, ByRef updateFleet As Boolean)

        Dim params(8) As Aurora.Common.Data.Parameter
        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, rentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sShortName", DbType.String, shortName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("dtStart", DbType.Date, startDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(4) = New Aurora.Common.Data.Parameter("sAMPM", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(5) = New Aurora.Common.Data.Parameter("dVassSeqNum", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(6) = New Aurora.Common.Data.Parameter("sSapId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(7) = New Aurora.Common.Data.Parameter("returnError", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(8) = New Aurora.Common.Data.Parameter("bUpdateFleet", DbType.Int32, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        '' INPUT parameters
        'Call .appendParameter("@sRntId", 200, 64, sRntId)
        'Call .appendParameter("@sShortName", 200, 64, Trim(sNewPrd))
        'Call .appendParameter("@sUsrCode", 200, 64, sUserId)
        'Call .appendParameter("@dtStart", adDate, 10, dExgDate)

        '' OUTPUT parameters
        'Call .appendParameter2("@sAMPM", 200, 64, sExgAMPM, True)
        'Call .appendParameter2("@dVassSeqNum", 200, 64, strProductId, True)
        'Call .appendParameter2("@sSapId", 200, 64, strRetSapId, True)
        'Call .appendParameter2("@returnError", 200, 2000, strRetMsg, True)
        'Call .appendParameter2("@bUpdateFleet", 11, 1, bUpdateFleet, True)

        Aurora.Common.Data.ExecuteOutputSP("GEN_getSapIdForShortName", params)
        amPm = params(4).Value
        dVassSeqNum = params(5).Value
        sapId = params(6).Value
        returnError = params(7).Value
        updateFleet = Utility.ParseBoolean(params(8).Value, False)

    End Sub

    ' Added By Nimesh on 06th Aug 2015 - To Update Service Due Flag
    Public Shared Sub UpdateServiceDueFlag(ByVal unitNumber As Integer, ByRef returnError As String)
        Dim params(1) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("piUnitNumber", DbType.Int64, unitNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("psReturnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        Aurora.Common.Data.ExecuteOutputSP("RES_UpdateUnitForServiceDue", params)
        returnError = params(1).Value
    End Sub
    'End Added by Nimesh on 06th Aug 2015 - To Update Service Due Flag
    Public Shared Sub VehicleExchange(ByVal bpdId As String, ByVal exchangeWhen As Date, ByVal exchangeLocationCode As String, _
                                ByVal newSapId As String, ByVal exchangeCharge As Double, ByVal noteText As String, _
                                ByVal newUnitNo As String, ByVal newRegoNum As String, ByVal fromOdo As Double, _
                                ByVal toOdo As Double, ByVal userId As String, ByVal programName As String, _
                                ByVal exchangeType As String, ByRef returnError As String)

        Dim params(15) As Aurora.Common.Data.Parameter
        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, bpdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("pdExchWhen", DbType.DateTime, exchangeWhen, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("psExchLocCode", DbType.String, exchangeLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("psNewSapId", DbType.String, newSapId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sXchgCharge", DbType.Currency, exchangeCharge, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("pNteDesc", DbType.String, noteText, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("psNewUnitNo", DbType.String, newUnitNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("sNewRegoNum", DbType.String, newRegoNum, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("piXfrmOdo", DbType.Int64, fromOdo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("piXtoOdo", DbType.Int64, toOdo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("psUsrId", DbType.String, userId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("psProgramName", DbType.String, programName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("psExchangeTyp", DbType.String, exchangeType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        ' OUTPUT parameters
        params(13) = New Aurora.Common.Data.Parameter("psReturnError", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(14) = New Aurora.Common.Data.Parameter("psReturnMessages", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(15) = New Aurora.Common.Data.Parameter("psNewExchBpdIds", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)


        'With oAuroraDAL
        '    Call .appendParameter("@psBpdId", 200, 64, psBpdId)
        '    Call .appendParameter("@pdExchWhen", 7, 64, pdExchWhen)
        '    Call .appendParameter("@psExchLocCode", 200, 64, psExchLocCode)
        '    Call .appendParameter("@psNewSapId", 200, 64, psNewSapId)
        '    Call .appendParameter("@sXchgCharge", 6, 64, pmXchgCharge)
        '    Call .appendParameter("@pNteDesc", 200, 2000, psNteDesc)
        '    Call .appendParameter("@psNewUnitNo", 200, 64, psNewUnitNo)
        '    Call .appendParameter("@sNewRegoNum", 200, 64, psNewRegoNum)
        '    Call .appendParameter("@piXfrmOdo", 3, 30, piXfrmOdo)
        '    Call .appendParameter("@piXtoOdo", 3, 30, piXtoOdo)
        '    Call .appendParameter("@psUsrId", 200, 64, psUsrId)
        '    Call .appendParameter("@psProgramName", 200, 256, psProgramName)
        '    Call .appendParameter("@psExchangeTyp", 200, 256, sExchangeTyp)

        '    Call .appendParameter2("@psReturnError", 200, 500, strError, True)
        '    Call .appendParameter2("@psReturnMessages", 200, 500, sRetMsg, True)
        '    Call .appendParameter2("@psNewExchBpdIds", 200, 500, sRetBpd, True)

        Aurora.Common.Data.ExecuteOutputSP("RES_VehicleExchange", params)
        returnError = params(13).Value

    End Sub
#End Region

#Region "Incidents"


#Region "Call to stored procedures that update callback data for an incident."
    'Mod: Raj Feb 20 2012''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    'Get callbacks for an incident 
    Public Shared Function DBGetIncidentCallbacks(ByVal icdId As String) As DataSet
        Dim params(0) As Aurora.Common.Data.Parameter
        Dim ds As DataSet
        Try
            params(0) = New Aurora.Common.Data.Parameter("icdId", DbType.String, icdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ds = Aurora.Common.Data.ExecuteOutputDatasetSP("Incdnt_IncidentCallBackSelect", params)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving callbacks for an incident - for Incident Maintenance Screen")
        End Try
        Return ds

    End Function

    'Get the current status code for an incident - 'feedback, open, resolved'
    Public Shared Function DBGetStatusCodes(ByVal codCdtCode As Integer) As DataSet

        Dim params(0) As Aurora.Common.Data.Parameter
        Dim ds As DataSet
        Try
            params(0) = New Aurora.Common.Data.Parameter("codCdtNum", DbType.Int32, codCdtCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ds = Aurora.Common.Data.ExecuteOutputDatasetSP("Incdnt_GetIncidentCodes", params)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving status codes 'feedback,resolved, open' - for Incident Maintenance Screen")
        End Try
        Return ds

    End Function

    Public Shared Function DBGetIncidentStatus(ByVal incdtID As String) As DataSet

        Dim params(0) As Aurora.Common.Data.Parameter
        Dim ds As DataSet
        Try
            params(0) = New Aurora.Common.Data.Parameter("incdtID", DbType.String, incdtID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ds = Aurora.Common.Data.ExecuteOutputDatasetSP("Incdnt_GetIncidentStatus", params)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving status code for an incident - for Incident Maintenance Screen")
        End Try
        Return ds

    End Function

    Public Shared Function DBSaveCallbackData(ByVal IncCallBckIcdId As String, ByVal IncCallBckDescription As String) As Int64

        Dim CallbackID As Int64 = 0

        Try
            Dim params(5) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("IncCallBckIcdId", DbType.String, IncCallBckIcdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("IncCallBckDescription", DbType.String, IncCallBckDescription, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim integrityNo As Integer = 1
            params(2) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Int32, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrId
            params(3) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim prgName As String = "IncidentManagement.aspx"
            params(4) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("id", DbType.Int64, CallbackID, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Incdnt_IncidentCallBackInsert", params)
            CallbackID = Convert.ToInt64(params(5).Value)
        Catch exception As Exception
            Throw New Exception("ERROR: Adding entry to 'IncidentCallBack' tables")
        End Try
        Return CallbackID

    End Function


    Public Shared Function DBSaveCallbackStatus(ByVal IncCallBckStIcdId As String, ByVal IncCallBckStatusId As String) As Int64

        Dim CallbackStatusID As Int64 = 0

        Try
            Dim params(5) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("IncCallBckStIcdId", DbType.String, IncCallBckStIcdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("IncCallBckStatusId", DbType.String, IncCallBckStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim integrityNo As Int64 = 1
            params(2) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Int64, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrId
            params(3) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim prgName As String = "IncidentManagement.aspx"
            params(4) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("id", DbType.Int64, CallbackStatusID, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Incdnt_IncidentCallBackStatusInsert", params)
            CallbackStatusID = Convert.ToInt64(params(5).Value)
        Catch exception As Exception
            Throw New Exception("ERROR: Adding entry to 'IncidentCallBackStatus' tables")
        End Try
        Return CallbackStatusID

    End Function

    Public Shared Function DBSaveCallbackStatusDate(ByVal IncCallBckStatusId As String) As Integer

        Dim err As Integer = 0

        Try
            Dim params(4) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("IncCallBckStId", DbType.String, IncCallBckStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim integrityNo As Int64 = 1
            params(1) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Int64, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrId
            params(2) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim prgName As String = "IncidentManagement.aspx"
            params(3) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("err", DbType.Int64, err, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Incdnt_IncidentCallBackStatusDateInsert", params)
            err = Convert.ToInt64(params(4).Value)
        Catch exception As Exception
            Throw New Exception("ERROR: Adding entry to 'IncidentStatusDate' tables")
        End Try
        Return err

    End Function

    Public Shared Function DBGetUsernameforID(ByVal usrID As String) As DataSet

        Dim ds As DataSet
        Try
            Dim params(0) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("usrID", DbType.String, usrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Aurora.Common.Data.ExecuteOutputSP("Incdnt_GetUserNameforID", params)
            ds = Aurora.Common.Data.ExecuteOutputDatasetSP("Incdnt_GetUserNameforID", params)
        Catch exception As Exception
            Throw New Exception("ERROR: Adding getting user name for ID")
        End Try
        Return ds

    End Function
    'Mod: Raj Feb 20 2012''''''''''''''''''''''''''''''''''''''''''''''''''''''

#End Region


    Public Shared Function GetIncidents(ByVal RentalId, ByVal BookingId) As DataTable
        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("IncdntGetForTab", BookingId, RentalId)
        'Return oXmlDoc
        If oXmlDoc.DocumentElement.InnerText.Contains("GEN049") Then
            ' no records
            ' create some dummy row for display purposes

            oXmlDoc.LoadXml("<data>" & _
                                "<CurrIncidents>" & _
                                    "<Incident>" & _
                                        "<IncId />" & _
                                        "<Lodge />" & _
                                         "<Type />" & _
                                         "<Cause />" & _
                                         "<ResolveDtTm />" & _
                                         "<UnitNo />" & _
                                         "<Status />" & _
                                         "<AIMSRefNo />" & _
                                         "<RentalNo />" & _
                                    "</Incident>" & _
                                "</CurrIncidents>" & _
                            "</data>")

        End If

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Dim dtResults As DataTable
        If ds.Tables.Count > 0 Then
            dtResults = ds.Tables("Incident")
        Else
            dtResults = New DataTable
        End If

        Return dtResults

    End Function


    Public Shared Function GetIncident(ByVal BookingId As String, ByVal RentalId As String, ByVal IncidentId As String, ByVal UserCode As String) As XmlDocument
        'incdnt_GetIncident   BookId & "', '" & RntId & "', '" & incidentID & "','" & userCode & "'"
        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_GetIncident", BookingId, RentalId, IncidentId, UserCode, "", "", "", "", 0, "")
        'CREATE        PROCEDURE [dbo].[incdnt_GetIncident]   
        '@sBookId  VARCHAR(64) = '',  
        '@sRntId  VARCHAR(64) = '',  
        '@sIncdntId  VARCHAR(64) = '' ,  
        '@defaultUser VARCHAR(64) = '' ,  
        '@lcdLoggedLocCode VARCHAR  (64) = '',  
        '@icdLoggedWhen VARCHAR  (10) = '',  
        '@icdCodTypeId VARCHAR  (10) = '',  
        '@UicdCodMocId VARCHAR  (64) = '',  
        '@icdOdometer  MONEY  = 0 ,  
        '@sBpdId  VARCHAR  (64) = ''  
        Return oXmlDoc
    End Function


    Public Shared Function GetCodeCodeType(ByVal CodeCDTNum As Int16, Optional ByVal Code As String = "", Optional ByVal Imprint As String = "") As XmlDocument
        'sp_get_codecodetype
        '@sCodcdtnum INT   = DEFAULT, /*Mandatory*/  
        '@sCode  VARCHAR(24) = '',  
        '@sImprint varchar(24) = '' -- will be excluded  
        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_codecodetype", CodeCDTNum, Code, Imprint)
        Return oXmlDoc
    End Function

    Public Shared Function GetCauses(ByVal CauseTypeId As String) As XmlDocument
        'incdnt_GetCauseType
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_GetCauseType", CauseTypeId)
        Return oXml
    End Function

    Public Shared Function GetSubCauses(ByVal SubCauseTypeId As String) As XmlDocument
        Dim oXml As XmlDocument
        'exec incdnt_GetSubCauses '5FC82FA3-A22F-4CC6-A2B1-C936B73E0486'
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_GetSubCauses", SubCauseTypeId)
        Return oXml
    End Function

    Public Shared Function UpdateIncident(ByVal BookingId As String, _
                                          ByVal RentalId As String, _
                                          ByVal IncidentId As String, _
                                          ByVal UnitNumber As String, _
                                          ByVal Type As String, _
                                          ByVal Notified As String, _
                                          ByVal LoggedDate As String, _
                                          ByVal LoggedLocationCode As String, _
                                          ByVal IntegrityNo As String, _
                                          ByVal UserId As String, _
                                          ByVal ProgramName As String) As XmlDocument

        '@sBookingID  VARCHAR(64),  
        '@sRentalId  VARCHAR(64),  
        '@sIncidentId VARCHAR(64),  
        '@sUnitNum  VARCHAR(64) = NULL,  
        '@sType   VARCHAR(64) = NULL,  
        '@sStatus  VARCHAR(64) = NULL,  
        '@sComMode  VARCHAR(64) = NULL,  
        '@LoggedDate  VARCHAR(10) = NULL,  
        '@sLoggLocCode VARCHAR(64) = NULL,  
        '@ResolvDate  VARCHAR(10) = NULL,  
        '@bIsStaisfied VARCHAR(1) = NULL,  
        '@iOdometer  VARCHAR(8) = NULL,  
        '@iIntegrityNo VARCHAR(3) = '1',  
        '@sUserId   VARCHAR(64) = '',  
        '@PrgmName  VARCHAR(64) = ''  


        'Dim arParams(14) As Aurora.Common.Data.Parameter

        'arParams(0) = New Aurora.Common.Data.Parameter("sBookingID", DbType.String, BookingId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(1) = New Aurora.Common.Data.Parameter("sRentalId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(2) = New Aurora.Common.Data.Parameter("sIncidentId", DbType.String, IncidentId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(3) = New Aurora.Common.Data.Parameter("sUnitNum", DbType.String, UnitNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(4) = New Aurora.Common.Data.Parameter("sType", DbType.String, Type, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(5) = New Aurora.Common.Data.Parameter("sStatus", DbType.String, Status, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(6) = New Aurora.Common.Data.Parameter("sComMode", DbType.String, Notified, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(7) = New Aurora.Common.Data.Parameter("LoggedDate", DbType.String, LoggedDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(8) = New Aurora.Common.Data.Parameter("sLoggLocCode", DbType.String, LoggedLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(9) = New Aurora.Common.Data.Parameter("ResolvDate", DbType.String, ResolveDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(10) = New Aurora.Common.Data.Parameter("bIsStaisfied", DbType.String, Staisfied, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(11) = New Aurora.Common.Data.Parameter("iOdometer", DbType.String, Odometer, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(12) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.String, IntegrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(13) = New Aurora.Common.Data.Parameter("sUserId", DbType.String, UserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'arParams(14) = New Aurora.Common.Data.Parameter("PrgmName", DbType.String, ProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'Aurora.Common.Data.ExecuteOutputSP("incdnt_UpdateIncident", arParams)

        Dim oXmlDoc As New XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_UpdateIncident", BookingId, _
                                                                                 RentalId, _
                                                                                 IncidentId, _
                                                                                 UnitNumber, _
                                                                                 Type, _
                                                                                 Notified, _
                                                                                 LoggedDate, _
                                                                                 LoggedLocationCode, _
                                                                                 IntegrityNo, _
                                                                                 UserId, _
                                                                                 ProgramName)
        Return oXmlDoc






    End Function

    Public Shared Function UpdateNote(ByVal NoteId As String, _
                                      ByVal BookingId As String, _
                                      ByVal RentalId As String, _
                                      ByVal IncidentId As String, _
                                      ByVal NoteDescription As String, _
                                      ByVal IntegrityNo As String, _
                                      ByVal UserName As String, _
                                      ByVal ProgramName As String) As XmlDocument
        'incdnt_UpdateNote
        '@pNteId    VARCHAR (64)  = '',  
        '@pNteBooId    VARCHAR (64) = '',  
        '@pNteRntNo    VARCHAR (64)  = '',  
        '@NteIcdId   VARCHAR (64) = '',  
        '--@pNteCodTypId   VARCHAR (64) = '',  
        '@pNteDesc    VARCHAR (2048) = '',  
        '@pIntegrityNo  INT,  
        '@pUserName   VARCHAR (64),  
        '@pAddPrgmName  VARCHAR (64)  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_UpdateNote", NoteId, BookingId, RentalId, IncidentId, NoteDescription, IntegrityNo, UserName, ProgramName)
        Return xmlDoc
    End Function

    Public Shared Function UpdateIncidentSubCause(ByVal IncidentId As String, _
                                                  ByVal SubCauseId As String, _
                                                  ByVal IntrgrityNo As String, _
                                                  ByVal OtherText As String, _
                                                  ByVal UserName As String, _
                                                  ByVal ProgramName As String) As XmlDocument
        'incdnt_UpdateIncidentSubCause
        '@sIncidentId varchar(64) = '',  
        '@sSubCauseId varchar(64) = '',  
        '@iIntegrityNo varchar(3) = '1',  
        '@sOtherText  varchar(1024) = '',  
        '@AddUsrId varchar (64) = '',  
        '@AddPrgmName varchar(64) = ''  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_UpdateIncidentSubCause", IncidentId, SubCauseId, IntrgrityNo, OtherText, UserName, ProgramName)
        Return xmlDoc
    End Function

    Public Shared Function DeleteIncidentSubCause(ByVal IncidentId As String, _
                                                  ByVal SubCauseId As String) As XmlDocument
        ' @sIncidentId varchar(64) = '',  
        ' @sSubCauseId varchar(64) = ''  
        'incdnt_DeleteIncidentSubCause
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_DeleteIncidentSubCause", IncidentId, SubCauseId)
        Return xmlDoc
    End Function


    Public Shared Function GetIncidentType(ByVal Code As String) As String
        'incdnt_getIncidentType
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_getIncidentType", Code)
        Return xmlDoc.DocumentElement.InnerText
    End Function

    Public Shared Function UpdateAIMSReference(ByVal IncidentId As String, ByVal AIMSIncidentNo As String) As XmlDocument
        '@sIncidentId varchar(64),  
        '@IcdAimsIcdNum varchar(64)  
        'incdnt_UpdateAIMSReference
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("incdnt_UpdateAIMSReference", IncidentId, AIMSIncidentNo)
        Return xmlDoc
    End Function

    Public Shared Function GetLocationDescriptionAndIncidentTypeId(ByVal LocationId As String, ByVal IncidentType As String) As XmlDocument
        '     create  PROCEDURE sp_getIncidentLocType  
        '@sLoc    VARCHAR  (64) = '' ,  
        '@sType   VARCHAR  (64) = ''  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getIncidentLocType", LocationId, IncidentType)
        Return xmlDoc
    End Function


#End Region

#Region "Confirmation"

    Public Shared Function GetConfirmationSummary(ByVal BookingId As String, ByVal RentalId As String, ByVal ForAllAgents As Boolean) As DataTable
        'CREATE    PROCEDURE cfn_get_ConfirmationSummary  
        '@sBooId   VARCHAR  (64)  = '' ,  
        '@sRntId   VARCHAR  (64)  = '',  
        '@bforAllAgent   BIT    = 1  

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_get_ConfirmationSummary", BookingId, RentalId, ForAllAgents)
        'Return xmlDoc

        Dim dstConfirmations As DataSet = New DataSet()
        dstConfirmations.ReadXml(New XmlNodeReader(xmlDoc.DocumentElement.SelectSingleNode("Confirmations")))
        If dstConfirmations.Tables.Count > 0 Then
            Return dstConfirmations.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function GetDataForConfirmation(ByVal BookingId As String, ByVal ConfirmationId As String) As XmlDocument 'DataSet
        'PROCEDURE cfn_getRentalsFromBooking  
        '@sBooId varchar(64) ='',  
        '@sCfnId varchar(64)=''  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_getRentalsFromBooking", BookingId, ConfirmationId)
        Return xmlDoc
        'Dim dstMain As DataSet = New DataSet

        'dstMain.ReadXml(New XmlNodeReader(xmlDoc.DocumentElement))

        'Return dstMain

    End Function


    Public Shared Function GetConfirmationNoteSpec(ByVal CountryCode As String, ByVal ConfirmationId As String, ByVal UserCode As String) As DataTable

        'CREATE PROCEDURE [dbo].[cfn_get_NoteSpec]   
        '@sCtyCode       varchar(12) ,  
        '@scfnId varchar(64),  
        '@sUsrCode varchar(64) = ''  

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_get_NoteSpec", CountryCode, ConfirmationId, UserCode)
        'Return xmlDoc

        Dim dstConfirmations As DataSet = New DataSet()
        dstConfirmations.ReadXml(New XmlNodeReader(xmlDoc.DocumentElement))
        If dstConfirmations.Tables.Count > 0 Then
            Return dstConfirmations.Tables(0)
        Else
            Return New DataTable
        End If

    End Function


    Public Shared Function GetRentalCountry(ByVal RentalId As String) As String
        'cfn_getRentalCountry
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_getRentalCountry", RentalId)
        If xmlDoc.DocumentElement.SelectSingleNode("TownCity/TctCtyCode") Is Nothing Then
            Return ""
        Else
            Return xmlDoc.DocumentElement.SelectSingleNode("TownCity/TctCtyCode").InnerText
        End If


    End Function


    Public Shared Function CreateUpdateConfirmations( _
                            ByVal ConfirmationId As String, _
                            ByVal BookingId As String, _
                            ByVal RentalId As String, _
                            ByVal RentalNumber As String, _
                            ByVal AudienceType As String, _
                            ByVal HeaderText As String, _
                            ByVal FooterText As String, _
                            ByVal EmailId As String, _
                            ByVal FaxNumber As String, _
                            ByVal OutputType As String, _
                            ByVal FileName As String, _
                            ByVal Highlight As Boolean, _
                            ByVal IntegrityNumber As Integer, _
                            ByVal AddUserId As String, _
                            ByVal AddModifyUserId As String, _
                            ByVal AddModifyProgramName As String, _
                            ByVal Action As String
                            ) As XmlDocument

        '        'CREATE            PROCEDURE cfn_update_confirmations 
        '0:      '@sCfnId		         	varchar(64),    
        '1:      '@sBooId	         	    varchar(64),   
        '2:      '@sRntId		         	varchar(64),    
        '3:      '@sRntNum	         	varchar(64),   
        '4:      '@sAudType		        varchar(64),    
        '5:      '@sHead	         	    varchar(1000),   
        '6:      '@sFoot			        varchar(1000),    
        '7:      '@sEmail	         	    varchar(128),   
        '8:      '@sFax			        varchar(128),    
        '9:      '@sOutputType	        varchar(64),   
        '10:     '@sFileName		        varchar(256),
        '11:     '@bHighlight				BIT,    
        '12:     '@IntegrityNo        	int,                   
        '13:     '@sAdduserId				varchar(64),   	 
        '14:     '@sAddModuserId          varchar(64),    
        '15:     '@sAddProgramName  	    varchar(64) ,
        '16:     '@sAction                varchar(3)     


        'Dim params(16) As Aurora.Common.Data.Parameter

        'params(0) = New Aurora.Common.Data.Parameter("sCfnId", DbType.String, ConfirmationId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(1) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, BookingId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(2) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(3) = New Aurora.Common.Data.Parameter("sRntNum", DbType.String, RentalNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'params(4) = New Aurora.Common.Data.Parameter("sAudType", DbType.String, AudienceType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(5) = New Aurora.Common.Data.Parameter("sHead", DbType.String, HeaderText, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(6) = New Aurora.Common.Data.Parameter("sFoot", DbType.String, FooterText, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(7) = New Aurora.Common.Data.Parameter("sEmail", DbType.String, EmailId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'params(8) = New Aurora.Common.Data.Parameter("sFax", DbType.String, FaxNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(9) = New Aurora.Common.Data.Parameter("sOutputType", DbType.String, OutputType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(10) = New Aurora.Common.Data.Parameter("sFileName", DbType.String, FileName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(11) = New Aurora.Common.Data.Parameter("bHighlight", DbType.Boolean, Highlight, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'params(12) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int32, IntegrityNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(13) = New Aurora.Common.Data.Parameter("sAdduserId", DbType.String, AddUserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(14) = New Aurora.Common.Data.Parameter("sAddModuserId", DbType.String, AddModifyUserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(15) = New Aurora.Common.Data.Parameter("sAddProgramName", DbType.String, AddModifyProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'params(16) = New Aurora.Common.Data.Parameter("sAction", DbType.String, Action, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        'Try
        '    Aurora.Common.Data.ExecuteOutputSP("cfn_update_confirmations", params)
        '    Return True
        'Catch ex As Exception
        '    Return False
        'End Try

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_update_confirmations", ConfirmationId, _
                                                                                   BookingId, _
                                                                                   RentalId, _
                                                                                   RentalNumber, _
                                                                                   AudienceType, _
                                                                                   HeaderText, _
                                                                                   FooterText, _
                                                                                   EmailId, _
                                                                                   FaxNumber, _
                                                                                   OutputType, _
                                                                                   FileName, _
                                                                                   Highlight, _
                                                                                   IntegrityNumber, _
                                                                                   AddUserId, _
                                                                                   AddModifyUserId, _
                                                                                   AddModifyProgramName, _
                                                                                   Action)


        Return xmlDoc



    End Function

    ''rev:mia Oct.15 2012 - addition of NoteCfnBrand 
    Public Shared Function InsertDeleteNotesForConfirmation( _
                            ByVal ConfirmationId As String, _
                            ByVal NoteId As String, _
                            ByVal AddUserId As String, _
                            ByVal AddModifyUserId As String, _
                            ByVal AddProgramName As String, _
                            ByVal Action As String, _
                            Optional NoteCfnBrand As String = "") As XmlDocument

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_update_confirmationNoteSpec", ConfirmationId, NoteId, AddUserId, _
                                                                                   AddModifyUserId, AddProgramName, Action, NoteCfnBrand)

        Return xmlDoc

    End Function

    Public Shared Function UpdateCkiCkoForConfirmation(ByVal ConfirmationId As String, ByVal CkoLocation As String, ByVal CkiLocation As String) As XmlDocument
        'cfn_update_OnlyAddressDetail 
        'CREATE  PROCEDURE cfn_update_OnlyAddressDetail
        '	@sCfnId				VARCHAR	 (64) ,
        '	@sCfnCkoAddress		VARCHAR  (512) ,
        '	@sCfnCkiAddress		VARCHAR	 (512)

        Dim xmlDoc As New XmlDocument
        Dim sRetString As String
        sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("cfn_update_OnlyAddressDetail", ConfirmationId, CkoLocation, CkiLocation)
        xmlDoc.LoadXml(sRetString)
        Return xmlDoc

    End Function

    ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
    Public Shared Function GenerateConfirmationReport(ByVal ConfirmationId As String, _
                                                      ByVal ShowAllRentals As Boolean, _
                                                      Optional RentalExclustion As String = "") As XmlDocument

        Dim xmlDoc As New XmlDocument
        Dim sRetString As String
        Try
            sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("RES_Generate_Confirmation_Report", _
                                                            ConfirmationId, _
                                                            ShowAllRentals, _
                                                            RentalExclustion)

            xmlDoc.LoadXml(sRetString)

        Catch ex As Exception
            ''rev:mia feb.23 2011, for some unknown reason..there are some duplicates value in the root element
            If sRetString.Contains("<data><data>") Then
                sRetString = sRetString.Replace("<data><data>", "<data>")
            End If
            If sRetString.Contains("</data></data>") Then
                sRetString = sRetString.Replace("</data></data>", "</data>")
            End If
            xmlDoc.LoadXml(sRetString)
        End Try

        Return xmlDoc

    End Function

    'cfn_getFileName
    Public Shared Function GetFileNameForConfirmation(ByVal ConfirmationId As String) As String
        Dim xmlDoc As New XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_getFileName", ConfirmationId)
        If Not xmlDoc.DocumentElement.SelectSingleNode("Confirmations/CfnFileName") Is Nothing Then
            Return xmlDoc.DocumentElement.SelectSingleNode("Confirmations/CfnFileName").InnerText
        Else
            Return ""
        End If

    End Function

    'cfn_getInfo_efax
    Public Shared Function GetPathsForConfirmation(ByVal ConfirmationId As String) As XmlDocument
        Dim xmlDoc As New XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_getInfo_efax", ConfirmationId)
        Return xmlDoc

    End Function

    Public Shared Function UpdateResendConfirmation(ByVal ConfirmationId As String, ByVal Email As String, ByVal FaxNumber As String, ByVal OutputType As String, ByVal AddUserId As String, ByVal AddProgramName As String, ByVal Parameter As String) As Boolean

        '           CREATE      PROCEDURE cfn_update_reSend  
        '@sCfnId  varchar(64),  
        '@sEmai  varchar(64),                    
        '@sFax  varchar(64),               
        '@sOutputType varchar(64),  
        '@sAddUsrId varchar(64),   
        '@sAddPrgmName varchar(256),  
        '@sParam  varchar(256)  

        Dim params(6) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sCfnId", DbType.String, ConfirmationId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sEmai", DbType.String, Email, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sFax", DbType.String, FaxNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(3) = New Aurora.Common.Data.Parameter("sOutputType", DbType.String, OutputType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sAddUsrId", DbType.String, AddUserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, AddProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sParam", DbType.String, Parameter, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        Try
            Aurora.Common.Data.ExecuteOutputSP("cfn_update_reSend", params)
            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Shared Function GetPreviouslySavedFileName(ByVal ConfirmationId As String) As XmlDocument
        ' exec cfn_getFileName '7DA8941F74704A338AD495E0C62A1E84'
        ' <Confirmations><CfnFileName>Confirmations\3BF2A08E-2D24-44A7-B39C-E383472AB428.htm</CfnFileName></Confirmations>
        Dim xmlDoc As New XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cfn_getFileName", ConfirmationId)
        Return xmlDoc
    End Function

#End Region

#Region "Infringement"

    Public Shared Function GetInfringements(ByVal isAll As Boolean, ByVal bookingId As String, _
        ByVal rentalId As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_InfringementForGet", isAll, bookingId, rentalId)

        If oXmlDoc.DocumentElement.InnerText.Contains("GEN049") Then
            ' no records
            ' create some dummy row for display purposes
            oXmlDoc.LoadXml("<data>" & _
                                "<CurInfringements>" & _
                                "<Infringement>" & _
                                        "<IcdId />" & _
                                        "<OffenceDtTm />" & _
                                         "<Cause />" & _
                                         "<UnitNo />" & _
                                         "<Cadvised />" & _
                                         "<Status />" & _
                                         "<RntNum />" & _
                                "</Infringement>" & _
                                "</CurInfringements>" & _
                            "</data>")
        End If


        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

    Public Shared Function GetInfringement(ByVal icdId As String, ByVal bookingId As String, _
        ByVal rentalId As String, ByVal userCode As String) As XmlDocument

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_Infringement", icdId, bookingId, _
            rentalId, userCode)

        'Dim ds As DataSet = New DataSet()
        'ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return oXmlDoc

    End Function

    Public Shared Function GetDataFromRentalId(ByVal rentalId As String, ByVal icdId As String, _
       ByVal id As String, ByVal type As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_DataFromRentalId", rentalId, icdId, id, type)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

    Public Shared Function ManageInfringement(ByVal xmlDoc As XmlDocument) As String

        Dim sRetString As String
        sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("ManageInfringement", xmlDoc.InnerXml)
        Return sRetString

    End Function

#End Region

#Region "BookingMatchProduct"

    Public Shared Function GetMatchProducts(ByVal bookingId As String, ByVal rentalId As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getMatchProducts", bookingId, rentalId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

    Public Shared Function UpdateMatchProducts(ByVal xmlDoc As XmlDocument) As String

        Dim sRetString As String
        sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("RES_UpdateMatchProducts", xmlDoc.InnerXml)
        Return sRetString

    End Function

#End Region

#Region "BookingCancellation"

    Public Shared Function GetCancellationDetail(ByVal bookingId As String, ByVal rentalId As String, _
        ByVal userCode As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getCancellationDetail", bookingId, rentalId, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

    Public Shared Function GetIntegrityNoForRnt(ByVal rentalId As String, _
        ByVal bpdId As String, ByVal integrityNo As Integer) As String

        Dim errorReturn As String

        Dim params(3) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, rentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int32, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sErrorRet", DbType.String, 4000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("getIntegrityNoForRnt", params)
        errorReturn = params(3).Value

        Return errorReturn

    End Function

    Public Shared Function ManageCancelRental(ByVal bookingId As String, ByVal rentalId As String, _
        ByVal cancelWhen As String, ByVal reasonId As String, ByVal mocId As String, _
        ByVal typeId As String, ByVal isNoteRequired As Boolean, ByVal userCode As String, _
        ByVal programName As String) As String

        Return Aurora.Common.Data.ExecuteSqlXmlSP("RES_manageCancelRental", bookingId, rentalId, _
            cancelWhen, reasonId, mocId, typeId, isNoteRequired, userCode, programName)

    End Function

    Public Shared Function GetAllRentalFORBooking(ByVal bookingId As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getAllRentalFORBooking", bookingId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

    Public Shared Function ManageCancelBooking(ByVal bookingId As String, ByVal userCode As String, _
            ByVal programName As String) As String

        Return Aurora.Common.Data.ExecuteSqlXmlSP("RES_manageCancelBooking", bookingId, userCode, _
            programName)

    End Function

#End Region


#Region "BookingVehicle"

    Public Shared Function GetVehicle(ByVal bookingId As String, ByVal rentalId As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("tab_getVehicle", bookingId, rentalId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

#End Region

#Region "Booked Product Search"

    Public Shared Function DoBookedProductSearch(ByVal BookingId As String, ByVal RentalId As String, _
    ByVal BookedProductStatus As String, ByVal BookedProductInitialStatus As String, _
    ByVal ProductShortName As String, ByVal ProductClassDescription As String, _
    ByVal TypeDescription As String, ByVal CheckOutDate As String, ByVal CheckOutLocationCode As String, _
    ByVal CheckInDate As String, ByVal CheckInLocationCode As String, ByVal BookedProductCodeTypeId As String, _
    ByVal AgencyReference As String, ByVal ChargeTo As String, ByVal IsCurrent As String, _
    ByVal IsOriginal As String, ByVal IsFOCProduct As String, ByVal IsTransferredToFinance As String, _
    ByVal RecordId As String, ByVal UserCode As String) As XmlDocument

        ' exec BookedProductSearch @sBooId = '3333212', @sRntID = '3333212-1', @BpdStatus = 'KK', @BpdIniStatus = 'IN', _
        ' @PrdShortName = 'ICAR', @ClaDesc = 'AC', @TypDesc = 'MC', @RntCkoWhen = '01/01/2001', @RntCkoLocCode = 'AKL', _
        ' @RntCkiWhen = '01/01/2002', @RntCkiLocCode = 'AKL', @BpdCodTypeId = 'LER', @RntAgencyRef = '12345', _
        '@ChargeTo = '0', @IsCurrent = '1', @IsOriginal = '0', @IsFOCProducts = '0', @IsTransToFinan = '0', _
        '@RecordId = '098765', @sUserCode = 'sp7'

        'CREATE    PROCEDURE [dbo].[BookedProductSearch]   
        '@sBooId   VARCHAR(64),  
        '@sRntID   VARCHAR(64) = NULL,    
        '@BpdStatus  VARCHAR(64) = '',  
        '@BpdIniStatus VARCHAR(64) = '',  
        '@PrdShortName VARCHAR(8) = '',  
        '@ClaDesc  VARCHAR(64) = '',  
        '@TypDesc  VARCHAR(64) = '',  
        '@RntCkoWhen  VARCHAR(10) = '',  
        '@RntCkoLocCode VARCHAR(12) = '',  
        '@RntCkiWhen  VARCHAR(10) = '',  
        '@RntCkiLocCode VARCHAR(12) = '',  
        '@BpdCodTypeId VARCHAR(64) = '',  
        '@RntAgencyRef VARCHAR(64) = '',  
        '@ChargeTo  VARCHAR(1) = '',  
        '@IsCurrent  VARCHAR(1) = '',  
        '@IsOriginal  VARCHAR(1) = '',  
        '@IsFOCProducts VARCHAR(1) = '',  
        '@IsTransToFinan VARCHAR(1) = '',  
        '@RecordId    VARCHAR(64) = '',  
        '-- KX Changes :RKS  
        '@sUserCode  VARCHAR(64) = ''  

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("BookedProductSearch", BookingId, RentalId, BookedProductStatus, _
                                                       BookedProductInitialStatus, ProductShortName, ProductClassDescription, _
                                                       TypeDescription, CheckOutDate, CheckOutLocationCode, _
                                                       CheckInDate, CheckInLocationCode, BookedProductCodeTypeId, _
                                                       AgencyReference, ChargeTo, IsCurrent, IsOriginal, IsFOCProduct, _
                                                       IsTransferredToFinance, RecordId, UserCode)

        Return xmlDoc
    End Function


#End Region

#Region "Booked Product Mgt"

    Public Shared Function GetMaintainBookedProduct(ByVal bpdId As String) As XmlDocument

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getMaintainBookedProduct", bpdId)

        'Dim ds As DataSet = New DataSet()
        'ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        'Return ds
        Return oXmlDoc

    End Function

    Public Shared Sub ManageMaintainBookedProductPartA(ByVal xmlDoc As XmlDocument, ByVal userCode As String, _
        ByVal override As Boolean, ByVal oldUserCode As String, ByRef retSapId As String, _
        ByRef messages As String, ByRef errors As String)

        Dim params(7) As Aurora.Common.Data.Parameter
        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("XMLData", DbType.String, xmlDoc.OuterXml, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("bOverride", DbType.Boolean, override, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sOldUsrCode", DbType.String, oldUserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sNewSapid", DbType.String, retSapId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(5) = New Aurora.Common.Data.Parameter("sRetSapId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(6) = New Aurora.Common.Data.Parameter("sMessages", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(7) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("manageMaintainBookedProductPartA", params)
        retSapId = params(5).Value
        messages = params(6).Value
        errors = params(7).Value

        '@XMLData   TEXT   = DEFAULT,  
        '@sUserCode   VARCHAR  (64) = '',  
        '@bOverride   BIT    = 0,    
        '@sOldUsrCode  VARCHAR  (64) = NULL ,  
        '@sRetSapId   VARCHAR  (64)  OUTPUT ,    
        '@sMessages   VARCHAR  (2000)  OUTPUT ,   
        '@sErrors   VARCHAR  (2000)  OUTPUT  

    End Sub

    'Public Shared Function DeleteTProcessedBpds(ByVal rentalId As String) As String

    '    Return Aurora.Common.Data.ExecuteSqlXmlSP("RES_DeleteTProcessedBpds", rentalId)

    'End Function


    Public Shared Sub ValiateBookedProductForSurCharge(ByVal bpdId As String, ByVal ckoLocCode As String, _
       ByVal ckiLocCode As String, ByVal oldCkoLocCode As String, ByVal oldCkiLocCode As String, _
       ByVal ckoWhen As DateTime, ByVal ckiWhen As DateTime, ByVal oldCkoWhen As DateTime, _
       ByVal oldCkiWhen As DateTime, ByVal first As Boolean, ByVal userCode As String, ByRef errors As String)

        Dim params(11) As Aurora.Common.Data.Parameter
        ' INPUT parameters
        params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, bpdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCkoLocCode", DbType.String, ckoLocCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sCkiLocCode", DbType.String, ckiLocCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sOldCkoLocCode", DbType.String, oldCkoLocCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sOldCkiLocCode", DbType.String, oldCkiLocCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("dCkoWhen", DbType.DateTime, ckoWhen, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("dCkiWhen", DbType.DateTime, ckiWhen, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("dOldCkoWhen", DbType.DateTime, oldCkoWhen, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(8) = New Aurora.Common.Data.Parameter("dOldCkiWhen", DbType.DateTime, oldCkiWhen, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("bFirst", DbType.Binary, first, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("sUsrId", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        ' OUTPUT parameters
        params(11) = New Aurora.Common.Data.Parameter("sErrStr", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("RES_ValiateBookedProductForSurCharge", params)
        errors = params(11).Value

        '@psBpdId	varchar	64
        '@sCkoLocCode	varchar	64
        '@sCkiLocCode	varchar	64
        '@sOldCkoLocCode	varchar	64
        '@sOldCkiLocCode	varchar	64
        '@dCkoWhen	datetime	8
        '@dCkiWhen	datetime	8
        '@dOldCkoWhen	datetime	8
        '@dOldCkiWhen	datetime	8
        '@bFirst	bit	1
        '@sUsrId	varchar	64
        '@sErrStr	varchar	2000

    End Sub

    Public Shared Sub ManageBookedProductAgentChange(ByVal rentalId As String, ByVal agentId As String, _
        ByVal userCode As String, ByVal programName As String, ByVal exclBpdId As String, _
        ByVal rnhId As String, ByVal bpdId As String, ByVal newIsCusCharge As String, _
        ByRef errors As String, ByRef messages As String, ByRef newBpdIdsList As String)

        Dim params(10) As Aurora.Common.Data.Parameter
        Try


            ' INPUT parameters
            params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, rentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sAgnId", DbType.String, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("sUsrId", DbType.String, userCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, programName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("sExclBpdId", DbType.String, exclBpdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("rRnhId", DbType.String, rnhId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, bpdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            'params(7) = New Aurora.Common.Data.Parameter("sNewIsCusCharge", DbType.Binary, newIsCusCharge, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("sNewIsCusCharge", DbType.Boolean, CBool(newIsCusCharge), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)


            ' OUTPUT parameters
            params(8) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 500, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            params(9) = New Aurora.Common.Data.Parameter("sMessages", DbType.String, 200, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            ''params(10) = New Aurora.Common.Data.Parameter("sNewBpdIdsList", DbType.String, 8000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            params(10) = New Aurora.Common.Data.Parameter("sNewBpdIdsList", DbType.String, 4000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("RES_ManageBookedProductAgentChange", params)
            errors = params(8).Value
            messages = params(9).Value
            'newBpdIdsList = params(10).Value

            '@sRntId    VARCHAR (64) ,  
            '@sAgnId    VARCHAR (64)  = NULL ,  -- Added  
            '@sUsrId    VARCHAR (64) ,   
            '@sPrgName   VARCHAR (256),  
            '@sExclBpdId   VARCHAR (64)  = NULL ,   
            '@rRnhId    VARCHAR (64)  = NULL,  
            '@sBpdId    VARCHAR (64)  =   NULL,  
            '@sNewIsCusCharge BIT     =   NULL,  
            '@sErrors   VARCHAR (500)  OUTPUT ,  
            '@sMessages   VARCHAR (200)  OUTPUT ,  
            '@sNewBpdIdsList  VARCHAR (8000)  OUTPUT  

        Catch ex As Exception
            Throw New Exception("Error Occured in ManageBookedProductAgentChange")
            Logging.LogError("ManageBookedProductAgentChange", ex.Message)
        End Try
    End Sub

    Public Shared Function UpdateRntPrdReqVehId(ByVal retBpd As String, ByVal rentalId As String) As String

        Return Aurora.Common.Data.ExecuteSqlXmlSP("RES_updateRntPrdReqVehId", retBpd, DBNull.Value)

    End Function


#End Region

    Public Shared Function RntGetFollowUpDate(ByVal ckoDate As Date, ByVal ckoLocationCode As String, ByVal status As String) As String

        Dim ckoDateString As String
        If ckoDate = Date.MinValue Then
            ckoDateString = ""
        Else
            ckoDateString = Utility.DateDBToString(ckoDate) '.ToShortDateString()
        End If

        Return Aurora.Common.Data.ExecuteScalarSP("RES_RntGetFollowUpDate", ckoDateString, ckoLocationCode, status)

    End Function

#Region "THRIFTY HISTORY"

    ''' <summary>
    ''' This returns the Thrifty History for a Rental, for display in the booking.aspx page
    ''' </summary>
    ''' <param name="BookingNumber">Must be in XXXXXX/Y format</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetThriftyHistory(ByVal RentalNumber As String) As DataTable
        Dim dsResult As DataSet
        dsResult = Aurora.Common.Data.ExecuteDataSetSP("RES_GetThriftyHistory", New DataSet, RentalNumber)
        If dsResult.Tables.Count > 0 AndAlso dsResult.Tables(0).Rows.Count > 0 Then
            Return dsResult.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    ''' <summary>
    ''' This acts as a wrapper for the sp [GEN_IsThrifty], and does all the work with just a Rental Id
    ''' </summary>
    ''' <param name="RentalId">Rental Id in question</param>
    ''' <param name="ReturnError">Optional ref param that shows the error message, if any</param>
    ''' <param name="ThriftyLocType">Optional ref param that shows the thrifty loc type, if any</param>
    ''' <returns>A boolean value that says if the rental is a Thrifty rental or not</returns>
    ''' <remarks></remarks>
    Public Shared Function IsRentalThrifty(ByVal RentalId As String, Optional ByRef ReturnError As String = "", Optional ByRef ThriftyLocType As String = "") As Boolean
        'CREATE PROC RES_IsRentalThrifty 
        '@sRentalId			VARCHAR(64)
        '--	Output Parameters
        ',@bIsThrifty		BIT				OUTPUT
        ',@sErrors 			VARCHAR(5000)	OUTPUT
        ',@sThriftyLocType	VARCHAR(150)	OUTPUT

        'Dim params(3) As Aurora.Common.Data.Parameter
        'params(0) = New Aurora.Common.Data.Parameter("sRentalId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        'params(1) = New Aurora.Common.Data.Parameter("bIsThrifty", DbType.Boolean, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        'params(2) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 5000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        'params(3) = New Aurora.Common.Data.Parameter("sThriftyLocType", DbType.String, 150, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        'Aurora.Common.Data.ExecuteOutputSP("RES_IsRentalThrifty", params)
        'ReturnError = params(2).Value
        'ThriftyLocType = params(3).Value

        'Return CBool(params(1).Value)

        Dim xmlDoc As New XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_IsRentalThrifty", RentalId)

        Dim bIsThrifty As Boolean = False

        ' check the xml doc for data
        With xmlDoc.DocumentElement.FirstChild
            If Not .Attributes("ISTHRIFTY") Is Nothing Then
                bIsThrifty = CBool(.Attributes("ISTHRIFTY").Value)
            End If
            If Not .Attributes("ERRORS") Is Nothing Then
                ReturnError = .Attributes("ERRORS").Value
            End If
            If Not .Attributes("THRIFTYLOCTYPE") Is Nothing Then
                ThriftyLocType = .Attributes("THRIFTYLOCTYPE").Value
            End If
        End With
        ' check the xml doc for data

        Return bIsThrifty
    End Function

#End Region

#Region "Aurora WebService Methods"

#Region "Confirmation Mailing"

    Public Shared Function GetCustomerEmail(ByVal RentalId As String) As String
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("WEBP_GetCustomerEmail", RentalId)
        If xmlDoc IsNot Nothing _
           AndAlso _
           xmlDoc.OuterXml.Contains("EMAIL") _
           AndAlso _
           Not xmlDoc.DocumentElement.SelectSingleNode("Customer/EMAIL").InnerText.Trim().Equals(String.Empty) _
        Then
            Return xmlDoc.DocumentElement.SelectSingleNode("Customer/EMAIL").InnerText.Trim()
        Else
            Throw New Exception("Customer e-mail id not found")
            Return String.Empty
        End If
    End Function

#End Region

#End Region

#Region "Complaints Tab"

    Public Shared Function GetComplaintSummary(ByVal RentalId As String) As DataTable
        Dim dtResult As New DataTable

        Dim dstResult As New DataSet
        dstResult = Common.Data.ExecuteDataSetSP("Complaints_GetComplaintSummary", dstResult, RentalId)


        If dstResult IsNot Nothing AndAlso dstResult.Tables.Count > 0 Then
            If dstResult.Tables(0).Rows.Count = 0 Then
                Dim drDummyRow As DataRow = dstResult.Tables(0).NewRow()
                drDummyRow.Item("LocName") = ""
                drDummyRow.Item("CmpId") = -1
                drDummyRow.Item("RntId") = ""
                dstResult.Tables(0).Rows.Add(drDummyRow)
            End If
            dtResult = dstResult.Tables(0)
        End If

        Return dtResult
    End Function

    Public Shared Function GetComplaintData(ByVal ComplaintId As Int64, ByVal RentalId As String, ByVal sUsrCode As String) As DataSet
        Dim dstResult As New DataSet
        '-- Call 31092: Complaint tab enhancement
        '-- RKS : 18-Nov-2010
        '-- Added usercode for GerComplaintData to valiate role for editing
        '-- Changed FROM 
        '--               dstResult = Common.Data.ExecuteDataSetSP("Complaints_GetComplaint", dstResult, ComplaintId, RentalId)
        '--         TO
        '--               dstResult = Common.Data.ExecuteDataSetSP("Complaints_GetComplaint", dstResult, ComplaintId, RentalId, sUsrCode)
        dstResult = Common.Data.ExecuteDataSetSP("Complaints_GetComplaint", dstResult, ComplaintId, RentalId, sUsrCode)
        Return dstResult
    End Function

    Public Shared Function CreateUpdateComplaintData(ByVal CmpId As Int64, _
                                                     ByVal CmpRntId As String, _
                                                     ByVal CmpLocCode As String, _
                                                     ByVal CmpOwnerGroupRolId As String, _
                                                     ByVal CmpOwnerUserUsrCode As String, _
                                                     ByVal CmpDateReceived As DateTime, _
                                                     ByVal CmpDateResolved As DateTime, _
                                                     ByVal CmpStatus As Int16, _
                                                     ByVal CmpStatHols As Int16, _
                                                     ByVal CmpComplaintSummary As String, _
                                                     ByVal CmpResolutionSummary As String, _
                                                     ByVal CmpOriginalsHeldAt As String, _
                                                     ByVal CmpPrimaryRootCause As String, _
                                                     ByVal CmpPrimarySubCause As String, _
                                                     ByVal CmpPrimaryDetail As String, _
                                                     ByVal CmpSecondaryRootCause As String, _
                                                     ByVal CmpSecondarySubCause As String, _
                                                     ByVal CmpSecondaryDetail As String, _
                                                     ByVal CmpTertiaryRootCause As String, _
                                                     ByVal CmpTertiarySubCause As String, _
                                                     ByVal CmpTertiaryDetail As String, _
                                                     ByVal CmpCompensationPaidtoCustomer As Double, _
                                                     ByVal CmpCompensationCreditNote As Double, _
                                                     ByVal CmpCompensationFOCDays As Int16, _
                                                     ByVal CmpCompensationOther As String, _
                                                     ByVal CmpActivityId As Int32, _
                                                     ByVal CmpNotified As String, _
                                                     ByVal CmpHandlerUsrCode As String, _
                                                     ByVal AddUsrId As String, _
                                                     ByVal ModUsrId As String, _
                                                     ByVal AddPrgmName As String, _
                                                     ByVal IntegrityNo As Int16, _
                                                     ByRef ErrorMessage As String) As Int64

        Dim params(33) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("nCmpId", DbType.Int64, CmpId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCmpRntId", DbType.String, CmpRntId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sCmpLocCode", DbType.String, CmpLocCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sCmpOwnerGroupRolId", DbType.String, CmpOwnerGroupRolId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sCmpOwnerUserUsrCode", DbType.String, CmpOwnerUserUsrCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("dtCmpDateReceived", DbType.DateTime, CmpDateReceived, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        If CmpDateResolved = DateTime.MinValue Then
            params(6) = New Aurora.Common.Data.Parameter("dtCmpDateResolved", DbType.DateTime, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(6) = New Aurora.Common.Data.Parameter("dtCmpDateResolved", DbType.DateTime, CmpDateResolved, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        params(7) = New Aurora.Common.Data.Parameter("nCmpStatus", DbType.Int16, CmpStatus, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        If CmpStatHols = -1 Then
            params(8) = New Aurora.Common.Data.Parameter("nCmpStatHols", DbType.Int16, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(8) = New Aurora.Common.Data.Parameter("nCmpStatHols", DbType.Int16, CmpStatHols, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If


        params(9) = New Aurora.Common.Data.Parameter("sCmpComplaintSummary", DbType.String, CmpComplaintSummary, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        If String.IsNullOrEmpty(CmpResolutionSummary) Then
            params(10) = New Aurora.Common.Data.Parameter("sCmpResolutionSummary", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(10) = New Aurora.Common.Data.Parameter("sCmpResolutionSummary", DbType.String, CmpResolutionSummary, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        params(11) = New Aurora.Common.Data.Parameter("sCmpOriginalsHeldAt", DbType.String, CmpOriginalsHeldAt, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(12) = New Aurora.Common.Data.Parameter("sCmpPrimaryRootCause", DbType.String, CmpPrimaryRootCause, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("sCmpPrimarySubCause", DbType.String, CmpPrimarySubCause, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        If String.IsNullOrEmpty(CmpPrimaryDetail) Then
            params(14) = New Aurora.Common.Data.Parameter("sCmpPrimaryDetail", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(14) = New Aurora.Common.Data.Parameter("sCmpPrimaryDetail", DbType.String, CmpPrimaryDetail, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpSecondaryRootCause) Then
            params(15) = New Aurora.Common.Data.Parameter("sCmpSecondaryRootCause", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(15) = New Aurora.Common.Data.Parameter("sCmpSecondaryRootCause", DbType.String, CmpSecondaryRootCause, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpSecondarySubCause) Then
            params(16) = New Aurora.Common.Data.Parameter("sCmpSecondarySubCause", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(16) = New Aurora.Common.Data.Parameter("sCmpSecondarySubCause", DbType.String, CmpSecondarySubCause, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpSecondaryDetail) Then
            params(17) = New Aurora.Common.Data.Parameter("sCmpSecondaryDetail", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(17) = New Aurora.Common.Data.Parameter("sCmpSecondaryDetail", DbType.String, CmpSecondaryDetail, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpTertiaryRootCause) Then
            params(18) = New Aurora.Common.Data.Parameter("sCmpTertiaryRootCause", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(18) = New Aurora.Common.Data.Parameter("sCmpTertiaryRootCause", DbType.String, CmpTertiaryRootCause, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpTertiarySubCause) Then
            params(19) = New Aurora.Common.Data.Parameter("sCmpTertiarySubCause", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(19) = New Aurora.Common.Data.Parameter("sCmpTertiarySubCause", DbType.String, CmpTertiarySubCause, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpTertiaryDetail) Then
            params(20) = New Aurora.Common.Data.Parameter("sCmpTertiaryDetail", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(20) = New Aurora.Common.Data.Parameter("sCmpTertiaryDetail", DbType.String, CmpTertiaryDetail, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If CmpCompensationPaidtoCustomer = -1 Then
            params(21) = New Aurora.Common.Data.Parameter("dCmpCompensationPaidtoCustomer", DbType.Double, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(21) = New Aurora.Common.Data.Parameter("dCmpCompensationPaidtoCustomer", DbType.Double, CmpCompensationPaidtoCustomer, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If CmpCompensationCreditNote = -1 Then
            params(22) = New Aurora.Common.Data.Parameter("dCmpCompensationCreditNote", DbType.Double, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(22) = New Aurora.Common.Data.Parameter("dCmpCompensationCreditNote", DbType.Double, CmpCompensationCreditNote, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If CmpCompensationFOCDays = -1 Then
            params(23) = New Aurora.Common.Data.Parameter("nCmpCompensationFOCDays", DbType.Int16, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(23) = New Aurora.Common.Data.Parameter("nCmpCompensationFOCDays", DbType.Int16, CmpCompensationFOCDays, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(CmpCompensationOther) Then
            params(24) = New Aurora.Common.Data.Parameter("sCmpCompensationOther", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(24) = New Aurora.Common.Data.Parameter("sCmpCompensationOther", DbType.String, CmpCompensationOther, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        params(25) = New Aurora.Common.Data.Parameter("nCmpActivityId", DbType.Int32, CmpActivityId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(26) = New Aurora.Common.Data.Parameter("sCmpNotified", DbType.String, CmpNotified, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        If String.IsNullOrEmpty(CmpHandlerUsrCode) Then
            params(27) = New Aurora.Common.Data.Parameter("sCmpHandlerUsrCode", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(27) = New Aurora.Common.Data.Parameter("sCmpHandlerUsrCode", DbType.String, CmpHandlerUsrCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(AddUsrId) Then
            params(28) = New Aurora.Common.Data.Parameter("sAddUsrId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(28) = New Aurora.Common.Data.Parameter("sAddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(ModUsrId) Then
            params(29) = New Aurora.Common.Data.Parameter("sModUsrId", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(29) = New Aurora.Common.Data.Parameter("sModUsrId", DbType.String, ModUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        If String.IsNullOrEmpty(AddPrgmName) Then
            params(30) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, System.DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        Else
            params(30) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, AddPrgmName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        End If

        params(31) = New Aurora.Common.Data.Parameter("nIntegrityNo", DbType.Int16, IntegrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

        params(32) = New Aurora.Common.Data.Parameter("nCreatedCmpId", DbType.Int64, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
        params(33) = New Aurora.Common.Data.Parameter("sErrorMessage", DbType.String, 256, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("Complaints_CreateUpdateComplaint", params)

        ErrorMessage = params(33).Value

        Return Int64.Parse(params(32).Value)

    End Function

#End Region

#Region "rev:mia Oct.24 2012 - Addition of ThirdParty Reference Report"
    Public Shared Function GetThirdPartyRef(rntid As String, rntBooid As String) As String
        Try
            If (Not String.IsNullOrEmpty(rntBooid) AndAlso Not String.IsNullOrEmpty(rntid)) Then
                Return Aurora.Common.Data.ExecuteScalarSP("Booking_GetThirdPartyRef", rntid, rntBooid)
            End If

        Catch ex As Exception
            Logging.LogError("GetThirdPartyRef", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
        End Try

        Return String.Empty
    End Function
#End Region

#Region "rev:mia nov.2 2012 - Hide surcharge Text when there is no surcharge"
    ''will not be use
    Private Shared Function IsRentalHasSurcharge(ByVal sRntId As String) As Boolean

        Dim params(1) As Aurora.Common.Data.Parameter
        Dim returnMsg As String = ""
        Dim returnSurchage As String = ""
        Try


            returnMsg = Aurora.Common.Data.ExecuteSqlXmlSP("WEBA_GetCRSurcharge", sRntId, "")




            If (String.IsNullOrEmpty(returnMsg) = False) Then
                params(0) = New Aurora.Common.Data.Parameter("xmlInput", DbType.String, returnMsg, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("hassurcharge", DbType.String, 10, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("Confirmation_HasSurcharge", params)

                returnSurchage = params(1).Value
            End If





        Catch ex As Exception
            Logging.LogError("IsRentalHasSurcharge", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
        End Try

        Return IIf(String.IsNullOrEmpty(returnSurchage) = True Or returnSurchage = "0.00" Or CInt(returnSurchage) = 0, False, True)

    End Function
#End Region

    Public Shared Function GetVenueTypes() As DataTable
        Dim resultTable As DataTable = New DataTable
        Aurora.Common.Data.ExecuteTableSP("sp_getVenueTypes", resultTable)

        Return resultTable
    End Function

    Public Shared Function GetArrivalDepartureDetails(ByVal rentalId As String) As DataTable
        Dim resultTable As DataTable = New DataTable
        Aurora.Common.Data.ExecuteTableSP("sp_getArrivalDepartureDetails", resultTable, rentalId)

        Return resultTable
    End Function

    Public Shared Function GetPhysicalAddressCodeId() As String
        Return DirectCast(Aurora.Common.Data.ExecuteScalarSQL("select dbo.GetCodeId('1', 'Physical')"), String)
    End Function

    Public Shared Function UpdateArrivalDepartureDetails(ByVal id As Nullable(Of Long), ByVal rentalId As String,
        ByVal type As String, ByVal pickupDate As Nullable(Of DateTime), ByVal venueCodeId As String, ByVal venueName As String, _
        ByVal specialInstructions As String, ByVal integrityNumber As Integer, ByVal noteIntegrityNumber As Integer, _
        ByVal userCode As String, ByVal programName As String, _
        ByVal street As String, ByVal suburb As String, ByVal city As String, _
        ByVal postCode As String, ByVal state As String, ByVal countryCode As String, _
        ByVal adddressIntegrity As Integer) As String

        If venueCodeId = "" Then
            venueCodeId = Nothing
        End If

        Return Aurora.Common.Data.ExecuteScalarSP("sp_ManageArrivalDepartureDetails", _
            id, rentalId, _
            type, pickupDate, venueCodeId, venueName, _
            specialInstructions, integrityNumber, noteIntegrityNumber, _
            userCode, programName, _
            street, suburb, city, _
            postCode, state, countryCode, _
            adddressIntegrity)
    End Function

    Public Shared Sub GetEuropcarServiceLog(ByVal listDataset As Aurora.Common.IListDataSet, ByVal rentalID As String)
        Aurora.Common.Data.ExecuteListSP("GetThirdPartyLog", _
                                                listDataset, _
                                                New ParamInfo(rentalID, "rentalID") _
                                                )
    End Sub



#Region "rev:mia June.03 2013 - Addition of Price Match"
    Public Shared Function Booking_PriceMatchingInsertUpdate(PriceMSeqId As Int32, _
                            PriceMRntId As String, _
                            PriceMRequestCodTypeId As String, _
                            PriceMRequestor As String, _
                            PriceMCompetitorCodId As String, _
                            PriceMCompetitorOthers As String, _
                            PriceMCompetitorVehicle As String, _
                            PriceMCompetitorPrice As Decimal, _
                            PriceMCompetitorQuoteDate As String, _
                            PriceMCompetitorQuoteLink As String, _
                            PriceMStatusCodId As String, _
                            PriceMDecisionBy As String, _
                            PriceMNote As String, _
                            AddUsrId As String, _
                            PriceMAlternateVehicle As String, _
                            PriceMAlternatePrice As Decimal, _
                            Optional products As String = "") As Integer
        Dim rowcount As Integer
        Const NO_XML_DATA As String = "<PriceMatch><Products></Products></PriceMatch>"
        Try
            If (PriceMCompetitorVehicle = "NA") Then PriceMCompetitorVehicle = ""
            If (PriceMCompetitorQuoteDate = "NA") Then PriceMCompetitorQuoteDate = String.Empty

            Dim params(16) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("PriceMSeqId", DbType.Int64, PriceMSeqId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PriceMRntId", DbType.String, PriceMRntId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("PriceMRequestCodTypeId", DbType.String, PriceMRequestCodTypeId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("PriceMRequestor", DbType.String, PriceMRequestor, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("PriceMCompetitorCodId", DbType.String, PriceMCompetitorCodId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("PriceMCompetitorOthers", DbType.String, PriceMCompetitorOthers, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("PriceMCompetitorVehicle", DbType.String, PriceMCompetitorVehicle, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ''params(7) = New Aurora.Common.Data.Parameter("PriceMCompetitorPrice", DbType.Decimal, PriceMCompetitorPrice, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("PriceMCompetitorPrice", DbType.Decimal, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            If (String.IsNullOrEmpty(PriceMCompetitorQuoteDate)) Then
                params(8) = New Aurora.Common.Data.Parameter("PriceMCompetitorQuoteDate", DbType.DateTime, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Else
                params(8) = New Aurora.Common.Data.Parameter("PriceMCompetitorQuoteDate", DbType.DateTime, Convert.ToDateTime(PriceMCompetitorQuoteDate), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            End If

            params(9) = New Aurora.Common.Data.Parameter("PriceMCompetitorQuoteLink", DbType.String, PriceMCompetitorQuoteLink, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("PriceMStatusCodId", DbType.String, PriceMStatusCodId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("PriceMDecisionBy", DbType.String, PriceMDecisionBy, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(12) = New Aurora.Common.Data.Parameter("PriceMNote", DbType.String, PriceMNote, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(13) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(14) = New Aurora.Common.Data.Parameter("PriceMAlternateVehicle", DbType.String, PriceMAlternateVehicle, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ''params(15) = New Aurora.Common.Data.Parameter("PriceMAlternatePrice", DbType.Decimal, PriceMAlternatePrice, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(15) = New Aurora.Common.Data.Parameter("PriceMAlternatePrice", DbType.Decimal, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(16) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Booking_InsertUpdatePriceMatching", params)
            ''if its a new record, then rowcount is the new identity
            ''it its an update, then rowcount is the number affected so use the PriceMSeqId
            rowcount = params(16).Value
            If (rowcount = 0 And PriceMSeqId > 0) Then
                rowcount = PriceMSeqId
            End If

            If (NO_XML_DATA = products) Then
                Return rowcount
            End If

            If (Booking_PriceMatchingDetailsInsert(products, AddUsrId, rowcount, PriceMStatusCodId) = -1) Then
                rowcount = -1
            End If

        Catch ex As Exception
            Logging.LogError("Booking_PriceMatchingInsertUpdate", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
            rowcount = -1
        End Try
        Return rowcount
    End Function

    Private Shared Function Booking_PriceMatchingDetailsInsert(products As String, _
                                                               UserId As String, _
                                                               Optional PriceMSeqId As Int32 = -1, _
                                                               Optional PriceMStatusCodId As String = "") As Integer


        Dim PriceMMDSeqId As Integer = PriceMSeqId
        Dim AddUsrId As String = UserId

        Dim PriceMMDVehTHL As String = String.Empty
        Dim PriceMMDVehTHLPrice As Decimal = 0
        Dim PriceMMDVehTHLOffer As Decimal = 0
        Dim PriceMMDCompVeh As String = String.Empty
        Dim PriceMMDCompVehPrice As Decimal = 0
        Dim PriceMMDPriceVariance As Decimal = 0
        Dim PriceMMDFinalPrice As Decimal = 0
        Dim SequenceID As Integer = -1
        Dim MasterKey As Integer = -1

        Dim dsproduct As New DataSet
        dsproduct.ReadXml(New XmlTextReader(products, System.Xml.XmlNodeType.Document, Nothing))

        ''Dim sequenceTemp As String = dsproduct.Tables("Item").Rows(0)("SequenceID")
        ''Dim counts As Integer = Aurora.Common.Data.ExecuteScalarSQL("SELECT PriceMMDSeqId FROM RentalPriceMatchDetail WHERE PriceMMDSeqId = " & sequenceTemp)

        Try

            For Each row As DataRow In dsproduct.Tables("Item").Rows
                Try
                    If (String.IsNullOrEmpty(row("Masterkey").ToString)) Then
                        MasterKey = -1
                    Else
                        MasterKey = Convert.ToInt32(row("Masterkey").ToString)
                    End If

                    SequenceID = Convert.ToInt32(row("SequenceID").ToString)
                    If (SequenceID = -1 Or SequenceID = 0) Then
                        SequenceID = PriceMMDSeqId
                    End If

                Catch ex As Exception
                End Try


                PriceMMDVehTHL = row("THLProduct").ToString
                PriceMMDCompVeh = row("CompProduct").ToString

                If (String.IsNullOrEmpty(row("THLPrice").ToString)) Then
                    PriceMMDVehTHLPrice = 0
                Else
                    PriceMMDVehTHLPrice = Convert.ToDecimal(row("THLPrice").ToString)
                End If

                If (String.IsNullOrEmpty(row("THLOffer").ToString)) Then
                    PriceMMDVehTHLOffer = 0
                Else
                    PriceMMDVehTHLOffer = Convert.ToDecimal(row("THLOffer").ToString)
                End If

                If (String.IsNullOrEmpty(row("CompPrice").ToString)) Then
                    PriceMMDCompVehPrice = 0
                Else
                    PriceMMDCompVehPrice = Convert.ToDecimal(row("CompPrice").ToString)
                End If

                If (String.IsNullOrEmpty(row("PriceVariance").ToString)) Then
                    PriceMMDPriceVariance = 0
                Else
                    PriceMMDPriceVariance = Convert.ToDecimal(row("PriceVariance").ToString)
                End If

                If (String.IsNullOrEmpty(row("ApprovedPrice").ToString)) Then
                    PriceMMDFinalPrice = 0
                Else
                    PriceMMDFinalPrice = Convert.ToDecimal(row("ApprovedPrice").ToString)
                End If


                Dim retvalue As Integer = Aurora.Common.Data.ExecuteScalarSP("Booking_InsertUpdateDetailsPriceMatching", _
                                                                                              MasterKey, _
                                                                                              SequenceID, _
                                                                                              PriceMMDVehTHL, _
                                                                                              PriceMMDVehTHLPrice, _
                                                                                              PriceMMDVehTHLOffer, _
                                                                                              PriceMMDCompVeh, _
                                                                                              PriceMMDCompVehPrice, _
                                                                                              PriceMMDPriceVariance, _
                                                                                              PriceMMDFinalPrice, _
                                                                                              AddUsrId, _
                                                                                              PriceMStatusCodId)

                Logging.LogInformation("Booking_PriceMatchingDetailsInsert", "PriceMMDVehTHL: " & PriceMMDVehTHL & _
                                                                             ",PriceMMDVehTHLPrice: " & PriceMMDVehTHLPrice & _
                                                                             ",PriceMMDVehTHLOffer: " & PriceMMDVehTHLOffer & _
                                                                             ",PriceMMDCompVehPrice: " & PriceMMDCompVehPrice & _
                                                                             ",PriceMMDCompVeh: " & PriceMMDCompVeh & _
                                                                             ",PriceMMDPriceVariance: " & PriceMMDPriceVariance & _
                                                                             ",PriceMMDFinalPrice: " & PriceMMDFinalPrice & _
                                                                             ",PriceMStatusCodId: " & PriceMStatusCodId)
            Next

        Catch ex As Exception
            Return -1
        End Try
        Return 1
    End Function

#End Region


#Region "rev:mia Sept.2 2013 - Customer Title from query"
    Public Shared Function GetCustomerTitle() As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Customer_GetTitle", result)
        Catch ex As Exception
            Logging.LogError("GetCustomerTitle", ex.Message & ", " & ex.StackTrace)
            Return Nothing
        End Try
        Return result
    End Function

    Public Shared Function UpdateCustomerTitle(CusFirstName As String, CusLastName As String, CusTitle As String, CusId As String) As Boolean
        Dim retValue As Integer = -1
        Try
            retValue = Aurora.Common.Data.ExecuteScalarSP("Customer_UpdateTitleAndName", CusFirstName, CusLastName, CusTitle, CusId)
        Catch ex As Exception
            Logging.LogError("UpdateCustomerTitle", ex.Message & ", " & ex.StackTrace)
            retValue = -1
        End Try
        Return IIf(retValue > 0, True, False)

    End Function

    Public Shared Function GetCustomerTitleAndName(CusId As String) As DataSet

        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Customer_GetTitleAndName", result, CusId)
        Catch ex As Exception
            Logging.LogError("GetCustomerTitleAndName", ex.Message & ", " & ex.StackTrace)
            Return Nothing
        End Try
        Return result
    End Function


#End Region

#Region "rev:mia Nov.15 2013 - fixing the message deleted by user"
    Public Shared Function GetConfirmationId(cfnrntnum As String, booid As String, cfnrntid As String) As String
        Dim retValue As String
        Try
            retValue = Aurora.Common.Data.ExecuteScalarSP("Confirmation_GetConfirmationId", cfnrntnum, booid, cfnrntid)
        Catch ex As Exception
            Logging.LogError("GetConfirmationId", ex.Message & ", " & ex.StackTrace)
            retValue = "ERROR"
        End Try
        Return retValue
    End Function

#End Region


#Region "REV:10NOV2014 - SLOT MANAGEMENT"
    ''	@bIsSCI		VARCHAR(64)	=	1,
    Public Shared Function GetAvailableSlot(sRntId As String, sAvpId As String, sBpdId As String, Optional IsSCI As String = "0") As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("getAvailableSlotForRntIdOrAvp", result, IIf(String.IsNullOrEmpty(sRntId), Nothing, sRntId), IIf(String.IsNullOrEmpty(sAvpId), Nothing, sAvpId), IIf(String.IsNullOrEmpty(sBpdId), Nothing, sBpdId), IsSCI, False)
            result.Tables(0).TableName = "Slot"
        Catch ex As Exception
            Logging.LogError("GetAvailableSlot", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function


    Public Shared Sub SaveRentalSlot(sRntId As String, islotId As Integer, sSlotDesc As String)
        Try
            Aurora.Common.Data.ExecuteScalarSP("SaveRentalSlot", sRntId, islotId, sSlotDesc)
        Catch ex As Exception
            Logging.LogError("SaveRentalSlot", ex.Message & ", " & ex.StackTrace)
        End Try
    End Sub

#End Region


#Region "rev:mia 16jan2014-addition of supervisor override password and slot validation"
    Public Shared Function GetAndValidateAvailableSlot(sRntId As String, _
                                                       sAvpId As String, _
                                                       sBpdId As String, _
                                                       IsSCI As String, _
                                                       newPickupDate As Date, _
                                                       newPickupLocCode As String, _
                                                       newAgnId As String, _
                                                       newVehicleSapId As String, _
                                                       newPkgId As String, _
                                                       newRntStatus As String) As DataSet
        Dim result As New DataSet
        Dim sbLog As New StringBuilder
        sbLog.AppendFormat("sRntId            - {0}{1}", sRntId, vbCrLf)
        sbLog.AppendFormat("sAvpId            - {0}{1}", sAvpId, vbCrLf)
        sbLog.AppendFormat("sBpdId            - {0}{1}", sBpdId, vbCrLf)
        sbLog.AppendFormat("IsSCI             - {0}{1}", IsSCI, vbCrLf)
        sbLog.AppendFormat("newPickupDate     - {0}{1}", newPickupDate, vbCrLf)
        sbLog.AppendFormat("Date.MinValue     - {0}{1}", Date.MinValue, vbCrLf)
        sbLog.AppendFormat("newPickupLocCode  - {0}{1}", newPickupLocCode, vbCrLf)
        sbLog.AppendFormat("newAgnId          - {0}{1}", newAgnId, vbCrLf)
        sbLog.AppendFormat("newVehicleSapId   - {0}{1}", newVehicleSapId, vbCrLf)
        sbLog.AppendFormat("newPkgId          - {0}{1}", newPkgId, vbCrLf)
        sbLog.AppendFormat("newRntStatus      - {0}{1}", newRntStatus, vbCrLf)

        Try
            Aurora.Common.Data.ExecuteDataSetSP("slot_ValidateAndDisplaySlotData", result, _
                                                IIf(String.IsNullOrEmpty(sRntId), Nothing, sRntId), _
                                                IIf(String.IsNullOrEmpty(sAvpId), Nothing, sAvpId), _
                                                IIf(String.IsNullOrEmpty(sBpdId), Nothing, sBpdId), _
                                                IsSCI, _
                                                False, _
                                                IIf(String.IsNullOrEmpty(newRntStatus), Nothing, newRntStatus), _
                                                IIf(newPickupDate.Equals(Date.MinValue), DBNull.Value, newPickupDate), _
                                                IIf(String.IsNullOrEmpty(newPickupLocCode), Nothing, newPickupLocCode), _
                                                IIf(String.IsNullOrEmpty(newAgnId), Nothing, newAgnId), _
                                                IIf(String.IsNullOrEmpty(newVehicleSapId), Nothing, newVehicleSapId), _
                                                IIf(String.IsNullOrEmpty(newPkgId), Nothing, newPkgId))


            If ((result.Tables.Count - 1) > -1) Then
                result.Tables(0).TableName = "Slot"
            Else
                sbLog.AppendFormat("RESULT      - {0}{1}", "NO RECORD", vbCrLf)
            End If
            Logging.LogDebug("GetAndValidateAvailableSlot", sbLog.ToString)
        Catch ex As Exception
            sbLog.AppendFormat("ERROR      - {0}{1}", ex.Message & ", " & ex.StackTrace, vbCrLf)
            Logging.LogError("GetAndValidateAvailableSlot", sbLog.ToString)
        End Try
        Return result
    End Function
#End Region

End Class




