Partial Public Class DailyActivityDataSet

    Partial Class CheckinDataTable

        Public ReadOnly Property TotalOutstanding() As Integer
            Get
                Dim result As Integer = 0
                For Each checkinRow As CheckinRow In Me
                    If checkinRow.IsOverdue OrElse checkinRow.IsDue Then result += 1
                Next
                Return result
            End Get
        End Property

    End Class

End Class
