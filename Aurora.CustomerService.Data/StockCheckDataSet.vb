﻿Imports System.Drawing

Partial Class StockCheckDataSet

    Partial Class LocationRow
        Public Function GetInProgress() As LocationInventoryLookupRow
            For Each locationInventoryLookupRow As LocationInventoryLookupRow In Me.GetLocationInventoryLookupRows()
                If locationInventoryLookupRow.LivStatus = StockCheckConstants.StockCheckStatus_InProgress Then
                    Return locationInventoryLookupRow
                End If
            Next
            Return Nothing
        End Function

        Public ReadOnly Property HasInProgress() As Boolean
            Get
                Return GetInProgress() IsNot Nothing
            End Get
        End Property

        Public ReadOnly Property StatusColor() As Color
            Get
                If HasInProgress() Then
                    Return StockCheckConstants.CurrentColor
                ElseIf Me.GetLocationInventoryLookupRows().Length > 0 Then
                    Return StockCheckConstants.PastColor
                Else
                    Return StockCheckConstants.InactiveColor
                End If
            End Get
        End Property

    End Class

    Partial Class LocationInventoryLookupRow
        Public ReadOnly Property StatusColor() As Color
            Get
                If Me.LivStatus = StockCheckConstants.StockCheckStatus_InProgress Then
                    Return StockCheckConstants.CurrentColor
                Else
                    Return StockCheckConstants.PastColor
                End If
            End Get
        End Property

        Public ReadOnly Property CanComplete() As Boolean
            Get
                Return Me.LivStatus = StockCheckConstants.StockCheckStatus_InProgress
            End Get
        End Property

        Public ReadOnly Property CanUndo() As Boolean
            Get
                Dim stockCheckDataSet As StockCheckDataSet = CType(Me.Table.DataSet, StockCheckDataSet)

                Return Not CanComplete _
                 AndAlso Me.LocationRow.GetLocationInventoryLookupRows()(0) Is Me _
                 AndAlso Not Me.LocationRow.HasInProgress _
                 AndAlso stockCheckDataSet.Universalinfo.Count > 0 _
                 AndAlso Not stockCheckDataSet.Universalinfo(0).IsUviValueNull _
                 AndAlso stockCheckDataSet.Universalinfo(0).UviValue.ToLower() = "yes"
            End Get
        End Property
    End Class

    Partial Class CheckedProductItemRow

        Public ReadOnly Property Description() As String
            Get
                Dim result As String = ""
                If Not Me.IsPriUnitNumNull Then result &= Me.PriUnitNum
                If Not Me.IsPriUnitNumNull AndAlso Me.FleetAssetRow IsNot Nothing AndAlso Not Me.FleetAssetRow.IsRegistrationNumberNull Then result &= "/"
                If Me.FleetAssetRow IsNot Nothing AndAlso Not Me.FleetAssetRow.IsRegistrationNumberNull Then result &= Me.FleetAssetRow.RegistrationNumber
                Return result
            End Get
        End Property

    End Class
End Class
