Imports System.Xml

Imports Aurora.CustomerService.Data.StockCheckDataSet
Imports Aurora.Common.Data


Public Module DataRepository

#Region "Daily Close Off"
    Public Function GetDailyCloseOffFloat(ByVal UserCode As String) As XmlDocument
        'exec(get_DailyCloseOff_float) 'sp7'
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_DailyCloseOff_float", UserCode)
    End Function

    Public Function StartDailyCloseOff(ByVal UserCode As String, ByVal CloseOffDate As DateTime) As XmlDocument
        'CREATE  Procedure ManageDailyCloseOff_Start  
        '@pUser   VARCHAR (64),  
        '@pCODate  datetime                

        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("ManageDailyCloseOff_Start", UserCode, CloseOffDate)
    End Function



    Public Function SaveDailyCloseOffFloat(ByVal CloseOffData As String) As XmlDocument
        'CREATE         Procedure ManageDailyCloseOff_Float  
        '@pXMLData text = default  
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("ManageDailyCloseOff_Float", CloseOffData)
    End Function

    Public Function GetDailyCloseOffAccountReconciliation(ByVal LocationId As String) As XmlDocument
        'CREATE             PROCEDURE get_DailyCloseOff_AccRec  
        '@pLcoId VARCHAR  (64)  
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_DailyCloseOff_AccRec", LocationId)
    End Function

    ''' <summary>
    ''' Saves the Daily Close Off Account Reconcilliation Data
    ''' </summary>
    ''' <param name="DCOARData">The XML string containing the DCO Acct Recon data from the screen</param>
    ''' <returns>XML Document with the error/success status</returns>
    ''' <remarks></remarks>
    Public Function SaveDailyCloseOffAccountReconciliationData(ByVal DCOARData As String) As XmlDocument
        'CREATE Procedure ManageDailyCloseOff_AccRec  
        '@pXMLData varchar(8000)  

        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("ManageDailyCloseOff_AccRec", DCOARData)

    End Function

    Public Function GetDailyCloseOffPaymentReconcilliation(ByVal LocationId As String, ByVal UserCode As String) As XmlDocument
        ' CREATE PROCEDURE get_DailyCloseOff_PayRec  
        '@pLcoId  VARCHAR  (64),  
        '@pUserName VARCHAR  (64)  
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("get_DailyCloseOff_PayRec", LocationId, UserCode)

    End Function

    Public Function SaveDailyCloseOffPaymentReconciliationData(ByVal LocationId As String, _
                                                               ByVal ChequeDrawn As Double, _
                                                               ByVal CashBanked As Double, _
                                                               ByVal Difference As Double, _
                                                               ByVal IntegrityNumber As Integer, _
                                                               ByVal ChequeBanked As String, _
                                                               ByVal TotalBanking As Double, _
                                                               ByVal UserCode As String) As XmlDocument
        'CREATE          Procedure ManageDailyCloseOff_PayRec  
        '@pLcoId   VARCHAR (64),  
        '@pChqDrawn  money,  
        '@pCashBanked money,  
        '@pDiff   money,  
        '@pIntegrityNo int,  
        '@sBankedChq  VARCHAR(64),  
        '@pTotalBanking MONEY,  
        '@pUserName  varchar(64)  

        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("ManageDailyCloseOff_PayRec", LocationId, "$" & ChequeDrawn.ToString(), "$" & CashBanked.ToString(), "$" & Difference.ToString(), IntegrityNumber, ChequeBanked, "$" & TotalBanking.ToString(), UserCode)

    End Function

    Public Function ReconcileDailyCloseOff(ByVal LocationId As String) As XmlDocument
        'CREATE Procedure ManageDailyCloseOff_Reconcile  
        '@pLcoid VARCHAR (64)  
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("ManageDailyCloseOff_Reconcile", LocationId)

    End Function

    Public Function CancelDailyCloseOff(ByVal LocationId As String) As XmlDocument
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("ManageDailyCloseOff_Cancel", LocationId)
    End Function

#End Region

#Region "Daily Close Off Reports"

    Public Function GetPaymentTransactionReportByMethod(ByVal UserCode As String, ByVal ReportDate As String) As XmlDocument
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_Generate_PmtTransByMethod", UserCode, ReportDate)
    End Function

    Public Function GetPaymentTransactionReportByType(ByVal UserCode As String, ByVal ReportDate As String) As XmlDocument
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_Generate_PmtTransByType", UserCode, ReportDate)
    End Function

    Public Function GetDCOReportsPath() As String
        'Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_getUviValue", "DCOREPORTPATH", )

        ' CREATE   PROCEDURE GEN_getUviValue   
        ' @sUviKey varchar(24) = default,  
        ' @sCtyCode varchar(24) = '',  
        ' @param2  varchar(64) = '',  
        ' @param3  varchar(64) = '',  
        ' @param4  varchar(64) = '',  
        ' @UviValue varchar(8000) OUTPUT  

        Dim params(5) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("sUviKey", DbType.String, "DCOREPORTPATH", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("UviValue", DbType.String, 4000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("GEN_getUviValue", params)
        Return CStr(params(5).Value)



    End Function

    Public Function GetUsersEMailId(ByVal UserCode As String) As XmlDocument
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetUsersEMailId", UserCode)
    End Function

#End Region

#Region "Daily Activity"

    Public Function GetDailyActivity( _
        ByVal dailyActivityDataSet As DailyActivityDataSet, _
        ByVal usrCode As String, _
        ByVal locCode As String, _
        ByVal activityFor As Date, _
        ByVal includeOverdues As Boolean, _
        ByVal sortBy As Integer) As DailyActivityDataSet

        'CREATE PROCEDURE [dbo].[CustomerService_GetDailyActivity] 
        '	@UsrCode AS varchar(100),
        '	@LocCode AS varchar(50), 
        '	@ActivityFor AS datetime,
        '	@IncludeOverdues AS bit,
        '	@SortBy AS int = 0 -- 0 - BooName (customer), 1 - RntCkoWhen/RntCkiWhen (Date), 2 - BrdCode,PrdShortName (Vehicle)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("CustomerService_GetDailyActivity", result, usrCode, locCode, activityFor, includeOverdues, sortBy)

        If result.Tables.Count >= 2 Then

            Aurora.Common.Data.CopyDataTable(result.Tables(0), dailyActivityDataSet.Checkout)
            Aurora.Common.Data.CopyDataTable(result.Tables(1), dailyActivityDataSet.Checkin)
        End If

        Return dailyActivityDataSet
    End Function

#End Region

#Region "Stock Check"

    Public Function GetStockCheckLookups( _
        ByVal stockCheckDataSet As StockCheckDataSet, _
        ByVal comCode As String, _
        ByVal ctyCode As String) As StockCheckDataSet

        'CREATE PROCEDURE [dbo].[CustomerService_GetStockCheckLookups] 
        '	@ComCode AS varchar(64),
        '	@CtyCode AS varchar(64)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("CustomerService_GetStockCheckLookups", result, comCode, ctyCode)

        If result.Tables.Count >= 3 Then
            Aurora.Common.Data.CopyDataTable(result.Tables(0), stockCheckDataSet.Location)
            Aurora.Common.Data.CopyDataTable(result.Tables(1), stockCheckDataSet.LocationInventoryLookup)
            Aurora.Common.Data.CopyDataTable(result.Tables(2), stockCheckDataSet.Universalinfo)
        End If

        Return stockCheckDataSet
    End Function

    Public Function GetStockCheck( _
        ByVal stockCheckDataSet As StockCheckDataSet, _
        ByVal comCode As String, _
        ByVal livId As String) As StockCheckDataSet

        'CREATE PROCEDURE [dbo].[CustomerService_GetStockCheck] 
        '	@ComCode AS varchar(64),
        '	@LivId AS varchar(64)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("CustomerService_GetStockCheck", result, comCode, livId)

        If result.Tables.Count >= 4 Then
            Aurora.Common.Data.CopyDataTable(result.Tables(0), stockCheckDataSet.LocationInventory)
            Aurora.Common.Data.CopyDataTable(result.Tables(1), stockCheckDataSet.UserInfo)
            Aurora.Common.Data.CopyDataTable(result.Tables(2), stockCheckDataSet.CheckedProductItem)
            Aurora.Common.Data.CopyDataTable(result.Tables(3), stockCheckDataSet.FleetAsset)
            Aurora.Common.Data.CopyDataTable(result.Tables(4), stockCheckDataSet.RMReport)
        End If

        Return stockCheckDataSet
    End Function

    Public Function CreateLocationInventory( _
        ByVal comCode As String, _
        ByVal ctyCode As String, _
        ByVal locCode As String, _
        ByVal usrId As String, _
        ByVal prgrmName As String) As LocationInventoryDataTable

        'CREATE PROCEDURE [dbo].[CustomerService_CreateLocationInventory] 
        '	@ComCode AS varchar(64),
        '	@CtyCode AS varchar(64),
        '	@LocCode AS varchar(64),
        '	@UsrId AS varchar(64),
        '	@PrgrmName AS varchar(64)

        Return Aurora.Common.Data.ExecuteTableSP( _
            "CustomerService_CreateLocationInventory", _
            New LocationInventoryDataTable(), _
            comCode, ctyCode, locCode, usrId, prgrmName)
    End Function

    Public Function UpdateLocationInventory( _
        ByVal livId As String, _
        ByVal livStatus As String, _
        ByVal intergrityNo As Integer, _
        ByVal usrId As String, _
        ByVal prgrmName As String) As LocationInventoryDataTable

        'CREATE PROCEDURE [dbo].[CustomerService_UpdateLocationInventory] 
        '	@LivId AS varchar(64),
        '	@LivStatus AS varchar(64),
        '	@IntegrityNo AS int,
        '	@UsrId AS varchar(64),
        '	@PrgrmName AS varchar(64)

        Return Aurora.Common.Data.ExecuteTableSP( _
            "CustomerService_UpdateLocationInventory", _
            New LocationInventoryDataTable(), _
            livId, livStatus, intergrityNo, usrId, prgrmName)
    End Function

    Public Function CreateCheckedProductItem( _
        ByVal comCode As String, _
        ByVal livId As String, _
        ByVal priUnitNum As String, _
        ByVal isVehicle As Boolean, _
        ByVal usrId As String, _
        ByVal prgmName As String) As CheckedProductItemDataTable

        'CREATE PROCEDURE [dbo].[CustomerService_CreateCheckedProductItem] 
        '	@ComCode AS varchar(64),
        '	@LivId AS varchar(64),
        '	@PriUnitNum AS varchar(64),
        '	@IsVehicle AS bit,
        '	@UsrId AS varchar(64),
        '	@PrgrmName AS varchar(64)

        Return Aurora.Common.Data.ExecuteTableSP( _
            "CustomerService_CreateCheckedProductItem", _
            New CheckedProductItemDataTable(), _
            comCode, livId, priUnitNum, isVehicle, usrId, prgmName)
    End Function

    Public Function DeleteCheckedProductItem( _
        ByVal comCode As String, _
        ByVal livId As String, _
        ByVal priUnitNum As String, _
        ByVal isVehicle As Boolean)

        'CREATE PROCEDURE [dbo].[CustomerService_DeleteCheckedProductItem] 
        '	@ComCode AS varchar(64),
        '	@LivId AS varchar(64),
        '	@PriUnitNum AS varchar(64),
        '	@IsVehicle AS bit

        Return Aurora.Common.Data.ExecuteScalarSP( _
            "CustomerService_DeleteCheckedProductItem", _
            comCode, livId, priUnitNum, isVehicle)
    End Function

#End Region


#Region "Precheckout - rev:mia July 9 2012"

    Public Function PreCheckoutData(ByVal RntCkoLocCode As String) As DataSet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("Cust_PreCheckout", result, RntCkoLocCode)
        Return result
    End Function


    Public Function PreCheckoutDateScheduledRun(ByVal RntCkoLocCode As String) As DataSet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("Cust_PreCheckoutGetLastAndNexRun", result, RntCkoLocCode)
        Return result
    End Function

    Public Function PreCheckoutResetSchedule(ByVal RntCkoLocCode As String) As String
        Try
            Return Aurora.Common.Data.ExecuteScalarSP( _
           "Cust_PreCheckoutResetSchedule", _
           RntCkoLocCode)
        Catch ex As Exception
            Return "Error:" & ex.Message
        End Try

    End Function

    Public Function GetPreCheckOutNotes(ByVal sRntId As String) As DataSet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("Cus_getPreCheckoutErrorNotes", result, sRntId)
        Return result
    End Function

    Public Function PreCheckoutResetButtonActivation(ByVal RntCkoLocCode As String) As String
        Try
            Dim params(1) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("RntCkoLocCode", DbType.String, RntCkoLocCode, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("makeActive", DbType.Boolean, 1, Parameter.ParameterType.AddOutParameter)

            Dim strerror As String = Aurora.Common.Data.ExecuteOutputObjectSP("Cust_PreCheckoutResetButtonActivation", params)

            If (strerror.Equals("SUCCESS")) Then
                Return params(1).Value.ToString
            Else
                Return "Error:"
            End If
        Catch ex As Exception
            Return "Error:" & ex.Message
        End Try

    End Function


    Public Function GetRentalAgreement(RntId As String) As XmlDocument

        Dim xmlDoc As New XmlDocument
        Dim sRetString As String
        Try
            sRetString = Aurora.Common.Data.ExecuteSqlXmlSP("GetRentalAgreementText", RntId)
            xmlDoc.LoadXml(sRetString)

        Catch ex As Exception
            xmlDoc = Nothing
        End Try

        Return xmlDoc

    End Function
#End Region

End Module
