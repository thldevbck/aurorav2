Public Class StockCheckConstants
    Inherits Aurora.Common.DataConstants

    Public Const StockCheckStatus_InProgress As String = "In Progress"
    Public Const StockCheckStatus_Completed As String = "Completed"

End Class
