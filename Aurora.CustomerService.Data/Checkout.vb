Partial Public Class DailyActivityDataSet

    Partial Class CheckoutDataTable

        Public ReadOnly Property TotalOutstanding() As Integer
            Get
                Dim result As Integer = 0
                For Each checkoutRow As CheckoutRow In Me
                    If checkoutRow.IsOverdue OrElse checkoutRow.IsDue Then result += 1
                Next
                Return result
            End Get
        End Property
    End Class

End Class
