Imports System
Imports System.Collections.Generic
Imports System.Text
Imports System.Xml
Imports System.Xml.Xsl
Imports System.Xml.XPath
Imports System.IO

Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime


''' <summary>
''' A simple wrapper for System.Net.Mail.MailMessage, but it builds its html and text body from an Xslt template 
''' specified in an xml file supplied when the class is instantiated.
''' Non-template settings and parameters may specified in the xml,
''' and can be programmatically overriden before the email is sent. 
''' Templates can also be persisted and sent later.
''' </summary>
''' <example>
''' Dim mailTemplate As EmailTemplateFile = EmailTemplateFile.Load ("Jungle.xml");
''' mailTemplate.Subject = "Banana"
''' mailTemplate.SetSingleParameter("Name", "Monkey")
''' mailTemplate.Send()
''' mailTemplate.Save("junglemonkey.xml")
''' </example>

Public Class EmailTemplateFile
    Implements IDisposable

#Region "public properties"

    Private _basePath As String
    ''' <summary>
    ''' Base path used for Attatchments and Linked Resources if a fully qualified filename is not supplied
    ''' </summary>
    Public Property BasePath() As String
        Get
            Return _basePath
        End Get
        Set(ByVal value As String)
            If (value Is Nothing) Then
                Throw New ArgumentNullException
            End If
            _basePath = Path.GetFullPath(value)
        End Set
    End Property

    Private _client As New SmtpClient
    ''' <summary>
    ''' The SmtpClient connection
    ''' </summary>
    Public ReadOnly Property Client() As SmtpClient
        Get
            Return _client
        End Get
    End Property

    ''' <summary>
    ''' The Smtp Host address
    ''' </summary>
    Public Property Host() As String
        Get
            Return Client.Host
        End Get
        Set(ByVal value As String)
            Client.Host = value
        End Set
    End Property

    ''' <summary>
    ''' The Smtp Port
    ''' </summary>
    Public Property Port() As Integer
        Get
            Return Client.Port
        End Get
        Set(ByVal value As Integer)
            Client.Port = value
        End Set
    End Property

    Private _subject As String
    ''' <summary>
    ''' The email subject
    ''' </summary>
    Public Property Subject() As String
        Get
            Return _subject
        End Get
        Set(ByVal value As String)
            _subject = value
        End Set
    End Property

    Private _addressFrom As MailAddress
    ''' <summary>
    ''' The email from address
    ''' </summary>
    Public Property AddressFrom() As MailAddress
        Get
            Return _addressFrom
        End Get
        Set(ByVal value As MailAddress)
            _addressFrom = value
        End Set
    End Property

    Private _addressTo As New List(Of MailAddress)
    ''' <summary>
    ''' The collection of all the email reciepients in the "to" address line
    ''' </summary>
    Public Property AddressTo() As List(Of MailAddress)
        Get
            Return _addressTo
        End Get
        Set(ByVal value As List(Of MailAddress))
            _addressTo = value
        End Set
    End Property

    Private _addressCC As New List(Of MailAddress)
    ''' <summary>
    ''' The collection of all the email reciepients in the "cc" address line
    ''' </summary>
    Public Property AddressCC() As List(Of MailAddress)
        Get
            Return _addressCC
        End Get
        Set(ByVal value As List(Of MailAddress))
            _addressCC = value
        End Set
    End Property

    Private _attachments As List(Of String) = New List(Of String)
    ''' <summary>
    ''' A collection of all the attatchment file names.  Must be fully qualified or relative to the base path.
    ''' </summary>
    Public Property Attachments() As List(Of String)
        Get
            Return _attachments
        End Get
        Set(ByVal value As List(Of String))
            _attachments = value
        End Set
    End Property

    Private _parameters As XmlDocument = New XmlDocument
    ''' <summary>
    ''' The xml document of the parameters passed to the Xslt templates.  Must contain a root element called "Parameters".
    ''' </summary>
    Public ReadOnly Property Parameters() As XmlDocument
        Get
            Return _parameters
        End Get
    End Property

    ''' <summary>
    ''' The inner xml (e.g. no document root) of the parameter xml document passed to the Xslt templates.  
    ''' </summary>
    Public Property ParametersXml() As String
        Get
            Return Parameters.DocumentElement.InnerXml
        End Get
        Set(ByVal value As String)
            Parameters.DocumentElement.InnerXml = value
        End Set
    End Property

    ''' <summary>
    ''' A helper method to get a single value setting from the parameters xml
    ''' </summary>
    Public Function GetSingleParameter(ByVal name As String) As String
        Dim node As XmlNode = Parameters.SelectSingleNode(("/Parameters/" & name))
        If Not node Is Nothing Then
            Return node.InnerText
        Else
            Return Nothing
        End If
    End Function


    ''' <summary>
    ''' A helper method to add a single element to the parameter xml
    ''' </summary>
    Public Sub AddParameter(ByVal path As String, ByVal name As String, ByVal value As String)
        Dim node As XmlNode = Parameters.SelectSingleNode(path)
        If (Not node Is Nothing) Then
            AddParameter(node, name, value)
        End If
    End Sub

    ''' <summary>
    ''' A helper method to add a single element to the parameter xml
    ''' </summary>
    Public Sub AddParameter(ByVal node As XmlNode, ByVal name As String, ByVal value As String)
        Dim e As XmlElement = Parameters.CreateElement(name)
        If (Not value Is Nothing) Then
            e.InnerText = value
        End If
        node.AppendChild(e)
    End Sub

    ''' <summary>
    ''' A helper method to add a single element to the parameter xml
    ''' </summary>
    Public Sub AddParameter(ByVal name As String, ByVal value As String)
        Dim e As XmlElement = Parameters.CreateElement(name)
        If (Not value Is Nothing) Then
            e.InnerText = value
        End If
        Parameters.DocumentElement.AppendChild(e)
    End Sub

    ''' <summary>
    ''' A helper method to set a single value setting to the parameters xml.  If value is null the 
    ''' element is removed.
    ''' </summary>
    Public Sub SetSingleParameter(ByVal name As String, ByVal value As String)
        Dim node As XmlNode = Parameters.SelectSingleNode(("/Parameters/" & name))
        If ((Not node Is Nothing) AndAlso (value Is Nothing)) Then
            Parameters.DocumentElement.RemoveChild(node)
        ElseIf ((Not node Is Nothing) AndAlso (Not value Is Nothing)) Then
            node.InnerText = value
        ElseIf ((node Is Nothing) AndAlso (Not value Is Nothing)) Then
            AddParameter(Parameters.DocumentElement, name, value)
        End If
    End Sub

    Private _htmlTemplate As XslCompiledTransform = Nothing
    Private _htmlTemplateSource As String = Nothing
    ''' <summary>
    ''' The compiled Html Xslt template
    ''' </summary>
    Public ReadOnly Property HtmlTemplate() As XslCompiledTransform
        Get
            Return _htmlTemplate
        End Get
    End Property

    Private _textTemplate As XslCompiledTransform = Nothing
    Private _textTemplateSource As String = Nothing
    ''' <summary>
    ''' The compiled Text Xslt template
    ''' </summary>
    Public ReadOnly Property TextTemplate() As XslCompiledTransform
        Get
            Return _textTemplate
        End Get
    End Property

#End Region

#Region "Constructor & Dispose & Serialize"

    Private Shared Function LoadSettingValue(ByVal node As XmlNode, ByVal key As String, ByVal defaultValue As Nullable(Of Boolean)) As Nullable(Of Boolean)
        Dim val As String = EmailTemplateFile.LoadSettingValue(node, key, CStr(Nothing))
        Try
            If Not val Is Nothing Then
                Return New Nullable(Of Boolean)(Boolean.Parse(val))
            Else
                Return defaultValue
            End If
        Catch
            Return defaultValue
        End Try
    End Function

    Private Shared Function LoadSettingValue(ByVal node As XmlNode, ByVal key As String, ByVal defaultValue As Nullable(Of Integer)) As Nullable(Of Integer)
        Dim val As String = EmailTemplateFile.LoadSettingValue(node, key, CStr(Nothing))
        Try
            If Not val Is Nothing Then
                Return New Nullable(Of Integer)(Integer.Parse(val))
            Else
                Return defaultValue
            End If
        Catch
            Return defaultValue
        End Try
    End Function

    Private Shared Function LoadSettingValue(ByVal node As XmlNode, ByVal key As String, ByVal defaultValue As String) As String
        Dim valueNode As XmlNode = Nothing
        If Not node Is Nothing Then valueNode = node.SelectSingleNode(key)
        If Not valueNode Is Nothing Then
            Return valueNode.InnerText
        Else
            Return defaultValue
        End If
    End Function

    Private Shared Sub SaveSettingValue(ByVal writer As XmlWriter, ByVal key As String, ByVal value As Object)
        If (Not value Is Nothing) Then
            writer.WriteElementString(key, ("" & value))
        End If
    End Sub

    Private Shared Function LoadAddress(ByVal node As XmlNode) As MailAddress
        Dim address As String = EmailTemplateFile.LoadSettingValue(node, "Address", CStr(Nothing))
        Dim displayName As String = EmailTemplateFile.LoadSettingValue(node, "DisplayName", CStr(Nothing))
        If (address Is Nothing) Then
            Return Nothing
        End If
        If (Not displayName Is Nothing) Then
            Return New MailAddress(address, displayName)
        End If
        Return New MailAddress(address)
    End Function

    Private Shared Sub SaveAddress(ByVal writer As XmlWriter, ByVal key As String, ByVal address As MailAddress)
        If (Not address Is Nothing) Then
            writer.WriteStartElement(key)
            EmailTemplateFile.SaveSettingValue(writer, "Address", address.Address)
            If (Not address.DisplayName Is Nothing) Then
                EmailTemplateFile.SaveSettingValue(writer, "DisplayName", address.DisplayName)
            End If
            writer.WriteEndElement()
        End If
    End Sub

    Private Shared Sub LoadAddresses(ByVal node As XmlNode, ByVal addresses As List(Of MailAddress))
        If (Not node Is Nothing) Then
            Dim mailAddressNode As XmlNode
            For Each mailAddressNode In node.SelectNodes("MailAddress")
                Dim address As MailAddress = EmailTemplateFile.LoadAddress(mailAddressNode)
                If (Not address Is Nothing) Then
                    addresses.Add(address)
                End If
            Next
        End If
    End Sub

    Private Shared Sub SaveAddresses(ByVal writer As XmlWriter, ByVal key As String, ByVal addresses As List(Of MailAddress))
        If (addresses.Count <> 0) Then
            writer.WriteStartElement(key)
            Dim address As MailAddress
            For Each address In addresses
                EmailTemplateFile.SaveAddress(writer, "MailAddress", address)
            Next
            writer.WriteEndElement()
        End If
    End Sub

    Private Sub LoadAttachments(ByVal node As XmlNode)
        If (Not node Is Nothing) Then
            Dim filenameNode As XmlNode
            For Each filenameNode In node.SelectNodes("FileName")
                Attachments.Add(filenameNode.InnerText)
            Next
        End If
    End Sub

    Private Sub SaveAttachments(ByVal writer As XmlWriter)
        If (Attachments.Count <> 0) Then
            writer.WriteStartElement("Attachments")
            Dim fileName As String
            For Each fileName In Attachments
                EmailTemplateFile.SaveSettingValue(writer, "FileName", fileName)
            Next
            writer.WriteEndElement()
        End If
    End Sub

    Private Sub LoadParameters(ByVal node As XmlNode)
        If (node Is Nothing) Then
            Parameters.AppendChild(Parameters.CreateElement("Parameters"))
        Else
            Using stringReader As StringReader = New StringReader(node.OuterXml)
                Using reader As XmlTextReader = New XmlTextReader(stringReader)
                    Parameters.Load(reader)
                End Using
            End Using
        End If
    End Sub

    Private Sub SaveParameters(ByVal writer As XmlWriter)
        writer.WriteStartElement("Parameters")
        Parameters.DocumentElement.WriteContentTo(writer)
        writer.WriteEndElement()
    End Sub

    Private Sub LoadXsltTemplate(ByVal node As XmlNode, ByRef xslt As XslCompiledTransform, ByRef source As String)
        If (Not node Is Nothing) Then
            xslt = New XslCompiledTransform
            Using stringWriter As StringWriter = New StringWriter
                Using writer As XmlTextWriter = New XmlTextWriter(stringWriter)
                    node.WriteContentTo(writer)
                    source = stringWriter.ToString
                End Using
            End Using
            Using stringReader As StringReader = New StringReader(source)
                Using reader As XmlTextReader = New XmlTextReader(stringReader)
                    xslt.Load(reader)
                End Using
            End Using
        End If
    End Sub

    Private Sub SaveXsltTemplate(ByVal writer As XmlWriter, ByVal key As String, ByVal source As String)
        If (Not source Is Nothing) Then
            writer.WriteStartElement(key)
            writer.WriteRaw(source)
            writer.WriteEndElement()
        End If
    End Sub

    Private Sub New(ByVal reader As TextReader, ByVal basePath As String)
        Dim source As New XmlDocument
        source.PreserveWhitespace = True
        source.Load(reader)
        If basePath Is Nothing Then basePath = EmailTemplateFile.LoadSettingValue(source.DocumentElement, "BasePath", CStr(Nothing))
        If Not basePath Is Nothing Then Me.BasePath = basePath
        Dim host As String = EmailTemplateFile.LoadSettingValue(source.DocumentElement, "Host", Me.Host)
        If Not host Is Nothing Then Me.Host = host
        Port = EmailTemplateFile.LoadSettingValue(source.DocumentElement, "Port", New Nullable(Of Integer)(Port)).Value
        Subject = EmailTemplateFile.LoadSettingValue(source.DocumentElement, "Subject", Subject)
        AddressFrom = EmailTemplateFile.LoadAddress(source.SelectSingleNode("/EmailTemplate/From"))
        EmailTemplateFile.LoadAddresses(source.SelectSingleNode("/EmailTemplate/To"), AddressTo)
        EmailTemplateFile.LoadAddresses(source.SelectSingleNode("/EmailTemplate/CC"), AddressCC)
        LoadAttachments(source.SelectSingleNode("/EmailTemplate/Attachments"))
        LoadParameters(source.SelectSingleNode("/EmailTemplate/Parameters"))
        LoadXsltTemplate(source.SelectSingleNode("/EmailTemplate/HtmlTemplate"), _htmlTemplate, _htmlTemplateSource)
        LoadXsltTemplate(source.SelectSingleNode("/EmailTemplate/TextTemplate"), _textTemplate, _textTemplateSource)
        Client.Credentials = CredentialCache.DefaultNetworkCredentials
    End Sub

    Public Sub Dispose() Implements System.IDisposable.Dispose
    End Sub

    ''' <summary>
    ''' Load a EmailTemplateFile from a TextReader
    ''' </summary>
    ''' 
    Public Shared Function Load(ByVal reader As TextReader) As EmailTemplateFile
        Return New EmailTemplateFile(reader, Nothing)
    End Function

    ''' <summary>
    ''' Load a EmailTemplateFile from a filename.  If no basepath is specified in the template, the file path is used.
    ''' </summary>
    Public Shared Function Load(ByVal fileName As String) As EmailTemplateFile
        Using reader As TextReader = New StreamReader(fileName)
            Return New EmailTemplateFile(reader, Path.GetDirectoryName(Path.GetFullPath(fileName)))
        End Using
    End Function

    ''' <summary>
    ''' Load a EmailTemplateFile from a string.
    ''' </summary>
    Public Shared Function LoadFromString(ByVal text As String) As EmailTemplateFile
        Using reader As TextReader = New StringReader(text)
            Return New EmailTemplateFile(reader, Nothing)
        End Using
    End Function

    ''' <summary>
    ''' Save a EmailTemplateFile to a TextWriter
    ''' </summary>
    Public Sub Save(ByVal textWriter As TextWriter)
        Using writer As XmlTextWriter = New XmlTextWriter(textWriter)
            writer.Formatting = Formatting.Indented
            writer.WriteStartElement("EmailTemplate")
            EmailTemplateFile.SaveSettingValue(writer, "BasePath", BasePath)
            EmailTemplateFile.SaveSettingValue(writer, "Host", Host)
            EmailTemplateFile.SaveSettingValue(writer, "Port", Port)
            EmailTemplateFile.SaveSettingValue(writer, "Subject", Subject)
            EmailTemplateFile.SaveAddress(writer, "From", AddressFrom)
            EmailTemplateFile.SaveAddresses(writer, "To", AddressTo)
            EmailTemplateFile.SaveAddresses(writer, "CC", AddressCC)
            SaveAttachments(writer)
            SaveParameters(writer)
            SaveXsltTemplate(writer, "HtmlTemplate", _htmlTemplateSource)
            SaveXsltTemplate(writer, "TextTemplate", _textTemplateSource)
            writer.WriteEndElement()
        End Using
    End Sub

    ''' <summary>
    ''' Save a EmailTemplateFile to a filename
    ''' </summary>
    Public Sub Save(ByVal fileName As String)
        Using streamWriter As StreamWriter = New StreamWriter(fileName)
            Save(streamWriter)
        End Using
    End Sub

    ''' <summary>
    ''' Save a EmailTemplateFile to a string
    ''' </summary>
    Public Function SaveToString() As String
        Using stringWriter As StringWriter = New StringWriter
            Save(stringWriter)
            Return stringWriter.ToString
        End Using
    End Function

#End Region

#Region "Send"

    ''' <summary>
    ''' The outputed Html from the combining the parameters with the Html Xslt Template
    ''' </summary>
    Public ReadOnly Property OutputText() As String
        Get
            If (Me.TextTemplate Is Nothing) Then
                Return Nothing
            End If
            Using writer As StringWriter = New StringWriter
                Using xmlReader As XmlReader = New XmlNodeReader(Me.Parameters)
                    Using xmlWriter As XmlWriter = New XmlTextWriter(writer)
                        Me.TextTemplate.Transform(xmlReader, xmlWriter)
                    End Using
                End Using
                Return writer.ToString
            End Using
        End Get
    End Property

    Private Sub ProcessTextTemplate(ByVal message As MailMessage)
        Dim outputText As String = Me.OutputText
        If Not outputText Is Nothing Then
            Dim av As AlternateView = AlternateView.CreateAlternateViewFromString(outputText, Nothing, "text/plain")
            message.AlternateViews.Add(av)
        End If
    End Sub

    ''' <summary>
    ''' The outputed Html from the combining the parameters with the Html Xslt Template (before url's 
    ''' are modified to support Linked Resources).
    ''' </summary>
    Public ReadOnly Property OutputHtml() As String
        Get
            If (Me.HtmlTemplate Is Nothing) Then
                Return Nothing
            End If
            Using writer As StringWriter = New StringWriter
                Using xmlReader As XmlReader = New XmlNodeReader(Me.Parameters)
                    Using xmlWriter As XmlWriter = New XmlTextWriter(writer)
                        Me.HtmlTemplate.Transform(xmlReader, xmlWriter)
                    End Using
                End Using
                Return writer.ToString
            End Using
        End Get
    End Property

    Private Sub ProcessHtmlTemplate(ByVal message As MailMessage)
        Dim outputHtml As String = Me.OutputHtml
        If outputHtml Is Nothing Then Return

        Dim htmlDoc As New XmlDocument
        htmlDoc.InnerXml = outputHtml

        Dim linkedResources As New List(Of LinkedResource)
        For Each e As XmlElement In htmlDoc.GetElementsByTagName("img")
            Dim imgSrc As String = e.GetAttribute("src")
            If imgSrc Is Nothing Then Continue For

            Dim filename As String
            If Path.IsPathRooted(imgSrc) Then
                filename = Path.GetFullPath(imgSrc)
            Else
                filename = Path.GetFullPath((Me.BasePath & "\" & imgSrc))
            End If
            If Not File.Exists(filename) Then Continue For

            Dim name As String = Path.GetFileName(imgSrc)
            Dim linkedResource As LinkedResource = Nothing
            For Each linkedResource0 As LinkedResource In linkedResources
                If linkedResource0.ContentType.Name = name Then
                    linkedResource = linkedResource0
                    Exit For
                End If
            Next
            If linkedResource Is Nothing Then
                linkedResource = New LinkedResource(filename)
                linkedResource.ContentId = ("lr" & linkedResources.Count)
                linkedResource.ContentType.Name = name
                linkedResources.Add(linkedResource)
            End If

            e.SetAttribute("src", "cid:" & linkedResource.ContentId)
        Next

        Dim av As AlternateView = AlternateView.CreateAlternateViewFromString(htmlDoc.InnerXml, Nothing, "text/html")
        For Each linkedResource As LinkedResource In linkedResources
            av.LinkedResources.Add(linkedResource)
        Next

        message.AlternateViews.Add(av)
    End Sub

    Private Sub ProcessAttachments(ByVal message As MailMessage)
        For Each fileName0 As String In Attachments
            Dim fileName As String
            If Path.IsPathRooted(fileName0) Then
                fileName = Path.GetFullPath(fileName0)
            Else
                fileName = Path.GetFullPath((BasePath & "\" & fileName0))
            End If
            If Not File.Exists(fileName) Then Continue For

            Dim attachment As New Attachment(fileName, "application/octet-stream")
            Dim disposition As ContentDisposition = attachment.ContentDisposition
            disposition.CreationDate = File.GetCreationTime(fileName)
            disposition.ModificationDate = File.GetLastWriteTime(fileName)
            disposition.ReadDate = File.GetLastAccessTime(fileName)
            message.Attachments.Add(attachment)
        Next
    End Sub

    ''' <summary>
    ''' Process and send the email.  The BasePath, Host, From, and Email recepient properties must all be set.
    ''' </summary>
    Public Sub Send()
        If BasePath Is Nothing Then Throw New InvalidOperationException("BasePath not set")
        If Client.Host Is Nothing Then Throw New InvalidOperationException("Host not set")
        If AddressFrom Is Nothing Then Throw New InvalidOperationException("From not set")
        If (AddressTo.Count + AddressCC.Count) = 0 Then Throw New InvalidOperationException("No recepients set")

        Using message As MailMessage = New MailMessage
            message.Subject = Subject
            message.From = AddressFrom
            For Each address As MailAddress In AddressTo
                message.To.Add(address)
            Next
            For Each address As MailAddress In AddressCC
                message.CC.Add(address)
            Next
            ProcessTextTemplate(message)
            ProcessHtmlTemplate(message)
            ProcessAttachments(message)
            Client.Send(message)
        End Using
    End Sub

#End Region

End Class

