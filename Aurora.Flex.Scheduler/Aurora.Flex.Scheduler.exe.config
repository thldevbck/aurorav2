﻿<?xml version="1.0" encoding="utf-8" ?>
<configuration>
	<configSections>
		<section name="SchedulerConfiguration" type="Aurora.Flex.Scheduler.SchedulerConfigurationSectionHandler, Aurora.Flex.Scheduler" />
		<section name="FlexExportConfiguration" type="Aurora.Flex.Services.FlexExportConfigurationSectionHandler, Aurora.Flex.Services" />
		<section name="dataConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Data.Configuration.DatabaseSettings, Microsoft.Practices.EnterpriseLibrary.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null" />
		<section name="loggingConfiguration" type="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.LoggingSettings, Microsoft.Practices.EnterpriseLibrary.Logging" />
	</configSections>
	
	<appSettings>
		<add key="CommandTimeout" value="300"/>
	</appSettings>

	<!-- Scheduler configuration -->
	<SchedulerConfiguration>
		<ComCode>THL</ComCode>
		<UsrId>Flex Scheduler</UsrId>
		<DateFileDirectory>./Files/</DateFileDirectory>

		<FinalizeExportEnable>true</FinalizeExportEnable>
		<FinalizeBookingWeekEnable>true</FinalizeBookingWeekEnable>

		<EmailExportEnable>true</EmailExportEnable>
		<EmailExportDebug>true</EmailExportDebug>
		<EmailExportDebugAddress>wilfred.verkley@thlonline.com</EmailExportDebugAddress>
		<EmailSmtpServer>akl-xch-001.thl.com</EmailSmtpServer>
		<EmailSmtpPort>25</EmailSmtpPort>

		<FtpExportEnable>false</FtpExportEnable>
		<FtpExportDebug>true</FtpExportDebug>
		<FtpExportDebugUri>ftp://localhost/</FtpExportDebugUri>
		<FtpExportDebugUserName></FtpExportDebugUserName>
		<FtpExportDebugPassword></FtpExportDebugPassword>

		<AuroraExportEnable>true</AuroraExportEnable>
		
		<Brands>
			<SchedulerBrand>
				<BrandCode></BrandCode>
				<BrandName>Aurora</BrandName>
				<EmailTemplateFile>Template\AuroraEmailTemplate.config</EmailTemplateFile>
				<EmailFromAddress>wilfred.verkley@thlonline.com</EmailFromAddress>
				<EmailFromName>Wilfred Verkley</EmailFromName>
			</SchedulerBrand>
			<SchedulerBrand>
				<BrandCode>B</BrandCode>
				<BrandName>Britz</BrandName>
				<EmailTemplateFile>Template\BritzEmailTemplate.config</EmailTemplateFile>
				<EmailFromAddress>wilfred.verkley@thlonline.com</EmailFromAddress>
				<EmailFromName>Wilfred Verkley</EmailFromName>
			</SchedulerBrand>
			<SchedulerBrand>
				<BrandCode>P</BrandCode>
				<BrandName>Backpacker</BrandName>
				<EmailTemplateFile>Template\BackpackerEmailTemplate.config</EmailTemplateFile>
				<EmailFromAddress>wilfred.verkley@thlonline.com</EmailFromAddress>
				<EmailFromName>Wilfred Verkley</EmailFromName>
			</SchedulerBrand>
		</Brands>
	</SchedulerConfiguration>

	<!-- Flex Export Settings 
 	 CAUTION: though this "FlexExportConfiguration" is the whats used to generate and send the export to 
	 agents and users, it has to match the version with the Aurora web.config for consistency -->
	<FlexExportConfiguration>
		<GazelleProductColumns>2B4WDB,2BB,2BBXS,2BTSB,4B4WDB,4BB,6BB,PFMR,4BBXS,,,,,,,,,,</GazelleProductColumns>
		<AuroraToIndustryLocationCodes>HBT:HBA</AuroraToIndustryLocationCodes>
		<AlwaysRequestLocations>WLG,ZQN</AlwaysRequestLocations>
		<DefaultBookingWeekText>This week’s Flex Rates are attached for your reference. Please do not hesitate to contact your dedicated Account Manager should you have any queries.

To unsubscribe to the Flex Rates updates, please reply to this email with 'Unsubscribe' in the subject line.

Yours sincerely,

The Britz Flex Rate team</DefaultBookingWeekText>

	</FlexExportConfiguration>

	<connectionStrings>
		<!--<add name="Aurora" connectionString="Data Source=AKL-KXDev-001;Initial Catalog=Aurora;Persist Security Info=True;User ID=sa;Password=@dmin" providerName="System.Data.SqlClient" />-->
    <add name="Aurora" connectionString="Data Source=COR-SQL-001;Initial Catalog=Aurora;Persist Security Info=True;User ID=AuroraWeb;Password=@dmin$123" providerName="System.Data.SqlClient"/>
	</connectionStrings>
	<dataConfiguration defaultDatabase="Aurora" />

	<loggingConfiguration tracingEnabled="true" defaultCategory="Information">
		<logFilters>
			<add 
				name="LogEnabled Filter" 
				type="Microsoft.Practices.EnterpriseLibrary.Logging.Filters.LogEnabledFilter, Microsoft.Practices.EnterpriseLibrary.Logging, Version=2.0.0.0, Culture=neutral, PublicKeyToken=null" 
				enabled="true" />
			<add 
				titles="FLEX_SCHEDULER" 
				type="Aurora.Common.CustomLogFilter, Aurora.Common, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" 
				name="Custom Filter" 
				enabled="true" />
		</logFilters>
		<categorySources>
			<add name="Errors" switchValue="All">
				<listeners>
					<add name="Event Log Destination"/>
					<add name="Console Destination"/>
					<add name="Flat File Destination"/>
				</listeners>
			</add>
			<add name="Warning" switchValue="All">
				<listeners>
					<add name="Console Destination"/>
					<add name="Flat File Destination"/>
				</listeners>
			</add>
			<add name="Information" switchValue="All">
				<listeners>
					<add name="Console Destination"/>
					<add name="Flat File Destination"/>
				</listeners>
			</add>
			<add name="Debug" switchValue="All">
				<listeners>
					<add name="Console Destination"/>
					<add name="Flat File Destination"/>
				</listeners>
			</add>
		</categorySources>
		<specialSources>
			<errors name="Errors" switchValue="All">
			</errors>
		</specialSources>
		<listeners>
			<add 
				name="Event Log Destination" 
				type="Microsoft.Practices.EnterpriseLibrary.Logging.TraceListeners.FormattedEventLogTraceListener, Microsoft.Practices.EnterpriseLibrary.Logging" 
				listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.FormattedEventLogTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" 
				source="Aurora" 
				formatter="Text Formatter"/>
			<add 
				name="Console Destination" 
				type="Aurora.Common.ConsoleTraceListener, Aurora.Common" 
				listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.CustomTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" 
				formatter="Text Formatter"/>
			<add 
				name="Flat File Destination" 
				type="Aurora.Common.DateFlatFileTraceListener, Aurora.Common" 
				listenerDataType="Microsoft.Practices.EnterpriseLibrary.Logging.Configuration.CustomTraceListenerData, Microsoft.Practices.EnterpriseLibrary.Logging" 
				fileName="Log\Scheduler_{Date}.txt" 
				formatter="Text Formatter"/>
		</listeners>
		<formatters>
			<add 
				name="Text Formatter" 
				type="Microsoft.Practices.EnterpriseLibrary.Logging.Formatters.TextFormatter, Microsoft.Practices.EnterpriseLibrary.Logging" 
				template="{timestamp} - {category} : {message}"/>
		</formatters>
	</loggingConfiguration>

</configuration>
