Imports System.Collections.Generic
Imports System.Configuration
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Drawing
Imports System.Net.Mail
Imports System.Data
Imports System.Data.SqlClient

Imports Aurora.Common
Imports Aurora.Flex.Scheduler
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports Aurora.Flex.Services


''' <summary>
''' The scheduler is a console application that processes all the scheduled flex exports when past their scheduled
''' time and either emails, ftp's, or places them in Aurora, as well as saving the files to an archive directory.
''' Once exports are sent, the brand or whole booking week is "finalized" (marked as sent and historical data recorded)
''' </summary>
Class Scheduler
    Implements IDisposable

    Public Shared ReadOnly config As SchedulerConfiguration = SchedulerConfiguration.Instance

    Public Const FunctionCode As String = "FLEX_SCHEDULER"

    Public Sub New()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
    End Sub

    ''' <summary>
    ''' Save the text in an export to a file
    ''' </summary>
    Private Sub ExecuteFile(ByVal bookingWeek As BookingWeek, ByVal export As Export)

        Logging.LogInformation(FunctionCode, "    Executing File: " + export.WeekExportRow.Description + ", " + export.WeekExportRow.FileName)

        Dim dataFileDirectory As String = Path.GetFullPath(config.DataFileDirectory) & "\" & bookingWeek.BookingWeekRow.DirectoryName

        If Not Directory.Exists(dataFileDirectory) Then
            Directory.CreateDirectory(dataFileDirectory)
        End If

        'Using writer As New StreamWriter(dataFileDirectory & "\" & export.WeekExportRow.FileName)
        '    writer.Write(export.Text)
        'End Using

        Dim oEncoding As New System.Text.UTF8Encoding(False, False)
        ' This constructor prevents the BOM (Byte Order Mark) being generated at the start of the file
        ' this stops some unix servers from messing up the data
        Using writer As New StreamWriter(dataFileDirectory & "\" & export.WeekExportRow.FileName, False, oEncoding)
            ' specifying the encoding in this constructor forces the UTF-8 encoding to be used properly and without BOM at the start of file
            writer.Write(export.Text)
        End Using

    End Sub

    ''' <summary>
    ''' Upload the export to a specified ftp directory
    ''' </summary>
    Private Sub ExcuteFtp(ByVal bookingWeek As BookingWeek, ByVal export As Export)
        If Not config.FtpExportEnable OrElse Not export.WeekExportRow.FlxIsFtp Then Return

        Logging.LogInformation(FunctionCode, "    Executing Ftp: " + export.WeekExportRow.Description)

        Dim userName As String = ""
        Dim password As String = ""
        Dim uri As String = ""

        Try
            If Not config.FtpExportDebug Then
                If Not export.WeekExportRow.IsFlxFtpUserNameNull Then userName = export.WeekExportRow.FlxFtpUserName
                If Not export.WeekExportRow.IsFlxFtpPasswordNull Then password = export.WeekExportRow.FlxFtpPassword
                If Not export.WeekExportRow.IsFlxFtpUriNull Then uri = export.WeekExportRow.FlxFtpUri
            Else
                userName = config.FtpExportDebugUserName
                password = config.FtpExportDebugPassword
                uri = config.FtpExportDebugUri
            End If

            If String.IsNullOrEmpty(uri) Then
                Return
            End If

            If Not uri.EndsWith("/") Then uri += "/"
            uri += export.WeekExportRow.FileName

            Dim request As FtpWebRequest = CType(WebRequest.Create(uri), FtpWebRequest)
            request.UseBinary = False
            request.Method = WebRequestMethods.Ftp.UploadFile
            If Not String.IsNullOrEmpty(userName) Then request.Credentials = New NetworkCredential(userName, password)

            'Using stream As Stream = request.GetRequestStream()
            '    Using writer As StreamWriter = New StreamWriter(stream, System.Text.Encoding.UTF8)
            '        writer.Write(export.Text)
            '    End Using
            'End Using

            Dim oEncoding As New System.Text.UTF8Encoding(False, False)
            ' This constructor prevents the BOM (Byte Order Mark) being generated at the start of the file
            ' this stops some unix servers from messing up the data
            Using stream As Stream = request.GetRequestStream()
                Using writer As StreamWriter = New StreamWriter(stream, oEncoding)
                    ' specifying the encoding in this constructor forces the UTF-8 encoding to be used properly and without BOM at the start of file
                    writer.Write(export.Text)
                End Using
            End Using

            Using response As FtpWebResponse = CType(request.GetResponse(), FtpWebResponse)
                If response.StatusCode <> FtpStatusCode.ClosingData Then
                    Throw New Exception("Ftp File upload failed: " + response.StatusDescription)
                End If
            End Using

        Catch ex As Exception
            Logging.LogError(FunctionCode, "Ftp error: " + uri, ex)
        End Try

    End Sub

    ''' <summary>
    ''' Execute the sql in the export into aurora
    ''' </summary>
    Private Sub ExcuteAurora(ByVal bookingWeek As BookingWeek, ByVal export As Export)
        If Not config.AuroraExportEnable _
         OrElse Not export.WeekExportRow.FlxIsAurora Then Return

        Logging.LogInformation(FunctionCode, "    Executing Aurora: " + export.WeekExportRow.Description)
        Try
            Aurora.Common.Data.ExecuteScalarSQL(export.Text)
        Catch ex As Exception
            Logging.LogError(FunctionCode, "Aurora error", ex)
        End Try
    End Sub

    ''' <summary>
    ''' Email all the exports associated with an email contact 
    ''' </summary>
    Private Sub ExecuteEmailContact( _
     ByVal bookingWeek As BookingWeek, _
     ByVal brdCode As String, _
     ByVal schedulerBrand As SchedulerBrand, _
     ByVal exportContactsRow As ExportContactsRow, _
     ByVal exports As List(Of Export))

        Logging.LogInformation(FunctionCode, "      Executing Email Contact: " & exportContactsRow.Description & " for " & schedulerBrand.BrandName)

        If Not config.IsEmailAddressInWhiteList(exportContactsRow.Email.Trim()) Then
            Logging.LogInformation(FunctionCode, "        Ignored, not in whitelist")
            Return
        End If

        ' get the email subject and text of the booking week or brand
        Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = bookingWeek.FlexBookingWeekBrandRow(brdCode)
        Dim emailSubject As String
        Dim emailText As String
        If flexBookingWeekBrandRow Is Nothing Then
            emailSubject = bookingWeek.BookingWeekRow.FbwEmailSubject
            emailText = bookingWeek.BookingWeekRow.FbwEmailText
        Else
            emailSubject = flexBookingWeekBrandRow.FbbEmailSubject
            emailText = flexBookingWeekBrandRow.FbbEmailText
        End If

       
        
        Try
            Using template As EmailTemplateFile = EmailTemplateFile.Load(schedulerBrand.EmailTemplateFile)
                ' Host info
                template.Host = config.EmailSmtpServer
                template.Port = config.EmailSmtpPort

                ' Subject and Addresses
                template.Subject = emailSubject
                template.AddressFrom = New MailAddress(schedulerBrand.EmailFromAddress, schedulerBrand.EmailFromName)
                template.AddressTo.Clear()
                If Not config.EmailExportDebug Then
                    template.AddressTo.Add(New MailAddress(exportContactsRow.Email, exportContactsRow.Name))
                Else
                    template.AddressTo.Add(New MailAddress(config.EmailExportDebugAddress, exportContactsRow.Name))
                End If

                ' Basic content
                template.Parameters.DocumentElement.RemoveAll()
                template.AddParameter("Title", emailSubject)
                template.AddParameter("Text", emailText)

                template.Attachments.Clear()

                '' Static attachments (from an attachment directory if specified for the brand)
                'Try
                '    If Not String.IsNullOrEmpty(schedulerBrand.AttachmentPath) _
                '     AndAlso Directory.Exists(schedulerBrand.AttachmentPath) Then
                '        For Each fileName As String In Directory.GetFiles(schedulerBrand.AttachmentPath)
                '            If File.Exists(fileName) Then
                '                Logging.LogInformation(FunctionCode, "        Adding attachment: " + fileName)
                '                template.Attachments.Add(fileName)
                '            End If
                '        Next
                '    End If
                'Catch ex As Exception
                '    ' Log warning, but dont prevent the rest of the email execution
                '    Logging.LogWarning(FunctionCode, "        Error adding attachments", ex)
                'End Try

                ' File export attachments
                Dim dataFileDirectory As String = Path.GetFullPath(config.DataFileDirectory) & "\" & bookingWeek.BookingWeekRow.DirectoryName
                For Each export As Export In exports

                    ''------------------------------------------------------------------------
                    ''REV:MIA MAY162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                    Try
                        Dim filenameHeader As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                        Dim subjectHeader As String = IIf(filenameHeader.Contains("DOM"), " Domestic ", " International ")

                        If (filenameHeader.Contains("SUMMARY") = True) Or (export.WeekExportRow.Description.ToUpper().IndexOf("SUMMARY") > -1) Then
                            template.Subject = emailSubject.Insert(emailSubject.IndexOf(" ") + 1, subjectHeader)
                        End If

                    Catch ex As Exception
                        ''DO NOTHING
                    End Try
                    ''------------------------------------------------------------------------
                    
                    If export.WeekExportRow.FlxCode = schedulerBrand.HtmlExportCode Then
                        Dim productsElement As XmlElement = XmlUtil.CreateElement(template.Parameters.DocumentElement, schedulerBrand.HtmlExportParameterName)
                        productsElement.InnerXml = export.Text

                    Else
                        Logging.LogInformation(FunctionCode, "        Adding attachment: " + export.WeekExportRow.FileName)
                        template.Attachments.Add(dataFileDirectory & "\" & export.WeekExportRow.FileName)
                    End If

                    ' Static attachments (from an attachment directory if specified for the brand)
                    If export.WeekExportRow.Description.ToUpper().IndexOf("SUMMARY") > -1 Then
                        Try
                            If Not String.IsNullOrEmpty(schedulerBrand.AttachmentPath) _
                             AndAlso Directory.Exists(schedulerBrand.AttachmentPath) Then
                                For Each fileName As String In Directory.GetFiles(schedulerBrand.AttachmentPath)
                                    Logging.LogDebug(FunctionCode, "        Adding static attachment: " + fileName)
                                    If File.Exists(fileName) Then
                                        template.Attachments.Add(fileName)
                                    End If
                                Next
                            End If
                        Catch ex As Exception
                            ' Log warning, but dont prevent the rest of the email execution
                            Logging.LogWarning(FunctionCode, "        Error adding static attachments", ex)
                        End Try
                    End If

                Next

                ' Done, send it
                template.Send()
            End Using
        Catch ex As Exception
            Logging.LogError(FunctionCode, "        Error Executing Email Contact", ex)
        End Try
    End Sub

    ''' <summary>
    ''' For all the exports just generated, find their unique email contacts, then for every unique contact,
    ''' aggregate, and send them the exports relevant to them
    ''' </summary>
    Private Sub ExecuteEmail(ByVal bookingWeek As BookingWeek, ByVal brdCode As String, ByVal exports As List(Of Export))
        If Not config.EmailExportEnable Then Return

        Dim schedulerBrand As SchedulerBrand = config.BrandsByBrdCode(brdCode)
        If schedulerBrand Is Nothing Then Return

        Logging.LogInformation(FunctionCode, "    Executing Email")

        Dim contactsByEmail As New Dictionary(Of String, ExportContactsRow)
        Dim exportsByEmail As New Dictionary(Of String, List(Of Export))

        For Each export As Export In exports
            For Each exportContactsRow As ExportContactsRow In export.ExportContactsDataTable
                If Not contactsByEmail.ContainsKey(exportContactsRow.Email) Then
                    contactsByEmail.Add(exportContactsRow.Email, exportContactsRow)
                End If
                If Not exportsByEmail.ContainsKey(exportContactsRow.Email) Then
                    exportsByEmail.Add(exportContactsRow.Email, New List(Of Export))
                End If
                exportsByEmail(exportContactsRow.Email).Add(export)
            Next
        Next

        For Each email As String In contactsByEmail.Keys
            Dim exportContactsRow As ExportContactsRow = contactsByEmail(email)
            Dim exportsEmail As List(Of Export) = exportsByEmail(email)

            ExecuteEmailContact(bookingWeek, brdCode, schedulerBrand, exportContactsRow, exportsEmail)
        Next
    End Sub

    ''' <summary>
    ''' Execute a brand / booking weeek, sending all the booking weeks we can send, and finalize the booking week when
    ''' everything that can be sent, has been
    ''' </summary>
    Private Sub ExecuteBookingWeekBrand(ByVal bookingWeek As BookingWeek, ByVal brdCode As String)

        If ((String.IsNullOrEmpty(brdCode) AndAlso Not bookingWeek.CanFinalizeBookingWeek()) _
          OrElse (Not String.IsNullOrEmpty(brdCode) AndAlso Not bookingWeek.CanFinalizeBookingWeekBrand(brdCode))) Then
            Return
        End If

        If String.IsNullOrEmpty(brdCode) Then
            Logging.LogInformation(FunctionCode, "  Booking Week: " & bookingWeek.BookingWeekRow.Description)
        Else
            Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = bookingWeek.FlexBookingWeekBrandRow(brdCode)
            Logging.LogInformation(FunctionCode, "  Booking Week Brand: " & bookingWeek.BookingWeekRow.Description & " - " & flexBookingWeekBrandRow.Description)
        End If

        ' loop through the export rows and send things
        Dim exports As New List(Of Export)
        Dim exportSent As Boolean = False
        For Each weekExportsRow As WeekExportsRow In bookingWeek.GetWeekExportsForBrandCode(brdCode)
            If Not weekExportsRow.CanFinalize Then Continue For

            Dim export As New Export(weekExportsRow)
            If Not export.CanFinalizeExport() Then Continue For ' check again for security

            If config.FinalizeExportEnable Then
                Logging.LogInformation(FunctionCode, "    Finalizing Export: " + export.WeekExportRow.Description)
                export.FinalizeExport(weekExportsRow.IntegrityNo, config.UsrId)

                ' horrible hack, but export is a business object with its own instance of the weekExportRow data row
                ' so we update the booking week weekExportRow manually
                weekExportsRow.FwxStatus = export.WeekExportRow.FwxStatus
                weekExportsRow.FwxText = export.WeekExportRow.FwxText
                weekExportsRow.FwxContactsDescription = export.WeekExportRow.FwxContactsDescription
                weekExportsRow.IntegrityNo = export.WeekExportRow.IntegrityNo
                weekExportsRow.ModUsrId = export.WeekExportRow.ModUsrId
                weekExportsRow.ModDateTime = export.WeekExportRow.ModDateTime
            End If

            ExecuteFile(bookingWeek, export)
            ExcuteFtp(bookingWeek, export)
            ExcuteAurora(bookingWeek, export)

            exports.Add(export)
            exportSent = True
        Next

        If exportSent Then
            ' emails are formed from an aggregate of multiple exports all sent out
            ExecuteEmail(bookingWeek, brdCode, exports)
        End If

        ' if this is a booking week, and *all* the exports are sent, finalize it
        If String.IsNullOrEmpty(brdCode) _
         AndAlso bookingWeek.CanFinalizeBookingWeek() _
         AndAlso bookingWeek.AreAllExportsSent() _
         AndAlso config.FinalizeBookingWeekEnable Then
            Logging.LogInformation(FunctionCode, "    Finalizing Booking Week")
            bookingWeek.FinalizeBookingWeek(bookingWeek.BookingWeekRow.IntegrityNo, config.UsrId)
        End If

        ' if this is a brand, and all *its own* exports are sent, finalize its products, and finalize the brand
        If Not String.IsNullOrEmpty(brdCode) _
         AndAlso bookingWeek.CanFinalizeBookingWeekBrand(brdCode) _
         AndAlso bookingWeek.AreExportsSent(brdCode) _
         AndAlso config.FinalizeBookingWeekEnable Then

            Logging.LogInformation(FunctionCode, "    Finalizing Products")
            For Each weekProductsRow As WeekProductsRow In bookingWeek.GetWeekProductsForBrandCode(brdCode)
                Logging.LogInformation(FunctionCode, "      Finalizing Product: " + weekProductsRow.LongDescription)
                Dim product As Product = New Product(weekProductsRow)
                product.FinalizeProduct(weekProductsRow.IntegrityNo, config.UsrId)
            Next

            Logging.LogInformation(FunctionCode, "    Finalizing Brand")
            Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = bookingWeek.FlexBookingWeekBrandRow(brdCode)
            bookingWeek.FinalizeBookingWeekBrand(brdCode, flexBookingWeekBrandRow.IntegrityNo, config.UsrId)
        End If
    End Sub

    ''' <summary>
    ''' Execute a booking week, sending all the booking weeks we can send, and finalize the booking week when
    ''' everything that can be sent, has been
    ''' </summary>
    Private Sub ExecuteBookingWeek(ByVal bookingWeek As BookingWeek)
        ' do brands first
        For Each flexBookingWeekBrandRow As FlexBookingWeekBrandRow In bookingWeek.FlexBookingWeekBrandDataTable
            ExecuteBookingWeekBrand(bookingWeek, flexBookingWeekBrandRow.FbbBrdCode)
        Next

        ' finally do the booking week
        ExecuteBookingWeekBrand(bookingWeek, Nothing)
    End Sub

    ''' <summary>
    ''' Run/execute the scheduler, and process all the outstanding (approved but unsent) booking weeks
    ''' </summary>
    Public Sub Execute()
        Logging.LogInformation(FunctionCode, "Executing")
        Try
            ' loop through the booking weeks backwards (in chronological order) and execute any booking weeks
            ' not yet sent
            Dim bookingWeeks As New BookingWeeks(config.ComCode)
            For i As Integer = bookingWeeks.FlexBookingWeekDataTable.Rows.Count - 1 To 0 Step -1
                Dim flexBookingWeekRow As FlexBookingWeekRow = bookingWeeks.FlexBookingWeekDataTable(i)
                If flexBookingWeekRow.IsSent Then Continue For

                Try
                    Dim bookingWeek As New BookingWeek(flexBookingWeekRow)
                    ExecuteBookingWeek(bookingWeek)
                Catch ex As Exception
                    Logging.LogError(FunctionCode, "Error", ex)
                End Try
            Next

            Logging.LogInformation(FunctionCode, "Done")
        Catch ex As Exception
            Logging.LogError(FunctionCode, "Error", ex)
        End Try

    End Sub

    Shared Sub Main()

        Using scheduler As New Scheduler()
            scheduler.Execute()
        End Using

    End Sub


End Class
