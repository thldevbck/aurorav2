Imports System.Collections.Generic
Imports System.Net
Imports System.IO
Imports System.Xml
Imports System.Drawing

Imports Aurora.Common
Imports Aurora.Flex.Scheduler
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports Aurora.Flex.Services

''' <summary>
''' Class to generate the email paramaters from a list of exports given to it.
''' 
''' This is a bit like an export.....but it effectively works on a superset of exports, so we cant 
''' use a ExportGenerator as used bt ExportGenerators.
''' </summary>
''' <remarks></remarks>
Public Module EmailParameterGenerator

    '	<Parameters>
    '		<Title>Booking Week for 1 April 2007</Title>
    '		<Text>The flex rates are attached for your reference. Please do not hesitate to contact your
    'dedicated account manager should you have any queries. To unsubscribe to the Flex updates please reply
    'to this email with 'Unsubscribe' in the subject line.

    'Yours Sincerely,
    'Britz flex rate team
    '		</Text>
    '		<Product Name="2BB - Hitop">
    '			<WeekHeaders>
    '				<WeekHeader Number="1" Month="Apr" Day="1" TimeCode="past" />
    '			</WeekHeaders>
    '			<Location Name="ADL, ASP, BME, BNE, CNS, DRW, MEL, MKY, PER, SYD">
    '				<Week Number="1" Rate="D7" RateColor="#B4B4FF" FleetStatus="R" FleetStatusColor="#FFEEEE" />
    '			</Location>
    '		</Product>
    '	</Parameters>




    '			<WeekHeaders>
    '				<WeekHeader Number="1" Month="Apr" Day="1" TimeCode="past" />
    '			</WeekHeaders>
    Private Sub ExecuteEmailParametersWeekHeader( _
     ByVal template As EmailTemplateFile, _
     ByVal productElement As XmlElement, _
     ByVal product As Product)

        Dim weekHeadersElement As XmlElement = productElement.AppendChild(template.Parameters.CreateElement("WeekHeaders"))
        For Each flexWeekNumberRow As FlexWeekNumberRow In product.FlexWeekNumberDataTable
            Dim weekHeaderElement As XmlElement = weekHeadersElement.AppendChild(template.Parameters.CreateElement("WeekHeader"))

            weekHeaderElement.SetAttribute("Number", flexWeekNumberRow.FwnWkNo.ToString())
            weekHeaderElement.SetAttribute("Month", flexWeekNumberRow.FwnTrvWkStart.ToString("MMM"))
            weekHeaderElement.SetAttribute("Day", flexWeekNumberRow.FwnTrvWkStart.ToString("dd"))
            If flexWeekNumberRow.IsPast(product.WeekProductRow.FbwBookStart) Then
                weekHeaderElement.SetAttribute("TimeCode", "past")
            ElseIf flexWeekNumberRow.IsCurrent(product.WeekProductRow.FbwBookStart) Then
                weekHeaderElement.SetAttribute("TimeCode", "current")
            ElseIf flexWeekNumberRow.IsFuture(product.WeekProductRow.FbwBookStart) Then
                weekHeaderElement.SetAttribute("TimeCode", "future")
            End If
        Next

    End Sub

    '			<Location Name="ADL, ASP, BME, BNE, CNS, DRW, MEL, MKY, PER, SYD">
    '				<Week Number="1" Rate="D7" RateColor="#B4B4FF" FleetStatus="R" FleetStatusColor="#FFEEEE" />
    '			</Location>
    Private Sub ExecuteEmailParametersLocation( _
     ByVal template As EmailTemplateFile, _
     ByVal productElement As XmlElement, _
     ByVal product As Product, _
     ByVal locCode As String)

        Dim locCodes As New List(Of String)
        If locCode Is Nothing Then
            For Each locationsRow As LocationsRow In product.LocationsDataTable
                If locCode Is Nothing AndAlso product.IsLocationIncluded(locationsRow.LocCode) _
                 AndAlso Not product.IsLocationException(locationsRow.LocCode) Then
                    locCodes.Add(locationsRow.LocCode)
                End If
            Next
        Else
            locCodes.Add(locCode)
        End If

        If locCodes.Count > 0 Then
            Dim locationElement As XmlElement = productElement.AppendChild(template.Parameters.CreateElement("Location"))
            locationElement.SetAttribute("Name", String.Join(", ", locCodes.ToArray()))
            For Each flexWeekNumberRow As FlexWeekNumberRow In product.FlexWeekNumberDataTable

                Dim flexBookingWeekRateRow As FlexBookingWeekRateRow = product.FlexBookingWeekRateByWkNo(flexWeekNumberRow.FwnWkNo)

                Dim weekElement As XmlElement = locationElement.AppendChild(template.Parameters.CreateElement("Week"))
                weekElement.SetAttribute("Number", flexWeekNumberRow.FwnWkNo.ToString())
                Dim flexNum As Integer
                If locCode Is Nothing Then
                    flexNum = flexBookingWeekRateRow.FlrFlexNum
                Else
                    flexNum = product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locCode)
                End If

                Dim rate As String = FlexConstants.FlexNumToRate(flexNum)
                If Not flexBookingWeekRateRow.FlrChanged Then
                    weekElement.SetAttribute("Rate", FlexConstants.FlexNumToRate(flexNum))
                    weekElement.SetAttribute("RateColor", ColorTranslator.ToHtml(FlexConstants.FlexNumToColor(flexNum)))
                Else
                    weekElement.SetAttribute("Rate", FlexConstants.FlexNumToRate(flexNum) + "*")
                    weekElement.SetAttribute("RateColor", ColorTranslator.ToHtml(FlexConstants.FlexRateChangedColor))
                End If
                weekElement.SetAttribute("FleetStatus", product.FleetStatusByWkNo(flexWeekNumberRow.FwnWkNo))
                weekElement.SetAttribute("FleetStatusColor", ColorTranslator.ToHtml(FlexConstants.FleetStatusToColor(product.FleetStatusByWkNo(flexWeekNumberRow.FwnWkNo))))
            Next
        End If

    End Sub

    '		<Title>Booking Week for 1 April 2007</Title>
    '		<Text>The flex rates are attached for your reference. Please do not hesitate to contact your
    'dedicated account manager should you have any queries. To unsubscribe to the Flex updates please reply
    'to this email with 'Unsubscribe' in the subject line.

    'Yours Sincerely,
    'Britz flex rate team
    '		</Text>
    '		<Product Name="2BB - Hitop">
    '		</Product>
    Public Sub Create( _
     ByVal template As EmailTemplateFile, _
     ByVal bookingWeek As BookingWeek, _
     ByVal exportContactsRow As ExportContactsRow, _
     ByVal exports As List(Of Export))

        Dim config As SchedulerConfiguration = SchedulerConfiguration.Instance

        template.Parameters.DocumentElement.RemoveAll()
        template.AddParameter("Title", bookingWeek.BookingWeekRow.EmailSubject)
        If Not bookingWeek.BookingWeekRow.IsFbwEmailTextNull Then
            template.AddParameter("Text", bookingWeek.BookingWeekRow.FbwEmailText)
        Else
            template.AddParameter("Text", "")
        End If

        ' check if the agent or user is configured to get a fancy export
        If Not exportContactsRow.IsAgcEmailNull AndAlso Not config.EmailExportFancyAgent Then Return
        If Not exportContactsRow.IsUsrIdNull AndAlso Not config.EmailExportFancyUser Then Return

        ' get all the unique products in all the exports (different exports may contain the same product)
        Dim productsById As New Dictionary(Of Integer, Product)
        Dim productsBySortKey As New Dictionary(Of String, Product)
        Dim sortKeys As New List(Of String)()
        For Each export As Export In exports
            For Each product As Product In export.Products
                If Not productsById.ContainsKey(product.WeekProductRow.FlpId) Then
                    productsById.Add(product.WeekProductRow.FlpId, product)
                    productsBySortKey.Add(product.WeekProductRow.SortKey, product)
                    sortKeys.Add(product.WeekProductRow.SortKey)
                End If
            Next
        Next
        sortKeys.Sort()

        ' loop through each of the products and start making the xml
        For Each sortKey As String In sortKeys
            Dim product As Product = productsBySortKey(sortKey)

            Dim productElement As XmlElement = template.Parameters.DocumentElement.AppendChild(template.Parameters.CreateElement("Product"))

            productElement.SetAttribute("Name", product.WeekProductRow.LongDescription)

            ' create week headers
            ExecuteEmailParametersWeekHeader(template, productElement, product)

            ' create a line for all available locations that dont have an exception
            ExecuteEmailParametersLocation(template, productElement, product, Nothing)

            ' create a line for all available locations that dont have an exception
            For Each locationsRow As LocationsRow In product.LocationsDataTable
                If product.IsLocationException(locationsRow.LocCode) Then
                    ExecuteEmailParametersLocation(template, productElement, product, locationsRow.LocCode)
                End If
            Next
        Next


    End Sub

End Module
