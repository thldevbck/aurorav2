Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration


Public Class SchedulerConfiguration

    ' Database audit settings
    Public ComCode As String = "THL"
    Public UsrId As String = "flex scheduler"

    ' Directory where the files are archived
    Public DataFileDirectory As String = "./Files/"

    ' Finalize the exports or booking week when its (all) sent?
    Public FinalizeExportEnable As Boolean = True
    Public FinalizeBookingWeekEnable As Boolean = True

    ' Email settings
    Public EmailExportEnable As Boolean = False
    Public EmailExportDebug As Boolean = True
    Public EmailExportDebugAddress As String = "wilfred.verkley@thlonline.com"
    Public EmailSmtpServer As String = "localhost"
    Public EmailSmtpPort As Integer = 25

    Public EmailWhiteList As String = ""

    ' Ftp settings
    Public FtpExportEnable As Boolean = False
    Public FtpExportDebug As Boolean = True
    Public FtpExportDebugUri As String = "ftp://localhost/"
    Public FtpExportDebugUserName As String = ""
    Public FtpExportDebugPassword As String = ""
    Public FtpNoProxy As Boolean = True

    ' Aurora settings
    Public AuroraExportEnable As Boolean = False

    ' Brands
    Public Brands As SchedulerBrand() = {}

    Public ReadOnly Property BrandsByBrdCode(ByVal brdCode As String) As SchedulerBrand
        Get
            For Each b As SchedulerBrand In Brands
                If ("" & b.BrandCode).ToLower().Trim() = ("" & brdCode).ToLower().Trim() Then
                    Return b
                End If
            Next
            Return Nothing
        End Get
    End Property

    Private _emailWhiteList As List(Of String)
    Public Function IsEmailAddressInWhiteList(ByVal emailAddress As String) As Boolean
        If String.IsNullOrEmpty(EmailWhiteList.Trim()) Then Return True

        emailAddress = ("" & emailAddress).Trim().ToLower()

        If _emailWhiteList Is Nothing Then
            _emailWhiteList = New List(Of String)
            _emailWhiteList.AddRange(EmailWhiteList.Replace(vbCr, "").ToLower().Split(vbLf))
        End If

        For Each s As String In _emailWhiteList
            If s.Trim() = emailAddress Then Return True
        Next

        Return False
    End Function

    Private Shared _instance As SchedulerConfiguration
    Public Shared ReadOnly Property Instance() As SchedulerConfiguration
        Get
            If _instance Is Nothing Then _instance = CType(ConfigurationManager.GetSection("SchedulerConfiguration"), SchedulerConfiguration)
            If _instance Is Nothing Then _instance = New SchedulerConfiguration()
            Return _instance
        End Get
    End Property

End Class

Public Class SchedulerBrand
    Public BrandCode As String
    Public BrandName As String
    Public EmailTemplateFile As String
    Public EmailFromAddress As String = "wilfred.verkley@thlonline.com"
    Public EmailFromName As String = "Wilfred Verkley"
    Public HtmlExportCode As String = "SUMMARY"
    Public HtmlExportParameterName As String = "Products"
    Public AttachmentPath As String
End Class


Public Class SchedulerConfigurationSectionHandler
    Implements IConfigurationSectionHandler

    Public Function Create(ByVal parent As Object, ByVal configContext As Object, _
        ByVal section As System.Xml.XmlNode) As Object _
        Implements System.Configuration.IConfigurationSectionHandler.Create

        Dim xs As XmlSerializer = New XmlSerializer(GetType(SchedulerConfiguration))
        Return xs.Deserialize(New XmlNodeReader(section))
    End Function

End Class
