Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration

Public Class XmlSerializerSectionHandler
    Implements IConfigurationSectionHandler

    Public Function Create(ByVal parent As Object, ByVal configContext As Object, _
        ByVal section As System.Xml.XmlNode) As Object _
        Implements System.Configuration.IConfigurationSectionHandler.Create

        Dim xpn As XPathNavigator = section.CreateNavigator
        Dim TypeName As String = xpn.Evaluate("string(@type)").ToString
        Dim t As Type = Type.GetType(TypeName)
        Dim xs As XmlSerializer = New XmlSerializer(t)

        Return xs.Deserialize(New XmlNodeReader(section))
    End Function

End Class