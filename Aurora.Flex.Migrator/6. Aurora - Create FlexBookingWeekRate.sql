/*
DECLARE @FlpTravelYear AS datetime
DECLARE	@FlpId AS int
DECLARE	@FwnWkNo AS int
DECLARE	@FlrFlexNum AS int
DECLARE	@FlrChanged AS bit

SET @FlpTravelYear = '1/04/2006'
SET @FlpId = 1
SET @FwnWkNo = 1
SET @FlrFlexNum = 1
SET @FlrChanged = 0
*/

INSERT INTO FlexBookingWeekRate
(
	FlrFlpId,
	FlrWkNo,
	FlrTravelFrom,
	FlrFlexNum,
	FlrChanged
)
SELECT 
	Params.FlpId AS FlrFlpId,
	FlexWeekNumber.FwnWkNo AS FlrWkNo,
	FlexWeekNumber.FwnTrvWkStart AS FlrTravelFrom,
	Params.FlrFlexNum AS FlrFlexNum,
	Params.FlrChanged AS FlrChanged
FROM
	(SELECT 	
		@FlpTravelYear AS FlpTravelYear,
		@FlpId AS FlpId,
		@FwnWkNo AS FwnWkNo,
		@FlrFlexNum AS FlrFlexNum,
		@FlrChanged AS FlrChanged
	) Params
	INNER JOIN FlexWeekNumber 
	 ON FlexWeekNumber.FwnTrvYearStart = Params.FlpTravelYear
		AND FlexWeekNumber.FwnWkNo = Params.FwnWkNo
	LEFT JOIN FlexBookingWeekRate
	 ON FlexBookingWeekRate.FlrFlpId = Params.FlpId
		AND FlexBookingWeekRate.FlrTravelFrom = FlexWeekNumber.FwnTrvWkStart
WHERE
	FlexBookingWeekRate.FlrId IS NULL


IF SCOPE_IDENTITY() IS NOT NULL
BEGIN
	SELECT 
		* 
	FROM 
		FlexBookingWeekRate 
	WHERE 
		FlexBookingWeekRate.FlrId = SCOPE_IDENTITY()
END