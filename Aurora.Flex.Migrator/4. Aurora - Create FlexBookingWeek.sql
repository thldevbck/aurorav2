/*
DECLARE	@FbwBookStart AS datetime
DECLARE	@FbwTravelYearStart AS datetime
DECLARE	@FbwTravelYearEnd AS datetime

SET @FbwBookStart = '20/03/2006'
SET @FbwTravelYearStart = '01/04/2006'
SET @FbwTravelYearEnd = '31/03/2007'
*/

INSERT INTO FlexBookingWeek
(
	FbwComCode,
	FbwBookStart,
	FbwTravelYearStart,
	FbwTravelYearEnd,
	FbwVersion,
	FbwStatus,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
)
SELECT
	'THL' AS FbwComCode,
	@FbwBookStart AS FbwBookStart,
	@FbwTravelYearStart AS FbwTravelYearStart,
	@FbwTravelYearEnd AS FbwTravelYearEnd,
	1 AS FbwVersion,
	'sent' AS FbwStatus,
	1 AS IntegrityNo,
	'wv1' AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	'migrator' AS AddPrgmName
WHERE
	NOT EXISTS (SELECT 1 FROM FlexBookingWeek WHERE FlexBookingWeek.FbwBookStart = @FbwBookStart)
	

IF SCOPE_IDENTITY() IS NULL 
BEGIN
	UPDATE FlexBookingWeek SET
		FbwTravelYearStart = CASE WHEN @FbwTravelYearStart < FbwTravelYearStart THEN @FbwTravelYearStart ELSE FbwTravelYearStart END,
		FbwTravelYearEnd = CASE WHEN @FbwTravelYearEnd > FbwTravelYearEnd THEN @FbwTravelYearEnd ELSE FbwTravelYearEnd END
	WHERE
		FlexBookingWeek.FbwBookStart = @FbwBookStart
END 


SELECT
	FlexBookingWeek.*
FROM
	FlexBookingWeek 
WHERE
	FlexBookingWeek.FbwBookStart = @FbwBookStart

