SELECT 
	Country,
	Brand,
	Vehicle,
	Book_Start,
	Start_On,
	BookingStart,
	BookingEnd,
	TravelStart,
	TravelEnd,
	City,
	FromTo,
	Change
FROM 
	delta2
WHERE
	Start_On IS NOT NULL
	AND Book_Start IS NOT NULL
	AND Book_Start >= '26/02/2007'
	