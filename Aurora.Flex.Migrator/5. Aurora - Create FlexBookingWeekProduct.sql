/*
DECLARE	@FbwId AS int
DECLARE @FlpTravelYear AS datetime
DECLARE	@FlpCtyCode AS varchar(12)
DECLARE	@FlpBrdCode AS varchar(1)
DECLARE	@PrdShortName AS varchar(8)

SET @FbwId = 1
SET @FlpTravelYear = '1/04/2006'
SET @FlpCtyCode = 'NZ'
SET @FlpBrdCode = 'B'
SET @PrdShortName = '6BB'
*/

INSERT INTO FlexBookingWeekProduct
(
	FlpFbwId,
	FlpTravelYear,
	FlpComCode,
	FlpCtyCode,
	FlpBrdCode,
	FlpPrdId,
	FlpStatus,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
) 
SELECT 
	Params.FbwId AS FlpFbwId,
	Params.FlpTravelYear AS FlpTravelYear,
	FlexProductsView.ComCode AS FlpComCode,
	Params.FlpCtyCode AS FlpCtyCode,
	FlexProductsView.BrdCode AS FlpBrdCode,
	FlexProductsView.PrdId AS FlpPrdId,
	'approved' AS FlpStatus,
	1 AS IntegrityNo,
	'wv1' AS AddUsrId,
	NULL AS ModUsrId,
	GETDATE() AS AddDateTime,
	NULL AS ModDateTime,
	'migrator' AS AddPrgmName
FROM
	(SELECT 	
		@FbwId AS FbwId,
		@FlpTravelYear AS FlpTravelYear,
		@FlpCtyCode AS FlpCtyCode,
		@FlpBrdCode AS FlpBrdCode,
		@PrdShortName AS PrdShortName
	) Params
	INNER JOIN FlexProductsView 
		ON FlexProductsView.ComCode = 'THL'
		AND FlexProductsView.BrdCode = Params.FlpBrdCode
		AND FlexProductsView.PrdShortName = Params.PrdShortName
		AND FlexProductsView.PrdIsActive = 1
	LEFT JOIN FlexBookingWeekProduct 
		ON FlexBookingWeekProduct.FlpFbwId = Params.FbwId 
		AND FlexBookingWeekProduct.FlpTravelYear = Params.FlpTravelYear 
		AND FlexBookingWeekProduct.FlpComCode = FlexProductsView.ComCode 
		AND FlexBookingWeekProduct.FlpCtyCode = Params.FlpCtyCode
		AND FlexBookingWeekProduct.FlpBrdCode = FlexProductsView.BrdCode 
		AND FlexBookingWeekProduct.FlpPrdId = FlexProductsView.PrdId 
WHERE
	FlexBookingWeekProduct.FlpId IS NULL


IF SCOPE_IDENTITY() IS NOT NULL
BEGIN
	SELECT 
		* 
	FROM 
		FlexBookingWeekProduct 
	WHERE 
		FlexBookingWeekProduct.FlpId = SCOPE_IDENTITY()
END