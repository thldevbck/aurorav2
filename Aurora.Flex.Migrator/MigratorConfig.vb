Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration


Public Class MigratorConfigMapping
    <XmlAttribute()> Public Name As String
    <XmlAttribute()> Public Flex As String
    <XmlAttribute()> Public NZ As String
    <XmlAttribute()> Public AU As String
End Class


Public Class MigratorConfig

    Public AuroraConnectionString As String
    Public AuroraCommandTimeout As Integer

    Public FlexConnectionString As String
    Public FlexCommandTimeout As Integer

    Public MigrateRates As Boolean = True
    Public MigrateExceptions As Boolean = True

    Public LogFileName As String = "AuroraFlexMigrator.log"

    Public Mappings As MigratorConfigMapping() = {}

    Public Function GetAuroraCodesByFlexCountryVehicle(ByVal vehicle As String, ByVal country As String) As String()
        vehicle = ("" + vehicle).Trim().ToUpper()
        country = ("" + country).Trim().ToUpper()

        Dim mapping As MigratorConfigMapping = Nothing
        For Each mapping0 As MigratorConfigMapping In Mappings
            If mapping0.Flex = vehicle Then
                mapping = mapping0
            End If
        Next
        If mapping Is Nothing Then
            Return Nothing
        End If

        Dim codes As String = IIf(country = "NZ", mapping.NZ, mapping.AU)
        Dim result As New List(Of String)
        For Each code As String In codes.Split(",")
            code = code.Trim().ToUpper()
            If code <> "" Then result.Add(code)
        Next
        If result.Count > 0 Then
            Return result.ToArray()
        Else
            Return Nothing
        End If
    End Function


End Class