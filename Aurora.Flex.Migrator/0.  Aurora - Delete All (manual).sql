TRUNCATE TABLE FlexBookingWeekException
TRUNCATE TABLE FlexBookingWeekRate
DELETE FROM FlexBookingWeekProduct 
DELETE FROM FlexBookingWeek 


DBCC CHECKIDENT ( 'FlexBookingWeekException', RESEED, 0)
DBCC CHECKIDENT ( 'FlexBookingWeekRate', RESEED, 0)
DBCC CHECKIDENT ( 'FlexBookingWeekProduct', RESEED, 0)
DBCC CHECKIDENT ( 'FlexBookingWeek', RESEED, 0)
