/*
DECLARE	@FleBookStart AS datetime
DECLARE	@FleBookEnd AS datetime
DECLARE	@FleTravelStart AS datetime
DECLARE	@FleTravelEnd AS datetime
DECLARE	@FleLocCodeFrom AS varchar(12)
DECLARE	@FleLocCodeTo AS varchar(12)
DECLARE	@FleDelta AS int
DECLARE	@FbwBookStart AS datetime
DECLARE	@FlpTravelYear AS datetime
DECLARE	@CtyCode AS varchar(12)
DECLARE	@BrdCode AS varchar(1)
DECLARE	@PrdShortName AS varchar(8)


SET @FleBookStart='29/9/2003'
SET @FleBookEnd='31/3/2006'
SET @FleTravelStart='12/12/2005'
SET @FleTravelEnd='31/3/2006'
SET @FleLocCodeFrom='HBT'
SET @FleLocCodeTo = NULL
SET @FleDelta='3'
SET @FbwBookStart='7/3/2005'
SET @FlpTravelYear='1/4/2005'
SET @CtyCode='AU'
SET @BrdCode='B'
SET @PrdShortName='2B4WDB'
*/

INSERT INTO FlexBookingWeekException
(
	FleFlpId,
	FleBookStart,
	FleBookEnd,
	FleTravelStart,
	FleTravelEnd,
	FleLocCodeFrom,
	FleLocCodeTo,
	FleDelta
)
SELECT 
	FlexBookingWeekProduct.FlpId AS FleFlpId,
	Params.FleBookStart AS FleBookStart,
	Params.FleBookEnd AS FleBookEnd,
	Params.FleTravelStart AS FleTravelStart,
	Params.FleTravelEnd AS FleTravelEnd,
	Params.FleLocCodeFrom AS FleLocCodeFrom,
	Params.FleLocCodeTo AS FleLocCodeTo,
	Params.FleDelta AS FleDelta
FROM 
	(SELECT 	
		@FleBookStart AS FleBookStart,
		@FleBookEnd AS FleBookEnd,
		@FleTravelStart AS FleTravelStart,
		@FleTravelEnd AS FleTravelEnd,
		@FleLocCodeFrom AS FleLocCodeFrom,
		@FleLocCodeTo AS FleLocCodeTo,
		@FleDelta AS FleDelta,
		@FbwBookStart AS FbwBookStart,
		@FlpTravelYear AS FlpTravelYear,
		@CtyCode AS CtyCode,
		@BrdCode AS BrdCode,
		@PrdShortName AS PrdShortName
	) Params
	INNER JOIN FlexBookingWeek 
	 ON FlexBookingWeek.FbwBookStart = Params.FbwBookStart
	INNER JOIN FlexProductsView 
	 ON FlexProductsView.ComCode = 'THL'
		AND FlexProductsView.BrdCode = Params.BrdCode
		AND FlexProductsView.PrdShortName = Params.PrdShortName
		AND FlexProductsView.PrdIsActive = 1
	INNER JOIN FlexBookingWeekProduct 
	 ON FlexBookingWeekProduct.FlpFbwId = FlexBookingWeek.FbwId 
		AND FlexBookingWeekProduct.FlpTravelYear = Params.FlpTravelYear
		AND FlexBookingWeekProduct.FlpComCode = FlexProductsView.ComCode 
		AND FlexBookingWeekProduct.FlpCtyCode = Params.CtyCode 
		AND FlexBookingWeekProduct.FlpBrdCode = FlexProductsView.BrdCode 
		AND FlexBookingWeekProduct.FlpPrdId = FlexProductsView.PrdId 
	LEFT JOIN FlexBookingWeekException 
	 ON FlexBookingWeekException.FleFlpId = FlexBookingWeekProduct.FlpId
		AND FlexBookingWeekException.FleBookStart = Params.FleBookStart
		AND FlexBookingWeekException.FleBookEnd = Params.FleBookEnd
		AND FlexBookingWeekException.FleTravelStart = Params.FleTravelStart
		AND FlexBookingWeekException.FleTravelEnd = Params.FleTravelEnd
		AND ISNULL (FlexBookingWeekException.FleLocCodeFrom, '') = ISNULL (Params.FleLocCodeFrom, '')
		AND ISNULL (FlexBookingWeekException.FleLocCodeTo, '') = ISNULL (Params.FleLocCodeTo, '')
WHERE
	FlexBookingWeekException.FleId IS NULL	


IF SCOPE_IDENTITY() IS NOT NULL
BEGIN
	SELECT 
		* 
	FROM 
		FlexBookingWeekException 
	WHERE 
		FlexBookingWeekException.FleId = SCOPE_IDENTITY()
END 
