INSERT INTO FlexBookingWeekExport
(
	FwxFbwId,
	FwxFlxId,
	FwxStatus,
	FwxScheduledTime,
	IntegrityNo,
	AddUsrId,
	ModUsrId,
	AddDateTime,
	ModDateTime,
	AddPrgmName
)
SELECT 
	FlexBookingWeek.FbwId AS FwxFbwId,
	FlexExportType.FlxId AS FwxFlxId,
    FlexBookingWeek.FbwStatus AS FwxStatus, 
	DATEADD (hh, -FlexExportType.FlxScheduleHoursBefore, FlexBookingWeek.FbwBookStart) AS FwxScheduledTime,
	FlexBookingWeek.IntegrityNo AS IntegrityNo,
	FlexBookingWeek.AddUsrId AS AddUsrId,
	FlexBookingWeek.ModUsrId AS ModUsrId,
	FlexBookingWeek.AddDateTime AS AddDateTime,
	FlexBookingWeek.ModDateTime AS ModDateTime,
	FlexBookingWeek.AddPrgmName AS AddPrgmName
FROM 
	FlexBookingWeek,
	FlexExportType
WHERE
	FlexExportType.FlxIsActive = 1
	AND FlexExportType.FlxCode = 'AURORA'
	AND NOT EXISTS (SELECT 1 FROM FlexBookingWeekExport WHERE FlexBookingWeekExport.FwxFbwId = FlexBookingWeek.FbwId AND FlexBookingWeekExport.FwxFlxId = FlexExportType.FlxId)
ORDER BY
	FlexBookingWeek.FbwId,
	FlexExportType.FlxCode
