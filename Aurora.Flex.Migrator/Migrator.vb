Imports System.Configuration
Imports System.Data.SqlClient
Imports System.IO

Module Migrator
    Private config As MigratorConfig = CType(ConfigurationManager.GetSection("MigratorConfig"), MigratorConfig)

    Private auroraConnection As SqlConnection = Nothing
    Private flexConnection As SqlConnection = Nothing

    Private createBookingWeekExceptionCommand As SqlCommand = Nothing
    Private createBookingWeekCommand As SqlCommand = Nothing
    Private createBookingWeekProductCommand As SqlCommand = Nothing
    Private createBookingWeekRateCommand As SqlCommand = Nothing

    Private flexRatesTable As New DataTable()
    Private flexExceptionsTable As New DataTable()

    Private Sub LogClear()
        File.Delete(config.LogFileName)
    End Sub

    Private Sub LogWriteLine(ByVal line As String)
        Using writer As New StreamWriter(config.LogFileName, True)
            writer.WriteLine("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "]" + vbTab + line)
        End Using
        Console.WriteLine(line)
    End Sub

    Private Sub LogWriteLine()
        LogWriteLine("")
    End Sub

    Private Function ReadSql(ByVal filename As String) As String
        Using reader As New StreamReader(filename)
            Return reader.ReadToEnd()
        End Using
    End Function

    Private Function CreateCommand(ByVal connection As SqlConnection, ByVal commandTimeout As Integer, ByVal sql As String, ByVal ParamArray params() As String) As SqlCommand
        Dim result As New SqlCommand(sql, connection)
        result.CommandTimeout = commandTimeout
        For Each paramName As String In params
            result.Parameters.AddWithValue(paramName, DBNull.Value)
        Next
        Return result
    End Function


    Private Function RateToFlexNum(ByVal rate As String) As Integer
        rate = ("" + rate).Trim().ToUpper()

        If rate.Length = 2 AndAlso Char.IsLetter(rate(0)) AndAlso Char.IsDigit(rate(1)) Then
            rate = Chr(Asc(Left(rate, 1)) + Asc("1") - Asc("A")) + Right(rate, 1)
        End If
        If rate.Length = 2 AndAlso Char.IsDigit(rate(0)) AndAlso Char.IsDigit(rate(1)) Then
            Return Integer.Parse(rate)
        End If

        ' Throw New Exception("Invalid Rate")
        Return 11
    End Function


    Function ExecuteInsertCommand(ByVal commandName As String, ByVal command As SqlCommand, ByVal reportFailure As Boolean) As DataRow
        Try
            Using da As New SqlDataAdapter(command)
                Dim table As New DataTable()
                da.Fill(table)

                If table.Rows.Count = 0 Then
                    Throw New Exception("Nothing Inserted")
                End If

                Return table.Rows(0)
            End Using
        Catch ex As Exception
            LogWriteLine("Error calling " + commandName + ": " + ex.Message)
            For Each param As SqlParameter In command.Parameters
                If TypeOf param.Value Is DBNull Then
                    LogWriteLine("SET " + param.ParameterName + " = NULL")
                ElseIf TypeOf param.Value Is DateTime Then
                    Dim dt As DateTime = CType(param.Value, DateTime)
                    LogWriteLine("SET " + param.ParameterName + "='" + dt.Day.ToString() + "/" + dt.Month.ToString() + "/" + dt.Year.ToString() + "'")
                ElseIf TypeOf param.Value Is Integer Then
                    Dim i As Integer = CType(param.Value, Integer)
                    LogWriteLine("SET " + param.ParameterName + "=" + i.ToString())
                Else
                    LogWriteLine("SET " + param.ParameterName + "='" + param.Value.ToString() + "'")
                End If
            Next
            LogWriteLine()
            Return Nothing
        End Try
    End Function


    Sub ProcessException(ByVal exceptionRow As DataRow)

        Dim country As String = exceptionRow("Country").ToString().Trim()
        Dim brand As String = ((exceptionRow("Brand").ToString().Trim() + " ").Substring(0, 1)).Trim()
        Dim cityList As String() = exceptionRow("City").ToString().Trim().Split(",")
        Dim vehicle = exceptionRow("Vehicle").ToString().Trim().ToUpper()
        If country = "" OrElse brand = "" OrElse cityList.Length = 0 OrElse vehicle = "" Then
            Return
        End If

        Dim auroraCodes As String() = config.GetAuroraCodesByFlexCountryVehicle(vehicle, country)
        If auroraCodes Is Nothing Then
            Return
        End If

        createBookingWeekExceptionCommand.Parameters("@FleBookStart").Value = exceptionRow("BookingStart")
        createBookingWeekExceptionCommand.Parameters("@FleBookEnd").Value = exceptionRow("BookingEnd")
        createBookingWeekExceptionCommand.Parameters("@FleTravelStart").Value = exceptionRow("TravelStart")
        createBookingWeekExceptionCommand.Parameters("@FleTravelEnd").Value = exceptionRow("TravelEnd")
        createBookingWeekExceptionCommand.Parameters("@FleLocCodeFrom").Value = DBNull.Value
        createBookingWeekExceptionCommand.Parameters("@FleLocCodeTo").Value = DBNull.Value
        createBookingWeekExceptionCommand.Parameters("@FleDelta").Value = exceptionRow("Change")
        createBookingWeekExceptionCommand.Parameters("@FbwBookStart").Value = exceptionRow("Book_Start")
        createBookingWeekExceptionCommand.Parameters("@FlpTravelYear").Value = exceptionRow("Start_On")
        createBookingWeekExceptionCommand.Parameters("@CtyCode").Value = country
        createBookingWeekExceptionCommand.Parameters("@BrdCode").Value = brand
        createBookingWeekExceptionCommand.Parameters("@PrdShortName").Value = DBNull.Value

        For Each city As String In cityList
            createBookingWeekExceptionCommand.Parameters("@FleLocCodeFrom").Value = city

            For Each auroraCode As String In auroraCodes
                createBookingWeekExceptionCommand.Parameters("@ClaCode").Value = DBNull.Value
                createBookingWeekExceptionCommand.Parameters("@PrdShortName").Value = auroraCode

                ExecuteInsertCommand("createBookingWeekExceptionCommand", createBookingWeekExceptionCommand, True)
            Next

        Next


    End Sub


    Sub ProcessRate(ByVal rateRow As DataRow)

        Dim country As String = rateRow("Country").ToString().Trim()
        Dim brand As String = ((rateRow("Brand").ToString().Trim() + " ").Substring(0, 1)).Trim()
        Dim vehicle = rateRow("VEHIC_TYPE").ToString().Trim().ToUpper()
        If country = "" OrElse brand = "" OrElse vehicle = "" Then
            Return
        End If

        Dim auroraCodes As String() = config.GetAuroraCodesByFlexCountryVehicle(vehicle, country)
        If auroraCodes Is Nothing Then
            Return
        End If

        createBookingWeekCommand.Parameters("@FbwBookStart").Value = rateRow("BOOK_START")
        createBookingWeekCommand.Parameters("@FbwTravelYearStart").Value = rateRow("START_ON")
        createBookingWeekCommand.Parameters("@FbwTravelYearEnd").Value = rateRow("END_ON")
        Dim bookingWeekDataRow As DataRow = ExecuteInsertCommand("createBookingWeekCommand", createBookingWeekCommand, True)

        If Not bookingWeekDataRow Is Nothing Then
            For Each auroraCode As String In auroraCodes
                createBookingWeekProductCommand.Parameters("@FbwId").Value = bookingWeekDataRow("FbwId")
                createBookingWeekProductCommand.Parameters("@FlpTravelyear").Value = rateRow("START_ON")
                createBookingWeekProductCommand.Parameters("@FlpCtyCode").Value = country
                createBookingWeekProductCommand.Parameters("@FlpBrdCode").Value = brand
                createBookingWeekProductCommand.Parameters("@PrdShortName").Value = auroraCode

                Dim bookingWeekProductDataRow = ExecuteInsertCommand("createBookingWeekProductCommand", createBookingWeekProductCommand, True)
                If Not bookingWeekProductDataRow Is Nothing Then
                    For wkNo As Integer = 1 To 52
                        Dim sRate As String = rateRow("W" + wkNo.ToString().PadLeft(2, "0")).ToString().Trim()
                        Dim flexNum As Integer = RateToFlexNum(sRate)
                        Dim changed As Boolean = rateRow("C" + wkNo.ToString().PadLeft(2, "0")).ToString().Trim().Length > 0

                        createBookingWeekRateCommand.Parameters("@FlpId").Value = bookingWeekProductDataRow("FlpId")
                        createBookingWeekRateCommand.Parameters("@FlpTravelYear").Value = rateRow("START_ON")
                        createBookingWeekRateCommand.Parameters("@FwnWkNo").Value = wkNo
                        createBookingWeekRateCommand.Parameters("@FlrFlexNum").Value = FlexNum
                        createBookingWeekRateCommand.Parameters("@FlrChanged").Value = changed
                        ExecuteInsertCommand("createBookingWeekRateCommand", createBookingWeekRateCommand, True)
                    Next
                End If
            Next
        End If

    End Sub


    Sub Main()
        Try
            LogClear()

            Using flexConnection = New SqlConnection(config.FlexConnectionString)
                If config.MigrateRates Then
                    LogWriteLine("Get Flex Rates")
                    Using command As SqlCommand = CreateCommand(flexConnection, config.FlexCommandTimeout, _
                     ReadSql("2. Flex - Load Rates.sql"))
                        Using da As New SqlDataAdapter(command)
                            da.Fill(flexRatesTable)
                        End Using
                    End Using
                End If

                If config.MigrateExceptions Then
                    LogWriteLine("Get Flex Exceptions")
                    Using command As SqlCommand = CreateCommand(flexConnection, config.FlexCommandTimeout, _
                     ReadSql("3. Flex - Load Exceptions.sql"))
                        Using da As New SqlDataAdapter(command)
                            da.Fill(flexExceptionsTable)
                        End Using
                    End Using
                End If
            End Using

            LogWriteLine("Connecting to Aurora")
            auroraConnection = New SqlConnection(config.AuroraConnectionString)
            auroraConnection.Open()

            If config.MigrateRates Then
                LogWriteLine("Create Aurora Flex Rates")

                createBookingWeekCommand = CreateCommand(auroraConnection, config.AuroraCommandTimeout, _
                 ReadSql("4. Aurora - Create FlexBookingWeek.sql"), _
                 "@FbwBookStart", "@FbwTravelYearStart", "@FbwTravelYearEnd")

                createBookingWeekProductCommand = CreateCommand(auroraConnection, config.AuroraCommandTimeout, _
                 ReadSql("5. Aurora - Create FlexBookingWeekProduct.sql"), _
                 "@FbwId", "@FlpTravelYear", "@FlpCtyCode", "@FlpBrdCode", "@PrdShortName")

                createBookingWeekRateCommand = CreateCommand(auroraConnection, config.AuroraCommandTimeout, _
                 ReadSql("6. Aurora - Create FlexBookingWeekRate.sql"), _
                 "@FlpTravelYear", "@FlpId", "@FwnWkNo", "@FlrFlexNum", "@FlrChanged")

                For Each rateRow As DataRow In flexRatesTable.Rows
                    LogWriteLine("  " + (flexRatesTable.Rows.IndexOf(rateRow) + 1).ToString() + " / " + flexRatesTable.Rows.Count.ToString())
                    ProcessRate(rateRow)
                Next
            End If

            If config.MigrateExceptions Then
                LogWriteLine("Create Aurora Flex Exceptions")

                createBookingWeekExceptionCommand = CreateCommand(auroraConnection, config.AuroraCommandTimeout, _
                     ReadSql("7. Aurora - Create FlexBookingWeekException.sql"), _
                     "@FleBookStart", "@FleBookEnd", "@FleTravelStart", "@FleTravelEnd", _
                     "@FleLocCodeFrom", "@FleLocCodeTo", "@FleDelta", _
                     "@FbwBookStart", "@FlpTravelYear", _
                     "@CtyCode", "@BrdCode", "@ClaCode", "@PrdShortName")

                For Each exceptionRow As DataRow In flexExceptionsTable.Rows
                    LogWriteLine("  " + (flexExceptionsTable.Rows.IndexOf(exceptionRow) + 1).ToString() + " / " + flexExceptionsTable.Rows.Count.ToString())
                    ProcessException(exceptionRow)
                Next
            End If

            LogWriteLine("Success")

        Catch ex As Exception
            LogWriteLine("Error: " + ex.Message)

        Finally
            If Not createBookingWeekRateCommand Is Nothing Then createBookingWeekRateCommand.Dispose()
            If Not createBookingWeekProductCommand Is Nothing Then createBookingWeekProductCommand.Dispose()
            If Not createBookingWeekCommand Is Nothing Then createBookingWeekCommand.Dispose()
            If Not createBookingWeekExceptionCommand Is Nothing Then createBookingWeekExceptionCommand.Dispose()
            If Not auroraConnection Is Nothing Then auroraConnection.Close()
        End Try
    End Sub

End Module
