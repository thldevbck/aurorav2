Imports Aurora.Common
Imports Aurora.Fleet.Data
Imports System.Xml

Public Class Fleet

    'Public Shared Function GetRepairsList(ByVal SearchFieldValue As String, ByVal FleetAssetId As String, ByVal UserCode As String) As DataSet

    '    Return DataRepository.GetRepairsList(SearchFieldValue, FleetAssetId, UserCode)

    'End Function


    'Public Shared Function GetFleetRepair(ByVal RepairId As String, ByVal FleetAssetId As String, ByVal UnitNo As String) As XmlDocument
    '    Return DataRepository.GetFleetRepair(RepairId, FleetAssetId, UnitNo)
    'End Function

    ''GetComboData
    'Public Shared Function GetOpsAndLogsReasons() As DataTable
    '    'GEN_getComboData 'OPLOGREASON'
    '    Dim dstReasons As DataSet
    '    dstReasons = Aurora.Common.Data.GetComboData("OPLOGREASON", "", "", "")

    '    If dstReasons.Tables.Count <= 0 Then
    '        Return New DataTable
    '    Else
    '        Return dstReasons.Tables(0)
    '    End If

    'End Function

    'Public Shared Function GetServicePoint(ByVal AssetId As String) As DataTable
    '    'GEN_getComboData 'OPLOGSERVICEPOINT','29211'
    '    Dim dstServicePoints As DataSet
    '    dstServicePoints = Aurora.Common.Data.GetComboData("OPLOGSERVICEPOINT", AssetId, "", "")

    '    If dstServicePoints.Tables.Count <= 0 Then
    '        Return New DataTable
    '    Else
    '        Return dstServicePoints.Tables(0)
    '    End If


    'End Function



    '''' <summary>
    '''' Purpose : To remove Repair & Maintenance record from AIMS databse using AIMS API.
    '''' Process-Logic :
    '''' Validations:
    '''' *PON should not be null
    '''' If PON=null
    ''''     return error message in XML format.
    '''' Begin transaction
    '''' Instantiate the AIMS Interface Object( AI.CMaster )
    '''' Execute F3_RMDelete(PON)
    '''' res = AIMS.F3_RMDelete(PON)
    '''' Destroy oAIMS object
    '''' If res LT 0   i.e. any errors
    ''''     rollback the transaction
    '''' Else
    ''''     commit the transaction
    '''' Get the message from AURORA using res as message code;
    '''' For ret=0 to get success message for delete operation.
    '''' Return the message.
    '''' </summary>
    '''' <param name="PurchaseOrderNo">Purchase Order Number</param>
    '''' <returns>An XML with the success/Error message</returns>
    '''' <remarks></remarks>
    'Public Shared Function DeleteRepairMaintenanceRecord(ByVal PurchaseOrderNo As String) As XmlDocument

    '    Dim oReturnDoc As New XmlDocument

    '    Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

    '        Try

    '            If PurchaseOrderNo.Trim().Equals(String.Empty) Then
    '                Throw (New Exception("App/Purchase Order Number should not be null"))
    '            End If

    '            'AIMS object
    '            Dim oAIMSObject As New AI.CMaster

    '            Dim nErrorNo As Integer

    '            nErrorNo = oAIMSObject.F3_RMDelete(PurchaseOrderNo)

    '            If nErrorNo < 0 Then
    '                Throw (New Exception("App/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(CStr(nErrorNo))))
    '                'GetAimsMessage
    '            End If

    '            oTransaction.CommitTransaction()

    '            oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN047") & "</Message></Root>")

    '        Catch ex As Exception

    '            oTransaction.RollbackTransaction()
    '            oTransaction.Dispose()

    '            Select Case ex.Message.Split("/"c)(0)
    '                Case "App"
    '                    oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
    '                Case Else
    '                    oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
    '            End Select



    '        End Try

    '        Return oReturnDoc
    '    End Using

    'End Function




    '''' <summary>
    '''' Purpose : Server-side posting of Repair and Maintenance Data to Aims.
    '''' Process/Logic :
    '''' Begin transaction
    '''' Parse the XML record xRMComplete
    '''' If Not valid XML then
    ''''     return error message in XML format.
    '''' Else
    '''' Extract PON, Odometer, dtENd
    '''' Destroy DOM object
    '''' Instantiate the AIMS Interface Object( AI.CMaster )
    '''' Execute F2_RMComplete()
    '''' res = AIMS.F2_RMComplete(PON, Odometer, dtENd)
    '''' Destroy oAIMS object
    '''' If res LT 0   i.e. any errors
    ''''     rollback the transaction
    '''' Else
    ''''     commit the transaction
    '''' Get the message from AURORA using the value of res as message code;
    '''' For ret=0 to get success message for delete operation.
    '''' Return the message in Xml Format.
    '''' </summary>
    '''' <param name="ProviderId">Provider Id</param>
    '''' <param name="PurchaseOrderNo">Purchase Order Number</param>
    '''' <param name="OdometerEnd">Ending Odometer Reading</param>
    '''' <param name="EndDate">End Date</param>
    '''' <returns>XmlDocument with the Error/Success Status</returns>
    '''' <remarks></remarks>
    'Public Shared Function CompleteRepairMaintenanceRecord(ByVal ProviderId As String, ByVal PurchaseOrderNo As String, ByVal OdometerEnd As Integer, ByVal EndDate As Date) As XmlDocument

    '    Dim oReturnDoc As New XmlDocument

    '    Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
    '        Try

    '            If Not ValidateProvider(ProviderId) Then
    '                'is not a valid provider
    '                Throw (New Exception("App/'" & ProviderId & "' is not a valid Provider"))
    '            End If

    '            Dim oAIMSObject As New AI.CMaster
    '            Dim nErrorNo As Integer
    '            nErrorNo = oAIMSObject.F2_RMComplete(PurchaseOrderNo, OdometerEnd, EndDate)

    '            If nErrorNo < 0 Then
    '                Throw (New Exception("App/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(CStr(nErrorNo))))
    '                'GetAimsMessage
    '            End If

    '            oTransaction.CommitTransaction()
    '            oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
    '        Catch ex As Exception

    '            oTransaction.RollbackTransaction()
    '            oTransaction.Dispose()

    '            Select Case ex.Message.Split("/"c)(0)
    '                Case "App"
    '                    oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
    '                Case Else
    '                    oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
    '            End Select
    '            'error handling

    '        End Try

    '        Return oReturnDoc

    '    End Using

    'End Function


    'Public Shared Function CreateUpdateRepairMaintenanceRecord(ByVal ProviderId As String, _
    '                                                           ByVal UnitNo As String, _
    '                                                           ByVal FromDate As Date, _
    '                                                           ByVal ToDate As Date, _
    '                                                           ByVal Branch As String, _
    '                                                           ByVal OdometerStart As Integer, _
    '                                                           ByVal Reason1 As String, _
    '                                                           ByVal Reason2 As String, _
    '                                                           ByVal Reason3 As String, _
    '                                                           ByVal OtherReasonDescription As String, _
    '                                                           ByVal ServicePoint As Integer, _
    '                                                           ByVal RepairerId As Integer, _
    '                                                           ByVal Reference As String, _
    '                                                           ByVal PurchaseOrderNo As String) As XmlDocument


    '    Dim oReturnDoc As New XmlDocument

    '    Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
    '        Try



    '            If Not ValidateProvider(ProviderId) Then
    '                'is not a valid provider
    '                Throw (New Exception("App/'" & ProviderId & "' is not a valid Provider"))
    '            End If

    '            Dim oAIMSObj As New AI.CMaster
    '            Dim nErrorNo As Integer

    '            nErrorNo = oAIMSObj.F1_RMCreateUpdate(UnitNo, FromDate, ToDate, Branch, OdometerStart, Reason1, Reason2, Reason3, OtherReasonDescription, ServicePoint, RepairerId, Reference, PurchaseOrderNo)

    '            If nErrorNo <> 0 Then
    '                Throw (New Exception("App/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(CStr(nErrorNo))))
    '                'GetAimsMessage
    '            End If

    '            oTransaction.CommitTransaction()
    '            If PurchaseOrderNo = "" Then
    '                'create
    '                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</Message></Root>")
    '            Else
    '                'update
    '                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
    '            End If

    '        Catch ex As Exception

    '            oTransaction.RollbackTransaction()
    '            oTransaction.Dispose()
    '            Select Case ex.Message.Split("/"c)(0)
    '                Case "App"
    '                    oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
    '                Case Else
    '                    oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
    '            End Select

    '        End Try
    '    End Using

    '    Return oReturnDoc



    '    'sErrNo = oAIMS.F1_RMCreateUpdate(UnitNo, _
    '    '                                 CDate(FrDt), _
    '    '                                 CDate(ToDt), _
    '    '                                 UCase(Branch), _
    '    '                                 CLng(OdoSt), _
    '    '                                 Reason, _
    '    '                                 Reason1, _
    '    '                                 Reason2, _
    '    '                                 OthReasonDesc, _
    '    '                                 CLng(ServPt), _
    '    '                                 CLng(RepairerID), _
    '    '                                 Ref, _
    '    '                                 PONo)



    'End Function

    'Public Shared Function ValidateProvider(ByVal ProviderId As String) As Boolean
    '    'Dim bIsValid As Boolean = False

    '    Dim oXml As XmlDocument
    '    oXml = DataRepository.GetProviderInfo(ProviderId)
    '    If Trim(oXml.DocumentElement.InnerText).Equals(String.Empty) Then
    '        Return False
    '    Else
    '        Return True
    '    End If

    'End Function


    'Public Shared Function GetRMReportData(ByVal DateEntered As String, ByVal ComparisonValue As String, ByVal LocationCode As String, ByVal UserCode As String) As DataTable
    '    'GetPopUpData 
    '    'exec GEN_GetPopUpData @case = 'tmpRMreport', @param1 = '', @param2 = 'LE', @param3 = '', @param4 = '', @param5 = '', @sUsrCode = 'sp7'
    '    'exec GEN_GetPopUpData @case = 'tmpRMreport', @param1 = '12/12/2007', @param2 = 'LE', @param3 = 'AKL', @param4 = '', @param5 = '', @sUsrCode = 'sp7'
    '    Dim dstRMReport As DataSet
    '    dstRMReport = Aurora.Common.Data.GetPopUpData("tmpRMreport", DateEntered, ComparisonValue, LocationCode, "", "", UserCode)
    '    If dstRMReport.Tables.Count <= 0 Then
    '        Return New DataTable
    '    Else
    '        Return dstRMReport.Tables(0)
    '    End If
    'End Function

    'Public Shared Function GetLocations(ByVal CountryCode As String, ByVal UserCode As String) As DataTable
    '    'exec GEN_GetPopUpData  @case = 'LOCATIONFORCOUNTRY', @param1 = 'NZ', @sUsrCode = 'sp7'
    '    Dim dstLocations As DataSet
    '    dstLocations = Aurora.Common.Data.GetPopUpData("LOCATIONFORCOUNTRY", CountryCode, "", "", "", "", UserCode)
    '    If dstLocations.Tables.Count <= 0 Then
    '        Return New DataTable
    '    Else
    '        Return dstLocations.Tables(0)
    '    End If
    'End Function


End Class
