Imports Aurora.Common
Imports Aurora.Common.Data
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common


Public Class ManageBPDList

    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                       ByVal sErrNumber As String, _
                                       ByVal sErrType As String, _
                                       ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Public Shared Function manageBookedPrdList( _
                                           ByVal sRntId As String, _
                                           ByVal sXMLData As String, _
                                           ByVal bResult As Boolean, _
                                           ByVal UserCode As String) As String


        Dim oXMLDom As Xml.XmlDocument = New Xml.XmlDocument
        Dim sXmlString As String = String.Empty
        Dim sErrorString As String = String.Empty

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction()
            Try
                oXMLDom.LoadXml(sXMLData)

                '@pRntID			VARCHAR(64)		=	'',
                '@XMLData		TEXT			=	DEFAULT ,
                '@bResult		BIT ,
                '@UsrCode		VARCHAR	 (64)	=

                sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_manageBookedProductList", sRntId, oXMLDom.OuterXml, bResult, UserCode).ToString()

                ' Added by Shoel on 30.10.2008
                If Not sXmlString.ToUpper().Contains("OVABCDEF*") Then
                    Try
                        Dim oXmlError As XmlDocument = New XmlDocument
                        oXmlError.LoadXml(sXmlString.Replace("<Messages></Messages>", ""))
                        If oXmlError.DocumentElement.SelectSingleNode("error").InnerText <> String.Empty Then
                            ' some error
                            Throw New Exception("APP/" & oXmlError.DocumentElement.SelectSingleNode("error").InnerText)
                        End If
                    Catch ex As Exception
                        If ex.Message.IndexOf("APP/") > -1 Then
                            Throw ex
                        End If
                    End Try
                    Booking.Data.DataRepository.UpdateRentalIntegrityNumber(sRntId)
                End If
                ' Added by Shoel on 30.10.2008

                oTransaction.CommitTransaction()
                oTransaction.Dispose()

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                'sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", "")
                sErrorString = "<Error><ErrStatus>True</ErrStatus><ErrDescription>" & ex.Message.Replace("APP/", "") & "</ErrDescription></Error>"
                Return String.Concat("<Root>", sErrorString, "<Data></Data></Root>")
                oXMLDom = Nothing

            End Try
        End Using

        Return "<Data>" & sXmlString & "</Data>"

    End Function

    Public Shared Function getBookedProductsDetail(ByVal sRntId As String, _
                                           ByVal sXMLData As String, _
                                           ByVal sUsrId As String, _
                                           ByVal sPrgmName As String) As String


        Dim oXMLDom As New Xml.XmlDocument

        Dim sError As String = String.Empty
        Dim sErrorNo As String = String.Empty
        Dim oRetXml As New Xml.XmlDocument
        Dim NoOfBPD As Integer
        Dim I As Integer
        Dim sQuery As String = String.Empty
        Dim BpdId As String = String.Empty
        Dim SapId As String = String.Empty
        Dim PrrIdsList As String = String.Empty

        Dim RetData As String = String.Empty
        Dim sfunctionName As String = String.Empty

        Dim NoOfElement As Integer
        Dim PreviousEle As Integer
        Dim Arr As String() = Nothing
        Dim PtdArr As String() = Nothing
        Dim Temp As Integer
        Dim oNode As XmlNode
        Dim Flag As Integer
        Dim PtdNoOfElement As Integer
        Dim PtdPreviousEle As Integer
        Dim PtdNodeNumber As Integer
        Dim PtdIdsList As String
        Dim PtdUniqueHeader As String = String.Empty
        Dim NoOfPtd As Integer
        Dim Header As String = String.Empty
        Dim PrrUniqueHeader As String = String.Empty

        Dim sCkoDt As String
        Dim sCkiDt As String
        Dim sCkoLocCode As String
        Dim sCkiLocCode As String
        Dim sErrorString As String = String.Empty

        Dim sresult As String
        Dim NodeNumber As String = String.Empty
        Dim xmlData As String

        Dim j As Integer

        ''RES_getDataForBookedProductsDetail
        ''RES_getDataForBookedProductDetail

        'sresult = Aurora.Common.Data.ExecuteScalarSP("RES_getDataForBookedProductsDetail", sRntId, sXMLData, sUsrId, String.Empty)
        sresult = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getDataForBookedProductDetail", sRntId, sXMLData, sUsrId, String.Empty).OuterXml.ToString
        sresult = sresult.Replace("<data>", "<Data>")
        sresult = sresult.Replace("</data>", "</Data>")
        oRetXml.LoadXml(sresult)

        Try


            NoOfBPD = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes.Count
            NoOfElement = 0 : PreviousEle = 0


            For I = 0 To NoOfBPD - 1
                oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("SeqId").InnerText = I
                PrrIdsList = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").InnerText
                PtdIdsList = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").InnerText

                If Trim(PrrIdsList) <> "" Then
                    Arr = PrrIdsList.Split(",")
                    NoOfElement = UBound(Arr)
                    If NoOfElement = 0 Then NoOfElement = 1
                    If NoOfElement > PreviousEle Then
                        Temp = NoOfElement
                        NoOfElement = PreviousEle
                        PreviousEle = Temp
                        NodeNumber = I
                    End If
                End If
                If Trim(PtdIdsList) <> "" Then
                    PtdArr = PtdIdsList.Split(",")
                    PtdNoOfElement = UBound(PtdArr)
                    If PtdNoOfElement = 0 Then PtdNoOfElement = 1
                    If PtdNoOfElement > PtdPreviousEle Then
                        Temp = PtdNoOfElement
                        PtdNoOfElement = PtdPreviousEle
                        PtdPreviousEle = Temp
                        PtdNodeNumber = I
                    End If
                End If
            Next

            Flag = 5

            For I = 0 To NoOfBPD - 1
                BpdId = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("BpdId").InnerText
                SapId = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("SapId").InnerText
                PrrIdsList = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").InnerText
                PtdIdsList = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").InnerText

                sCkoDt = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkoDt").InnerText
                If Trim(sCkoDt) = "" Then
                    sCkoDt = String.Empty
                Else
                    sCkoDt = String.Concat(oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkoDt").InnerText, " ", oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkoTm").InnerText)
                End If

                sCkiDt = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkiDt").InnerText
                If Trim(sCkiDt) = "" Then
                    sCkiDt = String.Empty
                Else
                    sCkiDt = String.Concat(oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkiDt").InnerText, " ", oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkiTm").InnerText)
                End If

                sCkoLocCode = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkoLocCode").InnerText
                sCkiLocCode = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("CkiLocCode").InnerText

                If Trim(PrrIdsList) <> "" Then
                    '@sBpdId			VARCHAR	 (64) ,
                    '@sRntId			VARCHAR	(64) ,
                    '@sSapId			VARCHAR	 (64) ,
                    '@sCkoDt 			VARCHAR	 (30)	= NULL , 
                    '@sCkiDt 			VARCHAR	 (30) 	= NULL,
                    '@sCkoLocCode 	VARCHAR	 (64) ,
                    '@sCkiLocCode	VARCHAR	 (64) , 
                    '@CSL	 			VARCHAR	 (2000) ,
                    '@sUsrId 			VARCHAR	 (64) ,
                    '@sPrgmName 		VARCHAR	 (256)
                    sQuery = "RES_getRate '" & BpdId & "','" & sRntId & "','" & SapId & "','" & sCkoDt & "','" & sCkiDt & "','" & sCkoLocCode & "','" & sCkiLocCode & "','" & PrrIdsList & "','" & sUsrId & "','" & sPrgmName & "'"
                    xmlData = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getRate", _
                                                               Trim(BpdId), _
                                                               Trim(sRntId), _
                                                               Trim(SapId), _
                                                               Trim(sCkoDt), _
                                                               Trim(sCkiDt), _
                                                               Trim(sCkoLocCode), _
                                                               Trim(sCkiLocCode), _
                                                               Trim(PrrIdsList), Trim(sUsrId), Trim(sPrgmName)).DocumentElement.InnerXml

                    oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").InnerText = xmlData
                    RetData = (Replace(oRetXml.OuterXml, "&gt;", ">"))
                    RetData = Replace(RetData, "&lt;", "<")
                    Call oRetXml.LoadXml(RetData)
                Else
                    oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").InnerText = "*"
                End If

                If Trim(PtdIdsList) <> "" Then

                    xmlData = Aurora.Common.Data.ExecuteScalarSP("RES_getPersonTypeDiscount", PtdIdsList)

                    oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").InnerText = xmlData
                    RetData = (Replace(oRetXml.OuterXml, "&gt;", ">"))
                    RetData = Replace(RetData, "&lt;", "<")
                    oRetXml.LoadXml(RetData)
                Else
                    oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").InnerText = "*"
                End If

            Next

            PrrUniqueHeader = ""
            For I = 0 To NoOfBPD - 1
                If oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").InnerText <> "*" Then
                    NoOfPtd = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").ChildNodes.Count
                    For j = 0 To NoOfPtd - 1
                        Header = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PrrIdsList").ChildNodes(j).SelectSingleNode("Code").InnerText
                        If InStr(PrrUniqueHeader, Header) = 0 Then
                            PrrUniqueHeader = PrrUniqueHeader & "<Head>" & Header & "</Head>"
                        End If
                    Next j
                End If
            Next I

            PtdUniqueHeader = ""
            For I = 0 To NoOfBPD - 1
                If oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").InnerText <> "*" Then
                    NoOfPtd = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").ChildNodes.Count
                    For j = 0 To NoOfPtd - 1
                        Header = oRetXml.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(I).SelectSingleNode("PtdIdsList").ChildNodes(j).SelectSingleNode("Header").InnerText
                        If InStr(PtdUniqueHeader, Header) = 0 Then
                            PtdUniqueHeader = PtdUniqueHeader & "<Head>" & Header & "</Head>"
                        End If
                    Next j
                End If
            Next I

            Flag = 6

            oNode = oRetXml.CreateElement("Max")
            If PreviousEle <> 0 Then
                oNode.InnerText = CInt(NodeNumber) + 1
            Else
                oNode.InnerText = 0
            End If
            oRetXml.DocumentElement.AppendChild(oNode)

            oNode = oRetXml.CreateElement("PtdMax")
            If PtdPreviousEle <> 0 Then
                oNode.InnerText = PtdNodeNumber + 1
            Else
                oNode.InnerText = 0
            End If
            Call oRetXml.DocumentElement.AppendChild(oNode)

            oNode = oRetXml.CreateElement("PrrHeader")

            If PrrUniqueHeader <> "" Then
                oNode.InnerText = PrrUniqueHeader
            End If
            Call oRetXml.DocumentElement.AppendChild(oNode)
            RetData = (Replace(oRetXml.OuterXml, "&gt;", ">"))
            RetData = Replace(RetData, "&lt;", "<")
            Call oRetXml.LoadXml(RetData)

            oNode = oRetXml.CreateElement("PtdHeader")
            If PtdUniqueHeader <> "" Then
                oNode.InnerText = PtdUniqueHeader
            End If
            Call oRetXml.DocumentElement.AppendChild(oNode)
            RetData = (Replace(oRetXml.OuterXml, "&gt;", ">"))
            RetData = Replace(RetData, "&lt;", "<")
            Call oRetXml.LoadXml(RetData)


            I = oRetXml.DocumentElement.SelectSingleNode("PtdHeader").ChildNodes.Count
            oNode = oRetXml.CreateElement("NoOfPtdHd")
            oNode.InnerText = I
            Call oRetXml.DocumentElement.AppendChild(oNode)

            getBookedProductsDetail = oRetXml.OuterXml
            Flag = 8


        Catch ex As Exception
            getBookedProductsDetail = "<Root>" & GetErrorTextFromResource(True, sErrorNo, "System", sError) & "</Root>"
        Finally

            oXMLDom = Nothing
        End Try

        Return getBookedProductsDetail
    End Function
End Class
