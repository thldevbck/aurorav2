Imports Aurora.Common
Imports Aurora.Common.Data
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports Aurora.Reservations.Services

Public Class BookingRentalMod


    Private Shared mOutputparameter As String
    Public Shared Property OutputParameter() As String
        Get
            Return mOutputparameter
        End Get
        Set(ByVal value As String)
            mOutputparameter = value
        End Set
    End Property

    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                            ByVal sErrNumber As String, _
                                            ByVal sErrType As String, _
                                            ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
        'Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("sp_get_ErrorString", params)
        'If Not String.IsNullOrEmpty(OutputParameter) Then
        '    Return xmlstring
        'Else
        '    Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", xmlstring)
        'End If
        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Public Shared Function ManageSummaryOfChoosenVehicle( _
             ByVal StrXML As String, _
             ByVal Param1 As String, _
             ByVal Param2 As String) As String


        Dim ObjDVASS As New AuroraDvass
        Dim oSelVehXmlDom As New XmlDocument
        
        Dim sXmlString As String = String.Empty
        Dim sErrorString As String = String.Empty

        Dim retValue As String = String.Empty
        
        Try

            Logging.LogInformation("ManageSummaryOfChoosenVehicle", "Start")
            Logging.LogInformation("ManageSummaryOfChoosenVehicle", "Calling AddRentalProc()")


            retValue = Aurora.Reservations.Services.AuroraDvass.AddRentalProc(StrXML, Param1, Param2)
            Logging.LogInformation("ManageSummaryOfChoosenVehicle", "AddRentalProc() returned = " & retValue)

            oSelVehXmlDom.LoadXml(retValue)
            If oSelVehXmlDom.SelectSingleNode("//Root/Error/ErrStatus").InnerText = True Then
                Logging.LogError("ManageSummaryOfChoosenVehicle", "Error From AddRentalProc() = " & oSelVehXmlDom.SelectSingleNode("//Root/Error/ErrDescription").InnerText)
            Else
                Logging.LogInformation("ManageSummaryOfChoosenVehicle", "Success From .AddRentalProc")
                Logging.LogInformation("ManageSummaryOfChoosenVehicle", "End")
            End If

        Catch xml As XmlException
            Logging.LogError("ManageSummaryOfChoosenVehicle", xml.Message)
            sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", retValue)
            Return String.Concat("<Root>", sErrorString, "<Data></Data></Root>")



        Catch ex As Exception
            sXmlString = getMessageFromDB("GEN023")
            Return String.Concat("<Root>", sXmlString, "<Data></Data></Root>")
        End Try

        Return retValue
    End Function
End Class
