﻿Imports Aurora.Common
Imports Aurora.Common.Logging
Imports Aurora.AIMS.Data
Imports System.Net.Mail
Imports System.Text
Imports System.Configuration
Imports Newtonsoft
Imports Aurora.Common.Data

Public Class FleetSaleDisposalPlan
    'Public Shared Function GetAllAssetNum() As DataSet
    '    Return DataRepository.GetAllAssetNum
    'End Function

    Public Shared Function GetVehicleSaleRecords(iCountryId As Integer, iFleetModelId As String, w_unit_from As Integer?, w_unit_to As Integer?, sRegoNum As String, sUsrCode As String) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.GetVehicleSaleRecords(iCountryId, iFleetModelId, w_unit_from, w_unit_to, sRegoNum, sUsrCode),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetVehicleSaleRecords()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function GetDropdownForSalePlan(iCountryId As Integer) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.GetDropdownForSalePlan(iCountryId),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetDropdownForSalePlan()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function UpdateSaleRecord(iSaleId As Integer?, dOffFleetDate As DateTime?, sSaleLocation As String, iSaleDealerId As Integer, sUserCode As String) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.UpdateSaleRecord(iSaleId, dOffFleetDate, sSaleLocation, iSaleDealerId, sUserCode),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("UpdateSaleRecord()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function CreateSalePlan(iCountryId As Integer, iFromUnitNum As Integer, iToUnitNum As Integer, iFleetModelID As Integer, dStartDate As DateTime?, dEndDate As DateTime?, iplanquantity As Integer, sUserCode As String) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.CreateSalePlan(iCountryId, iFromUnitNum, iToUnitNum, iFleetModelID, dStartDate, dEndDate, iplanquantity, sUserCode),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("CreateSalePlan()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function CreateSalPlanLine(iCountryId As Integer, iUnitNumber As Integer, iPlanId As Integer, sOffFleetLication As String, dOffFleetDate As DateTime?, p_dealer As Integer, sUserCode As String) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.CreateSalPlanLine(iCountryId, iUnitNumber, iPlanId, sOffFleetLication, dOffFleetDate, p_dealer, sUserCode),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("CreateSalPlanLine()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function DeleteSalePlanRecord(iFleetAssetId As Integer, iSaleId As Integer, sUserCode As String) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.DeleteSalePlanRecord(iFleetAssetId, iSaleId, sUserCode),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("DeleteSalePlanRecord()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    ''rev:mia 14nov2014 start - added RRP --------
    Public Shared Function UpdateSalesPlan(iCountryId As Integer,
                                            p_fleet_asset_id As Integer,
                                            dDateSold As DateTime?,
                                            dPickupDate As DateTime?,
                                            sCustomerType As String,
                                            iSalePrice As Integer,
                                            iSalePriceIncTax As Integer,
                                            iSalesTaxPercent As Integer,
                                            iDealerId As Integer,
                                            sCustomerName As String,
                                            sSettleMethod As String,
                                            dSettleBankDate As DateTime?,
                                            sSettleLocation As String,
                                            sSaleComment As String,
                                            bRelease As Integer,
                                            iPreSaleActualLocId As Integer,
                                            iCurrentLocationId As Integer,
                                            dModifiedDate As DateTime?,
                                            sModifiedBy As String,
                                            iSaleId As Integer, _
                                            Optional RRP As String = "") As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.UpdateSalesPlan(iCountryId,
                                                p_fleet_asset_id,
                                                dDateSold,
                                                dPickupDate,
                                                sCustomerType,
                                                iSalePrice,
                                                iSalePriceIncTax,
                                                iSalesTaxPercent,
                                                iDealerId,
                                                sCustomerName,
                                                sSettleMethod,
                                                dSettleBankDate,
                                                sSettleLocation,
                                                sSaleComment,
                                                bRelease,
                                                iPreSaleActualLocId,
                                                iCurrentLocationId,
                                                dModifiedDate,
                                                sModifiedBy,
                                                iSaleId,
                                                RRP),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("ReleaseSalesPlan()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function SaveProcessSteps(iProcessStepsId As Integer?,
                                            iFleetAssetId As Integer,
                                            iProcessId As Integer,
                                            sAddUserCode As String,
                                            bCompleted As Integer) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.SaveProcessSteps(iProcessStepsId,
                                                        iFleetAssetId,
                                                        iProcessId,
                                                        sAddUserCode,
                                                        bCompleted),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("ReleaseSalesPlan()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result

    End Function
    Public Shared Function GetProcessedSteps(iFleetAssetId As Integer) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.GetProcessedSteps(iFleetAssetId),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetProcessedSteps()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

#Region "Nyt 3rd Nov 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-314 "
    Shared Function SaveProcessStepsDate(iProcessStepsId As Integer, iFleetAssetId As Integer, iProcessId As Integer, sAddUserCode As String, dProcessedDate As Date) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.SaveProcessStepsDate(iProcessStepsId,
                                                        iFleetAssetId,
                                                        iProcessId,
                                                        sAddUserCode,
                                                        dProcessedDate),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("ReleaseSalesPlan()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function
#End Region
End Class
