﻿Imports Aurora.Common
Imports Aurora.Common.Logging
Imports Aurora.AIMS.Data
Imports System.Net.Mail
Imports System.Text
Imports System.Configuration
Imports Newtonsoft
Imports Aurora.Common.Data

Public Class FleetActivity
    Public Shared Function GetFleetActivityHistory(iCountryId As Integer?, iUnitNumber As Integer?,
                                            sRegoNum As String) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.GetFleetActivityHistory(iCountryId, iUnitNumber, sRegoNum),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetFleetActivityHistory()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function GetDropDownForFleetActivity(iCountryId As Integer) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.GetDropDownForFleetActivity(iCountryId),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetDropDownForFleetActivity()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function CreateNewActivity(iFleetAssetId As Integer, sActivityType As String, sExternalRef As String, dStartDate As DateTime, _
                                             dEndDate As DateTime, iStartLocation As Integer, iEndLocation As Integer, iStartOddometer As Integer, _
                                             iEndOddometer As Integer, bCompleted As Boolean, sModifiedBy As String, sdescription As String, _
                                             bForce As Boolean) As String
        Dim result(0 To 1) As String

        Dim sReturnError As String = ""
        Dim activity_id As String = ""

        Dim returnResult As String

        Try
            DataRepository.CreateNewActivity(iFleetAssetId, sActivityType, sExternalRef,
                                                             dStartDate, dEndDate, iStartLocation, iEndLocation, iStartOddometer, iEndOddometer,
                                                             bCompleted, sModifiedBy, sdescription, bForce, sReturnError, activity_id)
            result(0) = sReturnError
            result(1) = activity_id

            returnResult = Json.JsonConvert.SerializeObject(result, Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("CreateNewActivity()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            returnResult = String.Concat("ERROR:", ex.Message)
        End Try

        Return returnResult
    End Function

    Public Shared Function UpdateActivity(iFleetActivityId As Integer, iFleetAssetId As Integer, sActivityType As String, sExternalRef As String, dStartDate As DateTime, dEndDate As DateTime, iStartLocation As Integer, iEndLocation As Integer, iStartOddometer As Integer, iEndOddometer As Integer, bCompleted As Boolean, sModifiedBy As String, sdescription As String, bForce As Boolean) As String
        Dim result As String = String.Empty

        Try
            result = Json.JsonConvert.SerializeObject(
                DataRepository.UpdateActivity(iFleetActivityId, iFleetAssetId, sActivityType, sExternalRef, dStartDate, dEndDate, iStartLocation, iEndLocation, iStartOddometer, iEndOddometer, bCompleted, sModifiedBy, sdescription, bForce),
                Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("UpdateActivity()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function
End Class
