﻿''rev:mia Jan.17,2014 - First Version
''                    - refer to the Load Vehicle Plan Document


Imports Aurora.Common
Imports Aurora.Common.Logging
Imports Aurora.AIMS.Data
Imports System.Net.Mail
Imports System.Text
Imports System.Configuration
Imports Newtonsoft
Imports Aurora.Common.Data

Public Class LoadVehicle
    Public Shared Function GetDropDownDataForLoadVehiclePlan(ByVal ParamArray params() As Object) As DataSet
        Return DataRepository.GetDropDownDataForLoadVehiclePlan(params)
    End Function

    Public Shared Function CreateVehiclePlan(ByVal ParamArray params() As Object) As Object
        Return DataRepository.CreateVehiclePlan(params)
    End Function
End Class
