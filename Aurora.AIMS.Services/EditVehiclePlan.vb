﻿''rev:mia Jan.17,2014 - First Version
''                    - refer to the Edit Vehicle Plan Document
''today in history: April 11, 2014 - Royal Couple visited Auckland (prince william,kate,george)

Imports Aurora.Common
Imports Aurora.Common.Logging
Imports Aurora.AIMS.Data
Imports System.Net.Mail
Imports System.Text
Imports System.Configuration
Imports Newtonsoft
Imports Aurora.Common.Data

Public Class EditVehiclePlan
    Public Shared Function GetUnitNumber(ByVal ParamArray params() As Object) As DataSet
        Return DataRepository.GetUnitNumber(params)
    End Function

    Public Shared Function GetScheduledChangeFleetModelRules(ByVal ParamArray params() As Object) As DataSet
        Return DataRepository.GetScheduledChangeFleetModelRules(params)
    End Function

    Public Shared Function GetFleetModelCodes(ByVal ParamArray params() As Object) As DataSet
        Return DataRepository.GetFleetModelCodes(params)
    End Function



    Public Shared Function UpdateFleetDetailsV2(iFleetAssetIds As List(Of String), _
                                                iRUCPaidToKms As Integer, _
                                                iManufacturerId As Integer, _
                                                iNextServiceId As Integer, _
                                                iManufacturerModelId As Integer, _
                                                iIdCompliancePlateYear As Integer, _
                                                iIdCompliancePlateMonth As Integer, _
                                                dPurProposedDelivDate As DateTime, _
                                                dFirstOnFleetDate As DateTime, _
                                                dPurReceiptDate As DateTime, _
                                                dRegFirstDate As DateTime, _
                                                dRCCofEndDate As DateTime, _
                                                dRegoEndDate As DateTime, _
                                                dWarrantyExpDate As DateTime, _
                                                dSelfContainExpDate As DateTime, _
                                                dTelematicsInstallDate As DateTime, _
                                                sComment As String, _
                                                sTelematicsDevice As String, _
                                                sTelematicsTablet As String, _
                                                sPurLocationOnFleet As String, _
                                                sIdEngineNumber As String, _
                                                sIdChassisNumber As String, _
                                                sKeyNrIgnition As String, _
                                                sRegistrationNumber As String, _
                                                sETag As String, _
                                                sEPurb As String, _
                                                sUserCode As String, _
                                                sMdFuelType As String, _
                                                sMdTransmission As String, _
                                                sTelematicsInstaller As String, _
                                                dTelematicsTabletInstallDate As DateTime, _
                                                sTelematicsTabletInstaller As String, _
                                                Optional iFleetModelID As Integer = 0 _
                                                ) As String

        Dim result As String = String.Empty
        Try
            Using transaction As New DatabaseTransaction()
                Try
                    For Each iFleetAssetId As String In iFleetAssetIds
                        result = DataRepository.UpdateFleetDetailsV2(CInt(iFleetAssetId),
                                                                         iRUCPaidToKms, _
                                                                         iManufacturerId, _
                                                                         iNextServiceId, _
                                                                         iManufacturerModelId, _
                                                                         iIdCompliancePlateYear, _
                                                                         iIdCompliancePlateMonth, _
                                                                         dPurProposedDelivDate, _
                                                                         dFirstOnFleetDate, _
                                                                         dPurReceiptDate, _
                                                                         dRegFirstDate, _
                                                                         dRCCofEndDate, _
                                                                         dRegoEndDate, _
                                                                         dWarrantyExpDate, _
                                                                         dSelfContainExpDate, _
                                                                         dTelematicsInstallDate, _
                                                                         sComment, _
                                                                         sTelematicsDevice, _
                                                                         sTelematicsTablet, _
                                                                         sPurLocationOnFleet, _
                                                                         sIdEngineNumber, _
                                                                         sIdChassisNumber, _
                                                                         sKeyNrIgnition, _
                                                                         sRegistrationNumber, _
                                                                         sETag, _
                                                                         sEPurb, _
                                                                         sUserCode, _
                                                                         sMdFuelType, _
                                                                         sMdTransmission, _
                                                                         sTelematicsInstaller, dTelematicsTabletInstallDate, sTelematicsTabletInstaller)

                        ' Added by Nimesh for updating fleet model id Ref: Aurora-41
                        If iFleetModelID <> 0 Then
                            DataRepository.UpdateFleetModelID(iFleetAssetId, iFleetModelID, sUserCode)

                        End If
                        ' End Added by Nimesh for updating fleet model id Ref: Aurora-41

                        If (result.IndexOf("ERRORS") <> -1) Then
                            Throw New Exception(result)
                        End If
                        'transaction.CommitTransaction()
                    Next
                    'Transaction.CommitTransaction was moved out of fornext loop and added at the end of loop by Nimesh on 5th Apr 2015
                    transaction.CommitTransaction()
                Catch ex As Exception
                    transaction.RollbackTransaction()
                    Throw New Exception(ex.Message)
                End Try

            End Using


        Catch ex As Exception
            Logging.LogError("UpdateFleetDetailsV2".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR: ", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function UpdateFleetDetails(iFleetAssetIds As List(Of String),
                                                dPurProposedDelivDate As DateTime?,
                                                dFirstOnFleetDate As DateTime?,
                                                sPurLocationOnFleet As String,
                                                dPurReceiptDate As DateTime?,
                                                sIdEngineNumber As String,
                                                sIdChassisNumber As String,
                                                sKeyNrIgnition As String,
                                                sIdCompliancePlateYear As Integer?,
                                                sIdCompliancePlateMonth As Integer?,
                                                sRegistrationNumber As String,
                                                dRegFirstDate As DateTime?,
                                                dRCCofEndDate As DateTime?,
                                                sETag As String,
                                                sEPurb As String,
                                                sUserCode As String, _
                                                Optional rucPaidTo As Integer = -1, _
                                                Optional manufacturerId As Integer = -1, _
                                                Optional manufacturerModelId As Integer = -1, _
                                                Optional mdFuelType As String = "", _
                                                Optional mdTransmission As String = "", _
                                                Optional NextServiceId As Integer = -1, _
                                                Optional comments As String = "", _
                                                Optional regExpirydate As String = "", _
                                                Optional WarrantyExpDate As String = "", _
                                                Optional SelfContainExpDate As String = "", _
                                                Optional TelematicsDevice As String = "", _
                                                Optional TelematicsTablet As String = "", _
                                                Optional TelematicsInstallDate As String = "", _
                                                Optional TelematicsInstaller As String = "" _
                                                ) As String

        ''@dTelematicsInstallDate	DATE	= NULL,
        ''@sTelematicsInstaller	
        Dim result As String = String.Empty
        If (String.IsNullOrEmpty(regExpirydate)) Then
            regExpirydate = Nothing
        End If
        Try
            Using transaction As New DatabaseTransaction()
                Try
                    For Each iFleetAssetId As String In iFleetAssetIds
                        result = DataRepository.UpdateFleetDetails(iFleetAssetId,
                                                        dPurProposedDelivDate,
                                                        dFirstOnFleetDate,
                                                        sPurLocationOnFleet,
                                                        dPurReceiptDate,
                                                        sIdEngineNumber,
                                                        sIdChassisNumber,
                                                        sKeyNrIgnition,
                                                        sIdCompliancePlateYear,
                                                        sIdCompliancePlateMonth,
                                                        sRegistrationNumber,
                                                        dRegFirstDate,
                                                        dRCCofEndDate,
                                                        IIf(rucPaidTo = -1, Nothing, rucPaidTo), _
                                                        sETag, _
                                                        sEPurb, _
                                                        sUserCode, _
                                                        IIf(manufacturerId = -1, Nothing, manufacturerId), _
                                                        IIf(manufacturerModelId = -1, Nothing, manufacturerModelId), _
                                                        IIf(String.IsNullOrEmpty(mdFuelType), Nothing, mdFuelType), _
                                                        IIf(String.IsNullOrEmpty(mdTransmission), Nothing, mdTransmission), _
                                                        IIf(NextServiceId = -1, Nothing, NextServiceId), _
                                                        IIf(String.IsNullOrEmpty(regExpirydate), Nothing, regExpirydate), _
                                                        IIf(String.IsNullOrEmpty(comments), Nothing, comments), _
                                                        IIf(String.IsNullOrEmpty(WarrantyExpDate), Nothing, WarrantyExpDate), _
                                                        IIf(String.IsNullOrEmpty(SelfContainExpDate), Nothing, SelfContainExpDate), _
                                                        IIf(String.IsNullOrEmpty(TelematicsDevice), Nothing, TelematicsDevice), _
                                                        IIf(String.IsNullOrEmpty(TelematicsTablet), Nothing, TelematicsTablet), _
                                                        IIf(String.IsNullOrEmpty(TelematicsInstallDate), DBNull.Value, TelematicsInstallDate), _
                                                        IIf(String.IsNullOrEmpty(TelematicsInstaller), DBNull.Value, TelematicsInstaller) _
                                                        )

                        If (result.IndexOf("ERROR") <> -1) Then
                            ''transaction.RollbackTransaction()
                            Throw New Exception(result)
                        End If
                    Next

                    ''transaction.RollbackTransaction()
                    transaction.CommitTransaction()
                Catch ex As Exception
                    transaction.RollbackTransaction()
                    Throw New Exception(ex.Message)
                End Try

            End Using

        Catch ex As Exception
            Logging.LogError("UpdateFleetDetails".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR: ", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function InsUpFleetModelChange(dt As DataTable,
                                                 bIsInsert As Boolean,
                                                 bulkScheduleCode As String,
                                                 newEffectiveDate As DateTime,
                                                 userCode As String
                                                 ) As String
        Dim result As String = String.Empty

        Try
            Using transaction As New DatabaseTransaction()
                Try
                    For Each row As DataRow In dt.Rows

                        '@iUnitNumber				BIGINT,

                        '@iNewFleetModelId			BIGINT,

                        '@dNewEffectiveDate			DATETIME,

                        '@sCurrentFleetModelCode		VARCHAR(64) = null,

                        '@dCurrEffectiveDate			DATETIME = null,

                        '@bIsInsert					BIT	=	1,

                        '@sUserCode					VARCHAR(64)

                        result = DataRepository.InsUpFleetModelChange(
                            row("UnitNumber"),
                            bulkScheduleCode,
                            newEffectiveDate,
                            row("ModelCode"),
                            IIf(String.IsNullOrEmpty(row("EffectiveDate")), Nothing, row("EffectiveDate")),
                            bIsInsert,
                            userCode)

                        If (result.IndexOf("ERROR") <> -1) Then
                            'transaction.RollbackTransaction()
                            Throw New Exception(result)
                        End If
                    Next

                    transaction.CommitTransaction()
                Catch ex As Exception
                    transaction.RollbackTransaction()
                    Throw New Exception(ex.Message)
                End Try

            End Using

        Catch ex As Exception
            Logging.LogError("InsUpFleetModelChange".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR: ", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function GetLocationList(ByVal ParamArray params() As Object) As DataSet
        Return DataRepository.GetLocationList(params)
    End Function

    Public Shared Function GetUnitNumberDetails(fleetAssetId As Integer) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetUnitNumberDetails(fleetAssetId), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetUnitNumberDetails()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Logging.LogDebug("GetUnitNumberDetails - MANNY", result)
        Return result
    End Function

    Public Shared Function GetSingleUnitNumber(fleetAssetId As Integer) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetSingleUnitNumber(fleetAssetId), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetSingleUnitNumber()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function CheckUserRole(usercode As String, role As String) As String

        Dim result As String = "False"
        Try
            usercode = usercode.Trim
            role = role.Trim

            Dim oXmlDoc As System.Xml.XmlDocument = DataRepository.CheckUserRole(usercode, role)
            If Not oXmlDoc.DocumentElement.SelectSingleNode("/Error") Is Nothing Then
                ''result = oXmlDoc.DocumentElement.SelectSingleNode("/Error").InnerText
                result = "False"
            Else
                If Not oXmlDoc.DocumentElement.SelectSingleNode("/isUser") Is Nothing Then
                    If CBool(oXmlDoc.DocumentElement.SelectSingleNode("/isUser").InnerText) Then
                        result = "True"
                    End If
                End If
            End If
        Catch ex As Exception
            Logging.LogError("CheckUserRole()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try

        Return result
    End Function

    Public Shared Function CheckGenericUserRole(usercode As String, RoleMappingActualPage As String) As String
        Dim roleaccess As String = DataRepository.CheckGenericUserRole(usercode, RoleMappingActualPage)
        Select Case roleaccess.Trim()
            Case "AimsFull"
                roleaccess = "FullAccess"
                Exit Select
            Case "AimsPartial"
                roleaccess = "PartialAccess"
                Exit Select
            Case "AimsViewOnly"
                roleaccess = "ViewAccess"
                Exit Select
        End Select
        Return roleaccess
    End Function
End Class
