﻿''rev:mia Jan.17,2014 - First Version
''                    - refer to the Fleet Model MGT Plan Document


Imports Aurora.Common
Imports Aurora.Common.Logging
Imports Aurora.AIMS.Data
Imports System.Net.Mail
Imports System.Text
Imports System.Configuration
Imports Newtonsoft
Imports Aurora.Common.Data

Public Class FleetModel

    Public Shared Function GetDefaultCountry(UserCode As String) As String
        '' Return Json.JsonConvert.SerializeObject(DataRepository.GetDefaultCountry(UserCode), Json.Formatting.Indented)
        Return DataRepository.GetDefaultCountry(UserCode)
    End Function

    ''https://thlonline.atlassian.net/browse/JUSTGOUK-83
    Public Shared Function GetDefaultCountryId(UserCode As String) As String
        Return DataRepository.GetDefaultCountryId(UserCode)
    End Function


    Public Shared Function CheckIfVehiclesAreOnFleet(AssetLists As String) As String

        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.CheckIfVehiclesAreOnFleet(AssetLists), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetFleetModelDetails()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
        'Return DataRepository.CheckIfVehiclesAreOnFleet(AssetLists)
    End Function
    Public Shared Function GetCountries() As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetCountries, Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetCountries()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetFleetModelCodes() As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetFleetModelCodes(), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetFleetModelCodes()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetFleetModelCodesAutoSuggest(fleetmodel As String, ICountryId As Integer) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetFleetModelCodesAutoSuggest(fleetmodel, ICountryId), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetFleetModelCodesAutoSuggest()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    ''rev:mia 18-Oct-2016 Issues recoding NZ Fleet
    Public Shared Function GetSingleFleetModelCode(fleetmodel As String, ICountryId As Integer) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetSingleFleetModelCode(fleetmodel, ICountryId), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetSingleFleetModelCodes()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function InsertUpdateFleetModelCode(iFleetModelId As Integer, _
                                                      sFleetModelCode As String, _
                                                      sFleetModelDescription As String, _
                                                      bConversionIndicator As Boolean, _
                                                      mDailyRunningCost As Decimal, _
                                                      mDailyDelayCost As Decimal, _
                                                      bIsLocal As Boolean, _
                                                      sUserCode As String, _
                                                      Optional InactiveDate As String = "") As String
        Dim result As String = String.Empty
        Try
            result = DataRepository.InsertUpdateFleetModelCode(
                                                                   iFleetModelId, _
                                                                   sFleetModelCode, _
                                                                   sFleetModelDescription, _
                                                                   bConversionIndicator, _
                                                                   mDailyRunningCost, _
                                                                   mDailyDelayCost, _
                                                                   bIsLocal, _
                                                                   sUserCode, _
                                                                   InactiveDate _
                                                       )
        Catch ex As Exception
            Logging.LogError("InsertUpdateFleetModelCode()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result

    End Function

    Public Shared Function LoadDataForManufacturerModel() As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.LoadDataForManufacturerModel(), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("LoadDataForManufacturerModel()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function InsertUpdateFleetManufacturerModel(iFleetModelId As Integer, _
                                                         iManufacturerID As Integer, _
                                                         iFleetManufacturerModelID As Integer, _
                                                         sManufacturerModelCode As String, _
                                                         iCountryId As Integer, _
                                                         sFuelType As String, _
                                                         sTransmission As String, _
                                                         sManufacturerModel As String, _
                                                         iServiceScheduleId As Decimal, _
                                                         mChassisCost As String, _
                                                         iChassisCostId As Integer, _
                                                         iRegionModelId As Integer, _
                                                         iFleetModelManufacturerModelId As Integer, _
                                                         iRegionId As Integer, _
                                                         userCode As String) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.InsertUpdateFleetManufacturerModel(
                                                                      iFleetModelId, _
                                                                       iManufacturerID, _
                                                                       iFleetManufacturerModelID, _
                                                                       sManufacturerModelCode, _
                                                                       iCountryId, _
                                                                       sFuelType, _
                                                                       sTransmission, _
                                                                       sManufacturerModel, _
                                                                       iServiceScheduleId, _
                                                                       mChassisCost,
                                                                       iChassisCostId, _
                                                                       iRegionModelId, _
                                                                       iFleetModelManufacturerModelId, _
                                                                       iRegionId, _
                                                                       userCode), Json.Formatting.Indented)

            If (result = "ERROR") Then Throw New Exception("Error: Failed to update")
        Catch ex As Exception
            Logging.LogError("InsertUpdateFleetManufacturerModel()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetFleetModelDetails(fleetModelId As Integer, countryId As Integer) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetFleetModelDetails(fleetModelId, countryId), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetFleetModelDetails()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetManufacturerModeLCodeAutoSuggest(ManufacturerModeLCode As String, CountryId As Integer, ManufacturerId As Integer) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetManufacturerModeLCodeAutoSuggest(ManufacturerModeLCode, CountryId, ManufacturerId), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetManufacturerModeLCodeAutoSuggest()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetManufacturerModelDescriptionAutoSuggest(ManufacturerModeL As String) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetManufacturerModelDescriptionAutoSuggest(ManufacturerModeL), Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetManufacturerModelDescriptionAutoSuggest()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetNumberRules() As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetNumberRules _
                                                                   , Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetNumberRules()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function LinkFleetModelManufacturerModel(ManufacturerModelIDS As String, FleetModelId As Integer, usercode As String) As String
        Dim result As String = String.Empty
        Try
            Using transaction As New DatabaseTransaction()
                Dim ids() As String = ManufacturerModelIDS.Split(":")
                Dim id As String
                Try
                    For Each id In ids
                        result = DataRepository.LinkFleetModelManufacturerModel(CInt(id), FleetModelId, usercode)
                        If (result.IndexOf("ERROR") <> -1) Then
                            transaction.RollbackTransaction()
                            Throw New Exception("ERROR")
                        End If
                    Next
                    transaction.CommitTransaction()
                Catch ex As Exception
                    transaction.RollbackTransaction()
                    Throw New Exception(ex.Message)
                End Try

            End Using

        Catch ex As Exception
            Logging.LogError("LinkFleetModelManufacturerModel".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function GetManufacturerAutoSuggest(manufacturer As String) As String
        Dim result As String = String.Empty
        Try
            result = Json.JsonConvert.SerializeObject(DataRepository.GetManufacturer(manufacturer) _
                                                                   , Json.Formatting.Indented)
        Catch ex As Exception
            Logging.LogError("GetManufacturer()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            result = String.Concat("ERROR:", ex.Message)
        End Try
        Return result
    End Function

    Public Shared Function InsUpNumberRules(FleetNumberId As Integer, CountryId As Integer, FleetModelId As Integer, ManufacturerId As Integer, BeginAssetNum As Integer, EndAssetNum As Integer, UserCode As String) As String
        Dim output As Integer = -1
        Dim returnvalue As String = ""

        Try
            returnvalue = DataRepository.InsUpNumberRules(
                                                                   FleetNumberId, _
                                                                   CountryId, _
                                                                   FleetModelId, _
                                                                   ManufacturerId, _
                                                                   BeginAssetNum, _
                                                                   EndAssetNum, _
                                                                   UserCode)

        Catch ex As Exception
            Logging.LogError("InsUpNumberRules()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            returnvalue = String.Concat("ERROR: ", ex.Message)
        End Try
        Return returnvalue

    End Function
End Class
