Partial Public Class AgentDataSet
    Partial Class ContactRow

        Public ReadOnly Property AgentDataSet() As AgentDataSet
            Get
                Return CType(Me.Table.DataSet, AgentDataSet)
            End Get
        End Property

        Public ReadOnly Property Type() As String
            Get
                If Me.CodeRow IsNot Nothing Then
                    Return Me.CodeRow.CodDesc
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property FullName() As String
            Get
                Dim result As String = ""
                If Not IsConTitleNull() AndAlso Not String.IsNullOrEmpty(ConTitle.Trim()) Then result &= ConTitle.Trim()
                If Not IsConFirstNameNull() AndAlso Not String.IsNullOrEmpty(ConFirstName.Trim()) Then result &= " " & ConFirstName.Trim()
                If Not IsConLastNameNull() AndAlso Not String.IsNullOrEmpty(ConLastName.Trim()) Then result &= " " & ConLastName.Trim()
                If Not IsConPositionNull() AndAlso Not String.IsNullOrEmpty(ConPosition.Trim()) Then result &= " (" & ConPosition.Trim() & ")"
                Return result
            End Get
        End Property

        Public ReadOnly Property PhoneNumber() As String
            Get
                Return Me.PhoneNumberField("Day", "PhnCountryCode") _
                 & " " & Me.PhoneNumberField("Day", "PhnAreaCode") _
                 & " " & Me.PhoneNumberField("Day", "PhnNumber")
            End Get
        End Property

        Public ReadOnly Property FaxNumber() As String
            Get
                Return Me.PhoneNumberField("Fax", "PhnCountryCode") _
                 & " " & Me.PhoneNumberField("Fax", "PhnAreaCode") _
                 & " " & Me.PhoneNumberField("Fax", "PhnNumber")
            End Get
        End Property

        Public ReadOnly Property Email() As String
            Get
                Return Me.PhoneNumberField("Email", "PhnEmailAddress")
            End Get
        End Property

        ''' <summary>
        ''' Returns a Phone Number Row by its code
        ''' </summary>
        Public ReadOnly Property PhoneNumberRow(ByVal code As String) As PhoneNumberRow
            Get
                For Each result As PhoneNumberRow In Me.GetPhoneNumberRows()
                    If result.CodeRow IsNot Nothing And result.CodeRow.CodCode = code Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

        ''' <summary>
        ''' Returns a Phone Number Field by its row code and field name
        ''' </summary>
        Public ReadOnly Property PhoneNumberField(ByVal code As String, ByVal fieldName As String) As String
            Get
                Dim phoneNumberRow As PhoneNumberRow = Me.PhoneNumberRow(code)
                If phoneNumberRow IsNot Nothing AndAlso Not phoneNumberRow.IsNull(fieldName) Then
                    Return phoneNumberRow(fieldName).ToString()
                Else
                    Return ""
                End If
            End Get
        End Property

    End Class

End Class

