Imports Aurora.Common

Partial Public Class AgentDataSet
    Partial Class AgentPackageRow

        Public ReadOnly Property AvailableBookDescription() As String
            Get
                Dim result As String = ""
                If Not IsApkBookFromNull() Then result &= Me.ApkBookFrom.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsApkBookToNull() Then result &= Me.ApkBookTo.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property AvailableBookedFromDate() As Date
            Get
                If Not Me.IsApkBookFromNull Then Return Me.ApkBookFrom Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property AvailableBookedToDate() As Date
            Get
                If Not Me.IsApkBookToNull Then Return Me.ApkBookTo Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property BookedFromDate() As Date
            Get
                If Not Me.PackageRow.IsPkgBookedFromDateNull() Then Return Me.PackageRow.PkgBookedFromDate Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property BookedToDate() As Date
            Get
                If Not Me.PackageRow.IsPkgBookedToDateNull() Then Return Me.PackageRow.PkgBookedToDate Else Return Date.MaxValue
            End Get
        End Property

        Public ReadOnly Property TravelFromDate() As Date
            Get
                If Not Me.PackageRow.IsPkgTravelFromDateNull() Then Return Me.PackageRow.PkgTravelFromDate Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property TravelToDate() As Date
            Get
                If Not Me.PackageRow.IsPkgTravelToDateNull() Then Return Me.PackageRow.PkgTravelToDate Else Return Date.MaxValue
            End Get
        End Property

        Public ReadOnly Property IsPast(ByVal currentDate As Date) As Boolean
            Get
                'Return Not IsCurrent(currentDate) And Not IsFuture(currentDate)
                If currentDate > BookedToDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property


        Public ReadOnly Property IsCurrent(ByVal currentDate As Date) As Boolean
            Get
                'Return currentDate >= BookedFromDate AndAlso currentDate <= BookedToDate _
                ' AndAlso currentDate >= TravelFromDate AndAlso currentDate <= TravelToDate _
                ' AndAlso currentDate >= AvailableBookedFromDate AndAlso currentDate <= AvailableBookedToDate
                If currentDate <= BookedToDate And currentDate >= BookedFromDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsFuture(ByVal currentDate As Date) As Boolean
            Get
                'Return currentDate < BookedFromDate _
                ' AndAlso currentDate < TravelFromDate _
                ' AndAlso currentDate < AvailableBookedFromDate
                If currentDate < BookedFromDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Me.PackageRow.IsActive
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If Me.IsActive AndAlso Me.IsFuture(currentDate) Then
                    Return DataConstants.FutureColor
                ElseIf Me.IsActive AndAlso Me.IsCurrent(currentDate) Then
                    Return DataConstants.CurrentColor
                ElseIf Me.IsActive AndAlso Me.IsPast(currentDate) Then
                    Return DataConstants.PastColor
                Else
                    Return DataConstants.InactiveColor
                End If
            End Get
        End Property

    End Class
End Class
