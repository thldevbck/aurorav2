﻿Imports Aurora.Common

Public Class DebtorGroups

    Public Shared Function GetDebtorGroups(AgentID As String) As DataSet
        Dim result As DataSet = New DataSet

        Try
            Aurora.Common.Data.ExecuteDataSetSP("Agent_GetDebtorGroups", result, AgentID)

        Catch ex As Exception
            Logging.LogError("GetDebtorGroups", String.Concat(ex.Message, ", ", ex.StackTrace))
        End Try
        Return result
    End Function

    Public Shared Function UpdateDebtorGroups(input As String) As String
        Dim result As String = "OK"
        Try
            result = Aurora.Common.Data.ExecuteScalarSP("Agent_InsUpdDebtorStatus", input)
            If (String.IsNullOrEmpty(result)) Then result = "OK"
        Catch ex As Exception
            Logging.LogError("UpdateDebtorGroups", String.Concat(ex.Message, ", ", ex.StackTrace))
            result = "ERROR: Saving Debtor Groups"
        End Try

        Return result
    End Function
End Class
