Imports Aurora.Common

Partial Public Class AgentDataSet
    Partial Class PackageRow

        Public ReadOnly Property AgentDataSet() As AgentDataSet
            Get
                Return CType(Me.Table.DataSet, AgentDataSet)
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsPkgNameNull Then Return Me.PkgName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.PkgCode & " - " & Me.Name
            End Get
        End Property

        Public ReadOnly Property BrandDescription() As String
            Get
                If Me.BrandRow IsNot Nothing Then Return Me.BrandRow.BrdCode & " - " & Me.BrandRow.BrdName Else Return ""
            End Get
        End Property

        Public ReadOnly Property TypeDescription() As String
            Get
                If Not Me.IsPkgCodTypIdNull AndAlso Me.AgentDataSet.Code.FindById(Me.PkgCodTypId) IsNot Nothing Then
                    Dim codeRow As CodeRow = Me.AgentDataSet.Code.FindById(Me.PkgCodTypId)
                    Return codeRow.CodDesc
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property CountryDescription() As String
            Get
                If Me.CountryRow IsNot Nothing Then Return Me.CountryRow.CtyCode & " - " & Me.CountryRow.CtyName Else Return ""
            End Get
        End Property

        Public ReadOnly Property BookedDescription() As String
            Get
                Dim result As String = ""
                If Not IsPkgBookedFromDateNull() Then result &= Me.PkgBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsPkgBookedToDateNull() Then result &= Me.PkgBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property TravelDescription() As String
            Get
                Dim result As String = ""
                If Not IsPkgTravelFromDateNull() Then result &= Me.PkgTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsPkgTravelToDateNull() Then result &= Me.PkgTravelToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property StatusDescription() As String
            Get
                Return Me.PkgIsActive
            End Get
        End Property

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Me.PkgIsActive = "Active"
            End Get
        End Property

        Public ReadOnly Property IsPending() As Boolean
            Get
                Return Me.PkgIsActive = "Pending"
            End Get
        End Property

        Public ReadOnly Property IsInactive() As Boolean
            Get
                Return Not Me.IsActive AndAlso Not Me.IsPending
            End Get
        End Property


    End Class
End Class