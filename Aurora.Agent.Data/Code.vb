
Partial Class AgentDataSet
    Partial Class CodeDataTable

        ''' <summary>
        ''' Find the Code Data Row by Id
        ''' </summary>
        ''' <remarks>This would be far better done with relationships....but i dont want to create a hundred relationships on the dataset (code is used a lot)</remarks>
        Public Function FindById(ByVal CodId As String) As CodeRow
            For Each codeRow As CodeRow In Me.Rows
                If codeRow.CodId = CodId Then
                    Return codeRow
                End If
            Next
            Return Nothing

        End Function

        Public Function FindByCodCode(ByVal CodCode As String) As CodeRow
            For Each codeRow As CodeRow In Me.Rows
                If codeRow.CodCode = CodCode Then
                    Return codeRow
                End If
            Next
            Return Nothing

        End Function

        Public Function FindByCodCode(ByVal CodCode As String, ByVal AgentContact As Boolean) As CodeRow
            'For Each codeRow As CodeRow In Me.Rows
            '    If codeRow.CodCode = CodCode AndAlso IIf(AgentContact, IIf(codeRow.CodCdtNum = 45, True, False), True) Then
            '        Return codeRow
            '    End If
            'Next
            'Return Nothing
            Dim rwCodeRow As CodeRow

            For i As Integer = 0 To Me.Rows.Count - 1
                If CType(Me.Rows(i), CodeRow).CodCdtNum = 24 AndAlso CType(Me.Rows(i), CodeRow).CodCode = CodCode Then
                    rwCodeRow = CType(Me.Rows(i), CodeRow)
                End If
            Next

            If rwCodeRow Is Nothing Then ' do a default search
                Return FindByCodCode(CodCode)
            Else
                Return rwCodeRow
            End If

        End Function

    End Class

    Partial Class CodeRow
        Public Const CodeType_Address_Type As Integer = 1
        Public Const CodeType_Agent_Type As Integer = 3
        Public Const CodeType_Booked_Product_Type As Integer = 4
        Public Const CodeType_Incident_Type As Integer = 8
        Public Const CodeType_Location_Type As Integer = 9
        Public Const CodeType_Marketing_Region As Integer = 10
        Public Const CodeType_Mode_Of_Communication As Integer = 11
        Public Const CodeType_Note_Type As Integer = 13
        Public Const CodeType_Package_Type As Integer = 14
        Public Const CodeType_Person_Type As Integer = 16
        Public Const CodeType_Product_Rate_Discount_type As Integer = 17
        Public Const CodeType_Unit_of_Measure As Integer = 18
        Public Const CodeType_Referral_Type As Integer = 19
        Public Const CodeType_Specification_Unit_Of_Value As Integer = 20
        Public Const CodeType_Traveller_Type As Integer = 22
        Public Const CodeType_Currency As Integer = 23
        Public Const CodeType_Phone_Number_Type As Integer = 24
        Public Const CodeType_Contact_Type As Integer = 25
        Public Const CodeType_Audience_Type As Integer = 26
        Public Const CodeType_Cancellation_Reason As Integer = 27
        Public Const CodeType_Cancellation_Type As Integer = 28
        Public Const CodeType_Knock_Back_Type As Integer = 30
        Public Const CodeType_Note_Spec_Type As Integer = 31
        Public Const CodeType_Repairs_Maintenance_Reasons As Integer = 32
        Public Const CodeType_Relocation_Rule_Type As Integer = 33
        Public Const CodeType_Agent_Category As Integer = 35
        Public Const CodeType_Vis_Level As Integer = 36
        Public Const CodeType_Activity_Type As Integer = 37
        Public Const CodeType_Administration_Fee As Integer = 38
        Public Const CodeType_Payment_Method As Integer = 39
        Public Const CodeType_Booking_Request_Unit_of_Measure As Integer = 40
        Public Const CodeType_Person_Rate As Integer = 41
        Public Const CodeType_Status_Code As Integer = 42
        Public Const CodeType_Fleet_Override_Reasons As Integer = 43
        Public Const CodeType_AGENT_DEBTOR_STATUS As Integer = 44

        Public ReadOnly Property Description() As String
            Get
                If Me.IsCodDescNull Then
                    Return Me.CodCode
                Else
                    Return Me.CodCode + " - " & Me.CodDesc
                End If
            End Get
        End Property

    End Class
End Class
