Partial Public Class AgentDataSet
    Partial Class AgentRow

        Public ReadOnly Property AgentDataSet() As AgentDataSet
            Get
                Return CType(Me.Table.DataSet, AgentDataSet)
            End Get
        End Property

        Public ReadOnly Property AgentDescription() As String
            Get
                Dim result = ""
                If Not Me.IsAgnCodeNull Then result += Me.AgnCode
                result += " - "
                If Not Me.IsAgnNameNull Then result += Me.AgnName
                Return result
            End Get
        End Property

        Public ReadOnly Property AgentTypeDescription() As String
            Get
                If Me.AgentTypeRow IsNot Nothing AndAlso Not Me.AgentTypeRow.IsAtyDescNull Then
                    Return Me.AgentTypeRow.AtyDesc
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property AgentGroupDescription() As String
            Get
                If Me.AgentSubGroupRow IsNot Nothing AndAlso Me.AgentSubGroupRow.AgentGroupRow IsNot Nothing _
                 AndAlso Not Me.AgentSubGroupRow.IsAsgCodeNull AndAlso Not Me.AgentSubGroupRow.AgentGroupRow.IsAgpCodeNull Then
                    Return Me.AgentSubGroupRow.AgentGroupRow.AgpCode & " - " & Me.AgentSubGroupRow.AsgCode
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property DebtorStatusDescription() As String
            Get
                If Me.CodeRow IsNot Nothing AndAlso Not Me.CodeRow.IsCodDescNull Then
                    Return Me.CodeRow.CodDesc
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If Not Me.IsAgnIsActiveNull AndAlso Me.AgnIsActive Then
                    Return AgentConstants.ActiveColor
                Else
                    Return AgentConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property AddressDetailsRow(ByVal code As String) As AddressDetailsRow
            Get
                For Each result As AddressDetailsRow In Me.GetAddressDetailsRows()
                    If result.CodeRow IsNot Nothing And result.CodeRow.CodCode = code Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property BillingAddressDescription() As String
            Get
                Dim addressDetailsRow As AddressDetailsRow = Me.AddressDetailsRow("Billing")
                If addressDetailsRow IsNot Nothing Then
                    Return addressDetailsRow.ShortDescription
                Else
                    Return ""
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns a Phone Number Row by its code
        ''' </summary>
        Public ReadOnly Property PhoneNumberRow(ByVal code As String) As PhoneNumberRow
            Get
                For Each result As PhoneNumberRow In Me.GetPhoneNumberRows()
                    If result.CodeRow IsNot Nothing And result.CodeRow.CodCode = code Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

        ''' <summary>
        ''' Returns a Phone Number Field by its row code and field name
        ''' </summary>
        Public ReadOnly Property PhoneNumberField(ByVal code As String, ByVal fieldName As String) As String
            Get
                Dim phoneNumberRow As PhoneNumberRow = Me.PhoneNumberRow(code)
                If phoneNumberRow IsNot Nothing AndAlso Not phoneNumberRow.IsNull(fieldName) Then
                    Return phoneNumberRow(fieldName).ToString()
                Else
                    Return ""
                End If
            End Get
        End Property

        ''' <summary>
        ''' Returns an agent company row by company code
        ''' </summary>
        Public ReadOnly Property AgentCompanyRow(ByVal comCode As String) As AgentCompanyRow
            Get
                For Each result As AgentCompanyRow In Me.GetAgentCompanyRows()
                    If result.CompanyRow IsNot Nothing And result.CompanyRow.ComCode = comCode Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

        ''' <summary>
        ''' Get all the flex export contact rows by company code
        ''' </summary>
        Public Function GetFlexExportContactRows(ByVal comCode As String) As FlexExportContactRow()
            Dim list As New List(Of FlexExportContactRow)

            For Each flexExportContactRow As FlexExportContactRow In Me.GetFlexExportContactRows()
                If flexExportContactRow.FlexExportTypeRow IsNot Nothing AndAlso flexExportContactRow.FlexExportTypeRow.FlxComCode = comCode Then
                    list.Add(flexExportContactRow)
                End If
            Next

            Return list.ToArray()
        End Function

        ''' <summary>
        ''' Get all the contact rows by specific type (code)
        ''' </summary>
        Public Function GetContactRows(ByVal code As String) As ContactRow()
            Dim list As New List(Of ContactRow)

            For Each contactRow As ContactRow In Me.GetContactRows()
                If contactRow.CodeRow IsNot Nothing AndAlso contactRow.CodeRow.CodCode = code Then
                    list.Add(contactRow)
                End If
            Next

            Return list.ToArray()
        End Function

        Private Sub UpdateAgentCompany(ByVal companyRow As CompanyRow)

            Dim agentCompanyRow As AgentCompanyRow = Me.AgentCompanyRow(companyRow.ComCode)
            Dim flexExportContactRows As FlexExportContactRow() = Me.GetFlexExportContactRows(companyRow.ComCode)
            Dim contactRows As ContactRow() = Me.GetContactRows("Flex")

            If flexExportContactRows.Length = 0 OrElse contactRows.Length = 0 Then
                ' we are not liked to any flex export types or we no flex contacts at all, so delete the agent company record if it exists

                If agentCompanyRow IsNot Nothing Then
                    agentCompanyRow.Delete()
                    Aurora.Common.Data.ExecuteDataRow(agentCompanyRow)
                    AgentDataSet.AgentCompany.RemoveAgentCompanyRow(agentCompanyRow)
                End If
            Else
                ' ok, we have contacts, so create an agent company row

                ' calculate the emails
                Dim email0 As Object = Nothing
                If contactRows.Length > 0 Then email0 = contactRows(0).Email.Trim()
                Dim email1 As Object = Nothing
                If contactRows.Length > 1 Then email1 = contactRows(1).Email.Trim()
                Dim email2 As Object = Nothing
                If contactRows.Length > 2 Then email2 = contactRows(2).Email.Trim()

                If agentCompanyRow Is Nothing Then
                    agentCompanyRow = AgentDataSet.AgentCompany.NewAgentCompanyRow()
                    agentCompanyRow.AgcAgnId = Me.AgnId
                    agentCompanyRow.AgcComCode = companyRow.ComCode
                    If Not String.IsNullOrEmpty(email0) Then
                        agentCompanyRow.AgcEmail0 = email0
                    Else
                        agentCompanyRow.SetAgcEmail0Null()
                    End If
                    If Not String.IsNullOrEmpty(email1) Then
                        agentCompanyRow.AgcEmail1 = email1
                    Else
                        agentCompanyRow.SetAgcEmail1Null()
                    End If
                    If Not String.IsNullOrEmpty(email2) Then
                        agentCompanyRow.AgcEmail2 = email2
                    Else
                        agentCompanyRow.SetAgcEmail2Null()
                    End If
                    AgentDataSet.AgentCompany.AddAgentCompanyRow(agentCompanyRow)
                    Aurora.Common.Data.ExecuteDataRow(agentCompanyRow)
                Else
                    If ((String.IsNullOrEmpty(email0) AndAlso agentCompanyRow.IsAgcEmail0Null) OrElse (Not String.IsNullOrEmpty(email0) AndAlso Not agentCompanyRow.IsAgcEmail0Null AndAlso email0 = agentCompanyRow.AgcEmail0)) _
                     AndAlso ((String.IsNullOrEmpty(email1) AndAlso agentCompanyRow.IsAgcEmail1Null) OrElse (Not String.IsNullOrEmpty(email1) AndAlso Not agentCompanyRow.IsAgcEmail1Null AndAlso email1 = agentCompanyRow.AgcEmail1)) _
                     AndAlso ((String.IsNullOrEmpty(email2) AndAlso agentCompanyRow.IsAgcEmail2Null) OrElse (Not String.IsNullOrEmpty(email2) AndAlso Not agentCompanyRow.IsAgcEmail2Null AndAlso email2 = agentCompanyRow.AgcEmail2)) Then
                        ' nothing has changed, leave it
                    Else
                        ' update the agent company with the new emails from the contacts
                        If Not String.IsNullOrEmpty(email0) Then
                            agentCompanyRow.AgcEmail0 = email0
                        Else
                            agentCompanyRow.SetAgcEmail0Null()
                        End If
                        If Not String.IsNullOrEmpty(email1) Then
                            agentCompanyRow.AgcEmail1 = email1
                        Else
                            agentCompanyRow.SetAgcEmail1Null()
                        End If
                        If Not String.IsNullOrEmpty(email2) Then
                            agentCompanyRow.AgcEmail2 = email2
                        Else
                            agentCompanyRow.SetAgcEmail2Null()
                        End If
                        Aurora.Common.Data.ExecuteDataRow(agentCompanyRow)
                    End If
                End If
            End If
        End Sub

        ''' <summary>
        ''' Refresh the Agent Company records for a specific based on information in FlexExportContact and Contact
        ''' </summary>
        Public Sub UpdateAgentCompany()
            For Each companyRow As CompanyRow In AgentDataSet.Company
                UpdateAgentCompany(companyRow)
            Next
        End Sub

    End Class
End Class
