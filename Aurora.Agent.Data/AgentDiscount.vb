Partial Public Class AgentDataSet
    Partial Class AgentDiscountRow

        Public ReadOnly Property BrandDescription() As String
            Get
                If Me.BrandRow IsNot Nothing _
                 AndAlso Not Me.BrandRow.IsBrdIsGenericNull _
                 AndAlso Not Me.BrandRow.BrdIsGeneric Then
                    Return Me.BrandRow.BrdCode & " - " & Me.BrandRow.BrdName
                Else
                    Return "All"
                End If
            End Get
        End Property

        Public ReadOnly Property ChgAgentDescription() As String
            Get
                Return IIf(Not Me.IsAdsChgAgentNull AndAlso Me.AdsChgAgent, "Yes", "No")
            End Get
        End Property

        Public ReadOnly Property BookedDateTypeDescription() As String
            Get
                Return IIf(Not Me.IsAdsIsBookedDateTypeNull AndAlso Me.AdsIsBookedDateType, "Booked", "Travel")
            End Get
        End Property

        Public ReadOnly Property PercentageDescription() As String
            Get
                If Not Me.IsAdsPercentageNull Then
                    Return Me.AdsPercentage.ToString("f2")
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property CountryDescription() As String
            Get
                If Me.CountryRow IsNot Nothing Then
                    Return Me.CountryRow.CtyCode & " - " & Me.CountryRow.CtyName
                Else
                    Return "All"
                End If
            End Get
        End Property

        Public ReadOnly Property EffectiveDateDescription() As String
            Get
                If Not Me.IsAdsEffDateNull Then
                    Return Me.AdsEffDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Else
                    Return ""
                End If
            End Get
        End Property

#Region "rev:mia january 5 "
        ''rev:mia jan.5,2009 this is a fixed on issue Call : 22933 - Unable to Amend Agent Discuont to Zero
        Public ReadOnly Property PercentageDescriptionDiscounts() As String
            Get
                If Not Me.IsAdsPercentageNull Then

                    If Me.AdsPercentage = 0D Then
                        Return "0"
                    End If

                    If Me.AdsPercentage = CDec(9999999999) Then
                        Return ""
                    End If

                    Return Me.AdsPercentage.ToString("f2")
                Else
                    Return ""
                End If
            End Get
        End Property
#End Region

    End Class


End Class

