Imports System.Data
Imports System.Web
Imports System.Configuration

Imports Aurora.Agent.Data.AgentDataSet
Public Module DataRepository

    Public Function GetLookups(ByVal agentDataSet As AgentDataSet, ByVal comCode As String) As AgentDataSet
        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Agent_GetLookups", result, comCode)

        Aurora.Common.Data.CopyDataTable(result.Tables(0), agentDataSet.Code)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), agentDataSet.AgentType)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), agentDataSet.Country)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), agentDataSet.FlexExportType)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), agentDataSet.Brand)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), agentDataSet.Company)

        Return agentDataSet
    End Function

    Public Function GetNewId() As String
        Return Aurora.Common.Data.GetNewId()
    End Function

    Public Function SearchAgent( _
        ByVal agentDataSet As AgentDataSet, _
        ByVal agnId As String, _
        ByVal agnCode As String, _
        ByVal agnName As String, _
        ByVal agnAgpCode As String, _
        ByVal agnAtyId As String, _
        ByVal agnDebtStatId As String, _
        ByVal agnPhnCountryCode As String, _
        ByVal agnPhnAreaCode As String, _
        ByVal agnPhnNumber As String, _
        ByVal agnAddAddress1 As String, _
        ByVal agnAddAddress2 As String, _
        ByVal agnAddAddress3 As String, _
        ByVal agnAddState As String, _
        ByVal agnAddPostcode As String, _
        ByVal agnAddCtyCode As String, _
        ByVal agnIsActive As Nullable(Of Boolean), _
        ByVal agnCodeExact As String, _
        Optional ByVal agnSoundEx As Boolean = False, _
        Optional ByVal hasB2B As Integer = -1) As AgentDataSet

        'CREATE PROCEDURE [dbo].[Agent_SearchAgent] 
        '(
        '	@AgnId AS varchar(64) = NULL,
        '	@AgnCode AS varchar(10) = NULL,
        '	@AgnName AS varchar(64) = NULL,
        '	@AgnAgpCode AS varchar(24) = NULL,
        '	@AgnAtyId AS varchar(64) = NULL,
        '	@AgnDebtStatId AS varchar(64) = NULL,
        '	@AgnPhnCountryCode AS varchar(6) = NULL,
        '	@AgnPhnAreaCode AS varchar(6) = NULL,
        '	@AgnPhnNumber AS varchar(24) = NULL,
        '	@AgnAddAddress1 AS varchar(64) = NULL,
        '	@AgnAddAddress2 AS varchar(64) = NULL,
        '	@AgnAddAddress3 AS varchar(64) = NULL,
        '	@AgnAddState AS varchar(24) = NULL,
        '	@AgnAddPostcode AS varchar(15) = NULL,
        '	@AgnAddCtyCode AS varchar(12) = NULL,
        '	@AgnIsActive AS bit = NULL,
        '	@AgnCodeExact AS varchar(10) = NULL
        ')

        ''REV:MIA JAN.23 2012 - RELATED TO B2B SEARCH: HELPDESK # 31925
        Dim storedproc As String = "Agent_SearchAgent_B2Baccess" ''"Agent_SearchAgent"


        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP(storedproc, result, _
            IIf(Not String.IsNullOrEmpty(agnId), agnId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnCode), agnCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnName), agnName, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAgpCode), agnAgpCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAtyId), agnAtyId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnDebtStatId), agnDebtStatId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnPhnCountryCode), agnPhnCountryCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnPhnAreaCode), agnPhnAreaCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnPhnNumber), agnPhnNumber, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAddAddress1), agnAddAddress1, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAddAddress2), agnAddAddress2, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAddAddress3), agnAddAddress3, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAddState), agnAddState, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAddPostcode), agnAddPostcode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnAddCtyCode), agnAddCtyCode, DBNull.Value), _
            IIf(agnIsActive.HasValue, agnIsActive.GetValueOrDefault(), DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnCodeExact), agnCodeExact, DBNull.Value), _
            agnSoundEx, _
            IIf(hasB2B = -1, hasB2B, DBNull.Value))  ''REV:MIA JAN.23 2012 - RELATED TO B2B SEARCH: HELPDESK # 31925

        If (hasB2B = 0) Then
            Aurora.Common.Data.CopyDataTable(result.Tables(0), agentDataSet.Agent)
        End If

        Aurora.Common.Data.CopyDataTable(result.Tables(1), agentDataSet.AgentSubGroup)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), agentDataSet.AgentGroup)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), agentDataSet.AddressDetails)

        ''REV:MIA JAN.23 2012 - RELATED TO B2B SEARCH: HELPDESK # 31925
        If (hasB2B <> 0) Then
            Aurora.Common.Data.CopyDataTable(result.Tables(4), agentDataSet.Agent)
        End If


        Return agentDataSet
    End Function

    Public Function GetAgentByCode( _
     ByVal agentDataSet As AgentDataSet, _
     ByVal agnCode As String)

        Return SearchAgent(agentDataSet, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, New Nullable(Of Boolean), agnCode)

    End Function

    Public Function GetAgent( _
        ByVal agentDataSet As AgentDataSet, _
        ByVal comCode As String, _
        ByVal agnId As String) As AgentDataSet

        'CREATE PROCEDURE [dbo].[Agent_GetAgent] 
        '(
        '	@ComCode AS varchar(64),
        '	@AgnId AS varchar(64)
        ')

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Agent_GetAgent", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agnId), agnId, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), agentDataSet.Agent)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), agentDataSet.PhoneNumber)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), agentDataSet.AgentSubGroup)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), agentDataSet.AgentGroup)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), agentDataSet.AddressDetails)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), agentDataSet.Product)
        Aurora.Common.Data.CopyDataTable(result.Tables(6), agentDataSet.Note)
        Aurora.Common.Data.CopyDataTable(result.Tables(7), agentDataSet.Contact)
        Aurora.Common.Data.CopyDataTable(result.Tables(8), agentDataSet.FlexExportContact)
        Aurora.Common.Data.CopyDataTable(result.Tables(9), agentDataSet.AgentDiscount)
        Aurora.Common.Data.CopyDataTable(result.Tables(10), agentDataSet.AgentCompany)
        Aurora.Common.Data.CopyDataTable(result.Tables(11), agentDataSet.AgentPackage)
        Aurora.Common.Data.CopyDataTable(result.Tables(12), agentDataSet.Package)
        Aurora.Common.Data.CopyDataTable(result.Tables(13), agentDataSet.AgentUserInfo)
        Aurora.Common.Data.CopyDataTable(result.Tables(14), agentDataSet.B2BUserInfo)
        Aurora.Common.Data.CopyDataTable(result.Tables(15), agentDataSet.B2BAgentExchangeRate)
        Aurora.Common.Data.CopyDataTable(result.Tables(16), agentDataSet.Currency)
        Aurora.Common.Data.CopyDataTable(result.Tables(17), agentDataSet.B2BCurrencyConfig)

        Return agentDataSet
    End Function

    ' Shoel - b2b changes 

    ''' <summary>
    ''' Called from user maint, a single agent id is passed
    ''' </summary>
    ''' <param name="B2BUserInfo"></param>
    ''' <param name="AgentId"></param>
    ''' <param name="UserCode"></param>
    ''' <param name="AddProgramName"></param>
    ''' <param name="ErrorMessage"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function InsertUpdateB2BConfig(ByVal B2BUserInfo As Aurora.Agent.Data.AgentDataSet.B2BUserInfoRow, _
                                      ByVal AgentId As String, _
                                      ByVal UserCode As String, _
                                      ByVal AddProgramName As String, _
                                      ByVal UpdateCurrency As Boolean, _
                                      ByRef ErrorMessage As String) As Boolean

        Try
            'ALTER PROCEDURE [dbo].[WEB_InsertUpdateB2BConfig]
            '	@sUsrCode			VARCHAR(64)				
            '	,@shasCurrOpt		VARCHAR(5)
            '	,@shasCusCongOpt	VARCHAR(5)
            '	,@shasGrossNetOpt	VARCHAR(5)
            '	,@sGrossNetWhat		VARCHAR(5)
            '	,@sGrossNetDef		VARCHAR(1)
            '	,@sAddUsrCode		VARCHAR(64)
            '	,@pAddPrgm			VARCHAR(256)	


            ' 2 phase approach

            ' 1 - save b2b config
            Dim arParrams(7) As Aurora.Common.Data.Parameter

            arParrams(0) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, B2BUserInfo.UsrCode, Common.Data.Parameter.ParameterType.AddInParameter)
            arParrams(1) = New Aurora.Common.Data.Parameter("shasCurrOpt", DbType.String, IIf(B2BUserInfo.B2BHasCurrOpt, "Yes", "No"), Common.Data.Parameter.ParameterType.AddInParameter)
            arParrams(2) = New Aurora.Common.Data.Parameter("shasCusCongOpt", DbType.String, IIf(B2BUserInfo.B2BHasCusConfirmationOpt, "Yes", "No"), Common.Data.Parameter.ParameterType.AddInParameter)
            arParrams(3) = New Aurora.Common.Data.Parameter("shasGrossNetOpt", DbType.String, IIf(B2BUserInfo.B2BHasGrossNetOpt, "Yes", "No"), Common.Data.Parameter.ParameterType.AddInParameter)
            arParrams(4) = New Aurora.Common.Data.Parameter("sGrossNetWhat", DbType.String, B2BUserInfo.B2BGrossNetWhatOpt, Common.Data.Parameter.ParameterType.AddInParameter)

            arParrams(5) = New Aurora.Common.Data.Parameter("sGrossNetDef", DbType.String, B2BUserInfo.B2BGrossNetDefOpt, Common.Data.Parameter.ParameterType.AddInParameter)
            arParrams(6) = New Aurora.Common.Data.Parameter("sAddUsrCode", DbType.String, UserCode, Common.Data.Parameter.ParameterType.AddInParameter)
            arParrams(7) = New Aurora.Common.Data.Parameter("pAddPrgm", DbType.String, AddProgramName, Common.Data.Parameter.ParameterType.AddInParameter)

            'Aurora.Common.Data.ExecuteScalarSP("WEB_InsertUpdateB2BConfig", arParrams)
            Aurora.Common.Data.ExecuteOutputObjectSP("WEB_InsertUpdateB2BConfig", arParrams)

            If UpdateCurrency Then
                ' 2 - save currency data
                Return Aurora.Admin.Data.SaveB2BUserAgentCurrencyData(B2BUserInfo.UsrId, AgentId, B2BUserInfo.B2BCurrencyCodeList, _
                                                                      B2BUserInfo.B2BDefaultCurrencyCode, String.Empty, UserCode, AddProgramName)
            Else
                Return True
            End If

            'Return True
        Catch ex As Exception
            ErrorMessage = ex.Message

            Return False
        End Try


    End Function




    Function InsertUpdateB2BConfig(ByVal B2BUserInfo As Aurora.Agent.Data.AgentDataSet.B2BUserInfoRow, _
                                   ByVal AgentId As String, _
                                   ByVal UserCode As String, _
                                   ByVal AddProgramName As String, _
                                   ByRef ErrorMessage As String) As Boolean
        
        Return InsertUpdateB2BConfig(B2BUserInfo, AgentId, UserCode, AddProgramName, False, ErrorMessage)
    End Function


    Function InsertUpdateB2BConfig(ByVal B2BUserInfo As Aurora.Agent.Data.AgentDataSet.B2BUserInfoRow, _
                                   ByVal UserCode As String, _
                                   ByVal AddProgramName As String, _
                                   ByRef ErrorMessage As String) As Boolean

        ' called from agent maint
        Return InsertUpdateB2BConfig(B2BUserInfo, B2BUserInfo.AgentUserInfoRow.AuiAgnId, UserCode, AddProgramName, ErrorMessage)

    End Function

    Function SaveB2BExchangeRates(ByVal DefaultCurrencyId As String, ByVal AgentId As String, ByVal FromCurrencyId As String, _
                                  ByVal ToCurrencyId As String, ByVal EffectiveDate As DateTime, _
                                  ByVal ExchangeRate As Double, ByVal ProgramName As String, ByVal UserCode As String, _
                                  ByRef ErrorMessage As String) As Boolean
        'CREATE PROC Agent_SaveB2BExchangeRates
        '	@sB2BAgnDefaultCurrId AS VARCHAR(64)
        '	,@sB2BAgnId AS VARCHAR(64)
        '	,@sB2BFromCodCurrId AS VARCHAR(64)
        '	,@sB2BToCodCurrId AS VARCHAR(64)
        '	,@dtB2BEffectiveDate AS DATETIME
        '	,@dB2BExchRate AS MONEY
        '	,@sAddPrgmName AS VARCHAR(128) 
        '	,@sUserCode AS VARCHAR(12)

        ErrorMessage = Aurora.Common.Data.ExecuteSqlXmlSP("Agent_SaveB2BExchangeRates", DefaultCurrencyId, AgentId, FromCurrencyId, ToCurrencyId, EffectiveDate, ExchangeRate, ProgramName, UserCode)

        Dim xmlTemp As New System.Xml.XmlDocument
        xmlTemp.LoadXml(ErrorMessage)

        If xmlTemp.DocumentElement.InnerText <> "" Then
            Return False
        Else
            ErrorMessage = xmlTemp.DocumentElement.InnerText
            Return True
        End If

    End Function

    Function SaveB2BCurrencyConfig(ByVal AgentId As String, _
                                   ByVal CurrencyOptionEnabled As Boolean, _
                                   ByVal CurrencyIdList As String, _
                                   ByVal UserCode As String, _
                                   ByVal ProgramName As String, _
                                   ByRef ErrorMessage As String) As Boolean
        'ALTER PROC Agent_SaveB2BCurrencyOptions
        '	@sAgnId AS VARCHAR(64)
        '	,@bCurrencyOptionEnabled AS BIT
        '	,@sCurrencyIdList AS VARCHAR(MAX)
        '	,@sUserCode AS VARCHAR(12)
        '	,@sProgramName AS VARCHAR(64)

        ErrorMessage = Aurora.Common.Data.ExecuteSqlXmlSP("Agent_SaveB2BCurrencyOptions", AgentId, CurrencyOptionEnabled, CurrencyIdList, UserCode, ProgramName)

        Dim xmlTemp As New System.Xml.XmlDocument
        xmlTemp.LoadXml(ErrorMessage)

        If xmlTemp.DocumentElement.InnerText <> "" Then
            Return False
        Else
            ErrorMessage = xmlTemp.DocumentElement.InnerText
            Return True
        End If

    End Function



End Module
