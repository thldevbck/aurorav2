Partial Public Class AgentDataSet
    Partial Class ProductRow

        Public ReadOnly Property Description() As String
            Get
                Return Me.PrdShortName & " - " & Me.PrdName
            End Get
        End Property


        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If Not Me.IsPrdIsActiveNull AndAlso Me.PrdIsActive Then
                    Return AgentConstants.ActiveColor
                Else
                    Return AgentConstants.InactiveColor
                End If
            End Get
        End Property

    End Class
End Class
