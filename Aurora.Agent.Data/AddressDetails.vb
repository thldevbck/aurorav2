Partial Public Class AgentDataSet

    Partial Class AddressDetailsRow

        Public ReadOnly Property AgentDataSet() As AgentDataSet
            Get
                Return CType(Me.Table.DataSet, AgentDataSet)
            End Get
        End Property

        Public ReadOnly Property ShortDescription() As String
            Get
                Dim result As String = ""
                If Not Me.IsAddAddress3Null Then result += Me.AddAddress3 + ", "
                If Me.CountryRow IsNot Nothing AndAlso Not Me.CountryRow.IsCtyNameNull Then result += Me.CountryRow.CtyName
                Return result
            End Get
        End Property

    End Class
End Class

