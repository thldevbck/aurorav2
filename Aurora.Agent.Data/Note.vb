Partial Public Class AgentDataSet
    Partial Class NoteRow
        Public Const DefaultPriority As Integer = 3


        Public ReadOnly Property AgentDataSet() As AgentDataSet
            Get
                Return CType(Me.Table.DataSet, AgentDataSet)
            End Get
        End Property

        Public Property Description() As String
            Get
                If Not Me.IsNteDescNull Then Return Me.NteDesc Else Return ""
            End Get
            Set(ByVal value As String)
                Me.NteDesc = value
            End Set
        End Property

        Public Property CodAudTypId() As String
            Get
                If Not Me.IsNteCodAudTypIdNull Then Return Me.NteCodAudTypId Else Return ""
            End Get
            Set(ByVal value As String)
                Me.NteCodAudTypId = value
            End Set
        End Property

        Public ReadOnly Property AudienceCode() As CodeRow
            Get
                If Not Me.IsNteCodAudTypIdNull Then
                    Return AgentDataSet.Code.FindById(Me.NteCodAudTypId)
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property Audience() As String
            Get
                Dim code As CodeRow = AudienceCode
                If code IsNot Nothing AndAlso Not code.IsCodDescNull Then Return code.CodDesc Else Return ""
            End Get
        End Property

        Public ReadOnly Property TypeCode() As CodeRow
            Get
                If Not Me.IsNteCodTypIdNull Then
                    Return AgentDataSet.Code.FindById(Me.NteCodTypId)
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property Type() As String
            Get
                Dim code As CodeRow = TypeCode
                If code IsNot Nothing AndAlso Not code.IsCodDescNull Then Return code.CodDesc Else Return ""
            End Get
        End Property

        Public Property Priority() As Integer
            Get
                If Not Me.IsNtePriorityNull Then Return Me.NtePriority Else Return DefaultPriority
            End Get
            Set(ByVal value As Integer)
                Me.NtePriority = value
            End Set
        End Property

        Public Property IsActive() As Boolean
            Get
                If Not Me.IsNteIsActiveNull Then Return Me.NteIsActive Else Return False
            End Get
            Set(ByVal value As Boolean)
                Me.NteIsActive = value
            End Set
        End Property

        Public ReadOnly Property DetailsDescription() As String
            Get
                Return "" _
                    & "Audience: " & Audience _
                    & vbCrLf & "Priority: " & Priority & ", Active: " & IIf(IsActive, "Yes", "No")
            End Get
        End Property

        Public ReadOnly Property UserDescription() As String
            Get
                Return Aurora.Common.Data.GetAddModDescription(Me)
            End Get
        End Property

        Public ReadOnly Property ModifiedDate() As Date
            Get
                If Not Me.IsModDateTimeNull Then
                    Return Me.ModDateTime
                Else
                    Return Me.AddDateTime
                End If
            End Get
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If Me.IsActive Then
                    Return AgentConstants.CurrentColor
                Else
                    Return AgentConstants.InactiveColor
                End If
            End Get
        End Property

    End Class
End Class
