''rev:mia Sept 16 2013 - add ComCode
Imports System.Data
Imports System.Web
Imports System.Configuration


Imports Aurora.Common.Data
Imports Aurora.Package.Data.PackageDataSet

Imports Aurora.Package.Data
Imports Aurora.Common

Public Module DataRepository

    Private Sub CopyDataTable(ByVal source As DataTable, ByVal target As DataTable)
        For Each dataRow As DataRow In source.Rows
            target.ImportRow(dataRow)
        Next
    End Sub

    '=======================================================================================================
    ' Lookups
    '=======================================================================================================

    Public Function GetLookups(ByVal packageDataSet As PackageDataSet, ByVal comCode As String) As PackageDataSet
        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Package_GetLookups", result, comCode)

        CopyDataTable(result.Tables(0), packageDataSet.Brand)
        CopyDataTable(result.Tables(1), packageDataSet.Code)
        CopyDataTable(result.Tables(2), packageDataSet.Country)
        CopyDataTable(result.Tables(3), packageDataSet.Location)
        CopyDataTable(result.Tables(4), packageDataSet.Type)
        CopyDataTable(result.Tables(5), packageDataSet.FlexTravelYear)

        Return packageDataSet
    End Function

    '=======================================================================================================
    ' Packages
    '=======================================================================================================

    Public Function GetNewId() As String
        Return Aurora.Common.Data.GetNewId()
    End Function

    Public Function SearchPackage( _
     ByVal packageDataSet As PackageDataSet, _
     ByVal comCode As String, _
     ByVal pkgId As String, _
     ByVal pkgCode As String, _
     ByVal pkgName As String, _
     ByVal pkgBookedFromDate As Date, _
     ByVal pkgBookedToDate As Date, _
     ByVal pkgTravelFromDate As Date, _
     ByVal pkgTravelToDate As Date, _
     ByVal pkgBrdCode As String, _
     ByVal pkgCodTypId As String, _
     ByVal pkgCtyCode As String, _
     ByVal pkgIsActive As String, _
     ByVal pkgSapId As String, _
     ByVal pkgIsWebEnabled As Boolean, _
     ByVal pkgIsInclusive As Boolean _
     ) As PackageDataSet

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Package_SearchPackage", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgId), pkgId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgCode), pkgCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgName), pkgName, DBNull.Value), _
            IIf(pkgBookedFromDate <> Date.MinValue, pkgBookedFromDate, DBNull.Value), _
            IIf(pkgBookedToDate <> Date.MinValue, pkgBookedToDate, DBNull.Value), _
            IIf(pkgTravelFromDate <> Date.MinValue, pkgTravelFromDate, DBNull.Value), _
            IIf(pkgTravelToDate <> Date.MinValue, pkgTravelToDate, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgBrdCode), pkgBrdCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgCodTypId), pkgCodTypId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgCtyCode), pkgCtyCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgIsActive), pkgIsActive, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgSapId), pkgSapId, DBNull.Value), _
            pkgIsInclusive, _
            pkgIsWebEnabled)

        CopyDataTable(result.Tables(0), packageDataSet.Package)

        Return packageDataSet
    End Function

    Public Function GetPackageByCode( _
     ByVal packageDataSet As PackageDataSet, _
     ByVal comCode As String, _
     ByVal pkgCode As String)

        Return SearchPackage(packageDataSet, comCode, Nothing, pkgCode, Nothing, Date.MinValue, Date.MinValue, Date.MinValue, Date.MinValue, Nothing, Nothing, Nothing, Nothing, Nothing, False, False)
    End Function

    Public Function GetPackageByPkgId( _
     ByVal packageDataSet As PackageDataSet, _
     ByVal comCode As String, _
     ByVal pkgId As String)

        Return SearchPackage(packageDataSet, comCode, pkgId, Nothing, Nothing, Date.MinValue, Date.MinValue, Date.MinValue, Date.MinValue, Nothing, Nothing, Nothing, Nothing, Nothing, False, False)
    End Function

    Public Function GetPackage( _
     ByVal packageDataSet As PackageDataSet, _
     ByVal comCode As String, _
     ByVal pkgId As String) As PackageDataSet

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Package_GetPackage", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgId), pkgId, DBNull.Value))

        CopyDataTable(result.Tables(0), packageDataSet.Package)
        CopyDataTable(result.Tables(1), packageDataSet.PackageProduct)
        CopyDataTable(result.Tables(2), packageDataSet.SaleableProduct)
        CopyDataTable(result.Tables(3), packageDataSet.Product)
        CopyDataTable(result.Tables(4), packageDataSet.LocationAvailability)
        CopyDataTable(result.Tables(5), packageDataSet.NonAvailabilityProfile)
        CopyDataTable(result.Tables(6), packageDataSet.WeeklyProfile)
        CopyDataTable(result.Tables(7), packageDataSet.PackageMarketingRegion)
        CopyDataTable(result.Tables(8), packageDataSet.Note)
        CopyDataTable(result.Tables(9), packageDataSet.NoteUserInfo)
        CopyDataTable(result.Tables(10), packageDataSet.AgentPackage)
        CopyDataTable(result.Tables(11), packageDataSet.Agent)
        CopyDataTable(result.Tables(12), packageDataSet.AgentGroup)
        CopyDataTable(result.Tables(13), packageDataSet.FlexLevel)

        Return packageDataSet
    End Function

    Public Function CreatePackageProduct(ByVal pkgId As String, ByVal sapId As String, ByVal copyCreateLink As String, ByVal usrId As String, ByVal prgmName As String) As PackageProductRow
        Dim result As PackageProductDataTable = Aurora.Common.Data.ExecuteTableSP( _
            "Package_CreatePackageProduct", _
            New PackageProductDataTable(), _
            pkgId, sapId, copyCreateLink, usrId, prgmName)
        If result.Count = 1 Then
            Return result(0)
        Else
            Return Nothing
        End If
    End Function

    Public Sub DeletePackageProduct(ByVal pkgId As String, ByVal sapId As String)
        Aurora.Common.Data.ExecuteScalarSP( _
            "Package_DeletePackageProduct", _
            pkgId, sapId)
    End Sub

#Region "rev:mia Project PHOENIX"

    Public Function InsertDomesticCountries(ByVal PkgSetupId As String, ByVal PkgId As String, _
                                                         ByVal PkgCountries As String, _
                                                         ByVal AddUsrId As String) As String

        Dim retValue As String = ""
        Dim params(3) As Aurora.Common.Data.Parameter

        Try

            params(0) = New Aurora.Common.Data.Parameter("PkgId", DbType.String, Left(PkgId, 64), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PkgCtyCode", DbType.String, Left(PkgCountries, 10), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, Left(AddUsrId, 10), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("returnValue", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("PackageSetup_InsertDomesticCountries", params)
            retValue = params(3).Value

        Catch ex As Exception
            retValue = "ERROR: Cannot map countries to this package."
        End Try
        Return retValue
    End Function


    Public Function SaveB2CPackageInformation( _
                                            ByVal PkgId As String, _
                                            ByVal PkgWebEnabledFromDate As Date, _
                                            ByVal PkgWebEnabledToDate As Date, _
                                            ByVal PgkINFOurl As String, _
                                            ByVal PkgParentID As String, _
                                            ByVal ModUsrId As String _
                                            ) As String

        Dim retValue As String = ""
        ' Changed by Shoel on 19.5.9 - No more TC Refs in package table, they go in their own table
        ' Changed by Shoel on 21.7.9 - now we have webenabledfrom and to dates!
        Dim params(6) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("PkgId", DbType.String, PkgId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PkgWebEnabledFromDate", DbType.DateTime, IIf(PkgWebEnabledFromDate.Date = DateTime.MinValue, DBNull.Value, PkgWebEnabledFromDate), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("PkgWebEnabledToDate", DbType.DateTime, IIf(PkgWebEnabledToDate.Date = DateTime.MinValue, DBNull.Value, PkgWebEnabledToDate), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("PgkINFOurl", DbType.String, IIf(Left(PgkINFOurl, 256).Trim() = String.Empty, DBNull.Value, Left(PgkINFOurl, 256).Trim()), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("PkgParentID", DbType.String, IIf(Left(PkgParentID, 64).Trim() = String.Empty, DBNull.Value, Left(PkgParentID, 64).Trim()), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, Left(ModUsrId, 10), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("returnValue", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("PackageSetup_UpdatePackageForB2CSetup", params)
            retValue = params(6).Value

        Catch ex As Exception
            retValue = "ERROR/Cannot update B2C Package Information."
        End Try
        Return retValue

    End Function

    Public Function GetB2CInformations(ByVal PkgId As String) As DataSet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("PackageSetup_GetB2CInformations", result, PkgId)
        Return result
    End Function

    Public Sub DeletePackageSetupDomesticCountries(ByVal PkgId As String, _
                                                   ByVal PkgSetupId As Integer, _
                                                   ByVal PkgCtyCode As String, _
                                                   ByVal DeleteAll As Boolean)
        Aurora.Common.Data.ExecuteScalarSP( _
           "PackageSetup_DeletePackageSetupDomesticCountries", _
            PkgId, PkgSetupId, PkgCtyCode, IIf(DeleteAll = False, 0, 1))
    End Sub

    Public Function CheckBeforeDeletingPackageSetupDomesticCountries(ByVal PkgId As String, _
                                                   ByVal PkgSetupId As String) As DataSet

        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("PackageSetup_CheckBeforeDeletingPackageSetupDomesticCountries", result, PkgSetupId, PkgId)
        Return result

    End Function


#Region "Stuff Added by Shoel on 19.5.9"
    Public Function GetPackageTermsAndConditions(ByVal PkgId As String) As DataTable
        'Package_GetTermsAndConditions
        Dim dstResult As DataSet = New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Package_GetTermsAndConditions", dstResult, PkgId)
        If dstResult.Tables.Count > 0 AndAlso dstResult.Tables(0).Rows.Count > 0 Then
            Return dstResult.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Function UpdatePackageTermsAndConditions(ByVal XmlData As String, ByVal PackageId As String, ByVal UserId As String) As String
        Return CStr(Aurora.Common.Data.ExecuteScalarSP("Package_UpdateTermsAndConditions", XmlData, PackageId, UserId))
    End Function

    Public Function DeletePackageTermsAndConditions(ByVal XmlData As String) As String
        Return CStr(Aurora.Common.Data.ExecuteScalarSP("Package_DeleteTermsAndConditions", XmlData))
    End Function

#End Region


    Public Function SaveFlexData(ByVal FlexId As String, ByVal FlexNumber As Int16, ByVal FlexPackageId As String, _
                                 ByVal FlexSapId As String, ByVal FlexEffectiveDate As DateTime, ByVal FlexRate As Double, _
                                 ByVal Complete As Char, ByVal FlexYearStart As String, ByVal IntegrityNo As Int16, _
                                 ByVal AddUserId As String, ByVal ModUserId As String, ByVal AddProgramName As String, _
                                 ByVal Action As String) As System.Xml.XmlDocument

        Dim xmlResult As New System.Xml.XmlDocument
        xmlResult.LoadXml(Aurora.Common.Data.ExecuteSqlXmlSP("flex_update_FlexRates", FlexId, FlexNumber, FlexPackageId, FlexSapId, FlexEffectiveDate, FlexRate, Complete, FlexYearStart, _
        IntegrityNo, AddUserId, ModUserId, AddProgramName, Action))

        '@sFlxId		        varchar(64),    
        '@iFlxNum			int,   
        '@sFlxPkgId			varchar(64),
        '@sFlxSapId			varchar(64),
        '@sFlxEffDate		datetime,
        '@sFlxRate			money,	
        '@bComplete	        varchar(1),
        '@sFlxYearStart		varchar(64),
        '@IntegrityNo        int,	
        '@sAdduserId			varchar(64),   	 
        '@sAddModuserId      varchar(64),    
        '@sAddProgramName  	varchar(64) ,
        '@sAction            varchar(3)   

        Return xmlResult
    End Function


#End Region

#Region "Nimesh Aug 2015 - NonAvailabilityDiscount Profile - Ref-https://thlonline.atlassian.net/browse/PEH-31"
    Public Function DiscountProfile_InsertUpdateNonAvailability(mDisProID As String, mDisProNonAvID As String, mDisProNonAvTravelFrom As DateTime, mDisProNonAvTravelTo As DateTime, mUsrId As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(5) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("DisProID", DbType.Int64, Convert.ToInt64(mDisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProNonAvID", DbType.Int64, Convert.ToInt64(mDisProNonAvID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("DisProNonAvTravelFrom", DbType.DateTime, mDisProNonAvTravelFrom, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("DisProNonAvTravelTo", DbType.DateTime, mDisProNonAvTravelTo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("UsrId", DbType.String, mUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("DiscountProfile_InsertUpdateSingleNonAvailability", params)
            rowcount = params(5).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function

    Public Function DiscountProfile_DeleteNonAvailability(mDisProID As String, mDisProNonAvID As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("DisProID", DbType.Int64, Convert.ToInt64(mDisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProNonAvID", DbType.Int64, Convert.ToInt64(mDisProNonAvID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("DiscountProfile_DeleteSingleNonAvailability", params)
            rowcount = params(2).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function

#End Region
#Region "rev:mia Feb.21 2013 - addition of profile edit agent"


    Public Function Package_InsertPackageProfile(ByVal PkgDisProID As String, ByVal PkgDisProDisProID As String, ByVal PkgDisProPkgId As String, ByVal PkgDisProIsActive As Boolean, ByVal UsrId As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(5) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("PkgDisProID", DbType.Int64, Convert.ToInt64(PkgDisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PkgDisProDisProID", DbType.Int64, Convert.ToInt64(PkgDisProDisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("PkgDisProPkgId", DbType.String, PkgDisProPkgId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("PkgDisProIsActive", DbType.Boolean, PkgDisProIsActive, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("UsrId", DbType.String, UsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            ''rev:mia 02-feb-2016 - procedure name is incorrect courtesy of Nimesh --DiscountProfile_InsertDeleteSingleNonAvailability
            Aurora.Common.Data.ExecuteOutputSP("Package_InsertDeleteSinglePackageProfile", params)
            rowcount = params(5).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function


    Public Function Package_DeletePackageDiscountProfile(ByVal PkgDisProDisProID As Int64, ByVal PkgDisProPkgId As String) As Integer
        Dim rowcount As Integer = 0
        Try
            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("PkgDisProDisProID", DbType.Int64, PkgDisProDisProID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PkgDisProPkgId", DbType.String, PkgDisProPkgId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Package_DeletePackageDiscountProfile", params)
            rowcount = params(2).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function

    ''rev:mia Oct 16 2014 - addition of PromoCode
    Public Function Package_InsertUpdateSingleProfile(
                  ByVal DisProID As Int64, _
                  ByVal DisProCode As String, _
                  ByVal DisProText As String, _
                  ByVal DisProEnableTile As Boolean, _
                  ByVal DisProDescription As String, _
                  ByVal DisProTile As String, _
                  ByVal DisProDiscountCodType As String, _
                  ByVal DisProTravelFrom As DateTime, _
                  ByVal DisProTravelTo As DateTime, _
                  ByVal DisProBookedFromDate As DateTime, _
                  ByVal DisProBookedToDate As DateTime, _
                  ByVal DisProCkoFromDate As DateTime, _
                  ByVal DisProCkoToDate As DateTime, _
                  ByVal DisProMinDays As Int16, _
                  ByVal DisProMaxDays As Int16, _
                  ByVal DisProDiscountPer As Decimal, _
                  ByVal DisProDiscountAmt As Decimal, _
                  ByVal DisProNoOfDaysBeforeTravel As Int16, _
                  ByVal DisProIsActive As Boolean, _
                  ByVal DisProOverridable As Boolean, _
                  ByVal DisProMustTravelBetween As Boolean, _
                  ByVal DisProApplyToOnlyVehicle As Boolean, _
                  ByVal UsrId As String, _
                  ByVal DisProIsPromoCode As Boolean
        ) As Integer
        Dim rowcount As Integer = 0
        Try


            Dim params(24) As Aurora.Common.Data.Parameter

            params(0) = New Aurora.Common.Data.Parameter("DisProID", DbType.Int64, DisProID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProCode", DbType.String, DisProCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("DisProText", DbType.String, DisProText, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("DisProEnableTile", DbType.Boolean, DisProEnableTile, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(4) = New Aurora.Common.Data.Parameter("DisProTile", DbType.String, DisProTile, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("DisProDiscountCodType", DbType.String, DisProDiscountCodType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("DisProTravelFrom", DbType.DateTime, DisProTravelFrom.ToString("dd-MM-yyyy"), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("DisProTravelTo", DbType.DateTime, DisProTravelTo.ToString("dd-MM-yyyy hh:mm:ss"), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(8) = New Aurora.Common.Data.Parameter("DisProBookedFromDate", DbType.DateTime, DisProBookedFromDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("DisProBookedToDate", DbType.DateTime, DisProBookedToDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(10) = New Aurora.Common.Data.Parameter("DisProCkoFromDate", DbType.DateTime, IIf(DisProCkoFromDate.Equals(New Date(1900, 1, 1, 23, 59, 0)), DBNull.Value, DisProCkoFromDate.ToString("dd-MM-yyyy")), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("DisProCkoToDate", DbType.DateTime, IIf(DisProCkoToDate.Equals(New Date(1900, 1, 1, 23, 59, 0)), DBNull.Value, DisProCkoToDate.ToString("dd-MM-yyyy hh:mm:ss")), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(12) = New Aurora.Common.Data.Parameter("DisProMinDays", DbType.Int16, DisProMinDays, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(13) = New Aurora.Common.Data.Parameter("DisProMaxDays", DbType.Int16, DisProMaxDays, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            If (DisProDiscountPer <> 0) Then
                params(14) = New Aurora.Common.Data.Parameter("DisProDiscountPer", DbType.Decimal, DisProDiscountPer, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(15) = New Aurora.Common.Data.Parameter("DisProDiscountAmt", DbType.Decimal, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Else
                params(14) = New Aurora.Common.Data.Parameter("DisProDiscountPer", DbType.Decimal, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(15) = New Aurora.Common.Data.Parameter("DisProDiscountAmt", DbType.Decimal, DisProDiscountAmt, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            End If



            params(16) = New Aurora.Common.Data.Parameter("DisProNoOfDaysBeforeTravel", DbType.Int16, DisProNoOfDaysBeforeTravel, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(17) = New Aurora.Common.Data.Parameter("DisProIsActive", DbType.Boolean, DisProIsActive, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(18) = New Aurora.Common.Data.Parameter("DisProOverridable", DbType.Boolean, DisProOverridable, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(19) = New Aurora.Common.Data.Parameter("DisProMustTravelBetween", DbType.Boolean, DisProMustTravelBetween, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(20) = New Aurora.Common.Data.Parameter("DisProApplyToOnlyVehicle", DbType.Boolean, DisProApplyToOnlyVehicle, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(21) = New Aurora.Common.Data.Parameter("UsrId", DbType.String, UsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(22) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            params(23) = New Aurora.Common.Data.Parameter("DisProDescription", DbType.String, DisProDescription, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            params(24) = New Aurora.Common.Data.Parameter("DisProIsPromoCode", DbType.Boolean, DisProIsPromoCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("Package_InsertUpdateSingleProfile", params)
            rowcount = params(22).Value
        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount
    End Function

    Public Function Package_InsertExcludeAgentProfile(ByVal DisProADisProID As String, ByVal DisProAAgpId As String, ByVal DisProAgnId As String, ByVal DisProAExclude As Boolean, ByVal UsrId As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(5) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("DisProADisProID", DbType.Int64, Convert.ToInt64(DisProADisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProAAgpId", DbType.String, If(String.IsNullOrEmpty(DisProAAgpId), DBNull.Value, DisProAAgpId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("DisProAgnId", DbType.String, DisProAgnId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("DisProAExclude", DbType.Boolean, DisProAExclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("UsrId", DbType.String, UsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Package_InsertExcludeAgentProfile", params)
            rowcount = params(5).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function

    Public Function Package_DeleteAgentProfile(ByVal DisProADisProID As String, ByVal DisProAgnId As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("DisProADisProID", DbType.Int64, Convert.ToInt64(DisProADisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProAgnId", DbType.String, If(String.IsNullOrEmpty(DisProAgnId), DBNull.Value, DisProAgnId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Package_DeleteAgentProfile ", params)
            rowcount = params(2).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function


    Public Function Package_InsertExcludeProductProfile(ByVal DisProPDisProID As String, ByVal DisProPPrdId As String, ByVal DisProPExclude As Boolean, ByVal UsrId As String, ByVal dependencyProductIdsCsv As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(5) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("DisProPDisProID", DbType.Int64, Convert.ToInt64(DisProPDisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProPPrdId", DbType.String, DisProPPrdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("DisProPExclude", DbType.Boolean, DisProPExclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("UsrId", DbType.String, UsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("DependencyProducts", DbType.String, dependencyProductIdsCsv, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Package_InsertExcludeProductProfile", params)
            rowcount = params(5).Value

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function

    Public Function Package_DeleteProductProfile(ByVal DisProPDisProID As String, ByVal DisProPPrdId As String) As Integer
        Dim rowcount As Integer = 0
        Try

            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("DisProPDisProID", DbType.Int64, Convert.ToInt64(DisProPDisProID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("DisProPPrdId", DbType.String, DisProPPrdId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("rowcount", DbType.Int16, rowcount, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Package_DeleteProductProfile", params)
            rowcount = CInt(params(2).Value)

        Catch ex As Exception
            rowcount = -1
        End Try

        Return rowcount

    End Function

    Public Function Package_CopyDiscountProfile(ByVal discountProfileId As Long, ByVal addUserId As String) As Long
        Dim result As Long = 0
        Try

            Dim resultParam As New Parameter("iRetDisProID", DbType.Int64, discountProfileId, Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Copy_DiscountProfile", _
                New Parameter() { _
                New Parameter("iDisProID", DbType.Int64, discountProfileId, Parameter.ParameterType.AddInParameter), _
                New Parameter("sAddUsrId", DbType.String, addUserId, Parameter.ParameterType.AddInParameter), _
                resultParam _
                })

            result = CLng(resultParam.Value)

        Catch ex As Exception
            Return 0
        End Try

        Return result
    End Function

#End Region

End Module



#Region "DiscountProfileSearch"

Public Class DiscountProfileSearchRepository




    ''' <summary>
    ''' Returns collections of values that can be used as criteria for identifying a specific set of discounts.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Overridable Function GetSearchCriteriaData(comCode As String) As DiscountProfileFilters

        Dim codesDataSet As New DiscountCodeDataSet()
        Dim codeTypesDataSet As New DiscountCodeTypeDataSet

        Aurora.Common.Data.ExecuteListSP("GetDiscountProfileFilters", codesDataSet, codeTypesDataSet, New ParamInfo(comCode, "sComCode"))

        Return New DiscountProfileFilters(codesDataSet.ResultSet, codeTypesDataSet.ResultSet)
    End Function

    Public Overridable Function GetSearchResults( _
        ByVal code As String, _
        ByVal codeTypeId As String, _
        ByVal travelFrom As Nullable(Of DateTime), _
        ByVal travelTo As Nullable(Of DateTime), _
        ByVal bookedFrom As Nullable(Of DateTime), _
        ByVal bookedTo As Nullable(Of DateTime), _
        ByVal pickupFrom As Nullable(Of DateTime), _
        ByVal pickupTo As Nullable(Of DateTime), _
        ByVal packageId As String, _
        ByVal agentId As String, _
        ByVal isActive As Nullable(Of Boolean), _
        ByVal sComCode As String) As List(Of DiscountProfile)

        Dim dataSet As New DiscountSearchDataSet()
        Dim productDataSet As New DiscountProductDataSet()

        ''rev:mia Sept 16 2013 - add ComCode
        Aurora.Common.Data.ExecuteListSP("SearchDiscountProfiles", _
                                                dataSet,
                                                productDataSet,
                                                New ParamInfo(code, "code"), _
                                                New ParamInfo(codeTypeId, "codeTypeId"), _
                                                New ParamInfo(travelFrom, "travelFrom"), _
                                                New ParamInfo(travelTo, "travelTo"), _
                                                New ParamInfo(bookedFrom, "bookedFrom"), _
                                                New ParamInfo(bookedTo, "bookedTo"), _
                                                New ParamInfo(pickupFrom, "pickupFrom"), _
                                                New ParamInfo(pickupTo, "pickupTo"), _
                                                New ParamInfo(packageId, "packageId"), _
                                                New ParamInfo(agentId, "agentId"), _
                                                New ParamInfo(isActive, "isActive"), _
                                                New ParamInfo(sComCode, "sComCode")
                                                )

        MapProducts(dataSet.ResultSet, productDataSet.ResultSet)

        Return dataSet.ResultSet
    End Function

    Protected Sub MapProducts(ByVal discounts As List(Of DiscountProfile), ByVal products As List(Of DiscountProduct))

        For Each product As DiscountProduct In products
            Dim discountId As Long = product.DiscountId
            For Each discount As DiscountProfile In discounts
                If discount.DiscountProfileId = discountId Then
                    discount.Products.Add(product)
                End If
            Next
        Next

    End Sub

    ''' <summary>
    ''' Used to populate discounts from data reader.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Class DiscountSearchDataSet
        Inherits ListDataSet(Of DiscountProfile)

        Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As DiscountProfile
            Dim discount As New DiscountProfile()

            discount.DiscountProfileId = GetDbValue(Of Long)(reader, "DisProID")
            discount.Text = GetDbValue(Of String)(reader, "DisProText")

            discount.Code = GetDbValue(Of String)(reader, "DisProCode")
            discount.CodeType = GetDbValue(Of String)(reader, "CodCode")

            discount.TravelFrom = GetDbValue(Of DateTime)(reader, "DisProTravelFrom")
            discount.TravelTo = GetDbValue(Of DateTime)(reader, "DisProTravelTo")

            discount.BookedFrom = GetDbValue(Of DateTime)(reader, "DisProBookedFromDate")
            discount.BookedTo = GetDbValue(Of DateTime)(reader, "DisProBookedToDate")

            discount.IsActive = GetDbValue(Of Boolean)(reader, "DisProIsActive")


            discount.sComCode = GetDbValue(Of String)(reader, "DisProComCode") ''rev:mia Sept 16 2013 - add ComCode
            Return discount
        End Function
    End Class

    Protected Class DiscountProductDataSet
        Inherits ListDataSet(Of DiscountProduct)

        Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As DiscountProduct
            Dim product As New DiscountProduct()

            product.DiscountId = GetDbValue(Of Long)(reader, "DisProPDisProID")
            product.ProductName = GetDbValue(Of String)(reader, "PrdShortName")
            product.Exclude = GetDbValue(Of Boolean)(reader, "DisProPExclude")
            product.sComCode = GetDbValue(Of String)(reader, "DisProComCode") ''rev:mia Sept 16 2013 - add ComCode
            Return product
        End Function
    End Class

    ''' <summary>
    ''' Used to read codes from data reader.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Class DiscountCodeDataSet
        Inherits ListDataSet(Of String)

        Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As String
            Return GetDbValue(Of String)(reader("DisProCode"))
        End Function

    End Class

    ''' <summary>
    ''' Used to read codes from data reader.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Class DiscountCodeTypeDataSet
        Inherits ListDataSet(Of DiscountProfileCodeType)

        Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As DiscountProfileCodeType
            Return New DiscountProfileCodeType( _
                                        GetDbValue(Of String)(reader("CodId")), _
                                        GetDbValue(Of String)(reader("CodCode")) _
                                        )
        End Function

    End Class
End Class

#End Region
