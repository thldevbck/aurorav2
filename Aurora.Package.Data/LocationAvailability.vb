Partial Public Class PackageDataSet
    Partial Public Class LocationAvailabilityRow

        Public Property IsNotAvailable() As Boolean
            Get
                Return Not Me.IsLcaIsNotAvailableNull AndAlso Me.LcaIsNotAvailable
            End Get
            Set(ByVal value As Boolean)
                Me.LcaIsNotAvailable = value
            End Set
        End Property

    End Class
End Class