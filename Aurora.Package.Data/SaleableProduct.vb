Imports Aurora.Package.Data

Partial Public Class PackageDataSet
    Partial Class SaleableProductRow

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Me.SapStatus = "Active"
            End Get
        End Property

        Public ReadOnly Property IsPending() As Boolean
            Get
                Return Me.SapStatus = "Pending"
            End Get
        End Property

        Public ReadOnly Property IsInactive() As Boolean
            Get
                Return Not Me.IsActive AndAlso Not Me.IsPending
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.ProductRow.PrdShortName & "-" & Me.SapSuffix.ToString()
            End Get
        End Property

        Public ReadOnly Property HasFuture(ByVal currentDate As Date)
            Get
                If IsActive Then
                    For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows()
                        If packageProductRow.PackageRow IsNot Nothing _
                         AndAlso packageProductRow.PackageRow.IsActive _
                         AndAlso packageProductRow.PackageRow.IsFuture(currentDate) Then
                            Return True
                        End If
                    Next
                End If

                Return False
            End Get
        End Property

        Public ReadOnly Property HasCurrent(ByVal currentDate As Date)
            Get
                If IsActive Then
                    If Me.GetPackageProductRows().Length = 0 Then Return True

                    For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows()
                        If packageProductRow.PackageRow IsNot Nothing _
                         AndAlso packageProductRow.PackageRow.IsActive _
                         AndAlso packageProductRow.PackageRow.IsCurrent(currentDate) Then
                            Return True
                        End If
                    Next
                End If

                Return False
            End Get
        End Property

        Public ReadOnly Property HasPast(ByVal currentDate As Date)
            Get
                If IsActive Then
                    For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows()
                        If packageProductRow.PackageRow IsNot Nothing _
                         AndAlso packageProductRow.PackageRow.IsActive _
                         AndAlso packageProductRow.PackageRow.IsPast(currentDate) Then
                            Return True
                        End If
                    Next
                End If

                Return False
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If IsActive AndAlso HasCurrent(currentDate) Then
                    Return PackageConstants.CurrentColor
                ElseIf IsActive AndAlso HasFuture(currentDate) Then
                    Return PackageConstants.FutureColor
                ElseIf IsActive AndAlso HasPast(currentDate) Then
                    Return PackageConstants.PastColor
                ElseIf IsActive Then
                    Return PackageConstants.ActiveColor
                Else
                    Return PackageConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property StatusText(ByVal currentDate As Date) As String
            Get
                If IsActive AndAlso HasCurrent(currentDate) Then
                    Return "Active / Current"
                ElseIf IsActive AndAlso HasFuture(currentDate) Then
                    Return "Active / Future"
                ElseIf IsActive AndAlso HasPast(currentDate) Then
                    Return "Active / Past"
                ElseIf IsActive Then
                    Return "Active"
                ElseIf IsPending Then
                    Return "Pending"
                Else
                    Return "Inactive"
                End If
            End Get
        End Property


    End Class

End Class
