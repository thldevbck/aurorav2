Partial Public Class PackageDataSet
    Partial Class AgentRow

        Public ReadOnly Property Code() As String
            Get
                If Not Me.IsAgnCodeNull Then Return Me.AgnCode Else Return ""
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsAgnNameNull Then Return Me.AgnName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.Code & " - " & Me.Name
            End Get
        End Property

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Not Me.IsAgnIsActiveNull AndAlso Me.AgnIsActive
            End Get
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If Me.IsActive Then
                    Return PackageConstants.CurrentColor
                Else
                    Return PackageConstants.InactiveColor
                End If
            End Get
        End Property
    End Class
End Class
