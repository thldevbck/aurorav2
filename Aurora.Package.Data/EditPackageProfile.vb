﻿Imports System.Data
Imports Aurora.Common
Imports Aurora.Package.Data.DataRepository

Public Class EditPackageProfile

    Private _EditPackageProfileDataset As DataSet

    Sub New(ByVal DisProID As String)
        LoadDataSet(DisProID, Nothing, Nothing, 0)
    End Sub

    Sub New(ByVal DisProID As String, ByVal selectedItems As String, ByVal productsOnly As Boolean, ByVal selectedItemsOffset As Integer)
        LoadDataSet(DisProID, selectedItems, productsOnly, selectedItemsOffset)
    End Sub

    Sub New()

    End Sub

    Public ReadOnly Property DiscountProfilesDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfiles")
        End Get
    End Property

    Public ReadOnly Property PackageDiscountProfilesDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("PackageDiscountProfiles")
        End Get
    End Property

    Public ReadOnly Property DiscountProfilesProductNotLinkDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfilesProductNotLink")
        End Get
    End Property

    Public ReadOnly Property DiscountProfilesProductLinkDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfilesProductLink")
        End Get
    End Property

    Public ReadOnly Property DiscountProfilesAgentNotLinkDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfilesAgentNotLink")
        End Get
    End Property

    Public ReadOnly Property DiscountProfilesNonAvailabilityTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfilesNonAvailability")
        End Get
    End Property

    Public ReadOnly Property DiscountProfilesAgentLinkDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfilesAgentLink")
        End Get
    End Property

    Public ReadOnly Property CodeDataTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("Codes")
        End Get
    End Property

    Public ReadOnly Property ProductDependenciesTable As DataTable
        Get
            Return _EditPackageProfileDataset.Tables("DiscountProfilesProductDependencies")
        End Get
    End Property


    Private Sub LoadDataSet(ByVal DisProID As String, ByVal selectedItems As String, ByVal productsOnly As Boolean, ByVal selectedItemsOffset As Integer)
        _EditPackageProfileDataset = New DataSet()
        If (String.IsNullOrEmpty(DisProID)) Then
            DisProID = -1
        End If
        Aurora.Common.Data.ExecuteDataSetSP("Package_GetSingleProfile", _EditPackageProfileDataset, Convert.ToInt32(DisProID), selectedItems, productsOnly, selectedItemsOffset)
        _EditPackageProfileDataset.Tables(0).TableName = "DiscountProfilesProductNotLink"
        _EditPackageProfileDataset.Tables(1).TableName = "DiscountProfilesProductLink"
        _EditPackageProfileDataset.Tables(2).TableName = "DiscountProfilesProductDependencies"

        If Not productsOnly Then
            _EditPackageProfileDataset.Tables(3).TableName = "DiscountProfiles"
            _EditPackageProfileDataset.Tables(4).TableName = "PackageDiscountProfiles"
            _EditPackageProfileDataset.Tables(5).TableName = "Codes"
            _EditPackageProfileDataset.Tables(6).TableName = "DiscountProfilesAgentNotLink"
            _EditPackageProfileDataset.Tables(7).TableName = "DiscountProfilesAgentLink"
            ''   _EditPackageProfileDataset.Tables(8).TableName = "DiscountProfilesNonAvailability"
        End If
    End Sub

    Public Function PackageInsertUpdateSingleNonAvailableDiscountProfile(dataTable As DataTable) As Integer
        Dim rowCount As Integer = 0

        Try

            Dim DisProID As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProID"))) Then
                DisProID = dataTable.Rows(0).Item("DisProID").ToString.Trim
            End If

            Dim discountcodeTextBox As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProCode"))) Then
                discountcodeTextBox = dataTable.Rows(0).Item("DisProCode").ToString.Trim
            End If

            Dim discounttextTextBox As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProText"))) Then
                discounttextTextBox = dataTable.Rows(0).Item("DisProText").ToString.Trim
            End If

            Dim chkEnablePromoTile As Boolean = False
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProEnableTile"))) Then
                chkEnablePromoTile = dataTable.Rows(0).Item("DisProEnableTile").ToString.Trim
            End If

            Dim discountDescriptionTextBox As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDescription"))) Then
                discountDescriptionTextBox = dataTable.Rows(0).Item("DisProDescription").ToString.Trim
            End If

            Dim txtPromoTileText As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProTile"))) Then
                txtPromoTileText = dataTable.Rows(0).Item("DisProTile").ToString.Trim
            End If

            Dim typeDropDown As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDiscountCodType"))) Then
                typeDropDown = dataTable.Rows(0).Item("DisProDiscountCodType").ToString.Trim
            End If


            Dim travelFromTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProTravelFrom"))) Then
                travelFromTextBox = DateWithOut2359(dataTable.Rows(0).Item("DisProTravelFrom").ToString.Trim)
            End If


            Dim travelToTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProTravelTo"))) Then
                travelToTextBox = DateWith2359(dataTable.Rows(0).Item("DisProTravelTo").ToString.Trim)
            End If


            Dim bookedFromTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProBookedFromDate"))) Then
                bookedFromTextBox = DirectCast(dataTable.Rows(0).Item("DisProBookedFromDate"), DateTime)
            End If

            Dim bookedToTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProBookedToDate"))) Then
                bookedToTextBox = DirectCast(dataTable.Rows(0).Item("DisProBookedToDate"), DateTime)
            End If


            Dim pickupFromTextBox As DateTime = New DateTime(1900, 1, 1, 23, 59, 0)
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProCkoFromDate"))) Then
                pickupFromTextBox = dataTable.Rows(0).Item("DisProCkoFromDate").ToString.Trim
                If (pickupFromTextBox.Equals(DateTime.MinValue)) Then
                    pickupFromTextBox = New DateTime(1900, 1, 1, 23, 59, 0)
                End If
            End If

            Dim pickupToTextBox As DateTime = New DateTime(1900, 1, 1, 23, 59, 0)
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProCkoToDate"))) Then
                pickupToTextBox = dataTable.Rows(0).Item("DisProCkoToDate").ToString.Trim
                If (pickupToTextBox.Equals(DateTime.MinValue)) Then
                    pickupToTextBox = New DateTime(1900, 1, 1, 23, 59, 0)
                Else
                    pickupToTextBox = DateWith2359(pickupToTextBox)
                End If
            End If


            Dim DisProMustTravelBetweenCheckbox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProMustTravelBetween").ToString)
            Dim DisProOverridableCheckBox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProOverridable").ToString)
            Dim DisProIsActiveCheckBox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProIsActive").ToString)
            Dim DisProApplyToOnlyVehicleCheckBox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProApplyToOnlyVehicle").ToString)

            ''rev:mia April 30,2014 - changes based on this https://www.pivotaltracker.com/story/show/69596394
            Dim DisProDiscountPer As Decimal
            Dim DisProDiscountAmt As Decimal
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDiscountPer"))) Then
                DisProDiscountPer = dataTable.Rows(0).Item("DisProDiscountPer").ToString.Trim
                DisProDiscountAmt = 0
            End If


            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDiscountAmt"))) Then
                DisProDiscountAmt = dataTable.Rows(0).Item("DisProDiscountAmt").ToString.Trim
                DisProDiscountPer = 0
            End If

            Dim UsrId As String = dataTable.Rows(0).Item("AddUsrId")

            ''rev:mia Oct 16 2014 - addition of PromoCode
            Dim ispromocode As Boolean = True
            If (dataTable.Rows(0).Item("DisProIsPromoCode") Is Nothing) Then
                ispromocode = False
            Else
                ispromocode = Convert.ToBoolean(dataTable.Rows(0).Item("DisProIsPromoCode"))
            End If


            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

                Try

                    rowCount = Package_InsertUpdateSingleProfile(IIf(String.IsNullOrEmpty(DisProID), -1, Convert.ToInt32(DisProID)), _
                                               discountcodeTextBox, _
                                               discounttextTextBox, _
                                               chkEnablePromoTile, _
                                               discountDescriptionTextBox, _
                                               txtPromoTileText, _
                                               typeDropDown, _
                                               travelFromTextBox, _
                                               travelToTextBox, _
                                               bookedFromTextBox, _
                                               bookedToTextBox, _
                                               pickupFromTextBox, _
                                               pickupToTextBox, _
                                               0, _
                                               0, _
                                               DisProDiscountPer, _
                                               DisProDiscountAmt, _
                                               0, _
                                               DisProIsActiveCheckBox, _
                                               DisProOverridableCheckBox, _
                                               DisProMustTravelBetweenCheckbox, _
                                               DisProApplyToOnlyVehicleCheckBox, _
                                               UsrId, _
                                               ispromocode)

                    ''oTransaction.RollbackTransaction()
                    oTransaction.CommitTransaction()
                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    oTransaction.Dispose()
                    rowCount = -1
                End Try
            End Using

        Catch ex As Exception

        End Try

        Return rowCount
    End Function


    Public Function PackageInsertUpdateSingleProfile(dataTable As DataTable) As Integer
        Dim rowCount As Integer = 0

        Try

            Dim DisProID As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProID"))) Then
                DisProID = dataTable.Rows(0).Item("DisProID").ToString.Trim
            End If

            Dim discountcodeTextBox As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProCode"))) Then
                discountcodeTextBox = dataTable.Rows(0).Item("DisProCode").ToString.Trim
            End If

            Dim discounttextTextBox As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProText"))) Then
                discounttextTextBox = dataTable.Rows(0).Item("DisProText").ToString.Trim
            End If

            Dim chkEnablePromoTile As Boolean = False
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProEnableTile"))) Then
                chkEnablePromoTile = dataTable.Rows(0).Item("DisProEnableTile").ToString.Trim
            End If

            Dim discountDescriptionTextBox As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDescription"))) Then
                discountDescriptionTextBox = dataTable.Rows(0).Item("DisProDescription").ToString.Trim
            End If

            Dim txtPromoTileText As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProTile"))) Then
                txtPromoTileText = dataTable.Rows(0).Item("DisProTile").ToString ''.Trim
            End If

            Dim typeDropDown As String = String.Empty
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDiscountCodType"))) Then
                typeDropDown = dataTable.Rows(0).Item("DisProDiscountCodType").ToString.Trim
            End If


            Dim travelFromTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProTravelFrom"))) Then
                travelFromTextBox = DateWithOut2359(dataTable.Rows(0).Item("DisProTravelFrom").ToString.Trim)
            End If


            Dim travelToTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProTravelTo"))) Then
                travelToTextBox = DateWith2359(dataTable.Rows(0).Item("DisProTravelTo").ToString.Trim)
            End If


            Dim bookedFromTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProBookedFromDate"))) Then
                bookedFromTextBox = DirectCast(dataTable.Rows(0).Item("DisProBookedFromDate"), DateTime)
            End If

            Dim bookedToTextBox As DateTime = DateTime.MinValue
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProBookedToDate"))) Then
                bookedToTextBox = DirectCast(dataTable.Rows(0).Item("DisProBookedToDate"), DateTime)
            End If


            Dim pickupFromTextBox As DateTime = New DateTime(1900, 1, 1, 23, 59, 0)
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProCkoFromDate"))) Then
                pickupFromTextBox = dataTable.Rows(0).Item("DisProCkoFromDate").ToString.Trim
                If (pickupFromTextBox.Equals(DateTime.MinValue)) Then
                    pickupFromTextBox = New DateTime(1900, 1, 1, 23, 59, 0)
                End If
            End If

            Dim pickupToTextBox As DateTime = New DateTime(1900, 1, 1, 23, 59, 0)
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProCkoToDate"))) Then
                pickupToTextBox = dataTable.Rows(0).Item("DisProCkoToDate").ToString.Trim
                If (pickupToTextBox.Equals(DateTime.MinValue)) Then
                    pickupToTextBox = New DateTime(1900, 1, 1, 23, 59, 0)
                Else
                    pickupToTextBox = DateWith2359(pickupToTextBox)
                End If
            End If


            Dim DisProMustTravelBetweenCheckbox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProMustTravelBetween").ToString)
            Dim DisProOverridableCheckBox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProOverridable").ToString)
            Dim DisProIsActiveCheckBox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProIsActive").ToString)
            Dim DisProApplyToOnlyVehicleCheckBox As Boolean = Convert.ToBoolean(dataTable.Rows(0).Item("DisProApplyToOnlyVehicle").ToString)

            ''rev:mia April 30,2014 - changes based on this https://www.pivotaltracker.com/story/show/69596394
            Dim DisProDiscountPer As Decimal
            Dim DisProDiscountAmt As Decimal
            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDiscountPer"))) Then
                DisProDiscountPer = dataTable.Rows(0).Item("DisProDiscountPer").ToString.Trim
                DisProDiscountAmt = 0
            End If


            If (Not IsDBNull(dataTable.Rows(0).Item("DisProDiscountAmt"))) Then
                DisProDiscountAmt = dataTable.Rows(0).Item("DisProDiscountAmt").ToString.Trim
                DisProDiscountPer = 0
            End If

            Dim UsrId As String = dataTable.Rows(0).Item("AddUsrId")

            ''rev:mia Oct 16 2014 - addition of PromoCode
            Dim ispromocode As Boolean = True
            If (dataTable.Rows(0).Item("DisProIsPromoCode") Is Nothing) Then
                ispromocode = False
            Else
                ispromocode = Convert.ToBoolean(dataTable.Rows(0).Item("DisProIsPromoCode"))
            End If


            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

                Try

                    rowCount = Package_InsertUpdateSingleProfile(IIf(String.IsNullOrEmpty(DisProID), -1, Convert.ToInt32(DisProID)), _
                                               discountcodeTextBox, _
                                               discounttextTextBox, _
                                               chkEnablePromoTile, _
                                               discountDescriptionTextBox, _
                                               txtPromoTileText, _
                                               typeDropDown, _
                                               travelFromTextBox, _
                                               travelToTextBox, _
                                               bookedFromTextBox, _
                                               bookedToTextBox, _
                                               pickupFromTextBox, _
                                               pickupToTextBox, _
                                               0, _
                                               0, _
                                               DisProDiscountPer, _
                                               DisProDiscountAmt, _
                                               0, _
                                               DisProIsActiveCheckBox, _
                                               DisProOverridableCheckBox, _
                                               DisProMustTravelBetweenCheckbox, _
                                               DisProApplyToOnlyVehicleCheckBox, _
                                               UsrId, _
                                               ispromocode)

                    ''oTransaction.RollbackTransaction()
                    oTransaction.CommitTransaction()
                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    oTransaction.Dispose()
                    rowCount = -1
                End Try
            End Using

        Catch ex As Exception

        End Try

        Return rowCount
    End Function

    Public Function PackageDeletePackageDiscountProfile(PkgDisProDisProID As Int64, PkgDisProPkgId As String) As Integer
        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = Package_DeletePackageDiscountProfile(PkgDisProDisProID, PkgDisProPkgId)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount

    End Function

    Public Function PackageInsertSinglePackageProfile(PkgDisProID As String, PkgDisProDisProID As String, PkgDisProPkgId As String, PkgDisProIsActive As Boolean, UsrId As String) As Integer
        If (String.IsNullOrEmpty(PkgDisProID)) Then
            PkgDisProID = -1
        End If

        If (String.IsNullOrEmpty(PkgDisProDisProID)) Then
            Return -1
        End If

        If (String.IsNullOrEmpty(PkgDisProPkgId)) Then
            Return -1
        End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = Package_InsertPackageProfile(PkgDisProID, PkgDisProDisProID, PkgDisProPkgId, 1, UsrId)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount

    End Function

    ' Added by Nimesh on 7th Aug 2015 - Ref https://thlonline.atlassian.net/browse/PEH-31
    Public Function InsertSingleDiscountProfileNonAvailability(mDisProID As String, mDisProNonAvID As String, mDisProNonAvTravelFrom As DateTime, mDisProNonAvTravelTo As DateTime, mUsrId As String) As Integer
        If (String.IsNullOrEmpty(mDisProID)) Then
            mDisProID = -1
        End If

        If (String.IsNullOrEmpty(mDisProNonAvID)) Then
            mDisProNonAvID = -1
        End If

        If (String.IsNullOrEmpty(mDisProNonAvTravelFrom)) Then
            Return -1
        End If

        If (String.IsNullOrEmpty(mDisProNonAvTravelTo)) Then
            Return -1
        End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                rowCount = DiscountProfile_InsertUpdateNonAvailability(mDisProID, mDisProNonAvID, mDisProNonAvTravelFrom, mDisProNonAvTravelTo, mUsrId)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount

    End Function

    Public Function DeleteSingleDiscountProfileNonAvailability(mDisProID As String, mDisProNonAvID As String) As Integer
        If (String.IsNullOrEmpty(mDisProID)) Then
            mDisProID = -1
        End If

        If (String.IsNullOrEmpty(mDisProNonAvID)) Then
            Return -1
        End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = DiscountProfile_DeleteNonAvailability(mDisProID, mDisProNonAvID)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount

    End Function

    ' End Added by Nimesh on 7th Aug 2015 - Ref https://thlonline.atlassian.net/browse/PEH-31
    Public Function PackageInsertExcludeProductProfile(ByVal DisProPDisProID As String, ByVal DisProPPrdId As String, ByVal DisProPExclude As Boolean, ByVal UsrId As String, ByVal dependencyProductIdsCsv As String) As Integer
        If (String.IsNullOrEmpty(DisProPDisProID)) Then
            DisProPDisProID = -1
        End If

        If (String.IsNullOrEmpty(DisProPPrdId)) Then
            Return -1
        End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = Package_InsertExcludeProductProfile(DisProPDisProID, DisProPPrdId, DisProPExclude, UsrId, dependencyProductIdsCsv)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount

    End Function

    Public Function PackageInsertExcludeAgentProfile(DisProADisProID As String, DisProAAgpId As String, DisProAgnId As String, DisProAExclude As Boolean, UsrId As String) As Integer
        If (String.IsNullOrEmpty(DisProADisProID)) Then
            DisProADisProID = -1
        End If

        If (String.IsNullOrEmpty(DisProAgnId)) Then
            Return -1
        End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = Package_InsertExcludeAgentProfile(DisProADisProID, DisProAAgpId, DisProAgnId, DisProAExclude, UsrId)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount

    End Function


    Public Function PackageDeleteAgentProfile(DisProADisProID As String, DisProAgnId As String) As Integer
        If (String.IsNullOrEmpty(DisProADisProID)) Then
            DisProADisProID = -1
        End If

        'If (String.IsNullOrEmpty(DisProAgnId)) Then
        '    Return -1
        'End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = Package_DeleteAgentProfile(DisProADisProID, DisProAgnId)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount
    End Function

    Public Function PackageDeleteProductProfile(DisProPDisProID As String, DisProPPrdId As String) As Integer

        If (String.IsNullOrEmpty(DisProPDisProID)) Then
            DisProPDisProID = -1
        End If

        'If (String.IsNullOrEmpty(DisProPPrdId)) Then
        '    Return -1
        'End If

        Dim rowCount As Integer = 0
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                rowCount = Package_DeleteProductProfile(DisProPDisProID, DisProPPrdId)
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                rowCount = -1
            End Try
        End Using

        Return rowCount
    End Function

    Public Function CopyDiscountProfile(ByVal discountProfileId As Integer, ByVal addUserId As String)
        Return Package_CopyDiscountProfile(discountProfileId, addUserId)
    End Function

    Private Function DateWithOut2359(ByVal input As DateTime) As DateTime
        Return New DateTime(input.Year, input.Month, input.Day, 0, 0, 0)
    End Function
End Class
