
Partial Public Class PackageDataSet
    Partial Class LocationRow

        Public ReadOnly Property Code() As String
            Get
                Return Me.LocCode
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsLocNameNull Then Return Me.LocName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.Code + " - " & Me.Name
            End Get
        End Property

    End Class

End Class
