Partial Public Class PackageDataSet
    Partial Public Class FlexLevelRow

        Public ReadOnly Property Description() As String
            Get
                Return Me.FlxEffDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) & " - " & Me.FlxTravelYrStart.ToString("yyyy") & " - " & Me.AddUsrId
            End Get
        End Property

    End Class
End Class
