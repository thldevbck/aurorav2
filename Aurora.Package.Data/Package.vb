Partial Public Class PackageDataSet
    Partial Class PackageRow

        Public ReadOnly Property PackageDataSet() As PackageDataSet
            Get
                Return CType(Me.Table.DataSet, PackageDataSet)
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsPkgNameNull Then Return Me.PkgName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.PkgCode & " - " & Me.Name
            End Get
        End Property

        Public ReadOnly Property BrandDescription() As String
            Get
                If Me.BrandRow IsNot Nothing Then Return Me.BrandRow.Description Else Return ""
            End Get
        End Property

        Public ReadOnly Property TypeDescription() As String
            Get
                If Not Me.IsPkgCodTypIdNull AndAlso Me.PackageDataSet.Code.FindById(Me.PkgCodTypId) IsNot Nothing Then
                    Dim codeRow As CodeRow = Me.PackageDataSet.Code.FindById(Me.PkgCodTypId)
                    Return codeRow.CodDesc
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property CountryDescription() As String
            Get
                If Me.CountryRow IsNot Nothing Then Return Me.CountryRow.Description Else Return ""
            End Get
        End Property

        Public ReadOnly Property BookedDescription() As String
            Get
                Dim result As String = ""
                If Not IsPkgBookedFromDateNull() Then result &= Me.PkgBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsPkgBookedToDateNull() Then result &= Me.PkgBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property TravelDescription() As String
            Get
                Dim result As String = ""
                If Not IsPkgTravelFromDateNull() Then result &= Me.PkgTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsPkgTravelToDateNull() Then result &= Me.PkgTravelToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property StatusDescription() As String
            Get
                Return Me.PkgIsActive
            End Get
        End Property

        Public ReadOnly Property BookedFromDate() As Date
            Get
                If Not IsPkgBookedFromDateNull() Then Return Me.PkgBookedFromDate Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property BookedToDate() As Date
            Get
                If Not IsPkgBookedToDateNull() Then Return Me.PkgBookedToDate Else Return Date.MaxValue
            End Get
        End Property

        Public ReadOnly Property TravelFromDate() As Date
            Get
                If Not IsPkgTravelFromDateNull() Then Return Me.PkgTravelFromDate Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property TravelToDate() As Date
            Get
                If Not IsPkgTravelToDateNull() Then Return Me.PkgTravelToDate Else Return Date.MaxValue
            End Get
        End Property

        Public ReadOnly Property IsPast(ByVal currentDate As Date) As Boolean
            Get
                'Return Not IsCurrent(currentDate) And Not IsFuture(currentDate)
                If currentDate > BookedToDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property


        Public ReadOnly Property IsCurrent(ByVal currentDate As Date) As Boolean
            Get
                'Return currentDate >= BookedFromDate AndAlso currentDate <= BookedToDate _
                ' AndAlso currentDate >= TravelFromDate AndAlso currentDate <= TravelToDate
                If currentDate <= BookedToDate And currentDate >= BookedFromDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsFuture(ByVal currentDate As Date) As Boolean
            Get
                'Return currentDate < BookedFromDate _
                ' AndAlso currentDate < TravelFromDate
                If currentDate < BookedFromDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Me.PkgIsActive = "Active"
            End Get
        End Property

        Public ReadOnly Property IsPending() As Boolean
            Get
                Return Me.PkgIsActive = "Pending"
            End Get
        End Property

        Public ReadOnly Property IsInactive() As Boolean
            Get
                Return Not Me.IsActive AndAlso Not Me.IsPending
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If Me.IsActive AndAlso Me.IsFuture(currentDate) Then
                    Return PackageConstants.FutureColor
                ElseIf Me.IsActive AndAlso Me.IsCurrent(currentDate) Then
                    Return PackageConstants.CurrentColor
                ElseIf Me.IsActive AndAlso Me.IsPast(currentDate) Then
                    Return PackageConstants.PastColor
                Else
                    Return PackageConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property StatusText(ByVal currentDate As Date) As String
            Get
                If Me.IsActive AndAlso Me.IsFuture(currentDate) Then
                    Return "Active / Future"
                ElseIf Me.IsActive AndAlso Me.IsCurrent(currentDate) Then
                    Return "Active / Current"
                ElseIf Me.IsActive AndAlso Me.IsPast(currentDate) Then
                    Return "Active / Past"
                Else
                    Return "Inactive"
                End If
            End Get
        End Property

        Public Property DepositPercentage() As Double
            Get
                If Not Me.IsPkgDepositPercentageNull Then Return Me.PkgDepositPercentage Else Return 0
            End Get
            Set(ByVal value As Double)
                Me.PkgDepositPercentage = value
            End Set
        End Property

        Public Property DepositAmount() As Double
            Get
                If Not Me.IsPkgDepositAmtNull Then Return Me.PkgDepositAmt Else Return 0
            End Get
            Set(ByVal value As Double)
                Me.PkgDepositAmt = value
            End Set
        End Property

        Public Property LumpSumDesc() As String
            Get
                If Not Me.IsPkgLumpSumDescNull Then Return Me.PkgLumpSumDesc Else Return ""
            End Get
            Set(ByVal value As String)
                Me.PkgLumpSumDesc = value
            End Set
        End Property

        Public Property SortB4Veh() As Boolean
            Get
                If Not Me.IsPkgSortB4VehNull Then Return Me.PkgSortB4Veh Else Return False
            End Get
            Set(ByVal value As Boolean)
                Me.PkgSortB4Veh = value
            End Set
        End Property

        Public Property OvrAgnDiscount() As Double
            Get
                ''If Not Me.IsPkgOvrAgnDiscountNull Then Return Me.PkgOvrAgnDiscount Else Return 0
                ''rev:mia jan 28 just return..dont convert to zero if null
                If Not Me.IsPkgOvrAgnDiscountNull Then Return Me.PkgOvrAgnDiscount Else Return CDec(9999999999)
            End Get
            Set(ByVal value As Double)
                Me.PkgOvrAgnDiscount = value
            End Set
        End Property

        Public Property AddAgnDiscount() As Double
            Get
                ''If Not Me.IsPkgAddAgnDiscountNull Then Return Me.PkgAddAgnDiscount Else Return 0
                ''rev:mia jan 28 just return..dont convert to zero if null
                If Not Me.IsPkgAddAgnDiscountNull Then Return Me.PkgAddAgnDiscount Else Return CDec(9999999999)
            End Get
            Set(ByVal value As Double)
                Me.PkgAddAgnDiscount = value
            End Set
        End Property

        ''' <summary>
        ''' Get all the unique flex level rows by effective date for this package.  Use the flex levels with the smallest create dates
        ''' </summary>
        Public ReadOnly Property UniqueFlexLevelSets() As FlexLevelRow()
            Get
                Dim effectiveDates As New List(Of Date)
                For Each flexLevelRow As FlexLevelRow In Me.GetFlexLevelRows()
                    If Not flexLevelRow.IsFlxEffDateNull AndAlso Not effectiveDates.Contains(flexLevelRow.FlxEffDate) Then
                        effectiveDates.Add(flexLevelRow.FlxEffDate)
                    End If
                Next
                effectiveDates.Sort()
                effectiveDates.Reverse()

                Dim result As New List(Of FlexLevelRow)
                For Each effectiveDate As Date In effectiveDates
                    Dim flexLevelRowFound As FlexLevelRow = Nothing
                    For Each flexLevelRow As FlexLevelRow In Me.GetFlexLevelRows()
                        If (Not flexLevelRow.IsFlxEffDateNull AndAlso flexLevelRow.FlxEffDate = effectiveDate) _
                         AndAlso (flexLevelRowFound Is Nothing OrElse flexLevelRowFound.AddDateTime > flexLevelRow.AddDateTime) Then
                            flexLevelRowFound = flexLevelRow
                        End If
                    Next
                    If flexLevelRowFound IsNot Nothing Then result.Add(flexLevelRowFound)
                Next

                Return result.ToArray()
            End Get
        End Property


    End Class
End Class





#Region "DiscountProfile"

''' <summary>
''' Contains collections of values that can be used as criteria for identifying a specific set of discounts.
''' </summary>
''' <remarks></remarks>
Public Class DiscountProfileFilters
    Private mCodes As List(Of String)
    Private mCodeTypes As List(Of DiscountProfileCodeType)

    Public Property Codes As List(Of String)
        Get
            Return mCodes
        End Get
        Set(ByVal value As List(Of String))
            mCodes = value
        End Set
    End Property

    Public Property CodeTypes As List(Of DiscountProfileCodeType)
        Get
            Return mCodeTypes
        End Get
        Set(ByVal value As List(Of DiscountProfileCodeType))
            mCodeTypes = value
        End Set
    End Property

    Public Sub New(ByVal codes As List(Of String), ByVal codeTypes As List(Of DiscountProfileCodeType))
        If codes Is Nothing Then
            Throw New ArgumentNullException("codes")
        End If

        If codeTypes Is Nothing Then
            Throw New ArgumentNullException("codeTypes")
        End If

        mCodes = codes
        mCodeTypes = codeTypes
    End Sub
End Class

''' <summary>
''' Contains discount profile data.
''' </summary>
''' <remarks></remarks>
Public Class DiscountProfile

    Private mDiscountProfileId As Long
    Private mText As String
    Private mCode As String
    Private mCodeType As String

    Private mTravelFrom As DateTime
    Private mTravelTo As DateTime

    Private mBookedFrom As DateTime
    Private mBookedTo As DateTime

    Private mIsActive As Boolean

    Private mProducts As List(Of DiscountProduct)

    Public Property DiscountProfileId As Long
        Get
            Return mDiscountProfileId
        End Get
        Set(ByVal value As Long)
            mDiscountProfileId = value
        End Set
    End Property

    Public Property Text As String
        Get
            Return mText
        End Get
        Set(ByVal value As String)
            mText = value
        End Set
    End Property

    Public Property Code As String
        Get
            Return mCode
        End Get
        Set(ByVal value As String)
            mCode = value
        End Set
    End Property

    Public Property CodeType As String
        Get
            Return mCodeType
        End Get
        Set(ByVal value As String)
            mCodeType = value
        End Set
    End Property

    Public Property TravelFrom As DateTime
        Get
            Return mTravelFrom
        End Get
        Set(ByVal value As DateTime)
            mTravelFrom = value
        End Set
    End Property

    Public Property TravelTo As DateTime
        Get
            Return mTravelTo
        End Get
        Set(ByVal value As DateTime)
            mTravelTo = value
        End Set
    End Property

    Public Property BookedFrom As DateTime
        Get
            Return mBookedFrom
        End Get
        Set(ByVal value As DateTime)
            mBookedFrom = value
        End Set
    End Property

    Public Property BookedTo As DateTime
        Get
            Return mBookedTo
        End Get
        Set(ByVal value As DateTime)
            mBookedTo = value
        End Set
    End Property

    Public Property IsActive As Boolean
        Get
            Return mIsActive
        End Get
        Set(ByVal value As Boolean)
            mIsActive = value
        End Set
    End Property

    Public Property Products As List(Of DiscountProduct)
        Get
            If mProducts Is Nothing Then
                mProducts = New List(Of DiscountProduct)
            End If

            Return mProducts
        End Get
        Set(ByVal value As List(Of DiscountProduct))
            mProducts = value
        End Set
    End Property

    Public ReadOnly Property AreProductsExcluded As Boolean
        Get
            If mProducts Is Nothing OrElse mProducts.Count = 0 Then
                Return False
            End If

            Return mProducts(0).Exclude
        End Get
    End Property

    ''rev:mia Sept 16 2013 - add ComCode
    Private mComCode As String
    Public Property sComCode As String
        Get
            Return mComCode
        End Get
        Set(value As String)
            mComCode = value
        End Set
    End Property

End Class

Public Class DiscountProfileCodeType
    Private mCodeId As String
    Private mName As String

    Public Property CodeId As String
        Get
            Return mCodeId
        End Get
        Set(ByVal value As String)
            mCodeId = value
        End Set
    End Property

    Public Property Name As String
        Get
            Return mName
        End Get
        Set(ByVal value As String)
            mName = value
        End Set
    End Property

    Public Sub New()

    End Sub

    Public Sub New(ByVal codeId As String, ByVal name As String)
        mCodeId = codeId
        mName = name
    End Sub
End Class

Public Class DiscountProduct
    Private mDiscountId As Long
    Private mProductName As String
    Private mExclude As Boolean

    Public Property DiscountId As Long
        Get
            Return mDiscountId
        End Get
        Set(ByVal value As Long)
            mDiscountId = value
        End Set
    End Property

    Public Property ProductName As String
        Get
            Return mProductName
        End Get
        Set(ByVal value As String)
            mProductName = value
        End Set
    End Property

    Public Property Exclude As String
        Get
            Return mExclude
        End Get
        Set(ByVal value As String)
            mExclude = value
        End Set
    End Property

    ''rev:mia Sept 16 2013 - add ComCode
    Private mComCode As String
    Public Property sComCode As String
        Get
            Return mComCode
        End Get
        Set(value As String)
            mComCode = value
        End Set
    End Property
End Class

#End Region