Partial Public Class PackageDataSet
    Partial Class BrandRow

        Public ReadOnly Property Code() As String
            Get
                Return Me.BrdCode
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsBrdNameNull Then Return Me.BrdName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.Code & " - " & Me.Name
            End Get
        End Property

    End Class
End Class