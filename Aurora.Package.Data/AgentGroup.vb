Partial Public Class PackageDataSet
    Partial Class AgentGroupRow

        Public ReadOnly Property Code() As String
            Get
                If Not Me.IsAgpCodeNull Then Return Me.AgpCode Else Return ""
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsAgpDescNull Then Return Me.AgpDesc Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.Code & " - " & Me.Name
            End Get
        End Property
    End Class
End Class

