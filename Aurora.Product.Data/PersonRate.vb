Partial Public Class ProductDataSet
    Partial Class PersonRateRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.PrrId
            End Get
            Set(ByVal value As String)
                Me.PrrId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.PrrPtbId
            End Get
            Set(ByVal value As String)
                Me.PrrPtbId = value
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return Me.PrrQty
            End Get
            Set(ByVal value As Decimal)
                Me.PrrQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                Return Me.PrrRate
            End Get
            Set(ByVal value As Decimal)
                Me.PrrRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                Return Me.ProductRateTravelBandRow.UomId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                Return Me.ProductRateTravelBandRow.UomCode
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                Return Me.ProductRateTravelBandRow.Uom
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return Me.Code
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        ''' <summary>
        ''' Get the person rate code
        ''' </summary>
        Public ReadOnly Property Code() As String
            Get
                If Not Me.IsPrrCodPrtIdNull AndAlso ProductDataSet.Code.FindById(Me.PrrCodPrtId) IsNot Nothing Then
                    Return ProductDataSet.Code.FindById(Me.PrrCodPrtId).CodCode
                Else
                    Return Nothing
                End If

            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return Me.Code & " - " & CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function

    End Class
End Class
