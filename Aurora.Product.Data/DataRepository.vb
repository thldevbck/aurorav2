Imports System.Data
Imports System.Web
Imports System.Configuration

Imports Aurora.Product.Data.ProductDataSet

Public Module DataRepository

    '=======================================================================================================
    ' Lookups
    '=======================================================================================================

    Public Function GetProductLookups(ByVal productDataSet As ProductDataSet, ByVal comCode As String) As ProductDataSet
        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_GetProductLookups", result, comCode)

        Aurora.Common.Data.CopyDataTable(result.Tables(0), productDataSet.Brand)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), productDataSet.Feature)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), productDataSet.Type)

        Return productDataSet
    End Function


    Public Function GetSaleableProductLookups(ByVal productDataSet As ProductDataSet, ByVal comCode As String) As ProductDataSet
        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_GetSaleableProductLookups", result, comCode)

        Aurora.Common.Data.CopyDataTable(result.Tables(0), productDataSet.Attribute)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), productDataSet.Code)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), productDataSet.Country)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), productDataSet.Location)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), productDataSet.VehicleProduct)

        Return productDataSet
    End Function

    '=======================================================================================================
    ' Products & Rates
    '=======================================================================================================

    Public Function GetNewId() As String
        Return Aurora.Common.Data.GetNewId()
    End Function

    Public Function GetMaxSapSuffix( _
     ByVal comCode As String, _
     ByVal prdId As String) As Integer
        Return CInt(Aurora.Common.Data.ExecuteScalarSP("Product_GetMaxSapSuffix", comCode, prdId))
    End Function

    Public Function SearchProduct(ByVal productDataSet As ProductDataSet, _
                                  ByVal comCode As String, _
                                  ByVal prdId As String, _
                                  ByVal prdTypId As String, _
                                  ByVal prdBrdCode As String, _
                                  ByVal prdShortName As String, _
                                  ByVal prdName As String, _
                                  ByVal prdIsActive As Nullable(Of Boolean), _
                                  ByVal ftrId0 As String, _
                                  ByVal ftrId1 As String, _
                                  ByVal ftrId2 As String, _
                                  ByVal ftrId3 As String, _
                                  ByVal ftrId4 As String, _
                                  ByVal webEnabledSelection As Integer) As ProductDataSet


        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_GetProduct", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdId), prdId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdTypId), prdTypId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdBrdCode), prdBrdCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdShortName), prdShortName, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdName), prdName, DBNull.Value), _
            IIf(prdIsActive.HasValue, prdIsActive.GetValueOrDefault(), DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ftrId0), ftrId0, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ftrId1), ftrId1, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ftrId2), ftrId2, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ftrId3), ftrId3, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ftrId4), ftrId4, DBNull.Value), _
            webEnabledSelection)

        Aurora.Common.Data.CopyDataTable(result.Tables(0), productDataSet.Product)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), productDataSet.ProductFeature)
        ' Product Non-Availability Enhancement - Added by Shoel on 12.8.8
        Aurora.Common.Data.CopyDataTable(result.Tables(2), productDataSet.NonAvailabilityProfile)
        ' Product Non-Availability Enhancement - Added by Shoel on 12.8.8
        Return productDataSet
    End Function

    Public Function GetProduct( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal prdId As String)


        Return SearchProduct(productDataSet, comCode, prdId, Nothing, Nothing, Nothing, Nothing, New Nullable(Of Boolean), Nothing, Nothing, Nothing, Nothing, Nothing, 0)
    End Function

    Public Function GetProductByShortName( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal prdShortName As String)


        Return SearchProduct(productDataSet, comCode, Nothing, Nothing, Nothing, prdShortName, Nothing, New Nullable(Of Boolean), Nothing, Nothing, Nothing, Nothing, Nothing, 0)
    End Function

    'CREATE PROCEDURE [dbo].[Product_SearchSaleableProduct] 
    '(
    '	@ComCode AS varchar(64),
    '	@PrdId AS varchar(64) = NULL,
    '	@PrdTypId AS varchar(64) = NULL,
    '	@PrdBrdCode AS varchar(1) = NULL,
    '	@PrdShortName AS varchar(8) = NULL,
    '	@PrdName AS varchar(64) = NULL,
    '	@SapCtyCode AS varchar(12) = NULL,
    '	@SapSuffix AS int = NULL,
    '	@SapStatus AS varchar(12) = NULL,
    '	@SapIsBase AS bit = NULL,
    '	@SapIsFlex AS bit = NULL,
    '	@PkgId AS varchar(64) = NULL,
    '   @CurrentDate AS datetime,
    '   @CurrentFutureOnly AS bit = 0,
    '   @IncludeProduct AS bit = 0,
    '   @IncludeRates AS bit = 0
    ')

    Public Function SearchSaleableProduct( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal prdId As String, _
     ByVal prdTypId As String, _
     ByVal prdBrdCode As String, _
     ByVal prdShortName As String, _
     ByVal prdName As String, _
     ByVal sapCtyCode As String, _
     ByVal sapSuffix As Nullable(Of Integer), _
     ByVal sapStatus As String, _
     ByVal sapIsBase As Nullable(Of Boolean), _
     ByVal sapIsFlex As Nullable(Of Boolean), _
     ByVal pkgId As String, _
     ByVal currentFutureOnly As Boolean, _
     ByVal includeProduct As Boolean, _
     ByVal includeRates As Boolean)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_SearchSaleableProduct", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdId), prdId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdTypId), prdTypId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdBrdCode), prdBrdCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdShortName), prdShortName, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(prdName), prdName, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(sapCtyCode), sapCtyCode, DBNull.Value), _
            IIf(sapSuffix.HasValue, sapSuffix.GetValueOrDefault(), DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(sapStatus), sapStatus, DBNull.Value), _
            IIf(sapIsBase.HasValue, sapIsBase.GetValueOrDefault(), DBNull.Value), _
            IIf(sapIsFlex.HasValue, sapIsFlex.GetValueOrDefault(), DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgId), pkgId, DBNull.Value), _
            Date.Now.Date, _
            currentFutureOnly, _
            includeProduct, _
            includeRates)

        Dim i As Integer = 0

        If includeProduct Then
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.Product) : i += 1
        End If
        Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.SaleableProduct) : i += 1
        Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.ProductAttribute) : i += 1
        Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.Package) : i += 1
        Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.PackageProduct) : i += 1
        If includeRates Then
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.TravelBandSet) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.ProductRateTravelBand) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.DiscountProduct) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.ProductRateDiscount) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.PersonRate) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.ProductRateBookingBand) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.LocationDependentRate) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.VehicleDependentRate) : i += 1
            Aurora.Common.Data.CopyDataTable(result.Tables(i), productDataSet.FixedRate) : i += 1
        End If

        Return productDataSet
    End Function


    Public Function SearchSaleableProduct( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal prdId As String, _
     ByVal sapStatus As String, _
     ByVal currentFutureOnly As Boolean)

        Return SearchSaleableProduct(productDataSet, comCode, prdId, Nothing, Nothing, Nothing, Nothing, Nothing, New Nullable(Of Integer), sapStatus, New Nullable(Of Boolean), New Nullable(Of Boolean), Nothing, currentFutureOnly, False, True)
    End Function


    Public Function GetSaleableProduct( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal sapId As String)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_GetSaleableProduct", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(sapId), sapId, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), productDataSet.Product)                  ' 0 - Product 
        Aurora.Common.Data.CopyDataTable(result.Tables(1), productDataSet.SaleableProduct)          ' 1 - SaleableProduct 
        Aurora.Common.Data.CopyDataTable(result.Tables(2), productDataSet.ProductAttribute)         ' 2 - ProductAttribute 
        Aurora.Common.Data.CopyDataTable(result.Tables(3), productDataSet.PersonTypeDiscount)       ' 3 - PersonTypeDiscount 
        Aurora.Common.Data.CopyDataTable(result.Tables(4), productDataSet.LocationAvailability)     ' 4 - LocationAvailability 
        Aurora.Common.Data.CopyDataTable(result.Tables(5), productDataSet.NonAvailabilityProfile)   ' 5 - NonAvailabilityProfile 
        Aurora.Common.Data.CopyDataTable(result.Tables(6), productDataSet.Package)                  ' 6 - Package
        Aurora.Common.Data.CopyDataTable(result.Tables(7), productDataSet.PackageProduct)           ' 7 - PackageProduct 
        Aurora.Common.Data.CopyDataTable(result.Tables(8), productDataSet.TravelBandSet)            ' 8 - TravelBandSet 
        ''Aurora.Common.Data.CopyDataTable(result.Tables(9), productDataSet.VehicleNumberAvailable)   ' 9 - VehicleNumberAvailable REV:MIA MAY 3 2011

        Return productDataSet
    End Function

    Public Function GetTravelBandSet( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal tbsId As String)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_GetTravelBandSet", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(tbsId), tbsId, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), productDataSet.ProductRateTravelBand)   ' 0 - ProductRateTravelBand */

        Return productDataSet
    End Function

    'ALTER PROCEDURE [dbo].[Product_CopyTravelBandSet] 
    '(
    '	@ComCode AS varchar(64),
    '	@FromTbsId AS varchar(64),
    '	@TbsEffDateTime AS datetime,
    '	@AddUsrId AS varchar(64)	= '',
    '	@AddPrgmName AS varchar(64)	= ''
    ')    
    Public Function CopyTravelBandSet( _
     ByVal comCode As String, _
     ByVal fromTbsId As String, _
     ByVal tbsEffDateTime As Date, _
     ByVal AddUsrId As String, _
     ByVal AddPrgmName As String) As String

        Return "" & Aurora.Common.Data.ExecuteScalarSP("Product_CopyTravelBandSet", _
            comCode, fromTbsId, tbsEffDateTime, AddUsrId, AddPrgmName)

    End Function

    Public Function GetRates( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal tbsId As String, _
     ByVal ptbId As String)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Product_GetRates", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(tbsId), tbsId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ptbId), ptbId, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), productDataSet.Agent)                   ' 0 - Agent 
        Aurora.Common.Data.CopyDataTable(result.Tables(1), productDataSet.AgentDependentRate)      ' 1 - AgentDependentRate
        Aurora.Common.Data.CopyDataTable(result.Tables(2), productDataSet.PersonRate)              ' 2 - PersonRate
        Aurora.Common.Data.CopyDataTable(result.Tables(3), productDataSet.PersonDependentRate)     ' 3 - PersonDependentRate
        Aurora.Common.Data.CopyDataTable(result.Tables(4), productDataSet.DiscountProduct)         ' 4 - DiscountProduct (Product)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), productDataSet.ProductRateDiscount)     ' 5 - ProductRateDiscount
        Aurora.Common.Data.CopyDataTable(result.Tables(6), productDataSet.ProductRateBookingBand)  ' 6 - ProductRateBookingBand
        Aurora.Common.Data.CopyDataTable(result.Tables(7), productDataSet.LocationDependentRate)   ' 7 - LocationDependentRate
        Aurora.Common.Data.CopyDataTable(result.Tables(8), productDataSet.VehicleDependentRate)    ' 8 - VehicleDependentRate
        Aurora.Common.Data.CopyDataTable(result.Tables(9), productDataSet.FixedRate)               ' 9 - FixedRate

        Return productDataSet
    End Function

    Public Sub DeleteRate(ByVal ptbId As String, ByVal rateId As String)
        Aurora.Common.Data.ExecuteScalarSP("Product_DeleteRate", ptbId, rateId)
    End Sub

    '=======================================================================================================
    ' Packages
    '=======================================================================================================

    Public Function SearchPackage( _
     ByVal productDataSet As ProductDataSet, _
     ByVal comCode As String, _
     ByVal pkgId As String, _
     ByVal pkgCode As String, _
     ByVal pkgBookedFromDate As Date, _
     ByVal pkgBookedToDate As Date, _
     ByVal pkgTravelFromDate As Date, _
     ByVal pkgTravelToDate As Date, _
     ByVal pkgBrdCode As String, _
     ByVal pkgCtyCode As String, _
     ByVal pkgIsActive As String, _
     ByVal pkgSapId As String) As ProductDataSet

        Dim result As New ProductDataSet()
        Aurora.Common.Data.ExecuteTableSP("Product_SearchPackage", result.Package, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgId), pkgId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgCode), pkgCode, DBNull.Value), _
            IIf(pkgBookedFromDate <> Date.MinValue, pkgBookedFromDate, DBNull.Value), _
            IIf(pkgBookedToDate <> Date.MinValue, pkgBookedToDate, DBNull.Value), _
            IIf(pkgTravelFromDate <> Date.MinValue, pkgTravelFromDate, DBNull.Value), _
            IIf(pkgTravelToDate <> Date.MinValue, pkgTravelToDate, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgBrdCode), pkgBrdCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgCtyCode), pkgCtyCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgIsActive), pkgIsActive, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(pkgSapId), pkgSapId, DBNull.Value))

        Return productDataSet
    End Function

    Public Function CreatePackageProduct(ByVal pkgId As String, ByVal sapId As String, ByVal copyCreateLink As String, ByVal usrId As String, ByVal prgmName As String) As PackageProductRow
        Dim result As PackageProductDataTable = Aurora.Common.Data.ExecuteTableSP( _
            "Product_CreatePackageProduct", _
            New PackageProductDataTable(), _
            pkgId, sapId, copyCreateLink, usrId, prgmName)
        If result.Count = 1 Then
            Return result(0)
        Else
            Return Nothing
        End If
    End Function

    Public Sub DeletePackageProduct(ByVal pkgId As String, ByVal sapId As String)
        Aurora.Common.Data.ExecuteScalarSP( _
            "Product_DeletePackageProduct", _
            pkgId, sapId)
    End Sub

    Public Function IsTypeVehicle(ByVal TypeCode As String) As Boolean
        'Product_IsTypeVehicle
        Dim oXml As System.Xml.XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("Product_IsTypeVehicle", TypeCode)
        If oXml.DocumentElement.SelectSingleNode("Type/IsVehicle") IsNot Nothing _
        AndAlso oXml.DocumentElement.SelectSingleNode("Type/IsVehicle").InnerText.Trim() <> String.Empty Then
            Return CBool(oXml.DocumentElement.SelectSingleNode("Type/IsVehicle").InnerText.Trim())
        Else
            Return False
        End If

    End Function

    Public Function UpdateChildDicounts(ByVal ParentId As String, ByVal ParentAmount As Decimal) As Object
        If ParentAmount = 9999999999D Then
            Return Aurora.Common.Data.ExecuteScalarSP("Product_UpdateChildDiscounts", ParentId, DBNull.Value)
        Else
            Return Aurora.Common.Data.ExecuteScalarSP("Product_UpdateChildDiscounts", ParentId, ParentAmount)
        End If

    End Function

#Region "Webservice Stuff"

    Public Function CreateVehicleDependentNote(ByVal NoteId As String, ByVal NoteDescription As String, ByVal NoteVdrId As String, ByVal UserId As String, ByVal ProgramName As String) As Object

        ' CREATE PROC TEMP_WEBS_CreateVehicleDependentNote
        ' @NteId VARCHAR(64)
        ',@NteDesc VARCHAR(2048)
        ',@NteVdrId VARCHAR(64)
        ',@UsrId VARCHAR(64)
        ',@ProgramName VARCHAR(256)

        Return Aurora.Common.Data.ExecuteScalarSP("Product_CreateVehicleDependentNote", NoteId, NoteDescription, NoteVdrId, UserId, ProgramName)
    End Function

    Public Function UpdateVehicleDependentNote(ByVal NoteId As String, ByVal NoteDescription As String, ByVal UserId As String) As Object

        ' CREATE PROC TEMP_WEBS_UpdateVehicleDependentNote
        ' @NteId VARCHAR(64)
        ' ,@NteDesc VARCHAR(2048)
        ' ,@UsrId VARCHAR(64)

        Return Aurora.Common.Data.ExecuteScalarSP("Product_UpdateVehicleDependentNote", NoteId, NoteDescription, UserId)
    End Function

    Public Function DeleteVehicleDependentNote(ByVal NoteId As String) As Object
        'CREATE PROC TEMP_WEBS_DeleteVehicleDependentNote
        ' @NteId VARCHAR(64)
        ' ,@UsrId VARCHAR(64)

        Return Aurora.Common.Data.ExecuteScalarSP("Product_DeleteVehicleDependentNote", NoteId)
    End Function

#End Region
#Region "rev:mia Project PHOENIX"

    Public Function UpdateProductForB2CSetup( _
                                           ByVal PrdId As String, _
                                           ByVal PrdWebEnabledFromDate As Date, _
                                           ByVal PrdWebEnabledToDate As Date, _
                                           ByVal PrdInfoURL As String, _
                                           ByVal PrdNoOfAdult As Integer, _
                                           ByVal PrdNoOfChild As Integer, _
                                           ByVal PrdNoOfInfant As Integer, _
                                           ByVal ModUsrId As String _
                                           ) As String

        Dim retValue As String = ""
        Dim params(8) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("PrdId", DbType.String, Left(PrdId, 64), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PrdWebEnabledFromDate", DbType.DateTime, PrdWebEnabledFromDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("PrdWebEnabledToDate", DbType.DateTime, PrdWebEnabledToDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("PrdInfoURL", DbType.String, Left(PrdInfoURL, 256), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("PrdNoOfAdult", DbType.Int16, PrdNoOfAdult, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("PrdNoOfChild", DbType.Int16, PrdNoOfChild, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("PrdNoOfInfant", DbType.Int16, PrdNoOfInfant, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, Left(ModUsrId, 10), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("returnValue", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)


            Aurora.Common.Data.ExecuteOutputSP("ProductSetup_UpdateProductForB2CSetup", params)
            retValue = params(8).Value

        Catch ex As Exception
            retValue = "ERROR/Failed to update Web Product tab"
        End Try
        Return retValue

    End Function

    Public Function GetProductForB2CSetup(ByVal PrdId As String) As DataSet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("ProductSetup_GetProductForB2CSetup", result, PrdId)
        Return result
    End Function


#Region "Additions/Fixes for Phoenix by Shoel"

    ''' <summary>
    ''' This function reads the Universal Info table and gets the max number of adult/child pax
    ''' </summary>
    ''' <param name="PaxType">1 - Adult, 2 - Child</param>
    ''' <returns>Max Pax count</returns>
    ''' <remarks></remarks>
    Public Function GetMaxPaxForWebProductTab(ByVal PaxType As Integer) As Integer
        Select Case PaxType
            Case 1
                Return CInt(Common.Data.ExecuteScalarSQL("SELECT dbo.getUviValue('WEBPRDMAXADULTPAX')"))
            Case 2
                Return CInt(Common.Data.ExecuteScalarSQL("SELECT dbo.getUviValue('WEBPRDMAXCHILDPAX')"))
        End Select
    End Function
#End Region

#End Region


End Module
