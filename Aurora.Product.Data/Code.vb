
Partial Class ProductDataSet
    Partial Class CodeDataTable

        ''' <summary>
        ''' Find the Code Data Row by Id
        ''' </summary>
        ''' <remarks>This would be far better done with relationships....but i dont want to create a hundred relationships on the dataset (code is used a lot)</remarks>
        Public Function FindById(ByVal CodId As String) As CodeRow
            For Each codeRow As CodeRow In Me.Rows
                If codeRow.CodId = CodId Then
                    Return codeRow
                End If
            Next
            Return Nothing

        End Function

    End Class

    Partial Class CodeRow
        Public Const CodeType_Address_Type As Integer = 1
        Public Const CodeType_Agent_Type As Integer = 3
        Public Const CodeType_Booked_Product_Type As Integer = 4
        Public Const CodeType_Incident_Type As Integer = 8
        Public Const CodeType_Location_Type As Integer = 9
        Public Const CodeType_Marketing_Region As Integer = 10
        Public Const CodeType_Mode_Of_Communication As Integer = 11
        Public Const CodeType_Note_Type As Integer = 13
        Public Const CodeType_Package_Type As Integer = 14
        Public Const CodeType_Person_Type As Integer = 16
        Public Const CodeType_Product_Rate_Discount_type As Integer = 17
        Public Const CodeType_Unit_of_Measure As Integer = 18
        Public Const CodeType_Referral_Type As Integer = 19
        Public Const CodeType_Specification_Unit_Of_Value As Integer = 20
        Public Const CodeType_Traveller_Type As Integer = 22
        Public Const CodeType_Currency As Integer = 23
        Public Const CodeType_Phone_Number_Type As Integer = 24
        Public Const CodeType_Contact_Type As Integer = 25
        Public Const CodeType_Audience_Type As Integer = 26
        Public Const CodeType_Cancellation_Reason As Integer = 27
        Public Const CodeType_Cancellation_Type As Integer = 28
        Public Const CodeType_Knock_Back_Type As Integer = 30
        Public Const CodeType_Note_Spec_Type As Integer = 31
        Public Const CodeType_Repairs_Maintenance_Reasons As Integer = 32
        Public Const CodeType_Relocation_Rule_Type As Integer = 33
        Public Const CodeType_Agent_Category As Integer = 35
        Public Const CodeType_Vis_Level As Integer = 36
        Public Const CodeType_Activity_Type As Integer = 37
        Public Const CodeType_Administration_Fee As Integer = 38
        Public Const CodeType_Payment_Method As Integer = 39
        Public Const CodeType_Booking_Request_Unit_of_Measure As Integer = 40
        Public Const CodeType_Person_Rate As Integer = 41
        Public Const CodeType_Status_Code As Integer = 42
        Public Const CodeType_Fleet_Override_Reasons As Integer = 43
        Public Const CodeType_AGENT_DEBTOR_STATUS As Integer = 44

        Public Const UOM_FixedFee As String = ""  ' Fixed Fee"
        Public Const UOM_24HR As String = "24HR"  ' 24 Hour Day"
        Public Const UOM_DAY As String = "DAY"    ' Calendar Day"
        Public Const UOM_HOUR As String = "HOUR"  ' Hour"
        Public Const UOM_KM As String = "KM"      ' Kilometre"

        Public ReadOnly Property Description() As String
            Get
                If Me.IsCodDescNull Then
                    Return Me.CodCode
                Else
                    Return Me.CodCode + " - " & Me.CodDesc
                End If
            End Get
        End Property

        Public Function DescribeQty(ByVal qty As Decimal) As String
            Return qty.ToString("0.##")
        End Function

        Public Shared Function DescribeQty(ByVal code As CodeRow, ByVal qty As Decimal) As String
            If code IsNot Nothing Then
                Return code.DescribeQty(qty)
            Else
                Return qty.ToString("0.##")
            End If
        End Function

        Public Function DescribeUom(ByVal qty As Decimal) As String
            If CodCode = UOM_24HR OrElse CodCode = UOM_DAY OrElse CodCode = UOM_HOUR Then
                Return Me.CodDesc & IIf(qty > 1, "s", "")
            ElseIf CodCode = UOM_KM Then
                Return Me.CodDesc & IIf(qty > 1, "s", "")
            Else
                Return ""
            End If
        End Function

        Public Shared Function DescribeUom(ByVal code As CodeRow, ByVal qty As Decimal) As String
            If code IsNot Nothing Then
                Return code.DescribeUom(qty)
            Else
                Return ""
            End If
        End Function

        Public Function DescribeQtyUom(ByVal qty As Decimal) As String
            Return (DescribeQty(qty) & " " & Me.DescribeUom(qty)).Trim()
        End Function

        Public Shared Function DescribeQtyUom(ByVal code As CodeRow, ByVal qty As Decimal) As String
            If code IsNot Nothing Then
                Return code.DescribeQtyUom(qty)
            Else
                Return qty.ToString("0.##")
            End If
        End Function

        Public Shared Function DescribeQtyUomRate(ByVal code As CodeRow, ByVal qty As Decimal, ByVal rate As Decimal) As String
            If rate <> 0 Then
                Return DescribeQtyUom(code, qty) & " @ " & ProductConstants.RateToString(rate)
            Else
                Return "(none)"
            End If
        End Function

    End Class

End Class
