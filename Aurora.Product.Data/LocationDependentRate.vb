Partial Public Class ProductDataSet
    Partial Class LocationDependentRateRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.LdrId
            End Get
            Set(ByVal value As String)
                Me.LdrId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.ProductRateBookingBandRow.ParentPtbId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return Me.LdrQty
            End Get
            Set(ByVal value As Decimal)
                Me.LdrQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                Return Me.LdrRate
            End Get
            Set(ByVal value As Decimal)
                Me.LdrRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                Return Me.ProductRateBookingBandRow.UomId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                Return Me.ProductRateBookingBandRow.UomCode
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                Return Me.ProductRateBookingBandRow.Uom
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return Me.LocFromCode() & " - " & Me.LocToCode()
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public ReadOnly Property LocFromCode() As String
            Get
                If Me.LocationRowByLocation_LocationDependentRate_From IsNot Nothing Then
                    Return Me.LocationRowByLocation_LocationDependentRate_From.LocCode
                Else
                    Return "ANY"
                End If
            End Get
        End Property

        Public ReadOnly Property LocToCode() As String
            Get
                If Me.LocationRowByLocation_LocationDependentRate_To IsNot Nothing Then
                    Return Me.LocationRowByLocation_LocationDependentRate_To.LocCode()
                Else
                    Return "ANY"
                End If
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return _
             "Booked From " & Me.ProductRateBookingBandRow.BapBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) _
             & " To " & Me.ProductRateBookingBandRow.BapBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) _
             & " From " & Me.LocFromCode() _
             & " To " & Me.LocToCode() _
             & " - " _
             & CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function

    End Class
End Class
