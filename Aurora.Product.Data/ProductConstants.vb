Imports System.Drawing

Imports Aurora.Common
Imports Aurora.Product.Data.ProductDataSet

Public Class ProductConstants
    Inherits DataConstants

    Public Const DiscountType_DBT As String = "DBT" ' Days Before Travel
    Public Const DiscountType_DDT As String = "DDT" ' Day Dependent Travel
    Public Const DiscountType_FOC As String = "FOC" ' Free of Charge Days
    Public Const DiscountType_FRR As String = "FRR" ' Fixed Rate Reductions
    Public Const DiscountType_LHD As String = "LHD" ' Long Hire Discount
    Public Const DiscountType_PDR As String = "PDR" ' Person Dependent Rate
    Public Const DiscountType_PersonRate As String = "PersonRate" ' Person Rate type discount

    Public Const SaleableProductStatus_Active As String = "Active"
    Public Const SaleableProductStatus_Pending As String = "Pending"
    Public Const SaleableProductStatus_Inactive As String = "Inactive"

    Public Const AttributeDataType_Integer As String = "Integer"
    Public Const AttributeDataType_String As String = "String"
    Public Const AttributeDataType_Boolean As String = "Boolean"

    Public Shared Function RateToString(ByVal rate As Decimal) As String
        'If rate <> 0 Then
        If IsNumeric(rate) Then
            ' 16.1.9 - Shoel - to allow 0 values to be displayed
            'Return "$" & rate.ToString("0.00")
            Return "$" & rate.ToString("f2")
        Else
            Return ""
        End If
    End Function

    Public Shared Function PercentToString(ByVal percent As Decimal) As String
        If IsNumeric(percent) Then
            'If percent <> 0 Then
            ' 16.1.9 - Shoel - to allow 0 values to be displayed
            'Return percent.ToString("0.00") & "%"
            Return percent.ToString("f2") & "%"
        Else
            Return ""
        End If
    End Function

End Class
