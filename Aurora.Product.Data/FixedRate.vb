Partial Public Class ProductDataSet
    Partial Class FixedRateRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.FfrId
            End Get
            Set(ByVal value As String)
                Me.FfrId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.FfrPtbId
            End Get
            Set(ByVal value As String)
                Me.FfrPtbId = value
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return 1
            End Get
            Set(ByVal value As Decimal)
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                Return Me.FfrRate
            End Get
            Set(ByVal value As Decimal)
                Me.FfrRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                Return Me.ProductRateTravelBandRow.UomId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                Return Me.ProductRateTravelBandRow.UomCode
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                Return Me.ProductRateTravelBandRow.Uom
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return Me.From.ToString() & " - " & Me._To.ToString()
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, 2)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public ReadOnly Property From() As Integer
            Get
                Return Me.FfrFrom
            End Get
        End Property

        Public ReadOnly Property _To() As Integer
            Get
                If Not Me.IsFfrToNull Then Return Me.FfrTo Else Return 0
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return "Fixed Rate " & Me.ProductRateTravelBandRow.Uom() & "(s)" _
             & " From " & Me.From.ToString() _
             & " To " & Me._To.ToString() _
             & " @ $" & Me.FfrRate.ToString("0.00")
        End Function

    End Class
End Class
