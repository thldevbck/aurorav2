Partial Public Class ProductDataSet
    Partial Class CountryRow

        Public ReadOnly Property Code() As String
            Get
                Return Me.CtyCode
            End Get
        End Property

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsCtyNameNull Then Return Me.CtyName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.Code & " - " & Me.Name
            End Get
        End Property

    End Class
End Class