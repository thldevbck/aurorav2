Partial Public Class ProductDataSet
    Partial Class PackageRow

        Public ReadOnly Property Name() As String
            Get
                If Not Me.IsPkgNameNull Then Return Me.PkgName Else Return ""
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.PkgCode & " - " & Me.Name
            End Get
        End Property

        Public ReadOnly Property BookedDescription() As String
            Get
                Dim result As String = ""
                If Not IsPkgBookedFromDateNull() Then result &= Me.PkgBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsPkgBookedToDateNull() Then result &= Me.PkgBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property TravelDescription() As String
            Get
                Dim result As String = ""
                If Not IsPkgTravelFromDateNull() Then result &= Me.PkgTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                result &= " - "
                If Not IsPkgTravelToDateNull() Then result &= Me.PkgTravelToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                Return result
            End Get
        End Property

        Public ReadOnly Property BookedFromDate() As Date
            Get
                If Not IsPkgBookedFromDateNull() Then Return Me.PkgBookedFromDate Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property BookedToDate() As Date
            Get
                If Not IsPkgBookedToDateNull() Then Return Me.PkgBookedToDate Else Return Date.MaxValue
            End Get
        End Property

        Public ReadOnly Property TravelFromDate() As Date
            Get
                If Not IsPkgTravelFromDateNull() Then Return Me.PkgTravelFromDate Else Return Date.MinValue
            End Get
        End Property

        Public ReadOnly Property TravelToDate() As Date
            Get
                If Not IsPkgTravelToDateNull() Then Return Me.PkgTravelToDate Else Return Date.MaxValue
            End Get
        End Property

        Public ReadOnly Property IsPast(ByVal currentDate As Date) As Boolean
            Get
                'Return Not IsCurrent(currentDate) And Not IsFuture(currentDate)
                If currentDate > BookedToDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property


        Public ReadOnly Property IsCurrent(ByVal currentDate As Date) As Boolean
            Get
                'Return currentDate >= BookedFromDate AndAlso currentDate <= BookedToDate _
                ' AndAlso currentDate >= TravelFromDate AndAlso currentDate <= TravelToDate
                If currentDate <= BookedToDate And currentDate >= BookedFromDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsFuture(ByVal currentDate As Date) As Boolean
            Get
                'Return currentDate < BookedFromDate _
                ' AndAlso currentDate < TravelFromDate
                If currentDate < BookedFromDate Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Me.PkgIsActive = "Active"
            End Get
        End Property

        Public ReadOnly Property IsPending() As Boolean
            Get
                Return Me.PkgIsActive = "Pending"
            End Get
        End Property

        Public ReadOnly Property IsInactive() As Boolean
            Get
                Return Not Me.IsActive AndAlso Not Me.IsPending
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If Me.IsActive AndAlso Me.IsFuture(currentDate) Then
                    Return ProductConstants.FutureColor
                ElseIf Me.IsActive AndAlso Me.IsCurrent(currentDate) Then
                    Return ProductConstants.CurrentColor
                ElseIf Me.IsActive AndAlso Me.IsPast(currentDate) Then
                    Return ProductConstants.PastColor
                Else
                    Return ProductConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property StatusText(ByVal currentDate As Date) As String
            Get
                If Me.IsActive AndAlso Me.IsFuture(currentDate) Then
                    Return "Active / Future"
                ElseIf Me.IsActive AndAlso Me.IsCurrent(currentDate) Then
                    Return "Active / Current"
                ElseIf Me.IsActive AndAlso Me.IsPast(currentDate) Then
                    Return "Active / Past"
                Else
                    Return "Inactive"
                End If
            End Get
        End Property

    End Class
End Class
