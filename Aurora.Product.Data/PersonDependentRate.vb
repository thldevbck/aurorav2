Partial Public Class ProductDataSet
    Partial Class PersonDependentRateRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.PdrId
            End Get
            Set(ByVal value As String)
                Me.PdrId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.PdrPtbId
            End Get
            Set(ByVal value As String)
                Me.PdrPtbId = value
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return Me.PdrQty
            End Get
            Set(ByVal value As Decimal)
                Me.PdrQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                Return Me.PdrRate
            End Get
            Set(ByVal value As Decimal)
                Me.PdrRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                Return Me.ProductRateTravelBandRow.UomId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                Return Me.ProductRateTravelBandRow.UomCode
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                Return Me.ProductRateTravelBandRow.Uom
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function

    End Class
End Class
