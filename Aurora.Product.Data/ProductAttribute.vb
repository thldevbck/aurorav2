
Partial Public Class ProductDataSet
    Partial Class ProductAttributeDataTable
        Public Function FindByAttIdSapId(ByVal attId As String, ByVal sapId As String) As ProductAttributeRow
            For Each productAttributeRow As ProductAttributeRow In Me.Rows
                If productAttributeRow.PatAttId = attId AndAlso productAttributeRow.PatSapId = sapId Then
                    Return productAttributeRow
                End If
            Next
            Return Nothing
        End Function
    End Class



    Partial Class ProductAttributeRow

        Public Property Value() As String
            Get
                If Not Me.IsPatValueNull Then Return Me.PatValue Else Return ""
            End Get
            Set(ByVal value As String)
                If value IsNot Nothing Then
                    Me.PatValue = value
                Else
                    Me.SetPatValueNull()
                End If
            End Set
        End Property

        Public Overrides Function ToString() As String
            If Me.AttributeRow Is Nothing Then
                Return ""
            End If

            If Me.AttributeRow.AttDataType = "Boolean" Then
                Return Me.AttributeRow.AttDesc
            ElseIf Me.AttributeRow.AttDataType = "Integer" Then
                Return Me.AttributeRow.AttDesc + " - " + Me.Value
            Else
                Return Me.AttributeRow.AttDesc + " - """ + Me.Value + """"
            End If
        End Function

    End Class

End Class
