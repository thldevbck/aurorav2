Imports System.Text
''REV:MIA JULY 13 2011 - REMOVE HARDCODED STUFF FOR INSIDE TypeDescription() PROPERTY
Imports Aurora.Common.Data

Partial Class ProductDataSet

    Partial Class ProductRateDiscountRow

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public ReadOnly Property UomCode() As CodeRow
            Get
                If Not Me.IsPdsCodUomIdNull AndAlso ProductDataSet.Code.FindById(Me.PdsCodUomId) IsNot Nothing Then
                    Return ProductDataSet.Code.FindById(Me.PdsCodUomId)
                Else
                    Return Me.ProductRateTravelBandRow.UomCode
                End If
            End Get
        End Property

        Public ReadOnly Property Uom() As String
            Get
                If UomCode IsNot Nothing Then Return UomCode.CodCode Else Return Nothing
            End Get
        End Property

        Public Property UomId() As String
            Get
                If Not Me.IsPdsCodUomIdNull Then
                    Return Me.PdsCodUomId
                Else
                    Return Nothing
                End If
            End Get
            Set(ByVal value As String)
                If Not String.IsNullOrEmpty(value) Then
                    Me.PdsCodUomId = value
                Else
                    Me.SetPdsCodUomIdNull()
                End If
            End Set
        End Property

        Public Property Qty() As Decimal
            Get
                Return Me.PdsQty
            End Get
            Set(ByVal value As Decimal)
                Me.PdsQty = value
            End Set
        End Property

        Public Property Amt() As Decimal
            Get
                If Not Me.IsPdsAmtNull Then Return Me.PdsAmt Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsAmt = value
            End Set
        End Property

        Public Property Percentage() As Decimal
            Get
                If Not Me.IsPdsPercentageNull Then Return Me.PdsPercentage Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsPercentage = value
            End Set
        End Property

        Public Property DiscountType() As String
            Get
                If Not Me.IsPdsCodRtyIdNull AndAlso ProductDataSet.Code.FindById(Me.PdsCodRtyId) IsNot Nothing Then
                    Return ProductDataSet.Code.FindById(Me.PdsCodRtyId).CodCode
                Else
                    Return Nothing
                End If
            End Get
            Set(ByVal value As String)
                If Not String.IsNullOrEmpty(value) AndAlso ProductDataSet.Code.FindByCodCdtNumCodCode(CodeRow.CodeType_Product_Rate_Discount_type, value) IsNot Nothing Then
                    Me.PdsCodRtyId = ProductDataSet.Code.FindByCodCdtNumCodCode(CodeRow.CodeType_Product_Rate_Discount_type, value).CodId
                Else
                    Me.SetPdsCodRtyIdNull()
                End If
            End Set
        End Property

        ''' <summary>
        ''' Free Unit of Measure 
        ''' </summary>
        Public ReadOnly Property FreeUomCode() As CodeRow
            Get
                If Not Me.IsPdsCodFreeUomIdNull AndAlso ProductDataSet.Code.FindById(Me.PdsCodFreeUomId) IsNot Nothing Then
                    Return ProductDataSet.Code.FindById(Me.PdsCodFreeUomId)
                Else
                    Return Me.UomCode
                End If
            End Get
        End Property

        Public ReadOnly Property FreeUom() As String
            Get
                If FreeUomCode IsNot Nothing Then Return FreeUomCode.CodCode Else Return Nothing
            End Get
        End Property

        Public Property FreeUomId() As String
            Get
                If Not Me.IsPdsCodFreeUomIdNull Then
                    Return Me.PdsCodFreeUomId
                Else
                    Return Nothing
                End If
            End Get
            Set(ByVal value As String)
                If Not String.IsNullOrEmpty(value) Then
                    Me.PdsCodFreeUomId = value
                Else
                    Me.SetPdsCodFreeUomIdNull()
                End If
            End Set
        End Property

        Public Property FreeQty() As Decimal
            Get
                If Not Me.IsPdsFreeQtyNull Then Return Me.PdsFreeQty Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsFreeQty = value
            End Set
        End Property

        Public Property FromDayOfWeek() As Decimal
            Get
                If Not Me.IsPdsFromDayOfWeekNull Then Return Me.PdsFromDayOfWeek Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsFromDayOfWeek = value
            End Set
        End Property

        Public Property ToDayOfWeek() As Decimal
            Get
                If Not Me.IsPdsToDayOfWeekNull Then Return Me.PdsToDayOfWeek Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsToDayOfWeek = value
            End Set
        End Property

        Public Property FromHire() As Decimal
            Get
                If Not Me.IsPdsFromHireNull Then Return Me.PdsFromHire Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsFromHire = value
            End Set
        End Property

        Public Property ToHire() As Decimal
            Get
                If Not Me.IsPdsToHireNull Then Return Me.PdsToHire Else Return -999999999
            End Get
            Set(ByVal value As Decimal)
                Me.PdsToHire = value
            End Set
        End Property

        Public Overrides Function ToString() As String
            Dim result As String = TypeDescription
            If Not String.IsNullOrEmpty(Me.QtyUomDescription) Then result += " : " & Me.QtyUomDescription
            If Not String.IsNullOrEmpty(Me.RateDescription) Then result += " @ " & Me.RateDescription
            If Not String.IsNullOrEmpty(Me.Description) Then result += " : " & Me.Description
            Return result
        End Function

        Public ReadOnly Property TypeDescription() As String
            Get

                ''REV:MIA JULY 13 2011 - REMOVE HARDCODED STUFF 
                Dim desc As String = Aurora.Common.Data.ExecuteScalarSP("ProductSaleable_GetFreeChargeDiscountText", Me.DiscountType())
                If (String.IsNullOrEmpty(desc)) Then
                    Return ""
                Else
                    Return desc
                End If

                
                'If Me.DiscountType() = ProductConstants.DiscountType_FOC Then
                '    Return "Free of Charge"
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_LHD Then
                '    Return "Long Hire Discount"
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_DBT Then
                '    Return "Days Before Travel"
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_PDR Then
                '    Return "Person Dependent Discount"
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_DDT Then
                '    Return "Day Dependent Discount"
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_FRR Then
                '    Return "Fixed Rate Reductions"
                'Else
                '    Return ""
                'End If
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String
            Get
                If Me.DiscountType() = ProductConstants.DiscountType_FOC Then
                    Return CodeRow.DescribeQty(Me.UomCode, Me.PdsQty)
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_LHD Then
                    Return CodeRow.DescribeQty(Me.UomCode, Me.PdsQty)
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_DBT Then
                    Return Me.PdsQty.ToString("0")
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_PDR Then
                    Return Me.PdsQty.ToString("0")
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_DDT Then
                    Return ""
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_FRR Then
                    Return Me.FromHire.ToString("#.####") & " - " & Me.ToHire.ToString("#.####")
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String
            Get
                If Me.DiscountType() = ProductConstants.DiscountType_FOC Then
                    Return CodeRow.DescribeUom(Me.UomCode, Me.PdsQty)
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_LHD Then
                    Return CodeRow.DescribeUom(Me.UomCode, Me.PdsQty)
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_DBT Then
                    Return "Day" & IIf(Me.PdsQty <> 1, "(s)", "")
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_PDR Then
                    Return "Person" & IIf(Me.PdsQty <> 1, "(s)", "")
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_DDT Then
                    Return ""
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_FRR Then
                    Return CodeRow.DescribeUom(Me.UomCode, 2)
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property QtyUomDescription() As String
            Get
                Return (QtyDescription & " " & UomDescription).Trim()
            End Get
        End Property

        Public ReadOnly Property RateDescription() As String
            Get
                'If Me.Amt = -999999999 _
                'Or Me.Percentage = -999999999 Then
                '    Return String.Empty
                'End If

                'If Me.DiscountType() = ProductConstants.DiscountType_FOC Then
                '    Return ""
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_LHD Then
                '    Return IIf(Me.Amt <> 0 OrElse Me.Percentage = 0, ProductConstants.RateToString(Me.Amt), ProductConstants.PercentToString(Me.Percentage))
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_DBT Then
                '    Return IIf(Me.Amt <> 0 OrElse Me.Percentage = 0, ProductConstants.RateToString(Me.Amt), ProductConstants.PercentToString(Me.Percentage))
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_PDR Then
                '    Return IIf(Me.Amt <> 0 OrElse Me.Percentage = 0, ProductConstants.RateToString(Me.Amt), ProductConstants.PercentToString(Me.Percentage))
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_DDT Then
                '    Return IIf(Me.Amt <> 0 OrElse Me.Percentage = 0, ProductConstants.RateToString(Me.Amt), ProductConstants.PercentToString(Me.Percentage))
                'ElseIf Me.DiscountType() = ProductConstants.DiscountType_FRR Then
                '    Return IIf(Me.Amt <> 0 OrElse Me.Percentage = 0, ProductConstants.RateToString(Me.Amt), ProductConstants.PercentToString(Me.Percentage))
                'Else
                '    Return ""
                'End If

                Select Case Me.DiscountType()
                    Case ProductConstants.DiscountType_FOC
                        Return String.Empty
                    Case ProductConstants.DiscountType_LHD, ProductConstants.DiscountType_DBT, ProductConstants.DiscountType_PDR, ProductConstants.DiscountType_DDT, ProductConstants.DiscountType_FRR
                        
                        If Me.Amt = -999999999 Then
                            ' there is no amount
                            If Me.Percentage <> -999999999 Then
                                ' there is no amount, but there is a percentage
                                Return ProductConstants.PercentToString(Me.Percentage)
                            Else
                                ' there is no amount and no percentage
                                Return String.Empty
                            End If
                        Else
                            ' there is an amount 
                            If Me.Percentage <> -999999999 Then
                                ' there is an amount, and there is a percentage
                                ' percentage wins :)
                                Return ProductConstants.PercentToString(Me.Percentage)
                            Else
                                ' there is an amount but no percentage
                                Return ProductConstants.RateToString(Me.Amt)
                            End If
                        End If

                    Case Else
                        Return String.Empty
                End Select

            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                If Me.DiscountType() = ProductConstants.DiscountType_FOC Then
                    If Me.DiscountProductRowByProductRateDiscount_Free IsNot Nothing Then
                        Return CodeRow.DescribeQtyUom(Me.FreeUomCode, Me.FreeQty) & " of " & Me.DiscountProductRowByProductRateDiscount_Free.PrdShortName & " Free"
                    Else
                        Return ""
                    End If
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_LHD Then
                    Return ""
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_DBT Then
                    Return ""
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_PDR Then
                    Return ""
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_DDT Then
                    Return "From: " & WeekdayName(Me.FromDayOfWeek, False, FirstDayOfWeek.Monday) & " to " & WeekdayName(Me.ToDayOfWeek, False, FirstDayOfWeek.Monday)
                ElseIf Me.DiscountType() = ProductConstants.DiscountType_FRR Then
                    Return ""
                Else
                    Return ""
                End If
            End Get
        End Property

    End Class

End Class
