

Partial Class ProductDataSet

    Partial Class TravelBandSetRow

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return "Effective from " & Me.TbsEffDateTime.ToString(Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") & ", added by " & Me.AddUsrId
            End Get
        End Property

        ''' <summary>
        ''' Unit of Measure 
        ''' </summary>
        Public ReadOnly Property UomCode() As CodeRow
            Get
                Return ProductDataSet.Code.FindById(Me.TbsCodUomId)
            End Get
        End Property

        Public ReadOnly Property Uom() As String
            Get
                If UomCode IsNot Nothing Then Return UomCode.CodDesc Else Return Nothing
            End Get
        End Property

        Public ReadOnly Property ShortDescription() As String
            Get
                Return Me.TbsEffDateTime.ToString(Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") & " - " & Me.AddUsrId
            End Get
        End Property

        Public ReadOnly Property IsFuture(ByVal currentDate As Date) As Boolean
            Get
                Return Me.TbsEffDateTime > currentDate
            End Get
        End Property

        Public ReadOnly Property IsCurrent(ByVal currentDate As Date) As Boolean
            Get
                Return Not Me.IsFuture(currentDate) AndAlso Me Is Me.SaleableProductRow.CurrentTravelBandSetRow(currentDate)
            End Get
        End Property

        Public ReadOnly Property IsPast(ByVal currentDate As Date) As Boolean
            Get
                Return Not Me.IsFuture(currentDate) AndAlso Not Me.IsCurrent(currentDate)
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If Me.IsFuture(currentDate) Then
                    Return ProductConstants.FutureColor
                ElseIf Me.IsCurrent(currentDate) Then
                    Return ProductConstants.CurrentColor
                Else
                    Return ProductConstants.PastColor
                End If
            End Get
        End Property

        Public ReadOnly Property CurrentProductRateTravelBandRow(ByVal currentDate As Date) As ProductRateTravelBandRow
            Get
                ' loop through all the rate bands, future to past, determining the status till we find ours
                ' the first travel date we find less then our current date is the current rate band, and the default, 
                ' the rest are past which we ignore

                Dim productRateTravelBandRows As ProductRateTravelBandRow() = Me.GetProductRateTravelBandRows()
                Dim i As Integer
                For i = productRateTravelBandRows.Length - 1 To 0 Step -1
                    Dim productRateTravelBandRow As ProductRateTravelBandRow = productRateTravelBandRows(i)
                    If productRateTravelBandRow.PtbTravelFromDate <= currentDate Then
                        Return productRateTravelBandRow
                    End If
                Next

                Return Nothing
            End Get
        End Property


    End Class

End Class
