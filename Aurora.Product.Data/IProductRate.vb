Partial Class ProductDataSet


    Public Interface IProductRate

        Property Id() As String
        Property ParentPtbId() As String

        Property Qty() As Decimal
        Property Rate() As Decimal
        Property UomId() As String

        ReadOnly Property UomCode() As CodeRow
        ReadOnly Property Uom() As String

        ReadOnly Property QtyDescription() As String
        ReadOnly Property UomDescription() As String
        ReadOnly Property Description() As String

        Function ToString() As String
    End Interface

End Class