Partial Public Class ProductDataSet
    Partial Class VehicleDependentRateRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.VdrId
            End Get
            Set(ByVal value As String)
                Me.VdrId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.VdrPtbId
            End Get
            Set(ByVal value As String)
                Me.VdrPtbId = value
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return Me.VdrQty
            End Get
            Set(ByVal value As Decimal)
                Me.VdrQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                If Not Me.IsVdrRateNull Then Return Me.VdrRate Else Return 0
            End Get
            Set(ByVal value As Decimal)
                Me.VdrRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                If Not Me.IsVdrCodUomIdNull AndAlso ProductDataSet.Code.FindById(Me.VdrCodUomId) IsNot Nothing Then
                    Return Me.VdrCodUomId
                Else
                    Return Me.ProductRateTravelBandRow.UomId
                End If
            End Get
            Set(ByVal value As String)
                If Not String.IsNullOrEmpty(value) Then Me.VdrCodUomId = value Else Me.SetVdrPrdIdNull()
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                If Not Me.IsVdrCodUomIdNull AndAlso ProductDataSet.Code.FindById(Me.VdrCodUomId) IsNot Nothing Then
                    Return ProductDataSet.Code.FindById(Me.VdrCodUomId)
                Else
                    Return Me.ProductRateTravelBandRow.UomCode
                End If
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                If UomCode IsNot Nothing Then Return UomCode.CodDesc Else Return Nothing
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Dim result As String = Me.VehicleShortName & " - " & Me.VehicleName
                If Me.MinChg <> 0 OrElse Me.MaxChg <> 0 Then
                    If Me.MinChg <> 0 Then
                        result += ".  Min Charge $" & Me.MinChg.ToString("0.00") & ", Max Charge $" & Me.MaxChg.ToString("0.00")
                    Else
                        result += ".  Max Charge $" & Me.MaxChg.ToString("0.00")
                    End If
                End If
                Return result
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public ReadOnly Property VehicleShortName() As String
            Get
                If Me.VehicleProductRow IsNot Nothing Then Return Me.VehicleProductRow.PrdShortName Else Return ""
            End Get
        End Property

        Public ReadOnly Property VehicleName() As String
            Get
                If Me.VehicleProductRow IsNot Nothing Then Return Me.VehicleProductRow.PrdName Else Return ""
            End Get
        End Property

        Public ReadOnly Property MinChg() As Decimal
            Get
                If Not Me.IsVdrMinChgNull Then Return Me.VdrMinChg Else Return 0
            End Get
        End Property

        Public ReadOnly Property MaxChg() As Decimal
            Get
                If Not Me.IsVdrMaxChgNull Then Return Me.VdrMaxChg Else Return 0
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return VehicleShortName & " - " & CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function

    End Class
End Class
