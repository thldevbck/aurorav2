Imports System.Text

Partial Class ProductDataSet

    Partial Class ProductRateTravelBandRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.PtbId
            End Get
            Set(ByVal value As String)
                Me.PtbId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.PtbId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return Me.PtbQty
            End Get
            Set(ByVal value As Decimal)
                Me.PtbQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                If Not Me.IsPtbBaseRateNull Then
                    Return Me.PtbBaseRate
                Else
                    Return 0
                End If
            End Get
            Set(ByVal value As Decimal)
                Me.PtbBaseRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                If Not Me.IsptbCodUomIdNull Then Return Me.ptbCodUomId Else Return Nothing
            End Get
            Set(ByVal value As String)
                If Not String.IsNullOrEmpty(value) Then Me.ptbCodUomId = value Else Me.SetptbCodUomIdNull()
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                If Not Me.IsptbCodUomIdNull AndAlso ProductDataSet.Code.FindById(Me.ptbCodUomId) IsNot Nothing Then
                    Return ProductDataSet.Code.FindById(Me.ptbCodUomId)
                Else
                    Return Me.TravelBandSetRow.UomCode
                End If
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                If UomCode IsNot Nothing Then Return UomCode.CodDesc Else Return Nothing
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return ""
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function

        Public ReadOnly Property LongDescription() As String
            Get
                Return "Travel from " & Me.PtbTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) & ", added by " & Me.AddUsrId
            End Get
        End Property

        Public ReadOnly Property ShortDescription() As String
            Get
                Return Me.PtbTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) & " - " & Me.AddUsrId
            End Get
        End Property

        ''' <summary>
        ''' Describe a discount for a given Product Rate Travel Band and parent entity Id
        ''' </summary>
        Public Function GetProductRateDiscountForParent(ByVal parentId As String) As ProductRateDiscountRow()
            Dim result As New List(Of ProductRateDiscountRow)
            For Each productRateDiscountRow As ProductRateDiscountRow In Me.GetProductRateDiscountRows
                If productRateDiscountRow.PdsParentId = parentId Then
                    result.Add(productRateDiscountRow)
                End If
            Next
            Return result.ToArray()
        End Function

        Public Function GetProductRates() As IProductRate()
            Dim result As New List(Of IProductRate)
            result.Add(Me)
            result.AddRange(Me.GetAgentDependentRateRows())
            result.AddRange(Me.GetVehicleDependentRateRows())
            For Each productRateBookingBandRow As ProductRateBookingBandRow In Me.GetProductRateBookingBandRows()
                result.Add(productRateBookingBandRow)
                result.AddRange(productRateBookingBandRow.GetLocationDependentRateRows())
            Next
            result.AddRange(Me.GetPersonDependentRateRows())
            result.AddRange(Me.GetFixedRateRows())
            result.AddRange(Me.GetPersonRateRows())
            Return result.ToArray()
        End Function

        Private Function BuildDiscountsHtml(ByVal parentId As String) As String

            Dim result As New StringBuilder()

            Dim productRateDiscounts As ProductRateDiscountRow() = Me.GetProductRateDiscountForParent(parentId)
            If productRateDiscounts.Length > 0 Then
                result.AppendLine()
                result.AppendLine(vbTab & vbTab & "<ul>")
                For Each productRateDiscountRow As ProductRateDiscountRow In productRateDiscounts
                    result.AppendLine(vbTab & vbTab & vbTab & "<li>" & System.Web.HttpUtility.HtmlEncode(productRateDiscountRow.ToString()) & "</li>")
                Next
                result.AppendLine(vbTab & vbTab & "</ul>")
            End If

            Return result.ToString()
        End Function

        Private Function BuildRateSummeryRateHtml(ByVal productRate As IProductRate) As String
            Return "<li>" _
             & System.Web.HttpUtility.HtmlEncode(productRate.ToString()) _
             & BuildDiscountsHtml(productRate.Id) _
             & "</li>"
        End Function

        Private Function BuildRateSummeryRatesHtml(ByVal productRates As IProductRate()) As String
            Dim result As New StringBuilder()
            For Each productRate As IProductRate In productRates
                result.AppendLine(BuildRateSummeryRateHtml(productRate))
            Next
            Return result.ToString()
        End Function

        Public Function BuildRateSummeryHtml() As String

            Dim result As New StringBuilder()

            result.AppendLine("<ul>")

            result.AppendLine(BuildRateSummeryRateHtml(Me))
            result.AppendLine(BuildRateSummeryRatesHtml(Me.GetPersonRateRows()))
            result.AppendLine(BuildRateSummeryRatesHtml(Me.GetVehicleDependentRateRows()))
            For Each productRateBookingBandRow As ProductRateBookingBandRow In Me.GetProductRateBookingBandRows()
                result.AppendLine(BuildRateSummeryRateHtml(productRateBookingBandRow))
                For Each locationDependentRateRow As LocationDependentRateRow In productRateBookingBandRow.GetLocationDependentRateRows()
                    result.AppendLine(BuildRateSummeryRateHtml(locationDependentRateRow))
                Next
            Next
            result.AppendLine(BuildRateSummeryRatesHtml(Me.GetFixedRateRows()))
            result.AppendLine("</ul>")

            Return result.ToString()
        End Function

        Public ReadOnly Property IsFuture(ByVal currentDate As Date) As Boolean
            Get
                Return Me.PtbTravelFromDate > currentDate
            End Get
        End Property

        Public ReadOnly Property IsCurrent(ByVal currentDate As Date) As Boolean
            Get
                Return Not Me.IsFuture(currentDate) AndAlso Me Is Me.TravelBandSetRow.CurrentProductRateTravelBandRow(currentDate)
            End Get
        End Property

        Public ReadOnly Property IsPast(ByVal currentDate As Date) As Boolean
            Get
                Return Not Me.IsFuture(currentDate) AndAlso Not Me.IsCurrent(currentDate)
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If Not Me.TravelBandSetRow.IsCurrent(currentDate) Then
                    Return ProductConstants.InactiveColor
                ElseIf Me.IsFuture(currentDate) Then
                    Return ProductConstants.FutureColor
                ElseIf Me.IsCurrent(currentDate) Then
                    Return ProductConstants.CurrentColor
                Else
                    Return ProductConstants.PastColor
                End If
            End Get
        End Property


    End Class

End Class
