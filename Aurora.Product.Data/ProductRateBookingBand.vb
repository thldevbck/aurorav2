Partial Public Class ProductDataSet
    Partial Public Class ProductRateBookingBandRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.BapId
            End Get
            Set(ByVal value As String)
                Me.BapId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.BapPtbId
            End Get
            Set(ByVal value As String)
                Me.BapPtbId = value
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                Return Me.BapQty
            End Get
            Set(ByVal value As Decimal)
                Me.BapQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                Return Me.BapRate
            End Get
            Set(ByVal value As Decimal)
                Me.BapRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                Return Me.ProductRateTravelBandRow.UomId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                Return Me.ProductRateTravelBandRow.UomCode
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                Return Me.ProductRateTravelBandRow.Uom
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return "From " & Me.BapBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) _
                 & " - " & Me.BapBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return _
             "Booked From " & Me.BapBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) _
             & " To " & Me.BapBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) _
             & " - " _
             & CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function

    End Class
End Class
