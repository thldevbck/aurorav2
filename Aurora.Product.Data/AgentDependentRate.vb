Partial Public Class ProductDataSet
    Partial Class AgentDependentRateRow
        Implements IProductRate

        Public Property Id() As String Implements IProductRate.Id
            Get
                Return Me.AdrId
            End Get
            Set(ByVal value As String)
                Me.AdrId = value
            End Set
        End Property

        Public Property ParentPtbId() As String Implements IProductRate.ParentPtbId
            Get
                Return Me.AdrPtbId
            End Get
            Set(ByVal value As String)
                Me.AdrPtbId = value
            End Set
        End Property

        Public Property Qty() As Decimal Implements IProductRate.Qty
            Get
                If Not Me.IsAdrQtyNull Then Return Me.AdrQty Else Return 0
            End Get
            Set(ByVal value As Decimal)
                Me.AdrQty = value
            End Set
        End Property

        Public Property Rate() As Decimal Implements IProductRate.Rate
            Get
                Return Me.AdrRate
            End Get
            Set(ByVal value As Decimal)
                Me.AdrRate = value
            End Set
        End Property

        Public Property UomId() As String Implements IProductRate.UomId
            Get
                Return Me.ProductRateTravelBandRow.UomId
            End Get
            Set(ByVal value As String)
            End Set
        End Property

        Public ReadOnly Property UomCode() As CodeRow Implements IProductRate.UomCode
            Get
                Return Me.ProductRateTravelBandRow.UomCode
            End Get
        End Property

        Public ReadOnly Property Uom() As String Implements IProductRate.Uom
            Get
                Return Me.ProductRateTravelBandRow.Uom
            End Get
        End Property

        Public ReadOnly Property Description() As String Implements IProductRate.Description
            Get
                Return Me.AgentCode & " - " & Me.AgentName
            End Get
        End Property

        Public ReadOnly Property QtyDescription() As String Implements IProductRate.QtyDescription
            Get
                Return CodeRow.DescribeQty(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property UomDescription() As String Implements IProductRate.UomDescription
            Get
                Return CodeRow.DescribeUom(Me.UomCode, Me.Qty)
            End Get
        End Property

        Public ReadOnly Property ProductDataSet() As ProductDataSet
            Get
                Return CType(Me.Table.DataSet, ProductDataSet)
            End Get
        End Property

        Public ReadOnly Property AgentCode() As String
            Get
                If Me.AgentRow IsNot Nothing Then Return Me.AgentRow.AgnCode Else Return ""
            End Get
        End Property

        Public ReadOnly Property AgentName() As String
            Get
                If Me.AgentRow IsNot Nothing Then Return Me.AgentRow.AgnName Else Return ""
            End Get
        End Property

        Public Overrides Function ToString() As String Implements IProductRate.ToString
            Return Me.AgentCode & " - " & CodeRow.DescribeQtyUomRate(Me.UomCode, Me.Qty, Me.Rate)
        End Function


    End Class
End Class
