Imports System.Text

Partial Public Class ProductDataSet
    Partial Class SaleableProductRow

        Public ReadOnly Property IsActive() As Boolean
            Get
                Return Me.SapStatus = "Active"
            End Get
        End Property

        Public ReadOnly Property IsPending() As Boolean
            Get
                Return Me.SapStatus = "Pending"
            End Get
        End Property

        Public ReadOnly Property IsInactive() As Boolean
            Get
                Return Not Me.IsActive AndAlso Not Me.IsPending
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.ProductRow.PrdShortName & "-" & Me.SapSuffix.ToString()
            End Get
        End Property

        Public ReadOnly Property HasFuture(ByVal currentDate As Date)
            Get
                If IsActive Then
                    For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows()
                        If packageProductRow.PackageRow IsNot Nothing _
                         AndAlso packageProductRow.PackageRow.IsActive _
                         AndAlso packageProductRow.PackageRow.IsFuture(currentDate) Then
                            Return True
                        End If
                    Next
                End If

                Return False
            End Get
        End Property

        Public ReadOnly Property HasCurrent(ByVal currentDate As Date)
            Get
                If IsActive Then
                    If Me.GetPackageProductRows().Length = 0 Then Return True

                    For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows()
                        If packageProductRow.PackageRow IsNot Nothing _
                         AndAlso packageProductRow.PackageRow.IsActive _
                         AndAlso packageProductRow.PackageRow.IsCurrent(currentDate) Then
                            Return True
                        End If
                    Next
                End If

                Return False
            End Get
        End Property

        Public ReadOnly Property HasPast(ByVal currentDate As Date)
            Get
                If IsActive Then
                    For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows()
                        If packageProductRow.PackageRow IsNot Nothing _
                         AndAlso packageProductRow.PackageRow.IsActive _
                         AndAlso packageProductRow.PackageRow.IsPast(currentDate) Then
                            Return True
                        End If
                    Next
                End If

                Return False
            End Get
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If IsActive Then
                    Return ProductConstants.ActiveColor
                Else
                    Return ProductConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property StatusColor(ByVal currentDate As Date) As System.Drawing.Color
            Get
                If IsActive AndAlso HasCurrent(currentDate) Then
                    Return ProductConstants.CurrentColor
                ElseIf IsActive AndAlso HasFuture(currentDate) Then
                    Return ProductConstants.FutureColor
                ElseIf IsActive AndAlso HasPast(currentDate) Then
                    Return ProductConstants.PastColor
                ElseIf IsActive Then
                    Return ProductConstants.ActiveColor
                Else
                    Return ProductConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property StatusText(ByVal currentDate As Date) As String
            Get
                If IsActive AndAlso HasCurrent(currentDate) Then
                    Return "Active / Current"
                ElseIf IsActive AndAlso HasFuture(currentDate) Then
                    Return "Active / Future"
                ElseIf IsActive AndAlso HasPast(currentDate) Then
                    Return "Active / Past"
                ElseIf IsActive Then
                    Return "Active"
                ElseIf IsPending Then
                    Return "Pending"
                Else
                    Return "Inactive"
                End If
            End Get
        End Property

        Public Function GetProductAttributeSummaryHtml() As String
            Dim result As New StringBuilder()
            result.Append("<ul>")
            For Each productAttributeRow As ProductAttributeRow In Me.GetProductAttributeRows()
                If productAttributeRow.AttributeRow IsNot Nothing Then
                    result.Append("<li>" & System.Web.HttpUtility.HtmlEncode(productAttributeRow.ToString()) & "</li>")
                End If
            Next
            result.Append("</ul>")

            Return result.ToString()
        End Function


        Public ReadOnly Property PackageProductRowByPkgId(ByVal pkgId As String) As PackageProductRow
            Get
                For Each packageProductRow As PackageProductRow In Me.GetPackageProductRows
                    If packageProductRow.PplPkgId = pkgId Then
                        Return packageProductRow
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property CurrentTravelBandSetRow(ByVal currentDate As Date) As TravelBandSetRow
            Get
                ' loop through all the rate sets, future to past, determining the status till we find ours
                ' the first effective date we find less then our current date is the current rate set, and the default, 
                ' the rest are past which we ignore

                Dim travelBandSetRows As TravelBandSetRow() = Me.GetTravelBandSetRows()
                Dim i As Integer
                For i = travelBandSetRows.Length - 1 To 0 Step -1
                    Dim travelBandSetRow As TravelBandSetRow = travelBandSetRows(i)
                    If travelBandSetRow.TbsEffDateTime <= currentDate Then
                        Return travelBandSetRow
                    End If
                Next

                Return Nothing
            End Get
        End Property

    End Class

End Class
