
Partial Public Class ProductDataSet
    Partial Class ProductRow

        Public ReadOnly Property Description() As String
            Get
                Return Me.PrdShortName + " - " & Me.PrdName
            End Get
        End Property

        Public ReadOnly Property LongDescription() As String
            Get
                Return Me.BrandRow.BrdCode & " - " & Me.BrandRow.BrdName & " - " & Me.TypeRow.TypCode & " - " & Me.PrdShortName + " - " & Me.PrdName
            End Get
        End Property

        Public Property IsActive() As Boolean
            Get
                Return Not Me.IsPrdIsActiveNull AndAlso Me.PrdIsActive
            End Get
            Set(ByVal value As Boolean)
                Me.PrdIsActive = value
            End Set
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If IsActive Then
                    Return ProductConstants.ActiveColor
                Else
                    Return ProductConstants.InactiveColor
                End If
            End Get
        End Property


     
    End Class

End Class
