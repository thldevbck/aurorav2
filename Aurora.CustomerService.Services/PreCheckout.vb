﻿Imports System.Text
Imports System.Xml

Public Class PreCheckout
#Region "Precheckout - rev:mia July 9 2012"

    Public Shared Function GetPreCheckoutData(ByVal RntCkoLocCode As String) As DataSet
        Return Aurora.CustomerService.Data.PreCheckoutData(RntCkoLocCode)
    End Function

    Public Shared Function GetPreCheckoutDateScheduledRun(ByVal RntCkoLocCode As String) As DataSet
        Return Aurora.CustomerService.Data.PreCheckoutDateScheduledRun(RntCkoLocCode)
    End Function

    Public Shared Function PreCheckoutResetSchedule(ByVal RntCkoLocCode As String) As String
        Return Aurora.CustomerService.Data.PreCheckoutResetSchedule(RntCkoLocCode)
    End Function

    Public Shared Function GetPreCheckOutNotes(bookingId As String) As String
        Dim ds As DataSet = Aurora.CustomerService.Data.GetPreCheckOutNotes(bookingId)

        Dim sb As New StringBuilder
        sb.AppendFormat("<h3>{0}</h3>", bookingId)
        sb.Append("<table border='1'><tr><td><b>DESCRIPTION</b></td><td><b>TIME</b></td></tr>")
        sb.AppendFormat("<tr><td colspan='2'>{0}</td><td></td></tr>", "<hr/>")
        Dim row As DataRow
        If (ds.Tables.Count - 1) <> -1 Then

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                row = ds.Tables(0).Rows(i)
                sb.AppendFormat("<tr><td>{0}</td><td valign='top'>{1}</td></tr>", CleanUpInvalidContent(row.Item(1).ToString), row.Item(2).ToString)
                If (i > 0) Then
                    sb.AppendFormat("<tr><td colspan='2'>{0}</td><td></td></tr>", "<hr/>")
                End If
            Next
            sb.Append("</table>")
        Else
            sb.Append("</table>")
            Return sb.ToString
        End If
        Return sb.ToString
    End Function

    Public Shared Function PreCheckoutResetButtonActivation(ByVal RntCkoLocCode As String) As String
        Return Aurora.CustomerService.Data.PreCheckoutResetButtonActivation(RntCkoLocCode)
    End Function

    Private Shared Function CleanUpInvalidContent(content As String) As String
        If (Not String.IsNullOrEmpty(content)) Then
            content = content.Replace("/", "")
            content = content.Replace("\", "")
            content = content.Replace(">", "&gt;")
            content = content.Replace("<", "&lt;")
        End If
        Return content
    End Function
#End Region

#Region "Rental Agreement"
    Private Shared Function ListOfRentalID(sListOfRentalID As String) As ArrayList
        If (String.IsNullOrEmpty(sListOfRentalID)) Then
            Return Nothing
        End If

        Dim listRentalID As String() = sListOfRentalID.Trim.Split(",")
        Dim RentalIDarray As New ArrayList
        RentalIDarray.AddRange(listRentalID)
        Return RentalIDarray
    End Function

    Private Shared Function GetRentalAgreement(RntId As String) As String
        Dim xmlDoc As XmlDocument = Aurora.CustomerService.Data.GetRentalAgreement(RntId)
        If (xmlDoc Is Nothing) Then
            Return String.Concat("Error: ", RntId, " Can't generate XML ")
        End If
        If (Not xmlDoc Is Nothing) Then
            If (xmlDoc.OuterXml.ToUpper.Equals("<data></data>".ToUpper)) Then
                Return String.Concat("Error: ", RntId, " has no Rental Agreement saved")
            End If
        End If

        Return xmlDoc.SelectSingleNode("data/Data").OuterXml
    End Function

    Public Shared Function BuildRentalAgreement(sListOfRentalID As String) As String
        Dim sb As New StringBuilder
        Try

            If (ListOfRentalID(sListOfRentalID) Is Nothing) Then
                Return "Error: No Rental Id"
            End If

            Dim listRentalID As ArrayList = ListOfRentalID(sListOfRentalID)

            Dim result As String
            If (Not listRentalID Is Nothing) Then
                For Each obj As Object In listRentalID
                    result = GetRentalAgreement(obj)
                    If (result.IndexOf("Error") = -1) Then
                        sb.Append(result)
                    End If
                Next
            End If
            If (listRentalID.Count - 1 <> -1) Then
                sb.Insert(0, "<data>").Insert(sb.ToString.Length, "</data>")
            End If


        Catch ex As Exception
            Return "Error: " & ex.Message
        End Try
        Return sb.ToString

    End Function


#End Region

End Class
