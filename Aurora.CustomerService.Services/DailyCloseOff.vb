Imports System.IO
Imports System.Xml
Imports Aurora.CustomerService.Data

Public Class DailyCloseOff

    Public Shared Function GetDailyCloseOffFloat(ByVal UserCode As String) As XmlDocument
        Return DataRepository.GetDailyCloseOffFloat(UserCode)
    End Function

    Public Shared Function StartDailyCloseOff(ByVal UserCode As String, ByVal CloseOffDate As String) As XmlDocument

        Dim dtCODate As DateTime
        dtCODate = DateValue(Mid$(CloseOffDate, InStr(1, Trim$(CloseOffDate), " ") + 1))

        '  Return DataRepository.StartDailyCloseOff(UserCode, dtCODate)

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.StartDailyCloseOff(UserCode, dtCODate)
                If Not xmlReturn.DocumentElement.InnerText.Contains("GEN122") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN122") & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using

    End Function

    Public Shared Function SaveDailyCloseOffFloat(ByVal ScreenData As String) As XmlDocument

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.SaveDailyCloseOffFloat(ScreenData)
                If Not xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using




    End Function

    Public Shared Function GetDailyCloseOffAccountReconciliation(ByVal LocationId As String) As XmlDocument
        Return DataRepository.GetDailyCloseOffAccountReconciliation(LocationId)
    End Function

    Public Shared Function SaveDailyCloseOffAccountReconciliationData(ByVal DCOARDATA As String) As XmlDocument

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.SaveDailyCloseOffAccountReconciliationData(DCOARDATA)
                If Not xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using

    End Function

    Public Shared Function GetDailyCloseOffPaymentReconcilliation(ByVal LocationId As String, ByVal UserCode As String) As XmlDocument
        'get_DailyCloseOff_PayRec
        Return DataRepository.GetDailyCloseOffPaymentReconcilliation(LocationId, UserCode)
    End Function

    Public Shared Function SaveDailyCloseOffPaymentReconciliationData(ByVal LocationId As String, _
                                                               ByVal ChequeDrawn As Double, _
                                                               ByVal CashBanked As Double, _
                                                               ByVal Difference As Double, _
                                                               ByVal IntegrityNumber As Integer, _
                                                               ByVal ChequeBanked As String, _
                                                               ByVal TotalBanking As Double, _
                                                               ByVal UserCode As String) As XmlDocument

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.SaveDailyCloseOffPaymentReconciliationData(LocationId, _
                                                                                        ChequeDrawn, _
                                                                                        CashBanked, _
                                                                                        Difference, _
                                                                                        IntegrityNumber, _
                                                                                        ChequeBanked, _
                                                                                        TotalBanking, _
                                                                                        UserCode)


                If Not xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using




    End Function

    Public Shared Function ReconcileDailyCloseOff(ByVal LocationId As String) As XmlDocument
        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.ReconcileDailyCloseOff(LocationId)


                If Not xmlReturn.DocumentElement.InnerText.Contains("GEN127") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN127") & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using
    End Function

    Public Shared Function CancelDailyCloseOff(ByVal LocationId As String) As XmlDocument
        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.CancelDailyCloseOff(LocationId)


                If Not xmlReturn.DocumentElement.InnerText.Contains("GEN078") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN078") & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using
    End Function

#Region "Daily Close Off Reports"

    Public Shared Function GetPaymentTransactionReportByMethod(ByVal UserCode As String, ByVal ReportDate As String) As XmlDocument
        Return DataRepository.GetPaymentTransactionReportByMethod(UserCode, ReportDate)
    End Function

    Public Shared Function GetPaymentTransactionReportByType(ByVal UserCode As String, ByVal ReportDate As String) As XmlDocument
        Return DataRepository.GetPaymentTransactionReportByType(UserCode, ReportDate)
    End Function

    'Public Shared Function SaveFile(ByVal Path As String, ByVal FileName As String, ByVal Text As String, ByVal Extension As String) As Boolean
    '    If File.Exists(Path & FileName & "." & Extension) Then
    '        Return False
    '    Else
    '        File.WriteAllText(Path & FileName & "." & Extension, Text)
    '        Return True
    '    End If
    'End Function

    Public Shared Function GetDCOReportsPath() As String
        Return DataRepository.GetDCOReportsPath()
    End Function

#End Region

    ' function to get the user's mail id
    Public Shared Function GetUsersEMailId(ByVal UserCode As String) As String
        Dim oXml As XmlDocument
        oXml = DataRepository.GetUsersEMailId(UserCode)
        If Not oXml.DocumentElement.FirstChild Is Nothing AndAlso oXml.DocumentElement.FirstChild.Name = "UserInfo" Then
            If Not oXml.DocumentElement.FirstChild.FirstChild Is Nothing AndAlso oXml.DocumentElement.FirstChild.FirstChild.Name = "EmailId" Then
                ' its the path 
                '<UserInfo>
                '  <EmailId>Shoel.Palli@thlonline.com</EmailId>
                '</UserInfo>
                Return oXml.DocumentElement.FirstChild.FirstChild.InnerText
            Else
                Return String.Empty
            End If
        Else
            Return String.Empty
        End If
    End Function



End Class
