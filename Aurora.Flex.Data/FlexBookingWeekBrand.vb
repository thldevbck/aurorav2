Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class FlexBookingWeekBrandRow

        Public ReadOnly Property StatusText() As String
            Get
                Return FbbStatus + " (version: " + FbbVersion.ToString() + ")"
            End Get
        End Property

        Public ReadOnly Property StatusColor() As Color
            Get
                Return FlexConstants.BookingWeekStatusToColor(Me.FbbStatus)
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return FbbBrdCode & " - " & FbbBrdName
            End Get
        End Property

        Public ReadOnly Property IsCreated() As Boolean
            Get
                Return FbbStatus = FlexConstants.BookingWeekStatus_Created
            End Get
        End Property

        Public ReadOnly Property IsApproved() As Boolean
            Get
                Return FbbStatus = FlexConstants.BookingWeekStatus_Approved
            End Get
        End Property

        Public ReadOnly Property IsRejected() As Boolean
            Get
                Return FbbStatus = FlexConstants.BookingWeekStatus_Rejected
            End Get
        End Property

        Public ReadOnly Property IsSent() As Boolean
            Get
                Return FbbStatus = FlexConstants.BookingWeekStatus_Sent
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean
            Get
                Return (Not IsCreated AndAlso Not IsRejected)
            End Get
        End Property

        Public ReadOnly Property CanApprove() As Boolean
            Get
                Return IsCreated OrElse IsRejected
            End Get
        End Property

        Public ReadOnly Property CanReject() As Boolean
            Get
                Return IsApproved OrElse IsSent
            End Get
        End Property

        Public ReadOnly Property CanFinalize() As Boolean
            Get
                Return IsApproved
            End Get
        End Property

    End Class

End Class