Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class FlexWeekNumberRow
        ''' <summary>
        ''' Automatically calculate the TravelToDate
        ''' </summary>
        Public ReadOnly Property FwnTrvWkEnd() As Date
            Get
                Return FlexConstants.NextTravelStart(FwnTrvWkStart).AddDays(-1)
            End Get
        End Property

        ''' <summary>
        ''' Is the week in the past?  (the travel date is before the booking week)
        ''' </summary>
        Public ReadOnly Property IsPast(ByVal bookStart As Date) As Boolean
            Get
                Return FwnTrvWkEnd < bookStart
            End Get
        End Property

        ''' <summary>
        ''' Is the week current?  (the travel date is within the booking week)
        ''' </summary>
        Public ReadOnly Property IsCurrent(ByVal bookStart As Date) As Boolean
            Get
                Return FwnTrvWkEnd >= bookStart _
                 AndAlso FwnTrvWkStart < (bookStart + New TimeSpan(7, 0, 0, 0))
            End Get
        End Property

        ''' <summary>
        ''' Is the week in the future?  (the travel date is after the booking week)
        ''' </summary>
        Public ReadOnly Property IsFuture(ByVal bookStart As Date) As Boolean
            Get
                Return FwnTrvWkStart >= (bookStart + New TimeSpan(7, 0, 0, 0))
            End Get
        End Property

    End Class

End Class
