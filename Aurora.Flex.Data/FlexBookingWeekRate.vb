Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class FlexBookingWeekRateRow
        ''' <summary>
        ''' Automatically calculate the TravelToDate
        ''' </summary>
        Public ReadOnly Property FlrTravelTo() As Date
            Get
                Return FlexConstants.NextTravelStart(FlrTravelFrom).AddDays(-1)
            End Get
        End Property


        ''' <summary>
        ''' Is the rate in the past?  (the travel date is before the booking week)
        ''' </summary>
        Public ReadOnly Property IsPast(ByVal bookStart As Date) As Boolean
            Get
                Return FlrTravelTo < bookStart
            End Get
        End Property

        ''' <summary>
        ''' Is the rate current?  (the travel date is within the booking week)
        ''' </summary>
        Public ReadOnly Property IsCurrent(ByVal bookStart As Date) As Boolean
            Get
                Return FlrTravelTo >= bookStart _
                 AndAlso FlrTravelFrom < (bookStart + New TimeSpan(7, 0, 0, 0))
            End Get
        End Property

        ''' <summary>
        ''' Is the rate in the future?  (the travel date is after the booking week)
        ''' </summary>
        Public ReadOnly Property IsFuture(ByVal bookStart As Date) As Boolean
            Get
                Return FlrTravelFrom >= (bookStart + New TimeSpan(7, 0, 0, 0))
            End Get
        End Property
    End Class

End Class