Imports System.Drawing

Imports Aurora.Common

Public Class FlexConstants
    Inherits DataConstants

    Public Shared ReadOnly FlexErrorColor As Color = ColorTranslator.FromHtml("#FFCCCC")

    Public Const WeekNoMin As Integer = 1
    ''rev:mia 10sept2015-I have created new travel date as 28-Mar-2015 week number 53 and it's not working on staging
    Public Shared GlobalWeekNoMax As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("WeekNoMax").ToString())
    Public Shared GlobalWeekCount As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("WeekCount").ToString())

    Public Const WeekNoMax As Integer = 53
    Public Const WeekCount As Integer = 53

    Public Const ExceptionMax As Integer = 50

    Public Const FlexNumDefault As Integer = 11 ' A1

    Public Const FlexIntMin As Integer = 0
    Public Const FlexIntMax As Integer = 63
    Public Const FlexIntCount As Integer = 64

    Public Shared ReadOnly FlexNum_All() As Integer = { _
        11, 12, 13, 14, 15, 16, 17, 18, _
        21, 22, 23, 24, 25, 26, 27, 28, _
        31, 32, 33, 34, 35, 36, 37, 38, _
        41, 42, 43, 44, 45, 46, 47, 48, _
        51, 52, 53, 54, 55, 56, 57, 58, _
        61, 62, 63, 64, 65, 66, 67, 68, _
        71, 72, 73, 74, 75, 76, 77, 78, _
        81, 82, 83, 84, 85, 86, 87, 88}

    Public Const FlexChangeMin As Integer = -20
    Public Const FlexChangeMax As Integer = 20

    Public Const BookingWeekStatus_Created As String = "created"
    Public Const BookingWeekStatus_Approved As String = "approved"
    Public Const BookingWeekStatus_Rejected As String = "rejected"
    Public Const BookingWeekStatus_Sent As String = "sent"
    Public Const BookingWeekStatus_None As String = "none"

    Public Shared ReadOnly BookingWeekStatus_All As String() = {BookingWeekStatus_Created, BookingWeekStatus_Approved, BookingWeekStatus_Rejected, BookingWeekStatus_Sent, BookingWeekStatus_None}
    Public Shared ReadOnly BookingWeekStatus_Colors As Color() = { _
        ColorTranslator.FromHtml("#DDFFDD"), _
        ColorTranslator.FromHtml("#DDDDFF"), _
        ColorTranslator.FromHtml("#FFDDDD"), _
        ColorTranslator.FromHtml("#9999DD"), _
        ColorTranslator.FromHtml("#CCCCCC")}

    Public Shared Function BookingWeekStatusToColor(ByVal status As String) As Color
        Dim index As Integer = Array.IndexOf(BookingWeekStatus_All, status)
        If index = -1 Then
            Return BookingWeekStatus_Colors(Array.IndexOf(BookingWeekStatus_All, BookingWeekStatus_None))
        Else
            Return BookingWeekStatus_Colors(index)
        End If
    End Function

    Public Const ProductStatus_Approved As String = "approved"
    Public Const ProductStatus_NotApproved As String = "not approved"

    Public Shared ReadOnly ProductStatus_ApprovedColor As Color = ColorTranslator.FromHtml("#BBEEBB")
    Public Shared ReadOnly ProductStatus_NotApprovedColor As Color = ColorTranslator.FromHtml("#EEBBBB")

    Public Shared ReadOnly FlexRateChangedColor As Color = ColorTranslator.FromHtml("#CCFFCC")
    Public Shared ReadOnly FlexRateReadonlyColor As Color = ColorTranslator.FromHtml("#DDDDDD")


    Public Shared Function FlexChangeToColor(ByVal change As Integer) As Color
        If change < 0 Then
            Return Color.FromArgb(255 + change * 8, 255 + change * 8, 255)
        ElseIf change > 0 Then
            Return Color.FromArgb(255 - change * 8, 255, 255 - change * 8)
        Else
            Return Color.White
        End If
    End Function

    Public Shared Function ValidateFlexRate(ByVal flexRate As String, ByRef flexNum As Integer) As Boolean
        Try
            flexRate = ("" + flexRate).Trim().ToUpper()
            If flexRate.Length <> 2 Then
                Throw New Exception("Invalid Flex Rate")
            End If

            Dim digit0 As Integer = Asc(flexRate(0)) - Asc("A") + 1
            Dim digit1 As Integer = CInt(flexRate(1).ToString())
            If digit0 < 1 OrElse digit0 > 8 OrElse digit1 < 1 OrElse digit1 > 8 Then
                Throw New Exception("Invalid Flex Rate")
            End If

            flexNum = digit0 * 10 + digit1

            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function ValidateFlexRate(ByVal flexRate As String) As Boolean
        Dim flexNum As Integer
        Return ValidateFlexRate(flexRate, flexNum)
    End Function

    Public Shared Function FlexRateToNum(ByVal flexRate As String) As Integer
        Try
            Dim flexNum As Integer
            If Not ValidateFlexRate(flexRate, flexNum) Then
                Throw New Exception("Invalid Flex Rate")
            End If

            Return flexNum
        Catch
            Return Integer.MinValue
        End Try
    End Function

    Public Shared Function ValidateFlexNum(ByVal flexNum As Integer, ByRef digit0 As Integer, ByRef digit1 As Integer) As Boolean
        Try
            Dim flexNumString As String = flexNum.ToString()
            If flexNumString.Length <> 2 Then
                Throw New Exception("Invalid FlexNum")
            End If

            digit0 = Int(flexNum / 10)
            digit1 = flexNum Mod 10
            If digit0 < 1 OrElse digit0 > 8 OrElse digit1 < 1 OrElse digit1 > 8 Then
                Throw New Exception("Invalid FlexNum")
            End If

            Return True
        Catch
            Return False
        End Try
    End Function

    Public Shared Function ValidateFlexNum(ByVal flexNum As Integer) As Boolean
        Dim digit0 As Integer
        Dim digit1 As Integer
        Return ValidateFlexNum(flexNum, digit0, digit1)
    End Function

    Public Shared Function FlexNumToRate(ByVal flexNum As Integer) As String
        Try
            Dim digit0 As Integer
            Dim digit1 As Integer
            If Not ValidateFlexNum(flexNum, digit0, digit1) Then
                Throw New Exception("Invalid FlexNum")
            End If

            Return Chr(Asc("A") + digit0 - 1) + digit1.ToString()
        Catch ex As Exception
            Return ""
        End Try

    End Function

    Public Shared Function FlexNumToInt(ByVal flexNum As Integer) As Integer
        Try
            Dim digit0 As Integer
            Dim digit1 As Integer
            If Not ValidateFlexNum(flexNum, digit0, digit1) Then
                Throw New Exception("Invalid FlexNum")
            End If

            Return (digit0 - 1) * 8 + (digit1 - 1)
        Catch ex As Exception
            Return Integer.MinValue
        End Try
    End Function

    Public Shared Function FlexIntToColor(ByVal flexInt As Integer) As Color
        Return Color.FromArgb(255 - flexInt * 3, 255 - flexInt * 3, 255)
    End Function


    Public Shared Function FlexNumToColor(ByVal flexNum As Integer) As Color
        Return FlexIntToColor(FlexNumToInt(flexNum))
    End Function

    Public Shared Function FlexIntToFlexNum(ByVal flexInt As Integer) As Integer
        Return (flexInt \ 8 + 1) * 10 + (flexInt Mod 8 + 1)
    End Function

    Public Shared Function FlexIntToRate(ByVal flexInt As Integer) As String
        Return FlexNumToRate(FlexIntToFlexNum(flexInt))
    End Function

    Public Enum FlexExportFileNameParams
        TravelYY
        TravelYYYY
        TravelYYYYMMDD
        BookYYYYMMDD
    End Enum

End Class
