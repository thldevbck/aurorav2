Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class ExportContactsRow

        Public ReadOnly Property IsAgent() As Boolean
            Get
                Return Not Me.IsAgcEmailNull
            End Get
        End Property

        Public ReadOnly Property IsUser() As Boolean
            Get
                Return Not Me.IsUsrEmailNull
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                If Me.IsEmailNull Then
                    Return ""
                End If

                Dim result As String = Me.Email
                If Not Me.IsNameNull() AndAlso Not String.IsNullOrEmpty(("" & Me.Name).Trim()) Then
                    result += "<" + Me.Name + ">"
                End If
                Return result
            End Get
        End Property

    End Class

End Class