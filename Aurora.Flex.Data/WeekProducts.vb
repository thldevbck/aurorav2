Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class WeekProductsRow

        Public ReadOnly Property FbwBookEnd() As Date
            Get
                Return FbwBookStart.AddDays(6)
            End Get
        End Property

        Public ReadOnly Property TravelYearStart() As Date
            Get
                Return Me.TravelYear
            End Get
        End Property

        Public ReadOnly Property TravelYearEnd() As Date
            Get
                Return New Date(Me.TravelYear.Year + 1, 3, 31)
            End Get
        End Property

        Public ReadOnly Property CanApprove() As Boolean
            Get
                Return True ' FlpStatus doesnt matter, since product can be "re-approved"
            End Get
        End Property

        Public ReadOnly Property CanReject() As Boolean
            Get
                Return FlpStatus = FlexConstants.BookingWeekStatus_Approved
            End Get
        End Property

        Public ReadOnly Property CanFinalize() As Boolean
            Get
                Return True
            End Get
        End Property

        Public ReadOnly Property IsApproved() As Boolean
            Get
                Return FlpStatus = FlexConstants.BookingWeekStatus_Approved
            End Get
        End Property

        Public ReadOnly Property IsUnApproved() As Boolean
            Get
                Return Not IsApproved
            End Get
        End Property

        Public ReadOnly Property StatusText() As String
            Get
                Return FlpStatus
            End Get
        End Property

        Public ReadOnly Property StatusColor() As Color
            Get
                If IsApproved Then
                    Return FlexConstants.ProductStatus_ApprovedColor
                Else
                    Return FlexConstants.ProductStatus_NotApprovedColor
                End If
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return PrdShortName + " - " + PrdName
            End Get
        End Property

        Public ReadOnly Property LongDescription() As String
            Get
                Return CtyName + " - " + BrdName + " - " + PrdShortName + " - " + PrdName + " for " + TravelYear.Year.ToString()
            End Get
        End Property

        Public ReadOnly Property SortKey() As String
            Get
                Return TravelYear.Year.ToString() + "," + CtyName + "," + BrdName + "," + PrdShortName
            End Get
        End Property

    End Class

End Class