Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class FlexBookingWeekRow

        Public ReadOnly Property FbwBookEnd() As Date
            Get
                Return FbwBookStart.AddDays(6)
            End Get
        End Property

        Public ReadOnly Property StatusText() As String
            Get
                Return FbwStatus + " (version: " + FbwVersion.ToString() + ")"
            End Get
        End Property

        Public ReadOnly Property StatusColor() As Color
            Get
                Return FlexConstants.BookingWeekStatusToColor(Me.FbwStatus)
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return FbwBookStart.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            End Get
        End Property

        Public ReadOnly Property DirectoryName() As String
            Get
                Return FbwBookStart.ToString("yyyy-MM-dd")
            End Get
        End Property

        Public ReadOnly Property IsCreated() As Boolean
            Get
                Return FbwStatus = FlexConstants.BookingWeekStatus_Created
            End Get
        End Property

        Public ReadOnly Property IsApproved() As Boolean
            Get
                Return FbwStatus = FlexConstants.BookingWeekStatus_Approved
            End Get
        End Property

        Public ReadOnly Property IsRejected() As Boolean
            Get
                Return FbwStatus = FlexConstants.BookingWeekStatus_Rejected
            End Get
        End Property

        Public ReadOnly Property IsSent() As Boolean
            Get
                Return FbwStatus = FlexConstants.BookingWeekStatus_Sent
            End Get
        End Property

        Public ReadOnly Property IsPast() As Boolean
            Get
                Return Date.Now.Date > FbwBookEnd
            End Get
        End Property

        Public ReadOnly Property IsCurrent() As Boolean
            Get
                Dim today As Date = DateTime.Now.Date
                Return today >= FbwBookStart AndAlso today <= FbwBookEnd
            End Get
        End Property

        Public ReadOnly Property IsFuture() As Boolean
            Get
                Return Date.Now.Date < FbwBookStart
            End Get
        End Property

        Public ReadOnly Property IsReadOnly() As Boolean
            Get
                Return (Not IsCreated AndAlso Not IsRejected) OrElse IsPast
            End Get
        End Property

        Public ReadOnly Property CanApprove() As Boolean
            Get
                Return (IsCreated OrElse IsRejected) AndAlso Not IsPast
            End Get
        End Property

        Public ReadOnly Property CanReject() As Boolean
            Get
                Return (IsApproved OrElse IsSent) AndAlso Not IsPast
            End Get
        End Property

        Public ReadOnly Property CanFinalize() As Boolean
            Get
                Return IsApproved
            End Get
        End Property

    End Class

End Class