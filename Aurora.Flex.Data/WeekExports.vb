Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class WeekExportsRow

        Public ReadOnly Property IsReadonly() As Boolean
            Get
                Return FwxStatus <> FlexConstants.BookingWeekStatus_Created _
                 AndAlso FwxStatus <> FlexConstants.BookingWeekStatus_Rejected _
                 AndAlso FwxStatus <> FlexConstants.BookingWeekStatus_Approved
            End Get
        End Property

        Public ReadOnly Property BookingWeekStatusColor() As Color
            Get
                Return FlexConstants.BookingWeekStatusToColor(Me.FbwStatus)
            End Get
        End Property

        Public ReadOnly Property BookingWeekDescription() As String
            Get
                Return FbwBookStart.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) + " - " + FbwStatus + " (version: " + FbwVersion.ToString() + ")"
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Dim result As String = FlxCode + " - " + FlxName
                If Not Me.IsFwxTravelYearNull() Then
                    result += " for " + FwxTravelYear.Year.ToString()
                End If
                Return result
            End Get
        End Property

        Public ReadOnly Property FileName() As String
            Get
                Dim result As String = Me.FlxFileName
                If Not IsFwxTravelYearNull() Then
                    result = result _
                        .Replace("{" + FlexConstants.FlexExportFileNameParams.TravelYY.ToString() + "}", Me.FwxTravelYear.ToString("yy")) _
                        .Replace("{" + FlexConstants.FlexExportFileNameParams.TravelYYYY.ToString() + "}", Me.FwxTravelYear.ToString("yyyy")) _
                        .Replace("{" + FlexConstants.FlexExportFileNameParams.TravelYYYYMMDD.ToString() + "}", Me.FwxTravelYear.ToString("yyyyMMdd"))
                End If
                result = result.Replace("{" + FlexConstants.FlexExportFileNameParams.BookYYYYMMDD.ToString() + "}", Me.FbwBookStart.ToString("yyyyMMdd"))
                Return result
            End Get
        End Property

        Public ReadOnly Property StatusColor() As Color
            Get
                If Me.ProductCount = 0 Then
                    Return FlexConstants.BookingWeekStatusToColor(FlexConstants.BookingWeekStatus_None)
                Else
                    Return FlexConstants.BookingWeekStatusToColor(FwxStatus)
                End If
            End Get
        End Property

        Public ReadOnly Property StatusText() As String
            Get
                If Me.ProductCount = 0 Then
                    Return "no products"
                Else
                    Return FwxStatus
                End If
            End Get
        End Property

        Public ReadOnly Property IsSent() As Boolean
            Get
                Return FwxStatus = FlexConstants.BookingWeekStatus_Sent
            End Get
        End Property

        Public ReadOnly Property IsUnsent() As Boolean
            Get
                Return FwxStatus <> FlexConstants.BookingWeekStatus_Sent
            End Get
        End Property

        Public ReadOnly Property ScheduleDescription(ByVal locHoursBehindServerLocation As Double) As String
            Get
                If Me.ProductCount = 0 Then
                    Return ""
                ElseIf IsUnsent() Then
                    Dim dt As Date = FwxScheduledTime
                    Dim ts As TimeSpan = dt - Date.Now
                    If (ts.Ticks > 0) Then
                        Return dt.AddHours(-locHoursBehindServerLocation).ToString(Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") + "  (in " + CInt(Math.Floor(ts.TotalHours)).ToString() + ":" + ts.Minutes.ToString().PadLeft(2, "0") + ")"
                    Else
                        Return "*asap*"
                    End If
                Else
                    Return "sent"
                End If
            End Get
        End Property

        Public ReadOnly Property CanFinalize() As Boolean
            Get
                Return FwxStatus = FlexConstants.BookingWeekStatus_Approved _
                 AndAlso ProductCount > 0 _
                 AndAlso FwxScheduledTime <= Date.Now
            End Get
        End Property

    End Class

End Class