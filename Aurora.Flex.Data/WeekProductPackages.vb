Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class WeekProductPackagesRow

        Public ReadOnly Property BookedDateDesc() As String
            Get
                Return PkgBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) + " - " + PkgBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            End Get
        End Property

        Public ReadOnly Property TravelDateDesc() As String
            Get
                Return PkgTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) + " - " + PkgTravelToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            End Get
        End Property

    End Class

End Class