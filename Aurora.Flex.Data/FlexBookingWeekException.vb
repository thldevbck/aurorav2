Imports System.Drawing

Partial Public Class FlexDataset

    Partial Class FlexBookingWeekExceptionRow

        Public ReadOnly Property LocationCode() As String
            Get
                If Not IsFleLocCodeFromNull() AndAlso IsFleLocCodeToNull() Then
                    Return FleLocCodeFrom
                ElseIf IsFleLocCodeFromNull() AndAlso Not IsFleLocCodeToNull() Then
                    Return FleLocCodeTo()
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property FromTo() As String
            Get
                If Not IsFleLocCodeFromNull() AndAlso IsFleLocCodeToNull() Then
                    Return "From"
                ElseIf IsFleLocCodeFromNull() AndAlso Not IsFleLocCodeToNull() Then
                    Return "To"
                Else
                    Return ""
                End If
            End Get
        End Property

        Public ReadOnly Property IsFrom() As Boolean
            Get
                If Not IsFleLocCodeFromNull() AndAlso IsFleLocCodeToNull() Then
                    Return True
                ElseIf IsFleLocCodeFromNull() AndAlso Not IsFleLocCodeToNull() Then
                    Return False
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property IsTo() As Boolean
            Get
                If Not IsFleLocCodeFromNull() AndAlso IsFleLocCodeToNull() Then
                    Return False
                ElseIf IsFleLocCodeFromNull() AndAlso Not IsFleLocCodeToNull() Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

    End Class

End Class