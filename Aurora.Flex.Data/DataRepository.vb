Imports System.Data
Imports System.Web
Imports System.Configuration

Imports Aurora.Flex.Data.FlexDataset

Public Module DataRepository

    '=======================================================================================================
    ' Travel Years & Weeks
    '=======================================================================================================

    Public Function GetFlexWeekNumbers(ByVal travelYear As Date) As FlexWeekNumberDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetFlexWeekNumbers", _
            New FlexWeekNumberDataTable(), _
            travelYear)
    End Function

    Public Function GetFlexWeekNumbersBetween(ByVal travelYearFrom As Date, ByVal travelYearTo As Date) As FlexWeekNumberDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetFlexWeekNumbersBetween", _
            New FlexWeekNumberDataTable(), _
            travelYearFrom, travelYearTo)
    End Function

    Public Function GetTravelYears() As TravelYearsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetTravelYears", _
            New TravelYearsDataTable())
    End Function

    '=======================================================================================================
    ' Locations
    '=======================================================================================================

    Public Function GetLocations(ByVal comCode As String, ByVal ctyCode As String) As LocationsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetLocations", _
            New LocationsDataTable(), _
            comCode, ctyCode)
    End Function

    Public Function GetLocationsExport(ByVal comCode As String, ByVal fwxId As Integer) As LocationsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetLocationsExport", _
            New LocationsDataTable(), _
            comCode, fwxId)
    End Function

    Public Function GetLocationAvailability(ByVal comCode As String, ByVal flpId As Integer) As WeekProductLocationAvailabilityDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekProductLocationAvailability", _
            New WeekProductLocationAvailabilityDataTable(), _
            comCode, flpId)
    End Function

    '=======================================================================================================
    ' Booking Weeks
    '=======================================================================================================

    Public Function GetBookingWeeks(ByVal comCode As String) As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetBookingWeeks", _
            New FlexBookingWeekDataTable(), _
            comCode)
    End Function

    Public Function GetBookingWeeksScheduler() As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetBookingWeeksScheduler", _
            New FlexBookingWeekDataTable())
    End Function

    Public Function GetBookingWeek(ByVal comCode As String, ByVal fbwId As Integer) As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetBookingWeek", _
            New FlexBookingWeekDataTable(), _
            comCode, fbwId)
    End Function

    ''' <summary>
    ''' REV:MIA JAN.25 2012
    ''' ADDITION OF WithDomesticFlex
    ''' </summary>
    ''' <param name="comCode"></param>
    ''' <param name="bookStart"></param>
    ''' <param name="travelYearStart"></param>
    ''' <param name="travelYearEnd"></param>
    ''' <param name="usrId"></param>
    ''' <param name="prgmName"></param>
    ''' <param name="WithDomesticFlex"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function CreateBookingWeek(ByVal comCode As String,
                                      ByVal bookStart As Date,
                                      ByVal travelYearStart As Date,
                                      ByVal travelYearEnd As Date,
                                      ByVal usrId As String,
                                      ByVal prgmName As String, _
                                      ByVal WithDomesticFlex As Boolean) As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_CreateBookingWeek", _
            New FlexBookingWeekDataTable(), _
            comCode, bookStart, travelYearStart, travelYearEnd, usrId, prgmName, WithDomesticFlex)

    End Function

    Public Function ApproveBookingWeek(ByVal fbwId As Integer, ByVal emailSubject As String, ByVal emailText As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_ApproveBookingWeek", _
            New FlexBookingWeekDataTable(), _
            fbwId, emailSubject, emailText, integrityNo, usrId)
    End Function

    Public Function RejectBookingWeek(ByVal fbwId As Integer, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_RejectBookingWeek", _
            New FlexBookingWeekDataTable(), _
            fbwId, integrityNo, usrId)
    End Function

    Public Function FinalizeBookingWeek(ByVal fbwId As Integer, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_FinalizeBookingWeek", _
            New FlexBookingWeekDataTable(), _
            fbwId, integrityNo, usrId)
    End Function

    '=======================================================================================================
    ' Brands
    '=======================================================================================================

    Public Function GetBookingWeekBrands(ByVal comCode As String, ByVal fbwId As Integer) As FlexBookingWeekBrandDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetBookingWeekBrands", _
            New FlexBookingWeekBrandDataTable(), _
            comCode, fbwId, Nothing)
    End Function

    Public Function GetBookingWeekBrand(ByVal comCode As String, ByVal fbbId As Integer) As FlexBookingWeekBrandDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetBookingWeekBrands", _
            New FlexBookingWeekBrandDataTable(), _
            comCode, Nothing, fbbId)
    End Function

    Public Function ApproveBookingWeekBrand(ByVal fbbId As Integer, ByVal emailSubject As String, ByVal emailText As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekBrandDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_ApproveBookingWeekBrand", _
            New FlexBookingWeekBrandDataTable(), _
            fbbId, emailSubject, emailText, integrityNo, usrId)
    End Function

    Public Function RejectBookingWeekBrand(ByVal fbbId As Integer, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekBrandDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_RejectBookingWeekBrand", _
            New FlexBookingWeekBrandDataTable(), _
            fbbId, integrityNo, usrId)
    End Function

    Public Function FinalizeBookingWeekBrand(ByVal fbbId As Integer, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekBrandDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_FinalizeBookingWeekBrand", _
            New FlexBookingWeekBrandDataTable(), _
            fbbId, integrityNo, usrId)
    End Function

    '=======================================================================================================
    ' Products (and related)
    '=======================================================================================================

    Public Function GetWeekProducts(ByVal comCode As String, ByVal flwId As Integer) As WeekProductsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekProducts", _
            New WeekProductsDataTable(), _
            comCode, flwId, DBNull.Value)
    End Function

    Public Function GetWeekProduct(ByVal comCode As String, ByVal flwId As Integer, ByVal flpId As Integer) As WeekProductsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekProducts", _
            New WeekProductsDataTable(), _
            comCode, flwId, flpId)
    End Function

    Public Function GetProductRates(ByVal flpId As Integer) As FlexBookingWeekRateDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetProductRates", _
            New FlexBookingWeekRateDataTable(), _
            flpId)
    End Function

    Public Function GetProductExceptions(ByVal flpId As Integer) As FlexBookingWeekExceptionDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetProductExceptions", _
            New FlexBookingWeekExceptionDataTable(), _
            flpId)
    End Function

    Public Function GetWeekProductPackages(ByVal comCode As String, ByVal flpId As Integer) As WeekProductPackagesDataTable
        'HACK: something in the table data is breaking the constraints, but i cant figure out why...hence EnforceContstaints = False
        Dim result As New FlexDataset
        result.EnforceConstraints = False
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekProductPackages", _
            result.WeekProductPackages, _
            comCode, flpId, DBNull.Value)
    End Function

    Public Function GetWeekProductPackage(ByVal comCode As String, ByVal flwId As Integer, ByVal flpId As Integer, ByVal pkgId As String) As WeekProductPackagesDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekProductPackages", _
            New WeekProductPackagesDataTable(), _
            comCode, flwId, flpId, pkgId)
    End Function

    Public Function GetFleetStatus(ByVal comCode As String, ByVal ctyCode As String, ByVal brdCode As String, ByVal travelYear As Date, ByVal prdId As String) As FleetStatusDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetFleetStatus", _
            New FleetStatusDataTable(), _
            comCode, ctyCode, brdCode, travelYear, prdId)
    End Function

    Public Function GetFlexLevels(ByVal pkgId As String, ByVal sapId As String, ByVal travelYrStart As Date) As FlexLevelDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetFlexLevels", _
            New FlexLevelDataTable(), _
            pkgId, sapId, travelYrStart)

    End Function

    ''------------------------------------------------------------------------------
    '' REV:MIA FEB12012 -- added Type
    ''------------------------------------------------------------------------------
    Public Function ApproveWeekProduct( _
     ByVal flpId As Integer, _
     ByVal integrityNo As Integer, _
     ByVal usrId As String, _
     ByVal Type As String) As FlexBookingWeekProductDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_ApproveWeekProduct", _
            New FlexBookingWeekProductDataTable(), _
            flpId, integrityNo, usrId, Type)
    End Function


    ''------------------------------------------------------------------------------
    '' REV:MIA FEB12012 -- ByVal WithDomesticFlex As Boolean
    ''------------------------------------------------------------------------------
    Public Function RejectWeekProduct( _
     ByVal flpId As Integer, _
     ByVal integrityNo As Integer, _
     ByVal usrId As String, _
     ByVal WithDomesticFlex As Boolean) As FlexBookingWeekProductDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_RejectWeekProduct", _
            New FlexBookingWeekProductDataTable(), _
            flpId, integrityNo, usrId, WithDomesticFlex)
    End Function

    Public Function FinalizeWeekProduct( _
     ByVal flpId As Integer, _
     ByVal locationsInclude As String, _
     ByVal locationsExclude As String, _
     ByVal integrityNo As Integer, _
     ByVal usrId As String) As FlexBookingWeekProductDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_FinalizeWeekProduct", _
            New FlexBookingWeekProductDataTable(), _
            flpId, locationsInclude, locationsExclude, integrityNo, usrId)
    End Function

    Public Function UpdateProductRate( _
     ByVal flrId As Integer, _
     ByVal flexNum As Integer, _
     ByVal changed As Boolean) As FlexBookingWeekRateDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_UpdateProductRate", _
            New FlexBookingWeekRateDataTable(), _
            flrId, flexNum, changed)
    End Function

    Public Function FinalizeProductRate( _
     ByVal flrId As Integer, _
     ByVal fleetStatus As String) As FlexBookingWeekRateDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_FinalizeProductRate", _
            New FlexBookingWeekRateDataTable(), _
            flrId, fleetStatus)
    End Function

    Public Function CreateUpdateProductException( _
     ByVal fleId As Nullable(Of Integer), _
     ByVal flpId As Integer, _
     ByVal bookStart As Date, _
     ByVal bookEnd As Date, _
     ByVal travelStart As Date, _
     ByVal travelEnd As Date, _
     ByVal locCodeFrom As String, _
     ByVal locCodeTo As String, _
     ByVal delta As Integer) As FlexBookingWeekExceptionDataTable
        Dim fleIdParam As Object
        If fleId.HasValue Then
            fleIdParam = fleId.Value
        Else
            fleIdParam = DBNull.Value
        End If

        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_CreateUpdateProductException", _
            New FlexBookingWeekExceptionDataTable(), _
            fleIdParam, _
            flpId, _
            bookStart, _
            bookEnd, _
            travelStart, _
            travelEnd, _
            IIf(Not locCodeFrom Is Nothing, locCodeFrom, DBNull.Value), _
            IIf(Not locCodeTo Is Nothing, locCodeTo, DBNull.Value), _
            delta)
    End Function

    Public Sub DeleteProductException(ByVal fleId As Integer)
        Aurora.Common.Data.ExecuteScalarSP("Flex2_DeleteProductException", fleId)
    End Sub

    '=======================================================================================================
    ' Exports (and related)
    '=======================================================================================================

    Public Function GetWeekExports(ByVal comCode As String, ByVal flwId As Integer) As WeekExportsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekExports", _
            New WeekExportsDataTable(), _
            comCode, flwId, DBNull.Value)
    End Function

    Public Function GetWeekExport(ByVal comCode As String, ByVal fwxId As Integer) As WeekExportsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekExports", _
            New WeekExportsDataTable(), _
            comCode, DBNull.Value, fwxId)
    End Function

    Public Function GetExportContacts(ByVal comCode As String, ByVal flxId As Integer) As ExportContactsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetExportContacts", _
            New ExportContactsDataTable(), _
            comCode, flxId, DBNull.Value, DBNull.Value, True)
    End Function

    Public Function GetWeekProductsExport(ByVal comCode As String, ByVal fwxId As Integer) As WeekProductsDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetWeekProductsExport", _
            New WeekProductsDataTable(), _
            comCode, fwxId)
    End Function

    Public Function GetFlexProducts(ByVal comCode As String) As FlexProductDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_GetFlexProducts", _
            New FlexProductDataTable(), _
            comCode)
    End Function

    Public Function UpdateWeekExport(ByVal fwxId As Integer, ByVal fwxScheduledTime As Date, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekExportDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_UpdateWeekExport", _
            New FlexBookingWeekExportDataTable(), _
            fwxId, fwxScheduledTime, integrityNo, usrId)
    End Function

    Public Function FinalizeWeekExport(ByVal fwxId As Integer, ByVal text As String, ByVal contactsDescription As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekExportDataTable
        Return Aurora.Common.Data.ExecuteTableSP( _
            "Flex2_FinalizeWeekExport", _
            New FlexBookingWeekExportDataTable(), _
            fwxId, text, contactsDescription, integrityNo, usrId)
    End Function

#Region "rev:mia March 15 2012 - Domestic Flex"
    Public Function HasDomesticType(ByVal flpId As Integer) As Boolean
        Dim retValue As String = ""
        Dim params(1) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("FlpId", DbType.Int32, flpId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("count", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)


            Aurora.Common.Data.ExecuteOutputSP("Flex2_GetDomesticNumber", params)
            retValue = params(1).Value

        Catch ex As Exception
            retValue = "ERROR/Failed to get Flex2_GetDomesticNumber"
            Return False
        End Try
        Return IIf(retValue = "0" Or CInt(retValue) = 0, False, True)
    End Function
#End Region

#Region "rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107"
    Public Function CopiedFlextProductTo(ComCode As String, _
                                         FlpIdOfProductCopied As Integer, _
                                         FlpIdOfProductThatCopied As Integer, _
                                         PrdIdOfProductCopied As String, _
                                         PrdIdOfProductThatCopied As String) As String

        Dim result As String = "OK"
        Try
            Dim params(5) As Aurora.Common.Data.Parameter

            params(0) = New Aurora.Common.Data.Parameter("ComCode", DbType.String, ComCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("FlpIdOfProductCopied", DbType.Int64, FlpIdOfProductCopied, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("FlpIdOfProductThatCopied", DbType.Int64, FlpIdOfProductThatCopied, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("PrdIdOfProductCopied", DbType.String, PrdIdOfProductCopied, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("PrdIdOfProductThatCopied", DbType.String, PrdIdOfProductThatCopied, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("result", DbType.String, 200, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Flex2_CopiedProductTo", params)
            result = params(5).Value
        Catch ex As Exception
            result = "ERROR: " & ex.Message
        End Try

        Return result
    End Function
#End Region
End Module






