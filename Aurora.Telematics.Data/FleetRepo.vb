﻿Imports System.Data.SqlClient
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Text
Imports System.IO

Public Class FleetRepo

    Sub UpdateFleetsGUIDs(dt As DataTable)
        Aurora.Common.Logging.LogInformation("Telematics.Data.UpdateFleetGUIDs", "Starting UpdateFleetGUIDs for [" + dt.Rows.Count().ToString() + "] Records")
        Dim database As Database = Aurora.Common.Data.GetDatabase()
        Dim sqlCmd As New SqlCommand("dbo.Fleet_UpdateGUIDs")
        sqlCmd.CommandType = CommandType.StoredProcedure
        Dim param As SqlParameter = sqlCmd.Parameters.AddWithValue("@TelematicsGUIDTable", dt)
        param.SqlDbType = SqlDbType.Structured
        Try
            Aurora.Common.Data.ExecuteNonQuery(database, sqlCmd)
            Aurora.Common.Logging.LogInformation("Telematics.Data.UpdateFleetGUIDs", "GUIDs updated successfully for [" + dt.Rows.Count().ToString() + "] Records")
        Catch ex As Exception
            Aurora.Common.Logging.LogInformation("Telematics.Data.UpdateFleetGUIDs", "Error Occured while accessing database from UpdateFleetGUIDs, Message=" + ex.Message)
        End Try

    End Sub

    Function DumpToCSV(dt As DataTable, filePath As String, delimiter As Char) As Integer
        'Dim sb As StringBuilder = New StringBuilder

        'For Each Dr As DataRow In dt.Rows
        '    sb.Append(Dr("UnitNumber").ToString + ":" + Dr("TelematicsGUID").ToString + ",")
        'Next
        Dim result = -1
        Try
            If File.Exists(filePath) Then
                File.Delete(filePath)
            End If
            'Always Overwrite.
            Dim objWriter = New StreamWriter(filePath)
            Dim stringBuilder = New StringBuilder()
            Dim columnCount As Integer = dt.Columns.Count
            For Each myCol As DataColumn In dt.Columns
                stringBuilder.Append(myCol.ColumnName)
                stringBuilder.Append(delimiter)
            Next
            stringBuilder.Append(vbLf)
            For Each row As DataRow In dt.Rows
                For Each myCol As DataColumn In dt.Columns
                    stringBuilder.Append(row(myCol))
                    stringBuilder.Append(delimiter)
                Next
                stringBuilder.Append(vbLf)
            Next
            stringBuilder.Append(vbLf)
            objWriter.Write(stringBuilder.ToString())
            objWriter.Close()
        Catch generatedExceptionName As Exception
            result = False
            Aurora.Common.Logging.LogInformation("Telematics.Data.DumpToCSV", "Error Occured while dumping to CSV, Message=" + generatedExceptionName.Message)
        End Try
        Return result
    End Function

End Class
