#Region "Imports"

Imports Aurora.ScheduleViewer.Data.DataRepository
Imports Aurora.ScheduleViewer.Services
Imports Aurora.Common
Imports System.Text
Imports System.Xml
Imports System.Collections
#End Region

Public Class Vehicle

    Private _locationList As List(Of String) = New List(Of String)
    Public ReadOnly Property LocationList() As List(Of String)
        Get
            Return _locationList
        End Get
    End Property

    Private _vehicleCollection As Collection
    Public ReadOnly Property VehicleCollection() As Collection
        Get
            Return _vehicleCollection
        End Get
    End Property

    Private _country As String
    Private _userCode As String
    Private _city As String

    Public Sub New(ByVal country As String, ByVal city As String, ByVal userCode As String)
        _country = country
        _city = city
        _userCode = userCode
    End Sub

    Public Function SetupVehicles() As XmlDocument
        Try

            'Call GetMyVehicles to retrieve vechicles
            Dim vehiclesDatatable As DataTable
            vehiclesDatatable = GetMyVehicles(_country, _userCode)

            ' Dim vehicleCollection As Collection = New Collection()
            _vehicleCollection = New Collection() '.Clear()
            'locationList = New List(Of String)
            _locationList = New List(Of String) '.Clear()

            'Building a xml document
            Dim xmlDoc As XmlDocument
            xmlDoc = New XmlDocument()
            'Vehicles node
            Dim vehiclesNode As XmlNode
            vehiclesNode = xmlDoc.CreateElement("Vehicles")

            Dim i As Integer = 0

            For Each dr As DataRow In vehiclesDatatable.Rows

                ' Retrieve values for data row
                Dim unitNumber As Integer = Utility.ParseInt(dr.Item("UnitNumber"), -999)
                Dim model As String = Trim(dr.Item("FleetModelCode"))
                Dim rego As String = Utility.ParseString(dr.Item("Rego"), "")
                Dim odo As Integer = Utility.ParseInt(dr.Item("LastOdometer"), 0)
                Dim nextDateTime As DateTime = dr.Item("NextDateTime")
                Dim firstOnFleetDate As DateTime = dr.Item("FirstOnFleetDate")
                Dim locationCode As String = Trim(Utility.ParseString(dr.Item("LocationCode")))
                Dim modelId As Integer = Utility.ParseInt(dr.Item("FleetModelId"), -1)

                If locationCode = _city Or _city = "" Then

                    Dim vehicleArray As ArrayList = New ArrayList()
                    vehicleArray.Insert(0, i)
                    vehicleArray.Insert(1, unitNumber)
                    vehicleArray.Insert(2, model)
                    vehicleArray.Insert(3, rego)
                    vehicleArray.Insert(4, odo)
                    vehicleArray.Insert(5, nextDateTime)
                    vehicleArray.Insert(6, firstOnFleetDate)
                    vehicleArray.Insert(7, locationCode)
                    vehicleArray.Insert(8, modelId)

                    ' Populate the vehicle collection
                    VehicleCollection.Add(vehicleArray)

                    'Vehicle node
                    Dim vehicleNode As XmlNode
                    vehicleNode = xmlDoc.CreateElement("Vehicle")

                    Dim idNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "Id", Utility.ParseInt(i))
                    Dim unitNumberNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "UnitNumber", unitNumber)
                    Dim fleetModelCodeNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "FleetModelCode", model)
                    Dim regoCodeNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "Rego", rego)
                    Dim lastOdometerNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "LastOdometer", odo)
                    Dim nextDateTimeNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "NextDateTime", nextDateTime)
                    Dim firstOnFleetDateNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "FirstOnFleetDate", firstOnFleetDate)
                    Dim locationCodeNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "LocationCode", locationCode)
                    Dim fleetModelIdNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "FleetModelId", modelId)

                    vehicleNode.AppendChild(idNode)
                    vehicleNode.AppendChild(unitNumberNode)
                    vehicleNode.AppendChild(fleetModelCodeNode)
                    vehicleNode.AppendChild(regoCodeNode)
                    vehicleNode.AppendChild(lastOdometerNode)
                    vehicleNode.AppendChild(nextDateTimeNode)
                    vehicleNode.AppendChild(firstOnFleetDateNode)
                    vehicleNode.AppendChild(locationCodeNode)
                    vehicleNode.AppendChild(fleetModelIdNode)

                    vehiclesNode.AppendChild(vehicleNode)

                    ' Populate the location list
                    If Not LocationList.Contains(Trim(locationCode)) Then
                        LocationList.Add(Trim(locationCode))
                    End If

                    i = i + 1
                End If
            Next

            xmlDoc.AppendChild(vehiclesNode)
            Return xmlDoc

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return New XmlDocument()
        End Try

    End Function

    Public Function GetVehicleStartingPosition() As XmlDocument
        'Building a xml document
        Dim xmlDoc As XmlDocument
        xmlDoc = New XmlDocument()
        Dim vehiclesNode As XmlNode
        vehiclesNode = xmlDoc.CreateElement("Vehicles")

        For i As Integer = 1 To vehicleCollection.Count

            Dim vehicleArray As ArrayList = vehicleCollection.Item(i)

            Dim id As Integer = vehicleArray.Item(0)
            Dim firstOnFleetDate As DateTime = vehicleArray.Item(6)
            Dim nextDateTime As DateTime = vehicleArray.Item(5)
            Dim locationCode As String = vehicleArray.Item(7)
            Dim iAm As Integer '= 1 commented by Shoel
            Dim dateIn As Long

            If firstOnFleetDate > Now.AddDays(-3) Then
                ' Paint a Coming on fleet line
                Dim tempDate As DateTime = Now.AddDays(-5)
                If firstOnFleetDate.AddHours(-12) > Now.AddDays(-5) Then
                    tempDate = firstOnFleetDate.AddHours(-12)
                End If
                dateIn = DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), tempDate)
            Else

                ' Paint an idle line to first available date
                Dim endLastActDate As DateTime = Now.AddDays(-5)

                If Now.AddDays(-5) < nextDateTime Then
                    endLastActDate = nextDateTime
                End If

                'If endLastActDate.Hour >= 12 Then
                '    iAm = 0
                'End If
                ' fix by Shoel
                If endLastActDate.Hour >= 12 Then
                    iAm = 1
                Else
                    iAm = 0
                End If
                ' fix by Shoel

                Dim tempDate As DateTime = Now.AddDays(-5)
                If nextDateTime > Now.AddDays(-5) Then
                    tempDate = nextDateTime
                End If
                dateIn = DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), tempDate)
            End If

            'Vehicle node
            Dim vehicleNode As XmlNode
            vehicleNode = xmlDoc.CreateElement("Vehicle")

            Dim dateOut As Long
            dateOut = DateDiff(DateInterval.Day, New DateTime(2000, 1, 1), Now.AddDays(-5))

            Dim idNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "Id", id)
            Dim exCityNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "ExCity", locationCode)
            Dim dateOutNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "DateOut", dateOut)
            Dim amOutNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "AmOut", 0)
            Dim toNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "ToCity", locationCode)
            Dim dateInNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "DateIn", dateIn)
            Dim amInNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "AmIn", iAm)
            Dim episodeNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "Episode", "")
            Dim episodeTypeNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "EpisodeType", "S")
            Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(xmlDoc, "Layer", 0)

            vehicleNode.AppendChild(idNode)
            vehicleNode.AppendChild(exCityNode)
            vehicleNode.AppendChild(dateOutNode)
            vehicleNode.AppendChild(amOutNode)
            vehicleNode.AppendChild(toNode)
            vehicleNode.AppendChild(dateInNode)
            vehicleNode.AppendChild(amInNode)
            vehicleNode.AppendChild(episodeNode)
            vehicleNode.AppendChild(episodeTypeNode)
            vehicleNode.AppendChild(layerNode)

            vehiclesNode.AppendChild(vehicleNode)
        Next

        xmlDoc.AppendChild(vehiclesNode)

        Return xmlDoc

    End Function

    Public Function GetModel(ByVal unitNum As Integer) As ArrayList
        Try
            Dim fleetModelId As Integer
            Dim fleetModelCode As String
            fleetModelCode = ""
            fleetModelId = 0

            For Each vehicleArray As ArrayList In vehicleCollection
                If vehicleArray.Item(1) = unitNum Then
                    fleetModelCode = vehicleArray.Item(2)
                    fleetModelId = vehicleArray.Item(8)
                    Exit For
                End If
            Next

            Dim output As ArrayList = New ArrayList()
            output.Insert(0, fleetModelId)
            output.Insert(1, fleetModelCode)
            Return output
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return Nothing
        End Try
    End Function

    Public Sub GetVehicleCities()
        Try
            LocationList.Clear()

            ' Populate the location list
            Dim dt As DataTable
            dt = GetMyVehicleCities(_country, _userCode)

            For Each dr As DataRow In dt.Rows
                Dim locationCode As String = Trim(dr.Item("LocationCode"))
                LocationList.Add(Trim(locationCode))
            Next
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try
    End Sub

    Public Function IsValidVehicle(ByVal unit As String) As Boolean

        If IsNothing(VehicleCollection) OrElse VehicleCollection.Count = 0 Then
            SetupVehicles()
        End If

        For Each al As ArrayList In vehicleCollection
            Dim vehicle As String = ""
            vehicle = Utility.ParseString(al.Item(1), "")
            If unit = vehicle Then
                Return True
            End If
        Next

        Return False

    End Function

End Class


