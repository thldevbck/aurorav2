#Region "Imports"
Imports Aurora.Common
Imports AuroraReservations
Imports System.Text
#End Region


Public Class Dvass

    Public Shared Function TestDvass(ByVal country As String, ByVal user As String, ByVal isShow As Boolean) As String
        Dim output As StringBuilder
        Try

            Dim reservation As AuroraReservations.auroraDvass
            Dim comStatus As COMDVASSMgrLib.comStatus = New COMDVASSMgrLib.comStatus()

            Dim numMonitors As Long
            Dim currentDate As Long
            Dim currentTime As Long
            Dim inSafeMode As Long

            reservation = New AuroraReservations.auroraDvass()
            If country = "AU" Then
                reservation.selectCountry(0)
            Else
                reservation.selectCountry(1)
            End If

            If isShow Then
                reservation.getDvassSystemState(numMonitors, currentDate, currentTime, inSafeMode, comStatus)
                output = New StringBuilder()
                output.Append("This process fixes the nominated rental to a specific vehicle or to one ")
                output.Append("of a list or range of vehicles. Leaving the vehicle list empty will unfix a rental ")
                output.Append("which has already been fixed.  The Note must be completed to indicate the reason ")
                output.Append("for doing it.")
                output.Append(Environment.NewLine & Environment.NewLine)
                output.Append("Current User: " & user & Environment.NewLine)
                output.Append("Country Code: " & country & ", ")
                output.Append("DVASS Available: " & Utility.ParseString(reservation.dvassIsAvailable("brits\aurora"), "") & Environment.NewLine)
                output.Append("DVASS Version: " & reservation.Version & Environment.NewLine)
                output.Append("DVASS state: " & reservation.dvassState & Environment.NewLine)
                output.Append("Monitors: " & Utility.ParseString(numMonitors, "") & ", Date: " & Utility.ParseString(currentDate, ""))
                output.Append(", Time: " & Utility.ParseString(currentTime, "") & ", Safe Mode: " & IIf(inSafeMode = 0, "No", "Yes"))
                output.Append(", Status: " & Utility.ParseString(comStatus.status, "") & Environment.NewLine)
                output.Append("Msg: " & Utility.ParseString(comStatus.msg, "") & Environment.NewLine)
                output.Append("Detail: " & Utility.ParseString(comStatus.detail, ""))
                Return output.ToString()
            Else
                Return reservation.dvassState
            End If

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ""
        End Try
    End Function

    Public Shared Function ForceIt(ByVal country As String, ByVal user As String, ByVal rental As String, ByVal vehicleList As String, ByVal note As String) As String
        Try

            Dim reservation As AuroraReservations.auroraDvass
            Dim xmlString As String

            If TestDvass(country, user, False) = "Running" Then
                ' create the object on the application server
                reservation = New AuroraReservations.auroraDvass()

                ' Make sure we are working in the right country
                If country = "AU" Then
                    reservation.selectCountry(0)
                Else
                    reservation.selectCountry(1)
                End If

                xmlString = "<Note><Id/><Desc>" + note + "</Desc><AudTyp>Internal</AudTyp><Pri>3</Pri><Act>1</Act><Int/></Note>"
                ForceIt = reservation.AssignVehicle(rental, vehicleList, xmlString, 1, user)
                If InStr(1, ForceIt, "GEN046") > 0 Then
                    Logging.LogInformation(My.Settings.AuroraFunctionCodeAttribute, "Force Successful. " & note)
                    Return "Force Successful"
                Else
                    Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Force Fail. " & note)
                    Return ""
                End If
            Else
                Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Sorry, there was a problem sending this, please try again later.  If the problem persists please contact the Help Desk. " & "TestDvass(" & user & ", False) is not running.")
                Return "Sorry, there was a problem sending this, please try again later.  If the problem persists please contact the Help Desk"
            End If
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return "Sorry, there was a problem sending this, please try again later.  If the problem persists please contact the Help Desk"
        End Try
    End Function


End Class
