#Region "Imports"
Imports Aurora.ScheduleViewer.Data.DataRepository
Imports Aurora.ScheduleViewer.Services
Imports Aurora.Common
Imports System.Text
Imports System.Xml
Imports System.Collections
#End Region

Public Class Current

    Private currentRentalCollection As Collection = New Collection()
    Private _currentRentalXmlDoc As XmlDocument
    Public ReadOnly Property CurrentRentalXmlDoc() As XmlDocument
        Get
            Return _currentRentalXmlDoc
        End Get
    End Property

    Private currentMaintCollection As Collection = New Collection()
    Private _currentMaintXmlDoc As XmlDocument
    Public ReadOnly Property CurrentMaintXmlDoc() As XmlDocument
        Get
            Return _currentMaintXmlDoc
        End Get
    End Property

    Private _country As String
    Private _userCode As String
    Private _vehicleObject As Vehicle

    Public Sub New(ByVal country As String, ByVal userCode As String, ByRef vehicleObject As Vehicle)
        _country = country
        _userCode = userCode
        _vehicleObject = vehicleObject
    End Sub

    Public Sub SetupCurrent()

        Try
            'Reading in Current Rentals....

            SetupMyCurrentRentals()

            ' Current Rentals
            'Adding Current Rentals....

            'Building a xml document
            _currentRentalXmlDoc = New XmlDocument()
            'Rentals Node 
            Dim currentRentalsNode As XmlNode
            currentRentalsNode = currentRentalXmlDoc.CreateElement("CurrentRentals")

            For Each sList As ArrayList In currentRentalCollection
                Dim vehicleIndex As Integer = -1

                For Each vList As ArrayList In _vehicleObject.VehicleCollection
                    If vList.Item(1) = sList.Item(1) Then
                        vehicleIndex = vList.Item(0)
                        Exit For
                    End If
                Next

                If vehicleIndex <> -1 Then

                    sList.Item(8) = vehicleIndex

                    'rental node
                    Dim currentRentalNode As XmlNode
                    currentRentalNode = currentRentalXmlDoc.CreateElement("CurrentRental")

                    Dim idNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "Id", vehicleIndex)
                    Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "FromLoc", sList.Item(2))
                    Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "FromDate", sList.Item(3))
                    Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "FromAmPm", sList.Item(4))
                    Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "ToLoc", sList.Item(5))
                    Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "ToDate", sList.Item(6))
                    Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "ToAmPm", sList.Item(7))
                    Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "Rental", sList.Item(0))
                    Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "EpType", sList.Item(10))
                    Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(currentRentalXmlDoc, "Layer", "1")

                    currentRentalNode.AppendChild(idNode)
                    currentRentalNode.AppendChild(fromLocNode)
                    currentRentalNode.AppendChild(fromDateNode)
                    currentRentalNode.AppendChild(fromAmPmNode)
                    currentRentalNode.AppendChild(toLocNode)
                    currentRentalNode.AppendChild(toDateNode)
                    currentRentalNode.AppendChild(toAmPmNode)
                    currentRentalNode.AppendChild(rentalNode)
                    currentRentalNode.AppendChild(epTypeNode)
                    currentRentalNode.AppendChild(layerNode)

                    currentRentalsNode.AppendChild(currentRentalNode)

                Else
                    Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Unable to find unit for Current Rental: " & Utility.ParseString(sList.Item(1), "") & " for rental: " & Utility.ParseString(sList.Item(0), ""))
                End If

            Next

            currentRentalXmlDoc.AppendChild(currentRentalsNode)


            'Reading in Current OffFleets....
            SetupMyCurrentMaint()
            'Adding Scheduled OffFleets....

            'Building a xml document
            _currentMaintXmlDoc = New XmlDocument()
            'Maints Node 
            Dim currentMaintsNode As XmlNode
            currentMaintsNode = currentMaintXmlDoc.CreateElement("CurrentMaints")

            For Each sList As ArrayList In currentMaintCollection
                Dim vehicleIndex As Integer = -1

                For Each vList As ArrayList In _vehicleObject.VehicleCollection
                    If vList.Item(1) = sList.Item(1) Then
                        vehicleIndex = vList.Item(0)
                        Exit For
                    End If
                Next

                If vehicleIndex <> -1 Then

                    sList.Item(8) = vehicleIndex

                    'rental node
                    Dim currentMaintNode As XmlNode
                    currentMaintNode = currentMaintXmlDoc.CreateElement("CurrentMaint")

                    Dim idNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "Id", vehicleIndex)
                    Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "FromLoc", sList.Item(2))
                    Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "FromDate", sList.Item(3))
                    Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "FromAmPm", sList.Item(4))
                    Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "ToLoc", sList.Item(5))
                    Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "ToDate", sList.Item(6))
                    Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "ToAmPm", sList.Item(7))
                    Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "Rental", sList.Item(0))
                    Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "EpType", sList.Item(10))
                    Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(currentMaintXmlDoc, "Layer", "1")

                    currentMaintNode.AppendChild(idNode)
                    currentMaintNode.AppendChild(fromLocNode)
                    currentMaintNode.AppendChild(fromDateNode)
                    currentMaintNode.AppendChild(fromAmPmNode)
                    currentMaintNode.AppendChild(toLocNode)
                    currentMaintNode.AppendChild(toDateNode)
                    currentMaintNode.AppendChild(toAmPmNode)
                    currentMaintNode.AppendChild(rentalNode)
                    currentMaintNode.AppendChild(epTypeNode)
                    currentMaintNode.AppendChild(layerNode)

                    currentMaintsNode.AppendChild(currentMaintNode)
                Else
                    Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Unable to find unit for OffFleet: " & Utility.ParseString(sList.Item(1), "") & " OffFleet: " & Utility.ParseString(sList.Item(0), ""))
                End If
            Next

            currentMaintXmlDoc.AppendChild(currentMaintsNode)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try
    End Sub

    Private Sub SetupMyCurrentRentals()

        Try

            currentRentalCollection = New Collection()

            'Reading in Current Bookings....

            Dim dt As DataTable
            dt = GetCurrentActivities(_country, _userCode)

            For Each dr As DataRow In dt.Rows

                Dim rental As String = Utility.ParseString(dr.Item("ExternalRef"), "")
                Dim unit As Integer = Utility.ParseInt(dr.Item("UnitNumber"), 0)
                Dim fromLoc As String = Utility.ParseString(dr.Item("StartLocation"), "")
                Dim fromDate As Integer = DateDiff("d", CDate("1-Jan-2000"), dr.Item("StartDateTime"))
                Dim fromAmPm As Integer = 0
                If DatePart("h", dr.Item("StartDateTime")) > 11 Then
                    fromAmPm = 1
                End If
                Dim toLoc As String = Utility.ParseString(dr.Item("EndLocation"), "")
                Dim toDate As Integer = DateDiff("d", CDate("1-Jan-2000"), dr.Item("EndDateTime"))
                Dim toAmPm As Integer = 0
                If DatePart("h", dr.Item("EndDateTime")) > 11 Then
                    toAmPm = 1
                End If

                Dim currentRentalArray As ArrayList = New ArrayList()
                currentRentalArray.Insert(0, rental)
                currentRentalArray.Insert(1, unit)
                currentRentalArray.Insert(2, fromLoc)
                currentRentalArray.Insert(3, fromDate)
                currentRentalArray.Insert(4, fromAmPm)
                currentRentalArray.Insert(5, toLoc)
                currentRentalArray.Insert(6, toDate)
                currentRentalArray.Insert(7, toAmPm)
                currentRentalArray.Insert(8, Nothing)
                currentRentalArray.Insert(9, Nothing)
                currentRentalArray.Insert(10, "b")

                ' Populate the currentRental collection
                currentRentalCollection.Add(currentRentalArray)

            Next
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try
    End Sub

    Private Sub SetupMyCurrentMaint()

        Try
            currentMaintCollection = New Collection()

            'Reading in Current OffFleets....

            Dim dt As DataTable
            dt = GetCurrentOffFleets(_country, _userCode)

            For Each dr As DataRow In dt.Rows

                Dim reference As String = Utility.ParseString(dr.Item("RepairType"), "")
                Dim unit As Integer = Utility.ParseInt(dr.Item("UnitNumber"), 0)

                Dim fromLoc As String = Trim(Utility.ParseString(dr.Item("LocationCode"), ""))
                Dim fromDate As Integer = DateDiff("d", CDate("1-Jan-2000"), dr.Item("StartDateTime"))
                Dim fromAmPm As Integer = 0
                If DatePart("h", dr.Item("StartDateTime")) > 11 Then
                    fromAmPm = 1
                End If
                Dim toLoc As String = Trim(Utility.ParseString(dr.Item("LocationCode"), ""))
                Dim toDate As Integer = DateDiff("d", CDate("1-Jan-2000"), dr.Item("EndDateTime"))
                Dim toAmPm As Integer = 0
                If DatePart("h", dr.Item("EndDateTime")) > 11 Then
                    toAmPm = 1
                End If


                Dim currentMaintArray As ArrayList = New ArrayList()
                currentMaintArray.Insert(0, reference)
                currentMaintArray.Insert(1, unit)
                currentMaintArray.Insert(2, fromLoc)
                currentMaintArray.Insert(3, fromDate)
                currentMaintArray.Insert(4, fromAmPm)
                currentMaintArray.Insert(5, toLoc)
                currentMaintArray.Insert(6, toDate)
                currentMaintArray.Insert(7, toAmPm)
                currentMaintArray.Insert(8, Nothing)
                currentMaintArray.Insert(9, Nothing)
                currentMaintArray.Insert(10, "o")

                ' Populate the currentMaint collection
                currentMaintCollection.Add(currentMaintArray)

            Next
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try


    End Sub

End Class
