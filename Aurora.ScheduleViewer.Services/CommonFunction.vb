#Region "Imports"

Imports System.Xml
Imports System.Text
Imports Aurora.Common

#End Region

Public Class CommonFunction

    Public Shared Function HasAdminPermission(ByVal userCode As String) As Boolean
        Dim adminPermissionXmlDoc As XmlDocument
        adminPermissionXmlDoc = Aurora.Common.Data.GetUniversalInfo("SCHEDULEVIEWER_FORCERENT")

        Dim adminPermissioXmlNode As XmlNode
        adminPermissioXmlNode = adminPermissionXmlDoc.SelectSingleNode("/data/universalinfo/uviValue")
        Dim adminPermissionList As String = ""
        adminPermissionList = adminPermissioXmlNode.InnerText

        Dim userList() As String
        userList = adminPermissionList.Split(";")

        For Each user As String In userList
            user = user.Trim()
            If userCode.ToUpper() = user.ToUpper() Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Shared Function CreateXmlNode(ByRef xmlDoc As XmlDocument, ByVal name As String, ByVal value As Object) As XmlNode
        Dim node As XmlNode
        ' In order to min the size of the the xmlDoc, we only use a char for the element name
        node = xmlDoc.CreateElement(Strings.Left(name, 1)) '(name)
        node.InnerXml = Utility.ParseString(value, "")
        Return node
    End Function

    Public Shared Function BuildXmlScript(ByVal name As String, ByVal xmldoc As XmlDocument)
        Dim outputXml As StringBuilder
        outputXml = New StringBuilder()
        outputXml.Append("<script id='" & name & "Script' language='javascript'>" & Environment.NewLine)
        outputXml.Append("var " & name & Environment.NewLine)
        outputXml.Append(name & " = """ & xmldoc.InnerXml & """" & Environment.NewLine)
        outputXml.Append("</script>")
        Return outputXml

    End Function

    'Public Shared Function BuildScript(ByVal name As String, ByVal value As String)
    '    Dim output As StringBuilder
    '    output = New StringBuilder()
    '    output.Append("<script id='" & name & "Script' language='javascript'>" & Environment.NewLine)
    '    output.Append("var " & name & Environment.NewLine)
    '    output.Append(name & " = """ & value & """" & Environment.NewLine)
    '    output.Append("</script>")
    '    Return output

    'End Function


End Class

