#Region "Imports"

Imports Aurora.ScheduleViewer.Data.DataRepository
Imports Aurora.ScheduleViewer.Services
Imports Aurora.Common
Imports System.Text
Imports System.Xml
Imports System.Collections
#End Region

Public Class Substitution

    Private substitutionCollection As Collection = New Collection()
    Private _substitutionListCollection As Collection = New Collection()
    Public ReadOnly Property SubstitutionListCollection() As Collection
        Get
            Return _substitutionListCollection
        End Get
    End Property


    Private _country As String
    Private _userCode As String

    Public Sub New(ByVal country As String, ByVal userCode As String)
        _country = country
        _userCode = userCode
    End Sub

    Public Sub SetupSubs()

        Try
            'Call GetSubstitutions to retrieve ubstitutions
            Dim substitutionsDatatable As DataTable
            substitutionsDatatable = GetSubstitutions(_country, _userCode)

            substitutionCollection.Clear()

            'Building a xml document
            Dim xmlDoc As XmlDocument
            xmlDoc = New XmlDocument()

            'Substitutions node
            Dim substitutionsNode As XmlNode
            substitutionsNode = xmlDoc.CreateElement("Substitutions")

            For Each dr As DataRow In substitutionsDatatable.Rows

                ' Retrieve values for data row
                Dim prdId As Integer = Utility.ParseInt(dr.Item("PrdId"), -1)
                Dim flmId As String = Utility.ParseInt(dr.Item("SrpFlmId"), -1)
                Dim startDate As DateTime = dr.Item("SrpStartDate")
                Dim endDate As DateTime = dr.Item("SrpEndDate")

                Dim substitutionArray As ArrayList = New ArrayList()
                substitutionArray.Insert(0, prdId)
                substitutionArray.Insert(1, flmId)
                substitutionArray.Insert(2, startDate)
                substitutionArray.Insert(3, endDate)

                ' Populate the substitution collection
                substitutionCollection.Add(substitutionArray)

            Next

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Public Function IsItASub(ByRef vehicleObject As Vehicle, ByVal prdId As Integer, ByVal unitNum As Integer, ByVal jDate As Integer, ByVal product As String) As Boolean
        Try
            Dim isReturn As Boolean
            Dim isFound As Boolean
            isReturn = False
            isFound = False

            Dim modelArray As ArrayList

            'aModel = getModel(lUnitNum)
            modelArray = vehicleObject.GetModel(unitNum)

            Dim myStart As DateTime
            myStart = New DateTime(2000, 1, 1)
            myStart = myStart.AddDays(jDate)
            'dMyStart = DateAdd("d", jDate, CDate("1-Jan-2000"))

            If modelArray(0) > 0 Then
                isReturn = True
                For Each substituation As ArrayList In substitutionCollection
                    If substituation(0) = prdId And substituation(1) = modelArray(0) And substituation(2) <= myStart And substituation(3) >= myStart Then
                        isFound = True
                        Exit For
                    End If
                Next
                If isFound Then
                    isReturn = False
                End If
            End If

            ' Now maintain the substitutionListCollection
            If myStart < Now.AddDays(60) Then

                isFound = False
                For Each a As ArrayList In substitutionListCollection
                    If a(0) = product And a.Item(1) = modelArray(1) Then
                        a(2) = a(2) + 1
                        isFound = True
                    End If
                Next
                If Not isFound Then
                    Dim list As ArrayList = New ArrayList()
                    list.Insert(0, product)
                    list.Insert(1, modelArray(1))
                    list.Insert(2, 1)
                    substitutionListCollection.Add(list)
                End If

            End If

            Return isReturn

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return False
        End Try
    End Function

End Class
