#Region "Imports"
Imports Aurora.ScheduleViewer.Data.DataRepository
Imports Aurora.ScheduleViewer.Services
Imports Aurora.ScheduleViewer.Data
Imports Aurora.Common
Imports System.Text
Imports System.Xml
Imports System.Collections
#End Region

Public Class Schedule

    Private scheduleCollection As Collection = New Collection()
    Private _scheduleXmlDoc As XmlDocument
    Public ReadOnly Property ScheduleXmlDoc() As XmlDocument
        Get
            Return _scheduleXmlDoc
        End Get
    End Property

    Private _scheduleCount As Integer = 0
    Public ReadOnly Property ScheduleCount() As Integer
        Get
            Return _scheduleCount
        End Get
    End Property

    Private unScheduleCollection As Collection = New Collection()
    Private _unScheduleArrayList As ArrayList = New ArrayList()
    Public ReadOnly Property UnScheduleArrayList() As ArrayList
        Get
            Return _unScheduleArrayList
        End Get
    End Property

    Private _unScheduleXmlDoc As XmlDocument
    Public ReadOnly Property UnScheduleXmlDoc() As XmlDocument
        Get
            Return _unScheduleXmlDoc
        End Get
    End Property

    Private _unScheduleCount As Integer = 0
    Public ReadOnly Property UnScheduleCount() As Integer
        Get
            Return _unScheduleCount
        End Get
    End Property

    Private relocationCollection As Collection = New Collection()
    Private _relocationXmlDoc As XmlDocument
    Public ReadOnly Property RelocationXmlDoc() As XmlDocument
        Get
            Return _relocationXmlDoc
        End Get
    End Property

    Private _relocationCount As Integer = 0
    Public ReadOnly Property RelocationCount() As Integer
        Get
            Return _relocationCount
        End Get
    End Property

    Private maintCollection As Collection = New Collection()
    Private _maintXmlDoc As XmlDocument
    Public ReadOnly Property MaintXmlDoc() As XmlDocument
        Get
            Return _maintXmlDoc
        End Get
    End Property

    Private _maintCount As Integer = 0
    Public ReadOnly Property MaintCount() As Integer
        Get
            Return _maintCount
        End Get
    End Property

    Private unMaintCollection As Collection = New Collection()
    Private _unMaintArrayList As ArrayList = New ArrayList()
    Public ReadOnly Property UnMaintArrayList() As ArrayList
        Get
            Return _unMaintArrayList
        End Get
    End Property

    Private _unMaintCount As Integer = 0
    Public ReadOnly Property UnMaintCount() As Integer
        Get
            Return _unMaintCount
        End Get
    End Property

    Private disposalCollection As Collection = New Collection()
    Private _disposalXmlDoc As XmlDocument
    Public ReadOnly Property DisposalXmlDoc() As XmlDocument
        Get
            Return _disposalXmlDoc
        End Get
    End Property
    Private _disposalCount As Integer = 0
    Public ReadOnly Property DisposalCount() As Integer
        Get
            Return _disposalCount
        End Get
    End Property

    Private _unDisposalCount As Integer = 0
    Public ReadOnly Property UnDisposalCount() As Integer
        Get
            Return _unDisposalCount
        End Get
    End Property

    Private _country As String
    Private _userCode As String
    Private _vehicleObject As Vehicle
    Private _substitutionObject As Substitution

    Public Sub New(ByVal country As String, ByVal userCode As String, ByRef vehicleObject As Vehicle, ByRef substitutionObject As Substitution)
        _country = country
        _userCode = userCode
        _vehicleObject = vehicleObject
        _substitutionObject = substitutionObject
    End Sub

    Public Sub SetupScheduled()
        Try
            _scheduleCount = 0
            _unScheduleCount = 0
            _relocationCount = 0
            _maintCount = 0
            _unMaintCount = 0
            _disposalCount = 0
            _unDisposalCount = 0

            ' Scheduled Rentals
            ' Reading Scheduled Rentals

            SetupMySched()

            ' Adding Scheduled Rentals

            'Building a xml document
            _scheduleXmlDoc = New XmlDocument()
            'schedules Node 
            Dim schedulesNode As XmlNode
            schedulesNode = scheduleXmlDoc.CreateElement("Schedules")

            For Each sList As ArrayList In scheduleCollection
                Dim vehicleIndex As Integer = -1

                For Each vList As ArrayList In _vehicleObject.vehicleCollection
                    If vList.Item(1) = sList.Item(1) Then
                        vehicleIndex = vList.Item(0)
                        Exit For
                    End If
                Next

                If vehicleIndex <> -1 Then

                    sList.Item(8) = vehicleIndex

                    'schedule node
                    Dim scheduleNode As XmlNode
                    scheduleNode = scheduleXmlDoc.CreateElement("Schedule")

                    Dim idNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "Id", vehicleIndex)
                    Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "FromLoc", sList.Item(2))
                    Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "FromDate", sList.Item(3))
                    Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "FromAmPm", sList.Item(4))
                    Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "ToLoc", sList.Item(5))
                    Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "ToDate", sList.Item(6))
                    Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "ToAmPm", sList.Item(7))
                    Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "Rental", sList.Item(0))
                    Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "EpType", sList.Item(10))
                    Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(scheduleXmlDoc, "Layer", "1")

                    scheduleNode.AppendChild(idNode)
                    scheduleNode.AppendChild(fromLocNode)
                    scheduleNode.AppendChild(fromDateNode)
                    scheduleNode.AppendChild(fromAmPmNode)
                    scheduleNode.AppendChild(toLocNode)
                    scheduleNode.AppendChild(toDateNode)
                    scheduleNode.AppendChild(toAmPmNode)
                    scheduleNode.AppendChild(rentalNode)
                    scheduleNode.AppendChild(epTypeNode)
                    scheduleNode.AppendChild(layerNode)

                    schedulesNode.AppendChild(scheduleNode)

                Else
                    Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Unable to find unit for Rental: " & Utility.ParseString(sList.Item(1), "") & " for rental: " & Utility.ParseString(sList.Item(0), ""))
                End If
            Next

            scheduleXmlDoc.AppendChild(schedulesNode)


            ' Unscheduled Rentals
            'Adding Unscheduled Rentals....
            SetupMyUnSched()

            'Adding Unscheduled Rentals....

            unScheduleArrayList.Clear()

            'Building a xml document
            _unScheduleXmlDoc = New XmlDocument()
            'unSchedules Node 
            Dim unSchedulesNode As XmlNode
            unSchedulesNode = unScheduleXmlDoc.CreateElement("UnSchedules")

            For Each sList As ArrayList In unScheduleCollection
                Dim description As StringBuilder = New StringBuilder()
                description.Append(Strings.Left(sList.Item(0) & "          ", 10))
                description.Append(" " & sList.Item(8) & " ")
                description.Append(sList.Item(2) & "-" & sList.Item(5))
                description.Append(" " & CStr(DateAdd("d", sList.Item(3), CDate("1-1-2000"))))

                If sList.Item(4) = 1 Then
                    description.Append("pm ")
                Else
                    description.Append("am ")
                End If
                description.Append("-" & CStr(DateAdd("d", sList.Item(6), CDate("1-1-2000"))))
                If sList.Item(7) = 1 Then
                    description.Append("pm ")
                Else
                    description.Append("am ")
                End If

                unScheduleArrayList.Add(description)

                'unSchedule node
                Dim unScheduleNode As XmlNode
                unScheduleNode = unScheduleXmlDoc.CreateElement("UnSchedule")

                Dim idNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "Id", "")
                Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "FromLoc", sList.Item(2))
                Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "FromDate", sList.Item(3))
                Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "FromAmPm", sList.Item(4))
                Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "ToLoc", sList.Item(5))
                Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "ToDate", sList.Item(6))
                Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "ToAmPm", sList.Item(7))
                Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "Rental", sList.Item(0))
                Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "EpType", "")
                Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(unScheduleXmlDoc, "Layer", "1")

                unScheduleNode.AppendChild(idNode)
                unScheduleNode.AppendChild(fromLocNode)
                unScheduleNode.AppendChild(fromDateNode)
                unScheduleNode.AppendChild(fromAmPmNode)
                unScheduleNode.AppendChild(toLocNode)
                unScheduleNode.AppendChild(toDateNode)
                unScheduleNode.AppendChild(toAmPmNode)
                unScheduleNode.AppendChild(rentalNode)
                unScheduleNode.AppendChild(epTypeNode)
                unScheduleNode.AppendChild(layerNode)

                unSchedulesNode.AppendChild(unScheduleNode)
            Next

            unScheduleXmlDoc.AppendChild(unSchedulesNode)


            ' Relocations
            ' Adding Scheduled Relocations....
            SetupMyRelocs()

            ' Adding Scheduled Relocations....

            'Building a xml document
            _relocationXmlDoc = New XmlDocument()
            'Relocations Node 
            Dim relocationsNode As XmlNode
            relocationsNode = relocationXmlDoc.CreateElement("Relocations")

            For Each rList As ArrayList In relocationCollection
                Dim vehicleIndex As Integer = -1

                For Each vList As ArrayList In _vehicleObject.VehicleCollection
                    If vList.Item(1) = rList.Item(1) Then
                        vehicleIndex = vList.Item(0)
                        Exit For
                    End If
                Next

                If vehicleIndex <> -1 Then

                    rList.Item(8) = vehicleIndex

                    'relocation node
                    Dim relocationNode As XmlNode
                    relocationNode = relocationXmlDoc.CreateElement("Relocation")

                    Dim idNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "Id", vehicleIndex)
                    Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "FromLoc", rList.Item(2))
                    Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "FromDate", rList.Item(3))
                    Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "FromAmPm", rList.Item(4))
                    Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "ToLoc", rList.Item(5))
                    Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "ToDate", rList.Item(6))
                    Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "ToAmPm", rList.Item(7))
                    Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "Rental", rList.Item(0))
                    Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "EpType", "R")
                    Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(relocationXmlDoc, "Layer", "1")

                    relocationNode.AppendChild(idNode)
                    relocationNode.AppendChild(fromLocNode)
                    relocationNode.AppendChild(fromDateNode)
                    relocationNode.AppendChild(fromAmPmNode)
                    relocationNode.AppendChild(toLocNode)
                    relocationNode.AppendChild(toDateNode)
                    relocationNode.AppendChild(toAmPmNode)
                    relocationNode.AppendChild(rentalNode)
                    relocationNode.AppendChild(epTypeNode)
                    relocationNode.AppendChild(layerNode)

                    relocationsNode.AppendChild(relocationNode)

                Else
                    Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Unable to find unit for Reloc: " & Utility.ParseString(rList.Item(1), "") & " for reloc: " & Utility.ParseString(rList.Item(0), ""))
                End If
            Next

            relocationXmlDoc.AppendChild(relocationsNode)


            ' OffFleets
            'Adding Scheduled OffFleets....
            SetupMyMaint()

            'Adding Scheduled OffFleets....

            'Building a xml document
            _maintXmlDoc = New XmlDocument()
            'Maints Node 
            Dim maintsNode As XmlNode
            maintsNode = maintXmlDoc.CreateElement("Maints")

            For Each mList As ArrayList In maintCollection
                Dim vehicleIndex As Integer = -1

                For Each vList As ArrayList In _vehicleObject.VehicleCollection
                    If vList.Item(1) = mList.Item(1) Then
                        vehicleIndex = vList.Item(0)
                        Exit For
                    End If
                Next

                If vehicleIndex <> -1 Then

                    mList.Item(8) = vehicleIndex

                    'maint node
                    Dim maintNode As XmlNode
                    maintNode = maintXmlDoc.CreateElement("Maint")

                    Dim idNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "Id", vehicleIndex)
                    Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "FromLoc", mList.Item(2))
                    Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "FromDate", mList.Item(3))
                    Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "FromAmPm", mList.Item(4))
                    Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "ToLoc", mList.Item(5))
                    Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "ToDate", mList.Item(6))
                    Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "ToAmPm", mList.Item(7))
                    Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "Rental", mList.Item(0))
                    Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "EpType", "O")
                    Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(maintXmlDoc, "Layer", "1")

                    maintNode.AppendChild(idNode)
                    maintNode.AppendChild(fromLocNode)
                    maintNode.AppendChild(fromDateNode)
                    maintNode.AppendChild(fromAmPmNode)
                    maintNode.AppendChild(toLocNode)
                    maintNode.AppendChild(toDateNode)
                    maintNode.AppendChild(toAmPmNode)
                    maintNode.AppendChild(rentalNode)
                    maintNode.AppendChild(epTypeNode)
                    maintNode.AppendChild(layerNode)

                    maintsNode.AppendChild(maintNode)
                Else
                    Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Unable to find unit for OffFleet: " & Utility.ParseString(mList.Item(1), "") & " for OffFleet: " & Utility.ParseString(mList.Item(0), ""))
                End If
            Next

            maintXmlDoc.AppendChild(maintsNode)


            ' Unscheduled OffFleets
            'Adding Unscheduled OffFleets....
            SetupMyUnMaint()

            'Adding Unscheduled OffFleets....

            unMaintArrayList.Clear()

            For Each sList As ArrayList In unMaintCollection
                Dim description As StringBuilder = New StringBuilder()
                description.Append(Strings.Left(sList.Item(0) & "          ", 10))
                description.Append(" " & sList.Item(8) & " ")
                description.Append(sList.Item(2)) '& "-" & sList.Item(5))
                description.Append(" " & CStr(DateAdd("d", sList.Item(3), CDate("1-1-2000"))))

                If sList.Item(4) = 1 Then
                    description.Append("pm ")
                Else
                    description.Append("am ")
                End If
                description.Append("-" & CStr(DateAdd("d", sList.Item(6), CDate("1-1-2000"))))
                If sList.Item(7) = 1 Then
                    description.Append("pm ")
                Else
                    description.Append("am ")
                End If

                unMaintArrayList.Add(description)
            Next


            ' Disposals
            'Adding Scheduled Disposals....
            SetupMyDisposals()

            'Adding Scheduled Disposals....
            'Building a xml document
            _disposalXmlDoc = New XmlDocument()
            'disposal Node 
            Dim disposalsNode As XmlNode
            disposalsNode = disposalXmlDoc.CreateElement("Disposals")

            For Each dList As ArrayList In disposalCollection
                Dim vehicleIndex As Integer = -1
                If dList.Item(2) = 0 Then
                    _unDisposalCount = UnDisposalCount + 1

                Else
                    For Each vList As ArrayList In _vehicleObject.VehicleCollection
                        If vList.Item(1) = dList.Item(2) Then
                            vehicleIndex = vList.Item(0)
                            Exit For
                        End If
                    Next

                    If vehicleIndex <> -1 And dList.Item(4) <> "" Then

                        dList.Item(7) = vehicleIndex

                        'disposal node
                        Dim disposalNode As XmlNode
                        disposalNode = disposalXmlDoc.CreateElement("Disposal")

                        Dim idNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "Id", vehicleIndex)
                        Dim fromLocNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "FromLoc", dList.Item(4))
                        Dim fromDateNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "FromDate", DateDiff("d", CDate("1-Jan-2000"), dList.Item(5)))
                        Dim fromAmPmNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "FromAmPm", "0")
                        Dim toLocNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "ToLoc", dList.Item(4))
                        Dim toDateNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "ToDate", DateDiff("d", CDate("1-Jan-2000"), DateAdd("d", 365, dList.Item(5))))
                        Dim toAmPmNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "ToAmPm", "1")
                        Dim rentalNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "Rental", dList.Item(0) & " - " & dList.Item(3))
                        Dim epTypeNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "EpType", "F")
                        Dim layerNode As XmlNode = CommonFunction.CreateXmlNode(disposalXmlDoc, "Layer", "1")


                        disposalNode.AppendChild(idNode)
                        disposalNode.AppendChild(fromLocNode)
                        disposalNode.AppendChild(fromDateNode)
                        disposalNode.AppendChild(fromAmPmNode)
                        disposalNode.AppendChild(toLocNode)
                        disposalNode.AppendChild(toDateNode)
                        disposalNode.AppendChild(toAmPmNode)
                        disposalNode.AppendChild(rentalNode)
                        disposalNode.AppendChild(epTypeNode)
                        disposalNode.AppendChild(layerNode)

                        disposalsNode.AppendChild(disposalNode)

                        _disposalCount = _disposalCount + 1
                    Else
                        If vehicleIndex = -1 Then
                            Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Unable to find unit for Disposal: " & Utility.ParseString(dList.Item(2), "") & " for Disposal: " & Utility.ParseString(dList.Item(0) & " - " & dList.Item(3), ""))
                        Else
                            Logging.LogWarning(My.Settings.AuroraFunctionCodeAttribute, "Problem does not have sale location. Unit: " & Utility.ParseString(dList.Item(2), "") & " for Disposal: " & Utility.ParseString(dList.Item(0) & " - " & dList.Item(3), ""))
                        End If
                    End If
                End If
            Next

            disposalXmlDoc.AppendChild(disposalsNode)

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Private Sub SetupMySched()

        Try
            Dim epType As String
            Dim bRelocBkg As Boolean

            bRelocBkg = False

            scheduleCollection = New Collection()

            'Reading in Scheduled Bookings....

            Dim dt As DataTable
            dt = GetMyBookingAssignments(_country, _userCode, True)

            _substitutionObject.SubstitutionListCollection.Clear()

            For Each dr As DataRow In dt.Rows

                Dim rentNum As String = Utility.ParseString(dr.Item("DraRntNum"), "")
                Dim isRelocBkg As Boolean = False
                If Strings.Left(rentNum, 1) = "R" Then
                    isRelocBkg = True
                End If
                Dim rental As String = rentNum & " - " & Utility.ParseString(dr.Item("Product"), "")
                Dim rntStatus As String = Utility.ParseString(dr.Item("RenStatus"), "")
                Dim force As String = Utility.ParseString(dr.Item("RenAllowVehSpec"), "")
                Dim revenue As Long = dr.Item("RenRevenue")
                rental = rental & "\r\n" & "Revenue: $" & Utility.ParseString(revenue, "")
                'rental = rental & Environment.NewLine & "Revenue: $" & Commonfunction.ParseString(revenue, "")

                Dim unit As Integer = Utility.ParseInt(dr.Item("DraUnitNum"), 0)
                Dim fromLoc As String = Utility.ParseString(dr.Item("DraFromLocCode"), "")
                Dim fromDate As Integer = Utility.ParseInt(dr.Item("DraFromJulianDate"), 0)
                Dim fromAmPm As Integer = Utility.ParseInt(dr.Item("DraFromAmPm"), 0)
                Dim toLoc As String = Utility.ParseString(dr.Item("DraToLocCode"), "")
                Dim toDate As Integer = Utility.ParseInt(dr.Item("DraToJulianDate"), 0)
                Dim toAmPm As Integer = Utility.ParseInt(dr.Item("DraToAmPm"), 0)
                Dim prdId As Integer = Utility.ParseInt(dr.Item("renPrdId"), 0)
                Dim product As String = Utility.ParseString(dr.Item("Product"), "")

                Dim isSubst As Boolean
                isSubst = _substitutionObject.IsItASub(_vehicleObject, prdId, unit, fromDate, product)

                If Not force = "" Then
                    rental = rental & "\r\n" & "FORCE: " & force
                    'rental = rental & Environment.NewLine & "FORCE: " & force
                    epType = "A"
                    If isRelocBkg Then
                        epType = "a"     'Show as Force
                    End If
                Else
                    epType = "B"        'Show as Normal Booking
                    If isRelocBkg Then
                        epType = "r"     'Show as Relocation Booking
                        If isSubst Then
                            epType = "L" 'Show as Substituted Reloc Booking
                        End If
                    Else
                        If isSubst Then
                            epType = "U" 'Show as Substitution
                        Else
                            If revenue < 25 Then
                                epType = CStr("c") ' Show as Cheap
                            End If
                        End If
                    End If

                    ' Check for Delays
                    Dim renFromDate As Integer = Utility.ParseInt(dr.Item("RenFromDate"), 0)
                    Dim renFromAmPm As Integer = Utility.ParseInt(dr.Item("RenFromAmPm"), 0)

                    If renFromDate <> fromDate Or renFromAmPm <> fromAmPm Then
                        If (renFromDate = fromDate) Then
                            epType = CStr("D")
                        Else
                            epType = CStr("d")
                        End If
                    End If
                End If

                If Strings.UCase(rntStatus) = "NN" Then epType = "N"


                Dim scheduleArray As ArrayList = New ArrayList()
                scheduleArray.Insert(0, rental)
                scheduleArray.Insert(1, unit)
                scheduleArray.Insert(2, fromLoc)
                scheduleArray.Insert(3, fromDate)
                scheduleArray.Insert(4, fromAmPm)
                scheduleArray.Insert(5, toLoc)
                scheduleArray.Insert(6, toDate)
                scheduleArray.Insert(7, toAmPm)
                scheduleArray.Insert(8, Nothing)
                scheduleArray.Insert(9, Nothing)
                scheduleArray.Insert(10, epType)

                ' Populate the schedule collection
                scheduleCollection.Add(scheduleArray)

            Next
            _scheduleCount = scheduleCollection.Count
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try
    End Sub

    Private Sub SetupMyUnSched()

        Try
            unScheduleCollection = New Collection()
            Dim dt As DataTable
            dt = GetMyBookingAssignments(_country, _userCode, False)

            For Each dr As DataRow In dt.Rows

                Dim rentNum As String = Utility.ParseString(dr.Item("DraRntNum"), "")
                Dim unit As Integer = Utility.ParseInt(dr.Item("DraUnitNum"), 0)

                Dim fromLoc As String = Utility.ParseString(dr.Item("DraFromLocCode"), "")
                Dim fromDate As Integer = Utility.ParseInt(dr.Item("DraFromJulianDate"), 0)
                Dim fromAmPm As Integer = Utility.ParseInt(dr.Item("DraFromAmPm"), 0)
                Dim toLoc As String = Utility.ParseString(dr.Item("DraToLocCode"), "")
                Dim toDate As Integer = Utility.ParseInt(dr.Item("DraToJulianDate"), 0)
                Dim toAmPm As Integer = Utility.ParseInt(dr.Item("DraToAmPm"), 0)
                Dim product As String = Utility.ParseString(dr.Item("Product"), "")

                Dim unScheduleArray As ArrayList = New ArrayList()
                unScheduleArray.Insert(0, rentNum)
                unScheduleArray.Insert(1, unit)
                unScheduleArray.Insert(2, fromLoc)
                unScheduleArray.Insert(3, fromDate)
                unScheduleArray.Insert(4, fromAmPm)
                unScheduleArray.Insert(5, toLoc)
                unScheduleArray.Insert(6, toDate)
                unScheduleArray.Insert(7, toAmPm)
                unScheduleArray.Insert(8, product)
                unScheduleArray.Insert(9, Nothing)

                ' Populate the unschedule collection
                unScheduleCollection.Add(unScheduleArray)

            Next

            _unScheduleCount = unScheduleCollection.Count

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Private Sub SetupMyRelocs()

        Try
            'Reading in Scheduled Relocations....

            relocationCollection = New Collection()

            Dim dt As DataTable
            dt = GetMyRelocations(_country, _userCode)

            For Each dr As DataRow In dt.Rows

                Dim rental As String = Utility.ParseString(dr.Item("DrlRelocID"), "")
                Dim unit As Integer = Utility.ParseInt(dr.Item("DrlUnitNum"), 0)
                Dim fromLoc As String = Utility.ParseString(dr.Item("DrlFromLocCode"), "")
                Dim fromDate As Integer = Utility.ParseInt(dr.Item("DrlFromJulianDate"), 0)
                Dim fromAmPm As Integer = Utility.ParseInt(dr.Item("DrlFromAmPm"), 0)
                Dim toLoc As String = Utility.ParseString(dr.Item("DrlToLocCode"), "")
                Dim toDate As Integer = Utility.ParseInt(dr.Item("DrlToJulianDate"), 0)
                Dim toAmPm As Integer = Utility.ParseInt(dr.Item("DrlToAmPm"), 0)

                Dim relocationArray As ArrayList = New ArrayList()
                relocationArray.Insert(0, rental)
                relocationArray.Insert(1, unit)
                relocationArray.Insert(2, fromLoc)
                relocationArray.Insert(3, fromDate)
                relocationArray.Insert(4, fromAmPm)
                relocationArray.Insert(5, toLoc)
                relocationArray.Insert(6, toDate)
                relocationArray.Insert(7, toAmPm)
                relocationArray.Insert(8, Nothing)
                relocationArray.Insert(9, Nothing)

                ' Populate the relocation collection
                relocationCollection.Add(relocationArray)

            Next

            _relocationCount = relocationCollection.Count
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Private Sub SetupMyMaint()

        Try
            'Reading in Scheduled OffFleets....

            maintCollection = New Collection()

            Dim dt As DataTable
            dt = GetMyOffFleets(_country, _userCode, True)

            For Each dr As DataRow In dt.Rows

                Dim reference As String = ""
                Dim unit As Integer = Utility.ParseInt(dr.Item("DmaUnitNum"), 0)
                Dim fromLoc As String = Utility.ParseString(dr.Item("DmaLocCode"), "")
                Dim fromDate As Integer = Utility.ParseInt(dr.Item("DmaFromJulianDate"), 0)
                Dim fromAmPm As Integer = Utility.ParseInt(dr.Item("DmaFromAmPm"), 0)
                Dim toLoc As String = Utility.ParseString(dr.Item("DmaLocCode"), "")
                Dim toDate As Integer = Utility.ParseInt(dr.Item("DmaToJulianDate"), 0)
                Dim toAmPm As Integer = Utility.ParseInt(dr.Item("DmaToAmPm"), 0)

                Dim maintArray As ArrayList = New ArrayList()
                maintArray.Insert(0, reference)
                maintArray.Insert(1, unit)
                maintArray.Insert(2, fromLoc)
                maintArray.Insert(3, fromDate)
                maintArray.Insert(4, fromAmPm)
                maintArray.Insert(5, toLoc)
                maintArray.Insert(6, toDate)
                maintArray.Insert(7, toAmPm)
                maintArray.Insert(8, Nothing)
                maintArray.Insert(9, Nothing)

                ' Populate the maint collection
                maintCollection.Add(maintArray)

            Next

            _maintCount = maintCollection.Count
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Private Sub SetupMyUnMaint()

        Try
            'Reading in UnScheduled OffFleets....

            unMaintCollection = New Collection()

            Dim dt As DataTable
            dt = GetMyOffFleets(_country, _userCode, False)

            For Each dr As DataRow In dt.Rows

                Dim reference As String = ""
                Dim unit As Integer = Utility.ParseInt(dr.Item("DmaUnitNum"), 0)
                Dim fromLoc As String = Utility.ParseString(dr.Item("DmaLocCode"), "")
                Dim fromDate As Integer = Utility.ParseInt(dr.Item("DmaFromJulianDate"), 0)
                Dim fromAmPm As Integer = Utility.ParseInt(dr.Item("DmaFromAmPm"), 0)
                Dim toLoc As String = Utility.ParseString(dr.Item("DmaLocCode"), "")
                Dim toDate As Integer = Utility.ParseInt(dr.Item("DmaToJulianDate"), 0)
                Dim toAmPm As Integer = Utility.ParseInt(dr.Item("DmaToAmPm"), 0)

                Dim unMaintArray As ArrayList = New ArrayList()
                unMaintArray.Insert(0, reference)
                unMaintArray.Insert(1, unit)
                unMaintArray.Insert(2, fromLoc)
                unMaintArray.Insert(3, fromDate)
                unMaintArray.Insert(4, fromAmPm)
                unMaintArray.Insert(5, toLoc)
                unMaintArray.Insert(6, toDate)
                unMaintArray.Insert(7, toAmPm)
                unMaintArray.Insert(8, Nothing)
                unMaintArray.Insert(9, Nothing)

                ' Populate the unmaint collection
                unMaintCollection.Add(unMaintArray)

            Next

            _unMaintCount = unMaintCollection.Count

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Private Sub SetupMyDisposals()

        Try
            'Reading in Disposals....

            disposalCollection = New Collection()

            Dim dt As DataTable
            dt = GetMyDisposals(_country, _userCode)

            For Each dr As DataRow In dt.Rows

                Dim reference As String = ""
                Dim seqNo As Integer = Utility.ParseInt(dr.Item("DdaSeqNo"), 0)
                Dim planNo As String = Utility.ParseString(dr.Item("DdaPlanNo"), "")
                Dim unit As Integer = Utility.ParseInt(dr.Item("DdaUnitNum"), 0)
                Dim unitList As String = Utility.ParseString(dr.Item("UnitList"), "")
                unitList = unitList & "\n\r" & "Dealer: "
                If Utility.ParseString(dr.Item("Consigned"), "") = "" Then
                    unitList = unitList & "Not Consigned"
                Else
                    unitList = unitList & Utility.ParseString(dr.Item("Consigned"), "")
                End If
                Dim saleLocation As String = Utility.ParseString(dr.Item("SaleLocation"), "")
                Dim offFleetDate As DateTime = Convert.ToDateTime(dr.Item("OffFleetDate"))
                Dim unitCount As Integer = Utility.ParseInt(dr.Item("UnitCount"), 0)

                Dim disposalArray As ArrayList = New ArrayList()
                disposalArray.Insert(0, planNo)
                disposalArray.Insert(1, seqNo)
                disposalArray.Insert(2, unit)
                disposalArray.Insert(3, unitList)
                disposalArray.Insert(4, saleLocation)
                disposalArray.Insert(5, offFleetDate)
                disposalArray.Insert(6, unitCount)
                disposalArray.Insert(7, Nothing)
                disposalArray.Insert(8, Nothing)

                ' Populate the disposal collection
                disposalCollection.Add(disposalArray)

            Next
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try

    End Sub

    Public Shared Function GetMyRentalDetails(ByVal strBooRnt As String, ByVal userCode As String, ByRef RentalId As String) As String

        Dim output As String = ""
        Dim myBoo As String
        Dim myRnt As Integer
        Dim myStr As String
        Dim nPos As Integer
        Dim nPos2 As Integer
        Dim nPos3 As Integer
        Dim sTemp As String

        nPos = InStr(1, strBooRnt, "/", vbTextCompare)
        If nPos > 0 Then
            myBoo = Strings.Trim(Strings.Left(strBooRnt, nPos - 1))
            myStr = Strings.Trim(Strings.Mid(strBooRnt, nPos + 1))
            nPos = InStr(1, myStr, " ", vbTextCompare)
            nPos2 = InStr(1, myStr, vbCrLf, vbTextCompare)
            ' Enhanced to take care of /1x1 exchange notation
            nPos3 = InStr(1, myStr, "x", vbTextCompare)
            If nPos > 0 And nPos2 > 0 Then
                nPos = Math.Min(nPos, nPos2)
                If nPos3 > 0 Then
                    nPos = Math.Min(nPos, nPos3)
                End If
            Else
                nPos = Math.Max(nPos, nPos2)
                If nPos3 > 0 Then
                    nPos = Math.Min(nPos, nPos3)
                End If
            End If
            If nPos > 0 Then
                sTemp = Strings.Trim(Strings.Left(myStr, nPos - 1))
                If IsNumeric(sTemp) Then
                    myRnt = CInt(sTemp)
                Else
                    myRnt = 0
                End If
            Else
                myRnt = CInt(Strings.Trim(myStr))
            End If

            If myRnt > 0 Then

                Dim dt As DataTable
                dt = DataRepository.GetMyRentalDetails(myBoo, myRnt, userCode)
                RentalId = myBoo & "/" & myRnt
                If dt.Rows.Count = 1 Then
                    Dim dr As DataRow
                    dr = dt.Rows(0)
                    output = "Last Name: " & dr.Item(0) & "\r\n"
                    output = output & "Booked: " & Utility.ParseString(dr.Item("rntCkoWhen"), "")
                    output = output & " - " & Utility.ParseString(dr.Item("rntCkiWhen"), "") & "\r\n"

                    If dr.Item(1) = True Then
                        output = output & "    VIP!!" & "\r\n"
                    End If
                Else
                    output = "Rental Details not found" & "\r\n"
                End If
            Else
                output = "Rental ID invalid!" & Environment.NewLine
            End If
        Else
            output = "Rental ID invalid!" & Environment.NewLine
        End If
        Return output
    End Function

End Class