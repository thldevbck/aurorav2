Imports Aurora.Common
Imports System.Configuration
Imports System.Web.Mail

Public Class Archiving
    'Arch_GetScheduleList

    Public Shared Function GetScheduleList() As DataSet
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Arch_GetScheduleList", dstResult)
        Return dstResult
    End Function

    Public Shared Function GetRateScheduleList() As DataSet
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Arch_GetRateScheduleList", dstResult)
        Return dstResult
    End Function

    Public Shared Function UpdateSchedule(ByVal ScheduleId As Int64, ByVal StartDateTime As DateTime, ByVal RunFrqID As Integer, _
                                          ByVal RunForHrs As Integer, ByVal NotificationList As String, _
                                          ByVal StatusId As Integer, ByVal FinancialYearText As String, _
                                          ByVal FinancialYearEndDate As DateTime, _
                                          ByVal UserId As String, ByVal AddProgramName As String, _
                                          ByRef ErrorMessage As String) As Boolean
        '   ALTER PROC [dbo].[Arch_SaveSchedule]
        '	@nScheduleID BIGINT
        '	,@dStartDateTime DATETIME
        '   ,@nRunFrqID INT
        '   ,@nRunForHrs INT
        '   ,@sNotificationList VARCHAR(MAX)
        '	,@nStatusID INT 
        '	,@sFinancialYearText VARCHAR(9) = NULL
        '	,@sFinancialYearEndDate DATETIME = NULL
        '	,@sUserID VARCHAR(64)
        '	,@sAddProgram VARCHAR(256) = NULL
        '	,@sErrorMessage VARCHAR(MAX) OUTPUT
        Try

            Dim params(10) As Aurora.Common.Data.Parameter

            params(0) = New Aurora.Common.Data.Parameter("nScheduleID", DbType.Int64, ScheduleId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("dStartDateTime", DbType.DateTime, IIf(StartDateTime = DateTime.MinValue, System.DBNull.Value, StartDateTime), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("nRunFrqID", DbType.Int32, IIf(RunFrqID = -1, System.DBNull.Value, RunFrqID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("nRunForHrs", DbType.Int32, IIf(RunForHrs = -1, System.DBNull.Value, RunForHrs), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("sNotificationList", DbType.String, IIf(NotificationList.Trim() = "", System.DBNull.Value, NotificationList), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("nStatusID", DbType.Int32, StatusId, Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("sFinancialYearText", DbType.String, IIf(FinancialYearText.Trim() = "", System.DBNull.Value, FinancialYearText), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("sFinancialYearEndDate", DbType.DateTime, IIf(FinancialYearEndDate = DateTime.MinValue, System.DBNull.Value, FinancialYearEndDate), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("sUserID", DbType.String, UserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("sAddProgram", DbType.String, AddProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("sErrorMessage", DbType.String, 1024, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Arch_SaveSchedule", params)

            ErrorMessage = CStr(params(10).Value)

            Try
                If Not String.IsNullOrEmpty(ErrorMessage) AndAlso Trim(ErrorMessage) <> "" Then
                    Aurora.Common.Logging.LogInformation("ARCHIVING", "ErrorMessage = " + ErrorMessage)
                End If
            Catch ex As Exception
            End Try

        Catch ex As Exception
            Aurora.Common.Logging.LogException("ARCHIVING", ex)
            Aurora.Common.Logging.LogInformation("ARCHIVING", ex.StackTrace)
        End Try


    End Function

    Public Shared Function UpdateRateSchedule(ByVal ScheduleId As Int64, ByVal StartDateTime As DateTime, ByVal RunFrqID As Integer, _
                                          ByVal RunForHrs As Integer, ByVal NotificationList As String, _
                                          ByVal StatusId As Integer, ByVal FinancialYearText As String, _
                                          ByVal FinancialYearEndDate As DateTime, _
                                          ByVal UserId As String, ByVal AddProgramName As String, _
                                          ByRef ErrorMessage As String) As Boolean
        '   CREATE PROC [dbo].[Arch_SaveRateSchedule]
        '	@nScheduleID BIGINT
        '	,@dStartDateTime DATETIME
        '   ,@nRunFrqID INT
        '   ,@nRunForHrs INT
        '   ,@sNotificationList VARCHAR(MAX)
        '	,@nStatusID INT 
        '	,@sFinancialYearText VARCHAR(9) = NULL
        '	,@sFinancialYearEndDate DATETIME = NULL
        '	,@sUserID VARCHAR(64)
        '	,@sAddProgram VARCHAR(256) = NULL
        '	,@sErrorMessage VARCHAR(MAX) OUTPUT

        Try

            Dim params(10) As Aurora.Common.Data.Parameter

            params(0) = New Aurora.Common.Data.Parameter("nScheduleID", DbType.Int64, ScheduleId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("dStartDateTime", DbType.DateTime, IIf(StartDateTime = DateTime.MinValue, System.DBNull.Value, StartDateTime), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("nRunFrqID", DbType.Int32, IIf(RunFrqID = -1, System.DBNull.Value, RunFrqID), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("nRunForHrs", DbType.Int32, IIf(RunForHrs = -1, System.DBNull.Value, RunForHrs), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("sNotificationList", DbType.String, IIf(NotificationList.Trim() = "", System.DBNull.Value, NotificationList), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("nStatusID", DbType.Int32, StatusId, Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("sFinancialYearText", DbType.String, IIf(FinancialYearText.Trim() = "", System.DBNull.Value, FinancialYearText), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("sFinancialYearEndDate", DbType.DateTime, IIf(FinancialYearEndDate = DateTime.MinValue, System.DBNull.Value, FinancialYearEndDate), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("sUserID", DbType.String, UserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("sAddProgram", DbType.String, AddProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("sErrorMessage", DbType.String, 1024, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Arch_SaveRateSchedule", params)

            ErrorMessage = CStr(params(10).Value)

            Try
                If Not String.IsNullOrEmpty(ErrorMessage) AndAlso Trim(ErrorMessage) <> "" Then
                    Aurora.Common.Logging.LogInformation("ARCHIVING", "ErrorMessage = " + ErrorMessage)
                End If
            Catch ex As Exception
            End Try

        Catch ex As Exception
            Aurora.Common.Logging.LogException("ARCHIVING", ex)
            Aurora.Common.Logging.LogInformation("ARCHIVING", ex.StackTrace)
        End Try


    End Function

    Public Shared Function StopRunningSchedule(ByVal UserCode As String, ByRef ErrorMessage As String) As Boolean
        Try
            Aurora.Common.Data.ExecuteScalarSP("Arch_StopRunningSchedule", UserCode)
            Return True
        Catch ex As Exception
            ErrorMessage = ex.Message & vbNewLine & ex.StackTrace
            Return False
        End Try
    End Function

    Public Shared Function StopRunningRateSchedule(ByVal UserCode As String, ByRef ErrorMessage As String) As Boolean
        Try
            Aurora.Common.Data.ExecuteScalarSP("Arch_StopRunningRateSchedule", UserCode)
            Return True
        Catch ex As Exception
            ErrorMessage = ex.Message & vbNewLine & ex.StackTrace
            Return False
        End Try
    End Function

    Public Shared Function GetScheduleDetails(ByVal ScheduleId As Int64) As DataTable
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Arch_GetScheduleDetails", dstResult, ScheduleId)
        If dstResult IsNot Nothing AndAlso dstResult.Tables.Count > 0 Then
            Return dstResult.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function GetRateScheduleDetails(ByVal ScheduleId As Int64) As DataTable
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Arch_GetRateScheduleDetails", dstResult, ScheduleId)
        If dstResult IsNot Nothing AndAlso dstResult.Tables.Count > 0 Then
            Return dstResult.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function ValidateTableStructures(ByRef ErrorsTable As DataTable) As Boolean
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Arch_ValidateRentalArchivedTables", dstResult)
        If dstResult IsNot Nothing AndAlso dstResult.Tables.Count > 0 AndAlso dstResult.Tables(0).Rows.Count > 0 Then
            ErrorsTable = dstResult.Tables(0)
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function ValidateRateTableStructures(ByRef ErrorsTable As DataTable) As Boolean
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Arch_ValidateRateArchivedTables", dstResult) 
        If dstResult IsNot Nothing AndAlso dstResult.Tables.Count > 0 AndAlso dstResult.Tables(0).Rows.Count > 0 Then
            ErrorsTable = dstResult.Tables(0)
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function SendEmail(ByVal FromAddress As String, _
                                     ByVal ToAddress As String, _
                                     ByVal Subject As String, _
                                     ByVal MessageBody As String, _
                                     ByRef ErrorMessage As String) As Boolean
        Try
            Dim oMailMsg As MailMessage = New MailMessage
            oMailMsg.From = FromAddress
            oMailMsg.To = ToAddress
            ' fix for weird mapping problem
            oMailMsg.Headers.Add("Reply-To", FromAddress)
            ' fix for weird mapping problem
            oMailMsg.BodyEncoding = System.Text.Encoding.ASCII
            oMailMsg.Subject = Subject
            oMailMsg.Body = MessageBody
            oMailMsg.BodyFormat = MailFormat.Html

            SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("MailServerURL"))
            SmtpMail.Send(oMailMsg)

            Return True
        Catch ex As Exception
            ErrorMessage = ex.Message & vbNewLine & ex.StackTrace
            Return False
        End Try
    End Function

End Class
