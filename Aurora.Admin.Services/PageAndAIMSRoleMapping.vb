﻿Imports Aurora.Common
Imports Aurora.Admin.Data

Public Class PageAndAIMSRoleMapping
    Public Shared Function AIMSRoleMappingData(userCode As String) As DataSet
        Return DataRepository.AIMSRoleMappingData(userCode)
    End Function
    Public Function AIMSInsUpdRoleMappingData(RoleMappingPageId As Integer, _
                                             RoleMappingRoleId As Integer, _
                                             RoleMappingUserCode As String, _
                                             AddUsrId As String) As Boolean


        Dim slog As New Text.StringBuilder()

        slog = slog.AppendFormat("Parameters{0}RoleMappingPageId: {1}{2}", vbCrLf, RoleMappingPageId, vbCrLf)
        slog = slog.AppendFormat("RoleMappingRoleId: {0}{1}", RoleMappingRoleId, vbCrLf)
        slog = slog.AppendFormat("RoleMappingUserCode: {0}{1}", RoleMappingUserCode, vbCrLf)
        slog = slog.AppendFormat("AddUsrId: {0}{1}", AddUsrId, vbCrLf)
        Logging.LogInformation("AIMSInsUpdRoleMappingData", slog.ToString())

        Return DataRepository.AIMSInsUpdRoleMappingData(RoleMappingPageId, RoleMappingRoleId, RoleMappingUserCode, AddUsrId)
    End Function


    Public Function AIMSDelRoleMappingData(RoleMappingUserId As Integer, _
                                         RoleMappingUserCode As String, _
                                         isAll As Boolean) As Boolean


        Dim slog As New Text.StringBuilder()

        slog = slog.AppendFormat("Parameters{0}RoleMappingUserId: {1}{2}", vbCrLf, RoleMappingUserId, vbCrLf)
        slog = slog.AppendFormat("RoleMappingUserCode: {0}{1}", RoleMappingUserCode, vbCrLf)
        slog = slog.AppendFormat("isAll: {0}{1}", isAll, vbCrLf)
        Logging.LogInformation("AIMSDelRoleMappingData", slog.ToString())
        Return DataRepository.AIMSDelRoleMappingData(RoleMappingUserId, RoleMappingUserCode, isAll)

    End Function
End Class
