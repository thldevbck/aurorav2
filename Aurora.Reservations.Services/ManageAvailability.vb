Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports Aurora.Reservations.Services

Public Class ManageAvailability

#Region " Private Function"

    Private Function FindVehicleRequest(ByVal sBkrId As String, ByVal dCkoWhen As String, ByVal dCkiWhen As String, ByVal sCkoAmPm As String, ByVal sCkiAmPm As String, ByVal sCkoLocCode As String, ByVal sCkiLocCode As String, ByVal sPrdId As String) As String

        Dim strXml As String = "<Data>" & Aurora.Common.Data.ExecuteSqlXmlSP("RES_findVechileRequest", sBkrId, dCkoWhen, dCkiWhen, sCkoAmPm, sCkiAmPm, sCkoLocCode, sCkiLocCode, sPrdId) & "</Data>"
        If strXml.Contains("<Error><ErrStatus>") Or (strXml.Length = 0) Then
            Return GetErrorTextFromResource(True, "", "Application", "No Records Found (FindVehicleRequest)")
        Else
            Return strXml
        End If
    End Function

    Private Shared Function ValidXML(ByVal xmlString As String) As Xml.XmlDocument
        Dim xmldoc As Xml.XmlDocument = Nothing
        Dim hasError As Boolean = False
        Try
            xmldoc = New Xml.XmlDocument
            xmldoc.LoadXml(xmlString)

        Catch xml As Xml.XmlException
            hasError = True
        Catch ex As System.Exception
            hasError = True
        Finally
            If hasError = True Then
                xmldoc = Nothing
            End If
        End Try

        Return xmldoc
    End Function

    Private Shared Function FindVehicleRequest( _
                                   ByVal sBkrId As String, _
                                   ByVal sCkoLocCode As String, _
                                   ByVal dCkoWhen As String, _
                                   ByVal sCkoAmPm As String, _
                                   ByVal sCkiLocCode As String, _
                                   ByVal dCkiWhen As String, _
                                   ByVal sCkiAmPm As String, _
                                   ByVal sPrdId As String, _
                                   ByRef varByPass As String, _
                                   ByRef varErrorString As String, _
                                   Optional ByVal sUsercode As String = "") As String



        Dim objDom As New Xml.XmlDocument
        Dim root As Xml.XmlElement
        Dim szVechileId As String = String.Empty
        Dim xmlString As String = "<Error><ErrStatus>"
        FindVehicleRequest = String.Empty

        ''Dim szQueryString As String = String.Concat("RES_findVechileRequest '", sBkrId, "', '", dCkoWhen & "', '", dCkiWhen, "', '", sCkoAmPm, "', '", sCkiAmPm, "', '", sCkoLocCode, "', '", sCkiLocCode, "', '", sPrdId & "'")

        Try

            ''rev:mia sept17
            dCkoWhen = dCkoWhen.Split(" ")(0)
            dCkiWhen = dCkiWhen.Split(" ")(0)
            Dim szVehicleRequest As String = Aurora.Common.Data.ExecuteSqlXmlSP("RES_findVechileRequest", sBkrId, dCkoWhen, dCkiWhen, sCkoAmPm, sCkiAmPm, sCkoLocCode, sCkiLocCode, sPrdId)
            Logging.LogInformation("FindVehicleRequest: RES_findVechileRequest: ", szVehicleRequest)
            If szVehicleRequest.Contains("<Error><ErrStatus>") Or (szVehicleRequest.Length = 0) Or szVehicleRequest.Contains("<Data></Data>") Then
                varErrorString = GetErrorTextFromResource(True, "", "Application", "No Records Found")
                Return GetErrorTextFromResource(True, "", "Application", "No Records Found")
            End If

            objDom = ValidXML(szVehicleRequest)
            If objDom Is Nothing Then
                varErrorString = getMessageFromDB("GEN023")
                Return varErrorString
            End If
            root = objDom.DocumentElement

            If root.ChildNodes(0).ChildNodes(0).Name.Equals("VhrId") Then
                szVechileId = root.ChildNodes(0).ChildNodes(0).InnerText
                varByPass = root.ChildNodes(0).ChildNodes(1).InnerText
            End If

            If (szVechileId.Length) > 0 Then
                FindVehicleRequest = szVechileId
            End If



        Catch ex As Exception

            FindVehicleRequest = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message & " - " & ex.StackTrace & " (FindVehicleRequest)") & "</Root>"

        Finally
            objDom = Nothing
            root = Nothing
        End Try

        Return FindVehicleRequest
    End Function

    Private Shared Function StringContain(ByVal strA As String, ByVal strB As String) As Boolean
        Return strA.Contains(strB)
    End Function

    Public Shared Function GetAvailabilityWithCost(ByVal sBkrId As String, _
                                      ByVal sCkoLocCode As String, _
                                      ByVal dCkoWhen As String, _
                                      ByVal sCkoAmPm As String, _
                                      ByVal sCkiLocCode As String, _
                                      ByVal dCkiWhen As String, _
                                      ByVal sCkiAmPm As String, _
                                      ByVal iHirePeriod As String, _
                                      ByVal sPrdId As String, _
                                      ByVal iHirePeriodUOM As String, _
                                      ByVal sBooId As String, _
                                      ByVal sBpdId As String, _
                                      ByVal pMaximumResultsRequired As String, _
                                      ByVal pPriority As String, _
                                      ByVal pVisibility As String, _
                                      ByVal pAvailableVehicles As String, _
                                      ByVal sUsercode As String, _
                                      ByVal sProgrammName As String, _
                                      ByVal sCtyCode As String, _
                                      Optional ByRef BlockingMessage As String = "", _
                                      Optional ByVal IsQuickAvail As Boolean = False, _
                                      Optional ByVal PromoCode As String = "") As String ''rev:mia Oct 16 2014 - addition of PromoCode




        Dim varErrorString As String = String.Empty
        Dim varByPass As String = String.Empty
        Dim szVhrId As String = String.Empty
        Dim szRequestType As String = String.Empty
        Dim sErrorNo As String = String.Empty
        Dim sError As String = String.Empty

        Dim sErrorString As String = String.Empty


        Dim nMaximumResultsRequired As Integer
        Dim nVisibility As Integer
        Dim dblPriority As Double
        Dim szAvailableVehicles As String = String.Empty
        Dim objDom As New Xml.XmlDocument
        Dim root As Xml.XmlElement
        Dim nodeList As Xml.XmlNodeList

        Dim nVehicleNumber As Long

        Dim lngNumberOfVehiclesFound As Long
        Dim szProductId As String = String.Empty

        Dim szXMLReturn As String = String.Empty


        Dim nLngCountry As Integer


        'Logging.LogInformation(String.Concat("GetAvailabilityWithCost", sUsercode & " - GetAvailabilityWithCost -> Starts"), 2)

        '--Set the Country of Operation for DVASS
        If (sCtyCode.ToUpper.Equals("AU")) Then nLngCountry = 0
        If (sCtyCode.ToUpper.Equals("NZ")) Then nLngCountry = 1

        If (pMaximumResultsRequired = Nothing) Then
            nMaximumResultsRequired = 20
        Else
            nMaximumResultsRequired = CInt(pMaximumResultsRequired)
        End If

        If (pPriority = Nothing) Then
            dblPriority = 1
        Else
            dblPriority = CInt(pPriority)
        End If

        If (pVisibility = Nothing) Then
            nVisibility = 10
        Else
            nVisibility = CInt(pVisibility)
        End If

        ''Dim sbText As New StringBuilder

        Try


            ''-->Check already
            ''Logging.LogInformation("FindVehicleRequest : ", "Invoke")
            szVhrId = FindVehicleRequest( _
                                         sBkrId, _
                                         sCkoLocCode, _
                                         dCkoWhen, _
                                         sCkoAmPm, _
                                         sCkiLocCode, _
                                         dCkiWhen, _
                                         sCkiAmPm, _
                                         sPrdId, _
                                         varByPass, _
                                         varErrorString, _
                                         sUsercode)

            If (varErrorString <> "") Then

                ''Logging.LogInformation("FindVehicleRequest Error : ", varErrorString)
                If StringContain(varErrorString, "No Records Found") = True Then

                    If varErrorString = "" Then
                        sErrorString = String.Empty
                        GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                    Else
                        sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                        GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                    End If
                    Exit Function
                End If


            End If
            ''Logging.LogInformation("FindVehicleRequest Result : ", szVhrId)


            If (szVhrId.Length) > 0 Then

                ''Logging.LogInformation("GetVehicleSearchType :", "Invoke")
                szRequestType = GetVehicleSearchType(szVhrId, varErrorString, sUsercode)


                If varErrorString <> "" Then

                    ''Logging.LogInformation("GetVehicleSearchType Error : ", varErrorString)

                    If varErrorString = "" Then
                        sErrorString = String.Empty
                        GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                    Else
                        sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                        GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                    End If
                    Exit Function
                End If
                ''Logging.LogInformation("GetVehicleSearchType Result : ", szRequestType)

                Aurora.Reservations.Services.AuroraDvass.setSelectedCountry(nLngCountry)

                If Not (varByPass.ToUpper.Equals("BYPASS")) Then
                    Logging.LogInformation("queryRequestVehicleAvailability :", "Invoke")
                    If (Aurora.Reservations.Services.AuroraDvass.queryRequestVehicleAvailability(szVhrId, _
                                                                 szRequestType, _
                                                                 nMaximumResultsRequired, _
                                                                 dblPriority, _
                                                                 nVisibility, _
                                                                 lngNumberOfVehiclesFound, _
                                                                 szProductId, _
                                                                 szAvailableVehicles)) = False Then
                        varErrorString = szAvailableVehicles
                        If varErrorString = "" Then
                            sErrorString = String.Empty
                            GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                            ''Logging.LogInformation("queryRequestVehicleAvailability Result: ", GetAvailabilityWithCost)
                        Else
                            sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                            GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                            ''Logging.LogInformation("queryRequestVehicleAvailability Error : ", GetAvailabilityWithCost)
                            Exit Function
                        End If


                    End If
                    Logging.LogInformation("queryRequestVehicleAvailability Result : ", "True, no. of available is " & szAvailableVehicles)


                    ''reV:mia June 9 2011 - skip other calculations and return the value
                    If IsQuickAvail = True Then
                        ''Logging.LogInformation("queryRequestVehicleAvailability Result(Bypass): ", varByPass)
                        Logging.LogInformation("queryRequestVehicleAvailability Result(QuickAvail) : ", IsQuickAvail)
                        Return szAvailableVehicles
                    End If
                Else
                    ''reV:mia June 9 2011 - skip other calculations and return the value
                    If (varByPass.ToUpper.Equals("BYPASS")) Then
                        If IsQuickAvail = True Then
                            ''Logging.LogInformation("queryRequestVehicleAvailability Result(QuickAvail) : ", IsQuickAvail)
                            ''Logging.LogInformation("queryRequestVehicleAvailability Result(Bypass): ", varByPass)
                            ''Logging.LogInformation("queryRequestVehicleAvailability Result: ", szAvailableVehicles)
                            Return "<Data><AvailableVehicle><NumberOfVehicles>BYPASS</NumberOfVehicles></AvailableVehicle></Data>"
                        End If
                    End If
                End If

                If lngNumberOfVehiclesFound = 0 Then szAvailableVehicles = szProductId & "+"
                ''Logging.LogInformation("DeleteAvailableVehicle : ", "Invoke")
                If DeleteAvailableVehicle( _
                                          szVhrId, _
                                          sCkoLocCode.Trim, _
                                          sCkiLocCode.Trim, _
                                          szAvailableVehicles, _
                                          CInt(lngNumberOfVehiclesFound), _
                                          varErrorString, _
                                          sUsercode) = False Then


                    If varErrorString = "" Then
                        sErrorString = String.Empty
                        GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                        ''Logging.LogInformation("DeleteAvailableVehicle Result : ", GetAvailabilityWithCost)
                    Else
                        sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                        GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                        ''Logging.LogInformation("DeleteAvailableVehicle Error : ", GetAvailabilityWithCost)
                        Exit Function
                    End If
                End If
                ''Logging.LogInformation("DeleteAvailableVehicle Result : ", "True")

                If lngNumberOfVehiclesFound = 0 Or varByPass.ToString.Equals("BYPASS") Then
                    ''Logging.LogInformation("CreateAvvWhenDVASSReturnsNo : ", "Invoke (Vehicle found is zero or its a bypass)")

                    If CreateAvvWhenDVASSReturnsNo( _
                                                   szVhrId, _
                                                   sCkoLocCode, _
                                                   dCkoWhen, _
                                                   sCkoAmPm, _
                                                   sCkiLocCode, _
                                                   dCkiWhen, _
                                                   sCkiAmPm, _
                                                   iHirePeriod, _
                                                   iHirePeriodUOM, _
                                                   sUsercode, _
                                                   sProgrammName, _
                                                   varErrorString) = False Then


                        If varErrorString = "" Then
                            sErrorString = String.Empty
                            GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                            ''Logging.LogInformation("CreateAvvWhenDVASSReturnsNo Result : ", GetAvailabilityWithCost)
                        Else
                            sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                            GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                            ''Logging.LogInformation("CreateAvvWhenDVASSReturnsNo Error : ", GetAvailabilityWithCost)
                            Exit Function
                        End If
                    End If

                    ''Logging.LogInformation("CreateAvvWhenDVASSReturnsNo Result : ", "True")
                Else


                    objDom = ValidXML(szAvailableVehicles)
                    ''Logging.LogInformation("Vehicle found is greater than zero and  its not a bypass : ", szAvailableVehicles)

                    If objDom Is Nothing Then

                        varErrorString = getMessageFromDB("GEN023")

                        If varErrorString = "" Then
                            sErrorString = String.Empty
                            GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                            ''Logging.LogInformation("getMessageFromDB Result : ", GetAvailabilityWithCost)
                        Else
                            sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                            GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                            ''Logging.LogInformation("getMessageFromDB Error : ", GetAvailabilityWithCost)
                            Exit Function
                        End If

                    End If


                    root = objDom.DocumentElement
                    If root.SelectSingleNode("//Data").ChildNodes.Count = 0 Then
                        root = Nothing
                        objDom = Nothing

                        If varErrorString = "" Then
                            sErrorString = String.Empty
                            GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                        Else
                            sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                            GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                            Exit Function
                        End If

                    End If

                    nodeList = root.SelectSingleNode("//Data").ChildNodes
                    ''Logging.LogInformation("CreateAvailableVehicle : ", "Invoke")
                    If CreateAvailableVehicle( _
                                              szVhrId, _
                                              sCkoLocCode, _
                                              dCkoWhen, _
                                              sCkoAmPm, _
                                              sCkiLocCode, _
                                              dCkiWhen, _
                                              sCkiAmPm, _
                                              iHirePeriod, _
                                              iHirePeriodUOM, _
                                              sUsercode, _
                                              sProgrammName, _
                                              szAvailableVehicles, _
                                              varErrorString) = False Then

                        If varErrorString = "" Then
                            sErrorString = String.Empty
                            GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                            ''Logging.LogInformation("CreateAvailableVehicle Result : ", GetAvailabilityWithCost)
                        Else
                            sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                            GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                            ''Logging.LogInformation("CreateAvailableVehicle Error : ", GetAvailabilityWithCost)
                            Exit Function
                        End If
                    End If

                    ''Logging.LogInformation("CreateAvailableVehicle Result : ", "True")

                    BlockingMessage = varErrorString ''rev:mia nov.14 - this will the blocking message 
                End If

                ''Logging.LogInformation("CreateAvailableVehiclePackage : ", "Invoke")
                If CreateAvailableVehiclePackage( _
                                                 szVhrId, _
                                                 sBooId, _
                                                 sBpdId, _
                                                 sUsercode, _
                                                 sProgrammName, _
                                                 varErrorString, _
                                                 PromoCode) = False Then ''rev:mia Oct 16 2014 - addition of PromoCode
                    If varErrorString = "" Then
                        sErrorString = String.Empty
                        GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                        ''Logging.LogInformation("CreateAvailableVehiclePackage Result : ", GetAvailabilityWithCost)
                    Else
                        sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                        GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                        ''Logging.LogInformation("CreateAvailableVehiclePackage Error : ", GetAvailabilityWithCost)
                        Exit Function
                    End If

                End If

                ''Logging.LogInformation("CreateAvailableVehiclePackage Result : ", "True")


                ''Logging.LogInformation("GetPackageXML : ", "Invoke")
                szXMLReturn = GetPackageXML( _
                                            szVhrId, _
                                            CInt(nVehicleNumber), _
                                            varErrorString, _
                                            sUsercode)


                If IsNothing(varErrorString) Then

                    If varErrorString = "" Then
                        sErrorString = String.Empty
                        GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                        ''Logging.LogInformation("GetPackageXML Result : ", GetAvailabilityWithCost)
                    Else
                        sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                        GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                        ''Logging.LogInformation("GetPackageXML Error : ", GetAvailabilityWithCost)
                        Exit Function
                    End If

                End If



                root = Nothing
                objDom = Nothing

            Else 'did not find VehicleRequest
                'Logging.LogInformation("GetAvailabilityWithCost", String.Concat(sUsercode, " - No Vehicle Request Id Found"))
                If varErrorString = "" Then
                    sErrorString = String.Empty
                    GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")
                Else
                    sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                    GetAvailabilityWithCost = String.Concat("<Data>", sErrorString, "</Data>")
                    Exit Function
                End If

            End If

            GetAvailabilityWithCost = String.Concat("<Data>", szXMLReturn, "</Data>")


        Catch ex As Exception

            sError = ex.Message
            sErrorNo = "Exception"

            GetAvailabilityWithCost = String.Concat("<Root>", GetErrorTextFromResource(True, sErrorNo, "System", sError), "</Root>")

        End Try

        ''Logging.LogInformation("Final GetAvailabilityWithCost:", GetAvailabilityWithCost)
    End Function


    Private Shared Function GetPackageXML( _
                                 ByVal sVhrId As String, _
                                 ByVal DvassReturnedNo As Integer, _
                                 ByRef varErrorString As String, _
                                 Optional ByVal sUsercode As String = "") As String





        Dim objDom As New Xml.XmlDocument
        Dim szReturnQuery As String = String.Empty
        Dim szQueryString As String = String.Concat("RES_GetAvailQueryXML '", sVhrId, "', ", CStr(DvassReturnedNo))
        Dim xmlString As String = "<Error><ErrStatus>"
        GetPackageXML = Nothing


        Try


            'szReturnQuery = Aurora.Common.Data..ExecuteScalarSP("RES_GetAvailQueryXML", sVhrId, CStr(DvassReturnedNo))
            Dim xmldoc As New XmlDocument
            xmldoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetAvailQueryXML_OSLO", sVhrId, CStr(DvassReturnedNo))

            szReturnQuery = xmldoc.OuterXml

            If szReturnQuery.IndexOf(xmlString) > 0 Then
                varErrorString = String.Empty
            Else
                GetPackageXML = szReturnQuery
                varErrorString = "SUCCESS"
            End If

            'Logging.LogInformation("GetPackageXML", "Executing RES_GetAvailQueryXML() Success")

        Catch ex As Exception
            varErrorString = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message & " GetPackageXML ") & "</Root>"

        Finally
            If Not (objDom Is Nothing) Then objDom = Nothing
        End Try

        Return GetPackageXML
    End Function

    Private Shared Function CreateAvailableVehiclePackage( _
                                             ByVal sVhrId As String, _
                                             ByVal sBooId As String, _
                                             ByVal sBpdId As String, _
                                             ByVal sUsercode As String, _
                                             ByVal sProgrammName As String, _
                                             ByRef varErrorString As String, _
                                             Optional ByVal PromoCode As String = "") As Boolean ''rev:mia Oct 16 2014 - addition of PromoCode


        Dim szVehicleRequest As String = String.Empty
        'Dim szQueryString As String = String.Empty
        'szQueryString = String.Concat("RES_CreateAvailableVehiclePackage '", sVhrId, "', '" & sBooId & "', '" & sBpdId & "', '" & sUsercode & "', '", sProgrammName, "'")


        Try
            ''''rev:mia Oct 16 2014 - addition of PromoCode
            szVehicleRequest = Aurora.Common.Data.ExecuteScalarSP("RES_CreateAvailableVehiclePackage_OSLO", sVhrId, sBooId, PromoCode, sBpdId, sUsercode, sProgrammName)

            If Not String.IsNullOrEmpty(szVehicleRequest) Then
                CreateAvailableVehiclePackage = False
                varErrorString = "Error"
            Else
                CreateAvailableVehiclePackage = True
                varErrorString = String.Empty
            End If
        Catch ex As Exception

            CreateAvailableVehiclePackage = False

        End Try

        Return CreateAvailableVehiclePackage

    End Function

    Private Shared Function CreateAvailableVehicle( _
                                      ByVal sVhrId As String, _
                                      ByVal sCkoLocCode As String, _
                                      ByVal dCkoWhen As String, _
                                      ByVal sCkoAmPm As String, _
                                      ByVal sCkiLocCode As String, _
                                      ByVal dCkiWhen As String, _
                                      ByVal sCkiAmPm As String, _
                                      ByVal sHirePeriod As String, _
                                      ByVal sHirePeriodUom As String, _
                                      ByVal sUsercode As String, _
                                      ByVal sProgrammName As String, _
                                      ByVal DvassXml As String, _
                                      ByRef varErrorString As String) As Boolean

        Dim objDom As New Xml.XmlDocument
        Dim parObjDom As New Xml.XmlDocument
        Dim newElem As Xml.XmlElement
        Dim root As Xml.XmlElement
        Dim paramRoot As Xml.XmlElement
        Dim nodeList As Xml.XmlNodeList
        Dim node As Xml.XmlNode
        Dim sVehicleXML As String


        Try

            parObjDom = ValidXML("<Vehicle></Vehicle>")
            If parObjDom Is Nothing Then
                varErrorString = getMessageFromDB("GEN023")
                Exit Function
            End If
            paramRoot = parObjDom.DocumentElement

            objDom = ValidXML(DvassXml)
            If objDom Is Nothing Then
                varErrorString = getMessageFromDB("GEN023")
                Exit Function
            End If

            root = objDom.DocumentElement
            If root.SelectSingleNode("//Data").ChildNodes.Count = 0 Then
                root = Nothing : objDom = Nothing
                Return False
            End If

            nodeList = root.SelectSingleNode("//Data").ChildNodes
            For Each node In nodeList
                Dim tmpDate As String
                newElem = parObjDom.CreateElement("veh")
                newElem.SetAttribute("ID", node.ChildNodes(0).InnerText)
                newElem.SetAttribute("ckoLoc", node.ChildNodes(1).InnerText)
                tmpDate = DatePart(DateInterval.Day, CDate(node.ChildNodes(2).InnerText)) & "/" & DatePart(DateInterval.Month, CDate(node.ChildNodes(2).InnerText)) & "/" & DatePart(DateInterval.Year, CDate(node.ChildNodes(2).InnerText))
                newElem.SetAttribute("ckoDate", tmpDate) ''node.childNodes(2).Text
                newElem.SetAttribute("ckoAMPM", "")
                newElem.SetAttribute("ckiLoc", node.ChildNodes(3).InnerText)
                tmpDate = DatePart(DateInterval.Day, CDate(node.ChildNodes(4).InnerText)) & "/" & DatePart(DateInterval.Month, CDate(node.ChildNodes(4).InnerText)) & "/" & DatePart(DateInterval.Year, CDate(node.ChildNodes(4).InnerText))
                newElem.SetAttribute("ckiDate", tmpDate)
                newElem.SetAttribute("ckiAMPM", "")
                newElem.SetAttribute("NumAvail", node.ChildNodes(7).InnerText)
                newElem.SetAttribute("rel", "")
                newElem.SetAttribute("pref", node.ChildNodes(5).InnerText)
                paramRoot.AppendChild(newElem)
            Next

            sVehicleXML = paramRoot.OuterXml

            nodeList = Nothing
            root = Nothing
            objDom = Nothing

            ''POC
            varErrorString = Aurora.Common.Data.ExecuteScalarSP("RES_CreateAvailableVehicle_OSLO_POS", sVhrId, sCkoLocCode, dCkoWhen, sCkoAmPm, sCkiLocCode, dCkiWhen, sCkiAmPm, sHirePeriod, sHirePeriodUom, sUsercode, sProgrammName, sVehicleXML)
            ''varErrorString = Aurora.Common.Data.ExecuteScalarSP("RES_CreateAvailableVehicle_OSLO", sVhrId, sCkoLocCode, dCkoWhen, sCkoAmPm, sCkiLocCode, dCkiWhen, sCkiAmPm, sHirePeriod, sHirePeriodUom, sUsercode, sProgrammName, sVehicleXML)


            If StringContain(varErrorString, "GEN045") = True Then
                varErrorString = String.Empty
                CreateAvailableVehicle = True

                ''REV:Manny POC
            ElseIf StringContain(varErrorString, "GEN172") = True Then
                ''orig
                ''varErrorString = String.Empty
                ''CreateAvailableVehicle = True

                CreateAvailableVehicle = True
            Else
                CreateAvailableVehicle = False
            End If

            
        Catch ex As Exception
            CreateAvailableVehicle = False
        End Try
    End Function

    Private Shared Function CheckParameters(ByVal ParamArray param As String()) As Boolean
        Dim paramstring As String
        For Each paramstring In param
            paramstring = paramstring.Trim
            If (paramstring.Length = 0) Then
                Return False
            End If
            If (paramstring Is Nothing) Then
                Return False
            End If
            If (paramstring = "") Then
                Return False
            End If
            If (paramstring Is String.Empty) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Shared Function CreateAvvWhenDVASSReturnsNo( _
                                               ByVal sVhrId As String, _
                                               ByVal sVhrCkoLocCode As String, _
                                               ByVal sVhrCkoLocDate As String, _
                                               ByVal sVhrCkoAmPm As String, _
                                               ByVal sVhrCkiLocCode As String, _
                                               ByVal sVhrCkiLocDate As String, _
                                               ByVal sVhrCkiAmPm As String, _
                                               ByVal VhrHirePeriod As String, _
                                               ByVal VhrHirePeriodUOM As String, _
                                               ByVal sUsercode As String, _
                                               ByVal sProgrammName As String, _
                                               ByRef varErrorString As String) As Boolean

        If CheckParameters(sVhrId) = False Then
            varErrorString = "CreateAvvWhenDVASSReturnsNo(), Invalid parameter"
            Return False
        End If

        Try
            ''sVhrCkoLocDate = sVhrCkoLocDate.Split(" ")(0)
            ''sVhrCkiLocDate = sVhrCkiLocDate.Split(" ")(0)
            varErrorString = Aurora.Common.Data.ExecuteScalarSP("RES_CreateAvvWhenDVASSReturnsNo", sVhrId, _
                                                                                                   sVhrCkoLocCode, _
                                                                                                   sVhrCkoLocDate, _
                                                                                                   sVhrCkoAmPm, _
                                                                                                   sVhrCkiLocCode, _
                                                                                                   sVhrCkiLocDate, _
                                                                                                   sVhrCkiAmPm, _
                                                                                                   VhrHirePeriod, _
                                                                                                   VhrHirePeriodUOM, _
                                                                                                   sUsercode, _
                                                                                                   sProgrammName, _
                                                                                                   "No", _
                                                                                                   "")

            'sVhrCkoLocDate = sVhrCkoLocDate.Split(" ")(0)
            'sVhrCkiLocDate = sVhrCkiLocDate.Split(" ")(0)
            'varErrorString = Aurora.Common.Data.ExecuteScalarSP("RES_CreateAvvWhenDVASSReturnsNo_OSLO", sVhrId, _
            '                                                                                       sVhrCkoLocCode, _
            '                                                                                       sVhrCkoLocDate, _
            '                                                                                       sVhrCkoAmPm, _
            '                                                                                       sVhrCkiLocCode, _
            '                                                                                       sVhrCkiLocDate, _
            '                                                                                       sVhrCkiAmPm, _
            '                                                                                       VhrHirePeriod, _
            '                                                                                       VhrHirePeriodUOM, _
            '                                                                                       sUsercode, _
            '                                                                                       sProgrammName, _
            '                                                                                       "No", _
            '                                                                                       "")
            If varErrorString.IndexOf("GEN045") = -1 Then
                Return False
            Else
                varErrorString = String.Empty
                CreateAvvWhenDVASSReturnsNo = True
            End If

        Catch ex As Exception

            CreateAvvWhenDVASSReturnsNo = False

        End Try

        Return CreateAvvWhenDVASSReturnsNo
    End Function

    Private Shared Function DeleteAvailableVehicle( _
                                      ByVal sVhrId As String, _
                                      ByVal CkoLoc As String, _
                                      ByVal CkiLoc As String, _
                                      ByVal DvassXml As String, _
                                      ByVal DVASSNumberOfVehicle As Integer, _
                                      ByRef varErrorString As String, _
                                      Optional ByVal sUsercode As String = "") As Boolean


        Dim objDom As New Xml.XmlDocument
        Dim root As Xml.XmlElement = Nothing
        Dim nodeList As Xml.XmlNodeList = Nothing
        Dim node As Xml.XmlNode = Nothing
        Dim szProdIdList As String = String.Empty
        Dim tckoLoc As String = String.Empty
        Dim tckiLoc As String = String.Empty
        Dim extraChar As String = String.Empty

        Try

            If DVASSNumberOfVehicle > 0 Then

                objDom = ValidXML(DvassXml)
                If objDom Is Nothing Then
                    varErrorString = getMessageFromDB("GEN023")
                    Exit Function
                End If

                root = objDom.DocumentElement

                If root.SelectSingleNode("//Data").ChildNodes.Count = 0 Then
                    root = Nothing
                    objDom = Nothing
                    Return False
                End If

                nodeList = root.SelectSingleNode("//Data").ChildNodes

                For Each node In nodeList
                    tckoLoc = node.ChildNodes(1).InnerText.Trim
                    tckiLoc = node.ChildNodes(3).InnerText.Trim

                    If tckoLoc.Equals(CkoLoc) AndAlso tckiLoc.Equals(CkiLoc) Then
                        extraChar = "+"
                    Else
                        extraChar = "-"
                    End If

                    If (szProdIdList.Length) > 0 Then
                        szProdIdList = String.Concat(szProdIdList, ",")
                    End If
                    szProdIdList = String.Concat(szProdIdList, node.ChildNodes(0).InnerText, extraChar)
                Next

                nodeList = Nothing
                root = Nothing
                objDom = Nothing

                If Len(szProdIdList) = 0 Then
                    Exit Function
                End If

            Else 'else for DVASSNumberOfVehicle > 0
                szProdIdList = DvassXml
            End If 'end if for DVASSNumberOfVehicle > 0



            varErrorString = Aurora.Common.Data.ExecuteScalarSP("RES_DeleteAvailableVehicle", sVhrId, szProdIdList)
            If Not String.IsNullOrEmpty(varErrorString) Then
                Return False
            End If

            varErrorString = String.Empty
            Return True


        Catch ex As Exception

            Return False

        End Try




    End Function

    Private Shared Function GetVehicleSearchType( _
                                     ByVal sVhrId As String, _
                                     ByRef varErrorString As String, _
                                     ByVal sUsercode As String) As String


        Dim szVehicleSearchType As String = String.Empty
        Dim objDom As New Xml.XmlDocument
        Dim root As Xml.XmlElement = Nothing
        Dim szVechileSType As String = String.Empty
        Dim szQueryString As String = String.Concat("RES_getRequestType '", sVhrId, "'")
        Dim xmlString As String = "<Error><ErrStatus>"
        GetVehicleSearchType = Nothing



        Logging.LogInformation("GetVehicleSearchType", "Executing " & szQueryString)

        Try


            szVehicleSearchType = Aurora.Common.Data.ExecuteSqlXmlSP("RES_getRequestType", sVhrId)
            If szVehicleSearchType.Contains("<Error><ErrStatus>") Or (szVehicleSearchType.Length = 0) Then
                varErrorString = GetErrorTextFromResource(True, "", "Application", "No Records Found")
                Return GetErrorTextFromResource(True, "", "Application", "No Records Found")
            End If


            objDom = ValidXML(szVehicleSearchType)
            If objDom Is Nothing Then
                varErrorString = getMessageFromDB("GEN023")
                Return varErrorString
            End If
            root = objDom.DocumentElement



            If root.ChildNodes(0).ChildNodes(0).Name.Equals("VhrRequestType") Then
                szVechileSType = root.ChildNodes(0).ChildNodes(0).InnerText
            End If

            If (szVechileSType.Length) > 0 Then
                GetVehicleSearchType = szVechileSType
            End If

            'Logging.LogInformation("GetVehicleSearchType", "Executing RES_getRequestType() Success")

        Catch ex As Exception

            GetVehicleSearchType = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message & " GetVehicleSearchType") & "</Root>"
            'Logging.LogException("GetVehicleSearchType", ex)

        Finally
            objDom = Nothing
            root = Nothing
        End Try

        Return GetVehicleSearchType

    End Function

    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                          ByVal sErrNumber As String, _
                                          ByVal sErrType As String, _
                                          ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function


    Public Shared Function manageAvailableVehicle _
         (ByVal sVhrId As String, _
          ByVal sXmlDataString As String) As String


        manageAvailableVehicle = Nothing


        Dim sXmlString As String = Nothing
        Dim sErrorString As String = Nothing
        Dim sErrNo As String = Nothing
        Dim sErrDesc As String = Nothing
        Dim sQueryString As String = Nothing


        Dim xsrpIds As String() = Nothing
        Dim oSelVehXmlDom As New Xml.XmlDocument


        Dim hasXMLerror As Boolean = True



        oSelVehXmlDom = ValidXML(sXmlDataString)
        If (oSelVehXmlDom Is Nothing) Then
            sXmlString = getMessageFromDB("GEN023")
            sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", sXmlString)
            Return String.Concat("<Root>", sErrorString, "<Data></Data></Root>")
        Else
            hasXMLerror = False
        End If


        Try

            If (hasXMLerror = False) Then

                sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_manageAvailableVehicle", oSelVehXmlDom.OuterXml)

                If Not (sXmlString.StartsWith("GEN046/GEN046")) Then
                    xsrpIds = sXmlString.Split("/")
                    sErrNo = xsrpIds(0)
                    sErrDesc = xsrpIds(1)

                    If sErrNo.Equals("GEN045") Or _
                       sErrNo.Equals("GEN046") Or _
                       sErrNo.Equals("GEN047") Then

                        sErrorString = GetErrorTextFromResource("False", sErrNo, "Application", sErrDesc)

                    Else
                        sErrorString = GetErrorTextFromResource("True", sErrNo, "Application", sErrDesc)
                    End If ''END IF FOR sErrNo = "GEN045"

                Else ''ELSE FOR sXmlString.Equals("SUCCESS")


                    sErrorString = GetErrorTextFromResource("False", "GEN046", "Application", sXmlString)

                End If ''END IF FOR sXmlString.Equals("SUCCESS")

                Dim xmldoc As XmlDocument
                ''rev:mia sept18
                xmldoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetAvailQueryXML_OSLO", sVhrId, "0")
                ''xmldoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetAvailQueryXML", sVhrId, "0")

                sXmlString = xmldoc.OuterXml
                manageAvailableVehicle = String.Concat("<Root>", sErrorString, "<Data>", sXmlString, "</Data></Root>")

            End If ''END IF FOR hasXMLerror


        Catch ex As Exception

            manageAvailableVehicle = String.Concat("<Root>", GetErrorTextFromResource(True, "Exception", "System", ex.Message & " manageAvailableVehicle "), "</Root>")
            'Logging.LogException("ManageAvailableVehicle", ex)

        End Try

        Return manageAvailableVehicle

    End Function
#End Region

#Region " Protected Properties "
    Private Shared mOutputParameter As String
    Private Shared Property OutputParameter() As String
        Get
            Return mOutputParameter
        End Get
        Set(ByVal value As String)
            mOutputParameter = value
        End Set
    End Property
    Private Shared mErrorMessage As String = ""
    Public Shared Property ErrroMessage() As String
        Get
            Return mErrorMessage
        End Get
        Set(ByVal value As String)
            mErrorMessage = value
        End Set
    End Property

#End Region

End Class
