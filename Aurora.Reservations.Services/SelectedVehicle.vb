
Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class SelectedVehicle

    Private Shared oSelVehXmlDom As System.Xml.XmlDocument   'Global Dom object
    'Private oContext As ObjectContext        'Global Transaction object

    Private Shared sError As String
    Private Shared mOutputParameter As String
    Private Shared Property OutputParameter() As String
        Get
            Return mOutputParameter
        End Get
        Set(ByVal value As String)
            mOutputParameter = value
        End Set
    End Property
    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
        'Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("sp_get_ErrorString", params)
        'If Not String.IsNullOrEmpty(OutputParameter) Then
        '    Return xmlstring
        'Else
        '    Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", xmlstring)
        'End If
        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Public Shared Function manageRecalculateRates(ByVal xFleetModelforBookableProduct As String) As String

        Dim sBkrId, sXmlString, sErrorString, sErrNo, sErrDesc As String
        Dim xsrpIds As Object, sBooId As String

        oSelVehXmlDom = New System.Xml.XmlDocument

        Try
            '==== Check for valid xml ====
            Try
                oSelVehXmlDom.LoadXml(xFleetModelforBookableProduct)
            Catch ex As System.Xml.XmlException
                sXmlString = getMessageFromDB("GEN023")
                sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", sXmlString)
                manageRecalculateRates = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End Try

            sBkrId = oSelVehXmlDom.DocumentElement.SelectSingleNode("//Root/Header/@id").InnerText
            sBooId = oSelVehXmlDom.DocumentElement.SelectSingleNode("//Root/BooId").InnerText


            sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_recalculate_Rates", oSelVehXmlDom.InnerXml)
            If Not String.IsNullOrEmpty(sXmlString) Then
                If (sXmlString <> "SUCCESS") Then
                    xsrpIds = Split(sXmlString, "/")
                    sErrNo = xsrpIds(0)
                    sErrDesc = xsrpIds(1)
                    sErrorString = GetErrorTextFromResource("True", sErrNo, "Application", sErrDesc)
                Else
                    sErrorString = GetErrorTextFromResource("False", String.Empty, "Application", String.Empty)
                End If
            Else
                sErrorString = GetErrorTextFromResource("False", String.Empty, "Application", String.Empty)
            End If



            Dim xmldoc As XmlDocument
            ''rev:mia sept18
            ''xmldoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_SummaryOfChosenVehicles", sBkrId, sBooId, "", "")
            xmldoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_SummaryOfChosenVehicles_OSLO", sBkrId, sBooId, "", "")
            manageRecalculateRates = "<Root>" & sErrorString & "<Data>" & xmldoc.OuterXml & "</Data></Root>"

            
        Catch ex As Exception
            manageRecalculateRates = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message) & "</Root>"
            Logging.LogError("", "manageAgentPackage" & ex.Message)
        End Try


    End Function

    Public Shared Function manageAvailableVehicle(ByVal sVhrId As String, ByVal sXmlDataString As String) As String

        Dim sXmlString, sErrorString, sErrNo, sErrDesc As String
        Dim xsrpIds As Object

        oSelVehXmlDom = New System.Xml.XmlDocument

        Try
            '==== Check for valid xml ====
            Try
                oSelVehXmlDom.LoadXml(sXmlDataString)
            Catch ex As System.Xml.XmlException
                sXmlString = getMessageFromDB("GEN023")
                sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", sXmlString)
                manageAvailableVehicle = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End Try

            sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_manageAvailableVehicle", oSelVehXmlDom.InnerXml)


            If (sXmlString <> "SUCCESS") Then
                xsrpIds = Split(sXmlString, "/")
                sErrNo = xsrpIds(0)
                sErrDesc = xsrpIds(1)
                If sErrNo = "GEN045" Or sErrNo = "GEN046" Or sErrNo = "GEN047" Then
                    sErrorString = GetErrorTextFromResource("False", sErrNo, "Application", sErrDesc)
                Else
                    sErrorString = GetErrorTextFromResource("True", sErrNo, "Application", sErrDesc)
                End If
            Else
                sErrorString = GetErrorTextFromResource("False", String.Empty, "Application", String.Empty)
            End If

            sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_GetAvailQueryXML", sVhrId, 0)


            manageAvailableVehicle = "<Root>" & sErrorString & "<Data>" & sXmlString & "</Data></Root>"

        Catch ex As Exception
            manageAvailableVehicle = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message) & "</Root>"
            Logging.LogError("", "manageAvailableVehicle" & ex.Message)
        End Try


    End Function


    Public Shared Function manageDuplicateVehicleRequest(ByVal sXmlDataString As String) As String

        Dim sXmlString, sErrorString, sErrNo, sErrDesc, sNewVhrId As String
        Dim saIds As String()

        oSelVehXmlDom = New System.Xml.XmlDocument

        Try
            '==== Check for valid xml ====
            Try
                oSelVehXmlDom.LoadXml(sXmlDataString)

            Catch ex As System.Xml.XmlException
                sXmlString = getMessageFromDB("GEN023")
                sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", sXmlString)
                manageDuplicateVehicleRequest = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End Try

            sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_createDuplicateVehicleRequest", oSelVehXmlDom.InnerXml)

            If (sXmlString <> "SUCCESS") Then
                saIds = Split(sXmlString, "/")
                sErrNo = saIds(0)
                sErrDesc = saIds(1)
                If sErrNo = "GEN045" Or sErrNo = "GEN046" Or sErrNo = "GEN047" Then
                    sNewVhrId = saIds(2)
                    sErrorString = GetErrorTextFromResource("False", sErrNo, "Application", sErrDesc)
                Else

                    sErrorString = GetErrorTextFromResource("True", sErrNo, "Application", sErrDesc)
                End If
            Else

                sErrorString = GetErrorTextFromResource("False", String.Empty, "Application", String.Empty)
            End If
            manageDuplicateVehicleRequest = "<Root>" & sErrorString & "<Data>" & CStr(sNewVhrId) & "</Data></Root>"

        Catch ex As Exception
            manageDuplicateVehicleRequest = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message) & "</Root>"
            Logging.LogError("", "manageDuplicateVehicleRequest" & ex.Message)

        End Try

    End Function







    ''' <summary>
    ''' GetErrorTextFromResource function will Build a proper ErrorString and send
    ''' it to the calling method as a string.
    ''' GetErrorTextFromResource function will be called from all the methods of
    ''' AuroraSalesMark Component.
    ''' </summary>
    ''' <param name="bErrStatus"></param>
    ''' <param name="sErrNumber"></param>
    ''' <param name="sErrType"></param>
    ''' <param name="sErrDescription"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                           ByVal sErrNumber As String, _
                                           ByVal sErrType As String, _
                                           ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

#Region "rev:mia jan 23 2009"
    Public Shared Function VehicleRequestUpdateBypassValue(ByVal LoginUserID As String, _
                                                            ByVal sAgnId As String, _
                                                            ByVal dCkoDate As Date, _
                                                            ByVal dCkiDate As Date, _
                                                            ByVal lHirePeriod As Integer, _
                                                            ByVal sCkoLocation As String, _
                                                            ByVal sCkiLocation As String, _
                                                            ByVal BookedDate As Date, _
                                                            ByVal sPrdId As String, _
                                                            ByVal sPkgId As String, _
                                                            ByVal VhrId As String) As Boolean



        Try


            Dim params(11) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("LoginUserID", DbType.String, LoginUserID, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ProgramName", DbType.String, "BookingProcess.aspx", Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("sAgnId", DbType.String, sAgnId, Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, dCkoDate, Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, dCkiDate, Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("lHirePeriod", DbType.Int16, lHirePeriod, Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("sCkoLocation", DbType.String, sCkoLocation, Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("sCkiLocation", DbType.String, sCkiLocation, Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("BookedDate", DbType.DateTime, BookedDate, Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("sPrdId", DbType.String, sPrdId, Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("sPkgId", DbType.String, sPkgId, Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("VhrId", DbType.String, VhrId, Parameter.ParameterType.AddInParameter)
            Aurora.Common.Data.ExecuteOutputSP("RES_VehicleRequestUpdateBypassValue", params)

        Catch ex As Exception
            Logging.LogError("VehicleRequestUpdateBypassValue", ex.Message)
            Return False
        End Try
        Return True


    End Function
#End Region
End Class
