Imports Aurora.Common.Data
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class VehicleRequest
    Private Shared sQueryResult As String

    Public Shared Sub getVehicleRequest( _
                                ByVal pVhrId As String, _
                                ByRef pXml As String)


        Dim dicParameters As New Hashtable
        Dim strTemp As String = Nothing

        pXml = Nothing

        Dim params(1) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("VhrId", DbType.String, pVhrId, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("ReturnXml", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
        Try

            Aurora.Common.Data.ExecuteOutputSP("RES_getVehicleRequest", params)
            pXml = params(1).Value
            sQueryResult = "SUCCESS"
        Catch ex As Exception
            sQueryResult = "FAIL"
        End Try
        
        
    End Sub
End Class
