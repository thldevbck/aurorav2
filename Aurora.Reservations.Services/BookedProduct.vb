Imports Aurora.Common
Imports Aurora.Common.data
Imports Aurora.Reservations.Data.DataRepository
Imports Aurora.Reservations.Data
Imports System.text
Imports System.xml


Public Class BookedProduct

#Region "Booking Extension releated stuff"

    '    '
    '    ' ManageBookedProduct_Ext()
    '    ' =========================
    '    ' Implements the "EXT" event of the ManageBookedProduct()
    '    ' method described in the BTD 352-10 document.
    '    '
    '    '

    '    Public Shared Function ManageBookedProduct_Ext _
    '       ( _
    '          ByVal BookedProductID As String, _
    '          ByVal ExtCkoWhen As DateTime, _
    '          ByVal ExtCkiWhen As DateTime, _
    '          ByVal ApplyOriginalRates As Boolean, _
    '          ByVal BpdBooId As String, _
    '          ByVal BpdRntId As String, _
    '          ByVal CtyCode As String, _
    '          ByVal CheckOutStatus As String, _
    '          ByRef BookedProductIDNew As String, _
    '          ByRef RentalHistoryID As String, _
    '          ByVal UserId As String, _
    '          ByVal ProgramName As String, _
    '          ByRef ReturnError As String, _
    '          ByRef ReturnMessage As String _
    '       ) As Boolean

    '        On Error GoTo ErrHandler

    '        Dim sError As String, sErrorNo As String, sNewBpdId As String, strHistError As String
    '        Dim BpdArr As Object, i As Integer
    '        Dim oErrlog As Object
    '        Dim sExtref As String, sRntStatus As String, dCkoWhen As Date, dCkiWhen As Date
    '        Dim sCkoLocCode As String, sCkiLocCode As String, sDVASSSeqNum As String, sUnitNum As String
    '        Dim sRegoNum As String, sCkoDayPart As String, sCkiDayPart As String, bUpdateFleet As Boolean
    '        Dim sOddometerOut As Double, strDvassReturnError As String, strAimsReturnError As String, strWar As String


    '        Dim strError, strReturnMessage, strQueryString As String

    '        'oErrlog = CreateObject("LogError.Error")


    '        'oAuroraDAL = CreateObject("DAL.DataLayer")

    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        '   First step is to assemble parameters and execute
    '        '   ManageBookedProductExtension() stored procedure.
    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        ' BUILD parameters collection
    '        oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "--->> Start RES_ManageBookedProductExtension <<---")


    '        '<<<<<Execution>>>>>
    '        Dim oParamArray
    '        oParamArray = DataRepository.ManageBookedProductExtension(BookedProductID, ExtCkoWhen, ExtCkiWhen, ApplyOriginalRates, UserId, ProgramName)


    '        '    oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & " RETURN Value :" & strError)
    '        '    ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        '    ' IF an error is not returned, we want
    '        '    ' to retrieve the pReturnError output
    '        '    ' parameter from the collection
    '        '    ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++

    '        '<<<<<<<<<<<<<<TO BE DONE!!!!!!!!!!!>>>>>>>>>>>>>>>

    '        '    If Len(Trim(strError)) = 0 Then
    '        '        strReturnError = .RetOutPutValue("@psErrors")
    '        '        If Len(Trim(strReturnError)) <> 0 Then
    '        '            oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & " RETURN Value :" & strReturnError)
    '        '            GoTo GarbageCleaner
    '        '        End If
    '        '        sNewBpdId = Replace(.RetOutPutValue("@psBpdIdsList"), ",", "")
    '        '        strHistError = CreateHistory("", "", sNewBpdId, "", "", "BPD", "MODIFY", UserId, ProgramName & " COM, _EXT")
    '        '    Else
    '        '        strReturnError = strError
    '        '        GoTo GarbageCleaner
    '        '    End If
    '        '    strReturnMessage = .RetOutPutValue("@sWarning")
    '        'End With ' oAuroraDAL


    '        oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "--->> NO ERROR END RES_ManageBookedProductExtension <<---")

    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        ' ALWAYS update fleet
    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++

    '        '-- Fetch Information to Update DVASS
    '        oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & "<< << Preparing to Update Fleet for Extension >> >>")
    '        oAuroraDAL = Nothing
    '        oAuroraDAL = CreateObject("DAL.DataLayer")


    '        'With oAuroraDAL
    '        '    '-- Input Parameter
    '        '    Call .appendParameter("@sRntId", adVarChar, 64, strBpdRntId)
    '        '    '-- Output Paramater
    '        '    Call .appendParameter2("@sExtRef", adVarChar, 2000, sExtref, True)
    '        '    Call .appendParameter2("@sRntStatus", adVarChar, 2000, sRntStatus, True)
    '        '    Call .appendParameter2("@sCkoWhen", adVarChar, 2000, dCkoWhen, True)
    '        '    Call .appendParameter2("@sCkiWhen", adVarChar, 2000, dCkiWhen, True)
    '        '    Call .appendParameter2("@sCkoLocCode", adVarChar, 2000, sCkoLocCode, True)
    '        '    Call .appendParameter2("@sCkiLocCode", adVarChar, 2000, sCkiLocCode, True)
    '        '    Call .appendParameter2("@sDVASSSeqNum", adVarChar, 2000, sDVASSSeqNum, True)
    '        '    Call .appendParameter2("@sUnitNum", adVarChar, 2000, sUnitNum, True)
    '        '    Call .appendParameter2("@sRegoNum", adVarChar, 2000, sRegoNum, True)
    '        '    Call .appendParameter2("@sCkoDayPart", adVarChar, 2000, sCkoDayPart, True)
    '        '    Call .appendParameter2("@sCkiDayPart", adVarChar, 2000, sCkiDayPart, True)
    '        '    Call .appendParameter2("@sOddometerOut", adVarChar, 2000, sOddometerOut, True)
    '        '    Call .appendParameter2("@bUpdateFleet", adVarChar, 2000, bUpdateFleet, True)
    '        '    strQueryString = "RES_FetchDetailForExtension"
    '        '    ' EXECUTE it
    '        '    oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & "Executing SP RES_FetchDetailForExtension")
    '        '    strError = .updateRecords(strQueryString, "", True)
    '        '    oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & "Executed Return value " & strError)
    '        '    If strError = "SUCCESS" Then strError = ""
    '        '    If Len(strError) <> 0 Then
    '        '        strReturnError = strError
    '        '        GoTo GarbageCleaner
    '        '    End If
    '        oParamArray = Nothing
    '        oParamArray = DataRepository.FetchDetailForExtension(RentalHistoryID)



    '        '<<<<<<to be done - unpack the returned values!!!!!!>>>>>>>>>>>>>>>>>
    '        '    sExtref = .RetOutPutValue("@sExtRef")
    '        '    sRntStatus = .RetOutPutValue("@sRntStatus")
    '        '    dCkoWhen = .RetOutPutValue("@sCkoWhen")
    '        '    dCkiWhen = .RetOutPutValue("@sCkiWhen")
    '        '    sCkoLocCode = .RetOutPutValue("@sCkoLocCode")
    '        '    sCkiLocCode = .RetOutPutValue("@sCkiLocCode")
    '        '    sDVASSSeqNum = .RetOutPutValue("@sDVASSSeqNum")
    '        '    sUnitNum = .RetOutPutValue("@sUnitNum")
    '        '    sRegoNum = .RetOutPutValue("@sRegoNum")
    '        '    sCkoDayPart = .RetOutPutValue("@sCkoDayPart")
    '        '    sCkiDayPart = .RetOutPutValue("@sCkiDayPart")
    '        '    sOddometerOut = .RetOutPutValue("@sOddometerOut")
    '        '    bUpdateFleet = .RetOutPutValue("@bUpdateFleet")
    '        'End With


    '        oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(5) & "bUpdateFleet " & bUpdateFleet)
    '        If bUpdateFleet = True Then
    '            If UCase(sRntStatus) = "KK" Or UCase(sRntStatus) = "NN" Then
    '                '-- Call DVASS.rentalModify
    '                '-- Start DVSS for Specific Country

    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "<< << Stating DVASS for  " & sCtyCode & " >> >>")
    '                objDvassCollaborate = CreateObject("AuroraReservations.auroraDvass")
    '                If UCase(sCtyCode) = "AU" Then
    '                    Call objDvassCollaborate.selectCountry(0)
    '                ElseIf UCase(sCtyCode) = "NZ" Then
    '                    Call objDvassCollaborate.selectCountry(1)
    '                End If
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sExtref " & sExtref)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sDVASSSeqNum " & sDVASSSeqNum)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sCkoLocCode " & sCkoLocCode)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "dCkoWhen " & dCkoWhen)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sCkoDayPart " & sCkoDayPart)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sCkiLocCode " & sCkiLocCode)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "dCkiWhen " & dCkiWhen)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sCkiDayPart " & sCkiDayPart)

    '                Call objDvassCollaborate.rentalModify _
    '                        ( _
    '                           sExtref, _
    '                           sDVASSSeqNum, _
    '                           sCkoLocCode, _
    '                           dCkoWhen, _
    '                           convertStringDayPartToInteger(sCkoDayPart), _
    '                           sCkiLocCode, _
    '                           CDate(dCkiWhen), _
    '                           convertStringDayPartToInteger(sCkiDayPart), _
    '                           CDbl(0), _
    '                           1, _
    '                           10, _
    '                           "", _
    '                           1, _
    '                           strDvassReturnError _
    '                        )

    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "End od DVASS Rental Modify Return Value " & strDvassReturnError)

    '                objDvassCollaborate = Nothing
    '                If UCase(strDvassReturnError) = "SUCCESS" Then strDvassReturnError = ""

    '                If Len(strDvassReturnError) <> 0 Then
    '                    strReturnError = strDvassReturnError
    '                    GoTo GarbageCleaner
    '                End If

    '            End If  '-- If sRntStatus = "KK" Or sRntStatus = "NN" Then


    '            If sRntStatus = "CO" Then
    '                '-- Call AIMS.ActivityModify
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "<< << Updating the Activity >> >>")
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sExtref " & sExtref)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sUnitNum " & sUnitNum)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sOddometerOut " & sOddometerOut)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "sCkiLocCode " & sCkiLocCode)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "dCkiWhen " & dCkiWhen)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "Force Flag " & "<F9NotReq>")
    '                objAimsCollaborate = CreateObject("AI.CMaster")

    '                strAimsReturnError = objAimsCollaborate.F7_ActivityUpdate _
    '                          ( _
    '                             "Rental", _
    '                             sExtref, _
    '                             sUnitNum, _
    '                             sOddometerOut, _
    '                             sCkiLocCode, _
    '                             dCkiWhen, _
    '                             -1, _
    '                             "")
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(6) & "<< End of Activity Update Return Value : " & strAimsReturnError & ">>")
    '                If strAimsReturnError <> 0 Then
    '                    strReturnError = getAimsMessage(strAimsReturnError)
    '                    GoTo GarbageCleaner
    '                End If ' If AIMSRetValue <> 0 Then
    '            End If  '-- If sRntStaus = "CO" Then
    '        End If '-- bUpdateFleet

    '        GoTo GarbageCleaner
    '        BookedProductID = oAuroraDAL.RetOutPutValue("@psBpdIdsList")
    '        oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "Return BPDIds " & BookedProductID)
    '        If Len(Trim(BookedProductID)) > 0 Then
    '            BpdArr = Split(BookedProductID, ",")
    '            For i = 0 To UBound(BpdArr)
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "<< Call to ManageFleet for BPDId >>" & BpdArr(i))
    '                Call ManageFleetUpdate _
    '                  ( _
    '                     EVENT_TYPE_EXT, _
    '                     BpdArr(i), _
    '                     strCheckOutStatus, _
    '                     strBpdBooId, _
    '                     strBpdRntId, _
    '                     CStr(sCtyCode), _
    '                     strReturnError, _
    '                     strWar _
    '                  )
    '                ' PASS error back to caller
    '                oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "<< RETURN Value from ManageFleet for BPDId >>" & BpdArr(i))
    '                If Len(strReturnError) <> 0 Then GoTo GarbageCleaner
    '            Next i
    '        End If  '----  If Len(Trim(BookedProductID)) > 0 Then

    '        GoTo GarbageCleaner

    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        ' LAST (but certainly not least) step in the EVENT=Ext
    '        ' process is to update the rental history.
    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        strError = prepareAuroraDalForReuse
    '        If Len(strError) <> 0 Then
    '            ' PASS error back to caller
    '            strReturnError = strError
    '            Exit Function
    '        End If

    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        ' REACHING here means there were no errors. So, get
    '        ' ready for and execute the CheckRentalHistory method
    '        ' as described in the BTD 352-10.
    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++

    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
    '        '   NOW our work is done....
    '        ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++

    'ErrHandler:
    '        sError = Err.Description
    '        sErrorNo = Err.Number

    '        strReturnError = sError

    'GarbageCleaner:
    '        oErrlog = Nothing
    '        oAuroraDAL = Nothing


    '    End Function

    '
    ' ManageBookedProduct_Ext()
    ' =========================
    ' Implements the "EXT" event of the ManageBookedProduct()
    ' method described in the BTD 352-10 document.
    '
    '
    ''rev:mia 16Jan2015 - Added SlotId,SlotDescription and RentalId optionals
    ''rev:mia 30July2015 - Extension over a stopsell period ..AgentId
    Public Shared Function ManageBookedProduct_Ext _
       ( _
          ByVal strBookedProductID As String, _
          ByVal dteExtCkoWhen As DateTime, _
          ByVal dteExtCkiWhen As DateTime, _
          ByVal booApplyOriginalRates As Boolean, _
          ByVal booForceDVASSCalls As Boolean, _
          ByVal strBpdBooId As String, _
          ByVal strBpdRntId As String, _
          ByVal sCtyCode As String, _
          ByVal strCheckOutStatus As String, _
          ByRef strBookedProductIDNew As String, _
          ByRef strRentalHistoryID As String, _
          ByVal strUserId As String, _
          ByVal strProgramName As String, _
          ByRef strReturnError As String, _
          ByRef strReturnMessage As String, _
          Optional SlotId As String = "", _
          Optional SlotDescription As String = "", _
          Optional RentalId As String = ""
       )

        Dim sNewBpdId As String, strHistError As String
        Dim sExtref As String, sRntStatus As String, dCkoWhen As Date, dCkiWhen As Date
        Dim sCkoLocCode As String, sCkiLocCode As String, sDVASSSeqNum As String, sUnitNum As String
        Dim sRegoNum As String, sCkoDayPart As String, sCkiDayPart As String, bUpdateFleet As Boolean
        Dim sOddometerOut As Double, strDvassReturnError As String, strAimsReturnError As String, strWar As String
        Dim strError As String

        Try



            '   First step is to assemble parameters and execute
            '   ManageBookedProductExtension() stored procedure.
            strError = DataRepository.ManageBookedProductExtension(strBookedProductID, dteExtCkoWhen, dteExtCkiWhen, booApplyOriginalRates, strUserId, strProgramName, strReturnError, sNewBpdId, strReturnMessage, SlotId, SlotDescription, RentalId)

            If Len(Trim(strError)) = 0 Then
                If Len(Trim(strReturnError)) <> 0 Then
                    Throw New Exception(strReturnError)
                Else
                    sNewBpdId = Replace(sNewBpdId, ",", "")
                    strHistError = CreateHistory("", "", sNewBpdId, "", "", "BPD", "MODIFY", strUserId, strProgramName & " COM, _EXT")

                End If
            Else
                Throw New Exception(strError)
            End If


            ' ALWAYS update fleet

            '-- Fetch Information to Update DVASS
            '<< << Preparing to Update Fleet for Extension >> >>")

            strError = DataRepository.FetchDetailForExtension(strBpdRntId, sExtref, sRntStatus, dCkoWhen, _
                                                   dCkiWhen, sCkoLocCode, sCkiLocCode, sDVASSSeqNum, sUnitNum, _
                                                   sRegoNum, sCkoDayPart, sCkiDayPart, sOddometerOut, bUpdateFleet)

            If Len(strError) <> 0 Then
                Throw New Exception(strError)
            End If

            If bUpdateFleet = True Then
                If UCase(sRntStatus) = "KK" Or UCase(sRntStatus) = "NN" Then
                    '-- Call DVASS.rentalModify
                    '-- Start DVSS for Specific Country

                    Dim nSelectedCountry As Integer
                    If UCase(sCtyCode) = "AU" Then
                        nSelectedCountry = 0
                    ElseIf UCase(sCtyCode) = "NZ" Then
                        nSelectedCountry = 1
                    End If

                    strDvassReturnError = AuroraDvass.ModifyRental(nSelectedCountry, sExtref, sDVASSSeqNum, _
                                                                   dCkoWhen, convertStringDayPartToInteger(sCkoDayPart), _
                                                                   dCkiWhen, convertStringDayPartToInteger(sCkiDayPart), _
                                                                   sCkoLocCode, sCkiLocCode, CDbl(0), CDbl(1), "", CInt(booForceDVASSCalls))

                    If UCase(strDvassReturnError) = "SUCCESS" Then strDvassReturnError = ""

                    If Len(strDvassReturnError) <> 0 Then
                        Throw New Exception("DVASSMESSAGE->" & strDvassReturnError)
                    End If

                End If  '-- If sRntStatus = "KK" Or sRntStatus = "NN" Then


                If sRntStatus = "CO" Then
                    '-- Call AIMS.ActivityModify

                    Dim objAimsCollaborate As New AI.CMaster

                    strAimsReturnError = objAimsCollaborate.F7_ActivityUpdate("Rental", sExtref, sUnitNum, sOddometerOut, sCkiLocCode, dCkiWhen, -1, "")

                    If strAimsReturnError <> 0 Then
                        Throw New Exception(getAimsMessage(strAimsReturnError))

                    End If ' If AIMSRetValue <> 0 Then
                End If  '-- If sRntStaus = "CO" Then
            End If '-- bUpdateFleet

            'logically ends here!!
            'GoTo GarbageCleaner

            'no sense here - > its jumping ahead
            ' so not bothering with rest of the stuff


            'strBookedProductID = oAuroraDAL.RetOutPutValue("@psBpdIdsList")
            'oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "Return BPDIds " & strBookedProductID)
            'If Len(Trim(strBookedProductID)) > 0 Then
            '    BpdArr = Split(strBookedProductID, ",")
            '    For i = 0 To UBound(BpdArr)
            '        oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "<< Call to ManageFleet for BPDId >>" & BpdArr(i))
            '        'Call ManageFleetUpdate _
            '        '  ( _
            '        '     EVENT_TYPE_EXT, _
            '        '     BpdArr(i), _
            '        '     strCheckOutStatus, _
            '        '     strBpdBooId, _
            '        '     strBpdRntId, _
            '        '     CStr(sCtyCode), _
            '        '     strReturnError, _
            '        '     strWar _
            '        '  )
            '        ' PASS error back to caller
            '        'oErrlog.WriteErrorToFile("", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(4) & "<< RETURN Value from ManageFleet for BPDId >>" & BpdArr(i))
            '        If Len(strReturnError) <> 0 Then GoTo GarbageCleaner
            '    Next i
            'End If  '----  If Len(Trim(strBookedProductID)) > 0 Then

            'GoTo GarbageCleaner

            ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
            ' LAST (but certainly not least) step in the EVENT=Ext
            ' process is to update the rental history.
            ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
            ' strError = prepareAuroraDalForReuse
            'If Len(strError) <> 0 Then
            ' PASS error back to caller
            'strReturnError = strError
            'Exit Function
            'End If

            ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
            ' REACHING here means there were no errors. So, get
            ' ready for and execute the CheckRentalHistory method
            ' as described in the BTD 352-10.
            ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++

            ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++
            '   NOW our work is done....
            ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++

            'ErrHandler:
            ' sError = Err.Description
            '  sErrorNo = Err.Number

            '  strReturnError = sError

            'GarbageCleaner:
            'oErrlog = Nothing
            ' oAuroraDAL = Nothing
            Return String.Empty
        Catch ex As Exception
            Return ex.Message
        End Try


    End Function



    ' This has to be called with in transaction
    ' since this function does not have transaction
    Public Shared Function CreateHistory(ByVal sBooId As String, _
    ByVal sRntId As String, _
    ByVal sBpdId As String, _
    ByVal rRnhId As String, _
    ByVal sChangeEvent As String, _
    ByVal sDataType As String, _
    ByVal sBphChangedEvent As String, _
    ByVal sUsercode As String, _
    ByVal sAddPrgmName As String) As String



        'Dim objHistoryDAL As Object
        'Dim xsrpIds As Object
        ' Dim sXmlString As String, sErrNo As String, sErrDesc As String

        Try



            DataRepository.CreateHistory(sBooId, sRntId, sBpdId, rRnhId, sChangeEvent, sDataType, sBphChangedEvent, sUsercode, sAddPrgmName)


            'objHistoryDAL.appendParameter("@sBooId", 200, 64, sBooId)
            'objHistoryDAL.appendParameter("@sRntId", 200, 64, sRntId)
            'objHistoryDAL.appendParameter("@sBpdId", 200, 64, sBpdId)
            'objHistoryDAL.appendParameter("@rRnhId", 200, 64, rRnhId)
            'objHistoryDAL.appendParameter("@sChangeEvent", 200, 64, sChangeEvent)
            'objHistoryDAL.appendParameter("@sDataType", 200, 64, sDataType)
            'objHistoryDAL.appendParameter("@sBphChangedEvent", 200, 64, sBphChangedEvent)
            'objHistoryDAL.appendParameter("@sUserCode", 200, 64, sUsercode)
            'objHistoryDAL.appendParameter("@sAddPrgmName", 200, 256, sAddPrgmName)
            'sXmlString = objHistoryDAL.updateRecords("RES_checkRentalHistory", "", True)

            'If (sXmlString <> "SUCCESS") Then
            '    sXmlString = Replace(sXmlString, "/", " - ")
            '    CreateHistory = sXmlString
            '    GoTo ErrHandler
            'End If ' If (sXmlString <> "SUCCESS") Then
            'GoTo Success

            'Success:
            Return (String.empty)
            'If Not (objHistoryDAL Is Nothing) Then objHistoryDAL = Nothing
            'ErrorLog("History From COM END WITH SUCCESS")

        Catch ex As Exception
            ' If Not (objHistoryDAL Is Nothing) Then objHistoryDAL = Nothing
            ' ErrorLog("History From COM END WITH ERROR -> " & CreateHistory)
            Return ("ERROR/" & ex.message)
        End Try

    End Function



    ' convertStringDayPartToInteger()
    ' ===============================
    ' DVASS requires the day part (AM/PM) represented as an
    ' integer data type. This function does a consistent
    ' conversion.
    '
    Public Shared Function convertStringDayPartToInteger _
       ( _
    ByVal strDayPart As String _
       ) _
       As Integer
        Select Case UCase(strDayPart)
            Case "AM"
                convertStringDayPartToInteger = 0
            Case "PM"
                convertStringDayPartToInteger = 1
            Case Else
                ' THIS will cause DVASS to fail
                convertStringDayPartToInteger = -1
        End Select
    End Function



    Public Shared Function getAimsMessage(ByVal sErrNo As String) As String
        Select Case sErrNo
            Case "-2147217843"
                getAimsMessage = sErrNo & " - " & "Not User"
            Case "-2147220463"
                getAimsMessage = sErrNo & " - " & "Other Reason Code 2 Not Found"
            Case "-2147220464"
                getAimsMessage = sErrNo & " - " & "Other Reason Code 1 Not Found"
            Case "-2147220473"
                getAimsMessage = sErrNo & " - " & "Field can not be an empty"
            Case "-2147220474"
                getAimsMessage = sErrNo & " - " & "Record already Completed"
            Case "-2147220475"
                getAimsMessage = sErrNo & " - " & "Vehicle is due for service"
            Case "-2147220476"
                getAimsMessage = sErrNo & " - " & "Exchange Date is Invalid"
            Case "-2147220477"
                getAimsMessage = sErrNo & " - " & "Activity is Invalid"
            Case "-2147220478"
                getAimsMessage = sErrNo & " - " & "Other Repairer is Invalid"
            Case "-2147220479"
                getAimsMessage = sErrNo & " - " & "Reason Description is Invalid"
            Case "-2147220480"
                getAimsMessage = sErrNo & " - " & "Repairer is Invalid"
            Case "-2147220481"
                getAimsMessage = sErrNo & " - " & "End Odometer is Invalid"
            Case "-2147220482"
                getAimsMessage = sErrNo & " - " & "Start Odometer is Invalid"
            Case "-2147220483"
                getAimsMessage = sErrNo & " - " & "End Date is Invalid"
            Case "-2147220484"
                getAimsMessage = sErrNo & " - " & "Start Date is Invalid"
            Case "-2147220489"
                getAimsMessage = sErrNo & " - " & "Rego Is Expired"
            Case "-2147220490"
                getAimsMessage = sErrNo & " - " & "Location In Wrong Region"
            Case "-2147220491"
                getAimsMessage = sErrNo & " - " & "New Asset Not Found"
            Case "-2147220492"
                getAimsMessage = sErrNo & " - " & "Old Asset Not Found"
            Case "-2147220493"
                getAimsMessage = sErrNo & " - " & "Asset Not Found"
            Case "-2147220494"
                getAimsMessage = sErrNo & " - " & "Other Assets Not Found"
            Case "-2147220495"
                getAimsMessage = sErrNo & " - " & "Activity Type NOt Found"
            Case "-2147220496"
                getAimsMessage = sErrNo & " - " & "Activity Not Found"
            Case "-2147220497"
                getAimsMessage = sErrNo & " - " & "Incident Not Found"
            Case "-2147220498"
                getAimsMessage = sErrNo & " - " & "Service Number Not Found"
            Case "-2147220499"
                getAimsMessage = sErrNo & " - " & "Reason Code Not Found"
            Case "-2147220500"
                getAimsMessage = sErrNo & " - " & "Repairer Not Found"
            Case "-2147220501"
                getAimsMessage = sErrNo & " - " & "Location Code Not Found"
            Case "-2147220502"
                getAimsMessage = sErrNo & " - " & "PO Number Not Found"
            Case "-2147220503"
                getAimsMessage = sErrNo & " - " & "Unit Number Not Found"
            Case "-2147352571"
                getAimsMessage = sErrNo & " - " & "Type Mismatch"
            Case "-2147467259"
                getAimsMessage = sErrNo & " - " & "DataBase Not Found"
            Case Else
                getAimsMessage = sErrNo & " - Unknown AIMS Message!."
        End Select
    End Function

#End Region


#Region "REV:manny Added in reference to BookedProductDetails.aspx for Next Button "

    Private Const BookedProduct_EVENT_TYPE_ADD = "Add"
    Private Const BookedProduct_EVENT_TYPE_MOD = "Mod"
    Private Const BookedProduct_EVENT_TYPE_MODBOO = "Mod Boo"
    Private Const BookedProduct_EVENT_TYPE_MODRNT = "Mod Rnt"
    Private Const BookedProduct_EVENT_TYPE_CKO = "Cko"
    Private Const BookedProduct_EVENT_TYPE_CKI = "Cki"
    Private Const BookedProduct_EVENT_TYPE_EXT = "Ext"
    Private Const BookedProduct_EVENT_TYPE_EXCH = "Exch"
    Private Const BookedProduct_BOOKED_PRODUCT_TYPECODE_XFRM = "XFRM"
    Private Const BookedProduct_BOOKED_PRODUCT_TYPECODE_XTO = "XTO"
    Private Const BookedProduct_BOOKED_PRODUCT_TYPECODE_OTHER = ""
    Private Const BookedProduct_RENTAL_HISTORY_DATATYPE_BPD = "Bpd"
    Private Const BookedProduct_DVASS_PRIORITY_ONE = 1
    Private Const BookedProduct_DVASS_VISIBILITY_TEN = 10
    Private Const BookedProduct_DVASS_FORCEFLAG_OFF = 0
    Private Const BookedProduct_DVASS_FORCEFLAG_ON = 1
    Private Const BookedProduct_AIMS_ACTIVITY_TYPE_RENTAL = "Rental"
    Private Const XMLError_ManageBookedProduct_Add As String = "<Error><ErrStatus>True</ErrStatus>" & _
                   "<ErrNumber>GEN099</ErrNumber>" & _
                   "<ErrType>General</ErrType>" & _
                   "<ErrDescription>GEN099/Rental ID and Saleable Product ID are mandatory.</ErrDescription></Error>"

    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                             ByVal sErrNumber As String, _
                                             ByVal sErrType As String, _
                                             ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function CheckManageBookedProductParameters(ByVal ParamArray param As String()) As Boolean
        Dim paramstring As String
        For Each paramstring In param
            paramstring = paramstring.Trim
            If (paramstring.Length = 0) Then
                Return False
            End If
            If (paramstring Is Nothing) Then
                Return False
            End If
            If (paramstring = "") Then
                Return False
            End If
            If (paramstring Is String.Empty) Then
                Return False
            End If
        Next
        Return True
    End Function

    Public Shared Function ManageBookedProduct_Add _
       ( _
            ByVal strRentalID As String, _
            ByVal strSaleableProductID As String, _
            ByVal booApplyOriginalRates As Boolean, _
            ByVal dteCkoWhen As String, _
            ByVal dteCkiWhen As String, _
            ByVal strCkoLocCode As String, _
            ByVal strCkiLocCode As String, _
            ByVal dblRate As String, _
            ByVal lngQuantity As Long, _
            ByVal strPrrIdList As String, _
            ByVal strPrrQtyList As String, _
            ByVal strPrrRateList As String, _
            ByVal strPtdId As String, _
            ByVal strCodTypeId As String, _
            ByVal strUnitNumber As String, _
            ByVal strSerialCode As String, _
            ByVal strBpdIdParent As String, _
            ByVal dteChargeFrom As String, _
            ByVal dteChargeTo As String, _
            ByVal lngOdometerFrom As String, _
            ByVal lngOdometerTo As String, _
            ByRef strRentalHistoryID As String, _
            ByVal strUserId As String, _
            ByVal strProgramName As String, _
            ByRef sRetBpdId As String _
       ) As String


        ManageBookedProduct_Add = String.Empty




        Dim sError As String = Nothing
        Dim strHistError As String = Nothing
        Dim sErrorNo As String = Nothing
        Dim strBpdIdListNew As String = Nothing
        Dim strBpdId As String = Nothing
        Dim varBpdIdArray As Object = Nothing
        Dim lngLBound As Long = Nothing
        Dim lngIndex As Long = Nothing
        Dim lngUBound As Long = Nothing
        Dim sRetMsg As String = Nothing
        Dim strError As String = Nothing
        Dim retError As String = Nothing

        If CheckManageBookedProductParameters(strRentalID, strSaleableProductID) = False Then
            Return XMLError_ManageBookedProduct_Add
        End If

        Try



            strError = ManageBookedProduct_Validate _
                        ( _
                           "Add", _
                           strRentalID, _
                           strSaleableProductID, _
                           "", _
                           dteCkoWhen, _
                           dteCkiWhen, _
                           Nothing, _
                           Nothing, _
                           strCkoLocCode, _
                           strCkiLocCode, _
                           Nothing, _
                           Nothing, retError _
                        )

            If (strError.Length) > 0 Then
                Return strError
            End If


            If Execute_RES_ManageBookedProductAddNonVehicleRequest( _
                        Trim(strRentalID), _
                        Trim(strSaleableProductID), _
                        booApplyOriginalRates, _
                        dteCkoWhen, _
                        dteCkiWhen, _
                        Trim(strCkoLocCode), _
                        Trim(strCkiLocCode), _
                        dblRate, _
                        lngQuantity, _
                        strPrrIdList, _
                        strPrrQtyList, _
                        strPrrRateList, _
                        strPtdId, _
                        dteChargeFrom, _
                        dteChargeTo, _
                        lngOdometerFrom, _
                        lngOdometerTo, _
                        strUserId, _
                        strProgramName, _
                        sRetBpdId, _
                        strBpdIdListNew, _
                        varBpdIdArray, _
                        sRetMsg, strError) = False Then


                Return strError

            End If

            'If Execute_RES_ssTransToFinance( _
            '        strRentalID, _
            '        strSaleableProductID, strError) = False Then
            '    Return strError
            'End If


        Catch ex As Exception
            strError = GetErrorTextFromResource(True, "GEN023", "Application", "")
            Return "<Root>" & strError & "<Data></Data></Root>"
        End Try

        'strHistError = CreateHistory(String.Empty, String.Empty, sRetBpdId, strRentalHistoryID, "", "BPD", "ADD", strUserId, strProgramName & "COM, _ADD")
        strRentalHistoryID = DataRepository.CreateHistoryPhoenix(String.Empty, String.Empty, sRetBpdId, strRentalHistoryID, "", "BPD", "ADD", strUserId, strProgramName & "COM, _ADD")

        Return strError

    End Function

    Private Shared Function Execute_RES_ssTransToFinance(ByVal strRentalID As String, ByVal strSaleableProductID As String, ByRef strerror As String) As Boolean



        '@sBooId   VARCHAR  (64) = NULL ,  
        '@sRntId   VARCHAR  (64) = NULL ,  
        '@sSapId   VARCHAR  (64) = NULL ,  
        '@sBpdId   VARCHAR  (64) = NULL  
        'Call .appendParameterIn("@sBooId", 200, 64, "")
        'Call .appendParameterIn("@sRntId", 200, 64, strRentalID)
        'Call .appendParameterIn("@sSapId", 200, 64, strSaleableProductID)
        'Call .appendParameterIn("@sBpdId", 200, 64, "")

        strerror = Aurora.Common.Data.ExecuteScalarSP("RES_ssTransToFinance", String.Empty, strRentalID, strSaleableProductID, String.Empty)


        If strerror IsNot Nothing Then

            If strerror.Equals("SUCCESS") = True Then
                strerror = String.Empty
            Else
                Return False
            End If

        End If

        Return True
    End Function

    Private Shared Function ManageBookedProduct_Validate _
            ( _
            ByVal strEvent As String, _
            ByVal strRentalID As String, _
            ByVal strSaleableProductID As String, _
            ByVal strBookedProductID As String, _
            ByVal dteCkoWhen As String, _
            ByVal dteCkiWhen As String, _
            ByVal dteCkoWhenPrevious As String, _
            ByVal dteCkiWhenPrevious As String, _
            ByVal strCkoLocCode As String, _
            ByVal strCkiLocCode As String, _
            ByVal strCkoLocCodePrevious As String, _
            ByVal strCkiLocCodePrevious As String, _
            ByRef strerror As String) As String


        Try



            If (strRentalID.Length > 0 AndAlso strSaleableProductID.Length > 0) Then


                ' @psRntId   VARCHAR(64),  
                ' @psSapId   VARCHAR(64),  
                ' @pdPrevCkoWhen  DATETIME =NULL,  
                ' @pdCkoWhen   DATETIME =NULL,  

                ' @pdPrevCkiWhen  DATETIME =NULL,  
                ' @pdCkiWhen   DATETIME =NULL,  
                ' @psPrevCkoLocCode VARCHAR(12) =NULL,  
                ' @psCkoLocCode  VARCHAR(12) =NULL,  

                ' @psPrevCkiLocCode VARCHAR(12) =NULL,  
                ' @psCkiLocCode  VARCHAR(12) =NULL,   
                ' @pdPrevChargeFrom DATETIME =NULL,  
                ' @pdChargeFrom  DATETIME =NULL,  

                ' @pdPrevChargeTo  DATETIME =NULL,  
                ' @pdChargeTo   DATETIME =NULL,  
                ' @psOridAlwaysRoles VARCHAR(500) OUTPUT,  
                ' @psOridAlwaysErrors VARCHAR(500) OUTPUT  


                ''Call .appendParameterIn("@psRntId", 200, 64, strRentalID)
                ''Call .appendParameterIn("@psSapId", 200, 64, strSaleableProductID)
                ''Call .appendParameterIn("@pdPrevCkoWhen", 7, 8, Nothing) 'date
                ''Call .appendParameterIn("@pdCkoWhen", 7, 8, dteCkoWhen)        'date

                ''Call .appendParameterIn("@pdPrevCkiWhen", 7, 8, dteCkiWhenPrevious) ''date
                ''Call .appendParameterIn("@pdCkiWhen", 7, 8, dteCkiWhen) 'date
                ''Call .appendParameterIn("@psPrevCkoLocCode", 200, 64, strCkoLocCodePrevious)
                ''Call .appendParameterIn("@psCkoLocCode", 200, 64, strCkoLocCode)


                ''Call .appendParameterIn("@psPrevCkiLocCode", 200, 64, strCkiLocCodePrevious)
                ''Call .appendParameterIn("@psCkiLocCode", 200, 64, strCkiLocCode)
                ''Call .appendParameterIn("@pdChargeFrom", 7, 8, Nothing) ''date
                ''Call .appendParameterIn("@pdPrevChargeTo", 7, 8, Nothing) ''date

                ''Call .appendParameterIn("@pdChargeTo", 7, 8, Nothing) ''date
                ''Call .appendParameterOut("@psOridAlwaysRoles", 200, 500, strError, True)
                ''Call .appendParameterOut("@psOridAlwaysErrors", 200, 500, strError, True)


                Dim params(15) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("psRntId", DbType.String, strRentalID, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("psSapId", DbType.String, strSaleableProductID, Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("pdPrevCkoWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
                params(3) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.String, dteCkoWhen, Parameter.ParameterType.AddInParameter)

                params(4) = New Aurora.Common.Data.Parameter("pdPrevCkiWhen", DbType.String, dteCkiWhenPrevious, Parameter.ParameterType.AddInParameter)
                params(5) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.String, dteCkiWhen, Parameter.ParameterType.AddInParameter)
                params(6) = New Aurora.Common.Data.Parameter("psPrevCkoLocCode", DbType.String, strCkoLocCodePrevious, Parameter.ParameterType.AddInParameter)
                params(7) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, strCkoLocCode, Parameter.ParameterType.AddInParameter)

                params(8) = New Aurora.Common.Data.Parameter("psPrevCkiLocCode", DbType.String, strCkiLocCodePrevious, Parameter.ParameterType.AddInParameter)
                params(9) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, strCkiLocCode, Parameter.ParameterType.AddInParameter)
                params(10) = New Aurora.Common.Data.Parameter("pdPrevChargeFrom", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
                params(11) = New Aurora.Common.Data.Parameter("pdChargeFrom", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)

                params(12) = New Aurora.Common.Data.Parameter("pdPrevChargeTo", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
                params(13) = New Aurora.Common.Data.Parameter("pdChargeTo", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
                params(14) = New Aurora.Common.Data.Parameter("psOridAlwaysRoles", DbType.String, 500, Parameter.ParameterType.AddOutParameter)
                params(15) = New Aurora.Common.Data.Parameter("psOridAlwaysErrors", DbType.String, 500, Parameter.ParameterType.AddOutParameter)
                strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_CheckCkoCkiWhen", params)

                If strerror IsNot Nothing Then
                    If String.IsNullOrEmpty(strerror) = False Then
                        If strerror.ToUpper.Equals("SUCCESS") = True Then
                            strerror = String.Empty
                        End If
                    End If
                End If



                If strerror.Length = 0 Then

                    Dim retOut As String = params(15).Value.ToString
                    If retOut.Length = 0 Then
                        strerror = String.Empty
                    End If
                End If




            ElseIf (strBookedProductID.Length > 0) Then


                '@psBpdId  VARCHAR(64),  
                '@pdCkoWhen  DATETIME,  
                '@pdCkiWhen  DATETIME,  
                '@psCkoLocCode  VARCHAR(64),  

                '@psCkiLocCode  VARCHAR(64),  
                '@pdCkoWhenPrevious DATETIME,  
                '@pdCkiWhenPrevious DATETIME,  
                '@psCkoLocCodePrevious VARCHAR(64) ,  

                '@psCkiLocCodePrevious VARCHAR(64),  
                '@pReturnError  VARCHAR(500) OUTPUT  
                'Call .appendParameterIn("@psBpdId", 200, 64, strBookedProductID)
                'Call .appendParameterIn("@pdCkoWhen", 7, 8, dteCkoWhen)
                'Call .appendParameterIn("@pdCkiWhen", 7, 8, dteCkiWhen)
                'Call .appendParameterIn("@psCkoLocCode", 200, 64, strCkoLocCode)

                'Call .appendParameterIn("@psCkiLocCode", 200, 64, strCkiLocCode)
                'Call .appendParameterIn("@pdCkoWhenPrevious", 7, 8, dteCkoWhenPrevious)
                'Call .appendParameterIn("@pdCkiWhenPrevious", 7, 8, dteCkiWhenPrevious)
                'Call .appendParameterIn("@psCkoLocCodePrevious", 200, 64, strCkoLocCodePrevious)

                'Call .appendParameterIn("@psCkiLocCodePrevious", 200, 64, strCkiLocCodePrevious)

                'Call .appendParameterOut("@pReturnError", 200, 2000, strerror, True)

                Dim params(9) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, strBookedProductID, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.String, dteCkoWhen, Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.String, dteCkiWhen, Parameter.ParameterType.AddInParameter)
                params(3) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, strCkoLocCode, Parameter.ParameterType.AddInParameter)

                params(4) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, strCkiLocCode, Parameter.ParameterType.AddInParameter)
                params(5) = New Aurora.Common.Data.Parameter("pdCkoWhenPrevious", DbType.String, dteCkoWhenPrevious, Parameter.ParameterType.AddInParameter)
                params(6) = New Aurora.Common.Data.Parameter("pdCkiWhenPrevious", DbType.String, dteCkiWhenPrevious, Parameter.ParameterType.AddInParameter)
                params(7) = New Aurora.Common.Data.Parameter("psCkoLocCodePrevious", DbType.String, strCkoLocCodePrevious, Parameter.ParameterType.AddInParameter)

                params(8) = New Aurora.Common.Data.Parameter("psCkiLocCodePrevious", DbType.String, strCkiLocCodePrevious, Parameter.ParameterType.AddInParameter)
                params(9) = New Aurora.Common.Data.Parameter("pReturnError", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
                strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_CheckCkoCkiWhenForBpd", params)




                If (strerror.Length) = 0 Then
                    ''strerror = params("pReturnError").Value
                    strerror = params(9).Value
                    If String.IsNullOrEmpty(strerror) Then
                        strerror = String.Empty
                    End If


                End If



            Else


                strerror = "VAL001 - The ManageBookedProduct.Validate() method expects either (rantal id, saleable product id) or booked product id. Validation failed."

            End If



        Catch ex As Exception

            ManageBookedProduct_Validate = ex.Message

        End Try

        Return strerror

    End Function

    Private Shared Function Execute_RES_ManageBookedProductAddNonVehicleRequest( _
          ByVal strRentalID As String, _
          ByVal strSaleableProductID As String, _
          ByVal booApplyOriginalRates As Boolean, _
          ByVal dteCkoWhen As String, _
          ByVal dteCkiWhen As String, _
          ByVal strCkoLocCode As String, _
          ByVal strCkiLocCode As String, _
          ByVal dblRate As String, _
          ByVal lngQuantity As Long, _
          ByVal strPrrIdList As String, _
          ByVal strPrrQtyList As String, _
          ByVal strPrrRateList As String, _
          ByVal strPtdId As String, _
          ByVal dteChargeFrom As String, _
          ByVal dteChargeTo As String, _
          ByVal lngOdometerFrom As String, _
          ByVal lngOdometerTo As String, _
          ByVal strUserId As String, _
          ByVal strProgramName As String, _
          ByRef sRetBpdId As String, _
          ByRef strBpdIdListNew As String, _
          ByRef varBpdIdArray() As String, _
          ByRef sRetMsg As String, ByRef strerror As String) As Boolean



        Dim params(29) As Aurora.Common.Data.Parameter


        params(0) = New Aurora.Common.Data.Parameter("sRntID", DbType.String, strRentalID, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sSapId", DbType.String, strSaleableProductID, Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("bApplyOriginalRates", DbType.Boolean, IIf(booApplyOriginalRates = True, 1, 0), Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("dCkoWhen", DbType.String, dteCkoWhen, Parameter.ParameterType.AddInParameter)

        params(4) = New Aurora.Common.Data.Parameter("sCkoLocCode", DbType.String, strCkoLocCode, Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("dCkiWhen", DbType.String, dteCkiWhen, Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sCkiLocCode", DbType.String, strCkiLocCode, Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("cRate", DbType.Currency, CDec(dblRate), Parameter.ParameterType.AddInParameter)

        params(8) = New Aurora.Common.Data.Parameter("iQuantity", DbType.Int16, CInt(lngQuantity), Parameter.ParameterType.AddInParameter)
        params(9) = New Aurora.Common.Data.Parameter("sPrrIdList", DbType.String, strPrrIdList, Parameter.ParameterType.AddInParameter)
        params(10) = New Aurora.Common.Data.Parameter("sPrrQtyList", DbType.String, strPrrQtyList, Parameter.ParameterType.AddInParameter)
        params(11) = New Aurora.Common.Data.Parameter("sPrrRateList", DbType.String, strPrrRateList, Parameter.ParameterType.AddInParameter)

        params(12) = New Aurora.Common.Data.Parameter("sPtdId", DbType.String, strPtdId, Parameter.ParameterType.AddInParameter)
        params(13) = New Aurora.Common.Data.Parameter("sCodTypId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
        params(14) = New Aurora.Common.Data.Parameter("sBpdIDParent", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)

        If dteChargeFrom Is Nothing Then
            params(15) = New Aurora.Common.Data.Parameter("dChargeFrom", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
        Else
            If dteChargeFrom.Length > 0 Then

                params(15) = New Aurora.Common.Data.Parameter("dChargeFrom", DbType.DateTime, dteChargeFrom, Parameter.ParameterType.AddInParameter)
            Else
                params(15) = New Aurora.Common.Data.Parameter("dChargeFrom", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If
        End If

        If dteChargeTo Is Nothing Then
            params(16) = New Aurora.Common.Data.Parameter("dChargeTo", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
        Else
            If dteChargeTo.Length > 0 Then
                params(16) = New Aurora.Common.Data.Parameter("dChargeTo", DbType.DateTime, dteChargeTo, Parameter.ParameterType.AddInParameter)
            Else
                params(16) = New Aurora.Common.Data.Parameter("dChargeTo", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If
        End If



        params(17) = New Aurora.Common.Data.Parameter("psUnitNo", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
        params(18) = New Aurora.Common.Data.Parameter("psBpdRego", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
        params(19) = New Aurora.Common.Data.Parameter("iFromOdo", DbType.Currency, DBNull.Value, Parameter.ParameterType.AddInParameter)


        If lngOdometerTo Is Nothing Then
            params(20) = New Aurora.Common.Data.Parameter("iToOdo", DbType.Double, DBNull.Value, Parameter.ParameterType.AddInParameter)
        Else
            If lngOdometerTo.Length > 0 Then
                params(20) = New Aurora.Common.Data.Parameter("iToOdo", DbType.Double, dteChargeTo, Parameter.ParameterType.AddInParameter)
            Else
                params(20) = New Aurora.Common.Data.Parameter("iToOdo", DbType.Double, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If
        End If


     
        params(21) = New Aurora.Common.Data.Parameter("psExtRef", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
        params(22) = New Aurora.Common.Data.Parameter("bIsCusCharge", DbType.Boolean, DBNull.Value, Parameter.ParameterType.AddInParameter)
        params(23) = New Aurora.Common.Data.Parameter("sUserId", DbType.String, strUserId, Parameter.ParameterType.AddInParameter)
        params(24) = New Aurora.Common.Data.Parameter("sProgramName", DbType.String, strProgramName, Parameter.ParameterType.AddInParameter)
        params(25) = New Aurora.Common.Data.Parameter("bCreateHistory", DbType.Boolean, 0, Parameter.ParameterType.AddInParameter)
        params(26) = New Aurora.Common.Data.Parameter("psEvent", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)


        params(27) = New Aurora.Common.Data.Parameter("sNewBpdIdsList", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
        params(28) = New Aurora.Common.Data.Parameter("ReturnError", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
        params(29) = New Aurora.Common.Data.Parameter("ReturnMessage", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
        strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_ManageBookedProductAddNonVehicleRequest", params)

        ' 12.1.8 - Shoel - Fix for issues where there is a message and it gets bypassed
        If Not String.IsNullOrEmpty(CStr(params(28).Value)) Then
            ' there is an error returned, so it takes highest priority
            strerror = CStr(params(28).Value)
            sRetMsg = CStr(params(29).Value)
            Return False
        End If
        ' 12.1.8 - Shoel - Fix for issues where there is a message and it gets bypassed

        If strerror IsNot Nothing Then
            If Not String.IsNullOrEmpty(strerror) Then
                If (strerror.ToUpper.TrimEnd.Equals("SUCCESS")) Then
                    strerror = String.Empty
                Else
                    Return False
                    Throw New Exception(strerror)
                End If
                ' 12.1.8 - Shoel - Fix for issues where there is a message and it gets bypassed
                'ElseIf strerror.Length = 0 Then
                '    strerror = params("ReturnError").Value
                '    If strerror IsNot Nothing Then
                '        Return False
                '    End If
            End If
            ' 12.1.8 - Shoel - Fix for issues where there is a message and it gets bypassed
        End If




        strBpdIdListNew = params(27).Value ''"sNewBpdIdsList

        varBpdIdArray = strBpdIdListNew.Split(",")

        sRetBpdId = varBpdIdArray(0)

        Return True

    End Function

    Private Shared Function CreateBookedProductHistory( _
              ByVal sBooId As String, _
              ByVal sRntId As String, _
              ByVal sBpdId As String, _
              ByVal rRnhId As String, _
              ByVal sChangeEvent As String, _
              ByVal sDataType As String, _
              ByVal sBphChangedEvent As String, _
              ByVal sUsercode As String, _
              ByVal sAddPrgmName As String) As String

        Dim sXmlString As String = Nothing


        Try


            '@sBooId    VARCHAR  (64),  
            '@sRntId    VARCHAR  (64) = NULL,  
            '@sBpdId    VARCHAR  (64) = NULL,  
            '@rRnhId    VARCHAR  (64) =  NULL ,  
            '@sChangeEvent  VARCHAR  (64) = NULL,  -- Valid Values are ADD, MODIFY, CONTRACT, CONFIRMATION OR cANCEL  
            '@sDataType   VARCHAR  (64) = NULL,  -- RPT(RentalPayment), BOO(booking), RNT(Rental),BPD(Bookedproduct)  
            '@sBphChangedEvent VARCHAR  (64) = NULL,  -- Valid Values are ADD OR MODIFY  
            '-- Issue 879: RKS 18-MAY-2006  
            '-- START  
            '@sVehSapId   VARCHAR(64) = NULL,  
            '-- END  
            '@sUserCode   VARCHAR  (64),  
            '@sAddPrgmName  VARCHAR  (256)  


            'objHistoryDAL.appendParameterIn("@sBooId", 200, 64, sBooId)
            'objHistoryDAL.appendParameterIn("@sRntId", 200, 64, sRntId)
            'objHistoryDAL.appendParameterIn("@sBpdId", 200, 64, sBpdId)
            'objHistoryDAL.appendParameterIn("@rRnhId", 200, 64, rRnhId)
            'objHistoryDAL.appendParameterIn("@sChangeEvent", 200, 64, sChangeEvent)
            'objHistoryDAL.appendParameterIn("@sDataType", 200, 64, sDataType)
            'objHistoryDAL.appendParameterIn("@sBphChangedEvent", 200, 64, sBphChangedEvent)
            'objHistoryDAL.appendParameterIn("@sUserCode", 200, 64, sUsercode)
            'objHistoryDAL.appendParameterIn("@sAddPrgmName", 200, 256, sAddPrgmName)

            'sXmlString = objHistoryDAL.UpdateRecords("RES_checkRentalHistory", "", True)

            sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_checkRentalHistory", sBooId, sRntId, sBpdId, rRnhId, sChangeEvent, sDataType, sBphChangedEvent, sUsercode, sAddPrgmName)
            If Not sXmlString Is Nothing Then
                If Not (sXmlString.Equals("SUCCESS")) Then
                    Return sXmlString.Replace("/", " - ")
                End If
            End If
            

        Catch ex As Exception


            Return sXmlString

        End Try

        Return sXmlString

    End Function

    Private Shared Function XMLErrorGeneric(ByVal ErrStatus As String, ByVal Errnumber As String, ByVal Errtype As String, ByVal ErrDescription As String, Optional ByVal Errquery As String = "", Optional ByVal sqlErr As Integer = 0) As String

        Dim xmlError As String = String.Empty

        If sqlErr = 0 Then

            xmlError = "<Error><ErrStatus>objA</ErrStatus><ErrNumber>objB</ErrNumber><ErrType>objC</ErrType><ErrDescription>objD</ErrDescription></Error>"

        Else


            xmlError = "<Error><ErrStatus>objA</ErrStatus><ErrNumber>objB</ErrNumber><ErrType>objC</ErrType><ErrDescription>objD</ErrDescription><ErrSqlProcedure>objE</ErrSqlProcedure></Error>"




        End If


        xmlError = xmlError.Replace("objA", ErrStatus)
        xmlError = xmlError.Replace("objB", Errnumber)
        xmlError = xmlError.Replace("objC", Errtype)
        xmlError = xmlError.Replace("objD", ErrDescription)
        xmlError = xmlError.Replace("objE", Errquery)

        Return xmlError
    End Function

    ''rev:mia 19jan2015-addition of supervisor override password and slot validation by adding SlotId, SlotDescription and RentalId
    ''                  - today in PH history, Pope Francis finished his 4 days visit.He's going back to Rome
    Public Shared Function ManageBookedProduct_Mod _
       ( _
            ByVal strBookedProductID As String, _
            ByVal booIsCusCharge As String, _
            ByVal strSaleableProductID As String, _
            ByVal strAgentID As String, _
            ByVal dteCkoWhen As String, _
            ByVal dteCkiWhen As String, _
            ByVal strCkoLocCode As String, _
            ByVal strCkiLocCode As String, _
            ByVal lngFrom As Integer, _
            ByVal lngTo As Integer, _
            ByVal dblRate As String, _
            ByVal lngQuantity As Integer, _
            ByVal strPrrIdList As String, _
            ByVal strPrrQtyList As String, _
            ByVal strPrrRateList As String, _
            ByVal strPtdId As String, _
            ByVal strCodTypeId As String, _
            ByVal strUnitNumber As String, _
            ByVal strSerialCode As String, _
            ByVal strBpdIdParent As String, _
            ByVal dteChargeFrom As String, _
            ByVal dteChargeTo As String, _
            ByVal lngOdometerFrom As Integer, _
            ByVal lngOdometerTo As Integer, _
            ByVal booOverride As String, _
            ByVal sFoc As String, _
            ByVal strStatus As String, _
            ByVal sCtyCode As String, _
            ByVal strReturnBlrIdList As String, _
            ByVal booCallManageBlockingMessage As String, _
            ByVal strBookingRequestIdNew As String, _
            ByRef strRentalHistoryID As String, _
            ByVal strUserId As String, _
            ByVal strProgramName As String, _
            ByRef strReturnError As String, _
            ByRef strReturnMessage As String, _
            ByRef strRetBpdId As String, _
            Optional ByVal bVehicleOverride As String = Nothing, _
            Optional ByVal sOldRntStatus As String = Nothing, _
            Optional ByVal strRegoNumber As String = Nothing, _
            Optional ByVal sEvent As String = Nothing, _
            Optional ByVal sOldCkoWhen As String = Nothing, _
            Optional ByVal sOldCkiWhen As String = Nothing, _
            Optional ByVal sRntStatus As String = Nothing, _
            Optional ByVal sOldCkoLoc As String = Nothing, _
            Optional ByVal sOldCkiLoc As String = Nothing, _
            Optional ByVal sExcludeBpdList As String = Nothing, _
            Optional ByVal sExclLstIn As String = Nothing, _
            Optional ByVal IsFirstFlag As Boolean = True, _
            Optional ByVal sCalledFrom As String = "", _
            Optional SlotId As String = "", _
            Optional SlotDescription As String = "", _
            Optional RentalId As String = "") As String



        ' validations to see if AIMS call should happen
        Dim bSkipAnyAIMSCall As Boolean = False

        If Trim(UCase(sRntStatus)) = "CO" Then
            ' you cannot change the co loc/date of a CO rental
            If sOldCkoWhen <> dteCkoWhen Then
                bSkipAnyAIMSCall = True
            End If
            If sOldCkoLoc <> strCkoLocCode Then
                bSkipAnyAIMSCall = True
            End If
        ElseIf Trim(UCase(sRntStatus)) = "CI" Then
            ' you cannot change the co,ci loc/date of a CI rental
            If sOldCkoWhen <> dteCkoWhen Then
                bSkipAnyAIMSCall = True
            End If
            If sOldCkoLoc <> strCkoLocCode Then
                bSkipAnyAIMSCall = True
            End If
            If sOldCkiWhen <> dteCkiWhen Then
                bSkipAnyAIMSCall = True
            End If
            If sOldCkiLoc <> strCkiLocCode Then
                bSkipAnyAIMSCall = True
            End If
        End If
        ' validations to see if AIMS call should happen



        ManageBookedProduct_Mod = String.Empty

        Dim strBit As String = Nothing
        Dim sNewBpdId As String = Nothing
        Dim booUpdateFleet As Boolean = False
        Dim strCheckOutStatus As String = Nothing
        Dim strBpdBooId As String = Nothing
        Dim strBpdRntId As String = Nothing
        Dim sError As String = Nothing
        Dim strHistError As String = Nothing
        Dim sErrorNo As String = Nothing
        Dim strNewReturnMessage As Object = Nothing
        Dim ArrBpdIdList As Object = Nothing
        Dim bIsFirstFlag As Boolean = IsFirstFlag ''Nothing ''rev:mia sept 25
        Dim blnUpdateFleet As Boolean = Nothing
        Dim strError As String = Nothing


        If CheckManageBookedProductParameters(strBookedProductID) = False Then
            strReturnError = XMLErrorGeneric(True, "GEN099", "General", "GEN099/Booked Product ID is mandatory.")
            Return strReturnError
        End If




        Try
            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting call to Execute_RES_ManageBookedProductChanges")

            If Execute_RES_ManageBookedProductChanges(Trim(strBookedProductID), _
                                                         booIsCusCharge, _
                                                         Trim(strSaleableProductID), _
                                                         Trim(strAgentID), _
                                                         dteCkoWhen, _
                                                         dteCkiWhen, _
                                                         Trim(strCkoLocCode), _
                                                         Trim(strCkiLocCode), _
                                                         lngFrom, _
                                                         lngTo, _
                                                         dblRate, _
                                                         lngQuantity, _
                                                         Trim(strPrrIdList), _
                                                         Trim(strPrrQtyList), _
                                                         Trim(strPrrRateList), _
                                                         Trim(strPtdId), _
                                                         Trim(strUnitNumber), _
                                                         strSerialCode, _
                                                         dteChargeFrom, _
                                                         dteChargeTo, _
                                                         lngOdometerFrom, _
                                                         lngOdometerTo, _
                                                         booOverride, _
                                                         Trim(sFoc), _
                                                         strStatus, _
                                                         strReturnBlrIdList, _
                                                         booCallManageBlockingMessage, _
                                                         strRentalHistoryID, _
                                                         strUserId, _
                                                         strProgramName, _
                                                         strReturnError, _
                                                         strReturnMessage, _
                                                         strBit, _
                                                         sNewBpdId, _
                                                         booUpdateFleet, _
                                                         bIsFirstFlag, _
                                                         bVehicleOverride, _
                                                         strRegoNumber, _
                                                         sEvent, _
                                                         sExcludeBpdList, blnUpdateFleet
                                                         ) = False Then

                Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to Execute_RES_ManageBookedProductChanges. Return = False, " & _
                                                                                "strReturnError=""" & strReturnError & """, " & _
                                                                                "strReturnMessage=""" & strReturnMessage & """")

                Return strReturnError
            Else
                ''rev:mia 19Jan2015 - 
                If (Not String.IsNullOrEmpty(SlotId) And Not String.IsNullOrEmpty(SlotDescription)) Then
                    SaveRentalSlot(RentalId, SlotId, SlotDescription)
                End If
            End If


            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to Execute_RES_ManageBookedProductChanges. Return = True, " & _
                                                                            "strReturnError=""" & strReturnError & """, " & _
                                                                            "strReturnMessage=""" & strReturnMessage & """")

            ''----------------------

            Dim pdBpdCkoWhen As String = String.Empty
            Dim pdBpdCkiWhen As String = String.Empty
            Dim strErrfrom As String = String.Empty
            Dim psBpdCkoLocCode As String = String.Empty
            Dim psBpdCkiLocCode As String = String.Empty

            If String.IsNullOrEmpty(dteCkoWhen) = False AndAlso String.IsNullOrEmpty(sOldCkoWhen) = False Then
                If CDate(dteCkoWhen) <> CDate(sOldCkoWhen) Then
                    pdBpdCkoWhen = sOldCkoWhen
                Else
                    pdBpdCkoWhen = String.Empty
                End If
            Else
                pdBpdCkoWhen = String.Empty
            End If


            If String.IsNullOrEmpty(dteCkiWhen) = False AndAlso String.IsNullOrEmpty(sOldCkiWhen) = False Then
                If CDate(dteCkiWhen) <> CDate(sOldCkiWhen) Then
                    pdBpdCkiWhen = sOldCkiWhen
                Else
                    pdBpdCkiWhen = String.Empty
                End If
            Else
                pdBpdCkiWhen = String.Empty
            End If


            ''--------------------------------------------------
            If (strCkoLocCode.ToUpper.Trim.Equals(sOldCkoLoc.ToUpper.Trim) = False) Then
                psBpdCkoLocCode = sOldCkoLoc
            Else
                psBpdCkoLocCode = String.Empty
            End If
            ''--------------------------------------------------


            If (strCkiLocCode.ToUpper.Trim.Equals(sOldCkiLoc.ToUpper.Trim) = False) Then
                psBpdCkiLocCode = sOldCkiLoc
            Else
                psBpdCkiLocCode = String.Empty
            End If

            ' 19.2.10 - Shoel - Re-writing this part to fix legacy error

            'If Not String.IsNullOrEmpty(dteCkoWhen) _
            '   AndAlso _
            '   Not String.IsNullOrEmpty(sOldCkoWhen) _
            '   AndAlso _
            '   CDate(dteCkoWhen) <> CDate(sOldCkoWhen) _
            'Then
            '    ' change
            '    pdBpdCkoWhen = dteCkoWhen
            'End If


            'If Not String.IsNullOrEmpty(dteCkiWhen) _
            '   AndAlso _
            '   Not String.IsNullOrEmpty(sOldCkiWhen) _
            '   AndAlso _
            '   CDate(dteCkiWhen) <> CDate(sOldCkiWhen) _
            'Then
            '    ' change
            '    pdBpdCkiWhen = dteCkiWhen
            'End If


            'If Not strCkoLocCode.ToUpper.Trim.Equals(sOldCkoLoc.ToUpper.Trim) Then
            '    ' change
            '    psBpdCkoLocCode = strCkoLocCode
            'End If


            'If Not strCkiLocCode.ToUpper.Trim.Equals(sOldCkiLoc.ToUpper.Trim) Then
            '    ' change
            '    psBpdCkiLocCode = strCkiLocCode
            'End If

            ' 19.2.10 - Shoel - Re-writing this part to fix legacy error

            ''rev:mia oct.3 ---commented this part of code
            strBit = blnUpdateFleet


            If (sNewBpdId.Length) <> 0 Then
                If sNewBpdId.IndexOf(",") > 0 Then
                    ArrBpdIdList = sNewBpdId.Split(",")
                    strRetBpdId = ArrBpdIdList(0)
                Else
                    strRetBpdId = sNewBpdId
                End If
            End If


            If ((sRntStatus.ToUpper.Equals("CO")) Or (sRntStatus.ToUpper.Equals("CI"))) _
            And (( _
              (Not IsNothing(sOldCkoWhen) And Not dteCkoWhen.Equals(sOldCkoWhen)) _
              Or (Not IsNothing(sOldCkoLoc) And Not sOldCkoLoc.Equals(strCkoLocCode)) _
            ) _
        Or ( _
             (Not IsNothing(dteCkiWhen) And Not dteCkiWhen.Equals(sOldCkiWhen)) _
              Or (Not IsNothing(sOldCkiLoc) And Not sOldCkiLoc.Equals(strCkiLocCode)) _
            ) _
            ) Then
                If blnUpdateFleet Then

                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting call to Execute_RES_UpdateAimsAfterEvent")

                    If Execute_RES_UpdateAimsAfterEvent(strBookedProductID, _
                                                  strRetBpdId, _
                                                  pdBpdCkoWhen, _
                                                  pdBpdCkiWhen, _
                                                  psBpdCkoLocCode, _
                                                  psBpdCkiLocCode, _
                                                  strUserId, _
                                                  strErrfrom, _
                                                  strReturnError _
                                                  ) = False Then

                        Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to Execute_RES_UpdateAimsAfterEvent. Result = False, strErrfrom=""" & strErrfrom & """, strReturnError=""" & strReturnError & """")

                        Return strReturnError
                    End If
                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to Execute_RES_UpdateAimsAfterEvent. Result = True, strErrfrom=""" & strErrfrom & """, strReturnError=""" & strReturnError & """")

                End If
            End If

            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "strReturnError=""" & strReturnError & """, strBit=""" & strBit & """")

            If (strReturnError.Length) = 0 Then
                booUpdateFleet = IIf((String.IsNullOrEmpty(strBit)), 0, strBit)
                If (CBool(booUpdateFleet) = True) AndAlso Not bSkipAnyAIMSCall Then

                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting call to ManageFleetUpdate")

                    Call ManageFleetUpdate( _
                             BookedProduct_EVENT_TYPE_MOD, _
                             strRetBpdId, _
                             strCheckOutStatus, _
                             strBpdBooId, strBpdRntId, _
                             sCtyCode, _
                             strReturnError, _
                             strNewReturnMessage, _
                             bVehicleOverride, _
                             sOldRntStatus _
                             , sCalledFrom _
                          )

                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to ManageFleetUpdate. strReturnError=""" & strReturnError & """")

                End If
            End If

            If (strReturnError IsNot Nothing) Then
                If strReturnError.Length <> 0 Then
                    Return strReturnError
                End If
            End If
            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting RES_UpdateBookingRentalStatus")
            strError = Aurora.Common.Data.ExecuteScalarSP("RES_UpdateBookingRentalStatus", strRetBpdId)
            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished RES_UpdateBookingRentalStatus. strError=""" & strError & """")

            If Not strError Is Nothing Then
                If strError.Equals("SUCCESS") Then strError = String.Empty
                If (strError.Length) <> 0 Then
                    Return strError
                End If
            End If


            If (strRetBpdId <> Nothing) Then
                If strRetBpdId.Length <> 0 Then
                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting CreateHistory")
                    strHistError = CreateHistory(String.Empty, String.Empty, strRetBpdId, strRentalHistoryID, String.Empty, "BPD", "MODIFY", strUserId, strProgramName & " COM, _MOD")
                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished CreateHistory. strHistError=""" & strHistError & """")
                End If
            End If

            '@sBooId			VARCHAR	 (64)	=	NULL ,
            '@sRntId			VARCHAR	 (64)	=	NULL ,
            '@sSapId			VARCHAR	 (64)	=	NULL ,
            '@sBpdId			VARCHAR	 (64)	=	NULL

            'strError = Aurora.Common.Data.ExecuteScalarSP("RES_ssTransToFinance", String.Empty, String.Empty, strSaleableProductID, strRetBpdId)

            'If strError IsNot Nothing Then
            '    If strError.Equals("SUCCESS") Then strError = String.Empty
            '    If (strError.Length) <> 0 Then
            '        strReturnError = strError
            '        Return strError
            '    End If
            'End If


            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting RES_UpdateRntFollowUpDate")
            strError = Aurora.Common.Data.ExecuteScalarSP("RES_UpdateRntFollowUpDate", "", strRetBpdId)
            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished RES_UpdateRntFollowUpDate. strError=""" & strError & """, strRetBpdId=""" & strRetBpdId & """")

            If strError IsNot Nothing Then
                If strError.Equals("SUCCESS") Then strError = String.Empty
                If (strError.Length) <> 0 Then
                    Return strError
                End If
            End If


            Dim blnstrCheckOutStatus As Boolean = False
            If Not strCheckOutStatus Is Nothing Then
                Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "TESTING CONDITION -> blnstrCheckOutStatus = strCheckOutStatus.ToLower.Equals(""before check-out"")" & vbNewLine & "strCheckOutStatus=""" & strCheckOutStatus & """")
                blnstrCheckOutStatus = strCheckOutStatus.ToLower.Equals("before check-out")
            End If

            If strError IsNot Nothing Then
                If (strError.Length) <> 0 _
                           And (blnstrCheckOutStatus) Then

                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Starting call to Execute_RES_ManageBpdModFleetError")
                    If Execute_RES_ManageBpdModFleetError( _
                                 strBookedProductID, _
                                 strSaleableProductID, _
                                 dteCkoWhen, _
                                 dteCkiWhen, _
                                 strCkoLocCode, _
                                 strCkiLocCode, _
                                 lngFrom, _
                                 lngTo, _
                                 dblRate, _
                                 lngQuantity, _
                                 strPrrIdList, _
                                 strPrrQtyList, _
                                 strPrrRateList, _
                                 strPtdId, _
                                 strUnitNumber, _
                                 strSerialCode, _
                                 strReturnBlrIdList, _
                                 strUserId, _
                                 strProgramName, _
                                 strReturnError, _
                                 strReturnMessage, _
                                 strBookingRequestIdNew _
                                 ) = False Then

                        Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to Execute_RES_ManageBpdModFleetError. " & _
                                                             "Return = False, strReturnError=""" & strReturnError & """," & _
                                                             "strReturnMessage = """ & strReturnMessage & """")

                        Return strReturnError

                    End If
                    Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "Finished call to Execute_RES_ManageBpdModFleetError. " & _
                                                                                "Return = True, strReturnError=""" & strReturnError & """," & _
                                                                                "strReturnMessage = """ & strReturnMessage & """")


                    ''----------------------
                End If ' errors and not yet checked-out
            End If





        Catch ex As Exception

            Aurora.Common.Logging.LogException("ManageBookedProduct_Mod", ex)
            Aurora.Common.Logging.LogInformation("ManageBookedProduct_Mod", "EXCEPTION STACK TRACE = " & ex.StackTrace)

            sError = ex.Message

            Return String.Concat("<Root>", GetErrorTextFromResource(True, sErrorNo, "System", sError), "</Root>")


        End Try

        Return strReturnError

    End Function


    Private Shared Function Execute_RES_ManageBookedProductChanges( _
          ByVal strBookedProductID As String, _
          ByVal booIsCusCharge As String, _
          ByVal strSaleableProductID As String, _
          ByVal strAgentID As String, _
          ByVal dteCkoWhen As String, _
          ByVal dteCkiWhen As String, _
          ByVal strCkoLocCode As String, _
          ByVal strCkiLocCode As String, _
          ByVal lngFrom As Integer, _
          ByVal lngTo As Integer, _
          ByVal dblRate As String, _
          ByVal lngQuantity As Integer, _
          ByVal strPrrIdList As String, _
          ByVal strPrrQtyList As String, _
          ByVal strPrrRateList As String, _
          ByVal strPtdId As String, _
          ByVal strUnitNumber As String, _
          ByVal strSerialCode As String, _
          ByVal dteChargeFrom As String, _
          ByVal dteChargeTo As String, _
          ByVal lngOdometerFrom As Integer, _
          ByVal lngOdometerTo As Integer, _
          ByVal booOverride As String, _
          ByVal sFoc As String, _
          ByVal strStatus As String, _
          ByVal strReturnBlrIdList As String, _
          ByVal booCallManageBlockingMessage As String, _
          ByRef strRentalHistoryID As String, _
          ByVal strUserId As String, _
          ByVal strProgramName As String, _
          ByRef strReturnError As String, _
          ByRef strReturnMessage As String, _
          ByRef strBit As String, _
          ByRef sNewBpdId As String, _
          ByRef booUpdateFleet As String, _
          ByRef bIsFirstFlag As String, _
          ByVal bVehicleOverride As String, _
          ByVal strRegoNumber As String, _
          ByVal sEvent As String, _
          ByVal sExcludeBpdList As String, _
          ByRef blnupdatefleet As Boolean
          ) As Boolean
        Dim strerror As String = Nothing

        If bVehicleOverride = Nothing Then
            bIsFirstFlag = True
            'Else ''''rev:mia sept25
            '    ''  bIsFirstFlag = IIf((bVehicleOverride = True), False, True)
        End If

        Dim params(44) As Aurora.Common.Data.Parameter

        Try


            '@psBpdID						VARCHAR(64),
            If Not (strBookedProductID = Nothing) Or (strBookedProductID.Length > 0) Then
                params(0) = New Aurora.Common.Data.Parameter("psBpdID", DbType.String, strBookedProductID, Parameter.ParameterType.AddInParameter)
            Else
                params(0) = New Aurora.Common.Data.Parameter("psBpdID", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pbIsCusCharge					BIT ,
            If Not (booIsCusCharge = Nothing) Then
                params(1) = New Aurora.Common.Data.Parameter("pbIsCusCharge", DbType.Boolean, Convert.ToBoolean(CInt(booIsCusCharge)), Parameter.ParameterType.AddInParameter)
            Else
                params(1) = New Aurora.Common.Data.Parameter("pbIsCusCharge", DbType.Boolean, False, Parameter.ParameterType.AddInParameter)
            End If

            ''@psCancelScreen - test
            If Not IsNothing(String.Empty) Then
                params(2) = New Aurora.Common.Data.Parameter("psCancelScreen", DbType.String, "test", Parameter.ParameterType.AddInParameter)
            End If

            '@psSapId						VARCHAR(64)		= NULL ,
            If Not (strSaleableProductID = Nothing) Then
                params(3) = New Aurora.Common.Data.Parameter("psSapId", DbType.String, strSaleableProductID, Parameter.ParameterType.AddInParameter)
            Else
                params(3) = New Aurora.Common.Data.Parameter("psSapId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psAgnId						VARCHAR(64)		= NULL ,
            If Not (strAgentID = Nothing) Then
                params(4) = New Aurora.Common.Data.Parameter("psAgnId", DbType.String, strAgentID, Parameter.ParameterType.AddInParameter)
            Else
                params(4) = New Aurora.Common.Data.Parameter("psAgnId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdCkoWhen						DATETIME		= NULL ,
            If Not (dteCkoWhen.Length = Nothing) Then
                params(5) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.DateTime, dteCkoWhen, Parameter.ParameterType.AddInParameter)
            Else
                params(5) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psCkoLocCode					VARCHAR(64)		= NULL ,
            If Not (strCkoLocCode = Nothing) Then
                params(6) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, strCkoLocCode, Parameter.ParameterType.AddInParameter)
            Else
                params(6) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdCkiWhen						DATETIME		= NULL ,
            If Not (dteCkiWhen = Nothing) Then
                params(7) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.DateTime, dteCkiWhen, Parameter.ParameterType.AddInParameter)
            Else
                params(7) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psCkiLocCode					VARCHAR(64)		= NULL ,
            If Not (strCkiLocCode = Nothing) Then
                params(8) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, strCkiLocCode, Parameter.ParameterType.AddInParameter)
            Else
                params(8) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@piFrom							INTEGER			= NULL ,
            If (lngFrom > 0) Then
                params(9) = New Aurora.Common.Data.Parameter("piFrom", DbType.Int32, lngFrom, Parameter.ParameterType.AddInParameter)
            Else
                params(9) = New Aurora.Common.Data.Parameter("piFrom", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@piTo							INTEGER			= NULL ,
            If (lngTo > 0) Then
                params(10) = New Aurora.Common.Data.Parameter("piTo", DbType.Int32, CInt(lngTo), Parameter.ParameterType.AddInParameter)
            Else
                params(10) = New Aurora.Common.Data.Parameter("piTo", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            ''rev:mia oct.18
            '@pcRate							MONEY			= NULL ,
            If (dblRate > 0) Then
                params(11) = New Aurora.Common.Data.Parameter("pcRate", DbType.Currency, CDbl(dblRate), Parameter.ParameterType.AddInParameter)
            Else
                '
                If (dblRate.Contains("-")) = True Then
                    params(11) = New Aurora.Common.Data.Parameter("pcRate", DbType.Currency, CDbl(dblRate), Parameter.ParameterType.AddInParameter)
                Else
                    params(11) = New Aurora.Common.Data.Parameter("pcRate", DbType.Currency, DBNull.Value, Parameter.ParameterType.AddInParameter)
                End If

            End If


            '@piQuantity						INTEGER			= NULL ,
            If (lngQuantity > 0) Then
                params(12) = New Aurora.Common.Data.Parameter("piQuantity", DbType.Int32, CInt(lngQuantity), Parameter.ParameterType.AddInParameter)
            Else
                params(12) = New Aurora.Common.Data.Parameter("piQuantity", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If


            '@psPrrIdList					VARCHAR(2000)	= NULL ,
            If Not (strPrrIdList = Nothing) Then
                params(13) = New Aurora.Common.Data.Parameter("psPrrIdList", DbType.String, strPrrIdList, Parameter.ParameterType.AddInParameter)
            Else
                params(13) = New Aurora.Common.Data.Parameter("psPrrIdList", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psPrrQtyList					VARCHAR(2000)	= NULL ,
            If Not (strPrrQtyList = Nothing) Then
                params(14) = New Aurora.Common.Data.Parameter("psPrrQtyList", DbType.String, strPrrQtyList, Parameter.ParameterType.AddInParameter)
            Else
                params(14) = New Aurora.Common.Data.Parameter("psPrrQtyList", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psPrrRateList					VARCHAR(2000)	= NULL ,					-- MTS 09JUN02 : Added
            If Not (strPrrRateList = Nothing) Then
                params(15) = New Aurora.Common.Data.Parameter("psPrrRateList", DbType.String, strPrrRateList, Parameter.ParameterType.AddInParameter)
            Else
                params(15) = New Aurora.Common.Data.Parameter("psPrrRateList", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psPtdId						VARCHAR(64)		= NULL ,
            If Not (strPtdId = Nothing) Then
                params(16) = New Aurora.Common.Data.Parameter("psPtdId", DbType.String, strPtdId, Parameter.ParameterType.AddInParameter)
            Else
                params(16) = New Aurora.Common.Data.Parameter("psPtdId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psUnitNumber					VARCHAR(64)		= NULL ,
            If Not (strUnitNumber = Nothing) Then
                params(17) = New Aurora.Common.Data.Parameter("psUnitNumber", DbType.String, strUnitNumber, Parameter.ParameterType.AddInParameter)
            Else
                params(17) = New Aurora.Common.Data.Parameter("psUnitNumber", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psRegoNum						VARCHAR(64)		= NULL ,
            If Not (strRegoNumber = Nothing) Then
                params(18) = New Aurora.Common.Data.Parameter("psRegoNum", DbType.String, strRegoNumber, Parameter.ParameterType.AddInParameter)
            Else
                params(18) = New Aurora.Common.Data.Parameter("psRegoNum", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If


            '@psSerialCode					VARCHAR(64)		= NULL ,
            If Not (strSerialCode = Nothing) Then
                params(19) = New Aurora.Common.Data.Parameter("psSerialCode", DbType.String, strSerialCode, Parameter.ParameterType.AddInParameter)
            Else
                params(19) = New Aurora.Common.Data.Parameter("psSerialCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdChargeFromWhen				DATETIME		= NULL ,
            If Not (dteChargeFrom = Nothing) Then
                params(20) = New Aurora.Common.Data.Parameter("pdChargeFromWhen", DbType.DateTime, dteChargeFrom, Parameter.ParameterType.AddInParameter)
            Else
                params(20) = New Aurora.Common.Data.Parameter("pdChargeFromWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdChargeToWhen					DATETIME		= NULL ,
            If Not (dteChargeTo = Nothing) Then
                params(21) = New Aurora.Common.Data.Parameter("pdChargeToWhen", DbType.DateTime, dteChargeTo, Parameter.ParameterType.AddInParameter)
            Else
                params(21) = New Aurora.Common.Data.Parameter("pdChargeToWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@piFromOdo						INTEGER			= NULL ,	

            If (lngOdometerFrom > 0) Then
                params(22) = New Aurora.Common.Data.Parameter("piFromOdo", DbType.Int32, CInt(lngOdometerFrom), Parameter.ParameterType.AddInParameter)
            Else
                params(22) = New Aurora.Common.Data.Parameter("piFromOdo", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psEvent						VARCHAR(2000)	= NULL ,
            params(23) = New Aurora.Common.Data.Parameter("psEvent", DbType.String, sEvent, Parameter.ParameterType.AddInParameter)

            '@pbOverride						BIT				= 0 ,					-- MTS 14JUN02 : Added
            If Not (booOverride = Nothing) Then
                params(24) = New Aurora.Common.Data.Parameter("pbOverride", DbType.Boolean, Convert.ToBoolean(booOverride), Parameter.ParameterType.AddInParameter)
            Else
                params(24) = New Aurora.Common.Data.Parameter("pbOverride", DbType.Boolean, 0, Parameter.ParameterType.AddInParameter)
            End If

            '@psFoc							VARCHAR(1) 		= NULL ,   				-- RS 26/06 : ADDED ,
            If Not (sFoc = Nothing) Then
                params(25) = New Aurora.Common.Data.Parameter("psFoc", DbType.String, sFoc, Parameter.ParameterType.AddInParameter)
            Else
                params(25) = New Aurora.Common.Data.Parameter("psFoc", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If


            '@pbApplyOriginalRates			BIT	 			= NULL ,				-- RKS 14/08
            params(26) = New Aurora.Common.Data.Parameter("pbApplyOriginalRates", DbType.Boolean, DBNull.Value, Parameter.ParameterType.AddInParameter)

            '@psStatus						VARCHAR (64)	= NULL ,					-- RKS 31/07 : Added
            If Not (strStatus = Nothing) Then
                params(27) = New Aurora.Common.Data.Parameter("psStatus", DbType.String, strStatus, Parameter.ParameterType.AddInParameter)
            Else
                params(27) = New Aurora.Common.Data.Parameter("psStatus", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@psUserId						VARCHAR(64)		= NULL ,
            params(28) = New Aurora.Common.Data.Parameter("psUserId", DbType.String, strUserId, Parameter.ParameterType.AddInParameter)

            '@psProgramName					VARCHAR(256)	= NULL ,
            params(29) = New Aurora.Common.Data.Parameter("psProgramName", DbType.String, strProgramName, Parameter.ParameterType.AddInParameter)

            '@rRnhId							VARCHAR(64)		= NULL ,
            params(30) = New Aurora.Common.Data.Parameter("rRnhId", DbType.String, strRentalHistoryID, Parameter.ParameterType.AddInParameter)


            '@psExtRef						VARCHAR	 (64) 	= NULL ,
            params(31) = New Aurora.Common.Data.Parameter("psExtRef", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)

            '@iNumOfAdults 	 				INT				= NULL ,				-- RKS Added: 10/07/2003
            params(32) = New Aurora.Common.Data.Parameter("iNumOfAdults", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)

            '@iNumOfChildren 				INT				= NULL ,				-- RKS Added: 10/07/2003
            params(33) = New Aurora.Common.Data.Parameter("iNumOfChildren", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)

            '@iNumOfInfants 					INT				= NULL ,				-- RKS Added: 10/07/2003
            params(34) = New Aurora.Common.Data.Parameter("iNumOfInfants", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)

            '@sPkgId							VARCHAR(64)		= NULL ,
            params(35) = New Aurora.Common.Data.Parameter("sPkgId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)

            '@sExclLstIn						VARCHAR(8000)	= NULL,
            params(36) = New Aurora.Common.Data.Parameter("sExclLstIn", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)

            '@bIsFirstFlag					BIT		= 1,
            params(37) = New Aurora.Common.Data.Parameter("bIsFirstFlag", DbType.Boolean, Convert.ToBoolean(bIsFirstFlag), Parameter.ParameterType.AddInParameter)

            '-- Return values 	

            '@pReturnError					VARCHAR(500)		OUTPUT ,
            params(38) = New Aurora.Common.Data.Parameter("pReturnError", DbType.String, 500, Parameter.ParameterType.AddOutParameter)
            '@pReturnMessage					VARCHAR(4000)		OUTPUT ,
            params(39) = New Aurora.Common.Data.Parameter("pReturnMessage", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
            '@pbUpdateFleet					BIT					OUTPUT ,
            params(40) = New Aurora.Common.Data.Parameter("pbUpdateFleet", DbType.Boolean, 1, Parameter.ParameterType.AddOutParameter)
            '@psBlrIdList					VARCHAR(2000)		OUTPUT ,
            params(41) = New Aurora.Common.Data.Parameter("psBlrIdList", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
            '@pbCallManageBlockingMessage	BIT					OUTPUT ,
            params(42) = New Aurora.Common.Data.Parameter("pbCallManageBlockingMessage", DbType.Boolean, 1, Parameter.ParameterType.AddOutParameter)
            '@NewBpdId						VARCHAR	 (3000)		OUTPUT					--	Added : RKS 08/07/2002
            params(43) = New Aurora.Common.Data.Parameter("NewBpdId", DbType.String, 3000, Parameter.ParameterType.AddOutParameter)


            '@piToOdo						INTEGER			= NULL ,
            If (lngOdometerTo > 0) Then
                params(44) = New Aurora.Common.Data.Parameter("piToOdo", DbType.Int32, CInt(lngOdometerTo), Parameter.ParameterType.AddInParameter)
            Else
                params(44) = New Aurora.Common.Data.Parameter("piToOdo", DbType.Int32, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            ''rev:mia 19Jan2015 - addition of these new parameters
            'If (Not String.IsNullOrEmpty(SlotId) And Not String.IsNullOrEmpty(SlotDescription) And Not String.IsNullOrEmpty(RentalId)) Then
            '    params(45) = New Aurora.Common.Data.Parameter("RntSelectedSlotId", DbType.String, SlotId, Parameter.ParameterType.AddInParameter)
            '    params(46) = New Aurora.Common.Data.Parameter("RntSelectedSlot", DbType.String, SlotDescription, Parameter.ParameterType.AddInParameter)
            '    params(47) = New Aurora.Common.Data.Parameter("RntId", DbType.String, RentalId, Parameter.ParameterType.AddInParameter)
            'Else
            '    If (String.IsNullOrEmpty(SlotId)) Then
            '        params(45) = New Aurora.Common.Data.Parameter("RntSelectedSlotId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            '    End If

            '    If (String.IsNullOrEmpty(SlotDescription)) Then
            '        params(46) = New Aurora.Common.Data.Parameter("RntSelectedSlot", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            '    End If

            '    If (String.IsNullOrEmpty(RentalId)) Then
            '        params(47) = New Aurora.Common.Data.Parameter("RntId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            '    End If

            'End If

            strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_ManageBookedProductChanges", params)

            If Not strerror Is Nothing Then
                If (strerror.Equals("SUCCESS") = True) Then strerror = String.Empty

                If (strerror.Length) <> 0 Then
                    strReturnError = strerror
                    Return False
                Else
                    ' change by shoel to fix the bpd page!
                    'strerror = params(38).Value
                    strReturnError = params(38).Value
                End If
                strReturnMessage = params(39).Value
            Else

                If (strerror.Length) <> 0 Then
                    strReturnError = strerror
                    Return False
                End If

            End If

            ' TODO Manny = "" means false
            ' "0" = false
            ' "1" = true
            ''REV:MIA SEPT25
            If params(40).Value.ToString = "" Then
                blnupdatefleet = False ''set from true to false
            ElseIf CBool(params(40).Value.ToString) = False Then
                blnupdatefleet = False
            Else
                blnupdatefleet = CBool(params(40).Value.ToString)
            End If

            ' re-coded by Shoel
            ' Assumptions : 0, False, "" mean False
            '               1, True mean True
            'If params(40).Value.ToString().Trim().Equals(String.Empty) Then
            '    blnupdatefleet = False
            'Else
            '    blnupdatefleet = CBool(params(40).Value.ToString().Trim())
            'End If
            ' re-coded by Shoel

            strReturnBlrIdList = params(41).Value
            strBit = params(42).Value.ToString
            sNewBpdId = params(43).Value

            sExcludeBpdList = sNewBpdId

            booCallManageBlockingMessage = convertSqlBitToVbBoolean(strBit)

            If booCallManageBlockingMessage = True Then
                strReturnError = String.Empty
                Return False
            Else
                If strReturnError.Length > 0 Then
                    Return False
                Else
                    Return True
                End If
            End If
        Catch ex As Exception

            strReturnError = ex.Message
            Return False

        End Try

    End Function

    Private Shared Function convertSqlBitToVbBoolean _
         (ByVal strBit As String) As Boolean

        ''strBit = Convert.ToInt16(strBit)

        If (strBit.Length) = 0 Then
            Return False
        Else

            Try
                strBit = Convert.ToInt16(strBit)
                If CLng(strBit) = 0 Then
                    Return False
                End If

            Catch ex As Exception
                Return False
            End Try

        End If ' IsNumeric(strBit) Else...

        Return True
    End Function

    Private Shared Function Execute_RES_UpdateAimsAfterEvent(ByVal strBookedProductID As String, _
                   ByVal strRetBpdId As String, _
                   ByVal pdBpdCkoWhen As String, _
                   ByVal pdBpdCkiWhen As String, _
                   ByVal psBpdCkoLocCode As String, _
                   ByVal psBpdCkiLocCode As String, _
                   ByVal strUserId As String, _
                   ByRef strErrfrom As String, _
                   ByRef strReturnError As String _
                   ) As Boolean


        Aurora.Common.Logging.LogInformation("Execute_RES_UpdateAimsAfterEvent", "Input Params ->" & vbNewLine & _
        "strBookedProductID =""" & strBookedProductID & """, " & vbNewLine & _
        "strRetBpdId =""" & strRetBpdId & """, " & vbNewLine & _
        "pdBpdCkoWhen =""" & pdBpdCkoWhen & """, " & vbNewLine & _
        "pdBpdCkiWhen =""" & pdBpdCkiWhen & """, " & vbNewLine & _
        "psBpdCkoLocCode =""" & psBpdCkoLocCode & """, " & vbNewLine & _
        "psBpdCkiLocCode =""" & psBpdCkiLocCode & """, " & vbNewLine & _
        "strUserId =""" & strUserId & """, " & vbNewLine & _
        "strErrfrom =""" & strErrfrom & """, " & vbNewLine & _
        "strReturnError =""" & strReturnError & """")

        Dim params(6) As Aurora.Common.Data.Parameter
        Dim strerror As String
        Try

            '@psBpdId		VARCHAR(64),
            If String.IsNullOrEmpty(strRetBpdId) Then
                params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, strBookedProductID, Parameter.ParameterType.AddInParameter)
            Else
                params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, strRetBpdId, Parameter.ParameterType.AddInParameter)
            End If

            '@pdBpdCkoWhen	DATETIME	=	NULL,
            If Not String.IsNullOrEmpty(pdBpdCkoWhen) Then
                params(1) = New Aurora.Common.Data.Parameter("pdBpdCkoWhen", DbType.DateTime, pdBpdCkoWhen, Parameter.ParameterType.AddInParameter)
            Else
                params(1) = New Aurora.Common.Data.Parameter("pdBpdCkoWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdBpdCkiWhen	DATETIME	=	NULL,
            If Not String.IsNullOrEmpty(pdBpdCkiWhen) Then
                params(2) = New Aurora.Common.Data.Parameter("pdBpdCkiWhen", DbType.DateTime, pdBpdCkiWhen, Parameter.ParameterType.AddInParameter)
            Else
                params(2) = New Aurora.Common.Data.Parameter("pdBpdCkiWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdBpdCkoLoc	VARCHAR(64)	=	NULL,
            If Not String.IsNullOrEmpty(psBpdCkoLocCode) Then
                params(3) = New Aurora.Common.Data.Parameter("pdBpdCkoLoc", DbType.String, psBpdCkoLocCode, Parameter.ParameterType.AddInParameter)
            Else
                params(3) = New Aurora.Common.Data.Parameter("pdBpdCkoLoc", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If

            '@pdBpdCkiLoc	VARCHAR(64)	=	NULL,
            If Not String.IsNullOrEmpty(psBpdCkiLocCode) Then
                params(4) = New Aurora.Common.Data.Parameter("pdBpdCkiLoc", DbType.String, psBpdCkiLocCode, Parameter.ParameterType.AddInParameter)
            Else
                params(4) = New Aurora.Common.Data.Parameter("pdBpdCkiLoc", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            End If


            '@sUsrCode		VARCHAR(64),
            params(5) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, strUserId, Parameter.ParameterType.AddInParameter)

            'JL 2006/06/26 
            'Reduce the length of the return value
            '@psReturnError	VARCHAR(8000)	OUTPUT
            'params(6) = New Aurora.Common.Data.Parameter("psReturnError", DbType.String, 8000, Parameter.ParameterType.AddOutParameter)
            params(6) = New Aurora.Common.Data.Parameter("psReturnError", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)

            strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_UpdateAimsAfterEvent", params)
            Aurora.Common.Logging.LogInformation("Execute_RES_UpdateAimsAfterEvent", "Completed RES_UpdateAimsAfterEvent, return value = """ & strerror & """, " & "psReturnError = """ & strErrfrom & """")

            'JL 2006/06/26 
            'Use index instead
            'strErrfrom = params("psReturnError").Value
            strErrfrom = params(6).Value

            If strerror.ToUpper.Equals("SUCCESS") Then strerror = String.Empty

            If strerror.Trim.Length <> 0 Then

                strReturnError = strerror

                Return False

            End If



            If strErrfrom.Trim.Length <> 0 Then
                strReturnError = strErrfrom
                Return False

            End If

            Return True

        Catch ex As Exception
            strReturnError = ex.Message
            Return False
        End Try

    End Function

    Public Shared Function ManageFleetUpdate _
      ( _
           ByVal strEvent As String, _
           ByVal strBookedProductID As String, _
           ByVal strCheckOutStatus As String, _
           ByVal strBpdBooId As String, _
           ByVal strBpdRntId As String, _
           ByVal sCtyCode As String, _
           ByRef strReturnError As String, _
           ByRef strReturnMessage As String, _
           Optional ByVal bVehicleOverride As Object = Nothing, _
           Optional ByVal sRntStatus As Object = Nothing, _
           Optional ByVal sCalledFrom As String = "" _
      ) As String

        ''rev:mia sept 25
        If (bVehicleOverride Is Nothing) Or (bVehicleOverride = "") Then
            bVehicleOverride = False
        End If
        ManageFleetUpdate = String.Empty


        Dim strBit As String = Nothing

        Dim strXml As String = Nothing
        Dim objXmlDocument As New Xml.XmlDocument
        Dim objXmlNodesToProcess As Xml.XmlNodeList = Nothing
        Dim objXmlFirstCkoStatusNode As Xml.XmlElement = Nothing


        Dim strBpdCkoWhen As String = Nothing
        Dim strBpdCkoDayPart As String = Nothing
        Dim strBpdCkoLocCode As String = Nothing
        Dim strBpdCkoOdo As String = Nothing
        Dim strBpdCkiWhen As String = Nothing
        Dim strBpdCkiDayPart As String = Nothing
        Dim strBpdCkiLocCode As String = Nothing
        Dim strBpdCkiOdo As String = Nothing
        Dim strBpdBooNum As String = Nothing
        Dim strBpdRntNum As String = Nothing
        Dim strBpdPrdId As String = Nothing
        Dim strBpdUnitNum As String = Nothing


        Dim dteBpdCkoWhen As Nullable(Of Date) = Nothing
        Dim dteBpdCkiWhen As Nullable(Of Date) = Nothing
        Dim intBpdCkoDayPart As Integer = 0
        Dim intBpdCkiDayPart As Integer = 0

        Dim intBpdCkoOdometer As Double = Nothing
        Dim intBpdCkiOdometer As Double = Nothing



        Dim AIMSRetValue As String = Nothing
        Dim sErrorString As String = Nothing

        Dim hasXMLError As Boolean = False
        Dim objXmlFirstCkoStatusNodeHasXMLerror As Boolean = True
        Dim strerror As String = Nothing



        Try

            '@psBpdId	VARCHAR(64)	= NULL,
            '@pReturnXml	VARCHAR(4000)	OUTPUT,
            '@pReturnError	VARCHAR(500)	OUTPUT

            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, strBookedProductID, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("pReturnXml", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
            params(2) = New Aurora.Common.Data.Parameter("pReturnError", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
            strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_getBookedProductsForDvassAndAims", params)

            Try
                Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "RES_getBookedProductsForDvassAndAims completed. pReturnXml=""" & CStr(params(1).Value) & """, pReturnError=""" & CStr(params(2).Value) & """, strerror=""" & strerror & """")
            Catch ex As Exception
            End Try

            If strerror.ToString.Equals("SUCCESS") Then strerror = String.Empty

            If (strerror.Length) <> 0 Then
                strReturnError = strerror & "From Here.."
                Exit Function
            End If
            strXml = params(1).Value


            If (strXml.Length) = 0 Then
                strReturnError = String.Empty
                Exit Function
            End If

            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Start loading objXmlDocument")
            objXmlDocument.LoadXml(strXml)

            Try
                Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished loading objXmlDocument. objXmlDocument=""" & CStr(objXmlDocument.OuterXml) & """")
            Catch ex As Exception
            End Try


            If objXmlDocument Is Nothing Then
                sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", strXml)
                Return String.Concat("<Root>", sErrorString, "<Data></Data></Root>")
            End If

            objXmlFirstCkoStatusNode = objXmlDocument.DocumentElement.SelectSingleNode("//Row[BpdCkoStatus]/BpdCkoStatus")
            If objXmlFirstCkoStatusNode Is Nothing Then
                Return XMLErrorGeneric(True, "SYS000", "System", "SYS000 - An error occurred in the ManageFleetUpdate() method: unable to locate first BpdCkoStatus node. Recovery was not possible.")
            End If ''end if for objXmlFirstCkoStatusNode

            strCheckOutStatus = objXmlFirstCkoStatusNode.InnerText
            objXmlNodesToProcess = objXmlDocument.DocumentElement.SelectNodes("//Row[(./BpdTypeCode = 'XFRM') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]")

            Try
                Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "objXmlNodesToProcess.Count=""" & objXmlNodesToProcess.Count & """")
            Catch ex As Exception
            End Try

            If (objXmlNodesToProcess.Count <> 0) Then
                If Not objXmlNodesToProcess Is Nothing Then

                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Starting getValuesForBpdType (1)")

                    Call getValuesForBpdType _
                                                             ( _
                                                                (objXmlNodesToProcess), _
                                                                strBpdCkoWhen, _
                                                                strBpdCkoDayPart, _
                                                                strBpdCkoLocCode, _
                                                                strBpdCkoOdo, _
                                                                strBpdCkiWhen, _
                                                                strBpdCkiDayPart, _
                                                                strBpdCkiLocCode, _
                                                                strBpdCkiOdo, _
                                                                strBpdBooNum, _
                                                                strBpdRntNum, _
                                                                strBpdPrdId, _
                                                                 strBpdUnitNum, _
                                                                strBpdBooId, _
                                                                strBpdRntId _
                                                             )

                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished getValuesForBpdType (1)")

                    If (strBpdCkoWhen = Nothing) Or _
                            (strBpdCkiWhen = Nothing) Then
                        strerror = String.Empty
                        strReturnError = strerror

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> (strBpdCkoWhen = Nothing) Or (strBpdCkiWhen = Nothing)" _
                                                                                  & vbNewLine _
                                                                                  & "Exiting - strReturnError=""" & strReturnError & """")

                        Return strReturnError
                    End If


                    If "#AM#PM#".ToString.IndexOf(strBpdCkoDayPart) = 0 Then
                        strerror = String.Empty
                        strReturnError = "Error While getting the Date Part"

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> ""#AM#PM#"".ToString.IndexOf(strBpdCkoDayPart) = 0" _
                                                                                  & vbNewLine _
                                                                                  & "Exiting - strReturnError=""" & strReturnError & """")

                        Return strReturnError
                    End If


                    If "#AM#PM#".ToString.IndexOf(strBpdCkiDayPart) = 0 Then
                        strerror = String.Empty
                        strReturnError = "Error While getting the Date Part"

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> ""#AM#PM#"".ToString.IndexOf(strBpdCkiDayPart) = 0" _
                                                                                  & vbNewLine _
                                                                                  & "Exiting - strReturnError=""" & strReturnError & """")

                        Return strReturnError
                    End If



                    If IsDate(strBpdCkoWhen) = False Then
                        strerror = String.Empty
                        strReturnError = "Not a valid Check Out Date"

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsDate(strBpdCkoWhen) = False" _
                                                                                 & vbNewLine _
                                                                                 & "Exiting - strReturnError=""" & strReturnError & """")


                        Return strReturnError
                    End If


                    If IsDate(strBpdCkiWhen) = False Then
                        strerror = ""
                        strReturnError = "Not a valid Check In Date"

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsDate(strBpdCkiWhen) = False" _
                                                                                & vbNewLine _
                                                                                & "Exiting - strReturnError=""" & strReturnError & """")


                        Return strReturnError
                    End If


                    If (strBpdCkoOdo = Nothing) Then strBpdCkoOdo = 0
                    If (strBpdCkiOdo = Nothing) Then strBpdCkiOdo = 0


                    dteBpdCkoWhen = Convert.ToDateTime(strBpdCkoWhen)
                    dteBpdCkiWhen = Convert.ToDateTime(strBpdCkiWhen)


                    intBpdCkoDayPart = convertStringDayPartToInteger(strBpdCkoDayPart)
                    intBpdCkiDayPart = convertStringDayPartToInteger(strBpdCkiDayPart)
                    intBpdCkoOdometer = Convert.ToDouble(strBpdCkoOdo)

                    intBpdCkoOdometer = Convert.ToDouble(strBpdCkoOdo)

                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Starting call to doAimsUpdateFleetCollaboration")

                    doAimsUpdateFleetCollaboration _
                                         ( _
                                            BookedProduct_EVENT_TYPE_MOD, _
                                            BookedProduct_BOOKED_PRODUCT_TYPECODE_XFRM, _
                                            strBpdBooNum, _
                                            strBpdRntNum, _
                                            strBpdCkoLocCode, _
                                            dteBpdCkoWhen, _
                                            intBpdCkoOdometer, _
                                            strBpdCkiLocCode, _
                                            dteBpdCkiWhen, _
                                            intBpdCkiOdometer, _
                                            strBpdUnitNum, _
                                            AIMSRetValue _
                                         )

                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished call to doAimsUpdateFleetCollaboration")
                End If

                If (AIMSRetValue.Length <> 0) Then
                    strReturnError = AIMSRetValue
                    Return strReturnError
                End If

            End If

            objXmlNodesToProcess = objXmlDocument.DocumentElement.SelectNodes("//Row[(./BpdTypeCode = 'XTO') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]")

            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "hasXMLError=""" & hasXMLError & """")

            If hasXMLError = False Then

                Try
                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "objXmlNodesToProcess.Count=""" & objXmlNodesToProcess.Count & """")
                Catch ex As Exception
                End Try


                If (objXmlNodesToProcess.Count <> 0) Then
                    If Not objXmlNodesToProcess Is Nothing Then

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Starting getValuesForBpdType (2)")

                        Call getValuesForBpdType _
                                            ( _
                                               objXmlNodesToProcess, _
                                               strBpdCkoWhen, _
                                               strBpdCkoDayPart, _
                                               strBpdCkoLocCode, _
                                               strBpdCkoOdo, _
                                               strBpdCkiWhen, _
                                               strBpdCkiDayPart, _
                                               strBpdCkiLocCode, _
                                               strBpdCkiOdo, _
                                               strBpdBooNum, _
                                               strBpdRntNum, _
                                               strBpdPrdId, _
                                               strBpdUnitNum, _
                                               strBpdBooId, _
                                               strBpdRntId _
                                            )

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished getValuesForBpdType (2)")

                        If IsNothing(strBpdCkoWhen) OrElse (IsNothing(strBpdCkiWhen)) Then
                            strerror = "Invalid Check out/In Date"

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsNothing(strBpdCkoWhen) OrElse (IsNothing(strBpdCkiWhen))" _
                                                                                 & vbNewLine _
                                                                                 & "Exiting - strerror=""" & strerror & """")

                            Return strerror
                        End If

                        If "#AM#PM#".ToString.IndexOf(strBpdCkoDayPart) = 0 Then
                            strerror = "Error in converting Date Part"

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> ""#AM#PM#"".ToString.IndexOf(strBpdCkoDayPart) = 0" _
                                                                                        & vbNewLine _
                                                                                        & "Exiting - strerror=""" & strerror & """")

                            Return strerror

                        End If

                        If IsDate(strBpdCkoWhen) = False Then
                            strerror = "Invalid Check Out Date"

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsDate(strBpdCkoWhen) = False" _
                                                                                      & vbNewLine _
                                                                                      & "Exiting - strerror=""" & strerror & """")

                            Return strerror
                        End If

                        If IsDate(strBpdCkiWhen) = False Then
                            strerror = "Invalid Check In Date "

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsDate(strBpdCkiWhen) = False" _
                                                                                     & vbNewLine _
                                                                                     & "Exiting - strerror=""" & strerror & """")

                            Return strerror
                        End If

                        dteBpdCkoWhen = Convert.ToDateTime(strBpdCkoWhen)
                        dteBpdCkiWhen = Convert.ToDateTime(strBpdCkiWhen)

                        intBpdCkoDayPart = convertStringDayPartToInteger(strBpdCkoDayPart)
                        intBpdCkiDayPart = convertStringDayPartToInteger(strBpdCkiDayPart)

                        If IsNothing(strBpdCkoOdo) Then strBpdCkoOdo = 0

                        intBpdCkoOdometer = CInt(strBpdCkoOdo)

                        If IsNothing(strBpdCkiOdo) Then strBpdCkiOdo = 0

                        intBpdCkiOdometer = CInt(strBpdCkiOdo)

                        ' Shoel : 3.4.9 : dont do this weird stuff if its a mod to a confirmed booking!
                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "sCalledFrom = """ & sCalledFrom & """" & vbNewLine & _
                                                                                  "strCheckOutStatus.ToString.ToLower = """ & strCheckOutStatus.ToString.ToLower & """")

                        ' If Not sCalledFrom = "BookedProdDetails" Then

                        Const CheckOutStatus As String = "before check-out"

                        If (strCheckOutStatus.ToString.ToLower) = CheckOutStatus Then

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Starting doDvassUpdateFleetCollaboration")

                            Call doDvassUpdateFleetCollaboration _
                                                     ( _
                                                        BookedProduct_EVENT_TYPE_MOD, _
                                                        strBpdBooNum, _
                                                        strBpdRntNum, _
                                                        strBpdPrdId, _
                                                        strBpdCkoLocCode, _
                                                        dteBpdCkoWhen, _
                                                        strBpdCkoDayPart, _
                                                        strBpdCkiLocCode, _
                                                        dteBpdCkiWhen, _
                                                        strBpdCkiDayPart, _
                                                        CStr(sCtyCode), _
                                                        bVehicleOverride, _
                                                        sRntStatus, _
                                                        strerror _
                                                     )

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished doDvassUpdateFleetCollaboration. strerror=""" & CStr(strerror) & """")

                            If strerror.Length <> 0 Then
                                If (strCheckOutStatus.ToString.ToLower) = CheckOutStatus Then
                                    Return strerror
                                End If
                            End If

                        Else ''Else for strCheckOutStatus.ToString.ToLower

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Starting doAimsUpdateFleetCollaboration")

                            Call doAimsUpdateFleetCollaboration _
                                                      ( _
                                                         BookedProduct_EVENT_TYPE_MOD, _
                                                         BookedProduct_BOOKED_PRODUCT_TYPECODE_XTO, _
                                                         strBpdBooNum, _
                                                         strBpdRntNum, _
                                                         strBpdCkoLocCode, _
                                                         dteBpdCkoWhen, _
                                                         intBpdCkoOdometer, _
                                                         strBpdCkiLocCode, _
                                                         dteBpdCkiWhen, _
                                                         intBpdCkiOdometer, _
                                                         strBpdUnitNum, _
                                                         AIMSRetValue _
                                                      )

                            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished doAimsUpdateFleetCollaboration. AIMSRetValue=""" & AIMSRetValue & """")

                            If (AIMSRetValue.Length) <> 0 Then
                                strReturnError = AIMSRetValue
                                Return strReturnError
                            End If
                        End If ''endif for strCheckOutStatus.ToString.ToLower

                    End If ' Shoel : 3.4.9 : dont do this weird stuff if its a mod to a confirmed booking!


                End If ''endif for (objXmlNodesToProcess.Count <> 0)
                'End If
            End If ''objXmlNodesToProcess.Count <> 0

            objXmlNodesToProcess = _
         objXmlDocument.DocumentElement.SelectNodes("//Row[(./BpdTypeCode != 'XFRM' and ./BpdTypeCode != 'XTO') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]")

            Try
                Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "DEBUG" & vbNewLine & " Using ""//Row[(./BpdTypeCode != 'XFRM' and ./BpdTypeCode != 'XTO') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]"" as a filter " & vbNewLine & objXmlDocument.DocumentElement.SelectSingleNode("//Row[(./BpdTypeCode != 'XFRM' and ./BpdTypeCode != 'XTO') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]").OuterXml)
            Catch ex As Exception
            End Try

            Try
                Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "DEBUG" & vbNewLine & " Using ""//Data/Row[(./BpdTypeCode != 'XFRM' and ./BpdTypeCode != 'XTO') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]"" as a filter " & vbNewLine & objXmlDocument.DocumentElement.SelectSingleNode("//Data/Row[(./BpdTypeCode != 'XFRM' and ./BpdTypeCode != 'XTO') and (./BpdStatus = 'KK' or ./BpdStatus = 'NN')]").OuterXml)
            Catch ex As Exception
            End Try

            Try
                Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "objXmlNodesToProcess.Count=" & objXmlNodesToProcess.Count)
            Catch ex As Exception
            End Try

            If (objXmlNodesToProcess.Count <> 0) Then

                If Not objXmlNodesToProcess Is Nothing Then

                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Starting getValuesForBpdType (3)")

                    Call getValuesForBpdType _
                                ( _
                                   objXmlNodesToProcess, _
                                   strBpdCkoWhen, _
                                   strBpdCkoDayPart, _
                                   strBpdCkoLocCode, _
                                   strBpdCkoOdo, _
                                   strBpdCkiWhen, _
                                   strBpdCkiDayPart, _
                                   strBpdCkiLocCode, _
                                   strBpdCkiOdo, _
                                   strBpdBooNum, _
                                   strBpdRntNum, _
                                   strBpdPrdId, _
                                   strBpdUnitNum, _
                                   strBpdBooId, _
                                   strBpdRntId _
                                )

                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Finished getValuesForBpdType (3)")

                    If IsNothing(strBpdCkoWhen) = True OrElse IsNothing(strBpdCkiWhen) = True Then
                        strerror = ""
                        strReturnError = strerror

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsNothing(strBpdCkoWhen) = True OrElse IsNothing(strBpdCkiWhen) = True" _
                                                                                    & vbNewLine _
                                                                                    & "Exiting - strerror=""" & strerror & """")

                        Return strerror
                    End If


                    If "#AM#PM#".ToString.IndexOf(strBpdCkoDayPart) = 0 Then
                        strerror = ""
                        strReturnError = strerror

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> ""#AM#PM#"".ToString.IndexOf(strBpdCkoDayPart) = 0" _
                                                                                    & vbNewLine _
                                                                                    & "Exiting - strerror=""" & strerror & """")

                        Return strerror
                    End If


                    If "#AM#PM#".ToString.IndexOf(strBpdCkiDayPart) = 0 Then
                        strerror = ""
                        strReturnError = strerror

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> ""#AM#PM#"".ToString.IndexOf(strBpdCkiDayPart) = 0" _
                                                                                            & vbNewLine _
                                                                                            & "Exiting - strerror=""" & strerror & """")

                        Return strerror
                    End If


                    If IsDate(strBpdCkoWhen) = False Then
                        strerror = ""

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsDate(strBpdCkoWhen) = False" _
                                                                                       & vbNewLine _
                                                                                       & "Exiting - strerror=""" & strerror & """")

                        Return strerror
                    End If



                    If IsDate(strBpdCkiWhen) = False Then
                        strerror = ""

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "Clause -> IsDate(strBpdCkiWhen) = False" _
                                                                                       & vbNewLine _
                                                                                       & "Exiting - strerror=""" & strerror & """")

                        Return strerror
                    End If


                    dteBpdCkoWhen = Convert.ToDateTime(strBpdCkoWhen)
                    dteBpdCkiWhen = Convert.ToDateTime(strBpdCkiWhen)

                    intBpdCkoDayPart = convertStringDayPartToInteger(strBpdCkoDayPart)
                    intBpdCkiDayPart = convertStringDayPartToInteger(strBpdCkiDayPart)

                    If IsNothing(strBpdCkoOdo) Then strBpdCkoOdo = 0
                    intBpdCkoOdometer = strBpdCkoOdo


                    If IsNothing(strBpdCkiOdo) Then strBpdCkiOdo = 0
                    intBpdCkiOdometer = strBpdCkiOdo

                    ' Shoel : 3.4.9 : dont do this weird stuff if its a mod to a confirmed booking!
                    Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "sCalledFrom=""" & sCalledFrom & """")

                    'If Not sCalledFrom = "BookedProdDetails" Then

                    Const CheckOutStatus As String = "before check-out"
                    If (strCheckOutStatus.ToString.ToLower) = CheckOutStatus Then

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "-- Starting doDvassUpdateFleetCollaboration")

                        Call doDvassUpdateFleetCollaboration _
                                         ( _
                                            BookedProduct_EVENT_TYPE_MOD, _
                                            strBpdBooNum, _
                                            strBpdRntNum, _
                                            strBpdPrdId, _
                                            strBpdCkoLocCode, _
                                            dteBpdCkoWhen, _
                                            strBpdCkoDayPart, _
                                            strBpdCkiLocCode, _
                                            dteBpdCkiWhen, _
                                            strBpdCkiDayPart, _
                                            CStr(sCtyCode), _
                                            bVehicleOverride, _
                                            sRntStatus, _
                                            strerror _
                                         )

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "-- Finished doDvassUpdateFleetCollaboration. strerror= """ & strerror & """")

                        If strerror.Length <> 0 Then
                            If (strCheckOutStatus.ToString.ToLower) = CheckOutStatus Then
                                strReturnError = strerror
                                Return strReturnError
                            End If
                        End If

                    Else ''Else for (strCheckOutStatus.ToString.ToLower)

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "-- Starting doAimsUpdateFleetCollaboration")

                        Call doAimsUpdateFleetCollaboration _
                              ( _
                                 BookedProduct_EVENT_TYPE_MOD, _
                                 BookedProduct_BOOKED_PRODUCT_TYPECODE_OTHER, _
                                 strBpdBooNum, _
                                 strBpdRntNum, _
                                 strBpdCkoLocCode, _
                                 dteBpdCkoWhen, _
                                 intBpdCkoOdometer, _
                                 strBpdCkiLocCode, _
                                 dteBpdCkiWhen, _
                                 intBpdCkiOdometer, _
                                 strBpdUnitNum, _
                                 AIMSRetValue _
                              )

                        Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "-- Finished doAimsUpdateFleetCollaboration. strerror= """ & strerror & """, AIMSRetValue=""" & AIMSRetValue & """")

                        If (AIMSRetValue.Length) <> 0 Then
                            strReturnError = AIMSRetValue
                            Return strReturnError

                        End If
                        If strerror.Length <> 0 Then
                            If (strCheckOutStatus.ToString.ToLower) = CheckOutStatus Then
                                strReturnError = strerror
                                Return strReturnError
                            End If
                        End If
                    End If ''endif for (strCheckOutStatus.ToString.ToLower)
                End If ' Shoel : 3.4.9 : dont do this weird stuff if its a mod to a confirmed booking!
                'End If
                ''  End If ''endif for objXmlNodesToProcess.Count <> 0
            End If ''hasXMLError = False

        Catch ex As Exception

            Aurora.Common.Logging.LogException("ManageFleetUpdate", ex)
            Aurora.Common.Logging.LogInformation("ManageFleetUpdate", "EXCEPTION STACK TRACE = " & ex.StackTrace)

            strReturnError = ex.Message

        End Try

    End Function

    Private Shared Sub getValuesForBpdType _
     ( _
          ByVal objXmlNodeList As Xml.XmlNodeList, _
          ByRef strCkoWhen As String, _
          ByRef strCkoDayPart As String, _
          ByRef strCkoLocCode As String, _
          ByRef strCkoOdo As String, _
          ByRef strCkiWhen As String, _
          ByRef strCkiDayPart As String, _
          ByRef strCkiLocCode As String, _
          ByRef strCkiOdo As String, _
          ByRef strBooNum As String, _
          ByRef strRntNum As String, _
          ByRef strPrdId As String, _
          ByRef strUnitNum As String, _
          ByRef strBooId As String, _
          ByRef strRntId As String _
     )


        Dim objListNode As XmlElement = Nothing


        strCkoWhen = String.Empty
        strCkoDayPart = String.Empty
        strCkoLocCode = String.Empty
        strCkoOdo = String.Empty
        strCkiWhen = String.Empty
        strCkiDayPart = String.Empty
        strCkiLocCode = String.Empty
        strCkiOdo = String.Empty
        strBooNum = String.Empty
        strRntNum = String.Empty
        strPrdId = String.Empty
        strUnitNum = String.Empty
        strBooId = String.Empty
        strRntId = String.Empty


        Try


            If objXmlNodeList Is Nothing Then
                Exit Sub
            Else

                If (objXmlNodeList.Count > 0) Then
                    objListNode = objXmlNodeList.Item(0)
                    strCkoWhen = (objListNode.SelectSingleNode("./BpdCkoWhen").InnerText.ToString)
                    strCkoDayPart = (objListNode.SelectSingleNode("./BpdCkoDayPart").InnerText.ToString)
                    strCkoLocCode = (objListNode.SelectSingleNode("./BpdCkoLocCode").InnerText.ToString)
                    strCkoOdo = (objListNode.SelectSingleNode("./BpdCkoOdometer").InnerText.ToString)
                    strBooNum = (objListNode.SelectSingleNode("./BpdBooNum").InnerText.ToString)
                    strRntNum = (objListNode.SelectSingleNode("./BpdRntNum").InnerText.ToString)
                    strPrdId = (objListNode.SelectSingleNode("./BpdSapPrdId").InnerText.ToString)
                    strUnitNum = (objListNode.SelectSingleNode("./BpdUnitNum").InnerText.ToString)
                    strBooId = (objListNode.SelectSingleNode("./BpdBooId").InnerText.ToString)
                    strRntId = (objListNode.SelectSingleNode("./BpdRntId").InnerText.ToString)

                    If objXmlNodeList.Count > 1 Then
                        objListNode = objXmlNodeList.Item(objXmlNodeList.Count - 1)
                    End If

                    strCkiWhen = (objListNode.SelectSingleNode("./BpdCkiWhen").InnerText.ToString)
                    strCkiDayPart = (objListNode.SelectSingleNode("./BpdCkiDayPart").InnerText.ToString)
                    strCkiLocCode = (objListNode.SelectSingleNode("./BpdCkiLocCode").InnerText.ToString)
                    strCkiOdo = (objListNode.SelectSingleNode("./BpdCkiOdometer").InnerText.ToString)
                End If
            End If

        Catch ex As Exception

            objXmlNodeList = Nothing

        End Try

    End Sub

    Private Shared Sub doAimsUpdateFleetCollaboration _
                ( _
                 ByVal strEvent As String, _
                 ByVal strType As String, _
                 ByVal strAimsBooNum As String, _
                 ByVal strAimsRntNum As String, _
                 ByVal strAimsCkoLocCode As String, _
                 ByVal dteAimsCkoWhen As Date, _
                 ByVal intAimsCkoOdometer As Double, _
                 ByVal strAimsCkiLocCode As String, _
                 ByVal dteAimsCkiWhen As Date, _
                 ByVal intAimsCkiOdometer As Double, _
                 ByVal strAimsUnitNum As String, _
                 ByRef strAimsReturnError As String _
                )


        Dim AimsExternalReference As String = String.Empty


        Dim objAimsCollaborate As Object = Nothing

        Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "-- Starting CheckAimsCollaborateAvailability")
        If CheckAimsCollaborateAvailability(objAimsCollaborate, strAimsReturnError) = False Then
            Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "-- Finished CheckAimsCollaborateAvailability. Result = False")
            Exit Sub
        End If
        Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "-- Finished CheckAimsCollaborateAvailability. Result = True")

        Try

            Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case selection  - """ & strType.ToString.ToUpper & """")

            Select Case (strType.ToString.ToUpper)

                Case "XTO"


                    AimsExternalReference = assembleAimsExternalReference _
                                   ( _
                                      strAimsBooNum, _
                                      strAimsRntNum, _
                                      createAssignmentNumberToMakeAimsReferenceUnique() _
                                   )

                    Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case XTO" & vbNewLine & "AimsExternalReference=""" & AimsExternalReference & """")

                    strAimsReturnError = objAimsCollaborate.F6_ActivityCreate _
                             ( _
                                BookedProduct_AIMS_ACTIVITY_TYPE_RENTAL, _
                                AimsExternalReference, _
                                strAimsUnitNum, _
                                strAimsCkoLocCode, _
                                dteAimsCkoWhen, _
                                intAimsCkoOdometer, _
                                strAimsCkiLocCode, _
                                dteAimsCkiWhen, _
                                intAimsCkiOdometer)

                    Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case XTO" & vbNewLine & "strAimsReturnError=""" & strAimsReturnError & """")

                Case "XFRM"



                    AimsExternalReference = assembleAimsExternalReference(strAimsBooNum, strAimsRntNum, "")
                    Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case XFRM" & vbNewLine & "AimsExternalReference=""" & AimsExternalReference & """")

                    strAimsReturnError = objAimsCollaborate.F8_ActivityComplete _
                          ( _
                             BookedProduct_AIMS_ACTIVITY_TYPE_RENTAL, _
                             AimsExternalReference, _
                             strAimsCkiLocCode, _
                             dteAimsCkiWhen, _
                             intAimsCkiOdometer)
                    Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case XFRM" & vbNewLine & "strAimsReturnError=""" & strAimsReturnError & """")

                Case Else


                    AimsExternalReference = assembleAimsExternalReference(strAimsBooNum, strAimsRntNum, "")
                    Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case ELSE" & vbNewLine & "AimsExternalReference=""" & AimsExternalReference & """")

                    strAimsReturnError = objAimsCollaborate.F7_ActivityUpdate _
                            ( _
                               BookedProduct_AIMS_ACTIVITY_TYPE_RENTAL, _
                               AimsExternalReference, _
                               strAimsUnitNum.Trim, _
                               intAimsCkoOdometer, _
                               strAimsCkiLocCode, _
                               dteAimsCkiWhen, _
                               -1, _
                               String.Empty)
                    Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "Case ELSE" & vbNewLine & "strAimsReturnError=""" & strAimsReturnError & """")
            End Select ' type


            If Not strAimsReturnError.Equals("0".ToString) Then

                'JL 20080718 begin
                'Make sure bjAimsCollaborate.LastError is not nothing

                'Dim st As String = (objAimsCollaborate.LastError.ToString)
                Dim st As String = ""

                Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "objAimsCollaborate.LastError=""" & objAimsCollaborate.LastError & """")

                If Not objAimsCollaborate.LastError Is Nothing Then
                    st = (objAimsCollaborate.LastError.ToString)
                End If
                'JL 20080718 end


                If Not String.IsNullOrEmpty(st) Then

                    strAimsReturnError = getAimsMessage(strAimsReturnError) & st

                Else

                    strAimsReturnError = getAimsMessage(strAimsReturnError)

                End If

            Else

                strAimsReturnError = String.Empty

            End If ' aims reported errors



        Catch ex As Exception
            Aurora.Common.Logging.LogException("doAimsUpdateFleetCollaboration", ex)
            Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "EXCEPTION STACK TRACE = " & ex.StackTrace)

            strAimsReturnError = ex.Message

        Finally


            Aurora.Common.Logging.LogInformation("doAimsUpdateFleetCollaboration", "FINALLY Part. strAimsReturnError=""" & strAimsReturnError & """")

        End Try

    End Sub

    Private Shared Function createAssignmentNumberToMakeAimsReferenceUnique() As String

        '   @psRntId			VARCHAR(64),
        '	@psSapId			VARCHAR(64),
        '	@pdPrevCkoWhen		DATETIME	=NULL,
        '	@pdCkoWhen			DATETIME	=NULL,

        '	@pdPrevCkiWhen		DATETIME	=NULL,
        '	@pdCkiWhen			DATETIME	=NULL,
        '	@psPrevCkoLocCode	VARCHAR(12)	=NULL,
        '	@psCkoLocCode		VARCHAR(12)	=NULL,

        '	@psPrevCkiLocCode	VARCHAR(12)	=NULL,
        '	@psCkiLocCode		VARCHAR(12)	=NULL,	
        '	@pdPrevChargeFrom	DATETIME	=NULL,
        '	@pdChargeFrom		DATETIME	=NULL,

        '	@pdPrevChargeTo		DATETIME	=NULL,
        '	@pdChargeTo			DATETIME	=NULL,
        '--  Output Parameters
        '	@psOridAlwaysRoles	VARCHAR(500)	OUTPUT,
        '	@psOridAlwaysErrors	VARCHAR(500)	OUTPUT

        Dim strTemp As String = Nothing
        Dim params(15) As Aurora.Common.Data.Parameter

        Try


            params(0) = New Aurora.Common.Data.Parameter("psRntId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("psSapId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("pdPrevCkoWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)


            params(4) = New Aurora.Common.Data.Parameter("pdPrevCkiWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("psPrevCkoLocCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)


            params(8) = New Aurora.Common.Data.Parameter("psPrevCkiLocCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("pdPrevChargeFrom", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("pdChargeFrom", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)

            params(12) = New Aurora.Common.Data.Parameter("pdPrevChargeTo", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(13) = New Aurora.Common.Data.Parameter("pdChargeTo", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)

            params(14) = New Aurora.Common.Data.Parameter("psOridAlwaysRoles", DbType.String, 500, Parameter.ParameterType.AddOutParameter)
            params(15) = New Aurora.Common.Data.Parameter("psOridAlwaysErrors", DbType.String, 500, Parameter.ParameterType.AddOutParameter)

            strTemp = Aurora.Common.Data.ExecuteOutputObjectSP("RES_CheckCkoCkiWhen", params)

            If strTemp Is Nothing Then
                strTemp = params("psOridAlwaysErrors").Value
            End If


            Return strTemp
        Catch ex As Exception
            Return ex.Message
        End Try

    End Function

    Private Shared Function CheckAimsCollaborateAvailability(ByRef objAIMS As Object, ByRef RetMsg As String, Optional ByVal logMsg As String = "") As Boolean

        Try

            If objAIMS Is Nothing Then
                objAIMS = CreateObject("AI.CMaster")
            End If
        Catch ex As Exception

            RetMsg = ex.Message

            Return False

        End Try

        Return True
    End Function

    Private Shared Function assembleAimsExternalReference _
      ( _
           ByVal strBooNum As String, _
           ByVal strRntNum As String, _
           Optional ByVal strAsnNum As String = "" _
      ) As String

        Const AIMS_SEPARATOR = "/"

        Dim strtemp As String = IIf((strAsnNum.Length) = 0, String.Empty, String.Concat(AIMS_SEPARATOR, strAsnNum))
        Return String.Concat(strBooNum, AIMS_SEPARATOR, strRntNum, strtemp)

    End Function

    Private Shared Sub doDvassUpdateFleetCollaboration _
               ( _
                ByVal strEvent As String, _
                ByVal strRntBooNum As String, _
                ByVal strRntNum As String, _
                ByVal strProductId As String, _
                ByVal strCkoLocation As String, _
                ByVal dteCkoDate As Date, _
                ByVal strCkoDayPart As String, _
                ByVal strCkiLocation As String, _
                ByVal dteCkiDate As Date, _
                ByVal strCkiDayPart As String, _
                ByVal sCtyCode As String, _
                ByVal bVehicleOverride As Boolean, _
                ByVal sRntStatus As String, _
                ByRef strDvassReturnError As String _
               )

        Dim dblDvassRevenue As Double = 0
        Dim pNumberOfResultsReturned As Long = 0
        Dim objDvassCollaborateHasError As Boolean = False
        Dim strTempA As New StringBuilder


        Try

            If objDvassCollaborateHasError = False Then

                If (sRntStatus.Length > 0) And _
                   Not (sRntStatus.Equals("KK")) And _
                   Not (sRntStatus.Equals("NN")) Then

                    If (sCtyCode.ToString.Equals("AU")) Then
                        Aurora.Reservations.Services.AuroraDvass.selectCountry(0)
                    ElseIf (sCtyCode.ToString.Equals("NZ")) Then
                        Aurora.Reservations.Services.AuroraDvass.selectCountry(1)
                    End If


                    If Aurora.Reservations.Services.AuroraDvass.WrapFunFordoDvassQueryAvailability(strProductId, _
                                         strCkoLocation, _
                                         strCkiLocation, _
                                         dteCkoDate, _
                                         convertStringDayPartToInteger(strCkoDayPart), _
                                         dteCkiDate, _
                                         convertStringDayPartToInteger(strCkiDayPart), _
                                         strDvassReturnError, _
                                         pNumberOfResultsReturned _
                                         ) = False Then

                        If (strDvassReturnError.Length) > 0 Then
                            Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Exiting Sub on Clause -> (strDvassReturnError.Length) > 0" & vbNewLine & "strDvassReturnError=""" & strDvassReturnError & """")
                            Exit Sub
                        End If

                        If pNumberOfResultsReturned = 0 Then
                            strDvassReturnError = "DVASS: No Suitable vehicle available"
                            Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Exiting Sub on Clause -> pNumberOfResultsReturned = 0" & vbNewLine & "strDvassReturnError=""" & strDvassReturnError & """")
                            Exit Sub
                        End If

                    End If ''endif for objDvassCollaborate.WrapFunFordoDvassQueryAvailability
                End If ''endif for (sRntStatus.ToString.Length > 0)


                If (sCtyCode.ToString.Equals("AU") = True) Then
                    Call Aurora.Reservations.Services.AuroraDvass.selectCountry(0)
                ElseIf (sCtyCode.ToString.Equals("NZ") = True) Then
                    Call Aurora.Reservations.Services.AuroraDvass.selectCountry(1)
                End If


                Dim bAvaiResult As Boolean = False
                Dim iCountry As Integer

                If sCtyCode.ToUpper.Equals("AU") = True Then
                    iCountry = 0
                Else
                    iCountry = 1
                End If

                Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "bVehicleOverride=""" & bVehicleOverride & """" & vbNewLine & "sRntStatus=""" & sRntStatus & """")

                If (bVehicleOverride = False) AndAlso _
                   (sRntStatus.Equals("KK") = False) AndAlso _
                   (sRntStatus.Equals("NN") = False) Then

                    Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Starting call to Aurora.Reservations.Services.AuroraDvass.QueryDvassAvailability")
                    
                    bAvaiResult = Aurora.Reservations.Services.AuroraDvass.QueryDvassAvailability _
                                        (iCountry, _
                                         strProductId, _
                                         strCkoLocation, _
                                         dteCkoDate, _
                                         convertStringDayPartToInteger(strCkoDayPart), _
                                         strCkiLocation, _
                                         dteCkiDate, _
                                         convertStringDayPartToInteger(strCkiDayPart), _
                                         strDvassReturnError)

                    Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Finished call to Aurora.Reservations.Services.AuroraDvass.QueryDvassAvailability. strDvassReturnError=""" & strDvassReturnError & """, bAvaiResult=""" & bAvaiResult & """")

                    If (strDvassReturnError.Length) > 0 Then

                        Exit Sub

                    End If


                    If bAvaiResult = True Then

                        Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Starting call to Aurora.Reservations.Services.AuroraDvass.rentalModify")

                        Call Aurora.Reservations.Services.AuroraDvass.rentalModify _
                                             ( _
                                                assembleDvassRentalId(strRntBooNum, strRntNum), _
                                                strProductId, _
                                                strCkoLocation, _
                                                dteCkoDate, _
                                                convertStringDayPartToInteger(strCkoDayPart), _
                                                strCkiLocation, _
                                                dteCkiDate, _
                                                convertStringDayPartToInteger(strCkiDayPart), _
                                                dblDvassRevenue, _
                                                BookedProduct_DVASS_PRIORITY_ONE, _
                                                BookedProduct_DVASS_VISIBILITY_TEN, _
                                                String.Empty, _
                                                IIf(bVehicleOverride, BookedProduct_DVASS_FORCEFLAG_ON, BookedProduct_DVASS_FORCEFLAG_OFF), _
                                                strDvassReturnError _
                                             )

                        Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Finished call to Aurora.Reservations.Services.AuroraDvass.rentalModify. strDvassReturnError=""" & strDvassReturnError & """")

                    Else

                        strDvassReturnError = String.Concat("AURORA - DVASS Query Availability", vbCrLf, " No Suitable Vehicle Available")
                        Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "bAvaiResult=False, strDvassReturnError = """ & strDvassReturnError & """")

                    End If ''ENDIF FOR bAvaiResult = True

                Else
                    Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Starting call to Aurora.Reservations.Services.AuroraDvass.rentalModify")
                    Call Aurora.Reservations.Services.AuroraDvass.rentalModify _
                                         ( _
                                            assembleDvassRentalId(strRntBooNum, strRntNum), _
                                            strProductId, _
                                            strCkoLocation, _
                                            dteCkoDate, _
                                            convertStringDayPartToInteger(strCkoDayPart), _
                                            strCkiLocation, _
                                            dteCkiDate, _
                                            convertStringDayPartToInteger(strCkiDayPart), _
                                            dblDvassRevenue, _
                                            BookedProduct_DVASS_PRIORITY_ONE, _
                                            BookedProduct_DVASS_VISIBILITY_TEN, _
                                            String.Empty, _
                                            IIf(bVehicleOverride, BookedProduct_DVASS_FORCEFLAG_ON, BookedProduct_DVASS_FORCEFLAG_OFF), _
                                            strDvassReturnError _
                                         )
                    Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "Finished call to Aurora.Reservations.Services.AuroraDvass.rentalModify. strDvassReturnError=""" & strDvassReturnError & """")
                End If



                If strDvassReturnError.Equals("SUCCESS") = True Then strDvassReturnError = String.Empty

            End If ''endif for objDvassCollaborateHasError

        Catch ex As Exception
            Aurora.Common.Logging.LogException("doDvassUpdateFleetCollaboration", ex)
            Aurora.Common.Logging.LogInformation("doDvassUpdateFleetCollaboration", "EXCEPTION STACK TRACE = " & ex.StackTrace)
            strDvassReturnError = XMLErrorGeneric(True, "Error", "[doDvassUpdateFleetCollaboration] " & ex.Source, ex.Message)

        Finally


        End Try

    End Sub

    Private Shared Function Execute_RES_ManageBpdModFleetError( _
          ByVal strBookedProductID As String, _
          ByVal strSaleableProductID As String, _
          ByVal dteCkoWhen As String, _
          ByVal dteCkiWhen As String, _
          ByVal strCkoLocCode As String, _
          ByVal strCkiLocCode As String, _
          ByVal lngFrom As Long, _
          ByVal lngTo As Long, _
          ByVal dblRate As String, _
          ByVal lngQuantity As Long, _
          ByVal strPrrIdList As String, _
          ByVal strPrrQtyList As String, _
          ByVal strPrrRateList As String, _
          ByVal strPtdId As String, _
          ByVal strUnitNumber As String, _
          ByVal strSerialCode As String, _
          ByVal strReturnBlrIdList As String, _
          ByVal strUserId As String, _
          ByVal strProgramName As String, _
          ByRef strReturnError As String, _
          ByRef strReturnMessage As String, _
          ByVal strBookingRequestIdNew As String _
          ) As Boolean



        Dim params(6) As Aurora.Common.Data.Parameter
        Dim strerror As String
        Try


            ''   @psBpdId		VARCHAR(64),Call .appendParameterIn("@psBpdId", 200, 64, strBookedProductID)
            params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, strBookedProductID, Parameter.ParameterType.AddInParameter)

            '@psSapId		VARCHAR(64),Call .appendParameterIn("@sSapId", 200, 64, strSaleableProductID)
            params(0) = New Aurora.Common.Data.Parameter("sSapId", DbType.String, strSaleableProductID, Parameter.ParameterType.AddInParameter)

            '@pdCkoWhen		DATETIME,Call .appendParameterIn("@pdCkoWhen", 7, 8, dteCkoWhen)
            params(0) = New Aurora.Common.Data.Parameter("pdCkoWhen", DbType.String, dteCkoWhen, Parameter.ParameterType.AddInParameter)

            '@pdCkiWhen		DATETIME,Call .appendParameterIn("@pdCkiWhen", 7, 8, dteCkiWhen)
            params(0) = New Aurora.Common.Data.Parameter("pdCkiWhen", DbType.String, dteCkiWhen, Parameter.ParameterType.AddInParameter)

            '@psCkoLocCode		VARCHAR(64),Call .appendParameterIn("@psCkoLocCode", 200, 64, strCkoLocCode)
            params(0) = New Aurora.Common.Data.Parameter("psCkoLocCode", DbType.String, strCkoLocCode, Parameter.ParameterType.AddInParameter)

            '@psCkiLocCode		VARCHAR(64), Call .appendParameterIn("@psCkiLocCode", 200, 64, strCkiLocCode)
            params(0) = New Aurora.Common.Data.Parameter("psCkiLocCode", DbType.String, strCkiLocCode, Parameter.ParameterType.AddInParameter)

            '@piFrom			INTEGER		= NULL,Call .appendParameterIn("@piFrom", 3, 0, CInt(lngFrom))
            params(0) = New Aurora.Common.Data.Parameter("piFrom", DbType.String, CInt(lngFrom), Parameter.ParameterType.AddInParameter)

            '@piTo			INTEGER		= NULL,Call .appendParameterIn("@piTo", 3, 0, CInt(lngTo))
            params(0) = New Aurora.Common.Data.Parameter("piTo", DbType.String, CInt(lngTo), Parameter.ParameterType.AddInParameter)

            '@pcRate			MONEY		= NULL,
            params(0) = New Aurora.Common.Data.Parameter("pcRate", DbType.String, CInt(dblRate), Parameter.ParameterType.AddInParameter)

            '@piQuantity		INTEGER		= NULL,Call .appendParameterIn("@piQuantity", 3, 0, CInt(lngQuantity))
            params(0) = New Aurora.Common.Data.Parameter("piQuantity", DbType.String, CInt(lngQuantity), Parameter.ParameterType.AddInParameter)

            '@psPrrIdList		VARCHAR(2000)	= NULL,    Call .appendParameterIn("@psPrrIdList", 200, 2000, strPrrIdList)
            params(0) = New Aurora.Common.Data.Parameter("psPrrIdList", DbType.String, strPrrIdList, Parameter.ParameterType.AddInParameter)

            '@psPrrQtyList		VARCHAR(2000)	= NULL,Call .appendParameterIn("@psPrrQtyList", 200, 2000, strPrrQtyList)
            params(0) = New Aurora.Common.Data.Parameter("psPrrQtyList", DbType.String, strPrrQtyList, Parameter.ParameterType.AddInParameter)

            '@psPrrRateList		VARCHAR(2000)	= NULL,Call .appendParameterIn("@psPrrRateList", 200, 2000, strPrrRateList)
            params(0) = New Aurora.Common.Data.Parameter("psPrrRateList", DbType.String, strPrrRateList, Parameter.ParameterType.AddInParameter)

            '@psPtdId		VARCHAR(64)	= NULL,Call .appendParameterIn("@psPtdId", 200, 64, strPtdId)
            params(0) = New Aurora.Common.Data.Parameter("psPtdId", DbType.String, strPtdId, Parameter.ParameterType.AddInParameter)

            '@psUnitNumber		VARCHAR(64)	= NULL,Call .appendParameterIn("@psUnitNumber", 200, 64, strUnitNumber)
            params(0) = New Aurora.Common.Data.Parameter("psUnitNumber", DbType.String, strUnitNumber, Parameter.ParameterType.AddInParameter)

            '@psSerialCode		VARCHAR(64)	= NULL,Call .appendParameterIn("@psSerialCode", 200, 64, strSerialCode)
            params(0) = New Aurora.Common.Data.Parameter("psSerialCode", DbType.String, strSerialCode, Parameter.ParameterType.AddInParameter)

            '@psBlrIdList		VARCHAR(2000)	= NULL,Call .appendParameterIn("@psBlrIdList", 200, 2000, strReturnBlrIdList)
            params(0) = New Aurora.Common.Data.Parameter("psBlrIdList", DbType.String, strReturnBlrIdList, Parameter.ParameterType.AddInParameter)

            '-- For audit columns
            '@psUserId		VARCHAR(64)	= NULL,Call .appendParameterIn("@psUserId", 200, 64, strUserId)
            params(0) = New Aurora.Common.Data.Parameter("psUserId", DbType.String, strUserId, Parameter.ParameterType.AddInParameter)

            '@psProgramName		VARCHAR(256)	= NULL,Call .appendParameterIn("@psProgramName", 200, 64, strProgramName)
            params(0) = New Aurora.Common.Data.Parameter("psProgramName", DbType.String, strProgramName, Parameter.ParameterType.AddInParameter)

            '-- Return parameters
            '@psBkrId		VARCHAR(64)	OUTPUT,
            params(6) = New Aurora.Common.Data.Parameter("psBkrId", DbType.String, 64, Parameter.ParameterType.AddOutParameter)

            '@pReturnError		VARCHAR(500)	OUTPUT,
            params(6) = New Aurora.Common.Data.Parameter("pReturnError", DbType.String, 500, Parameter.ParameterType.AddOutParameter)

            '@pReturnMessage		VARCHAR(500)	OUTPUT
            params(6) = New Aurora.Common.Data.Parameter("pReturnMessage", DbType.String, 500, Parameter.ParameterType.AddOutParameter)



            strerror = Aurora.Common.Data.ExecuteOutputObjectSP("RES_ManageBpdModFleetError", params)

            If (strerror.Length) <> 0 AndAlso (strReturnError.Length) = 0 Then
                strReturnError = strerror
            End If

            If strReturnError.Length <> 0 Then
                Return strReturnError
            End If

        Catch ex As Exception
            strerror = XMLErrorGeneric(True, "Error", _
                                         "[ Execute_RES_ManageBpdModFleetError] " & ex.Message, "", 1)
            Return False
        End Try



        Return True

    End Function

    Private Shared Function assembleDvassRentalId _
                ( _
                ByVal strBooNum As String, _
                ByVal strRntNum As String _
                ) As String

        Return String.Concat(strBooNum, "/", strRntNum)

    End Function
#End Region

#Region "For Recalculate Charges and validate bookings"

    Public Shared Function CheckRentalIntegrity(ByVal sRentalId As String, _
               ByVal sBPDId As String, _
               ByVal sBooID As String, _
               ByVal nScreenRentalIntegrityNo As String, _
               ByVal nScreenBookingIntegrityNo As String) As Boolean


        Dim bDataIsCurrent As Boolean = False


        Dim sDataFromDB As String
        Dim oXml As New XmlDocument
        Dim nDataBaseRentalIntegrityNo As String = "0"
        Dim nDataBaseBooIntNo As String = "0"



        sDataFromDB = Aurora.Common.Data.ExecuteScalarSP("RES_GetRentalIntegrity", sRentalId, sBPDId, sBooID)
        oXml.LoadXml(sDataFromDB)


        If Not oXml.SelectSingleNode("/Rental/IntegrityNo") Is Nothing Then
            nDataBaseRentalIntegrityNo = (oXml.SelectSingleNode("/Rental/IntegrityNo").InnerText)
        End If

        If nDataBaseRentalIntegrityNo.Equals(nScreenRentalIntegrityNo) Then

            If Not oXml.SelectSingleNode("/Rental/Booking/BooIntNo") Is Nothing Then
                nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("/Rental/Booking/BooIntNo").InnerText)
            Else

                If Not oXml.SelectSingleNode("/Booking/BooIntNo") Is Nothing Then
                    nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("/Booking/BooIntNo").InnerText)
                End If
            End If


            If nDataBaseBooIntNo.Equals(nScreenBookingIntegrityNo) Then
                bDataIsCurrent = True
            Else
                bDataIsCurrent = False
            End If


        Else
            'failed very first check!
            bDataIsCurrent = False
        End If

        Return bDataIsCurrent

    End Function

    Public Shared Function ManageRecalculateCharges(ByVal sbpdID As String, _
                                                    ByVal soldIntno As String, _
                                                    ByVal sForce As String, _
                                                    ByVal sUserCode As String, _
                                                    ByVal sprogramName As String) As String
        Try

            ''getIntegrityNoForRnt
            '@sRntId   VARCHAR  (64) = NULL ,  
            '@sBpdId   VARCHAR  (64) = NULL ,  
            '@iIntegrityNo BIGINT   = NULL ,  
            '@sErrorRet  VARCHAR  (8000) OUTPUT  
            ''exec getIntegrityNoForRnt @sBpdId='F82930AF92B040E2B9121A76326F2DC5',@iIntegrityNo=1,@sErrorRet=@p3 output

            Dim params(3) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, sbpdID, Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.String, soldIntno, Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sErrorRet", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
            Dim tempStrError As String = Aurora.Common.Data.ExecuteOutputObjectSP("getIntegrityNoForRnt", params)

            ''check return value
            If tempStrError = "SUCCESS" Then tempStrError = ""
            If Not String.IsNullOrEmpty(tempStrError) Then
                Return "<Root><Error>" & GetErrorTextFromResource(True, "Error", "Application", tempStrError) & "</Error><Msg></Msg></Root>"
            End If

            ''check parameter output value
            tempStrError = params(3).Value
            If Not String.IsNullOrEmpty(tempStrError) Then
                Return "<Root><Error>" & GetErrorTextFromResource(True, "Error", "Application", tempStrError) & "</Error><Msg></Msg></Root>"
            End If


            '@sBpdId   VARCHAR  (64) = '' ,  
            '@bForce   VARCHAR  (1) = '' ,  
            '@sPrgmName  VARCHAR  (64) = '' ,  
            '@sAddUsrName VARCHAR  (64) = '' ,  
            '@sErrStr  VARCHAR  (2000) OUTPUT ,  
            '@sMsg   VARCHAR  (2000) OUTPUT ,  
            '@sBpdIds  VARCHAR  (64) OUTPUT  

            Dim paramsA(6) As Aurora.Common.Data.Parameter
            paramsA(0) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, sbpdID, Parameter.ParameterType.AddInParameter)
            paramsA(1) = New Aurora.Common.Data.Parameter("bForce", DbType.String, sForce, Parameter.ParameterType.AddInParameter)
            paramsA(2) = New Aurora.Common.Data.Parameter("sPrgmName", DbType.String, sprogramName, Parameter.ParameterType.AddInParameter)
            paramsA(3) = New Aurora.Common.Data.Parameter("sAddUsrName", DbType.String, sUserCode, Parameter.ParameterType.AddInParameter)
            paramsA(4) = New Aurora.Common.Data.Parameter("sErrStr", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
            paramsA(5) = New Aurora.Common.Data.Parameter("sMsg", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
            paramsA(6) = New Aurora.Common.Data.Parameter("sBpdIds", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            tempStrError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_RecalculateForBpd", paramsA)

            ''check return value
            If tempStrError = "SUCCESS" Then tempStrError = ""
            If Not String.IsNullOrEmpty(tempStrError) Then
                Return "<Root><Error>" & GetErrorTextFromResource(True, "Error", "Application", tempStrError) & "</Error><Msg></Msg></Root>"
            End If

            ''check parameter output value
            tempStrError = paramsA(4).Value
            If Not String.IsNullOrEmpty(tempStrError) Then
                Return "<Root><Error>" & GetErrorTextFromResource(True, "Error", "Application", tempStrError) & "</Error><Msg></Msg></Root>"
            End If

            Dim tempStrMsg As String = paramsA(5).Value
            Dim tempStrBPDID As String = paramsA(6).Value

            If Not String.IsNullOrEmpty(tempStrBPDID) Then
                tempStrBPDID = tempStrBPDID.Split(",")(0)
            Else
                tempStrBPDID = sbpdID
            End If

            '@sBooId   VARCHAR  (64) = NULL ,  
            '@sRntId   VARCHAR  (64) = NULL ,  
            '@sSapId   VARCHAR  (64) = NULL ,  
            '@sBpdId   VARCHAR  (64) = NULL  
            Dim paramsB(3) As Aurora.Common.Data.Parameter
            paramsB(0) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsB(1) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsB(2) = New Aurora.Common.Data.Parameter("sSapId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsB(3) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, sbpdID, Parameter.ParameterType.AddInParameter)
            tempStrError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_ssTransToFinance", paramsB)

            If tempStrError = "SUCCESS" Then tempStrError = ""
            If Not String.IsNullOrEmpty(tempStrError) Then
                Return "<Root><Error>" & GetErrorTextFromResource(True, "Error", "Application", tempStrError) & "</Error><Msg></Msg></Root>"
            End If

            ''@sBpdId   VARCHAR  (64) = NULL ,  
            ''@sRntId   VARCHAR  (64) = NULL  
            Dim paramsC(1) As Aurora.Common.Data.Parameter
            paramsC(0) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, tempStrBPDID, Parameter.ParameterType.AddInParameter)
            paramsC(1) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            tempStrError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_UpdateRentalIntegrityNo", paramsC)

            Return "<Root>" & GetErrorTextFromResource(False, "", "", "") & "<Msg>" & tempStrMsg & "</Msg>" & "</Root>"

        Catch ex As Exception

            Return "<Root>" & GetErrorTextFromResource(True, "Error", "System", ex.Message) & "</Root>"

        End Try

    End Function
#End Region

#Region "rev:mia 16jan2014-addition of supervisor override password and slot validation"
    Private Shared Sub SaveRentalSlot(sRntId As String, islotId As Integer, sSlotDesc As String)
        Try
            Aurora.Common.Data.ExecuteScalarSP("SaveRentalSlot", sRntId, islotId, sSlotDesc)
        Catch ex As Exception
            Logging.LogError("SaveRentalSlot", ex.Message & ", " & ex.StackTrace)
        End Try
    End Sub

#End Region


End Class

