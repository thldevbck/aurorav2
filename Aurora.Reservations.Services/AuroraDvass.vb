Imports Aurora.Common
Imports Aurora.Common.Data
Imports Aurora.Reservations.Data
Imports COMDVASSMgrLib
Imports System.text
Imports System.Xml

Public Class AuroraDvass

#Region " Variables"
    Const KK_status As String = "KK"
    Const NN_status As String = "NN"

#End Region

#Region " Shoel's Work "



    ' the new class - streamlined and not weirdly esoteric!
    Public Shared Function ModifyRental( _
        ByVal SelectedCountry As Integer, _
        ByVal RentalID As String, _
        ByVal ProductId As String, _
        ByVal CkoDate As Date, _
        ByVal CkoDayPart As Long, _
        ByVal CkiDate As Date, _
        ByVal CkiDayPart As Long, _
        ByVal CkoLocation As String, _
        ByVal CkiLocation As String, _
        ByVal Revenue As Double, _
        ByVal Priority As Double, _
        ByVal VehicleIdList As String, _
        ByVal ForceFlag As Integer) As String


        ' CHECK availability
        Dim sDvassStatusMessage = Nothing
        Dim comDvassStatus As COMDVASSMgrLib.comStatus
        With comDvassStatus
            .status = 0
            .msg = ""
            .detail = 0
        End With

        Try
            If Not IsDVASSAvailable(SelectedCountry, sDvassStatusMessage) Then
                'Bye bye!
                'return failure
                Return "DVASS Unavailable - " & sDvassStatusMessage
                Exit Function
            End If

            ' CONVERT dates to Julian with base 2000-01-01
            Dim comCkoDateTime As COMDVASSMgrLib.comDateTime
            Dim comCkiDateTime As COMDVASSMgrLib.comDateTime

            comCkoDateTime = ConvertDateTimeToDVASSDateTime(CkoDate, CkoDayPart)
            comCkiDateTime = ConvertDateTimeToDVASSDateTime(CkiDate, CkiDayPart)

            ' call dvass!
            Dim objDvass As New COMDVASSMgrLib.CDVASSMgr

            objDvass.ChangeRental( _
                                            SelectedCountry, _
                                            RentalID, _
                                            ProductId, _
                                            CkoLocation, _
                                            comCkoDateTime, _
                                            CkiLocation, _
                                            comCkiDateTime, _
                                            Revenue, _
                                            Priority, _
                                            VehicleIdList, _
                                            ForceFlag, _
                                            comDvassStatus _
                                         )

            ' CHECK status from DVASS
            Return TranslateDvassStatus(comDvassStatus, ForceFlag)
        Catch ex As Exception
            'Logging.LogException("ModifyRental - DVASS Part ", ex)
            Return "DVASS Unavailable - " & ex.Message
        End Try
    End Function

    ''' <summary>
    ''' This Functions checks if DVASS is available or not
    ''' </summary>
    ''' <param name="SelectedCountry">The Country Code : 0 - Aus, 1 - NZ</param>
    ''' <param name="Message">Optional Param That returns the message</param>
    ''' <returns>If DVASS is available</returns>
    ''' <remarks></remarks>
    Public Shared Function IsDVASSAvailable(ByVal SelectedCountry As Integer, Optional ByRef Message As String = "", Optional ByRef DvassStatus As String = "", Optional ByVal usercode As String = "") As Boolean
        Dim dvassReturnStatus As comStatus = Nothing

        Try
            Dim objDvass As New COMDVASSMgrLib.CDVASSMgr

            objDvass.GetStatus(SelectedCountry, 0, 0, 0, 0, dvassReturnStatus)

            Message = dvassReturnStatus.msg
            DvassStatus = TranslateDvassStatus(dvassReturnStatus)

            If dvassReturnStatus.detail <> 0 Then
                ' 900 == not started
                'Logging.LogError("IsDVASSAvailable", "DVASS Returned - STATUS : " & dvassReturnStatus.status & ", DETAIL : " & dvassReturnStatus.detail & ", MSG : " & dvassReturnStatus.msg)
                Return False
            Else
                'valid
                Return True
            End If
        Catch ex As Exception
            'Logging.LogException("IsDVASSAvailable", ex)
            Message = ex.Message
            Return False
        End Try
    End Function

    ''' <summary>
    ''' DVASS uses Julian date format. This function provides the necessary conversion.
    ''' </summary>
    ''' <param name="pDate">The Date</param>
    ''' <param name="pTime">The Time Parameter</param>
    ''' <returns>DVASS Datetime</returns>
    ''' <remarks></remarks>
    Public Shared Function ConvertDateTimeToDVASSDateTime(ByVal pDate As Date, ByVal pTime As Object) As comDateTime

        Dim lngJulian As Long
        Dim lngTimeType As Long
        Dim tmpDateTime As comDateTime

        On Error Resume Next

        ' TRANSLATE or not?
        Select Case UCase(TypeName(pTime))
            Case "LONG"
                lngTimeType = CLng(pTime)
            Case "STRING"
                If UCase(pTime) = "PM" Then
                    lngTimeType = 1
                Else
                    lngTimeType = 0
                End If
            Case Else
                lngTimeType = 0
        End Select

        lngJulian = DateDiff("d", CDate("2000-01-01"), pDate)
        With tmpDateTime
            .d = lngJulian
            If lngTimeType = 0 Then
                .t = 0  ' MID-NIGHT
            Else
                .t = 1  ' MID-DAY
            End If
        End With
        ConvertDateTimeToDVASSDateTime = tmpDateTime
    End Function



    ''' <summary>
    ''' Responsible for taking a DVASS message and translating it to a
    ''' "meaningful" and operator friendly version.
    '''
    ''' DVASS documentation describes the status structure and possible
    ''' results. The primary determinant is the (status) field. This is
    ''' an integer that has one of the following values:
    '''   * 1 => The call completed successfully. The (detail) field may
    '''          need interrogation to see if the outcome was successful.
    '''   * 2 => "soft-fail 1" This means that the call did not succeed,
    '''                        as completing the call would have resulted
    '''                        in either a rental becoming unassigned, or
    '''                        a rule being broken, such as an illegal
    '''                        relocation being required.
    '''   * 3 => "soft-fail 2" The call completed, but the schedule now
    '''                        contains either an unassigned rental, or
    '''                        an illegal relocation or substitution.
    '''   * 4 => "Hard fail"   The call failed because of problems that
    '''                        could not be overcome by "forcing" (refer
    '''                        to DVASS documentation, "force flag").
    '''   * 5 => "Interface error"
    '''                        The called failed because of problems passing
    '''                        a message between the manager component
    '''                        (ComDVASSMgr) and the country specific DVASS
    '''                        component (ComDVASScc where cc is AU or NZ).
    '''   * 9 => "State error" Indicates DVASS has not yet started (not that
    '''                        a diplomatic incident has occurred).
    ''' </summary>
    ''' <param name="Status">The Status from the call</param>
    ''' <param name="ForceFlag">Optional Param indicating forced</param>
    ''' <returns>Dvass status in a more readable form</returns>
    ''' <remarks></remarks>
    Public Shared Function TranslateDvassStatus(ByVal Status As comStatus, Optional ByVal ForceFlag As Long = 0) As String
        Dim strTemp As String
        On Error Resume Next
        strTemp = ""
        With Status
            ' OVERALL success/failure
            Select Case .status
                Case 1      ' SUCCESS
                    strTemp = "SUCCESS"
                Case 2, 3   ' SOFT-failure
                    If ForceFlag = 0 Then
                        strTemp = _
                              "SOFT-FAIL-" & CStr(.status) & _
                              ": detail=" & CStr(.detail) & _
                              ", message=" & .msg
                    End If
                Case 4      ' HARD failure
                    strTemp = _
                          "HARD-FAIL (" & CStr(.status) & _
                          "): detail=" & CStr(.detail) & _
                          ", message=" & .msg
                Case 5      ' INTERFACE error
                    strTemp = _
                          "INTERFACE ERROR (" & CStr(.status) & _
                          "): detail=" & CStr(.detail) & _
                          ", message=" & .msg
                Case 9      ' STATE error
                    strTemp = "RES064/RES064 - DVASS is not yet started."
                Case Else
                    strTemp = _
                          "UNKNOWN STATUS (" & CStr(.status) & _
                          "): detail=" & CStr(.detail) & _
                          ", message=" & .msg
            End Select
        End With ' pStatus
        TranslateDvassStatus = strTemp
        On Error GoTo 0
    End Function





    ''' <summary>
    ''' Appears to return the vehicle id allocated to a rental.
    ''' </summary>
    ''' <param name="RentalID">ID of rental</param>
    ''' <param name="IsRental">Refer DVASS documentation</param>
    ''' <param name="VehicleID">ID of vehicle (returned)</param>
    ''' <param name="DvassStatus">Status of method(returned)</param>
    ''' <param name="SelectedCountry">Country Code</param>
    ''' <param name="DvassMessage">Dvass message (returned)</param>
    ''' <remarks></remarks>
    Public Shared Sub GetUnitNumber _
                ( _
                   ByVal RentalID As String, _
                   ByVal IsRental As Integer, _
                   ByRef VehicleID As String, _
                   ByRef DvassStatus As String, _
                   ByVal SelectedCountry As Integer, _
                   Optional ByRef DvassMessage As String = "" _
                )


        If IsDVASSAvailable(SelectedCountry, DvassMessage, DvassStatus) Then

            Dim objDvass As New COMDVASSMgrLib.CDVASSMgr
            Dim dvassReturnStatus As comStatus
            With dvassReturnStatus
                .status = 0
                .msg = ""
                .detail = 0
            End With

            'call up dvass
            objDvass.UnitQuery(SelectedCountry, RentalID, IsRental, VehicleID, dvassReturnStatus)
            DvassStatus = TranslateDvassStatus(dvassReturnStatus)
            DvassMessage = dvassReturnStatus.msg

        Else
            'dvass is dead!
            'If DvassStatus = 900 Then
            '    DvassStatus = "RES064/RES064"
            '    DvassMessage = "DVASS is not started. Unit number not found."
            '    Exit Sub
            'Else
            '    DvassStatus = "RES065/RES065"
            '    DvassMessage = "DVASS is not available. Unit number not found."
            Exit Sub
            'End If
        End If


    End Sub


    ' Exposes the DVASS AddRental() method. It would seem likely
    ' (needs testing) that this will only be used when a booking is
    ' confirmed in Aurora and the database updated.
    '
    ' The DVASS startup process should load existing rentals straight
    ' from the database. This should be tested.
    '
    ' Parameters:
    ' -----------
    '
    ' +------------------------+------+-------------------------------+
    ' |  Name                  | Type | Description of Use            |
    ' +------------------------+------+-------------------------------+
    ' | RentalID               | IN   | Unique ID of rental to add.   |
    ' | ProductID              | IN   | Unique ID of product being    |
    ' |                        |      | rented.                       |
    ' | CkoLocation            | IN   | Check-OUT location [code].    |
    ' | CkoDate                | IN   | Check-OUT date.               |
    ' | CkoDayPart             | IN   | AM or PM.                     |
    ' | CkiLocation            | IN   | Check-IN location [code].     |
    ' | CkiDate                | IN   | Check-IN date.                |
    ' | CkiDayPart             | IN   | AM or PM.                     |
    ' | Revenue                | IN   | Refer to DVASS documentation. |
    ' | Priority               | IN   | Refer to DVASS documentation. |
    ' | Visibility             | IN   | Refer to DVASS documentation. |
    ' | VehicleIdList          | IN   | Range of vehicles to select   |
    ' |                        |      | from.                         |
    ' | Status                 | OUT  | Status of add rental method.  |
    ' +------------------------+------+-------------------------------+
    '
    Public Shared Sub RentalAdd(ByVal nCountryCode As Integer, _
                         ByVal pRentalID As String, _
                         ByVal pProductId As String, _
                         ByVal pCkoLocation As String, _
                         ByVal pCkoDate As Date, _
                         ByVal pCkoDayPart As Long, _
                         ByVal pCkiLocation As String, _
                         ByVal pCkiDate As Date, _
                         ByVal pCkiDayPart As Long, _
                         ByVal pRevenue As Double, _
                         ByVal pPriority As Double, _
                         ByVal pVisibility As Integer, _
                         ByVal pVehicleIdList As String, _
                         ByVal pForceFlag As Long, _
                         ByRef pDvassStatus As String, _
                               Optional ByVal sUsercode As String = "")

        Dim comDvassStatus As comStatus
        Dim comCkoDateTime As comDateTime
        Dim comCkiDateTime As comDateTime

        Dim objDvass As New COMDVASSMgrLib.CDVASSMgr

        If IsDVASSAvailable(nCountryCode, pDvassStatus) = False Then
            Exit Sub
        Else

            ' CONVERT dates to Julian with base 2000-01-01
            comCkoDateTime = ConvertDateTimeToDVASSDateTime(pCkoDate, pCkoDayPart)
            comCkiDateTime = ConvertDateTimeToDVASSDateTime(pCkiDate, pCkiDayPart)

            ' ASSUME no errors right now (...see what happens)
            With comDvassStatus
                .status = 0
                .msg = ""
                .detail = 0
            End With

            With objDvass

                Call .AddRental(nCountryCode, _
                                pRentalID, _
                                pProductId, _
                                pCkoLocation, _
                                comCkoDateTime, _
                                pCkiLocation, _
                                comCkiDateTime, _
                                pRevenue, _
                                pPriority, _
                                pVehicleIdList, _
                                pForceFlag, _
                                comDvassStatus)

                ' CHECK status from DVASS
                pDvassStatus = TranslateDvassStatus(comDvassStatus)


            End With ' objDvass
        End If
    End Sub


    '  public shared ' Appears to cancel existing rental information from DVASS so as
    ' scheduling can take advantage of additional vehicle(s).
    '
    ' Parameters:
    ' -----------
    '
    ' +------------------------+------+-------------------------------+
    ' |  Name                  | Type | Description of Use            |
    ' +------------------------+------+-------------------------------+
    ' | RentalID               | IN   | Unique ID of rental to cancel.|
    ' | ForceFlag              | IN   | Refer DVASS documentation.    |
    ' | Cost                   | IN   | Refer to DVASS documentation. |
    ' | Status                 | OUT  | Status of method.             |
    ' +------------------------+------+-------------------------------+
    '
    Public Shared Sub CancelRental(ByVal pRentalID As String, ByVal pForceFlag As Long, _
                            ByVal pCost As Double, ByRef pDvassStatus As String, _
                            ByVal pSelectedCountry As Integer, _
                            Optional ByVal sUsercode As String = "")

        Dim comDvassStatus As comStatus
        Dim objDvass As New COMDVASSMgrLib.CDVASSMgr

        If IsDVASSAvailable(pSelectedCountry, "", pDvassStatus) Then
            'dvass working
            With comDvassStatus
                .status = 0
                .msg = ""
                .detail = 0
            End With


            With objDvass
                ' INVOKE "real" method
                Call .CancelRental _
                         ( _
                            pSelectedCountry, _
                            pRentalID, _
                            pForceFlag, _
                            pCost, _
                            comDvassStatus _
                         )

                ' CHECK status from DVASS
                pDvassStatus = TranslateDvassStatus(comDvassStatus)
            End With ' objDvass
            ' Else
            'dvass dead
        End If

    End Sub
#End Region

#Region " Added By Manny in references to the VehicleRequestResultList.aspx"

    Private Const QUERYFAIL_ERROR_DVASS_NOTAVAILABLE = "<Aurora><Error><Description>Query Availability failed: DVASS is not available.</Description></Error></Aurora>"
    Private Const QUERYFAIL_ERROR_VEHICLEREQUEST_NOTAVAILABLE = "<Aurora><Error><Description>Query Availability failed: vehicle Request class is not available.</Description></Error></Aurora>"

    Shared ReadOnly Property ReturnDifferenceInSeconds(ByVal initialseconds As Int32) As Integer
        Get
            Return (DateTime.Now.TimeOfDay.TotalSeconds - initialseconds) / 1000
        End Get
    End Property

    Private Shared mSetSelectedCountry As Integer
    Private Shared sngDvassCallTimer As Single


    Private Shared ReadOnly Property ReturnInitialSeconds() As Int32
        Get
            Return DateTime.Now.TimeOfDay.TotalSeconds
        End Get
    End Property

    Private Shared ReadOnly Property ReturnBytes(ByVal objValue As Object) As Byte()
        Get
            Return System.Text.Encoding.ASCII.GetBytes(objValue)
        End Get
    End Property
    Private Shared ReadOnly Property SelectedCountry() As Integer
        Get
            Return mLngSelectedCountry

        End Get
    End Property

    Private Overloads Shared Function convertDVASSDateToVBDate(ByVal pDate As Long) As Date
        Return DateAdd(DateInterval.Day, pDate, New Date(2000, 1, 1))
    End Function

    Private Shared mLngSelectedCountry As Integer
    Private Shared mStrSelectedCountryCode As String

    Private Shared Function isValidCountry(ByVal pCountryIdentifier As String) As Boolean
        Dim strTemp As String = "#NZ#1#AU#0"

        Try
            ''lets try to convert it to a number
            pCountryIdentifier = Convert.ToInt16(pCountryIdentifier)

        Catch
            ''do nothing..its not number..lets change it to uppercase
            pCountryIdentifier = pCountryIdentifier.ToString.ToUpper

        End Try

        If (strTemp.IndexOf(pCountryIdentifier) = 0) Or (strTemp.IndexOf(pCountryIdentifier) = -1) Then
            Return False
        Else
            Return True
        End If

    End Function

    Public Shared Sub selectCountry(ByVal pCountryIdentifier As String)


        Try

            ''lets try to convert the argument to a number type
            pCountryIdentifier = Convert.ToInt16(pCountryIdentifier)

        Catch

            ''oh..no, it is a string..exit
            If Not IsNumeric(pCountryIdentifier) Then Exit Sub

        End Try



        If Not isValidCountry(pCountryIdentifier.ToString) Then Exit Sub


        mLngSelectedCountry = CInt(pCountryIdentifier)


        ''NOTE: under SELECT..CASE statement of mapCountryIdentifier are combination of strings and numbers ..
        mStrSelectedCountryCode = CStr(mapCountryIdentifier(pCountryIdentifier))

    End Sub

    Private Shared Function mapCountryIdentifier _
         (ByVal pCountryIdentifier As String) As String


        Try

            ''lets try to convert it to a number
            pCountryIdentifier = Convert.ToInt16(pCountryIdentifier)

        Catch
            ''do nothing..its not number..lets change it to uppercase
            pCountryIdentifier = pCountryIdentifier.ToString.ToUpper
        End Try

        Select Case pCountryIdentifier
            Case "NZ"
                mapCountryIdentifier = "1".ToString ''CLng(1)
            Case "1"
                mapCountryIdentifier = "NZ"
            Case "AU"
                mapCountryIdentifier = "0".ToString ''CLng(0)
            Case "0"
                mapCountryIdentifier = "AU"
            Case Else
                mapCountryIdentifier = "-1".ToString ''CLng(-1)
        End Select

        Return mapCountryIdentifier
    End Function

    'Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
    '                                      ByVal sErrNumber As String, _
    '                                      ByVal sErrType As String, _
    '                                      ByVal sErrDescription As String) As String

    '    Dim sXmlString As String
    '    If sErrDescription = "" Then
    '        sErrDescription = "Success"
    '    End If
    '    sXmlString = "<Error>"
    '    sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
    '    sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
    '    sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
    '    sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
    '    Return sXmlString

    'End Function


    Public Shared Function queryRequestVehicleAvailability _
          ( _
           ByVal pVehicleRequestID As String, _
           ByVal pSearchType As String, _
           ByVal pMaximumResultsRequired As Long, _
           ByVal pPriority As Double, _
           ByVal pVisibility As Long, _
           ByRef lngNumberOfVehiclesFound As Long, _
           ByRef strProductId As String, _
           ByRef pAvailableVehicles As String _
          ) As Boolean


        Dim strCkoLocation As String = String.Empty
        Dim dtCkoDate As Date = Nothing
        Dim strCkoAmPm As String = String.Empty
        Dim strCkiLocation As String = String.Empty
        Dim dtCkiDate As Date = Nothing
        Dim strCkiAmPm As String = Nothing
        Dim lngNumberOfVehiclesRequested As Long
        Dim DvassStatus As comStatus = Nothing
        Dim varDvassResults As Object = Nothing
        Dim hasError As Boolean = False

        queryRequestVehicleAvailability = False


        'Dim blnIsDvassAvailable As Boolean = AuroraDvass.IsDVASSAvailable(mLngSelectedCountry)
        'If Not blnIsDvassAvailable Then
        '    pAvailableVehicles = QUERYFAIL_ERROR_DVASS_NOTAVAILABLE
        '    Exit Function
        'End If

        sngDvassCallTimer = ReturnInitialSeconds

        Try

            If (getVehicleRequestDetails _
                     ( _
                        CStr(pVehicleRequestID), _
                        strProductId, _
                        strCkoLocation, _
                        dtCkoDate, _
                        strCkoAmPm, _
                        strCkiLocation, _
                        dtCkiDate, _
                        strCkiAmPm, _
                        lngNumberOfVehiclesRequested _
                     ) = False) Then

                pAvailableVehicles = QUERYFAIL_ERROR_VEHICLEREQUEST_NOTAVAILABLE
                Exit Function

            End If



            If pMaximumResultsRequired < 20 Then pMaximumResultsRequired = 20
            If pMaximumResultsRequired > 20 Then
                ReDim varDvassResults(8, pMaximumResultsRequired - 1)
            End If

            If pVisibility < 1 Then
                pVisibility = 1
            ElseIf pVisibility > 10 Then
                pVisibility = 10
            End If

            If pPriority <= 0 Then pPriority = CDbl(1)

            'REV:MIA NOV.23, 2012 - THIS IS THE ORIGINAL CODES
            If doDvassQueryAvailability _
                        ( _
                           strProductId, _
                           lngNumberOfVehiclesRequested, _
                            "", _
                           strCkoLocation, _
                           strCkiLocation, _
                           dtCkoDate, _
                           IIf(LCase(strCkoAmPm) = "pm", 1, 0), _
                           dtCkiDate, _
                           IIf(LCase(strCkiAmPm) = "pm", 1, 0), _
                           pPriority, _
                           pVisibility, _
                           pSearchType, _
                           pMaximumResultsRequired, _
                           lngNumberOfVehiclesFound, _
                           DvassStatus, _
                           varDvassResults, _
                           pAvailableVehicles _
                        ) = False Then
                Exit Function
            End If

            If (DvassStatus.status <> 0 And DvassStatus.status <> 1) Then
                Logging.LogWarning("queryRequestVehicleAvailability()", "doDvassQueryAvailability() ERROR: dvassStatus.Status: " & DvassStatus.status & " dvassStatus.msg: " & DvassStatus.msg)
                Exit Function
            End If

            'If doDvassQueryAvailability _
            '            ( _
            '               "", _
            '               SelectedCountry, _
            '                strProductId, _
            '                strCkoLocation, _
            '                dtCkoDate, _
            '                strCkiLocation, _
            '                dtCkiDate, _
            '                "", _
            '                "", _
            '                "", _
            '                True, _
            '                pAvailableVehicles
            '            ) = False Then
            '    Exit Function
            'End If

        Catch ex As Exception

            hasError = True

        Finally

            If hasError = True Then
                queryRequestVehicleAvailability = False
            Else
                queryRequestVehicleAvailability = True
            End If

        End Try

        Return queryRequestVehicleAvailability

    End Function


    Private Shared Function doDvassQueryAvailability _
                     ( _
                           ByVal pProduct As String, _
                           ByVal pNumberOfVehiclesRequired As Long, _
                           ByVal pCandidateVehicleList As String, _
                           ByVal pCkoLocation As String, _
                           ByVal pCkiLocation As String, _
                           ByVal pStartDate As Date, _
                           ByVal pStartTime As Long, _
                           ByVal pEndDate As Date, _
                           ByVal pEndTime As Long, _
                           ByVal pPriority As Double, _
                           ByVal pVisibility As Long, _
                           ByVal pAlternativesFlag As String, _
                           ByVal pMaximumResultsRequired As Long, _
                           ByRef pNumberOfResultsReturned As Long, _
                           ByRef pStatus As comStatus, _
                           ByRef pResults As Array, _
                           ByRef pXmlResult As String _
                     ) As Boolean

        doDvassQueryAvailability = False
        Dim objDvass As New CDVASSMgr
        Dim comStartDateTime As comDateTime = Nothing
        Dim comEndDateTime As comDateTime = Nothing

        Dim lngAlternativesFlag As Long = Nothing
        Dim lngResultIndex As Long = Nothing
        Dim bstrProductID() As Byte = Nothing
        Dim bstrCkoLocnID() As Byte = Nothing
        Dim bstrCkiLocnID() As Byte = Nothing
        Dim bstrAlternatives() As Byte = Nothing
        Dim strTemp As String = Nothing

        Dim AvailQueryHasErrors As Boolean = True
        Dim dvassLogMessage As String = ""


        pXmlResult = String.Empty
        If (objDvass Is Nothing) Then

            With pStatus
                .status = 2
                .msg = "DVASS is not started."
                .detail = 900
            End With

            ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
            InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, "DVASS is not started.", "", "", False, "")

            Exit Function
        Else ''ELSE FOR objDvass Is Nothing

            If Not (AuroraDvass.IsDVASSAvailable(mLngSelectedCountry)) Then
                With pStatus
                    .status = 2
                    .msg = "DVASS is not available."
                    .detail = 900
                End With

                ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
                InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, "DVASS is not available.", "", "", False, "")

                Exit Function
            End If ''ENDIF FOR dvassAvailable() = False 



            comStartDateTime = AuroraDvass.ConvertDateTimeToDVASSDateTime(pStartDate, pStartTime)
            comEndDateTime = AuroraDvass.ConvertDateTimeToDVASSDateTime(pEndDate, pEndTime)


            With pStatus
                .status = 0
                .msg = String.Empty
                .detail = 0
            End With

            Select Case (pAlternativesFlag.ToUpper)
                Case "NEVER", "NO ALTERNATIVE"
                    lngAlternativesFlag = 0
                Case "ALWAYS", "ALTERNATIVES"
                    lngAlternativesFlag = 1
                Case "ON NO", "ALTERNATIVES IF NOT AVAILABLE"
                    lngAlternativesFlag = 2
                Case Else
                    lngAlternativesFlag = 0
            End Select


            bstrProductID = ReturnBytes(pProduct)
            bstrCkoLocnID = ReturnBytes(pCkoLocation)
            bstrCkiLocnID = ReturnBytes(pCkiLocation)
            bstrAlternatives = ReturnBytes(pAlternativesFlag)

            If pMaximumResultsRequired < 20 Then pMaximumResultsRequired = 20


        End If ''ENDIF FOR objDvass Is Nothing


        sngDvassCallTimer = ReturnInitialSeconds
        dvassLogMessage = pStatus.msg
        Try

            Dim sbLogString As StringBuilder = New StringBuilder

            Try
                sbLogString.Append("-----------DVASS CALL-----------")
                sbLogString.Append(vbNewLine)
                sbLogString.Append("Calling AvailQuery with the params ")
                sbLogString.Append(vbNewLine)
                sbLogString.Append("icountry = " & SelectedCountry)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("prodid = " & pProduct)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("nvehicles = " & pNumberOfVehiclesRequired)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("candidate_vehicles = " & pCandidateVehicleList)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("locid_fr = " & pCkoLocation)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pdat_fr = " & comStartDateTime.d & " - " & comStartDateTime.t)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("locid_to = " & pCkiLocation)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pdat_to = " & comEndDateTime.d & " - " & comStartDateTime.t)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("priority = " & pPriority)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("vis_flag = " & pVisibility)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("alterns_flag = " & lngAlternativesFlag)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("max_nresults = " & pMaximumResultsRequired)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pnresults = " & pNumberOfResultsReturned)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pstat = " & pStatus.detail & " - " & pStatus.msg & " - " & pStatus.status)
                sbLogString.Append(vbNewLine)
            Catch ex As Exception
            End Try

            Call objDvass.AvailQuery _
                                          ( _
                                              SelectedCountry, _
                                              pProduct, _
                                              pNumberOfVehiclesRequired, _
                                              pCandidateVehicleList, _
                                              pCkoLocation, _
                                              comStartDateTime, _
                                              pCkiLocation, _
                                              comEndDateTime, _
                                              pPriority, _
                                              pVisibility, _
                                              lngAlternativesFlag, _
                                              pMaximumResultsRequired, _
                                              pNumberOfResultsReturned, _
                                              pStatus, _
                                              pResults _
                                          )
            Try
                sbLogString.Append(vbNewLine)
                sbLogString.Append("This is the data after call was completed ")
                sbLogString.Append(vbNewLine)
                sbLogString.Append("icountry = " & SelectedCountry)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("prodid = " & pProduct)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("nvehicles = " & pNumberOfVehiclesRequired)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("candidate_vehicles = " & pCandidateVehicleList)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("locid_fr = " & pCkoLocation)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pdat_fr = " & comStartDateTime.d & " - " & comStartDateTime.t)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("locid_to = " & pCkiLocation)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pdat_to = " & comEndDateTime.d & " - " & comStartDateTime.t)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("priority = " & pPriority)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("vis_flag = " & pVisibility)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("alterns_flag = " & lngAlternativesFlag)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("max_nresults = " & pMaximumResultsRequired)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pnresults = " & pNumberOfResultsReturned)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pstat = " & pStatus.detail & " - " & pStatus.msg & " - " & pStatus.status)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("pres = " & pResults.ToString)
                sbLogString.Append(vbNewLine)
                sbLogString.Append("-----------DVASS CALL-----------")

                Logging.LogInformation("DVASSCALL", sbLogString.ToString())
            Catch ex As Exception
            End Try



            AvailQueryHasErrors = False

            ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
            If (pStatus.status <> 0 And pStatus.status <> 1) Then
                InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, "", "", False, "")
            End If

        Catch ex As Exception
            Logging.LogException("AvailQuery() FAILED.", ex)
            ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
            InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, "", ex.Message & "," & ex.StackTrace, False, "")
            Exit Function
        End Try

        If AvailQueryHasErrors = False Then
            sngDvassCallTimer = ReturnDifferenceInSeconds(sngDvassCallTimer)
            For lngResultIndex = 0 To pNumberOfResultsReturned - 1
                strTemp = "<AvailableVehicle><PrdId>" & CStr(pResults(0, lngResultIndex)) & _
                          "</PrdId><CkoLocation>" & CStr(pResults(1, lngResultIndex)) & _
                          "</CkoLocation><CkoDate>" & convertDVASSDateToVBDate(CLng(pResults(2, lngResultIndex))) & _
                          "</CkoDate><CkiLocation>" & CStr(pResults(3, lngResultIndex)) & _
                          "</CkiLocation><CkiDate>" & convertDVASSDateToVBDate(CLng(pResults(4, lngResultIndex))) & _
                          "</CkiDate><Preference>" & CStr(pResults(5, lngResultIndex)) & _
                          "</Preference><Cost>" & CStr(pResults(6, lngResultIndex)) & _
                          "</Cost><NumberOfVehicles>" & CStr(pResults(7, lngResultIndex)) & _
                          "</NumberOfVehicles><ReducesRelocationCount>" & CStr(pResults(8, lngResultIndex)) & _
                          "</ReducesRelocationCount></AvailableVehicle>"
                pXmlResult &= strTemp
            Next

            pXmlResult = String.Concat("<Data>", pXmlResult, "</Data>")
            Logging.LogInformation("DoDvassQueryAvailability : pxmlresult values: ", pXmlResult)
        End If ''ENDIF FOR AvailQueryHasErrors=FALSE


        ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
        dvassLogMessage = pStatus.msg
        Dim NumberOfVehicles As String = -1
        Try
            Dim xmlresult As New XmlDocument
            xmlresult.LoadXml(pXmlResult.Trim)
            NumberOfVehicles = xmlresult.DocumentElement.SelectSingleNode("/Data/AvailableVehicle/NumberOfVehicles").InnerText
            If (String.IsNullOrEmpty(NumberOfVehicles)) Then
                InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, pXmlResult, "", False, "")
            Else
                If (CInt(NumberOfVehicles) <= 0) Then
                    InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, pXmlResult, "", False, "")
                Else
                    Dim LogOutputOfSuccessfullAvailability As Boolean = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("LogOutputOfSuccessfullAvailability"))
                    If (LogOutputOfSuccessfullAvailability) Then
                        InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, pXmlResult, "", True, "")
                    End If
                End If
            End If
            xmlresult = Nothing
        Catch ex As Exception
            If (CInt(NumberOfVehicles) <= 0) Then
                InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, pXmlResult, ex.Message & "," & ex.StackTrace, False, "")
            Else
                InsertDvassNonAvailabilityLogging(pStartDate, pEndDate, pCkoLocation, pCkiLocation, pProduct, dvassLogMessage, pXmlResult, ex.Message & "," & ex.StackTrace, True, "")
            End If

        End Try


        doDvassQueryAvailability = True

    End Function

    Public Shared Function setSelectedCountry(ByVal lngCountry As Integer) As Int16
        mLngSelectedCountry = lngCountry
    End Function

    Public Shared Function getVehicleRequestDetails _
             ( _
                ByVal pVehicleRequestID As String, _
                ByRef pProductId As String, _
                ByRef pCkoLocation As String, _
                ByRef pCkoDate As Date, _
                ByRef pCkoAmPm As String, _
                ByRef pCkiLocation As String, _
                ByRef pCkiDate As Date, _
                ByRef pCkiAmPm As String, _
                ByRef pNumberOfVehiclesRequested As Long _
             ) _
             As Boolean


        getVehicleRequestDetails = False

        Dim strXmlVehicleRequest As String = Nothing
        Dim objXml As New Xml.XmlDocument
        Dim objXmlNode As Xml.XmlNode = Nothing
        Dim objVehicleRequest As New VehicleRequest



        Try

            Call VehicleRequest.getVehicleRequest(pVehicleRequestID, strXmlVehicleRequest)


            If (strXmlVehicleRequest.Length = 0) Then Exit Function

            objXml.LoadXml(strXmlVehicleRequest)
            If objXml Is Nothing Then Exit Function


            If objXml.DocumentElement.ChildNodes.Count > 0 Then


                objXmlNode = objXml.SelectSingleNode("/Data/VhrPrdId")
                If (objXmlNode Is Nothing) Then

                    Throw New Exception

                Else
                    pProductId = (objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                ''---------------------------------------------------
                '' NUMBER OF VEHICLES REQUESTED
                objXmlNode = objXml.SelectSingleNode("/Data/VhrNumOfVehicles")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pNumberOfVehiclesRequested = (objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                ''---------------------------------------------------
                '' CHECK-OUT...(Location)
                objXmlNode = objXml.SelectSingleNode("/Data/VhrCkoLocCode")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pCkoLocation = (objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                ''---------------------------------------------------
                '' CHECK-OUT...(Date)
                objXmlNode = objXml.SelectSingleNode("/Data/VhrCkoDate")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pCkoDate = Convert.ToDateTime(objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                ''---------------------------------------------------
                '' CHECK-OUT...(AM/PM)
                objXmlNode = objXml.SelectSingleNode("/Data/VhrCkoAmPm")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pCkoAmPm = (objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                ''---------------------------------------------------
                '' CHECK-in...(Location)
                objXmlNode = objXml.SelectSingleNode("/Data/VhrCkiLocCode")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pCkiLocation = (objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                ''---------------------------------------------------
                '' CHECK-OUT...(Date)
                objXmlNode = objXml.SelectSingleNode("/Data/VhrCkiDate")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pCkiDate = Convert.ToDateTime(objXmlNode.InnerText)
                End If
                ''---------------------------------------------------


                '' CHECK-OUT...(AM/PM)
                objXmlNode = objXml.SelectSingleNode("/Data/VhrCkiAmPm")
                If (objXmlNode Is Nothing) Then
                    Throw New Exception
                Else
                    pCkiAmPm = (objXmlNode.InnerText)
                End If

            Else

                Return True

            End If ''--objXml.DocumentElement.ChildNodes.Count


        Catch ex As Exception

            Return True

        Finally

            getVehicleRequestDetails = True

        End Try

        Return getVehicleRequestDetails

    End Function

#End Region

#Region " Added by Manny in reference to the SelectedVehicleList.aspx"

    Private Shared mOutputparameter As String
    Public Shared Property OutputParameter() As String
        Get
            Return mOutputparameter
        End Get
        Set(ByVal value As String)
            mOutputparameter = value
        End Set
    End Property

    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                            ByVal sErrNumber As String, _
                                            ByVal sErrType As String, _
                                            ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        '@sCode		varchar(64) = default,
        '@param1		varchar(64) = '',
        '@param2		varchar(64) = '',
        '@param3		varchar(64) = '',
        '@param4		varchar(64) = '',
        '@oparam1	varchar(8000) OUTPUT

        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
        'Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("sp_get_ErrorString", params)
        'If Not String.IsNullOrEmpty(OutputParameter) Then
        '    Return xmlstring
        'Else
        '    Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", xmlstring)
        'End If
        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)

    End Function

    Public Shared Function AddRentalProc(ByVal sXmlSelectedVehicle As String, _
                                  ByVal sCtyCode As String, _
                                  Optional ByVal sSaveNext As Boolean = False) As String

        AddRentalProc = Nothing


        Dim sError As String = Nothing
        Dim sErrorNo As String = Nothing

        Dim sBkrId As String = Nothing
        Dim xmlData As String = Nothing

        Dim sXmlString As String = Nothing
        Dim sErrorString As String = Nothing

        Dim sQueryString As String
        Dim osError As String = Nothing

        Dim pSlvId As String = Nothing
        Dim DvassNotifiedError As String = Nothing

        Dim pStatus As String = Nothing
        Dim sBooId As String = Nothing

        Dim sNewBooNum As String = Nothing
        Dim pCtyCode As String = Nothing

        Dim pDvassStatus As String = Nothing
        Dim pVehicleList As String = Nothing

        Dim pRentalID As String = Nothing
        Dim pProductId As String = Nothing

        Dim pCkoLocation As String = Nothing
        Dim pCkiLocation As String = Nothing

        Dim xmlBookingId As String = Nothing
        Dim sPrgName As String = Nothing

        Dim sUsercode As String = Nothing
        Dim isRentalAvailable As String = Nothing


        Dim pCkoDate As Date = Nothing
        Dim pCkiDate As Date = Nothing


        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim pVisibility As Integer = 0

        Dim pRevenue As Double = 0
        Dim pPriority As Double = 0
        Dim pCost As Double = 0

        Dim pForceFlag As Int64
        Dim tmpIdentity As Int64
        Dim pCkiDayPart As Int64
        Dim pCkoDayPart As Int64


        Dim pIsDvassNotified As Boolean
        Dim pIsThriftyUpdateDvass As Boolean


        Dim oSelVehXmlDom As New Xml.XmlDocument
        Dim tmpXmlDoc As New Xml.XmlDocument
        Dim nodeList As Xml.XmlNodeList = Nothing
        Dim node As Xml.XmlNode = Nothing


        Dim haserror As Boolean = True
        Dim isvalidxml As Boolean = False

        Dim countrycode As Integer
        Dim strTemp As New StringBuilder



        oSelVehXmlDom.LoadXml(sXmlSelectedVehicle)
        If oSelVehXmlDom Is Nothing Then
            Logging.LogError("AddRentalProc", "Transaction ABORTED")
            Return getMessageFromDB("GEN023")
        End If


        isvalidxml = True : haserror = False

        ''rev:mia May 18 2011 - added email address
        Dim emailaddress As String = ""


        ''rev:mia July 4 2011 - added 3rd party reference field
        Dim thirdPartyReference As String = ""
        Try

            tmpXmlDoc.LoadXml(sXmlSelectedVehicle)

            With oSelVehXmlDom

                sBkrId = .DocumentElement.SelectSingleNode("//Root/Header/@id").InnerText
                sBooId = .DocumentElement.SelectSingleNode("//Root/BooId").InnerText
                sNewBooNum = .DocumentElement.SelectSingleNode("//Root/NewBooNum").InnerText
                sUsercode = .DocumentElement.SelectSingleNode("//Root/UserCode").InnerText
                sPrgName = .DocumentElement.SelectSingleNode("//Root/PrgName").InnerText

                ''rev:mia May 18 2011 - added email address
                If Not (.DocumentElement.SelectSingleNode("//Root/EmailAddress")) Is Nothing Then
                    emailaddress = .DocumentElement.SelectSingleNode("//Root/EmailAddress").InnerText
                End If
            End With

            Logging.LogInformation("AddRentalProc: Check email: ", emailaddress)
            Logging.LogInformation("AddRentalProc", "Started")

            Call updateSelectedVehicle(oSelVehXmlDom.OuterXml, sUsercode, sPrgName, osError)



            If Not osError.Equals("SUCCESS") Then
                Dim strTempA As String = oSelVehXmlDom.SelectSingleNode("//Root/Header").OuterXml
                Dim strTempB As String = oSelVehXmlDom.SelectSingleNode("//Root/SelectedVehicles").OuterXml
                Logging.LogError("AddRentalProc", String.Concat(sUsercode, " - Error In updateSelectedVehicle()", "- Transaction ABORTED AddRentalProcedure() Error Section"))
                sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", osError)
                Return String.Concat("<Root>", sErrorString, "</Root>")
            End If


            sQueryString = "RES_SummaryOfChosenVehicles '" & sBkrId & "', '" & sBooId & "', '', ''"

            Logging.LogInformation("AddRentalProc", "Calling " & sQueryString)
            'xmlData = Aurora.Common.Data.ExecuteScalarSP("RES_SummaryOfChosenVehicles", sBkrId, sBooId, Nothing, Nothing)

            '@psBkrId		VARCHAR(64),
            '@psBooId		VARCHAR(64) = NULL,
            '@psBpdId		VARCHAR(64) = NULL,
            '@psNextScreen	VARCHAR(64)
            Dim xmldoc As XmlDocument
            xmldoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_SummaryOfChosenVehicles", sBkrId, sBooId, Nothing, Nothing)
            xmlData = xmldoc.OuterXml
            xmlData = xmlData.Replace("<data>", "")
            xmlData = xmlData.Replace("</data>", "")
            oSelVehXmlDom.LoadXml(String.Concat("<Root>", xmlData, "</Root>"))


            For i = 0 To oSelVehXmlDom.SelectNodes("//Root/SelectedVehicles/SelectedVehicle").Count - 1
                For j = 0 To tmpXmlDoc.SelectNodes("//Root/SelectedVehicles/SelectedVehicle").Count - 1
                    If oSelVehXmlDom.SelectSingleNode("//Root/SelectedVehicles").ChildNodes(i).SelectSingleNode("SlvId").InnerText = tmpXmlDoc.SelectSingleNode("//Root/SelectedVehicles").ChildNodes(j).SelectSingleNode("SlvId").InnerText AndAlso _
                        tmpXmlDoc.SelectSingleNode("//Root/SelectedVehicles").ChildNodes(j).SelectSingleNode("VehicleRanges").InnerText.ToString.Equals(String.Empty) = False Then

                        oSelVehXmlDom.SelectSingleNode("//Root/SelectedVehicles").ChildNodes(i).SelectSingleNode("VehicleRanges").InnerText = _
                        tmpXmlDoc.SelectSingleNode("//Root/SelectedVehicles").ChildNodes(j).SelectSingleNode("VehicleRanges").InnerText.ToString

                    End If
                Next
            Next

            nodeList = oSelVehXmlDom.DocumentElement.SelectSingleNode("//Root/SelectedVehicles").ChildNodes


            Logging.LogInformation("AddRentalProc", String.Concat(sUsercode, " - Start Of DVASS Call"))

            For Each node In nodeList

                Try
                    ''rev:mia July 4 2011 - added 3rd party reference field
                    If Not node.SelectSingleNode("ThirdPartyRef") Is Nothing Then
                        thirdPartyReference = node.SelectSingleNode("ThirdPartyRef").InnerText
                    End If

                Catch ex As Exception
                End Try

                pSlvId = node.SelectSingleNode("SlvId").InnerText.ToString

                pRentalID = CStr(IIf((node.SelectSingleNode("RentalId").InnerText.ToString.Trim.Length) = 0, _
                            tmpIdentity, node.SelectSingleNode("RentalId").InnerText.ToString))

                pProductId = node.SelectSingleNode("PrdId").InnerText.ToString.Trim
                pCkoLocation = node.SelectSingleNode("CoLoc").InnerText.ToString.Trim
                pCkoDate = node.SelectSingleNode("CoDate").InnerText.ToString.Trim

                pCkoDayPart = CInt(IIf(node.SelectSingleNode("CkoDayPart").InnerText.ToString.Equals("AM") = True, 0, 1))
                pStatus = node.SelectSingleNode("Status").InnerText.ToString.Trim
                pCkiLocation = node.SelectSingleNode("CiLoc").InnerText.ToString.Trim
                pCkiDate = node.SelectSingleNode("CiDate").InnerText.ToString.Trim
                pCkiDayPart = CInt(IIf(node.SelectSingleNode("CkiDayPart").InnerText.ToString.Equals("AM"), 0, 1))
                pRevenue = CDbl(IIf(Len(node.SelectSingleNode("Revenue").InnerText.ToString.Trim) = 0, 0, node.SelectSingleNode("Revenue").InnerText.ToString.Trim))
                pVisibility = CInt(IIf((Len(node.SelectSingleNode("Visibility").InnerText.ToString.Trim) = 0), 10, node.SelectSingleNode("Visibility").InnerText.ToString.Trim))
                pPriority = IIf(Len(node.SelectSingleNode("Priority").InnerText.ToString.Trim) = 0, 1, node.SelectSingleNode("Priority").InnerText.ToString.Trim)
                pVehicleList = node.SelectSingleNode("VehicleRanges").InnerText.ToString.Trim
                pForceFlag = CLng(IIf(Len(node.SelectSingleNode("Force").InnerText.ToString.Trim) = 0, 0, node.SelectSingleNode("Force").InnerText.ToString.Trim))
                pIsDvassNotified = node.SelectSingleNode("DvassNotified").InnerText.ToString.Trim
                pIsThriftyUpdateDvass = node.SelectSingleNode("IsThriftyUpdateDvass").InnerText.ToString.Trim
                pCtyCode = node.SelectSingleNode("CtyCode").InnerText.ToString.Trim

                If (pCtyCode.ToUpper.Equals("AU")) Then
                    Call selectCountry(0)
                    countrycode = 0
                ElseIf (pCtyCode.ToUpper.Equals("NZ")) Then
                    Call selectCountry(1)
                    countrycode = 1
                End If

                If (pIsThriftyUpdateDvass) Then
                    If (pStatus.Equals(KK_status) Or pStatus.Equals(NN_status)) Then

                        If Not (pIsDvassNotified) Then


                            Call RentalAdd(countrycode, _
                               pRentalID, _
                               pProductId, _
                               pCkoLocation, _
                               pCkoDate, _
                               pCkoDayPart, _
                               pCkiLocation, _
                               pCkiDate, _
                               pCkiDayPart, _
                               pRevenue, _
                               pPriority, _
                               pVisibility, _
                               pVehicleList, _
                               pForceFlag, _
                               pDvassStatus, _
                               sUsercode)


                            If Not pDvassStatus.Equals("SUCCESS") Then

                                If Not (pForceFlag = 1 AndAlso _
                                    pDvassStatus.IndexOf("SOFT-FAIL") <> 0) Then
                                    Logging.LogError("AddRentalProc", String.Concat(sUsercode, " - error in rentalAdd() = ", pDvassStatus))
                                    sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", pDvassStatus)
                                    Return String.Concat("<Root>", sErrorString, "<Id></Id><Data>", xmlData, "</Data></Root>")
                                End If ''END IF FOR pForceFlag
                            End If ''END IF FOR pDvassStatus

                            '@sSlvID VARCHAR(64),
                            '@sIsDvassNotified BIT

                            DvassNotifiedError = Aurora.Common.Data.ExecuteScalarSP("RES_DvassNotified", pSlvId, 1)
                        End If ''END IF FOR pIsDvassNotified

                    Else

                        ''ELSE FOR pStatus.Equals
                        pForceFlag = 1 : pCost = 0

                        If (pIsDvassNotified) Then


                            Logging.LogInformation("AddRentalProc", "Calling rentalCancel")
                            Call rentalCancel( _
                                      pRentalID, _
                                      pForceFlag, _
                                      pCost, _
                                      pDvassStatus, _
                                      sUsercode)


                            If Not pDvassStatus.Equals("SUCCESS") AndAlso (pDvassStatus.IndexOf("333") = 0) Then
                                sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", pDvassStatus & " -> rentalCancel")
                                Logging.LogError("AddRentalProc", "Transaction ABORTED AddRentalProcedure() Error Section")
                                Return String.Concat("<Root>", sErrorString, "<Id></Id><Data>", xmlData, "</Data></Root>")
                            End If ''END IF FOR pDvassStatus.Equals("SUCCESS")


                            Logging.LogInformation("AddRentalProc", "Start of Setting Notification Flag")

                            '@sSlvID VARCHAR(64),
                            '@sIsDvassNotified BIT
                            DvassNotifiedError = Aurora.Common.Data.ExecuteScalarSP("RES_DvassNotified", pSlvId, 0)

                        End If ''END IF FOR pIsDvassNotified

                    End If ''END IF FOR pStatus.Equals

                End If ''END IF FOR pIsThriftyUpdateDvass

            Next ''end for next


            Logging.LogInformation("AddRentalProc", "End Of DVASS Call")

            xmlBookingId = String.Empty

            If (sSaveNext) Then


                Logging.LogInformation("AddRentalProc", "Start of ManageCreateBookingFromRequest")
                Logging.LogInformation("sXmlSelectedVehicle - MANNY", sXmlSelectedVehicle)
                ''rev:mia July 4 2011 - added 3rd party reference field
                Dim blnManageCreateBookingFromRequest As Boolean
                blnManageCreateBookingFromRequest = _
                                ManageCreateBookingFromRequest( _
                                          sBkrId, _
                                          sBooId, _
                                          sNewBooNum, _
                                          sUsercode, _
                                          sPrgName, _
                                          osError, _
                                          emailaddress, thirdPartyReference)


                If Not (blnManageCreateBookingFromRequest) Then

                    strTemp = Nothing : strTemp = New StringBuilder
                    With strTemp
                        .Append(sUsercode & " - Error in ManageCreateBookingFromRequest = " & osError & vbCrLf)
                        .Append(sUsercode & " - Transaction ABORTED AddRentalProcedure() Error Section" & vbCrLf)
                    End With
                    Logging.LogError("ManageCreateBookingFromRequest", strTemp.ToString)

                    sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", osError & " -> ManageCreateBookingFromRequest")

                    Return String.Concat("<Root>", sErrorString, "<Id>", xmlBookingId, "</Id><Data>", xmlData, "</Data></Root>")

                Else ''ELSE IF FOR blnManageCreateBookingFromRequest

                    '@Bkrid 		VARCHAR(64) = null,
                    '@BooId		VARCHAR(64) = null,
                    '@NewBooNum	VARCHAR(10) = ''
                    xmlBookingId = Aurora.Common.Data.ExecuteScalarSP("RES_getBookingId", sBkrId, sBooId, sNewBooNum)

                End If ''END IF FOR blnManageCreateBookingFromRequest


                Logging.LogInformation("ManageCreateBookingFromRequest", "End")

            End If ''END IF FOR sSaveNext

            sXmlString = getMessageFromDB("GEN046")
            sErrorString = GetErrorTextFromResource(False, "GEN046", "Application", sXmlString)

            AddRentalProc = String.Concat("<Root>", sErrorString, "<Id>" & xmlBookingId, "</Id><Data>", xmlData, "</Data></Root>")
            Logging.LogInformation("AddRentalProc", "End")


        Catch ex As Exception

            haserror = True
            Logging.LogError("AddRentalProc", "Error " & ex.Message & "," & ex.Source & ", " & ex.StackTrace)

        End Try

        Return AddRentalProc

    End Function

    Private Shared Function CheckParameters(ByVal ParamArray param As String()) As Boolean
        Dim paramstring As String
        For Each paramstring In param
            paramstring = paramstring.Trim
            If (paramstring.Length = 0) Then
                Return False
            End If
            If (paramstring Is Nothing) Then
                Return False
            End If
            If (paramstring = "") Then
                Return False
            End If
            If (paramstring Is String.Empty) Then
                Return False
            End If
        Next
        Return True
    End Function

    Private Function UpdateSelectedAvailVehicle( _
                                              ByVal sVhrId As String, _
                                              ByVal sPrdId As String, _
                                              ByVal iAvailFromDVASS As String, _
                                              ByRef varErrorString As String) As Boolean



        UpdateSelectedAvailVehicle = True


        If CheckParameters(sVhrId, sPrdId) = False Then
            varErrorString = "UpdateSelectedAvailVehicle(), Invalid parameter"
            Logging.LogError("UpdateSelectedAvailVehicle", "Invalid parameter")
            Return False
        End If


        Try

            varErrorString = Aurora.Common.Data.ExecuteScalarSP("RES_UpdateAvailField", sVhrId, sPrdId, CInt(iAvailFromDVASS))

            If varErrorString.ToString.IndexOf("GEN045") < 1 Then
                Return False
            Else
                varErrorString = String.Empty
                UpdateSelectedAvailVehicle = True
            End If

        Catch ex As Exception

            varErrorString = ex.Message
            Logging.LogError("UpdateSelectedAvailVehicle", varErrorString)
            Return False

        End Try

        Return True

    End Function

    Public Shared Function updateSelectedVehicle(ByVal sNode As String, _
                                         ByVal sUsercode As String, _
                                         ByVal sPrgName As String, _
                                         ByRef osError As String) As String

        Dim sXmlString As String = String.Empty
        Dim sErrNo As String = String.Empty
        Dim sErrDesc As String = String.Empty
        Dim xsrpIds As String() = Nothing


        Logging.LogInformation("updateSelectedVehicle", String.Concat(sUsercode, " - Calling RES_ManageSelectedVehicles sNode", "', '", sUsercode, "','", sPrgName, "', ''"))

        sXmlString = Aurora.Common.Data.ExecuteScalarSP("RES_ManageSelectedVehicles", sNode, sUsercode, sPrgName, "")

        Logging.LogInformation("RESULT updateSelectedVehicle", sXmlString)

        If Not String.IsNullOrEmpty(sXmlString) Then
            If Not (sXmlString.Equals("SUCCESS")) Then
                xsrpIds = (sXmlString.Split("/"))
                sErrNo = xsrpIds(0)
                sErrDesc = xsrpIds(1)
                osError = sErrDesc
                Logging.LogError("updateSelectedVehicle", String.Concat(sUsercode, " - Error in RES_ManageSelectedVehicles = ", osError))

            Else
                osError = "SUCCESS"
                Logging.LogInformation("updateSelectedVehicle", String.Concat(sUsercode, String.Concat(sUsercode, " - RES_ManageSelectedVehicles = ", osError)))
            End If

        Else
            osError = "SUCCESS"
            Logging.LogInformation("updateSelectedVehicle", String.Concat(sUsercode, String.Concat(sUsercode, " - RES_ManageSelectedVehicles = ", osError)))
        End If


        Return sXmlString

    End Function

    Public Shared Sub rentalCancel( _
                          ByVal pRentalID As String, _
                          ByVal pForceFlag As Long, _
                          ByVal pCost As Double, _
                          ByRef pDvassStatus As String, _
                          Optional ByVal sUsercode As String = "" _
                          )

        Dim objDvass As New CDVASSMgr
        Dim comDvassStatus As comStatus = Nothing
        Dim bstrRentalID() As Byte = Nothing
        Dim strTemp As String = Nothing

        Dim CancelRentalHasError As Boolean = True

        Dim blnDVASS As Boolean = IsDVASSAvailable(mLngSelectedCountry, sUsercode)

        If Not (blnDVASS) Then
            Logging.LogError("RentalCancel", "DVASS is not started")
            pDvassStatus = "RES064/RES064 - DVASS is not started. Rental not cancelled."
            Exit Sub

        Else ''ELSEIF FOR dvassIsAvailable(sUsercode)

            If Not (blnDVASS) Then
                Logging.LogError("RentalCancel", "DVASS is not started")
                pDvassStatus = "RES065/RES065 - DVASS is not available. Rental not cancelled."
                Exit Sub
            End If


            bstrRentalID = ReturnBytes(pRentalID)

            With comDvassStatus
                .status = 0
                .msg = String.Empty
                .detail = 0
            End With

            With objDvass

                sngDvassCallTimer = ReturnInitialSeconds

                Try
                    Call .CancelRental _
                                                 ( _
                                                     SelectedCountry, _
                                                     pRentalID, _
                                                     pForceFlag, _
                                                     pCost, _
                                                     comDvassStatus _
                                                 )


                    CancelRentalHasError = False


                Catch ex As Exception
                    strTemp = String.Concat("<Errors>", makeXmlErrorDocument("AuroraDvass.rentalCancel", comDvassStatus, ex), "</Errors>")
                    Logging.LogError("RentalCancel", "rentalCancel() Failed " & strTemp)
                End Try

            End With


            If CancelRentalHasError = False Then
                pDvassStatus = TranslateDvassStatus(comDvassStatus)
                sngDvassCallTimer = ReturnDifferenceInSeconds(sngDvassCallTimer)
                Logging.LogInformation("RentalCancel", String.Concat(sUsercode, " - rentalCancel() Time elapsed = " & sngDvassCallTimer.ToString("N")))
                Logging.LogInformation("RentalCancel", String.Concat(sUsercode, " - rentalCancel() status = " & pDvassStatus & ", detail=" & (comDvassStatus.detail.ToString) & ", status=" & (comDvassStatus.status.ToString) & ", msg=" & comDvassStatus.msg))
            End If

        End If ''ENDIF FOR dvassIsAvailable(sUsercode)


    End Sub

    Private Shared Function makeXmlErrorDocument _
              ( _
                     ByVal pApplicationMessage As String, _
                     ByVal pDvassStatus As comStatus, _
                     Optional ByVal pError As Object = Nothing _
               ) As String

        Dim strTemp As String = String.Empty

        If pError Is Nothing Then
            strTemp = String.Concat("<Code /><Description /><Source /><Application>", pApplicationMessage, "</Application>")
        Else

            ''NOTE: since we are anticipating errors object,I put another Exception Object so just incase
            ''      an error is thrown with this type, we can catch it.
            ''
            Select Case pError
                Case TypeOf (pError) Is Exception

                    pError = CType(pError, Exception)

                    ''we cant catch any number of an exception in CLR, so what will i do
                    ''is to put a default enumeration type for this element
                    ''having a default value of 9999

                    strTemp = "<Code>" & "Error" & "</Code><Description>" & pError.message.ToString & "</Description><Source>" & pError.Source & "</Source><Application>" & pApplicationMessage & "</Application>"

                Case TypeOf (pError) Is ErrObject

                    pError = CType(pError, ErrObject)
                    strTemp = "<Code>" & CStr(pError.Number) & "</Code><Description>" & pError.Description & "</Description><Source>" & pError.Source & "</Source><Application>" & pApplicationMessage & "</Application>"

            End Select

        End If

        strTemp &= String.Concat("<DVASSStatus><Detail>", pDvassStatus.detail.ToString & "</Detail><Status>" & pDvassStatus.status.ToString & "</Status><Msg>", pDvassStatus.msg.ToString, "</Msg></DVASSStatus>")

        Return String.Concat("<Error>", strTemp, "</Error>")
    End Function

    ''reV:mia May 18 2011 addred emailaddress
    ''REV:MIA july 3,2012 -  addition of 3rd party reference
    Private Shared Function ManageCreateBookingFromRequest( _
                                                       ByVal sBkrId As String, _
                                                       ByVal sBooId As String, _
                                                       ByVal sNewBooNum As String, _
                                                       ByVal sUsercode As String, _
                                                       ByVal sPrgName As String, _
                                                       ByRef varErrorString As String, _
                                                       Optional ByVal emailaddress As String = "", _
                                                       Optional ByVal ThirdPartyRefNum As String = "") As Boolean


        ManageCreateBookingFromRequest = True

        Logging.LogInformation("ManageCreateBookingFromRequest", "Start")

        If CheckParameters(sBkrId, sUsercode) = False Then
            Logging.LogError("ManageCreateBookingFromRequest", "Invalid parameter")
            varErrorString = "getBookingNumber(), Invalid parameter"
            Return False
        End If


        Logging.LogInformation("ManageCreateBookingFromRequest", String.Concat(sUsercode, " - Calling RES_ManageCreateBookingFromRequest '" & sBkrId & "','" & sBooId & "','" & sUsercode & "','" & sPrgName & "'"))
        Try

            ''rev:mia sept18
            ''rev:mia may 19 2011 

            Dim spname As String = "RES_ManageCreateBookingFromRequest_OSLO"
            ''note: check the RES_ManageCreateCustomerFromRequest_EmailAddress called inside the RES_ManageCreateBookingFromRequest_OSLO_EmailAddress
            ''REV:MIA july 3,2012 -  addition of 3rd party reference
            varErrorString = Aurora.Common.Data.ExecuteScalarSP(spname, sBkrId, sBooId, sNewBooNum, sUsercode, sPrgName, emailaddress, ThirdPartyRefNum)


            If Not String.IsNullOrEmpty(varErrorString) Then
                If Not (varErrorString.TrimEnd.Equals("SUCCESS")) Then
                    Logging.LogError("ManageCreateBookingFromRequest", String.Concat(sUsercode, " - error in RES_ManageCreateBookingFromRequest = ", varErrorString))
                    ManageCreateBookingFromRequest = False
                End If
            Else

                Logging.LogInformation("ManageCreateBookingFromRequest", String.Concat(sUsercode, " -  RES_ManageCreateBookingFromRequest = ", varErrorString))
                ManageCreateBookingFromRequest = True
                varErrorString = String.Empty

            End If '�ND IF FOR (varErrorString.ToString.Trim)





        Catch ex As Exception

            varErrorString = "Error #" & ex.Message

            Logging.LogError("ManageCreateBookingFromRequest", varErrorString)
            ManageCreateBookingFromRequest = False

        End Try

        Return ManageCreateBookingFromRequest


    End Function

#End Region

#Region " Added by Manny in reference to BookedProductDetails.aspx"

    Public Shared Function WrapFunFordoDvassQueryAvailability( _
                          ByVal pProduct As String, _
                          ByVal pCkoLocation As String, _
                          ByVal pCkiLocation As String, _
                          ByVal pStartDate As Date, _
                          ByVal pStartTime As Long, _
                          ByVal pEndDate As Date, _
                          ByVal pEndTime As Long, _
                          ByRef pAvailableVehicles As String, _
                          ByRef pNumberOfResultsReturned As Long _
                           ) As Boolean


        Dim pStatus As comStatus = Nothing
        Dim pResults As Array = Nothing
        Dim pXmlResult As String = Nothing

        Dim XMLDom As New Xml.XmlDocument
        Dim bln As Boolean = IsDVASSAvailable(mLngSelectedCountry)

        If Not (bln) Then
            pAvailableVehicles = QUERYFAIL_ERROR_DVASS_NOTAVAILABLE
            XMLDom.LoadXml(pAvailableVehicles)
            If Not (XMLDom IsNot Nothing) Then
                pAvailableVehicles = XMLDom.SelectSingleNode("//Aurora/Error/Description").InnerText
            End If

            Exit Function
        End If


        Const NO_AVAILABILITY As String = "NO AVAILABILITY REQUIRED"


        WrapFunFordoDvassQueryAvailability = doDvassQueryAvailability _
                                        ( _
                                           pProduct, _
                                           CLng(1), _
                                           CStr(""), _
                                           pCkoLocation, _
                                           pCkiLocation, _
                                           pStartDate, _
                                           pStartTime, _
                                           pEndDate, _
                                           pEndTime, _
                                           CDbl(1), _
                                           CLng(10), _
                                           NO_AVAILABILITY, _
                                           CLng(20), _
                                           pNumberOfResultsReturned, _
                                           pStatus, _
                                           pResults, _
                                           pXmlResult _
                                       )



    End Function

    Public Shared Function QueryDvassAvailability _
      (ByVal iCountry As Integer, _
       ByVal sProdId As String, _
       ByVal sLocId_Fr As String, _
       ByVal dDate_Fr As Date, _
       ByVal dDate_Fr_Part As Double, _
       ByVal sLocId_To As String, _
       ByVal dDate_To As Date, _
       ByVal dDate_To_Part As Double, _
       ByRef sError As String _
       ) As Boolean


        Dim objDVASSMgr As New CDVASSMgr

        Dim objDVASSStat As COMDVASSMgrLib.comStatus = Nothing
        Dim oDateFr As COMDVASSMgrLib.comDateTime = Nothing
        Dim oDateTo As COMDVASSMgrLib.comDateTime = Nothing
        Dim oRes As Array = Nothing

        Dim iPnmons As Long
        Dim iPdate As Long
        Dim iPtime As Long
        Dim iPin As Long
        Dim i As Long = 0

        Dim iRetNoOfResults As Long


        Dim dPriority As Double = 1
        Dim iVisibility As Long = 10
        Dim iAlternative_Flag As Long = 0
        Dim iMaxResult_Flag As Long = 1

        QueryDvassAvailability = False
        Dim dvassLogMessage As String
        Try

            Call objDVASSMgr.GetStatus(iCountry, iPnmons, iPdate, iPtime, iPin, objDVASSStat)
            With objDVASSStat
                If (.detail <> 0) Then
                    sError = String.Concat("Message = ", .msg, _
                                           " Detail = " & .detail, _
                                           " Status = " & .status)

                    Return False
                End If
            End With



            oDateFr = ConvertDateTimeToDVASSDateTime(dDate_Fr, dDate_Fr_Part)
            oDateTo = ConvertDateTimeToDVASSDateTime(dDate_To, dDate_To_Part)



            Call objDVASSMgr.AvailQuery(iCountry, _
                                        sProdId, _
                                        1, _
                                        "", _
                                        sLocId_Fr, _
                                        oDateFr, _
                                        sLocId_To, _
                                        oDateTo, _
                                        dPriority, _
                                        iVisibility, _
                                        iAlternative_Flag, _
                                        iMaxResult_Flag, _
                                        iRetNoOfResults, _
                                        objDVASSStat, _
                                        oRes)
            With objDVASSStat
                If (.detail <> 0) Then
                    sError = String.Concat("Message = " & .msg, _
                                           " Detail = " & .detail, _
                                           " Status = " & .status)

                    dvassLogMessage = .msg
                    ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
                    InsertDvassNonAvailabilityLogging(dDate_Fr, dDate_To, sLocId_Fr, sLocId_To, sProdId, dvassLogMessage, "", "", False, "")
                    Return False
                End If
            End With

            iRetNoOfResults = CInt(CStr(oRes(7, 0)))
            dvassLogMessage = objDVASSStat.msg

            ''REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message
            If iRetNoOfResults > 0 Then
                QueryDvassAvailability = True
                Dim LogOutputOfSuccessfullAvailability As Boolean = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("LogOutputOfSuccessfullAvailability"))
                If (LogOutputOfSuccessfullAvailability) Then
                    InsertDvassNonAvailabilityLogging(dDate_Fr, dDate_To, sLocId_Fr, sLocId_To, sProdId, dvassLogMessage, iRetNoOfResults, "", True, "")
                End If
            Else
                InsertDvassNonAvailabilityLogging(dDate_Fr, dDate_To, sLocId_Fr, sLocId_To, sProdId, dvassLogMessage, iRetNoOfResults, "", False, "")
                QueryDvassAvailability = False
            End If




        Catch ex As Exception
            QueryDvassAvailability = False

        End Try

        Return QueryDvassAvailability


    End Function

    Public Shared Sub rentalModify( _
                      ByVal pRentalID As String, _
                      ByVal pProductId As String, _
                      ByVal pCkoLocation As String, _
                      ByVal pCkoDate As Date, _
                      ByVal pCkoDayPart As String, _
                      ByVal pCkiLocation As String, _
                      ByVal pCkiDate As Date, _
                      ByVal pCkiDayPart As String, _
                      ByVal pRevenue As Double, _
                      ByVal pPriority As Double, _
                      ByVal pVisibility As Integer, _
                      ByVal pVehicleIdList As String, _
                      ByVal pForceFlag As Integer, _
                      ByRef pDvassStatus As String _
                      )

        Dim objDvass As New CDVASSMgr
        Dim comDvassStatus As comStatus = Nothing
        Dim comCkoDateTime As comDateTime = Nothing
        Dim comCkiDateTime As comDateTime = Nothing

        Dim bstrRentalID() As Byte = Nothing
        Dim bstrProductID() As Byte = Nothing
        Dim bstrCkoLocnID() As Byte = Nothing
        Dim bstrCkiLocnID() As Byte = Nothing
        Dim bstrVehicleIDRange() As Byte = Nothing


        Dim strTemp As String = Nothing

        Dim ChangeRentalHasError As Boolean = True
        Dim bln As Boolean = IsDVASSAvailable(mLngSelectedCountry)

        If Not (bln) Then
            pDvassStatus = "RES064/RES064 - DVASS is not started. Rental not modified."
            Exit Sub

        Else ''ELSEIF dvassIsAvailable = False

            If Not (bln) Then
                pDvassStatus = "RES065/RES065 - DVASS is not available. Rental not modified."
                Exit Sub
            End If


            ''---------------------------------------------------------------
            comCkoDateTime = ConvertDateTimeToDVASSDateTime(pCkoDate, pCkoDayPart)
            comCkiDateTime = ConvertDateTimeToDVASSDateTime(pCkiDate, pCkiDayPart)


            bstrRentalID = ReturnBytes(pRentalID)
            bstrProductID = ReturnBytes(pProductId)
            bstrCkoLocnID = ReturnBytes(pCkoLocation)
            bstrCkiLocnID = ReturnBytes(pCkiLocation)
            bstrVehicleIDRange = ReturnBytes(pVehicleIdList)


            With comDvassStatus
                .status = 0
                .msg = String.Empty
                .detail = 0
            End With

            With objDvass

                sngDvassCallTimer = ReturnInitialSeconds

                Try

                    ''''EventLog.WriteEntry("DPS", "SelectedCountry : " & SelectedCountry, EventLogEntryType.Information)

                    Dim sbLogString As StringBuilder = New StringBuilder

                    Try
                        sbLogString.Append("-----------DVASS CALL-----------")
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("Calling ChangeRental with the params ")
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("SelectedCountry = " & SelectedCountry)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pRentalID = " & pRentalID)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pProductId = " & pProductId)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pCkoLocation = " & pCkoLocation)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("comCkoDateTime = " & comCkoDateTime.d & " - " & comCkoDateTime.t)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pCkiLocation = " & pCkiLocation)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("comCkiDateTime = " & comCkiDateTime.d & " - " & comCkiDateTime.t)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pRevenue = " & pRevenue)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pPriority = " & pPriority)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pVehicleIdList = " & pVehicleIdList)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pForceFlag = " & pForceFlag)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("comDvassStatus = " & comDvassStatus.detail & " - " & comDvassStatus.msg & " - " & comDvassStatus.status)
                        sbLogString.Append(vbNewLine)
                    Catch ex As Exception
                    End Try


                    Call .ChangeRental _
                         ( _
                             SelectedCountry, _
                             pRentalID, _
                             pProductId, _
                             pCkoLocation, _
                             comCkoDateTime, _
                             pCkiLocation, _
                             comCkiDateTime, _
                             pRevenue, _
                             pPriority, _
                             pVehicleIdList, _
                             pForceFlag, _
                             comDvassStatus _
                         )

                    Try
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("This is the data after call was completed ")
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("SelectedCountry = " & SelectedCountry)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pRentalID = " & pRentalID)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pProductId = " & pProductId)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pCkoLocation = " & pCkoLocation)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("comCkoDateTime = " & comCkoDateTime.d & " - " & comCkoDateTime.t)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pCkiLocation = " & pCkiLocation)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("comCkiDateTime = " & comCkiDateTime.d & " - " & comCkiDateTime.t)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pRevenue = " & pRevenue)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pPriority = " & pPriority)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pVehicleIdList = " & pVehicleIdList)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("pForceFlag = " & pForceFlag)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("comDvassStatus = " & comDvassStatus.detail & " - " & comDvassStatus.msg & " - " & comDvassStatus.status)
                        sbLogString.Append(vbNewLine)
                        sbLogString.Append("-----------DVASS CALL-----------")

                        Logging.LogInformation("DVASSCALL", sbLogString.ToString())
                    Catch ex As Exception
                    End Try

                    ChangeRentalHasError = False

                Catch ex As Exception
                    strTemp = String.Concat("<Errors>", makeXmlErrorDocument("AuroraDvass.rentalModify", comDvassStatus, ex), "</Errors>")
                    Exit Sub
                End Try

            End With

            If ChangeRentalHasError = False Then

                pDvassStatus = TranslateDvassStatus(comDvassStatus)
                sngDvassCallTimer = ReturnInitialSeconds


            End If

        End If ''ENDIF dvassIsAvailable = False

    End Sub

#End Region

    ''rev:mia April 11, 2013 - Start working on these stuff after long hiatus
    ''                       - retrieval of long-forgotten codes 
#Region "rev:mia  July 23 2012 STARTED: Create a new function for GetWSAvailability "

    'Public Shared Function IsWSDvassAvailable _
    '                                        (ByVal iCountry As Integer, _
    '                                         ByVal sProdId As String, _
    '                                         ByVal sLocId_Fr As String, _
    '                                         ByVal dDate_Fr As Date, _
    '                                         ByVal dDate_Fr_Part As Double, _
    '                                         ByVal sLocId_To As String, _
    '                                         ByVal dDate_To As Date, _
    '                                         ByVal dDate_To_Part As Double, _
    '                                         ByRef sReturnMessage As String, _
    '                                         ByVal NumberOfVehicleTarget As Integer, _
    '                                         ByRef NumberOfVehicle As Integer, _
    '                                         ByRef sbResult As String) As Boolean


    '    Dim objDVASSMgr As New CDVASSMgr

    '    Dim objDVASSStat As COMDVASSMgrLib.comStatus = Nothing
    '    Dim oDateFr As COMDVASSMgrLib.comDateTime = Nothing
    '    Dim oDateTo As COMDVASSMgrLib.comDateTime = Nothing
    '    Dim oRes As Array = Nothing

    '    Dim iPnmons As Long
    '    Dim iPdate As Long
    '    Dim iPtime As Long
    '    Dim iPin As Long
    '    Dim i As Long = 0


    '    Dim iRetNoOfResults As Long


    '    Dim dPriority As Double = 1
    '    Dim iVisibility As Long = 10
    '    Dim iAlternative_Flag As Long = 0

    '    ''make result equal to expected numbes of vehicle
    '    Dim iMaxResult_Flag As Long = NumberOfVehicleTarget

    '    IsWSDvassAvailable = False

    '    Dim sb As New StringBuilder
    '    sb.Append(sbResult)

    '    Try
    '        sb.Append("||4.Calling AuroraDvass.IsWSDvassAvailable " & vbCrLf)
    '        sb.Append("||************************" & vbCrLf)


    '        sb.Append("||5.Calling DVASSMgr.GetStatus " & vbCrLf)
    '        sb.Append("||************************" & vbCrLf)
    '        sb.Append("||DVASS.GetStatus PARAMETERS " & vbCrLf)
    '        sb.Append("||iCountry             : " & iCountry & vbCrLf)
    '        sb.Append("||iPnmons              : " & iPnmons & vbCrLf)
    '        sb.Append("||iPdate               : " & iPdate & vbCrLf)
    '        sb.Append("||iPtime               : " & iPtime & vbCrLf)
    '        Call objDVASSMgr.GetStatus(iCountry, iPnmons, iPdate, iPtime, iPin, objDVASSStat)

    '        With objDVASSStat

    '            If (.detail <> 0) Then
    '                sReturnMessage = String.Concat("Error:", _
    '                                                vbCrLf, " Message = ", .msg, _
    '                                                vbCrLf, " Detail = " & .detail, _
    '                                                vbCrLf, " Status = " & .status)

    '                sb.Append("||COMDVASSMgrLib.comStatus              : " & sReturnMessage & vbCrLf)
    '                sbResult = sb.ToString
    '                Logging.LogDebug("IsWSDvassAvailable: Error: ", sReturnMessage)
    '                Return False
    '            End If
    '        End With

    '        sb.Append("||COMDVASSMgrLib.comStatus              : " & objDVASSStat.status & " - " & objDVASSStat.msg & vbCrLf)

    '        sb.Append("||ConvertDateTimeToDVASSDateTime PARAMETERS " & vbCrLf)
    '        sb.Append("||dDate_Fr             : " & dDate_Fr & vbCrLf)
    '        sb.Append("||dDate_Fr_Part        : " & dDate_Fr_Part & vbCrLf)
    '        oDateFr = ConvertDateTimeToDVASSDateTime(dDate_Fr, dDate_Fr_Part)


    '        ''sb.Append("||ConvertDateTimeToDVASSDateTime PARAMETERS " & vbCrLf)
    '        sb.Append("||dDate_To             : " & dDate_To & vbCrLf)
    '        sb.Append("||dDate_To_Part        : " & dDate_To_Part & vbCrLf)
    '        oDateTo = ConvertDateTimeToDVASSDateTime(dDate_To, dDate_To_Part)


    '        Logging.LogDebug("IsWSDvassAvailable: Start: ", " Calling objDVASSMgr.AvailQuery")

    '        Try
    '            sb.Append("||6.Calling DVASS.AvailQuery " & vbCrLf)
    '            sb.Append("||************************" & vbCrLf)

    '            sb.Append("||DVASS.AvailQuery PARAMETERS " & vbCrLf)
    '            sb.Append("||iCountry             : " & iCountry & vbCrLf)
    '            sb.Append("||sProdId              : " & sProdId & vbCrLf)
    '            sb.Append("||NumberOfVehicleTarget              : " & NumberOfVehicleTarget & vbCrLf)
    '            sb.Append("||CandidateVehicle     : " & "" & vbCrLf)
    '            sb.Append("||sLocId_Fr            : " & sLocId_Fr & vbCrLf)
    '            sb.Append("||oDateFr              : " & oDateFr.ToString & vbCrLf)
    '            sb.Append("||sLocId_To            : " & sLocId_To & vbCrLf)
    '            sb.Append("||oDateTo              : " & oDateTo.ToString & vbCrLf)
    '            sb.Append("||dPriority            : " & dPriority.ToString & vbCrLf)
    '            sb.Append("||iVisibility          : " & iVisibility.ToString & vbCrLf)
    '            sb.Append("||iAlternative_Flag    : " & iAlternative_Flag.ToString & vbCrLf)
    '            sb.Append("||iMaxResult_Flag      : " & iMaxResult_Flag.ToString & vbCrLf)
    '            sb.Append("||iRetNoOfResults      : " & iRetNoOfResults.ToString & vbCrLf)
    '            sb.Append("||iRetNoOfResults  (ref)    : " & iRetNoOfResults.ToString & vbCrLf)
    '            sb.Append("||COMDVASSMgrLib.comStatus  (ref): " & objDVASSStat.msg & vbCrLf)
    '        Catch ex As Exception
    '            ''do nothing

    '        End Try



    '        Call objDVASSMgr.AvailQuery(iCountry, _
    '                                    sProdId, _
    '                                    NumberOfVehicleTarget, _
    '                                    String.Empty, _
    '                                    sLocId_Fr, _
    '                                    oDateFr, _
    '                                    sLocId_To, _
    '                                    oDateTo, _
    '                                    dPriority, _
    '                                    iVisibility, _
    '                                    iAlternative_Flag, _
    '                                    iMaxResult_Flag, _
    '                                    iRetNoOfResults, _
    '                                    objDVASSStat, _
    '                                    oRes)

    '        Logging.LogDebug("IsWSDvassAvailable: End: ", " Called objDVASSMgr.AvailQuery")

    '        With objDVASSStat
    '            If (.detail <> 0) Then
    '                sReturnMessage = String.Concat("Error:", _
    '                                                vbCrLf, " Message = ", .msg, _
    '                                                vbCrLf, " Detail = " & .detail, _
    '                                                vbCrLf, " Status = " & .status)
    '                sb.Append("|||DVASS.AvailQuery              : " & sReturnMessage & vbCrLf)
    '                sbResult = sb.ToString
    '                Logging.LogDebug("IsWSDvassAvailable: Error: ", sReturnMessage)
    '                Return False
    '            End If
    '        End With


    '        NumberOfVehicle = CInt(CStr(oRes(7, 0)))
    '        Dim xmlSbstring As New StringBuilder

    '        Dim strTemp As String = String.Empty


    '        For lngResultIndex As Integer = 0 To iRetNoOfResults - 1

    '            strTemp = String.Concat("<AvailableVehicle><PrdId>", CStr(oRes(0, lngResultIndex)), _
    '                      "</PrdId><CkoLocation>", CStr(oRes(1, lngResultIndex)), _
    '                      "</CkoLocation><CkoDate>", convertDVASSDateToVBDate(CLng(oRes(2, lngResultIndex))), _
    '                      "</CkoDate><CkiLocation>", CStr(oRes(3, lngResultIndex)), _
    '                      "</CkiLocation><CkiDate>", convertDVASSDateToVBDate(CLng(oRes(4, lngResultIndex))), _
    '                      "</CkiDate><Preference>", CStr(oRes(5, lngResultIndex)), _
    '                      "</Preference><Cost>", CStr(oRes(6, lngResultIndex)), _
    '                      "</Cost><NumberOfVehicles>", CStr(oRes(7, lngResultIndex)), _
    '                      "</NumberOfVehicles><ReducesRelocationCount>", CStr(oRes(8, lngResultIndex)), _
    '                      "</ReducesRelocationCount></AvailableVehicle>")

    '            xmlSbstring.Append(strTemp)

    '        Next


    '        If iRetNoOfResults > 0 Then
    '            IsWSDvassAvailable = True
    '            xmlSbstring.Insert(0, "<Data>")
    '            xmlSbstring.Append("</Data>")
    '            sReturnMessage = xmlSbstring.ToString

    '        Else
    '            sReturnMessage = String.Empty
    '            IsWSDvassAvailable = False
    '        End If

    '        sb.Append("||oRes  (ref): " & xmlSbstring.ToString & vbCrLf)
    '        Logging.LogDebug("IsWSDvassAvailable: Xml returned: ", sReturnMessage)


    '    Catch ex As Exception
    '        IsWSDvassAvailable = False

    '        sReturnMessage = String.Concat("Error:", _
    '                                             vbCrLf, " Message = ", ex.Message, _
    '                                             vbCrLf, " Detail = " & ex.StackTrace, _
    '                                             vbCrLf, " Status = ERROR")

    '        sb.Append("|||Exception              : " & sReturnMessage & vbCrLf)
    '    End Try

    '    Logging.LogDebug("IsWSDvassAvailable: Available? : ", IsWSDvassAvailable)
    '    sbResult = sb.ToString
    '    Return IsWSDvassAvailable


    'End Function

    'Public Shared Function GetDVASSXmlResult _
    '                                   (ByVal iCountry As Integer, _
    '                                    ByVal sProdId As String, _
    '                                    ByVal sLocId_Fr As String, _
    '                                    ByVal dDate_Fr As Date, _
    '                                    ByVal sLocId_To As String, _
    '                                    ByVal dDate_To As Date, _
    '                                    ByVal NumberOfVehicleTarget As Integer _
    '                                    ) As String


    '    Dim objDVASSMgr As New CDVASSMgr


    '    Dim pStatus As comStatus = Nothing
    '    Dim oDateFr As COMDVASSMgrLib.comDateTime = Nothing
    '    Dim oDateTo As COMDVASSMgrLib.comDateTime = Nothing
    '    Dim oRes As Array = Nothing

    '    Dim iPnmons As Long
    '    Dim iPdate As Long
    '    Dim iPtime As Long
    '    Dim iPin As Long
    '    Dim i As Long = 0

    '    If (objDVASSMgr Is Nothing) Then

    '        With pStatus
    '            .status = 2
    '            .msg = "DVASS is not started."
    '            .detail = 900
    '        End With

    '        Throw New Exception("DVASS is not started.")
    '    End If

    '    If Not (AuroraDvass.IsDVASSAvailable(mLngSelectedCountry)) Then
    '        With pStatus
    '            .status = 2
    '            .msg = "DVASS is not available."
    '            .detail = 900
    '        End With

    '        Throw New Exception("DVASS is not available.")

    '    End If ''ENDIF FOR dvassAvailable() = False 

    '    Dim iRetNoOfResults As Long
    '    Dim pstarttime As Long = 1
    '    Dim pendtime As Long = 1

    '    oDateFr = AuroraDvass.ConvertDateTimeToDVASSDateTime(dDate_Fr, pstarttime)
    '    oDateTo = AuroraDvass.ConvertDateTimeToDVASSDateTime(dDate_To, pendtime)

    '    With pStatus
    '        .status = 0
    '        .msg = String.Empty
    '        .detail = 0
    '    End With

    '    Dim dPriority As Double = 1
    '    Dim iVisibility As Long = 10
    '    Dim iAlternative_Flag As Long = 0

    '    ''make result equal to expected numbes of vehicle
    '    Dim iMaxResult_Flag As Long = NumberOfVehicleTarget



    '    Dim sb As New StringBuilder
    '    Dim xmlReturnMessage As New XmlDocument

    '    Try

    '        sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
    '        sb.Append("||************************" & vbCrLf)
    '        sb.Append("||DVASS.GetStatus PARAMETERS " & vbCrLf)
    '        sb.Append("||iCountry             : " & iCountry & vbCrLf)
    '        sb.Append("||iPnmons              : " & iPnmons & vbCrLf)
    '        sb.Append("||iPdate               : " & iPdate & vbCrLf)
    '        sb.Append("||iPtime               : " & iPtime & vbCrLf)

    '        Call objDVASSMgr.GetStatus(iCountry, iPnmons, iPdate, iPtime, iPin, pStatus)

    '        With pStatus
    '            If (.detail <> 0) Then
    '                Throw New Exception(.msg & " - " & .status)
    '            End If

    '        End With

    '        Try
    '            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
    '            sb.Append("||Calling DVASS.AvailQuery " & vbCrLf)
    '            sb.Append("||************************" & vbCrLf)

    '            sb.Append("||PARAMETERS " & vbCrLf)
    '            sb.Append("||iCountry                               (icountry)            : " & iCountry & vbCrLf)
    '            sb.Append("||sProdId                                (prodid)              : " & sProdId & vbCrLf)
    '            sb.Append("||NumberOfVehicleTarget                  (nvehicles)           : " & NumberOfVehicleTarget & vbCrLf)
    '            sb.Append("||CandidateVehicle                       (candidate_vehicles)  : " & "" & vbCrLf)
    '            sb.Append("||sLocId_Fr                              (locid_fr)            : " & sLocId_Fr & vbCrLf)
    '            sb.Append("||oDateFr                                (pdat_fr)             : " & oDateFr.d & "-" & oDateFr.t & vbCrLf)
    '            sb.Append("||sLocId_To                              (locid_to)            : " & sLocId_To & vbCrLf)
    '            sb.Append("||oDateTo                                (pdat_to)             : " & oDateTo.d & "-" & oDateTo.t & vbCrLf)
    '            sb.Append("||dPriority                              (priority)            : " & dPriority.ToString & vbCrLf)
    '            sb.Append("||iVisibility                            (vis_flag)            : " & iVisibility.ToString & vbCrLf)
    '            sb.Append("||iAlternative_Flag                      (alterns_flag)        : " & iAlternative_Flag.ToString & vbCrLf)
    '            sb.Append("||iMaxResult_Flag                        (max_nresults)        : " & iMaxResult_Flag.ToString & vbCrLf)
    '            sb.Append("||iRetNoOfResults                        (pnresults)           : " & iRetNoOfResults.ToString & vbCrLf)
    '            sb.Append("||pstat                                  (pstat)               : " & pStatus.detail & "-" & pStatus.msg & "-" & pStatus.status & vbCrLf)
    '            sb.Append("||************************" & vbCrLf)
    '        Catch ex As Exception
    '            ''do nothing
    '        End Try



    '        Call objDVASSMgr.AvailQuery(iCountry, _
    '                                    sProdId, _
    '                                    1, _
    '                                    String.Empty, _
    '                                    sLocId_Fr, _
    '                                    oDateFr, _
    '                                    sLocId_To, _
    '                                    oDateTo, _
    '                                    dPriority, _
    '                                    iVisibility, _
    '                                    iAlternative_Flag, _
    '                                    iMaxResult_Flag, _
    '                                    iRetNoOfResults, _
    '                                    pStatus, _
    '                                    oRes)

    '        sb.Append("||RESULT" & vbCrLf)
    '        sb.Append("||iCountry                               (icountry)            : " & iCountry & vbCrLf)
    '        sb.Append("||sProdId                                (prodid)              : " & sProdId & vbCrLf)
    '        sb.Append("||NumberOfVehicleTarget                  (nvehicles)           : " & NumberOfVehicleTarget & vbCrLf)
    '        sb.Append("||CandidateVehicle                       (candidate_vehicles)  : " & "" & vbCrLf)
    '        sb.Append("||sLocId_Fr                              (locid_fr)            : " & sLocId_Fr & vbCrLf)
    '        sb.Append("||oDateFr                                (pdat_fr)             : " & oDateFr.d.ToString & "-" & oDateFr.t.ToString & vbCrLf)
    '        sb.Append("||sLocId_To                              (locid_to)            : " & sLocId_To & vbCrLf)
    '        sb.Append("||oDateTo                                (pdat_to)             : " & oDateTo.d.ToString & "-" & oDateTo.t.ToString & vbCrLf)
    '        sb.Append("||dPriority                              (priority)            : " & dPriority.ToString & vbCrLf)
    '        sb.Append("||iVisibility                            (vis_flag)            : " & iVisibility.ToString & vbCrLf)
    '        sb.Append("||iAlternative_Flag                      (alterns_flag)        : " & iAlternative_Flag.ToString & vbCrLf)
    '        sb.Append("||iMaxResult_Flag                        (max_nresults)        : " & iMaxResult_Flag.ToString & vbCrLf)
    '        sb.Append("||iRetNoOfResults                        (pnresults)           : " & iRetNoOfResults.ToString & vbCrLf)
    '        sb.Append("||pstat                                  (pstat)               : " & pStatus.detail & "-" & pStatus.msg & "-" & pStatus.status & vbCrLf)
    '        sb.Append("||************************" & vbCrLf)





    '        Dim xmlSbstring As New StringBuilder

    '        Dim strTemp As String = String.Empty


    '        For lngResultIndex As Integer = 0 To iRetNoOfResults - 1

    '            strTemp = String.Concat("<AvailableVehicle><PrdId>", CStr(oRes(0, lngResultIndex)), _
    '                      "</PrdId><CkoLocation>", CStr(oRes(1, lngResultIndex)), _
    '                      "</CkoLocation><CkoDate>", convertDVASSDateToVBDate(CLng(oRes(2, lngResultIndex))), _
    '                      "</CkoDate><CkiLocation>", CStr(oRes(3, lngResultIndex)), _
    '                      "</CkiLocation><CkiDate>", convertDVASSDateToVBDate(CLng(oRes(4, lngResultIndex))), _
    '                      "</CkiDate><Preference>", CStr(oRes(5, lngResultIndex)), _
    '                      "</Preference><Cost>", CStr(oRes(6, lngResultIndex)), _
    '                      "</Cost><NumberOfVehicles>", CStr(oRes(7, lngResultIndex)), _
    '                      "</NumberOfVehicles><ReducesRelocationCount>", CStr(oRes(8, lngResultIndex)), _
    '                      "</ReducesRelocationCount></AvailableVehicle>")

    '            xmlSbstring.Append(strTemp)

    '        Next


    '        If iRetNoOfResults > 0 Then
    '            xmlReturnMessage.LoadXml(xmlSbstring.ToString)
    '        Else
    '            xmlReturnMessage.LoadXml( _
    '                                  "<Status>FINISHED</Status>" & _
    '                                  "<Result>0</Result>" & _
    '                                  "<Message>RetNoOfResults is zero</Message>")


    '        End If

    '    Catch ex As Exception

    '        xmlReturnMessage.LoadXml( _
    '                                 "<Status>ERROR</Status>" & _
    '                                 "<Result>" & False & "</Result>" & _
    '                                 "<Message>" & ex.StackTrace & "," & ex.Message & "</Message>")


    '    End Try
    '    sb.Append("||GetDVASSXmlResult: " & xmlReturnMessage.OuterXml & vbCrLf)

    '    Logging.LogInformation("GetDVASSXmlResult:".ToUpper, sb.ToString)
    '    Dim xml As String = String.Concat("<Data>", xmlReturnMessage.OuterXml, "</Data>")

    '    Return xml


    'End Function


    ''Private Shared Function doDvassQueryAvailability( _
    ''                        ByVal Brand As String, _
    ''                        ByVal CountryCode As String, _
    ''                        ByVal VehicleCode As String, _
    ''                        ByVal CheckOutZoneCode As String, _
    ''                        ByVal CheckOutDateTime As Date, _
    ''                        ByVal CheckInZoneCode As String, _
    ''                        ByVal CheckInDateTime As Date, _
    ''                        ByVal AgentCode As String, _
    ''                        ByVal PackageCode As String, _
    ''                        ByVal CountryOfResidence As String, _
    ''                        ByVal IsQuickAvail As Boolean, _
    ''                        ByRef pXmlResult As String, _
    ''                        optional  ChannelRequest As Integer = 0, _
    ''                        Optional NoOfAdults As Short = 1, _
    ''                        Optional NoOfChildren As Short = 0, _
    ''                        Optional IsVan As Boolean = True, _
    ''                        Optional IsTestMode As Boolean = False, _
    ''                        Optional IsGross As Boolean = False, _
    ''                        Optional IsBestBuy As Boolean = False
    ''                 ) As Boolean


    ''    Dim wsPhoenix As New PhoenixWebService
    ''    Dim xmlreturn As New XmlDocument
    ''    Try
    ''        xmlreturn = wsPhoenix.GetAvailabilityV2(Brand, _
    ''                                  CountryCode, _
    ''                                  VehicleCode, _
    ''                                  CheckOutZoneCode, _
    ''                                  CheckOutDateTime, _
    ''                                  CheckInZoneCode, _
    ''                                  CheckInDateTime, _
    ''                                  NoOfAdults, _
    ''                                  NoOfChildren, _
    ''                                  AgentCode, _
    ''                                  PackageCode, _
    ''                                  IsVan, _
    ''                                  IsBestBuy, _
    ''                                  CountryOfResidence, _
    ''                                  IsTestMode, _
    ''                                  IsGross, _
    ''                                  ChannelRequest, _
    ''                                  IsQuickAvail)


    ''        Dim xquery As String = "Data/AvailableVehicle/"
    ''        Dim vehnum As String = String.Empty
    ''        If (xmlreturn.OuterXml.Contains("ERROR") = True) Then
    ''            doDvassQueryAvailability = False
    ''        Else
    ''            vehnum = xmlreturn.SelectSingleNode(xquery & "NumberOfVehicles").InnerText
    ''            doDvassQueryAvailability = IIf(CInt(vehnum) > 0, True, False)
    ''        End If

    ''        pXmlResult = xmlreturn.OuterXml

    ''    Catch ex As Exception
    ''        doDvassQueryAvailability = False
    ''    End Try

    ''    Return doDvassQueryAvailability

    ''End Function


#End Region

#Region "REV:MIA 05AUG2015 - DVASS Investigation - Log all availability issues with requested parameter and returned dvass message"
    Private Shared Sub InsertDvassNonAvailabilityLogging(PikcupDate As DateTime, DropOffDate As DateTime, PickupLocation As String, DropOffLocation As String, Product As String, dVASSMessage As String, xmlResult As String, dvasserrors As String, IsAvailable As Boolean, events As String)
        Dim sbLogString As New StringBuilder
        sbLogString.AppendFormat("PikcupDate:       {0}{1}", PikcupDate, vbCrLf)
        sbLogString.AppendFormat("DropOffDate:      {0}{1}", DropOffDate, vbCrLf)
        sbLogString.AppendFormat("PickupLocation:   {0}{1}", PickupLocation, vbCrLf)
        sbLogString.AppendFormat("DropOffLocation:  {0}{1}", DropOffLocation, vbCrLf)
        sbLogString.AppendFormat("Product:          {0}{1}", Product, vbCrLf)
        sbLogString.AppendFormat("Message:          {0}{1}", dVASSMessage, vbCrLf)
        sbLogString.AppendFormat("XmlResult:        {0}{1}", xmlResult, vbCrLf)
        sbLogString.AppendFormat("Exceptions:       {0}{1}", dvasserrors, vbCrLf)
        sbLogString.AppendFormat("XmlResult:        {0}{1}", xmlResult, vbCrLf)
        sbLogString.AppendFormat("IsAvailable:      {0}{1}", IsAvailable, vbCrLf)
        sbLogString.AppendFormat("Event:            {0}{1}", events, vbCrLf)
        Try
            Aurora.Common.Data.ExecuteScalarSP("INS_DVASS_NonAvailabilityLogging", PikcupDate, DropOffDate, PickupLocation, DropOffLocation, Product, dVASSMessage, xmlResult, dvasserrors, IsAvailable, events)
        Catch ex As Exception
            sbLogString.AppendFormat("Exception Message:          {0}{1}", ex.Message & vbCrLf & ex.StackTrace, vbCrLf)

        End Try
        Logging.LogInformation("InsertDvassNonAvailabilityLogging", sbLogString.ToString())
    End Sub
#End Region
End Class
