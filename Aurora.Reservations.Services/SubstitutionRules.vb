Imports Aurora.Common
Imports Aurora.Common.Data
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class SubstitutionRules

    Private Shared oOpsLogsDom As System.Xml.XmlDocument   'Global Dom object
    Private Shared sXmlString As Object, iCount As Integer, sError As String
    Private Shared sErrNo As String, sErrDesc As String
    Private Shared sQueryString As String, sErrorString As String


    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                              ByVal sErrNumber As String, _
                                              ByVal sErrType As String, _
                                              ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function
    Private Shared mOutputParameter As String
    Private Shared Property OutputParameter() As String
        Get
            Return mOutputParameter
        End Get
        Set(ByVal value As String)
            mOutputParameter = value
        End Set
    End Property
    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
        'Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("sp_get_ErrorString", params)
        'If Not String.IsNullOrEmpty(OutputParameter) Then
        '    Return xmlstring
        'Else
        '    Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", xmlstring)
        'End If
        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Private Function Is2ndDateGreater(ByVal FromDate As String, ByVal ToDate As String) As Boolean
        Is2ndDateGreater = True

        If CDate(ToDate) > CDate(FromDate) Then
            Is2ndDateGreater = True
        Else
            ' changed by Shoel
            ' sErrorString = GetErrorTextFromResource(True, "", "Application", "The end date must not be greater than the start date.")
            sErrorString = GetErrorTextFromResource(True, String.Empty, "Application", "The end date must not be less than the start date.")
            sErrorString = "<Root>" & sErrorString & "<Data></Data></Root>"
            Is2ndDateGreater = False
        End If


    End Function

    Public Shared Function manageSubstitutionRule(ByVal xFleetModelforBookableProduct As String) As String
        '---------------------------------Declaration of variables---------------------------------

        Dim BookableProduct As String
        Dim iSize As Integer
        Dim sctyCode As String, sprdId As String
        Dim saIds As String()

        '------------------------------------End of Declaration------------------------------------


        Try
            oOpsLogsDom = New System.Xml.XmlDocument

            '==== Check for valid xml ====
            Try
                oOpsLogsDom.LoadXml(xFleetModelforBookableProduct)
            Catch ex As System.Xml.XmlException

                sXmlString = getMessageFromDB("GEN023")
                sErrorString = GetErrorTextFromResource(True, "GEN023", "Application", sXmlString)
                manageSubstitutionRule = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End Try

            iSize = oOpsLogsDom.DocumentElement.ChildNodes(2).ChildNodes.Count
            sctyCode = oOpsLogsDom.DocumentElement.SelectSingleNode("//Root/Cop/@code").InnerText
            sprdId = oOpsLogsDom.DocumentElement.SelectSingleNode("//Root/Prd/@id").InnerText


            sXmlString = Aurora.Common.Data.ExecuteScalarSP("OPL_updateSubstitutionRuleProductList", oOpsLogsDom.InnerXml)


            saIds = Split(sXmlString, "/")
            sErrNo = saIds(0)
            sErrDesc = saIds(1)

            If (sprdId <> String.Empty) Then
                BookableProduct = Aurora.Common.Data.ExecuteScalarSP("OPL_getSubstitutionRuleProductList", sctyCode, sprdId, "", "")
            Else
                BookableProduct = Aurora.Common.Data.ExecuteScalarSP("OPL_getSubstitutionRuleProductList", sctyCode, "", "", "")
            End If

            If sErrNo = "GEN045" Or sErrNo = "GEN046" Or sErrNo = "GEN047" Then
                sErrorString = GetErrorTextFromResource("False", sErrNo, "Application", sErrDesc)
            Else
                sErrorString = GetErrorTextFromResource("True", sErrNo, "Application", sErrDesc)
            End If
            manageSubstitutionRule = "<Root>" & sErrorString & "<Data>" & BookableProduct & "</Data></Root>"

        Catch ex As Exception
            manageSubstitutionRule = "<Root>" & GetErrorTextFromResource(True, "Exception", "System", ex.Message) & "</Root>"
            Logging.LogError("", "manageSubstitutionRule" & ex.Message)
        End Try

    End Function
End Class
