
Imports Aurora.Confirmation.Data
Imports Aurora.Common

Public Class Confirmation

    Public Shared Function GetConfirmationSetUp(ByVal user As String, ByVal country As String) As DataSet
        Try
            Return (Data.DataRepository.GetConfirmationSetUp(user, country))
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return Nothing
        End Try

    End Function

    Public Shared Function GetRentalStatus(ByVal rentalStatusId As String) As DataTable
        Try
            Return Data.DataRepository.GetRentalStatus(rentalStatusId)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertUpdateConfirmationSetUp(ByVal country As String, ByVal user As String, ByVal confirmationRentalStatusId As String, ByVal programName As String, ByVal status As String, ByVal text As String) As Boolean
        Try
            Data.DataRepository.InsertUpdateConfirmationSetUp(country, user, confirmationRentalStatusId, programName, status, text)

            Return True

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return False
        End Try
    End Function

    Public Shared Function GetNewsFlash(ByVal newsFlashId As String) As DataTable
        Try
            Return Data.DataRepository.GetNewsFlash(newsFlashId)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return Nothing
        End Try
    End Function

    Public Shared Function InsertNewsFlash(ByVal country As String, ByVal user As String, ByVal programName As String, ByVal audienceType As String, ByVal effectiveDate As DateTime, ByVal terminationDate As DateTime, ByVal text As String) As String
        Try
            Return Data.DataRepository.InsertNewsFlash(country, user, programName, audienceType, effectiveDate, terminationDate, text)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function

    Public Shared Function UpdateNewsFlash(ByVal country As String, ByVal user As String, ByVal newsFlashId As String, ByVal audienceType As String, ByVal effectiveDate As DateTime, ByVal terminationDate As DateTime, ByVal text As String) As String
        Try
            Return Data.DataRepository.UpdateNewsFlash(country, user, newsFlashId, audienceType, effectiveDate, terminationDate, text)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function

    Public Shared Function DeleteNewsFlash(ByVal newsFlashId As String) As String
        Try
            Return Data.DataRepository.DeleteNewsFlash(newsFlashId)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function

    Public Shared Function GetConfirmationText(ByVal country As String, ByVal userCode As String, ByVal activity As Boolean, Optional ByVal noteSpecId As String = "") As DataSet
        Return Data.DataRepository.GetConfirmationText(userCode, country, activity, noteSpecId)
    End Function

    ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
    Public Shared Function InsertConfirmationText(ByVal country As String, ByVal user As String, ByVal type As String, ByVal isHeader As Boolean, ByVal text As String, ByVal order As Integer, ByVal isCus As Boolean, ByVal isAgn As Boolean, ByVal isActive As Boolean, ByVal programName As String, Optional NtsBrdCode As String = "") As String
        Try
            Return Data.DataRepository.InsertConfirmationText(country, user, type, isHeader, text, order, isCus, isAgn, isActive, programName, NtsBrdCode)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function

    ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
    Public Shared Function UpdateConfirmationText(ByVal country As String, ByVal user As String, ByVal noteSpecId As String, ByVal type As String, ByVal isHeader As Boolean, ByVal text As String, ByVal order As Integer, ByVal isCus As Boolean, ByVal isAgn As Boolean, ByVal isActive As Boolean, ByVal programName As String, Optional NtsBrdCode As String = "") As String
        Try
            Return Data.DataRepository.UpdateConfirmationText(country, user, noteSpecId, type, isHeader, text, order, isCus, isAgn, isActive, programName, NtsBrdCode)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function

    'Public Shared Function DeleteConfirmationText(ByVal noteSpecId As String) As String
    '    Try
    '        Return Data.DataRepository.DeleteConfirmationText(noteSpecId) 
    '    Catch ex As Exception
    '        Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
    '        Return ex.Message
    '    End Try
    'End Function

    Public Shared Function ConfirmationGetBrand(rntId As String) As String
        Try
            Return Data.DataRepository.ConfirmationGetBrand(rntId)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function


    Public Shared Function ConfirmationGetBrandName(brandCode As String) As String
        Try
            Return Data.DataRepository.ConfirmationGetBrandName(brandCode)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ex.Message
        End Try
    End Function

    ''rev:mia 22-march-2016 https://thlonline.atlassian.net/browse/AURORA-754 Question marks "?" showing on Experience Voucher Email?
    Public Shared Sub InsertConfirmationHTML(ByVal confirmationid As String, _
                               ByVal confirmationtext As String, _
                               ByVal userid As String, _
                               ByVal newconfirmationid As String)
        Try
            Data.DataRepository.InsertConfirmationHTML(confirmationid, confirmationtext, userid, newconfirmationid)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
        End Try
    End Sub
End Class
