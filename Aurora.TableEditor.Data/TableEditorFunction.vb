Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Partial Class TableEditorDataSet

    Partial Class TableEditorFunctionRow

        Public Function GetEntityByName(ByVal name As String) As TableEditorEntityRow
            For Each e As TableEditorEntityRow In Me.GetTableEditorEntityRows()
                If e.teeName = name Then
                    Return e
                End If
            Next
            Return Nothing
        End Function

        Public ReadOnly Property QueryStringParams() As String
            Get
                Return "?" & CodeParam & "=" & System.Web.HttpUtility.UrlEncode(Me.tefCode)
            End Get
        End Property

        Public Sub Validate()
            If Not ValidateDatabaseName(Me.tefCode) Then
                Throw New ValidationException("Invalid Table Editor Function Code: " & Me.tefCode, Me)
            End If

            If Not ValidateName(Me.tefName) Then
                Throw New ValidationException("Invalid Table Editor Function Name: " & Me.tefName, Me)
            End If

            ' ensure there is only one root table editor entity
            Dim count As Integer
            For Each t As TableEditorEntityRow In Me.GetTableEditorEntityRows()
                If t.IsteeParentIdNull() Then
                    count += 1
                End If
            Next
            If count = 0 Then
                Throw New ValidationException("No TableEditor Entities Defined", Me)
            ElseIf count > 1 Then
                Throw New ValidationException("Duplicate Table Editor Entities", Me)
            End If

            ' ensure no circular dependencies
            Dim entitiesById As New Dictionary(Of Integer, TableEditorEntityRow)
            Dim changed As Boolean = True
            While changed
                changed = False
                For Each e As TableEditorEntityRow In Me.GetTableEditorEntityRows()
                    If Not entitiesById.ContainsKey(e.teeId) AndAlso (e.IsteeParentIdNull() OrElse entitiesById.ContainsKey(e.teeParentId)) Then
                        entitiesById.Add(e.teeId, e)
                        changed = True
                    End If
                Next
            End While
            If entitiesById.Count <> Me.GetTableEditorEntityRows().Length Then
                Throw New ValidationException("Invalid TableEditor Entities, possibly caused by a circular dependency", Me)
            End If

            ' validate entities
            For Each e As TableEditorEntityRow In Me.GetTableEditorEntityRows()
                e.Validate()
            Next
        End Sub

    End Class

End Class
