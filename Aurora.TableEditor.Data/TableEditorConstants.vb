Partial Class TableEditorDataSet

    Public Const CodeParam = "Code"
    Public Const EntityParam = "Entity"

    Public Shared Function ValidateName(ByVal name As String) As Boolean
        If name.Trim().Length = 0 Then Return False
        Return True
    End Function

    Public Shared Function ValidateDatabaseName(ByVal name As String) As Boolean
        If name Is Nothing OrElse name.Length = 0 OrElse name.Trim().Length <> name.Length Then Return False

        Return True
    End Function

    Public Shared Function NiceName(ByVal name As String) As String
        Dim cn As String = name.Trim()
        If cn.Length = 0 Then Return name

        ' put spaces before capitals and numbers
        Dim inCap As Boolean = True
        Dim result As String = ""
        For i As Integer = 0 To cn.Length - 1
            If Not inCap AndAlso (Char.IsUpper(cn(i)) OrElse Char.IsNumber(cn(i))) Then
                result += " "
                inCap = True
            Else
                inCap = False
            End If
            result += cn(i)
        Next

        Return result
    End Function

End Class