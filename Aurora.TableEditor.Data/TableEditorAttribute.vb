Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Class TableEditorDataSet

    Partial Class TableEditorAttributeRow

        Public ReadOnly Property TableEditorDataSet() As TableEditorDataSet
            Get
                Return CType(Me.Table.DataSet, TableEditorDataSet)
            End Get
        End Property

        Public ReadOnly Property InnerSelectName() As String
            Get
                Return Me.teaFieldName
            End Get
        End Property

        Public ReadOnly Property OuterSelectName() As String
            Get
                Return "Attribute" & Me.teaId.ToString()
            End Get
        End Property

        Public ReadOnly Property InnerInnerSelectName() As String
            Get
                Return Me.TableEditorEntityRow.InnerSelectTableName & ".[" & Me.teaFieldName & "]"
            End Get
        End Property

        Public ReadOnly Property OuterInnerSelectName() As String
            Get
                Return Me.TableEditorEntityRow.OuterSelectTableName & ".[" & Me.teaFieldName & "]"
            End Get
        End Property

        Public ReadOnly Property QueryStringName() As String
            Get
                Return "Attribute" & Me.teaId.ToString()
            End Get
        End Property

        Public ReadOnly Property OrderByName() As String
            Get
                Return OuterInnerSelectName
            End Get
        End Property

        Public ReadOnly Property UpdateName() As String
            Get
                Return "[" & Me.teaFieldName & "]"
            End Get
        End Property

        Public ReadOnly Property ID() As String
            Get
                Return "Attribute" & Me.teaId.ToString()
            End Get
        End Property

        Public ReadOnly Property SelectParamName() As String
            Get
                Return "@Attribute" & Me.teaId.ToString()
            End Get
        End Property

        Public ReadOnly Property OldUpdateParamName() As String
            Get
                Return "@Old_" & Me.teaFieldName.ToString()
            End Get
        End Property

        Public ReadOnly Property NewUpdateParamName() As String
            Get
                Return "@" & Me.teaFieldName.ToString()
            End Get
        End Property

        Public ReadOnly Property FilterExactParamName() As String
            Get
                Return "@FilterExact" & Me.teaId.ToString()
            End Get
        End Property

        Public ReadOnly Property FilterLikeParamName() As String
            Get
                Return "@FilterLike" & Me.teaId.ToString()
            End Get
        End Property

        Public ReadOnly Property ParentAttribute() As TableEditorAttributeRow
            Get
                If IsteaParentIdNull() Then Return Nothing

                Dim parentEntity As TableEditorEntityRow = Me.TableEditorEntityRow.TableEditorEntityRowParent
                If parentEntity Is Nothing Then Return Nothing

                Return parentEntity.GetAttributeById(Me.teaParentId)
            End Get
        End Property

        Public ReadOnly Property DataType() As Type
            Get
                If Me.TableEditorEntityRow.DataTable.Columns.Contains(Me.OuterSelectName) Then
                    Return Me.TableEditorEntityRow.DataTable.Columns(Me.OuterSelectName).DataType
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property DataValue(ByVal dataRow As DataRow) As Object
            Get
                If dataRow IsNot Nothing _
                 AndAlso dataRow.Table.Columns.Contains(Me.OuterSelectName) _
                 AndAlso Not dataRow.IsNull(Me.OuterSelectName) Then
                    Return dataRow(Me.OuterSelectName)
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property DataValue() As Object
            Get
                Return DataValue(Me.TableEditorEntityRow.DataRow)
            End Get
        End Property

        Public ReadOnly Property DataValueAsString(ByVal dataRow As DataRow) As String
            Get
                Return Aurora.Common.Utility.ObjectValueToString(DataValue(dataRow))
            End Get
        End Property

        Public ReadOnly Property DataValueAsString() As Object
            Get
                Return Aurora.Common.Utility.ObjectValueToString(DataValue())
            End Get
        End Property

        Public ReadOnly Property DefaultValue() As Object
            Get
                If Not Me.IsteaDefaultValueNull Then
                    Return Me.teaDefaultValue
                ElseIf Me.TableEditorEntityRow.DefaultValueDataRow IsNot Nothing _
                 AndAlso Me.TableEditorEntityRow.DefaultValueDataRow.Table.Columns.Contains(Me.InnerSelectName) _
                 AndAlso Not Me.TableEditorEntityRow.DefaultValueDataRow.IsNull(Me.InnerSelectName) Then
                    Return Me.TableEditorEntityRow.DefaultValueDataRow(Me.InnerSelectName)
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property DefaultValueAsString() As String
            Get
                Return Aurora.Common.Utility.ObjectValueToString(DefaultValue())
            End Get
        End Property

        Public Sub Validate()

            If Not ValidateName(Me.teaName) Then
                Throw New ValidationException("Invalid Table Editor Attribute Name: " & Me.teaName, Me)
            End If

            If Not ValidateDatabaseName(Me.teaFieldName) Then
                Throw New ValidationException("Invalid Table Editor Attribute Field Name: " & Me.teaFieldName, Me)
            End If

            If Not Me.IsteaParentIdNull Then
                Dim entityRow As TableEditorEntityRow = Me.TableEditorEntityRow()

                Dim parentEntity As TableEditorEntityRow = entityRow.TableEditorEntityRowParent
                If parentEntity Is Nothing Then
                    Throw New ValidationException("Invalid Table Editor Attribute Parent Relationship: " & Me.teaName, Me)
                End If

                Dim parentAttribute As TableEditorAttributeRow = parentEntity.GetAttributeById(Me.teaParentId)
                If parentAttribute Is Nothing Then
                    Throw New ValidationException("Invalid Table Editor Attribute Parent Relationship: " & Me.teaName, Me)
                End If
            End If

        End Sub

        Private _lookupDataTable As DataTable
        Public ReadOnly Property LookupDataTable() As DataTable
            Get
                If _lookupDataTable IsNot Nothing Then
                    Return _lookupDataTable
                End If

                If Me.IsteaLookupSqlNull Then
                    Return Nothing
                End If

                _lookupDataTable = New DataTable()

                Dim database As Database = Aurora.Common.Data.GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(Me.teaLookupSql)
                    command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                    If Me.TableEditorEntityRow.TableEditorEntityRowParent IsNot Nothing _
                     AndAlso Me.TableEditorEntityRow.TableEditorEntityRowParent.DataRow IsNot Nothing Then
                        For Each a As TableEditorAttributeRow In Me.TableEditorEntityRow.TableEditorEntityRowParent.GetTableEditorAttributeRows()
                            Dim p As DbParameter = command.CreateParameter()
                            p.ParameterName = a.NewUpdateParamName
                            If Not Me.TableEditorEntityRow.TableEditorEntityRowParent.DataRow.IsNull(a.OuterSelectName) Then
                                p.Value = Me.TableEditorEntityRow.TableEditorEntityRowParent.DataRow(a.OuterSelectName)
                            Else
                                p.Value = DBNull.Value
                            End If
                            command.Parameters.Add(p)
                        Next
                    End If

                    TableEditorDataSet.AddEnvironmentParamsToCommand(command)

                    Using da As DbDataAdapter = Aurora.Common.Data.GetDataAdapter(database, command)
                        da.Fill(_lookupDataTable)
                    End Using

                    Return _lookupDataTable
                End Using
            End Get
        End Property

    End Class

End Class
