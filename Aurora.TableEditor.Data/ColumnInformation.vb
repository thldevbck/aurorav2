Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Class TableEditorDataSet

    Partial Class ColumnInformationRow

        Public ReadOnly Property NiceName()
            Get
                If Me.ColumnName.Trim().Length = 0 Then Return Me.ColumnName

                ' put spaces before capitals and numbers
                Dim result As String = TableEditorDataSet.NiceName(Me.ColumnName)

                ' check for any common prefixes
                Dim resultWords As String() = result.Split(" ")
                If resultWords.Length <= 1 Then Return result

                Dim prefix As String = resultWords(0).ToLower()
                Dim count = 0
                For Each ci As ColumnInformationRow In Me.tableColumnInformation
                    If ci.ColumnName.Trim().ToLower().StartsWith(prefix) Then
                        count += 1
                    End If
                Next
                If (count < 2) OrElse Not Me.tableColumnInformation(0).ColumnName.ToLower().StartsWith(prefix) Then Return result

                ' common prefix found, remove it
                result = ""
                For i As Integer = 1 To resultWords.Length - 1
                    result += " " & resultWords(i)
                Next
                Return result.Trim()
            End Get
        End Property


    End Class


End Class