CREATE TABLE [dbo].[TableEditorFunction](
	[tefId] [int] IDENTITY(1,1) NOT NULL,
	[tefCode] [varchar](64) NOT NULL,
	[tefFunCode] [varchar](12) NOT NULL,
	[tefName] [nvarchar](64) NOT NULL,
	[tefDescription] [ntext] NULL,
 CONSTRAINT [PK_TableEditorFunction] PRIMARY KEY CLUSTERED 
(
	[tefId] ASC
) ON [PRIMARY]
)



CREATE TABLE [dbo].[TableEditorAttribute](
	[teaId] [int] IDENTITY(1,1) NOT NULL,
	[tefId] [int] NOT NULL,
	[teeId] [int] NOT NULL,
	[teaParentId] [int] NULL,
	[teaName] [nvarchar](64) NOT NULL,
	[teaDescription] [ntext] NULL,
	[teaFieldName] [nvarchar](64) NOT NULL,
	[teaIsPrimaryKey] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsPrimaryKey]  DEFAULT (0),
	[teaIsFilterable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsFilter]  DEFAULT (1),
	[teaIsListable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaList]  DEFAULT (1),
	[teaIsSortable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsSort]  DEFAULT (1),
	[teaListWidth] [nvarchar](12) NULL,
	[teaListAlign] [nvarchar](12) NULL,
	[teaIsViewable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaCanEdit]  DEFAULT (1),
	[teaIsUpdateable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaCanUpdate]  DEFAULT (1),
	[teaIsInsertable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaCanInsert]  DEFAULT (1),
	[teaIsNullable] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsNullable]  DEFAULT (1),
	[teaIsBoolean] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaIsBoolean]  DEFAULT (0),
	[teaLookupSql] [ntext] NULL,
	[teaTextRowCount] [int] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaTextRowCount]  DEFAULT (1),
	[teaTextWrap] [bit] NOT NULL CONSTRAINT [DF_TableEditorAttribute_teaTextWrap]  DEFAULT (1),
	[teaDefaultValueSql] [ntext] NULL,
 CONSTRAINT [PK_TableEditorAttribute] PRIMARY KEY CLUSTERED 
(
	[teaId] ASC
) ON [PRIMARY]
)


CREATE TABLE [dbo].[TableEditorEntity](
	[teeId] [int] IDENTITY(1,1) NOT NULL,
	[tefId] [int] NOT NULL,
	[teeParentId] [int] NULL,
	[teeName] [nvarchar](64) NOT NULL,
	[teeDescription] [ntext] NULL,
	[teeTableName] [nvarchar](64) NOT NULL,
	[teeSelectSql] [ntext] NULL,
	[teeMustFilter] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeMustFilter]  DEFAULT (1),
	[teeUpdateable] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeCanUpdate]  DEFAULT (1),
	[teeUpdateSql] [ntext] NULL,
	[teeUpdateableFieldName] [nvarchar](64) NULL,
	[teeInsertable] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeInsertable]  DEFAULT (1),
	[teeInsertSql] [ntext] NULL,
	[teeInsertableFieldName] [nvarchar](64) NULL,
	[teeDeleteable] [bit] NOT NULL CONSTRAINT [DF_TableEditorEntity_teeDeleteable]  DEFAULT (1),
	[teeDeleteSql] [ntext] NULL,
	[teeDeleteableFieldName] [nvarchar](64) NULL,
 CONSTRAINT [PK_TableEditorEntity] PRIMARY KEY CLUSTERED 
(
	[teeId] ASC
) ON [PRIMARY]
) 


ALTER TABLE [dbo].[TableEditorAttribute]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorAttribute_TableEditorAttribute] FOREIGN KEY([teaParentId])
REFERENCES [dbo].[TableEditorAttribute] ([teaId])

ALTER TABLE [dbo].[TableEditorAttribute] CHECK CONSTRAINT [FK_TableEditorAttribute_TableEditorAttribute]

ALTER TABLE [dbo].[TableEditorAttribute]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorAttribute_TableEditorEntity] FOREIGN KEY([teeId])
REFERENCES [dbo].[TableEditorEntity] ([teeId])
ON DELETE CASCADE

ALTER TABLE [dbo].[TableEditorAttribute] CHECK CONSTRAINT [FK_TableEditorAttribute_TableEditorEntity]

ALTER TABLE [dbo].[TableEditorAttribute]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorAttribute_TableEditorFunction] FOREIGN KEY([tefId])
REFERENCES [dbo].[TableEditorFunction] ([tefId])

ALTER TABLE [dbo].[TableEditorAttribute] CHECK CONSTRAINT [FK_TableEditorAttribute_TableEditorFunction]

ALTER TABLE [dbo].[TableEditorEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorEntity_TableEditorEntity] FOREIGN KEY([teeParentId])
REFERENCES [dbo].[TableEditorEntity] ([teeId])

ALTER TABLE [dbo].[TableEditorEntity] CHECK CONSTRAINT [FK_TableEditorEntity_TableEditorEntity]

ALTER TABLE [dbo].[TableEditorEntity]  WITH NOCHECK ADD  CONSTRAINT [FK_TableEditorEntity_TableEditorFunction] FOREIGN KEY([tefId])
REFERENCES [dbo].[TableEditorFunction] ([tefId])
ON DELETE CASCADE

ALTER TABLE [dbo].[TableEditorEntity] CHECK CONSTRAINT [FK_TableEditorEntity_TableEditorFunction]





CREATE PROCEDURE TableEditor_AttributeInsert
(
	@tefId AS int,
	@teeId AS int,
	@teaParentId AS int,
	@teaName AS nvarchar(64),
	@teaDescription AS ntext,
	@teaFieldName AS nvarchar(64),
	@teaIsPrimaryKey AS bit,
	@teaIsFilterable AS bit,
	@teaIsListable AS bit,
	@teaIsSortable AS bit,
	@teaListWidth AS nvarchar(12),
	@teaListAlign AS nvarchar(12),
	@teaIsViewable AS bit,
	@teaIsUpdateable AS bit,
	@teaIsInsertable AS bit,
	@teaIsNullable AS bit,
	@teaIsBoolean AS bit,
	@teaLookupSql AS ntext,
	@teaTextRowCount AS int,
	@teaTextWrap AS bit,
	@teaDefaultValueSql AS ntext
)
AS

INSERT INTO TableEditorAttribute
(
	tefId,
	teeId,
	teaParentId,
	teaName,
	teaDescription,
	teaFieldName,
	teaIsPrimaryKey,
	teaIsFilterable,
	teaIsListable,
	teaIsSortable,
	teaListWidth,
	teaListAlign,
	teaIsViewable,
	teaIsUpdateable,
	teaIsInsertable,
	teaIsNullable,
	teaIsBoolean,
	teaLookupSql,
	teaTextRowCount,
	teaTextWrap,
	teaDefaultValueSql 
)
VALUES
(
	@tefId,
	@teeId,
	@teaParentId,
	@teaName,
	@teaDescription,
	@teaFieldName,
	@teaIsPrimaryKey,
	@teaIsFilterable,
	@teaIsListable,
	@teaIsSortable,
	@teaListWidth,
	@teaListAlign,
	@teaIsViewable,
	@teaIsUpdateable,
	@teaIsInsertable,
	@teaIsNullable,
	@teaIsBoolean,
	@teaLookupSql,
	@teaTextRowCount,
	@teaTextWrap,
	@teaDefaultValueSql 
)

SELECT 
	TableEditorAttribute.* 
FROM 
	TableEditorAttribute 
WHERE
	TableEditorAttribute.teaId = SCOPE_IDENTITY()






CREATE PROCEDURE TableEditor_EntityInsert
(
	@tefId AS int,
	@teeParentId AS int,
	@teeName AS  nvarchar(64),
	@teeDescription AS  ntext,
	@teeTableName AS nvarchar(64),
	@teeSelectSql AS ntext,
	@teeMustFilter AS bit,
	@teeUpdateable AS bit,
	@teeUpdateSql AS ntext,
	@teeUpdateableFieldName AS nvarchar(64),
	@teeInsertable AS bit,
	@teeInsertSql AS ntext,
	@teeInsertableFieldName AS nvarchar(64),
	@teeDeleteable AS bit,
	@teeDeleteSql AS ntext,
	@teeDeleteableFieldName AS nvarchar(64)
)
AS

INSERT INTO TableEditorEntity
(
	tefId,
	teeParentId,
	teeName,
	teeDescription,
	teeTableName,
	teeSelectSql,
	teeMustFilter,
	teeUpdateable,
	teeUpdateSql,
	teeUpdateableFieldName,
	teeInsertable,
	teeInsertSql,
	teeInsertableFieldName,
	teeDeleteable,
	teeDeleteSql,
	teeDeleteableFieldName
)
VALUES
(
	@tefId,
	@teeParentId,
	@teeName,
	@teeDescription,
	@teeTableName,
	@teeSelectSql,
	@teeMustFilter,
	@teeUpdateable,
	@teeUpdateSql,
	@teeUpdateableFieldName,
	@teeInsertable,
	@teeInsertSql,
	@teeInsertableFieldName,
	@teeDeleteable,
	@teeDeleteSql,
	@teeDeleteableFieldName
)

SELECT 
	TableEditorEntity.* 
FROM 
	TableEditorEntity 
WHERE
	TableEditorEntity.teeId = SCOPE_IDENTITY()
	
	
	
	
CREATE PROCEDURE TableEditor_FunctionInsert
(
	@tefCode AS varchar(64),
    @tefFunCode  AS varchar(12),
    @tefName AS nvarchar(64),
    @tefDescription AS ntext
)
AS

INSERT INTO TableEditorFunction
(
	tefCode,
	tefFunCode,
    tefName,
	tefDescription
)
VALUES
(
	@tefCode,
    @tefFunCode, 
    @tefName, 
    @tefDescription
)

SELECT 
	TableEditorFunction.* 
FROM 
	TableEditorFunction 
WHERE
	TableEditorFunction.tefId = SCOPE_IDENTITY()	
	
	
	
	
CREATE PROCEDURE dbo.TableEditor_GetAttributes
(
	@code varchar(64)
)
AS
	SELECT 
		TableEditorAttribute.*
	FROM
		TableEditorAttribute 
		INNER JOIN TableEditorEntity ON TableEditorEntity.teeId = TableEditorAttribute.teeId
		INNER JOIN TableEditorFunction ON TableEditorFunction.tefId = TableEditorEntity.tefId
	WHERE
		TableEditorFunction.tefCode = @code

	
	
	
	
CREATE PROCEDURE dbo.TableEditor_GetColumnInformation
(
	@tableName AS nvarchar(256)
)
AS

SELECT 
	c.COLUMN_NAME AS ColumnName, 
	CAST (CASE WHEN ISNULL (tc.CONSTRAINT_TYPE, '') = 'PRIMARY KEY' THEN 1 ELSE 0 END AS bit) AS IsPrimaryKey,
	CAST (CASE WHEN c.IS_NULLABLE = 'NO' THEN 0 ELSE 1 END AS bit) AS IsNullable, 
	COLUMNPROPERTY (OBJECT_ID (c.TABLE_NAME), c.COLUMN_NAME, 'IsIdentity') AS IsIdentity,
	ISNULL (c.DATA_TYPE, '') AS DataType, 
	ISNULL (c.CHARACTER_MAXIMUM_LENGTH, 0) AS MaxLength
	
FROM 
	INFORMATION_SCHEMA.COLUMNS c 
	LEFT JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE kcu on c.COLUMN_NAME = kcu.COLUMN_NAME AND c.TABLE_NAME = kcu.TABLE_NAME 
	LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc on kcu.CONSTRAINT_NAME = tc.CONSTRAINT_NAME 
WHERE 
	c.TABLE_NAME = @tableName 
ORDER 
	BY c.ORDINAL_POSITION	
	
	
	
	
CREATE PROCEDURE dbo.TableEditor_GetEntities
(
	@code varchar(64)
)
AS
	SELECT 
		TableEditorEntity.*
	FROM
		TableEditorEntity 
		INNER JOIN TableEditorFunction ON TableEditorFunction.tefId = TableEditorEntity.tefId
	WHERE
		TableEditorFunction.tefCode = @code



CREATE PROCEDURE dbo.TableEditor_GetFunction 
(
	@code varchar(64)
)
AS
	SELECT 
		TableEditorFunction.*
	FROM 
		TableEditorFunction
	WHERE
		TableEditorFunction.tefCode = @code


CREATE PROCEDURE dbo.TableEditor_GetFunctions 
AS
	SELECT 
		TableEditorFunction.*
	FROM 
		TableEditorFunction
	ORDER BY
		TableEditorFunction.tefCode



CREATE PROCEDURE dbo.TableEditor_GetTableNames

AS

	SELECT 
		CAST (sysobjects.id AS varchar(20)) AS Value,
		sysobjects.name AS Text
	FROM 
		sysobjects
	WHERE 
		sysobjects.xtype = 'U' 
	ORDER BY 
		sysobjects.name