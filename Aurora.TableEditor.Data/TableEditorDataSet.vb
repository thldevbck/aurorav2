﻿Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Class TableEditorDataSet

    Public ReadOnly Property RootFunction() As TableEditorFunctionRow
        Get
            Return TableEditorFunction.Rows(0)
        End Get
    End Property

    Public ReadOnly Property RootEntity() As TableEditorEntityRow
        Get
            For Each t As TableEditorEntityRow In Me.TableEditorEntity
                If t.IsteeParentIdNull() Then
                    Return t
                End If
            Next
            Return Nothing
        End Get
    End Property

    Public Function GetEntityByName(ByVal name As String) As TableEditorEntityRow
        Return RootFunction.GetEntityByName(name)
    End Function


    Public Sub Validate()
        ' ensure there is only one root table editor function
        If TableEditorFunction.Rows.Count = 0 Then
            Throw New ValidationException("No TableEditor Function Defined")
        ElseIf TableEditorFunction.Rows.Count > 1 Then
            Throw New ValidationException("Duplicate Table Editor Functions")
        ElseIf Me.TableEditorEntity.Count <> RootFunction.GetTableEditorEntityRows().Length Then
            Throw New ValidationException("Not all Table Editor Entities are associated with the Function")
        End If

        RootFunction.Validate()
    End Sub


    Private _currentEntity As TableEditorEntityRow
    Public ReadOnly Property CurrentEntity() As TableEditorEntityRow
        Get
            Return _currentEntity
        End Get
    End Property

    Private _environmentParams As Dictionary(Of String, Object)
    Public ReadOnly Property EnvironmentParams() As Dictionary(Of String, Object)
        Get
            Return _environmentParams
        End Get
    End Property


    Public Sub AddEnvironmentParamsToCommand(ByVal dbCommand As DbCommand)
        If dbCommand Is Nothing Then Throw New ArgumentNullException("dbCommand")
        If _environmentParams Is Nothing Then Return

        For Each key As String In _environmentParams.Keys
            For Each p As DbParameter In dbCommand.Parameters
                If p.ParameterName = key Then Continue For
            Next

            Dim dbParameter As DbParameter = dbCommand.CreateParameter()
            dbParameter.ParameterName = key
            If _environmentParams(key) IsNot Nothing Then
                dbParameter.Value = _environmentParams(key)
            Else
                dbParameter.Value = DBNull.Value
            End If
            dbCommand.Parameters.Add(dbParameter)
        Next
    End Sub


    Public Shared Function LoadFromParams(ByVal params As NameValueCollection) As TableEditorDataSet

        If params Is Nothing Then Throw New ArgumentNullException("params")

        Dim code As String = params.Get(CodeParam)
        If String.IsNullOrEmpty(code) Then Throw New ValidationException("Table Editor Function Code not specified")
        code = System.Web.HttpContext.Current.Server.UrlDecode(code)

        Dim result As TableEditorDataSet = DataRepository.GetTableEditor(code)
        If result Is Nothing OrElse result.Tables.Count = 0 Then Throw New ValidationException("Table Editor Function Code is invalid")

        result.Validate()
        Return result
    End Function

    Public Sub LoadDataFromParams(ByVal params As NameValueCollection, ByVal environmentParams As Dictionary(Of String, Object))

        _environmentParams = environmentParams

        Dim entityName As String = params.Get(EntityParam)
        If String.IsNullOrEmpty(entityName) Then
            Me.RootEntity.LoadDataFromParams(New NameValueCollection())

            For Each tableEditorEntityRowChild As TableEditorEntityRow In Me.RootEntity.GetTableEditorEntityRows()
                tableEditorEntityRowChild.LoadDataFromParams(New NameValueCollection())
            Next
        Else
            entityName = System.Web.HttpContext.Current.Server.UrlDecode(entityName)
            _currentEntity = GetEntityByName(entityName)
            If _currentEntity Is Nothing Then Throw New ValidationException("Invalid Table Editor Entity specified")

            Dim tableEditorEntityRow As TableEditorEntityRow = _currentEntity
            While tableEditorEntityRow IsNot Nothing
                tableEditorEntityRow.LoadDataFromParams(params)
                tableEditorEntityRow = tableEditorEntityRow.TableEditorEntityRowParent
            End While

            For Each tableEditorEntityRowChild As TableEditorEntityRow In _currentEntity.GetTableEditorEntityRows()
                tableEditorEntityRowChild.LoadDataFromParams(New NameValueCollection())
            Next
        End If

    End Sub

End Class
