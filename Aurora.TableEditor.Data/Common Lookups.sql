SELECT CtyCode AS Value, CtyCode + ' - ' + CtyName AS Text
FROM Country
WHERE CtyHasProducts = 1
ORDER BY Value

SELECT BrdCode AS Value, BrdCode + ' - ' + BrdName AS Text
FROM Brand
ORDER BY Value

SELECT ComCode AS Value, ComCode + ' - ' + ComName AS Text 
FROM Company
ORDER BY Value

SELECT DISTINCT DATEPART (yy, FwnTrvYearStart) AS Value, DATEPART (yy, FwnTrvYearStart) AS Text
FROM FlexWeekNumber
ORDER BY Value

SELECT DISTINCT PrdShortName AS Value, PrdShortName + ' - ' + PrdName AS Text
FROM FlexProductsView
ORDER BY Text

SELECT ClaID AS Value, ClaCode + ' - ' + ClaDesc AS Text 
FROM Class
WHERE ClaCode IN ('AV', 'AC')
ORDER BY Text

SELECT TypId AS Value, Type.TypCode + ' - ' + Type.TypDesc AS Text
FROM 
	Type
	INNER JOIN Class ON Class.ClaID = Type.TypClaId
WHERE Class.ClaCode IN ('AV', 'AC')
ORDER BY Text


SELECT DISTINCT PrdId AS Value, PrdShortName + ' - ' + PrdName AS Text
FROM FlexProductsView
ORDER BY Text
