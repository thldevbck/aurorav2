''rev:mia March 31 2014 - addition of AIMSPROD
Imports System.Data
Imports System.Data.Common
Imports System.Collections
Imports System.Collections.Specialized
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports System.Xml

Partial Class TableEditorDataSet

    Partial Class TableEditorEntityRow

        Public ReadOnly Property TableEditorDataSet() As TableEditorDataSet
            Get
                Return CType(Me.Table.DataSet, TableEditorDataSet)
            End Get
        End Property

        Public Function GetAttributeById(ByVal id As Integer) As TableEditorAttributeRow
            For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                If a.teaId = id Then
                    Return a
                End If
            Next
            Return Nothing
        End Function

        Public ReadOnly Property InnerSelectTableName() As String
            Get
                Return "[" & Me.teeTableName & "]"
            End Get
        End Property

        Public ReadOnly Property OuterSelectTableName() As String
            Get
                Return "Table" & Me.teeId.ToString()
            End Get
        End Property

        Public ReadOnly Property UpdateTableName() As String
            Get
                Return "[" & Me.teeTableName & "]"
            End Get
        End Property

        Public ReadOnly Property IsInsertable()
            Get
                If Not Me.teeInsertable Then Return False

                Dim count As Integer = 0
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsInsertable Then count += 1
                Next
                If count = 0 Then Return False

                If Not Me.IsteeUpdateableFieldNameNull Then
                    If TableEditorEntityRowParent Is Nothing OrElse TableEditorEntityRowParent.DataRow Is Nothing Then Return False

                    If TableEditorEntityRowParent.DataRow.IsNull(Me.teeInsertableFieldName) Then Return True

                    Try
                        Return CType(TableEditorEntityRowParent.DataRow(Me.teeUpdateableFieldName), Integer) <> 0
                    Catch ex As Exception
                        Return False
                    End Try
                Else
                    Return True
                End If

                Return True
            End Get
        End Property

        Public ReadOnly Property IsUpdateable()
            Get
                If Not Me.teeUpdateable OrElse DataRow Is Nothing Then Return False

                Dim count As Integer = 0
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsUpdateable Then count += 1
                Next
                If count = 0 Then Return False

                If Not Me.IsteeUpdateableFieldNameNull Then
                    If DataRow.IsNull(Me.teeUpdateableFieldName) Then Return True

                    Try
                        Return CType(DataRow(Me.teeUpdateableFieldName), Integer) <> 0
                    Catch ex As Exception
                        Return False
                    End Try
                Else
                    Return True
                End If
            End Get
        End Property

        Public ReadOnly Property IsDeletable()
            Get
                If Not Me.teeDeleteable OrElse DataRow Is Nothing Then Return False

                If Not Me.IsteeDeleteableFieldNameNull Then
                    If DataRow.IsNull(Me.teeDeleteableFieldName) Then Return True

                    Try
                        Return CType(DataRow(Me.teeDeleteableFieldName), Integer) <> 0
                    Catch ex As Exception
                        Return False
                    End Try
                Else
                    Return True
                End If
            End Get
        End Property

        Public ReadOnly Property IsSortable() As Boolean
            Get
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsSortable Then Return True
                Next
                Return False
            End Get
        End Property

        Public ReadOnly Property IsFilterable() As Boolean
            Get
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsFilterable Then Return True
                Next
                Return False
            End Get
        End Property

        Public ReadOnly Property PrimaryKeyAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsPrimaryKey Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property ParentIdAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If Not a.IsteaParentIdNull Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property InsertableAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsInsertable Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property FilterableAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsFilterable Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property UpdateableAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsUpdateable Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property ListableAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsListable Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property SortableAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If a.teaIsSortable Then result.Add(a)
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property AllParentEntities() As TableEditorEntityRow()
            Get
                Dim result As New List(Of TableEditorEntityRow)
                Dim parentRow As TableEditorEntityRow = Me.TableEditorEntityRowParent
                While parentRow IsNot Nothing
                    result.Insert(0, parentRow)
                    parentRow = parentRow.TableEditorEntityRowParent
                End While
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property AllEntities() As TableEditorEntityRow()
            Get
                Dim result As New List(Of TableEditorEntityRow)
                result.AddRange(AllParentEntities())
                result.Add(Me)
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property AllParentAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each e As TableEditorEntityRow In AllParentEntities
                    result.AddRange(e.GetTableEditorAttributeRows())
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property AllAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each e As TableEditorEntityRow In AllEntities
                    result.AddRange(e.GetTableEditorAttributeRows())
                Next
                Return result.ToArray()
            End Get
        End Property


        Public ReadOnly Property AllParentPrimaryKeyAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                For Each e As TableEditorEntityRow In AllParentEntities
                    result.AddRange(e.PrimaryKeyAttributes())
                Next
                Return result.ToArray()
            End Get
        End Property

        Public ReadOnly Property AllPrimaryKeyAttributes() As TableEditorAttributeRow()
            Get
                Dim result As New List(Of TableEditorAttributeRow)
                result.AddRange(PrimaryKeyAttributes())
                result.AddRange(AllParentPrimaryKeyAttributes())
                Return result.ToArray()
            End Get
        End Property



        ''rev:mia March 31 2014 - addition of AIMSPROD
        Private Function GetAIMSTableName(tablename As String) As String
            Dim xml As New XmlDocument
            Try
                xml.Load(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/AIMSPROD.Tables.xml"))
                Return xml.SelectSingleNode(String.Concat("Tables/Name_", tablename)).InnerText
            Catch ex As Exception
                Return ""
            End Try
        End Function

        Private Function PrefixAIMSDatabase(tablename As String) As String
            tablename = tablename.Replace("[", "").Replace("]", "").ToUpper.Trim
            Return IIf(Not String.IsNullOrEmpty(GetAIMSTableName(tablename)), "AIMSPROD..", "")
        End Function

        Private ReadOnly Property InnerSelectSql() As String
            Get
                If Not Me.IsteeSelectSqlNull Then Return Me.teeSelectSql

                Dim result As New StringBuilder()
                Dim first As Boolean

                result.AppendLine(vbTab & vbTab & "SELECT")
                first = True
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If first Then
                        first = False
                    Else
                        result.AppendLine(",")
                    End If
                    result.Append(vbTab & vbTab & vbTab & a.InnerInnerSelectName)
                Next
                result.AppendLine()
                result.AppendLine(vbTab & vbTab & "FROM")
                ''rev:mia March 31 2014 - addition of AIMSPROD
                result.AppendLine(vbTab & vbTab & vbTab & String.Concat(PrefixAIMSDatabase(Me.InnerSelectTableName), Me.InnerSelectTableName))

                Return result.ToString()
            End Get
        End Property

        Private ReadOnly Property OuterSelectSql() As String
            Get
                Dim result As New StringBuilder()
                Dim first As Boolean
                Dim childRow As TableEditorEntityRow
                Dim parentRow As TableEditorEntityRow

                ' SELECT
                result.AppendLine("SELECT")
                first = True
                For Each a As TableEditorAttributeRow In AllAttributes()
                    If first Then
                        first = False
                    Else
                        result.AppendLine(",")
                    End If
                    result.Append(vbTab & a.OuterInnerSelectName & " AS " & a.OuterSelectName)
                Next
                result.AppendLine()


                ' FROM and INNER JOIN's
                result.AppendLine("FROM")
                result.AppendLine(vbTab & "(")
                result.Append(InnerSelectSql())
                result.AppendLine(vbTab & ") AS " & Me.OuterSelectTableName)
                childRow = Me
                parentRow = childRow.TableEditorEntityRowParent
                While parentRow IsNot Nothing
                    result.AppendLine(vbTab & "INNER JOIN")
                    result.AppendLine(vbTab & "(")
                    result.Append(parentRow.InnerSelectSql())
                    result.AppendLine(vbTab & ") AS " & parentRow.OuterSelectTableName)

                    first = True
                    For Each childAttribute As TableEditorAttributeRow In childRow.ParentIdAttributes()
                        Dim parentAttribute As TableEditorAttributeRow = childAttribute.ParentAttribute()
                        If parentAttribute Is Nothing Then Continue For

                        If first Then
                            result.Append(vbTab & vbTab & "ON ")
                            first = False
                        Else
                            result.Append(vbTab & vbTab & "AND ")
                        End If
                        result.AppendLine(parentAttribute.OuterInnerSelectName & " = " & childAttribute.OuterInnerSelectName)
                    Next

                    childRow = parentRow
                    parentRow = childRow.TableEditorEntityRowParent
                End While

                ' WHERE (parent PK's)
                first = True
                childRow = Me
                parentRow = childRow.TableEditorEntityRowParent
                While parentRow IsNot Nothing
                    For Each attribute As TableEditorAttributeRow In parentRow.PrimaryKeyAttributes()
                        If first Then
                            result.AppendLine("WHERE")
                            result.Append(vbTab)
                            first = False
                        Else
                            result.Append(vbTab & "AND ")
                        End If

                        result.AppendLine(attribute.OuterInnerSelectName & " = " & attribute.SelectParamName)
                    Next

                    childRow = parentRow
                    parentRow = childRow.TableEditorEntityRowParent
                End While

                Return result.ToString()
            End Get
        End Property


        Public ReadOnly Property SelectSql()
            Get
                Dim result As New StringBuilder()
                result.Append(OuterSelectSql)

                Dim first As Boolean = Me.IsteeParentIdNull()

                ' WHERE (PK's)
                For Each attribute As TableEditorAttributeRow In Me.PrimaryKeyAttributes()
                    If first Then
                        result.AppendLine("WHERE")
                        result.Append(vbTab)
                        first = False
                    Else
                        result.Append(vbTab & "AND ")
                    End If
                    result.AppendLine(attribute.OuterInnerSelectName & " = " & attribute.SelectParamName)
                Next

                Return result.ToString()
            End Get
        End Property

        Public Function GetSearchSql(ByVal orderByFieldName As String, ByVal orderByDirection As String) As String
            Dim result As New StringBuilder()
            result.Append(OuterSelectSql)

            Dim first As Boolean = Me.IsteeParentIdNull()

            ' WHERE (filter)
            For Each attribute As TableEditorAttributeRow In Me.FilterableAttributes()
                If first Then
                    result.AppendLine("WHERE")
                    result.Append(vbTab)
                    first = False
                Else
                    result.Append(vbTab & "AND ")
                End If
                result.AppendLine("((" & attribute.FilterExactParamName & " IS NULL) OR (" & attribute.OuterInnerSelectName & " = " & attribute.FilterExactParamName & "))")
                result.AppendLine(vbTab & "AND ((" & attribute.FilterLikeParamName & " IS NULL) OR (CAST (" & attribute.OuterInnerSelectName & " AS nvarchar(256)) LIKE ('%' + " & attribute.FilterLikeParamName & " + '%')))")
            Next

            ' ORDER BY
            Dim orderByList As New List(Of String)
            If orderByFieldName IsNot Nothing Then orderByList.Add(orderByFieldName & " " & orderByDirection)
            For Each orderByAttributeRow As TableEditorAttributeRow In Me.SortableAttributes()
                If orderByFieldName Is Nothing OrElse orderByAttributeRow.OrderByName <> orderByFieldName Then
                    orderByList.Add(orderByAttributeRow.OrderByName)
                End If
            Next
            For Each listableAttributeRow As TableEditorAttributeRow In Me.ListableAttributes
                If Not orderByList.Contains(listableAttributeRow.OrderByName) AndAlso (orderByFieldName Is Nothing OrElse listableAttributeRow.OrderByName <> orderByFieldName) Then
                    orderByList.Add(listableAttributeRow.OrderByName)
                End If
            Next
            If orderByList.Count > 0 Then
                result.AppendLine("ORDER BY")
                result.Append(vbTab)
                result.Append(String.Join("," & vbCrLf & vbTab, orderByList.ToArray()))
            End If

            Return result.ToString()
        End Function

        Public ReadOnly Property UpdateSql() As String
            Get
                If Not Me.IsteeUpdateSqlNull Then Return Me.teeUpdateSql

                Dim result As New StringBuilder()
                Dim first As Boolean
                ''rev:mia March 31 2014 - addition of AIMSPROD
                result.AppendLine("UPDATE " & String.Concat(PrefixAIMSDatabase(Me.UpdateTableName), Me.UpdateTableName))
                result.AppendLine("SET")
                first = True
                For Each a As TableEditorAttributeRow In Me.UpdateableAttributes()
                    If Not a.IsteaParentIdNull Then Continue For

                    If first Then
                        first = False
                    Else
                        result.AppendLine(",")
                    End If
                    result.Append(vbTab & a.UpdateName & " = " & a.NewUpdateParamName)
                Next
                result.AppendLine()
                result.AppendLine("FROM")
                ''rev:mia March 31 2014 - addition of AIMSPROD
                result.AppendLine(vbTab & String.Concat(PrefixAIMSDatabase(Me.UpdateTableName), Me.UpdateTableName))
                result.AppendLine("WHERE")
                first = True
                For Each a As TableEditorAttributeRow In Me.PrimaryKeyAttributes()
                    If first Then
                        first = False
                    Else
                        result.Append(vbTab & "AND ")
                    End If
                    result.AppendLine(vbTab & a.UpdateName & " = " & a.OldUpdateParamName)
                Next

                Return result.ToString()
            End Get
        End Property

        Public ReadOnly Property DeleteSql() As String
            Get
                If Not Me.IsteeDeleteSqlNull Then Return Me.teeDeleteSql

                Dim result As New StringBuilder()
                Dim first As Boolean
                ''rev:mia March 31 2014 - addition of AIMSPROD
                result.AppendLine("DELETE FROM " & String.Concat(PrefixAIMSDatabase(Me.UpdateTableName), Me.UpdateTableName))
                result.AppendLine("WHERE")
                first = True
                For Each a As TableEditorAttributeRow In Me.PrimaryKeyAttributes()
                    If first Then
                        first = False
                    Else
                        result.Append(vbTab & "AND ")
                    End If
                    result.AppendLine(vbTab & a.UpdateName & " = " & a.NewUpdateParamName)
                Next

                Return result.ToString()
            End Get
        End Property

        Public ReadOnly Property InsertSql() As String
            Get
                If Not Me.IsteeInsertSqlNull Then Return Me.teeInsertSql

                Dim result As New StringBuilder()
                Dim first As Boolean
                ''rev:mia March 31 2014 - addition of AIMSPROD
                result.AppendLine("INSERT INTO " & String.Concat(PrefixAIMSDatabase(Me.UpdateTableName), Me.UpdateTableName))
                result.AppendLine("(")
                first = True
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If Not a.teaIsInsertable() AndAlso a.IsteaParentIdNull Then Continue For
                    If first Then
                        first = False
                    Else
                        result.AppendLine(",")
                    End If
                    result.Append(vbTab & a.UpdateName)
                Next
                result.AppendLine()
                result.AppendLine(")")
                result.AppendLine("VALUES")
                result.AppendLine("(")
                first = True
                For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    If Not a.teaIsInsertable() AndAlso a.IsteaParentIdNull Then Continue For
                    If first Then
                        first = False
                    Else
                        result.AppendLine(",")
                    End If
                    result.Append(vbTab & a.NewUpdateParamName)
                Next
                result.AppendLine()
                result.AppendLine(")")

                Return result.ToString()
            End Get
        End Property

        Public Sub Validate()

            If Not ValidateName(Me.teeName) Then
                Throw New ValidationException("Invalid Table Editor Entity Name: " & Me.teeName, Me)
            End If

            If Not ValidateDatabaseName(Me.teeTableName) Then
                Throw New ValidationException("Invalid Table Editor Entity Table Name: " & Me.teeTableName, Me)
            End If

            Dim primaryKeyCount As Integer = 0
            Dim isListableCount As Integer = 0
            Dim parentIdCount As Integer = 0
            For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                If a.teaIsPrimaryKey Then primaryKeyCount += 1
                If a.teaIsListable Then isListableCount += 1
                If Not a.IsteaParentIdNull Then parentIdCount += 1
            Next
            If primaryKeyCount = 0 Then
                Throw New ValidationException("No primary key defined for Table Editor Entity : " & Me.teeName, Me)
            ElseIf isListableCount = 0 Then
                Throw New ValidationException("No listable attributes defined for Table Editor Entity : " & Me.teeName, Me)
            ElseIf Me.IsteeParentIdNull AndAlso parentIdCount > 0 Then
                Throw New ValidationException("Root Entity has attributes with a parent relationship: " & Me.teeName, Me)
            ElseIf Not Me.IsteeParentIdNull AndAlso parentIdCount = 0 Then
                Throw New ValidationException("Entity has a parent but no attributes with a parent relationships: " & Me.teeName, Me)
            End If

            ' validate attributes
            For Each a As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                a.Validate()
            Next

            ' ensure no duplicate names & field names
            Dim attributesByName As New Dictionary(Of String, TableEditorAttributeRow)
            Dim attributesByFieldName As New Dictionary(Of String, TableEditorAttributeRow)
            For Each e As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                If Not attributesByName.ContainsKey(e.teeId) Then
                    attributesByName.Add(e.teaFieldName, e)
                Else
                    Throw New ValidationException("Duplicate Attribute Name in Table Editor Entity : " & Me.teeName, Me)
                End If
                If Not attributesByFieldName.ContainsKey(e.teeId) Then
                    attributesByFieldName.Add(e.teaFieldName, e)
                Else
                    Throw New ValidationException("Duplicate Attribute Field Name in Table Editor Entity : " & Me.teeName, Me)
                End If
            Next
        End Sub

        Private _dataTable As New DataTable()
        Public ReadOnly Property DataTable() As DataTable
            Get
                Return _dataTable
            End Get
        End Property

        Private _dataRow As DataRow
        Public ReadOnly Property DataRow() As DataRow
            Get
                Return _dataRow
            End Get
        End Property

        Public ReadOnly Property QueryStringParams(ByVal dataRow As DataRow) As String
            Get
                If dataRow Is Nothing Then Return Nothing

                Dim result As String = Me.TableEditorFunctionRow.QueryStringParams _
                 & "&" & EntityParam & "=" & System.Web.HttpUtility.UrlEncode(Me.teeName)

                For Each attribute As TableEditorAttributeRow In Me.AllPrimaryKeyAttributes()
                    result += "&" & attribute.QueryStringName & "=" & System.Web.HttpUtility.UrlEncode(attribute.DataValue(dataRow).ToString())
                Next

                Return result
            End Get
        End Property

        Public ReadOnly Property QueryStringParams() As String
            Get
                Return QueryStringParams(_dataRow)
            End Get
        End Property


        Public ReadOnly Property ListableText(ByVal dataRow As DataRow) As String
            Get
                If dataRow Is Nothing Then Return Nothing

                Dim result As String = ""
                For Each attribute As TableEditorAttributeRow In Me.ListableAttributes
                    If Not attribute.DataType Is GetType(Boolean) Then
                        If Not String.IsNullOrEmpty(result) Then result += " ... "

                        ' boolean attributes dont make much sense in descriptions
                        If attribute.DataValue(dataRow) Is Nothing Then
                            result += "NULL"
                        ElseIf attribute.IsteaLookupSqlNull Then
                            result += attribute.DataValueAsString(dataRow)
                        Else
                            result += LookupDataTable.GetTextByValue(attribute.LookupDataTable, dataRow(attribute.OuterSelectName))
                        End If
                    End If
                Next
                Return result
            End Get
        End Property

        Public ReadOnly Property ListableText() As String
            Get
                Return ListableText(_dataRow)
            End Get
        End Property

        Public Sub LoadDataFromParams(ByVal params As NameValueCollection)

            Dim database As Database = Aurora.Common.Data.GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(Me.SelectSql)
                command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                For Each attribute As TableEditorAttributeRow In Me.AllPrimaryKeyAttributes()
                    Dim p As DbParameter = command.CreateParameter()
                    p.ParameterName = attribute.SelectParamName
                    If params(attribute.QueryStringName) IsNot Nothing Then
                        p.Value = System.Web.HttpContext.Current.Server.UrlDecode(params(attribute.QueryStringName))
                    Else
                        p.Value = DBNull.Value
                    End If
                    command.Parameters.Add(p)
                Next

                TableEditorDataSet.AddEnvironmentParamsToCommand(command)

                _dataTable.Clear()
                Using da As DbDataAdapter = Aurora.Common.Data.GetDataAdapter(database, command)
                    da.Fill(_dataTable)
                End Using

                If _dataTable.Rows.Count <= 0 Then
                    _dataRow = Nothing
                ElseIf _dataTable.Rows.Count > 1 Then
                    _dataRow = Nothing
                Else
                    _dataRow = _dataTable.Rows(0)
                End If
            End Using
        End Sub

        Private _searchDataTable As DataTable = New DataTable()
        Public ReadOnly Property SearchDataTable() As DataTable
            Get
                Return _searchDataTable
            End Get
        End Property

        Public Sub LoadSearchDataFromParams(ByVal filterParams As Dictionary(Of String, Object), ByVal orderByFieldName As String, ByVal orderByDirection As String)
            If filterParams Is Nothing Then Throw New ArgumentNullException("filterParams")

            Dim database As Database = Aurora.Common.Data.GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(Me.GetSearchSql(orderByFieldName, orderByDirection))
                command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                ' parent PK parameters
                If Me.TableEditorEntityRowParent IsNot Nothing AndAlso Me.TableEditorEntityRowParent.DataRow IsNot Nothing Then
                    For Each a As TableEditorAttributeRow In Me.AllParentAttributes()
                        Dim p As DbParameter = command.CreateParameter()
                        p.ParameterName = a.SelectParamName
                        If Not Me.TableEditorEntityRowParent.DataRow.IsNull(a.OuterSelectName) Then
                            p.Value = Me.TableEditorEntityRowParent().DataRow(a.OuterSelectName)
                        Else
                            p.Value = DBNull.Value
                        End If
                        command.Parameters.Add(p)
                    Next
                End If

                ' filter parameters
                For Each attribute As TableEditorAttributeRow In Me.FilterableAttributes()
                    For Each paramName As String In New String() {attribute.FilterExactParamName, attribute.FilterLikeParamName}
                        Dim p As DbParameter = command.CreateParameter()
                        p.ParameterName = paramName
                        If filterParams.ContainsKey(paramName) Then
                            p.Value = filterParams(paramName)
                        Else
                            p.Value = DBNull.Value
                        End If
                        command.Parameters.Add(p)
                    Next
                Next

                TableEditorDataSet.AddEnvironmentParamsToCommand(command)

                _searchDataTable.Clear()
                Using da As DbDataAdapter = Aurora.Common.Data.GetDataAdapter(database, command)
                    da.Fill(_searchDataTable)
                End Using
            End Using
        End Sub


        Private _defaultValueDataRow As DataRow = Nothing
        Public ReadOnly Property DefaultValueDataRow() As DataRow
            Get
                If _defaultValueDataRow IsNot Nothing Then
                    Return _defaultValueDataRow
                End If

                If Me.IsteeDefaultSqlNull Then
                    Return Nothing
                End If

                Dim database As Database = Aurora.Common.Data.GetDatabase()
                Using command As DbCommand = database.GetSqlStringCommand(Me.teeDefaultSql)
                    command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                    If Me.TableEditorEntityRowParent IsNot Nothing _
                     AndAlso Me.TableEditorEntityRowParent.DataRow IsNot Nothing Then
                        For Each a As TableEditorAttributeRow In Me.TableEditorEntityRowParent.GetTableEditorAttributeRows()
                            Dim p As DbParameter = command.CreateParameter()
                            p.ParameterName = a.NewUpdateParamName
                            If Not Me.TableEditorEntityRowParent.DataRow.IsNull(a.OuterSelectName) Then
                                p.Value = Me.TableEditorEntityRowParent.DataRow(a.OuterSelectName)
                            Else
                                p.Value = DBNull.Value
                            End If
                            command.Parameters.Add(p)
                        Next
                    End If

                    TableEditorDataSet.AddEnvironmentParamsToCommand(command)

                    Dim result As New DataTable()
                    Using da As DbDataAdapter = Aurora.Common.Data.GetDataAdapter(database, command)
                        da.Fill(result)
                    End Using

                    If result.Rows.Count = 1 Then
                        _defaultValueDataRow = result.Rows(0)
                    End If

                    Return _defaultValueDataRow
                End Using
            End Get
        End Property


        Public Sub UpdateDataRow(ByVal updateParams As Dictionary(Of String, Object))
            ''REV:MIA AMEX SURCHARGE
            Dim tempvalue As String = ""
            Dim tempvalueqry As String = ""

            Dim database As Database = Aurora.Common.Data.GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(Me.UpdateSql)
                command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                For Each attribute As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    For Each paramName As String In New String() {attribute.OldUpdateParamName, attribute.NewUpdateParamName}
                        Dim p As DbParameter = command.CreateParameter()
                        p.ParameterName = paramName
                        p.Value = _dataRow(attribute.OuterSelectName)
                        If paramName = attribute.NewUpdateParamName AndAlso updateParams.ContainsKey(attribute.NewUpdateParamName) Then
                            p.Value = updateParams(attribute.NewUpdateParamName)
                        End If
                        command.Parameters.Add(p)
                    Next
                Next

                TableEditorDataSet.AddEnvironmentParamsToCommand(command)

                Aurora.Common.Data.ExecuteNonQuery(database, command)

                ''REV:MIA AMEX SURCHARGE
                If command.Parameters.Contains("@PtmId") Then
                    tempvalue = command.Parameters("@PtmId").Value.ToString
                    tempvalueqry = command.CommandText
                End If
                If command.Parameters.Contains("@LocZoneCode") Then
                    tempvalue = command.Parameters("@LocZoneCode").Value
                    tempvalueqry = command.CommandText
                End If
                ' update the data with the new values
                For Each attribute As TableEditorAttributeRow In Me.UpdateableAttributes()
                    If Not attribute.IsteaParentIdNull Then Continue For

                    If updateParams.ContainsKey(attribute.NewUpdateParamName) Then
                        _dataRow(attribute.OuterSelectName) = updateParams(attribute.NewUpdateParamName)
                    End If
                Next
            End Using

            ''REV:MIA AMEX SURCHARGE
            If tempvalueqry.Contains("PaymentMethod") AndAlso Not String.IsNullOrEmpty(tempvalue) Then
                If Not tempvalueqry.Contains("UPDATE") Then Exit Sub
                Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateSurchargeSplit = True
                Dim blnActive As Boolean = Aurora.TableEditor.Data.DataRepository.ActiveSplitSurchargeValue
                Dim blnSurchargeRefund As Boolean = Aurora.TableEditor.Data.DataRepository.ActiveRefundSurchargeValue
                Aurora.TableEditor.Data.DataRepository.UpdateSplitSurchargeValue(tempvalue, blnActive, blnSurchargeRefund)
            End If
            If tempvalueqry.Contains("LocationZone") AndAlso Not String.IsNullOrEmpty(tempvalue) Then
                If Not tempvalueqry.Contains("UPDATE") Then Exit Sub
                Dim blnActive As Boolean = Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue
                Aurora.TableEditor.Data.DataRepository.UpdateLocationActiveZone(tempvalue, blnActive)
            End If
        End Sub

        Public Sub DeleteDataRow()
            Dim database As Database = Aurora.Common.Data.GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(Me.DeleteSql)
                command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                ' delete parameters
                For Each attribute As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    Dim p As DbParameter = command.CreateParameter()
                    p.ParameterName = attribute.NewUpdateParamName
                    If Not _dataRow.IsNull(attribute.OuterSelectName) Then
                        p.Value = _dataRow(attribute.OuterSelectName)
                    Else
                        p.Value = DBNull.Value
                    End If
                    command.Parameters.Add(p)
                Next

                TableEditorDataSet.AddEnvironmentParamsToCommand(command)

                Aurora.Common.Data.ExecuteNonQuery(database, command)
            End Using
        End Sub

        Public Sub InsertDataRow(ByVal insertParams As Dictionary(Of String, Object))
            ''REV:MIA AMEX SURCHARGE
            Dim tempvalue As String = ""
            Dim tempvalueqry As String = ""

            Dim database As Database = Aurora.Common.Data.GetDatabase()
            Using command As DbCommand = database.GetSqlStringCommand(Me.InsertSql)
                command.CommandTimeout = Aurora.Common.Data.CommandTimeout

                ' insert parameters
                For Each attribute As TableEditorAttributeRow In Me.GetTableEditorAttributeRows()
                    Dim p As DbParameter = command.CreateParameter()
                    p.ParameterName = attribute.NewUpdateParamName
                    If insertParams.ContainsKey(attribute.NewUpdateParamName) Then
                        p.Value = insertParams(attribute.NewUpdateParamName)
                    ElseIf attribute.DefaultValue IsNot Nothing Then
                        p.Value = attribute.DefaultValue
                    Else
                        p.Value = DBNull.Value
                    End If

                    command.Parameters.Add(p)
                Next

                TableEditorDataSet.AddEnvironmentParamsToCommand(command)
                Aurora.Common.Data.ExecuteNonQuery(database, command)

                ''REV:MIA AMEX SURCHARGE
                If command.Parameters.Contains("@PtmId") Then
                    tempvalue = command.Parameters("@PtmId").Value.ToString
                    tempvalueqry = command.CommandText
                End If
                If command.Parameters.Contains("@LocZoneCode") Then
                    tempvalue = command.Parameters("@LocZoneCode").Value
                    tempvalueqry = command.CommandText
                End If
            End Using

            ''REV:MIA AMEX SURCHARGE
            If tempvalueqry.Contains("PaymentMethod") AndAlso Not String.IsNullOrEmpty(tempvalue) Then
                If Not tempvalueqry.Contains("INSERT INTO PaymentMethod") Then Exit Sub
                Dim blnActive As Boolean = Aurora.TableEditor.Data.DataRepository.ActiveSplitSurchargeValue
                Dim blnSurchargeRefund As Boolean = Aurora.TableEditor.Data.DataRepository.ActiveRefundSurchargeValue
                Aurora.TableEditor.Data.DataRepository.UpdateSplitSurchargeValue(tempvalue, blnActive, blnSurchargeRefund)
            End If

            If tempvalueqry.Contains("LocationZone") AndAlso Not String.IsNullOrEmpty(tempvalue) Then
                If Not tempvalueqry.Contains("INSERT INTO LocationZone") Then Exit Sub
                Dim blnActive As Boolean = Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue
                Aurora.TableEditor.Data.DataRepository.UpdateLocationActiveZone(tempvalue, blnActive)
            End If
        End Sub

    End Class

End Class
