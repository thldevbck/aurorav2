-- FlexBookingWeek

-- SelectSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252
FROM
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
WHERE
	Table18.[FbwId] = @Attribute239

-- SearchSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252
FROM
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
ORDER BY
	Table18.[FbwBookStart]

-- UpdateSql: 
UPDATE [FlexBookingWeek]
SET
	[FbwComCode] = @FbwComCode,
	[FbwBookStart] = @FbwBookStart,
	[FbwTravelYearStart] = @FbwTravelYearStart,
	[FbwTravelYearEnd] = @FbwTravelYearEnd,
	[FbwVersion] = @FbwVersion,
	[FbwStatus] = @FbwStatus,
	[FbwEmailText] = @FbwEmailText,
	[IntegrityNo] = @IntegrityNo,
	[AddUsrId] = @AddUsrId,
	[ModUsrId] = @ModUsrId,
	[AddDateTime] = @AddDateTime,
	[ModDateTime] = @ModDateTime,
	[AddPrgmName] = @AddPrgmName
FROM
	[FlexBookingWeek]
WHERE
	[FbwId] = @Old_FbwId

-- DeleteSql: 
DELETE FROM [FlexBookingWeek]
WHERE
	[FbwId] = @Old_FbwId

-- InsertSql: 
INSERT INTO [FlexBookingWeek]
(
	[FbwComCode],
	[FbwBookStart],
	[FbwTravelYearStart],
	[FbwTravelYearEnd],
	[FbwVersion],
	[FbwStatus],
	[FbwEmailText],
	[IntegrityNo],
	[AddUsrId],
	[ModUsrId],
	[AddDateTime],
	[ModDateTime],
	[AddPrgmName]
)
VALUES
(
	@FbwComCode,
	@FbwBookStart,
	@FbwTravelYearStart,
	@FbwTravelYearEnd,
	@FbwVersion,
	@FbwStatus,
	@FbwEmailText,
	@IntegrityNo,
	@AddUsrId,
	@ModUsrId,
	@AddDateTime,
	@ModDateTime,
	@AddPrgmName
)

-- FlexBookingWeekProduct

-- SelectSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252,
	Table31.[FlpId] AS Attribute455,
	Table31.[FlpFbwId] AS Attribute456,
	Table31.[FlpComCode] AS Attribute457,
	Table31.[FlpCtyCode] AS Attribute458,
	Table31.[FlpBrdCode] AS Attribute459,
	Table31.[FlpTravelYear] AS Attribute460,
	Table31.[FlpPrdId] AS Attribute461,
	Table31.[FlpStatus] AS Attribute462,
	Table31.[FlpLocationsInclude] AS Attribute463,
	Table31.[FlpLocationsExclude] AS Attribute464,
	Table31.[IntegrityNo] AS Attribute465,
	Table31.[AddUsrId] AS Attribute466,
	Table31.[ModUsrId] AS Attribute467,
	Table31.[AddDateTime] AS Attribute468,
	Table31.[ModDateTime] AS Attribute469,
	Table31.[AddPrgmName] AS Attribute470
FROM
	(
		SELECT
			[FlexBookingWeekProduct].[FlpId],
			[FlexBookingWeekProduct].[FlpFbwId],
			[FlexBookingWeekProduct].[FlpComCode],
			[FlexBookingWeekProduct].[FlpCtyCode],
			[FlexBookingWeekProduct].[FlpBrdCode],
			[FlexBookingWeekProduct].[FlpTravelYear],
			[FlexBookingWeekProduct].[FlpPrdId],
			[FlexBookingWeekProduct].[FlpStatus],
			[FlexBookingWeekProduct].[FlpLocationsInclude],
			[FlexBookingWeekProduct].[FlpLocationsExclude],
			[FlexBookingWeekProduct].[IntegrityNo],
			[FlexBookingWeekProduct].[AddUsrId],
			[FlexBookingWeekProduct].[ModUsrId],
			[FlexBookingWeekProduct].[AddDateTime],
			[FlexBookingWeekProduct].[ModDateTime],
			[FlexBookingWeekProduct].[AddPrgmName]
		FROM
			[FlexBookingWeekProduct]
	) AS Table31
	INNER JOIN
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
		ON Table18.[FbwId] = Table31.[FlpFbwId]
WHERE
	Table18.[FbwId] = @Attribute239
	AND Table31.[FlpId] = @Attribute455

-- SearchSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252,
	Table31.[FlpId] AS Attribute455,
	Table31.[FlpFbwId] AS Attribute456,
	Table31.[FlpComCode] AS Attribute457,
	Table31.[FlpCtyCode] AS Attribute458,
	Table31.[FlpBrdCode] AS Attribute459,
	Table31.[FlpTravelYear] AS Attribute460,
	Table31.[FlpPrdId] AS Attribute461,
	Table31.[FlpStatus] AS Attribute462,
	Table31.[FlpLocationsInclude] AS Attribute463,
	Table31.[FlpLocationsExclude] AS Attribute464,
	Table31.[IntegrityNo] AS Attribute465,
	Table31.[AddUsrId] AS Attribute466,
	Table31.[ModUsrId] AS Attribute467,
	Table31.[AddDateTime] AS Attribute468,
	Table31.[ModDateTime] AS Attribute469,
	Table31.[AddPrgmName] AS Attribute470
FROM
	(
		SELECT
			[FlexBookingWeekProduct].[FlpId],
			[FlexBookingWeekProduct].[FlpFbwId],
			[FlexBookingWeekProduct].[FlpComCode],
			[FlexBookingWeekProduct].[FlpCtyCode],
			[FlexBookingWeekProduct].[FlpBrdCode],
			[FlexBookingWeekProduct].[FlpTravelYear],
			[FlexBookingWeekProduct].[FlpPrdId],
			[FlexBookingWeekProduct].[FlpStatus],
			[FlexBookingWeekProduct].[FlpLocationsInclude],
			[FlexBookingWeekProduct].[FlpLocationsExclude],
			[FlexBookingWeekProduct].[IntegrityNo],
			[FlexBookingWeekProduct].[AddUsrId],
			[FlexBookingWeekProduct].[ModUsrId],
			[FlexBookingWeekProduct].[AddDateTime],
			[FlexBookingWeekProduct].[ModDateTime],
			[FlexBookingWeekProduct].[AddPrgmName]
		FROM
			[FlexBookingWeekProduct]
	) AS Table31
	INNER JOIN
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
		ON Table18.[FbwId] = Table31.[FlpFbwId]
WHERE
	Table18.[FbwId] = @Attribute239

-- UpdateSql: 
UPDATE [FlexBookingWeekProduct]
SET
	[FlpFbwId] = @FlpFbwId,
	[FlpComCode] = @FlpComCode,
	[FlpCtyCode] = @FlpCtyCode,
	[FlpBrdCode] = @FlpBrdCode,
	[FlpTravelYear] = @FlpTravelYear,
	[FlpPrdId] = @FlpPrdId,
	[FlpStatus] = @FlpStatus,
	[FlpLocationsInclude] = @FlpLocationsInclude,
	[FlpLocationsExclude] = @FlpLocationsExclude,
	[IntegrityNo] = @IntegrityNo,
	[AddUsrId] = @AddUsrId,
	[ModUsrId] = @ModUsrId,
	[AddDateTime] = @AddDateTime,
	[ModDateTime] = @ModDateTime,
	[AddPrgmName] = @AddPrgmName
FROM
	[FlexBookingWeekProduct]
WHERE
	[FlpId] = @Old_FlpId

-- DeleteSql: 
DELETE FROM [FlexBookingWeekProduct]
WHERE
	[FlpId] = @Old_FlpId

-- InsertSql: 
INSERT INTO [FlexBookingWeekProduct]
(
	[FlpFbwId],
	[FlpComCode],
	[FlpCtyCode],
	[FlpBrdCode],
	[FlpTravelYear],
	[FlpPrdId],
	[FlpStatus],
	[FlpLocationsInclude],
	[FlpLocationsExclude],
	[IntegrityNo],
	[AddUsrId],
	[ModUsrId],
	[AddDateTime],
	[ModDateTime],
	[AddPrgmName]
)
VALUES
(
	@FlpFbwId,
	@FlpComCode,
	@FlpCtyCode,
	@FlpBrdCode,
	@FlpTravelYear,
	@FlpPrdId,
	@FlpStatus,
	@FlpLocationsInclude,
	@FlpLocationsExclude,
	@IntegrityNo,
	@AddUsrId,
	@ModUsrId,
	@AddDateTime,
	@ModDateTime,
	@AddPrgmName
)

-- FlexBookingWeekException

-- SelectSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252,
	Table31.[FlpId] AS Attribute455,
	Table31.[FlpFbwId] AS Attribute456,
	Table31.[FlpComCode] AS Attribute457,
	Table31.[FlpCtyCode] AS Attribute458,
	Table31.[FlpBrdCode] AS Attribute459,
	Table31.[FlpTravelYear] AS Attribute460,
	Table31.[FlpPrdId] AS Attribute461,
	Table31.[FlpStatus] AS Attribute462,
	Table31.[FlpLocationsInclude] AS Attribute463,
	Table31.[FlpLocationsExclude] AS Attribute464,
	Table31.[IntegrityNo] AS Attribute465,
	Table31.[AddUsrId] AS Attribute466,
	Table31.[ModUsrId] AS Attribute467,
	Table31.[AddDateTime] AS Attribute468,
	Table31.[ModDateTime] AS Attribute469,
	Table31.[AddPrgmName] AS Attribute470,
	Table35.[FleId] AS Attribute528,
	Table35.[FleFlpId] AS Attribute529,
	Table35.[FleBookStart] AS Attribute530,
	Table35.[FleBookEnd] AS Attribute531,
	Table35.[FleTravelStart] AS Attribute532,
	Table35.[FleTravelEnd] AS Attribute533,
	Table35.[FleLocCodeFrom] AS Attribute534,
	Table35.[FleLocCodeTo] AS Attribute535,
	Table35.[FleDelta] AS Attribute536
FROM
	(
		SELECT
			[FlexBookingWeekException].[FleId],
			[FlexBookingWeekException].[FleFlpId],
			[FlexBookingWeekException].[FleBookStart],
			[FlexBookingWeekException].[FleBookEnd],
			[FlexBookingWeekException].[FleTravelStart],
			[FlexBookingWeekException].[FleTravelEnd],
			[FlexBookingWeekException].[FleLocCodeFrom],
			[FlexBookingWeekException].[FleLocCodeTo],
			[FlexBookingWeekException].[FleDelta]
		FROM
			[FlexBookingWeekException]
	) AS Table35
	INNER JOIN
	(
		SELECT
			[FlexBookingWeekProduct].[FlpId],
			[FlexBookingWeekProduct].[FlpFbwId],
			[FlexBookingWeekProduct].[FlpComCode],
			[FlexBookingWeekProduct].[FlpCtyCode],
			[FlexBookingWeekProduct].[FlpBrdCode],
			[FlexBookingWeekProduct].[FlpTravelYear],
			[FlexBookingWeekProduct].[FlpPrdId],
			[FlexBookingWeekProduct].[FlpStatus],
			[FlexBookingWeekProduct].[FlpLocationsInclude],
			[FlexBookingWeekProduct].[FlpLocationsExclude],
			[FlexBookingWeekProduct].[IntegrityNo],
			[FlexBookingWeekProduct].[AddUsrId],
			[FlexBookingWeekProduct].[ModUsrId],
			[FlexBookingWeekProduct].[AddDateTime],
			[FlexBookingWeekProduct].[ModDateTime],
			[FlexBookingWeekProduct].[AddPrgmName]
		FROM
			[FlexBookingWeekProduct]
	) AS Table31
		ON Table31.[FlpId] = Table35.[FleFlpId]
	INNER JOIN
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
		ON Table18.[FbwId] = Table31.[FlpFbwId]
WHERE
	Table31.[FlpId] = @Attribute455
	AND Table18.[FbwId] = @Attribute239
	AND Table35.[FleId] = @Attribute528

-- SearchSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252,
	Table31.[FlpId] AS Attribute455,
	Table31.[FlpFbwId] AS Attribute456,
	Table31.[FlpComCode] AS Attribute457,
	Table31.[FlpCtyCode] AS Attribute458,
	Table31.[FlpBrdCode] AS Attribute459,
	Table31.[FlpTravelYear] AS Attribute460,
	Table31.[FlpPrdId] AS Attribute461,
	Table31.[FlpStatus] AS Attribute462,
	Table31.[FlpLocationsInclude] AS Attribute463,
	Table31.[FlpLocationsExclude] AS Attribute464,
	Table31.[IntegrityNo] AS Attribute465,
	Table31.[AddUsrId] AS Attribute466,
	Table31.[ModUsrId] AS Attribute467,
	Table31.[AddDateTime] AS Attribute468,
	Table31.[ModDateTime] AS Attribute469,
	Table31.[AddPrgmName] AS Attribute470,
	Table35.[FleId] AS Attribute528,
	Table35.[FleFlpId] AS Attribute529,
	Table35.[FleBookStart] AS Attribute530,
	Table35.[FleBookEnd] AS Attribute531,
	Table35.[FleTravelStart] AS Attribute532,
	Table35.[FleTravelEnd] AS Attribute533,
	Table35.[FleLocCodeFrom] AS Attribute534,
	Table35.[FleLocCodeTo] AS Attribute535,
	Table35.[FleDelta] AS Attribute536
FROM
	(
		SELECT
			[FlexBookingWeekException].[FleId],
			[FlexBookingWeekException].[FleFlpId],
			[FlexBookingWeekException].[FleBookStart],
			[FlexBookingWeekException].[FleBookEnd],
			[FlexBookingWeekException].[FleTravelStart],
			[FlexBookingWeekException].[FleTravelEnd],
			[FlexBookingWeekException].[FleLocCodeFrom],
			[FlexBookingWeekException].[FleLocCodeTo],
			[FlexBookingWeekException].[FleDelta]
		FROM
			[FlexBookingWeekException]
	) AS Table35
	INNER JOIN
	(
		SELECT
			[FlexBookingWeekProduct].[FlpId],
			[FlexBookingWeekProduct].[FlpFbwId],
			[FlexBookingWeekProduct].[FlpComCode],
			[FlexBookingWeekProduct].[FlpCtyCode],
			[FlexBookingWeekProduct].[FlpBrdCode],
			[FlexBookingWeekProduct].[FlpTravelYear],
			[FlexBookingWeekProduct].[FlpPrdId],
			[FlexBookingWeekProduct].[FlpStatus],
			[FlexBookingWeekProduct].[FlpLocationsInclude],
			[FlexBookingWeekProduct].[FlpLocationsExclude],
			[FlexBookingWeekProduct].[IntegrityNo],
			[FlexBookingWeekProduct].[AddUsrId],
			[FlexBookingWeekProduct].[ModUsrId],
			[FlexBookingWeekProduct].[AddDateTime],
			[FlexBookingWeekProduct].[ModDateTime],
			[FlexBookingWeekProduct].[AddPrgmName]
		FROM
			[FlexBookingWeekProduct]
	) AS Table31
		ON Table31.[FlpId] = Table35.[FleFlpId]
	INNER JOIN
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
		ON Table18.[FbwId] = Table31.[FlpFbwId]
WHERE
	Table31.[FlpId] = @Attribute455
	AND Table18.[FbwId] = @Attribute239

-- UpdateSql: 
UPDATE [FlexBookingWeekException]
SET
	[FleFlpId] = @FleFlpId,
	[FleBookStart] = @FleBookStart,
	[FleBookEnd] = @FleBookEnd,
	[FleTravelStart] = @FleTravelStart,
	[FleTravelEnd] = @FleTravelEnd,
	[FleLocCodeFrom] = @FleLocCodeFrom,
	[FleLocCodeTo] = @FleLocCodeTo,
	[FleDelta] = @FleDelta
FROM
	[FlexBookingWeekException]
WHERE
	[FleId] = @Old_FleId

-- DeleteSql: 
DELETE FROM [FlexBookingWeekException]
WHERE
	[FleId] = @Old_FleId

-- InsertSql: 
INSERT INTO [FlexBookingWeekException]
(
	[FleFlpId],
	[FleBookStart],
	[FleBookEnd],
	[FleTravelStart],
	[FleTravelEnd],
	[FleLocCodeFrom],
	[FleLocCodeTo],
	[FleDelta]
)
VALUES
(
	@FleFlpId,
	@FleBookStart,
	@FleBookEnd,
	@FleTravelStart,
	@FleTravelEnd,
	@FleLocCodeFrom,
	@FleLocCodeTo,
	@FleDelta
)

-- FlexBookingWeekRate

-- SelectSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252,
	Table31.[FlpId] AS Attribute455,
	Table31.[FlpFbwId] AS Attribute456,
	Table31.[FlpComCode] AS Attribute457,
	Table31.[FlpCtyCode] AS Attribute458,
	Table31.[FlpBrdCode] AS Attribute459,
	Table31.[FlpTravelYear] AS Attribute460,
	Table31.[FlpPrdId] AS Attribute461,
	Table31.[FlpStatus] AS Attribute462,
	Table31.[FlpLocationsInclude] AS Attribute463,
	Table31.[FlpLocationsExclude] AS Attribute464,
	Table31.[IntegrityNo] AS Attribute465,
	Table31.[AddUsrId] AS Attribute466,
	Table31.[ModUsrId] AS Attribute467,
	Table31.[AddDateTime] AS Attribute468,
	Table31.[ModDateTime] AS Attribute469,
	Table31.[AddPrgmName] AS Attribute470,
	Table36.[FlrId] AS Attribute537,
	Table36.[FlrFlpId] AS Attribute538,
	Table36.[FlrWkNo] AS Attribute539,
	Table36.[FlrTravelFrom] AS Attribute540,
	Table36.[FlrFlexNum] AS Attribute541,
	Table36.[FlrChanged] AS Attribute542,
	Table36.[FlrFleetStatus] AS Attribute543
FROM
	(
		SELECT
			[FlexBookingWeekRate].[FlrId],
			[FlexBookingWeekRate].[FlrFlpId],
			[FlexBookingWeekRate].[FlrWkNo],
			[FlexBookingWeekRate].[FlrTravelFrom],
			[FlexBookingWeekRate].[FlrFlexNum],
			[FlexBookingWeekRate].[FlrChanged],
			[FlexBookingWeekRate].[FlrFleetStatus]
		FROM
			[FlexBookingWeekRate]
	) AS Table36
	INNER JOIN
	(
		SELECT
			[FlexBookingWeekProduct].[FlpId],
			[FlexBookingWeekProduct].[FlpFbwId],
			[FlexBookingWeekProduct].[FlpComCode],
			[FlexBookingWeekProduct].[FlpCtyCode],
			[FlexBookingWeekProduct].[FlpBrdCode],
			[FlexBookingWeekProduct].[FlpTravelYear],
			[FlexBookingWeekProduct].[FlpPrdId],
			[FlexBookingWeekProduct].[FlpStatus],
			[FlexBookingWeekProduct].[FlpLocationsInclude],
			[FlexBookingWeekProduct].[FlpLocationsExclude],
			[FlexBookingWeekProduct].[IntegrityNo],
			[FlexBookingWeekProduct].[AddUsrId],
			[FlexBookingWeekProduct].[ModUsrId],
			[FlexBookingWeekProduct].[AddDateTime],
			[FlexBookingWeekProduct].[ModDateTime],
			[FlexBookingWeekProduct].[AddPrgmName]
		FROM
			[FlexBookingWeekProduct]
	) AS Table31
		ON Table31.[FlpId] = Table36.[FlrFlpId]
	INNER JOIN
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
		ON Table18.[FbwId] = Table31.[FlpFbwId]
WHERE
	Table31.[FlpId] = @Attribute455
	AND Table18.[FbwId] = @Attribute239
	AND Table36.[FlrId] = @Attribute537

-- SearchSql: 
SELECT
	Table18.[FbwId] AS Attribute239,
	Table18.[FbwComCode] AS Attribute240,
	Table18.[FbwBookStart] AS Attribute241,
	Table18.[FbwTravelYearStart] AS Attribute242,
	Table18.[FbwTravelYearEnd] AS Attribute243,
	Table18.[FbwVersion] AS Attribute244,
	Table18.[FbwStatus] AS Attribute245,
	Table18.[FbwEmailText] AS Attribute246,
	Table18.[IntegrityNo] AS Attribute247,
	Table18.[AddUsrId] AS Attribute248,
	Table18.[ModUsrId] AS Attribute249,
	Table18.[AddDateTime] AS Attribute250,
	Table18.[ModDateTime] AS Attribute251,
	Table18.[AddPrgmName] AS Attribute252,
	Table31.[FlpId] AS Attribute455,
	Table31.[FlpFbwId] AS Attribute456,
	Table31.[FlpComCode] AS Attribute457,
	Table31.[FlpCtyCode] AS Attribute458,
	Table31.[FlpBrdCode] AS Attribute459,
	Table31.[FlpTravelYear] AS Attribute460,
	Table31.[FlpPrdId] AS Attribute461,
	Table31.[FlpStatus] AS Attribute462,
	Table31.[FlpLocationsInclude] AS Attribute463,
	Table31.[FlpLocationsExclude] AS Attribute464,
	Table31.[IntegrityNo] AS Attribute465,
	Table31.[AddUsrId] AS Attribute466,
	Table31.[ModUsrId] AS Attribute467,
	Table31.[AddDateTime] AS Attribute468,
	Table31.[ModDateTime] AS Attribute469,
	Table31.[AddPrgmName] AS Attribute470,
	Table36.[FlrId] AS Attribute537,
	Table36.[FlrFlpId] AS Attribute538,
	Table36.[FlrWkNo] AS Attribute539,
	Table36.[FlrTravelFrom] AS Attribute540,
	Table36.[FlrFlexNum] AS Attribute541,
	Table36.[FlrChanged] AS Attribute542,
	Table36.[FlrFleetStatus] AS Attribute543
FROM
	(
		SELECT
			[FlexBookingWeekRate].[FlrId],
			[FlexBookingWeekRate].[FlrFlpId],
			[FlexBookingWeekRate].[FlrWkNo],
			[FlexBookingWeekRate].[FlrTravelFrom],
			[FlexBookingWeekRate].[FlrFlexNum],
			[FlexBookingWeekRate].[FlrChanged],
			[FlexBookingWeekRate].[FlrFleetStatus]
		FROM
			[FlexBookingWeekRate]
	) AS Table36
	INNER JOIN
	(
		SELECT
			[FlexBookingWeekProduct].[FlpId],
			[FlexBookingWeekProduct].[FlpFbwId],
			[FlexBookingWeekProduct].[FlpComCode],
			[FlexBookingWeekProduct].[FlpCtyCode],
			[FlexBookingWeekProduct].[FlpBrdCode],
			[FlexBookingWeekProduct].[FlpTravelYear],
			[FlexBookingWeekProduct].[FlpPrdId],
			[FlexBookingWeekProduct].[FlpStatus],
			[FlexBookingWeekProduct].[FlpLocationsInclude],
			[FlexBookingWeekProduct].[FlpLocationsExclude],
			[FlexBookingWeekProduct].[IntegrityNo],
			[FlexBookingWeekProduct].[AddUsrId],
			[FlexBookingWeekProduct].[ModUsrId],
			[FlexBookingWeekProduct].[AddDateTime],
			[FlexBookingWeekProduct].[ModDateTime],
			[FlexBookingWeekProduct].[AddPrgmName]
		FROM
			[FlexBookingWeekProduct]
	) AS Table31
		ON Table31.[FlpId] = Table36.[FlrFlpId]
	INNER JOIN
	(
		SELECT
			[FlexBookingWeek].[FbwId],
			[FlexBookingWeek].[FbwComCode],
			[FlexBookingWeek].[FbwBookStart],
			[FlexBookingWeek].[FbwTravelYearStart],
			[FlexBookingWeek].[FbwTravelYearEnd],
			[FlexBookingWeek].[FbwVersion],
			[FlexBookingWeek].[FbwStatus],
			[FlexBookingWeek].[FbwEmailText],
			[FlexBookingWeek].[IntegrityNo],
			[FlexBookingWeek].[AddUsrId],
			[FlexBookingWeek].[ModUsrId],
			[FlexBookingWeek].[AddDateTime],
			[FlexBookingWeek].[ModDateTime],
			[FlexBookingWeek].[AddPrgmName]
		FROM
			[FlexBookingWeek]
	) AS Table18
		ON Table18.[FbwId] = Table31.[FlpFbwId]
WHERE
	Table31.[FlpId] = @Attribute455
	AND Table18.[FbwId] = @Attribute239

-- UpdateSql: 
UPDATE [FlexBookingWeekRate]
SET
	[FlrFlpId] = @FlrFlpId,
	[FlrWkNo] = @FlrWkNo,
	[FlrTravelFrom] = @FlrTravelFrom,
	[FlrFlexNum] = @FlrFlexNum,
	[FlrChanged] = @FlrChanged,
	[FlrFleetStatus] = @FlrFleetStatus
FROM
	[FlexBookingWeekRate]
WHERE
	[FlrId] = @Old_FlrId

-- DeleteSql: 
DELETE FROM [FlexBookingWeekRate]
WHERE
	[FlrId] = @Old_FlrId

-- InsertSql: 
INSERT INTO [FlexBookingWeekRate]
(
	[FlrFlpId],
	[FlrWkNo],
	[FlrTravelFrom],
	[FlrFlexNum],
	[FlrChanged],
	[FlrFleetStatus]
)
VALUES
(
	@FlrFlpId,
	@FlrWkNo,
	@FlrTravelFrom,
	@FlrFlexNum,
	@FlrChanged,
	@FlrFleetStatus
)
