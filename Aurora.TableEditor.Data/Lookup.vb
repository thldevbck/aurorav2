Imports System.Data
Imports System.Data.Common
Imports System.Collections.Generic
Imports System.Text

Imports Aurora.Common

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Class TableEditorDataSet

    Public Class LookupDataTable

        Public Shared Function GetTextByValue(ByVal dataTable As DataTable, ByVal value As Object) As String
            For Each lookupRow As DataRow In dataTable.Rows
                If (value Is Nothing And lookupRow.IsNull("Value")) _
                 OrElse (value IsNot Nothing AndAlso Not lookupRow.IsNull("Value") And Utility.ObjectValueToString(value).ToLower() = Utility.ObjectValueToString(lookupRow("Value")).ToLower()) Then
                    If Not lookupRow.IsNull("Text") Then
                        Return lookupRow("Text")
                    Else
                        Return Nothing
                    End If
                End If
            Next

            Return value
        End Function

    End Class

End Class