Imports System.Data
Imports System.Web
Imports System.Configuration

Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Public Module DataRepository

    Public Function GetTableEditor(ByVal code As String) As TableEditorDataSet

        Dim result As New TableEditorDataSet()

        GetFunction(code, result.TableEditorFunction)
        GetEntities(code, result.TableEditorEntity)
        GetAttributes(code, result.TableEditorAttribute)

        Return result
    End Function


    Public Function GetFunction(ByVal code As String, ByVal functionDataTable As TableEditorFunctionDataTable) As TableEditorFunctionDataTable

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_GetFunction", _
            functionDataTable, _
            code)

        Return functionDataTable
    End Function

    Public Function GetEntities(ByVal code As String, ByVal entityDataTable As TableEditorEntityDataTable) As TableEditorEntityDataTable

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_GetEntities", _
            entityDataTable, _
            code)

        Return entityDataTable
    End Function

    Public Function GetAttributes(ByVal code As String, ByVal attributeDataTable As TableEditorAttributeDataTable) As TableEditorAttributeDataTable

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_GetAttributes", _
            attributeDataTable, _
            code)

        Return attributeDataTable
    End Function

    Public Sub DeleteTableEditor(ByVal tefId As Integer)

        Aurora.Common.Data.ExecuteScalarSP( _
            "TableEditor_DeleteFunction", _
            tefId)
    End Sub

    Public Function GetFunctions() As TableEditorFunctionDataTable

        Dim result As New TableEditorFunctionDataTable()

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_GetFunctions", _
            result)

        Return result
    End Function

    Public Function GetFunctionCodes() As DataTable
        Return Aurora.Common.Data.ExecuteTableSP("TableEditor_GetFunctionCodes", New DataTable())
    End Function

    Public Function GetTableNames() As DataTable
        Return Aurora.Common.Data.ExecuteTableSP("TableEditor_GetTableNames", New DataTable())
    End Function

    Public Function GetColumnInformation(ByVal tableName As String) As ColumnInformationDataTable

        Dim result As New ColumnInformationDataTable()

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_GetColumnInformation", _
            result, _
            tableName)

        Return result

    End Function

    Public Function InsertFunction( _
     ByVal tefCode As String, _
     ByVal tefFunCode As String, _
     ByVal tefName As String, _
     ByVal tefDescription As String) As TableEditorFunctionRow

        Dim result As New TableEditorFunctionDataTable()

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_FunctionInsert", _
            result, _
            IIf(tefCode Is Nothing, DBNull.Value, tefCode), _
            IIf(tefFunCode Is Nothing, DBNull.Value, tefFunCode), _
            IIf(tefName Is Nothing, DBNull.Value, tefName), _
            IIf(tefDescription Is Nothing, DBNull.Value, tefDescription))

        If result.Rows.Count = 1 Then
            Return result(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function InsertEntity( _
     ByVal tefId As Integer, _
     ByVal teeParentId As Nullable(Of Integer), _
     ByVal teeName As String, _
     ByVal teeDescription As String, _
     ByVal teeTableName As String, _
     ByVal teeSelectSql As String, _
     ByVal teeMustFilter As Boolean, _
     ByVal teeUpdateable As Boolean, _
     ByVal teeUpdateSql As String, _
     ByVal teeUpdateableFieldName As String, _
     ByVal teeInsertable As Boolean, _
     ByVal teeInsertSql As String, _
     ByVal teeInsertableFieldName As String, _
     ByVal teeDeleteable As Boolean, _
     ByVal teeDeleteSql As String, _
     ByVal teeDeleteableFieldName As String, _
     ByVal teeDefaultSql As String, _
     ByVal teeOrder As Integer) As TableEditorEntityRow

        Dim result As New TableEditorEntityDataTable()

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_EntityInsert", _
            result, _
            tefId, _
            IIf(Not teeParentId.HasValue, DBNull.Value, teeParentId.GetValueOrDefault()), _
            IIf(teeName Is Nothing, DBNull.Value, teeName), _
            IIf(teeDescription Is Nothing, DBNull.Value, teeDescription), _
            IIf(teeTableName Is Nothing, DBNull.Value, teeTableName), _
            IIf(teeSelectSql Is Nothing, DBNull.Value, teeSelectSql), _
            teeMustFilter, _
            teeUpdateable, _
            IIf(teeUpdateSql Is Nothing, DBNull.Value, teeUpdateSql), _
            IIf(teeUpdateableFieldName Is Nothing, DBNull.Value, teeUpdateableFieldName), _
            teeInsertable, _
            IIf(teeInsertSql Is Nothing, DBNull.Value, teeInsertSql), _
            IIf(teeInsertableFieldName Is Nothing, DBNull.Value, teeInsertableFieldName), _
            teeDeleteable, _
            IIf(teeDeleteSql Is Nothing, DBNull.Value, teeDeleteSql), _
            IIf(teeDeleteableFieldName Is Nothing, DBNull.Value, teeDeleteableFieldName), _
            IIf(teeDefaultSql Is Nothing, DBNull.Value, teeDefaultSql), _
            teeOrder)

        If result.Rows.Count = 1 Then
            Return result(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function InsertAttribute( _
     ByVal tefId As Integer, _
     ByVal teeId As Integer, _
     ByVal teaParentId As Nullable(Of Integer), _
     ByVal teaName As String, _
     ByVal teaDescription As String, _
     ByVal teaFieldName As String, _
     ByVal teaIsPrimaryKey As Boolean, _
     ByVal teaIsFilterable As Boolean, _
     ByVal teaIsListable As Boolean, _
     ByVal teaIsSortable As Boolean, _
     ByVal teaListWidth As String, _
     ByVal teaListAlign As String, _
     ByVal teaIsViewable As Boolean, _
     ByVal teaIsUpdateable As Boolean, _
     ByVal teaIsInsertable As Boolean, _
     ByVal teaIsNullable As Boolean, _
     ByVal teaIsBoolean As Boolean, _
     ByVal teaLookupSql As String, _
     ByVal teaTextRowCount As Nullable(Of Integer), _
     ByVal teaTextWrap As Boolean, _
     ByVal teaDefaultValue As String, _
     ByVal teaOrder As Integer) As TableEditorAttributeRow

        Dim result As New TableEditorAttributeDataTable()

        Aurora.Common.Data.ExecuteTableSP( _
            "TableEditor_AttributeInsert", _
            result, _
            tefId, _
            teeId, _
            IIf(Not teaParentId.HasValue, DBNull.Value, teaParentId.GetValueOrDefault()), _
            IIf(teaName Is Nothing, DBNull.Value, teaName), _
            IIf(teaDescription Is Nothing, DBNull.Value, teaDescription), _
            IIf(teaFieldName Is Nothing, DBNull.Value, teaFieldName), _
            teaIsPrimaryKey, _
            teaIsFilterable, _
            teaIsListable, _
            teaIsSortable, _
            IIf(teaListWidth Is Nothing, DBNull.Value, teaListWidth), _
            IIf(teaListAlign Is Nothing, DBNull.Value, teaListAlign), _
            teaIsViewable, _
            teaIsUpdateable, _
            teaIsInsertable, _
            teaIsNullable, _
            teaIsBoolean, _
            IIf(teaLookupSql Is Nothing, DBNull.Value, teaLookupSql), _
            IIf(Not teaTextRowCount.HasValue, DBNull.Value, teaTextRowCount.GetValueOrDefault()), _
            teaTextWrap, _
            IIf(teaDefaultValue Is Nothing, DBNull.Value, teaDefaultValue), _
            teaOrder)

        If result.Rows.Count = 1 Then
            Return result(0)
        Else
            Return Nothing
        End If
    End Function

    Public Function DeleteDefinition(ByVal TefId As String, ByRef ErrorMessage As String) As Boolean
        Dim sDeleteSQL As System.Text.StringBuilder = New System.Text.StringBuilder

        sDeleteSQL.Append("delete TableEditorAttribute " & vbNewLine)
        sDeleteSQL.Append("where tefid  = " & TefId & ";" & vbNewLine)

        sDeleteSQL.Append("delete TableEditorEntity " & vbNewLine)
        sDeleteSQL.Append("where tefid  = " & TefId & ";" & vbNewLine)

        sDeleteSQL.Append("delete TableEditorFunction " & vbNewLine)
        sDeleteSQL.Append("where tefid = " & TefId & ";")

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                Aurora.Common.Data.ExecuteScalarSQL(sDeleteSQL.ToString())
                oTransaction.CommitTransaction()
                oTransaction.Dispose()
                ErrorMessage = "Definition deleted successfully"
                Return True
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                ErrorMessage = ex.Message
                Try
                    Aurora.Common.Logging.LogInformation("TABLE_EDITOR_DELETE", "FAILED TO DELETE DEFINITION: " & _
                                                                                vbNewLine & sDeleteSQL.ToString() & _
                                                                                vbNewLine & ex.Message)
                Catch
                End Try
                Return False
            End Try
        End Using
    End Function

#Region "rev:mia feb 3 added zone location"
    Public Function InsertLocationZone(ByVal LocZoneCode As String, _
                                       ByVal LocZoneName As String, _
                                       ByVal AddUsrId As String) As String

        Dim retValue As String = ""
        Dim params(3) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocZoneCode", DbType.String, Left(LocZoneCode, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("LocZoneName", DbType.String, Left(LocZoneName, 64), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, Left(AddUsrId, 64), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("retValue", DbType.String, 100, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("res_InsertLocationZone", params)
            retValue = params(3).Value

        Catch ex As Exception
            retValue = "ERROR: Cannot save Zone Name and Location"
        End Try
        Return retValue

    End Function
#End Region

#Region "REV:MIA AMEX SURCHARGE"
    Private _IsNeedToUpdateSurchargeSplit As Boolean

    Public Property IsNeedToUpdateSurchargeSplit() As Boolean
        Get
            Return _IsNeedToUpdateSurchargeSplit
        End Get
        Set(ByVal value As Boolean)
            _IsNeedToUpdateSurchargeSplit = value
        End Set
    End Property

    Private _ActiveSurchargeValue As Boolean
    Public Property ActiveSplitSurchargeValue() As Boolean
        Get
            Return _ActiveSurchargeValue
        End Get
        Set(ByVal value As Boolean)
            _ActiveSurchargeValue = value
        End Set
    End Property

    Private _ActiveRefundSurchargeValue As Boolean
    Public Property ActiveRefundSurchargeValue() As Boolean
        Get
            Return _ActiveRefundSurchargeValue
        End Get
        Set(ByVal value As Boolean)
            _ActiveRefundSurchargeValue = value
        End Set
    End Property

    Public Sub UpdateSplitSurchargeValue(ByVal PmtId As String, _
                                         ByVal PtmIsSurchargeSplit As Boolean, _
                                         ByVal PmtIsRefundSurcharge As Boolean)
        If Not IsNeedToUpdateSurchargeSplit Then Return

        Dim retValue As String = ""
        Dim params(2) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("PtmId", DbType.String, PmtId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("PmtIsSurchargeSplit", DbType.Boolean, PtmIsSurchargeSplit, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("PmtIsRefundSurcharge", DbType.Boolean, PmtIsRefundSurcharge, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("Admin_UpdatePaymentMethodSurchargeSplit", params)
        Catch ex As Exception

        End Try
    End Sub

    Public Function GetAllPaymentMethod() As DataSet
        Dim result As New DataSet()
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Admin_GetAllPaymentMethod", result)
        Catch ex As Exception
        End Try
        Return result
    End Function

    Public Function GetPaymentMethodSplitSurchargeValue(ByVal PmtId As String, _
                                                        ByRef retValueForRefund As Boolean) As Boolean
        Dim retValue As Boolean = False
        Dim params(2) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("PtmId", DbType.String, PmtId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("retValue", DbType.Boolean, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            params(2) = New Aurora.Common.Data.Parameter("retValueForRefund", DbType.Boolean, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("Admin_GetPaymentMethodSplitSurchargeValue", params)
            retValue = CBool(params(1).Value)
            retValueForRefund = CBool(params(2).Value)

        Catch ex As Exception
        End Try

        Return retValue
    End Function
#End Region
    Public Function GetAllZone() As DataSet
        Dim result As New DataSet()
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Admin_GetAllZone", result)
        Catch ex As Exception
        End Try
        Return result
    End Function

    Private _IsNeedToUpdateActiveZone As Boolean

    Public Property IsNeedToUpdateActiveZone() As Boolean
        Get
            Return _IsNeedToUpdateActiveZone
        End Get
        Set(ByVal value As Boolean)
            _IsNeedToUpdateActiveZone = value
        End Set
    End Property

    Private _IsActiveZoneValue As Boolean
    Public Property IsActiveZoneValue() As Boolean
        Get
            Return _IsActiveZoneValue
        End Get
        Set(ByVal value As Boolean)
            _IsActiveZoneValue = value
        End Set
    End Property

    Public Sub UpdateLocationActiveZone(ByVal LocZoneCode As String, ByVal LocZoneIsActive As Boolean)
        If Not IsNeedToUpdateActiveZone Then Return
        ''Admin_UpdateLocationActiveZone
        Dim retValue As String = ""
        Dim params(1) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocZoneCode", DbType.String, Left(LocZoneCode, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("LocZoneIsActive", DbType.Boolean, LocZoneIsActive, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("Admin_UpdateLocationActiveZone", params)
        Catch ex As Exception

        End Try

    End Sub

    Public Function GetLocationZoneStatus(ByVal LocZoneCode As String) As Boolean
        Dim retValue As Boolean = False
        Dim params(1) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocZoneCode", DbType.String, Left(LocZoneCode, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("retValue", DbType.Boolean, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_GetActiveZone", params)
            retValue = CBool(params(1).Value)

        Catch ex As Exception
        End Try

        Return retValue
    End Function

    Public Function CheckMappedZoneBeforeDeleting(ByVal LocZoneCode As String, ByRef counter As Integer) As Boolean
        Dim retValue As Boolean = False
        Dim params(1) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocZoneCode", DbType.String, Left(LocZoneCode, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("retvalue", DbType.Int16, 2, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_CheckMappedZoneBeforeDeleting", params)
            counter = CInt(params(1).Value)
            retValue = IIf(counter > 0, False, True)

        Catch ex As Exception
            Return False
        End Try

        Return retValue
    End Function

    ''Admin_ResetChildLocationForInactiveZone
    Public Sub ResetChildLocationForInactiveZone(ByVal LocZoneId As String)
        Dim retValue As Integer = 0
        Dim params(0) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocZoneId", DbType.Int32, LocZoneId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_ResetChildLocationForInactiveZone", params)
        Catch ex As Exception
        End Try

    End Sub
End Module
