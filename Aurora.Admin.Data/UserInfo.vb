Partial Class UserDataSet

    Partial Class UserInfoRow

        Public ReadOnly Property Description() As String
            Get
                Dim result As String = Me.UsrCode
                If Not Me.IsUsrNameNull Then result &= " - " & Me.UsrName
                Return result
            End Get
        End Property

        Public Property IsActive() As Boolean
            Get
                Return Not Me.IsUsrIsActiveNull AndAlso Me.UsrIsActive
            End Get
            Set(ByVal value As Boolean)
                Me.UsrIsActive = value
            End Set
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If IsActive Then
                    Return Aurora.Common.DataConstants.ActiveColor
                Else
                    Return Aurora.Common.DataConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property CompanyRow() As CompanyRow
            Get
                Dim userCompanyRows As UserCompanyRow() = Me.GetUserCompanyRows()
                If userCompanyRows.Length > 0 Then
                    Return userCompanyRows(0).CompanyRow
                Else
                    Return Nothing
                End If
            End Get
        End Property

    End Class

End Class
