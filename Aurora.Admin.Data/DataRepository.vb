Imports Aurora.Admin.Data.UserDataSet
Imports Aurora.Common.Logging

Public Module DataRepository

#Region "Users"

    Public Function GetUserLookups(ByVal userDataSet As UserDataSet) As UserDataSet
        'CREATE PROCEDURE [dbo].[Admin_GetUserLookups]

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Admin_GetUserLookups", result)

        Aurora.Common.Data.CopyDataTable(result.Tables(0), userDataSet.Company)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), userDataSet.Country)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), userDataSet.Location)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), userDataSet.FlexExportType)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), userDataSet.Role)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), userDataSet.Menu)

        Return userDataSet
    End Function

    Private Function GetUser(ByVal userDataSet As UserDataSet, ByVal usrId As String, ByVal usrCode As String, _
                             ByVal usrName As String, ByVal usrComCode As String, ByVal usrCtyCode As String, _
                             ByVal usrLocCode As String, ByVal usrIsActive As Nullable(Of Boolean), _
                             ByVal usrIsAgentUser As Nullable(Of Boolean), ByVal exactUsrCode As String, _
                             ByVal exactUsrEmail As String, ByVal agentCode As String) As UserDataSet
        'CREATE PROCEDURE [dbo].[Admin_GetUser]
        '	@UsrId AS varchar(64),
        '	@UsrCode AS varchar(24),
        '	@UsrName AS varchar(64),
        '	@UsrComCode AS varchar(64),
        '	@UsrCtyCode AS varchar(12),
        '	@UsrLocCode AS varchar(12),
        '	@UsrIsActive AS bit,
        '   @UsrIsAgentUser AS bit,
        '   @ExactUsrCode AS varchar(24),
        '   @ExactUsrEmail AS varchar(64)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Admin_GetUser", result, _
            IIf(Not String.IsNullOrEmpty(usrId), usrId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(usrCode), usrCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(usrName), usrName, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(usrComCode), usrComCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(usrCtyCode), usrCtyCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(usrLocCode), usrLocCode, DBNull.Value), _
            IIf(usrIsActive.HasValue, usrIsActive.GetValueOrDefault(), DBNull.Value), _
            IIf(usrIsAgentUser.HasValue, usrIsAgentUser.GetValueOrDefault(), DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(exactUsrCode), exactUsrCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(exactUsrEmail), exactUsrEmail, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(agentCode), agentCode, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), userDataSet.UserInfo)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), userDataSet.UserCompany)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), userDataSet.FlexExportContact)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), userDataSet.UserRole)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), userDataSet.AgentUserInfo)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), userDataSet.Agent)

        ' B2b change
        Aurora.Common.Data.CopyDataTable(result.Tables(6), userDataSet.B2BUserConfiguration)
        Aurora.Common.Data.CopyDataTable(result.Tables(7), userDataSet.SubUsers)
        Aurora.Common.Data.CopyDataTable(result.Tables(8), userDataSet.SuperUser)
        ' B2b change

        Return userDataSet
    End Function

    Public Function SearchUser(ByVal userDataSet As UserDataSet, _
                               ByVal usrCode As String, _
                               ByVal usrName As String, _
                               ByVal usrComCode As String, _
                               ByVal usrCtyCode As String, _
                               ByVal usrLocCode As String, _
                               ByVal usrIsActive As Nullable(Of Boolean), _
                               ByVal usrIsAgentUser As Nullable(Of Boolean), _
                               ByVal usrEmail As String, _
                               ByVal agentCode As String) As UserDataSet

        Return GetUser(userDataSet, Nothing, usrCode, usrName, usrComCode, usrCtyCode, usrLocCode, usrIsActive, usrIsAgentUser, Nothing, usrEmail, agentCode)

    End Function

    Public Function GetUser(ByVal userDataSet As UserDataSet, ByVal usrId As String, ByVal usrIsAgentUser As Nullable(Of Boolean)) As UserDataSet

        Return GetUser(userDataSet, usrId, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, usrIsAgentUser, Nothing, Nothing, Nothing)

    End Function

    Public Function GetUserByCode(ByVal userDataSet As UserDataSet, ByVal usrCode As String) As UserDataSet

        Return GetUser(userDataSet, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, usrCode, Nothing, Nothing)

    End Function

    Public Function GetUserByEmail(ByVal userDataSet As UserDataSet, ByVal usrEmail As String) As UserDataSet

        Return GetUser(userDataSet, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, usrEmail, Nothing)

    End Function

#End Region


#Region "Locations"

    Public Function GetLocationLookups(ByVal locationDataSet As LocationDataSet) As LocationDataSet
        'CREATE PROCEDURE [dbo].[Admin_GetLocationLookups] 

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Admin_GetLocationLookups", result)

        Aurora.Common.Data.CopyDataTable(result.Tables(0), locationDataSet.Country)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), locationDataSet.TownCity)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), locationDataSet.Code)

        Return locationDataSet
    End Function

    Private Function GetLocation(ByVal locationDataSet As LocationDataSet, ByVal comCode As String, ByVal locCode As String, ByVal locCodeSearch As String, ByVal locCodTypId As String, ByVal ctyCode As String, ByVal tctCode As String, ByVal conName As String, ByVal locIsActive As Nullable(Of Boolean)) As LocationDataSet
        'CREATE PROCEDURE [dbo].[Admin_GetLocation] 
        '   @ComCode AS varchar(64),
        '   @LocCode AS varchar(12),
        '   @LocCodeSearch AS varchar(12),
        '   @LocCodTypId AS varchar(64),
        '   @CtyCode AS varchar(12),
        '   @TctCode AS varchar(12),
        '   @ConName AS varchar(64),
        '   @LocIsActive AS bit

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Admin_GetLocation", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(locCode), locCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(locCodeSearch), locCodeSearch, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(locCodTypId), locCodTypId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ctyCode), ctyCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(tctCode), tctCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(conName), conName, DBNull.Value), _
            IIf(locIsActive.HasValue, locIsActive.GetValueOrDefault(), DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), locationDataSet.Location)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), locationDataSet.AddressDetails)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), locationDataSet.Contact)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), locationDataSet.PhoneNumber)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), locationDataSet.LocationSeason)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), locationDataSet.LocationCheckInOutTime)

        Return locationDataSet
    End Function

    Public Function SearchLocation(ByVal locationDataSet As LocationDataSet, ByVal comCode As String, ByVal locCodeSearch As String, ByVal locCodTypId As String, ByVal ctyCode As String, ByVal tctCode As String, ByVal conName As String, ByVal locIsActive As Nullable(Of Boolean)) As LocationDataSet

        Return GetLocation(locationDataSet, comCode, Nothing, locCodeSearch, locCodTypId, ctyCode, tctCode, conName, locIsActive)

    End Function

    Public Function GetLocation(ByVal locationDataSet As LocationDataSet, ByVal comCode As String, ByVal locCode As String) As LocationDataSet

        Return GetLocation(locationDataSet, comCode, locCode, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing)

    End Function

#End Region

#Region "rev:mia feb 3 added zone location"

    Public Function SelectLocationZone() As DataSet
        Dim result As New DataSet()
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Admin_SelectLocationZone", result)
        Catch ex As Exception
        End Try
        Return result
    End Function

    Public Function SelectOtherLocation(ByVal LocCodTypId As String, ByVal LocTctCode As String, ByVal LocCode As String) As DataSet
        Dim result As New DataSet()
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Admin_SelectOtherLocation", result, LocCodTypId, LocTctCode, LocCode)
        Catch ex As Exception
        End Try
        Return result
    End Function

 
    Public Function UpdateLocationZoneMapping(ByVal LocCodTypId As String, _
                                              ByVal LocTctCode As String, _
                                              ByVal LocCode As String, _
                                              ByVal LocZoneId As Integer, _
                                              ByVal modifiedBy As String, _
                                              ByVal LocWebEnabledDate As String) As String

        Dim retValue As String = ""
        Dim params(6) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocCodTypId", DbType.String, Left(LocCodTypId, 64), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("LocTctCode", DbType.String, Left(LocTctCode, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("LocCode", DbType.String, Left(LocCode, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ''params(3) = New Aurora.Common.Data.Parameter("LocZoneId", DbType.Int32, LocZoneId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("LocZoneId", DbType.Int32, IIf(LocZoneId = -1, DBNull.Value, LocZoneId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("modifiedBy", DbType.String, Left(modifiedBy, 3), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("retValue", DbType.String, 100, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            params(6) = New Aurora.Common.Data.Parameter("LocWebEnabledDate", DbType.DateTime, IIf(LocWebEnabledDate = "", DBNull.Value, LocWebEnabledDate), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("Admin_UpdateLocationZoneMapping", params)
            retValue = params(5).Value

        Catch ex As Exception
            retValue = "ERROR: Location and Zone mapping updating failed."
        End Try
        Return retValue

    End Function

    Public Function TotalNumberOfChildLocationUnMappedtoZone(ByVal LocCodTypId As String, ByVal LocTctCode As String, ByVal locCode As String) As String
        Dim temp As String = ""
        Try
            temp = Aurora.Common.Data.ExecuteScalarSP("Admin_TotalNumberOfChildLocationUnMappedtoZone", LocCodTypId, LocTctCode, locCode)
        Catch ex As Exception
        End Try
        Return temp
    End Function
    Public Function TotalNumberOfChildLocation(ByVal LocCodTypId As String, ByVal LocTctCode As String, ByVal locCode As String) As String
        Dim temp As String = ""
        Try
            temp = Aurora.Common.Data.ExecuteScalarSP("Admin_TotalNumberOfChildLocation", LocCodTypId, LocTctCode, locCode)
        Catch ex As Exception
        End Try
        Return temp
    End Function

    Public Function TotalNumberOfChildLocationUnMapped(ByVal LocCodTypId As String, ByVal LocTctCode As String, ByVal locCode As String) As String
        Dim temp As String = ""
        Try
            temp = Aurora.Common.Data.ExecuteScalarSP("Admin_TotalNumberOfChildLocationUnMapped", LocCodTypId, LocTctCode, locCode)
        Catch ex As Exception
        End Try
        Return temp
    End Function

    Public Function ResetChildLocationUnMapping(ByVal LocCodTypId As String, ByVal LocTctCode As String, ByVal locCode As String) As String
        Dim temp As String = ""
        Try
            temp = Aurora.Common.Data.ExecuteScalarSP("Admin_ResetChildLocationUnMapping", LocCodTypId, LocTctCode, locCode)
        Catch ex As Exception
            temp = "Error:" & ex.Message
        End Try
        Return temp
    End Function

    Public Function GetLocationFilteredByZone(ByVal locationDataSet As LocationDataSet, _
                                               ByVal comCode As String, _
                                               ByVal locCode As String, _
                                               ByVal locCodeSearch As String, _
                                               ByVal locCodTypId As String, _
                                               ByVal ctyCode As String, _
                                               ByVal tctCode As String, _
                                               ByVal conName As String, _
                                               ByVal locIsActive As Nullable(Of Boolean), _
                                               ByVal Zoneid As Nullable(Of Integer)) As LocationDataSet

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("Admin_GetLocation_PHOENIX", result, _
            IIf(Not String.IsNullOrEmpty(comCode), comCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(locCode), locCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(locCodeSearch), locCodeSearch, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(locCodTypId), locCodTypId, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(ctyCode), ctyCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(tctCode), tctCode, DBNull.Value), _
            IIf(Not String.IsNullOrEmpty(conName), conName, DBNull.Value), _
            IIf(locIsActive.HasValue, locIsActive.GetValueOrDefault(), DBNull.Value), _
            IIf(Zoneid.HasValue, Zoneid.GetValueOrDefault, DBNull.Value))

        Aurora.Common.Data.CopyDataTable(result.Tables(0), locationDataSet.Location)
        Aurora.Common.Data.CopyDataTable(result.Tables(1), locationDataSet.AddressDetails)
        Aurora.Common.Data.CopyDataTable(result.Tables(2), locationDataSet.Contact)
        Aurora.Common.Data.CopyDataTable(result.Tables(3), locationDataSet.PhoneNumber)
        Aurora.Common.Data.CopyDataTable(result.Tables(4), locationDataSet.LocationSeason)
        Aurora.Common.Data.CopyDataTable(result.Tables(5), locationDataSet.LocationCheckInOutTime)

        Return locationDataSet
    End Function

    
    Public Function SelectOtherLocationMappedtoOneLocation(ByVal LocCodTypId As String, _
                                                           ByVal LocTctCode As String, _
                                                           ByVal LocCode As String) As Integer
        Dim retValue As Integer = 0
        Dim params(3) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("LocCodTypId", DbType.String, LocCodTypId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("LocTctCode", DbType.String, LocTctCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("LocCode", DbType.String, LocCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("retvalue", DbType.String, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("SelectOtherLocationMappedtoOneLocation", params)
            retValue = CInt(params(3).Value)

        Catch ex As Exception
        End Try
        Return retValue
    End Function
#End Region

#Region "Tourism Holdings and the project called PHOENIX"

    Public Function GetBrandsAndVehicleClasses(Optional ByVal CompanyCode As String = "") As DataTable()
        Dim arrTables(1) As DataTable
        Dim dstTemp As DataSet = New DataSet
        dstTemp = Aurora.Common.Data.ExecuteDataSetSP("WEBP_GetBrandsAndVehicleClasses", dstTemp)
        If dstTemp.Tables.Count > 0 Then
            Dim dvFilter As DataView = New DataView(dstTemp.Tables(0))

            If Not CompanyCode.Trim().Equals(String.Empty) Then
                ' some co-co specified :(
                dvFilter.RowFilter = "ComCode = '" & CompanyCode & "'"
            End If

            dvFilter.Sort = "ClaDesc ASC"
            arrTables(0) = dvFilter.ToTable(True, "ClaId", "ClaDesc")

            dvFilter.Sort = "BrdName ASC"
            arrTables(1) = dvFilter.ToTable(True, "BrdCode", "BrdName")
        Else
            arrTables(0) = New DataTable
            arrTables(1) = New DataTable
        End If

        Return arrTables

    End Function

    Public Function UpdateLocationBrands(ByVal LocationCode As String, ByVal AddedBrandIdList As String, ByVal RemovedBrandIdList As String, ByVal UserId As String, ByVal ProgramName As String) As String
        Dim oReturnObject As Object

        Dim sbXMLData As Text.StringBuilder = New Text.StringBuilder

        sbXMLData.Append("<data>")
        sbXMLData.Append("  <LocationCode>" & LocationCode & "</LocationCode>")
        sbXMLData.Append("  <AddedBrandIdList>" & AddedBrandIdList & "</AddedBrandIdList>")
        sbXMLData.Append("  <RemovedBrandIdList>" & RemovedBrandIdList & "</RemovedBrandIdList>")
        sbXMLData.Append("  <UserId>" & UserId & "</UserId>")
        sbXMLData.Append("  <ProgramName>" & ProgramName & "</ProgramName>")
        sbXMLData.Append("</data>")

        oReturnObject = Aurora.Common.Data.ExecuteScalarSP("WEBP_UpdateLocationBrands", sbXMLData.ToString())
        If oReturnObject IsNot Nothing Then
            Return CStr(oReturnObject)
        Else
            Return String.Empty
        End If

    End Function

    Public Function LoadLocationBrands(ByVal LocationCode As String) As String
        Dim oReturnObject As Object
        oReturnObject = Aurora.Common.Data.ExecuteScalarSP("WEBP_GetLocationBrands", LocationCode)
        If oReturnObject IsNot Nothing Then
            Return CStr(oReturnObject)
        Else
            Return String.Empty
        End If
        'WEBP_GetLocationBrands
    End Function

    Public Function LoadLocationVehicleClasses(ByVal LocationCode As String) As String
        Dim oReturnObject As Object
        oReturnObject = Aurora.Common.Data.ExecuteScalarSP("WEBP_GetLocationVehicleClasses", LocationCode)
        If oReturnObject IsNot Nothing Then
            Return CStr(oReturnObject)
        Else
            Return String.Empty
        End If
    End Function

    Public Function UpdateLocationVehicleClasses(ByVal LocationCode As String, ByVal AddedVehicleClassIdList As String, ByVal RemovedVehicleClassIdList As String, ByVal UserId As String, ByVal ProgramName As String) As String
        Dim oReturnObject As Object

        Dim sbXMLData As Text.StringBuilder = New Text.StringBuilder

        sbXMLData.Append("<data>")
        sbXMLData.Append("  <LocationCode>" & LocationCode & "</LocationCode>")
        sbXMLData.Append("  <AddedVehicleClassIdList>" & AddedVehicleClassIdList & "</AddedVehicleClassIdList>")
        sbXMLData.Append("  <RemovedVehicleClassIdList>" & RemovedVehicleClassIdList & "</RemovedVehicleClassIdList>")
        sbXMLData.Append("  <UserId>" & UserId & "</UserId>")
        sbXMLData.Append("  <ProgramName>" & ProgramName & "</ProgramName>")
        sbXMLData.Append("</data>")

        oReturnObject = Aurora.Common.Data.ExecuteScalarSP("WEBP_UpdateLocationVehicleClasses", sbXMLData.ToString())
        If oReturnObject IsNot Nothing Then
            Return CStr(oReturnObject)
        Else
            Return String.Empty
        End If
    End Function

#End Region

#Region "B2B User Currency stuff"

    Public Function GetB2BUserAgentCurrencyData(ByVal UserId As String) As DataSet
        '       ALTER PROC Admin_GetUserB2BCurrencySettings 'USR1255'
        '@sUsrId AS VARCHAR(64)

        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Admin_GetUserB2BCurrencySettings", dstResult, UserId)
        Return dstResult

    End Function

    Public Function SaveB2BUserAgentCurrencyData(ByVal UserId As String, ByVal AgentId As String, _
                                                 ByVal CurrencyList As String, ByVal DefaultCurrency As String, _
                                                 ByVal AgentConfigString As String, ByVal UserCode As String, _
                                                 ByVal ProgramName As String) As Boolean

        'ALTER PROC [dbo].[Admin_SaveUserB2BCurrencySettings]  
        ' @sAgentId AS VARCHAR(64)  
        ' ,@sUserId AS VARCHAR(64)  
        ' ,@sCurrencyOptions AS VARCHAR(256)  
        ' ,@sDefaultCurrency AS VARCHAR(3)	
        ' ,@pAgentDutyCode VARCHAR(128)  
        ' ,@pUserCode VARCHAR(64)
        ' ,@pPrgmName VARCHAR(256)	

        Try
            Aurora.Common.Data.ExecuteScalarSP("Admin_SaveUserB2BCurrencySettings", AgentId, UserId, CurrencyList, DefaultCurrency, _
                                               AgentConfigString, UserCode, ProgramName)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

#End Region


#Region "rev:mia 29sept2014-AIMS ROLE MAPPING"
    Public Function AIMSRoleMappingData(userCode As String) As DataSet
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("Admin_RoleMappingData", dstResult, userCode)
        Return dstResult
    End Function



    Public Function AIMSInsUpdRoleMappingData(RoleMappingPageId As Integer, _
                                              RoleMappingRoleId As Integer, _
                                              RoleMappingUserCode As String, _
                                              AddUsrId As String) As Boolean
        '@RoleMappingPageId int,
        '@RoleMappingRoleId int,
        '@RoleMappingUserCode varchar(64),
        '@AddUsrId varchar(64),
        '@result	INT OUT

        Dim params(4) As Aurora.Common.Data.Parameter
        Dim retvalue As String
        Try
            params(0) = New Aurora.Common.Data.Parameter("RoleMappingPageId", DbType.Int32, RoleMappingPageId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("RoleMappingRoleId", DbType.Int32, RoleMappingRoleId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("RoleMappingUserCode", DbType.String, RoleMappingUserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("result", DbType.Int16, 2, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)


            Aurora.Common.Data.ExecuteOutputSP("Admin_InsUpdRoleMappingData", params)
            retvalue = params(4).Value
            LogDebug("Admin_InsUpdRoleMappingData retvalue: ", retvalue)

        Catch ex As Exception
            LogError("AIMSInsUpdRoleMappingData()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            Return False
        End Try

        Return True
    End Function


    Public Function AIMSDelRoleMappingData(RoleMappingUserId As Integer, _
                                           RoleMappingUserCode As String, _
                                           isAll As Boolean) As Boolean

        Dim params(2) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("RoleMappingUserId", DbType.Int32, RoleMappingUserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("RoleMappingUserCode", DbType.String, RoleMappingUserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("isAll", DbType.Boolean, isAll, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            

            Aurora.Common.Data.ExecuteOutputSP("Admin_DelRoleMappingData", params)

            LogDebug("Admin_DelRoleMappingData: ", "OK")

        Catch ex As Exception
            LogError("AIMSDelRoleMappingData()".ToUpper, String.Concat(ex.Message, " - ", ex.StackTrace))
            Return False
        End Try

        Return True
    End Function
#End Region

End Module
