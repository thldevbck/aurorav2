Imports System.Drawing

Imports Aurora.Common

Public Class LocationConstants
    Inherits DataConstants

    Public Enum DayOfWeek
        Monday = 1
        Tuesday = 2
        Wednesday = 3
        Thursday = 4
        Friday = 5
        Saturday = 6
        Sunday = 7
        Min = 1
        Max = 7
    End Enum

    Public Shared ReadOnly DayOfWeekNames As String() = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}


End Class