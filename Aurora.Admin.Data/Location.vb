Partial Class LocationDataSet

    Partial Class LocationRow

        Public ReadOnly Property LocationDataSet() As LocationDataSet
            Get
                Return CType(Me.Table.DataSet, LocationDataSet)
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Dim result As String = Me.LocCode
                If Not Me.IsLocNameNull Then result &= " - " & Me.LocName
                Return result
            End Get
        End Property

        Public Property IsActive() As Boolean
            Get
                Return Not Me.IsLocIsActiveNull AndAlso Me.LocIsActive
            End Get
            Set(ByVal value As Boolean)
                Me.LocIsActive = value
            End Set
        End Property

        Public ReadOnly Property StatusColor() As System.Drawing.Color
            Get
                If IsActive Then
                    Return Aurora.Common.DataConstants.ActiveColor
                Else
                    Return Aurora.Common.DataConstants.InactiveColor
                End If
            End Get
        End Property

        Public ReadOnly Property ContactRow() As ContactRow
            Get
                Dim contactRows As ContactRow() = Me.GetContactRows()
                If contactRows.Length > 0 Then
                    Return contactRows(0)
                Else
                    Return Nothing
                End If
            End Get
        End Property

        Public ReadOnly Property AddressDetailsRow(ByVal code As String) As AddressDetailsRow
            Get
                For Each result As AddressDetailsRow In Me.GetAddressDetailsRows()
                    If result.CodeRow IsNot Nothing And result.CodeRow.CodCode = code Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property PhoneNumberRow(ByVal tableName As String, ByVal code As String) As PhoneNumberRow
            Get
                For Each result As PhoneNumberRow In Me.GetPhoneNumberRows()
                    If result.PhnPrntTableName = tableName _
                     AndAlso result.CodeRow IsNot Nothing And result.CodeRow.CodCode = code Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

        Public ReadOnly Property LocationSeasonRow(ByVal losId As String) As LocationSeasonRow
            Get
                For Each result As LocationSeasonRow In Me.GetLocationSeasonRows()
                    If result.LosId = losId Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property

    End Class

End Class
