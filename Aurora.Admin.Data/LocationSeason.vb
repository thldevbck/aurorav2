Partial Class LocationDataSet

    Partial Class LocationSeasonRow

        Public ReadOnly Property LocationDataSet() As LocationDataSet
            Get
                Return CType(Me.Table.DataSet, LocationDataSet)
            End Get
        End Property

        Public ReadOnly Property LocationCheckInOutTimeRow(ByVal dayOfWeek As LocationConstants.DayOfWeek) As LocationCheckInOutTimeRow
            Get
                For Each result As LocationCheckInOutTimeRow In Me.GetLocationCheckInOutTimeRows()
                    If result.lioDayOfWeek = CType(dayOfWeek, Integer) Then
                        Return result
                    End If
                Next
                Return Nothing
            End Get
        End Property


    End Class

End Class
