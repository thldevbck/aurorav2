Partial Class LocationDataSet

    Partial Class CodeDataTable

        Function GetByCdtNumCode(ByVal cdtNum As Integer, ByVal code As String)
            For Each codeRow As CodeRow In Me
                If codeRow.CodCdtNum = cdtNum AndAlso codeRow.CodCode = code Then Return codeRow
            Next
            Return Nothing
        End Function

    End Class

End Class
