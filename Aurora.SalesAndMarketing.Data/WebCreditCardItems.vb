Public Class WebCreditCardItems

    Private _WbCrdTypSPtmID As String
    Private _PmtName As String
    Private _AddDateTime As DateTime

    Public Property WbCrdTypSPtmID() As String
        Get
            Return _WbCrdTypSPtmID
        End Get
        Set(ByVal value As String)
            _WbCrdTypSPtmID = value
        End Set
    End Property

    Public Property PmtName() As String
        Get
            Return _PmtName
        End Get
        Set(ByVal value As String)
            _PmtName = value
        End Set
    End Property

    Public Property AddDateTime() As DateTime
        Get
            Return _AddDateTime
        End Get
        Set(ByVal value As DateTime)
            _AddDateTime = value
        End Set
    End Property

    Sub New(ByVal sWbCrdTypSPtmID As String, ByVal sPmtName As String, ByVal dAddDateTime As DateTime)
        _WbCrdTypSPtmID = sWbCrdTypSPtmID
        _PmtName = sPmtName
        _AddDateTime = dAddDateTime
    End Sub

End Class
