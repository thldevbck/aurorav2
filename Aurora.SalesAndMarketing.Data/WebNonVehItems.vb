Public Class WebNonVehItems


    Private _HreItmPrdId As String
    Private _PrdShortName As String
    Private _HreItmDispOrder As String
    Private _DependencyProductId As String
    Private _DependencyPrdShortName As String
    Private _DependencyAction As String
    Private _ActionId As String
    Private _AddDateTime As DateTime
    Private _HreItmId As String
    Private _Rating As String
    Private _HreItmIdNew As String
    Private _SiteIds As String

    Public Property SiteId As String
        Get
            Return _SiteIds
        End Get
        Set(ByVal value As String)
            _SiteIds = value
        End Set
    End Property

    Public Property HreItmIdNew As String
        Get
            Return _HreItmIdNew
        End Get
        Set(ByVal value As String)
            _HreItmIdNew = value
        End Set
    End Property

    Public Property HreItmId() As String
        Get
            Return _HreItmId
        End Get
        Set(ByVal value As String)
            _HreItmId = value
        End Set
    End Property

    Public Property HreItmPrdId() As String
        Get
            Return _HreItmPrdId
        End Get
        Set(ByVal value As String)
            _HreItmPrdId = value
        End Set
    End Property

    Public Property PrdShortName() As String
        Get
            Return _PrdShortName
        End Get
        Set(ByVal value As String)
            _PrdShortName = value
        End Set
    End Property

    Public Property HreItmDispOrder() As String
        Get
            Return _HreItmDispOrder
        End Get
        Set(ByVal value As String)
            _HreItmDispOrder = value
        End Set
    End Property

    Public Property DependencyProductId() As String
        Get
            Return _DependencyProductId
        End Get
        Set(ByVal value As String)
            _DependencyProductId = value
        End Set
    End Property

    Public Property DependencyPrdShortName() As String
        Get
            Return _DependencyPrdShortName
        End Get
        Set(ByVal value As String)
            _DependencyPrdShortName = value
        End Set
    End Property

    Public Property DependencyAction() As String
        Get
            Return _DependencyAction
        End Get
        Set(ByVal value As String)
            _DependencyAction = value
        End Set
    End Property

    Public Property ActionId() As String
        Get
            Return _ActionId
        End Get
        Set(ByVal value As String)
            _ActionId = value
        End Set
    End Property

    Public Property AddDatetime() As DateTime
        Get
            Return _AddDateTime
        End Get
        Set(ByVal value As DateTime)
            _AddDateTime = value
        End Set
    End Property

    Public Property Rating() As String
        Get
            Return _Rating
        End Get
        Set(ByVal value As String)
            _Rating = value
        End Set
    End Property

    Sub New(ByVal sHreItmPrdId As String, _
            ByVal sPrdShortName As String, _
            ByVal sHreItmDispOrder As String, _
            ByVal sDependencyProductId As String, _
            ByVal sDependencyPrdShortName As String, _
            ByVal sDependencyAction As String, _
            ByVal sActionId As String, _
            ByVal dAddDateTime As DateTime, _
            ByVal sHreItmId As String, _
            Optional ByVal sRating As String = "", _
            Optional ByVal sHreItmIdNew As String = "", _
            Optional ByVal sSiteId As String = "")

        _HreItmPrdId = sHreItmPrdId
        _PrdShortName = sPrdShortName
        _HreItmDispOrder = sHreItmDispOrder
        _DependencyProductId = sDependencyProductId
        _DependencyPrdShortName = sDependencyPrdShortName
        _DependencyAction = sDependencyAction
        _ActionId = sActionId
        _AddDateTime = dAddDateTime
        _HreItmId = sHreItmId
        _Rating = sRating
        _HreItmIdNew = sHreItmIdNew
        _SiteIds = sSiteId
    End Sub
End Class
