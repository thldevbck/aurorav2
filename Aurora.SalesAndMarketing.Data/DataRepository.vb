''rev:mia jan 07 2013 Call : 37671 - Change request - Section to search for agents to exclude from the bulk allocation in Aurora
Imports System.Xml


Public Class DataRepository
    'added by shoel for bulk product
#Region "BULK PRODUCT LOADING"
    ''REV:MIA JAN 07 2012 - Added agentid and packageid
    Public Shared Function FindBulkAgent(ByVal Country As String, _
                                         ByVal Group As String, _
                                         ByVal MarketCode As String, _
                                         ByVal AgentType As String, _
                                         ByVal Category As String, _
                                         Optional AgnCodName As String = "", _
                                         Optional PackageId As String = "") As XmlDocument


        Dim xmlDoc As XmlDocument
        Dim spname As String = "sp_findBulkAgent"

        'rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        If (Country.Contains(",") = True Or _
            AgentType.Contains(",") = True Or _
            MarketCode.Contains(",") = True Or _
            Category.Contains(",") = True Or _
            PackageId.Contains(",") = True Or _
            Group.Contains(",") = True Or _
            AgnCodName.Contains(",") = True) Then
            ''treat this as a multiple selection and use other Stored procedure
            spname = "sp_findBulkMultipleAgent "
        End If

        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc(spname, 0, AgnCodName, Group, "", AgentType, "", "", "", "", "", "", "", "", Country, MarketCode, Category, PackageId)


        Return xmlDoc

    End Function

    Public Shared Function GetClassTypes() As DataTable
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_getComboData", "CLASS", "", "", "")

        Dim dstResults As DataSet = New DataSet()
        dstResults.ReadXml(New XmlNodeReader(xmlDoc))
        If dstResults.Tables.Count > 0 Then
            Return (dstResults.Tables(0))
        Else
            Return (New DataTable)
        End If


        'CREATE PROCEDURE [dbo].[GEN_getComboData]   
        ' @case  VARCHAR(20) = '',  
        ' @param1  VARCHAR(64) = '',  
        ' @param2  VARCHAR(64) = '',  
        ' --KX Changes: RKS  
        ' @sUsrCode VARCHAR(64) = ''  

    End Function

#Region "PRODUCT"

    Public Shared Function GetBulkProduct(ByVal AllProduct As Boolean, ByVal ProductShortName As String, ByVal Brand As String, ByVal [Class] As String, ByVal Type As String, ByVal UserCode As String) As XmlDocument
        'sp_getBulkProduct
        'exec sp_getBulkProduct @bAllProduct = 0, @PrdShortName = '', @sBrand = 'B', @sClass = '75F44824-60F8-4CC6-BCEF-CBCAFD4DAEC4', @sType = 'SC', @sUsrCode = 'sp7'

        'CREATE     PROCEDURE [dbo].[sp_getBulkProduct]   
        '@bAllProduct     BIT  = 1 ,  
        '@PrdShortName     VARCHAR  (64) = '' ,  
        '@sBrand      VARCHAR  (64) = '' ,  
        '@sClass      VARCHAR  (64) = '' ,  
        '@sType      VARCHAR  (64) = '' ,  
        '@sUsrCode     VARCHAR  (64) = ''  
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getBulkProduct", AllProduct, ProductShortName, Brand, [Class], Type, UserCode)

        Return xmlDoc



    End Function

    Public Shared Function SaveBulkAgentProducts(ByVal XmlDataToSave As String) As String

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_UpdateBulkAgentProduct", XmlDataToSave)

        Return xmlDoc.InnerText

    End Function

#End Region

#Region "PACKAGE"

    ''rev:mia Jan 7 2013 - change for this year. addtion of AgnCodName
    Public Shared Function GetBulkPackages(ByVal AllPackages As Boolean, _
                                            ByVal PackageCode As String, _
                                            ByVal CountryCode As String, _
                                            ByVal BrandCode As String, _
                                            ByVal Type As String, _
                                            ByVal TravelFromDate As String, _
                                            ByVal TravelToDate As String, _
                                            ByVal BookedFromDate As String, _
                                            ByVal BookedToDate As String, _
                                            ByVal UserCode As String, _
                                            Optional AgnCodName As String = "") As XmlDocument

        'exec sp_getBulkAvailablePackages 
        '@bAllPackages = '0', 
        '@pkgCode = '', 
        '@pkgCtyCode = 'NZ', 
        '@pkgBrdCode = 'B', 
        '@PkgCodTypId = '6ABB1B42-2066-42C8-97FD-2B71B6CEE182', 
        '@pkgTravelFromDate = '01/01/2001', 
        '@pkgTravelToDate = '02/02/2002', 
        '@pkgBookedFromDate = '03/03/2003', 
        '@pkgBookedToDate = '04/04/2004', 
        '@sUsrCode = 'sp7'

        'CREATE   PROCEDURE [dbo].[sp_getBulkAvailablePackages]   
        '  @bAllPackages  BIT  = 1,  
        '  @pkgCode  VARCHAR(64) = '',  
        '  @pkgCtyCode  VARCHAR(12) = '',    
        '  @pkgBrdCode  VARCHAR(1) = '',  
        '  @PkgCodTypId  VARCHAR(64) = '',  
        '  @pkgTravelFromDate VARCHAR(64) = '',  
        '  @pkgTravelToDate VARCHAR(64) = '',  
        '  @pkgBookedFromDate VARCHAR(64) = '',  
        '  @pkgBookedToDate VARCHAR(64) = '',  
        '  @sUsrCode   VARCHAR(64) = ''  

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getBulkAvailablePackages", AllPackages, _
                                                                                      PackageCode, _
                                                                                      CountryCode, _
                                                                                      BrandCode, _
                                                                                      Type, _
                                                                                      TravelFromDate, _
                                                                                      TravelToDate, _
                                                                                      BookedFromDate, _
                                                                                      BookedToDate, _
                                                                                      UserCode, _
                                                                                      AgnCodName)

        Return xmlDoc


    End Function

    Public Shared Function GetPackageDetails(ByVal PackageId As String, ByVal UserCode As String) As XmlDocument

        'SP_getPackage
        '@PkgId = N'CCA7C382-0604-478E-AB2B-545F10F405F5',
        '@sUsrCode = N'sp7'

        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("SP_getPackage", PackageId, UserCode)

        Return xmlDoc

    End Function

    Public Shared Function SaveBulkAgentPackages(ByVal XmlDataToSave As String) As String
        'CREATE          PROCEDURE sp_UpdateBulkAgentPackage  
        '@XMLData TEXT = DEFAULT  
        Dim xmlDoc As XmlDocument

        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_UpdateBulkAgentPackage", XmlDataToSave)
        Return xmlDoc.InnerText
    End Function

#End Region

#End Region

#Region "rev:mia May 13 - changes for Web vehicleAvailability Display"
    Public Shared Function Get_WebLocationsAndBrands(Optional ByVal usercode As String = "") As DataSet
        Dim result As New DataSet
        Try
            Dim sp As String = "Products_WEbGetLocationsAndBrands"
            Aurora.Common.Data.ExecuteDataSetSP(sp, result, usercode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Locations and Brands")
        End Try
        Return result
    End Function

    Public Shared Function Get_WebProductListWithTarget(ByVal CityCode As String, _
                                              ByVal BrandCode As String, _
                                              ByVal classType As String) As DataSet
        Dim result As New DataSet
        Try
            Dim sp As String = "webp_getproductlist"
            Aurora.Common.Data.ExecuteDataSetSP(sp, result, "TARGETSITE", CityCode, BrandCode, classType)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List with Targetsite")
        End Try
        Return result
    End Function


    Public Shared Function Get_WebProductList(ByVal queryCase As String, _
                                              ByVal CityCode As String, _
                                              ByVal BrandCode As String, _
                                              ByVal classType As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("webp_getproductlist", result, queryCase, CityCode, BrandCode, classType)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Product List")
        End Try
        Return result
    End Function

    
    ''rev:mia 21Jan2016 - AURORA-416 Set up A Web Non Vehicle Maintenance page for Relocation Bookings
    ''mia:rev 17-Oct-2016 - Add new Category in WebNonVehicle for Relocation and General Category
    Public Shared Function Get_WebSetupIDPlusData(ByVal CityCode As String, _
                                                  ByVal BrandCode As String, _
                                                  ByVal classType As String, _
                                                  ByVal catId As String) As XmlDocument

        Dim comCode As String = ""
        Dim setUpid As String = ""
        Dim xmlDom As XmlDocument = Nothing
        Dim param(1) As Aurora.Common.Data.Parameter
        Dim params(5) As Aurora.Common.Data.Parameter

        Try
            param(0) = New Aurora.Common.Data.Parameter("BrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            param(1) = New Aurora.Common.Data.Parameter("@comCode", DbType.String, 5, Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_getComCode", param)
            comCode = param(1).Value
            If String.IsNullOrEmpty(comCode) Then Throw New Exception("ERROR: Retrieving ComCode")


            params(0) = New Aurora.Common.Data.Parameter("sComCode", DbType.String, comCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, CityCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("BrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sClaCode", DbType.String, classType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("sCatId", DbType.String, IIf(String.IsNullOrEmpty(catId), DBNull.Value, catId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("WebSetupId", DbType.String, 5, Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("WEBP_getHreItmWebSetupId", params)
            setUpid = params(5).Value
            If String.IsNullOrEmpty(setUpid) Then Throw New Exception("ERROR: Retrieving WebSetup ID. No record found")

            ''xmlDom = Aurora.Common.Data.ExecuteSqlXmlSPDoc("WEBP_getForWebExtraHireSetup", setUpid)
            xmlDom = Aurora.Common.Data.ExecuteSqlXmlSPDoc("WEBP_getForWebExtraHireSetupExtraHireItems", setUpid)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return xmlDom
    End Function

    ''rev:mia 21Jan2016 - AURORA-416 Set up A Web Non Vehicle Maintenance page for Relocation Bookings
    ''mia:rev 17-Oct-2016 - Add new Category in WebNonVehicle for Relocation and General Category
    Public Shared Function Get_WebSetupIDPlusInsuranceData(ByVal CityCode As String, _
                                                ByVal BrandCode As String, _
                                                ByVal classType As String, _
                                                ByVal catId As String) As XmlDocument

        Dim comCode As String = ""
        Dim setUpid As String = ""
        Dim xmlDom As XmlDocument = Nothing
        Dim param(1) As Aurora.Common.Data.Parameter
        Dim params(5) As Aurora.Common.Data.Parameter

        Try
            param(0) = New Aurora.Common.Data.Parameter("BrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            param(1) = New Aurora.Common.Data.Parameter("@comCode", DbType.String, 5, Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_getComCode", param)
            comCode = param(1).Value
            If String.IsNullOrEmpty(comCode) Then Throw New Exception("ERROR: Retrieving ComCode")

            params(0) = New Aurora.Common.Data.Parameter("sComCode", DbType.String, comCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, CityCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("BrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sClaCode", DbType.String, classType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("sCatId", DbType.String, IIf(String.IsNullOrEmpty(catId), DBNull.Value, catId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("WebSetupId", DbType.String, 5, Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("WEBP_getHreItmWebSetupId", params)
            setUpid = params(5).Value
            If String.IsNullOrEmpty(setUpid) Then Throw New Exception("ERROR: Retrieving WebSetup ID. No record found")

            xmlDom = Aurora.Common.Data.ExecuteSqlXmlSPDoc("WEBP_getForWebExtraHireSetupInsurance", setUpid)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return xmlDom
    End Function

    ''rev:mia 21Jan2016 - AURORA-416 Set up A Web Non Vehicle Maintenance page for Relocation Bookings
    ''mia:rev 17-Oct-2016 - Add new Category in WebNonVehicle for Relocation and General Category
    Public Shared Function Get_WebSetupIDPlusFerryData(ByVal CityCode As String, _
                                               ByVal BrandCode As String, _
                                               ByVal classType As String, _
                                               ByVal catId As String) As XmlDocument

        Dim comCode As String = ""
        Dim setUpid As String = ""
        Dim xmlDom As XmlDocument = Nothing
        Dim param(1) As Aurora.Common.Data.Parameter
        Dim params(5) As Aurora.Common.Data.Parameter

        Try
            param(0) = New Aurora.Common.Data.Parameter("BrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            param(1) = New Aurora.Common.Data.Parameter("@comCode", DbType.String, 5, Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_getComCode", param)
            comCode = param(1).Value
            If String.IsNullOrEmpty(comCode) Then Throw New Exception("ERROR: Retrieving ComCode")

            
            params(0) = New Aurora.Common.Data.Parameter("sComCode", DbType.String, comCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sCtyCode", DbType.String, CityCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("BrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sClaCode", DbType.String, classType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("sCatId", DbType.String, IIf(String.IsNullOrEmpty(catId), DBNull.Value, catId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("WebSetupId", DbType.String, 5, Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("WEBP_getHreItmWebSetupId", params)
            setUpid = params(5).Value
            If String.IsNullOrEmpty(setUpid) Then Throw New Exception("ERROR: Retrieving WebSetup ID. No record found")

            xmlDom = Aurora.Common.Data.ExecuteSqlXmlSPDoc("WEBP_getForWebExtraHireSetupFerry", setUpid)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return xmlDom
    End Function

    ''mia:rev 17-Oct-2016 - Add new Category in WebNonVehicle for Relocation and General Category
    Public Shared Function InsertUpdateWebNonVehicleItems(ByVal CityCode As String, _
                                                          ByVal BrandCode As String, _
                                                          ByVal classType As String, _
                                                          ByVal AddUsrId As String, _
                                                          ByVal xmlstring As String, _
                                                          catId As String) As String

        Dim params(6) As Aurora.Common.Data.Parameter
        Dim retvalue As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("WebCtyCode", DbType.String, CityCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("WebBrdCode", DbType.String, BrandCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("WebClaID", DbType.String, classType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("retValue", DbType.String, 200, Common.Data.Parameter.ParameterType.AddOutParameter)
            params(5) = New Aurora.Common.Data.Parameter("doc", DbType.String, xmlstring, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("sCatId", DbType.String, IIf(String.IsNullOrEmpty(catId), DBNull.Value, catId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)


            Dim Spname As String = "WEBP_InsertUpdateWebNonVehicleItems"
            Aurora.Common.Data.ExecuteOutputSP(Spname, params)

            retvalue = params(4).Value
            If retvalue.Contains("ERROR") Then Throw New Exception("Error saving record")

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
        Return retvalue
    End Function

    Public Shared Function Get_WebBrandsFiltered(ByVal cty As String, Optional ByVal usercode As String = "") As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Get_WebBrandsFiltered", result, cty, usercode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Filtered Brands")
        End Try
        Return result
    End Function

    Public Shared Function Get_WebClassFiltered(ByVal ctyCode As String, ByVal brdcode As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("Get_WebClassFiltered", result, ctyCode, brdcode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Web Filtered Class")
        End Try
        Return result
    End Function
#End Region

#Region "Added by : Raj - April 25 - Database interaction code to support inclusive products maintenance tab"

    Public Shared Function addCompletelyNewPackage(ByVal sapID As String, ByVal fromDate As String, ByVal toDate As String) As Long
        Dim num2 As Long
        Dim parameterArray As Aurora.Common.Data.Parameter() = New Aurora.Common.Data.Parameter(7 - 1) {}
        Try
            Dim str As String = sapID
            parameterArray(0) = New Aurora.Common.Data.Parameter("incPrdSapID", DbType.String, str, 1)
            Dim time As DateTime = Convert.ToDateTime(fromDate)
            parameterArray(1) = New Aurora.Common.Data.Parameter("IncPrdSetupCkoFromdate", DbType.DateTime, time, 1)
            Dim time2 As DateTime = Convert.ToDateTime(toDate)
            Dim time3 As New DateTime(time2.Year, time2.Month, time2.Day, &H17, &H3B, 0)
            parameterArray(2) = New Aurora.Common.Data.Parameter("IncPrdSetupCkoToDate", DbType.DateTime, time3, 1)
            Dim num4 As Byte = 1
            parameterArray(3) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Byte, num4, 1)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
            parameterArray(4) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, 1)
            Dim str2 As String = "Backend"
            parameterArray(5) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, str2, 1)
            Dim num3 As Int64 = 0
            parameterArray(6) = New Aurora.Common.Data.Parameter("id", DbType.Int64, num3, 2)
            Aurora.Common.Data.ExecuteOutputSP("Product_AddIncPrdSetup", parameterArray)
            num2 = Convert.ToInt64(parameterArray(6).Value)
        Catch exception1 As Exception
            Throw New Exception("ERROR: Adding entry to 'InclusiveProductSetup' table")
        End Try
        Return num2
    End Function

    Public Shared Function updateLastInclusivePrdSetupDate(ByVal fromDate As String, ByVal sapID As String) As String
        Dim str As String = ""
        Dim toDt As Date
        Try
            toDt = Convert.ToDateTime(fromDate).ToLongDateString
            Dim lastToDate As DateTime
            lastToDate = New Date(toDt.Year, toDt.Month, toDt.Day, 23, 59, 0)
            lastToDate = lastToDate.AddDays(-1)

            Dim parameterArray As Aurora.Common.Data.Parameter() = New Aurora.Common.Data.Parameter(3 - 1) {}
            parameterArray(0) = New Aurora.Common.Data.Parameter("dtUpd", DbType.DateTime, lastToDate, 1)
            parameterArray(1) = New Aurora.Common.Data.Parameter("incprdsetupsapid", DbType.String, sapID, 1)
            Dim num As Integer = 0
            parameterArray(2) = New Aurora.Common.Data.Parameter("err", DbType.Int32, num, 2)
            Aurora.Common.Data.ExecuteOutputSP("Product_UpdSetupDate", parameterArray)
            str = parameterArray(2).Value
        Catch exception1 As Exception
            Throw New Exception("ERROR: Updating product setup date in InclusiveProductSetup table")
        End Try
        Return str
    End Function


    Public Shared Function removeInclusiveProducts_FromPackage(ByVal dsIncl As DataSet, ByVal selectedVal As String) As String
        Dim returnedMessage As String = ""
        Dim i As Integer = 0
        Try
            For i = 0 To dsIncl.Tables(0).Rows.Count - 1
                If (dsIncl.Tables(0).Rows(i)(6) = False) Then
                    Dim params(3) As Aurora.Common.Data.Parameter
                    Dim incprdSetupID As Int64 = Convert.ToInt64(selectedVal)
                    params(0) = New Aurora.Common.Data.Parameter("incprdSetupID", DbType.Int64, incprdSetupID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim incLinkID As Int64 = Convert.ToInt64(dsIncl.Tables(0).Rows(i)(5))
                    params(1) = New Aurora.Common.Data.Parameter("incLinkID", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim modUsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
                    params(2) = New Aurora.Common.Data.Parameter("modUsrID", DbType.String, modUsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim err As Integer = 0
                    params(3) = New Aurora.Common.Data.Parameter("err", DbType.Int32, err, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                    Aurora.Common.Data.ExecuteOutputSP("Product_UpdateIncStatus", params)
                    returnedMessage = params(3).Value
                End If
            Next
        Catch ex As Exception
            Throw New Exception("ERROR: Adding updating/removing entries in 'InclusiveLinkedProduct' & 'InclusiveLinkedProductVehicle' tables")
        End Try

        Return returnedMessage
    End Function

    Public Shared Function getMaxDteforSaleableProduct(ByVal sapIdP As String) As DataSet
        Dim params(0) As Aurora.Common.Data.Parameter
        Dim ds As DataSet
        Dim sapID As String = sapIdP
        Try
            params(0) = New Aurora.Common.Data.Parameter("sapID", DbType.String, sapID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            ds = Aurora.Common.Data.ExecuteOutputDatasetSP("Product_GetMaxDatefoSapID", params)
        Catch ex As Exception
            Throw New Exception("ERROR: Adding retrieving Max Setup date for saleable product from InclusiveProductSetup table")
        End Try

        Return ds
    End Function

    Public Shared Function addExistingInclusiveProductIncLinkID(ByVal prdID As String, ByVal incprdSetupID As Int64) As Int64
        Dim incLinkID As Int64 = 0
        Try
            Dim paramsE(2) As Aurora.Common.Data.Parameter
            paramsE(0) = New Aurora.Common.Data.Parameter("incPrdSetupId", DbType.Int64, incprdSetupID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            paramsE(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, prdID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim id As Int64 = 0
            paramsE(2) = New Aurora.Common.Data.Parameter("id", DbType.Int64, id, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_PrdInclExistsRet_IncLinkID", paramsE)
            Dim retMessage As String = paramsE(2).Value
            If (retMessage <> "") Then
                incLinkID = Convert.ToInt64(paramsE(2).Value)
            End If
            Return incLinkID
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving list of incLinkID For a inclusive product")
        End Try
        Return incLinkID
    End Function


    Public Shared Function addExistingInclusiveProductAndVehicles_ToPackage(ByVal dsNewAdd As DataSet, ByVal fromDate As String, ByVal toDate As String, ByVal i As Integer, ByVal incprdSetupID As Int64) As String
        Dim strArr() As String
        Dim returnedMessage As String = ""

        Try
            Dim prdFullName As String = dsNewAdd.Tables(0).Rows(i)(0)
            Dim vehicle As String = ""
            strArr = prdFullName.Split("-")
            'gets the product short name
            Dim prdshortName As String = strArr(0).Trim()
            Dim prdID As String = ""

            prdID = dsNewAdd.Tables(0).Rows(i)(7)
            '''''''add to inclusivelinkedproduct table''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim params(6) As Aurora.Common.Data.Parameter
            'Dim incprdSetupID As Int64 = Convert.ToInt64(Session("selectedVal"))
            params(0) = New Aurora.Common.Data.Parameter("incprdSetupID", DbType.Int64, incprdSetupID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'get the product short name
            params(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, prdID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim incMaxNo As String = Nothing
            params(2) = New Aurora.Common.Data.Parameter("incMaxNo", DbType.String, incMaxNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim integrityNo As Byte = 1
            params(3) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Byte, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
            params(4) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim prgName As String = "Backend"
            params(5) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim id As Int64 = 0
            params(6) = New Aurora.Common.Data.Parameter("id", DbType.Int64, id, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            'Aurora.Common.Data.ExecuteOutputSP("Product_AddIncIncPrdPkg", params)
            Dim paramsE(4) As Aurora.Common.Data.Parameter
            paramsE(0) = New Aurora.Common.Data.Parameter("incPrdSetupId", DbType.Int64, incprdSetupID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            paramsE(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, prdID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'Dim ckoFrom As DateTime = Convert.ToDateTime(ckoFromDropdown.SelectedItem.Text)
            Dim ckoFrom As DateTime = Convert.ToDateTime(fromDate)
            paramsE(2) = New Aurora.Common.Data.Parameter("ckoFrom", DbType.DateTime, ckoFrom, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'Dim ckoTo As DateTime = Convert.ToDateTime(ckoToTextbox.Text)
            Dim ckoTo As DateTime = Convert.ToDateTime(toDate)

            paramsE(3) = New Aurora.Common.Data.Parameter("ckoTo", DbType.DateTime, ckoTo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim id1 As Int64 = 0
            paramsE(4) = New Aurora.Common.Data.Parameter("id", DbType.Int64, id1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_PrdInclExists", paramsE)
            'value of 1 indicates that the inclusiveproduct already exists so do not add to inclusive products table
            Dim retMessage As String = paramsE(4).Value
            Dim incLinkID As Int64 = 0

            'if the linked product exists but the product is ACTIVE get the inclinkid - alter stored procedure to reflect the changes
            If (retMessage <> "") Then
                incLinkID = Convert.ToInt64(paramsE(4).Value)
            End If

            'if the linked product exists but the product is INACTIVE get the new inclinkid - alter stored procedure to reflect the changes
            If (retMessage = "") Then
                Aurora.Common.Data.ExecuteOutputSP("Product_AddIncIncPrdPkg", params)
                returnedMessage = params(6).Value
                incLinkID = Convert.ToInt64(params(6).Value)
            End If

            'get the vehicle short name
            prdFullName = dsNewAdd.Tables(0).Rows(i)(8)
            strArr = prdFullName.Split(",")
            Dim incr As Integer = 0

            If (strArr(0).Trim().ToUpper() = "ALL") Then
                'remove the inclusive product from the existing package
                Dim params1(3) As Aurora.Common.Data.Parameter
                params1(0) = New Aurora.Common.Data.Parameter("incprdSetupID", DbType.Int64, incprdSetupID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                Dim incLinkID1 As Int64 = Convert.ToInt64(dsNewAdd.Tables(0).Rows(i)(5))

                'Modified June 24 2011''''''''''''''''''''''''''''''''''''''''''''''''
                'params1(1) = New Aurora.Common.Data.Parameter("incLinkID", DbType.Int64, incLinkID1, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params1(1) = New Aurora.Common.Data.Parameter("incLinkID", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                'Modified June 24 2011''''''''''''''''''''''''''''''''''''''''''''''''

                Dim modUsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
                params1(2) = New Aurora.Common.Data.Parameter("modUsrID", DbType.String, modUsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                Dim err As Integer = 0
                params1(3) = New Aurora.Common.Data.Parameter("err", DbType.Int32, err, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("Product_UpdateIncStatus_1", params1)
                returnedMessage = params(3).Value
            End If

            If (strArr(0).Trim().ToUpper() <> "ALL") Then
                For incr = 0 To strArr.Length - 1
                    vehicle = strArr(incr).Trim()
                    Dim paramsNew(6) As Aurora.Common.Data.Parameter
                    paramsNew(0) = New Aurora.Common.Data.Parameter("incLinkId", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    'get the product short name
                    paramsNew(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, vehicle, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim IncLnkPrdVehIsInclude As Boolean
                    If (dsNewAdd.Tables(0).Rows(i)(4).ToString().ToLower() = "no") Then
                        IncLnkPrdVehIsInclude = False
                    Else
                        IncLnkPrdVehIsInclude = True
                    End If
                    paramsNew(2) = New Aurora.Common.Data.Parameter("IncLnkPrdVehIsInclude", DbType.Boolean, IncLnkPrdVehIsInclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    integrityNo = 1
                    paramsNew(3) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Byte, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    UsrID = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
                    paramsNew(4) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    prgName = "Backend"
                    paramsNew(5) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim err1 As Integer = 0
                    paramsNew(6) = New Aurora.Common.Data.Parameter("err", DbType.Int32, err1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

                    Dim paramsNew0(3) As Aurora.Common.Data.Parameter
                    paramsNew0(0) = New Aurora.Common.Data.Parameter("incLinkID", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    'get the product short name
                    paramsNew0(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, vehicle, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    paramsNew0(2) = New Aurora.Common.Data.Parameter("IncLnkPrdVehIsInclude", DbType.Boolean, IncLnkPrdVehIsInclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim rows1 As Integer = 0
                    paramsNew0(3) = New Aurora.Common.Data.Parameter("rows", DbType.Int32, rows1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                    Aurora.Common.Data.ExecuteOutputSP("Product_PrdInclVehExists", paramsNew0)

                    returnedMessage = paramsNew0(3).Value
                    If (returnedMessage = "0") Then
                        Aurora.Common.Data.ExecuteOutputSP("Product_AddIncPrdVeh", paramsNew)
                        returnedMessage = paramsNew(6).Value
                    End If
                Next
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Adding entry to existing product/linked product in 'InclusiveLinkedProduct' & 'InclusiveLinkedProductVehicle' tables")
        End Try
        Return returnedMessage

    End Function

    Public Shared Function getInclusiveProducts(ByVal dsincl As DataSet, ByVal prdSetupID As Int64) As DataSet
        Dim dsRet As New DataSet
        Try
            dsRet = Aurora.Common.Data.ExecuteDataSetSP("Aurora_GetInclusiveProducts", dsincl, prdSetupID)
        Catch ex As Exception
            Throw New Exception("ERROR: Adding retrieving list of Inclusive Linked Products")
        End Try
        Return dsRet
    End Function

    Public Shared Function getSaleableProducts(ByVal dsincl As DataSet, ByVal sapID As String) As DataSet
        Dim dsRet As New DataSet
        Try
            dsRet = Aurora.Common.Data.ExecuteDataSetSP("Aurora_GetSaleableProducts", dsincl, sapID)
        Catch ex As Exception
            Throw New Exception("ERROR: Adding retrieving list of Saleable Products")
        End Try
        Return dsRet
    End Function

    Public Shared Function getvehicles(ByVal dsVehicles As DataSet, ByVal sapCityCode As String) As DataSet
        Dim dsRet As New DataSet
        Try
            dsRet = Aurora.Common.Data.ExecuteDataSetSP("Aurora_GetVehicles", dsVehicles, "VehicleProduct", "", sapCityCode, Nothing, Nothing, Nothing, Aurora.Common.UserSettings.Current.UsrCode.ToLower())
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving list of vehicles- @case='VehicleProduct'")
        End Try
        Return dsRet
    End Function

    Public Shared Function checkDateOverlapforSapID(ByVal sapID As String, ByVal fromDate As DateTime) As Integer
        Dim count As Integer = 0
        Try
            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("fromDate", DbType.DateTime, fromDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sapID", DbType.String, sapID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("count", DbType.Int32, count, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_DateSaleableOverlap", params)
            Dim retMessage As String = params(2).Value

            If (retMessage.ToString() <> "") Then
                count = Convert.ToInt32(retMessage)
            Else
                count = 0
            End If

        Catch ex As Exception
            Throw New Exception("ERROR: Checking overlapping dates for a saleable product")
        End Try

        Return count
    End Function

    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Shared Function getVehiclesforLinkId(ByVal dsVehicles As DataSet, ByVal incLinkID As Int64) As DataSet
        Dim dsRet As New DataSet
        Try
            dsRet = Aurora.Common.Data.ExecuteDataSetSP("Product_LinkedPrdVeh", dsVehicles, incLinkID)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving list of vehicles for Link ID")
        End Try
        Return dsRet
    End Function

    Public Shared Function removeInclusiveVehicle(ByVal incLinkID As Int64, ByVal prdVehID As String) As Integer
        Dim retInt As Integer = 0
        Try
            Dim paramsE(2) As Aurora.Common.Data.Parameter
            paramsE(0) = New Aurora.Common.Data.Parameter("incLinkID", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            paramsE(1) = New Aurora.Common.Data.Parameter("prdVehID", DbType.String, prdVehID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim err As Integer = 0
            paramsE(2) = New Aurora.Common.Data.Parameter("err", DbType.Int32, err, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_RemoveInclusiveVehicle", paramsE)
            retInt = Convert.ToInt32(paramsE(2).Value)
            Return retInt
        Catch ex As Exception
            Throw New Exception("ERROR: Removing vehicle for incLinkID.")
        End Try
        Return retInt
    End Function

    Public Shared Function addInclusiveVehicle(ByVal incLinkID As Int64, ByVal prdID As String, ByVal IncLnkPrdVehIsInclude As Boolean) As Int64
        Dim returnedMessage As Int64
        Try
            Dim paramsNew(6) As Aurora.Common.Data.Parameter
            paramsNew(0) = New Aurora.Common.Data.Parameter("incLinkId", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            paramsNew(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, prdID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            paramsNew(2) = New Aurora.Common.Data.Parameter("IncLnkPrdVehIsInclude", DbType.Boolean, IncLnkPrdVehIsInclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim integrityNo As Byte = 1
            paramsNew(3) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Byte, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
            paramsNew(4) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim prgName As String = "Backend"
            paramsNew(5) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim err As Integer = 0
            paramsNew(6) = New Aurora.Common.Data.Parameter("err", DbType.Int64, err, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_AddIncPrdVeh", paramsNew)
            returnedMessage = Convert.ToInt64(paramsNew(6).Value)

        Catch ex As Exception
            Throw New Exception("ERROR: Adding vehicle for incLinkID.")
        End Try
        Return returnedMessage

    End Function

    Public Shared Function updInclusiveVehicle(ByVal incLinkID As Int64, ByVal IncLnkPrdVehIsInclude As Boolean) As Integer
        Dim returnedMessage As Integer
        Try
            Dim paramsNew(3) As Aurora.Common.Data.Parameter
            paramsNew(0) = New Aurora.Common.Data.Parameter("incLinkId", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            paramsNew(1) = New Aurora.Common.Data.Parameter("IncLnkPrdVehIsInclude", DbType.Boolean, IncLnkPrdVehIsInclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
            paramsNew(2) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim err As Integer = 0
            paramsNew(3) = New Aurora.Common.Data.Parameter("err", DbType.Int32, err, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_UpdIncPrdVeh", paramsNew)
            returnedMessage = paramsNew(3).Value

        Catch ex As Exception
            Throw New Exception("ERROR: Updating vehicle for incLinkID.")
        End Try
        Return returnedMessage

    End Function

    Public Shared Function addNewInclusiveProductAndVehicles_ToPackage(ByVal dsNew As DataSet, ByVal i As Integer, ByVal incprdSetupID As Int64) As String
        Dim strArr() As String
        Dim returnedMessage As String = ""

        Try
            'gets the product short name
            Dim prdNonvehID As String = dsNew.Tables(0).Rows(i)(7)
            '''''''add to inclusivelinkedproduct table''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            Dim params(6) As Aurora.Common.Data.Parameter
            'Dim incprdSetupID As Int64 = Convert.ToInt64(Session("selectedVal"))
            params(0) = New Aurora.Common.Data.Parameter("incprdSetupID", DbType.Int64, incprdSetupID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            'get the product short name
            params(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, prdNonvehID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim incMaxNo As String = Nothing
            params(2) = New Aurora.Common.Data.Parameter("incMaxNo", DbType.String, incMaxNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim integrityNo As Byte = 1
            params(3) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Byte, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim UsrID As String = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
            params(4) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim prgName As String = "Backend"
            params(5) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim id As Int64 = 0
            params(6) = New Aurora.Common.Data.Parameter("id", DbType.Int64, id, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Product_AddIncIncPrdPkg", params)
            returnedMessage = params(6).Value

            Dim incLinkID As Int64 = Convert.ToInt64(params(6).Value)
            Dim prdALLIds As String = dsNew.Tables(0).Rows(i)(8)
            Dim vehicleID As String = ""
            'strArr = prdFullName.Split("-")
            'get the vehicle short name
            prdALLIds = dsNew.Tables(0).Rows(i)(8)
            strArr = prdALLIds.Split(",")
            Dim incr As Integer = 0

            'If (strArr(0).Trim().ToUpper() <> "ALL") Then
            If (dsNew.Tables(0).Rows(i)(1).Trim().ToUpper() <> "ALL") Then
                For incr = 0 To strArr.Length - 1
                    vehicleID = strArr(incr).Trim()
                    Dim paramsNew(6) As Aurora.Common.Data.Parameter
                    paramsNew(0) = New Aurora.Common.Data.Parameter("incLinkId", DbType.Int64, incLinkID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    'get the product short name
                    paramsNew(1) = New Aurora.Common.Data.Parameter("prdID", DbType.String, vehicleID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim IncLnkPrdVehIsInclude As Boolean

                    If (dsNew.Tables(0).Rows(i)(4).ToString().ToLower() = "no") Then
                        IncLnkPrdVehIsInclude = False
                    Else
                        IncLnkPrdVehIsInclude = True
                    End If
                    paramsNew(2) = New Aurora.Common.Data.Parameter("IncLnkPrdVehIsInclude", DbType.Boolean, IncLnkPrdVehIsInclude, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    integrityNo = 1
                    paramsNew(3) = New Aurora.Common.Data.Parameter("integrityNo", DbType.Byte, integrityNo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    UsrID = Aurora.Common.UserSettings.Current.UsrCode.ToLower()
                    paramsNew(4) = New Aurora.Common.Data.Parameter("UsrID", DbType.String, UsrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    prgName = "Backend"
                    paramsNew(5) = New Aurora.Common.Data.Parameter("pgrmName", DbType.String, prgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                    Dim err1 As Integer = 0
                    paramsNew(6) = New Aurora.Common.Data.Parameter("err", DbType.Int32, err1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                    Aurora.Common.Data.ExecuteOutputSP("Product_AddIncPrdVeh", paramsNew)
                    returnedMessage = paramsNew(6).Value
                Next
            End If
        Catch ex As Exception
            Throw New Exception("ERROR: Adding new entry to 'InclusiveLinkedProduct' & 'InclusiveLinkedProductVehicle' tables")
        End Try
        Return returnedMessage

    End Function

#End Region

#Region "Raj Dec 7 2011 - Disable Web Package Tab for Non-BSS user on the basis of the return value. 1=Enable, 0=Disable"

    'Public Shared Function CheckUserIsInRole(ByVal usrID As String, ByVal roleCode As String) As Integer
    '    Dim retCount As Integer
    '    Try
    '        Dim params(2) As Aurora.Common.Data.Parameter
    '        params(0) = New Aurora.Common.Data.Parameter("usrID", DbType.String, usrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(1) = New Aurora.Common.Data.Parameter("roleCode", DbType.String, roleCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        Dim count As Integer = 0
    '        params(2) = New Aurora.Common.Data.Parameter("count", DbType.Int32, count, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        Aurora.Common.Data.ExecuteOutputSP("SP_CheckUserForRole", params)
    '        retCount = params(2).Value

    '    Catch ex As Exception
    '        Throw New Exception("ERROR: Getting user status for role.")
    '    End Try
    '    Return retCount

    'End Function

#End Region

#Region "REV:MIA 03NOV2014 - SLOT MANAGEMENT"

    Public Shared Function Get_SlotManagementData(SlotCtyCode As String, LocSlotLocCode As String, sUsrCode As String) As DataSet

        Dim result As New DataSet
        Try
            Dim sp As String = "SlotManagement_LoadData"
            result = Aurora.Common.Data.ExecuteDataSetSP(sp, result, SlotCtyCode, LocSlotLocCode, sUsrCode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Slot Management Data")
        End Try
        Return result
    End Function


    Public Shared Function DeleteUpdate_SlotNumber(Slotid As Integer, LocSlotid As Integer, SlotIsActive As Boolean, LocSlotNumOfCko As Integer, AddUserCode As String) As String
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                Dim sp As String = "SlotManagement_DeleteUpdateSlotNumber"
                Aurora.Common.Data.ExecuteScalarSP(sp, Slotid, LocSlotid, SlotIsActive, LocSlotNumOfCko, AddUserCode)
                ''oTransaction.RollbackTransaction()
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Throw New Exception("ERROR: Saving Slot Management Data")
            End Try
        End Using

        Return "OK"
    End Function


#End Region

End Class
