Partial Class BlockingRuleDataSet

    Partial Class BlockingRuleRow

        Public ReadOnly Property ProductDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleProductRow As BlockingRuleProductRow In Me.GetBlockingRuleProductRows()
                    If blockingRuleProductRow.ProductRow IsNot Nothing Then
                        result.Add(blockingRuleProductRow.ProductRow.PrdShortName)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property ProductIsIncl() As Boolean
            Get
                For Each blockingRuleProductRow As BlockingRuleProductRow In Me.GetBlockingRuleProductRows()
                    If Not blockingRuleProductRow.IsBlpIsInclNull AndAlso Not blockingRuleProductRow.BlpIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleProductRow As BlockingRuleProductRow In Me.GetBlockingRuleProductRows()
                    blockingRuleProductRow.BlpIsIncl = value
                Next
            End Set
        End Property


        Public ReadOnly Property BrandDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleBrandRow As BlockingRuleBrandRow In Me.GetBlockingRuleBrandRows()
                    If blockingRuleBrandRow.BrandRow IsNot Nothing Then
                        result.Add(blockingRuleBrandRow.BrandRow.BrdCode & " - " & blockingRuleBrandRow.BrandRow.BrdName)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property BrandIsIncl() As Boolean
            Get
                For Each blockingRuleBrandRow As BlockingRuleBrandRow In Me.GetBlockingRuleBrandRows()
                    If Not blockingRuleBrandRow.IsBlbIsInclNull AndAlso Not blockingRuleBrandRow.BlbIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleBrandRow As BlockingRuleBrandRow In Me.GetBlockingRuleBrandRows()
                    blockingRuleBrandRow.BlbIsIncl = value
                Next
            End Set
        End Property


        Public ReadOnly Property PackageDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRulePackageRow As BlockingRulePackageRow In Me.GetBlockingRulePackageRows()
                    If blockingRulePackageRow.PackageRow IsNot Nothing Then
                        result.Add(blockingRulePackageRow.PackageRow.PkgCode)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property PackageIsIncl() As Boolean
            Get
                For Each blockingRulePackageRow As BlockingRulePackageRow In Me.GetBlockingRulePackageRows()
                    If Not blockingRulePackageRow.IsBrpIsInclNull AndAlso Not blockingRulePackageRow.BrpIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRulePackageRow As BlockingRulePackageRow In Me.GetBlockingRulePackageRows()
                    blockingRulePackageRow.BrpIsIncl = value
                Next
            End Set
        End Property

        Public ReadOnly Property LocationFromDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleLocationRow As BlockingRuleLocationRow In Me.GetBlockingRuleLocationRows()
                    If blockingRuleLocationRow.LocationRow IsNot Nothing _
                     AndAlso blockingRuleLocationRow.BrlDirection = "From" Then
                        result.Add(blockingRuleLocationRow.LocationRow.LocCode)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property LocationFromIsIncl() As Boolean
            Get
                For Each blockingRuleLocationRow As BlockingRuleLocationRow In Me.GetBlockingRuleLocationRows()
                    If blockingRuleLocationRow.BrlDirection = "From" _
                     AndAlso Not blockingRuleLocationRow.IsBrlIsInclNull AndAlso Not blockingRuleLocationRow.BrlIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleLocationRow As BlockingRuleLocationRow In Me.GetBlockingRuleLocationRows()
                    If blockingRuleLocationRow.BrlDirection = "From" Then
                        blockingRuleLocationRow.BrlIsIncl = value
                    End If
                Next
            End Set
        End Property

        Public ReadOnly Property LocationToDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleLocationRow As BlockingRuleLocationRow In Me.GetBlockingRuleLocationRows()
                    If blockingRuleLocationRow.LocationRow IsNot Nothing _
                     AndAlso blockingRuleLocationRow.BrlDirection = "To" Then
                        result.Add(blockingRuleLocationRow.LocationRow.LocCode)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property LocationToIsIncl() As Boolean
            Get
                For Each blockingRuleLocationRow As BlockingRuleLocationRow In Me.GetBlockingRuleLocationRows()
                    If blockingRuleLocationRow.BrlDirection = "To" _
                     AndAlso Not blockingRuleLocationRow.IsBrlIsInclNull AndAlso Not blockingRuleLocationRow.BrlIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleLocationRow As BlockingRuleLocationRow In Me.GetBlockingRuleLocationRows()
                    If blockingRuleLocationRow.BrlDirection = "To" Then
                        blockingRuleLocationRow.BrlIsIncl = value
                    End If
                Next
            End Set
        End Property

        Public ReadOnly Property AgentCountryDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleAgentCountryRow As BlockingRuleAgentCountryRow In Me.GetBlockingRuleAgentCountryRows()
                    If blockingRuleAgentCountryRow.CountryRow IsNot Nothing Then
                        result.Add(blockingRuleAgentCountryRow.CountryRow.CtyName)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property AgentCountryIsIncl() As Boolean
            Get
                For Each blockingRuleAgentCountryRow As BlockingRuleAgentCountryRow In Me.GetBlockingRuleAgentCountryRows()
                    If Not blockingRuleAgentCountryRow.IsBrcIsInclNull AndAlso Not blockingRuleAgentCountryRow.BrcIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleAgentCountryRow As BlockingRuleAgentCountryRow In Me.GetBlockingRuleAgentCountryRows()
                    blockingRuleAgentCountryRow.BrcIsIncl = value
                Next
            End Set
        End Property

        Public ReadOnly Property AgentCategoryDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleAgentCategoryRow As BlockingRuleAgentCategoryRow In Me.GetBlockingRuleAgentCategoryRows()
                    If blockingRuleAgentCategoryRow.CodeRow IsNot Nothing Then
                        result.Add(blockingRuleAgentCategoryRow.CodeRow.CodDesc & " - " & blockingRuleAgentCategoryRow.CodeRow.CodDesc)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property AgentCategoryIsIncl() As Boolean
            Get
                For Each blockingRuleAgentCategoryRow As BlockingRuleAgentCategoryRow In Me.GetBlockingRuleAgentCategoryRows()
                    If Not blockingRuleAgentCategoryRow.IsBacIsInclNull AndAlso Not blockingRuleAgentCategoryRow.BacIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleAgentCategoryRow As BlockingRuleAgentCategoryRow In Me.GetBlockingRuleAgentCategoryRows()
                    blockingRuleAgentCategoryRow.BacIsIncl = value
                Next
            End Set
        End Property

        Public ReadOnly Property AgentDescription() As String
            Get
                Dim result As New List(Of String)
                For Each blockingRuleAgentRow As BlockingRuleAgentRow In Me.GetBlockingRuleAgentRows()
                    If blockingRuleAgentRow.AgentRow IsNot Nothing Then
                        result.Add(blockingRuleAgentRow.AgentRow.AgnCode)
                    End If
                Next
                Return String.Join(", ", result.ToArray())
            End Get
        End Property

        Public Property AgentIsIncl() As Boolean
            Get
                For Each blockingRuleAgentRow As BlockingRuleAgentRow In Me.GetBlockingRuleAgentRows()
                    If Not blockingRuleAgentRow.IsBraIsInclNull AndAlso Not blockingRuleAgentRow.BraIsIncl Then
                        Return False
                    End If
                Next
                Return True
            End Get
            Set(ByVal value As Boolean)
                For Each blockingRuleAgentRow As BlockingRuleAgentRow In Me.GetBlockingRuleAgentRows()
                    blockingRuleAgentRow.BraIsIncl = value
                Next
            End Set
        End Property


    End Class

End Class
