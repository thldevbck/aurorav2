
Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Common




Public Module DataRepository

#Region "Fleet"

    Public Function GetRepairsList(ByVal SearchFieldValue As String, ByVal FleetAssetId As String, ByVal UserCode As String) As DataSet
        '@sSearchField    VARCHAR  (64) = '',  
        '@sFleetAssetId    VARCHAR  (64) = '',  
        '-- KX Chnages :RKS  
        '@sUsrCode     VARCHAR  (64) = ''  
        Dim xmlResults As XmlDocument
        xmlResults = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getRepairsList", SearchFieldValue, FleetAssetId, UserCode)

        Dim dsResults As DataSet = New DataSet()
        dsResults.ReadXml(New XmlNodeReader(xmlResults))
        Return dsResults

    End Function


    Public Function GetFleetRepair(ByVal RepairId As String, ByVal FleetAssetId As String, ByVal UnitNo As String) As XmlDocument
        'sp_getFleetRepair
        '@sRepairId   VARCHAR  (64) = '',  
        '@sFleetAssetId   VARCHAR  (64) = '',  
        '@sUnitNo   VARCHAR  (64) = ''  

        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getFleetRepair", RepairId, FleetAssetId, UnitNo)
        Return oXml
    End Function

    'Public Function GetOpsAndLogsReasons() As DataTable
    '    'GEN_getComboData 'OPLOGREASON'
    '    Dim xmlResults As XmlDocument
    '    xmlResults = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_getComboData", SearchFieldValue, FleetAssetId, UserCode)

    '    Dim dsResults As DataSet = New DataSet()
    '    dsResults.ReadXml(New XmlNodeReader(xmlResults))
    '    Return dsResults
    'End Function


    Public Function GetProviderInfo(ByVal ProviderId As String) As XmlDocument
        '@case VARCHAR(30) = '',  
        '@param1 VARCHAR(64) = '',  
        '@param2 VARCHAR(64) = '',  
        '@param3 VARCHAR(500)= '',  
        '@param4 VARCHAR(64) = '',  
        '@param5 VARCHAR(64) = ''  
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_GetPopUpData", "OPLOGFLEETREPPROVIDERID", ProviderId, "", "", "", "")
        Return oXml

    End Function

#End Region

#Region "Substitution Rule"

    Public Function GetSubstitutionRuleProductList(ByVal country As String, ByVal usercode As String, _
            ByVal productId As String, ByVal isNew As String, ByVal dateFrom As Nullable(Of DateTime), _
            ByVal dateTo As Nullable(Of DateTime), ByVal locationFrom As String) As XmlDocument

        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("OPL_getSubstitutionRuleProductList", country, _
            usercode, productId, isNew, dateFrom, dateTo, locationFrom)

    End Function

    Public Function UpdateSubstitutionRuleProductList(ByVal xmlDoc As XmlDocument) As String

        Return Aurora.Common.Data.ExecuteSqlXmlSP("OPL_updateSubstitutionRuleProductList", xmlDoc.InnerXml)

    End Function

    Public Function ListFleetAlternatives(ByVal search As String, ByVal loggedInUserCode As String) As DataSet

        Dim oXmlDoc As XmlDocument
        oXmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_listFleetAlternatives", search, loggedInUserCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        Return ds

    End Function

#End Region

#Region "Fleet Status"

    Public Function GetFleetStatus( _
     ByVal fleetStatusDataSet As FleetStatusDataSet, _
     ByVal ctyCode As String, _
     ByVal brdCode As String, _
     ByVal travelYear As Date) As FleetStatusDataSet

        'ALTER PROCEDURE [dbo].[OpsAndLogs_GetFleetStatus]
        '	@ctyCode AS varchar(2),
        '	@brdCode AS varchar(1),
        '	@travelYear	AS datetime

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("OpsAndLogs_GetFleetStatus", result, ctyCode, brdCode, travelYear)

        If result.Tables.Count >= 3 Then
            Aurora.Common.Data.CopyDataTable(result.Tables(0), fleetStatusDataSet.FlexWeekNumber)
            Aurora.Common.Data.CopyDataTable(result.Tables(1), fleetStatusDataSet.Product)
            Aurora.Common.Data.CopyDataTable(result.Tables(2), fleetStatusDataSet.FleetStatus)
        End If

        Return fleetStatusDataSet
    End Function

    Public Function GetFleetStatusLookups( _
     ByVal fleetStatusDataSet As FleetStatusDataSet, _
     ByVal comCode As String) As FleetStatusDataSet

        'ALTER PROCEDURE [dbo].[OpsAndLogs_GetFleetStatusLookup]
        '	@ComCode AS varchar(64)

        Aurora.Common.Data.ExecuteTableSP("OpsAndLogs_GetFleetStatusLookup", fleetStatusDataSet.FleetStatusLookup, comCode)
        Return fleetStatusDataSet
    End Function

#End Region

#Region "Blocking Rule"

    Public Function GetBlockingRuleLookups( _
     ByVal blockingRuleDataSet As BlockingRuleDataSet, _
     ByVal comCode As String, _
     ByVal ctyCode As String) As BlockingRuleDataSet

        'ALTER PROCEDURE [dbo].[OpsAndLogs_GetBlockingRuleLookups]
        '	@comCode AS varchar(64),
        '	@ctyCode AS varchar(2)

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("OpsAndLogs_GetBlockingRuleLookups", result, comCode, ctyCode)

        If result.Tables.Count >= 5 Then
            Aurora.Common.Data.CopyDataTable(result.Tables(0), blockingRuleDataSet.Code)
            Aurora.Common.Data.CopyDataTable(result.Tables(1), blockingRuleDataSet.Country)
            Aurora.Common.Data.CopyDataTable(result.Tables(2), blockingRuleDataSet.Brand)
            Aurora.Common.Data.CopyDataTable(result.Tables(3), blockingRuleDataSet.Location)
            Aurora.Common.Data.CopyDataTable(result.Tables(4), blockingRuleDataSet.Product)
        End If

        Return blockingRuleDataSet
    End Function

    Public Function GetBlockingRule( _
     ByVal blockingRuleDataSet As BlockingRuleDataSet, _
     ByVal comCode As String, _
     ByVal ctyCode As String, _
     ByVal blrId As String) As BlockingRuleDataSet

        'ALTER PROCEDURE [dbo].[OpsAndLogs_GetBlockingRule]
        '	@comCode AS varchar(64),
        '	@ctyCode AS varchar(2),
        '	@blrId AS varchar(64) = NULL

        Dim result As New DataSet()
        Aurora.Common.Data.ExecuteDataSetSP("OpsAndLogs_GetBlockingRule", result, comCode, ctyCode, IIf(String.IsNullOrEmpty(blrId), DBNull.Value, blrId))

        If result.Tables.Count >= 10 Then
            Aurora.Common.Data.CopyDataTable(result.Tables(0), blockingRuleDataSet.BlockingRule)
            Aurora.Common.Data.CopyDataTable(result.Tables(1), blockingRuleDataSet.BlockingRuleAgent)
            Aurora.Common.Data.CopyDataTable(result.Tables(2), blockingRuleDataSet.Agent)
            Aurora.Common.Data.CopyDataTable(result.Tables(3), blockingRuleDataSet.BlockingRuleAgentCategory)
            Aurora.Common.Data.CopyDataTable(result.Tables(4), blockingRuleDataSet.BlockingRuleAgentCountry)
            Aurora.Common.Data.CopyDataTable(result.Tables(5), blockingRuleDataSet.BlockingRuleBrand)
            Aurora.Common.Data.CopyDataTable(result.Tables(6), blockingRuleDataSet.BlockingRuleLocation)
            Aurora.Common.Data.CopyDataTable(result.Tables(7), blockingRuleDataSet.BlockingRulePackage)
            Aurora.Common.Data.CopyDataTable(result.Tables(8), blockingRuleDataSet.Package)
            Aurora.Common.Data.CopyDataTable(result.Tables(9), blockingRuleDataSet.BlockingRuleProduct)
        End If

        Return blockingRuleDataSet
    End Function

    Public Function GetBlockingRuleList( _
     ByVal blockingRuleDataSet As BlockingRuleDataSet, _
     ByVal comCode As String, _
     ByVal ctyCode As String) As BlockingRuleDataSet
        Return GetBlockingRule(blockingRuleDataSet, comCode, ctyCode, Nothing)
    End Function

    Public Sub DeleteBlockingRule( _
     ByVal comCode As String, _
     ByVal ctyCode As String, _
     ByVal blrId As String)

        'ALTER PROCEDURE [dbo].[OpsAndLogs_DeleteBlockingRule]
        '	@comCode AS varchar(64),
        '	@ctyCode AS varchar(2),
        '	@blrId AS varchar(64)

        Aurora.Common.Data.ExecuteScalarSP("OpsAndLogs_DeleteBlockingRule", comCode, ctyCode, blrId)

    End Sub

#End Region

#Region "Maintain Relocation Rule"

    'sp_getRelocationRules
    Public Function GetReloactionRules(ByVal CountryCode As String, ByVal UserCode As String, ByVal locFrom As String, ByVal locTo As String, ByVal fleetModel As Nullable(Of Integer)) As XmlDocument

        'CREATE   PROCEDURE [dbo].[sp_getRelocationRules]   
        ' @sScrCtyCode varchar(12) = '',  
        ' @sUsrCode varchar(24) = ''  

        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getRelocationRules", CountryCode, UserCode, locFrom, locTo, fleetModel)
        Return oXml

    End Function

    Public Function UpdateRelocationRule(ByVal ScreenData As XmlDocument, ByVal SaveFlag As Boolean) As XmlDocument
        'CREATE PROCEDURE [dbo].[sp_updateRelocationRules]   
        ' @XMLData Text = DEFAULT,  
        ' @bSave  BIT = 1  

        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_updateRelocationRules", ScreenData.OuterXml, SaveFlag)
        Return oXml

    End Function

#End Region

#Region "Maintain Fleet Model Costs"
    Public Function GetFleetModelCosts(ByVal CountryCode As String, ByVal UserCode As String) As XmlDocument
        'CREATE   PROCEDURE [dbo].[sp_getFleetModelCosts]   
        ' @sSearch   VARCHAR  (64) = '',  
        ' @sLoggedInUsrCode  VARCHAR  (64) = ''  
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getFleetModelCosts", CountryCode, UserCode)
        Return oXml
    End Function

    Public Function UpdateFleetModelCosts(ByVal ScreenData As XmlDocument) As XmlDocument
        'CREATE  PROCEDURE sp_UpdateFleetModelCosts  
        ' @XMLData   TEXT  = DEFAULT  
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_UpdateFleetModelCosts", ScreenData.OuterXml)
        Return oXml
    End Function
#End Region

#Region "Alternative Product List"

    Public Function GetAlternativeProductList(ByVal CountryCode As String, ByVal UserCode As String) As XmlDocument
        'sp_listAlternativeProducts
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_listAlternativeProducts", CountryCode, UserCode)
        Return oXml
    End Function

#End Region

#Region "Alternative Product Management"

    Public Function GetAlternativeProducts(ByVal CountryCode As String, ByVal ProductId As String) As XmlDocument
        'sp_listAlternativeProducts
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getAlternativeProducts", CountryCode, ProductId)
        Return oXml
    End Function

    Public Function UpdateAlternativeProducts(ByVal ScreenData As XmlDocument) As XmlDocument
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_updateAlternativeProducts", ScreenData.OuterXml)
        Return oXml
    End Function

#End Region

#Region "Vehicle Assign"

    Public Function GetVehicleAssetsGrid(ByVal BookingId As String, ByVal UserCode As String) As XmlDocument
        'exec GEN_GetPopUpData @case = 'VEHASSGET_GRID', @param1 = '3333360/1', @param2 = '', @param3 = '', @param4 = '', @param5 = '', @sUsrCode = 'sp7'
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", "VEHASSGET_GRID", BookingId, "", "", "", "", UserCode)
        Return oXml
    End Function

    'Public Function GetVehicleAssetsGrid(ByVal Vehicle As String, ByVal UserCode As String) As XmlDocument
    '    'exec GEN_GetPopUpData @case = 'VALIDATEVEHICLE', @param1 = 'TEST', @param2 = '', @param3 = '', @param4 = '', @param5 = '', @sUsrCode = 'sp7'
    '    Dim oXml As XmlDocument
    '    oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_GetPopUpData", "VALIDATEVEHICLE", Vehicle, "", "", "", "", UserCode)
    '    Return oXml
    'End Function

    Public Function SaveVehicleAssign(ByVal BookingNo As String, ByVal RentalNo As String, ByVal VehicleSpec As String, ByVal UserCode As String) As XmlDocument
        'CREATE PROCEDURE [dbo].[OPL_VehicleAssignSaveAssign]   
        ' @BookingNo VARCHAR(64) = '',    
        ' @RentalNo VARCHAR(64) = '',  
        ' @RentalAllowVehicleSpec VARCHAR(64) = ''  
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("OPL_VehicleAssignSaveAssign", BookingNo, RentalNo, VehicleSpec, UserCode)
        Return oXml
    End Function

    Public Function SaveVehicleAssignNote(ByVal BookingNo As String, ByVal RentalNo As String, ByVal NoteId As String, _
                                          ByVal NoteDesc As String, ByVal NotePriority As Integer, _
                                          ByVal NoteAudienceType As String, ByVal NoteActiveFlag As Boolean, _
                                          ByVal NoteIntegrityNo As Integer, ByVal UserCode As String) As XmlDocument
        'CREATE PROCEDURE [dbo].[OPL_VehicleAssignSaveNoteDetails]   
        '@BookingNo VARCHAR(64) = '',    
        '@RentalNo VARCHAR(64) = '',  
        '@NoteId VARCHAR(64) = '',  
        '@NoteDesc VARCHAR(64) = '',  
        '@NotePriority INT = 1,  
        '@NoteAudienceType VARCHAR(64) = '',  
        '@NoteActiveFlag BIT = 0,  
        '@NoteIntegrityNo INT = 1,  
        '@UserCode VARCHAR(64) = ''  

        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("OPL_VehicleAssignSaveNoteDetails", BookingNo, RentalNo, NoteId, NoteDesc, _
                                                     NotePriority, NoteAudienceType, NoteActiveFlag, NoteIntegrityNo, UserCode)
        Return oXml

    End Function

    Public Function UndoVehicleAssign(ByVal BookingNo As String, ByVal RentalNo As String, ByVal UserCode As String) As XmlDocument
        'CREATE PROCEDURE [dbo].[OPL_VehicleUndoAssign] 
        '     @BookingNo VARCHAR(64) = '',  
        '     @RentalNo VARCHAR(64) = '',       
        '     @UserCode VARCHAR(64)

        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("OPL_VehicleUndoAssign", BookingNo, RentalNo, UserCode)
        Return oXml
    End Function



#End Region

#Region "Vehicle Allocation"

    Public Function GetVehicleAllocationGrid(ByVal CountryCode As String, ByVal UserCode As String) As XmlDocument
        ' CREATE   PROCEDURE [dbo].[sp_getFleetAllocationMaintenence]   
        ' @sScrCtyCode varchar(12) = '',  
        ' @sUsrCode  varchar(64) = ''  
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getFleetAllocationMaintenence", CountryCode, UserCode)
        Return oXml
    End Function

    Public Function SaveVehicleAllocationData(ByVal XMLData As XmlDocument, ByVal SaveFlag As Boolean) As XmlDocument
        'CREATE PROCEDURE [dbo].[sp_updateFleetAllocation] @XMLData Text = DEFAULT,  
        '@bSave  BIT = 1  
        Dim oXml As XmlDocument
        oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_updateFleetAllocation", XMLData.OuterXml, SaveFlag)
        Return oXml
    End Function

#End Region

#Region "Update General Data"

    Public Function UpdateGeneralData(ByVal xmlDoc As XmlDocument, ByVal userCode As String, ByVal programName As String) As String

        Return Aurora.Common.Data.ExecuteSqlXmlSP("GEN_UpdateGeneralData", xmlDoc.InnerXml, userCode, programName)

    End Function

    Public Function UpdateGeneralDataForUndoCheckIn(ByVal bookingRef As String, ByVal userCode As String) As String

        Return Aurora.Common.Data.ExecuteScalarSP("GEN_UpdateGeneralDataForUndoCheckIn", bookingRef, userCode)

    End Function

#End Region

#Region "Tourism Holdings and the Project called PHOENIX"

#Region "Fleet Status Changes"

    Public Function GetFleetStatusExceptions(ByVal FleetStatusId As Int64, ByVal WeekNo As Integer) As DataTable
        Dim dtResults As DataTable = New DataTable
        dtResults = Aurora.Common.Data.ExecuteTableSP("OpsAndLogs_GetFleetStatusExceptions", dtResults, FleetStatusId, WeekNo)
        Return dtResults
    End Function

    Public Function UpdateFleetStatusExceptions(ByVal XmlData As String, ByVal UserId As String, ByVal AddProgramName As String) As String
        Return CStr(Aurora.Common.Data.ExecuteScalarSP("OpsAndLogs_UpdateFleetStatusExceptions", XmlData, UserId, AddProgramName))
    End Function

    Public Function DeleteFleetStatusExceptions(ByVal FsxIdList As String) As String
        Return CStr(Aurora.Common.Data.ExecuteScalarSP("OpsAndLogs_DeleteFleetStatusExceptions", FsxIdList))
    End Function

#End Region

#End Region

#Region "rev:mia August 08 2011 for PPF"

    Public Function GetPPF_WEbGetLocationsAndBrands(ByVal usercode As String, ByVal countryCode As String) As DataSet
        Dim result As New DataSet
        Dim sp As String = "PPF_WEbGetLocationsAndBrands"
        Try

            Aurora.Common.Data.ExecuteDataSetSP(sp, result, usercode, countryCode)


            If result.Tables.Count - 1 = 7 Then
                ''one stored procedure returns unwanted tables.
                ''lets remove it
                While result.Tables.Count - 1 > 2
                    result.Tables.RemoveAt(result.Tables.Count - 1)
                    System.Diagnostics.Debug.WriteLine(result.Tables.Count - 1)
                End While

            End If

        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving PPF Web Locations and Brands")
        End Try
        Return result
    End Function

    Public Function PPF_FuelPriceCalculation(ByVal locCode As String, _
                                             ByVal effectiveDate As String, _
                                             ByVal petrolPrice As Decimal, _
                                             ByVal dieselPrice As Decimal, _
                                             ByVal userId As String, _
                                             ByVal ctyCode As String) As String

        Dim sp As String = "PPF_FuelPriceCalculation"

        Try
            Dim oParamArray(6) As Aurora.Common.Data.Parameter
            oParamArray(0) = New Aurora.Common.Data.Parameter("locCode", System.Data.DbType.String, locCode, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(1) = New Aurora.Common.Data.Parameter("effectiveDate", System.Data.DbType.DateTime, effectiveDate, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(2) = New Aurora.Common.Data.Parameter("petrolPrice", System.Data.DbType.Decimal, petrolPrice, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(3) = New Aurora.Common.Data.Parameter("dieselPrice", System.Data.DbType.Decimal, dieselPrice, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(4) = New Aurora.Common.Data.Parameter("userid", System.Data.DbType.String, userId, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(5) = New Aurora.Common.Data.Parameter("ctyCode", System.Data.DbType.String, ctyCode, Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(6) = New Aurora.Common.Data.Parameter("isOk", System.Data.DbType.Boolean, 1, Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP(sp, oParamArray)

            If (oParamArray(6).Value.ToString = "True") Then
                Return "OK"
            Else
                Return "Failed"
            End If

        Catch ex As Exception
            Throw New Exception("ERROR: Failed doing calculations on PPF_FuelPriceCalculation ")
        End Try
    End Function

    Public Function PPF_GetLatestFuelPriceCalculation(ByVal rateId As Integer) As DataSet

        Dim result As New DataSet
        Dim sp As String = "PPF_DisplayLatestFuelPriceCalculation"

        Try
            Aurora.Common.Data.ExecuteDataSetSP(sp, result, rateId)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving PPF_GetLatestFuelPriceCalculation")
        End Try
        Return result
    End Function


    Public Function PPF_GetRatePerBranch(ByVal locCode As String) As DataSet

        Dim result As New DataSet
        Dim sp As String = "PPF_GetRatePerBranch"
        Try

            Aurora.Common.Data.ExecuteDataSetSP(sp, result, locCode)
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Fuel Rate Per Branch")
        End Try
        Return result
    End Function



    Public Function PPF_GetTBrandName(ByVal brdCode As String) As String
        Dim sp As String = "PPF_GetBrandName"
        Try
            Return CStr(Aurora.Common.Data.ExecuteScalarSP(sp, brdCode))
        Catch ex As Exception
            Throw New Exception("ERROR: Retrieving Get BrandName")
        End Try
        Return ""
    End Function
#End Region

#Region "rev:mia Oct. 2 2013 - Addition of Maintain Relocation Rule"
  
    Public Function GetCountriesLocationRestrictions(Usercode As String) As DataSet
        Dim ds As New DataSet
        Try

            Return Aurora.Common.Data.ExecuteDataSetSP("OPL_GetCountries", ds, _
                                                                          Usercode _
                                                       )
        Catch ex As Exception
            Logging.LogError("GetCountriesLocationRestrictions: ", ex.Message & " , " & ex.StackTrace)
            Return Nothing
        End Try
        Return ds
    End Function

    Public Function GetLocationRestrictions(CtyCode As String) As DataSet
        Dim ds As New DataSet
        Try

            Return Aurora.Common.Data.ExecuteDataSetSP("OPL_LocRestrictionCRUD", ds, _
                                                                                 CtyCode, _
                                                                                "SELECT", _
                                                                                ""
                                                       )
        Catch ex As Exception
            Logging.LogError("GetLocationRestrictions: ", ex.Message & " , " & ex.StackTrace)
            Return Nothing
        End Try
        Return ds
    End Function

    Public Function UpdateLocationRestriction(CtyCode As String, XmlData As String) As String
        ''Logging.LogDebug("UpdateLocationRestriction: XmlData ", XmlData)
        Dim returnValue As String = "OK"
        Try
            Aurora.Common.Data.ExecuteScalarSP("OPL_LocRestrictionCRUD", CtyCode, _
                                                                           "INSERTUPDATE", _
                                                                           XmlData)

        Catch ex As Exception
            Logging.LogError("UpdateLocationRestriction: ", ex.Message & " , " & ex.StackTrace)
            Return "ERROR:" & ex.Message
        End Try

        Return returnValue
    End Function
    

#End Region

#Region "rev:mia 28Aug2014 - AIMS Refactoring for DVASS problem"
    
    Public Function IsActivityExist(unitnumber As Integer, dStartDate As DateTime, dEndtDate As DateTime) As Boolean
        Dim ds As New DataSet
        Dim returnValue As Boolean = False
        Try

            Aurora.Common.Data.ExecuteDataSetSP("AIMS_IsActivityExist", ds, unitnumber, dStartDate, dEndtDate)
            If (ds.Tables.Count <> -1) Then
                returnValue = IIf(ds.Tables(0).Rows(0)(0).ToString = "Yes", True, False)
            End If

        Catch ex As Exception
            Logging.LogError("IsActivityExist: ", ex.Message & " , " & ex.StackTrace)
        End Try
        Return returnValue
    End Function

    ''rev:mia 19Sept2014 - splitting of AIMS Call for DVASS.Today in history: Scotland referendum/election
    Public Function CreateRepairMaintenance(fleet_asset_id As Integer, _
                                                  date_raised As DateTime, _
                                                  LocationCode As String, _
                                                  repair_type As String, _
                                                  other_type_1 As String, _
                                                  other_type_2 As String, _
                                                  repair_code As String, _
                                                  off_fleet_flag As Boolean, _
                                                  estimated_cost As Decimal, _
                                                  actual_cost As Decimal, _
                                                  invoice_number As String, _
                                                  modified_by As String, _
                                                  reason_description As String, _
                                                  start_date_time As DateTime, _
                                                  end_date_time As DateTime, _
                                                  start_odometer As Integer, _
                                                  end_odometer As Integer, _
                                                  completed As Boolean, _
                                                  other_repairer As String, _
                                                  service_id As Integer, _
                                                  service_provider_id As String, _
                                                  ByRef po_number As String, _
                                                  ByRef errorMsg As String) As Boolean


        Dim result As Boolean = True

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                '@fleet_asset_id		int,
                '@date_raised		smalldatetime,
                '@LocationCode		varchar(3),
                '@repair_type		varchar(20),
                '@other_type_1		varchar(20),
                '@other_type_2		varchar(20),
                '@repair_code		varchar(20),
                '@off_fleet_flag		bit,
                '@estimated_cost		numeric,
                '@actual_cost		numeric,
                '@invoice_number		varchar(12),
                '@modified_by		varchar(20),
                '@reason_description	varchar(254),
                '@start_date_time	smalldatetime,
                '@end_date_time		smalldatetime,
                '@start_odometer		int,
                '@end_odometer		int,
                '@completed		bit,
                '@other_repairer		varchar(100),
                '@service_id		int,
                '@service_provider_id	int,
                '@po_number		varchar(10) OUTPUT
                Dim oParamArray(22) As Aurora.Common.Data.Parameter
                oParamArray(0) = New Aurora.Common.Data.Parameter("fleet_asset_id", System.Data.DbType.Int32, fleet_asset_id, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(1) = New Aurora.Common.Data.Parameter("date_raised", System.Data.DbType.DateTime, IIf(date_raised <> DateTime.MinValue, date_raised, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(2) = New Aurora.Common.Data.Parameter("locationCode", System.Data.DbType.String, LocationCode, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(3) = New Aurora.Common.Data.Parameter("repair_type", System.Data.DbType.String, IIf(String.IsNullOrEmpty(repair_type), DBNull.Value, repair_type), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(4) = New Aurora.Common.Data.Parameter("other_type_1", System.Data.DbType.String, IIf(String.IsNullOrEmpty(other_type_1), DBNull.Value, other_type_1), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(5) = New Aurora.Common.Data.Parameter("other_type_2", System.Data.DbType.String, IIf(String.IsNullOrEmpty(other_type_2), DBNull.Value, other_type_2), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(6) = New Aurora.Common.Data.Parameter("repair_code", System.Data.DbType.String, IIf(String.IsNullOrEmpty(repair_code), DBNull.Value, repair_code), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(7) = New Aurora.Common.Data.Parameter("off_fleet_flag", System.Data.DbType.Boolean, off_fleet_flag, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(8) = New Aurora.Common.Data.Parameter("estimated_cost", System.Data.DbType.Decimal, IIf(estimated_cost <> -9999, estimated_cost, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(9) = New Aurora.Common.Data.Parameter("actual_cost", System.Data.DbType.Decimal, IIf(actual_cost <> -9999, actual_cost, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(10) = New Aurora.Common.Data.Parameter("invoice_number", System.Data.DbType.String, IIf(String.IsNullOrEmpty(invoice_number), DBNull.Value, invoice_number), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(11) = New Aurora.Common.Data.Parameter("modified_by", System.Data.DbType.String, modified_by, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(12) = New Aurora.Common.Data.Parameter("reason_description", System.Data.DbType.String, IIf(String.IsNullOrEmpty(reason_description), DBNull.Value, reason_description), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(13) = New Aurora.Common.Data.Parameter("start_date_time", System.Data.DbType.DateTime, start_date_time, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(14) = New Aurora.Common.Data.Parameter("end_date_time", System.Data.DbType.DateTime, end_date_time, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(15) = New Aurora.Common.Data.Parameter("start_odometer", System.Data.DbType.Int32, start_odometer, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(16) = New Aurora.Common.Data.Parameter("end_odometer", System.Data.DbType.Int32, end_odometer, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(17) = New Aurora.Common.Data.Parameter("completed", System.Data.DbType.Boolean, completed, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(18) = New Aurora.Common.Data.Parameter("other_repairer", System.Data.DbType.String, IIf(String.IsNullOrEmpty(other_repairer), DBNull.Value, other_repairer), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(19) = New Aurora.Common.Data.Parameter("service_id", System.Data.DbType.String, IIf((service_id <> -1 And service_id <> 0), service_id, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(20) = New Aurora.Common.Data.Parameter("service_provider_id", System.Data.DbType.String, service_provider_id, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(21) = New Aurora.Common.Data.Parameter("po_number", System.Data.DbType.String, 20, Common.Data.Parameter.ParameterType.AddOutParameter)
                oParamArray(22) = New Aurora.Common.Data.Parameter("errorMsg", System.Data.DbType.String, 50, Common.Data.Parameter.ParameterType.AddOutParameter)

                Aurora.Common.Data.ExecuteOutputSP("Fleet_AddFleetRepair", oParamArray)
                ''get the output
                po_number = oParamArray(21).Value
                Logging.LogInformation("CreateRepairMaintenance: OK ", po_number)

                errorMsg = oParamArray(22).Value
                If (Not String.IsNullOrEmpty(errorMsg)) Then
                    Throw New Exception(errorMsg)
                End If

                ''commit to database
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()

            Catch ex As Exception

                errorMsg = "ERROR: " & ex.Message
                ''rollback and dispose from mem.
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                result = False
                ''log any errors
                Logging.LogError("CreateRepairMaintenance: ", errorMsg & ", " & ex.StackTrace)
            End Try

        End Using

        Return result

    End Function

    ''rev:mia 22Sept2014 - splitting of AIMS Call for DVASS.Today in history
    Public Function UpdateRepairMaintenance(fleet_asset_id As Integer, date_raised As DateTime, _
                                            locationcode As String, repair_type As String, other_type_1 As String, other_type_2 As String, _
                                            repair_code As String, off_fleet_flag As Boolean, estimated_cost As Integer, actual_cost As Integer, _
                                            invoice_number As String, modified_by As String, reason_description As String, start_date_time As DateTime, _
                                            end_date_time As DateTime, start_odometer As Integer, end_odometer As Integer, completed As Boolean, _
                                            other_repairer As String, service_id As Integer, service_provider_id As String, po_number As String, ByRef errorMsg As String _
                                            ) As Boolean

        Dim result As Boolean = True

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim oParamArray(22) As Aurora.Common.Data.Parameter
                oParamArray(0) = New Aurora.Common.Data.Parameter("fleet_asset_id", System.Data.DbType.Int32, fleet_asset_id, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(1) = New Aurora.Common.Data.Parameter("date_raised", System.Data.DbType.DateTime, IIf(date_raised <> DateTime.MinValue, date_raised, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(2) = New Aurora.Common.Data.Parameter("locationCode", System.Data.DbType.String, locationcode, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(3) = New Aurora.Common.Data.Parameter("repair_type", System.Data.DbType.String, IIf(String.IsNullOrEmpty(repair_type), DBNull.Value, repair_type), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(4) = New Aurora.Common.Data.Parameter("other_type_1", System.Data.DbType.String, IIf(String.IsNullOrEmpty(other_type_1), DBNull.Value, other_type_1), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(5) = New Aurora.Common.Data.Parameter("other_type_2", System.Data.DbType.String, IIf(String.IsNullOrEmpty(other_type_2), DBNull.Value, other_type_2), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(6) = New Aurora.Common.Data.Parameter("repair_code", System.Data.DbType.String, IIf(String.IsNullOrEmpty(repair_code), DBNull.Value, repair_code), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(7) = New Aurora.Common.Data.Parameter("off_fleet_flag", System.Data.DbType.Boolean, off_fleet_flag, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(8) = New Aurora.Common.Data.Parameter("estimated_cost", System.Data.DbType.Decimal, IIf(estimated_cost <> -9999, estimated_cost, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(9) = New Aurora.Common.Data.Parameter("actual_cost", System.Data.DbType.Decimal, IIf(actual_cost <> -9999, actual_cost, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(10) = New Aurora.Common.Data.Parameter("invoice_number", System.Data.DbType.String, IIf(String.IsNullOrEmpty(invoice_number), DBNull.Value, invoice_number), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(11) = New Aurora.Common.Data.Parameter("modified_by", System.Data.DbType.String, modified_by, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(12) = New Aurora.Common.Data.Parameter("reason_description", System.Data.DbType.String, IIf(String.IsNullOrEmpty(reason_description), DBNull.Value, reason_description), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(13) = New Aurora.Common.Data.Parameter("start_date_time", System.Data.DbType.DateTime, start_date_time, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(14) = New Aurora.Common.Data.Parameter("end_date_time", System.Data.DbType.DateTime, end_date_time, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(15) = New Aurora.Common.Data.Parameter("start_odometer", System.Data.DbType.Int32, start_odometer, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(16) = New Aurora.Common.Data.Parameter("end_odometer", System.Data.DbType.Int32, end_odometer, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(17) = New Aurora.Common.Data.Parameter("completed", System.Data.DbType.Boolean, completed, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(18) = New Aurora.Common.Data.Parameter("other_repairer", System.Data.DbType.String, IIf(String.IsNullOrEmpty(other_repairer), DBNull.Value, other_repairer), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(19) = New Aurora.Common.Data.Parameter("service_id", System.Data.DbType.String, IIf((service_id <> -1 And service_id <> 0), service_id, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(20) = New Aurora.Common.Data.Parameter("service_provider_id", System.Data.DbType.String, service_provider_id, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(21) = New Aurora.Common.Data.Parameter("po_number", System.Data.DbType.String, po_number, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(22) = New Aurora.Common.Data.Parameter("errorMsg", System.Data.DbType.String, 50, Common.Data.Parameter.ParameterType.AddOutParameter)


                Aurora.Common.Data.ExecuteOutputSP("Fleet_UpdateFleetRepairsAIMS", oParamArray)


                errorMsg = oParamArray(22).Value
                If (Not String.IsNullOrEmpty(errorMsg)) Then
                    Throw New Exception(errorMsg)
                End If

                Logging.LogInformation("UpdateRepairMaintenance: OK ", po_number)

                ''commit to database
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                errorMsg = "ERROR: " & ex.Message
                ''rollback and dispose from mem.
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                result = False
                ''log any errors
                Logging.LogError("UpdateRepairMaintenance: ", errorMsg & ", " & ex.StackTrace)
            End Try

        End Using

        Return result
    End Function

    Public Function DeleteRepairMaintenance(fleet_asset_id As Integer, iRepairId As Integer, sUsrCode As String, sAddModPrgmName As String, ByRef errorMsg As String) As Boolean

        Dim result As Boolean = True
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim oParamArray(3) As Aurora.Common.Data.Parameter
                oParamArray(0) = New Aurora.Common.Data.Parameter("fleet_asset_id", System.Data.DbType.Int32, fleet_asset_id, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(1) = New Aurora.Common.Data.Parameter("iRepairId", System.Data.DbType.Int32, iRepairId, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(2) = New Aurora.Common.Data.Parameter("sUsrCode", System.Data.DbType.String, sUsrCode, Common.Data.Parameter.ParameterType.AddInParameter)
                oParamArray(3) = New Aurora.Common.Data.Parameter("sAddModPrgmName", System.Data.DbType.String, sAddModPrgmName, Common.Data.Parameter.ParameterType.AddInParameter)
                Aurora.Common.Data.ExecuteOutputSP("Fleet_DeleteFleetRepairs", oParamArray)
                Logging.LogInformation("DeleteRepairMaintenance: OK ", "SUCCESSS")

                ''commit to database
                oTransaction.CommitTransaction()
                ''oTransaction.RollbackTransaction()
            Catch ex As Exception
                errorMsg = "ERROR: " & ex.Message
                ''rollback and dispose from mem.
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                result = False
                ''log any errors
                Logging.LogError("DeleteRepairMaintenance: ", errorMsg & ", " & ex.StackTrace)
            End Try
        End Using

        Return result
    End Function

#End Region
End Module

