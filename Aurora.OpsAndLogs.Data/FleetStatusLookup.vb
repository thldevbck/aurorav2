Partial Class FleetStatusDataSet

    Partial Class FleetStatusLookupDataTable
        Public Function FindByCode(ByVal code As String) As FleetStatusLookupRow
            For Each fleetStatusLookupRow As FleetStatusLookupRow In Me
                If fleetStatusLookupRow.Code = code Then
                    Return fleetStatusLookupRow
                End If
            Next
            Return Nothing
        End Function

        Private _countryLookups As FleetStatusLookupRow()
        Public ReadOnly Property CountryLookups() As FleetStatusLookupRow()
            Get
                If _countryLookups Is Nothing Then
                    Dim resultCodes As New List(Of String)
                    Dim result As New List(Of FleetStatusLookupRow)
                    For Each fleetStatusLookupRow As FleetStatusLookupRow In Me
                        If resultCodes.Contains(fleetStatusLookupRow.CtyCode) Then Continue For
                        resultCodes.Add(fleetStatusLookupRow.CtyCode)
                        result.Add(fleetStatusLookupRow)
                    Next
                    _countryLookups = result.ToArray()
                End If
                Return _countryLookups
            End Get
        End Property

        Private _lookupsForCountry As New Dictionary(Of String, FleetStatusLookupRow())
        Public ReadOnly Property LookupsForCountry(ByVal ctyCode As String) As FleetStatusLookupRow()
            Get
                If Not _lookupsForCountry.ContainsKey(ctyCode) Then
                    Dim result As New List(Of FleetStatusLookupRow)
                    For Each fleetStatusLookupRow As FleetStatusLookupRow In Me
                        If fleetStatusLookupRow.CtyCode = ctyCode Then
                            result.Add(fleetStatusLookupRow)
                        End If
                    Next
                    _lookupsForCountry.Add(ctyCode, result.ToArray())
                End If
                Return _lookupsForCountry(ctyCode)
            End Get
        End Property

        Public ReadOnly Property DefaultLookupForCountry(ByVal ctyCode As String) As FleetStatusLookupRow
            Get
                Dim result As FleetStatusLookupRow = Nothing
                For Each fleetStatusLookupRow As FleetStatusLookupRow In LookupsForCountry(ctyCode)
                    If result Is Nothing _
                     OrElse (fleetStatusLookupRow.FwnTrvYearStart >= Date.Now AndAlso fleetStatusLookupRow.FwnTrvYearStart < result.FwnTrvYearStart) Then
                        result = fleetStatusLookupRow
                    End If
                Next
                Return result
            End Get
        End Property

    End Class


    Partial Class FleetStatusLookupRow

        Public ReadOnly Property CountryDescription() As String
            Get
                Return Me.CtyCode & "-" & Me.CtyName
            End Get
        End Property

        Public ReadOnly Property Description() As String
            Get
                Return Me.FwnTrvYearStart.Year.ToString() & " - " & Me.BrdName
            End Get
        End Property

        Public ReadOnly Property Code() As String
            Get
                Return Me.CtyCode & "-" & Me.BrdCode & "-" & Me.FwnTrvYearStart.Year.ToString()
            End Get
        End Property

    End Class

End Class
