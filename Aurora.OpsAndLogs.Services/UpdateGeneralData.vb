
Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class UpdateGeneralData

    Public Shared Function UpdateOdoMeter(ByVal unit As String, ByVal odo As String, _
        ByVal userCode As String, ByVal programCode As String) As String

        Dim xmlDoc As New XmlDocument

        Dim xmlString As String = "<Data><Type>ODOMETER</Type><UnitNum>" & unit & "</UnitNum><ActKms>" & odo & "</ActKms>" & _
            "<BooRefCko></BooRefCko><BooRefCki></BooRefCki></Data>"

        xmlDoc.LoadXml(xmlString)

        Return DataRepository.UpdateGeneralData(xmlDoc, userCode, programCode)

    End Function

    Public Shared Function UndoCheckOut(ByVal bookingRef As String, _
        ByVal userCode As String, ByVal programCode As String) As String

        Dim xmlDoc As New XmlDocument

        Dim xmlString As String = "<Data><Type>UNDOCKO</Type><UnitNum></UnitNum><ActKms></ActKms>" & _
            "<BooRefCko>" & bookingRef & "</BooRefCko><BooRefCki></BooRefCki></Data>"

        xmlDoc.LoadXml(xmlString)

        Return DataRepository.UpdateGeneralData(xmlDoc, userCode, programCode)

    End Function

    Public Shared Function UndoCheckIn(ByVal bookingRef As String, _
        ByVal userCode As String) As String

        Return UpdateGeneralDataForUndoCheckIn(bookingRef, userCode)

    End Function


End Class


