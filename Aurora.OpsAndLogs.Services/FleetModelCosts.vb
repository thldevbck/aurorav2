Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class FleetModelCosts

    Public Shared Function GetFleetModelCosts(ByVal CountryCode As String, ByVal UserCode As String) As XmlDocument
        Return DataRepository.GetFleetModelCosts(CountryCode, UserCode)
    End Function

    Public Shared Function UpdateFleetModelCosts(ByVal ScreenData As XmlDocument) As XmlDocument
        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.UpdateFleetModelCosts(ScreenData)

                If Not (xmlReturn.DocumentElement.InnerText.Contains("GEN045") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN046") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN047")) Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                If xmlReturn.DocumentElement.InnerText.Contains("GEN045") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN047") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN047") & "</Message></Root>")
                End If

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using


    End Function

End Class
