
Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class UndoCheckOut

    Public Shared Function UpdateGeneralData(ByVal bookingRef As String, _
        ByVal userCode As String, ByVal programCode As String) As String

        Dim xmlDoc As New XmlDocument

        Dim xmlString As String = "<Data><Type>UNDOCKO</Type><UnitNum></UnitNum><ActKms></ActKms>" & _
            "<BooRefCko>" & bookingRef & "</BooRefCko><BooRefCki></BooRefCki></Data>"

        xmlDoc.LoadXml(xmlString)

        Return DataRepository.UpdateGeneralData(xmlDoc, userCode, programCode)

    End Function

End Class

