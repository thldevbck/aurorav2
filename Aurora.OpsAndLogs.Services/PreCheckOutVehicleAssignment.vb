Imports System.Data
Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class PreCheckOutVehicleAssignment

    Public Shared Function GetRentalsPendingVehicleAllocation(ByVal LocationCode As String, ByVal CountryCode As String, ByVal CheckOutDate As DateTime, ByVal BrandCode As String, ByVal CompanyCode As String) As DataSet
        'OPL_GetRentalsPendingVehicleAllocation
        Dim dstResult As New DataSet
        dstResult = Aurora.Common.Data.ExecuteDataSetSP("OPL_GetRentalsPendingVehicleAllocation", dstResult, LocationCode, CountryCode, CheckOutDate, BrandCode, CompanyCode)
        Return dstResult
    End Function

    Public Shared Function SaveVehicleAllocation(ByVal AllocationData As DataTable, ByVal UserCode As String, ByRef ErrorMessage As String) As Boolean
        Dim bSuccess As Boolean = False

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                For Each oRow As DataRow In AllocationData.Rows

                    Dim xmlReturn As XmlDocument
                    Dim sBookingNum, sRentalNum, sVehicleSpecification, sNoteText, sNoteAudienceType As String
                    Dim nNotePriority As Integer

                    sBookingNum = oRow("BooNum").ToString()
                    sRentalNum = oRow("RntNum").ToString()
                    sVehicleSpecification = oRow("VehicleSpecification").ToString()
                    sNoteText = oRow("NoteText").ToString()
                    sNoteAudienceType = oRow("NoteAudienceType").ToString()
                    nNotePriority = CInt(oRow("NotePriority"))

                    ' first check for the booking/rental - ("GEN_GetPopUpData 'CHKBOORENT', '" & BooNum & "', '" & RntNum & "'")
                    xmlReturn = Aurora.Common.Data.GetPopUpData(Aurora.Popups.Data.PopupType.CHKBOORENT, sBookingNum, sRentalNum, "", "", "", UserCode)
                    If xmlReturn.InnerText.Contains("ERROR") Then
                        Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                    End If

                    ' is a valid thingie
                    With xmlReturn.DocumentElement.SelectSingleNode("DVASS")
                        ' modify the rental
                        Dim sReturn As String = ""
                        Dim dRevenue As Double = 0.0
                        sReturn = Aurora.Reservations.Services.AuroraDvass.ModifyRental(CInt(.SelectSingleNode("Cty").InnerText), _
                                                                                        .SelectSingleNode("RntId").InnerText, _
                                                                                        .SelectSingleNode("PrdSeq").InnerText, _
                                                                                        CDate(.SelectSingleNode("CkoDate").InnerText), _
                                                                                        CLng(.SelectSingleNode("CkoDayPart").InnerText), _
                                                                                        CDate(.SelectSingleNode("CkiDate").InnerText), _
                                                                                        CLng(.SelectSingleNode("CkiDayPart").InnerText), _
                                                                                        .SelectSingleNode("CkoLoc").InnerText, _
                                                                                        .SelectSingleNode("CkiLoc").InnerText, _
                                                                                        dRevenue, _
                                                                                        CDbl(.SelectSingleNode("PRIORITY").InnerText), _
                                                                                        sVehicleSpecification, _
                                                                                        CInt(.SelectSingleNode("FORCEFLAG").InnerText))

                        If Not sReturn.ToUpper().Contains("SUCCESS") Then
                            ' some dvass problem
                            Throw New Exception(sReturn)
                        End If

                        ' save the stuff to rental table first
                        xmlReturn = DataRepository.SaveVehicleAssign(sBookingNum, sRentalNum, sVehicleSpecification, UserCode)

                        If xmlReturn.DocumentElement.InnerText.ToUpper().Contains("ERROR") Then
                            ' something wrong
                            Throw New Exception(xmlReturn.DocumentElement.InnerText)
                        End If

                        ' save the notes data
                        ' decision to skip or not
                        If Not (sNoteText.Trim().Equals(String.Empty)) Then
                            ' save note
                            xmlReturn = DataRepository.SaveVehicleAssignNote(sBookingNum, sRentalNum, "", sNoteText, nNotePriority, sNoteAudienceType, True, 1, UserCode)
                            If xmlReturn.DocumentElement.InnerText.ToUpper().Contains("ERROR") Then
                                ' something wrong
                                Throw New Exception(xmlReturn.DocumentElement.InnerText)
                            End If
                        End If

                    End With

                    If Not (xmlReturn.DocumentElement.InnerText.Contains("GEN045") Or _
                            xmlReturn.DocumentElement.InnerText.Contains("GEN046") Or _
                            xmlReturn.DocumentElement.InnerText.Contains("GEN047")) Then
                        Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                    End If

                Next

                oTransaction.CommitTransaction()

                bSuccess = True
                ErrorMessage = ""

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                ErrorMessage = ex.Message & vbNewLine & ex.StackTrace

            End Try

            Return bSuccess
        End Using
    End Function

    Public Shared Function ValidateOrGetUnitNumber(ByVal SearchString As String, ByVal CountryCode As String) As XmlDocument
        'CREATE PROC OPL_ValidateOrGetUnitNumber_Temp
        '	@sSearchString VARCHAR(50),
        '	@sCountryCode VARCHAR(2)

        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("OPL_ValidateOrGetUnitNumber", SearchString, CountryCode)

    End Function

End Class
