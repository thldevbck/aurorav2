Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class RelocationRules

    Public Shared Function GetReloactionRules(ByVal CountryCode As String, ByVal UserCode As String, ByVal locFrom As String, ByVal locTo As String, ByVal fleetModel As Nullable(Of Integer)) As XmlDocument
        Return DataRepository.GetReloactionRules(CountryCode, UserCode, locFrom, locTo, fleetModel)
    End Function

    Public Shared Function ValidateRelocationRule(ByVal ScreenDataXML As XmlDocument) As String



        Dim iOutCount As Integer, iInCount As Integer, RcrTypeId As String
        Dim dFdate1 As Date, dTdate1 As Date, dFdate2 As Date, dTdate2 As Date
        Dim sFLoc1 As String, sTLoc1 As String, sFLoc2 As String, sTLoc2 As String
        Dim sAvl1 As String, sAvl2 As String

        For iOutCount = 0 To ScreenDataXML.DocumentElement.ChildNodes.Count - 1

            dFdate1 = ScreenDataXML.DocumentElement.ChildNodes(iOutCount).ChildNodes(3).InnerText
            dTdate1 = ScreenDataXML.DocumentElement.ChildNodes(iOutCount).ChildNodes(4).InnerText
            sFLoc1 = ScreenDataXML.DocumentElement.ChildNodes(iOutCount).ChildNodes(1).InnerText
            sTLoc1 = ScreenDataXML.DocumentElement.ChildNodes(iOutCount).ChildNodes(2).InnerText
            RcrTypeId = ScreenDataXML.DocumentElement.ChildNodes(iOutCount).ChildNodes(7).InnerText
            sAvl1 = ScreenDataXML.DocumentElement.ChildNodes(iOutCount).ChildNodes(8).InnerText

            For iInCount = iOutCount + 1 To ScreenDataXML.DocumentElement.ChildNodes.Count - 1

                dFdate2 = ScreenDataXML.DocumentElement.ChildNodes(iInCount).ChildNodes(3).InnerText
                dTdate2 = ScreenDataXML.DocumentElement.ChildNodes(iInCount).ChildNodes(4).InnerText
                sFLoc2 = ScreenDataXML.DocumentElement.ChildNodes(iInCount).ChildNodes(1).InnerText
                sTLoc2 = ScreenDataXML.DocumentElement.ChildNodes(iInCount).ChildNodes(2).InnerText
                sAvl2 = ScreenDataXML.DocumentElement.ChildNodes(iInCount).ChildNodes(8).InnerText

                If sFLoc1 = sFLoc2 And sTLoc1 = sTLoc2 And sAvl1 = sAvl2 _
                                   And RcrTypeId = ScreenDataXML.DocumentElement.ChildNodes(iInCount).ChildNodes(7).InnerText Then

                    If dFdate2 <= dFdate1 And dTdate2 < dTdate1 And dTdate2 > dFdate1 Or _
                            dFdate2 >= dFdate1 And dTdate2 <= dTdate1 Or _
                            dFdate2 > dFdate1 And dTdate2 >= dTdate1 And dFdate2 < dTdate1 Or _
                            dFdate2 <= dFdate1 And dTdate2 > dTdate1 Then

                        Return ("GEN070") ' overlap!!!
                        Exit Function
                    End If

                End If

            Next

        Next

        Return ("")
        


    End Function

    ''' <summary>
    ''' This function updates the Relocation Rules
    ''' </summary>
    ''' <param name="ScreenData">Changed or Added rows in XML</param>
    ''' <param name="SaveFlag">0 - Delete, 1 - Save/Update</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateRelocationRule(ByVal ScreenData As XmlDocument, ByVal SaveFlag As Boolean) As XmlDocument

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.UpdateRelocationRule(ScreenData, SaveFlag)

                If Not (xmlReturn.DocumentElement.InnerText.Contains("GEN045") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN046") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN047")) Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                If xmlReturn.DocumentElement.InnerText.Contains("GEN045") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN047") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN047") & "</Message></Root>")
                End If

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using

    End Function
End Class
