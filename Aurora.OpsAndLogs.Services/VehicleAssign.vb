Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class VehicleAssign

    Public Shared Function GetVehicleAssetsGrid(ByVal BookingId As String, ByVal UserCode As String) As XmlDocument
        Return DataRepository.GetVehicleAssetsGrid(BookingId, UserCode)
    End Function

    Public Shared Function SaveVehicleAssignData(ByVal BookingNo As String, ByVal RentalNo As String, ByVal VehicleSpec As String, _
                                          ByVal NoteId As String, ByVal NoteDesc As String, ByVal NotePriority As Integer, _
                                          ByVal NoteAudienceType As String, ByVal NoteActiveFlag As Boolean, _
                                          ByVal NoteIntegrityNo As Integer, ByVal UserCode As String) As XmlDocument

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                '  xmlReturn = DataRepository.UpdateAlternativeProducts(ScreenData)

                ' first check for the booking/rental - ("GEN_GetPopUpData 'CHKBOORENT', '" & BooNum & "', '" & RntNum & "'")
                xmlReturn = Aurora.Common.Data.GetPopUpData(Aurora.Popups.Data.PopupType.CHKBOORENT, BookingNo, RentalNo, "", "", "", UserCode)
                If xmlReturn.InnerText.Contains("ERROR") Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If

                ' is a valid thingie
                With xmlReturn.DocumentElement.SelectSingleNode("DVASS")
                    ' modify the rental
                    Dim sReturn As String = ""
                    Dim dRevenue As Double = 0.0
                    sReturn = Aurora.Reservations.Services.AuroraDvass.ModifyRental(CInt(.SelectSingleNode("Cty").InnerText), _
                                                                                    .SelectSingleNode("RntId").InnerText, _
                                                                                    .SelectSingleNode("PrdSeq").InnerText, _
                                                                                    CDate(.SelectSingleNode("CkoDate").InnerText), _
                                                                                    CLng(.SelectSingleNode("CkoDayPart").InnerText), _
                                                                                    CDate(.SelectSingleNode("CkiDate").InnerText), _
                                                                                    CLng(.SelectSingleNode("CkiDayPart").InnerText), _
                                                                                    .SelectSingleNode("CkoLoc").InnerText, _
                                                                                    .SelectSingleNode("CkiLoc").InnerText, _
                                                                                    dRevenue, _
                                                                                    CDbl(.SelectSingleNode("PRIORITY").InnerText), _
                                                                                    VehicleSpec, _
                                                                                    CInt(.SelectSingleNode("FORCEFLAG").InnerText))

                    If Not sReturn.ToUpper().Contains("SUCCESS") Then
                        ' some dvass problem
                        Throw New Exception(sReturn)
                    End If

                    ' save the stuff to rental table first
                    xmlReturn = DataRepository.SaveVehicleAssign(BookingNo, RentalNo, VehicleSpec, UserCode)

                    If xmlReturn.DocumentElement.InnerText.ToUpper().Contains("ERROR") Then
                        ' something wrong
                        Throw New Exception(xmlReturn.DocumentElement.InnerText)
                    End If

                    ' save the notes data
                    ' decision to skip or not
                    If Not (NoteDesc.Trim().Equals(String.Empty) And NotePriority = -1 And NoteAudienceType.Trim.Equals(String.Empty)) Then
                        ' save note
                        xmlReturn = DataRepository.SaveVehicleAssignNote(BookingNo, RentalNo, NoteId, NoteDesc, NotePriority, NoteAudienceType, NoteActiveFlag, NoteIntegrityNo, UserCode)
                        If xmlReturn.DocumentElement.InnerText.ToUpper().Contains("ERROR") Then
                            ' something wrong
                            Throw New Exception(xmlReturn.DocumentElement.InnerText)
                        End If
                    End If

                End With

                If Not (xmlReturn.DocumentElement.InnerText.Contains("GEN045") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN046") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN047")) Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                If xmlReturn.DocumentElement.InnerText.Contains("GEN045") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN047") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN047") & "</Message></Root>")
                End If

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using

    End Function

    Public Shared Function UndoVehicleAssign(ByVal BookingNo As String, ByVal RentalNo As String, ByVal UserCode As String) As XmlDocument
        ' first undo the vehicle
        ' then save a note on the same

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.UndoVehicleAssign(BookingNo, RentalNo, UserCode)

                If Not xmlReturn.DocumentElement.InnerText.ToUpper().Contains("SUCCESS") Then
                    ' some problem
                    Throw New Exception(xmlReturn.DocumentElement.InnerText)
                End If

                ' no problem, so try adding a note
                xmlReturn = DataRepository.SaveVehicleAssignNote(BookingNo, RentalNo, "", "Vehicle Assign Undone by - " & UserCode & " on " & Now.ToShortDateString() & " at " & Now.ToShortTimeString(), 1, "Internal", True, 1, UserCode)

                If Not (xmlReturn.DocumentElement.InnerText.Contains("GEN045") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN046") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN047")) Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                If xmlReturn.DocumentElement.InnerText.Contains("GEN045") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN047") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN047") & "</Message></Root>")
                End If

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using
    End Function




End Class
