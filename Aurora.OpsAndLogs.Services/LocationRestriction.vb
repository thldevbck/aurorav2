﻿Imports Aurora.OpsAndLogs.Data
Imports Aurora.OpsAndLogs.Data.DataRepository
Imports Aurora.Common
Imports System.Xml
Imports System.Data


Public Class LocationRestriction
    Public Shared Function GetCountriesForLocationRestriction(userCode As String) As DataSet
        Return GetCountriesLocationRestrictions(userCode)
    End Function

    Public Shared Function GetLocRestrictions(ctycode As String) As DataSet
        Return AddEmptyRow(GetLocationRestrictions(ctycode))
    End Function

    Public Shared Function UpdateLocRestriction(ctycode As String, Xmldata As String) As String
        Return UpdateLocationRestriction(ctycode, Xmldata)
    End Function

    Private Shared Function AddEmptyRow(ds As DataSet)
        Dim newrow As DataRow
        newrow = ds.Tables(0).NewRow
        newrow("LrId") = DBNull.Value
        newrow("LrFlmId") = DBNull.Value
        newrow("LrLocCode") = DBNull.Value
        newrow("LrStartDate") = DBNull.Value
        newrow("LrEndDate") = DBNull.Value
        ds.Tables(0).Rows.Add(newrow)
        Return ds

    End Function
End Class
