﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Aurora.OpsAndLogs.Services")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Tourism Holdings Ltd")> 
<Assembly: AssemblyProduct("Aurora.OpsAndLogs.Services")> 
<Assembly: AssemblyCopyright("Copyright © Tourism Holdings Ltd 2008")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("38a1cb95-a2c2-4a5d-a667-57baf6f7c98c")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
