
Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common
Imports System.Xml

Public Class SubstitutionRule

    Public Shared Function GetSubstitutionRuleProductList(ByVal country As String, ByVal usercode As String, _
        ByVal productId As String, ByVal isNew As String) As XmlDocument

        Return DataRepository.GetSubstitutionRuleProductList(country, usercode, productId, isNew, _
               Nothing, Nothing, Nothing)

    End Function

    Public Shared Function GetSubstitutionRuleProductList(ByVal country As String, ByVal usercode As String, _
        ByVal productId As String, ByVal isNew As String, _
        ByVal dateFrom As Nullable(Of DateTime), ByVal dateTo As Nullable(Of DateTime), ByVal locationFrom As String) As XmlDocument

        Return DataRepository.GetSubstitutionRuleProductList(country, usercode, productId, isNew, _
               dateFrom, dateTo, locationFrom)

    End Function

    Public Shared Function UpdateSubstitutionRuleProductList(ByVal xmlDoc As XmlDocument) As String

        Return DataRepository.UpdateSubstitutionRuleProductList(xmlDoc)

    End Function

    Public Shared Function ListFleetAlternatives(ByVal search As String, ByVal loggedInUserCode As String) As DataSet

        Return DataRepository.ListFleetAlternatives(search, loggedInUserCode)

    End Function

End Class
