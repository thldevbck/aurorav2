Imports Aurora.Common
Imports Aurora.OpsAndLogs.Data
Imports System.Xml
Imports COMDVASSMgrLib

Public Class Fleet

    Public Shared Function GetRepairsList(ByVal SearchFieldValue As String, ByVal FleetAssetId As String, ByVal UserCode As String) As DataSet
        Return DataRepository.GetRepairsList(SearchFieldValue, FleetAssetId, UserCode)
    End Function

    Public Shared Function GetFleetRepair(ByVal RepairId As String, ByVal FleetAssetId As String, ByVal UnitNo As String) As XmlDocument
        Return DataRepository.GetFleetRepair(RepairId, FleetAssetId, UnitNo)
    End Function

    Public Shared Function GetOpsAndLogsReasons() As DataTable
        'GEN_getComboData 'OPLOGREASON'
        Dim dstReasons As DataSet
        dstReasons = Aurora.Common.Data.GetComboData("OPLOGREASON", "", "", "")

        If dstReasons.Tables.Count <= 0 Then
            Return New DataTable
        Else
            Return dstReasons.Tables(0)
        End If

    End Function

    Public Shared Function GetServicePoint(ByVal AssetId As String) As DataTable
        'GEN_getComboData 'OPLOGSERVICEPOINT','29211'
        Dim dstServicePoints As DataSet
        dstServicePoints = Aurora.Common.Data.GetComboData("OPLOGSERVICEPOINT", AssetId, "", "")

        If dstServicePoints.Tables.Count <= 0 Then
            Return New DataTable
        Else
            Return dstServicePoints.Tables(0)
        End If


    End Function

    ''rev:mia 19Aug2014 - fixing AIMS and DVASS syncHronization by forcing transactions on these
    ' Purpose-To remove Repair and Maintenance record from AIMS databse using AIMS API.
    'An XML with the success-Error message

    '''' process-logic :
    '''' validations:
    '''' *pon should not be null
    '''' if pon=null
    ''''     return error message in xml format.
    '''' begin transaction
    '''' instantiate the aims interface object( ai.cmaster )
    '''' execute f3_rmdelete(pon)
    '''' res = aims.f3_rmdelete(pon)
    '''' destroy oaims object
    '''' if res lt 0   i.e. any errors
    ''''     rollback the transaction
    '''' else
    ''''     commit the transaction
    '''' get the message from aurora using res as message code;
    '''' for ret=0 to get success message for delete operation.
    '''' return the message.
    Public Shared Function DeleteRepairMaintenanceRecord(ByVal PurchaseOrderNo As String, _
                                                           ByVal ToDate As Date, _
                                                           ByRef DvassMessage As String, _
                                                           ByVal UnitNo As String, _
                                                           ByVal Branch As String, _
                                                           ByVal FromDate As Date, _
                                                           ByVal CkoDayPart As Integer, _
                                                           ByVal CkiDayPart As Integer, _
                                                           ByVal forceFlag As Integer, _
                                                           ByVal Activity As String, _
                                                           ByVal invokeDvass As Boolean _
                                                           ) As XmlDocument

        Dim oReturnDoc As New XmlDocument
        Dim nErrorNo As Integer
        Dim xmlReturnMessage As String = ""

        

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                If PurchaseOrderNo.Trim().Equals(String.Empty) Then
                    Throw (New Exception("App/Purchase Order Number should not be null"))
                End If

                'AIMS object
                Dim oAIMSObject As New AI.CMaster
                nErrorNo = oAIMSObject.F3_RMDelete(PurchaseOrderNo)

                If nErrorNo < 0 Then
                    Throw (New Exception("App/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(CStr(nErrorNo))))
                End If

                If (IsValidActivity(Activity)) Then

                    If ((Not String.IsNullOrEmpty(Activity) And invokeDvass = True) Or (Not invokeDvass And Activity = "DELETE")) Then
                        DvassMessage = RepairMaintenanceRequest(UnitNo, Branch, FromDate, CkoDayPart, ToDate, CkiDayPart, forceFlag, Activity)

                        If (DvassMessage <> "SUCCESS") Then
                            Throw (New Exception("DVASS: " & DvassMessage))
                        End If

                    End If
                End If
                

                xmlReturnMessage = Aurora.Common.Data.GetErrorMessage("GEN047")

                If (Not String.IsNullOrEmpty(DvassMessage) And DvassMessage = "SUCCESS") Then
                    xmlReturnMessage = xmlReturnMessage & " / DVASS: Success"
                End If

                oTransaction.CommitTransaction()
                oReturnDoc.LoadXml("<Root><Message>" & xmlReturnMessage & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select
            End Try

            'Dim sLog As New Text.StringBuilder
            'sLog = sLog.AppendFormat("Parameters{0}UnitNo: {0}{1}", vbCrLf, UnitNo, vbCrLf)
            'sLog = sLog.AppendFormat("FromDate: {0}{1}", FromDate, vbCrLf)
            'sLog = sLog.AppendFormat("ToDate: {0}{1}", ToDate, vbCrLf)
            'sLog = sLog.AppendFormat("DvassMessage: {0}{1}", DvassMessage, vbCrLf)
            'sLog = sLog.AppendFormat("Branch: {0}{1}", Branch, vbCrLf)
            'sLog = sLog.AppendFormat("PurchaseOrderNo: {0}{1}", PurchaseOrderNo, vbCrLf)
            'sLog = sLog.AppendFormat("ckoDayPart: {0}{1}", CkoDayPart, vbCrLf)
            'sLog = sLog.AppendFormat("ckiDayPart: {0}{1}", CkiDayPart, vbCrLf)
            'sLog = sLog.AppendFormat("forceFlag: {0}{1}", forceFlag, vbCrLf)
            'sLog = sLog.AppendFormat("Activity: {0}{1}", Activity, vbCrLf)
            'sLog = sLog.AppendFormat("InvokeDvass: {0}{1}", invokeDvass, vbCrLf)
            'sLog = sLog.AppendFormat("RETURN: {0}{1}", oReturnDoc.OuterXml, vbCrLf)
            ''Logging.LogInformation("Fleet.DeleteRepairMaintenanceRecord", sLog.ToString())
            Return oReturnDoc
        End Using

    End Function

    ''rev:mia 19Aug2014 - fixing AIMS and DVASS syncHronization by forcing transactions on these
    ''' <summary>
    ''' Purpose : Server-side posting of Repair and Maintenance Data to Aims.
    ''' Process/Logic :
    ''' Begin transaction
    ''' Parse the XML record xRMComplete
    ''' If Not valid XML then
    '''     return error message in XML format.
    ''' Else
    ''' Extract PON, Odometer, dtENd
    ''' Destroy DOM object
    ''' Instantiate the AIMS Interface Object( AI.CMaster )
    ''' Execute F2_RMComplete()
    ''' res = AIMS.F2_RMComplete(PON, Odometer, dtENd)
    ''' Destroy oAIMS object
    ''' If res LT 0   i.e. any errors
    '''     rollback the transaction
    ''' Else
    '''     commit the transaction
    ''' Get the message from AURORA using the value of res as message code;
    ''' For ret=0 to get success message for delete operation.
    ''' Return the message in Xml Format.
    ''' </summary>
    ''' <param name="ProviderId">Provider Id</param>
    ''' <param name="PurchaseOrderNo">Purchase Order Number</param>
    ''' <param name="OdometerEnd">Ending Odometer Reading</param>
    ''' <param name="EndDate">End Date</param>
    ''' <returns>XmlDocument with the Error/Success Status</returns>
    ''' <remarks></remarks>
    ''' 
    Public Shared Function CompleteRepairMaintenanceRecord(ByVal ProviderId As String, _
                                                           ByVal PurchaseOrderNo As String, _
                                                           ByVal OdometerEnd As Integer, _
                                                           ByVal ToDate As Date, _
                                                           ByRef DvassMessage As String, _
                                                           ByVal UnitNo As String, _
                                                           ByVal Branch As String, _
                                                           ByVal FromDate As Date, _
                                                           ByVal CkoDayPart As Integer, _
                                                           ByVal CkiDayPart As Integer, _
                                                           ByVal forceFlag As Integer, _
                                                           ByVal Activity As String, _
                                                           ByVal invokeDvass As Boolean _
                                                           ) As XmlDocument

        Dim oReturnDoc As New XmlDocument
        Dim nErrorNo As Integer
        Dim xmlReturnMessage As String = ""

        



        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                If Not ValidateProvider(ProviderId, 0) Then
                    Throw (New Exception("App/'" & ProviderId & "' is not a valid Provider"))
                End If

                Dim oAIMSObject As New AI.CMaster

                nErrorNo = oAIMSObject.F2_RMComplete(PurchaseOrderNo, OdometerEnd, ToDate)

                If nErrorNo < 0 Then
                    Throw (New Exception("App/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(CStr(nErrorNo))))
                End If

                ''rev:mia 28Aug2014 - not needed anymore
                'If (IsValidActivity(Activity)) Then
                '    If ((Not String.IsNullOrEmpty(Activity) And invokeDvass = True) Or (Not invokeDvass And Activity = "DELETE")) Then
                '        DvassMessage = RepairMaintenanceRequest(UnitNo, Branch, FromDate, CkoDayPart, ToDate, CkiDayPart, forceFlag, Activity)

                '        If (DvassMessage <> "SUCCESS") Then
                '            Throw (New Exception("DVASS: " & DvassMessage))
                '        End If

                '    End If
                'End If

                xmlReturnMessage = Aurora.Common.Data.GetErrorMessage("GEN046")

                If (Not String.IsNullOrEmpty(DvassMessage) And DvassMessage = "SUCCESS") Then
                    xmlReturnMessage = xmlReturnMessage & " / DVASS: Success"
                End If

                oTransaction.CommitTransaction()
                oReturnDoc.LoadXml("<Root><Message>" & xmlReturnMessage & "</Message></Root>")

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select
            End Try

            'Dim sLog As New Text.StringBuilder
            'sLog = sLog.AppendFormat("Parameters{0}ProviderId: {0}{1}", vbCrLf, ProviderId, vbCrLf)
            'sLog = sLog.AppendFormat("UnitNo: {0}{1}", UnitNo, vbCrLf)
            'sLog = sLog.AppendFormat("FromDate: {0}{1}", FromDate, vbCrLf)
            'sLog = sLog.AppendFormat("ToDate: {0}{1}", ToDate, vbCrLf)
            'sLog = sLog.AppendFormat("DvassMessage: {0}{1}", DvassMessage, vbCrLf)
            'sLog = sLog.AppendFormat("Branch: {0}{1}", Branch, vbCrLf)
            'sLog = sLog.AppendFormat("PurchaseOrderNo: {0}{1}", PurchaseOrderNo, vbCrLf)
            'sLog = sLog.AppendFormat("ckoDayPart: {0}{1}", CkoDayPart, vbCrLf)
            'sLog = sLog.AppendFormat("ckiDayPart: {0}{1}", CkiDayPart, vbCrLf)
            'sLog = sLog.AppendFormat("forceFlag: {0}{1}", forceFlag, vbCrLf)
            'sLog = sLog.AppendFormat("Activity: {0}{1}", Activity, vbCrLf)
            'sLog = sLog.AppendFormat("InvokeDvass: {0}{1}", invokeDvass, vbCrLf)
            'sLog = sLog.AppendFormat("RETURN: {0}{1}", oReturnDoc.OuterXml, vbCrLf)
            ''Logging.LogInformation("Fleet.CompleteRepairMaintenanceRecord", sLog.ToString())

            Return oReturnDoc

        End Using

    End Function

    ''rev:mia 19Aug2014 - fixing AIMS and DVASS syncHronization by forcing transactions on these
    Public Shared Function CreateUpdateRepairMaintenanceRecord(ByVal ProviderId As String, _
                                                               ByVal UnitNo As String, _
                                                               ByVal FromDate As Date, _
                                                               ByVal ToDate As Date, _
                                                               ByVal Branch As String, _
                                                               ByVal OdometerStart As Integer, _
                                                               ByVal Reason1 As String, _
                                                               ByVal Reason2 As String, _
                                                               ByVal Reason3 As String, _
                                                               ByVal OtherReasonDescription As String, _
                                                               ByVal ServicePoint As Integer, _
                                                               ByVal Reference As String, _
                                                               ByRef PurchaseOrderNo As String, _
                                                               ByRef DvassMessage As String, _
                                                               Optional ckoDayPart As Integer = 0, _
                                                               Optional ckiDayPart As Integer = 1, _
                                                               Optional forceFlag As Integer = 0, _
                                                               Optional Activity As String = "ADD", _
                                                               Optional InvokeDvass As Boolean = False, _
                                                               Optional paramObject As Object = Nothing _
                                                               ) As XmlDocument


        Dim oReturnDoc As New XmlDocument
        Dim RepairerId As Integer
        Dim IsNew As Boolean = IIf(Activity = "ADD", True, False)
        Dim xmlReturnMessage As String = ""
        Dim nErrorNo As Integer


        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                If Not ValidateProvider(ProviderId, RepairerId) Then
                    Throw (New Exception("App/'" & ProviderId & "' is not a valid Provider"))
                End If


                Dim oAIMSObj As New AI.CMaster

                ''recode
                nErrorNo = oAIMSObj.F1_RMCreateUpdate(UnitNo, FromDate, ToDate, Branch, OdometerStart, Reason1, Reason2, Reason3, OtherReasonDescription, ServicePoint, RepairerId, Reference, PurchaseOrderNo)

                ''inside the F1_RMCreateUpdate, there is a dvass call that update or insert
                If nErrorNo <> 0 And Activity <> "UPDATE" Then
                    Throw (New Exception("App/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(CStr(nErrorNo))))
                End If


                If (IsValidActivity(Activity)) Then
                    If ((Not String.IsNullOrEmpty(Activity) And InvokeDvass = True) Or (Not InvokeDvass And Activity = "DELETE")) Then

                        DvassMessage = RepairMaintenanceRequest(UnitNo, Branch, FromDate, ckoDayPart, ToDate, ckiDayPart, forceFlag, Activity, paramObject)

                        If (DvassMessage <> "SUCCESS") Then
                            Throw (New Exception("DVASS: " & DvassMessage))
                        End If
                    End If
                End If
                xmlReturnMessage = Aurora.Common.Data.GetErrorMessage(IIf(IsNew, "GEN045", "GEN046"))

                If (Not String.IsNullOrEmpty(DvassMessage) And DvassMessage = "SUCCESS") Then
                    xmlReturnMessage = xmlReturnMessage & " / DVASS: Success"
                End If

                oTransaction.CommitTransaction()
                oReturnDoc.LoadXml("<Root><Message>" & xmlReturnMessage & "</Message></Root>")


            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try
        End Using

        'Dim sLog As New Text.StringBuilder
        'sLog = sLog.AppendFormat("Parameters{0}ProviderId: {1}{2}", vbCrLf, ProviderId, vbCrLf)
        'sLog = sLog.AppendFormat("UnitNo: {0}{1}", UnitNo, vbCrLf)
        'sLog = sLog.AppendFormat("FromDate: {0}{1}", FromDate, vbCrLf)
        'sLog = sLog.AppendFormat("ToDate: {0}{1}", ToDate, vbCrLf)
        'sLog = sLog.AppendFormat("Branch: {0}{1}", Branch, vbCrLf)
        'sLog = sLog.AppendFormat("OdometerStart: {0}{1}", OdometerStart, vbCrLf)
        'sLog = sLog.AppendFormat("Reason1: {0}{1}", Reason1, vbCrLf)
        'sLog = sLog.AppendFormat("Reason2: {0}{1}", Reason2, vbCrLf)
        'sLog = sLog.AppendFormat("Reason3: {0}{1}", Reason3, vbCrLf)
        'sLog = sLog.AppendFormat("OtherReasonDescription: {0}{1}", OtherReasonDescription, vbCrLf)
        'sLog = sLog.AppendFormat("ServicePoint: {0}{1}", ServicePoint, vbCrLf)
        'sLog = sLog.AppendFormat("Reference: {0}{1}", Reference, vbCrLf)
        'sLog = sLog.AppendFormat("PurchaseOrderNo: {0}{1}", PurchaseOrderNo, vbCrLf)
        'sLog = sLog.AppendFormat("DvassMessage: {0}{1}", DvassMessage, vbCrLf)
        'sLog = sLog.AppendFormat("ckoDayPart: {0}{1}", ckoDayPart, vbCrLf)
        'sLog = sLog.AppendFormat("ckiDayPart: {0}{1}", ckiDayPart, vbCrLf)
        'sLog = sLog.AppendFormat("forceFlag: {0}{1}", forceFlag, vbCrLf)
        'sLog = sLog.AppendFormat("Activity: {0}{1}", Activity, vbCrLf)
        'sLog = sLog.AppendFormat("InvokeDvass: {0}{1}", InvokeDvass, vbCrLf)
        'sLog = sLog.AppendFormat("RETURN: {0}{1}", oReturnDoc.OuterXml, vbCrLf)
        ''Logging.LogInformation("Fleet.CreateUpdateRepairMaintenanceRecord", sLog.ToString())
        Return oReturnDoc
    End Function

 

    Public Shared Function ValidateProvider(ByVal ProviderId As String, ByRef RepairerId As Integer) As Boolean
        'Dim bIsValid As Boolean = False

        Dim oXml As XmlDocument
        oXml = DataRepository.GetProviderInfo(ProviderId)
        If Trim(oXml.DocumentElement.InnerText).Equals(String.Empty) Then
            Return False
        Else
            RepairerId = CInt(oXml.DocumentElement.SelectSingleNode("POPUP/ID").InnerText)
            Return True
        End If

    End Function

    Public Shared Function GetRMReportData(ByVal DateEntered As String, ByVal ComparisonValue As String, ByVal LocationCode As String, ByVal UserCode As String) As DataTable
        'GetPopUpData 
        'exec GEN_GetPopUpData @case = 'tmpRMreport', @param1 = '', @param2 = 'LE', @param3 = '', @param4 = '', @param5 = '', @sUsrCode = 'sp7'
        'exec GEN_GetPopUpData @case = 'tmpRMreport', @param1 = '12/12/2007', @param2 = 'LE', @param3 = 'AKL', @param4 = '', @param5 = '', @sUsrCode = 'sp7'
        Dim dstRMReport As DataSet
        dstRMReport = Aurora.Common.Data.GetPopUpData("tmpRMreport", DateEntered, ComparisonValue, LocationCode, "", "", UserCode)
        If dstRMReport.Tables.Count <= 0 Then
            Return New DataTable
        Else
            Return dstRMReport.Tables(0)
        End If
    End Function

    Public Shared Function GetLocations(ByVal CountryCode As String, ByVal UserCode As String) As DataTable
        'exec GEN_GetPopUpData  @case = 'LOCATIONFORCOUNTRY', @param1 = 'NZ', @sUsrCode = 'sp7'
        Dim dstLocations As DataSet
        dstLocations = Aurora.Common.Data.GetPopUpData("LOCATIONFORCOUNTRY", CountryCode, "", "", "", "", UserCode)
        If dstLocations.Tables.Count <= 0 Then
            Return New DataTable
        Else
            Return dstLocations.Tables(0)
        End If
    End Function



#Region "rev:mia 14Aug2014"

    Private Shared Function DvassStatus(ByVal Status As comStatus, Optional ByVal ForceFlag As Long = 0) As String
        Dim strTemp As String

        strTemp = ""
        With Status
            ' OVERALL success/failure
            Select Case .status
                Case 1      ' SUCCESS
                    strTemp = "SUCCESS"
                Case 2, 3   ' SOFT-failure
                    If ForceFlag = 0 Then
                        strTemp = _
                              "SOFT-FAIL-" & CStr(.status) & _
                              ": detail=" & CStr(.detail) & _
                              ", message=" & .msg
                    End If
                Case 4      ' HARD failure
                    strTemp = _
                          "HARD-FAIL (" & CStr(.status) & _
                          "): detail=" & CStr(.detail) & _
                          ", message=" & .msg
                Case 5      ' INTERFACE error
                    strTemp = _
                          "INTERFACE ERROR (" & CStr(.status) & _
                          "): detail=" & CStr(.detail) & _
                          ", message=" & .msg
                Case 9      ' STATE error
                    strTemp = "RES064/RES064 - DVASS is not yet started."
                Case Else
                    strTemp = _
                          "UNKNOWN STATUS (" & CStr(.status) & _
                          "): detail=" & CStr(.detail) & _
                          ", message=" & .msg
            End Select
        End With ' pStatus
        Return strTemp
    End Function

    Private Shared Function ToDVASSDateTime(ByVal pDate As Date, ByVal pTime As Object) As comDateTime

        Dim lngJulian As Long
        Dim lngTimeType As Long
        Dim tmpDateTime As comDateTime

        Try
            Select Case UCase(TypeName(pTime))
                Case "LONG"
                    lngTimeType = CLng(pTime)
                Case "STRING"
                    If UCase(pTime) = "PM" Then
                        lngTimeType = 1
                    Else
                        lngTimeType = 0
                    End If
                Case Else
                    lngTimeType = 0
            End Select

            lngJulian = DateDiff("d", CDate("2000-01-01"), pDate)
            With tmpDateTime
                .d = lngJulian
                If lngTimeType = 0 Then
                    .t = 0  ' MID-NIGHT
                Else
                    .t = 1  ' MID-DAY
                End If
            End With
        Catch ex As Exception

        End Try

        Return tmpDateTime
    End Function

    Private Shared Function RepairMaintenanceRequest(vehId As String, _
                                                   locId As String, _
                                                   ByVal CkoDate As Date, _
                                                   ByVal CkoDayPart As Long, _
                                                   ByVal CkiDate As Date, _
                                                   ByVal CkiDayPart As Long, _
                                                   forceFlag As Integer, _
                                                   Activity As String, _
                                                   Optional paramObject As Object = Nothing) As String


        ''REV:MIA 21AUG2014 - will wait for CSIRO to fix DVASS. this code will be intentionally ignored
        ''                  - today is my brother's birthday
        Dim result As String = ""
        Dim sLog As Text.StringBuilder = New Text.StringBuilder()
        Try

            Dim comCkoDateTime As COMDVASSMgrLib.comDateTime
            Dim comCkiDateTime As COMDVASSMgrLib.comDateTime
            Dim comDvassStatus As COMDVASSMgrLib.comStatus
            Dim iCountryId As Integer

            With comDvassStatus
                .status = 0
                .msg = ""
                .detail = 0
            End With


            comCkoDateTime = ToDVASSDateTime(CkoDate, CkoDayPart)
            comCkiDateTime = ToDVASSDateTime(CkiDate, CkiDayPart)
            sLog = sLog.AppendFormat("{0}", vbCrLf)
            sLog = sLog.AppendFormat("-----PARAMETERS-----{0}vehId: {1}{2}", vbCrLf, vehId, vbCrLf)

            sLog = sLog.AppendFormat("locId       : {0}{1}", locId, vbCrLf)
            sLog = sLog.AppendFormat("ckodate     : {0}{1}", CkoDate, vbCrLf)
            sLog = sLog.AppendFormat("ckodayPart  : {0}{1}", CkoDayPart, vbCrLf)
            sLog = sLog.AppendFormat("ckidate     : {0}{1}", CkiDate, vbCrLf)
            sLog = sLog.AppendFormat("ckidayPart  : {0}{1}", CkiDayPart, vbCrLf)
            sLog = sLog.AppendFormat("forcedflag  : {0}{1}", forceFlag, vbCrLf)
            sLog = sLog.AppendFormat("comckodatetime.d    : {0}{1}", comCkoDateTime.d, vbCrLf)

            Dim country As String = GetCountryForLocation(locId)
            sLog = sLog.AppendFormat("country     : {0}{1}", country, vbCrLf)

            iCountryId = IIf(country = "AU", 0, 1)
            sLog = sLog.AppendFormat("recheck icountry: {0}{1}", iCountryId, vbCrLf)
            Dim objDvass As New COMDVASSMgrLib.CDVASSMgr
            Dim isDvassCalled As Boolean = True

            If (Activity = "ADD") Then
                objDvass.AddMaintRequest(iCountryId, vehId, locId, comCkoDateTime, comCkiDateTime, forceFlag, comDvassStatus)
            ElseIf (Activity = "COMPLETED") Then
                objDvass.ChangeMaintRequest(iCountryId, vehId, comCkoDateTime.d, locId, comCkoDateTime, comCkiDateTime, forceFlag, comDvassStatus)
            ElseIf (Activity = "UPDATE") Then
                sLog = sLog.AppendFormat("MANNY'S MESSAGE     : {0}{1}", "1. For every update, we need to delete and reissue add call", vbCrLf)
                ''DVASS will initiate DVASS Delete and ADD for every update going to happen
                objDvass.DeleteMaintRequest(iCountryId, vehId, comCkoDateTime.d, comDvassStatus)
                objDvass.AddMaintRequest(iCountryId, vehId, locId, comCkoDateTime, comCkiDateTime, forceFlag, comDvassStatus)
                'objDvass.DeleteMaintRequest(iCountryId, vehId, comCkoDateTime.d, comDvassStatus)
                'result = DvassStatus(comDvassStatus, forceFlag)
                'If (result = "SUCCESS") Then
                '    sLog = sLog.AppendFormat("MANNY'S MESSAGE     : {0}{1}", "2.Delete is OK", vbCrLf)
                '    objDvass.CheckData(iCountryId, comDvassStatus)
                '    result = DvassStatus(comDvassStatus, forceFlag)
                'End If
                'If (result = "SUCCESS") Then
                '    sLog = sLog.AppendFormat("MANNY'S MESSAGE     : {0}{1}", "3.CheckData is OK", vbCrLf)
                '    objDvass.AddMaintRequest(iCountryId, vehId, locId, comCkoDateTime, comCkiDateTime, forceFlag, comDvassStatus)
                'End If


            ElseIf (Activity = "DELETE") Then
                objDvass.DeleteMaintRequest(iCountryId, vehId, comCkoDateTime.d, comDvassStatus)
            Else
                isDvassCalled = False
            End If


            If (isDvassCalled) Then
                result = DvassStatus(comDvassStatus, forceFlag)
            Else
                result = "DVASS was not called. Invalid Parameter " + Activity
            End If

            sLog = sLog.AppendFormat("RESULT: {0}{1}", result & "/" & Activity.ToUpper, vbCrLf)
        Catch ex As Exception
            sLog = sLog.AppendFormat("ERROR: {0}{1}", ex.Message & "," & ex.StackTrace, vbCrLf)
            result = "ERROR: " & ex.Message
        End Try
        Logging.LogInformation("OpsAndLogs.RepairMaintenanceRequest", sLog.ToString)
        Return result
    End Function

    Public Shared Function AddModifyDeleteMaintenanceRequest(ByVal vehId As String, _
                                                      ByVal locId As String, _
                                                      ByVal CkoDate As Date, _
                                                      ByVal CkoDayPart As Long, _
                                                      ByVal CkiDate As Date, _
                                                      ByVal CkiDayPart As Long, _
                                                      ByVal forceFlag As Integer, _
                                                      ByVal Activity As String) As String

        Return RepairMaintenanceRequest(vehId, locId, CkoDate, CkoDayPart, CkiDate, CkiDayPart, forceFlag, Activity)

    End Function

    Public Shared Function GetServicePointV2(ByVal AssetId As String) As DataTable

        Dim dstServicePoints As DataSet
        dstServicePoints = Aurora.Common.Data.GetComboData("OPLOGSERVICEPOINTV2", AssetId, "", "")

        If dstServicePoints.Tables.Count <= 0 Then
            Return New DataTable
        Else
            Return dstServicePoints.Tables(0)
        End If
    End Function

    Public Shared Function FleetRepair_InsUpFleetScheduleAndFlag(iRepairId As Integer, _
                                                       iFleetAssetId As Integer, _
                                                       sScheduledService As String, _
                                                       bOfffleetFlag As Boolean, _
                                                       sUserCode As String, _
                                                       sOtherRepairer As String, _
                                                       iServiceId As Integer, _
                                                       dfromDate As DateTime, _
                                                       dtodate As DateTime, _
                                                       Optional PO As String = "") As String

        Dim sLog As Text.StringBuilder = New Text.StringBuilder()
        sLog = sLog.AppendFormat("Parameters{0}iRepairId: {1}{2}", vbCrLf, iRepairId, vbCrLf)
        sLog = sLog.AppendFormat("iFleetAssetId: {0}{1}", iFleetAssetId, vbCrLf)
        sLog = sLog.AppendFormat("sScheduledService: {0}{1}", sScheduledService, vbCrLf)
        sLog = sLog.AppendFormat("iServiceId: {0}{1}", iServiceId, vbCrLf)
        sLog = sLog.AppendFormat("bOffFleetFlag: {0}{1}", bOfffleetFlag, vbCrLf)
        sLog = sLog.AppendFormat("sUserCode: {0}{1}", sUserCode, vbCrLf)
        sLog = sLog.AppendFormat("sOtherRepairer: {0}{1}", sOtherRepairer, vbCrLf)
        sLog = sLog.AppendFormat("poNumber: {0}{1}", PO, vbCrLf)

        If (iRepairId = -1 And iFleetAssetId = -1) Then
            sLog = sLog.Append("irepairId = -1 and iFleetAssetId = 1..iRepairId and iFleetAssetId are invalid " & vbCrLf)
            Logging.LogDebug("FleetRepair_InsUpFleetScheduleAndFlag", sLog.ToString)
            Return "Error:iRepairId and iFleetAssetId are invalid."
        End If


        Dim oParamArray(8) As Aurora.Common.Data.Parameter

        Try
            oParamArray(0) = New Aurora.Common.Data.Parameter("iRepairId", DbType.Int32, iRepairId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(1) = New Aurora.Common.Data.Parameter("iFleetAssetId", DbType.Int32, iFleetAssetId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(2) = New Aurora.Common.Data.Parameter("sScheduledService", DbType.String, sScheduledService, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(3) = New Aurora.Common.Data.Parameter("iServiceId", DbType.Int32, IIf(iServiceId <= 0, DBNull.Value, iServiceId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(4) = New Aurora.Common.Data.Parameter("bOffFleetFlag", DbType.Boolean, bOfffleetFlag, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(5) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, sUserCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(6) = New Aurora.Common.Data.Parameter("sOtherRepairer", DbType.String, IIf(String.IsNullOrEmpty(sOtherRepairer), DBNull.Value, sOtherRepairer), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(7) = New Aurora.Common.Data.Parameter("poNumber", DbType.String, IIf(String.IsNullOrEmpty(PO), DBNull.Value, PO), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParamArray(8) = New Aurora.Common.Data.Parameter("output", DbType.Int32, 10, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("FleetRepair_InsUpFleetScheduleAndFlag", oParamArray)

            sLog = sLog.AppendFormat("output: {0}{1}", IIf(oParamArray(7).Value = "1", "SUCCESS", ""), vbCrLf)
            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)

        Catch ex As Exception
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
            Return "Error:" & ex.Message
        End Try
        Logging.LogDebug("FleetRepair_InsUpFleetScheduleAndFlag", sLog.ToString)
        Return "OK"
    End Function

    Private Shared Function GetCountryForLocation(branch As String) As String
        Dim qrytext As String = "select dbo.getCountryforlocation('" & branch & "',NULL,NULL)"
        Return Aurora.Common.Data.ExecuteScalarSQL(qrytext)

    End Function

    Private Shared Function IsValidActivity(activity As String) As Boolean
        Return activity = "ADD" Or _
               activity = "UPDATE" Or _
               activity = "UPDATECKODATE" Or _
               activity = "UPDATECKIDATE" Or _
               activity = "UPDATEBRANCH" Or _
               activity = "DELETE" Or _
               activity = "COMPLETED"
    End Function

    Public Shared Function IsActivityExist(unitnumber As Integer, dStartDate As DateTime, dEndtDate As DateTime) As Boolean
        Return DataRepository.IsActivityExist(unitnumber, dStartDate, dEndtDate)
    End Function
#End Region

#Region "rev:mia 22Sept2014 - splitting of AIMS"
    ''rev:mia 19Sept2014 - splitting of AIMS Call for DVASS.Today in history: Scotland referendum/election
    Public Shared Function CreateRepairMaintenanceRecord(fleet_asset_id As Integer, _
                                                                  date_raised As DateTime, _
                                                                  LocationCode As String, _
                                                                  repair_type As String, _
                                                                  other_type_1 As String, _
                                                                  other_type_2 As String, _
                                                                  repair_code As String, _
                                                                  off_fleet_flag As Boolean, _
                                                                  estimated_cost As Decimal, _
                                                                  actual_cost As Decimal, _
                                                                  invoice_number As String, _
                                                                  modified_by As String, _
                                                                  reason_description As String, _
                                                                  start_date_time As DateTime, _
                                                                  end_date_time As DateTime, _
                                                                  start_odometer As Integer, _
                                                                  end_odometer As Integer, _
                                                                  completed As Boolean, _
                                                                  other_repairer As String, _
                                                                  service_id As Integer, _
                                                                  service_provider_id As String, _
                                                                  ByRef po_number As String, _
                                                                  ByRef errorMsg As String
                                                               ) As Boolean


        Dim sLog As New Text.StringBuilder
        sLog = sLog.AppendFormat("Parameters{0}fleet_asset_id: {1}{2}", vbCrLf, fleet_asset_id, vbCrLf)
        sLog = sLog.AppendFormat("date_raised: {0}{1}", date_raised, vbCrLf)
        sLog = sLog.AppendFormat("LocationCode: {0}{1}", LocationCode, vbCrLf)
        sLog = sLog.AppendFormat("repair_type: {0}{1}", repair_type, vbCrLf)
        sLog = sLog.AppendFormat("other_type_1: {0}{1}", other_type_1, vbCrLf)
        sLog = sLog.AppendFormat("other_type_2: {0}{1}", other_type_2, vbCrLf)
        sLog = sLog.AppendFormat("repair_code: {0}{1}", repair_code, vbCrLf)
        sLog = sLog.AppendFormat("off_fleet_flag: {0}{1}", off_fleet_flag, vbCrLf)
        sLog = sLog.AppendFormat("estimated_cost: {0}{1}", estimated_cost, vbCrLf)
        sLog = sLog.AppendFormat("actual_cost: {0}{1}", actual_cost, vbCrLf)
        sLog = sLog.AppendFormat("invoice_number: {0}{1}", invoice_number, vbCrLf)
        sLog = sLog.AppendFormat("modified_by: {0}{1}", modified_by, vbCrLf)
        sLog = sLog.AppendFormat("reason_description: {0}{1}", reason_description, vbCrLf)
        sLog = sLog.AppendFormat("start_date_time: {0}{1}", start_date_time, vbCrLf)
        sLog = sLog.AppendFormat("end_date_time: {0}{1}", end_date_time, vbCrLf)
        sLog = sLog.AppendFormat("start_odometer: {0}{1}", start_odometer, vbCrLf)
        sLog = sLog.AppendFormat("end_odometer: {0}{1}", end_odometer, vbCrLf)
        sLog = sLog.AppendFormat("completed: {0}{1}", completed, vbCrLf)
        sLog = sLog.AppendFormat("other_repairer: {0}{1}", other_repairer, vbCrLf)
        sLog = sLog.AppendFormat("service_id: {0}{1}", service_id, vbCrLf)
        sLog = sLog.AppendFormat("service_provider_id: {0}{1}", service_provider_id, vbCrLf)
        Logging.LogInformation("Fleet.CreateRepairMaintenanceRecord", sLog.ToString())

        Return DataRepository.CreateRepairMaintenance(fleet_asset_id,
                                                                      date_raised, _
                                                                      LocationCode, _
                                                                      repair_type, _
                                                                      other_type_1, _
                                                                      other_type_2, _
                                                                      repair_code, _
                                                                      off_fleet_flag, _
                                                                      estimated_cost, _
                                                                      actual_cost, _
                                                                      invoice_number, _
                                                                      modified_by, _
                                                                      reason_description, _
                                                                      start_date_time, _
                                                                      end_date_time, _
                                                                      start_odometer, _
                                                                      end_odometer, _
                                                                      completed, _
                                                                      other_repairer, _
                                                                      service_id, _
                                                                      service_provider_id, _
                                                                      po_number, _
                                                                      errorMsg)


    End Function

    Public Shared Function UpdateRepairMaintenanceRecord(fleet_asset_id As Integer, _
                                                                  date_raised As DateTime, _
                                                                  LocationCode As String, _
                                                                  repair_type As String, _
                                                                  other_type_1 As String, _
                                                                  other_type_2 As String, _
                                                                  repair_code As String, _
                                                                  off_fleet_flag As Boolean, _
                                                                  estimated_cost As Decimal, _
                                                                  actual_cost As Decimal, _
                                                                  invoice_number As String, _
                                                                  modified_by As String, _
                                                                  reason_description As String, _
                                                                  start_date_time As DateTime, _
                                                                  end_date_time As DateTime, _
                                                                  start_odometer As Integer, _
                                                                  end_odometer As Integer, _
                                                                  completed As Boolean, _
                                                                  other_repairer As String, _
                                                                  service_id As Integer, _
                                                                  service_provider_id As String, _
                                                                  po_number As String, _
                                                                  ByRef errorMsg As String
                                                               ) As Boolean


        Dim sLog As New Text.StringBuilder
        sLog = sLog.AppendFormat("Parameters{0}fleet_asset_id: {1}{2}", vbCrLf, fleet_asset_id, vbCrLf)
        sLog = sLog.AppendFormat("date_raised: {0}{1}", date_raised, vbCrLf)
        sLog = sLog.AppendFormat("LocationCode: {0}{1}", LocationCode, vbCrLf)
        sLog = sLog.AppendFormat("repair_type: {0}{1}", repair_type, vbCrLf)
        sLog = sLog.AppendFormat("other_type_1: {0}{1}", other_type_1, vbCrLf)
        sLog = sLog.AppendFormat("other_type_2: {0}{1}", other_type_2, vbCrLf)
        sLog = sLog.AppendFormat("repair_code: {0}{1}", repair_code, vbCrLf)
        sLog = sLog.AppendFormat("off_fleet_flag: {0}{1}", off_fleet_flag, vbCrLf)
        sLog = sLog.AppendFormat("estimated_cost: {0}{1}", estimated_cost, vbCrLf)
        sLog = sLog.AppendFormat("actual_cost: {0}{1}", actual_cost, vbCrLf)
        sLog = sLog.AppendFormat("invoice_number: {0}{1}", invoice_number, vbCrLf)
        sLog = sLog.AppendFormat("modified_by: {0}{1}", modified_by, vbCrLf)
        sLog = sLog.AppendFormat("reason_description: {0}{1}", reason_description, vbCrLf)
        sLog = sLog.AppendFormat("start_date_time: {0}{1}", start_date_time, vbCrLf)
        sLog = sLog.AppendFormat("end_date_time: {0}{1}", end_date_time, vbCrLf)
        sLog = sLog.AppendFormat("start_odometer: {0}{1}", start_odometer, vbCrLf)
        sLog = sLog.AppendFormat("end_odometer: {0}{1}", end_odometer, vbCrLf)
        sLog = sLog.AppendFormat("completed: {0}{1}", completed, vbCrLf)
        sLog = sLog.AppendFormat("other_repairer: {0}{1}", other_repairer, vbCrLf)
        sLog = sLog.AppendFormat("service_id: {0}{1}", service_id, vbCrLf)
        sLog = sLog.AppendFormat("service_provider_id: {0}{1}", service_provider_id, vbCrLf)
        sLog = sLog.AppendFormat("po_number: {0}{1}", po_number, vbCrLf)
        Logging.LogInformation("Fleet.UpdateRepairMaintenanceRecord", sLog.ToString())

        Return DataRepository.UpdateRepairMaintenance(fleet_asset_id,
                                                                      date_raised, _
                                                                      LocationCode, _
                                                                      repair_type, _
                                                                      other_type_1, _
                                                                      other_type_2, _
                                                                      repair_code, _
                                                                      off_fleet_flag, _
                                                                      estimated_cost, _
                                                                      actual_cost, _
                                                                      invoice_number, _
                                                                      modified_by, _
                                                                      reason_description, _
                                                                      start_date_time, _
                                                                      end_date_time, _
                                                                      start_odometer, _
                                                                      end_odometer, _
                                                                      completed, _
                                                                      other_repairer, _
                                                                      service_id, _
                                                                      service_provider_id, _
                                                                      po_number, _
                                                                      errorMsg)


    End Function

    Public Shared Function DeleteRepairMaintenance(fleet_asset_id As Integer, iRepairId As Integer, sUsrCode As String, sAddModPrgmName As String, ByRef errorMsg As String) As Boolean

        Dim sLog As New Text.StringBuilder
        sLog = sLog.AppendFormat("Parameters{0}fleet_asset_id: {1}{2}", vbCrLf, fleet_asset_id, vbCrLf)
        sLog = sLog.AppendFormat("iRepairId: {0}{1}", iRepairId, vbCrLf)
        sLog = sLog.AppendFormat("sUsrCode: {0}{1}", sUsrCode, vbCrLf)
        sLog = sLog.AppendFormat("sAddModPrgmName: {0}{1}", sAddModPrgmName, vbCrLf)
        Logging.LogInformation("Fleet.DeleteRepairMaintenance", sLog.ToString())
        Return DataRepository.DeleteRepairMaintenance(fleet_asset_id, iRepairId, sUsrCode, sAddModPrgmName, errorMsg)

    End Function

#End Region

End Class
