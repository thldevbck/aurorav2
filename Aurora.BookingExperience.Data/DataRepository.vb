﻿
Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Common
Imports System.Data.SqlClient

Public Class DataRepository
#Region "rev:mia 28Sept2015 - Experiences"
    Public Shared Function CallTicketsSummary(RentalId As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_CallTicketsSummary", result, RentalId)
            result.Tables(0).TableName = "HeaderTable"
            result.Tables(1).TableName = "HeaderSummaryTable"
        Catch ex As Exception
            Logging.LogError("CallTicketsSummary", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function
    Public Shared Function LoadStaticData(Country As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetStaticData", result, Country)
            result.Tables(0).TableName = "ExperienceCallStatus"
            result.Tables(1).TableName = "TicketStatus"
            result.Tables(2).TableName = "Supplier"
            result.Tables(3).TableName = "Experiences"
            result.Tables(4).TableName = "Items"
        Catch ex As Exception
            Logging.LogError("LoadStaticData", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetCustomerInfo(BookingId As String, RentalId As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetCustomerInfo", result, BookingId, RentalId)
        Catch ex As Exception
            Logging.LogError("GetCustomerInfo", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetCallContactHistory(ExpCallId As Integer, RentalId As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetCallContactHistory", result, RentalId, ExpCallId)
        Catch ex As Exception
            Logging.LogError("GetCallContactHistory", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetPurchaseHistory(ExpCallId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetPurchaseHistory", result, ExpCallId)
        Catch ex As Exception
            Logging.LogError("GetPurchaseHistory", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function InsertRecordInHistory(ExpCallRntId As String, ExpCallStatusId As String, ExpCallOutboundCallDt As DateTime, ExpCallNotes As String, AddUsrId As String, ExpCallPhNumber As String) As String
        Dim params(6) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallRntId", DbType.String, ExpCallRntId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ExpCallStatusId", DbType.String, ExpCallStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            If (ExpCallOutboundCallDt = DateTime.MinValue) Then
                params(2) = New Aurora.Common.Data.Parameter("ExpCallOutboundCallDt", DbType.DateTime, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Else
                params(2) = New Aurora.Common.Data.Parameter("ExpCallOutboundCallDt", DbType.DateTime, ExpCallOutboundCallDt, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            End If
            params(3) = New Aurora.Common.Data.Parameter("ExpCallNotes", DbType.String, ExpCallNotes, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("ExpCallPhNumber", DbType.String, ExpCallPhNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("result", DbType.String, 5, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXP_InsertRecordInHistory", params)
            result = params(6).Value

            If (Not String.IsNullOrEmpty(result)) Then
                If (UpdateChannelReferrer(CInt(result), ExpCallRntId, "AURORA").Contains("ERROR")) Then
                    Throw New Exception("Updating of ChannelReferrer in Aurora failed")
                End If
            End If

        Catch ex As Exception
            Logging.LogError("InsertRecordInHistory", ex.Message & ", " & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK-" + result
    End Function

    Public Shared Function CloneRecordInHistory(ExpCallId As Integer, ExpCallStatusId As String) As String
        Dim params(2) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallId", DbType.Int32, ExpCallId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ExpCallStatusCodId", DbType.String, ExpCallStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("result", DbType.String, 5, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXP_CloneRecordInHistory", params)
            result = params(2).Value
        Catch ex As Exception
            Logging.LogError("CloneRecordInHistory", ex.Message & ", " & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK-" + result
    End Function

    Public Shared Function UpdateRecordInHistory(ExpCallId As String, ExpCallStatusId As String, ExpCallOutboundCallDt As DateTime, ExpCallNotes As String, ModUsrId As String, ExpCallPhNumber As String) As String
        Dim params(6) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallId", DbType.String, ExpCallId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ExpCallStatusId", DbType.String, ExpCallStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            If (ExpCallOutboundCallDt = DateTime.MinValue) Then
                params(2) = New Aurora.Common.Data.Parameter("ExpCallOutboundCallDt", DbType.DateTime, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Else
                params(2) = New Aurora.Common.Data.Parameter("ExpCallOutboundCallDt", DbType.DateTime, ExpCallOutboundCallDt, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            End If
            params(3) = New Aurora.Common.Data.Parameter("ExpCallNotes", DbType.String, ExpCallNotes, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, ModUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(5) = New Aurora.Common.Data.Parameter("ExpCallPhNumber", DbType.String, ExpCallPhNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(6) = New Aurora.Common.Data.Parameter("result", DbType.String, 5, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXP_UpdateRecordInHistory", params)
            result = params(6).Value

            If (Not String.IsNullOrEmpty(result)) Then
                If (UpdateChannelReferrer(CInt(ExpCallId), "", "AURORA").Contains("ERROR")) Then
                    Throw New Exception("Updating of ChannelReferrer in Aurora failed")
                End If
            End If


        Catch ex As Exception
            Logging.LogError("UpdateRecordInHistory", ex.Message & ", " & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK-" + result
    End Function

    Public Shared Function GetCallContactHistories(ExpCallId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetCallContactHistories", result, ExpCallId)
        Catch ex As Exception
            Logging.LogError("GetCallContactHistories", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function InsertExperienceInHistory(ExpCallId As String, ExpItemId As String, ExpCallTktPrice As Decimal, ExpCallTktQty As Integer, ExpCallTktTotal As Decimal, ExpCallTktDateTime As DateTime, ExpCallTktSupInfo As String, ExpCallTktStatusId As String, ExpCallTktResRefNum As String, ExpCallTktPrdLinkId As String, AddUsrId As String, ExpCallTktSupplierRefNum As String) As String
        Dim params(12) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallId", DbType.String, ExpCallId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ExpItemId", DbType.Int32, ExpItemId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("ExpCallTktPrice", DbType.Decimal, ExpCallTktPrice, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("ExpCallTktQty", DbType.Int16, ExpCallTktQty, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("ExpCallTktTotal", DbType.Decimal, ExpCallTktTotal, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            If (ExpCallTktDateTime = DateTime.MinValue) Then
                params(5) = New Aurora.Common.Data.Parameter("ExpCallTktDateTime", DbType.DateTime, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Else
                params(5) = New Aurora.Common.Data.Parameter("ExpCallTktDateTime", DbType.DateTime, ExpCallTktDateTime, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            End If
            params(6) = New Aurora.Common.Data.Parameter("ExpCallTktSupInfo", DbType.String, ExpCallTktSupInfo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("ExpCallTktStatusId", DbType.String, ExpCallTktStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("ExpCallTktResRefNum", DbType.String, ExpCallTktResRefNum, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("ExpCallTktPrdLinkId", DbType.Int32, ExpCallTktPrdLinkId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("result", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            params(12) = New Aurora.Common.Data.Parameter("ExpCallTktSupplierRefNum", DbType.String, ExpCallTktSupplierRefNum, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXP_InsertExperienceInHistory", params)
            result = params(11).Value
            If (result.Contains("ERROR")) Then
                Throw New Exception(result.Replace("ERROR", ""))
            End If
        Catch ex As Exception
            Logging.LogError("InsertExperienceInHistory", ex.Message & ", " & ex.StackTrace)
            Return "ERROR: " + ex.Message.Replace("ERROR", "")
        End Try
        Return "OK-" + result
    End Function

    Public Shared Function GetExperienceSummary(ExpCallId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetExperienceSummary", result, ExpCallId)
        Catch ex As Exception
            Logging.LogError("GetExperienceSummary", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetSingleExperienceCall(ExpCallId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetSingleExperienceCall", result, ExpCallId)
        Catch ex As Exception
            Logging.LogError("GetSingleExperienceCall", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetSingleExperienceCallTickets(ExpCallTktId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetSingleExperienceCallTickets", result, ExpCallTktId)
        Catch ex As Exception
            Logging.LogError("GetSingleExperienceCallTickets", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function UpdateSingleExperienceCallTickets(ExpCallId As String, ExpItemId As String, ExpCallTktPrice As Decimal, ExpCallTktQty As Integer, ExpCallTktTotal As Decimal, ExpCallTktDateTime As DateTime, ExpCallTktSupInfo As String, ExpCallTktStatusId As String, ExpCallTktResRefNum As String, ExpCallTktPrdLinkId As String, ModUsrId As String, ExpCallTktId As Integer, ExpCallTktSupplierRefNum As String) As String
        Dim params(13) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallId", DbType.String, ExpCallId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ExpItemId", DbType.Int32, ExpItemId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("ExpCallTktPrice", DbType.Decimal, ExpCallTktPrice, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("ExpCallTktQty", DbType.Int16, ExpCallTktQty, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("ExpCallTktTotal", DbType.Decimal, ExpCallTktTotal, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            If (ExpCallTktDateTime = DateTime.MinValue) Then
                params(5) = New Aurora.Common.Data.Parameter("ExpCallTktDateTime", DbType.DateTime, DBNull.Value, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Else
                params(5) = New Aurora.Common.Data.Parameter("ExpCallTktDateTime", DbType.DateTime, ExpCallTktDateTime, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            End If
            params(6) = New Aurora.Common.Data.Parameter("ExpCallTktSupInfo", DbType.String, ExpCallTktSupInfo, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("ExpCallTktStatusId", DbType.String, ExpCallTktStatusId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("ExpCallTktResRefNum", DbType.String, ExpCallTktResRefNum, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("ExpCallTktPrdLinkId", DbType.Int32, ExpCallTktPrdLinkId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(10) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, ModUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(11) = New Aurora.Common.Data.Parameter("ExpCallTktId", DbType.String, ExpCallTktId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(12) = New Aurora.Common.Data.Parameter("result", DbType.String, 2000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            params(13) = New Aurora.Common.Data.Parameter("ExpCallTktSupplierRefNum", DbType.String, ExpCallTktSupplierRefNum, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXP_UpdateSingleExperienceCallTickets", params)
            result = params(12).Value
            If (result.Contains("ERROR")) Then
                Throw New Exception(result.Replace("ERROR", ""))
            End If
        Catch ex As Exception
            Logging.LogError("UpdateSingleExperienceCallTickets", ex.Message & ", " & ex.StackTrace)
            Return "ERROR: " + ex.Message.Replace("ERROR", "")
        End Try
        Return "OK-" + result
    End Function

    Public Shared Function GetExperienceSupplierAndItems(ExpCallId As Integer, ExpCallTktId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetExperienceSupplierAndItems", result, ExpCallId, ExpCallTktId)
        Catch ex As Exception
            Logging.LogError("GetExperienceSupplierAndItems", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function ConfirmationSummary(BookingId As String, RentalId As String, userId As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_ConfirmationSummary", result, BookingId, RentalId, userId)
        Catch ex As Exception
            Logging.LogError("ConfirmationSummary", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function ConfirmationSummary(BookingId As String, RentalId As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_ConfirmationSummaries", result, BookingId, RentalId)
        Catch ex As Exception
            Logging.LogError("ConfirmationSummary", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetSubItems(ExpItemId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetSubItems", result, ExpItemId)
        Catch ex As Exception
            Logging.LogError("GetSubItems", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetTags(ExpCallTktId As Integer, ExpCallId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetTags", result, ExpCallTktId, ExpCallId)
        Catch ex As Exception
            Logging.LogError("GetTags", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function GetCountry(rentalId As String) As String
        Dim result As New DataSet
        Dim country As String = "NZ"
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_CountryBaseOnRental", result, rentalId)

            If (result.Tables.Count - 1 <> -1) Then
                If (result.Tables(0).Rows.Count - 1 <> -1) Then
                    If (Not result.Tables(0).Rows(0)("TctCtyCode") Is Nothing) Then
                        country = result.Tables(0).Rows(0)("TctCtyCode").ToString
                    End If
                End If
            End If

        Catch ex As Exception
            Logging.LogError("GetCountry", ex.Message & ", " & ex.StackTrace)
        End Try
        Return country
    End Function

    Public Shared Function GetTagsForSupplierAndExperience(SupId As Integer) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_GetTagsForSupplierAndExperience", result, SupId)
        Catch ex As Exception
            Logging.LogError("GetTagsForSupplierAndExperience", ex.Message & ", " & ex.StackTrace)
        End Try
        Return result
    End Function

    Public Shared Function UpdateCallHistoryForModifiedUser(ExpCallId As String, ExpCallHisId As String, AddUsrId As String) As String
        Dim params(2) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallId", DbType.String, CInt(ExpCallId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ExpCallHisId", DbType.Int32, CInt(ExpCallHisId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, AddUsrId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXEC_UpdateCallHistoryForModifiedUser", params)
            
        Catch ex As Exception
            Logging.LogError("UpdateCallHistoryForModifiedUser", ex.Message & ", " & ex.StackTrace)
            Return "ERROR: " + ex.Message
        End Try
        Return "OK"
    End Function

    Public Shared Function UpdateChannelReferrer(ExpCallId As String, RentalId As String, ChannelReferrer As String) As String
        Dim params(2) As Aurora.Common.Data.Parameter
        Dim result As String = ""
        Try
            params(0) = New Aurora.Common.Data.Parameter("ExpCallId", DbType.String, CInt(ExpCallId), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("RentalId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("ChannelReferrer", DbType.String, ChannelReferrer, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("EXP_UpdateChannelReferrer", params)

        Catch ex As Exception
            Logging.LogError("UpdateChannelReferrer", ex.Message & ", " & ex.StackTrace)
            Return "ERROR: " + ex.Message
        End Try
        Return "OK"

    End Function

#End Region

#Region "rev:mia https://thlonline.atlassian.net/browse/AURORA-610 - as a Finance user I want to see invoice details on all Experience vouchers"
    Public Shared Function GetExperienceChargeDetails(BpdId As String) As DataSet
        Dim ds As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EXP_getProductChargeSummary", ds, BpdId, "", "")
        Catch ex As Exception
            Logging.LogError("GetExperienceChargeDetails", ex.Message & ", " & ex.StackTrace)
        End Try
        Return ds
    End Function
#End Region

End Class
