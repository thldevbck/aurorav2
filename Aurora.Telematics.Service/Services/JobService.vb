﻿Imports Aurora.Telematics.Data
Imports Aurora.Common.Logging
Imports Aurora.Common
Imports System.Data.Common

Namespace Services

    Public Class JobService
        Private jobProxy As New JobAPI()
        Private vehicleProxy As New VehicleAPI()
        Private vehSer As New VehicleService()

        Function CreateJobForVehicle(vehGuid As String, bookingId As String, regoNumber As String, jobStartDateTime As DateTime, connSessionId As Guid) As String
            Dim RetMessage As String = "Job Created Successfully"
            Dim vehSer As VehicleService = New VehicleService()
            If jobStartDateTime.Equals(New DateTime()) Then
                jobStartDateTime = DateTime.Now
            End If
            'Retrive the vehicle for which job has to be created
            Dim retVeh As i360Vehicle = vehSer.GetVehicleByGUID(vehGuid, connSessionId)
            If retVeh Is Nothing Then
                RetMessage = "Vehicle not found with GUID [" + vehGuid + "]"
                Return RetMessage
            End If
            'Create new Job an add values to attributes
            Dim newJob As i360Job = New i360Job
            newJob.Description = "BookingID: " + bookingId + " | Rego: " + IIf(String.IsNullOrEmpty(regoNumber), " ", regoNumber)
            newJob.Name = "BookingID: " + bookingId
            newJob.StartDate = jobStartDateTime ' DateTime.Now
            newJob.ID = Guid.NewGuid
            newJob.AssignedTo = retVeh.Name

            Dim myJobStatesResponse As ResponseOfListOfi360JobState = jobProxy.GetJobStates(connSessionId)
            For Each js As i360JobState In myJobStatesResponse.Value
                If js.Name.Equals("In Progress") Then
                    newJob.StatusID = js.ID
                End If
            Next

            Dim SaveJobResult As Aurora.Telematics.Data.ResponseOfi360Job = jobProxy.SaveJob(connSessionId, newJob)

            If SaveJobResult.Code = ResponseCode.OK Then
                vehSer.SetVehicleAttribute("ActiveJobID", newJob.ID.ToString(), retVeh, connSessionId)

                ' Now Create Task for the Job
                Dim newTask As New i360Task
                newTask.AssignedTo = retVeh.Name
                newTask.Description = "Checkout Task: " + DateTime.Now.ToShortTimeString
                newTask.Name = "CheckOut"
                newTask.StartDate = newJob.StartDate.AddMinutes(1) '// Add 1 minute so that task start is after job start (i360 requires this) There is a current issue with Timezones when creating Jobs and times are 4 hours out                      
                newTask.DueDate = newTask.StartDate

                For Each js As i360JobState In myJobStatesResponse.Value
                    If js.Name.Equals("Completed") Then
                        newTask.StatusID = js.ID
                    End If
                Next

                Dim SaveTaskResult As Aurora.Telematics.Data.ResponseOfi360Task = jobProxy.SaveTask(connSessionId, newJob.ID, newTask)
            Else
                RetMessage = "Job not Saved"
                Return RetMessage
            End If

            ' End Task Creation
            Return RetMessage

        End Function

        Function CreateCheckInJobForVehicle(vehGuid As String, bookingId As String, checkInDateTime As DateTime, connSessionId As Guid) As String
            Dim retmessage As String
            Dim attributeName As String = "ActiveJobID"
            Dim resValue As ResponseOfi360AttributeNameValue = vehicleProxy.GetVehicleAttributeValue(connSessionId, New Guid(vehGuid), attributeName)
            Dim returnedJobId As Guid = New Guid(resValue.Value.Value)
            Dim existingJobResponce As ResponseOfi360Job = jobProxy.GetJob(connSessionId, returnedJobId)
            Dim existingJob As i360Job = existingJobResponce.Value

            Dim newTask As New i360Task
            Dim retVeh As i360Vehicle = vehSer.GetVehicleByGUID(vehGuid, connSessionId)

            newTask.AssignedTo = retVeh.Name
            newTask.Description = "CheckIn Task: " + DateTime.Now.ToShortTimeString
            newTask.Name = "CheckIn"
            newTask.StartDate = checkInDateTime
            newTask.DueDate = newTask.StartDate

            Dim myJobStatesResponse As ResponseOfListOfi360JobState = jobProxy.GetJobStates(connSessionId)
            For Each js As i360JobState In myJobStatesResponse.Value
                If js.Name.Equals("Completed") Then
                    newTask.StatusID = js.ID
                End If
            Next

            Dim SaveTaskResult As Aurora.Telematics.Data.ResponseOfi360Task = jobProxy.SaveTask(connSessionId, existingJob.ID, newTask)

            If SaveTaskResult.Code = ResponseCode.OK Then
                retmessage = "Task Saved Successfully"
                existingJob.DueDate = newTask.DueDate.AddMinutes(1)
                existingJob.StatusID = newTask.StatusID
                Dim SaveJobResult As Aurora.Telematics.Data.ResponseOfi360Job = jobProxy.SaveJob(connSessionId, existingJob)

                retmessage = vehSer.SetVehicleAttribute("ActiveJobID", "", retVeh, connSessionId)

            Else
            retmessage = "Task was not saved for booking Id : " + bookingId
            End If

            Return retmessage
        End Function

    End Class
End Namespace
