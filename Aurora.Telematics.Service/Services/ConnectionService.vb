﻿
Imports System.Collections.Generic
Imports System.Text
Imports Aurora.Telematics.Data
Imports Aurora.Common.Logging
Imports Aurora.Common

Namespace Services
    Public Class ConnectionService
        Private connectProxy As New ConnectAPI()
        Public Function Login(userId As String, userPwd As String) As Guid
            Dim retValue As Guid = Guid.Empty
            Try
                Dim Session = connectProxy.Login(userId, userPwd)
                If Session.Code <> ResponseCode.OK Then
                    Aurora.Common.Logging.LogInformation("ConnectionService.Login", "Unable to Logon with credentials, check your config file For User[" + userId + "]")
                Else
                    retValue = Session.Value.SessionID
                End If
            Catch ex As Exception
                Dim retString As String = String.Empty
                Aurora.Common.Logging.LogInformation("ConnectionService.Login", "Unable to Logon [" + connectProxy.Url + "] - Message: " + ex.Message)
            End Try
            Return retValue

        End Function
    End Class
End Namespace
