﻿Imports System.Collections.Generic
Imports System.Text
Imports Aurora.Telematics.Data
Imports Aurora.Common.Logging
Imports System.Text.RegularExpressions

Namespace Services

    Public Class VehicleService
        Private vehicleProxy As New VehicleAPI()
        Public Function GetVehicleId(vehRego As String) As Guid
            Throw New NotImplementedException()
        End Function

        Public Function IsAttrubutePresent(p As String, vehId As Guid) As Boolean
            Throw New NotImplementedException()
        End Function

        Public Sub AddAttribute(p1 As String, p2 As String, vehId As Guid)
            Throw New NotImplementedException()
        End Sub



        Public Function GetVehicleList(ConnectedSessionID As Guid) As List(Of i360Vehicle)
            Dim retList As New List(Of i360Vehicle)()
            Try
                Dim myVehicleList As ResponseOfListOfi360Vehicle = vehicleProxy.GetVehicleList(ConnectedSessionID)
                For i As Integer = 0 To myVehicleList.Value.Length - 1
                    retList.Add(myVehicleList.Value(i))
                Next
                If retList.Count = 0 Then
                    Aurora.Common.Logging.LogInformation("GetVehicleList", "Vehicle List contains 0 vehicles")
                End If
            Catch ex As Exception
                Aurora.Common.Logging.LogInformation("GetVehicleList", "Vehicle List at [" + vehicleProxy.Url + "] returned error, Message = " + ex.Message)
            End Try
            Return retList
        End Function

        Public Function GetVehicleByDeviceId(deviceName As String, ConnectedSessionID As Guid) As i360Vehicle
            Dim retVehicle As i360Vehicle = Nothing
            Dim attachedVehName As String = [String].Empty
            Dim devSer As New DeviceService()
            Dim devList = devSer.GetDeviceList(ConnectedSessionID)
            Dim vehList = GetVehicleList(ConnectedSessionID)
            Dim vehName As String = String.Empty
            Dim ImardaDeviceName As String = GetImardaDeviceName(deviceName)
            Dim rex As New Regex("^V30\d-")
            For Each dev As i360Device In devList
                'Updated by Nimesh on 18/06/2015 - Ref - https://thlonline.atlassian.net/browse/TELMAT-13
                ' ◾Current information within Aurora has not followed pre-requirements above therefore device IDs have been added using the full version instead of the short version.
                '   The Telematics Track ID is the short device number ie 200301, but in Aurora we have the full version V301-00200301. 

                'if (dev.TrackID.Equals(deviceName))           
                '    attachedVehName = dev.AttachedVehicle;
                Dim I360TrackID As String = GetImardaDeviceName(dev.TrackID)
                'if (rex.IsMatch(dev.TrackID))
                'if (dev.TrackID.Equals(ImardaDeviceName))
                If I360TrackID.Equals(ImardaDeviceName) Then
                    attachedVehName = dev.AttachedVehicle
                    Exit For
                    'End - Updated by Nimesh on 18/06/2015 - Ref - https://thlonline.atlassian.net/browse/TELMAT-13
                End If
            Next
            For Each veh As i360Vehicle In vehList
                If veh.Name.Equals(attachedVehName) Then
                    retVehicle = veh
                End If
            Next
            If retVehicle Is Nothing Then
                Aurora.Common.Logging.LogInformation("VehicleServices.GetVehicleByDeviceID", (Convert.ToString("Vehicle with device id [") & deviceName) + "] is not present in Imarda.")
            End If
            Return retVehicle

        End Function

        Private Function GetImardaDeviceName(deviceName As String) As String
            Dim retDeviceName As String = String.Empty
            'if (deviceName.Contains("-"))
            '{
            '    retDeviceName = deviceName.Split("-".ToCharArray())[1];
            '}
            'else
            '    retDeviceName = deviceName;
            Dim rex As New Regex("^V30\d-")

            'bool result = rex.IsMatch("V301-000263748");

            Dim strResult As String = rex.Replace(deviceName, "")
            Dim xyz As Integer = 0
            retDeviceName = If(Integer.TryParse(strResult.Replace("-", ""), xyz), xyz.ToString(), strResult)
            ' int.Parse(strResult.Replace("-", "")).ToString();
            Return retDeviceName
        End Function
        Private Function IsAttributePresent(attributeName As String, VehicleID As Guid, ConnectedSessionID As Guid) As Boolean

            Dim retValue As Boolean = False
            Dim attList As ResponseOfListOfi360Attribute = vehicleProxy.GetVehicleAttributeList(ConnectedSessionID, i360AttributeKind.User)

            For Each att As i360Attribute In attList.Value
                If att.Name.Equals(attributeName) Then
                    'If att.Type <> i360AttributeType.Text Then
                    '    ' vehicleProxy.AddVehicleAttribute(ConnectedSessionID, newAttribute)

                    '    att.Type = i360AttributeType.Text
                    'End If

                    retValue = True
                    Exit For
                End If
            Next
            ' Dim xx = attList.Value.Where(Function(p) p.Name.Equals(attributeName))

            'If xx.Count() > 0 Then
            '    retValue = True
            'End If

            Return retValue


        End Function

        Public Sub RemoveVehicleAttribute(attributeName As String, ConnectedSessionID As Guid)
            'vehicleProxy.attr
        End Sub
        Public Function SetVehicleAttribute(attributeName As String, value As String, veh As i360Vehicle, ConnectedSessionID As Guid) As String
            'var veh = GetVehicleByDeviceId(DeviceName, ConnectedSessionID);
            Dim isPresent As Boolean = IsAttributePresent(attributeName, veh.ID, ConnectedSessionID)
            Dim retMessage As String = String.Empty
            If Not isPresent Then
                Dim newAttribute = New i360Attribute()
                newAttribute.Name = attributeName
                newAttribute.Type = i360AttributeType.Text
                newAttribute.Description = attributeName
                Dim AddResponse As ResponseOfBoolean = vehicleProxy.AddVehicleAttribute(ConnectedSessionID, newAttribute)
            End If
            Dim attributeNameValue = New i360AttributeNameValue()
            attributeNameValue.Name = attributeName

            attributeNameValue.Value = value

            Dim SaveResponse As ResponseOfBoolean = vehicleProxy.SaveVehicleAttributeValue(ConnectedSessionID, veh.ID, attributeNameValue)
            If SaveResponse.Code = ResponseCode.OK Then
                retMessage = (attributeName & Convert.ToString(" for vehicle with unit no ")) + veh.Name + " changed"
            Else
                retMessage = SaveResponse.Message
            End If
            Aurora.Common.Logging.LogInformation("VehicleService.SetVehicleAttribute", retMessage)
            Return retMessage
        End Function
        Public Sub SetVehicleAttributeByDeviceName(attributeName As String, value As String, DeviceName As String, ConnectedSessionID As Guid)
            Dim veh = GetVehicleByDeviceId(DeviceName, ConnectedSessionID)
            Dim isPresent As Boolean = IsAttributePresent(attributeName, veh.ID, ConnectedSessionID)
            If Not isPresent Then
                Dim newAttribute = New i360Attribute()
                newAttribute.Name = attributeName
                newAttribute.Type = i360AttributeType.Text
                newAttribute.Description = "THL Booking ID"
                vehicleProxy.AddVehicleAttribute(ConnectedSessionID, newAttribute)
            End If
            Dim attributeNameValue = New i360AttributeNameValue()
            attributeNameValue.Name = attributeName
            attributeNameValue.Value = value

            vehicleProxy.SaveVehicleAttributeValue(ConnectedSessionID, veh.ID, attributeNameValue)
            Aurora.Common.Logging.LogInformation("VehicleService.SetVehicleAttribute", (Convert.ToString(attributeName & Convert.ToString(" for Device ")) & DeviceName) + " changed")

        End Sub

        Public Function GetVehicleByGUID(vehicleGUID As String, ConnectedSessionID As Guid) As i360Vehicle
            Dim retVehicle As i360Vehicle = Nothing
            Dim attachedVehName As String = [String].Empty
            Try
                retVehicle = vehicleProxy.GetVehicle(ConnectedSessionID, New Guid(vehicleGUID)).Value
            Catch ex As Exception
                Aurora.Common.Logging.LogInformation("VehicleService.GetVehicleByGuid", "Error occured at [" + vehicleProxy.Url + "] with vehicleguid [" + vehicleGUID + "], Message=" + ex.Message)
            End Try

            '
            'retVehicle = vehicleProxy.GetVehicle(ConnectedSessionID, new Guid(vehicleGUID));
            If retVehicle Is Nothing Then
                Aurora.Common.Logging.LogInformation("GetVehicleByGUID", (Convert.ToString("Vehicle not found with GUID :[") & vehicleGUID) + "]")
            End If
            Return retVehicle
        End Function
        Function UpdateVehicleByGUID(TelematicsGUID As String, newBookingID As String, ConnectedSessionID As Guid) As String
            Dim vehByGUID As i360Vehicle = GetVehicleByGUID(TelematicsGUID, ConnectedSessionID)
            Dim retMessage As String = String.Empty
            If vehByGUID Is Nothing Then
                'message = "No vehicle Found with GUID:" + txtGUID.Text
                retMessage = "No vehicle Found with GUID:" + TelematicsGUID
                Aurora.Common.Logging.LogInformation("VehicleService.UpdateVehicleByGUID", retMessage)
            Else
                retMessage = SetVehicleAttribute("BookingID", newBookingID, vehByGUID, ConnectedSessionID)
               
                'retMessage = "BookingID for unit number " + vehByGUID.Name + " changed to " + newBookingID
                'message = "Booking Number for unit number " + vehByGUID.Name + " changed to " + newBookingId
                Aurora.Common.Logging.LogInformation("VehicleService.UpdateVehicleByGUID", retMessage)



            End If
            Return retMessage

        End Function






    End Class


End Namespace
