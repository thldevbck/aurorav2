﻿
Imports System.Collections.Generic
Imports System.Text
Imports Aurora.Telematics.Data
Imports Aurora.Common.Logging

Namespace Services

    Public Class DeviceService
        Private deviceProxy As New DeviceAPI()



        Friend Function GetDeviceList(ConnectedSessionID As Guid) As List(Of i360Device)
            Dim retList As New List(Of i360Device)()
            Try
                Dim myDeviceList As ResponseOfListOfi360Device = deviceProxy.GetDeviceList(ConnectedSessionID)
                'myDeviceList.Value.GetLength()

                For i As Integer = 0 To myDeviceList.Value.Length - 1
                    retList.Add(myDeviceList.Value(i))
                Next
                If retList.Count = 0 Then
                    Aurora.Common.Logging.LogInformation("DeviceService.GetDeviceList", "Device List returned no records")
                End If

            Catch ex As Exception
                Aurora.Common.Logging.LogInformation("DeviceService.GetDeviceList", "Device List at [" + deviceProxy.Url + "] returned at no records Message: " + ex.Message)
            End Try
            Return retList
        End Function
    End Class
End Namespace



