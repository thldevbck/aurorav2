﻿
Imports System.Collections.Generic
Imports System.Text
Imports Aurora.Telematics.Data
Imports Aurora.Common.Logging
Imports Aurora.Common
Imports System.Data
Imports System.Data.SqlClient


Imports System.Data.Common

Namespace Services

    Public Class FleetService

        Public Function UpdateFleetGUIDs(ByVal connectedSessionId As Guid) As Boolean
            Dim retValue As Boolean = False

            Dim vehSer As VehicleService = New VehicleService
            Dim _dt As DataTable
            Dim retMessage As String
            Dim message As String = String.Empty
            ' create data table to insert items
            _dt = New DataTable("FleetAssetGUIDs")
            _dt.Columns.Add("UnitNumber", GetType(Integer))
            _dt.Columns.Add("TelematicsGUID", GetType(String))


            If connectedSessionId.Equals(Guid.Empty) Then
                'LoggingService.LogError("UpdateVehicleBookingId", "Unsuccessful Login");
                retMessage = "Your Login Was Unsuccessful"
            Else
                message = "Your Login was successful"
                Try
                    Dim I360VehList As List(Of i360Vehicle) = vehSer.GetVehicleList(connectedSessionId)
                    If I360VehList.Count > 0 Then
                        For Each curVeh As i360Vehicle In I360VehList
                            Dim curVehId As Guid = curVeh.ID
                            Dim curVehName As String = curVeh.Name
                            If IsNumeric(curVehName) Then _dt.Rows.Add(curVehName, curVehId)
                        Next
                        retValue = True

                        Dim FleetRep As FleetRepo = New FleetRepo
                        FleetRep.UpdateFleetsGUIDs(_dt)
                    Else
                        Aurora.Common.Logging.LogInformation("FleetService.UpdateFleetGuids", "Device List returned no records so database wont be updated.")
                    End If

                Catch ex As Exception
                    Aurora.Common.Logging.LogInformation("FleetService.UpdateFleetGuids", "Device List returned at no records, Message: " + ex.Message)
                End Try

            End If

            

            Return retValue
        End Function

        Public Function DumpFleetGUIDsToFile(ByVal connectedSessionId As Guid, filePath As String, delimiter As Char) As Boolean
            Dim retValue As Boolean = False

            Dim vehSer As VehicleService = New VehicleService
            Dim _dt As DataTable
            Dim retMessage As String
            Dim message As String = String.Empty
            ' create data table to insert items
            _dt = New DataTable("FleetAssetGUIDs")
            _dt.Columns.Add("UnitNumber", GetType(Integer))
            _dt.Columns.Add("TelematicsGUID", GetType(String))


            If connectedSessionId.Equals(Guid.Empty) Then
                'LoggingService.LogError("UpdateVehicleBookingId", "Unsuccessful Login");
                retMessage = "Your Login Was Unsuccessful"
            Else
                message = "Your Login was successful"
                Dim I360VehList As List(Of i360Vehicle) = vehSer.GetVehicleList(connectedSessionId)
                For Each curVeh As i360Vehicle In I360VehList
                    Dim curVehId As Guid = curVeh.ID
                    Dim curVehName As String = curVeh.Name
                    If IsNumeric(curVehName) Then _dt.Rows.Add(curVehName, curVehId)
                Next
            End If

            Dim FleetRep As FleetRepo = New FleetRepo
            'FleetRep.UpdateFleetsGUIDs(_dt)
            FleetRep.DumpToCSV(_dt, filePath, delimiter)
            retValue = True
            Return retValue
        End Function
        Public Function UpdateFleetGUID(ByVal connectedSessionId As Guid, ByVal unitNumber As String) As String
            Dim vehSer As VehicleService = New VehicleService
            Dim retMessage As String = String.Empty
            Dim message As String = String.Empty
            Aurora.Common.Logging.LogInformation("FleetService.UpdateFleetGUID", "Updating Guid for Unit Number " + unitNumber)
            Dim _dt As DataTable
            _dt = New DataTable("FleetAssetGUIDs")
            _dt.Columns.Add("UnitNumber", GetType(Integer))
            _dt.Columns.Add("TelematicsGUID", GetType(String))
            If connectedSessionId.Equals(Guid.Empty) Then
                'LoggingService.LogError("UpdateVehicleBookingId", "Unsuccessful Login");
                retMessage = "Your Login Was Unsuccessful"
                Aurora.Common.Logging.LogInformation("FleetService.UpdateFleetGUID", message)
            Else
                message = "Your Login was successful"
                Dim I360VehList As List(Of i360Vehicle) = vehSer.GetVehicleList(connectedSessionId)
                For Each curVeh As i360Vehicle In I360VehList
                    Dim curVehId As Guid = curVeh.ID
                    Dim curVehName As String = curVeh.Name
                    If IsNumeric(curVehName) AndAlso curVehName.Trim.Equals(unitNumber.Trim) Then
                        _dt.Rows.Add(curVehName, curVehId)
                        retMessage = "Updated unit number " + unitNumber + " with guid " + curVehId.ToString
                    End If

                Next
            End If

            If _dt.Rows.Count = 0 Then
                Aurora.Common.Logging.LogInformation("FleetService.UpdateFleetGUID", "No Vehicle found in Telematics with unit number passed")
                retMessage = "No Vehicle found in Telematics with unit number " + unitNumber
            Else

                Dim FleetRep As FleetRepo = New FleetRepo
                FleetRep.UpdateFleetsGUIDs(_dt)
                Aurora.Common.Logging.LogInformation("FleetService.UpdateFleetGUID", retMessage)
            End If

            Return retMessage

        End Function
    End Class



End Namespace
