﻿Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Common
Imports System.Data.SqlClient
Imports Aurora.Common.Data
Imports System.IO

Public Class DataRepository

    Private Const SP_GetCountries As String = "AURORA.dbo.FLEET_getCountries"
    Private Const SP_GetFleetModelCodes As String = "AURORA.dbo.FLEET_getFleetModelCodes"
    Private Const SP_GetFleetModelCodesAutoSuggest As String = "AURORA.dbo.Fleet_getFleetModelCodesAutoSuggest"
    Private Const SP_GetSingleFleetModelCode As String = "AURORA.dbo.Fleet_getSingleFleetModelCode"
    Private Const SP_InsertUpdateFleetModelCode As String = "AURORA.dbo.Fleet_InsertUpdateFleetModelCode"
    Private Const SP_LoadManufacturerModel As String = "AURORA.dbo.Fleet_getDropDownDataForManufacturerModel"
    Private Const SP_InsertUpdateManufacturerModel As String = "AURORA.dbo.Fleet_InsUpFleetManufacturerModel"
    Private Const SP_GetFleetModelDetails As String = "AURORA.dbo.Fleet_getFleetModelDetails"
    Private Const SP_GetManufacturerModeLCodeAutoSuggest As String = "AURORA.dbo.Fleet_GetManufacturerModeLCode"
    Private Const SP_GetManufacturerModelDescriptionAutoSuggest As String = "AURORA.dbo.Fleet_GetManufacturerModelDescription"
    Private Const SP_GetNumberRules As String = "AURORA.dbo.Fleet_getNumberRules"
    Private Const SP_LinkFleetModelManufacturerModel As String = "AURORA.dbo.Fleet_LinkFleetModelManufacturerModel"
    Private Const SP_GetManufacturer As String = "AURORA.dbo.Fleet_GetManufacturer"
    Private Const SP_InsUpNumberRules As String = "AURORA.dbo.Fleet_InsUpNumberRules"
    Private Const SP_DefaultCountry As String = "AURORA.dbo.Fleet_GetDefaultCountry"
    Private Const SP_GetunitNumberDetails As String = "AURORA.dbo.Fleet_getunitnumberdetails"
    Private Const SP_GetSingleUnitNumber As String = "AURORA.dbo.Fleet_getSingleUnitNumber"

    Private Const SP_Fleet_getDropDownDataForLoadVehiclePlan As String = "Fleet_getDropDownDataForLoadVehiclePlan"
    Private Const SP_Fleet_CreateVehiclePlan As String = "Fleet_CreateVehiclePlan"
    Private Const SP_Fleet_getUnitNumber As String = "Fleet_getUnitNumber"
    Private Const SP_Fleet_getScheduledChangeFleetModelRules As String = "Fleet_getScheduledChangeFleetModelRules"

    Private Const SP_Fleet_UpdateFleetDetails As String = "Fleet_UpdateFleetDetails"
    Private Const SP_Fleet_InsUpFleetModelChange As String = "Fleet_InsUpFleetModelChange"
    Private Const SP_Fleet_getLocationList As String = "Fleet_getLocationList"
    Private Const SP_CheckUser As String = "dialog_checkUserForCSR"

    'Fleet sale and disposal plan
    Private Const SP_Fleet_getVehicleSaleRecords As String = "Fleet_getVehicleSaleRecords"
    Private Const SP_Fleet_GetDropdownForSalePlan As String = "Fleet_GetDropdownForSalePlan"
    Private Const SP_Fleet_UpdateSaleRecord As String = "Fleet_UpdateSaleRecord"
    Private Const SP_Fleet_CreateSalePlan As String = "Fleet_CreateSalePlan"
    Private Const SP_Fleet_CreateSalPlanLine As String = "Fleet_CreateSalPlanLine"
    Private Const SP_Fleet_DeleteSalePlanRecord As String = "Fleet_DeleteSalePlanRecord"
    Private Const SP_Fleet_UpdateSalesPlan As String = "Fleet_UpdateSalesPlan"
    Private Const SP_Fleet_GetProcessedSteps As String = "Fleet_GetProcessedSteps"
    Private Const SP_Fleet_SaveProcessSteps As String = "Fleet_SaveProcessSteps"
    Private Const SP_Fleet_SaveProcessStepsDate As String = "Fleet_SaveProcessStepsDate"

    'Fleet Activity
    Private Const SP_Fleet_getFleetActivityHistory As String = "Fleet_getFleetActivityHistory"
    Private Const SP_Fleet_GetDropDownForFleetActivity As String = "Fleet_getDropDownForFleetActivity"
    Private Const SP_Fleet_CreateNewActivity As String = "Fleet_CreateNewActivity"
    Private Const SP_Fleet_UpdateActivity As String = "Fleet_UpdateActivity"

    Private Const SP_DefaultCountryId As String = "AURORA.dbo.Fleet_GetDefaultCountryId"

    Private Shared sbLog As StringBuilder

    Public Shared Function CheckUserRole(usercode As String, role As String) As XmlDocument
        sbLog = New StringBuilder().AppendFormat("Parameters{0}usercode: {1}{2}", vbCrLf, usercode, vbCrLf)
        sbLog.AppendFormat("role: {0}{1}", role, vbCrLf)
        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("dialog_checkUserForCSR", usercode, role)
        Logging.LogDebug("CheckUserRole", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        Dim xml As New XmlDocument()
        xml.LoadXml(result)
        sbLog = Nothing
        Return xml
    End Function

    ''rev:mia 2oct2014 - more generic aims role.. Kcenia's bday yesteday
    Public Shared Function CheckGenericUserRole(usercode As String, RoleMappingActualPage As String) As String
        sbLog = New StringBuilder().AppendFormat("Parameters{0}usercode: {1}{2}", vbCrLf, usercode, vbCrLf)
        sbLog.AppendFormat("RoleMappingActualPage: {0}{1}", RoleMappingActualPage, vbCrLf)
        Dim params(2) As Aurora.Common.Data.Parameter
        Dim retvalue As String

        Try
            params(0) = New Aurora.Common.Data.Parameter("userCode", DbType.String, usercode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("RoleMappingActualPage", DbType.String, RoleMappingActualPage, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("result", DbType.String, 12, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("Admin_GetRoleMappingData", params)
            retvalue = params(2).Value
            If (String.IsNullOrEmpty(retvalue)) Then
                retvalue = "AimsViewOnly"
            End If


            sbLog.AppendFormat("retvalue: {0}{1}", RoleMappingActualPage, vbCrLf)

        Catch ex As Exception
            sbLog.AppendFormat("error: {0}{1}", String.Concat(ex.Message, " - ", ex.StackTrace), vbCrLf)
            Logging.LogError("CheckGenericUserRole()".ToUpper, sbLog.ToString())
            Return "AimsViewOnly"
        End Try

        Logging.LogDebug("CheckGenericUserRole", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return retvalue
    End Function

    Public Shared Function GetSingleUnitNumber(fleetAssetId As Integer) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}fleetAssetId: {1}{2}", vbCrLf, fleetAssetId, vbCrLf)
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetSingleUnitNumber, result, fleetAssetId)
        result.Tables(0).TableName = "FleetModelCode"
        Logging.LogDebug("GetSingleUnitNumber", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function GetUnitNumberDetails(fleetAssetId As Integer) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}fleetAssetId: {1}{2}", vbCrLf, fleetAssetId, vbCrLf)
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetunitNumberDetails, result, fleetAssetId)
        If (result.Tables.Count - 1 = 10) Then
            result.Tables(0).TableName = "FleetModeLCode"
            result.Tables(1).TableName = "VehicleNotes"
            result.Tables(2).TableName = "ManufacturerEtc" ''date,location
            result.Tables(3).TableName = "Manufacturers"
            result.Tables(4).TableName = "ManufacturerModels"
            result.Tables(5).TableName = "Countries"
            result.Tables(6).TableName = "ExteriorInteriors"
            result.Tables(7).TableName = "Fuels"
            result.Tables(8).TableName = "Transmission"
            result.Tables(9).TableName = "ServiceSchedule"
            result.Tables(10).TableName = "History"

        End If
        Logging.LogDebug("GetunitNumberDetails", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function


    Public Shared Function GetDefaultCountry(UserCode As String) As String
        sbLog = New StringBuilder().AppendFormat("Parameters{0}Usercode: {1}{2}", vbCrLf, UserCode, vbCrLf)
        Dim returnvalue As String = Aurora.Common.Data.ExecuteScalarSP(SP_DefaultCountry, UserCode)
        Logging.LogDebug("GetDefaultCountry", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return returnvalue
    End Function


    Public Shared Function InsUpNumberRules(FleetNumberId As Integer, CountryId As Integer, FleetModelId As Integer, ManufacturerId As Integer, BeginAssetNum As Integer, EndAssetNum As Integer, UserCode As String) As String
        Dim output As Integer = -1
        Dim returnvalue As String = ""

        sbLog = New StringBuilder().AppendFormat("Parameters{0}FleetNumberId: {1}{2}", vbCrLf, FleetNumberId.ToString, vbCrLf)
        sbLog.AppendFormat("CountryId: {0}{1}", CountryId.ToString, vbCrLf)
        sbLog.AppendFormat("FleetModelId: {0}{1}", FleetModelId.ToString, vbCrLf)
        sbLog.AppendFormat("ManufacturerId: {0}{1}", ManufacturerId.ToString, vbCrLf)
        sbLog.AppendFormat("BeginAssetNum: {0}{1}", BeginAssetNum.ToString, vbCrLf)
        sbLog.AppendFormat("EndAssetNum: {0}{1}", EndAssetNum.ToString, vbCrLf)
        sbLog.AppendFormat("UserCode: {0}{1}", UserCode, vbCrLf)



        Using transaction As New DatabaseTransaction()
            Try
                returnvalue = Aurora.Common.Data.ExecuteScalarSP(SP_InsUpNumberRules, _
                                                                       FleetNumberId, _
                                                                       CountryId, _
                                                                       FleetModelId, _
                                                                       ManufacturerId, _
                                                                       BeginAssetNum, _
                                                                       EndAssetNum, _
                                                                       UserCode)
                transaction.CommitTransaction()
                sbLog.AppendFormat("RESULT: OK{0}", vbCrLf)
            Catch ex As Exception
                transaction.RollbackTransaction()
                returnvalue = String.Concat("ERROR: ", ex.Message)
                sbLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
            End Try

        End Using
        Logging.LogDebug("InsUpNumberRules", sbLog.ToString)
        sbLog = Nothing
        Return returnvalue
    End Function

    Public Shared Function GetManufacturerModeLCodeAutoSuggest(ManufacturerModeLCode As String, CountryId As Integer, ManufacturerId As Integer) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}ManufacturerModeLCode: {1}{2}", vbCrLf, ManufacturerModeLCode, vbCrLf)
        sbLog.AppendFormat("CountryId: {0}{1}", CountryId.ToString, vbCrLf)
        sbLog.AppendFormat("ManufacturerId: {0}{1}", ManufacturerId.ToString, vbCrLf)

        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetManufacturerModeLCodeAutoSuggest, result, ManufacturerModeLCode, CountryId, ManufacturerId)
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "ManufacturerModeLCode"
        End If
        Logging.LogDebug("GetManufacturerModeLCodeAutoSuggest", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function GetManufacturerModelDescriptionAutoSuggest(ManufacturerModeL As String) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}ManufacturerModeL: {1}{2}", vbCrLf, ManufacturerModeL, vbCrLf)

        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetManufacturerModelDescriptionAutoSuggest, result, ManufacturerModeL)
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "ManufacturerModeLCode"
        End If

        Logging.LogDebug("GetManufacturerModelDescriptionAutoSuggest", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function GetCountries() As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}NO PARAMETER{1}", vbCrLf, vbCrLf)
        Dim result As New DataSet
        result = Aurora.Common.Data.ExecuteOutputDatasetSP(SP_GetCountries, {})
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "Countries"
        End If
        Logging.LogDebug("GetCountries", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result

    End Function

    Public Shared Function GetFleetModelCodes() As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}NO PARAMETER{1}", vbCrLf, vbCrLf)
        Dim result As New DataSet
        result = Aurora.Common.Data.ExecuteOutputDatasetSP(SP_GetFleetModelCodes, {})
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "FleetModels"
        End If

        Logging.LogDebug("GetFleetModelCodes", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function GetFleetModelCodesAutoSuggest(FleetModelCode As String, iCountryId As Integer) As DataSet

        sbLog = New StringBuilder().AppendFormat("Parameters{0}FleetModelCode: {1}{2}", vbCrLf, FleetModelCode, vbCrLf)
        sbLog.AppendFormat("CountryId: {0}{1}", iCountryId.ToString, vbCrLf)
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetFleetModelCodesAutoSuggest, result, FleetModelCode, iCountryId)
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "FleetModels"
        End If

        Logging.LogDebug("GetFleetModelCodesAutoSuggest", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function GetManufacturer(manufacturer As String) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}manufacturer: {1}{2}", vbCrLf, manufacturer, vbCrLf)
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetManufacturer, result, manufacturer)
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "ManufacturerNames"
        End If
        Logging.LogDebug("GetManufacturer", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function GetSingleFleetModelCode(FleetModelCode As String, countryid As Integer) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}FleetModelCode: {1}{2}", vbCrLf, FleetModelCode, vbCrLf)
        ''rev:mia 18-Oct-2016 Issues recoding NZ Fleet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetSingleFleetModelCode, result, FleetModelCode, countryid)
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "FleetModels"
        End If
        Logging.LogDebug("GetSingleFleetModelCode", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function InsertUpdateFleetModelCode(iFleetModelId As Integer, _
                                                       sFleetModelCode As String, _
                                                       sFleetModelDescription As String, _
                                                       bConversionIndicator As Boolean, _
                                                       mDailyRunningCost As Decimal, _
                                                       mDailyDelayCost As Decimal, _
                                                       bIsLocal As Boolean, _
                                                       sUserCode As String, _
                                                       Optional InactiveDate As String = "") As String

        sbLog = New StringBuilder().AppendFormat("Parameters{0}iFleetModelId: {1}{2}", vbCrLf, iFleetModelId.ToString, vbCrLf)
        sbLog.AppendFormat("sFleetModelCode: {0}{1}", sFleetModelCode, vbCrLf)
        sbLog.AppendFormat("sFleetModelDescription: {0}{1}", sFleetModelDescription, vbCrLf)
        sbLog.AppendFormat("bConversionIndicator: {0}{1}", bConversionIndicator.ToString, vbCrLf)
        sbLog.AppendFormat("mDailyRunningCost: {0}{1}", mDailyRunningCost.ToString, vbCrLf)
        sbLog.AppendFormat("mDailyDelayCost: {0}{1}", mDailyDelayCost.ToString, vbCrLf)
        sbLog.AppendFormat("bIsLocal: {0}{1}", bIsLocal, vbCrLf)
        sbLog.AppendFormat("UserCode: {0}{1}", sUserCode, vbCrLf)
        sbLog.AppendFormat("InactiveDate: {0}{1}", InactiveDate, vbCrLf)

        Dim inactiveDateToPass As Nullable(Of DateTime) = Nothing

        If (Not String.IsNullOrEmpty(InactiveDate)) Then
            inactiveDateToPass = Convert.ToDateTime(InactiveDate).ToShortDateString()
        End If


        Dim output As Integer = -1
        Dim returnvalue As String = ""
        Using transaction As New DatabaseTransaction()
            Try
                returnvalue = Aurora.Common.Data.ExecuteScalarSP(SP_InsertUpdateFleetModelCode, _
                                                                       iFleetModelId, _
                                                                       sFleetModelCode, _
                                                                       sFleetModelDescription, _
                                                                       bConversionIndicator, _
                                                                       mDailyRunningCost, _
                                                                       mDailyDelayCost, _
                                                                       bIsLocal, _
                                                                       sUserCode, _
                                                                       inactiveDateToPass, _
                                                                       output)
                transaction.CommitTransaction()
                sbLog.AppendFormat("RESULT: {0}{1}", "OK", vbCrLf)
            Catch ex As Exception
                transaction.RollbackTransaction()
                returnvalue = String.Concat("ERROR: ", ex.Message)
                sbLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
            End Try

        End Using

        Logging.LogDebug("InsertUpdateFleetModelCode", sbLog.ToString)
        sbLog = Nothing
        Return returnvalue
    End Function

    Public Shared Function LoadDataForManufacturerModel() As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}NO PARAMETER{1}", vbCrLf, vbCrLf)
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_LoadManufacturerModel, result, {})
        If (result.Tables.Count - 1 > 0) Then
            Dim ctr As Integer = -1
            For i = 0 To result.Tables.Count - 1
                Select Case i
                    Case 0
                        result.Tables(i).TableName = "Manufacturers"
                        Exit Select
                    Case 1
                        result.Tables(i).TableName = "ManufacturerModels"
                        Exit Select
                    Case 2
                        result.Tables(i).TableName = "Countries"
                        Exit Select
                    Case 3
                        result.Tables(i).TableName = "ExteriorInteriors"
                        Exit Select
                    Case 4
                        result.Tables(i).TableName = "Fuels"
                        Exit Select
                    Case 5
                        result.Tables(i).TableName = "Transmission"
                        Exit Select
                    Case 6
                        result.Tables(i).TableName = "ServiceSchedule"
                        Exit Select
                End Select

            Next
        End If

        Logging.LogDebug("LoadDataForManufacturerModel", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function InsertUpdateFleetManufacturerModel(iFleetModelId As Integer, _
                                                         iManufacturerID As Integer, _
                                                         iFleetManufacturerModelID As Integer, _
                                                         sManufacturerModelCode As String, _
                                                         iCountryId As Integer, _
                                                         sFuelType As String, _
                                                         sTransmission As String, _
                                                         sManufacturerModel As String, _
                                                         iServiceScheduleId As Decimal, _
                                                         mChassisCost As String, _
                                                         iChassisCostId As Integer, _
                                                         iRegionModelId As Integer, _
                                                         iFleetModelManufacturerModelId As Integer, _
                                                         iRegionId As Integer, _
                                                         UserCode As String) As String

        sbLog = New StringBuilder().AppendFormat("Parameters{0}iFleetModelId: {1}{2}", vbCrLf, iFleetModelId, vbCrLf)
        sbLog.AppendFormat("iManufacturerID: {0}{1}", iManufacturerID.ToString, vbCrLf)
        sbLog.AppendFormat("iFleetManufacturerModelID: {0}{1}", iFleetManufacturerModelID.ToString, vbCrLf)
        sbLog.AppendFormat("sManufacturerModelCode: {0}{1}", sManufacturerModelCode, vbCrLf)
        sbLog.AppendFormat("iCountryId: {0}{1}", iCountryId.ToString, vbCrLf)
        sbLog.AppendFormat("sFuelType: {0}{1}", sFuelType, vbCrLf)
        sbLog.AppendFormat("sTransmission: {0}{1}", sTransmission, vbCrLf)
        sbLog.AppendFormat("sManufacturerModel: {0}{1}", sManufacturerModel, vbCrLf)
        sbLog.AppendFormat("iServiceScheduleId: {0}{1}", iServiceScheduleId.ToString, vbCrLf)
        sbLog.AppendFormat("mChassisCost: {0}{1}", mChassisCost.ToString, vbCrLf)
        sbLog.AppendFormat("iRegionModelId: {0}{1}", iRegionModelId.ToString, vbCrLf)
        sbLog.AppendFormat("iFleetModelManufacturerModelId: {0}{1}", iFleetModelManufacturerModelId.ToString, vbCrLf)
        sbLog.AppendFormat("iRegionId: {0}{1}", iRegionId.ToString, vbCrLf)
        sbLog.AppendFormat("UserCode: {0}{1}", UserCode, vbCrLf)


        Dim returnvalue As String
        Using transaction As New DatabaseTransaction()
            Try
                returnvalue = Aurora.Common.Data.ExecuteScalarSP(SP_InsertUpdateManufacturerModel, _
                                                                           iFleetModelId, _
                                                                           iManufacturerID, _
                                                                           iFleetManufacturerModelID, _
                                                                           sManufacturerModelCode, _
                                                                           iCountryId, _
                                                                           sFuelType, _
                                                                           sTransmission, _
                                                                           sManufacturerModel, _
                                                                           iServiceScheduleId, _
                                                                           mChassisCost, _
                                                                           iChassisCostId, _
                                                                           iRegionModelId, _
                                                                           iFleetModelManufacturerModelId, _
                                                                           iRegionId, _
                                                                           UserCode)
                transaction.CommitTransaction()
                returnvalue = "OK"

                sbLog.AppendFormat("RESULT: {0}{1}", "OK", vbCrLf)
            Catch ex As Exception
                returnvalue = String.Concat("ERROR: ", ex.Message)
                transaction.RollbackTransaction()
                sbLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
            End Try

        End Using
        Logging.LogDebug("InsertUpdateFleetManufacturerModel", sbLog.ToString)
        sbLog = Nothing

        Return returnvalue



    End Function

    Public Shared Function GetFleetModelDetails(fleetModelId As Integer, countryId As Integer) As DataSet

        sbLog = New StringBuilder().AppendFormat("Parameters{0}fleetModelId: {1}{2}", vbCrLf, fleetModelId, vbCrLf)
        sbLog.AppendFormat("CountryId: {0}{1}", countryId, vbCrLf)

        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP(SP_GetFleetModelDetails, result, fleetModelId, countryId)

        If (result.Tables.Count - 1 > 0) Then
            Dim ctr As Integer = -1
            For i = 0 To result.Tables.Count - 1
                Select Case i
                    Case 0
                        result.Tables(i).TableName = "FleetModelDetails"
                        Exit Select
                    Case 1
                        result.Tables(i).TableName = "ManufacturerModels"
                        Exit Select
                    Case 2
                        result.Tables(i).TableName = "AvailableModels"
                        Exit Select
                    Case 3
                        result.Tables(i).TableName = "AllocatedUnitNumbers"
                        Exit Select
                    Case 4
                        result.Tables(i).TableName = "VehiclePlans"
                        Exit Select
                    Case 5
                        result.Tables(i).TableName = "Regions"
                        Exit Select

                End Select
            Next
        End If
        Logging.LogDebug("GetFleetModelDetails", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing

        Return result
    End Function

    Public Shared Function GetNumberRules() As DataSet

        sbLog = New StringBuilder().AppendFormat("Parameters{0}NO PARAMETER{1}", vbCrLf, vbCrLf)
        Dim result As New DataSet
        result = Aurora.Common.Data.ExecuteOutputDatasetSP(SP_GetNumberRules, {})
        If (result.Tables.Count - 1 <> -1) Then
            result.Tables(result.Tables.Count - 1).TableName = "NumberRules"
        End If

        Logging.LogDebug("GetNumberRules", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return result
    End Function

    Public Shared Function LinkFleetModelManufacturerModel(ManufacturerModelID As Integer, FleetModelId As Integer, usercode As String) As String

        sbLog = New StringBuilder().AppendFormat("Parameters{0}ManufacturerModelID: {1}{2}", vbCrLf, ManufacturerModelID.ToString, vbCrLf)
        sbLog.AppendFormat("FleetModelId: {0}{1}", FleetModelId, vbCrLf)
        sbLog.AppendFormat("Usercode: {0}{1}", usercode, vbCrLf)


        Dim output As Integer = 0
        Dim returnvalue As String = "OK"

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_LinkFleetModelManufacturerModel, _
                                                                   ManufacturerModelID, _
                                                                   FleetModelId, _
                                                                   usercode)

            sbLog.AppendFormat("RESULT: {0}{1}", "OK", vbCrLf)

        Catch ex As Exception
            returnvalue = String.Concat("ERROR: ", ex.Message)
            sbLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("LinkFleetModelManufacturerModel", sbLog.ToString)
        sbLog = Nothing

        Return returnvalue
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params">double iFleetNumberId, double iCountryId</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetDropDownDataForLoadVehiclePlan(ByVal ParamArray params() As Object) As DataSet
        Dim ds As New DataSet

        Dim result As DataSet

        Dim _Log As StringBuilder

        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iFleetNumberId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        If Not (params(1) Is Nothing) Then
            _Log.AppendFormat("iCountryId: {0}{1}", params(1).ToString, vbCrLf)
        End If

        Try
            result = Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_getDropDownDataForLoadVehiclePlan, ds, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = Nothing
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GetDropDownDataForLoadVehiclePlan", _Log.ToString)

        Return result
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params"> iCountryId,//@iCountryId					BIGINT,
    '''           int.Parse(lblFleetModelId.Text),//@iFleetModelId				BIGINT,
    '''           int.Parse(lblManufacturerId.Text),//@iManufacturerId			BIGINT,
    '''           ddlManufacturerModel.SelectedValue,//@iManufacturerModelId		BIGINT,
    '''           int.Parse(txbRequestQuantity.Text),//@iFleetRequisitionQuantity	BIGINT,
    '''           dateDeliveryFromDate.Date,//@dLTDealerDeliveryFromDate	DATETIME,
    '''           txbComplianceYear.Text,//@sCompliancePlateYear		VARCHAR(10),
    '''           ddlDealer.SelectedValue,//@iDealerId					BIGINT,
    '''           ddlRegion.SelectedValue, //@iRegionId					BIGINT,
    '''          ddlFleetLocation.SelectedValue, //@sPurLocationOnFleet		VARCHAR(12),
    '''          int.Parse(txbHowMany.Text), //@iLTRateDelivered			BIGINT,
    '''           ddlLeadTimeFrequency.SelectedValue,//@sLTRateDeliveryFrequency	VARCHAR(12),
    '''           dateOnFleetDate.Date, //@dLT1stOnFleetDate			DATETIME,
    '''           UserCode//@sUserCode		VARCHAR(64)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function CreateVehiclePlan(ByVal ParamArray params() As Object) As Object
        Dim result As Object
        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        If Not (params(1) Is Nothing) Then
            _Log.AppendFormat("iFleetModelId: {0}{1}", params(1).ToString, vbCrLf)
        End If

        If Not (params(2) Is Nothing) Then
            _Log.AppendFormat("iManufacturerId: {0}{1}", params(2).ToString, vbCrLf)
        End If

        If Not (params(3) Is Nothing) Then
            _Log.AppendFormat("iManufacturerModelId: {0}{1}", params(3).ToString, vbCrLf)
        End If

        If Not (params(4) Is Nothing) Then
            _Log.AppendFormat("iFleetRequisitionQuantity: {0}{1}", params(4).ToString, vbCrLf)
        End If

        If Not (params(5) Is Nothing) Then
            _Log.AppendFormat("dLTDealerDeliveryFromDate: {0}{1}", params(5).ToString, vbCrLf)
        End If

        If Not (params(6) Is Nothing) Then
            _Log.AppendFormat("sCompliancePlateYear: {0}{1}", params(6).ToString, vbCrLf)
        End If

        If Not (params(7) Is Nothing) Then
            _Log.AppendFormat("iDealerId: {0}{1}", params(7).ToString, vbCrLf)
        End If

        If Not (params(8) Is Nothing) Then
            _Log.AppendFormat("iRegionId: {0}{1}", params(8).ToString, vbCrLf)
        End If

        If Not (params(9) Is Nothing) Then
            _Log.AppendFormat("sPurLocationOnFleet: {0}{1}", params(9).ToString, vbCrLf)
        End If

        If Not (params(10) Is Nothing) Then
            _Log.AppendFormat("iLTRateDelivered: {0}{1}", params(10).ToString, vbCrLf)
        End If

        If Not (params(11) Is Nothing) Then
            _Log.AppendFormat("sLTRateDeliveryFrequency: {0}{1}", params(11).ToString, vbCrLf)
        End If

        If Not (params(12) Is Nothing) Then
            _Log.AppendFormat("dLT1stOnFleetDate: {0}{1}", params(12).ToString, vbCrLf)
        End If

        If Not (params(13) Is Nothing) Then
            _Log.AppendFormat("sUserCode: {0}{1}", params(13).ToString, vbCrLf)
        End If


        Using transaction As New DatabaseTransaction()


            Try
                result = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_CreateVehiclePlan, params)

                If result IsNot Nothing Then
                    If result.ToString().Contains("Error") Then
                        Throw New Exception(result.ToString())
                    End If
                End If

                transaction.CommitTransaction()
                _Log.AppendFormat("RESULT: OK{0}", vbCrLf)

            Catch ex As Exception
                result = "ERROR" + ex.ToString()
                transaction.RollbackTransaction()
                _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)

            End Try
        End Using

        Logging.LogDebug("CreateVehiclePlan", _Log.ToString)

        Return result

    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params">	@iRequitionId		BIGINT	=	NULL,
    '''	@iFleetModelId		BIGINT	=	NULL,
    '''	@iUnitFromNumber	BIGINT	=	NULL,
    '''	@iUnitToNumber		BIGINT	=	NULL
    '''</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetUnitNumber(ByVal ParamArray params() As Object) As DataSet
        Dim ds As New DataSet

        Dim result As DataSet

        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iRequitionId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        If Not (params(1) Is Nothing) Then
            _Log.AppendFormat("iFleetModelId: {0}{1}", params(1).ToString, vbCrLf)
        End If

        If Not (params(2) Is Nothing) Then

            _Log.AppendFormat("iUnitFromNumber: {0}{1}", params(2).ToString, vbCrLf)
        End If

        If Not (params(3) Is Nothing) Then
            _Log.AppendFormat("iUnitToNumber: {0}{1}", params(3).ToString, vbCrLf)
        End If

        If Not (params(4) Is Nothing) Then
            _Log.AppendFormat("RegNumber: {0}{1}", params(4).ToString, vbCrLf)
        End If

        Try
            result = Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_getUnitNumber, ds, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = Nothing
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GetUnitNumber", _Log.ToString)
        Return result
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params">	@iFleetModelId		BIGINT		=	NULL,
    '''	@iFromUnitNum		BIGINT		=	NULL,
    '''	@iToUnitNum			BIGINT		=	NULL
    '''</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetScheduledChangeFleetModelRules(ByVal ParamArray params() As Object) As DataSet
        Dim ds As New DataSet

        Dim result As DataSet

        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iFleetModelId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If
        If Not (params(1) Is Nothing) Then
            _Log.AppendFormat("iUnitFromNumber: {0}{1}", params(1).ToString, vbCrLf)
        End If
        If Not (params(2) Is Nothing) Then
            _Log.AppendFormat("iUnitFromNumber: {0}{1}", params(2).ToString, vbCrLf)
        End If
        If Not (params(3) Is Nothing) Then
            _Log.AppendFormat("UserCode: {0}{1}", params(3).ToString, vbCrLf)
        End If
        If Not (params(4) Is Nothing) Then
            _Log.AppendFormat("RegNumber: {0}{1}", params(4).ToString, vbCrLf)
        End If

        Try
            result = Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_getScheduledChangeFleetModelRules, ds, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = Nothing
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)

            Throw New Exception(ex.Message)
        End Try

        Logging.LogDebug("GetScheduledChangeFleetModelRules", _Log.ToString)
        Return result
    End Function

    Public Shared Function GetFleetModelCodes(ByVal ParamArray params() As Object) As DataSet
        Dim ds As New DataSet

        Dim result As DataSet
        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iRequitionId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        Try
            result = Aurora.Common.Data.ExecuteDataSetSP(SP_GetFleetModelCodes, ds, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = Nothing
            Logging.LogError("GetFleetModelCodes", ex.Message & vbCrLf & ex.StackTrace)
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GetFleetModelCodes", _Log.ToString)
        Return result
    End Function

    ''' <summary>
    ''' ''rev:mia added march 13,2014 -added RUC Data
    ''' </summary>
    ''' <param name="params">	@iFleetAssetId				BIGINT,
    '''@dPurProposedDelivDate		DATETIME	= NULL,
    '''@dFirstOnFleetDate			DATETIME	= NULL,
    '''@sPurLocationOnFleet		VARCHAR(64)	= NULL,
    '''@dPurReceiptDate			DATETIME	= NULL,
    '''@sIdEngineNumber			varchar(6)	= NULL,
    '''@sIdChassisNumber			varchar(6)	= NULL,
    '''@sKeyNrIgnition				varchar(9)	= NULL,
    '''@sIdCompliancePlateYear		int			= NULL,
    '''@sIdCompliancePlateMonth	int			= NULL,
    '''@sRegistrationNumber		VARCHAR(64) = NULL,
    '''@dRegFirstDate				DATETIME	= NULL,
    '''@dRCCofEndDate				DATETIME	= NULL,
    '''@sUserCode					VARCHAR(64)	= NULL</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function UpdateFleetDetails(ByVal ParamArray params() As Object) As String
        Dim output As Integer = 0
        Dim returnvalue As String = "OK"
        Dim emptyMessage As String = "No value found"
        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        Else
            _Log.AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, emptyMessage, vbCrLf)
        End If

        If Not (params(1) Is Nothing) Then
            _Log.AppendFormat("dPurProposedDelivDate: {0}{1}", params(1).ToString, vbCrLf)
        Else
            _Log.AppendFormat("dPurProposedDelivDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(2) Is Nothing) Then
            _Log.AppendFormat("dFirstOnFleetDate: {0}{1}", params(2).ToString, vbCrLf)
        Else
            _Log.AppendFormat("dFirstOnFleetDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(3) Is Nothing) Then
            _Log.AppendFormat("sPurLocationOnFleet: {0}{1}", params(3).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sPurLocationOnFleet: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(4) Is Nothing) Then
            _Log.AppendFormat("dPurReceiptDate: {0}{1}", params(4).ToString, vbCrLf)
        Else
            _Log.AppendFormat("dPurReceiptDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(5) Is Nothing) Then
            _Log.AppendFormat("sIdEngineNumber: {0}{1}", params(5).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sIdEngineNumber: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(6) Is Nothing) Then
            _Log.AppendFormat("sIdChassisNumber: {0}{1}", params(6).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sIdChassisNumber: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(7) Is Nothing) Then
            _Log.AppendFormat("sKeyNrIgnition: {0}{1}", params(7).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sKeyNrIgnition: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(8) Is Nothing) Then
            _Log.AppendFormat("sIdCompliancePlateYear: {0}{1}", params(8).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sIdCompliancePlateYear: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(9) Is Nothing) Then
            _Log.AppendFormat("sIdCompliancePlateMonth: {0}{1}", params(9).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sIdCompliancePlateMonth: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(10) Is Nothing) Then
            _Log.AppendFormat("sRegistrationNumber: {0}{1}", params(10).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sRegistrationNumber: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(11) Is Nothing) Then
            _Log.AppendFormat("dRegFirstDate: {0}{1}", params(11).ToString, vbCrLf)
        Else
            _Log.AppendFormat("dRegFirstDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(12) Is Nothing) Then
            _Log.AppendFormat("dRCCofEndDate: {0}{1}", params(12).ToString, vbCrLf)
        Else
            _Log.AppendFormat("dRCCofEndDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(13) Is Nothing) Then
            _Log.AppendFormat("iRUCPaidToKms: {0}{1}", params(13).ToString, vbCrLf)
        Else
            _Log.AppendFormat("iRUCPaidToKms: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(14) Is Nothing) Then
            _Log.AppendFormat("sETag: {0}{1}", params(14).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sETag: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(15) Is Nothing) Then
            _Log.AppendFormat("sEPurb: {0}{1}", params(15).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sEPurb: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(16) Is Nothing) Then
            _Log.AppendFormat("sUserCode: {0}{1}", params(16).ToString, vbCrLf)
        Else
            _Log.AppendFormat("sUserCode: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(17) Is Nothing) Then
            _Log.AppendFormat("manufacturerName: {0}{1}", params(17).ToString, vbCrLf)
        Else
            _Log.AppendFormat("manufacturerName: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(18) Is Nothing) Then
            _Log.AppendFormat("manufacturerCode: {0}{1}", params(18).ToString, vbCrLf)
        Else
            _Log.AppendFormat("manufacturerCode: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(19) Is Nothing) Then
            _Log.AppendFormat("manufacturerFuelType: {0}{1}", params(19).ToString, vbCrLf)
        Else
            _Log.AppendFormat("manufacturerFuelType: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(20) Is Nothing) Then
            _Log.AppendFormat("manufacturerXmission: {0}{1}", params(20).ToString, vbCrLf)
        Else
            _Log.AppendFormat("manufacturerXmission: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(21) Is Nothing) Then
            _Log.AppendFormat("manufacturerServiceSchedule: {0}{1}", params(21).ToString, vbCrLf)
        Else
            _Log.AppendFormat("manufacturerServiceSchedule: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(22) Is Nothing) Then
            _Log.AppendFormat("RegistrationExpiryDate: {0}{1}", params(22).ToString, vbCrLf)
        Else
            _Log.AppendFormat("RegistrationExpiryDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(23) Is Nothing) Then
            _Log.AppendFormat("comments: {0}{1}", params(23).ToString, vbCrLf)
        Else
            _Log.AppendFormat("comments: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(24) Is Nothing) Then
            _Log.AppendFormat("WarrantyExpDate: {0}{1}", params(24).ToString, vbCrLf)
        Else
            _Log.AppendFormat("WarrantyExpDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(25) Is Nothing) Then
            _Log.AppendFormat("SelfContainExpDate: {0}{1}", params(25).ToString, vbCrLf)
        Else
            _Log.AppendFormat("SelfContainExpDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(26) Is Nothing) Then
            _Log.AppendFormat("TelematicsDevice: {0}{1}", params(26).ToString, vbCrLf)
        Else
            _Log.AppendFormat("TelematicsDevice: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(27) Is Nothing) Then
            _Log.AppendFormat("TelematicsTablet: {0}{1}", params(27).ToString, vbCrLf)
        Else
            _Log.AppendFormat("TelematicsTablet: {0}{1}", emptyMessage, vbCrLf)
        End If
        
        If Not (params(28) Is Nothing) Then
            _Log.AppendFormat("TelematicsInstallDate: {0}{1}", params(28).ToString, vbCrLf)
        Else
            _Log.AppendFormat("TelematicsInstallDate: {0}{1}", emptyMessage, vbCrLf)
        End If

        If Not (params(29) Is Nothing) Then
            _Log.AppendFormat("TelematicsInstaller: {0}{1}", params(29).ToString, vbCrLf)
        Else
            _Log.AppendFormat("TelematicsInstaller: {0}{1}", emptyMessage, vbCrLf)
        End If

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_UpdateFleetDetails, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)

        Catch ex As Exception
            returnvalue = String.Concat("ERROR: ", ex.Message)
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("UpdateFleetDetails", _Log.ToString)

        Return returnvalue

    End Function

    Public Shared Function UpdateFleetDetailsV2(iFleetAssetId As Integer, _
                                                iRUCPaidToKms As Integer, _
                                                iManufacturerId As Integer, _
                                                iNextServiceId As Integer?, _
                                                iManufacturerModelId As Integer, _
                                                iIdCompliancePlateYear As Integer, _
                                                iIdCompliancePlateMonth As Integer, _
                                                dPurProposedDelivDate As DateTime, _
                                                dFirstOnFleetDate As DateTime, _
                                                dPurReceiptDate As DateTime, _
                                                dRegFirstDate As DateTime, _
                                                dRCCofEndDate As DateTime, _
                                                dRegoEndDate As DateTime, _
                                                dWarrantyExpDate As DateTime, _
                                                dSelfContainExpDate As DateTime, _
                                                dTelematicsInstallDate As DateTime, _
                                                sComment As String, _
                                                sTelematicsDevice As String, _
                                                sTelematicsTablet As String, _
                                                sPurLocationOnFleet As String, _
                                                sIdEngineNumber As String, _
                                                sIdChassisNumber As String, _
                                                sKeyNrIgnition As String, _
                                                sRegistrationNumber As String, _
                                                sETag As String, _
                                                sEPurb As String, _
                                                sUserCode As String, _
                                                sMdFuelType As String, _
                                                sMdTransmission As String, _
                                                sTelematicsInstaller As String, _
                                                dTelematicsTabletInstallDate As DateTime, _
                                                sTelematicsTabletInstaller As String _
                                                ) As String


        Dim oParamArray(31) As Aurora.Common.Data.Parameter
        Dim returnvalue As String = "OK"
        Const EMPTY_MSG As String = "No value found"

        Dim _Log As StringBuilder = New StringBuilder()

        Try


            _Log.AppendFormat("{0}{1}------{2}-------                         {3}{4}", vbCrLf, vbCrLf, "start - updatefleetdetailsv2", vbCrLf, vbCrLf)
            _Log.AppendFormat("ifleetassetid               : {0}{1}", IIf(iFleetAssetId <> -1, iFleetAssetId, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("irucpaidtokms               : {0}{1}", IIf(iRUCPaidToKms <> -1, iRUCPaidToKms, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("imanufacturerid             : {0}{1}", IIf(iManufacturerId <> -1, iManufacturerId, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("inextserviceid              : {0}{1}", IIf(iNextServiceId.HasValue, iNextServiceId.Value, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("imanufacturermodelid        : {0}{1}", IIf(iManufacturerModelId <> -1, iManufacturerModelId, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("iidcomplianceplateyear      : {0}{1}", IIf(iIdCompliancePlateYear <> -1, iIdCompliancePlateYear, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("iidcomplianceplatemonth     : {0}{1}", IIf(iIdCompliancePlateMonth <> -1, iIdCompliancePlateMonth, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dpurproposeddelivdate       : {0}{1}", IIf(Not dPurProposedDelivDate.Date.Equals(DateTime.MinValue), dPurProposedDelivDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dfirstonfleetdate           : {0}{1}", IIf(Not dFirstOnFleetDate.Date.Equals(DateTime.MinValue), dFirstOnFleetDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dpurreceiptdate             : {0}{1}", IIf(Not dPurReceiptDate.Date.Equals(DateTime.MinValue), dPurReceiptDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dregfirstdate               : {0}{1}", IIf(Not dRegFirstDate.Date.Equals(DateTime.MinValue), dRegFirstDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("drccofenddate               : {0}{1}", IIf(Not dRCCofEndDate.Date.Equals(DateTime.MinValue), dRCCofEndDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dregoenddate                : {0}{1}", IIf(Not dRegoEndDate.Date.Equals(DateTime.MinValue), dRegoEndDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dwarrantyexpdate            : {0}{1}", IIf(Not dWarrantyExpDate.Date.Equals(DateTime.MinValue), dWarrantyExpDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dselfcontainexpdate         : {0}{1}", IIf(Not dSelfContainExpDate.Date.Equals(DateTime.MinValue), dSelfContainExpDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("dtelematicsinstalldate      : {0}{1}", IIf(Not dTelematicsInstallDate.Date.Equals(DateTime.MinValue), dTelematicsInstallDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("scomment                    : {0}{1}", IIf(Not String.IsNullOrEmpty(sComment), sComment, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("stelematicsdevice           : {0}{1}", IIf(Not String.IsNullOrEmpty(sTelematicsDevice), sTelematicsDevice, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("stelematicstablet           : {0}{1}", IIf(Not String.IsNullOrEmpty(sTelematicsTablet), sTelematicsTablet, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("spurlocationonfleet         : {0}{1}", IIf(Not String.IsNullOrEmpty(sPurLocationOnFleet), sPurLocationOnFleet, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("sidenginenumber             : {0}{1}", IIf(Not String.IsNullOrEmpty(sIdEngineNumber), sIdEngineNumber, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("sidchassisnumber            : {0}{1}", IIf(Not String.IsNullOrEmpty(sIdChassisNumber), sIdChassisNumber, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("skeynrignition              : {0}{1}", IIf(Not String.IsNullOrEmpty(sKeyNrIgnition), sKeyNrIgnition, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("sregistrationnumber         : {0}{1}", IIf(Not String.IsNullOrEmpty(sRegistrationNumber), sRegistrationNumber, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("setag                       : {0}{1}", IIf(Not String.IsNullOrEmpty(sETag), sETag, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("sepurb                      : {0}{1}", IIf(Not String.IsNullOrEmpty(sEPurb), sEPurb, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("susercode                   : {0}{1}", IIf(Not String.IsNullOrEmpty(sUserCode), sUserCode, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("smdfueltype                 : {0}{1}", IIf(Not String.IsNullOrEmpty(sMdFuelType), sMdFuelType, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("smdtransmission             : {0}{1}", IIf(Not String.IsNullOrEmpty(sMdTransmission), sMdTransmission, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("stelematicsinstaller        : {0}{1}", IIf(Not String.IsNullOrEmpty(sTelematicsInstaller), sTelematicsInstaller, EMPTY_MSG), vbCrLf)
            'Added by nimesh on 16th July 15 - DSD 865
            _Log.AppendFormat("dtelematicstabletinstalldt  : {0}{1}", IIf(Not dTelematicsInstallDate.Date.Equals(DateTime.MinValue), dTelematicsTabletInstallDate.Date, EMPTY_MSG), vbCrLf)
            _Log.AppendFormat("stelematicstabletinstaller  : {0}{1}", IIf(Not String.IsNullOrEmpty(sTelematicsTabletInstaller), sTelematicsTabletInstaller, EMPTY_MSG), vbCrLf)
            'End Added by nimesh on 16th July 15 - DSD 865

            ''@iFleetAssetId    BIGINT
            oParamArray(0) = New Aurora.Common.Data.Parameter("iFleetAssetId", DbType.Int32, IIf(iFleetAssetId <> -1, iFleetAssetId, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dPurProposedDelivDate  DATETIME = NULL,    
            oParamArray(1) = New Aurora.Common.Data.Parameter("dPurProposedDelivDate", DbType.DateTime, IIf(Not dPurProposedDelivDate.Date.Equals(DateTime.MinValue), dPurProposedDelivDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dFirstOnFleetDate   DATETIME = NULL,    
            oParamArray(2) = New Aurora.Common.Data.Parameter("dFirstOnFleetDate", DbType.DateTime, IIf(Not dFirstOnFleetDate.Date.Equals(DateTime.MinValue), dFirstOnFleetDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sPurLocationOnFleet  VARCHAR(64) = NULL,    
            oParamArray(3) = New Aurora.Common.Data.Parameter("sPurLocationOnFleet", DbType.String, IIf(Not String.IsNullOrEmpty(sPurLocationOnFleet), sPurLocationOnFleet, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dPurReceiptDate   DATETIME = NULL,    
            oParamArray(4) = New Aurora.Common.Data.Parameter("dPurReceiptDate", DbType.DateTime, IIf(Not dPurReceiptDate.Date.Equals(DateTime.MinValue), dPurReceiptDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sIdEngineNumber   varchar(64) = NULL,    
            oParamArray(5) = New Aurora.Common.Data.Parameter("sIdEngineNumber", DbType.String, IIf(Not String.IsNullOrEmpty(sIdEngineNumber), sIdEngineNumber, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sIdChassisNumber   varchar(64) = NULL,     
            oParamArray(6) = New Aurora.Common.Data.Parameter("sIdChassisNumber", DbType.String, IIf(Not String.IsNullOrEmpty(sIdChassisNumber), sIdChassisNumber, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sKeyNrIgnition   varchar(9) = NULL,
            oParamArray(7) = New Aurora.Common.Data.Parameter("sKeyNrIgnition", DbType.String, IIf(Not String.IsNullOrEmpty(sKeyNrIgnition), sKeyNrIgnition, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sIdCompliancePlateYear int   = NULL,    
            oParamArray(8) = New Aurora.Common.Data.Parameter("sIdCompliancePlateYear", DbType.Int16, IIf(iIdCompliancePlateYear <> -1, iIdCompliancePlateYear, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sIdCompliancePlateMonth int   = NULL,    
            oParamArray(9) = New Aurora.Common.Data.Parameter("sIdCompliancePlateMonth", DbType.Int16, IIf(iIdCompliancePlateMonth <> -1, iIdCompliancePlateMonth, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)


            ''@sRegistrationNumber  VARCHAR(64) = NULL,    
            oParamArray(10) = New Aurora.Common.Data.Parameter("sRegistrationNumber", DbType.String, IIf(Not String.IsNullOrEmpty(sRegistrationNumber), sRegistrationNumber, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dRegFirstDate    DATETIME = NULL,    
            oParamArray(11) = New Aurora.Common.Data.Parameter("dRegFirstDate", DbType.DateTime, IIf(Not dRegFirstDate.Date.Equals(DateTime.MinValue), dRegFirstDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dRCCofEndDate    DATETIME = NULL,    
            oParamArray(12) = New Aurora.Common.Data.Parameter("dRCCofEndDate", DbType.DateTime, IIf(Not dRCCofEndDate.Date.Equals(DateTime.MinValue), dRCCofEndDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@iRUCPaidToKms    BIGINT  = NULL,  
            oParamArray(13) = New Aurora.Common.Data.Parameter("iRUCPaidToKms", DbType.Int32, IIf(iRUCPaidToKms <> -1, iRUCPaidToKms, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sETag      VARCHAR(64) = NULL,    
            oParamArray(14) = New Aurora.Common.Data.Parameter("sETag", DbType.String, IIf(Not String.IsNullOrEmpty(sETag), sETag, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sEPurb     VARCHAR(64) = NULL,    
            oParamArray(15) = New Aurora.Common.Data.Parameter("sEPurb", DbType.String, IIf(Not String.IsNullOrEmpty(sEPurb), sEPurb, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sUserCode     VARCHAR(64) = NULL,  
            oParamArray(16) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, IIf(Not String.IsNullOrEmpty(sUserCode), sUserCode, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@iManufacturerId   BIGINT = NULL,  
            oParamArray(17) = New Aurora.Common.Data.Parameter("iManufacturerId", DbType.Int32, IIf(iManufacturerId <> -1, iManufacturerId, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@iManufacturerModelId  INT = NULL, 
            oParamArray(18) = New Aurora.Common.Data.Parameter("iManufacturerModelId", DbType.Int16, IIf(iManufacturerModelId <> -1, iManufacturerModelId, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)



            ''@sMdFuelType    VARCHAR(2) = NULL,  
            oParamArray(19) = New Aurora.Common.Data.Parameter("sMdFuelType", DbType.String, IIf(Not String.IsNullOrEmpty(sMdFuelType), sMdFuelType, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sMdTransmission   VARCHAR(10) = NULL,  
            oParamArray(20) = New Aurora.Common.Data.Parameter("sMdTransmission", DbType.String, IIf(Not String.IsNullOrEmpty(sMdTransmission), sMdTransmission, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@iNextServiceId   INT = NULL,
            oParamArray(21) = New Aurora.Common.Data.Parameter("iNextServiceId", DbType.Int16, IIf(iNextServiceId.HasValue, iNextServiceId.Value, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dRegoEndDate    DATETIME = NULL,     
            oParamArray(22) = New Aurora.Common.Data.Parameter("dRegoEndDate", DbType.DateTime, IIf(Not dRegoEndDate.Date.Equals(DateTime.MinValue), dRegoEndDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sComment     VARCHAR(1024) = NULL,  
            oParamArray(23) = New Aurora.Common.Data.Parameter("sComment", DbType.String, IIf(Not String.IsNullOrEmpty(sComment), sComment, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dWarrantyExpDate    DATETIME = NULL,
            oParamArray(24) = New Aurora.Common.Data.Parameter("dWarrantyExpDate", DbType.DateTime, IIf(Not dWarrantyExpDate.Date.Equals(DateTime.MinValue), dWarrantyExpDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dSelfContainExpDate DATETIME = NULL,
            oParamArray(25) = New Aurora.Common.Data.Parameter("dSelfContainExpDate", DbType.DateTime, IIf(Not dSelfContainExpDate.Date.Equals(DateTime.MinValue), dSelfContainExpDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sTelematicsDevice   NVARCHAR(50) = NULL,  
            oParamArray(26) = New Aurora.Common.Data.Parameter("sTelematicsDevice", DbType.String, IIf(Not String.IsNullOrEmpty(sTelematicsDevice), sTelematicsDevice, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sTelematicsTablet   NVARCHAR(50) = NULL,
            oParamArray(27) = New Aurora.Common.Data.Parameter("sTelematicsTablet", DbType.String, IIf(Not String.IsNullOrEmpty(sTelematicsTablet), sTelematicsTablet, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@dTelematicsInstallDate	DATETIME	= NULL,
            oParamArray(28) = New Aurora.Common.Data.Parameter("dTelematicsInstallDate", DbType.DateTime, IIf(Not dTelematicsInstallDate.Date.Equals(DateTime.MinValue), dTelematicsInstallDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sTelematicsInstaller	VARCHAR(50)
            oParamArray(29) = New Aurora.Common.Data.Parameter("sTelematicsInstaller", DbType.String, IIf(Not String.IsNullOrEmpty(sTelematicsInstaller), sTelematicsInstaller, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''Added by Nimesh on 16th July 2015 - DSD 865
            ''@dTelematicsInstallDate	DATETIME	= NULL,
            oParamArray(30) = New Aurora.Common.Data.Parameter("dTelematicsTabletInstallDate", DbType.DateTime, IIf(Not dTelematicsTabletInstallDate.Date.Equals(DateTime.MinValue), dTelematicsTabletInstallDate.Date, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''@sTelematicsInstaller	VARCHAR(50)
            oParamArray(31) = New Aurora.Common.Data.Parameter("sTelematicsTabletInstaller", DbType.String, IIf(Not String.IsNullOrEmpty(sTelematicsTabletInstaller), sTelematicsTabletInstaller, DBNull.Value), Common.Data.Parameter.ParameterType.AddInParameter)
            ''End Added by Nimesh on 16th July 2015 - DSD 865

            '' oParamArray(32) = New Aurora.Common.Data.Parameter("sResult", DbType.String, 200, Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP(SP_Fleet_UpdateFleetDetails, oParamArray)


            'If (Not String.IsNullOrEmpty(oParamArray(32).Value)) Then
            '    returnvalue = oParamArray(32).Value
            'End If
            '_Log.AppendFormat("OUTPUT PARAMETER", returnvalue)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)

        Catch ex As Exception
            Diagnostics.Debug.WriteLine(ex.StackTrace)
            returnvalue = String.Concat("ERROR: ", ex.Message)
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try
        _Log.AppendFormat("{0}------{1}-------                         : {2}", vbCrLf, "end - updatefleetdetailsv2", vbCrLf)
        Logging.LogDebug("UpdateFleetDetailsV2", _Log.ToString())

        Return returnvalue
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params">	@iUnitNumber				BIGINT,
    '''@iNewFleetModelId			BIGINT,
    '''@dNewEffectiveDate			DATETIME,
    '''@sCurrentFleetModelCode		VARCHAR(64) = null,
    '''@dCurrEffectiveDate			DATETIME = null,
    '''@bIsInsert					BIT	=	1,
    '''@sUserCode					VARCHAR(64)</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function InsUpFleetModelChange(ByVal ParamArray params() As Object) As String
        Dim output As Integer = 0
        Dim returnvalue As String = "OK"

        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iUnitNumber: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        If Not (params(1) Is Nothing) Then
            _Log.AppendFormat("iNewFleetModelId: {0}{1}", params(1).ToString, vbCrLf)
        End If

        If Not (params(2) Is Nothing) Then
            _Log.AppendFormat("sCurrentFleetModelCode: {0}{1}", params(2).ToString, vbCrLf)
        End If
        If Not (params(3) Is Nothing) Then
            _Log.AppendFormat("dCurrEffectiveDate: {0}{1}", params(3).ToString, vbCrLf)
        End If
        If Not (params(4) Is Nothing) Then
            _Log.AppendFormat("bIsInsert: {0}{1}", params(4).ToString, vbCrLf)
        End If
        If Not (params(5) Is Nothing) Then
            _Log.AppendFormat("sUserCode: {0}{1}", params(5).ToString, vbCrLf)
        End If

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_InsUpFleetModelChange, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            returnvalue = String.Concat("ERROR: ", ex.Message)
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("InsUpFleetModelChange", _Log.ToString)

        Return returnvalue
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="params">iCountryId</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetLocationList(ByVal ParamArray params() As Object) As DataSet
        Dim ds As New DataSet

        Dim result As DataSet

        Dim returnvalue As String = "OK"

        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        Try
            result = Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_getLocationList, ds, params)
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = Nothing
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try
        Logging.LogDebug("GetLocationList", _Log.ToString)

        Return result
    End Function

#Region "Fleet sale and disposal plan"
    ''' <summary>
    ''' GetVehicleSaleRecords
    '''@iCountryId		BIGINT,
    '''@iFleetModelId	BIGINT	=	NULL,
    '''@w_unit_from	BIGINT	=	NULL,
    '''@w_unit_to		BIGINT	=	NULL,
    '''@sUsrCode		VARCHAR(64) 
    ''' </summary>
    ''' <param name="iFleetModelId"></param>
    ''' <param name="iCountryId"></param>
    ''' <param name="w_unit_from"></param>
    ''' <param name="w_unit_to"></param>
    ''' <param name="sUsrCode"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetVehicleSaleRecords(iCountryId As Integer, iFleetModelId As String, w_unit_from As Integer?, w_unit_to As Integer?, sRegoNum As String, sUsrCode As String) As DataSet
        sbLog = New StringBuilder().AppendFormat("Parameters{0}iFleetModelId: {1}{2}", vbCrLf, iFleetModelId, vbCrLf)
        sbLog.AppendFormat("CountryId: {0}{1}", iCountryId.ToString, vbCrLf)
        sbLog.AppendFormat("w_unit_from: {0}{1}", w_unit_from.ToString, vbCrLf)
        sbLog.AppendFormat("w_unit_to: {0}{1}", w_unit_to.ToString, vbCrLf)
        sbLog.AppendFormat("sRegoNum: {0}{1}", sRegoNum, vbCrLf)
        sbLog.AppendFormat("sUsrCode: {0}{1}", sUsrCode, vbCrLf)

        Dim result As New DataSet

        Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_getVehicleSaleRecords, result, iCountryId, iFleetModelId, w_unit_from, w_unit_to, sRegoNum, sUsrCode)

        result.Tables(0).TableName = "VehicleSaleRecords"
        result.Tables(1).TableName = "CurrentPlans"

        Logging.LogDebug("GetVehicleSaleRecords", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        sbLog = Nothing

        Return result
    End Function

    Public Shared Function GetDropdownForSalePlan(iCountryId As Integer) As DataSet
        Dim sLog As StringBuilder
        sLog = New StringBuilder().AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, iCountryId, vbCrLf)

        Dim result As New DataSet

        Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_GetDropdownForSalePlan, result, iCountryId)

        If (result.Tables.Count >= 1) Then
            result.Tables(0).TableName = "CountryList"
        End If

        If (result.Tables.Count >= 2) Then
            result.Tables(1).TableName = "FleetModelInfoList"
        End If

        If (result.Tables.Count >= 3) Then
            result.Tables(2).TableName = "LocationList"
        End If

        If (result.Tables.Count >= 4) Then
            result.Tables(3).TableName = "CustomerTypeList"
        End If

        If (result.Tables.Count >= 5) Then
            result.Tables(4).TableName = "DealerList"
        End If

        If (result.Tables.Count >= 6) Then
            result.Tables(5).TableName = "PaymentMethodList"
        End If

        If (result.Tables.Count >= 7) Then
            result.Tables(6).TableName = "ReferenceList"
        End If

        If (result.Tables.Count >= 8) Then
            result.Tables(7).TableName = "CurrentLocationList"
        End If

        If (result.Tables.Count >= 9) Then
            result.Tables(8).TableName = "ActualLocationList"
        End If


        Logging.LogDebug("GetDropdownForSalePlan", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function UpdateSaleRecord(iSaleId As Integer?, dOffFleetDate As DateTime?, sSaleLocation As String, iSaleDealerId As Integer, sUserCode As String) As String
        Dim output As Integer = 0
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iSaleId: {1}{2}", vbCrLf, iSaleId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}dOffFleetDate: {1}{2}", vbCrLf, dOffFleetDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sSaleLocation: {1}{2}", vbCrLf, sSaleLocation, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iSaleDealerId: {1}{2}", vbCrLf, iSaleDealerId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sUserCode: {1}{2}", vbCrLf, sUserCode, vbCrLf)

        Dim result As String = "OK"

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_UpdateSaleRecord, iSaleId, dOffFleetDate, sSaleLocation, iSaleDealerId, sUserCode)
            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("UpdateSaleRecord", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function CreateSalePlan(iCountryId As Integer, iFromUnitNum As Integer, iToUnitNum As Integer, iFleetModelID As Integer, dStartDate As DateTime?, dEndDate As DateTime?, iplanquantity As Integer, sUserCode As String) As String
        Dim sLog As StringBuilder
        Dim result As String
        Dim iPlanId As String = 1000

        Dim oParamArray(8) As Aurora.Common.Data.Parameter

        oParamArray(0) = New Aurora.Common.Data.Parameter("iCountryId", DbType.Int32, iCountryId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(1) = New Aurora.Common.Data.Parameter("iFromUnitNum", DbType.Int32, iFromUnitNum, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(2) = New Aurora.Common.Data.Parameter("iToUnitNum", DbType.Int32, iToUnitNum, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(3) = New Aurora.Common.Data.Parameter("iFleetModelID", DbType.Int32, iFleetModelID, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(4) = New Aurora.Common.Data.Parameter("dStartDate", DbType.DateTime, dStartDate, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(5) = New Aurora.Common.Data.Parameter("dEndDate", DbType.DateTime, dEndDate, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(6) = New Aurora.Common.Data.Parameter("iplanquantity", DbType.Int32, iplanquantity, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(7) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, sUserCode, Common.Data.Parameter.ParameterType.AddInParameter)

        oParamArray(8) = New Aurora.Common.Data.Parameter("iPlanId", DbType.String, iPlanId, Common.Data.Parameter.ParameterType.AddOutParameter)

        sLog = New StringBuilder().AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, iCountryId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iFromUnitNum: {1}{2}", vbCrLf, iFromUnitNum, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iToUnitNum: {1}{2}", vbCrLf, iToUnitNum, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iFleetModelID: {1}{2}", vbCrLf, iFleetModelID, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}dStartDate: {1}{2}", vbCrLf, dStartDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}dEndDate: {1}{2}", vbCrLf, dEndDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iplanquantity: {1}{2}", vbCrLf, iplanquantity, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sUserCode: {1}{2}", vbCrLf, sUserCode, vbCrLf)


        Try
            Aurora.Common.Data.ExecuteOutputSP(SP_Fleet_CreateSalePlan, oParamArray)
            result = oParamArray(8).Value
            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("CreateSalePlan", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function CreateSalPlanLine(iCountryId As Integer, iUnitNumber As Integer, iPlanId As Integer, sOffFleetLication As String, dOffFleetDate As DateTime?, p_dealer As Integer, sUserCode As String) As String
        Dim output As Integer = 0
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0} iCountryId: {1}{2}", vbCrLf, iCountryId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0} iUnitNumber: {1}{2}", vbCrLf, iUnitNumber, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0} iPlanId: {1}{2}", vbCrLf, iPlanId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0} sOffFleetLication: {1}{2}", vbCrLf, sOffFleetLication, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0} dOffFleetDate: {1}{2}", vbCrLf, dOffFleetDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0} p_dealer: {1}{2}", vbCrLf, p_dealer, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sUserCode: {1}{2}", vbCrLf, sUserCode, vbCrLf)


        Dim result As String = "OK"

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_CreateSalPlanLine, iCountryId, iUnitNumber, iPlanId, sOffFleetLication, dOffFleetDate, p_dealer, sUserCode)
            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("CreateSalPlanLine", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function DeleteSalePlanRecord(iFleetAssetId As Integer, iSaleId As Integer, sUserCode As String) As String
        Dim output As Integer = 0
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, iFleetAssetId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iSaleId: {1}{2}", vbCrLf, iSaleId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sUserCode: {1}{2}", vbCrLf, sUserCode, vbCrLf)

        Dim result As String = "OK"

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_DeleteSalePlanRecord, iFleetAssetId, iSaleId, sUserCode)
            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("DeleteSalePlanRecord", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    ''rev:mia 14nov2014 start - added RRP --------
    Public Shared Function UpdateSalesPlan(iCountryId As Integer,
                                            p_fleet_asset_id As Integer,
                                            dDateSold As DateTime?,
                                            dPickupDate As DateTime?,
                                            sCustomerType As String,
                                            iSalePrice As Integer,
                                            iSalePriceIncTax As Integer,
                                            iSalesTaxPercent As Integer,
                                            iDealerId As Integer,
                                            sCustomerName As String,
                                            sSettleMethod As String,
                                            dSettleBankDate As DateTime?,
                                            sSettleLocation As String,
                                            sSaleComment As String,
                                            bRelease As Integer,
                                            iPreSaleActualLocId As Integer,
                                            iCurrentLocationId As Integer,
                                            dModifiedDate As DateTime?,
                                            sModifiedBy As String,
                                            iSaleId As Integer, _
                                            Optional RRP As String = "") As String
        Dim output As Integer = 0
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, iCountryId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}p_fleet_asset_id: {1}{2}", vbCrLf, p_fleet_asset_id, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}dDateSold: {1}{2}", vbCrLf, dDateSold, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}dPickupDate: {1}{2}", vbCrLf, dPickupDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sCustomerType: {1}{2}", vbCrLf, sCustomerType, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iSalePrice: {1}{2}", vbCrLf, iSalePrice, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iSalePriceIncTax: {1}{2}", vbCrLf, iSalePriceIncTax, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iSalesTaxPercent: {1}{2}", vbCrLf, iSalesTaxPercent, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iDealerId: {1}{2}", vbCrLf, iDealerId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sCustomerName: {1}{2}", vbCrLf, sCustomerName, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sSettleMethod: {1}{2}", vbCrLf, sSettleMethod, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}dSettleBankDate: {1}{2}", vbCrLf, dSettleBankDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sSettleLocation: {1}{2}", vbCrLf, sSettleLocation, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sSaleComment: {1}{2}", vbCrLf, sSaleComment, vbCrLf)

        sLog = New StringBuilder().AppendFormat("Parameters{0}bRelease: {1}{2}", vbCrLf, bRelease, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iPreSaleActualLocId: {1}{2}", vbCrLf, iPreSaleActualLocId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iCurrentLocationId: {1}{2}", vbCrLf, iCurrentLocationId, vbCrLf)

        sLog = New StringBuilder().AppendFormat("Parameters{0}dModifiedDate: {1}{2}", vbCrLf, dModifiedDate, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sModifiedBy: {1}{2}", vbCrLf, sModifiedBy, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iSaleId: {1}{2}", vbCrLf, iSaleId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}nRRP: {1}{2}", vbCrLf, RRP, vbCrLf)

        Dim result As String = "OK"

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_UpdateSalesPlan,
                                                        iCountryId,
                                                        p_fleet_asset_id,
                                                        dDateSold,
                                                        dPickupDate,
                                                        sCustomerType,
                                                        iSalePrice,
                                                        iSalePriceIncTax,
                                                        iSalesTaxPercent,
                                                        iDealerId,
                                                        sCustomerName,
                                                        sSettleMethod,
                                                        dSettleBankDate,
                                                        sSettleLocation,
                                                        sSaleComment,
                                                        bRelease,
                                                        iPreSaleActualLocId,
                                                        iCurrentLocationId,
                                                        dModifiedDate,
                                                        sModifiedBy,
                                                        iSaleId, _
                                                        IIf(String.IsNullOrEmpty(RRP), DBNull.Value, RRP))

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("ReleaseSalesPlan", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function SaveProcessSteps(iProcessStepsId As Integer?,
                                            iFleetAssetId As Integer,
                                            iProcessId As Integer,
                                            sAddUserCode As String,
                                            bCompleted As Integer) As String
        Dim output As Integer = 0
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iProcessStepsId: {1}{2}", vbCrLf, iProcessStepsId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, iFleetAssetId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iProcessId: {1}{2}", vbCrLf, iProcessId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sAddUserCode: {1}{2}", vbCrLf, sAddUserCode, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}bCompleted: {1}{2}", vbCrLf, bCompleted, vbCrLf)

        Dim result As String = "OK"

        Try
            output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_SaveProcessSteps,
                                                        iProcessStepsId,
                                                        iFleetAssetId,
                                                        iProcessId,
                                                        sAddUserCode,
                                                        bCompleted)

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("SaveProcessSteps", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function GetProcessedSteps(iFleetAssetId As Integer) As DataSet
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, iFleetAssetId, vbCrLf)

        Dim result As New DataSet

        Try
            Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_GetProcessedSteps, result,
                                                        iFleetAssetId)

            result.Tables(0).TableName = "ProcessSteps"
            result.Tables(1).TableName = "HistoryItems"

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GetProcessedSteps", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result

    End Function
#End Region

#Region "Fleet Activity"

    Public Shared Function GetFleetActivityHistory(iCountryId As Integer?, iUnitNumber As Integer?,
                                            sRegoNum As String) As DataSet
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, iCountryId, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}iUnitNumber: {1}{2}", vbCrLf, iUnitNumber, vbCrLf)
        sLog = New StringBuilder().AppendFormat("Parameters{0}sRegoNum: {1}{2}", vbCrLf, sRegoNum, vbCrLf)

        Dim result As New DataSet

        Try
            If (iUnitNumber = 0) Then
                iUnitNumber = Nothing
            End If

            Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_getFleetActivityHistory, result,
                                                iCountryId,
                                                        iUnitNumber,
                                                        sRegoNum)
            If (result.Tables.Count >= 1) Then
                result.Tables(0).TableName = "ThisFleetRelatedInformation"
            End If

            If (result.Tables.Count >= 2) Then
                result.Tables(1).TableName = "AllFleetActivityHistory"
            End If

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GetFleetActivityHistory", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function GetDropDownForFleetActivity(iCountryId As Integer) As DataSet
        Dim sLog As StringBuilder

        sLog = New StringBuilder().AppendFormat("Parameters{0}iCountryId: {1}{2}", vbCrLf, iCountryId, vbCrLf)

        Dim result As New DataSet

        Try
            Aurora.Common.Data.ExecuteDataSetSP(SP_Fleet_GetDropDownForFleetActivity, result,
                                                        iCountryId)
            If (result.Tables.Count >= 1) Then
                result.Tables(0).TableName = "CountryList"
            End If

            If (result.Tables.Count >= 2) Then
                result.Tables(1).TableName = "ActivityTypeList"
            End If

            If (result.Tables.Count >= 3) Then
                result.Tables(2).TableName = "LocationList"
            End If

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GetDropDownForFleetActivity", sLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)

        Return result
    End Function

    Public Shared Function CreateNewActivity(iFleetAssetId As Integer, sActivityType As String, sExternalRef As String, _
                                             dStartDate As DateTime, dEndDate As DateTime, iStartLocation As Integer, _
                                             iEndLocation As Integer, iStartOddometer As Integer, iEndOddometer As Integer, _
                                             bCompleted As Boolean, sModifiedBy As String, sdescription As String, bForce As Boolean, _
                                             ByRef sReturnError As String, ByRef activity_id As String)
        Dim sLog As StringBuilder = New StringBuilder()

        Dim oParamArray(14) As Aurora.Common.Data.Parameter

        oParamArray(0) = New Aurora.Common.Data.Parameter("iFleetAssetId", DbType.Int32, iFleetAssetId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(1) = New Aurora.Common.Data.Parameter("sActivityType", DbType.String, sActivityType, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(2) = New Aurora.Common.Data.Parameter("sExternalRef", DbType.String, sExternalRef, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(3) = New Aurora.Common.Data.Parameter("dStartDate", DbType.DateTime, dStartDate, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(4) = New Aurora.Common.Data.Parameter("dEndDate", DbType.DateTime, dEndDate, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(5) = New Aurora.Common.Data.Parameter("iStartLocation", DbType.Int32, iStartLocation, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(6) = New Aurora.Common.Data.Parameter("iEndLocation", DbType.Int32, iEndLocation, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(7) = New Aurora.Common.Data.Parameter("iStartOddometer", DbType.Int32, iStartOddometer, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(8) = New Aurora.Common.Data.Parameter("iEndOddometer", DbType.Int32, iEndOddometer, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(9) = New Aurora.Common.Data.Parameter("bCompleted", DbType.Boolean, bCompleted, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(10) = New Aurora.Common.Data.Parameter("sModifiedBy", DbType.String, sModifiedBy, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(11) = New Aurora.Common.Data.Parameter("sdescription", DbType.String, sdescription, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(12) = New Aurora.Common.Data.Parameter("bForce", DbType.Boolean, bForce, Common.Data.Parameter.ParameterType.AddInParameter)

        'output
        oParamArray(13) = New Aurora.Common.Data.Parameter("sReturnError", DbType.String, 2000, Common.Data.Parameter.ParameterType.AddOutParameter)
        oParamArray(14) = New Aurora.Common.Data.Parameter("activity_id", DbType.String, 2000, Common.Data.Parameter.ParameterType.AddOutParameter)


        sLog = sLog.AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, iFleetAssetId, vbCrLf)
        sLog = sLog.AppendFormat("sActivityType: {0}{1}", sActivityType, vbCrLf)
        sLog = sLog.AppendFormat("sExternalRef: {0}{1}", sExternalRef, vbCrLf)
        sLog = sLog.AppendFormat("dStartDate: {0}{1}", dStartDate, vbCrLf)
        sLog = sLog.AppendFormat("dEndDate: {0}{1}", dEndDate, vbCrLf)
        sLog = sLog.AppendFormat("iStartLocation: {0}{1}", iStartLocation, vbCrLf)
        sLog = sLog.AppendFormat("iEndLocation: {0}{1}", iEndLocation, vbCrLf)
        sLog = sLog.AppendFormat("iStartOddometer: {0}{1}", iStartOddometer, vbCrLf)
        sLog = sLog.AppendFormat("iEndOddometer: {0}{1}", iEndOddometer, vbCrLf)
        sLog = sLog.AppendFormat("bCompleted: {0}{1}", bCompleted, vbCrLf)
        sLog = sLog.AppendFormat("sModifiedBy: {0}{1}", sModifiedBy, vbCrLf)
        sLog = sLog.AppendFormat("sdescription: {0}{1}", sdescription, vbCrLf)
        sLog = sLog.AppendFormat("bForce: {0}{1}", bForce, vbCrLf)

        Try
            Aurora.Common.Data.ExecuteOutputSP(SP_Fleet_CreateNewActivity, oParamArray)
            sReturnError = oParamArray(13).Value
            activity_id = oParamArray(14).Value

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            sReturnError = ex.Message
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("CreateNewActivity", sLog.ToString)


    End Function

    Public Shared Function UpdateActivity(iFleetActivityId As Integer, iFleetAssetId As Integer, sActivityType As String, _
                                          sExternalRef As String, dStartDate As DateTime, dEndDate As DateTime, iStartLocation As Integer, _
                                          iEndLocation As Integer, iStartOddometer As Integer, iEndOddometer As Integer, bCompleted As Boolean, _
                                          sModifiedBy As String, sdescription As String, bForce As Boolean) As String()
        Dim sLog As StringBuilder = New StringBuilder()
        Dim result(0 To 1) As String

        Dim sReturnError As String = "" 'output
        Dim activity_id As String = "" 'output


        Dim oParamArray(15) As Aurora.Common.Data.Parameter

        oParamArray(0) = New Aurora.Common.Data.Parameter("iFleetActivityId", DbType.Int32, iFleetActivityId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(1) = New Aurora.Common.Data.Parameter("iFleetAssetId", DbType.Int32, iFleetAssetId, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(2) = New Aurora.Common.Data.Parameter("sActivityType", DbType.String, sActivityType, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(3) = New Aurora.Common.Data.Parameter("sExternalRef", DbType.String, sExternalRef, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(4) = New Aurora.Common.Data.Parameter("dStartDate", DbType.DateTime, dStartDate, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(5) = New Aurora.Common.Data.Parameter("dEndDate", DbType.DateTime, dEndDate, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(6) = New Aurora.Common.Data.Parameter("iStartLocation", DbType.Int32, iStartLocation, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(7) = New Aurora.Common.Data.Parameter("iEndLocation", DbType.Int32, iEndLocation, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(8) = New Aurora.Common.Data.Parameter("iStartOddometer", DbType.Int32, iStartOddometer, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(9) = New Aurora.Common.Data.Parameter("iEndOddometer", DbType.Int32, iEndOddometer, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(10) = New Aurora.Common.Data.Parameter("bCompleted", DbType.Boolean, bCompleted, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(11) = New Aurora.Common.Data.Parameter("sModifiedBy", DbType.String, sModifiedBy, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(12) = New Aurora.Common.Data.Parameter("sdescription", DbType.String, sdescription, Common.Data.Parameter.ParameterType.AddInParameter)
        oParamArray(13) = New Aurora.Common.Data.Parameter("bForce", DbType.Boolean, bForce, Common.Data.Parameter.ParameterType.AddInParameter)

        'output
        oParamArray(14) = New Aurora.Common.Data.Parameter("sReturnError", DbType.String, 2000, Common.Data.Parameter.ParameterType.AddOutParameter)
        oParamArray(15) = New Aurora.Common.Data.Parameter("activity_id", DbType.String, 2000, Common.Data.Parameter.ParameterType.AddOutParameter)

        sLog = sLog.AppendFormat("Parameters{0}iFleetActivityId: {1}{2}", vbCrLf, iFleetActivityId, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, iFleetAssetId, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}sActivityType: {1}{2}", vbCrLf, sActivityType, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}sExternalRef: {1}{2}", vbCrLf, sExternalRef, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}dStartDate: {1}{2}", vbCrLf, dStartDate, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}dEndDate: {1}{2}", vbCrLf, dEndDate, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}iStartLocation: {1}{2}", vbCrLf, iStartLocation, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}iEndLocation: {1}{2}", vbCrLf, iEndLocation, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}iStartOddometer: {1}{2}", vbCrLf, iStartOddometer, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}iEndOddometer: {1}{2}", vbCrLf, iEndOddometer, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}bCompleted: {1}{2}", vbCrLf, bCompleted, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}sModifiedBy: {1}{2}", vbCrLf, sModifiedBy, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}sdescription: {1}{2}", vbCrLf, sdescription, vbCrLf)
        sLog = sLog.AppendFormat("Parameters{0}bForce: {1}{2}", vbCrLf, bForce, vbCrLf)

        Try
            Aurora.Common.Data.ExecuteOutputSP(SP_Fleet_UpdateActivity, oParamArray)
            result(0) = oParamArray(14).Value
            result(1) = oParamArray(15).Value

            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result(0) = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("UpdateActivity", sLog.ToString)

        Return result
    End Function
#End Region

    Shared Function CheckIfVehiclesAreOnFleet(AssetLists As String) As String
        Dim output As Integer = 0
        Dim returnvalue As String

        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        Dim params(1) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("AssetIDList", DbType.String, AssetLists, Common.Data.Parameter.ParameterType.AddInParameter)

        params(1) = New Aurora.Common.Data.Parameter("RetMessage", DbType.String, 1000, Common.Data.Parameter.ParameterType.AddOutParameter)
        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}FleetAssetList: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If

        
        Try
            Aurora.Common.Data.ExecuteOutputSP("GEN_GetFleetListGeneralStatus", params)
            returnvalue = params(1).Value
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            returnvalue = String.Concat("ERROR: ", ex.Message)
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("GEN_GetFleetListGeneralStatus", _Log.ToString)

        Return returnvalue
    End Function

    Shared Function UpdateFleetModelID(iFleetAssetId As String, iFleetModelID As Integer, sUserCode As String) As String
        Dim output As Integer = 0
        Dim returnvalue As String

        Dim _Log As StringBuilder
        _Log = New StringBuilder()

        Dim params(3) As Aurora.Common.Data.Parameter

        params(0) = New Aurora.Common.Data.Parameter("iFleetAssetId", DbType.Int32, iFleetAssetId, Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("iNewFleetModelId", DbType.Int32, iFleetModelID, Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, sUserCode, Common.Data.Parameter.ParameterType.AddInParameter)

        params(3) = New Aurora.Common.Data.Parameter("RetMessage", DbType.String, 1000, Common.Data.Parameter.ParameterType.AddOutParameter)
        If Not (params(0) Is Nothing) Then
            _Log.AppendFormat("Parameters{0}FleetAssetList: {1}{2}", vbCrLf, params(0).ToString, vbCrLf)
        End If


        Try
            Aurora.Common.Data.ExecuteOutputSP("Fleet_UpdateFleetModel", params)
            returnvalue = params(3).Value
            _Log.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            returnvalue = String.Concat("ERROR: ", ex.Message)
            _Log.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("Fleet_UpdateFleetModel", _Log.ToString)

        Return returnvalue
    End Function

#Region "Nyt 3rd Nov 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-314 "
    Shared Function SaveProcessStepsDate(iProcessStepsId As Integer, iFleetAssetId As Integer, iProcessId As Integer, sAddUserCode As String, dProcessedDate As Date) As Object
        Dim output As Integer = 0
        Dim sLog As New StringBuilder

        sLog.AppendFormat("Parameters{0}iProcessStepsId: {1}{2}", vbCrLf, iProcessStepsId, vbCrLf)
        sLog.AppendFormat("Parameters{0}iFleetAssetId: {1}{2}", vbCrLf, iFleetAssetId, vbCrLf)
        sLog.AppendFormat("Parameters{0}iProcessId: {1}{2}", vbCrLf, iProcessId, vbCrLf)
        sLog.AppendFormat("Parameters{0}sAddUserCode: {1}{2}", vbCrLf, sAddUserCode, vbCrLf)
        sLog.AppendFormat("Parameters{0}dProcessedDate: {1}{2}", vbCrLf, dProcessedDate, vbCrLf)

        Dim result As String = "OK"

        Try

            Dim params(4) As Aurora.Common.Data.Parameter

            params(0) = New Aurora.Common.Data.Parameter("iProcessStepsId", DbType.Int32, iProcessStepsId, Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("iFleetAssetId", DbType.Int32, iFleetAssetId, Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("iProcessId", DbType.Int32, iProcessId, Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("sAddUserCode", DbType.String, sAddUserCode, Common.Data.Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("dProcessedDate", DbType.Date, dProcessedDate, Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP(SP_Fleet_SaveProcessStepsDate, params)
            

            'output = Aurora.Common.Data.ExecuteScalarSP(SP_Fleet_SaveProcessStepsDate,
            '                                            iProcessStepsId,
            '                                            iFleetAssetId,
            '                                            iProcessId,
            '                                            sAddUserCode,
            '                                            dProcessedDate)




            sLog.AppendFormat("RESULT: OK{0}", vbCrLf)
        Catch ex As Exception
            result = String.Concat("ERROR: ", ex.Message)
            sLog.AppendFormat("ERRORS: {0}-{1}{2}", ex.Message, ex.StackTrace, vbCrLf)
        End Try

        Logging.LogDebug("SaveProcessStepsDate", sLog.ToString)

        Return result
    End Function

    Public Shared Function GetDefaultCountryId(UserCode As String) As String
        sbLog = New StringBuilder().AppendFormat("Parameters{0}Usercode: {1}{2}", vbCrLf, UserCode, vbCrLf)
        Dim returnvalue As String = Aurora.Common.Data.ExecuteScalarSP(SP_DefaultCountryId, UserCode)
        Logging.LogDebug("GetDefaultCountryId", sbLog.AppendFormat("RESULT: OK{0}", vbCrLf).ToString)
        sbLog = Nothing
        Return returnvalue
    End Function
#End Region

End Class
