﻿Imports System.Data
Imports System.Data.Common
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.Data

Public Class DataRepository

    Public Shared Function GetConfirmationSetUp(ByVal user As String, ByVal country As String) As DataSet
        Return (Aurora.Common.Data.ExecuteDataSetSP("sp_getConfirmationSetUpTable", New DataSet, user, country))
    End Function

    Public Shared Function GetConfirmationSetUpXml(ByVal user As String, ByVal country As String) As XmlDocument
        Return (Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getConfirmationSetUp", user, country))
    End Function

    Public Shared Function GetRentalStatus(ByVal crsId As String) As DataTable
        Return Aurora.Common.Data.ExecuteTableSP("sp_getConfirmationRentalStatus", New DataTable(), crsId)
    End Function

    Public Shared Sub InsertUpdateConfirmationSetUp(ByVal country As String, ByVal user As String, ByVal confirmationRentalStatusId As String, ByVal programName As String, ByVal status As String, ByVal text As String)
        Aurora.Common.Data.ExecuteScalarSP("sp_insertUpdateConfirmationSetUp", country, user, confirmationRentalStatusId, programName, status, text)
    End Sub

    Public Shared Function GetNewsFlash(ByVal newsFlashId As String) As DataTable
        Return Aurora.Common.Data.ExecuteTableSP("sp_getNewsFlash", New DataTable(), newsFlashId)
    End Function

    Public Shared Function InsertNewsFlash(ByVal country As String, ByVal user As String, ByVal programName As String, ByVal audienceType As String, ByVal effectiveDate As DateTime, ByVal terminationDate As DateTime, ByVal text As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("sp_insertNewsFlash", country, user, programName, audienceType, effectiveDate, terminationDate, text)
    End Function

    Public Shared Function UpdateNewsFlash(ByVal country As String, ByVal user As String, ByVal newsFlashId As String, ByVal audienceType As String, ByVal effectiveDate As DateTime, ByVal terminationDate As DateTime, ByVal text As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("sp_updateNewsFlash", country, user, newsFlashId, audienceType, effectiveDate, terminationDate, text)
    End Function

    Public Shared Function DeleteNewsFlash(ByVal newsFlashId As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("sp_deleteNewsFlash", newsFlashId)
    End Function

    Public Shared Function GetConfirmationText(ByVal user As String, ByVal country As String, ByVal active As Boolean, Optional ByVal noteSpecId As String = "") As DataSet
        Dim xmlDoc As XmlDocument
        xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_getConfirmationText", user, active, country, noteSpecId)
        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))
        Return ds

    End Function

    ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
    Public Shared Function InsertConfirmationText(ByVal country As String, ByVal user As String, ByVal type As String, ByVal isHeader As Boolean, ByVal text As String, ByVal order As Integer, ByVal isCus As Boolean, ByVal isAgn As Boolean, ByVal isActive As Boolean, ByVal programName As String, Optional NtsBrdCode As String = "") As String
        Return Aurora.Common.Data.ExecuteScalarSP("sp_insertNoteSpec", user, country, type, isHeader, text, order, isCus, isAgn, isActive, programName, NtsBrdCode)
    End Function

    ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
    Public Shared Function UpdateConfirmationText(ByVal country As String, ByVal user As String, ByVal noteSpecId As String, ByVal type As String, ByVal isHeader As Boolean, ByVal text As String, ByVal order As Integer, ByVal isCus As Boolean, ByVal isAgn As Boolean, ByVal isActive As Boolean, ByVal programName As String, Optional NtsBrdCode As String = "") As String
        Return Aurora.Common.Data.ExecuteScalarSP("sp_updateNoteSpec", user, country, noteSpecId, type, isHeader, text, order, isCus, isAgn, isActive, programName, NtsBrdCode)
    End Function

    'Public Shared Function DeleteConfirmationText(ByVal noteSpecId As String) As String
    '    Return Aurora.Common.Data.ExecuteScalarSP("sp_deleteNoteSpec", noteSpecId)
    'End Function

    Public Shared Function ConfirmationGetBrand(rntId As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("Confirmation_GetBrand", rntId)
    End Function

    Public Shared Function ConfirmationGetBrandName(brandCode As String) As String
        Return Aurora.Common.Data.ExecuteScalarSP("Confirmation_GetBrandName", brandCode)
    End Function

    ''rev:mia 22-march-2016 https://thlonline.atlassian.net/browse/AURORA-754 Question marks "?" showing on Experience Voucher Email
    Public Shared Sub InsertConfirmationHTML(ByVal confirmationid As String, _
                               ByVal confirmationtext As String, _
                               ByVal userid As String, _
                               ByVal newconfirmationid As String)
        Aurora.Common.Logging.LogDebug("Aurora.Common.ConfirmationClassExtension", "Calling InsertConfirmationHTML: " & confirmationid)
        If String.IsNullOrEmpty(confirmationid) Then Exit Sub
        Const EOF_FLAG As String = "~EOF~"

        confirmationtext = confirmationtext.Replace(EOF_FLAG, "")

        Dim params(3) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("confirmationid", DbType.String, confirmationid, Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("confirmationhtml", DbType.String, confirmationtext, Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("userid", DbType.String, userid, Common.Data.Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("newconfirmationid", DbType.String, newconfirmationid, Common.Data.Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("Confirmation_InsUpdHTML", params)


        Catch ex As Exception
            Aurora.Common.Logging.LogError("Aurora.Common.ConfirmationClassExtension", "InsertConfirmationHTML Error: " & ex.Message & " , ConfirmationID: " & confirmationid)
        End Try

    End Sub
End Class
