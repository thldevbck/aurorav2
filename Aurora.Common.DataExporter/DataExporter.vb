Imports System.Collections.Generic
Imports System.Net
Imports System.IO
Imports System.Text
Imports System.Xml
Imports System.Net.Mail

Imports Aurora.Common
Imports Aurora.Common.Data

Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

''' <summary>
''' The data exporter is a console application that exports a table's data to a sql file (INSERT INTO's)
''' </summary>
''' <remarks></remarks>
Class DataExporter
    Implements IDisposable

    Public Shared ReadOnly config As DataExporterConfiguration = DataExporterConfiguration.Instance

    Public Const FunctionCode As String = "DATA_EXPORTER"

    Public Sub New()
    End Sub

    Public Sub Dispose() Implements IDisposable.Dispose
    End Sub

    ''' <summary>
    ''' Run/execute the dataExporter
    ''' </summary>
    Public Sub Execute()
        Logging.LogInformation(FunctionCode, "Executing")
        Try
            For Each table As DataExporterConfigurationTable In config.Tables
                Logging.LogInformation(FunctionCode, "  Getting Table Information: " + table.TableName)

                Dim _columnInformation = DataRepository.GetColumnInformation(table.TableName)

                Logging.LogInformation(FunctionCode, "  Getting Table Data: " + table.Sql)
                Dim dataTable As New DataTable()
                Data.ExecuteTableSql(table.Sql, dataTable)

                Dim first As Boolean = True

                Logging.LogInformation(FunctionCode, "  Writing File: " + table.FileName)
                Using writer As New StreamWriter(table.FileName)
                    For Each dataRow As DataRow In dataTable.Rows
                        Dim line As New StringBuilder
                        line.AppendLine("INSERT INTO [" & table.TableName & "]")

                        first = True
                        line.Append("(")
                        For Each columnInformationRow As ColumnInformationRow In _columnInformation
                            If columnInformationRow.IsIdentity() Then Continue For
                            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
                            If first Then first = False Else line.Append(",")
                            line.Append("[" & columnInformationRow.ColumnName & "]")
                        Next
                        line.AppendLine(")")

                        first = True
                        line.Append("VALUES (")
                        For Each columnInformationRow As ColumnInformationRow In _columnInformation
                            If columnInformationRow.IsIdentity() Then Continue For
                            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
                            If first Then first = False Else line.Append(",")
                            If dataRow.IsNull(columnInformationRow.ColumnName) Then
                                line.Append("NULL")
                            ElseIf TypeOf (dataRow(columnInformationRow.ColumnName)) Is Boolean Then
                                line.Append(IIf(CType(dataRow(columnInformationRow.ColumnName), Boolean), "1", "0"))
                            ElseIf TypeOf (dataRow(columnInformationRow.ColumnName)) Is Date Then
                                line.Append("'" & CType(dataRow(columnInformationRow.ColumnName), Date).ToString("dd/MM/yyyy") & "'")
                            Else
                                line.Append(Utility.EscapeSqlString(dataRow(columnInformationRow.ColumnName).ToString()))
                            End If
                        Next
                        line.AppendLine(")")

                        line.AppendLine()

                        Logging.LogInformation(FunctionCode, line.ToString())
                        writer.Write(line.ToString())
                    Next
                End Using
            Next

            Logging.LogInformation(FunctionCode, "Done")
        Catch ex As Exception
            Logging.LogError(FunctionCode, "Error", ex)
        End Try

    End Sub

    Shared Sub Main()

        Using dataExporter As New DataExporter()
            dataExporter.Execute()
        End Using

    End Sub


End Class
