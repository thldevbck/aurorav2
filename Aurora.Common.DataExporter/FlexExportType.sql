INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('AURORA','Aurora','FlxToAurora{BookYYYYMMDD}.sql',' ',0,'THL',NULL,NULL,NULL,NULL,NULL,'2',0,NULL,NULL,NULL,1,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('FTI','FTI Flex File for Britz','flxtrfti04.txt',' ',0,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('GAZELLE','Gazelle Word Wave File for Britz','GTHLFlex.txt',' ',0,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('SPECIALTXT','Special Text File for Britz, AU','AUB{TravelYYYY}{BookYYYYMMDD}.txt','',1,'THL','AU','B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('SPECIALTXT','Special Text File for Britz, NZ','NZB{TravelYYYY}{BookYYYYMMDD}.txt',' ',1,'THL','NZ','B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('SPECIALXML','Special Xml File for Britz, AU','AUB{TravelYYYY}.xml','',1,'THL','AU','B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('SPECIALXML','Special Xml File for Britz, NZ','NZB{TravelYYYY}.xml',' ',1,'THL','NZ','B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('GENERAL','General File for Britz','flxrtr{TravelYY}.txt',' ',1,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('CAF','CAF Flex File for Britz','flxrtf{TravelYY}.txt',' ',1,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('DER3','DER3 File for Britz','flxBRDER3.t{TravelYY}',' ',1,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('DER','DER File for Britz','flxBRDER.T{TravelYY}',' ',1,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('DER2','DER2 File for Britz','flxbrder.Txt',' ',0,'THL',NULL,'B',NULL,NULL,NULL,'48',0,NULL,NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

INSERT INTO [FlexExportType]
([FlxCode],[FlxName],[FlxFileName],[FlxEmailText],[FlxPerTravelYear],[FlxComCode],[FlxCtyCode],[FlxBrdCode],[FlxClaID],[FlxTypID],[FlxPrdId],[FlxScheduleHoursBefore],[FlxIsFtp],[FlxFtpUri],[FlxFtpUserName],[FlxFtpPassword],[FlxIsAurora],[FlxIsActive],[IntegrityNo],[AddUsrId],[ModUsrId],[AddDateTime],[ModDateTime],[AddPrgmName])
VALUES ('FLEXUPDATE','Xml Flex Update File for Britz Website','flupdate.xml',' ',0,'THL',NULL,'B',NULL,NULL,NULL,'48',1,'ftp://localhost',NULL,NULL,0,1,'1','wv1',NULL,'28/08/2007',NULL,'manual')

