Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration

Public Class DataExporterConfigurationTable
    ' Database audit settings
    Public TableName As String = "FlexExportType"

    Private _sql As String
    Public Property Sql() As String
        Get
            If _sql Is Nothing Then
                Return "SELECT * FROM " & TableName
            Else
                Return _sql
            End If
        End Get
        Set(ByVal value As String)
            _sql = value
        End Set
    End Property

    Private _fileName As String
    Public Property FileName() As String
        Get
            If _fileName Is Nothing Then
                Return TableName & ".sql"
            Else
                Return _fileName
            End If
        End Get
        Set(ByVal value As String)
            _fileName = value
        End Set
    End Property
End Class

Public Class DataExporterConfiguration

    Public Tables() As DataExporterConfigurationTable = {}

    Private Shared _instance As DataExporterConfiguration
    Public Shared ReadOnly Property Instance() As DataExporterConfiguration
        Get
            If _instance Is Nothing Then _instance = CType(ConfigurationManager.GetSection("DataExporterConfiguration"), DataExporterConfiguration)
            If _instance Is Nothing Then _instance = New DataExporterConfiguration()
            Return _instance
        End Get
    End Property

End Class


Public Class DataExporterConfigurationSectionHandler
    Implements IConfigurationSectionHandler

    Public Function Create(ByVal parent As Object, ByVal configContext As Object, _
        ByVal section As System.Xml.XmlNode) As Object _
        Implements System.Configuration.IConfigurationSectionHandler.Create

        Dim xs As XmlSerializer = New XmlSerializer(GetType(DataExporterConfiguration))
        Return xs.Deserialize(New XmlNodeReader(section))
    End Function

End Class
