INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'2B4WDB','BS  ','2B4WDB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'2BB','HP  ','2BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'2BBX','BX  ','2BBX')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'2BBXS','2BBX','2BBXS')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'2BTSB','ET  ','2BTSB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'4B4WDB','AV  ','4B4WDB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'4BB','EP  ','4BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'4BBXS','VY  ','4BBXS')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'6BB','FR  ','6BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'PFMR','PF  ','PFMR')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'T2B4WDB','BS  ','T2B4WDB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'T2BB','HP  ','T2BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'T2BTSB','ET  ','T2BTSB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'T4BB','EP  ','T4BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','AU','B',NULL,'T6BB','FR  ','T6BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'2BB','HT  ','2BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'2BBXS','2BBX','2BBXS')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'2BTSB','EL  ','2BTSB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'4B4WDB','4B4W','4B4WDB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'4BBXS','VY  ','4BBXS')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'4BMS','MX  ','4BMS')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('FTI','THL','NZ','B',NULL,'6BB','FR  ','6BB')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'2BM','2M','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'2BTSM','2B','Spirit 2 T/S')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'4BM','4B','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'6BM','6B','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'CCAR','M1','Compact Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'FCAR','M3','Large Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'FWAR','M4','Wagon')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'ICAR','M2','Intermediate Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'LVAR','M5','People Mover')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','M',NULL,'PFAR','PA','4WD')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'2BM','2M','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'2BTSM','2B','Spirit 2 T/S')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'4BM','4B','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'6BM','6B','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'CCAR','M1','Compact Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'FCAR','M3','Large Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'FWAR','FW','Wagon')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'ICAR','M2','Intermediate Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'IVAR','IV','Minibus')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'T2BM','2M','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'T2BTSM','2B','Spirit 2 T/S')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'T4BM','4B','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','M',NULL,'T6BM','6B','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'2BB','HT','Hi-Top Overlander')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'2BBXS','RK','Rookie')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'2BTSB','EL','Elite Campervan')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'4B4WDB','AV','Adventurer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'4BB','MX','Explorer Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'4BBXS','VY','Voyager')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','NZ','B',NULL,'6BB','FR','Frontier')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'2B4WDB','BS','Bushcamper')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'2BB','HP','HiTop')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'2BBX','BX','Pioneer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'2BBXS','RK','Rookie')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'2BTSB','ET','Elite')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'4B4WDB','AV','Adventurer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'4BB','EP','Explorer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'4BBXS','VY','Voyager')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'6BB','FR','Frontier')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'PFMR','PF','4WD Car')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'T2B4WDB','BS','Bushcamper')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'T2BB','HP','Hitop')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'T2BTSB','ET','Elite')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'T4BB','EP','Explorer/Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('CAF','THL','AU','B',NULL,'T6BB','FR','Country Club')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','M',NULL,'2BM','C2PMNNZ ','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','M',NULL,'2BTSM','C2EMRNZ ','Spirit 2 Delux')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','M',NULL,'4BM','C4LMRNZ ','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','M',NULL,'6BM','C6LMRNZ ','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','M',NULL,'2BM','B2PMNAU ','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','M',NULL,'2BTSM','B2EMRAU ','Spirit 2 T/S')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','M',NULL,'4BM','B4LMRAU ','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','M',NULL,'6BM','B6LMRAU ','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','B',NULL,'2BB','HITOPNZ ','Hi-Top Easy Rider Campervan')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','B',NULL,'4BB','EXPL-NZ ','Explorer Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','NZ','B',NULL,'6BB','6BB--NZ ','Frontier')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'2B4WDB','BMAN-AU ','Bushcamper')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'2BB','2BMN-AU ','Hi-Top Easy Rider Campervan')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'2BBX','2BBX-AU ','Pioneer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'2BTSB','2EMN-AU ','Elite Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'4B4WDB','4B4WBAU ','Adventurer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'4B4WDB','4B4WBAU ','Adventurer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'4BB','4BMS-AU ','Explorer Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'4BBXS','4BBX-AU ','Voyager')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER','THL','AU','B',NULL,'6BB','6BB--AU ','Frontier')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','M',NULL,'2BM','C2PMNNZ ','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','M',NULL,'2BTSM','C2EMRNZ ','Spirit 2 Delux')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','M',NULL,'4BM','C4LMRNZ ','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','M',NULL,'6BM','C6LMRNZ ','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','M',NULL,'2BM','B2PMNAU ','Spirit 2')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','M',NULL,'2BTSM','B2EMRAU ','Spirit 2 T/S')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','M',NULL,'4BM','B4LMRAU ','Spirit 4')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','M',NULL,'6BM','B6LMRAU ','Spirit 6')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','B',NULL,'2BB','HITOPNZ ','Hi-Top Easy Rider Campervan')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','B',NULL,'4BB','EXPL-NZ ','Explorer Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','NZ','B',NULL,'6BB','6BB--NZ ','Frontier')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'2B4WDB','BMAN-AU ','Bushcamper')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'2BB','2BMN-AU ','Hi-Top Easy Rider Campervan')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'2BBX','2BBX-AU ','Pioneer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'2BTSB','2EMN-AU ','Elite Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'4B4WDB','4B4WBAU ','Adventurer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'4B4WDB','4B4WBAU ','Adventurer')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'4BB','4BMS-AU ','Explorer Motorhome')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'4BBXS','4BBX-AU ','Voyager')

INSERT INTO [FlexExportVehicleMappings]
([FvmFlxCode],[FvmComCode],[FvmCtyCode],[FvmBrdCode],[FvmTravelYear],[FvmPrdShortName],[FvmVehicleCode],[FvmDescription])
VALUES ('DER2','THL','AU','B',NULL,'6BB','6BB--AU ','Frontier')

