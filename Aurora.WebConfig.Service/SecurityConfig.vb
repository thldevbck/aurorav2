﻿Imports System.Web
Imports System.Web.Configuration
Imports System.Configuration

Imports Microsoft.VisualBasic
Imports System.IO

Public Class SecurityConfig
    Inherits ConfigurationSection


#Region "REV:mia 30June2015 Encryption of WebConfig"
    Public Shared Function EncryptConnectionString(Optional webPath As String = "~/") As Boolean
        Try
            Dim WebEncryptionHasBeenApplied As Boolean = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("WebEncryptionHasBeenApplied"))
            If (WebEncryptionHasBeenApplied = True) Then
                Aurora.Common.Logging.LogDebug("EncryptConnectionString", "The connectionString has already been encrypted, will SKIP the Encrypt execution")
                Return True
            End If

            Dim config As System.Configuration.Configuration = WebConfigurationManager.OpenWebConfiguration(webPath)
            Aurora.Common.Logging.LogDebug("EncryptConnectionString", config.FilePath)

            Dim section As ConfigurationSection = config.GetSection("connectionStrings")
            If Not section.SectionInformation.IsProtected Then
                config.AppSettings.Settings.Item("WebEncryptionHasBeenApplied").Value = "True"
                section.SectionInformation.ProtectSection("RsaProtectedConfigurationProvider")
                config.Save()
            End If
        Catch ex As Exception
            Aurora.Common.Logging.LogError("EncryptConnectionString Error", ex.Message)
            Return False
        End Try
        Return True
    End Function

    Public Shared Function DecryptConnectionString(Optional webPath As String = "~/") As Boolean
        Try
            Dim WebEncryptionHasBeenApplied As Boolean = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings("WebEncryptionHasBeenApplied"))
            If (WebEncryptionHasBeenApplied = False) Then
                Aurora.Common.Logging.LogDebug("DecryptConnectionString", "The connectionString is not encrypted, will SKIP the Decrypt execution")
                Return True
            End If

            Dim config As Configuration = WebConfigurationManager.OpenWebConfiguration(webPath)
            Aurora.Common.Logging.LogDebug("DecryptConnString", config.FilePath)
            Dim section As ConfigurationSection = config.GetSection("connectionStrings")
            If section.SectionInformation.IsProtected Then
                config.AppSettings.Settings.Item("WebEncryptionHasBeenApplied").Value = "False"
                section.SectionInformation.UnprotectSection()
                config.Save()
            End If
        Catch ex As Exception
            Aurora.Common.Logging.LogError("DecryptConnectionString Error", ex.Message)
            Return False
        End Try
        Return True
    End Function
    
#End Region
End Class
