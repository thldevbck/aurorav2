﻿''Imports Aurora.Booking.Data
Imports Aurora.BookingExperience.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Data
Imports System.Net.Mail
Imports System.IO
Imports System.Configuration

Public Class BookingExperiences
    Public Shared Function CallTicketsSummary(RentalId As String) As DataSet
        Return DataRepository.CallTicketsSummary(RentalId)
    End Function

    Public Shared Function LoadStaticData(Country As String) As DataSet
        Return DataRepository.LoadStaticData(Country)
    End Function

    Public Shared Function GetCustomerInfo(BookingId As String, RentalId As String) As DataSet
        Return DataRepository.GetCustomerInfo(BookingId, RentalId)
    End Function

    Public Shared Function GetCallContactHistory(ExpCallId As Integer, RentalId As String) As DataSet
        Return DataRepository.GetCallContactHistory(ExpCallId, RentalId)
    End Function

    Public Shared Function GetPurchaseHistory(ExpCallId As Integer) As DataSet
        Return DataRepository.GetPurchaseHistory(ExpCallId)
    End Function

    Public Shared Function InsertRecordInHistory(ExpCallRntId As String, ExpCallStatusId As String, ExpCallOutboundCallDt As DateTime, ExpCallNotes As String, AddUsrId As String, ExpCallPhNumber As String) As String
        Return DataRepository.InsertRecordInHistory(ExpCallRntId, ExpCallStatusId, ExpCallOutboundCallDt, ExpCallNotes, AddUsrId, ExpCallPhNumber)
    End Function

    Public Shared Function UpdateRecordAndCloneInHistory(ExpCallId As String, ExpCallStatusId As String, ExpCallOutboundCallDt As DateTime, ExpCallNotes As String, ModUsrId As String, ExpCallPhNumber As String) As String
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                'Dim result As String = DataRepository.CloneRecordInHistory(ExpCallId, ExpCallStatusId)
                'If (result.Contains("OK")) Then
                '    Dim expHistId As String = result
                Dim result As String = DataRepository.UpdateRecordInHistory(ExpCallId, ExpCallStatusId, ExpCallOutboundCallDt, ExpCallNotes, ModUsrId, ExpCallPhNumber)
                '    DataRepository.UpdateCallHistoryForModifiedUser(ExpCallId, expHistId.Split("-")(1), ModUsrId)
                'End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Return "ERROR"
            End Try
        End Using
        Return "OK"
    End Function

    Public Shared Function GetCallContactHistories(ExpCallId As Integer) As DataSet
        Return DataRepository.GetCallContactHistories(ExpCallId)
    End Function

    Public Shared Function InsertExperienceInHistory(ExpCallId As String, ExpItemId As String, ExpCallTktPrice As Decimal, ExpCallTktQty As Integer, ExpCallTktTotal As Decimal, ExpCallTktDateTime As DateTime, ExpCallTktSupInfo As String, ExpCallTktStatusId As String, ExpCallTktResRefNum As String, ExpCallTktPrdLinkId As String, AddUsrId As String, ExpCallTktSupplierRefNum As String) As String
        Dim result As String = ""
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                result = DataRepository.InsertExperienceInHistory(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, AddUsrId, ExpCallTktSupplierRefNum)
                If (result.Contains("ERROR")) Then
                    Throw New Exception(result)
                End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Return "ERROR: " & ex.Message.Replace("ERROR", "")
            End Try
        End Using

        Return "OK"
    End Function

    Public Shared Function GetExperienceSummary(ExpCallId As Integer) As DataSet
        Return DataRepository.GetExperienceSummary(ExpCallId)
    End Function

    Public Shared Function GetSingleExperienceCall(ExpCallId As Integer) As DataSet
        Return DataRepository.GetSingleExperienceCall(ExpCallId)
    End Function

    Public Shared Function GetSingleExperienceCallTickets(ExpCallTktId As Integer) As DataSet
        Return DataRepository.GetSingleExperienceCallTickets(ExpCallTktId)
    End Function

    Public Shared Function UpdateSingleExperienceCallTickets(ExpCallId As String, ExpItemId As String, ExpCallTktPrice As Decimal, ExpCallTktQty As Integer, ExpCallTktTotal As Decimal, ExpCallTktDateTime As DateTime, ExpCallTktSupInfo As String, ExpCallTktStatusId As String, ExpCallTktResRefNum As String, ExpCallTktPrdLinkId As String, ModUsrId As String, ExpCallTktId As Integer, ExpCallTktSupplierRefNum As String) As String
        ''Return Data.DataRepository.UpdateSingleExperienceCallTickets(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, ModUsrId, ExpCallTktId)
        Dim result As String = ""
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                result = DataRepository.UpdateSingleExperienceCallTickets(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, ModUsrId, ExpCallTktId, ExpCallTktSupplierRefNum)
                If (result.Contains("ERROR")) Then
                    Throw New Exception(result)
                End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Return "ERROR: " & ex.Message.Replace("ERROR", "")
            End Try
        End Using

        Return "OK"
    End Function

    Public Shared Function GetExperienceSupplierAndItems(ExpCallId As Integer, ExpCallTktId As Integer) As DataSet
        Return DataRepository.GetExperienceSupplierAndItems(ExpCallId, ExpCallTktId)
    End Function

    Public Shared Function ConfirmationSummary(BookingId As String, RentalId As String, userId As String) As DataSet
        Return DataRepository.ConfirmationSummary(BookingId, RentalId, userId)
    End Function

    Public Shared Function ConfirmationSummary(BookingId As String, RentalId As String) As DataSet
        Return DataRepository.ConfirmationSummary(BookingId, RentalId)
    End Function

    Public Shared Function GetSubItems(ExpItemId As Integer) As DataSet
        Return DataRepository.GetSubItems(ExpItemId)
    End Function

    Public Shared Function GetTags(ExpCallTktId As Integer, ExpCallId As Integer) As DataSet
        Return DataRepository.GetTags(ExpCallTktId, ExpCallId)
    End Function

    Public Shared Function GetCountry(rentalId As String) As String
        Return DataRepository.GetCountry(rentalId)
    End Function

    Public Shared Function GetTagsForSupplierAndExperience(SupId As Integer) As DataSet
        Return DataRepository.GetTagsForSupplierAndExperience(SupId)
    End Function

#Region "rev:mia https://thlonline.atlassian.net/browse/AURORA-610 - as a Finance user I want to see invoice details on all Experience vouchers"
    Public Shared Function GetExperienceChargeDetails(BpdId As String) As DataSet
        Return DataRepository.GetExperienceChargeDetails(BpdId)
    End Function
#End Region

#Region "rev: mia 29-feb-2016 https://thlonline.atlassian.net/browse/AURORA-745"
    Private Shared Function VoucherHtmlTemplate(voucher As String, _
                              supplierRef As String, _
                              reservationRef As String, _
                              customerName As String, _
                              experienceItem As String, _
                              quantityAndDesc As String, _
                              experienceDate As String, _
                              experienceTime As String, _
                              supplierName As String, _
                              supplierAddress As String, _
                              supplierOperatingHours As String, _
                              supplierPhoneNumber As String, _
                              BrandName As String, _
                              termsAndConditions As String, _
                              TollPhone As String, _
                              Optional DateIssued As String = "", _
                              Optional CustomerAddress As String = "", _
                              Optional TotalExcludingGST As String = "", _
                              Optional TotalIncludingGST As String = "", _
                              Optional GST As String = "", _
                              Optional GSTCountry As String = "", _
                              Optional BrandFooterText As String = "", _
                              Optional ConditionValidity As String = "") As String ''rev:mia 16-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1167

        Dim sb As New Text.StringBuilder
        '' sb.Append("<!DOCTYPE html>")
        sb.Append("<html>")
        sb.Append("<head>")
        sb.Append("<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />")
        sb.Append("<title></title>")
        sb.Append("<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700,700italic' rel='stylesheet' type='text/css'>")
        sb.Append("<link rel='stylesheet' type='text/css' href='../include/css/MediaExperience.css'>")

        sb.Append("</head>")

        sb.Append("<body>")
        sb.Append("<div style='padding: 30px; max-width: 600px; margin: auto;'>")
        sb.Append("<img style='float: left; width: 100%; height: auto; display: block;' src='http://res.cloudinary.com/thl/image/upload/v1447274811/tcx/partner-network/voucher/generic/scissor.png' /><br/>")
        sb.Append("<img id=""headerImage"" style='float: left; width: 100%; height: auto; display: block;' src='http://res.cloudinary.com/diosasbwh/image/upload/v1444614139/voucher/header.png'/>")
        sb.Append("<div style='float: left; width: 100%; padding: 10px 0; border-bottom: 1px solid #ddd'>")
        sb.Append("<p id=""heading"" style='margin: 24px 0; width: 100%; text-align: center; text-transform: uppercase; letter-spacing: 1px; font-family: ""Open sans"", arial; font-weight: 600; font-size: 14px;'>Tourism Holding Limited</p>")
        sb.Append("<p style='margin: 12px 0; font-family: 'Open sans', arial; font-weight: 400;'></p></div>​")

        sb.Append("<div style='float: left; width: 100%; padding: 10px 0; border-bottom: 1px solid #ddd'><p id=""heading"" style='margin: 24px 0; width: 100%; text-align: center; text-transform: uppercase; letter-spacing: 1px; font-family: ""Open sans"", arial; font-weight: 600; font-size: 24px;'>Experience Voucher</p><p style='margin: 12px 0; font-family: ""Open sans"", arial; font-weight: 400;'></p></div>​")
        sb.AppendFormat("<div style='float: left; width: 100%; padding: 10px 0; border-bottom: 1px solid #ddd'><p id=""heading"" style='margin: 24px 0; width: 100%; text-align: center; text-transform: uppercase; letter-spacing: 1px; font-family: ""Open sans"", arial; font-weight: 600; font-size: 14px;'>{0}</p><p style='margin: 12px 0; font-family: ""Open sans"", arial; font-weight: 400;'></p></div>​", GSTCountry)
        sb.Append("<div style='float: left; width: 100%; padding: 12px 0 18px; border-bottom: 1px solid #ddd'>")
        sb.Append("<p style='margin: 12px 0; width: 100%; text-transform: uppercase; letter-spacing: 1px; font-family: ""Open sans"", arial; font-weight: 600; font-size: 24px;'>Reservation Details</p>")
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left; font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Date Issued:</p> <p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400; '>{0}</p> </div>", DateIssued)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left; font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Voucher Number:</p> <p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400; '>{0}</p> </div>", voucher)

        If (Not String.IsNullOrEmpty(supplierRef)) Then
            sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Supplier Reference:</p> <p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p> </div>", supplierRef)
        End If

        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Reservation Reference:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", reservationRef)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Customer Name:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", customerName)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Customer Address:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", CustomerAddress)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Experience:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", experienceItem)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Ticket:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", quantityAndDesc)

        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Total (includes GST):</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", TotalIncludingGST)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>GST Content:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", GST)
        If Not String.IsNullOrEmpty(experienceDate) Then sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Date:</p><p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", experienceDate)
        If Not String.IsNullOrEmpty(experienceTime) Then sb.AppendFormat("<div style='width: 100%; float: left;'> <p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Time:</p> <p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", experienceTime)

        sb.Append("</div>")


        sb.Append("<div style='float: left; width: 100%; padding: 12px 0 18px; border-bottom: 1px solid #ddd'>")
        sb.Append("<p style='margin: 12px 0; width: 100%; text-transform: uppercase; letter-spacing: 1px; font-family: ""Open sans"", arial; font-weight: 600; font-size: 24px;'> Experience Details</p> <div style='float: left; width: 100%;'>")
        sb.AppendFormat("<div style='width: 100%; float: left;'> <p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Experience Name:</p><p style='margin: 6px 0;'>{0}</p></div>", supplierName)
        sb.AppendFormat("<div style='width: 100%; float: left;'><p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Experience Address:</p><p style='margin: 6px 0;'>{0}</p> </div>", supplierAddress)
        If Not String.IsNullOrEmpty(supplierOperatingHours) Then
            sb.Append("<div style='width: 100%; float: left;'>")
            sb.Append("<p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Operating hours:</p>")
            sb.AppendFormat("<p style='margin: 6px 0; float:left; font-family: ""Open sans"", arial; font-weight: 400;'> {0} <br/> {1} <br/> {2}  </p>  </div>", supplierOperatingHours, "", "")
        End If
        ''rev:mia 14-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1167
        If (Not String.IsNullOrEmpty(ConditionValidity)) Then
            sb.AppendFormat("<div style='width: 100%; float: left;'> <p style='margin: 6px 0; float: left;  font-family: ""Open sans"", arial; font-weight: 600; padding-right: 12px;'>Conditions:</p> <p style='margin: 6px 0; font-family: ""Open sans"", arial; font-weight: 400;'>{0}</p></div>", ConditionValidity)
        End If

        sb.Append("</div>")
        sb.Append("</div>")

        sb.Append("<div style='margin-bottom: 12px; float: left; width: 100%; padding: 12px 0 28px;'>")
        If (Not String.IsNullOrEmpty(termsAndConditions)) Then
            If (termsAndConditions.IndexOf("http") = -1) Then
                termsAndConditions = "http://" & termsAndConditions
            End If
            sb.AppendFormat("<a id=""supplierHref"" style='display: block; clear: both; margin-top: 12px; font-family: ""Open sans"", arial; font-weight: 400;' href='{0}' target='_blank'>Terms and Conditions</a>", termsAndConditions)
        End If

        sb.AppendFormat("<p style='margin: 24px 0 12px; width: 100%; font-family: ""Open sans"", arial; font-weight: 400; font-size: 14px;'> Please contact us on {0} or <a href='mailto:hosts@thlonline.com'>hosts@thlonline.com</a> should you have any enquiries. </p>", TollPhone)
        sb.Append("<p style='width: 100%; margin: 6px 0; float:left; font-family: ""Open sans"", arial; font-weight: 400;'> Kind Regards </p>")
        sb.AppendFormat("<p style='width: 100%; margin: 6px 0 0 ; float:left; font-family: ""Open sans"", arial; font-weight: 600;'>The Crew at {0}</p>", BrandFooterText)
        sb.Append("<div style='width: 100%; height: 40px; display: block; float: left;'></div>")
        sb.Append("<a id=""thlHref"" style='display: block; float: left; font-family: ""Open sans"", arial;' href='http://www.thlonline.com/' target='_blank'>Tourism Holdings Limited</a>")
        sb.Append("</div>")
        sb.Append("</div>")

        sb.Append("</body>")
        sb.Append("</html>")

        Return sb.ToString
    End Function

    Public Shared Function BuildVouchers(bookingId As String, rentalId As String, usercode As String, GstCountry As String, BrandFooterText As String) As String
        Dim dsInfo As DataSet = BookingExperiences.ConfirmationSummary(bookingId, rentalId)
        Dim dsCus As DataSet = BookingExperiences.GetCustomerInfo(bookingId, rentalId)

        Dim nameOfClient As String = ""
        Dim emailOfClient As String = ""

        Dim customerAddress As String = ""
        If (dsCus.Tables(0).Rows.Count - 1 = -1) Then
            Return String.Empty
        Else
            For Each cusrow As DataRow In dsCus.Tables(0).Rows
                If (String.IsNullOrEmpty(nameOfClient)) Then
                    If Not cusrow("CName") Is Nothing Then
                        nameOfClient = cusrow("CName").ToString
                    End If
                End If
                If (String.IsNullOrEmpty(emailOfClient)) Then
                    If Not cusrow("EmailAddress") Is Nothing Then
                        emailOfClient = cusrow("EmailAddress").ToString
                    End If
                End If
                If (String.IsNullOrEmpty(customerAddress)) Then
                    If Not cusrow("CustomerAddress") Is Nothing Then
                        customerAddress = cusrow("CustomerAddress").ToString
                    End If
                End If
            Next

        End If

        Dim ExpItemId As String = ""
        Dim voucher As String = ""                  ''OK
        Dim supplierRef As String = ""              ''OK
        Dim reservationRef As String = ""           ''OK
        Dim experienceItem As String = ""           ''OK
        Dim quantityAndDesc As String = ""          ''OK
        Dim experienceDate As String = ""           ''OK
        Dim experienceTime As String = ""           ''OK
        Dim supplierName As String = ""             ''OK
        Dim supplierAddress As String = ""          ''OK
        Dim supplierOperatingHours As String = ""   ''OK
        Dim supplierPhoneNumber As String = ""
        Dim supplierOperatingDays As String = ""    'OK'    
        Dim brandName As String = ""                ''OK    
        Dim termsAndCondition As String = ""        ''OK
        Dim TollPhone As String = ""                ''OK
        Dim conditionvalidity As String = ""
        Dim invoiceIssuedDate As String = ""
        Dim gstIncluding As String = ""
        Dim gstExcluding As String = ""
        Dim bpdId As String = ""
        Dim gstAmount As Double = 0.0
        Dim gstAmountText As String = ""
        Dim currency As String = ""

        Dim sb As New Text.StringBuilder
        Dim ctr As Integer = 1
        For Each myrow As DataRow In dsInfo.Tables(0).Rows
            ExpItemId = myrow("ExpItemId")
            voucher = myrow("Voucher")
            supplierRef = myrow("SupplierRef")
            reservationRef = myrow("ReservationRef")
            experienceItem = myrow("ExperienceItem")
            quantityAndDesc = myrow("QuantityAndDesc")
            TollPhone = myrow("TollPhone")
            If (Not IsDBNull(myrow("ExperienceDate"))) Then
                experienceDate = CDate(myrow("ExperienceDate").ToString).ToString("dd/MM/yyyy")
                experienceTime = CDate(myrow("ExperienceDate").ToString).ToString("hh:mm")
            End If

            supplierName = myrow("SupplierName")
            supplierAddress = myrow("SupplierAddress")
            supplierOperatingHours = myrow("SupplierOperatingHours")
            supplierOperatingDays = myrow("SupplierOperatingDays")
            brandName = supplierName
            termsAndCondition = myrow("TermsAndCondition")
            ''rev:mia 16-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1167
            conditionvalidity = myrow("ConditionValidity")

            ''REV:MIA 25-JAN-2016 - as a Finance user I want to see invoice details on all Experience vouchers
            invoiceIssuedDate = myrow("DateAdded")
            bpdId = myrow("BpdId")
            Dim dsSubItems As DataSet = BookingExperiences.GetSubItems(CInt(ExpItemId))
            Dim dsChargeItems As DataSet = BookingExperiences.GetExperienceChargeDetails(bpdId)
            If (dsChargeItems.Tables.Count - 1 <> -1) Then
                gstAmount = Convert.ToDouble(dsChargeItems.Tables(0).Rows(0)("TotGst"))
                currency = dsChargeItems.Tables(0).Rows(0)("Curr")
                gstIncluding = dsChargeItems.Tables(0).Rows(0)("TotOw1")
                gstExcluding = Convert.ToDouble(gstIncluding) - gstAmount
                gstIncluding = currency & "$ " & gstIncluding
                gstExcluding = currency & "$ " & gstExcluding
                gstAmountText = currency & "$ " & gstAmount.ToString
            End If

            If (dsSubItems.Tables.Count - 1 <> -1) Then
                If (dsSubItems.Tables(0).Rows.Count - 1 <> -1) Then
                    For Each subrow As DataRow In dsSubItems.Tables(0).Rows
                        experienceItem = subrow("Name")
                        sb.AppendFormat(VoucherHtmlTemplate(voucher, supplierRef, reservationRef, nameOfClient, experienceItem, quantityAndDesc, experienceDate, experienceTime, supplierName, supplierAddress, supplierOperatingHours, supplierPhoneNumber, brandName, termsAndCondition, TollPhone, invoiceIssuedDate, customerAddress, gstExcluding, gstIncluding, gstAmountText, GstCountry, BrandFooterText, conditionvalidity))
                        ctr = ctr + 1
                    Next
                Else
                    sb.AppendFormat(VoucherHtmlTemplate(voucher, supplierRef, reservationRef, nameOfClient, experienceItem, quantityAndDesc, experienceDate, experienceTime, supplierName, supplierAddress, supplierOperatingHours, supplierPhoneNumber, brandName, termsAndCondition, TollPhone, invoiceIssuedDate, customerAddress, gstExcluding, gstIncluding, gstAmountText, GstCountry, BrandFooterText, conditionvalidity))
                    ctr = ctr + 1
                End If
            Else
                sb.AppendFormat(VoucherHtmlTemplate(voucher, supplierRef, reservationRef, nameOfClient, experienceItem, quantityAndDesc, experienceDate, experienceTime, supplierName, supplierAddress, supplierOperatingHours, supplierPhoneNumber, brandName, termsAndCondition, TollPhone, invoiceIssuedDate, customerAddress, gstAmountText, "", "", GstCountry, BrandFooterText, conditionvalidity))
                ctr = ctr + 1
            End If
        Next
        Return sb.ToString
    End Function
#End Region

#Region "rev:mia 22-march-2016 https://thlonline.atlassian.net/browse/AURORA-754"
    Public Shared Function GetBrandFullName(brand As String) As String
        Dim brandname As String = ""
        Select Case brand.ToUpper
            Case "B"
                brandname = "Britz"
            Case "Y"
                brandname = "Mighty"
            Case "M"
                brandname = "Maui"
            Case "U"
                brandname = "United"
            Case "A"
                brandname = "Alpha"
            Case "Q"
                brandname = "Kea"
            Case Else
                brandname = "THL"
        End Select
        Return brandname
    End Function

    'Private Shared _brandFooterText As String
    'Public Shared Property BrandFooterText As String
    '    Get
    '        Return _brandFooterText
    '    End Get
    '    Set(value As String)
    '        Select Case value.ToUpper
    '            Case "B"
    '                _brandFooterText = "Britz"
    '            Case "Y"
    '                _brandFooterText = "Mighty"
    '            Case "M"
    '                _brandFooterText = "Maui"
    '            Case "U"
    '                _brandFooterText = "United"
    '            Case "A"
    '                _brandFooterText = "Alpha"
    '            Case "Q"
    '                _brandFooterText = "Kea"
    '            Case Else
    '                _brandFooterText = "THL"
    '        End Select
    '    End Set
    'End Property
#End Region

End Class
