'' Change Log 
''  2.7.8 - Shoel - fixed the issue of extension creation on checkin making the transaction rollback :)
'' 18.8.8 - Shoel - fixes for popup and forced validations
'' 20.8.8 - Shoel - even more fixes for CICO + some more fixes for sup override
'' 21.8.8 - Shoel - some more fixes for sup override

Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Reservations
Imports Aurora.Telematics.Service
Imports Aurora.Telematics.Service.Services
Imports System.Configuration

Public Class BookingCheckInCheckOut

    Private Shared Property ConnectedSessionID As Guid




    Public Shared Function GetDataForCheckInCheckOut(ByVal BookingId As String, _
                                   ByVal RentalId As String, _
                                   ByVal UserCode As String, _
                                   ByVal ScreenValue As Integer, _
                                   ByVal DVASSUnitNumber As String, _
                                   ByRef RentalCountryCode As String) As XmlDocument

        'shoels version

        'prog params
        Dim oRentalData As System.Xml.XmlDocument
        Dim sRentalStatus As String
        Dim sBookingRentalNumber As String
        Dim nCountryCode As Integer = 0
        Dim sUnitNumber As String = ""
        Dim sDVASSStatus As String = ""
        'shoels version

        Try

            If DVASSUnitNumber.Trim().Equals(String.Empty) Then

                ' 2 executing an sp and getting xml data
                oRentalData = DataRepository.GetDataForCheckInCheckOut(BookingId, "CKOCKIGET", RentalId)

                sRentalStatus = oRentalData.SelectSingleNode("//Rental/Status").InnerText
                sBookingRentalNumber = Trim(oRentalData.SelectSingleNode("//Rental/BooNum").InnerText) + "/" + Trim(oRentalData.SelectSingleNode("//Rental/Rental").InnerText)
                RentalCountryCode = oRentalData.SelectSingleNode("//Rental/CtyCode").InnerText

                If (sRentalStatus = "KK" Or sRentalStatus = "NN") And oRentalData.SelectSingleNode("//Rental/DVASSCall").InnerText = 1 Then
                    If Trim(UCase(RentalCountryCode)) = "AU" Then
                        nCountryCode = 0
                    Else
                        nCountryCode = 1
                    End If
                    'dvass call coming up!

                    sUnitNumber = GetDVASSUnitNumber(sBookingRentalNumber, sDVASSStatus, nCountryCode, UserCode)

                    If UCase(Trim(sDVASSStatus)) <> "SUCCESS" Then
                        If InStr(sDVASSStatus, "101") = 0 Then
                            Throw New Exception("DVASS ERROR - DVASS STATUS = " & sDVASSStatus)
                        End If
                    End If

                End If
            Else
                sUnitNumber = Trim(DVASSUnitNumber)

            End If

            Return DataRepository.GetLocationStatusForCheckInCheckOut(BookingId, RentalId, UserCode, ScreenValue, sUnitNumber)
        Catch ex As Exception
            Dim oErrorXml As New XmlDocument
            oErrorXml.LoadXml("<Error><Message>" & ex.Message & "</Message><Type>App</Type></Error>")
            Return oErrorXml
        End Try


    End Function



    Public Shared Function ManagePreCheckOut(ByVal ScreenData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String) As XmlDocument

        Dim sBookingId, sRentalId, sUnit As String
        Dim oReturnXml As XmlDocument = New XmlDocument
        Dim oResult As XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                ' do in transaction
                'oContext = GetObjectContext()
                'load screendata xml

                sBookingId = ScreenData.DocumentElement.SelectSingleNode("BooId").InnerText
                sRentalId = ScreenData.DocumentElement.SelectSingleNode("RntId").InnerText
                sUnit = ScreenData.DocumentElement.SelectSingleNode("UnitNo").InnerText

                'sp execution
                oResult = DataRepository.ManagePreCheckOut(ScreenData, UserCode, ProgramName)
                If oResult.InnerText.Trim().ToUpper() <> "SUCCESS" And Not oResult.InnerText.Trim().Equals(String.Empty) Then
                    Throw New Exception(oResult.InnerText)
                End If
                oReturnXml = GetDataForCheckInCheckOut(sBookingId, sRentalId, UserCode, 0, sUnit, String.Empty)
                oTransaction.CommitTransaction()
                If Not oTransaction.IsClosed Then
                    oTransaction.Dispose()
                End If

                '        ManagePreCheckOut = GetForCheckInCheckOut(vBooId, vRntId, sUsercode, 0, sUnit)

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                If Not oTransaction.IsClosed Then
                    oTransaction.Dispose()
                End If

                oReturnXml.LoadXml("<Error><Message>" + ex.Message + "</Message></Error>")

            End Try

        End Using


        Return oReturnXml
        'ErrorHandler:
        '        sError = Err.Description
        '        sErrorNo = Err.Number
        '        ManagePreCheckOut = "<Root>" & GetErrorTextFromResource(True, sErrorNo, "System", sError) & "<Data></Data></Root>"
    End Function



    Public Shared Function GetDVASSUnitNumber(ByVal BookingRentalNumber As String, _
                                              ByRef DvassStatus As String, _
                                              ByVal SelectedCountry As Integer, _
                                              Optional ByVal UserCode As String = "") As String


        'shoels data

        'Dim BookingRentalNumber
        'Dim DvassStatus
        'Dim UserCode
        'Dim DvassMessage
        'Dim SelectedCountry As Integer

        'local

        Dim sVehicleID As String = ""
        Dim sRentalId As String
        Dim sProductId As String
        Dim sCkoLocation As String
        Dim sCkoDate As String
        Dim nCkoDayPart As Integer
        Dim sCkiLocation As String
        Dim sCkiDate As String
        Dim nCkiDayPart As Integer
        Dim nRevenue As Double
        Dim nPriority As Double
        Dim nVisibility As Integer
        Dim sVehicleList As String
        Dim nForceFlag As Long

        'shoel
        Try

            'make the dvass call
            Aurora.Reservations.Services.AuroraDvass.GetUnitNumber(BookingRentalNumber, 1, sVehicleID, DvassStatus, SelectedCountry)

            If DvassStatus <> "SUCCESS" Then
                If InStr(DvassStatus, "HARD-FAIL") <> 0 Then
                    'db call
                    Dim oXmlData As XmlDocument
                    oXmlData = DataRepository.GetDataRentalAddForCheckInCheckOut(BookingRentalNumber)

                    DvassStatus = String.Empty
                    'sRentalId = CStr(IIf(Len(oXmlData.SelectSingleNode("//Booking/RentalId").InnerText) = 0, tmpIdentity, oXmlData.SelectSingleNode("//Booking/RentalId").InnerText))
                    sRentalId = CStr(oXmlData.SelectSingleNode("//Booking/RentalId").InnerText)
                    sProductId = oXmlData.SelectSingleNode("//Booking/PrdId").InnerText

                    sCkoLocation = oXmlData.SelectSingleNode("//Booking/CoLoc").InnerText
                    sCkoDate = oXmlData.SelectSingleNode("//Booking/CoDate").InnerText
                    nCkoDayPart = CInt(IIf(oXmlData.SelectSingleNode("//Booking/CkoDayPart").InnerText = "AM", 0, 1))
                    sCkiLocation = oXmlData.SelectSingleNode("//Booking/CiLoc").InnerText
                    sCkiDate = oXmlData.SelectSingleNode("//Booking/CiDate").InnerText
                    nCkiDayPart = CInt(IIf(oXmlData.SelectSingleNode("//Booking/CkiDayPart").InnerText = "AM", 0, 1))
                    nRevenue = CDbl(IIf(Len(oXmlData.SelectSingleNode("//Booking/Revenue").InnerText) = 0, 0, oXmlData.SelectSingleNode("//Booking/Revenue").InnerText))
                    nPriority = CDbl(IIf(Len(oXmlData.SelectSingleNode("//Booking/Priority").InnerText) = 0, 1, oXmlData.SelectSingleNode("//Booking/Priority").InnerText))
                    nVisibility = CInt(IIf((Len(oXmlData.SelectSingleNode("//Booking/Visibility").InnerText) = 0), 10, oXmlData.SelectSingleNode("//Booking/Visibility").InnerText))
                    sVehicleList = oXmlData.SelectSingleNode("//Booking/VehicleRanges").InnerText
                    nForceFlag = CLng(IIf(Len(oXmlData.SelectSingleNode("//Booking/Force").InnerText) = 0, 0, oXmlData.SelectSingleNode("//Booking/Force").InnerText))

                    'another dvass call  :o

                    Aurora.Reservations.Services.AuroraDvass.RentalAdd(SelectedCountry, sRentalId, sProductId, sCkoLocation, sCkoDate, nCkoDayPart, sCkiLocation, sCkiDate, nCkiDayPart, nRevenue, nPriority, nVisibility, sVehicleList, nForceFlag, DvassStatus, UserCode)
                    If DvassStatus <> "SUCCESS" Then
                        Return ""
                        Exit Function
                    End If

                    'yet another call
                    Aurora.Reservations.Services.AuroraDvass.GetUnitNumber(BookingRentalNumber, True, sVehicleID, DvassStatus, SelectedCountry)

                    If DvassStatus <> "SUCCESS" Then
                        Return ""
                        Exit Function
                    End If
                Else
                    Return ""
                    Exit Function
                End If

            End If

            Return sVehicleID
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Public Shared Function ManageCheckOut(ByVal ScreenData As XmlDocument, _
    ByVal UserCode As String, _
    ByVal ProgramName As String, _
    ByVal ScreenValue As String, _
    ByRef ErrorMessage As String, _
    ByVal CheckOutValidationsDone As Boolean, _
    Optional ByVal SupervisorCode As String = "", _
    Optional ByVal CallDVASS As Object = Nothing) As XmlDocument

        'sPopWrn = "-2147220489,-2147220475,-2147220469"
        'sPopOvr = "-2147220471,-2147220470,-2147220488"

        'gathering variables
        Dim sBookingId, sRentalId, sDVASSCall, sForced, sActivityType, sExternalRef, sUnit, sStartLocation, sExpectedEndLocation, sCountryCode, sOtherAssets As String
        Dim bCrossHire As Boolean
        Dim dtStart, dtExpectedEnd As Date
        Dim nStartOdometer, nExpectedEndOdometer As Integer
        Dim sDVASSStatus As String = ""
        Dim regoNumber As String

        
        Dim oReturnXml As XmlDocument = New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                'main work

                sBookingId = ScreenData.DocumentElement.SelectSingleNode("BooId").InnerText
                sRentalId = ScreenData.DocumentElement.SelectSingleNode("RntId").InnerText
                sDVASSCall = ScreenData.DocumentElement.SelectSingleNode("DVASSCall").InnerText
                sForced = ScreenData.DocumentElement.SelectSingleNode("Forced").InnerText
                bCrossHire = ScreenData.DocumentElement.SelectSingleNode("CrossHr").InnerText
                sActivityType = "Rental"
                sExternalRef = ScreenData.DocumentElement.SelectSingleNode("BooRntNum").InnerText
                sUnit = ScreenData.DocumentElement.SelectSingleNode("UnitNo").InnerText
                sStartLocation = UCase(Trim(ScreenData.DocumentElement.SelectSingleNode("UserLocCode").InnerText))
                dtStart = CDate(ScreenData.DocumentElement.SelectSingleNode("RntCkoWhen").InnerText & " " & ScreenData.DocumentElement.SelectSingleNode("RntCkoTime").InnerText)
                nStartOdometer = CLng(ScreenData.DocumentElement.SelectSingleNode("OddometerOut").InnerText)
                sExpectedEndLocation = UCase(ScreenData.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText)
                dtExpectedEnd = CDate(ScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText & " " & ScreenData.DocumentElement.SelectSingleNode("RntCkiTime").InnerText)
                sCountryCode = UCase(ScreenData.DocumentElement.SelectSingleNode("CtyCode").InnerText)
                regoNumber = ScreenData.DocumentElement.SelectSingleNode("Rego").InnerText
                nExpectedEndOdometer = -1
                sOtherAssets = ""
                

                If ScreenValue Is Nothing OrElse ScreenValue.Trim().Equals(String.Empty) Then
                    ScreenValue = "0"
                End If

                If sForced Is Nothing OrElse sForced.Trim().Equals(String.Empty) Then
                    sForced = "0"
                End If

                ' integrity Check!! :)
                Logging.LogInformation("ManageCheckOut", "Starting Integrity Check")
                If Not Booking.CheckIntegrity(sBookingId, CInt(ScreenData.DocumentElement.SelectSingleNode("RntIntNo").InnerText), CInt(ScreenData.DocumentElement.SelectSingleNode("BooIntNo").InnerText), sRentalId) Then
                    Logging.LogInformation("ManageCheckOut", "Done Integrity Check - Fail")
                    Throw New Exception("ERROR/This rental has been modified by another user. Please refresh the screen.")
                End If
                Logging.LogInformation("ManageCheckOut", "Done Integrity Check - Pass")
                ' integrity Check passed - continue

                ' VALIDATIONS CHECK
                Dim oXml As XmlDocument

                If Not CheckOutValidationsDone Then ' unforced -> validate it!
                    Logging.LogInformation("ManageCheckOut", "Starting Validations. GetPopupData Case=VALIDATEBOOKING")
                    oXml = Common.Data.GetPopUpData(Aurora.Popups.Data.PopupType.VALIDATEBOOKING, sBookingId, sRentalId, UserCode, "", "", UserCode)
                    Logging.LogInformation("ManageCheckOut", "Done Validations. GetPopupData Case=VALIDATEBOOKING. Result xml = " & oXml.OuterXml)

                    If Not oXml.DocumentElement.SelectSingleNode("Error") Is Nothing Then
                        If Not oXml.DocumentElement.SelectSingleNode("Error").InnerText.Trim.Equals(String.Empty) _
                           AndAlso Not oXml.DocumentElement.SelectSingleNode("Error").InnerText.Trim().ToUpper().Contains(UCase("GST calculation advice")) _
                        Then
                            ' some errors
                            Throw New Exception("VALIDATIONERRORS/" & oXml.DocumentElement.SelectSingleNode("Error").InnerText)
                        End If
                    ElseIf Not oXml.DocumentElement.SelectSingleNode("Wrn") Is Nothing Then
                        If Not oXml.DocumentElement.SelectSingleNode("Wrn").InnerText.Trim.Equals(String.Empty) Then
                            ' some warnings
                            Throw New Exception("VALIDATIONWARNINGS/" & oXml.DocumentElement.SelectSingleNode("Wrn").InnerText)
                        End If
                    End If
                End If

                ' VALIDATIONS CHCK done :)

                ' db call
                Logging.LogInformation("ManageCheckOut", "Starting call to DataRepository.ManageCheckOut")
                If SupervisorCode.Trim().Equals(String.Empty) Then
                    oXml = DataRepository.ManageCheckOut(ScreenData, UserCode, ProgramName, ScreenValue)
                Else
                    ' there's an override happening
                    oXml = DataRepository.ManageCheckOut(ScreenData, SupervisorCode, ProgramName, ScreenValue)
                    ' fix for OLMAN overrides not happening!
                    CheckOutValidationsDone = True
                End If
                Logging.LogInformation("ManageCheckOut", "Finished call to DataRepository.ManageCheckOut. Result xml = " & oXml.OuterXml)

                If oXml.DocumentElement.InnerText <> "SUCCESS" And Not oXml.DocumentElement.InnerText.Trim().Equals(String.Empty) Then

                    ' needs a param to show error occured!
                    oReturnXml = DataRepository.GetLocationStatusForCheckInCheckOut(sBookingId, sRentalId, UserCode, ScreenValue, sUnit)
                    ' eg 
                    ' <data>POPUP3/CkoiCusPymt^Check In or Out a rental with unpaid customer charges</data> 

                    'rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129
                    Dim validationerror As String = oXml.DocumentElement.InnerText
                    If (validationerror.IndexOf("PROMPTCONTINUE") <> -1 And CheckOutValidationsDone) Then
                        ''just continue
                    Else
                        Throw New Exception(oXml.DocumentElement.InnerText)
                    End If


                    '<<check this part>>
                End If

                If Not bCrossHire Then
                    Logging.LogInformation("ManageCheckOut", "Starting AIMS Call")
                    'cross hire = false
                    'make aims obj
                    Logging.LogInformation("ManageCheckOut", "Start Initialising AIMS Object")
                    Dim oAimsOjb As New AI.CMaster
                    Logging.LogInformation("ManageCheckOut", "Done Initialising AIMS Object")
                    Dim sAimsReturnValue As Integer
                    Dim sLogInfo As String = ""
                    sLogInfo = "FOR CHECKOUT : F6 ACTIVITY CREATE." & vbNewLine & _
                                                                        "Params:" & vbNewLine & _
                                                                        "______________________________________________" & vbNewLine & _
                                                                        "ExternalRef   := " & sExternalRef & vbNewLine & _
                                                                        "Unit No       := " & sUnit & vbNewLine & _
                                                                        "Branch        := " & sStartLocation & vbNewLine & _
                                                                        "Exchange Date := " & dtStart & vbNewLine & _
                                                                        "Odometer Out  := " & nStartOdometer & vbNewLine & _
                                                                        "Check-In Locn := " & sExpectedEndLocation & vbNewLine & _
                                                                        "Check-In Date := " & dtExpectedEnd & vbNewLine & _
                                                                        "Odometer In   := " & nExpectedEndOdometer & vbNewLine & _
                                                                        "Other Assets  := " & sOtherAssets & vbNewLine

                    If sDVASSCall = "1" Then
                        'dvass call = 1
                        'If sForced = "0" Then
                        ' Change by Shoel : 13.10.08 : AIMS FORCE

                        If CheckOutValidationsDone Or sForced = "1" Then
                            sLogInfo = sLogInfo & "Force Flag    := " & UserCode & vbNewLine & _
                                                  "______________________________________________" & vbNewLine
                            Logging.LogInformation("AIMS CALL ", sLogInfo)

                            'If (UserCode.ToLower = "ma2") Then
                            '    Logging.LogDebug("managecheckout", "oAimsOjb.F6_ActivityCreate, 1. CheckOutValidationsDone " & CheckOutValidationsDone)
                            'Else
                            '    sAimsReturnValue = oAimsOjb.F6_ActivityCreate(sActivityType, sExternalRef, sUnit, sStartLocation, dtStart, nStartOdometer, sExpectedEndLocation, dtExpectedEnd, nExpectedEndOdometer, sOtherAssets, UserCode)
                            'End If
                            sAimsReturnValue = oAimsOjb.F6_ActivityCreate(sActivityType, sExternalRef, sUnit, sStartLocation, dtStart, nStartOdometer, sExpectedEndLocation, dtExpectedEnd, nExpectedEndOdometer, sOtherAssets, UserCode)

                        Else
                            sLogInfo = sLogInfo & "Force Flag    := " & vbNewLine & _
                                                  "______________________________________________" & vbNewLine
                            Logging.LogInformation("AIMS CALL ", sLogInfo)

                            'If (UserCode.ToLower = "ma2") Then
                            '    Logging.LogDebug("managecheckout", "oAimsOjb.F6_ActivityCreate, 2. CheckOutValidationsDone " & CheckOutValidationsDone)
                            'Else
                            '    sAimsReturnValue = oAimsOjb.F6_ActivityCreate(sActivityType, sExternalRef, sUnit, sStartLocation, dtStart, nStartOdometer, sExpectedEndLocation, dtExpectedEnd, nExpectedEndOdometer, sOtherAssets)
                            'End If
                            sAimsReturnValue = oAimsOjb.F6_ActivityCreate(sActivityType, sExternalRef, sUnit, sStartLocation, dtStart, nStartOdometer, sExpectedEndLocation, dtExpectedEnd, nExpectedEndOdometer, sOtherAssets)

                        End If
                        'dvass call = 1
                    Else
                        'dvass call <> 1
                        sLogInfo = sLogInfo & "Force Flag    := " & "<F9NotReq>" & vbNewLine & _
                                              "______________________________________________" & vbNewLine
                        Logging.LogInformation("AIMS CALL ", sLogInfo)

                        'If (UserCode.ToLower = "ma2") Then
                        '    Logging.LogDebug("managecheckout", "oAimsOjb.F6_ActivityCreate, 3. CheckOutValidationsDone sDVASSCall " & sDVASSCall)
                        'Else
                        '    sAimsReturnValue = oAimsOjb.F6_ActivityCreate(sActivityType, sExternalRef, sUnit, sStartLocation, dtStart, nStartOdometer, sExpectedEndLocation, dtExpectedEnd, nExpectedEndOdometer, sOtherAssets, "<F9NotReq>")
                        'End If
                        sAimsReturnValue = oAimsOjb.F6_ActivityCreate(sActivityType, sExternalRef, sUnit, sStartLocation, dtStart, nStartOdometer, sExpectedEndLocation, dtExpectedEnd, nExpectedEndOdometer, sOtherAssets, "<F9NotReq>")
                        'dvass call <> 1
                    End If


                    'check status of the return value
                    Dim sErrNo
                    sErrNo = Trim(sAimsReturnValue)


                    oReturnXml = DataRepository.GetLocationStatusForCheckInCheckOut(sBookingId, sRentalId, UserCode, ScreenValue, sUnit)

                    'Check for error which need to send back to client Conformation
                    ' This error is for Supervisor Override
                    If sErrNo <> 0 And (sErrNo = "-2147220471" Or _
                                        sErrNo = "-2147220470" Or _
                                        sErrNo = "-2147220488") Then


                        Throw New Exception("POPUP1/" & GetAimsMessage(sErrNo))

                        'sXmlString = GetAimsMessage(sErrNo)
                        'sErrorString = GetErrorTextFromResource(True, "POPUP1", "Application", sXmlString)
                        'manageCheckOut = "<Root>" & sErrorString & "<Data>" & xmlData & "</Data></Root>"
                        'Return oReturnXml


                    End If

                    ' This error is for Continue Cancel ie. Just Warning
                    If sErrNo <> 0 And (sErrNo = "-2147220489" Or _
                                        sErrNo = "-2147220475" Or _
                                        sErrNo = "-2147220469") Then

                        'sErrorString = GetErrorTextFromResource(True, "POPUP2", "Application", sXmlString)
                        'manageCheckOut = "<Root>" & sErrorString & "<Data>" & xmlData & "</Data></Root>"
                        Throw New Exception("POPUP2/" & GetAimsMessage(sErrNo))

                    End If

                    If sErrNo <> "0" Then
                        Throw New Exception(GetAimsMessage(sErrNo))
                    End If
                    'cross hire = false

                    Logging.LogInformation("ManageCheckOut", "Finished AIMS Call")
                Else
                    'cross hire = true
                    If sDVASSCall = "1" Then
                        'dvasscall = 1

                        'RENTAL CANCEL
                        Dim nSelectedCountry As Integer
                        If sCountryCode.ToUpper().Trim() = "AU" Then
                            nSelectedCountry = 0
                        Else
                            nSelectedCountry = 1
                        End If

                        'call dvass
                        Aurora.Reservations.Services.AuroraDvass.CancelRental(sRentalId, 1, 0, sDVASSStatus, nSelectedCountry, UserCode)


                        If sDVASSStatus <> "SUCCESS" Then
                            If InStr(sDVASSStatus, "333") = 0 Then


                                oReturnXml = DataRepository.GetLocationStatusForCheckInCheckOut(sBookingId, sRentalId, UserCode, ScreenValue, sUnit)
                                Throw New Exception(sDVASSStatus)


                            End If
                        End If

                        'dvasscall = 1
                    End If

                    'cross hire = true
                End If
                'cross hire

                'update rental integrity
                Logging.LogInformation("ManageCheckOut", "Starting DataRepository.UpdateRentalIntegrityNumber")
                DataRepository.UpdateRentalIntegrityNumber(sRentalId)
                Logging.LogInformation("ManageCheckOut", "Done DataRepository.UpdateRentalIntegrityNumber")

                oReturnXml = GetDataForCheckInCheckOut(sBookingId, sRentalId, UserCode, ScreenValue, sUnit, String.Empty)

                'main work
                ''rev:mia 25June2015 - Adding Imarda
                ''Nim to make it async 14th July 2015

                Dim tempTelematicsArgs As New TelematicsArgs
                tempTelematicsArgs.BookingID = sRentalId
                tempTelematicsArgs.UnitNumber = sUnit
                tempTelematicsArgs.IsCheckOut = True
                tempTelematicsArgs.CheckInOutDateTime = dtStart
                tempTelematicsArgs.RegoNumber = regoNumber
                System.Threading.ThreadPool.QueueUserWorkItem(AddressOf IMARDAtelematicsUpdateASYNC, tempTelematicsArgs)

                'IMARDAtelematicsUpdate(True, sRentalId, sUnit)
                ''End Nim to make it async 14th July 2015
                'If (UserCode.ToLower = "ma2") Then
                '    Logging.LogDebug("managecheckout", "rollback for testing")
                '    oTransaction.RollbackTransaction()
                '    oTransaction.Dispose()
                '    Return oXml
                'End If
                oTransaction.CommitTransaction()
                oTransaction.Dispose()

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Logging.LogException("ManageCheckOut", ex)

                'differentiating logic
                Dim sErrorMessage As String = ""
                Dim sErrorType As String = ""
                Dim sAdditionalParam As String = ""

                If ex.Message.ToUpper.Contains("VALIDATIONERRORS") Then
                    sErrorMessage = ex.Message.Replace("VALIDATIONERRORS/", String.Empty)
                    sErrorType = "VALIDATIONERRORS"
                ElseIf ex.Message.ToUpper.Contains("VALIDATIONWARNINGS") Then
                    sErrorMessage = ex.Message.Replace("VALIDATIONWARNINGS/", String.Empty)
                    sErrorType = "VALIDATIONWARNINGS"
                ElseIf ex.Message.ToUpper().Contains("ERROR/") Then
                    sErrorMessage = ex.Message.Replace("Error/", "")
                    ''rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129
                    sErrorType = IIf(sErrorMessage.Contains("PROMPTCONTINUE") = True, "PROMPTCONTINUE", "ERROR")
                    sErrorMessage = sErrorMessage.Replace("PROMPTCONTINUE/", "")

                    ''rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129
                ElseIf ex.Message.ToUpper().Contains("PROMPT/") Then
                    sErrorMessage = ex.Message.Replace("Prompt/", "")
                    sErrorType = "PROMPT"
                    sErrorMessage = sErrorMessage.Replace("Prompt/", "")

                ElseIf ex.Message.ToUpper().Contains("POPUP/") Then
                    sErrorMessage = ex.Message.Replace("POPUP/", "")
                    sErrorType = "POPUP"
                ElseIf ex.Message.ToUpper().Contains("POPUP1/") Then
                    sErrorMessage = ex.Message.Replace("POPUP1/", "")
                    sErrorType = "POPUP1"
                ElseIf ex.Message.ToUpper().Contains("POPUP2/") Then
                    sErrorMessage = ex.Message.Replace("POPUP2/", "")
                    sErrorType = "POPUP2"
                ElseIf ex.Message.ToUpper().Contains("POPUP3/") Then
                    sErrorMessage = ex.Message.Replace("POPUP3/", "")
                    sErrorType = "POPUP3"
                    'POPUP3/CkoiCusPymt^Check In or Out a rental with unpaid customer charges
                    If sErrorMessage.Contains("^") Then
                        sAdditionalParam = sErrorMessage.Split("^"c)(0)
                        sErrorMessage = sErrorMessage.Split("^"c)(1)
                    End If
                ElseIf ex.Message.ToUpper().Contains("POPUP4/") Then
                    sErrorMessage = ex.Message.Replace("POPUP4/", "")
                    sErrorType = "POPUP4"
                    'POPUP4/CkoiCusPymt^Check In or Out a rental with unpaid customer charges
                    If sErrorMessage.Contains("^") Then
                        sAdditionalParam = sErrorMessage.Split("^"c)(0)
                        sErrorMessage = sErrorMessage.Split("^"c)(1)
                    End If
                ElseIf ex.Message.ToUpper().Contains("POPUP-SOD/") Then
                    sErrorMessage = ex.Message.Replace("POPUP-SOD/", "")
                    sErrorType = "POPUP-SOD"
                Else
                    sErrorMessage = ex.Message
                    sErrorType = "OTHER"
                End If
                Try
                    If oReturnXml Is Nothing OrElse oReturnXml.OuterXml.Trim().Equals(String.Empty) Then
                        oReturnXml = New XmlDocument
                        oReturnXml.LoadXml("<Root/>")
                    End If

                    oReturnXml.DocumentElement.InnerXml = oReturnXml.DocumentElement.InnerXml & _
                                                                     "<Error>" & _
                                                                       "<Type>" & sErrorType & "</Type>" & _
                                                                       "<Message>" & sErrorMessage & "</Message>" & _
                                                                       "<AdditionalParam>" & sAdditionalParam & "</AdditionalParam>" & _
                                                                     "</Error>"
                Catch exp As Exception
                    Logging.LogException("ManageCheckOut", exp)
                End Try


            End Try

        End Using

        Return oReturnXml

    End Function



    Public Shared Function ManageCheckIn(ByVal ScreenData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String, ByVal ScreenValue As String, ByVal CheckInValidationsDone As Boolean, Optional ByVal SupervisorCode As String = "") As XmlDocument

        Dim oReturnXml As XmlDocument = New XmlDocument
        Dim sBookingId, sRentalId, sActivityType, sUnit, sExternalRef, sEndLocation As String
        Dim bCrossHire As Boolean
        Dim dtEnd As Date
        Dim nEndOdometer As Integer
        Dim regoNumber As String

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                'main work
                bCrossHire = ScreenData.DocumentElement.SelectSingleNode("CrossHr").InnerText
                sBookingId = ScreenData.DocumentElement.SelectSingleNode("BooId").InnerText
                sRentalId = ScreenData.DocumentElement.SelectSingleNode("RntId").InnerText
                sUnit = ScreenData.DocumentElement.SelectSingleNode("UnitNo").InnerText
                regoNumber = ScreenData.DocumentElement.SelectSingleNode("Rego").InnerText
                ' integrity Check!! :)
                If Not Booking.CheckIntegrity(sBookingId, CInt(ScreenData.DocumentElement.SelectSingleNode("RntIntNo").InnerText), CInt(ScreenData.DocumentElement.SelectSingleNode("BooIntNo").InnerText), sRentalId) Then
                    Throw New Exception("ERROR/This rental has been modified by another user. Please refresh the screen.")
                End If
                ' integrity Check passed - continue

                ' Enhancement #28 : Automation & revalidation of fees
                ' check if DTR applicable, if so apply and exit
                'If IsDieselRecoveryTaxApplicable(ScreenData, UserCode) Then
                '    ' do some thing cool!
                '    Dim sDTRReturnMessage As String = ""
                '    sDTRReturnMessage = ApplyDieselRecoveryTax(ScreenData, UserCode, ProgramName)
                '    If sDTRReturnMessage.Contains("ERR/") Then
                '        Throw New Exception(sDTRReturnMessage)
                '    End If
                'Else
                '    ' temp - take out!
                '    ' do some thing cool!
                '    Dim sDTRReturnMessage As String = ""
                '    sDTRReturnMessage = ApplyDieselRecoveryTax(ScreenData, UserCode, ProgramName)
                '    If sDTRReturnMessage.Contains("ERR/") Then
                '        Throw New Exception(sDTRReturnMessage)
                '    End If
                '    ' temp - take out!
                'End If
                ' Enhancement #28 : Automation & revalidation of fees

                'call db

                ' VALIDATIONS CHECK
                If ScreenValue Is Nothing OrElse ScreenValue.Trim() = String.Empty Then
                    ScreenValue = "0"
                End If

                Dim oXml As XmlDocument

                If Not CheckInValidationsDone Then ' unforced -> validate it!
                    oXml = Common.Data.GetPopUpData(Aurora.Popups.Data.PopupType.VALIDATEBOOKING, sBookingId, sRentalId, UserCode, "", "", UserCode)
                    If Not oXml.DocumentElement.SelectSingleNode("Error") Is Nothing Then
                        If Not oXml.DocumentElement.SelectSingleNode("Error").InnerText.Trim.Equals(String.Empty) _
                            AndAlso Not oXml.DocumentElement.SelectSingleNode("Error").InnerText.Trim().ToUpper().Contains(UCase("GST calculation advice")) _
                            Then
                            ' some errors
                            Throw New Exception("VALIDATIONERRORS/" & oXml.DocumentElement.SelectSingleNode("Error").InnerText)
                        End If
                    ElseIf Not oXml.DocumentElement.SelectSingleNode("Wrn") Is Nothing Then
                        If Not oXml.DocumentElement.SelectSingleNode("Wrn").InnerText.Trim.Equals(String.Empty) Then
                            ' some warnings
                            Throw New Exception("VALIDATIONWARNINGS/" & oXml.DocumentElement.SelectSingleNode("Wrn").InnerText)
                        End If
                    End If
                End If

                ' VALIDATIONS CHCK done :)
                If SupervisorCode.Trim().Equals(String.Empty) Then
                    ' no supervisor override
                    oXml = DataRepository.ManageCheckIn(ScreenData, UserCode, ProgramName, ScreenValue)
                Else
                    ' theres an override happening
                    oXml = DataRepository.ManageCheckIn(ScreenData, SupervisorCode, ProgramName, ScreenValue)
                End If

                ' bug fix!!!!
                If Not oXml.InnerText.ToUpper().Equals("SUCCESS") And _
                   Not oXml.InnerText.Trim().Equals(String.Empty) And _
                   Not oXml.InnerText.ToUpper().Contains("EXTENSION CREATED") And _
                   Not oXml.InnerText.ToUpper().Contains("MSG/") _
                Then
                    'If oXml.InnerText <> "SUCCESS" And oXml.InnerText <> "" Then
                    'some error -> rollback
                    'return new updated data
                    Logging.LogDebug("GetDataForCheckInCheckOut", "starting to call")
                    oReturnXml = GetDataForCheckInCheckOut(sBookingId, sRentalId, UserCode, ScreenValue, sUnit, String.Empty)
                    Logging.LogDebug("GetDataForCheckInCheckOut Result", oReturnXml.OuterXml)
                    'main work
                    Throw New Exception(oXml.InnerText)
                End If
                'db call over


                If Not oXml.InnerText.ToUpper().Contains("EXTENSION CREATED") And _
                   Not oXml.InnerText.ToUpper().Contains("MSG/") _
                Then
                    If bCrossHire = False Then
                        '----Updating AIMS Database
                        sActivityType = "Rental"
                        sExternalRef = ScreenData.DocumentElement.SelectSingleNode("BooRntNum").InnerText
                        sEndLocation = ScreenData.DocumentElement.SelectSingleNode("UserLocCode").InnerText
                        dtEnd = CDate(ScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText & " " & ScreenData.DocumentElement.SelectSingleNode("RntCkiTime").InnerText)
                        nEndOdometer = CLng(ScreenData.DocumentElement.SelectSingleNode("OddometerIn").InnerText)

                        Dim oAIMSObj As New AI.CMaster
                        Dim AIMSRetValue As Integer

                        AIMSRetValue = oAIMSObj.F8_ActivityComplete(sActivityType, sExternalRef, sEndLocation, dtEnd, nEndOdometer)

                        If AIMSRetValue <> 0 Then
                            Throw New Exception(GetAimsMessage(AIMSRetValue))
                        End If

                    End If
                End If

                'update integrity
                DataRepository.UpdateRentalIntegrityNumber(sRentalId)

                ' Added By Nimesh on 06th Aug 2015
                ' Description: Initial IsServiceDue flag in Fleet Asset was being updated in Stored procedure RES_VehicleExchange, but it was timing out in AIMS Call because of RowLock and 
                ' AIMS was not able to update in between the transaction. 
                ' Created new  Stored procedure RES_UpdateUnitForServiceDue in the database
                ' Created new Sub in Aurora.Booking.Data> DataRepository, UpdateServiceDueFlag to Call Stored procedure RES_UpdateUnitForServiceDue
                ' Now Calling the same method in AURORA.Booking.Data.UpdateServiceDueFlag here
                Dim returnError As String = ""
                DataRepository.UpdateServiceDueFlag(sUnit, returnError)

                If returnError.ToUpper() = "SUCCESS" Then
                    returnError = ""
                End If

                If returnError.Length <> 0 Then
                    Throw New Exception(returnError)
                End If
                'End Added by Nimesh on 06th Aug 2015
                'return new updated data
                Logging.LogDebug("GetDataForCheckInCheckOut Second Call", "starting to call")
                oReturnXml = GetDataForCheckInCheckOut(sBookingId, sRentalId, UserCode, ScreenValue, sUnit, String.Empty)
                Logging.LogDebug("GetDataForCheckInCheckOut Second Call Result", oReturnXml.OuterXml)
                'main work

                Dim sMessageString As String = ""

                If oXml.InnerText.ToUpper().Contains("EXTENSION CREATED") Or _
                   oXml.InnerText.ToUpper().Contains("MSG/") _
                Then
                    sMessageString = oXml.InnerText.Trim().Replace("Msg/", "")

                    oReturnXml.DocumentElement.InnerXml = oReturnXml.DocumentElement.InnerXml & _
                                                                                                       "<Message>" & _
                                                                                                         "<Type>MESSAGE</Type>" & _
                                                                                                         "<Message>" & sMessageString & "</Message>" & _
                                                                                                       "</Message>"
                End If
                ''rev:mia 25June2015 - Adding Imarda
                'IMARDAtelematicsUpdate(False, sRentalId, sUnit)
                ''Nim to make it async 14th July 2015

                Dim tempTelematicsArgs As New TelematicsArgs
                tempTelematicsArgs.BookingID = sRentalId
                tempTelematicsArgs.UnitNumber = sUnit
                tempTelematicsArgs.IsCheckOut = False
                tempTelematicsArgs.CheckInOutDateTime = dtEnd
                tempTelematicsArgs.RegoNumber = regoNumber

                System.Threading.ThreadPool.QueueUserWorkItem(AddressOf IMARDAtelematicsUpdateASYNC, tempTelematicsArgs)


                ''End Nim to make it async 14th July 2015

                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception

                ' oReturnXml.DocumentElement.InnerXml = oReturnXml.DocumentElement.InnerXml + "<Error><Message>" + ex.Message + "</Message></Error>"
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                'differentiating logic
                Dim sErrorMessage As String = ""
                Dim sErrorType As String = ""
                Dim sAdditionalParam As String = ""

                If ex.Message.ToUpper.Contains("VALIDATIONERRORS") Then
                    sErrorMessage = ex.Message.Replace("VALIDATIONERRORS/", String.Empty)
                    sErrorType = "VALIDATIONERRORS"
                ElseIf ex.Message.ToUpper.Contains("VALIDATIONWARNINGS") Then
                    sErrorMessage = ex.Message.Replace("VALIDATIONWARNINGS/", String.Empty)
                    sErrorType = "VALIDATIONWARNINGS"
                ElseIf ex.Message.ToUpper().Contains("ERROR/") Then
                    sErrorMessage = ex.Message.Replace("Error/", "")
                    sErrorType = "ERROR"
                ElseIf ex.Message.ToUpper().Contains("POPUP/") Then
                    sErrorMessage = ex.Message.Replace("POPUP/", "")
                    sErrorType = "POPUP"
                ElseIf ex.Message.ToUpper().Contains("POPUP1/") Then
                    sErrorMessage = ex.Message.Replace("POPUP1/", "")
                    sErrorType = "POPUP1"
                ElseIf ex.Message.ToUpper().Contains("POPUP2/") Then
                    sErrorMessage = ex.Message.Replace("POPUP2/", "")
                    sErrorType = "POPUP2"
                ElseIf ex.Message.ToUpper().Contains("POPUP3/") Then
                    sErrorMessage = ex.Message.Replace("POPUP3/", "")
                    sErrorType = "POPUP3"
                    'POPUP3/CkoiCusPymt^Check In or Out a rental with unpaid customer charges
                    ''rev:mia 07-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1048
                    If (sErrorMessage.Contains("^") = True) Then
                        sAdditionalParam = sErrorMessage.Split("^"c)(0)
                        sErrorMessage = sErrorMessage.Split("^"c)(1)
                    End If
                ElseIf ex.Message.ToUpper().Contains("MSG/") Then
                    sErrorMessage = ex.Message.Replace("Msg/", "")
                    sErrorType = "MESSAGE"

                Else
                    sErrorMessage = ex.Message
                    sErrorType = "OTHER"
                End If

                If oReturnXml Is Nothing OrElse oReturnXml.OuterXml.Trim().Equals(String.Empty) Then
                    oReturnXml = New XmlDocument
                    oReturnXml.LoadXml("<Root/>")
                End If
                oReturnXml.DocumentElement.InnerXml = oReturnXml.DocumentElement.InnerXml & _
                                                                                    "<Error>" & _
                                                                                      "<Type>" & sErrorType & "</Type>" & _
                                                                                      "<Message>" & sErrorMessage & "</Message>" & _
                                                                                      "<AdditionalParam>" & sAdditionalParam & "</AdditionalParam>" & _
                                                                                    "</Error>"
                Logging.LogError("ManageCheckin", ex.Message & vbCrLf & ex.StackTrace)
            End Try

        End Using

        Return oReturnXml

    End Function


    Public Shared Function IsCrossHire(ByVal CountryCrossHire As String, ByVal UnitNumber As String) As Boolean
        Dim oXml As XmlDocument
        oXml = DataRepository.GetCrossHire(CountryCrossHire, UnitNumber)
        If oXml.InnerText = "CROSS" Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function GetPrintAgreement(ByVal RentalId As String) As XmlDocument

        Return DataRepository.GetDataForRentalAgreementPrinting(RentalId)
        'do more stuff
        '<<TODO>>
    End Function


    Public Shared Function GetUVIData(ByVal UserCode As String) As XmlDocument
        'GEN_GetPopUpData @case = 'GETUVI', @param1 = 'sp7', @param2='ORID_AIMS'
        Return DataRepository.GetUVIData(UserCode, "ORID_AIMS")
    End Function


    Public Shared Function MaintainContractHistory(ByVal BookingId As String, ByVal RentalId As String, ByVal UserCode As String, ByVal ProgramName As String)
        Return DataRepository.MaintainContractHistory(BookingId, RentalId, UserCode, ProgramName)
    End Function


    Public Shared Function GetAIMSVehicleName(ByVal RegNo As String, ByVal UnitNo As String, ByVal RentalId As String, ByVal UserCode As String)
        Return DataRepository.GetAIMSVehicleName(RegNo, UnitNo, RentalId, "", UserCode)
    End Function

    Public Shared Function GetRentalInfo(ByVal BookingId As String, ByVal RentalId As String)
        Return DataRepository.GetDataForCheckInCheckOut(BookingId, "RentalRecord", RentalId)
    End Function

    ''' <summary>
    ''' Returns the Error description for an AIMS error number
    ''' </summary>
    ''' <param name="sErrNo">The Error Number</param>
    ''' <returns>Error Description</returns>
    ''' <remarks></remarks>
    Public Shared Function GetAimsMessage(ByVal sErrNo As String) As String
        '    Dim oAimsDAL As Object
        '    Set oAimsDAL = CreateObject(AURORA_DATA_ACCESS_LAYER)
        '    getAimsMessage = getMessageFromDB(oAimsDAL, sErrNo, "", "", "", "")
        '    If Not (oAimsDAL Is Nothing) Then Set oAimsDAL = Nothing
        Select Case sErrNo
            Case "-2147220503"
                GetAimsMessage = "1001 - Unit Number Not Found"
            Case "-2147220502"
                GetAimsMessage = "1002 - PO Number Not Found"
            Case "-2147220501"
                GetAimsMessage = "1003 - Location Code Not Found"
            Case "-2147220500"
                GetAimsMessage = "1004 - Repairer Not Found"
            Case "-2147220499"
                GetAimsMessage = "1005 - Reason Code Not Found"
            Case "-2147220498"
                GetAimsMessage = "1006 - Service Number Not Found"
            Case "-2147220497"
                GetAimsMessage = "1007 - Incident Not Found"
            Case "-2147220496"
                GetAimsMessage = "1008 - Activity Not Found"
            Case "-2147220495"
                GetAimsMessage = "1009 - Activity Type Not Found"
            Case "-2147220494"
                GetAimsMessage = "1010 - Other Assets Not Found"
            Case "-2147220493"
                GetAimsMessage = "1011 - Asset Not Found"
            Case "-2147220492"
                GetAimsMessage = "1012 - Old Asset Not Found"
            Case "-2147220491"
                GetAimsMessage = "1013 - New Asset Not Found"
            Case "-2147220490"
                GetAimsMessage = "1014 - Location is in wrong region"
            Case "-2147220489"
                GetAimsMessage = "1015 - Registration Date id Expired"
            Case "-2147220488"
                GetAimsMessage = "1016 - Location does not match vehicle idle location or vehicle is not idle"
            Case "-2147220484"
                GetAimsMessage = "1020 - Start Date is Invalid"
            Case "-2147220483"
                GetAimsMessage = "1021 - End Date is Invalid"
            Case "-2147220482"
                GetAimsMessage = "1022 - Start Odometer is Invalid"
            Case "-2147220481"
                GetAimsMessage = "1023 - End Odometer is Invalid"
            Case "-2147220480"
                GetAimsMessage = "1024 - Repairer is Invalid"
            Case "-2147220479"
                GetAimsMessage = "1025 - Reason Description is Invalid"
            Case "-2147220478"
                GetAimsMessage = "1026 - Other Repairer is Invalid"
            Case "-2147220477"
                GetAimsMessage = "1027 - Activity is Invalid"
            Case "-2147220476"
                GetAimsMessage = "1028 - Exchange Date is Invalid"
            Case "-2147220475"
                GetAimsMessage = "1029 - Vehicle is due for the Service"
            Case "-2147220474"
                GetAimsMessage = "1030 - Record already completed"
            Case "-2147220473"
                GetAimsMessage = "1031 - Field can not be an empty"
            Case "-2147220472"
                GetAimsMessage = "1032 - Record cannot be updated"
            Case "-2147220471"
                GetAimsMessage = "1033 - Vehicle is due for Disposal"
            Case "-2147220470"
                GetAimsMessage = "1034 - Vehicle is due for RM"
            Case "-2147220469"
                GetAimsMessage = "1035 - COF expiry date has passed or is about to pass"
            Case "-2147220468"
                GetAimsMessage = "1036 - RUC expiry has passed or is about to pass"
            Case "-2147220467"
                GetAimsMessage = "1037 - Vehicle is not idle"
            Case "-2147220464"
                GetAimsMessage = "1040 - DVASS SoftFail 1"
            Case "-2147220463"
                GetAimsMessage = "1041 - DVASS SoftFail 2"
            Case "-2147220462"
                GetAimsMessage = "1042 - DVASS Hard Fail"
            Case "-2147220461"
                GetAimsMessage = "1043 - DVASS Interface Error"
            Case "-2147220460"
                GetAimsMessage = "1044 - DVASS Interface Error - Time out on DVASS call"
            Case "-2147220459"
                GetAimsMessage = "1045 - DVASS State Error"
            Case "-2147220458"
                GetAimsMessage = "1046 - DVASS State Error - No monitor running DVASS"
            Case "-2147220457"
                GetAimsMessage = "1047 - DVASS State Error - DVASS is running to start up the system"
            Case "-2147220456"
                GetAimsMessage = "1048 - DVASS State Error - DEVASS is processing queued requests"
            Case Else
                GetAimsMessage = sErrNo & " - Unknown AIMS Message!."
        End Select
    End Function

#Region "Serial Number Business"

    Public Shared Function GetProductsRequiringSerialNumber(ByVal RentalId As String, ByVal ShowAll As Boolean) As XmlDocument
        Return DataRepository.GetProductsRequiringSerialNumber(RentalId, ShowAll)
    End Function

    Public Shared Function SaveProductSerialNumber(ByVal BookedProductId As String, ByVal SerialNumber As String, ByVal UserCode As String, ByVal ProductName As String, ByVal ProgramName As String) As XmlDocument

        Dim oReturnDoc As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try
                Dim xmlReturn As XmlDocument
                xmlReturn = DataRepository.SaveProductSerialNumber(BookedProductId, SerialNumber, UserCode, ProductName, ProgramName)

                If Not xmlReturn.DocumentElement.InnerText.ToUpper().Contains("SUCCESS") Then
                    ' some problem
                    Throw New Exception(xmlReturn.DocumentElement.InnerText)
                End If

                If Not (xmlReturn.DocumentElement.InnerText.Contains("GEN045") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN046") Or _
                        xmlReturn.DocumentElement.InnerText.Contains("GEN047")) Then
                    Throw (New Exception(xmlReturn.DocumentElement.InnerText))
                End If
                oTransaction.CommitTransaction()

                If xmlReturn.DocumentElement.InnerText.Contains("GEN045") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN046") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")
                ElseIf xmlReturn.DocumentElement.InnerText.Contains("GEN047") Then
                    oReturnDoc.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN047") & "</Message></Root>")
                End If

            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnDoc.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnDoc
        End Using






    End Function
#End Region


#Region "Enhancement #28 : Automation & revalidation of fees"

    'Public Shared Function IsDieselRecoveryTaxApplicable(ByVal ScreenData As XmlDocument, ByVal UserCode As String) As Boolean
    '    ' get country
    '    Dim sCountryCode As String
    '    sCountryCode = UCase(ScreenData.DocumentElement.SelectSingleNode("CtyCode").InnerText).Trim()

    '    If sCountryCode.Equals("NZ") Then
    '        ' its NZ
    '        Dim oXml As XmlDocument
    '        Dim sRentalId As String = ScreenData.DocumentElement.SelectSingleNode("RntId").InnerText
    '        Dim sBookingId As String = ScreenData.DocumentElement.SelectSingleNode("BooId").InnerText

    '        ' check is DTR is already applied!
    '        If Not Data.DataRepository.IsDieselRecoveryTaxAlreadyApplied(sRentalId) Then
    '            ' DTR not applied yet!
    '            oXml = Data.DataRepository.GetProductsRequiringDieselRecoveryTax(sRentalId)
    '            If oXml.DocumentElement.FirstChild.ChildNodes.Count > 0 Then
    '                ' we need to apply DTR
    '                Return True
    '            Else
    '                ' no drt :D
    '                Return False
    '            End If
    '        Else
    '            ' DTR already applied
    '            Return False
    '        End If
    '    Else
    '        Return False
    '    End If

    '    Return False
    'End Function

    'Public Shared Function ApplyDieselRecoveryTax(ByVal ScreenData As XmlDocument, ByVal UserCode As String, ByVal ProgramName As String) As String
    '    Try

    '        ' try and save this thingie! - SaveProduct()
    '        Dim sReturnMessage, sRentalId, sSaleableProductId, sCheckOutLocation, sCheckInLocation, sReturnError, sPackageId, sCountryCode, sProductId As String
    '        Dim dtCheckOutDate, dtCheckInDate As DateTime
    '        Dim dRate As Double = 0.0
    '        Dim nQuantity As Integer = 0

    '        sRentalId = ScreenData.DocumentElement.SelectSingleNode("RntId").InnerText
    '        sCountryCode = UCase(ScreenData.DocumentElement.SelectSingleNode("CtyCode").InnerText).Trim()

    '        sCheckOutLocation = UCase(ScreenData.DocumentElement.SelectSingleNode("RntCkoLocCode").InnerText)
    '        sCheckInLocation = UCase(ScreenData.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText)

    '        dtCheckOutDate = CDate(ScreenData.DocumentElement.SelectSingleNode("RntCkoWhen").InnerText & " " & ScreenData.DocumentElement.SelectSingleNode("RntCkoTime").InnerText)
    '        dtCheckInDate = CDate(ScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText & " " & ScreenData.DocumentElement.SelectSingleNode("RntCkiTime").InnerText)

    '        sReturnError = ""
    '        sPackageId = ""

    '        ' get the package Id and product id - based on rentalid
    '        sProductId = Data.DataRepository.GetDieselRecoveryTaxProductAndPackageId(sRentalId, sPackageId)

    '        sSaleableProductId = Data.DataRepository.GetDieselRecovertTaxSaleableProductId(sPackageId, sCountryCode, sProductId, sCheckOutLocation, sCheckInLocation, dtCheckOutDate, dtCheckInDate, sReturnError)
    '        If sSaleableProductId.Contains("ERR/") Then
    '            Return sSaleableProductId
    '        End If

    '        sReturnMessage = Data.DataRepository.SaveProduct(sRentalId, sSaleableProductId, dtCheckOutDate, sCheckOutLocation, dtCheckInDate, sCheckInLocation, dRate, nQuantity, UserCode, ProgramName, sReturnError)
    '        If sReturnMessage.Contains("ERR/") Then
    '            Return sReturnMessage
    '        End If

    '    Catch ex As Exception
    '        Return "ERR/" & ex.Message
    '    End Try

    '    Return False

    'End Function




#End Region

#Region "rev:mia 25June2015-Imarda Integration thru RESTAPI"
  
    ' New Procedure Added by Nimesh on 14th July
    Public Shared Sub IMARDAtelematicsUpdateASYNC(ByVal state As Object)
        Dim sb As New StringBuilder
        Dim isCheckout As Boolean = DirectCast(state, TelematicsArgs).IsCheckOut
        Dim bookingId As String = DirectCast(state, TelematicsArgs).BookingID
        Dim unitNumber As String = DirectCast(state, TelematicsArgs).UnitNumber
        Dim regoNumber As String = DirectCast(state, TelematicsArgs).RegoNumber
        Dim checkInOutDatetime As DateTime = DirectCast(state, TelematicsArgs).CheckInOutDateTime

        sb.AppendFormat("IsCheckOut: {0}{1}", isCheckout, vbCrLf)
        sb.AppendFormat("BookingId: {0}{1}", bookingId, vbCrLf)
        sb.AppendFormat("UnitNumber: {0}{1}", unitNumber, vbCrLf)
        sb.AppendFormat("Rego: {0}{1}", regoNumber, vbCrLf)
        Dim endPoint As String = ""
        ''Dim TelematicsGUID As String = Aurora.Common.Data.ExecuteScalarSP("GetTelematicsGUID", UnitNumber)

        Try
            Dim TelematicsGUID As String = Aurora.Common.Data.ExecuteScalarSQL("SELECT TelematicsGUID FROM aimsprod..FleetAsset WHERE UnitNumber = " + unitNumber).ToString
            sb.AppendFormat("TelematicsGUID: {0}{1}", TelematicsGUID, vbCrLf)

            If Not String.IsNullOrEmpty(TelematicsGUID) Then

                Dim vehSer As VehicleService = New VehicleService()

                Dim conSer As ConnectionService = New ConnectionService()
                Dim userName As String = ConfigurationManager.AppSettings("UserName").ToString()
                Dim userPassword As String = ConfigurationManager.AppSettings("Password").ToString()
                Try
                    ConnectedSessionID = conSer.Login(userName, userPassword)
                Catch ex As Exception

                    Dim exMessage As String = ex.Message
                End Try
                If ConnectedSessionID.Equals(Guid.Empty) Then
                    sb.AppendFormat("Your Login to Telematics was not successful: {0}", vbCrLf)
                Else
                    vehSer.UpdateVehicleByGUID(TelematicsGUID, IIf(isCheckout, bookingId, String.Empty), ConnectedSessionID)
                    Dim jobSer As JobService = New JobService
                    Dim jobDateTime As DateTime

                    If DateTime.TryParse(checkInOutDatetime, jobDateTime) Then
                        If isCheckout Then
                            jobSer.CreateJobForVehicle(TelematicsGUID, bookingId, regoNumber, jobDateTime, ConnectedSessionID)
                        Else
                            jobSer.CreateCheckInJobForVehicle(TelematicsGUID, "CheckIn", jobDateTime, ConnectedSessionID)
                        End If
                    Else
                        'lblStatus.Text = "[" + txtJobDateTime.Text + "] is not a valid date time"
                        sb.AppendFormat("Message: {0} will not be called. TelematicsGuid is empty {1}", endPoint, vbCrLf)
                        Return
                    End If
                End If
            Else
                sb.AppendFormat("Message: {0} will not be called. TelematicsGuid is empty {1}", endPoint, vbCrLf)
            End If
        Catch ex As Exception
            sb.AppendFormat("Error: {0}  {1}", ex.Message & "," & ex.StackTrace, vbCrLf)
        End Try
        Logging.LogDebug("IMARDAtelematicsUpdate: ", sb.ToString)
    End Sub


#End Region
End Class
