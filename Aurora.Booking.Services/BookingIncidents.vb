Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Reservations

Public Class BookingIncidents


    Public Shared Function GetIncidents(ByVal RentalId, ByVal BookingId) As DataTable
        'Dim oXml As XmlDocument
        'oXml = DataRepository.GetIncidents(RentalId, BookingId)
        'If oXml.DocumentElement.InnerText.Contains("GEN049") Then
        '    'no records
        '    Return
        'End If
        Dim dtResults As DataTable
        dtResults = DataRepository.GetIncidents(RentalId, BookingId)

        Return dtResults

    End Function

    Public Shared Function GetIncident(ByVal BookingId As String, ByVal RentalId As String, ByVal IncidentId As String, ByVal UserCode As String) As XmlDocument

        Dim oXmlDoc As XmlDocument
        oXmlDoc = DataRepository.GetIncident(BookingId, RentalId, IncidentId, UserCode)
        Return oXmlDoc

    End Function


    Public Shared Function GetIncidentTypes() As DataTable
        Dim oXml As XmlDocument
        oXml = DataRepository.GetCodeCodeType(8)
        Dim dstIncidentTypes As New DataSet
        dstIncidentTypes.ReadXml(New XmlNodeReader(oXml.DocumentElement))
        If dstIncidentTypes.Tables.Count > 0 Then
            Return dstIncidentTypes.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function GetNotificationTypes() As DataTable
        Dim oXml As XmlDocument
        ' Changed on 7.10.9
        'oXml = DataRepository.GetCodeCodeType(11)
        oXml = DataRepository.GetCodeCodeType(45)
        Dim dstIncidentTypes As New DataSet
        dstIncidentTypes.ReadXml(New XmlNodeReader(oXml.DocumentElement))
        If dstIncidentTypes.Tables.Count > 0 Then
            Return dstIncidentTypes.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function GetCauseTypes(ByVal CauseTypeId As String) As DataTable
        Dim oXml As XmlDocument
        oXml = DataRepository.GetCauses(CauseTypeId)
        Dim dstCauses As New DataSet
        dstCauses.ReadXml(New XmlNodeReader(oXml.DocumentElement))
        If dstCauses.Tables.Count > 0 Then
            Return dstCauses.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function GetSubCauses(ByVal SubCauseTypeId As String) As DataTable
        Dim oXml As XmlDocument
        oXml = DataRepository.GetSubCauses(SubCauseTypeId)
        Dim dstSubCauses As New DataSet
        dstSubCauses.ReadXml(New XmlNodeReader(oXml.DocumentElement))
        If dstSubCauses.Tables.Count > 0 Then
            Return dstSubCauses.Tables(0)
        Else
            Return New DataTable
        End If
    End Function

    Public Shared Function MaintainIncident(ByVal ScreenXmlDoc As XmlDocument, _
                                 ByVal UserId As String, _
                                 ByVal ProgramName As String, _
                                 Optional ByRef NewIncidentId As String = "") As XmlDocument




        Dim sBookingId, sRentalId, sIncidentId, sAIMSReference, sBranch, sType, sLoggedDate, sNotified, _
            sOdoMeter, sDetailId, sDetailIntegrityNo, sDetails, sCustomerSatisfaction, sIncidentStatus, _
            sResolvedDate, sIncidentIntegrityNo, sUpdateStatus, sCheckOutBranch, sCheckInBranch, sVehicle, _
            sUnitNo As String

        Dim oResult As New XmlDocument

        Dim sSubCauseId, sCauseDescription, sSubCauseDescription, sOtherText, sRemoveItem, sCauseId, sCauseIntegrityNo, sIncidentType, sAIMSDetails As String
        sAIMSDetails = ""

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                'main body

                With ScreenXmlDoc.DocumentElement
                    sBookingId = .SelectSingleNode("Incident/BookingId").InnerText
                    sRentalId = .SelectSingleNode("Incident/RentalId").InnerText
                    sIncidentId = .SelectSingleNode("Incident/IncidentId").InnerText
                    sAIMSReference = .SelectSingleNode("Incident/AIMSRefNo").InnerText
                    sBranch = .SelectSingleNode("Incident/Branch").InnerText.Split("-"c)(0).Trim()
                    sType = .SelectSingleNode("Incident/Type").InnerText
                    sLoggedDate = .SelectSingleNode("Incident/LoggedDate").InnerText
                    sNotified = .SelectSingleNode("Incident/Notified").InnerText
                    'sOdoMeter = .SelectSingleNode("Incident/Odometer").InnerText
                    sDetailId = .SelectSingleNode("Incident/DetailId").InnerText
                    sDetailIntegrityNo = .SelectSingleNode("Incident/DetailsIntegrity").InnerText
                    sDetails = .SelectSingleNode("Incident/Details").InnerText
                    'sCustomerSatisfaction = .SelectSingleNode("Incident/CustSatisfaction").InnerText
                    'sIncidentStatus = .SelectSingleNode("Incident/IncdntStatus").InnerText
                    'sResolvedDate = .SelectSingleNode("Incident/ResolutionWhen").InnerText
                    sIncidentIntegrityNo = .SelectSingleNode("Incident/IntegrityNo").InnerText


                    If sIncidentId = "" Then
                        sUpdateStatus = "I"
                        sIncidentIntegrityNo = 1
                        sDetailIntegrityNo = 1
                    Else
                        NewIncidentId = sIncidentId
                        sUpdateStatus = "U"
                    End If

                    'the customer must associate with rental, if it isn't there return
                    If Len(sRentalId) = 0 Then
                        Throw (New Exception("Application/Rental Id is NULL"))
                    End If

                    ' Get the VechileData
                    sCheckOutBranch = .SelectSingleNode("VechileData/FMOD/CheckOut").InnerText.Split("-")(0).Trim()
                    sCheckInBranch = .SelectSingleNode("VechileData/FMOD/CheckIn").InnerText.Split("-")(0).Trim()
                    sVehicle = .SelectSingleNode("VechileData/FMOD/Vehicle").InnerText
                    sUnitNo = .SelectSingleNode("VechileData/FMOD/UnitNo").InnerText.Split("-")(0).Trim()
                End With

                Dim oReturnXml As XmlDocument
                oReturnXml = DataRepository.UpdateIncident(sBookingId, sRentalId, sIncidentId, sUnitNo, sType, sNotified, sLoggedDate, sBranch, sIncidentIntegrityNo, UserId, ProgramName)

                If oReturnXml.DocumentElement.InnerText <> "SUCCESS" And oReturnXml.DocumentElement.InnerText <> "" Then
                    'failed
                    'however chk for "GEN046"
                    If Not oReturnXml.DocumentElement.InnerText.Contains("GEN046") Then
                        Throw (New Exception(oReturnXml.DocumentElement.InnerText))
                    Else
                        sIncidentId = oReturnXml.DocumentElement.InnerText.Split("/"c)(2)
                        NewIncidentId = sIncidentId
                    End If


                    '' ''If sErrNo <> "GEN046" Then
                    '' ''    MaintainIncident = "<Root>" & GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc) & "</Root>"
                    '' ''    ErrorLog(sUsrId & " " & "Updating Incident is Failure Error = " & sErrNo & " - " & sErrDesc, 2)
                    '' ''    GoTo Failure
                    '' ''Else
                    '' ''    sIcdId = sErrorArray(2)
                    '' ''    sQueryString = "incdnt_GetIncident '" & sBooId & "', '" & sRntId & "', '" & sIcdId & "'"
                    '' ''End If
                    '<<TOCHECK>>

                End If


                Dim oNoteReturnXml As XmlDocument
                oNoteReturnXml = DataRepository.UpdateNote(sDetailId, sBookingId, sRentalId, sIncidentId, sDetails, sDetailIntegrityNo, UserId, ProgramName)


                If oNoteReturnXml.DocumentElement.InnerText <> "SUCCESS" And oNoteReturnXml.DocumentElement.InnerText <> "" Then
                    Throw (New Exception("Application/" & oNoteReturnXml.DocumentElement.InnerText & " Updating Incident Notes failed"))
                    'failure
                    '<<TOCHECK>>
                End If



                '------------- Update Cause Start
                For Each oChildNode As XmlNode In ScreenXmlDoc.DocumentElement.SelectSingleNode("CauseDetails").ChildNodes

                    sSubCauseId = oChildNode.SelectSingleNode("SubCauseID").InnerText
                    If oChildNode.SelectSingleNode("IncidentId").InnerText.Trim() <> "" Then
                        sIncidentId = oChildNode.SelectSingleNode("IncidentId").InnerText
                    End If

                    sCauseDescription = oChildNode.SelectSingleNode("CauseDescription").InnerText
                    sSubCauseDescription = oChildNode.SelectSingleNode("SubCauseDescription").InnerText
                    sOtherText = oChildNode.SelectSingleNode("OtherText").InnerText
                    sRemoveItem = oChildNode.SelectSingleNode("RemoveItem").InnerText
                    sCauseId = oChildNode.SelectSingleNode("CauseID").InnerText
                    If oChildNode.SelectSingleNode("IntegrityNo").InnerText.Trim() <> "0" Then
                        sCauseIntegrityNo = oChildNode.SelectSingleNode("IntegrityNo").InnerText
                    Else
                        sCauseIntegrityNo = 1
                    End If

                    Dim oSubCauseReturnXml As XmlDocument = New XmlDocument

                    If sRemoveItem = "0" Then
                        oSubCauseReturnXml = DataRepository.UpdateIncidentSubCause(sIncidentId, sSubCauseId, sCauseIntegrityNo, sOtherText, UserId, ProgramName)
                    Else
                        If sUpdateStatus = "U" Then
                            oSubCauseReturnXml = DataRepository.DeleteIncidentSubCause(sIncidentId, sSubCauseId)
                        End If
                    End If

                    If oSubCauseReturnXml.DocumentElement.InnerText <> "" And oSubCauseReturnXml.DocumentElement.InnerText <> "" Then
                        'failure
                        Throw (New Exception("Application/" & oSubCauseReturnXml.DocumentElement.InnerText & " Insert/Delete Incident Sub Cause failed"))
                        '<<TODO>>
                    End If
                Next
                '------------- Update Cause End



                '------------- Update AIMS Start
                ' Need to call AIMS only if it is Accident and Breakdown
                ' cannot ever be called now, as type = complaints always

                'sIncidentType = UCase(Trim(DataRepository.GetIncidentType(sType)))

                'Dim nAIMSReturnValue As Long

                'If sIncidentType = "ACCIDENT" Or sIncidentType = "BREAKDOWN" Then
                '    Dim oAimsObj As New AI.CMaster
                '    nAIMSReturnValue = oAimsObj.F4_IncidentCreateUpdate(CStr(sUnitNo), CDate(Now), CStr(sBranch), CLng(sOdoMeter), CStr(sAIMSDetails), sAIMSReference)

                '    If nAIMSReturnValue <> 0 Then
                '        'failure
                '        Throw (New Exception("Application/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(nAIMSReturnValue) & " AIMS Call failed"))
                '        '<<TOCHECK>>
                '    End If

                '    If Len(sAIMSReference) > 0 Then
                '        'call to update aimsref
                '        Dim oAIMSRefReturnXml As XmlDocument
                '        oAIMSRefReturnXml = DataRepository.UpdateAIMSReference(sIncidentId, sAIMSReference)

                '        If oAIMSRefReturnXml.DocumentElement.InnerText <> "SUCCESS" And oAIMSRefReturnXml.DocumentElement.InnerText <> "" Then
                '            'failure
                '            Throw (New Exception("Application/" & oAIMSRefReturnXml.DocumentElement.InnerText))
                '            '<<TOCHECK>>
                '        End If
                '    End If
                'End If


                '------------- Update AIMS End

                If sUpdateStatus = "I" Then
                    'sErrNo = "GEN045"

                    oResult.LoadXml("<Error><ErrorMessage>" & Aurora.Common.Data.GetErrorMessage("GEN045") & "</ErrorMessage><ErrorType>" & "GEN045" & "</ErrorType></Error>")
                    '<<TODO>>
                ElseIf sUpdateStatus = "U" Then
                    'sErrNo = "GEN046"
                    oResult.LoadXml("<Error><ErrorMessage>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</ErrorMessage><ErrorType>" & "GEN046" & "</ErrorType></Error>")
                    '<<TODO>>
                End If

                'sgetXml = oAuroraDAL.getRecord(sQueryString)
                'MaintainIncident = "<Root>" & sErrDesc & "<Data>" & sgetXml & "</Data></Root>"

                'main body
                'success part
                oTransaction.CommitTransaction()
                oTransaction.Dispose()

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Dim sErrorType, sErrorMessage As String
                If ex.Message.Contains("Application") Then
                    'application
                    sErrorMessage = ex.Message.Replace("Application/", "")
                    sErrorType = "Application"
                Else
                    sErrorMessage = ex.Message
                    sErrorType = "Other"
                End If
                oResult.LoadXml("<Error><ErrorMessage>" & sErrorMessage & "</ErrorMessage><ErrorType>" & sErrorType & "</ErrorType></Error>")


            End Try



        End Using


        Return oResult


    End Function

    Public Shared Function RemoveSubCauses(ByVal CausesXmlData As XmlDocument, ByVal BookingId As String, ByVal RentalId As String, ByVal IncidentId As String, ByVal UserCode As String, ByVal ProgramName As String) As XmlDocument

        Dim oReturnXml As New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                
                'the customer must associate with booking, if it isn't there return
                If Len(BookingId.Trim()) = 0 Then
                    'GEN023
                    'exit
                    Throw (New Exception(Aurora.Common.Data.GetErrorMessage("GEN023")))
                End If

                'the customer must associate with rental, if it isn't there return
                If Len(RentalId.Trim()) = 0 Then
                    'GEN023
                    'exit
                    Throw (New Exception(Aurora.Common.Data.GetErrorMessage("GEN023")))
                End If

                For Each oCauseNode As XmlNode In CausesXmlData.DocumentElement.ChildNodes ' CausesXmlData.DocumentElement.SelectSingleNode("IncidentSubCause").ChildNodes

                    'update / delete selection
                    Dim oReturnedXml As XmlDocument
                    If CBool(oCauseNode.SelectSingleNode("RemoveItem").InnerText) Then
                        'delete this node
                        oReturnedXml = DataRepository.DeleteIncidentSubCause(oCauseNode.SelectSingleNode("IncidentId").InnerText, oCauseNode.SelectSingleNode("SubCauseID").InnerText)
                    Else
                        'update logic goes here
                        oReturnedXml = DataRepository.UpdateIncidentSubCause(oCauseNode.SelectSingleNode("IncidentId").InnerText, _
                                                                             oCauseNode.SelectSingleNode("SubCauseID").InnerText, _
                                                                             oCauseNode.SelectSingleNode("IntegrityNo").InnerText, _
                                                                             oCauseNode.SelectSingleNode("OtherText").InnerText, _
                                                                             UserCode, ProgramName)
                    End If

                    If oReturnedXml.DocumentElement.InnerText <> "" And oReturnedXml.DocumentElement.InnerText.ToUpper() <> "SUCCESS" Then
                        'error here!
                        Throw (New Exception("Application/Error Occured while updating/deleting the subcauses"))
                    End If

                Next

                'GEN046
                oReturnXml.LoadXml("<Error><ErrorType>GEN046</ErrorType><ErrorMessage>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</ErrorMessage></Error>")

                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                Dim sErrorMessage, sErrorType As String
                If ex.Message.Contains("Application/") Then
                    sErrorType = "Application"
                    sErrorMessage = ex.Message.Replace("Application/", "")
                Else
                    sErrorType = "Other"
                    sErrorMessage = ex.Message
                End If

                oReturnXml.LoadXml("<Error><ErrorType>" & sErrorType & "</ErrorType><ErrorMessage>" & sErrorMessage & "</ErrorMessage></Error>")

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
            End Try

        End Using

        Return oReturnXml

    End Function

    Public Shared Function GetLocationDescriptionAndIncidentTypeId(ByVal LocationId As String, ByVal IncidentType As String) As XmlDocument
        Return DataRepository.GetLocationDescriptionAndIncidentTypeId(LocationId, IncidentType)
    End Function

#Region "Call to data repository methods that update callback data for an incident."

    'Mod: Raj Feb 20 2012''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    Public Shared Function GetStatusCodes(ByVal codCdtCode As Integer) As DataSet
        Return DataRepository.DBGetStatusCodes(codCdtCode)
    End Function

    Public Shared Function GetIncidentStatus(ByVal incdtID As String) As DataSet
        Return DataRepository.DBGetIncidentStatus(incdtID)
    End Function

    Public Shared Function SaveCallbackData(ByVal incdtID As String, ByVal details As String) As Int64
        Return DataRepository.DBSaveCallbackData(incdtID, details)
    End Function

    Public Shared Function SaveCallbackStatus(ByVal incdtID As String, ByVal StatusCode As String) As Int64
        Return DataRepository.DBSaveCallbackStatus(incdtID, StatusCode)
    End Function

    Public Shared Function SaveCallbackStatusDate(ByVal IncCallBckStatusId As String) As Integer
        Return DataRepository.DBSaveCallbackStatusDate(IncCallBckStatusId)
    End Function

    Public Shared Function GetIncidentCallbacks(ByVal icdId As String) As DataSet
        Return DataRepository.DBGetIncidentCallbacks(icdId)
    End Function

    Public Shared Function GetUsernameforID(ByVal usrId As String) As DataSet
        Return DataRepository.DBGetUsernameforID(usrId)
    End Function

    'Mod: Raj Feb 20 2012''''''''''''''''''''''''''''''''''''''''''''''''''''''

#End Region

End Class