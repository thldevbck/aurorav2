﻿Imports System.Data
Imports Aurora.Booking.Data
Imports Aurora.Booking.Data.DataRepository
Imports Aurora.Common

<Serializable()> _
Public Class BookingPriceMatching

    Private _BookingPriceMatchingDataset As DataSet

    Public ReadOnly Property BookingInformationDataTable As DataTable
        Get
            Return _BookingPriceMatchingDataset.Tables("BookingInformation")
        End Get
    End Property

    Public ReadOnly Property BookingPriceMatchingDataTable As DataTable
        Get
            Return _BookingPriceMatchingDataset.Tables("BookingPriceMatching")
        End Get
    End Property

    Public ReadOnly Property PriceMatchingCodeDataTable As DataTable
        Get
            Return _BookingPriceMatchingDataset.Tables("PriceMatchingCode")
        End Get
    End Property

    Public ReadOnly Property CompetitorsDataTable As DataTable
        Get
            Return _BookingPriceMatchingDataset.Tables("Competitors")
        End Get
    End Property

    Public ReadOnly Property PriceMatchingStatusDataTable As DataTable
        Get
            Return _BookingPriceMatchingDataset.Tables("PriceMatchingStatus")
        End Get
    End Property

    'Public ReadOnly Property PriceMatchingTHLbrands As DataTable
    '    Get
    '        Return _BookingPriceMatchingDataset.Tables("THLbrands")
    '    End Get
    'End Property


    Sub New(RentalId As String)
        LoadDataSet(RentalId)
    End Sub

    Sub New()
    End Sub

    Private Sub LoadDataSet(RentalId As String)
        _BookingPriceMatchingDataset = New DataSet
        If (Not String.IsNullOrEmpty(RentalId)) Then
            Aurora.Common.Data.ExecuteDataSetSP("getRentalDetailsForPriceMatch", _BookingPriceMatchingDataset, RentalId)
            If (_BookingPriceMatchingDataset.Tables.Count - 1 = 4) Then
                _BookingPriceMatchingDataset.Tables(0).TableName = "BookingInformation"
                _BookingPriceMatchingDataset.Tables(1).TableName = "BookingPriceMatching"
                _BookingPriceMatchingDataset.Tables(2).TableName = "PriceMatchingCode"
                _BookingPriceMatchingDataset.Tables(3).TableName = "Competitors"
                _BookingPriceMatchingDataset.Tables(4).TableName = "PriceMatchingStatus"
                ''_BookingPriceMatchingDataset.Tables(5).TableName = "THLbrands"
            End If
        End If
    End Sub

#Region "Unused"
    'Private Function BookingPriceMatchingInsertUpdate(dataTable As DataTable) As Integer
    '    Dim rowCount As Integer = 0
    '    Try
    '        If (Not dataTable Is Nothing) Then

    '            Dim PriceMRntId As String = String.Empty
    '            Dim PriceMRequestCodTypeId As String = String.Empty
    '            Dim PriceMRequestor As String = String.Empty
    '            Dim PriceMCompetitorCodId As String = String.Empty
    '            Dim PriceMCompetitorOthers As String = String.Empty
    '            Dim PriceMCompetitorVehicle As String = String.Empty
    '            Dim PriceMCompetitorPrice As Decimal = 0.0
    '            Dim PriceMCompetitorQuoteDate As DateTime = DateTime.MinValue
    '            Dim PriceMCompetitorQuoteLink As String = String.Empty
    '            Dim PriceMStatusCodId As String = String.Empty
    '            Dim PriceMDecisionBy As String = String.Empty
    '            Dim PriceMNote As String = String.Empty
    '            Dim AddUsrId As String = String.Empty
    '            Dim PriceMSeqId As Int32 = -1

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMRntId"))) Then
    '                PriceMRntId = dataTable.Rows(0).Item("PriceMRntId").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMRequestCodTypeId"))) Then
    '                PriceMRequestCodTypeId = dataTable.Rows(0).Item("PriceMRequestCodTypeId").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMRequestor"))) Then
    '                PriceMRequestor = dataTable.Rows(0).Item("PriceMRequestor").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorCodId"))) Then
    '                PriceMCompetitorCodId = dataTable.Rows(0).Item("PriceMCompetitorCodId").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorOthers"))) Then
    '                PriceMCompetitorOthers = dataTable.Rows(0).Item("PriceMCompetitorOthers").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorVehicle"))) Then
    '                PriceMCompetitorVehicle = dataTable.Rows(0).Item("PriceMCompetitorVehicle").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorPrice"))) Then
    '                PriceMCompetitorPrice = Convert.ToDecimal(dataTable.Rows(0).Item("PriceMCompetitorPrice"))
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorVehicle"))) Then
    '                PriceMCompetitorVehicle = dataTable.Rows(0).Item("PriceMCompetitorVehicle").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorQuoteDate "))) Then
    '                PriceMCompetitorQuoteDate = Convert.ToDateTime(dataTable.Rows(0).Item("PriceMCompetitorQuoteDate "))
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMCompetitorQuoteLink"))) Then
    '                PriceMCompetitorQuoteLink = dataTable.Rows(0).Item("PriceMCompetitorQuoteLink").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMStatusCodId"))) Then
    '                PriceMStatusCodId = dataTable.Rows(0).Item("PriceMStatusCodId").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMDecisionBy"))) Then
    '                PriceMDecisionBy = dataTable.Rows(0).Item("PriceMDecisionBy").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMNote"))) Then
    '                PriceMNote = dataTable.Rows(0).Item("PriceMNote").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("AddUsrId"))) Then
    '                AddUsrId = dataTable.Rows(0).Item("AddUsrId").ToString.Trim
    '            End If

    '            If (Not IsDBNull(dataTable.Rows(0).Item("PriceMSeqId"))) Then
    '                PriceMSeqId = Convert.ToInt32(dataTable.Rows(0).Item("PriceMSeqId"))
    '            End If


    '            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

    '                Try

    '                    rowCount = Booking_PriceMatchingInsertUpdate(IIf(String.IsNullOrEmpty(PriceMSeqId), -1, Convert.ToInt32(PriceMSeqId)), _
    '                                               PriceMRntId, _
    '                                               PriceMRequestCodTypeId, _
    '                                               PriceMRequestor, _
    '                                               PriceMCompetitorCodId, _
    '                                                PriceMCompetitorOthers, _
    '                                                PriceMCompetitorVehicle, _
    '                                                PriceMCompetitorPrice, _
    '                                                PriceMCompetitorQuoteDate, _
    '                                                PriceMCompetitorQuoteLink, _
    '                                                PriceMStatusCodId, _
    '                                                PriceMDecisionBy, _
    '                                                PriceMNote, _
    '                                                AddUsrId)

    '                    oTransaction.RollbackTransaction()
    '                    '' oTransaction.CommitTransaction()
    '                Catch ex As Exception
    '                    oTransaction.RollbackTransaction()
    '                    oTransaction.Dispose()
    '                    rowCount = -1
    '                End Try
    '            End Using
    '        End If
    '    Catch ex As Exception
    '        rowCount = -1
    '    End Try
    '    Return rowCount
    'End Function
#End Region
    

    Public Shared Function BookingPriceMatchingInsertUpdate(PriceMSeqId As Int32, _
                            PriceMRntId As String, _
                            PriceMRequestCodTypeId As String, _
                            PriceMRequestor As String, _
                            PriceMCompetitorCodId As String, _
                            PriceMCompetitorOthers As String, _
                            PriceMCompetitorVehicle As String, _
                            PriceMCompetitorPrice As String, _
                            PriceMCompetitorQuoteDate As String, _
                            PriceMCompetitorQuoteLink As String, _
                            PriceMStatusCodId As String, _
                            PriceMDecisionBy As String, _
                            PriceMNote As String, _
                            AddUsrId As String, _
                            PriceMAlternateVehicle As String, _
                            PriceMAlternatePrice As String, _
                            Optional products As String = "") As Integer
        Dim rowCount As Integer = 0
        Try


            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

                Try

                    rowCount = Booking_PriceMatchingInsertUpdate(IIf(String.IsNullOrEmpty(PriceMSeqId), -1, Convert.ToInt32(PriceMSeqId)), _
                                               PriceMRntId, _
                                               PriceMRequestCodTypeId, _
                                               PriceMRequestor, _
                                               PriceMCompetitorCodId, _
                                                PriceMCompetitorOthers, _
                                                PriceMCompetitorVehicle, _
                                                Convert.ToDecimal(PriceMCompetitorPrice), _
                                                PriceMCompetitorQuoteDate, _
                                                PriceMCompetitorQuoteLink, _
                                                PriceMStatusCodId, _
                                                PriceMDecisionBy, _
                                                PriceMNote, _
                                                AddUsrId, _
                                                PriceMAlternateVehicle, _
                                                PriceMAlternatePrice, _
                                                products)

                    Logging.LogInformation("BookingPriceMatching".ToUpper, "Calling Booking_PriceMatchingInsertUpdate and result is " & rowCount)
                    If (rowCount = -1) Then
                        Throw New Exception
                    Else
                        ''oTransaction.RollbackTransaction()
                        oTransaction.CommitTransaction()
                    End If


                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    oTransaction.Dispose()
                    rowCount = -1
                    Logging.LogError("BookingPriceMatchingInsertUpdate INSIDE TRANSACTION", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
                End Try
            End Using

        Catch ex As Exception
            Logging.LogError("BookingPriceMatchingInsertUpdate", ex.Message & ", " & ex.Source & "," & ex.StackTrace)
            rowCount = -1
        End Try
        Return rowCount
    End Function
End Class
