Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookingCancellation

    Public Shared Function GetCancellationDetail(ByVal bookingId As String, ByVal rentalId As String, _
        ByVal userCode As String) As DataSet

        Return Data.DataRepository.GetCancellationDetail(bookingId, rentalId, userCode)

    End Function

    Public Shared Function GetIntegrityNoForRnt(ByVal rentalId As String, _
        ByVal bpdId As String, ByVal integrityNo As Integer) As String

        Return Data.DataRepository.GetIntegrityNoForRnt(rentalId, bpdId, integrityNo)

    End Function

    Public Shared Function ManageCancelRental(ByVal bookingId As String, ByVal rentalId As String, _
      ByVal cancelWhen As String, ByVal reasonId As String, ByVal mocId As String, _
      ByVal typeId As String, ByVal isNoteRequired As Boolean, ByVal userCode As String, _
      ByVal programName As String) As String

        Dim returnMessage As String
        returnMessage = Data.DataRepository.ManageCancelRental(bookingId, rentalId, cancelWhen, _
            reasonId, mocId, typeId, isNoteRequired, userCode, programName)

        returnMessage = returnMessage.Replace("<data>", "")
        returnMessage = returnMessage.Replace("</data>", "")
        Return returnMessage


    End Function

    Public Shared Function GetAllRentalFORBooking(ByVal bookingId As String) As DataTable

        Dim ds As DataSet
        ds = Data.DataRepository.GetAllRentalFORBooking(bookingId)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function

    Public Shared Function ManageCancelBooking(ByVal bookingId As String, ByVal userCode As String, _
             ByVal programName As String) As String

        Dim returnMessage As String
        returnMessage = Data.DataRepository.ManageCancelBooking(bookingId, userCode, programName)

        returnMessage = returnMessage.Replace("<data>", "")
        returnMessage = returnMessage.Replace("</data>", "")
        Return returnMessage

    End Function


    Public Shared Function CancelRental(ByVal rentalId As String, ByVal extnRef As String, ByVal status As String, _
        ByVal bookingId As String, ByVal dateString As String, ByVal timeString As String, _
        ByVal reason As String, ByVal notification As String, ByVal type As String, ByVal intNo As Integer, _
        ByVal cityCode As String, ByVal userCode As String, ByRef returnMessage As String) As Boolean

        'Validation for IntegrityNo 
        Dim errorMessage As String
        errorMessage = BookingCancellation.GetIntegrityNoForRnt(rentalId, "", intNo)

        If Not (String.IsNullOrEmpty(errorMessage) Or errorMessage.ToUpper().Trim() = "SUCCESS") Then
            'CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            returnMessage = errorMessage
            Return False
        End If

        'Cancel Rental
        errorMessage = BookingCancellation.ManageCancelRental(bookingId, rentalId, dateString & " " & timeString, _
            reason, notification, type, False, userCode, "CancelRental")

        If Not (String.IsNullOrEmpty(errorMessage) Or errorMessage.ToUpper().Trim() = "SUCCESS") Then
            'CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            returnMessage = errorMessage
            Return False
        End If

        'Update DVASS
        If status = "KK" Or status = "NN" Then

            If cityCode.ToString().ToUpper = "AU" Then
                Aurora.Reservations.Services.AuroraDvass.selectCountry(0)
            ElseIf cityCode.ToString().ToUpper = "NZ" Then
                Aurora.Reservations.Services.AuroraDvass.selectCountry(1)
            End If

            Dim pDvassStatus As String = ""

            Aurora.Reservations.Services.AuroraDvass.rentalCancel(extnRef, 1, 0, pDvassStatus)

            If pDvassStatus.ToUpper.Trim <> "SUCCESS" Then

                If InStr(pDvassStatus, "333") = 0 Then

                    'CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, pDvassStatus)
                    returnMessage = pDvassStatus
                    Return False
                End If ' If InStr(pDvassStatus, "333") = 0 Then

            End If 'If pDvassStatus.ToUpper.Trim <> "SUCCESS" Then
        End If 'If ViewState(BookingCancellation_RentalStatus_ViewState) = "KK" Or ViewState(BookingCancellation_RentalStatus_ViewState) = "NN" Then

        Return True


    End Function

    Public Shared Function CancelBooking(ByVal bookingId As String, ByVal dateString As String, ByVal timeString As String, _
        ByVal reason As String, ByVal notification As String, ByVal type As String, ByVal intNo As Integer, _
        ByVal cityCode As String, ByVal userCode As String, ByRef returnMessage As String) As Boolean

        Dim dt As DataTable
        dt = BookingCancellation.GetAllRentalFORBooking(bookingId)

        For Each dr As DataRow In dt.Rows

            If Not cancelRental(dr.Item("RntId"), dr.Item("ExtnRef"), dr.Item("RntStatus"), _
                bookingId, dateString, timeString, _
                reason, notification, type, intNo, _
                cityCode, userCode, returnMessage) Then

                Return False
            End If
        Next

        Dim errorMessage As String
        errorMessage = BookingCancellation.ManageCancelBooking(bookingId, userCode, "CancelBooking")

        If Not (String.IsNullOrEmpty(errorMessage) Or errorMessage.ToUpper().Trim() = "SUCCESS") Then
            'CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            returnMessage = errorMessage
            Return False
        End If

        Return True

    End Function




End Class
