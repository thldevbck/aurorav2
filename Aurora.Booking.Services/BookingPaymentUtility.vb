Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common


Public Class BookingPaymentUtility


#Region " Variables"
   
#End Region

#Region " Functions"

    Public Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                        ByVal sErrNumber As String, _
                                        ByVal sErrType As String, _
                                        ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Public Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Public Shared Function ValidateDate(ByVal sDate As String) As Boolean
        Dim sDateString As String()
        Dim mydate As String
        Try
            mydate = Convert.ToDateTime(sDate)
        Catch ex As Exception
            Return False
        End Try

        sDateString = sDate.Split("/")

        If UBound(sDateString) <> 2 Then
            Return False
        End If

        If sDateString(0) > 31 Or sDateString(1) > 12 Or Len(sDateString(2)) <> 4 Then
            Return False
        End If

        Return True

    End Function


#End Region



End Class
