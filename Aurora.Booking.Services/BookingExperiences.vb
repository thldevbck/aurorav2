﻿Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml


Public Class BookingExperiences
    Public Shared Function CallTicketsSummary(RentalId As String) As DataSet
        Return Data.DataRepository.CallTicketsSummary(RentalId)
    End Function

    Public Shared Function LoadStaticData(Country As String) As DataSet
        Return Data.DataRepository.LoadStaticData(Country)
    End Function

    Public Shared Function GetCustomerInfo(BookingId As String, RentalId As String) As DataSet
        Return Data.DataRepository.GetCustomerInfo(BookingId, RentalId)
    End Function

    Public Shared Function GetCallContactHistory(ExpCallId As Integer, RentalId As String) As DataSet
        Return Data.DataRepository.GetCallContactHistory(ExpCallId, RentalId)
    End Function

    Public Shared Function GetPurchaseHistory(ExpCallId As Integer) As DataSet
        Return Data.DataRepository.GetPurchaseHistory(ExpCallId)
    End Function

    Public Shared Function InsertRecordInHistory(ExpCallRntId As String, ExpCallStatusId As String, ExpCallOutboundCallDt As DateTime, ExpCallNotes As String, AddUsrId As String, ExpCallPhNumber As String) As String
        Return Data.DataRepository.InsertRecordInHistory(ExpCallRntId, ExpCallStatusId, ExpCallOutboundCallDt, ExpCallNotes, AddUsrId, ExpCallPhNumber)
    End Function

    Public Shared Function UpdateRecordAndCloneInHistory(ExpCallId As String, ExpCallStatusId As String, ExpCallOutboundCallDt As DateTime, ExpCallNotes As String, ModUsrId As String, ExpCallPhNumber As String) As String
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                Dim result As String = Data.DataRepository.CloneRecordInHistory(ExpCallId, ExpCallStatusId)
                If (result.Contains("OK")) Then
                    result = Data.DataRepository.UpdateRecordInHistory(ExpCallId, ExpCallStatusId, ExpCallOutboundCallDt, ExpCallNotes, ModUsrId, ExpCallPhNumber)
                End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Return "ERROR"
            End Try
        End Using
        Return "OK"
    End Function

    Public Shared Function GetCallContactHistories(ExpCallId As Integer) As DataSet
        Return Data.DataRepository.GetCallContactHistories(ExpCallId)
    End Function

    Public Shared Function InsertExperienceInHistory(ExpCallId As String, ExpItemId As String, ExpCallTktPrice As Decimal, ExpCallTktQty As Integer, ExpCallTktTotal As Decimal, ExpCallTktDateTime As DateTime, ExpCallTktSupInfo As String, ExpCallTktStatusId As String, ExpCallTktResRefNum As String, ExpCallTktPrdLinkId As String, AddUsrId As String) As String
        Dim result As String = ""
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                result = Data.DataRepository.InsertExperienceInHistory(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, AddUsrId)
                If (result.Contains("ERROR")) Then
                    Throw New Exception(result)
                End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Return "ERROR: " & ex.Message.Replace("ERROR", "")
            End Try
        End Using

        Return "OK"
    End Function

    Public Shared Function GetExperienceSummary(ExpCallId As Integer) As DataSet
        Return Data.DataRepository.GetExperienceSummary(ExpCallId)
    End Function

    Public Shared Function GetSingleExperienceCall(ExpCallId As Integer) As DataSet
        Return Data.DataRepository.GetSingleExperienceCall(ExpCallId)
    End Function

    Public Shared Function GetSingleExperienceCallTickets(ExpCallTktId As Integer) As DataSet
        Return Data.DataRepository.GetSingleExperienceCallTickets(ExpCallTktId)
    End Function

    Public Shared Function UpdateSingleExperienceCallTickets(ExpCallId As String, ExpItemId As String, ExpCallTktPrice As Decimal, ExpCallTktQty As Integer, ExpCallTktTotal As Decimal, ExpCallTktDateTime As DateTime, ExpCallTktSupInfo As String, ExpCallTktStatusId As String, ExpCallTktResRefNum As String, ExpCallTktPrdLinkId As String, ModUsrId As String, ExpCallTktId As Integer) As String
        ''Return Data.DataRepository.UpdateSingleExperienceCallTickets(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, ModUsrId, ExpCallTktId)
        Dim result As String = ""
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                result = Data.DataRepository.UpdateSingleExperienceCallTickets(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, ModUsrId, ExpCallTktId)
                If (result.Contains("ERROR")) Then
                    Throw New Exception(result)
                End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Return "ERROR: " & ex.Message.Replace("ERROR", "")
            End Try
        End Using

        Return "OK"
    End Function

    Public Shared Function GetExperienceSupplierAndItems(ExpCallId As Integer, ExpCallTktId As Integer) As DataSet
        Return Data.DataRepository.GetExperienceSupplierAndItems(ExpCallId, ExpCallTktId)
    End Function

    Public Shared Function ConfirmationSummary(BookingId As String, RentalId As String, userId As String) As DataSet
        Return Data.DataRepository.ConfirmationSummary(BookingId, RentalId, userId)
    End Function

End Class
