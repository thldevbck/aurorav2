''colPayment
Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class BookingPaymentEntry
    Public pPaymentId As Object
    Public pMethod As Object
    Public pSubMethod As Object
    Public pDetails As Object
    Public pCurrency As Object
    Public pCurrencyName As Object
    Public pAmount As Double
    Public pLocalAmount As Double
    Public pIntegrity As Long
    Public pIsRemove As Boolean
    Public pStatus As Object
    Public pPaymentDate As Object
    Public pCollectionItems As BookingPaymentItemsCollection
    Public pOriginalPaymentAmt As Object

End Class

Public Class BookingPaymentCollection
    Implements System.Collections.IEnumerable


    Private mcolCollection As New Collection

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        GetEnumerator = mcolCollection.GetEnumerator
    End Function

    Public Function Add(ByVal pItem As BookingPaymentEntry) As BookingPaymentEntry
        Dim itemNew As New BookingPaymentEntry
        With itemNew
            .pAmount = pItem.pAmount
            .pCurrency = pItem.pCurrency
            .pCurrencyName = pItem.pCurrencyName
            .pDetails = pItem.pDetails
            .pMethod = pItem.pMethod
            .pIsRemove = pItem.pIsRemove
            .pLocalAmount = pItem.pLocalAmount
            .pSubMethod = pItem.pSubMethod
            .pPaymentId = pItem.pPaymentId
            .pStatus = pItem.pStatus
            .pPaymentDate = pItem.pPaymentDate
            .pIntegrity = pItem.pIntegrity
            .pOriginalPaymentAmt = pItem.pOriginalPaymentAmt
        End With
        itemNew.pCollectionItems = pItem.pCollectionItems
        mcolCollection.Add(itemNew)
        Add = itemNew
    End Function

    Public Function Count() As Long
        Count = mcolCollection.Count
    End Function

    Public Sub Delete(ByVal index As Object)
        mcolCollection.Remove(index)
    End Sub


    Public Function item(ByVal index As Object) As BookingPaymentEntry
        item = mcolCollection.Item(index)
    End Function
End Class
