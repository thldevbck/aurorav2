Imports Aurora.Common
Imports Aurora.Common.Data
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports Aurora.Booking.Data

Public Class BookingProducts

    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                         ByVal sErrNumber As String, _
                                         ByVal sErrType As String, _
                                         ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Public Shared Function manageBookedProductList( _
            ByVal sRntId As String, _
            ByVal sXMLData As String, _
            ByVal bPopUpValue As String, _
            ByVal sUsrId As String, _
            ByVal sPrgmName As String, _
            ByVal bSomethingIsDeleted As Boolean, ByVal sCalledFrom As String) As String


        Dim oXMLDom As New Xml.XmlDocument
        ''Dim oBookedProduct As AuroraReservations.bookedProduct = Nothing
        ''Dim objDvassCollaborate As AuroraReservations.auroraDvass = Nothing
        Dim XMLDom As New Xml.XmlDocument

        Dim oPersonRateNode As XmlNode
        Dim oBPDRec As XmlElement
        Dim oBookedProductXML As XmlElement

        Dim AIMSObj As Object

        Dim sbText As New StringBuilder

        Dim iNoOfBPDRec As Integer
        Dim I As Integer
        Dim sBpdId As String = String.Empty
        Dim sErrorString As String = String.Empty
        Dim sRentalId As String = String.Empty
        Dim sSapId As String = String.Empty
        Dim dCkoWhen As String = String.Empty
        Dim dCkiWhen As String = String.Empty
        Dim sCkoLoc As String = String.Empty
        Dim sCkiLoc As String = String.Empty
        Dim dblRate As String = String.Empty
        Dim lngQty As Long    ''ok  
        Dim iNoOfPerRate As String = String.Empty

        Dim sPrrIdList As String = String.Empty
        Dim sPrrRateList As String = String.Empty
        Dim sPrrQtyList As String = String.Empty
        Dim sPtdId As String = String.Empty
        Dim sUnitNo As String = String.Empty
        Dim sSerialCode As String = String.Empty
        Dim dChgFrom As String = String.Empty
        Dim dChgTo As String = String.Empty
        Dim lngOddoMeterOut As String = String.Empty
        Dim lngOddoMeterIn As String = String.Empty

        Dim J As Integer

        Dim sRntHisId As String = String.Empty
        Dim sError As String = String.Empty
        Dim sAgnId As String = String.Empty
        Dim lngFrom As Integer ''ok
        Dim lngTo As Integer ''ok
        Dim sCodTypeId As String = String.Empty ''ok
        Dim sErrorNo As String = String.Empty ''ok
        Dim sPopUpErros As String = String.Empty
        Dim sSPName As String = String.Empty
        Dim sExtRef As String = String.Empty
        Dim strError As String = String.Empty
        Dim sBpdSt As String = String.Empty
        Dim sRetBpd As String = String.Empty
        Dim sFoc As String = String.Empty
        Dim booIsCusCharge As String = String.Empty
        Dim Flag As Integer
        Dim bAgtChgd As Boolean = False
        Dim sMsg As String = String.Empty
        Dim sCtyCode As String = String.Empty
        Dim strQueryString As String = String.Empty

        Dim sQueryString As String = String.Empty ''ok
        Dim sRetBpdId As String = String.Empty ''ok
        Dim sTrToFin As String = String.Empty
        Dim OCkoWhen As String = String.Empty
        Dim OCkiWhen As String = String.Empty
        Dim sRntStatus As String = String.Empty
        Dim sUsrRole As String = String.Empty
        Dim sRetMsgString As String = String.Empty
        Dim sRetMsg As String = String.Empty ''ok
        Dim sBookingRef As String = String.Empty
        Dim bBooRef As Boolean
        Dim sUom As String = String.Empty

        Dim sNoteString As String = String.Empty
        Dim NteText As String = String.Empty
        Dim pReturnXML As String = String.Empty
        Dim sUpdateFleet As String = String.Empty


        Dim sOldCkoLoc As String = String.Empty
        Dim sOldCkiLoc As String = String.Empty
        Dim sExcludeBpdList As String = String.Empty
        Dim sExclLstIn As String = String.Empty
        Dim sExclLstOut As String = String.Empty  ''ok


        Dim sReturnedBPDIDForSS As String = String.Empty

        Dim localcache As New Hashtable

        ' the TRAN
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction()
            ' The BIG Try
            Try

                oXMLDom.LoadXml(sXMLData)

                If bPopUpValue.Trim.Equals("YES") Then

                    '@sRntId   VARCHAR  (64) = '',  
                    '@XmlData   TEXT     = DEFAULT,  
                    '@sPopUpErrors VARCHAR  (7000)  OUTPUT  

                    Dim params(2) As Aurora.Common.Data.Parameter
                    params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, sRntId, Parameter.ParameterType.AddInParameter)
                    params(1) = New Aurora.Common.Data.Parameter("XmlData", DbType.String, oXMLDom.OuterXml, Parameter.ParameterType.AddInParameter)
                    params(2) = New Aurora.Common.Data.Parameter("sPopUpErrors", DbType.String, 500, Parameter.ParameterType.AddOutParameter)

                    strError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_checkForManageBookedProductDetails", params)

                    If Not strError Is Nothing Then
                        If (strError.ToUpper.Equals("SUCCESS")) Then
                            strError = String.Empty
                        Else

                            If (strError.Length) = 0 Then
                                If (params("sPopUpErrors").Value.Length) > 0 Then
                                    strError = params("sPopUpErrors").Value
                                End If
                            End If
                        End If
                        If (strError.Trim.Length) <> 0 Then
                            'Return String.Concat("<POPUp>", strError, "</POPUp>")
                            'Throw New Exception(String.Concat("<POPUp>", strError, "</POPUp>"))
                            Throw New Exception("POPUP/" & strError)
                        End If
                    End If
                End If

                oBookedProductXML = oXMLDom.DocumentElement.SelectSingleNode("BookedProducts")
                iNoOfBPDRec = oBookedProductXML.ChildNodes.Count

                For I = 0 To iNoOfBPDRec - 1
                    oBPDRec = oBookedProductXML.ChildNodes(I)
                    sBpdId = oBPDRec.SelectSingleNode("BpdId").InnerText
                    sAgnId = oBPDRec.SelectSingleNode("AgnId").InnerText

                    sRentalId = sRntId
                    sExtRef = oBPDRec.SelectSingleNode("ExtRef").InnerText
                    sSapId = oBPDRec.SelectSingleNode("SapId").InnerText & ""

                    sCtyCode = UCase(oBPDRec.SelectSingleNode("CtyCode").InnerText) & ""

                    If oBPDRec.SelectSingleNode("CkoDt").InnerText <> "" Then
                        dCkoWhen = String.Concat(oBPDRec.SelectSingleNode("CkoDt").InnerText, " ", oBPDRec.SelectSingleNode("CkoTm").InnerText)
                    Else
                        dCkoWhen = String.Empty
                    End If

                    If oBPDRec.SelectSingleNode("CkiDt").InnerText <> "" Then
                        dCkiWhen = String.Concat(oBPDRec.SelectSingleNode("CkiDt").InnerText, " ", oBPDRec.SelectSingleNode("CkiTm").InnerText)
                    Else
                        dCkiWhen = String.Empty
                    End If

                    sCkoLoc = (oBPDRec.SelectSingleNode("CkoLocCode").InnerText.ToUpper)
                    sCkiLoc = (oBPDRec.SelectSingleNode("CkiLocCode").InnerText.ToUpper)
                    OCkoWhen = oBPDRec.SelectSingleNode("OCkoDate").InnerText
                    OCkiWhen = oBPDRec.SelectSingleNode("OCkiDate").InnerText

                    dblRate = oBPDRec.SelectSingleNode("Rate").InnerText
                    dblRate = IIf((dblRate = ""), 0, dblRate) ''mia

                    lngQty = oBPDRec.SelectSingleNode("Qty").InnerText
                    lngQty = IIf((lngQty = Nothing), 0, lngQty)

                    iNoOfPerRate = oBookedProductXML.ChildNodes(I).SelectSingleNode("PrrIdsList").ChildNodes.Count
                    sPrrIdList = String.Empty : sPrrQtyList = String.Empty : sPrrRateList = String.Empty

                    For J = 0 To CInt(iNoOfPerRate - 1)
                        oPersonRateNode = oBookedProductXML.ChildNodes(I).SelectSingleNode("PrrIdsList").ChildNodes(J)
                        If Not oPersonRateNode.OuterXml.Equals("*") Then
                            If CInt(oPersonRateNode.SelectSingleNode("Qty").InnerText) > 0 Then
                                sPrrIdList = sPrrIdList & oPersonRateNode.SelectSingleNode("PrrId").InnerText & ","
                                sPrrRateList = sPrrRateList & oPersonRateNode.SelectSingleNode("Rate").InnerText & ","
                                sPrrQtyList = sPrrQtyList & CInt(oPersonRateNode.SelectSingleNode("Qty").InnerText) & ","
                            End If
                        End If
                    Next J

                    If sPrrIdList <> Nothing Then
                        sPrrIdList = sPrrIdList.Substring(0, sPrrIdList.Length - 1) ''mia
                    End If

                    If sPrrQtyList <> Nothing Then
                        sPrrQtyList = sPrrQtyList.Substring(0, sPrrQtyList.Length - 1) ''mia
                    End If

                    If sPrrRateList <> Nothing Then
                        sPrrRateList = sPrrRateList.Substring(0, sPrrRateList.Length - 1) ''mia
                    End If

                    If sPrrIdList = Nothing Then sPrrIdList = "NoPrr"
                    sPtdId = oBPDRec.SelectSingleNode("PtdId").InnerText

                    sUnitNo = oBPDRec.SelectSingleNode("UniNo").InnerText
                    sUnitNo = IIf((sUnitNo = "" Or sUnitNo = String.Empty), String.Empty, sUnitNo)
                    sUnitNo = String.Empty

                    sSerialCode = oBPDRec.SelectSingleNode("SerCd").InnerText
                    sSerialCode = IIf((sSerialCode = "" Or sSerialCode = String.Empty), String.Empty, sSerialCode)
                    sSerialCode = String.Empty
                    sRntStatus = oBPDRec.SelectSingleNode("RntStatus").InnerText
                    sUsrRole = oBPDRec.SelectSingleNode("UsrRole").InnerText


                    If Not (OCkoWhen.Equals(dCkoWhen)) And _
                            Not (sRntStatus.Equals("CO")) And _
                            Not (sRntStatus.Equals("CI")) And _
                            sUsrRole = 0 Then
                        dChgFrom = dCkoWhen
                    Else
                        dChgFrom = String.Empty
                    End If

                    If Not (OCkiWhen.Equals(dCkiWhen)) And _
                       Not (sRntStatus.Equals("CO")) And _
                       Not (sRntStatus.Equals("CI")) And _
                       sUsrRole = 0 Then
                        dChgTo = dCkiWhen
                    Else
                        dChgTo = String.Empty
                    End If

                    lngOddoMeterOut = oBPDRec.SelectSingleNode("OddFr").InnerText
                    lngOddoMeterOut = IIf((lngOddoMeterOut = ""), Nothing, lngOddoMeterOut) ''mia
                    lngOddoMeterOut = Nothing ''mia


                    lngOddoMeterIn = oBPDRec.SelectSingleNode("OddTo").InnerText
                    lngOddoMeterIn = IIf((lngOddoMeterIn = ""), Nothing, lngOddoMeterIn)
                    lngOddoMeterIn = Nothing


                    sFoc = oBPDRec.SelectSingleNode("Foc").InnerText
                    sFoc = String.Empty

                    booIsCusCharge = oBPDRec.SelectSingleNode("Cus").InnerText
                    sBpdSt = String.Empty
                    sTrToFin = oBPDRec.SelectSingleNode("TrToFin").InnerText
                    bBooRef = oBPDRec.SelectSingleNode("BooRef").InnerText
                    sUom = oBPDRec.SelectSingleNode("UOM").InnerText

                    If bBooRef = True Then
                        sBookingRef = oBPDRec.SelectSingleNode("BookingRef").InnerText
                    Else
                        sBookingRef = String.Empty
                    End If

                    sUsrId = sUsrId
                    sPrgmName = sPrgmName

                    sRntHisId = oBPDRec.SelectSingleNode("RnhId").InnerText
                    sUpdateFleet = oBPDRec.SelectSingleNode("Fleet").InnerText
                    sOldCkoLoc = IIf((oBPDRec.SelectSingleNode("OldCkoLoc").InnerText = ""), String.Empty, oBPDRec.SelectSingleNode("OldCkoLoc").InnerText)
                    sOldCkiLoc = IIf((oBPDRec.SelectSingleNode("OldCkiLoc").InnerText = ""), String.Empty, oBPDRec.SelectSingleNode("OldCkiLoc").InnerText)

                    strError = Aurora.Common.Data.ExecuteScalarSP("RES_DeleteTProcessedBpds", sRentalId)

                    If Not strError Is Nothing Then
                        If (strError.ToUpper.Equals("SUCCESS")) Then
                            strError = String.Empty
                        Else
                            If (strError.Trim.Length) <> 0 Then
                                Throw New Exception("DELTPBPD/" & strError)
                            End If
                        End If
                    End If

                    If (sBpdId.Trim.Length) = 0 Then
                        If sUom.Equals("KM") Then
                            lngOddoMeterOut = 1
                            lngOddoMeterIn = CInt(oBPDRec.SelectSingleNode("KmTravelled").InnerText) + 1
                        End If

                        dCkoWhen = IIf(dCkoWhen = "", Nothing, dCkoWhen)
                        dCkiWhen = IIf(dCkiWhen = "", Nothing, dCkiWhen)
                        dChgFrom = IIf(dChgFrom = "", Nothing, dChgFrom)
                        dChgTo = IIf(dChgTo = "", Nothing, dChgTo)


                        sError = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Add( _
                                            sRentalId, sSapId, _
                                            False, dCkoWhen, _
                                            dCkiWhen, sCkoLoc, _
                                            sCkiLoc, dblRate, _
                                            lngQty, sPrrIdList, _
                                            sPrrQtyList, sPrrRateList, _
                                            sPtdId, String.Empty, _
                                            sUnitNo, sSerialCode, _
                                            String.Empty, dChgFrom, _
                                            dChgTo, lngOddoMeterOut, _
                                            lngOddoMeterIn, sRntHisId, _
                                            sUsrId, sPrgmName, sRetBpdId)

                        ' store ret bpd id for ss trans...
                        If Not String.IsNullOrEmpty(sRetBpdId) AndAlso Not Trim(sRetBpdId).Equals(String.Empty) Then
                            sReturnedBPDIDForSS = sRetBpdId
                        End If


                        If Not sError Is Nothing Then

                            If (sError.Length > 0) Then
                                Throw New Exception("ADDBPD/" & sError)
                            End If


                        End If

                        If (sRetBpdId.Trim.Length) > 0 Then
                            If (sExcludeBpdList.Trim.Length) = 0 Then
                                sExcludeBpdList = sRetBpdId
                            Else
                                sExcludeBpdList = String.Concat(sExcludeBpdList, ",", sRetBpdId)
                            End If
                        End If

                        If oBPDRec.SelectSingleNode("AddReason").InnerText = "1" And oBPDRec.SelectSingleNode("Reason").InnerText <> "" Then
                            NteText = String.Concat(NteText, "Reason for adding ", oBPDRec.SelectSingleNode("PrdShortName").InnerText, ": ", oBPDRec.SelectSingleNode("Reason").InnerText, vbCrLf)
                        End If

                        booIsCusCharge = Aurora.Common.Data.ExecuteScalarSP("RES_getBpdIsCusChargeDetail", sRetBpdId)
                        If booIsCusCharge = 0 And bAgtChgd = False Then bAgtChgd = True

                    Else
                        sRetBpd = String.Empty
                        sExclLstIn = sExcludeBpdList

                        Dim sReturnedMessage As String = String.Empty

                        sReturnedMessage = auroraUpdateBookedProduct( _
                                           sBpdId, booIsCusCharge, _
                                           sSapId, sAgnId, _
                                           dCkoWhen, dCkiWhen, _
                                           sCkoLoc, sCkiLoc, _
                                           lngFrom, lngTo, _
                                           dblRate, lngQty, _
                                           sPrrIdList, sPrrQtyList, _
                                           sPrrRateList, sPtdId, _
                                           sCodTypeId, sUnitNo, _
                                           sSerialCode, dChgFrom, _
                                           dChgTo, lngOddoMeterOut, _
                                           lngOddoMeterIn, String.Empty, _
                                           sFoc, sBpdSt, _
                                           sCtyCode, sExtRef, _
                                           sRntHisId, sUsrId, _
                                           sPrgmName, sError, _
                                           sRetMsg, sRetBpd, _
                                           String.Empty, String.Empty, _
                                           String.Empty, "ManageBookedProduct", _
                                           sBookingRef, OCkoWhen, _
                                           OCkiWhen, sRntStatus, _
                                           sOldCkoLoc, sOldCkiLoc, _
                                           sExclLstOut, sExclLstIn, sCalledFrom:=sCalledFrom)
                        ' store ret bpd id for ss trans...
                        If Not String.IsNullOrEmpty(sRetBpd) AndAlso Not Trim(sRetBpd).Equals(String.Empty) Then
                            sReturnedBPDIDForSS = sRetBpd
                        End If

                        If sRetMsg IsNot Nothing Then
                            If (sRetMsg.Trim.Length) <> 0 Then sRetMsgString = String.Concat(sRetMsgString, sRetMsg)
                        End If

                        'If Not sReturnedMessage.Equals(String.Empty) Then
                        '    sRetMsgString = sRetMsgString & sReturnedMessage
                        'End If

                        If Not sError Is Nothing Then
                            If sError.Trim.Length <> 0 Then
                                'Throw New Exception(String.Concat("<Root><Error>", sError, "</Error><Msg>", sRetMsgString, "</Msg></Root>"))
                                Throw New Exception("UPDBPD/" & sError & "/" & sRetMsgString)
                            End If


                            If (sExclLstOut.Trim.Length = 0) Then
                                sRetBpd = sBpdId
                            Else
                                If sExcludeBpdList.Trim.Length = 0 Then
                                    sExcludeBpdList = sExclLstOut
                                Else
                                    sExcludeBpdList = String.Concat(sExcludeBpdList, ",", sExclLstOut)
                                End If
                            End If


                            If Not sBpdId.Trim.Equals(sRetBpd.Trim) Then
                                If booIsCusCharge = 0 And _
                                   sTrToFin.Trim.Length <> 0 And _
                                   bAgtChgd = False Then bAgtChgd = True
                            End If
                        End If
                    End If

                Next



                '' first logical part ends here

                '' do ss trans
                ''sReturnedBPDIDForSS
                If Not sReturnedBPDIDForSS.Trim().Equals(String.Empty) Then
                    strError = Aurora.Common.Data.ExecuteScalarSP("RES_ssTransToFinance", String.Empty, String.Empty, String.Empty, sReturnedBPDIDForSS)
                    If Not String.IsNullOrEmpty(strError) AndAlso Not strError.ToUpper().Trim().Equals("SUCCESS") Then
                        Throw (New Exception("SSX/" & strError))
                    End If
                ElseIf bSomethingIsDeleted Then
                    ' if something was deleted, then also we need to run this stuff...but with Rental Id - as we dont have any other data!
                    strError = Aurora.Common.Data.ExecuteScalarSP("RES_ssTransToFinance", String.Empty, sRntId, String.Empty, String.Empty)
                    If Not String.IsNullOrEmpty(strError) AndAlso Not strError.ToUpper().Trim().Equals("SUCCESS") Then
                        Throw (New Exception("SSX/" & strError))
                    End If
                End If
                '' done ss trans

                Flag = 100

                If Not NteText.Equals("") Then


                    '@psPkgNoteRntId	VARCHAR(64)		=	NULL,
                    '@psAgnNoteRntId	VARCHAR(64)		=	NULL,
                    '@psVIPRntId		VARCHAR(64)		=	NULL,
                    '@sRntId			VARCHAR(64)		=	NULL,
                    '@sTxt			VARCHAR(5000)	=	NULL,
                    '@psUsrId		VARCHAR(64),
                    '@psProgramName	VARCHAR(64),
                    '@psNteID		VARCHAR(64)		OUTPUT

                    Dim params(7) As Aurora.Common.Data.Parameter
                    params(0) = New Aurora.Common.Data.Parameter("psPkgNoteRntId", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)
                    params(1) = New Aurora.Common.Data.Parameter("psAgnNoteRntId", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)
                    params(2) = New Aurora.Common.Data.Parameter("psVIPRntId", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)
                    params(3) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, sRentalId, Parameter.ParameterType.AddInParameter)
                    params(4) = New Aurora.Common.Data.Parameter("sTxt", DbType.String, NteText.TrimStart.TrimEnd, Parameter.ParameterType.AddInParameter)
                    params(5) = New Aurora.Common.Data.Parameter("psUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                    params(6) = New Aurora.Common.Data.Parameter("psProgramName", DbType.String, sPrgmName, Parameter.ParameterType.AddInParameter)
                    params(7) = New Aurora.Common.Data.Parameter("psNteID", DbType.String, 64, Parameter.ParameterType.AddOutParameter)


                    ''oAuroraDAL.appendParameterIn("@psPkgNoteRntId", 200, 64, "")
                    ''oAuroraDAL.appendParameterIn("@psAgnNoteRntId", 200, 64, "")
                    ''oAuroraDAL.appendParameterIn("@psVIPRntId", 200, 64, "")
                    ''oAuroraDAL.appendParameterIn("@sRntId", 200, 64, sRentalId)
                    ''oAuroraDAL.appendParameterIn("@sTxt", 200, (NteText.Length), (NteText.TrimStart.TrimEnd))
                    ''oAuroraDAL.appendParameterIn("@psUsrId", 200, 64, sUsrId)
                    ''oAuroraDAL.appendParameterIn("@psProgramName", 200, 64, sPrgmName)
                    ''oAuroraDAL.appendParameterOut("@psNteID", 200, 64, "", True)
                    sNoteString = Aurora.Common.Data.ExecuteOutputObjectSP("RES_CreateNotes", params) ''oAuroraDAL.UpdateRecords("RES_CreateNotes")

                    If (sNoteString.ToUpper) <> "SUCCESS" Then
                        Throw New Exception("NOTE/" & sNoteString)
                    End If



                End If ''......If Not NteText.Equals("")

                ''manny: todo

                If bAgtChgd = True Then

                    Dim params(10) As Aurora.Common.Data.Parameter

                    ' @sRntId				VARCHAR (64) ,
                    params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, sRntId, Parameter.ParameterType.AddInParameter)

                    '@sAgnId				VARCHAR (64)		=	NULL ,		-- Added
                    params(1) = New Aurora.Common.Data.Parameter("sAgnId", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)

                    '@sUsrId				VARCHAR (64) ,	
                    params(2) = New Aurora.Common.Data.Parameter("sUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)

                    '@sPrgName			VARCHAR (256),
                    params(3) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, sPrgmName, Parameter.ParameterType.AddInParameter)

                    '@sExclBpdId			VARCHAR (64)		=	NULL ,	
                    params(4) = New Aurora.Common.Data.Parameter("sExclBpdId", DbType.String, sExcludeBpdList, Parameter.ParameterType.AddInParameter)

                    '@rRnhId				VARCHAR (64)		=	NULL,
                    params(5) = New Aurora.Common.Data.Parameter("rRnhId", DbType.String, sRntHisId, Parameter.ParameterType.AddInParameter)

                    '@sBpdId				VARCHAR (64)		=   NULL,
                    params(6) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)

                    '@sNewIsCusCharge	BIT					=   NULL,
                    params(7) = New Aurora.Common.Data.Parameter("sNewIsCusCharge", DbType.Boolean, DBNull.Value, Parameter.ParameterType.AddInParameter)

                    '@sErrors			VARCHAR (500)		OUTPUT ,
                    params(8) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 500, Parameter.ParameterType.AddOutParameter)

                    '@sMessages			VARCHAR (200)		OUTPUT ,
                    params(9) = New Aurora.Common.Data.Parameter("sMessages", DbType.String, 200, Parameter.ParameterType.AddOutParameter)

                    '@sNewBpdIdsList		VARCHAR (8000)		OUTPUT
                    params(10) = New Aurora.Common.Data.Parameter("sNewBpdIdsList", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
                    strError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_ManageBookedProductAgentChange", params)

                    If (strError.ToUpper.Equals("SUCCESS")) Then strError = ""

                    If (strError.Trim.Length) <> 0 Then
                        'Throw New Exception(strError)
                        Throw New Exception("AGNCHG/" & strError & "/")
                    End If

                    sMsg = params(9).Value
                    If (sMsg.Trim.Length) <> 0 Then sRetMsgString = sRetMsgString & sMsg

                    strError = params(8).Value

                    If (strError.Trim.Length <> 0) Then
                        'Throw New Exception(String.Concat("<Root><Error>", strError, "</Error><Msg>", sRetMsgString, "</Msg></Root>"))
                        Throw New Exception("AGNCHG/" & strError & "/" & sRetMsgString)
                    End If

                    'With oAuroraDAL
                    '    Call .appendParameterIn("@sRntId", 200, 64, sRntId)
                    '    Call .appendParameterIn("@sUsrId", 200, 64, sUsrId)
                    '    Call .appendParameterIn("@sPrgName", 200, 256, sPrgmName)
                    '    Call .appendParameterIn("@sExclBpdId", 200, 8000, sExcludeBpdList)
                    '    Call .appendParameterIn("@rRnhId", 200, 64, sRntHisId)
                    '    Call .appendParameterOut("@sErrors", 200, 2000, sError, True)
                    '    Call .appendParameterOut("@sMessages", 200, 2000, sMsg, True)
                    '    Call .appendParameterOut("@sNewBpdIdsList", 200, 2000, sRetBpd, True)

                    '    strError = .UpdateRecords(sSPName, "", True)

                    '    If (strError.ToUpper.Equals("SUCCESS")) Then strError = ""

                    '    If (strError.Trim.Length) <> 0 Then
                    '        ContextUtil.SetAbort()
                    '        DisposeExistingObject(oAuroraDAL)
                    '        Return String.Concat("<Root>", strError, "</Root>")
                    '    End If

                    '    sMsg = Datalayer.RetOutPutValue("@sMessages")
                    '    If (sMsg.Trim.Length) <> 0 Then sRetMsgString = sRetMsgString & sMsg
                    '    strError = Datalayer.RetOutPutValue("@sErrors")

                    '    If (strError.Trim.Length <> 0) Then
                    '        ContextUtil.SetAbort()
                    '        DisposeExistingObject(oAuroraDAL)
                    '        Return String.Concat("<Root><Error>", strError, "</Error><Msg>", sRetMsgString, "</Msg></Root>")
                    '    End If

                    'End With


                End If ''......If bAgtChgd = True


                If (sUpdateFleet.Trim.Length <> 0) Then

                    sSPName = "RES_getBPDetailsDvassAndAimsData"
                    Dim params(2) As Aurora.Common.Data.Parameter

                    ''@psBpdId	VARCHAR(64)	= NULL,
                    params(0) = New Aurora.Common.Data.Parameter("psBpdId", DbType.String, sBpdId, Parameter.ParameterType.AddInParameter)

                    ''@pReturnXml	VARCHAR(4000)	OUTPUT,
                    params(1) = New Aurora.Common.Data.Parameter("pReturnXml", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)

                    ''@pReturnError	VARCHAR(500)	OUTPUT
                    params(2) = New Aurora.Common.Data.Parameter("pReturnError", DbType.String, 500, Parameter.ParameterType.AddOutParameter)

                    strError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_getBPDetailsDvassAndAimsData", params)

                    If strError.ToUpper.Equals("SUCCESS") Then strError = String.Empty

                    If (strError.Trim.Length <> 0) Then
                        Throw New Exception("GETBPDDETDVS/" & strError)
                    End If

                    pReturnXML = params(1).Value

                    'With oAuroraDAL
                    '    Call .appendParameterIn("@psBpdId", 200, 64, sBpdId)
                    '    Call .appendParameterOut("@pReturnXml", 200, 4000, pReturnXML, True)
                    '    Call .appendParameterOut("@pReturnError", 200, 500, strError, True)

                    '    strError = .UpdateRecords(sSPName, "", True)
                    '    If strError.ToUpper.Equals("SUCCESS") Then strError = String.Empty

                    '    If (strError.Trim.Length <> 0) Then
                    '        ContextUtil.SetAbort()
                    '        DisposeExistingObject(oAuroraDAL)
                    '        Return String.Concat("<Root>", strError, "</Root>")
                    '    End If

                    '    pReturnXML = Datalayer.RetOutPutValue("@pReturnXml")
                    'End With '--  With oAuroraDAL

                    XMLDom.LoadXml(pReturnXML)

                    Dim dAiDvCkoDate As Date
                    Dim dAiDvCkiDate As Date

                    Dim mBpdCkoOdometer As Double
                    Dim mBpdCkiOdometer As Double

                    Dim sCkoDayPart As String
                    Dim sCkoLocCode As String
                    Dim sCkiDayPart As String
                    Dim sCkiLocCode As String
                    Dim sBpdStatus As String
                    Dim sBpdCkoStatus As String
                    Dim sBpdBooNum As String
                    Dim sBpdSapPrdId As String
                    Dim sBpdRntNum As String
                    Dim sUnitNum As String
                    Dim sExternalRef As String
                    Dim AIMSRetValue As Long

                    With XMLDom.DocumentElement
                        ''ErrorLog(Space(6) & "0")
                        dAiDvCkoDate = .SelectSingleNode("Row/BpdCkoWhen").InnerText
                        ''ErrorLog(Space(6) & "1")
                        sCkoDayPart = .SelectSingleNode("Row/BpdCkoDayPart").InnerText
                        ''ErrorLog(Space(6) & "2")
                        sCkoLocCode = .SelectSingleNode("Row/BpdCkoLocCode").InnerText
                        ''ErrorLog(Space(6) & "3")
                        mBpdCkoOdometer = .SelectSingleNode("Row/BpdCkoOdometer").InnerText
                        ''ErrorLog(Space(6) & "4")
                        dAiDvCkiDate = .SelectSingleNode("Row/BpdCkiWhen").InnerText
                        ''ErrorLog(Space(6) & "5")
                        sCkiDayPart = .SelectSingleNode("Row/BpdCkoDayPart").InnerText
                        ''ErrorLog(Space(6) & "6")
                        sCkiLocCode = .SelectSingleNode("Row/BpdCkiLocCode").InnerText
                        ''ErrorLog(Space(6) & "7")
                        mBpdCkiOdometer = .SelectSingleNode("Row/BpdCkiOdometer").InnerText
                        ''ErrorLog(Space(6) & "8")
                        sBpdStatus = .SelectSingleNode("Row/BpdStatus").InnerText
                        ''ErrorLog(Space(6) & "9")
                        sBpdCkoStatus = .SelectSingleNode("Row/BpdCkoStatus").InnerText
                        ''ErrorLog(Space(6) & "10")
                        sBpdBooNum = .SelectSingleNode("Row/BpdBooNum").InnerText
                        ''ErrorLog(Space(6) & "11")
                        sBpdSapPrdId = .SelectSingleNode("Row/BpdSapPrdId").InnerText
                        ''ErrorLog(Space(6) & "12")
                        sBpdRntNum = .SelectSingleNode("Row/BpdRntNum").InnerText
                        ''ErrorLog(Space(6) & "13")
                        sUnitNum = .SelectSingleNode("Row/BpdUnitNum").InnerText
                        ''ErrorLog(Space(6) & "14")
                    End With    '-- With XMLDom.documentElement


                    If sUpdateFleet.Trim.Equals("AIMS") Then

                        Dim sXmlString As String
                        sExternalRef = String.Concat(sBpdBooNum, "/", sBpdRntNum)
                        ''AIMSObj = CreateObject("AI.CMaster")
                        AIMSObj = New AI.CMaster


                        'With sbText
                        '    .AppendFormat("{0}", Space(4) & "------ START AIMS : F7_ActivityUpdate --------" & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "Parameters :" & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "Activity Type :" & "Rental" & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "External Ref :" & sExternalRef & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "Unit Number :" & sUnitNum & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "Oddometer Out :" & mBpdCkoOdometer & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "EndLocation :" & sCkiLocCode & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "End Date :" & dAiDvCkiDate & vbCrLf)
                        '    .AppendFormat("{0}", Space(6) & "EndOdometer :" & -1 & vbCrLf)
                        '    sbText = Nothing : sbText = New StringBuilder
                        'End With



                        AIMSRetValue = AIMSObj.F7_ActivityUpdate("Rental", sExternalRef, sUnitNum, CLng(mBpdCkoOdometer), sCkiLocCode, dAiDvCkiDate, -1, "")


                        If AIMSRetValue <> 0 Then
                            'sXmlString = Aurora.Reservations.Services.BookedProduct.getAimsMessage(AIMSRetValue)
                            'sErrorString = GetErrorTextFromResource(True, AIMSRetValue, "Application", sXmlString)
                            'Return String.Concat("<Root><Error>", sErrorString, "</Error><Msg></Msg></Root>")
                            Throw New Exception("AIMS/" & Aurora.Reservations.Services.BookedProduct.getAimsMessage(AIMSRetValue))

                        End If ' If AIMSRetValue <> 0 Then


                    ElseIf sUpdateFleet.Trim.Equals("DVASS") Then ''.....If sUpdateFleet.Trim.Equals("AIMS")

                        Dim strDvassReturnError As String = String.Empty

                        sExternalRef = String.Concat(sBpdBooNum, "/", sBpdRntNum)


                        'With sbText
                        '    .AppendFormat("{0}", "------ START DVASS CALL BECAUSE STATUS IS KK OR NN --------" & vbCrLf)
                        '    .AppendFormat("{0}", "------ PARAMETER PASSED --------" & vbCrLf)
                        '    .AppendFormat("{0}", "sBooRntNum " & sExternalRef & vbCrLf)
                        '    .AppendFormat("{0}", "strProductId " & sBpdSapPrdId & vbCrLf)
                        '    .AppendFormat("{0}", "sRntCkoLoc " & sCkoLocCode & vbCrLf)
                        '    .AppendFormat("{0}", "sRntCkoWhen " & CDate(dAiDvCkoDate) & vbCrLf)
                        '    .AppendFormat("{0}", "strCkoDayPart " & convertStringDayPartToInteger(sCkoDayPart) & vbCrLf)
                        '    .AppendFormat("{0}", "sRntCkiLocCode " & sCkiLocCode & vbCrLf)
                        '    .AppendFormat("{0}", "sRntCkiWhen " & CDate(dAiDvCkiDate) & vbCrLf)
                        '    .AppendFormat("{0}", "strCkiDayPart " & convertStringDayPartToInteger(sCkiDayPart) & vbCrLf)
                        '    .AppendFormat("{0}", "------ Starting DVASS for " & sCtyCode & " --------" & vbCrLf)
                        '    ErrorLog(.ToString)
                        'End With



                        If UCase(sCtyCode) = "AU" Then

                            Call Aurora.Reservations.Services.AuroraDvass.selectCountry(0)

                        ElseIf UCase(sCtyCode) = "NZ" Then

                            Call Aurora.Reservations.Services.AuroraDvass.selectCountry(1)
                        End If


                        Call Aurora.Reservations.Services.AuroraDvass.rentalModify _
                                   ( _
                                      sExternalRef, _
                                      sBpdSapPrdId, _
                                      sCkoLocCode, _
                                      CDate(dAiDvCkoDate), _
                                      Aurora.Reservations.Services.BookedProduct.convertStringDayPartToInteger(sCkoDayPart), _
                                      sCkiLocCode, _
                                      CDate(dAiDvCkiDate), _
                                      Aurora.Reservations.Services.BookedProduct.convertStringDayPartToInteger(sCkiDayPart), _
                                      CDbl(0), _
                                      1, _
                                      10, _
                                      "", _
                                      1, _
                                      strDvassReturnError _
                                   )

                        If Len(strDvassReturnError) <> 0 Then
                            'Throw New Exception(String.Concat("<Root><Error>", sError, "</Error><Msg>" & strDvassReturnError & "</Msg></Root>"))
                            Throw New Exception("RENTAL/" & sError & "/" & strDvassReturnError)
                        End If

                    End If ''.....If sUpdateFleet.Trim.Equals("AIMS")
                End If ''.....If (sUpdateFleet.Trim.Length <> 0)

                ' update the integrity nos!
                DataRepository.UpdateRentalIntegrityNumber(sRntId)
                '' tran has completed :)

                oTransaction.CommitTransaction()
                oTransaction.Dispose()

                'manageBookedProductList = String.Concat("<Root><Error></Error><Msg>" & getMessageFromDB("GEN046") & "</Msg></Root>")
                Return (String.Concat("<Root><Error></Error><Msg>" & getMessageFromDB("GEN046") & "</Msg></Root>"))


            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Try
                    Logging.LogException("BOOKEDPRODUCTDETAILS", ex)
                Catch
                End Try
                ' see the type of error and behave accordingly
                Select Case ex.Message.Split("/"c)(0)
                    Case "POPUP"
                        Return ("<POPUp>" & ex.Message.Split("/"c)(1) & "</POPUp>")
                    Case "DELTPBPD"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - Failed to delete Temp Processed BPD Ids</Msg></Root>")
                    Case "ADDBPD"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - Failed to add Booked Product(s) - " & ex.Message.Split("/"c)(1) & "</Msg></Root>")
                    Case "UPDBPD"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - " & ex.Message.Split("/"c)(2) & " - Failed to update BPD Ids" & "</Msg></Root>")
                    Case "NOTE"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - Failed to add note</Msg></Root>")
                    Case "AGNCHG"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - " & ex.Message.Split("/"c)(2) & " -  Failed to update agent change" & "</Msg></Root>")
                    Case "GETBPDDETDVS"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - Failed to get AIMS and DVASS data</Msg></Root>")
                    Case "AIMS"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - Failed to update AIMS</Msg></Root>")
                    Case "RENTAL"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - " & ex.Message.Split("/"c)(2) & " - Failed to update rental" & "</Msg></Root>")
                    Case "SSX"
                        Return ("<Root><Error>" & ex.Message.Split("/"c)(1) & "</Error><Msg>" & ex.Message.Split("/"c)(1) & " - Failed to update Smart Stream Postings" & "</Msg></Root>")
                    Case Else
                        Return ("<Root><Error>" & ex.Message & "</Error><Msg>" & ex.Message & " - Error occured while updating Booked product details</Msg></Root>")
                End Select


                'manageBookedProductList = String.Concat("<Root><Error>", ex.Message, "</Error><Msg>" & ex.Message, "</Msg></Root>")
            End Try
            ' End of the BIG Try
        End Using
        ' End of the TRAN


        'Return manageBookedProductList

    End Function

    ''rev:mia 19jan2015-addition of supervisor override password and slot validation by adding SlotId, SlotDescription and RentalId
    Public Shared Function auroraUpdateBookedProduct( _
                       ByVal sBpdId As String, ByVal booIsCusCharge As String, _
                       ByVal sSapId As String, ByVal sAgnId As String, _
                       ByVal dCkoWhen As String, ByVal dCkiWhen As String, _
                       ByVal sCkoLoc As String, ByVal sCkiLoc As String, _
                       ByVal lngFrom As Integer, ByVal lngTo As Integer, _
                       ByVal dblRate As String, ByVal lngQty As Integer, _
                       ByVal sPrrIdList As String, ByVal sPrrQtyList As String, _
                       ByVal sPrrRateList As String, ByVal sPtdId As String, _
                       ByVal sCodTypeId As String, ByVal sUnitNo As String, _
                       ByVal sSerialCode As String, ByVal dChgFrom As String, _
                       ByVal dChgTo As String, ByVal lngOddoMeterOut As Integer, _
                       ByVal lngOddoMeterIn As Integer, ByVal booOverride As String, _
                       ByVal sFoc As String, ByVal sBpdStatus As String, _
                       ByVal sCtyCode As String, ByVal sExternalRef As String, _
                       ByRef sRntHisId As String, ByVal sUsrId As String, _
                       ByVal sPrgmName As String, ByRef sErrRet As String, _
                       ByRef sRetMsg As String, ByRef sRetBpdId As String, _
                       Optional ByVal bVehicleOverride As String = Nothing, _
                       Optional ByVal sOldRntStatus As String = Nothing, _
                       Optional ByVal strRegoNumber As String = Nothing, _
                       Optional ByVal sEvent As String = Nothing, _
                       Optional ByVal sBookingRef As String = Nothing, _
                       Optional ByVal sOldCkoWhen As String = Nothing, _
                       Optional ByVal sOldCkiWhen As String = Nothing, _
                       Optional ByVal sRntStatus As String = Nothing, _
                       Optional ByVal sOldCkoLoc As String = Nothing, _
                       Optional ByVal sOldCkiLoc As String = Nothing, _
                       Optional ByVal sExcludeBpdList As String = Nothing, _
                       Optional ByVal sExclLstIn As String = Nothing, _
                       Optional ByVal IsFirstFlag As Boolean = True, _
                       Optional ByVal sCalledFrom As String = "", _
                       Optional ByVal SlotId As String = "", _
                       Optional ByVal SlotDescription As String = "", _
                       Optional ByVal RentalId As String = "") As String




        Dim sRetError As String = String.Empty
        Dim sRetBlrIdList As String = String.Empty
        Dim sBookingRequestIdNew As String = String.Empty
        Dim retValue As String = String.Empty


        Dim sbLog As New StringBuilder
        sbLog.AppendFormat("ManageBookedProduct_Mod - {0}{1}", "ManageBookedProduct_Mod".ToUpper, vbCrLf)
        sbLog.AppendFormat("sBpdStatus - {0}{1}", sBpdStatus, vbCrLf)
        sbLog.AppendFormat("sOldRntStatus - {0}{1}", sOldRntStatus, vbCrLf)
        sbLog.AppendFormat("sRntStatus - {0}{1}", sRntStatus, vbCrLf)


        retValue = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Mod( _
                                            sBpdId, booIsCusCharge, _
                                            sSapId, sAgnId, _
                                            dCkoWhen, dCkiWhen, _
                                            sCkoLoc, sCkiLoc, _
                                            lngFrom, lngTo, _
                                            dblRate, lngQty, _
                                            sPrrIdList, sPrrQtyList, _
                                            sPrrRateList, sPtdId, _
                                            sCodTypeId, sUnitNo, _
                                            sSerialCode, String.Empty, _
                                            dChgFrom, dChgTo, _
                                            lngOddoMeterOut, lngOddoMeterIn, _
                                            booOverride, sFoc, _
                                            sBpdStatus, sCtyCode, _
                                            sRetBlrIdList, String.Empty, _
                                            sBookingRequestIdNew, sRntHisId, _
                                            sUsrId, sPrgmName, _
                                            sRetError, sRetMsg, _
                                            sRetBpdId, bVehicleOverride, _
                                            sOldRntStatus, strRegoNumber, _
                                            sEvent, sOldCkoWhen, _
                                            sOldCkiWhen, sRntStatus, _
                                            sOldCkoLoc, sOldCkiLoc, _
                                            sExcludeBpdList, _
                                            sExclLstIn, _
                                            IsFirstFlag, _
                                            sCalledFrom, _
                                            SlotId, _
                                            SlotDescription, _
                                            RentalId)

        Logging.LogDebug("auroraUpdateBookedProduct", sbLog.ToString())

        If sRetError.Trim.Length <> 0 Then
            sErrRet = sRetError
        End If

        Return retValue

    End Function

    Public Shared Function GetMaintainBookedProduct(ByVal bpdId As String) As XmlDocument

        Return DataRepository.GetMaintainBookedProduct(bpdId)

    End Function

    ''rev:mia 19jan2015-addition of supervisor override password and slot validation by adding SlotId, SlotDescription and RentalId
    Public Shared Sub ModifyBookedProduct(ByVal xmlDoc As XmlDocument, _
                                          ByVal override As Boolean, _
                                          ByVal ret As String, _
                                          ByVal dvassForce As Boolean, _
                                          ByVal userCode As String, _
                                          ByRef retSapId As String, _
                                          ByRef messages As String, _
                                          ByRef errors As String, _
                                          ByVal programName As String, _
                                          ByRef strRetBpdId As String, _
                                          Optional ByVal IsFirstFlag As Boolean = True, _
                                          Optional ByVal Override4ManageBookedProductChanges As Boolean = False, _
                                          Optional ByVal sCalledFrom As String = "", _
                                          Optional SlotId As String = "", _
                                          Optional SlotDescription As String = "", _
                                          Optional SlotRentalId As String = "")

        Using transaction As New Aurora.Common.Data.DatabaseTransaction()

            Dim rentalId As String
            rentalId = xmlDoc.SelectSingleNode("//BookedProduct/RntId").InnerText

            '=========>> Call manageMaintainBookedProductPartA Stored Procedure   <<===========
            DataRepository.ManageMaintainBookedProductPartA(xmlDoc, userCode, override, "", retSapId, messages, errors)
            If (Not String.IsNullOrEmpty(errors)) And errors.ToUpper <> "SUCCESS" Then
                'RollbackTransaction
                transaction.RollbackTransaction()
                Return
            End If

            '----------->>> To Delete all Charge Record from Trmporary Table <<<-------------------
            If Not DataRepository.DeleteTProcessedBpds(rentalId) Then
                'RollbackTransaction
                transaction.RollbackTransaction()
                Return
            End If

            '-------------------   updateBookedProduct   ----------------------------------
            'updateBookedProduct(xmlDoc, retSapId, errors, messages, userCode, programName, dvassForce)

            Dim bpdId As String = xmlDoc.SelectSingleNode("//BookedProduct/BpdId").InnerText
            Dim isCusCharge As String = xmlDoc.SelectSingleNode("//BookedProduct/Cus").InnerText
            Dim sapId As String = retSapId
            Dim agentId As String = xmlDoc.SelectSingleNode("//BookedProduct/AgnId").InnerText

            Dim dCkoWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/BpdCkoDt").InnerText
            Dim dCkiWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/BpdCkiDt").InnerText
            Dim ckoLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/CkoLoc").InnerText
            Dim ckiLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/CkiLoc").InnerText
            Dim lngFrom As String = xmlDoc.SelectSingleNode("//BookedProduct/Fr").InnerText
            Dim lngTo As String = xmlDoc.SelectSingleNode("//BookedProduct/To").InnerText

            Dim rate As String = "0"
            Dim quantity As Integer = xmlDoc.SelectSingleNode("//BookedProduct/BpdQty").InnerText
            Dim strPrrIdList As String = ""
            Dim strPrrQtyList As String = ""
            Dim strPrrRateList As String = ""
            Dim strPtdId As String = ""
            Dim strCodTypeId As String = xmlDoc.SelectSingleNode("//BookedProduct/TypeId").InnerText

            ''rev:mia oct.14
            Dim strUnitNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/UnitNumber").InnerText
            ''Dim strUnitNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/UnitNum").InnerText

            Dim strSerialCode As String = xmlDoc.SelectSingleNode("//BookedProduct/SrCd").InnerText
            'Dim strBpdIdParent As String = xmlDoc.SelectSingleNode("//BookedProduct/UnitNum").InnerText
            Dim dteChargeFrom As String = xmlDoc.SelectSingleNode("//BookedProduct/ChgFrDt").InnerText
            Dim dteChargeTo As String = xmlDoc.SelectSingleNode("//BookedProduct/ChgToDt").InnerText
            Dim lngOdometerFrom As Integer = xmlDoc.SelectSingleNode("//BookedProduct/OddOut").InnerText
            Dim lngOdometerTo As Integer = xmlDoc.SelectSingleNode("//BookedProduct/OddIn").InnerText
            ''Dim booOverride As String = ""

            Dim sFoc As String = xmlDoc.SelectSingleNode("//BookedProduct/Foc").InnerText
            Dim strStatus As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
            Dim sCtyCode As String = xmlDoc.SelectSingleNode("//BookedProduct/CtyCode").InnerText

            'Dim strReturnBlrIdList As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
            'Dim booCallManageBlockingMessage As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
            'Dim strBookingRequestIdNew As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText

            Dim sExternalRef As String = xmlDoc.SelectSingleNode("//BookedProduct/ExternalRef").InnerText
            Dim strRentalHistoryID As String = xmlDoc.SelectSingleNode("//BookedProduct/RnhId").InnerText
            'Dim strProgramName As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
            'Dim strReturnError As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
            'Dim strReturnMessage As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
            '        Dim strRetBpdId As String = ""

            '    Optional ByVal bVehicleOverride As String = Nothing, _
            Dim sOldRntStatus As String = xmlDoc.SelectSingleNode("//BookedProduct/OldRntStatus").InnerText
            Dim strRegoNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/RegoNumber").InnerText
            Dim sEvent As String = "ManageBookedProduct"
            Dim sBookingRef As String = String.Empty
            Dim sOldCkoWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkoWhen").InnerText
            Dim sOldCkiWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkiWhen").InnerText
            Dim sRntStatus As String = xmlDoc.SelectSingleNode("//BookedProduct/Status").InnerText
            Dim sOldCkoLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkoLocCode").InnerText
            Dim sOldCkiLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkiLocCode").InnerText
            Dim sExcludeBpdList As String = ""
            Dim sExclLstIn As String = String.Empty

            Dim origCusCharge As String = xmlDoc.SelectSingleNode("//BookedProduct/OrigCusCharge").InnerText
            Dim sTrToFin As String = xmlDoc.SelectSingleNode("//BookedProduct/TrToFin").InnerText


            ''---------rev:rajesh dec15
            Dim nRntIntNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/RntIntNo").InnerText
            Dim nBooIntNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/BooIntNo").InnerText
            Dim bIsValidIntNo As Boolean
            bIsValidIntNo = Aurora.Reservations.Services.BookedProduct.CheckRentalIntegrity(rentalId, "", "", nRntIntNumber, nBooIntNumber)
            If bIsValidIntNo = False Then
                errors = "ERROR - This Rental has been modified by another user. Please refresh the screen."
                transaction.RollbackTransaction()
                Return
            End If
            ''---------


            messages = auroraUpdateBookedProduct(bpdId, isCusCharge, sapId, _
                     agentId, dCkoWhen, dCkiWhen, ckoLoc, ckiLoc, lngFrom, lngTo, rate, quantity, strPrrIdList, _
                     strPrrQtyList, strPrrRateList, strPtdId, strCodTypeId, strUnitNumber, strSerialCode, _
                     dteChargeFrom, dteChargeTo, lngOdometerFrom, lngOdometerTo, Override4ManageBookedProductChanges, sFoc, strStatus, _
                     sCtyCode, sExternalRef, strRentalHistoryID, userCode, programName, errors, messages, _
                     strRetBpdId, dvassForce, sOldRntStatus, strRegoNumber, sEvent, sBookingRef, sOldCkoWhen, _
                     sOldCkiWhen, sRntStatus, sOldCkoLoc, sOldCkiLoc, sExcludeBpdList, sExclLstIn, IsFirstFlag, sCalledFrom, SlotId, SlotDescription, SlotRentalId)


            If (Not String.IsNullOrEmpty(errors)) And errors.ToUpper <> "SUCCESS" Then
                'RollbackTransaction
                transaction.RollbackTransaction()
                Return
            End If

            ''sReturnedBPDIDForSS
            If Not strRetBpdId.Trim().Equals(String.Empty) Then
                Dim strError As String = String.Empty
                strError = Aurora.Common.Data.ExecuteScalarSP("RES_ssTransToFinance", String.Empty, String.Empty, String.Empty, strRetBpdId)
                If Not String.IsNullOrEmpty(strError) AndAlso Not strError.ToUpper().Trim().Equals("SUCCESS") Then
                    transaction.RollbackTransaction()
                    Logging.LogInformation("SSX", "Failed to update SS TRANS TO FINANCE")
                    Return
                End If
            End If
            '' done ss trans

            '------------------------ RES_ValiateBookedProductForSurCharge  ----------------------
            Dim isVehicle As Boolean = Utility.ParseBoolean(xmlDoc.SelectSingleNode("//BookedProduct/IsVehicle").InnerText, False)

            If strRetBpdId.Trim.Length <> 0 And isVehicle Then

                Data.DataRepository.ValiateBookedProductForSurCharge(strRetBpdId, ckoLoc, ckiLoc, sOldCkoLoc, sOldCkiLoc, _
                    dCkoWhen, dCkiWhen, sOldCkoWhen, sOldCkiWhen, override, userCode, errors)

                If (Not String.IsNullOrEmpty(errors)) And errors.ToUpper <> "SUCCESS" Then
                    'RollbackTransaction
                    transaction.RollbackTransaction()
                    Return
                End If
            End If


            If strRetBpdId.Trim.Length = 0 Then
                strRetBpdId = bpdId
            End If

            '------------------------ RES_ManageBookedProductAgentChange  ----------------------

            If (isCusCharge = 0 Or origCusCharge = 0) And (strRetBpdId <> bpdId) And (sTrToFin.Trim.Length <> 0) Then

                Data.DataRepository.ManageBookedProductAgentChange(rentalId, agentId, userCode, programName, _
                Nothing, strRentalHistoryID, bpdId, isCusCharge, errors, messages, strRetBpdId)

                'RES_ManageBookedProductAgentChange
                If (Not String.IsNullOrEmpty(errors)) And errors.ToUpper <> "SUCCESS" Then
                    'RollbackTransaction
                    transaction.RollbackTransaction()
                    Return
                End If

            End If

            '------------------------ RES_updateRntPrdReqVehId  ----------------------
            errors = Data.DataRepository.UpdateRntPrdReqVehId(strRetBpdId, "")
            errors = errors.Replace("<data>", "")
            errors = errors.Replace("</data>", "")
            If (Not String.IsNullOrEmpty(errors)) And errors.ToUpper <> "SUCCESS" Then
                'RollbackTransaction
                transaction.RollbackTransaction()
                Return
            End If

            ''RES_UpdateRentalIntegrity
            Data.DataRepository.UpdateRentalIntegrityNumber(rentalId)

            'CommitTransaction
            transaction.CommitTransaction()

        End Using

    End Sub



    'Private Shared Sub updateBookedProduct(ByRef xmlDoc As XmlDocument, ByVal retSapId As String, ByRef errors As String, _
    '    ByRef message As String, ByVal userCode As String, ByVal programName As String, ByVal dvassForce As Boolean)

    '    Dim bpdId As String = xmlDoc.SelectSingleNode("//BookedProduct/BpdId").InnerText
    '    Dim isCusCharge As String = xmlDoc.SelectSingleNode("//BookedProduct/Cus").InnerText
    '    Dim sapId As String = retSapId
    '    Dim agentId As String = xmlDoc.SelectSingleNode("//BookedProduct/AgnId").InnerText

    '    Dim dCkoWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/BpdCkoDt").InnerText
    '    Dim dCkiWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/BpdCkiDt").InnerText
    '    Dim ckoLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/CkoLoc").InnerText
    '    Dim ckiLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/CkiLoc").InnerText
    '    Dim lngFrom As String = xmlDoc.SelectSingleNode("//BookedProduct/Fr").InnerText
    '    Dim lngTo As String = xmlDoc.SelectSingleNode("//BookedProduct/To").InnerText

    '    Dim rate As String = "0"
    '    Dim quantity As Integer = xmlDoc.SelectSingleNode("//BookedProduct/BpdQty").InnerText
    '    Dim strPrrIdList As String = ""
    '    Dim strPrrQtyList As String = ""
    '    Dim strPrrRateList As String = ""
    '    Dim strPtdId As String = ""
    '    Dim strCodTypeId As String = xmlDoc.SelectSingleNode("//BookedProduct/TypeId").InnerText
    '    Dim strUnitNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/UnitNum").InnerText
    '    Dim strSerialCode As String = xmlDoc.SelectSingleNode("//BookedProduct/SrCd").InnerText
    '    'Dim strBpdIdParent As String = xmlDoc.SelectSingleNode("//BookedProduct/UnitNum").InnerText
    '    Dim dteChargeFrom As String = xmlDoc.SelectSingleNode("//BookedProduct/ChgFrDt").InnerText
    '    Dim dteChargeTo As String = xmlDoc.SelectSingleNode("//BookedProduct/ChgToDt").InnerText
    '    Dim lngOdometerFrom As Integer = xmlDoc.SelectSingleNode("//BookedProduct/OddOut").InnerText
    '    Dim lngOdometerTo As Integer = xmlDoc.SelectSingleNode("//BookedProduct/OddIn").InnerText
    '    Dim booOverride As String = ""

    '    Dim sFoc As String = xmlDoc.SelectSingleNode("//BookedProduct/Foc").InnerText
    '    Dim strStatus As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
    '    Dim sCtyCode As String = xmlDoc.SelectSingleNode("//BookedProduct/CtyCode").InnerText

    '    'Dim strReturnBlrIdList As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
    '    'Dim booCallManageBlockingMessage As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
    '    'Dim strBookingRequestIdNew As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText

    '    Dim sExternalRef As String = xmlDoc.SelectSingleNode("//BookedProduct/ExternalRef").InnerText
    '    Dim strRentalHistoryID As String = xmlDoc.SelectSingleNode("//BookedProduct/RnhId").InnerText
    '    'Dim strProgramName As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
    '    'Dim strReturnError As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
    '    'Dim strReturnMessage As String = xmlDoc.SelectSingleNode("//BookedProduct/St").InnerText
    '    Dim strRetBpdId As String = ""

    '    '    Optional ByVal bVehicleOverride As String = Nothing, _
    '    Dim sOldRntStatus As String = xmlDoc.SelectSingleNode("//BookedProduct/OldRntStatus").InnerText
    '    Dim strRegoNumber As String = xmlDoc.SelectSingleNode("//BookedProduct/RegoNumber").InnerText
    '    Dim sEvent As String = "ManageBookedProduct"
    '    Dim sBookingRef As String = String.Empty
    '    Dim sOldCkoWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkoWhen").InnerText
    '    Dim sOldCkiWhen As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkiWhen").InnerText
    '    Dim sRntStatus As String = xmlDoc.SelectSingleNode("//BookedProduct/Status").InnerText
    '    Dim sOldCkoLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkoLocCode").InnerText
    '    Dim sOldCkiLoc As String = xmlDoc.SelectSingleNode("//BookedProduct/OldCkiLocCode").InnerText
    '    Dim sExcludeBpdList As String = ""
    '    Dim sExclLstIn As String = String.Empty

    '    errors = auroraUpdateBookedProduct(bpdId, isCusCharge, sapId, _
    '             agentId, dCkoWhen, dCkoWhen, ckoLoc, ckiLoc, lngFrom, lngTo, rate, quantity, strPrrIdList, _
    '             strPrrQtyList, strPrrRateList, strPtdId, strCodTypeId, strUnitNumber, strSerialCode, _
    '             dteChargeFrom, dteChargeTo, lngOdometerFrom, lngOdometerTo, booOverride, sFoc, strStatus, _
    '             sCtyCode, sExternalRef, strRentalHistoryID, userCode, programName, errors, message, _
    '             strRetBpdId, dvassForce, sOldRntStatus, strRegoNumber, sEvent, sBookingRef, sOldCkoWhen, _
    '             sOldCkiWhen, sRntStatus, sOldCkoLoc, sOldCkiLoc, sExcludeBpdList, sExclLstIn)


    '    '    errors = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Mod(bpdId, isCusCharge, sapId, _
    '    '             agentId, dCkoWhen, dCkoWhen, ckoLoc, ckiLoc, lngFrom, lngTo, rate, quantity, strPrrIdList, _
    '    '             strPrrQtyList, strPrrRateList, strPtdId, strCodTypeId, strUnitNumber, strSerialCode, "", _
    '    '             dteChargeFrom, dteChargeTo, lngOdometerFrom, lngOdometerTo, booOverride, sFoc, strStatus, sCtyCode, "", "", "", _
    '    ' strRentalHistoryID, userCode, programName, errors, message, strRetBpdId, dvassForce, sOldRntStatus, strRegoNumber, sEvent, _
    '    'sOldCkoWhen, sOldCkiWhen, sRntStatus, sOldCkoLoc, sOldCkiLoc, sExcludeBpdList, sExclLstIn)

    'End Sub


End Class
