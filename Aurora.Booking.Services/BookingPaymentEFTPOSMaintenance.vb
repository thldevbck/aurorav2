''rev:mia november 22 2010
''        - addition of PdtEFTPOSCardMethodDontMatch parameter in SAVEPAYMENT AND REFUNDPAYMENT and new property was added to the bookingpaymentItemsCollection
''        - changes on paym_managePaymentDetail Table
''        - changes on paym_GetPayment
''BAU RELEASE : MARCH 06 2012

'rev:mia Sept 17 2012 - changes related to credit card with a chip embed on it.
'		              - Since DPS cant provide embedded card, fixes will be 
'                     - carried-out by removing the cardname comparison in every 
'                     - transactions.
'                     - IsCreditCardInBooking/IsCreditCardIsFoundInBondPayment/SaveEFTPOSPayment

Imports Aurora.Common.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.IO
Imports System.Web
Imports System.Runtime.InteropServices



Public Class BookingPaymentEFTPOSMaintenance

#Region "Property"
    Private _SubMethod As String
    Private Property SubMethod() As String
        Get
            Return _SubMethod
        End Get
        Set(ByVal value As String)
            _SubMethod = value
        End Set
    End Property

    Private _SurchargeValue As String
    Private Property SurchargeValue() As String
        Get
            Return _SurchargeValue
        End Get
        Set(ByVal value As String)
            _SurchargeValue = value
        End Set
    End Property


    Private _totalAmount As String
    Private Property TotalAmount() As String
        Get
            Return _totalAmount
        End Get
        Set(ByVal value As String)
            _totalAmount = value
        End Set
    End Property

    Private _currentAmt As String
    Private Property CurrentAmount() As String
        Get
            Return _currentAmt
        End Get
        Set(ByVal value As String)
            _currentAmt = value
        End Set
    End Property

    Private _usercode As String
    Private Property UsersCode() As String
        Get
            Return _usercode
        End Get
        Set(ByVal value As String)
            _usercode = value
        End Set
    End Property

    Private _BooID As String
    Private Property BooID() As String
        Get
            Return _BooID
        End Get
        Set(ByVal value As String)
            _BooID = value
        End Set
    End Property

    Private _RntId As String
    Private Property RntId() As String
        Get
            Return _RntId
        End Get
        Set(ByVal value As String)
            _RntId = value
        End Set
    End Property

#End Region

#Region "Public Function"

    Public Function IsAllowedToRefund(ByVal RptRntId As String, _
                                      ByVal cardNumber As String, _
                                      ByVal RptType As String, _
                                      ByVal amountToPay As Decimal, _
                                      ByRef message As String, _
                                      Optional ByVal ischanged As Boolean = False, _
                                      Optional ByVal mode As String = "", _
                                      Optional ByVal baseamount As String = "9999999999", _
                                      Optional ByVal pdtMptID As String = "", _
                                      Optional ByVal isAmex As Boolean = False) As Boolean

        Dim params(9) As Aurora.Common.Data.Parameter
        Try


            params(0) = New Aurora.Common.Data.Parameter("RptRntId", DbType.String, RptRntId, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("cardNumber", DbType.String, cardNumber, Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("RptType", DbType.String, RptType, Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("amountToPay", DbType.String, amountToPay, Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("isAllowed", DbType.String, 1, Parameter.ParameterType.AddOutParameter)
            params(5) = New Aurora.Common.Data.Parameter("message", DbType.String, 1000, Parameter.ParameterType.AddOutParameter)
            params(6) = New Aurora.Common.Data.Parameter("ischanged", DbType.Boolean, ischanged, Parameter.ParameterType.AddInParameter)
            params(7) = New Aurora.Common.Data.Parameter("mode", DbType.String, mode, Parameter.ParameterType.AddInParameter)
            params(8) = New Aurora.Common.Data.Parameter("baseamount", DbType.String, baseamount, Parameter.ParameterType.AddInParameter)
            params(9) = New Aurora.Common.Data.Parameter("PdtPmtID", DbType.String, pdtMptID, Parameter.ParameterType.AddInParameter)
            ''params(10) = New Aurora.Common.Data.Parameter("isAmex", DbType.Boolean, isAmex, Parameter.ParameterType.AddInParameter)
            Aurora.Common.Data.ExecuteOutputSP("EFTPOS_GetTotalAmountGroupByCardAndType", params)
            ''Aurora.Common.Data.ExecuteOutputSP("EFTPOS_GetTotalAmountGroupByCardAndType_TEST_ON_002", params)
            message = params(5).Value

            If (Not String.IsNullOrEmpty(params(4).Value)) Then
                Return Convert.ToBoolean(CInt(params(4).Value.ToString))
            Else
                Return True
            End If

        Catch ex As Exception
            message = "ERROR: " + ex.Message + vbCrLf + ex.StackTrace
            Logging.LogError("PAYMENT SCREEN: EFTPOS: IsAllowedToRefund", ex.Message & vbCrLf & ex.StackTrace)
            Return False
        End Try
    End Function

    Public Function MessageInformationFormat(ByVal strmessage() As String) As String
        Dim AX As String = MessageInformationFormat("PROPERTIES AND VALUES", strmessage)
        Logging.LogDebug("ACTIVEX CONTROL ", AX)
        Return "OK"
    End Function

    Private Function MessageInformationFormat(ByVal methodName As String, ByVal ParamArray strmessage As String()) As String
        Dim sb As New StringBuilder
        sb.AppendFormat("{0}{1}{2}", vbCrLf, "----------------STARTING EFTPOS ACTIVITIES-----------", vbCrLf)
        sb.Append(methodName.ToUpper)
        sb.AppendFormat("{0}{1}{2}", vbCrLf, "-----------------------------------------------------", vbCrLf)

        Dim ctr As Integer = 0
        For Each strvalue As String In strmessage
            If Not String.IsNullOrEmpty(strvalue) Then
                strvalue = strvalue.Trim
                If ctr = 0 Then
                    sb.AppendFormat("{0}:", strvalue.ToUpper)
                    ctr = ctr + 1
                Else
                    sb.AppendFormat("{0}{1}", strvalue, vbCrLf)
                    ctr = 0
                End If
            End If
        Next

        sb.AppendFormat("{0}{1}{2}", vbCrLf, "------------------------------------------", vbCrLf)
        Return sb.ToString
    End Function
    Public Function SaveEFTPOSPayment(ByVal xml As String) As String

        Try

            xml = System.Web.HttpUtility.UrlDecode(xml)
            Dim splitXML() As String = xml.Split("|")

            ''payment block
            ''check #1
            If Not IsValidXML(splitXML(0)) Then
                Logging.LogError("PAYMENT SCREEN:", MessageInformationFormat("SAVEEFTPOSPAYMENT", "check #1", splitXML(0)))
                Return "ERROR: This is not well-formed xml"
            End If

            ''paymententries block
            ''check #2
            If Not IsValidXML("<Root>" + splitXML(1) + "</Root>") Then
                Logging.LogError("PAYMENT SCREEN:", MessageInformationFormat("SAVEEFTPOSPAYMENT", "check #1", splitXML(1)))
                Return "ERROR: This is not well-formed xml"
            End If

            ''getting cardvalue
            Dim cardvalue As String = splitXML(2).Replace("<MethodValue>", "").Replace("</MethodValue>", "")
            Dim methodvalue As String = ""
            ''rev:mia nov 15 2010 - issue #113
            ''a debit plus is selected so we need to push the surcharge value
            Dim isForceEftpos As Boolean = False
            If cardvalue.Split("@")(0).Contains("^EFTPOS") Then
                isForceEftpos = True
                methodvalue = cardvalue.Split("@")(1)
                cardvalue = cardvalue.Split("@")(0).Split("^")(0)
                Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("CardValue is ", cardvalue + ". Note: Credit Card selected and swiped DEBIT PLUS CARD "))
            Else
                ''get the methodvalue 
                methodvalue = cardvalue.Split("@")(1).Replace("<", "").Replace(">", "")
                cardvalue = cardvalue.Split("@")(0)
            End If

            ''i hardcode the debit card id
            If methodvalue = "0D26F373-51E2-45B2-B543-26520BEE9005" Then
                Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("methodValue ID is ", methodvalue + ". Note: DEBIT CARD selected  but swiped non eftpos card "))
                SubMethod = GetCardName("EFT-POS")
            Else
                SubMethod = GetCardName(cardvalue)
            End If


            ''get surcharge
            Dim surcharge As String = splitXML(3) + "@" + SubMethod
            SurchargeValue = GetSurcharge(surcharge)
            Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("GetSurcharge", "GetCardName", SubMethod, "GetSurcharge", SurchargeValue))

            ''get total amoount as string
            TotalAmount = splitXML(4).Split("@")(0)
            CurrentAmount = splitXML(4).Split("@")(1)



            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(splitXML(0))
            xmldoc.SelectSingleNode("Root/Data/Payment/AmountToPay").InnerText = CurrentAmount
            xmldoc.SelectSingleNode("Root/Data/Payment/PaymentRunningTotal").InnerText = CurrentAmount
            xmldoc.SelectSingleNode("Root/Data/Payment/Balance").InnerText = 0
            BooID = xmldoc.SelectSingleNode("Root/Data/Payment/BookingId").InnerText
            RntId = xmldoc.SelectSingleNode("Root/Data/Payment/RentalId").InnerText
            Dim retvalue As String = xmldoc.SelectSingleNode("Root/Data/Payment").OuterXml

            retvalue = retvalue + splitXML(1)

            ''1.retreive xml and converto xmldocument
            ''checkk #4
            retvalue = CreatePaymentXML(retvalue)
            Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("SUBMETHOD", "RntId:", RntId, " check #4: ", retvalue))

            Dim xmlTest As New XmlDocument
            xmlTest.LoadXml(retvalue)
            Dim method As String = xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText

            'rev:mia Sept 17 2012 - changes related to credit card with a chip embed on it.
            '		              - Since DPS cant provide embedded card, fixes will be 
            '                     - carried-out by removing the cardname comparison in every 
            '                     - transactions.
            '                     - IsCreditCardInBooking/IsCreditCardIsFoundInBondPayment/SaveEFTPOSPayment
            Dim crossCountry As String = "False"
            Try
                If (Not xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/CrossCountryPayment") Is Nothing) Then
                    crossCountry = xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/CrossCountryPayment").InnerText
                End If
            Catch ex As Exception

            End Try

            ''if its not EFTPOS then we need add surcharge
            ''this is a tweak...
            If (CDec(TotalAmount) <> (CDec(CurrentAmount) + CDec(SurchargeValue)) And (method.IndexOf("Credit Card") <> -1 Or method.IndexOf("Debit") <> -1) And (TypeCode = "R" Or TypeCode = "D" Or TypeCode = "B")) Then
                TotalAmount = CDec(CurrentAmount) + CDec(SurchargeValue)
                Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("TotalAmount", "RntId:", RntId, " TotalAmount including Surcharge: ", TotalAmount))
                xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Amount").InnerText = TotalAmount
                xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = TotalAmount
                xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount").InnerText = TotalAmount
                xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtLocalAmount").InnerText = TotalAmount
                retvalue = xmlTest.OuterXml
            Else
                If (method.IndexOf("Debit Card") <> -1 And TypeCode = "R") Then
                    ''do nothing
                End If

                If (crossCountry = "AUNZ") Then
                    TotalAmount = getLocalCurrency("AUD", "NZD", TotalAmount)
                    xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Amount").InnerText = TotalAmount
                    Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("TotalAmount", "RntId:", RntId, " getLocalCurrency(AUD,NZD): ", TotalAmount))
                    retvalue = xmlTest.OuterXml
                End If

                If (crossCountry = "NZAU") Then
                    TotalAmount = getLocalCurrency("NZD", "AUD", TotalAmount)
                    xmlTest.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Amount").InnerText = TotalAmount
                    Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("TotalAmount", "RntId:", RntId, " getLocalCurrency(NZD,AUD): ", TotalAmount))
                    retvalue = xmlTest.OuterXml
                End If
            End If

            ''add encyption -- no more encryption
            ''Dim encryptedCardWithData As String = ""
            ''Dim blnSuccess As Boolean = EncryptCreditCardDataSuccess(retvalue, encryptedCardWithData)
            ''retvalue = encryptedCardWithData

            If (retvalue.Contains("ERROR") = False) Then

                ''Return "ERROR: Failed to update EFTPOS payment"
                Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("SavePayment", "RntId:", RntId, " retvalue", retvalue))
                retvalue = SavePayment(retvalue, UsersCode, "PaymentMGT.aspx?EFTPOS")
                Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("SavePayment", "RntId:", RntId, " RESULT", retvalue))

                If (IsValidXML(retvalue) = False) Then
                    Logging.LogError("SaveEFTPOSPayment", MessageInformationFormat("SavePayment", "RntId:", RntId, " IsValidXML(retvalue)", retvalue))
                    Return "ERROR: Failed to update EFTPOS payment"
                End If

                Dim xmlresult As New XmlDocument
                xmlresult.LoadXml(retvalue)
                If (xmlresult.SelectNodes("Root/Error/ErrStatus").Item(0).InnerText = "True") Then
                    Logging.LogError("SaveEFTPOSPayment", MessageInformationFormat("SavePayment", "RntId:", RntId, " Root/Error/ErrStatus", "true"))
                    Return String.Concat("ERROR:", xmlresult.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)
                Else

                    ''adding of overridereasons when its negative
                    Dim paymentid As String = xmlresult.SelectSingleNode("Root/Data/PaymentEntryRoot/PmEntryPaymentId").InnerText
                    Try
                        If splitXML.GetUpperBound(0) > 4 Then
                            If Not String.IsNullOrEmpty(splitXML(6)) And Not String.IsNullOrEmpty(splitXML(5)) And Not String.IsNullOrEmpty(splitXML(7)) Then
                                Dim reason As String = splitXML(6)
                                Dim usercode As String = splitXML(5)
                                Dim otherreason As String = ""
                                If (Not splitXML(7) Is Nothing) Then
                                    otherreason = splitXML(7).TrimEnd
                                End If
                                Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
                                objEFTPOS.InsertPaymentOverrideReasons(paymentid, reason, usercode, otherreason)
                                objEFTPOS.AddEFTPOSHistory(RntId, usercode, "EFTPOS OVERRIDE")
                            End If
                        End If
                    Catch ex As Exception
                        Logging.LogError("SaveEFTPOSPayment", MessageInformationFormat("exception", "RntId:", RntId, " source", ex.Source, " stack", ex.StackTrace, "Reason", splitXML(6), "otherreason", splitXML(7)))
                    End Try


                    retvalue = updatePaymentHistory(BooID, RntId, "PAYMENT")
                    Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("updatePaymentHistory", "RntId:", RntId, " retvalue", retvalue))

                    If Not retvalue.Contains("ERROR") Then
                        Dim pmtid As String = ""
                        If Not xmlresult.SelectSingleNode("Root/Data/PaymentEntryRoot/PaymentEntryItems/PmtItem/PmtPtmId") Is Nothing Then
                            pmtid = "|" & xmlresult.SelectSingleNode("Root/Data/PaymentEntryRoot/PaymentEntryItems/PmtItem/PmtPtmId").InnerText
                        End If
                        retvalue = "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10&hdBookingId=" + BooID + "&hdRentalId=" + RntId + pmtid
                        Logging.LogInformation("SaveEFTPOSPayment", MessageInformationFormat("updatePaymentHistory", "RntId:", RntId, " pmtid", pmtid))
                    End If
                End If
            End If
            Return retvalue
        Catch ex As Exception
            Logging.LogError("SaveEFTPOSPayment", MessageInformationFormat("exception", "RntId:", RntId, " source", ex.Source, " stack", ex.StackTrace, "xml", xml))
            Return "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10&hdBookingId=" + BooID + "&hdRentalId=" + RntId
        End Try
    End Function

    Public Function GetAllCardTypes(ByVal submethod As String) As String
        Dim xmlString As String = ""
        Dim sb As New StringBuilder
        Dim xml As New XmlDocument

        Try
            xmlString = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getMethodType", submethod).OuterXml
            xml.LoadXml(xmlString)
            For Each node As XmlElement In xml.SelectNodes("data/PaymentMethod")
                sb.AppendFormat("<option value='{0}'>{1}</option>", node.SelectSingleNode("ID").InnerXml, node.SelectSingleNode("NAME").InnerXml)
            Next
        Catch ex As Exception
            sb.Append("ERROR:Cannot retreive credit card types")

        End Try
        Return sb.ToString
    End Function

    Public Function Surcharge(ByVal qs As String) As Decimal
        Return GetSurcharge(qs)
    End Function

    Public Function IsCreditCard(ByRef asCardType As String, ByRef anCardNumber As String) As Boolean

        ' Performs a Mod 10 check To make sure the credit card number
        ' appears valid
        ' Developers may use the following numbers as dummy data:
        ' Visa:                               430-00000-00000
        ' American Express:            372-00000-00000
        ' Mastercard:                  521-00000-00000
        ' Discover:                           620-00000-00000

        Dim lsNumber           ' Credit card number stripped of all spaces, dashes, etc.
        Dim lsChar                     ' an individual character
        Dim lnTotal                    ' Sum of all calculations
        Dim lnDigit                    ' A digit found within a credit card number
        Dim lnPosition         ' identifies a character position In a String
        Dim lnSum                      ' Sum of calculations For a specific Set
        Dim lnMultiplier As Integer
        ' Default result is False
        IsCreditCard = False

        ' ====
        ' Strip all characters that are Not numbers.
        ' ====

        ' Loop through Each character inthe card number submited
        For lnPosition = 1 To Len(anCardNumber)
            ' Grab the current character
            lsChar = Mid(anCardNumber, lnPosition, 1)
            ' if the character is a number, append it To our new number
            If IsNumeric(lsChar) Then lsNumber = lsNumber & lsChar

        Next ' lnPosition

        ' ====
        ' The credit card number must be between 13 and 16 digits.
        ' ====
        ' if the length of the number is less Then 13 digits, then Exit the routine
        If Len(lsNumber) < 13 Then Exit Function

        ' if the length of the number is more Then 16 digits, then Exit the routine
        If Len(lsNumber) > 16 Then Exit Function


        ' ====
        ' The credit card number must start with: 
        '       4 For Visa Cards 
        '       37 For American Express Cards 
        '       5 For MasterCards 
        '       6 For Discover Cards 
        ' ====

        ' Choose action based on Type of card
        Select Case LCase(asCardType)
            ' VISA
            Case "visa", "v"
                ' if first digit Not 4, Exit function
                If Not Left(lsNumber, 1) = "4" Then Exit Function
                ' American Express
            Case "american express", "americanexpress", "american", "ax", "amex"
                ' if first 2 digits Not 37, Exit function
                If Not Left(lsNumber, 2) = "37" Then Exit Function
                ' Mastercard
            Case "mastercard", "master card", "master", "m"
                ' if first digit Not 5, Exit function
                If Not Left(lsNumber, 1) = "5" Then Exit Function
                ' Discover
            Case "discover", "discovercard", "discover card", "d"
                ' if first digit Not 6, Exit function
                If Not Left(lsNumber, 1) = "6" Then Exit Function
            Case "bankcard"
                ' if first digit Not 5, Exit function
                If Not Left(lsNumber, 1) = "5" Then Exit Function
            Case Else
        End Select ' LCase(asCardType)

        ' ====
        ' if the credit card number is less Then 16 digits add zeros
        ' To the beginning to make it 16 digits.
        ' ====
        ' Continue Loop While the length of the number is less Then 16 digits
        While Not Len(lsNumber) = 16

            ' Insert 0 To the beginning of the number
            lsNumber = "0" & lsNumber


        End While ' Not Len(lsNumber) = 16

        ' ====
        ' Multiply Each digit of the credit card number by the corresponding digit of
        ' the mask, and sum the results together.
        ' ====

        ' Loop through Each digit
        For lnPosition = 1 To 16

            ' Parse a digit from a specified position In the number
            lnDigit = Mid(lsNumber, lnPosition, 1)

            ' Determine if we multiply by:
            '       1 (Even)
            '       2 (Odd)
            ' based On the position that we are reading the digit from
            lnMultiplier = 1 + (lnPosition Mod 2)

            ' Calculate the sum by multiplying the digit and the Multiplier
            lnSum = lnDigit * lnMultiplier

            ' (Single digits roll over To remain single. We manually have to Do this.)
            ' if the Sum is 10 or more, subtract 9
            If lnSum > 9 Then lnSum = lnSum - 9

            ' Add the sum To the total of all sums
            lnTotal = lnTotal + lnSum

        Next ' lnPosition

        ' ====
        ' Once all the results are summed divide
        ' by 10, if there is no remainder Then the credit card number is valid.
        ' ====
        IsCreditCard = ((lnTotal Mod 10) = 0)

    End Function ' IsCreditCard

    Public Function GetPayment(ByVal qs As String) As String
        Dim sXmlData As String = ""
        Dim sb As New StringBuilder
        Dim xml As New XmlDocument

        Dim creditcard As String = ""
        Dim name As String = ""
        Dim approval As String = ""
        Dim amt As String = ""
        Dim surcharge As String = ""
        Dim OriginalPayment As String = ""
        Try
            sXmlData = Aurora.Booking.Services.BookingPaymentMaintenance.getPayment(qs)

            If (IsValidXML(sXmlData)) Then
                xml.LoadXml(sXmlData)
                If (xml.SelectSingleNode("Root/Error/ErrStatus").InnerText = "False") Then
                    If (xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/Method").InnerText.Contains("EFT-POS")) Then
                        OriginalPayment = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/Amount").InnerText
                    Else
                        OriginalPayment = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/OriginalPayment").InnerText
                    End If

                    creditcard = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/Method").InnerText
                    creditcard = creditcard.Substring(0, creditcard.LastIndexOf("-"))
                    name = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountName").InnerText
                    approval = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/ApprovalRef").InnerText
                    amt = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount").InnerText
                    surcharge = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/SurchargeAmtPmt").InnerText

                    ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                    Dim PdtEFTPOSCardMethodDontMatch As String = ""
                    Dim PdtEFTPOSCardTypeSwiped As String = ""
                    Dim PdtEFTPOSCardTypeSurchargeAdded As String = ""
                    Dim PaymentType As String = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/paymentType").InnerText
                    If (Not xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch") Is Nothing) Then
                        PdtEFTPOSCardMethodDontMatch = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch").InnerText
                    End If

                    If Not xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped") Is Nothing Then
                        PdtEFTPOSCardTypeSwiped = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped").InnerText
                    End If

                    If Not xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded") Is Nothing Then
                        PdtEFTPOSCardTypeSurchargeAdded = xml.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded").InnerText
                    End If

                    Dim selectedcard As String = ""
                    Dim method As String = ""
                    Dim actualcard As String = ""
                    Try

                        If PdtEFTPOSCardTypeSwiped.Contains("~") Then
                            selectedcard = PdtEFTPOSCardTypeSwiped.Split("~")(0)
                            method = PdtEFTPOSCardTypeSwiped.Split("~")(1)
                            actualcard = PdtEFTPOSCardTypeSwiped.Split("~")(2)
                        End If

                    Catch ex As Exception
                        ''do nothing
                    End Try

                    If (PdtEFTPOSCardTypeSwiped.Length > 0 And PdtEFTPOSCardMethodDontMatch.Equals("1")) Then


                        If PaymentType.Equals("B") Or PaymentType.Equals("b") Then
                            If (method.Equals("Credit Card") And (actualcard.Equals("EFTPOS") Or actualcard.Equals("EFT-POS")) And (Not selectedcard.Equals("EFTPOS") Or Not selectedcard.Equals("EFT-POS"))) Then
                            End If
                            If (method.Equals("Debit Card") And (Not actualcard.Equals("EFTPOS") And Not actualcard.Equals("EFT-POS")) And (selectedcard.Equals("EFTPOS") Or selectedcard.Equals("EFT-POS"))) Then
                            End If
                        End If


                        If PaymentType.Equals("R") Or PaymentType.Equals("r") Then
                            If (method.Equals("Credit Card") And (actualcard.Equals("EFTPOS") Or actualcard.Equals("EFT-POS")) And (Not selectedcard.Equals("EFTPOS") Or Not selectedcard.Equals("EFT-POS"))) Then
                                If Not String.IsNullOrEmpty(amt) And Not String.IsNullOrEmpty(OriginalPayment) Then
                                    If CDec(amt) = (CDec(OriginalPayment) - CDec(PdtEFTPOSCardTypeSurchargeAdded)) Then
                                        surcharge = PdtEFTPOSCardTypeSurchargeAdded
                                        amt = CDec(amt) - PdtEFTPOSCardTypeSurchargeAdded
                                        OriginalPayment = amt
                                    Else
                                        ''do nothing
                                    End If
                                End If
                            End If

                            If (method.Equals("Debit Card") And (Not actualcard.Equals("EFTPOS") And Not actualcard.Equals("EFT-POS")) And (selectedcard.Equals("EFTPOS") Or selectedcard.Equals("EFT-POS"))) Then
                            End If

                        End If
                    End If

                    sb.AppendFormat("{0}{1}", creditcard, "@")
                    sb.AppendFormat("{0}{1}", name, "@")
                    sb.AppendFormat("{0}{1}", approval, "@")
                    sb.AppendFormat("{0}{1}", amt, "@")
                    sb.AppendFormat("{0}{1}", surcharge, "@")
                    sb.AppendFormat("{0}{1}", OriginalPayment, "")

                End If
            End If

        Catch ex As Exception
            Return "ERROR: Retrieving Payment"
            Logging.LogError("PAYMENT SCREEN: EFTPOS: GetPayment", ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return sb.ToString
    End Function

    Public Function RefundEFTPOSpayment(ByVal xml As String, Optional ByVal IsOverride As Boolean = False, Optional ByVal isPaymentChange As Boolean = False) As String

        ''xml = System.Web.HttpUtility.UrlDecode(xml)

        ''payment block
        Dim splitXML() As String = xml.Split("|")
        If Not IsValidXML(splitXML(0)) Then
            Logging.LogError("PAYMENT SCREEN:", MessageInformationFormat("RefundEFTPOSpayment", "check #1", splitXML(0)))
            Return "ERROR: This is not well-formed xml"
        End If

        Try


            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(splitXML(0))
            BooID = xmldoc.SelectSingleNode("Root/Data/Payment/BookingId").InnerText
            RntId = xmldoc.SelectSingleNode("Root/Data/Payment/RentalId").InnerText
            Dim retvalue As String = xmldoc.SelectSingleNode("Root/Data/Payment").OuterXml


            If Not IsValidXML(splitXML(1)) Then
                Logging.LogError("PAYMENT SCREEN:", MessageInformationFormat("RefundEFTPOSpayment", "check #2", splitXML(1)))
                Return "ERROR: This is not well-formed xml"
            End If

            xmldoc.LoadXml(splitXML(1))

            Dim node As XmlNode = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/BranchLocCode")
            Dim commonparent As XmlNode = node.ParentNode

            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/IsPostedtoFinanc")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/MethodDesc")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/RevPmtId")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/IsRevPmt")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/UsrLocCode")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/DPSRev")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/DPSPmt")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/paymentType")
            commonparent = node.ParentNode
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/RntB4Cki")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/isCloseOff")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/SurchargeAmtPmt")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/SurchargeAmtLoc")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/HomeCurr")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentCurr")
            node = commonparent.RemoveChild(node)

            node = xmldoc.CreateElement("LocalIdentifier")
            xmldoc.SelectSingleNode("PaymentEntryRoot").AppendChild(node)


            Dim paymentType As String = splitXML(6)
            If (paymentType = "R" Or paymentType = "D") Then
                TotalAmount = splitXML(5)
                CurrentAmount = splitXML(5)
            Else
                TotalAmount = splitXML(4)
                CurrentAmount = splitXML(5)
            End If

            node = xmldoc.CreateElement("OriginalAmt")
            xmldoc.SelectSingleNode("PaymentEntryRoot").AppendChild(node)

            Dim originalpayment As String = 0
            node = xmldoc.SelectSingleNode("PaymentEntryRoot/OriginalPayment")
            originalpayment = node.InnerText
            commonparent = node.ParentNode
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AuthCode")
            commonparent = node.ParentNode
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/MerchRef")
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/TxnRef")
            node = commonparent.RemoveChild(node)

            node = xmldoc.CreateElement("AuthCode")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)

            node = xmldoc.CreateElement("MerchRef")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)

            node = xmldoc.CreateElement("TxnRef")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)

            node = xmldoc.CreateElement("PdtDpsEfTxnRef")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtDpsEfTxnRef").InnerText = splitXML(7)



            node = xmldoc.CreateElement("IsCrdPre")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/IsCrdPre").InnerText = "1"


            ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
            If Not xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch") Is Nothing Then
                Dim PdtEFTPOSCardMethodDontMatch As String = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch").InnerText

                ''remove the PdtEFTPOSCardMethodDontMatch so we can recreate this after the isCardPre Element
                node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch")
                commonparent = node.ParentNode
                node = commonparent.RemoveChild(node)

                ''and recreate at the last element to avoid error when doing retreival based on numbers
                node = xmldoc.CreateElement("PdtEFTPOSCardMethodDontMatch")
                xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)
                xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch").InnerText = IIf(String.IsNullOrEmpty(PdtEFTPOSCardMethodDontMatch), String.Empty, PdtEFTPOSCardMethodDontMatch)
            End If

            If Not xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped") Is Nothing Then
                Dim PdtEFTPOSCardTypeSwiped As String = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped").InnerText
                ''remove the PdtEFTPOSCardTypeSwiped so we can recreate this after the isCardPre Element
                node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped")
                commonparent = node.ParentNode
                node = commonparent.RemoveChild(node)

                ''and recreate at the last element to avoid error when doing retreival based on numbers
                node = xmldoc.CreateElement("PdtEFTPOSCardTypeSwiped")
                xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)
                xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped").InnerText = IIf(String.IsNullOrEmpty(PdtEFTPOSCardTypeSwiped), String.Empty, PdtEFTPOSCardTypeSwiped)
            End If

            If Not xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded") Is Nothing Then
                Dim PdtEFTPOSCardTypeSurchargeAdded As String = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded").InnerText

                node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded")
                commonparent = node.ParentNode
                node = commonparent.RemoveChild(node)

                ''and recreate at the last element to avoid error when doing retreival based on numbers
                node = xmldoc.CreateElement("PdtEFTPOSCardTypeSurchargeAdded")
                xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem").AppendChild(node)
                xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded").InnerText = IIf(String.IsNullOrEmpty(PdtEFTPOSCardTypeSurchargeAdded), String.Empty, PdtEFTPOSCardTypeSurchargeAdded)
            End If

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/DpsBrdCode")
            commonparent = node.ParentNode
            node = commonparent.RemoveChild(node)

            node = xmldoc.CreateElement("LocalIdentifier")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry").AppendChild(node)
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/LocalIdentifier").InnerText = GetLocalIdentifier()


            node = xmldoc.CreateElement("OriginalAmt")
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry").AppendChild(node)
            ''rev:mia dec 14 2010 
            If (CDec(CurrentAmount) <> CDec(originalpayment)) Then
                originalpayment = CurrentAmount
            End If
            xmldoc.SelectSingleNode("PaymentEntryRoot/PaymentEntry/OriginalAmt").InnerText = -1 * CDec(originalpayment) ''"-" & CurrentAmount

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/OriginalAmt")
            commonparent = node.ParentNode
            node = commonparent.RemoveChild(node)

            node = xmldoc.SelectSingleNode("PaymentEntryRoot/LocalIdentifier")
            node = commonparent.RemoveChild(node)

            retvalue = retvalue + xmldoc.SelectSingleNode("PaymentEntryRoot").OuterXml
            retvalue = retvalue + splitXML(2)
            UsersCode = splitXML(3)


            If (Not splitXML(11) Is Nothing And Not splitXML(12) Is Nothing) Then
                retvalue = CreateRefundXML(retvalue, splitXML(11), splitXML(12), "", splitXML(13))
                Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("CreateRefundXML", "check # 3", retvalue))
            Else
                retvalue = CreateRefundXML(retvalue)
                Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("CreateRefundXML", "check # 4", retvalue))
            End If


            If (IsValidXML(retvalue) = False) Then
                Logging.LogError("PAYMENT SCREEN:", MessageInformationFormat("CreateRefundXML", "check #5", retvalue))
                Return "ERROR: This is not well-formed xml"
            End If

            ''  Return "ERROR: STOP HERE FOR TESTING"
            Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("RefundPayment", "retvalue", retvalue))
            retvalue = RefundPayment(retvalue, UsersCode, "PaymentMGT.aspx?EFTPOS", isPaymentChange)
            Logging.LogInformation("PAYMENT SCREEN", MessageInformationFormat("RefundPayment", "RESULT", retvalue))

            Dim xmlresult As New XmlDocument
            xmlresult.LoadXml(retvalue)
            If (xmlresult.SelectNodes("Root/Error/ErrStatus").Item(0).InnerText = "True") Then
                Logging.LogError("refundEFTPOSPayment", MessageInformationFormat("RefundPayment", "RntId:", RntId, " retvalue", retvalue))
                Return String.Concat("ERROR:", xmlresult.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)
            Else


                If Not splitXML(8) Is Nothing AndAlso Not splitXML(9) Is Nothing Then
                    Dim paymentid As String = xmlresult.SelectSingleNode("Root/Data/PaymentEntryRoot/PmEntryPaymentId").InnerText
                    Dim reason As String = splitXML(9)
                    Dim usercode As String = splitXML(8)
                    Dim otherreason As String = ""
                    If (Not splitXML(10) Is Nothing) Then
                        otherreason = splitXML(10).TrimEnd
                    End If
                    Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
                    objEFTPOS.InsertPaymentOverrideReasons(paymentid, reason, usercode, otherreason)
                    If (IsOverride) Then
                        objEFTPOS.AddEFTPOSHistory(RntId, usercode, "EFTPOS OVERRIDE")
                    End If
                End If

                retvalue = updatePaymentHistory(BooID, RntId, "REVPAYMENT")
                Logging.LogInformation("refundEFTPOSPayment", MessageInformationFormat("updatePaymentHistory", "RntId:", RntId, " retvalue", retvalue))

                If Not retvalue.Contains("ERROR") Then
                    Dim pmtid As String = ""
                    If Not xmlresult.SelectSingleNode("Root/Data/PaymentEntryRoot/PaymentEntryItems/PmtItem/PmtPtmId") Is Nothing Then
                        pmtid = "|" & xmlresult.SelectSingleNode("Root/Data/PaymentEntryRoot/PaymentEntryItems/PmtItem/PmtPtmId").InnerText
                    End If
                    retvalue = "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10&hdBookingId=" + BooID + "&hdRentalId=" + RntId + pmtid
                    ''retvalue = "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10&hdBookingId=" + BooID + "&hdRentalId=" + RntId
                    Logging.LogInformation("refundEFTPOSPayment", MessageInformationFormat("updatePaymentHistory", "RntId:", RntId, " pmtid", pmtid))
                End If
            End If
            Return retvalue

        Catch ex As Exception
            Logging.LogError("refundEFTPOSPayment", MessageInformationFormat("exception", "RntId:", RntId, " source", ex.Source, " stack", ex.StackTrace, "xml", xml))
            Return "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10&hdBookingId=" + BooID + "&hdRentalId=" + RntId
        End Try

    End Function

    ''' <summary>
    ''' this will check if card to be use is already stored against that booking 
    ''' </summary>
    ''' <param name="bookingnumber"></param>
    ''' <param name="cardnumber"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsCreditCardInBooking(ByVal bookingnumber As String, _
                                          ByVal cardnumber As String, _
                                          Optional ByVal CardHolderName As String = "", _
                                          Optional ByVal type As String = "") As Boolean
        Dim result As New DataSet
        IsCreditCardInBooking = False

        Try

            Aurora.Common.Data.ExecuteDataSetSP("EFTPOS_GetBookingCreditCards", result, bookingnumber)
            Dim cardRetreive As String = ""
            Dim cardDecrypt As String = ""
            Dim holderNameRetreive As String = ""

            If result.Tables.Count <> -1 Then
                For Each row As DataRow In result.Tables(0).Rows
                    cardRetreive = row("PdtAccNum").ToString.TrimEnd
                    holderNameRetreive = row("PdtAccName").ToString.TrimEnd

                    ''possible that the card was encrypted
                    If Not String.IsNullOrEmpty(cardRetreive) Then '' AndAlso (cardRetreive.Length > 20) Then

                        IsCreditCardInBooking = True
                        Exit For

                        ''decrypt and compare 
                        'Dim blnResult As Boolean = DecryptCreditCardDataSuccess(cardRetreive, cardDecrypt)
                        'If (blnResult) Then
                        '    If (cardnumber.TrimEnd.Equals(cardDecrypt.TrimEnd)) And (CardHolderName.TrimEnd.Equals(holderNameRetreive)) Then
                        '        IsCreditCardInBooking = True
                        '        Exit For
                        '    End If
                        'Else
                        '    ''something goes wrong..but compare this card again
                        '    If (cardnumber.TrimEnd.Equals(cardRetreive.TrimEnd) And (CardHolderName.TrimEnd.Equals(holderNameRetreive))) Then
                        '        IsCreditCardInBooking = True
                        '        Exit For
                        '    End If ''If (cardnumber.TrimEnd.Equals(cardRetreive.TrimEnd)) Then

                        'End If 'If (blnResult) Then


                    End If ''If (cardRetreive.Length > 20) Then

                Next
            End If

        Catch ex As Exception
            IsCreditCardInBooking = False
            Logging.LogError("PAYMENT SCREEN: EFTPOS: IsCreditCardInBooking", ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return IsCreditCardInBooking
    End Function

    ''' <summary>
    '''  this will check if the cardholdername is in the booking
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function IsCardHolderNameInBooking() As Boolean

    End Function


    Public Function IsCreditCardIsFoundInBondPayment(ByVal bookingNumber As String, _
                                                     ByVal cardNumber As String, _
                                                     ByVal CardHolderName As String) As Boolean
        Dim result As New DataSet
        IsCreditCardIsFoundInBondPayment = False
        Try
            Aurora.Common.Data.ExecuteDataSetSP("EFTPOS_GetCreditCardUsedInBond", result, bookingNumber, cardNumber)
            Dim cardRetreive As String = ""
            Dim cardDecrypt As String = ""
            Dim holderNameRetreive As String = ""

            If Not String.IsNullOrEmpty(CardHolderName) Then
                CardHolderName = CardHolderName.ToUpper
            End If

            If Not String.IsNullOrEmpty(cardNumber) Then
                cardNumber = cardNumber.Trim
            End If


            If result.Tables.Count <> -1 Then
                For Each row As DataRow In result.Tables(0).Rows
                    cardRetreive = row("PdtAccNum").ToString.TrimEnd
                    holderNameRetreive = row("PdtAccName").ToString.TrimEnd

                    If Not String.IsNullOrEmpty(holderNameRetreive) Then
                        holderNameRetreive = holderNameRetreive.ToUpper
                    End If

                    ''possible that the card was encrypted
                    If Not String.IsNullOrEmpty(cardRetreive) Then '' AndAlso (cardRetreive.Length > 20) Then

                        'rev:mia Sept 17 2012 - changes related to credit card with a chip embed on it.
                        '		     - Since DPS cant provide embedded card, fixes will be 
                        '            - carried-out by removing the cardname comparison in every 
                        '            - transactions.
                        IsCreditCardIsFoundInBondPayment = True
                        Exit For

                        ' ''decrypt and compare 
                        'Dim blnResult As Boolean = DecryptCreditCardDataSuccess(cardRetreive, cardDecrypt)
                        'If (blnResult) Then

                        '    If (cardNumber.TrimEnd.Equals(cardDecrypt.TrimEnd)) And (CardHolderName.TrimEnd.Equals(holderNameRetreive)) Then
                        '        IsCreditCardIsFoundInBondPayment = True
                        '        Exit For
                        '    End If

                        'Else

                        '    ''something goes wrong..but compare this card again
                        '    If (cardNumber.TrimEnd.Equals(cardRetreive.TrimEnd) And (CardHolderName.TrimEnd.Equals(holderNameRetreive))) Then
                        '        IsCreditCardIsFoundInBondPayment = True
                        '        Exit For
                        '    End If ''If (cardnumber.TrimEnd.Equals(cardRetreive.TrimEnd)) Then

                        'End If 'If (blnResult) Then


                    End If ''If (cardRetreive.Length > 20) Then
                Next
            End If

        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: IsCreditCardIsFoundInBondPayment", ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return IsCreditCardIsFoundInBondPayment
    End Function

    Public Function AddEFTPOSHistory(ByVal sRntId As String, _
                                       ByVal sUserCode As String, _
                                       Optional ByVal eftposEvent As String = "") As String


        Dim BpdID As String = ""
        Dim changeEvent As String = ""

        Try
            Logging.LogInformation("AddEFTPOSHistory", MessageInformationFormat("AddEFTPOSHistory", "RntId:", sRntId, _
                                                                                                 " USERCODE", sUserCode, _
                                                                                                 " eftposEvent", eftposEvent))
            Aurora.Common.Data.ExecuteScalarSP("REs_CheckRentalHistory", "", _
                                                                        sRntId, _
                                                                        BpdID, _
                                                                        "", _
                                                                        eftposEvent.ToUpper, _
                                                                        "BPD", _
                                                                        changeEvent, _
                                                                        "", _
                                                                        sUserCode, _
                                                                        "BookingPaymenteftposMaintenance.AddEFTPOSHistory")
        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: AddEFTPOSHistory", ex.Message & vbCrLf & ex.StackTrace)
            Return ex.Message
        End Try
        Return ""
    End Function
#End Region

#Region "Updating Payment History"
    Private Function updatePaymentHistory(ByVal sbooid As String, _
                                    ByVal srentid As String, _
                                    ByVal smethod As String) As String

        Dim strData As String = "<Root><Data><paramA>" + sbooid + "</paramA><paramB>" + srentid + "</paramB></Data></Root>"
        Dim strManageResult As String = ""

        Try

            Logging.LogWarning("PAYMENT SCREEN: EFTPOS: updatePaymentHistory ", vbCrLf & vbCrLf & "updatePaymentHistory parameter: ".ToUpper & vbCrLf & strData & "," & smethod)
            strManageResult = Aurora.Booking.Services.BookingPaymentUpdateSingleItem.UpdateItem(strData, UsersCode, "Booking", "paym_updatePaymentHistory", smethod)
            Logging.LogWarning("PAYMENT SCREEN: EFTPOS: updatePaymentHistory ", vbCrLf & vbCrLf & "updatePaymentHistory result: ".ToUpper & vbCrLf & strManageResult)

        Catch ex As Exception
            Logging.LogWarning("PAYMENT SCREEN: EFTPOS error: updatePaymentHistory ", "ERROR: updatePaymentHistory: ".ToUpper & ex.StackTrace)
            Return "ERROR:" + ex.Message
        End Try
        Return strManageResult
    End Function


#End Region

#Region "Saving Of Payment"
    Private Function SavePayment(ByVal xml As String, _
                                 ByVal user As String, _
                                 ByVal urlreferrer As String) As String
        Dim strManageResult As String

        Try

            strManageResult = Aurora.Booking.Services.BookingPaymentMaintenance.maintainPayment(xml, user, urlreferrer)

        Catch ex As Exception
            Logging.LogWarning("PAYMENT SCREEN: EFTPOS error: SavePayment ", vbCrLf & vbCrLf & "SavePayment: ".ToUpper & vbCrLf & ex.Message)
            Return "ERROR:" + ex.Message
        End Try
        Return strManageResult
    End Function


#End Region

#Region "Refund payment"
    Private Function RefundPayment(ByVal xml As String, _
                           ByVal user As String, _
                           ByVal urlreferrer As String, _
                           Optional ByVal IsPaymentChange As Boolean = False) As String

        Dim strManageResult As String
        Try
            strManageResult = Aurora.Booking.Services.BookingPaymentMaintenance.paymentReversal(xml, user, urlreferrer, IsPaymentChange)
        Catch ex As Exception
            Logging.LogWarning("PAYMENT SCREEN: EFTPOS error: RefundPayment ", vbCrLf & vbCrLf & "RefundPayment: ".ToUpper & vbCrLf & ex.Message)
            Return "ERROR:" + ex.Message
        End Try
        Return strManageResult
    End Function

#End Region

#Region "Private Function"

    Private Function isSurChargeApplied(ByVal paymentid As String, _
                                    ByVal paramRentID As String, _
                                    ByVal paramSelectedMethod As String, _
                                    ByVal paramType As String, _
                                    ByVal paramAmt As Decimal, _
                                    ByVal usercode As String) As Decimal
        Try


            UsersCode = usercode

            If (paramAmt < CDec(0) AndAlso _
                ((paymentid = "") AndAlso _
                paramType <> "B")) Then
                Return 0
            End If

            If (String.IsNullOrEmpty(paramSelectedMethod) Or paramSelectedMethod = "0") Then
                Return 0
            End If
            Dim xmlString As String = Aurora.Common.Data.ExecuteScalarSP("RES_getPaymentMethodSurcharge", _
                                      paramRentID, _
                                      paramSelectedMethod, _
                                      paramType, _
                                      paramAmt, _
                                      usercode, _
                                      "PaymentMgt", _
                                      1, _
                                      0, _
                                      "", _
                                      "")

            If String.IsNullOrEmpty(xmlString) Then Return 0

            Dim tempObjXML As New XmlDocument
            tempObjXML.LoadXml(xmlString)

            If tempObjXML.OuterXml.Contains("Error") = True Then
                tempObjXML = Nothing
                Return 0
            Else
                Dim surcharge As String = (tempObjXML.DocumentElement.ChildNodes(0).InnerText)
                If surcharge <> "" Then
                    If CDec(surcharge) <> 0 Then
                        Return CDec(surcharge)
                    End If
                End If
            End If

        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: isSurChargeApplied", ex.Message & vbCrLf & ex.StackTrace)
            Return 0
        End Try
    End Function

    Private Function IsValidXML(ByVal xml As String) As Boolean
        Try

            Dim doc As New XmlDocument
            doc.LoadXml(xml)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

    Private Function CreateRefundXML(ByVal xmlrefund As String, _
                                     Optional ByVal newCard As String = "", _
                                     Optional ByVal CardTypeEntered As String = "", _
                                     Optional ByVal CardID As String = "", _
                                     Optional ByVal newCardHolderName As String = "") As String
        Dim xmldoc As New XmlDocument
        Dim sb As New StringBuilder
        sb.Append("<Root>")
        sb.AppendFormat("{0}", xmlrefund)
        sb.Append("</Root>")

        ''add id attribute
        sb.Replace("<PaymentEntry>", "<PaymentEntry id='0'>")

        If (IsValidXML(sb.ToString)) Then
            xmldoc.LoadXml(sb.ToString)
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Details").InnerText = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountName").InnerText
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Amount").InnerText = TotalAmount
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = TotalAmount
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/IsRemove").InnerText = "0"

            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount").InnerText = TotalAmount
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtLocalAmount").InnerText = TotalAmount

            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/IsCrdPre").InnerText = "1"
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AuthCode").InnerText = ""
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/MerchRef").InnerText = ""
            xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/TxnRef").InnerText = ""

            Dim method As String = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText
            If ("EFT-POS - Debit Card" = method) Then
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText = "Debit Card - EFT-POS"
            Else
                Dim objType() As String = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText.Split("-")
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText = String.Concat(objType(1), " - ", objType(0))
            End If


            Dim payment As String = xmldoc.SelectSingleNode("Root/Payment/PmPaymentDate").InnerText
            xmldoc.SelectSingleNode("Root/Payment/PmPaymentDate").InnerText = payment.Substring(0, 10) + " " + payment.Substring(11)

            ''check if its an amex
            Dim cardtype As String = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText
            If cardtype.IndexOf("Amex") > -1 Then
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Amount").InnerText = CurrentAmount
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = CurrentAmount
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount").InnerText = CurrentAmount
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtLocalAmount").InnerText = CurrentAmount
            End If

            ''compared credit card if its the same. otherwise change it to the new swipe card
            Dim encrypted As String = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountNo").InnerText
            Dim decrypted As String = ""
            Dim blnSuccess As Boolean = DecryptCreditCardDataSuccess(encrypted, decrypted, True)
            Dim isSame As Boolean = True

            If (Not String.IsNullOrEmpty(newCard) AndAlso Not String.IsNullOrEmpty(CardTypeEntered)) Then
                If (newCard.TrimEnd = decrypted) Then
                    ''do nothing
                Else
                    xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountNo").InnerText = newCard ''EncryptCreditCardDataSuccess(newCard)
                    isSame = False
                End If
            End If


            ''change SubMethod/Method for the card type.... if not the same
            If Not isSame Then
                ''name is different
                Dim details As String = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Details").InnerText
                If Not (newCardHolderName.Equals(details)) Then
                    xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Details").InnerText = newCardHolderName
                    xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountName").InnerText = newCardHolderName
                End If

                ''DPS returned EFTPOS  instead of EFT-POS
                If CardTypeEntered.ToUpper = "EFTPOS" Then
                    CardTypeEntered = "EFT-POS"
                End If
                xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/SubMethod").InnerText = GetCardName(CardTypeEntered)
                Dim aryNewMethod() As String = xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText.Split("-")


                If (CardTypeEntered.Equals("DEBIT") Or CardTypeEntered.Equals("Debit")) Then
                    xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText = "Debit Card - EFT-POS"
                Else

                    If aryNewMethod(0).TrimEnd.Equals("Debit Card") Then
                        xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText = "Credit Card - " + CardTypeEntered
                    Else
                        xmldoc.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Method").InnerText = aryNewMethod(0) + " - " + CardTypeEntered
                    End If ''If aryNewMethod(0).Equals("Debit Card") Then


                End If ''If (CardTypeEntered.Equals("DEBIT") Or CardTypeEntered.Equals("Debit")) Then

            End If ''If Not isSame Then

        Else
            sb = Nothing
            Return "ERROR:Cannot create XML Refund"
        End If
        sb = Nothing
        Return xmldoc.OuterXml
    End Function

    Private Function CreatePaymentXML(ByVal xmlpayment As String) As String
        Dim xmldoc As New XmlDocument
        Dim sb As New StringBuilder
        sb.Append("<Root>")
        sb.AppendFormat("{0}", xmlpayment)
        sb.Append("</Root>")
        sb.Replace("<LocalIdentifier>create a function</LocalIdentifier>", "<LocalIdentifier>" + GetLocalIdentifier() + "</LocalIdentifier>")

        If (IsValidXML(sb.ToString)) Then
            xmldoc.LoadXml(sb.ToString)
            xmldoc.SelectSingleNode("Root/AmountsOutstandingRoot/AmountsOutstanding/CustToPay").InnerText = CurrentAmount
        Else
            sb = Nothing
            Logging.LogError("PAYMENT SCREEN: EFTPOS: SaveEFTPOSPayment", String.Concat("ERROR check #3: ", xmlpayment))
            Return "ERROR:Cannot create XML Payment"
        End If
        sb = Nothing
        Return xmldoc.OuterXml
    End Function

    Private _TypeCode As String
    Private Property TypeCode() As String
        Get
            Return _TypeCode
        End Get
        Set(ByVal value As String)
            _TypeCode = value
        End Set
    End Property

    Private Function GetSurcharge(ByVal qs As String) As Decimal
        Dim aryQS() As String = qs.Split("@")
        Dim paymentid As String = aryQS(0)
        Dim RentID As String = aryQS(1)
        Dim Type As String = aryQS(2)
        TypeCode = Type
        Dim userCode As String = aryQS(3)
        Dim Amt As Decimal = aryQS(4)
        Dim SelectedMethod As String = aryQS(5)
        Return isSurChargeApplied(paymentid, RentID.Trim, SelectedMethod, Type, Amt, userCode)
    End Function

    Private Function GetLocalIdentifier() As String

        Dim tValue As String = Nothing
        Dim s As String = Nothing
        Dim n As Integer
        Dim randomNum As New Random

        Dim str As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        For i As Integer = 0 To 16
            n = randomNum.Next(25)
            s = str.Substring(n, 1)
            If Not String.IsNullOrEmpty(s) Then
                tValue = tValue & s
            End If
        Next
        Return tValue
    End Function

    Private Function GetCardName(ByVal cardName As String) As String
        ''rev:mia nov 15 2010 issue #113
        If cardName.ToUpper.Equals("EFTPOS") Then
            cardName = "EFT-POS"
        End If
        Return Aurora.Common.Data.ExecuteScalarSP("GetCardTypeEftPos", cardName)
    End Function

    Public Function GetCardNameForDebitIsNotEFTPOS(ByVal cardname As String) As String
        Return GetCardName(cardname)
    End Function

    Private Function EncryptCreditCardDataSuccess(ByVal strxml As String, ByRef returnedXML As String) As Boolean
        Dim xmlTempObject As New XmlDocument
        Dim creditcard As String = ""
        Try


            xmlTempObject.LoadXml(strxml)
            Dim nodes As XmlNodeList = xmlTempObject.SelectNodes("PaymentEntryRoot/PaymentEntry")
            Dim mKeyfile As String = HttpContext.Current.Server.MapPath("~/app_code/" & System.Configuration.ConfigurationManager.AppSettings("KEYFILE"))

            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = System.Configuration.ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = System.Configuration.ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)

            creditcard = xmlTempObject.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountNo").InnerText
            If creditcard <> "" And creditcard.Length < 64 Then
                Dim encryptedCreditcard As Byte() = Aurora.Booking.Services.BookingPaymentCardEncryption.EncyrptCardNumber(creditcard, mKeyfile)
                xmlTempObject.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountNo").InnerText = Convert.ToBase64String(encryptedCreditcard)
            Else
                Dim decryptedCreditcard As String = Aurora.Booking.Services.BookingPaymentCardEncryption.DecryptCardNumber(Convert.FromBase64String(creditcard), mKeyfile, True)
                xmlTempObject.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/AccountNo").InnerText = decryptedCreditcard
            End If

            returnedXML = xmlTempObject.OuterXml
            Return True

        Catch ex As Exception
            Logging.LogWarning("", "--------START: EncryptCreditCardDataSuccess MESSAGE-------------------")
            Logging.LogWarning("", vbCrLf & vbCrLf & "ERROR: Failed to Encrypt " & creditcard & vbCrLf & "MESSAGE: " & ex.Message & vbCrLf & "STACKTRACE: " & ex.StackTrace & vbCrLf & vbCrLf)
            Logging.LogWarning("", "--------END: EncryptCreditCardDataSuccess MESSAGE-------------------" & vbCrLf)
            Return False
        Finally
            xmlTempObject = Nothing
        End Try
    End Function

    Private Function EncryptCreditCardDataSuccess(ByVal creditcard As String) As String
        Try
            Dim mKeyfile As String = HttpContext.Current.Server.MapPath("~/app_code/" & System.Configuration.ConfigurationManager.AppSettings("KEYFILE"))

            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = System.Configuration.ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = System.Configuration.ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)

            If creditcard <> "" Then '' And creditcard.Length < 64 Then
                Dim encryptedCreditcard As Byte() = Aurora.Booking.Services.BookingPaymentCardEncryption.EncyrptCardNumber(creditcard, mKeyfile)
                Return Convert.ToBase64String(encryptedCreditcard)
            End If


        Catch ex As Exception
            Logging.LogWarning("", "--------START: EncryptCreditCardDataSuccess MESSAGE-------------------")
            Logging.LogWarning("", vbCrLf & vbCrLf & "ERROR: Failed to Encrypt " & creditcard & vbCrLf & "MESSAGE: " & ex.Message & vbCrLf & "STACKTRACE: " & ex.StackTrace & vbCrLf & vbCrLf)
            Logging.LogWarning("", "--------END: EncryptCreditCardDataSuccess MESSAGE-------------------" & vbCrLf)
            Return False
        End Try
    End Function

    Private Function DecryptCreditCardDataSuccess(ByVal cardNumberEncrypted As String, ByRef cardNumberdecrypted As String, Optional ByVal isAllowed As Boolean = False) As Boolean
        Try
            Dim mKeyfile As String = HttpContext.Current.Server.MapPath("~/app_code/" & System.Configuration.ConfigurationManager.AppSettings("KEYFILE"))
            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = System.Configuration.ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = System.Configuration.ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)

            cardNumberdecrypted = Aurora.Booking.Services.BookingPaymentCardEncryption.DecryptCardNumber(Convert.FromBase64String(cardNumberEncrypted), mKeyfile, isAllowed)
            Return True

        Catch ex As Exception
            cardNumberdecrypted = cardNumberEncrypted
            Logging.LogInformation("PAYMGT", "FAILED: DecryptCreditCardDataSuccess(" & cardNumberEncrypted & ") ---> " & ex.Message)
            Return False
        End Try

    End Function



#End Region

#Region "functions for logging the reasons during override"

    Public Function RetrieveEftposReasons() As DataSet
        Dim result As New DataSet
        Return Aurora.Common.Data.ExecuteDataSetSP("GetPaymentOverrideList", result, True)
    End Function

    Public Function RetrieveEftposReasons(ByVal displayType As String) As DataSet
        Dim result As New DataSet
        Return Aurora.Common.Data.ExecuteDataSetSP("GetPaymentOverrideListWithDisplayType", result, True, displayType.Trim)
    End Function

    Public Function InsertPaymentOverrideReasons(ByVal pmtid As String, ByVal pmtreasonid As Integer, ByVal usercode As String, Optional ByVal PmtReasonOthers As String = "") As Object
        Logging.LogInformation("InsertPaymentOverrideReasons", MessageInformationFormat("InsertPaymentOverrideReasons", "RntId:", RntId, _
                                                                                                             " PAYMENTID", pmtid, _
                                                                                                             " REASON", pmtreasonid, _
                                                                                                             " USERCODE", usercode, _
                                                                                                             " OTHERREASON", PmtReasonOthers))
        Return Aurora.Common.Data.ExecuteScalarSP("InsertPaymentOverrideReasons", pmtid, pmtreasonid, usercode, PmtReasonOthers)
    End Function
#End Region

#Region "Check Password for refunding"
    Public Function RefundOverridePassword(ByVal userinfo As String) As String

        Try
            Dim login As String = userinfo.Split("|")(0)
            Dim password As String = userinfo.Split("|")(1)
            Dim role As String = userinfo.Split("|")(2)

            Return Aurora.Common.Service.CheckUser(login, password, role)
        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: RefundOverridePassword", ex.Message & vbCrLf & ex.StackTrace)
            Return "ERROR:" + ex.Message
        End Try
    End Function
#End Region

#Region "functions for saving a receipt"
    Public Function SaveReceipt(ByVal info As String, ByVal receipt As String, ByVal intWidth As Integer) As String

        Dim PmtID As String = info.Split("|")(0)
        Dim PdtDpsEfTxnRef As String = info.Split("|")(1)
        Dim usercode As String = info.Split("|")(2)

        Dim ctr As Integer = 1
        Dim indexofChar As String = ""
        Dim newreceipt As New StringBuilder

        For i As Integer = 0 To receipt.Length
            If (i = 0) Then ctr = 0
            If (ctr > intWidth) Then ctr = 1

            Try

                indexofChar = receipt.Substring(i, 1)
                ''Debug.WriteLine(ctr & " : " + indexofChar + " --> " + newreceipt.ToString)

                If (ctr <= intWidth) Then
                    ''indexofChar = receipt.Substring(i, 1)
                    ''Debug.Assert(ctr < 30, "ctr is " & ctr)

                    If (ctr = intWidth) Then
                        newreceipt.AppendFormat("{0}{1}", ControlChars.NewLine, indexofChar)
                    Else
                        newreceipt.Append(indexofChar)
                    End If

                    ''Debug.WriteLine(ctr & " : " + indexofChar + " --> " + newreceipt.ToString)

                End If
                ctr = ctr + 1

            Catch ex As Exception
            End Try
        Next i
        receipt = newreceipt.ToString.TrimEnd
        Try
            Dim Result As Object = Aurora.Common.Data.ExecuteScalarSP("InsertPaymentEFTPOSreceipts", PmtID, PdtDpsEfTxnRef, receipt, usercode)
            ''PrintReceipt(receipt)
        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: SaveReceipt", ex.Message & vbCrLf & ex.StackTrace)
            Return "ERROR"
        End Try
        Return "OK"

    End Function
#End Region

#Region "UNUSED: ReadCard events call this function"

    'Private Function GetCardConfigXML() As XmlDocument
    '    Dim xmlCardConfig As New XmlDocument
    '    xmlCardConfig.Load(HttpContext.Current.Server.MapPath("~/app_code/CreditCards.xml"))

    'End Function

    'Public Function CreditCardDetection(ByRef anCardNumber As String) As Boolean

    '    ' Performs a Mod 10 check To make sure the credit card number
    '    ' appears valid
    '    ' Developers may use the following numbers as dummy data:
    '    ' Visa:                               430-00000-00000
    '    ' American Express:            372-00000-00000
    '    ' Mastercard:                  521-00000-00000
    '    ' Discover:                           620-00000-00000

    '    Dim lsNumber           ' Credit card number stripped of all spaces, dashes, etc.
    '    Dim lsChar                     ' an individual character
    '    Dim lnTotal                    ' Sum of all calculations
    '    Dim lnDigit                    ' A digit found within a credit card number
    '    Dim lnPosition         ' identifies a character position In a String
    '    Dim lnSum                      ' Sum of calculations For a specific Set
    '    Dim lnMultiplier As Integer
    '    ' Default result is False
    '    CreditCardDetection = False

    '    ' ====
    '    ' Strip all characters that are Not numbers.
    '    ' ====

    '    ' Loop through Each character inthe card number submited
    '    For lnPosition = 1 To Len(anCardNumber)
    '        ' Grab the current character
    '        lsChar = Mid(anCardNumber, lnPosition, 1)
    '        ' if the character is a number, append it To our new number
    '        If IsNumeric(lsChar) Then lsNumber = lsNumber & lsChar

    '    Next ' lnPosition

    '    ' ====
    '    ' The credit card number must be between 13 and 16 digits.
    '    ' ====
    '    ' if the length of the number is less Then 13 digits, then Exit the routine
    '    If Len(lsNumber) < 13 Then Exit Function

    '    ' if the length of the number is more Then 16 digits, then Exit the routine
    '    If Len(lsNumber) > 16 Then Exit Function


    '    ' ====
    '    ' The credit card number must start with: 
    '    '       4 For Visa Cards 
    '    '       37 For American Express Cards 
    '    '       5 For MasterCards 
    '    '       6 For Discover Cards 
    '    ' ====

    '    ' Choose action based on Type of card

    '    Dim asCardType As String = ""
    '    Dim intFirstTwo As Integer = -1
    '    ''mastercard
    '    If Left(lsNumber, 1) = "5" Then
    '        intFirstTwo = lsNumber.ToString.Substring(1, 2)

    '        If intFirstTwo >= 51 And intFirstTwo <= 55 Then
    '            asCardType = "MasterCard"
    '        Else
    '            asCardType = "EFTPOS"
    '        End If

    '    End If

    '    ''visa  assumption
    '    If Left(lsNumber, 1) = "4" Then
    '        asCardType = "Visa"
    '    End If

    '    ''american express
    '    If Left(lsNumber, 2) = "37" Then
    '        asCardType = "Amex"
    '    End If



    '    Select Case LCase(asCardType)
    '        ' VISA
    '        Case "visa"
    '            ' if first digit Not 4, Exit function
    '            If Not Left(lsNumber, 1) = "4" Then Exit Function
    '            ' American Express
    '        Case "Amex"
    '            ' if first 2 digits Not 37, Exit function
    '            If Not Left(lsNumber, 2) = "37" Then Exit Function
    '            ' Mastercard
    '        Case "MasterCard"
    '            ' if first digit Not 5, Exit function
    '            If Not Left(lsNumber, 1) = "5" Then Exit Function

    '        Case Else
    '    End Select ' LCase(asCardType)

    '    ' ====
    '    ' if the credit card number is less Then 16 digits add zeros
    '    ' To the beginning to make it 16 digits.
    '    ' ====
    '    ' Continue Loop While the length of the number is less Then 16 digits
    '    While Not Len(lsNumber) = 16

    '        ' Insert 0 To the beginning of the number
    '        lsNumber = "0" & lsNumber


    '    End While ' Not Len(lsNumber) = 16

    '    ' ====
    '    ' Multiply Each digit of the credit card number by the corresponding digit of
    '    ' the mask, and sum the results together.
    '    ' ====

    '    ' Loop through Each digit
    '    For lnPosition = 1 To 16

    '        ' Parse a digit from a specified position In the number
    '        lnDigit = Mid(lsNumber, lnPosition, 1)

    '        ' Determine if we multiply by:
    '        '       1 (Even)
    '        '       2 (Odd)
    '        ' based On the position that we are reading the digit from
    '        lnMultiplier = 1 + (lnPosition Mod 2)

    '        ' Calculate the sum by multiplying the digit and the Multiplier
    '        lnSum = lnDigit * lnMultiplier

    '        ' (Single digits roll over To remain single. We manually have to Do this.)
    '        ' if the Sum is 10 or more, subtract 9
    '        If lnSum > 9 Then lnSum = lnSum - 9

    '        ' Add the sum To the total of all sums
    '        lnTotal = lnTotal + lnSum

    '    Next ' lnPosition

    '    ' ====
    '    ' Once all the results are summed divide
    '    ' by 10, if there is no remainder Then the credit card number is valid.
    '    ' ====
    '    CreditCardDetection = ((lnTotal Mod 10) = 0)

    'End Function ' IsCreditCard
#End Region

#Region "CheckCSRRole"
    Function CheckCSRRole(ByVal id As String, ByVal role As String) As String
        Dim sReturnMsg As String = "Error"
        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("dialog_checkUserForCSR", id, role)
        Dim oXmlDoc As New System.Xml.XmlDocument
        oXmlDoc.LoadXml(result)
        If Not oXmlDoc.DocumentElement.SelectSingleNode("/Error") Is Nothing Then
            sReturnMsg = oXmlDoc.DocumentElement.SelectSingleNode("/Error").InnerText
        Else
            If Not oXmlDoc.DocumentElement.SelectSingleNode("/isUser") Is Nothing Then
                If CBool(oXmlDoc.DocumentElement.SelectSingleNode("/isUser").InnerText) Then
                    sReturnMsg = "True"
                End If
            End If
        End If
        Return sReturnMsg
    End Function
#End Region

#Region "Printing"
    Private strReader As StringReader
    Private printFont As Drawing.Font

    Private Function RebuildReceipt(ByVal receipt As String, ByVal intWidth As Integer) As String

        Dim ctr As Integer = 1
        Dim indexofChar As String = ""
        Dim newreceipt As New StringBuilder
        Dim row As Integer = 0

        Dim blnAsterisk As Boolean = False
        ''intWidth = intWidth - 1
        For i As Integer = 0 To receipt.Length - 1
            If (i = 0) Then
                ctr = 0
                newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, ControlChars.Lf)
            End If

            If (ctr > intWidth) Then ctr = 1

            Try

                indexofChar = receipt.Substring(i, 1)
                If (ctr <= intWidth) Then

                    If (ctr = intWidth) Then
                        newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, indexofChar)
                    Else
                        newreceipt.Append(indexofChar)
                    End If
                End If

                ''Debug.WriteLine(ctr & " : " + indexofChar + " " + ControlChars.Lf + newreceipt.ToString)
                ctr = ctr + 1

            Catch ex As Exception
            End Try
        Next i

        Dim CUSTOMERCOPYLINEBREAK As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("CUSTOMERCOPYLINEBREAK").ToString)
        Dim MERCHANTCOPYLINEBREAK As Integer = CInt(System.Configuration.ConfigurationManager.AppSettings("MERCHANTCOPYLINEBREAK").ToString)

        ''rev: fixed issue # 111
        'If receipt.Contains("ACCEPT WITH SIGNATURE") = False Then
        '    If (receipt.Contains("CUSTOMER COPY")) Then
        '        For i As Integer = 1 To CUSTOMERCOPYLINEBREAK
        '            newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, ControlChars.Lf)
        '        Next

        '    Else
        '        If ((receipt.Contains("MERCHANT COPY") Or receipt.Contains("DUPLICATE RECEIPT"))) Then
        '            For i As Integer = 1 To MERCHANTCOPYLINEBREAK
        '                newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, ControlChars.Lf)
        '            Next
        '        End If
        '    End If
        'Else

        '    If (receipt.Contains("MERCHANT COPY")) Then
        '        For i As Integer = 1 To CUSTOMERCOPYLINEBREAK
        '            newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, ControlChars.Lf)
        '        Next

        '    Else
        '        If (receipt.Contains("CUSTOMER COPY")) Then
        '            For i As Integer = 1 To MERCHANTCOPYLINEBREAK
        '                newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, ControlChars.Lf)
        '            Next
        '        End If
        '    End If

        'End If
        For i As Integer = 1 To CUSTOMERCOPYLINEBREAK + MERCHANTCOPYLINEBREAK
            newreceipt.AppendFormat("{0}{1}", ControlChars.Lf, ControlChars.Lf)
        Next

        newreceipt.Append(ChrW(27) + "d" + ChrW(0))
        Return newreceipt.ToString
    End Function

    Private receipttext As String
    Private useOrigDPSReceipt As Boolean
    Private addedtocounter As Integer

    Public Function PrintReceiptJS(ByVal data As String, ByVal intWidth As Integer) As String
        Return RebuildReceipt(data, intWidth)
    End Function

#Region "UNUSED"
    Public Function PrintReceipt(ByVal data As String, ByVal intWidth As Integer, _
                            Optional ByVal intSize As Integer = 8, _
                            Optional ByVal useDPSReceipt As Boolean = True, _
                            Optional ByVal printername As String = "") As String

        Try
            addedtocounter = 0
            ''strReader = New StringReader(data)
            useOrigDPSReceipt = useDPSReceipt
            Dim tempRawLine As String = RebuildReceipt(data, intWidth)
            ''Dim tempRawLineXXX As String = FormatHeaderAndLabel(tempRawLine)
            ''tempRawLine = FormatHeaderAndLabel(tempRawLine)
            strReader = New StringReader(tempRawLine)

            printFont = New Drawing.Font("Arial", intSize)

            Dim pd As New PrintDocument
            If Not String.IsNullOrEmpty(printername) Then
                pd.PrinterSettings.PrinterName = printername
                AddHandler pd.PrintPage, AddressOf Me.pd_PrintPage
                pd.Print()

            Else

                AddHandler pd.PrintPage, AddressOf Me.pd_PrintPage
                pd.Print()
            End If



        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: PrintReceipt", ex.Message & vbCrLf & ex.StackTrace)
            Return "ERROR:" & ex.Message
        End Try

    End Function

    Private Function FormatHeaderAndLabel(ByVal data As String) As String
        If String.IsNullOrEmpty(data) Then Return ""
        Dim asterisk As String() = data.Split("*")

        ''Dim tempLine As String() = asterisk(2).Replace(ControlChars.Lf, " ").Split(" ")
        Dim tempLine As String() = asterisk(2).Split(" ")
        If (tempLine.Length = -1) Then Return data

        Dim sb As New StringBuilder
        sb.Append(asterisk(0))
        sb.Append("*" + asterisk(1) + "*")

        Dim label As String = ""
        Dim found As Boolean = False
        Dim lenBase As Integer = "AUTHORIZATION".Length - 1
        Dim lenWord As Integer = 0

        Dim hasCR As Boolean = False
        Dim labelFirst As String = ""
        Dim labelSecond As String = ""

        For i As Integer = 0 To tempLine.Length - 1
            label = tempLine(i)
            If (label = "") Then Continue For

            If (label.Contains(ControlChars.Lf)) Then
                If (label.IndexOf(ControlChars.Lf) > 1) Then
                    labelFirst = label.Split(ControlChars.Lf)(0)
                    labelSecond = label.Split(ControlChars.Lf)(1)
                    label = labelFirst
                    hasCR = True
                End If
            Else
                hasCR = False
            End If

            lenWord = label.Length - 1
            If (lenWord < lenBase) Then
                lenWord = (lenBase - lenWord)
            End If

            Select Case label
                Case "TRAN", "ACCT", "EXP"
                    label = ControlChars.Lf & label & Space(lenWord) & ": "
                    found = True

                Case Else
                    found = False
            End Select

            If (i = 0 And Not found) Then
                sb.AppendFormat("{0}", label & Space(lenWord) & " : ")
            Else
                If (found) Then
                    sb.AppendFormat("{0}", label)
                Else
                    sb.AppendFormat("{0} ", label)
                End If
            End If

            If hasCR Then
                sb.AppendFormat("{0}", ControlChars.Lf & labelSecond & Space(lenWord) & " : ")
            End If
            Debug.WriteLine(sb.ToString)

        Next
        sb.Append("*" + asterisk(3) + "*")
        sb.Append(asterisk(4))
        Return sb.ToString
    End Function

    Private Sub pd_PrintPage(ByVal sender As Object, ByVal ev As PrintPageEventArgs)
        Dim linesPerPage As Single = 0
        Dim yPos As Single = 0
        Dim count As Integer = 0
        Dim leftMargin As Single = ev.MarginBounds.Left / 4
        Dim topMargin As Single = ev.MarginBounds.Top / 5
        Dim line As String = Nothing

        ' Calculate the number of lines per page.
        linesPerPage = ev.MarginBounds.Height / printFont.GetHeight(ev.Graphics)

        Dim blnAddOne As Boolean = False

        ' Print each line of the file.
        Dim tempLineSpaces() As String = Nothing

        While count < linesPerPage
            line = strReader.ReadLine()
            If line Is Nothing Then
                Exit While
            End If

            yPos = topMargin + count * printFont.GetHeight(ev.Graphics)
            ev.Graphics.DrawString(line, printFont, Brushes.Black, leftMargin, yPos, New Drawing.StringFormat())
            count += 1
        End While

        ' If more lines exist, print another page.
        If Not (line Is Nothing) Then
            ev.HasMorePages = True
        Else
            ev.HasMorePages = False
        End If
    End Sub
#End Region


    Public Function PrintIt(ByVal username As String, ByVal data As String, ByVal intwidth As Integer) As String
        Dim Resultprinter As Object = Nothing
        Try

            Resultprinter = Aurora.Common.Data.ExecuteScalarSP("GetUserNameAndPrinter", username)
            If (Not Resultprinter Is Nothing) Then
                PrinterSelection.PrintRaw(CType(Resultprinter, String), RebuildReceipt(data, intwidth))
            Else
                Throw New Exception("No printer assigned to this user!")
            End If

        Catch ex As Exception
            Return "ERROR " + ex.Message + " --> " + CType(Resultprinter, String) + " , " + username
        End Try
        Return ""
    End Function

    Public Function GetPrinterNameAndAssignToJS(ByVal username As String) As String
        Dim resultprinter As String = ""
        Try
            resultprinter = Aurora.Common.Data.ExecuteScalarSP("GetUserNameAndPrinter", username)
            If String.IsNullOrEmpty(resultprinter) Then Return ""

        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: GetPrinterNameAndAssignToJS", ex.Message & vbCrLf & ex.StackTrace)
            Return ""
        End Try

        Return resultprinter
    End Function

    Public Function DisplayNetworkPrinter(ByVal servername As String)
        Dim printerQuery As System.Management.ManagementObjectSearcher
        Dim queryResults As System.Management.ManagementObjectCollection
        Dim onePrinter As System.Management.ManagementObject

        If (String.IsNullOrEmpty(servername)) Then
            printerQuery = New System.Management.ManagementObjectSearcher( _
                       "SELECT * FROM Win32_Printer")
        Else
            printerQuery = New System.Management.ManagementObjectSearcher( _
           "SELECT * FROM Win32_Printer where serverName='" + servername + "'")
        End If

        queryResults = printerQuery.Get()

        Dim sb As New StringBuilder
        Dim ctr As Integer = 0
        For Each onePrinter In queryResults

            ''Dim newnode As TreeNode = New TreeNode(onePrinter!name, onePrinter!name)
            ''node.ChildNodes.Add(newnode)
            sb.AppendFormat("<input id=radio_{0} type='radio' name='radioPrinter' value={1}>{2}</input>{3}", ctr, onePrinter!name, onePrinter!name, "<br/>")
            ctr = ctr + 1
        Next onePrinter
        Return sb.ToString
    End Function

    'Public Function GetPrintersAssignedToBranchAndUser(ByVal username As String) As String
    '    Dim DSprinters As New DataSet
    '    Dim sb As New StringBuilder
    '    Try
    '        Aurora.Common.Data.ExecuteDataSetSP("GetPrintersAssignedToBranchAndUser", _
    '                                                    DSprinters, _
    '                                                    username)


    '        Dim ctr As Integer = 0
    '        sb.Append("<table align='center'>")
    '        sb.Append("<tr><td width='50px'></td></tr>")
    '        For Each table As DataTable In DSprinters.Tables
    '            For Each row As DataRow In table.Rows

    '                If (table.TableName = "Table1") Then
    '                    sb.Append("<tr>")
    '                    sb.Append("<td>")
    '                    sb.AppendFormat("<input id=radio_{0} type='radio' name='radioPrinter' value={1}>{2}</input>", ctr, row("LocPrnID"), row("LocPrnPrinterName"))
    '                    sb.Append("</td>")
    '                    sb.AppendFormat("<td><input type='button' value='Test It' id='btnTestPage_{0}'    onclick='TestPrint({1})'           /></td>", ctr, """" + row("LocPrnPrinterName").ToString + """")
    '                    sb.AppendFormat("<td><input type='button' value='Assign'     id='btnAssign_{0}'   onclick='AssignPrinter({1},{2})'   /></td>", ctr, """" + username + """", """" + row("LocPrnID").ToString + """")
    '                    ctr = ctr + 1
    '                    sb.Append("</tr>")
    '                ElseIf (table.TableName = "Table") Then
    '                    sb.Append("<tr><td width='100px;'></td></tr>")
    '                    sb.Append("<tr>")
    '                    sb.AppendFormat("<td>Location: <b>{0}</b> branch</td>", row("LocationCode"))
    '                    sb.AppendFormat("<td>Printer: <b>{0}</b></td>", IIf(row("UserPrinter").ToString.Length = 0, "empty", row("UserPrinter")))
    '                    sb.Append("</tr>")
    '                End If
    '            Next
    '        Next
    '        sb.Append("<tr><td></td></tr>")
    '        sb.AppendFormat("<tr><td></td><td></td><td>{0}</td></tr>", "<a href='#' id='lnkclose' onclick='setTimeout($.unblockUI,1);'>&nbsp;Close</a>")
    '        sb.Append("</table>")
    '    Catch ex As Exception
    '        Return "ERROR: " + ex.Message
    '    End Try
    '    Return sb.ToString
    'End Function

    Public Function GetPrintersAssignedToBranchAndUser(ByVal username As String) As DataSet
        Dim DSprinters As New DataSet
        Dim sb As New StringBuilder
        Try
            Aurora.Common.Data.ExecuteDataSetSP("GetPrintersAssignedToBranchAndUser", _
                                                        DSprinters, _
                                                        username)


        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: GetPrintersAssignedToBranchAndUser", ex.Message & vbCrLf & ex.StackTrace)
            Return Nothing
        End Try
        Return DSprinters
    End Function

    Public Function SetPrintersAssignedToBranchAndUser(ByVal username As String, ByVal locPrinterID As Integer) As String

        Try
            Dim Result As Object = Aurora.Common.Data.ExecuteScalarSP("SetPrintersAssignedToBranchAndUser", username, locPrinterID)
        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: SetPrintersAssignedToBranchAndUser", ex.Message & vbCrLf & ex.StackTrace)
            Return "ERROR:" + ex.Message
        End Try
        Return "Printer map to your profile"
    End Function

#End Region


#Region "rev:mia Sept. 19 2012 - adding currency conversion"
    Private Shared Function getLocalCurrency(ByVal basecurrency As String, ByVal loccurrency As String, ByVal nAmount As Decimal) As Decimal
        Dim xmlTemp As New XmlDocument
        Try
            xmlTemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("ADM_CurrencyConverter", nAmount, basecurrency, basecurrency, loccurrency)
            If xmlTemp.ChildNodes(0).ChildNodes.Count = 0 Then
                Return -1
            End If
            Return CDec(xmlTemp.SelectSingleNode("//Root/ConRate").InnerText)
        Catch ex As Exception
            Return -1
        Finally
            xmlTemp = Nothing
        End Try
    End Function
#End Region
    

End Class

Public Class PrinterSelection
    ' ----- Define the data type that supplies basic
    '       print job information to the spooler.
    <StructLayout(LayoutKind.Sequential, _
        CharSet:=CharSet.Unicode)> _
    Public Structure DOCINFO
        <MarshalAs(UnmanagedType.LPWStr)> _
        Public pDocName As String
        <MarshalAs(UnmanagedType.LPWStr)> _
        Public pOutputFile As String
        <MarshalAs(UnmanagedType.LPWStr)> _
        Public pDataType As String
    End Structure

    ' ----- Define interfaces to the functions supplied
    '       in the DLL.
    <DllImport("winspool.drv", EntryPoint:="OpenPrinterW", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function OpenPrinter( _
       ByVal printerName As String, ByRef hPrinter As IntPtr, _
       ByVal printerDefaults As Integer) As Boolean
    End Function

    <DllImport("winspool.drv", EntryPoint:="ClosePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function ClosePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
    End Function

    <DllImport("winspool.drv", EntryPoint:="StartDocPrinterW", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function StartDocPrinter( _
       ByVal hPrinter As IntPtr, ByVal level As Integer, _
       ByRef documentInfo As DOCINFO) As Boolean
    End Function

    <DllImport("winspool.drv", EntryPoint:="EndDocPrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function EndDocPrinter( _
       ByVal hPrinter As IntPtr) As Boolean
    End Function

    <DllImport("winspool.drv", EntryPoint:="StartPagePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function StartPagePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
    End Function

    <DllImport("winspool.drv", EntryPoint:="EndPagePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function EndPagePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
    End Function

    <DllImport("winspool.drv", EntryPoint:="WritePrinter", _
       SetLastError:=True, CharSet:=CharSet.Unicode, _
       ExactSpelling:=True, _
       CallingConvention:=CallingConvention.StdCall)> _
    Public Shared Function WritePrinter( _
       ByVal hPrinter As IntPtr, ByVal buffer As IntPtr, _
       ByVal bufferLength As Integer, _
       ByRef bytesWritten As Integer) As Boolean
    End Function

    Public Shared Function PrintRaw( _
          ByVal printerName As String, _
          ByVal origString As String) As Boolean
        Dim hPrinter As IntPtr
        Dim spoolData As New DOCINFO
        Dim dataToSend As IntPtr
        Dim dataSize As Integer
        Dim bytesWritten As Integer

        ' ----- The internal format of a .NET String is just
        '       different enough from what the printer expects
        '       that there will be a problem if we send it
        '       directly. Convert it to ANSI format before
        '       sending.
        dataSize = origString.Length()
        dataToSend = Marshal.StringToCoTaskMemAnsi(origString)

        ' ----- Prepare information for the spooler.
        spoolData.pDocName = "EFTPOS Receipt"
        spoolData.pDataType = "RAW"

        Try
            Call OpenPrinter(printerName, hPrinter, 0)

            ' ----- Start a new document and Section 1.1.
            Call StartDocPrinter(hPrinter, 1, spoolData)
            Call StartPagePrinter(hPrinter)

            ' ----- Send the data to the printer.
            Call WritePrinter(hPrinter, dataToSend, _
               dataSize, bytesWritten)

            ' ----- Close everything that we opened.
            EndPagePrinter(hPrinter)
            EndDocPrinter(hPrinter)
            ClosePrinter(hPrinter)
        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: EFTPOS: PrintRaw", ex.Message & vbCrLf & ex.StackTrace)
        Finally
            ' ----- Get rid of the special ANSI version.
            Marshal.FreeCoTaskMem(dataToSend)
        End Try
    End Function


    Public Shared Function CheckPrinter(ByVal USERNAME As String) As Boolean

        Dim online As Boolean = False
        Try
            Dim Resultprinter As String = Aurora.Common.Data.ExecuteScalarSP("GetUserNameAndPrinter", USERNAME)
            Dim printDocument As New PrintDocument()
            printDocument.PrinterSettings.PrinterName = Resultprinter
            online = printDocument.PrinterSettings.IsValid
        Catch
            online = False
        End Try
        Return online
    End Function


   

End Class
