Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookingHistory

    Public Shared Function GetHistory(ByVal RentalId As String) As DataTable
        '        Return DataRepository.GetHistory(rentalId)

        Dim oXmlFromDB As XmlDocument
        Dim dtHistory As New DataTable("History")
        dtHistory.Columns.Add("RnhId")
        dtHistory.Columns.Add("ChgType")
        dtHistory.Columns.Add("UsrName")
        dtHistory.Columns.Add("LocCode")
        dtHistory.Columns.Add("Dt")
        dtHistory.Columns.Add("prdname")
        dtHistory.Columns.Add("UntNum")
        dtHistory.Columns.Add("RntSt")
        dtHistory.Columns.Add("Cko")
        dtHistory.Columns.Add("Cki")
        dtHistory.Columns.Add("AgnName")
        dtHistory.Columns.Add("PkgCode")
        dtHistory.Columns.Add("HirePd")
        dtHistory.Columns.Add("VhRate")
        dtHistory.Columns.Add("GrossAmt")
        dtHistory.Columns.Add("GrossOver")
        dtHistory.Columns.Add("AgnDesc")
        dtHistory.Columns.Add("AgnDescOver")
        dtHistory.Columns.Add("ToTow")
        dtHistory.Columns.Add("GST")
        dtHistory.Columns.Add("TotPaid")
        dtHistory.Columns.Add("Sec")
        dtHistory.Columns.Add("Curr")
        dtHistory.Columns.Add("Exist")

        dtHistory.Columns.Add("Bph_ChEv")
        dtHistory.Columns.Add("Bph_BphPrd")
        dtHistory.Columns.Add("Bph_Qty")
        dtHistory.Columns.Add("Bph_UOM")
        dtHistory.Columns.Add("Bph_Rate")




        oXmlFromDB = DataRepository.GetHistory(RentalId)

        With oXmlFromDB.DocumentElement
            For Each oRentalHistoryNode As XmlNode In .ChildNodes


                Dim drHistory As DataRow = dtHistory.NewRow()
                Dim bBphStuffSet As Boolean = False

                For Each oNode As XmlNode In oRentalHistoryNode.ChildNodes
                    If oNode.Name <> "Bph" Then
                        'other nodes
                        drHistory.Item(oNode.Name) = oNode.InnerText
                    Else
                        'the #@!$&*% Bph node!
                        If oNode.ChildNodes.Count > 0 Then
                            For Each oBphSubNode As XmlNode In oNode
                                drHistory.Item("Bph_" & oBphSubNode.Name) += oBphSubNode.InnerText & "#"
                            Next
                            bBphStuffSet = True
                        End If

                    End If

                Next '  For Each oNode As XmlNode In oRentalHistoryNode.ChildNodes

                If Not bBphStuffSet Then
                    'add blanks here
                    drHistory.Item("Bph_ChEv") = ""
                    drHistory.Item("Bph_BphPrd") = ""
                    drHistory.Item("Bph_Qty") = ""
                    drHistory.Item("Bph_UOM") = ""
                    drHistory.Item("Bph_Rate") = ""

                End If

                dtHistory.Rows.Add(drHistory)

            Next 'For Each oRentalHistoryNode As XmlNode In .ChildNodes
        End With


        Return dtHistory

    End Function

End Class
