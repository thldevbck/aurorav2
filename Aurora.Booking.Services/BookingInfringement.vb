Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookingInfringement

    Public Shared Function GetInfringements(ByVal isAll As Boolean, ByVal bookingId As String, ByVal rentalId As String) As DataTable
        Dim ds As DataSet
        ds = Data.DataRepository.GetInfringements(isAll, bookingId, rentalId)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 1 Then
            dt = ds.Tables(1)
        End If
        Return dt
    End Function

    Public Shared Function GetNotified() As DataTable
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetCodecodetype("11", "")
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function

    Public Shared Function GetInfringement(ByVal icdId As String, ByVal bookingId As String, _
        ByVal rentalId As String, ByVal userCode As String) As XmlDocument

        Return Data.DataRepository.GetInfringement(icdId, bookingId, rentalId, userCode)

    End Function

    Public Shared Function GetDataFromRentalId(ByVal rentalId As String, ByVal icdId As String, _
     ByVal id As String, ByVal type As String) As DataTable

        Dim ds As DataSet
        ds = Data.DataRepository.GetDataFromRentalId(rentalId, icdId, id, type)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function

    Public Shared Function ManageInfringement(ByVal xmlDoc As XmlDocument) As String

        Dim message As String = ""
        message = Data.DataRepository.ManageInfringement(xmlDoc)
        message = message.Replace("<data>", "")
        message = message.Replace("</data>", "")
        Return message

    End Function

End Class
