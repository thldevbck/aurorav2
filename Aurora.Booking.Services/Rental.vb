''rev:mia july 4 2012 - start-- addition of 3rd party field

Imports Aurora.Booking.Data
Imports Aurora.Common


Public Class Rental

    Private Shared mPhysicalAddressCodeId As String = ""
    Private Shared mSyncRoot As Object = New Object()

    Public Shared Function GetForModifyRental(ByVal bookingId As String, ByVal rentalId As String, ByVal userCode As String) As DataTable
        Dim ds As DataSet
        ds = Data.DataRepository.GetForModifyRental(bookingId, rentalId, userCode)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function

    Public Shared Function ManageRental(ByVal rentalId As String, ByVal arrRef As String, ByVal arrDate As DateTime, _
       ByVal deptRef As String, ByVal deptDate As DateTime, ByVal noOfAdult As Integer, ByVal noOfChildren As Integer, _
       ByVal noOfInfants As Integer, ByVal vipRental As Boolean, ByVal refCustomer As Boolean, ByVal partOfConvoy As Boolean, _
       ByVal agentRef As String, ByVal agentVoucherNo As String, ByVal bookedDate As DateTime, ByVal followUpDate As DateTime, _
       ByVal rentalSource As String, ByVal knockBack As String, ByVal productId As String, ByVal rntIntNo As String, _
       ByVal booIntNo As String, ByVal userCode As String, ByVal programName As String, Optional ByVal thirdparty As String = "") As String

        Return ManageRental(rentalId, arrRef, arrDate, _
               deptRef, deptDate, noOfAdult, noOfChildren, _
               noOfInfants, vipRental, refCustomer, partOfConvoy, _
               agentRef, agentVoucherNo, bookedDate, followUpDate, _
               rentalSource, knockBack, productId, rntIntNo, _
               booIntNo, userCode, programName, thirdparty, _
               Nothing, Nothing, False)
    End Function

    ''rev:mia Oct 16 2014 - addition of PromoCode
    ''rev:mia 10nov2014   - slot management, addition of RntSelectedSlotId ,RntSelectedSlot   
    Public Shared Function ManageRental(ByVal rentalId As String, ByVal arrRef As String, ByVal arrDate As DateTime, _
       ByVal deptRef As String, ByVal deptDate As DateTime, ByVal noOfAdult As Integer, ByVal noOfChildren As Integer, _
       ByVal noOfInfants As Integer, ByVal vipRental As Boolean, ByVal refCustomer As Boolean, ByVal partOfConvoy As Boolean, _
       ByVal agentRef As String, ByVal agentVoucherNo As String, ByVal bookedDate As DateTime, ByVal followUpDate As DateTime, _
       ByVal rentalSource As String, ByVal knockBack As String, ByVal productId As String, ByVal rntIntNo As String, _
       ByVal booIntNo As String, ByVal userCode As String, ByVal programName As String, ByVal thirdparty As String, _
       ByVal estimatedArrivalDateTime As Nullable(Of DateTime), _
       ByVal estimatedDepartureTime As Nullable(Of DateTime), _
       Optional ByVal PromoCode As String = "", _
       Optional ByVal RntSelectedSlotId As String = "", _
       Optional ByVal RntSelectedSlot As String = "") As String

        Return ManageRental(rentalId, arrRef, arrDate, _
               deptRef, deptDate, noOfAdult, noOfChildren, _
               noOfInfants, vipRental, refCustomer, partOfConvoy, _
               agentRef, agentVoucherNo, bookedDate, followUpDate, _
               rentalSource, knockBack, productId, rntIntNo, _
               booIntNo, userCode, programName, thirdparty, _
               estimatedArrivalDateTime, estimatedDepartureTime, True, PromoCode, RntSelectedSlotId, RntSelectedSlot)
    End Function

    ''rev:mia july 4 2012 - start-- addition of 3rd party field
    ''rev:mia Oct 16 2014 - addition of PromoCode
    ''rev:mia 10nov2014   - slot management, addition of RntSelectedSlotId ,RntSelectedSlot   
    Private Shared Function ManageRental(ByVal rentalId As String, ByVal arrRef As String, ByVal arrDate As DateTime, _
       ByVal deptRef As String, ByVal deptDate As DateTime, ByVal noOfAdult As Integer, ByVal noOfChildren As Integer, _
       ByVal noOfInfants As Integer, ByVal vipRental As Boolean, ByVal refCustomer As Boolean, ByVal partOfConvoy As Boolean, _
       ByVal agentRef As String, ByVal agentVoucherNo As String, ByVal bookedDate As DateTime, ByVal followUpDate As DateTime, _
       ByVal rentalSource As String, ByVal knockBack As String, ByVal productId As String, ByVal rntIntNo As String, _
       ByVal booIntNo As String, ByVal userCode As String, ByVal programName As String, ByVal thirdparty As String, _
       ByVal estimatedArrivalDateTime As Nullable(Of DateTime), _
       ByVal estimatedDepartureTime As Nullable(Of DateTime), _
       ByVal canUpdateArrivalDepartureTime As Boolean, _
       Optional ByVal PromoCode As String = "", _
       Optional ByVal RntSelectedSlotId As String = "", _
       Optional ByVal RntSelectedSlot As String = "") As String

        Dim passIntegrityCheck As Boolean
        passIntegrityCheck = Booking.CheckIntegrity("", Utility.ParseInt(rntIntNo), Utility.ParseInt(booIntNo), rentalId, "")

        If passIntegrityCheck Then
            Dim dt As DataTable = New DataTable

            dt = Data.DataRepository.ManageRental(rentalId, arrRef, arrDate, deptRef, deptDate, _
              noOfAdult, noOfChildren, noOfInfants, vipRental, refCustomer, partOfConvoy, agentRef, agentVoucherNo, _
              bookedDate, followUpDate, rentalSource, knockBack, productId, userCode, programName, thirdparty, _
              estimatedArrivalDateTime, estimatedDepartureTime, canUpdateArrivalDepartureTime, _
              PromoCode, RntSelectedSlotId, RntSelectedSlot)

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0)(0)
            Else
                Return ""
            End If
        Else
            Return "This rental has been modified by another user. Please refresh the screen."
        End If

        Return ""

    End Function


    Public Shared Function RntGetFollowUpDate(ByVal ckoDate As Date, ByVal ckoLocationCode As String, _
        ByVal status As String) As String

        'Dim followupDateString As String
        'followupDateString = Aurora.Booking.Data.DataRepository.RntGetFollowUpDate(ckoDate, ckoLocationCode, status)
        'Return Utility.ParseDateTime(followupDateString)

        Return Aurora.Booking.Data.DataRepository.RntGetFollowUpDate(ckoDate, ckoLocationCode, status)

    End Function

    Public Shared Function ManageArrivalDepartureDetails(ByVal id As Nullable(Of Long), ByVal rentalId As String,
        ByVal type As ArrivalDepartureType, ByVal pickupDate As Nullable(Of DateTime), ByVal venueCodeId As String, ByVal venueName As String, _
        ByVal specialInstructions As String, ByVal integrityNumber As Integer, ByVal noteIntegrityNumber As Integer, _
        ByVal userCode As String, ByVal programName As String, _
        ByVal street As String, ByVal suburb As String, ByVal city As String, _
        ByVal postCode As String, ByVal state As String, ByVal countryCode As String, _
        ByVal adddressIntegrity As Integer) As String

        Dim typeText As String

        If type = ArrivalDepartureType.Arrival Then
            typeText = "arrival"
        Else
            typeText = "departure"
        End If

        Return Aurora.Booking.Data.DataRepository.UpdateArrivalDepartureDetails(id, rentalId, _
            typeText, pickupDate, venueCodeId, venueName, _
            specialInstructions, integrityNumber, noteIntegrityNumber, _
            userCode, programName, _
            street, suburb, city, _
            postCode, state, countryCode, _
            adddressIntegrity)
    End Function

    Public Shared Function GetVenueTypes() As DataTable
        Return Aurora.Booking.Data.DataRepository.GetVenueTypes()
    End Function

    Public Shared Function GetArrivalDepartureDetails(ByVal rentalId As String) As DataTable
        Dim resultTable As DataTable = New DataTable
        Aurora.Common.Data.ExecuteTableSP("sp_getArrivalDepartureDetails", resultTable, rentalId)

        Return resultTable
    End Function

    Public Shared Function GetPhysicalAddressCodeId() As String
        If String.IsNullOrEmpty(mPhysicalAddressCodeId) Then
            SyncLock mSyncRoot
                If String.IsNullOrEmpty(mPhysicalAddressCodeId) Then
                    mPhysicalAddressCodeId = DataRepository.GetPhysicalAddressCodeId()
                End If
            End SyncLock
        End If
        Return mPhysicalAddressCodeId
    End Function

    Public Enum ArrivalDepartureType
        Arrival
        Departure
    End Enum


#Region "REV:10NOV2014 - SLOT MANAGEMENT"

    Public Shared Function GetAvailableSlot(sRntId As String, sAvpId As String, sBpdId As String, Optional IsSCI As String = "0") As DataSet
        Return DataRepository.GetAvailableSlot(sRntId, sAvpId, sBpdId, IsSCI)
    End Function

    Public Shared Function SaveRentalSlot(sRntId As String, islotId As Integer, sSlotDesc As String) As Boolean
        Try
            DataRepository.SaveRentalSlot(sRntId, islotId, sSlotDesc)
        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "rev:mia 16jan2015-addition of supervisor override password and slot validation"
    Public Shared Function GetAndValidateAvailableSlot(sRntId As String, _
                                                       sAvpId As String, _
                                                       sBpdId As String, _
                                                       IsSCI As String, _
                                                       newPickupDate As DateTime, _
                                                       newPickupLocCode As String, _
                                                       newAgnId As String, _
                                                       newVehicleSapId As String, _
                                                       newPkgId As String, _
                                                       newRntStatus As String) As DataSet

        Return DataRepository.GetAndValidateAvailableSlot(sRntId, _
                                                          sAvpId, _
                                                          sBpdId, _
                                                          IsSCI, _
                                                          newPickupDate, _
                                                          newPickupLocCode, _
                                                          newAgnId, _
                                                          newVehicleSapId, _
                                                          newPkgId, _
                                                          newRntStatus)
    End Function
#End Region

End Class
