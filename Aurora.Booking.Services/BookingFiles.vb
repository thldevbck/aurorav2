Imports System.Xml
Imports Aurora.Common
Imports Aurora.Booking.Data
Imports System.IO

Public Class BookingFiles
    Public Shared Function GetFileTypes() As DataView
        Dim xml As XmlDocument
        Dim fileTypes As New DataTable("FileTypes")
        fileTypes.Columns.Add("CodCode")
        fileTypes.Columns.Add("CodId")
        xml = DataRepository.GetFileTypes()

        With xml.DocumentElement
            For Each xmlNode As XmlNode In .ChildNodes
                Dim fileType As DataRow = fileTypes.NewRow()

                For Each subNode As XmlNode In xmlNode.ChildNodes
                    fileType.Item(subNode.Name) = subNode.InnerText
                Next
                fileTypes.Rows.Add(fileType)
            Next
        End With

        Return New DataView(fileTypes)
    End Function

    Public Shared Function GetFileTypesAsArray() As BookingFileType()
        Dim bookingFileTypes As List(Of BookingFileType) = New List(Of BookingFileType)
        Dim codid As String = "CodId"
        Dim codcode As String = "CodCode"
        Dim fileTypes As DataView = GetFileTypes()

        For Each fileType As DataRowView In fileTypes
            bookingFileTypes.Add(New BookingFileType(fileType.Item(codcode), fileType.Item(codid)))
        Next
        Return bookingFileTypes.ToArray()
    End Function
    Public Shared Function GetFiles(ByVal rentalId As String, ByVal serverPath As String) As DataView
        Dim xml As XmlDocument
        Dim files As New DataTable("Files")
        files.Columns.Add("RntFileId")
        files.Columns.Add("RntId")
        files.Columns.Add("RntType")
        files.Columns.Add("RntDescription")
        files.Columns.Add("RntFileName")
        files.Columns.Add("RntFilePath")
        files.Columns.Add("AddUsrId")
        files.Columns.Add("AddDateTime")
        files.Columns.Add("FormatedAddDateTime")
        files.Columns.Add("LinkPath")

        xml = DataRepository.GetFiles(rentalId)

        With xml.DocumentElement
            For Each xmlNode As XmlNode In .ChildNodes
                Dim file As DataRow = files.NewRow()

                For Each subNode As XmlNode In xmlNode.ChildNodes
                    file.Item(subNode.Name) = subNode.InnerText
                Next

                Dim dateAdded As Date = Date.Parse(file.Item("AddDateTime"))
                file.Item("FormatedAddDateTime") = dateAdded.ToString("G")
                file.Item("LinkPath") = GetPathForFile(file.Item("RntFileName"), file.Item("RntFilePath"), serverPath)
                files.Rows.Add(file)
            Next
        End With

        Return New DataView(files)
    End Function

    Private Shared Function GetPathForFile(ByVal fileName As String, ByVal filePath As String, ByVal serverPath As String) As String
        Dim finalDirectory As String = GetPathForFile(filePath, serverPath)
        Return GetPathForFile(fileName, finalDirectory)
    End Function

    Private Shared Function GetPathForFile(ByVal fileName As String, ByVal filePath As String) As String
        Return IO.Path.Combine(filePath, fileName)
    End Function

    Private Shared Function GetValidFilePath(ByVal originalFileName As String, ByVal counter As Integer, ByVal rentalId As String, ByVal basePath As String, ByVal serverPath As String) As String
        Dim fileName As String = rentalId + "_" + Path.GetFileNameWithoutExtension(originalFileName) + "_" + counter.ToString() + IO.Path.GetExtension(originalFileName)
        Return GetFilePath(fileName, basePath, serverPath)
    End Function

    Private Shared Function GetValidFilePath(ByVal originalFileName As String, ByVal rentalId As String, ByVal basePath As String, ByVal serverPath As String) As String
        Dim fileName As String = rentalId + "_" + Path.GetFileNameWithoutExtension(originalFileName) + Path.GetExtension(originalFileName)
        Return GetFilePath(fileName, basePath, serverPath)
    End Function

    Private Shared Function GetFilePath(ByVal fileName As String, ByVal basePath As String, ByVal serverPath As String) As String
        Dim finalDirectory As String = Path.Combine(serverPath, basePath)
        Return GetPathForFile(fileName, finalDirectory)
    End Function

    Public Shared Function SaveNewFile(ByVal rentalId As String, ByVal fileType As String, ByVal description As String, _
                                    ByVal fileName As String, ByVal filePath As String, _
                                    ByVal userName As String, ByVal addProgramName As String) As String
        Try

            Return DataRepository.SaveFile(rentalId, fileType, description, Path.GetFileName(fileName), filePath, 0, userName, addProgramName)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ""
        End Try
    End Function

    Public Shared Function GetNewFilePath(ByVal rentalId As String, ByVal fileName As String, ByVal filePath As String, ByVal serverPath As String) As String
        Try
            Dim counter As Integer
            Dim originalFileName As String = fileName
            Dim fullPath As String = GetValidFilePath(originalFileName, rentalId, filePath, serverPath)

            While File.Exists(fullPath)
                counter = counter + 1
                fullPath = GetValidFilePath(originalFileName, counter, rentalId, filePath, serverPath)
            End While

            Return fullPath
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ""
        End Try
    End Function

    Public Shared Function DeleteFile(ByVal rentalFileId As Integer, ByVal fileName As String, ByVal filePath As String, ByVal serverPath As String) As String
        Try
            '            Dim pathForFile As String = GetPathForFile(fileName, filePath, serverPath)
            '            File.Delete(pathForFile)
            Return DataRepository.DeleteFile(rentalFileId)
        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            Return ""
        End Try
    End Function
End Class

Public Class BookingFileType
    Public Property Id As String
    Public Property Name As String

    Public Sub New()
        Me.Name = ""
        Me.Id = ""
    End Sub

    Public Sub New(ByVal name As String, ByVal id As String)
        Me.Name = name
        Me.Id = id
    End Sub
End Class