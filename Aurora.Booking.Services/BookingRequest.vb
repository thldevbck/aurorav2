Imports System.Text
Imports System.Data
Imports System.Data.Common
Imports Aurora.Common.data
Imports Aurora.Common



Public Class BookingRequest

#Region " Properties"

    Private Shared mreturnError As String = ""
    Private Shared mreturnWarning As String = ""
    Private Shared mreturnXML As String = ""

    Public Shared Property ReturnError() As String
        Get
            Return mreturnError
        End Get
        Set(ByVal value As String)
            mreturnError = value
        End Set
    End Property

    Public Shared Property ReturnWarning() As String
        Get
            Return mreturnWarning
        End Get
        Set(ByVal value As String)
            mreturnWarning = value
        End Set
    End Property

    Public Shared Property returnXML() As String
        Get
            Return mreturnXML
        End Get
        Set(ByVal value As String)
            mreturnXML = value
        End Set
    End Property

#End Region
    


    Public Shared Function ManageBookingRequest(ByVal xmlString As String) As String

        Try

        
            Dim params(3) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("xmlRequestIn", DbType.String, xmlString, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("ReturnError", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
            params(2) = New Aurora.Common.Data.Parameter("ReturnWarning", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)
            params(3) = New Aurora.Common.Data.Parameter("ReturnXml", DbType.String, 4000, Parameter.ParameterType.AddOutParameter)

            ''Aurora.Common.Data.ExecuteOutputSP("RES_manageBookingRequest", params)
            ''Aurora.Common.Data.ExecuteOutputSP("RES_manageBookingRequestForOslo", params)

            'POS enhancement
            Aurora.Common.Data.ExecuteOutputSP("RES_manageBookingRequestForOslo_POS", params)


            ReturnError = params(1).Value
            ReturnWarning = params(2).Value
            returnXML = params(3).Value
            Return String.Concat("<Op><E>", ReturnError, "</E><W>", ReturnWarning, "</W><D>", returnXML, "</D></Op>")

        Catch ex As Exception
            Return String.Concat("<Op><E>", ex.Message, "</E><W>", ex.Source, "</W><D>", xmlString, "</D></Op>")
        End Try
    End Function
End Class
