Imports Aurora.Common
Imports Aurora.Common.Data
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common


Public Class ChargeDetails
    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                          ByVal sErrNumber As String, _
                                          ByVal sErrType As String, _
                                          ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)

        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function


    Public Shared Function manageChargeDetails(ByVal sBpdId As String, _
                                      ByVal dFromWhen As String, _
                                      ByVal dToWhen As String, _
                                      ByVal iFrom As String, _
                                      ByVal iTo As String, _
                                      ByVal sUserId As String, _
                                      ByVal sPrgName As String, _
                                      ByVal ChgDet As String, _
                                      ByVal clientMsg As String, ByRef retValue As Boolean) As String
        Dim RtnVal As String() = Nothing
        Dim strErrMsg As String
        Dim sIncentives As String



        Try


            If (clientMsg = "0") Then
                '@sBpdId			varchar(64) 	= NULL,
                '@sFromWhen		varchar(24) 	= NULL,
                '@sToWhen		varchar(24) 	= NULL,
                '@iFrom			varchar(5) 		= NULL,
                '@iTo			varchar(5) 		= NULL,
                '@sUserId		varchar(64) 	= NULL,
                '@psProgramName	varchar(64) 	= NULL,
                '@ChgDet			text 			= default

                sIncentives = Aurora.Common.Data.ExecuteScalarSP("RES_ManageOverrideAccess", sBpdId, dFromWhen, dToWhen, iFrom, iTo, sUserId, sPrgName, ChgDet)


                RtnVal = sIncentives.Split("^")

                If (RtnVal(0) = "Error" Or UCase(RtnVal(0)) = "POPUP") Then
                    If UCase(RtnVal(0)) = "POPUP" Then
                        ''ErrorLog("Called Supervaiser Override")
                    End If

                    strErrMsg = GetErrorTextFromResource(True, RtnVal(0), "Application", RtnVal(1))
                    manageChargeDetails = "<Root>" & strErrMsg & "<Data></Data><RetVal/></Root>"
                    retValue = False
                    Exit Function
                End If


                strErrMsg = GetErrorTextFromResource(False, RtnVal(0), "Application", RtnVal(2))
                manageChargeDetails = "<Root>" & strErrMsg & "<Data></Data>" & RtnVal(1) & "</Root>"
                retValue = True
                Exit Function
            End If 'If (clientMsg = "0") Then

            ' Convoy Processing This process is called when the client clicks the yes  from the screen
            If (clientMsg = "1") Then
                '@sBpdId		varchar(64) = NULL,
                '@sFromWhen	varchar(24) = NULL,
                '@sToWhen	varchar(24) = NULL,
                '@iFrom		varchar(5) = NULL,
                '@iTo		varchar(5) = NULL,
                '@sUserId	varchar(64) = NULL,
                '@psProgramName	varchar(64) = NULL,
                '@ChgDet		text = default
                sIncentives = Aurora.Common.Data.ExecuteScalarSP("RES_ManageConvoyProcessing", sBpdId, dFromWhen, dToWhen, iFrom, iTo, sUserId, sPrgName, ChgDet)
                RtnVal = Split(sIncentives, "^")

                If (RtnVal(0) = "Error") Then
                    strErrMsg = GetErrorTextFromResource(True, RtnVal(0), "Application", RtnVal(1))
                    manageChargeDetails = "<Root>" & strErrMsg & "<Data></Data><RetVal/></Root>"
                    retValue = False
                    Exit Function
                End If

                strErrMsg = GetErrorTextFromResource(False, RtnVal(0), "", "Updated Successfully")
                manageChargeDetails = "<Root>" & strErrMsg & "<Data></Data><RetVal/></Root>"
                retValue = True
                Exit Function
            End If 'If (clientMsg = "1") Then

        Catch ex As Exception
            strErrMsg = GetErrorTextFromResource(True, "Error", "System", ex.Message)
            manageChargeDetails = "<Root>" & strErrMsg & "<Data></Data></Root>"
        End Try

    End Function
End Class
