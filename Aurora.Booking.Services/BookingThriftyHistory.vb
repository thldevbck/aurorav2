'' Change Log!
'' 28.8.8 - Shoel - Created for THRIFTY HISTORY

Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookingThriftyHistory

    Public Shared Function GetThriftyHistory(ByVal RentalId As String) As DataTable

        Dim dtThriftyHistory As New DataTable("ThriftyHistory")
        
        dtThriftyHistory = Data.DataRepository.GetThriftyHistory(RentalId)

        If dtThriftyHistory.Rows.Count = 0 Then
            ' its a new table
            dtThriftyHistory.Columns.Add("Status")
            dtThriftyHistory.Columns.Add("TimeStamp")
            dtThriftyHistory.Columns.Add("CustomerName")
            dtThriftyHistory.Columns.Add("CheckedOutDateTime")
            dtThriftyHistory.Columns.Add("CheckedOutLocation")
            dtThriftyHistory.Columns.Add("CheckedInDateTime")
            dtThriftyHistory.Columns.Add("CheckedInLocation")
            dtThriftyHistory.Columns.Add("AgencyReference")
            dtThriftyHistory.Columns.Add("BoosterSeat")
            dtThriftyHistory.Columns.Add("Airport")
            dtThriftyHistory.Columns.Add("Product")
            dtThriftyHistory.Columns.Add("Package")
            dtThriftyHistory.Columns.Add("DailyRate")
            dtThriftyHistory.Columns.Add("NetValue")
            dtThriftyHistory.Columns.Add("DepositAmount")
            dtThriftyHistory.Columns.Add("TotalAmount")

            Dim drDummy As DataRow = dtThriftyHistory.NewRow()
            dtThriftyHistory.Rows.Add(drDummy)

        End If

        Return dtThriftyHistory

    End Function

    Public Shared Function IsRentalThrifty(ByVal RentalId As String, Optional ByRef ReturnError As String = "", Optional ByRef ThriftyLocType As String = "") As Boolean
        Return Data.DataRepository.IsRentalThrifty(RentalId, ReturnError, ThriftyLocType)
    End Function
End Class
