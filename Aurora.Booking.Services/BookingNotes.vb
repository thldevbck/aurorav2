Imports Aurora.Booking.Data
Imports Aurora.Common

Public Class BookingNotes

    Public Shared Function GetBookingNotes(ByVal bookingId As String, ByVal rentalId As String, ByVal isAllNote As Boolean) As DataSet
        Return DataRepository.GetBookingNotes(bookingId, rentalId, isAllNote)
    End Function

    Public Shared Function GetBookingNote(ByVal noteId As String, ByVal bookingId As String, ByVal rentalId As String) As DataSet
        Return DataRepository.GetBookingNote(noteId, bookingId, rentalId)
    End Function

    Public Shared Function ManageNote(ByVal noteId As String, ByVal bookingId As String, ByVal rentalNo As String, _
        ByVal codTypId As String, ByVal noteDesc As String, ByVal codAudTypId As String, ByVal priority As Integer, _
        ByVal isActive As Boolean, ByVal integrityNo As Integer, ByVal userName As String, ByVal addProgramName As String) As String

        Try

            Return DataRepository.ManageNote(noteId, bookingId, rentalNo, codTypId, noteDesc, _
                codAudTypId, priority, isActive, integrityNo, userName, addProgramName)

            'Return True

        Catch ex As Exception
            Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)
            'Return False
            Return ""
        End Try
    End Function

    Public Shared Function GetNoteType() As DataTable
        'Return DataRepository.GetNoteType("13", "")

        Dim ds As DataSet
        'ds = DataRepository.GetNoteType("13", "")
        ds = Aurora.Common.Data.GetCodecodetype("13", "")
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function

    Public Shared Function GetAudienceType() As DataTable
        'Return Aurora.Common.Data.GetCodecodetype("26", "")

        Dim ds As DataSet
        ds = Aurora.Common.Data.GetCodecodetype("26", "")
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function

    Public Shared Function SearchNotes(ByVal bookingId As String, ByVal rentalNoFrom As String, _
         ByVal rentalNoTo As String, ByVal type As String, ByVal addedBy As String, ByVal fromDate As DateTime, _
         ByVal toDate As DateTime, ByVal audience As String, ByVal active As String, ByVal containingText As String, _
         ByVal userCode As String) As DataSet

        Return DataRepository.SearchNotes(bookingId, rentalNoFrom, _
            rentalNoTo, type, addedBy, fromDate, toDate, audience, active, containingText, userCode)
    End Function
End Class
