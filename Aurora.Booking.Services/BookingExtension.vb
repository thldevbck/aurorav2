Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Reservations

Public Class BookingExtension

    Public Shared Function GetHirePeriod(ByVal BPDId As String, ByVal CheckOutDateTime As DateTime, ByVal CheckInDateTime As DateTime, ByVal UOMId As String)
        Return DataRepository.GetHirePeriodForExtension(BPDId, CheckOutDateTime, CheckInDateTime, UOMId)
    End Function


    Public Shared Function GetRentalForExtension(ByVal BookingId As String, ByVal RentalId As String) As XmlDocument  'DataSet
        Return DataRepository.GetRentalForExtension(BookingId, RentalId)
    End Function

    'Sub Extension()
    '    ' changes by Shoel
    '    Dim nIntegrityNo, sRntId, bIsValid
    '    ' changes by Shoel
    '    Select Case sAction
    '        Case "GetExtension"
    '            obj = server.CreateObject("AuroraFindCom.GenericFindRecord")
    '            sfunctionName = "RES_getRentalForExtension '" & param1 & "','" & param2 & "'"
    '            resRecord = obj.getInformation(sfunctionName)
    '            Response.Write(resRecord)
    '            obj = Nothing
    '        Case "GetHirePeriod"
    '            obj = server.CreateObject("AuroraFindCom.GenericFindRecord")
    '            sfunctionName = "RES_getHirePeriod '" & param4 & "','" & param1 & "','" & param2 & "','" & param3 & "'"
    '            'sfunctionName = "RES_getHirePeriod '" &  param1 & "','" & param2 & "','" & param3 & "'"
    '            resRecord = obj.getInformation(sfunctionName)
    '            Response.Write(resRecord)
    '            obj = Nothing
    '        Case "SAVEExtension"
    '            obj = server.CreateObject("AuroraChkInOutRntCan.BookedProduct")
    '            XMLdom = Server.CreateObject("MSXML2.DomDocument")
    '            XMLdom.load(request)

    '            ' pick up the parts
    '            '<Data><ThisRental><Rental><RntIntNo>
    '            ' changes by shoel
    '            If Not XMLdom.selectSingleNode("//Data/ThisRental/Rental/RntIntNo") Is Nothing Then
    '                nIntegrityNo = CInt(XMLdom.selectSingleNode("//Data/ThisRental/Rental/RntIntNo").Text)
    '            End If

    '            If Not XMLdom.selectSingleNode("//Data/ThisRental/Rental/RntId") Is Nothing Then
    '                sRntId = XMLdom.selectSingleNode("//Data/ThisRental/Rental/RntId").Text
    '            End If

    '            bIsValid = CheckRentalIntegrity(sRntId, "", nIntegrityNo)

    '            If bIsValid Then
    '                'Response.Write param1 & vbcr & param2
    '                resRecord = obj.ManageBPDExtension(XMLdom.xml, userCode, PrevPageName)
    '                Response.Write(resRecord)
    '                'Response.Write  XMLdom.xml
    '                'set Obj = nothing
    '                XMLdom = Nothing
    '            Else
    '                ' rental has changed
    '                ' exit
    '                Response.Write("<Error><Error><ErrStatus>True</ErrStatus><ErrDescription>This Rental has been modified by another user. Please refresh the screen.</ErrDescription></Error></Error>")
    '                Response.End()
    '            End If
    '            ' changes by shoel

    '    End Select
    'End Sub

    ''rev:mia 16Jan2015 - Added SlotId,SlotDescription and RentalId optionals
    Public Shared Function SaveExtension(ByVal ScreenData As XmlDocument, _
                                         ByVal OnScreenRentalIntegrityNo As Integer, _
                                         ByVal OnScreenBookingIntegrityNo As Integer, _
                                         ByVal RentalId As String, _
                                         ByVal BookingId As String, _
                                         ByVal ForceDVASSCall As Boolean, _
                                         ByVal UserId As String, _
                                         ByVal PreviousPageName As String, _
                                         Optional SlotId As String = "", _
                                         Optional SlotDescription As String = "" _
                                        ) As String

        'check integrity
        If Booking.CheckIntegrity(BookingId, OnScreenRentalIntegrityNo, OnScreenBookingIntegrityNo, RentalId) Then
            ' all good
            ' call the ManageBPDExtension
            Return ManageBPDExtension(ScreenData, ForceDVASSCall, UserId, PreviousPageName, SlotId, SlotDescription, RentalId)

        Else
            'data has changed
            Return "This rental has been modified by another user. Please refresh the screen."
        End If
    End Function

    ''rev:mia 16Jan2015 - Added SlotId,SlotDescription and RentalId optionals
    Public Shared Function ManageBPDExtension(ByVal ScreenData As XmlDocument, _
                                              ByVal ForceDVASSCall As Boolean, _
                                              ByVal sUserId As String, _
                                              ByVal sPrgmName As String, _
                                              Optional SlotId As String = "", _
                                              Optional SlotDescription As String = "", _
                                              Optional RentalId As String = "") As String

        'Variables
        Dim oCurrentRental As New XmlDocument 'For This Specific Rental, Only One Record
        Dim oOtherRentals As New XmlDocument 'For other Rentals, It may have ZERO/ONE/MORE Records


        Dim FlowTrace As New StringBuilder()

        ''   Date Variables
        Dim OldCkoDate As Date
        Dim NewCkoDate As Date
        Dim OldCkiDate As Date
        Dim NewCkiDate As Date

        Dim sBpdId As String = ""

        Dim sRetMsg As String = ""
        'variables


        Dim bCkoApplyOrgRates As Boolean
        Dim bCkiApplyOrgRates As Boolean

        Dim sCtyCode As String

        Dim sBooId As String
        Dim sRntId As String

        Dim sBookingIdCurrent, sRentalIdCurrent, sBPDIdCurrent As String

        Dim strCheckOutStatus As String


        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                '====================>> Fetch Data for XML Document  <<<=====================
                FlowTrace.Append(", OK Valid XML..Now spliting the XML Document.." & ScreenData.OuterXml)
                oCurrentRental.LoadXml(ScreenData.DocumentElement.SelectSingleNode("ThisRental").SelectSingleNode("Rental").OuterXml)

                FlowTrace.Append(", Done for This Rental...Starting Other rental....")
                oOtherRentals.LoadXml(ScreenData.DocumentElement.SelectSingleNode("OtherRental").OuterXml)

                FlowTrace.Append(", Other Rental Splited....")
                '======================>> Retrieve data For This Rental <<====================
                FlowTrace.Append(", Split Complete OK, Fetching Data from XML Document..")

                With oCurrentRental.DocumentElement

                    OldCkoDate = .SelectSingleNode("CkoDate").InnerText & " " & .SelectSingleNode("CkoTm").InnerText
                    FlowTrace.Append(", OldCkoDate - >" & OldCkoDate)

                    NewCkoDate = .SelectSingleNode("NewCkoDate").InnerText & " " & .SelectSingleNode("NewCkoTm").InnerText
                    FlowTrace.Append(", NewCkoDate - >" & NewCkoDate)

                    OldCkiDate = .SelectSingleNode("CkiDate").InnerText & " " & .SelectSingleNode("CkiTm").InnerText
                    FlowTrace.Append(", OldCkiDate - >" & OldCkiDate)

                    NewCkiDate = .SelectSingleNode("NewCkiDate").InnerText & " " & .SelectSingleNode("NewCkiTm").InnerText
                    FlowTrace.Append(", NewCkiDate - >" & NewCkiDate)

                    bCkoApplyOrgRates = IIf((.SelectSingleNode("CkoApplyPreRates").InnerText = -1), 1, .SelectSingleNode("CkoApplyPreRates").InnerText)
                    FlowTrace.Append(", bCkoApplyOrgRates - >" & bCkoApplyOrgRates)

                    bCkiApplyOrgRates = IIf((.SelectSingleNode("CkiApplyPreRates").InnerText = -1), 1, .SelectSingleNode("CkiApplyPreRates").InnerText)
                    FlowTrace.Append(", bCkiApplyOrgRates - >" & bCkiApplyOrgRates)

                    sCtyCode = .SelectSingleNode("CtyCode").InnerText
                    FlowTrace.Append(", sCtyCode - >" & sCtyCode)

                    sBooId = .SelectSingleNode("BooId").InnerText
                    sBookingIdCurrent = sBooId
                    FlowTrace.Append(", sBooId - >" & sBooId)

                    sRntId = .SelectSingleNode("RntId").InnerText
                    sRentalIdCurrent = sRntId
                    FlowTrace.Append(", sRntId - >" & sRntId)

                    strCheckOutStatus = .SelectSingleNode("CheckOutSt").InnerText
                    FlowTrace.Append(", strCheckOutStatus - >" & strCheckOutStatus)

                    FlowTrace.Append(", Input Data Validation..")

                    If NewCkoDate > OldCkoDate Then
                        Throw New Exception("This new checkout date must be prior to the original check out date")
                    End If '--If NewCkoDate > OldCkoDate Then

                    If NewCkiDate < OldCkiDate Then
                        Throw New Exception("This new check in date must be after to the original check in date")
                    End If '-- If NewCkiDate < OldCkiDate Then


                    '/** b.If Check Out date was extended, call procedure to create new BookedProduct

                    If OldCkoDate <> NewCkoDate Then

                        '----------->>> To Delete all Charge Record from Temporary Table <<<-------------------

                        If Not DataRepository.DeleteTProcessedBpds(sRntId) Then
                            Throw New Exception("Failed to delete all charge records from temporary table")
                        End If

                        '------------------------------------>>> End of Delete <<<-------------------------------------

                        FlowTrace.Append(", Check-Out Date has changed..Call ManageBookedProduct_Ext..")
                        '-- Fetch the First BPD Id


                        sBpdId = oCurrentRental.DocumentElement.SelectSingleNode("FirstBpdId").InnerText
                        sBPDIdCurrent = sBpdId

                        Dim sErrStr, sErrMsg, sStrBPDId, sRntHisTd As String
                        sErrStr = ""
                        sErrMsg = ""
                        sStrBPDId = ""
                        sRntHisTd = ""


                        Dim strReturnedError As String
                        strReturnedError = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Ext(sBpdId, NewCkoDate, OldCkiDate, bCkoApplyOrgRates, ForceDVASSCall, sBooId, sRntId, sCtyCode, strCheckOutStatus, sStrBPDId, sRntHisTd, sUserId, sPrgmName, sErrStr, sErrMsg)
                        If strReturnedError <> "" Then
                            Throw New Exception(strReturnedError)
                        End If


                        '' '' '' '' '' ''Call oBookedProduct.ManageBookedProduct_Ext( _
                        '' '' '' '' '' ''        BookedProductID:=sBpdId, ExtCkoWhen:=NewCkoDate, ExtCkiWhen:=OldCkiDate, _
                        '' '' '' '' '' ''        ApplyOriginalRates:=bCkoApplyOrgRates, strBpdBooId:=sBooId, strBpdRntId:=sRntId, _
                        '' '' '' '' '' ''        sCtyCode:=sCtyCode, strCheckOutStatus:=strCheckOutStatus, BookedProductIDNew:=sStrBPDId, _
                        '' '' '' '' '' ''        strRentalHistoryID:=sRntHisTd, UserId:=sUserId, ProgramName:=sPrgmName, _
                        '' '' '' '' '' ''        strReturnError:=sErrStr, strReturnMessage:=sErrMsg)

                        FlowTrace.Append(", Returned from ManageBookedProduct_Ext..Checking for Error>>> " & sErrStr)


                        If Not sErrStr.Trim.Equals(String.Empty) Then
                            Throw New Exception("Manage Booked Product Extension failed")
                        End If

                        If Len(sErrMsg) > 0 Then
                            sRetMsg = sRetMsg & sErrMsg
                        End If
                        FlowTrace.Append(", Checking Error Complete.. << NO ERROR FOUND >>")
                    End If  '-- If OldCkoDate <> NewCkoDate Then


                    '/**c. If Check In date was extended, call procedure to create new Booked Product ***/

                    If OldCkiDate <> NewCkiDate Then

                        '----------->>> To Delete all Charge Record from Trmporary Table <<<-------------------

                        If Not DataRepository.DeleteTProcessedBpds(sRntId) Then
                            Throw New Exception("Failed to delete all charge records from temporary table")
                        End If

                        '------------------------------------>>> End of Delete <<<-------------------------------------

                        '-- Fetch the Last BPD Id
                        FlowTrace.Append(", Check-In Data has Changed >>> Call ManageBookedProduct_Ext method..")
                        sBpdId = oCurrentRental.DocumentElement.SelectSingleNode("LastBpdId").InnerText
                        sBPDIdCurrent = sBpdId


                        Dim sErrStr As String = ""
                        Dim sErrMsg As String = ""
                        Dim sStrBPDId As String = ""
                        Dim sRntHisTd As String = ""
                        Dim strReturnedError As String

                        strReturnedError = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Ext(sBpdId, OldCkiDate, NewCkiDate, bCkiApplyOrgRates, ForceDVASSCall, sBooId, sRntId, sCtyCode, strCheckOutStatus, sStrBPDId, sRntHisTd, sUserId, sPrgmName, sErrStr, sErrMsg)
                        If strReturnedError <> "" Then
                            Throw New Exception(strReturnedError)
                        End If
                        '' '' '' '' '' ''Call oBookedProduct.ManageBookedProduct_Ext( _
                        '' '' '' '' '' ''        BookedProductID:=sBpdId, _
                        '' '' '' '' '' ''        ExtCkoWhen:=OldCkiDate, ExtCkiWhen:=NewCkiDate, _
                        '' '' '' '' '' ''        ApplyOriginalRates:=bCkiApplyOrgRates, strBpdBooId:=sBooId, strBpdRntId:=sRntId, _
                        '' '' '' '' '' ''        sCtyCode:=sCtyCode, strCheckOutStatus:=strCheckOutStatus, BookedProductIDNew:=sStrBPDId, _
                        '' '' '' '' '' ''        strRentalHistoryID:=sRntHisTd, UserId:=sUserId, ProgramName:=sPrgmName, _
                        '' '' '' '' '' ''        strReturnError:=sErrStr, strReturnMessage:=sErrMsg)


                        FlowTrace.Append(", Returned from ManageBookedProduct_Ext..Checking for Error>>> " & sErrStr)

                        If Not sErrStr.Equals(String.Empty) Then
                            Throw New Exception("Manage Booked Product Extension failed")
                        End If



                        If Len(sErrMsg) > 0 Then
                            sRetMsg = sRetMsg & sErrMsg
                        End If

                        FlowTrace.Append(", Checking Error Complete.. << NO ERROR FOUND >>")
                    End If      '-- If OldCkiDate <> NewCkiDate Then
                End With '-- With ThisRental.documentElement

                '---->> For Other Rental, move through the loop and call ManageBookedProductExtension
                FlowTrace.Append(", Checking for << OTHER RENTAL >>")

                If oOtherRentals.DocumentElement.HasChildNodes Then
                    FlowTrace.Append(", Trying to fetch Count of other Rental..")
                    'NoOfOtherRental = oOtherRentals.DocumentElement.ChildNodes.length
                    For i As Integer = 0 To oOtherRentals.DocumentElement.ChildNodes.Count - 1
                        '-- Just to check, whether this Rental has been selected for Extension or Not
                        Dim SelectRnt As Integer
                        With oOtherRentals.DocumentElement.ChildNodes(i)

                            SelectRnt = CInt(.SelectSingleNode("Cancel").InnerText)
                            SelectRnt = IIf((SelectRnt = -1), 1, SelectRnt)

                            sRntId = .SelectSingleNode("RntId").InnerText
                            sBooId = .SelectSingleNode("BooId").InnerText
                            sCtyCode = .SelectSingleNode("CtyCode").InnerText
                            strCheckOutStatus = .SelectSingleNode("CheckOutSt").InnerText

                            If SelectRnt = 1 Then
                                If OldCkoDate <> NewCkoDate Then
                                    '-- Fetch the First BPD Id
                                    sBpdId = .SelectSingleNode("FirstBpdId").InnerText
                                    'sErrStr = ""
                                    'sErrMsg = ""

                                    Dim sErrStr As String = ""
                                    Dim sErrMsg As String = ""
                                    Dim sStrBPDId As String = ""
                                    Dim sRntHisTd As String = ""

                                    Dim strReturnedError As String

                                    strReturnedError = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Ext(sBpdId, NewCkoDate, OldCkiDate, bCkoApplyOrgRates, ForceDVASSCall, sBooId, sRntId, sCtyCode, strCheckOutStatus, sStrBPDId, sRntHisTd, sUserId, sPrgmName, sErrStr, sErrMsg)
                                    If strReturnedError <> "" Then
                                        Throw New Exception(strReturnedError)
                                    End If
                                    '' '' '' '' '' ''Call oBookedProduct.ManageBookedProduct_Ext( _
                                    '' '' '' '' '' ''        BookedProductID:=sBpdId, _
                                    '' '' '' '' '' ''        ExtCkoWhen:=NewCkoDate, ExtCkiWhen:=OldCkiDate, _
                                    '' '' '' '' '' ''        ApplyOriginalRates:=bCkoApplyOrgRates, strBpdBooId:=sBooId, _
                                    '' '' '' '' '' ''        strBpdRntId:=sRntId, sCtyCode:=sCtyCode, _
                                    '' '' '' '' '' ''        strCheckOutStatus:=strCheckOutStatus, BookedProductIDNew:=sStrBPDId, _
                                    '' '' '' '' '' ''        strRentalHistoryID:=sRntHisTd, UserId:=sUserId, _
                                    '' '' '' '' '' ''        ProgramName:=sPrgmName, strReturnError:=sErrStr, _
                                    '' '' '' '' '' ''        strReturnMessage:=sErrMsg)


                                    If Not sErrStr.Equals(String.Empty) Then
                                        Throw New Exception("Manage Booked Product Extension failed for Other Rentals")
                                    End If

                                    If Len(sErrMsg) > 0 Then
                                        sRetMsg = sRetMsg & sErrMsg
                                    End If
                                End If    '-- If OldCkoDate <> NewCkoDate Then

                                If OldCkiDate <> NewCkiDate Then
                                    '-- Fetch the Last BPD Id
                                    sBpdId = .SelectSingleNode("LastBpdId").InnerText

                                    'sErrStr = ""
                                    'sErrMsg = ""

                                    Dim sErrStr As String = ""
                                    Dim sErrMsg As String = ""
                                    Dim sStrBPDId As String = ""
                                    Dim sRntHisTd As String = ""
                                    Dim strReturnedError As String = ""

                                    strReturnedError = Aurora.Reservations.Services.BookedProduct.ManageBookedProduct_Ext(sBpdId, OldCkiDate, NewCkiDate, bCkiApplyOrgRates, ForceDVASSCall, sBooId, sRntId, sCtyCode, strCheckOutStatus, sStrBPDId, sRntHisTd, sUserId, sPrgmName, sErrStr, sErrMsg)
                                    If strReturnedError <> "" Then
                                        Throw New Exception(strReturnedError)
                                    End If
                                    '' '' '' '' '' ''Call oBookedProduct.ManageBookedProduct_Ext( _
                                    '' '' '' '' '' ''        BookedProductID:=sBpdId, _
                                    '' '' '' '' '' ''        ExtCkoWhen:=OldCkiDate, ExtCkiWhen:=NewCkiDate, _
                                    '' '' '' '' '' ''        ApplyOriginalRates:=bCkiApplyOrgRates, strBpdBooId:=sBooId, _
                                    '' '' '' '' '' ''        strBpdRntId:=sRntId, sCtyCode:=sCtyCode, strCheckOutStatus:=strCheckOutStatus, _
                                    '' '' '' '' '' ''        BookedProductIDNew:=sStrBPDId, strRentalHistoryID:=sRntHisTd, _
                                    '' '' '' '' '' ''        UserId:=sUserId, ProgramName:=sPrgmName, strReturnError:=sErrStr, _
                                    '' '' '' '' '' ''        strReturnMessage:=sErrMsg)


                                    If Not sErrStr.Equals(String.Empty) Then
                                        Throw New Exception("Manage Booked Product Extension failed for Other Rentals")
                                    End If

                                    If Len(sErrMsg) > 0 Then
                                        sRetMsg = sRetMsg & sErrMsg
                                    End If
                                End If    '-- If OldCkiDate <> NewCkiDate Then
                            End If      '-- If SelectRnt = 1 Then
                        End With    '-- With OtherRental.documentElement.childNodes(I)
                    Next i          '-- For I = 0 To NoOfOtherRental - 1
                End If              '-- If OtherRental.documentElement.haschildNodes Then


                ''rev:mia 16Jan2015 - Added SlotId,SlotDescription and RentalId optionals
                If (Not String.IsNullOrEmpty(SlotId) And Not String.IsNullOrEmpty(SlotDescription)) Then
                    Aurora.Booking.Services.Rental.SaveRentalSlot(RentalId, SlotId, SlotDescription)
                End If


                'If Not DataRepository.SSTransToFinance(sBooId, sRntId, "", sBpdId) Then
                If Not DataRepository.SSTransToFinance(sBookingIdCurrent, sRentalIdCurrent, "", sBPDIdCurrent) Then
                    Throw New Exception(Format(Now, "DD MMM YY dddd HH:MM:SS") & "Error in RES_ssTransToFinance Aborting the Transaction")
                End If

                

                ' changes by shoel for integrity

                'If Not DataRepository.UpdateRentalIntegrityNumber(sRntId, sBpdId) Then
                '    Throw New Exception("Failed to update integrity number for Rental Id=" & sRntId & ", BPDId =" & sBpdId)
                'End If
                DataRepository.UpdateRentalIntegrityNumber(sRentalIdCurrent, sBPDIdCurrent)
                ' end of additions by Shoel
                ' changes by shoel for integrity- for defect#23

               


                If Not oTransaction.IsClosed Then
                    oTransaction.CommitTransaction()
                    oTransaction.Dispose()
                End If

                

                'If Len(sRetMsg) > 0 Then
                ManageBPDExtension = sRetMsg
                ' End If



            Catch ex As Exception
                '--->> If any system failure then Rollback the Transaction <<---
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                ' log the exception
                ' Logging.LogException(My.Settings.AuroraFunctionCodeAttribute, ex)

                'return the error message
                Return "ERROR/" & ex.Message
            Finally
                Try
                    Logging.LogInformation("BOOKINGEXTENSION", FlowTrace.ToString())
                Catch
                End Try
            End Try
        End Using
    End Function







    ' integrity function
    'Public Shared Function CheckRentalIntegrity(ByVal ScreenRentalIntegrityNo As Integer, _
    '                                ByVal ScreenBookingIntegrityNo As Integer, _
    '                                Optional ByVal RentalId As String = "", _
    '                                Optional ByVal BPDId As String = "") As Boolean

    '    Dim bDataIsCurrent As Boolean
    '    Dim nDataBaseRentalIntegrityNo As Integer = 0

    '    'get integrity from database
    '    nDataBaseRentalIntegrityNo = DataRepository.GetIntegrityNumber(RentalId, BPDId)

    '    'check if onscreen integrity is same as that from database
    '    If ScreenRentalIntegrityNo = nDataBaseRentalIntegrityNo Then
    '        bDataIsCurrent = True
    '    Else
    '        bDataIsCurrent = False
    '    End If

    '    'return the integrity number
    '    CheckRentalIntegrity = bDataIsCurrent

    'End Function


    'Public Shared Function CheckRentalIntegrity(ByVal RentalId As String, ByVal BPDId As String, ByVal BooID As String, ByVal ScreenRentalIntegrityNo As Integer, ByVal ScreenBookingIntegrityNo As Integer) As Boolean

    '    ' Shoel's variables
    '    Dim bDataIsCurrent 'As Boolean
    '    Dim nDataBaseRentalIntegrityNo 'As Integer
    '    Dim nDataBaseBooIntNo
    '    ' Shoel's variables

    '    bDataIsCurrent = False

    '    'call db
    '    DataRepository.GetIntegrityNumber(nDataBaseRentalIntegrityNo, nDataBaseBooIntNo, RentalId, BPDId, BooID)


    '    If nDataBaseRentalIntegrityNo = ScreenRentalIntegrityNo Then


    '        ' if passed first check
    '        ' look at the booking integrity
    '        If nDataBaseBooIntNo = ScreenBookingIntegrityNo Then
    '            bDataIsCurrent = True
    '        Else
    '            bDataIsCurrent = False
    '        End If


    '    Else


    '        'failed very first check!
    '        bDataIsCurrent = False
    '    End If



    '    CheckRentalIntegrity = bDataIsCurrent

    'End Function



    ' integrity function
End Class
