Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO
Imports System.Net.Mail
Imports System.Text
Imports System.Configuration

Public Class BookingConfirmation

    Public Shared Function GetConfirmationSummary(ByVal BookingId As String, ByVal RentalId As String, ByVal ForAllAgents As Boolean) As DataTable

        ' Dim oXml As XmlDocument
        Dim dtConfirmations As DataTable
        dtConfirmations = DataRepository.GetConfirmationSummary(BookingId, RentalId, ForAllAgents)
        Return dtConfirmations


    End Function


    Public Shared Function GetDataForConfirmation(ByVal BookingId As String, ByVal ConfirmationId As String) As XmlDocument
        Return DataRepository.GetDataForConfirmation(BookingId, ConfirmationId)
    End Function


    Public Shared Function GetConfirmationNoteSpec(ByVal CountryCode As String, ByVal ConfirmationId As String, ByVal UserCode As String) As DataTable
        Return DataRepository.GetConfirmationNoteSpec(CountryCode, ConfirmationId, UserCode)
    End Function

    Public Shared Function GetRentalCountry(ByVal RentalId) As String
        Return DataRepository.GetRentalCountry(RentalId)
    End Function


    Public Shared Function SaveUpdateConfirmation( _
                            ByVal ConfirmationId As String, _
                            ByVal BookingId As String, _
                            ByVal RentalId As String, _
                            ByVal RentalNumber As String, _
                            ByVal AudienceType As String, _
                            ByVal HeaderText As String, _
                            ByVal FooterText As String, _
                            ByVal EmailId As String, _
                            ByVal FaxNumber As String, _
                            ByVal OutputType As String, _
                            ByVal FileName As String, _
                            ByVal Highlight As Boolean, _
                            ByVal IntegrityNumber As Integer, _
                            ByVal AddUserId As String, _
                            ByVal AddModifyUserId As String, _
                            ByVal AddModifyProgramName As String, _
                            ByVal Action As String, _
                            ByVal NotesXML As XmlDocument, _
                            ByVal CkiLocation As String, _
                            ByVal CkoLocation As String)

        ''Optional ByVal CfnMultipleRentalList As String = Nothing)rev:mia may 24 2012 - will not be part of release

        Dim oReturnXML As XmlDocument = New XmlDocument

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            Try

                '54V3 M41N D3741L5
                'If Not (DataRepository.CreateUpdateConfirmations(ConfirmationId, BookingId, RentalId, RentalNumber, AudienceType, HeaderText, FooterText, EmailId, FaxNumber, OutputType, FileName, Highlight, IntegrityNumber, AddUserId, AddModifyUserId, AddModifyProgramName, Action)) Then
                '    'F41LUR3!!!
                '    Throw (New Exception("App/Failed to Update the Confirmation data"))
                'End If

                oReturnXML = DataRepository.CreateUpdateConfirmations(ConfirmationId, _
                                                                      BookingId, _
                                                                      RentalId, _
                                                                      RentalNumber, _
                                                                      AudienceType, _
                                                                      HeaderText, _
                                                                      FooterText, _
                                                                      EmailId, _
                                                                      FaxNumber, _
                                                                      OutputType, _
                                                                      FileName, _
                                                                      Highlight, _
                                                                      IntegrityNumber, _
                                                                      AddUserId, _
                                                                      AddModifyUserId, _
                                                                      AddModifyProgramName, _
                                                                      Action)

                ' check for error
                If oReturnXML.DocumentElement.SelectSingleNode("Error") IsNot Nothing Then
                    ' there's an error :'(
                    Throw New Exception(oReturnXML.DocumentElement.SelectSingleNode("Error/ErrCode").InnerText.Trim() & " - " & oReturnXML.DocumentElement.SelectSingleNode("Error/ErrDesc").InnerText.Trim())
                End If

                '54V3 TH3 N0735 1N 7H3 M3G4 L00P!
                If Not NotesXML Is Nothing Then
                    For Each xmlNode As XmlNode In NotesXML.DocumentElement.ChildNodes

                        Dim noteCfnBrand As String
                        Try
                            noteCfnBrand = xmlNode.SelectSingleNode("NoteCfnBrand").InnerText
                        Catch ex As Exception
                            noteCfnBrand = ""
                        End Try


                        oReturnXML = DataRepository.InsertDeleteNotesForConfirmation(ConfirmationId, _
                                                xmlNode.SelectSingleNode("NoteId").InnerText, _
                                                AddUserId, _
                                                AddModifyUserId, _
                                                AddModifyProgramName, _
                                                xmlNode.SelectSingleNode("Action").InnerText, _
                                                noteCfnBrand)

                    Next
                End If


                ' 1 H4V3 N0 1D34 W7F 7H15 D035! JU57 M1GR471NG :)
                ' G0NN4 C4LL cfn_update_OnlyAddressDetail 
                'Dim oReturnXML As XmlDocument
                oReturnXML = DataRepository.UpdateCkiCkoForConfirmation(ConfirmationId, CkoLocation, CkiLocation)

                ' all done
                oReturnXML.LoadXml("<Root><Message>" & Aurora.Common.Data.GetErrorMessage("GEN046") & "</Message></Root>")

                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                Select Case ex.Message.Split("/"c)(0)
                    Case "App"
                        oReturnXML.LoadXml("<Root><Error><Message>" & ex.Message.Split("/"c)(1) & "</Message><Type>App</Type></Error></Root>")
                    Case Else
                        oReturnXML.LoadXml("<Root><Error><Message>" & ex.Message & "</Message><Type>Other</Type></Error></Root>")
                End Select

            End Try

            Return oReturnXML

        End Using


    End Function

    ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
    Public Shared Function GenerateConfirmationReport(ByVal ConfirmationId As String, _
                                                      ByVal ShowAllRentals As Boolean, _
                                                      Optional RentalExclustion As String = "")

        Return DataRepository.GenerateConfirmationReport(ConfirmationId, ShowAllRentals, RentalExclustion)

    End Function

    Public Shared Function TransformXML(ByVal xsltFileName As String, ByVal xmlText As String) As String

        Dim xst As System.Xml.Xsl.XslTransform = Nothing

        Dim xal As System.Xml.Xsl.XsltArgumentList = Nothing

        Dim xtr As System.Xml.XmlTextReader = Nothing

        Dim resolver As System.Xml.XmlUrlResolver = Nothing

        '  Dim xsltExt As XSLTExtension = Nothing

        Dim xtrIn As System.Xml.XmlTextReader = Nothing

        Dim xpd As System.Xml.XPath.XPathDocument

        '   Dim sw As StringWriterWithEncoding = Nothing
        Dim textWriter As New System.IO.StringWriter()

        Dim strReturn As String = String.Empty

        Try

            'Setup the XSL Transform object

            xtr = New System.Xml.XmlTextReader(xmlText, Xml.XmlNodeType.Document, Nothing)

            xst = New System.Xml.Xsl.XslTransform

            xst.Load(xsltFileName)

            'Prepare the XML document to be transformed

            xtrIn = New System.Xml.XmlTextReader(xmlText, Xml.XmlNodeType.Document, Nothing)

            xpd = New System.Xml.XPath.XPathDocument(xtrIn)

            'Prepare the output objects

            'This prevents unprintable characters from being printed at the top of the resulting string

            ' sw = New StringWriterWithEncoding(New System.Text.UTF8Encoding(False))


            'Transform the XML document using the XSLT file and place the output in "sw"

            resolver = New System.Xml.XmlUrlResolver

            xal = New System.Xml.Xsl.XsltArgumentList

            'xsltExt = New XSLTExtension

            'xal.AddExtensionObject("urn:date-format", xsltExt)

            ' xst.Transform(xpd, xal, sw, resolver)
            xst.Transform(xpd, xal, textWriter, resolver)


            strReturn = textWriter.ToString.Replace(" ", "&nbsp;") 'Replace the   character with &nbsp;

        Catch ex As Exception

            Dim strMessage As String

            strMessage = "xsltFileName: " & xsltFileName & ControlChars.CrLf

            strMessage &= "xmlText: " & ControlChars.CrLf & xmlText & ControlChars.CrLf

            'RaiseEvent Exception(Me, New EMailExceptionEventArgs(ex))

        Finally

            If Not xal Is Nothing Then

                xal = Nothing

            End If

            '  If Not sw Is Nothing Then

            '  sw.Close()

            ' sw = Nothing

            'End If

            If Not textWriter Is Nothing Then
                textWriter.Close()
                textWriter = Nothing
            End If

            If Not xtrIn Is Nothing Then

                xtrIn.Close()

                xtrIn = Nothing

            End If

            If Not xtr Is Nothing Then

                xtr.Close()

                xtr = Nothing

            End If

        End Try

        Return strReturn

    End Function



    '--------------------------------------------------------------------
    '	Function to save reprot to html file
    '--------------------------------------------------------------------
    Public Shared Function SaveFile(ByVal Path As String, ByVal FileName As String, ByVal Text As String, ByVal Extension As String) As Boolean
        'If File.Exists(Path & FileName & "." & Extension) Then
        '    Return False
        'Else
        File.WriteAllText(Path & FileName & "." & Extension, Text)
        Return True
        'End If
    End Function

    '--------------------------------------------------------------------
    '	Function to convert html file to pdf file
    '--------------------------------------------------------------------
    ''Public Shared Function CreatePdf(ByVal Path As String, ByVal FileName As String) As String
    'Dim msg As String = ""

    'Dim strHtml As String = ""
    'Dim strPdf As String = ""
    'Dim n
    '    strHtml = Path & FileName & ".htm"
    '    strPdf = Path & FileName & ".Pdf"
    '    Try
    '        If strHtml <> "" And strPdf <> "" Then
    'Dim objAuroraPDFGen As New AuroraPdfGen.Util
    '            n = objAuroraPDFGen.HtmlToPdf(strHtml, strPdf)
    '            If n <> 0 Then
    '                msg = "Failed to create pdf file. Error: " & CStr(Hex(n))
    '            End If
    '        End If
    '    Catch ex As Exception
    '        msg = ex.Message
    '    End Try


    '    Return msg
    'End Function


    '--------------------------------------------------------------------
    '	Function to get the html file for email
    '--------------------------------------------------------------------
    Public Shared Function GetEmailFile(ByVal fileName As String) As String

        'Dim fi
        'Dim fileText

        If File.Exists(fileName) Then
            Return File.ReadAllText(fileName)
        Else
            Return ""
        End If


        'If (fs.FileExists(fileName)) Then
        '    fi = fs.OpenTextFile(fileName, 1, False, False)
        '    fileText = fi.Readall
        '    GetEmailFile = fileText
        '    fi.Close()
        'Else
        '    GetEmailFile = ""
        'End If
        'fs = Nothing
    End Function


    '--------------------------------------------------------------------
    '	modified code for txt file. 22/10/2002 by James Liu
    '	modified on 11.3.8 by shoel to use .net 2.0 stuff :)
    '	Function to get the text file and create a control file for fax
    '--------------------------------------------------------------------
    Public Shared Function GetControlFile(ByVal sDestination As String, ByVal sAttention As String, ByVal sCompany As String, ByVal sReference As String, _
                            ByVal EFaxNotify As String, ByVal sCfnPath As String, ByVal psFilename As String) As String
        'fs = server.CreateObject("Scripting.FilesystemObject")
        'Dim fi
        Dim fileName As String
        Dim fileText As String
        fileName = sCfnPath & "\" & psFilename & ".TXT"
        'If (fs.FileExists(fileName)) Then
        ' fi = fs.OpenTextFile(fileName, 1, False, False)
        ' fileText = fi.Readall
        ' fi.Close()
        ' Else
        ' fileText = ""
        ' End If

        If File.Exists(fileName) Then
            fileText = File.ReadAllText(fileName)
        Else
            fileText = ""
        End If

        GetControlFile = _
         "!FXW - Aurora Confirmation Generator" & Chr(13) + Chr(10) & _
         "!PassThrough" & Chr(13) + Chr(10) & _
         "Notify=NON-Delivery" & Chr(13) + Chr(10) & _
         "UserId=" & EFaxNotify & Chr(13) + Chr(10) & _
         "Company=" & sCompany & Chr(13) + Chr(10) & _
         "Reference=" & sReference & Chr(13) + Chr(10) & _
         "Destination=" & sDestination & Chr(13) + Chr(10) & _
         "Attention=" & sAttention & Chr(13) + Chr(10) & _
         "!PassThrough-End" & Chr(13) + Chr(10) & _
         "!Bodytext,0,0" & Chr(13) + Chr(10) & _
         fileText & Chr(13) + Chr(10) & _
         "!Bodytext-End"


    End Function



    '--------------------------------------------------------------------
    '	Function to email report
    '--------------------------------------------------------------------
    ''rev:mia April 5 2013 - added BCC
    Public Shared Function EMailPdf(ByVal sCfnPath As String, _
                                    ByVal psFileName As String, _
                                    ByVal sEmailAddress As String, _
                                    ByVal EFaxNotify As String, _
                                    ByVal emailSubject As String, _
                                    Optional BCC As String = "") As Boolean
        Dim sText
        Dim fileName
        fileName = sCfnPath & "\" & psFileName & ".htm"
        sText = GetEmailFile(fileName)


        If sText <> "" Then
            sText = Replace(sText, "../Images/", "")

            Dim emailArray As Array
            Dim emailImg As String
            emailArray = Split(emailSubject, " - ", -1, 1)
            emailImg = Trim(emailArray(0))

            ''REV:MIA MAY 16 2012 - MAKE BRAND HEADERS DYNAMIC
            sText = GetBrandLogoImage(emailImg, sText)

            'If emailImg = "BACKPACKER" Then
            '    sText = Replace(sText, "Backpackerlogo.gif", CStr(ConfigurationManager.AppSettings("BackpackerLogoURL")))
            'ElseIf emailImg = "MAUI" Then
            '    sText = Replace(sText, "Mauilogosmall.gif", CStr(ConfigurationManager.AppSettings("MauiLogoURL")))
            'ElseIf emailImg = "BRITZ" Then
            '    sText = Replace(sText, "Britz.gif", CStr(ConfigurationManager.AppSettings("BritzLogoURL")))
            'ElseIf emailImg = "EXPLORE MORE" Then
            '    sText = Replace(sText, "ExploreMore-logo.gif", CStr(ConfigurationManager.AppSettings("ExploreMoreLogoURL")))
            'ElseIf emailImg = "MOTORHOMESANDCARS.COM" Then
            '    sText = Replace(sText, "MACLOGO.gif", CStr(ConfigurationManager.AppSettings("MACLogoURL")))
            'End If

        End If

        If sText <> "" Then
            Dim strBody As String = ""

            Dim oMailMsg As System.Web.Mail.MailMessage = New System.Web.Mail.MailMessage
            oMailMsg.From = EFaxNotify
            oMailMsg.To = sEmailAddress
            ''rev:mia April 5 2013 - added BCC
            If (Not String.IsNullOrEmpty(BCC)) Then
                oMailMsg.Bcc = BCC
            End If
            ' fix for weird mapping problem
            oMailMsg.Headers.Add("Reply-To", EFaxNotify)
            ' fix for weird mapping problem
            oMailMsg.BodyEncoding = System.Text.Encoding.ASCII
            oMailMsg.Subject = emailSubject
            oMailMsg.Body = sText
            oMailMsg.BodyFormat = Web.Mail.MailFormat.Html

            Web.Mail.SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("MailServerURL"))
            Web.Mail.SmtpMail.Send(oMailMsg)
            Return True
        Else
            Return False
        End If
    End Function

    '--------------------------------------------------------------------
    '	Function to fax report
    '--------------------------------------------------------------------

    Public Shared Function FaxPdf(ByVal sFaxNumber As String, ByVal sAttention As String, ByVal sCompany As String, ByVal sReference As String, _
                                  ByVal EFaxNotify As String, ByVal sEfaxPath As String, ByVal sCfnPath As String, ByVal psFileName As String, _
                                  ByVal FaxBackup As String) As Boolean
        Dim sText As String
        sText = GetControlFile(sFaxNumber, sAttention, sCompany, sReference, EFaxNotify, sCfnPath, psFileName)

        If sText = "" Then
            Return False
        Else
            SaveFile(sEfaxPath & "\", psFileName, sText, "CTL")
            ''''added for efax backup
            SaveFile(FaxBackup & "\", psFileName, sText, "CTL")

            Return True
        End If
    End Function


    '---------------------------------------------------------------------
    '	Function to create a formated text file for confirmation
    '---------------------------------------------------------------------	
    Public Shared Function CreateTextFile(ByVal sFileName As String, ByVal xmlDso As XmlDocument, ByVal sExtension As String) As Boolean

        'confirmation header
        Dim ConfirmationTitle As String = ""
        Dim CompanyName
        Dim ABNNumber
        Dim address
        Dim Phone0800
        Dim Web
        Dim GST

        'vehicle status
        Dim VehicleStatus
        Dim CfnDate
        Dim OurRef
        Dim OurConsultant
        Dim RentalStatusText
        'Agent details
        Dim AgnName As String = ""
        Dim AgentEmail As String = ""
        Dim ContactName As String = ""
        Dim AgentRef As String = ""
        Dim AgentPhone As String = ""
        Dim AgentFax As String = ""
        Dim Customer As String = ""
        'Customer details
        Dim CustomerName As String = ""
        Dim CustomerEmail As String = ""
        Dim CustomerPhone As String = ""
        Dim CustomerFax As String = ""
        Dim cAddress As String = ""
        Dim PersonalNote As String = ""
        'confirmation notes. for each
        Dim NtsType As String = ""
        Dim NtsText As String = ""
        'booking details
        Dim Prd As String = ""
        Dim CkoWhen
        Dim CkoAddress
        Dim CkoPhone
        Dim CkoStart
        Dim CkoFinish
        Dim CkiWhen
        Dim CkiAddress
        Dim CkiPhone
        Dim CkiStart
        Dim CkiFinish
        'Agent products, for each Curr
        Dim Curr
        'for each desc
        Dim Desc
        Dim Qty
        Dim Uom

        Dim Rate
        Dim Total

        'Customer products, for each Curr
        Dim cCurr
        'for each desc
        Dim cDesc
        Dim cQty
        Dim cUom

        Dim cRate
        Dim cTotal


        If File.Exists(sFileName & "." & sExtension) Then
            Return False
        Else

            Try




                Dim sbOutputToWrite As New StringBuilder

                'sFile = fso.CreateTextFile(sFileName & "." & sExtension)


                'xmlDso = server.CreateObject("MSXML2.DOMDOCUMENT")
                'xmlDso.async = False
                'xmlDso.loadXML(xmlObj.xml)
                Dim docLen
                docLen = xmlDso.DocumentElement.ChildNodes.Count
                Dim docnum
                For docnum = 1 To docLen - 8
                    If xmlDso.DocumentElement.ChildNodes(docnum).Name = "doc" Then

                        Dim cusomerSurname, bookRef
                        cusomerSurname = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/CustomerSurname").InnerText
                        bookRef = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/OurRef").InnerText

                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/brandlogo").InnerText = "../Images/Mauilogosmall.gif" Then
                            ConfirmationTitle = "MAUI - " & " " & cusomerSurname & " " & bookRef & " " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/ConfirmationTitle").InnerText
                        ElseIf xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/brandlogo").InnerText = "../Images/Backpackerlogo.gif" Then
                            ConfirmationTitle = "BACKPACKER - " & " " & cusomerSurname & " " & bookRef & " " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/ConfirmationTitle").InnerText
                        ElseIf xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/brandlogo").InnerText = "../Images/Britz.gif" Then
                            ConfirmationTitle = "BRITZ - " & " " & cusomerSurname & " " & bookRef & " " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/ConfirmationTitle").InnerText
                        ElseIf xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/brandlogo").InnerText = "../Images/ExploreMore-logo.gif" Then
                            ConfirmationTitle = "EXPLORE MORE - " & " " & cusomerSurname & " " & bookRef & " " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/ConfirmationTitle").InnerText
                        ElseIf xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/brandlogo").InnerText = "../Images/MACLOGO.gif" Then
                            ConfirmationTitle = "MOTORHOMESANDCARS.COM - " & " " & cusomerSurname & " " & bookRef & " " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/ConfirmationTitle").InnerText
                        End If

                        Dim emailSubject = ConfirmationTitle
                        CompanyName = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/CompanyName").InnerText

                        If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(4).Name <> "ABNNumber" Then
                            ABNNumber = ""
                        Else
                            ABNNumber = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/ABNNumber").InnerText
                        End If

                        address = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/address").InnerText
                        address = Replace(address, "[NL]", vbNewLine)


                        If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(4).Name <> "ABNNumber" Then
                            If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(6).Name <> "Phone0800" Then
                                Phone0800 = ""
                            Else
                                Phone0800 = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/Phone0800").InnerText
                            End If
                        Else
                            If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(7).Name <> "Phone0800" Then
                                Phone0800 = ""
                            Else
                                Phone0800 = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/Phone0800").InnerText
                            End If
                        End If

                        If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(4).Name <> "ABNNumber" Then
                            If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(6).Name <> "Phone0800" Then
                                If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(6).Name <> "Web" Then
                                    Web = ""
                                Else
                                    Web = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/Web").InnerText
                                End If
                            Else
                                If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(7).Name <> "Web" Then
                                    Web = ""
                                Else
                                    Web = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/Web").InnerText
                                End If
                            End If
                        Else
                            If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(7).Name <> "Phone0800" Then
                                If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(7).Name <> "Web" Then
                                    Web = ""
                                Else
                                    Web = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/Web").InnerText
                                End If
                            Else
                                If xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(0).ChildNodes(8).Name <> "Web" Then
                                    Web = ""
                                Else
                                    Web = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/Web").InnerText
                                End If
                            End If
                        End If

                        GST = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/GST").InnerText

                        VehicleStatus = "Vehicle Status:   " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/VehicleStatus").InnerText
                        VehicleStatus = leftAlign(VehicleStatus, 42)
                        CfnDate = "Date:             " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/CfnDate").InnerText
                        CfnDate = leftAlign(CfnDate, 42)
                        OurRef = "Our Ref:          " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/OurRef").InnerText
                        OurRef = leftAlign(OurRef, 42)
                        OurConsultant = "Our Consultant:   " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/OurConsultant").InnerText
                        OurConsultant = leftAlign(OurConsultant, 42)
                        RentalStatusText = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Header/RentalStatusText").InnerText

                        'Agent
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Audience").InnerText = "Agent" Then
                            AgnName = "Agent name: " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/AgnName").InnerText
                            If Len(AgnName) > 44 Then
                                AgnName = Mid(AgnName, 1, 44)
                            End If
                            AgnName = leftAlign(AgnName, 45)
                            AgentEmail = "Email:      " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/AgentEmail").InnerText
                            If Len(AgentEmail) > 44 Then
                                AgentEmail = Mid(AgentEmail, 1, 44)
                            End If
                            AgentEmail = leftAlign(AgentEmail, 45)
                            ContactName = "Contact:    " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/ContactName").InnerText
                            If Len(ContactName) > 44 Then
                                ContactName = Mid(ContactName, 1, 44)
                            End If
                            ContactName = leftAlign(ContactName, 45)
                            AgentRef = "Agent Ref:  " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/AgentRef").InnerText
                            If Len(AgentRef) > 44 Then
                                AgentRef = Mid(AgentRef, 1, 44)
                            End If
                            AgentRef = leftAlign(AgentRef, 45)
                            AgentPhone = "Phone:      " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/AgentPhone").InnerText
                            AgentPhone = leftAlign(AgentPhone, 45)
                            AgentFax = "Fax:        " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/AgentFax").InnerText
                            AgentFax = leftAlign(AgentFax, 45)
                            Customer = "Customer:   " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("AgentDetails/Customer").InnerText
                        End If

                        'Customer
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Audience").InnerText = "Customer" Then
                            CustomerName = "Customer: " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CustomerDetails/CustomerName").InnerText
                            If Len(CustomerName) > 44 Then
                                AgentRef = Mid(CustomerName, 1, 44)
                            End If
                            CustomerName = leftAlign(CustomerName, 45)
                            CustomerEmail = "Email:    " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CustomerDetails/CustomerEmail").InnerText
                            If Len(CustomerEmail) > 44 Then
                                AgentRef = Mid(CustomerEmail, 1, 44)
                            End If
                            CustomerEmail = leftAlign(CustomerEmail, 45)
                            CustomerPhone = "Phone:    " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CustomerDetails/CustomerPhone").InnerText
                            CustomerPhone = leftAlign(CustomerPhone, 45)
                            CustomerFax = "Fax:      " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CustomerDetails/CustomerFax").InnerText
                            CustomerFax = leftAlign(CustomerFax, 45)
                            cAddress = "Address:  " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CustomerDetails/Address").InnerText
                            If Len(cAddress) > 44 Then
                                AgentRef = Mid(cAddress, 1, 44)
                            End If
                            cAddress = leftAlign(cAddress, 45)
                        End If

                        'personal notes
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("PersonalNote").InnerText <> "" Then
                            PersonalNote = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("PersonalNote").InnerText
                        Else
                            PersonalNote = ""
                        End If

                        '	if fso.FileExists(sFileName & sExtension) then
                        '		return
                        '	else
                        '		set sFile = fso.CreateTextFile(sFileName & "." & sExtension)
                        '		
                        'confirmation header

                        sbOutputToWrite.Append(System.Environment.NewLine)
                        sbOutputToWrite.Append(ConfirmationTitle & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        sbOutputToWrite.Append(CompanyName & System.Environment.NewLine)
                        If ABNNumber <> "" Then
                            sbOutputToWrite.Append(ABNNumber & System.Environment.NewLine)
                        End If
                        sbOutputToWrite.Append(address & System.Environment.NewLine)
                        sbOutputToWrite.Append(Phone0800 & System.Environment.NewLine)
                        sbOutputToWrite.Append(Web & System.Environment.NewLine)
                        sbOutputToWrite.Append(GST & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        'vehcile status
                        sbOutputToWrite.Append(VehicleStatus)
                        sbOutputToWrite.Append(CfnDate & System.Environment.NewLine)
                        sbOutputToWrite.Append(OurRef)
                        sbOutputToWrite.Append(OurConsultant & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        sbOutputToWrite.Append(RentalStatusText & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)

                        'agent
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Audience").InnerText = "Agent" Then
                            sbOutputToWrite.Append(AgnName)
                            sbOutputToWrite.Append(AgentEmail & System.Environment.NewLine)
                            sbOutputToWrite.Append(ContactName)
                            sbOutputToWrite.Append(AgentRef & System.Environment.NewLine)
                            sbOutputToWrite.Append(AgentPhone)
                            sbOutputToWrite.Append(AgentFax & System.Environment.NewLine)
                            sbOutputToWrite.Append(Customer)
                        End If
                        'Customer
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Audience").InnerText = "Customer" Then
                            sbOutputToWrite.Append(CustomerName)
                            sbOutputToWrite.Append(CustomerEmail & System.Environment.NewLine)
                            sbOutputToWrite.Append(CustomerPhone)
                            sbOutputToWrite.Append(CustomerFax & System.Environment.NewLine)
                            sbOutputToWrite.Append(cAddress)
                        End If
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        ' personal notes 
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("PersonalNote").InnerText <> "" Then
                            sbOutputToWrite.Append(System.Environment.NewLine)
                            sbOutputToWrite.Append(PersonalNote & System.Environment.NewLine)
                        End If
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        'notes
                        Dim slen
                        For slen = 0 To xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(4).ChildNodes.Count - 1
                            NtsType = xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(4).ChildNodes(slen).ChildNodes(0).InnerText
                            NtsText = xmlDso.DocumentElement.ChildNodes(docnum).ChildNodes(4).ChildNodes(slen).ChildNodes(1).InnerText
                            sbOutputToWrite.Append(NtsType & System.Environment.NewLine)
                            sbOutputToWrite.Append(NtsText & System.Environment.NewLine)
                        Next
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        sbOutputToWrite.Append("_______________________________________________________________________________________" & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)

                        'booking details - agent
                        Prd = "Booking Details - " & xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/Prd").InnerText
                        Dim choStr, addStr, phStr
                        choStr = leftAlign("Check Out", 22)
                        addStr = leftAlign("Address", 50)
                        phStr = leftAlign("Phone", 15)
                        CkoWhen = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkoWhen").InnerText
                        CkoWhen = leftAlign(CkoWhen, 22)
                        CkoAddress = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkoAddress").InnerText
                        If Len(CkoAddress) > 49 Then
                            CkoAddress = Mid(CkoAddress, 1, 49)
                        End If
                        CkoAddress = leftAlign(CkoAddress, 50)
                        CkoPhone = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkoPhone").InnerText
                        CkoPhone = leftAlign(CkoPhone, 15)

                        CkoStart = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkoStart").InnerText
                        CkoFinish = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkoFinish").InnerText

                        Dim chiStr
                        chiStr = leftAlign("Check In", 22)

                        CkiWhen = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkiWhen").InnerText
                        CkiWhen = leftAlign(CkiWhen, 22)
                        CkiAddress = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkiAddress").InnerText
                        If Len(CkiAddress) > 49 Then
                            CkiAddress = Mid(CkiAddress, 1, 49)
                        End If
                        CkiAddress = leftAlign(CkiAddress, 50)
                        CkiPhone = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkiPhone").InnerText
                        CkiPhone = leftAlign(CkiPhone, 15)
                        CkiStart = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkiStart").InnerText
                        CkiFinish = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("BookingDetails/CkiFinish").InnerText
                        sbOutputToWrite.Append(Prd & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        sbOutputToWrite.Append(choStr)
                        sbOutputToWrite.Append(addStr)
                        sbOutputToWrite.Append(phStr & System.Environment.NewLine)
                        sbOutputToWrite.Append(CkoWhen)
                        sbOutputToWrite.Append(CkoAddress)
                        sbOutputToWrite.Append(CkoPhone & System.Environment.NewLine)
                        If CkoStart <> CkiStart Or CkoFinish <> CkiFinish Then
                            Dim strforCko
                            strforCko = "          Operating Hours : Mon-Sun " & CkoStart & " to " & CkoFinish
                            sbOutputToWrite.Append(strforCko & System.Environment.NewLine)
                        End If

                        sbOutputToWrite.Append(chiStr & System.Environment.NewLine)
                        sbOutputToWrite.Append(CkiWhen)
                        sbOutputToWrite.Append(CkiAddress)
                        sbOutputToWrite.Append(CkiPhone & System.Environment.NewLine)

                        Dim strforCki
                        strforCki = "          Operating Hours : Mon-Sun " & CkiStart & " to " & CkiFinish
                        sbOutputToWrite.Append(strforCki & System.Environment.NewLine)
                        sbOutputToWrite.Append(System.Environment.NewLine)

                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Audience").InnerText = "Agent" Then
                            If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes.Count > 0 Then
                                Dim currLen, prdLen
                                For currLen = 0 To xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes.Count - 1
                                    Curr = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(0).InnerText
                                    Curr = leftAlign(Curr, 5)
                                    For prdLen = 0 To xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(1).ChildNodes.Count - 1
                                        Desc = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(1).ChildNodes(prdLen).InnerText
                                        Qty = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(2).ChildNodes(prdLen).InnerText
                                        Uom = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(3).ChildNodes(prdLen).InnerText
                                        Rate = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(4).ChildNodes(prdLen).InnerText
                                        Total = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(5).ChildNodes(prdLen).InnerText
                                        If Len(Desc) > 54 Then
                                            Desc = Mid(Desc, 1, 54)
                                        End If
                                        Desc = leftAlign(Desc, 55)
                                        Qty = leftAlign(Qty, 4)
                                        Uom = leftAlign(Uom, 5)
                                        Rate = rightAlign(Rate, 7) & " "
                                        Total = rightAlign(Total, 10)

                                        sbOutputToWrite.Append(Desc)
                                        sbOutputToWrite.Append(Qty)
                                        sbOutputToWrite.Append(Uom)
                                        sbOutputToWrite.Append(Rate)
                                        sbOutputToWrite.Append(Curr)
                                        sbOutputToWrite.Append(Total & System.Environment.NewLine)
                                    Next
                                    'total charge
                                    Dim totalStr, totalChg
                                    totalChg = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(10).InnerText
                                    totalChg = rightAlign(totalChg, 10)
                                    totalStr = rightAlign("Total Charged", 71) & " "
                                    sbOutputToWrite.Append(totalStr)
                                    sbOutputToWrite.Append(Curr)
                                    sbOutputToWrite.Append(totalChg & System.Environment.NewLine)
                                    'agent discount
                                    Dim discStr, disc
                                    disc = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(8).InnerText
                                    disc = rightAlign(disc, 10)
                                    discStr = rightAlign("Less Agent Discount", 71) & " "
                                    sbOutputToWrite.Append(discStr)
                                    sbOutputToWrite.Append(Curr)
                                    sbOutputToWrite.Append(disc & System.Environment.NewLine)
                                    'less payment received
                                    Dim lessStr, less
                                    If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(11).InnerText = "" Then
                                        less = "0.00"
                                    Else
                                        less = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(11).InnerText
                                    End If
                                    less = rightAlign(less, 10)
                                    lessStr = rightAlign("Less Payment Received", 71) & " "
                                    sbOutputToWrite.Append(lessStr)
                                    sbOutputToWrite.Append(Curr)
                                    sbOutputToWrite.Append(less & System.Environment.NewLine)
                                    'Total Payable
                                    Dim totalCharge, agentDis, lessPay, totalPay
                                    If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(10).InnerText = "" Then
                                        totalCharge = 0
                                    Else
                                        totalCharge = CDbl(xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(10).InnerText)
                                    End If
                                    If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(8).InnerText = "" Then
                                        agentDis = 0
                                    Else
                                        agentDis = CDbl(xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(8).InnerText)
                                    End If
                                    If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(11).InnerText = "" Then
                                        lessPay = 0
                                    Else
                                        lessPay = CDbl(xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(11).InnerText)
                                    End If

                                    totalPay = FormatNumber((totalCharge - agentDis - lessPay), 2)
                                    totalPay = rightAlign(totalPay, 10)
                                    Dim allStr
                                    allStr = rightAlign("Total Payable (Inclusive of GST)", 71) & " "
                                    sbOutputToWrite.Append(allStr)
                                    sbOutputToWrite.Append(Curr)
                                    sbOutputToWrite.Append(totalPay & System.Environment.NewLine)
                                    'GST
                                    Dim gstStr, sgst
                                    sgst = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Agent").ChildNodes(currLen).ChildNodes(9).InnerText
                                    sgst = rightAlign(sgst, 10)
                                    gstStr = rightAlign("GST Content", 71) & " "
                                    sbOutputToWrite.Append(gstStr)
                                    sbOutputToWrite.Append(Curr)
                                    sbOutputToWrite.Append(sgst & System.Environment.NewLine)
                                    sbOutputToWrite.Append(System.Environment.NewLine)
                                Next
                                sbOutputToWrite.Append("_______________________________________________________________________________________" & System.Environment.NewLine)
                                sbOutputToWrite.Append(System.Environment.NewLine)
                            End If
                        End If

                        'booking details - customer

                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes.Count > 0 Then
                            sbOutputToWrite.Append("Customer to pay on arrive" & System.Environment.NewLine)
                            sbOutputToWrite.Append(System.Environment.NewLine)
                            Dim cCurrLen, cPrdLen

                            For cCurrLen = 0 To xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes.Count - 1
                                cCurr = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(0).InnerText
                                cCurr = leftAlign(cCurr, 5)
                                For cPrdLen = 0 To xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(1).ChildNodes.Count - 1
                                    cDesc = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(1).ChildNodes(cPrdLen).InnerText
                                    cQty = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(2).ChildNodes(cPrdLen).InnerText
                                    cUom = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(3).ChildNodes(cPrdLen).InnerText
                                    cRate = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(4).ChildNodes(cPrdLen).InnerText
                                    cTotal = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(5).ChildNodes(cPrdLen).InnerText
                                    If Len(cDesc) > 54 Then
                                        cDesc = Mid(cDesc, 1, 54)
                                    End If
                                    cDesc = leftAlign(cDesc, 55)
                                    cQty = leftAlign(cQty, 4)
                                    cUom = leftAlign(cUom, 5)
                                    cRate = rightAlign(cRate, 7) & " "
                                    cTotal = rightAlign(cTotal, 10)

                                    sbOutputToWrite.Append(cDesc)
                                    sbOutputToWrite.Append(cQty)
                                    sbOutputToWrite.Append(cUom)
                                    sbOutputToWrite.Append(cRate)
                                    sbOutputToWrite.Append(cCurr)
                                    sbOutputToWrite.Append(cTotal & System.Environment.NewLine)
                                Next
                                'total charge
                                Dim cTotalStr, cTotalChg
                                cTotalChg = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(10).InnerText
                                cTotalChg = rightAlign(cTotalChg, 10)
                                cTotalStr = rightAlign("Total Charged", 71) & " "
                                sbOutputToWrite.Append(cTotalStr)
                                sbOutputToWrite.Append(cCurr)
                                sbOutputToWrite.Append(cTotalChg & System.Environment.NewLine)

                                'less payment received
                                Dim cLessStr, cLess
                                Dim payLen, CurrPoint As Integer
                                Dim cTotalCharge, cLessPay, cTotalPay

                                cLessStr = rightAlign("Less Payment Received", 71) & " "

                                If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Payments").ChildNodes.Count > 0 Then
                                    For payLen = 0 To xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Payments").ChildNodes.Count - 1
                                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Payments").ChildNodes(payLen).ChildNodes(0).InnerText = xmlDso.DocumentElement.ChildNodes(1).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(0).InnerText Then
                                            CurrPoint = payLen
                                        End If
                                    Next
                                    cLess = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Payments").ChildNodes(CurrPoint).ChildNodes(1).InnerText
                                Else
                                    cLess = "0.00"
                                End If
                                cLessPay = CDbl(cLess)
                                cLess = rightAlign(cLess, 10)

                                sbOutputToWrite.Append(cLessStr)
                                sbOutputToWrite.Append(cCurr)
                                sbOutputToWrite.Append(cLess & System.Environment.NewLine)

                                'Total Payable

                                If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(10).InnerText = "" Then
                                    cTotalCharge = 0
                                Else
                                    cTotalCharge = CDbl(xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(10).InnerText)
                                End If

                                cTotalPay = FormatNumber((cTotalCharge - cLessPay), 2)
                                cTotalPay = rightAlign(cTotalCharge, 10)
                                Dim cAllStr
                                cAllStr = rightAlign("Total Payable (Inclusive of GST)", 71) & " "
                                sbOutputToWrite.Append(cAllStr)
                                sbOutputToWrite.Append(cCurr)
                                sbOutputToWrite.Append(cTotalPay & System.Environment.NewLine)
                                'GST
                                Dim cGstStr, cGst
                                cGst = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("Customer").ChildNodes(cCurrLen).ChildNodes(9).InnerText
                                cGst = rightAlign(cGst, 10)
                                cGstStr = rightAlign("GST Content", 71) & " "
                                sbOutputToWrite.Append(cGstStr)
                                sbOutputToWrite.Append(cCurr)
                                sbOutputToWrite.Append(cGst & System.Environment.NewLine)
                                sbOutputToWrite.Append(System.Environment.NewLine)
                            Next
                            sbOutputToWrite.Append("_______________________________________________________________________________________" & System.Environment.NewLine)
                            sbOutputToWrite.Append(System.Environment.NewLine)
                        End If
                        'footer note
                        Dim sFooterNote
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("FooterNote").InnerText <> "" Then
                            sFooterNote = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("FooterNote").InnerText
                            sbOutputToWrite.Append(sFooterNote & System.Environment.NewLine)
                        End If
                        sbOutputToWrite.Append(System.Environment.NewLine)
                        Dim NewsFlash
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes/NewsFlash").InnerText <> "" Then
                            NewsFlash = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes/NewsFlash").InnerText
                            sbOutputToWrite.Append(NewsFlash & System.Environment.NewLine)
                            sbOutputToWrite.Append(System.Environment.NewLine)
                        End If
                        Dim noteLen, noteType, notetext
                        If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes").ChildNodes.Count > 1 Then
                            For noteLen = 1 To xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes").ChildNodes.Count - 1
                                If xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes").ChildNodes(noteLen).ChildNodes(0).InnerText <> "" Then
                                    noteType = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes").ChildNodes(noteLen).ChildNodes(0).InnerText
                                    notetext = xmlDso.DocumentElement.ChildNodes(docnum).SelectSingleNode("CfnFooterNotes").ChildNodes(noteLen).ChildNodes(1).InnerText
                                    sbOutputToWrite.Append(noteType & System.Environment.NewLine)
                                    sbOutputToWrite.Append(notetext & System.Environment.NewLine)

                                End If
                            Next
                        End If
                        ''''''''		end if 
                    End If
                Next



                File.WriteAllText(sFileName & "." & sExtension, sbOutputToWrite.ToString())

                Return True


            Catch ex As Exception
                Return False
            End Try
        End If

    End Function



    '---------------------------------------------------------------------
    '	Function to defined a fixed length string and make right alignment
    '---------------------------------------------------------------------	
    Public Shared Function rightAlign(ByVal str, ByVal myValue) As String
        Dim mlen As Integer
        Dim mstr As String
        mlen = Len(str)
        If mlen < myValue Then
            'mstr = space(myValue - mlen) & str
            mstr = Space(myValue - mlen) & str
        Else
            mstr = str
        End If
        rightAlign = mstr
    End Function
    '---------------------------------------------------------------------
    '	Function to defined a fixed length string and make left alignment
    '---------------------------------------------------------------------	
    Public Shared Function leftAlign(ByVal str, ByVal myValue) As String
        Dim mlen As Integer
        Dim mstr As String

        mlen = Len(str)
        If mlen < myValue Then
            mstr = str & Space(myValue - mlen)
            'mstr = str & String((myValue - mlen)," ")
        Else
            mstr = str
        End If
        leftAlign = mstr
    End Function


    'Public Shared Function UpdateData(ByVal ConfirmationId As String, ByVal BookingId As String, ByVal RentalId As String, ByVal RentalNumber As String, ByVal AudienceType As String, ByVal UserCode As String, ByVal PageName As String)

    '    'exec cfn_update_confirmations 'FB8D5E6D-C572-4843-83BA-0907FB093D7F', '6353412', '6353412-2', '2', '34CE97B1-5807-41C5-B043-5B10D43685C7', '', '', '', '', '', '', 0, 1, 'sp7', 'sp7', 
    '    'confirmationlistener.asp', 'U'
    '    DataRepository.CreateUpdateConfirmations(ConfirmationId, BookingId, RentalId, RentalNumber, AudienceType, "", "", "", "", "", "", False, 1, UserCode, UserCode, PageName, "U")


    'End Function


    Public Shared Function ReadSavedHTMLConfirmation(ByVal ConfirmationId As String, ByVal BasePath As String) As String
        'Dim obj, sfunctionName, res
        'Dim sFileName
        'Dim phyPath As String
        'Dim fi, ts, tt
        'Dim TextStreamTest
        'Dim frmFile
        'Dim frmStr, tmp
        'Dim btnFile, frmBtn

        'fs = server.CreateObject("Scripting.FilesystemObject")
        'xmlDso = server.CreateObject("MSXML2.DOMDOCUMENT")

        'sfunctionName = "cfn_getFileName '" + param1 + "'"

        'obj = server.CreateObject("AuroraFindCom.GenericFindRecord")
        'If Err.Number <> 0 Then
        '    Response.Write("<Error><ErrStatus>True</ErrStatus><ErrDescription> " + Err.Description + "</ErrDescription></Error>")
        '    Response.End()
        'End If

        'res = obj.getInformation(sfunctionName)
        'xmlDso.loadXML(res)
        'sFileName = xmlDso.documentElement.selectSingleNode("//Confirmations/CfnFileName").text

        Dim sFullPath, sFileContentHTML As String
        sFullPath = BasePath & ConfirmationId & ".htm"
        '''''''''''''Dim sPath, sFullPath, sFileContentHTML As String
        '''''''''''''sPath = DataRepository.GetFileNameForConfirmation(ConfirmationId)

        '' '' '' '' ''If sPath = "" Then
        '' '' '' '' ''    Return ""
        '' '' '' '' ''Else
        '' '' '' '' ''    'go ahead...
        '' '' '' '' ''    sFullPath = BasePath & sPath
        '' '' '' '' ''    If File.Exists(sFullPath) Then
        '' '' '' '' ''        sFileContentHTML = File.ReadAllText(sFullPath)
        '' '' '' '' ''    Else
        '' '' '' '' ''        Return ""
        '' '' '' '' ''    End If
        '' '' '' '' ''End If

        'go ahead...
        If File.Exists(sFullPath) Then
            sFileContentHTML = File.ReadAllText(sFullPath)
        Else
            Return ""
        End If

        Return sFileContentHTML

        'i suppose this is loading and previewing the darn view page


        'phyPath = server.MapPath("/aurora")
        ''phyPath = server.MapPath("/auroraConfirmation")
        'sFileName = phyPath & "/" & sFileName

        'fi = fs.OpenTextFile(sFileName, 1, False, False)

        'TextStreamTest = fi.Readall
        'frmFile = phyPath & "/html/ConfirmationButtons.html"

        'ts = fso.OpenTextFile(frmFile, 1, False, False)

        'tmp = "<BODY>" 'LEFTMARGIN= '" & "0" & "' RIGHTMARGIN='" & "0" & "'>"
        'frmStr = ts.Readall
        'frmStr = Replace(frmStr, "myRequest", param1)
        'frmStr = tmp & frmStr
        'TextStreamTest = Replace(TextStreamTest, tmp, frmStr)
        'btnFile = phyPath & "/html/confirmationSend.html"

        'tt = fso.OpenTextFile(btnFile, 1, False, False)

        'frmBtn = "<HEAD>" & tt.Readall
        'TextStreamTest = Replace(TextStreamTest, "<HEAD>", frmBtn)
        'Response.Write(TextStreamTest)
        'fi.Close()
        'ts.Close()
        'tt.Close()
        'fs = Nothing

    End Function


    Public Shared Function GetPathsForConfirmation(ByVal ConfirmationId As String)
        Return DataRepository.GetPathsForConfirmation(ConfirmationId)
    End Function

    Public Shared Function UpdateResendConfirmation(ByVal ConfirmationId As String, ByVal Email As String, ByVal FaxNumber As String, ByVal OutputType As String, ByVal AddUserId As String, ByVal AddProgramName As String, ByVal Parameter As String) As Boolean
        Return DataRepository.UpdateResendConfirmation(ConfirmationId, Email, FaxNumber, OutputType, AddUserId, AddProgramName, Parameter)
    End Function

    Public Shared Function GetPreviouslySavedFileName(ByVal ConfirmationId As String) As String
        ' <Confirmations><CfnFileName>Confirmations\3BF2A08E-2D24-44A7-B39C-E383472AB428.htm</CfnFileName></Confirmations>
        Dim oXml As XmlDocument
        oXml = DataRepository.GetPreviouslySavedFileName(ConfirmationId)
        Try
            Return oXml.DocumentElement.SelectSingleNode("Confirmations/CfnFileName").InnerText
        Catch ex As Exception
            Return ""
        End Try

    End Function


#Region "rev:mia May 15 2012 - dynamic images in confirmation"
    Public Shared Function GetBrandLogoImage(ByVal emailImg As String, ByVal sText As String) As String

        Dim newtext As String = ""
        If (Not String.IsNullOrEmpty(emailImg)) Then
            Try

                emailImg = emailImg.Replace(" ", "").Replace("MOTORHOMESANDCARS.COM", "MAC").ToLower.Trim()
                Dim getfirstChar As String = emailImg.Substring(0, 1).ToUpper
                emailImg = emailImg.Remove(0, 1).Insert(0, getfirstChar)
                If (emailImg.IndexOf("Explore") <> -1) Then
                    emailImg = "ExploreMore"
                End If
                Dim text As String = sText
                newtext = text.Replace(String.Concat(emailImg, IIf(emailImg.IndexOf("Mighty") <> -1, ".gif", ".gif")), ConfigurationManager.AppSettings(emailImg & "LogoURL"))

            Catch ex As Exception
                Return sText
            End Try
            
        End If
        Return newtext

    End Function

    Public Shared Sub EMailDataPDF(ByVal sCfnText As String, _
                                  ByVal sEmailAddress As String, _
                                  ByVal EFaxNotify As String, _
                                  ByVal emailSubject As String, _
                                  Optional BCC As String = "")

        Dim sText As String = sCfnText

        Try
            Logging.LogInformation("EmailPDF", "EmailAddress: " & sEmailAddress & ", BCC:  " & BCC & ", EmailSubject : " & emailSubject)

            If sText <> "" Then
                sText = Replace(sText, "../Images/", "")

                Dim emailArray As Array
                Dim emailImg As String
                emailArray = Split(emailSubject, " - ", -1, 1)
                emailImg = Trim(emailArray(0))
                sText = GetBrandLogoImage(emailImg, sText)
            End If

            If sText <> "" Then
                Dim strBody As String = ""

                Dim oMailMsg As System.Web.Mail.MailMessage = New System.Web.Mail.MailMessage
                oMailMsg.From = EFaxNotify
                oMailMsg.To = sEmailAddress
                If (Not String.IsNullOrEmpty(BCC)) Then
                    oMailMsg.Bcc = BCC
                End If

                ' fix for weird mapping problem
                oMailMsg.Headers.Add("Reply-To", EFaxNotify)
                ' fix for weird mapping problem
                oMailMsg.BodyEncoding = System.Text.Encoding.ASCII
                oMailMsg.Subject = emailSubject
                oMailMsg.Body = sText
                oMailMsg.BodyFormat = Web.Mail.MailFormat.Html

                Web.Mail.SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("MailServerURL"))
                Web.Mail.SmtpMail.Send(oMailMsg)

            Else
                Logging.LogDebug("EMailDataPDF", "Calling EMailDataPDF sText is empty")
            End If

        Catch ex As Exception
            Logging.LogError("EMailDataPDF", "EMailDataPDF Error: " & ex.Message & " , ConfirmationText: " & sCfnText)
        End Try

    End Sub
#End Region

#Region "rev:mia Nov.15 2013 - fixing the message deleted by user"
    Public Shared Function GetConfirmationId(cfnrntnum As String, booid As String, cfnrntid As String) As String
        Return DataRepository.GetConfirmationId(cfnrntnum, booid, cfnrntid)
    End Function
#End Region
End Class
