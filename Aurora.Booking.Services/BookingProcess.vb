Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.text
Imports Aurora.Booking.Services
Imports Aurora.Common

Public Class BookingProcess

    Public Shared Function GetProductID(ByVal productID As String, ByVal usercode As String) As String

        Dim xmldoc As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getProduct", productID, "", "", "", "", "", "", usercode)
        If xmldoc.OuterXml <> "<data></data>" Then
            Return xmldoc.SelectSingleNode("data/PRODUCT/Id").InnerText
        Else
            Return productID
        End If
    End Function

    Public Shared Function CalculateRequestPeriod(ByVal cko As String, ByVal ckoampm As String, ByVal cki As String, ByVal ckiampm As String, ByVal hireperiod As String, ByVal daysmode As String, ByVal dataadd As Integer) As String

        Try
            Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("ADM_getDateDiff", _
                                                       cko, _
                                                       ckoampm, _
                                                       cki, _
                                                       ckiampm, _
                                                       hireperiod, _
                                                       daysmode, _
                                                       dataadd)
            Return xmlstring

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Shared Function ExtractGetAvailXML(ByVal xmlString As String) As String
        Dim sb As New StringBuilder
        Dim xmldoc As New XmlDocument

        Try

            xmldoc.LoadXml(xmlString)

            Dim nodes As XmlNodeList = xmldoc.GetElementsByTagName("Detail")
            sb.Append("<Package>")
            For Each node As XmlNode In nodes
                If node.Name = "Detail" Then
                    sb.Append("<Detail>")
                    For Each childnode As XmlNode In node.ChildNodes
                        Select Case childnode.Name
                            Case "pkg"
                                sb.AppendFormat("<pkg_id>{0}</pkg_id>", childnode.Attributes(0).Value)
                                sb.AppendFormat("<pkg_pkgid>{0}</pkg_pkgid>", childnode.Attributes(1).Value)
                                sb.AppendFormat("<pkg_pkgcom>{0}</pkg_pkgcom>", childnode.Attributes(2).Value)
                                sb.AppendFormat("<pkg_text>{0}</pkg_text>", childnode.InnerText)
                            Case "pp"
                                sb.AppendFormat("<pp>{0}</pp>", childnode.InnerText)
                            Case "br"
                                sb.AppendFormat("<br>{0}</br>", childnode.InnerText)
                            Case "uom"
                                sb.AppendFormat("<uom>{0}</uom>", childnode.InnerText)
                            Case "cur"
                                sb.AppendFormat("<cur>{0}</cur>", childnode.InnerText)
                            Case "Valqty"
                                sb.AppendFormat("<val_tc>{0}</val_tc>", childnode.ChildNodes(0).ChildNodes(0).Attributes(0).Value)
                                sb.AppendFormat("<val_pp>{0}</val_pp>", childnode.ChildNodes(0).ChildNodes(0).Attributes(1).Value)
                                sb.AppendFormat("<val_pd>{0}</val_pd>", childnode.ChildNodes(0).ChildNodes(0).Attributes(2).Value)
                                sb.AppendFormat("<val_vc>{0}</val_vc>", childnode.ChildNodes(0).ChildNodes(0).Attributes(3).Value)
                                sb.AppendFormat("<val_vdn>{0}</val_vdn>", childnode.ChildNodes(0).ChildNodes(0).Attributes(4).Value)
                                sb.AppendFormat("<val_vdg>{0}</val_vdg>", childnode.ChildNodes(0).ChildNodes(0).Attributes(5).Value)
                                sb.AppendFormat("<qty>{0}</qty>", childnode.ChildNodes(0).ChildNodes(1).InnerText)

                        End Select
                    Next
                    sb.Append("</Detail>")
                End If
            Next
            sb.Append("</Package>")
            Return sb.ToString

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Shared Sub SelectLocationBasedOnCodeAndUser(ByVal locationCode As String, Optional ByVal ddlControl As DropDownList = Nothing, Optional ByVal usercode As String = "", Optional ByVal dateinterval As Integer = 15)
        Try

            Dim index As Integer


            If String.IsNullOrEmpty(locationCode) Then Exit Sub
            If locationCode.Contains("-") Then
                locationCode = locationCode.Substring(0, locationCode.IndexOf("-"))
                If locationCode.EndsWith("-") Then
                    locationCode = locationCode.Remove(locationCode.Length - 1, 1)
                End If
            End If



            Dim dsTemp As New DataSet
            Dim ds As DataSet = Aurora.Common.Data.ExecuteDataSetSP("GEN_SelectLocationBasedOnCodeAndUser", dsTemp, locationCode, usercode)
            If ds.Tables(0).Rows.Count = 0 Then
                If ddlControl.Items.Count - 1 = 1 Then
                    index = ddlControl.SelectedIndex
                End If
                ddlControl.Items.Clear()
                ddlControl.Items.Add("AM")
                ddlControl.Items.Add("PM")
                ''MyBase.SetInformationShortMessage("No available time for location code '" & locationCode & "' and userCode '" & MyBase.UserCode & "'")
                ddlControl.SelectedIndex = index
                Exit Sub
            End If

            Dim startTime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(0))
            Dim endtime As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0)(1))

            Dim strtime As String = BookingProcessUtility.ParsetimeToString(startTime)

            ddlControl.Items.Clear()
            ddlControl.Items.Add(strtime)

            Do

                startTime = startTime.AddMinutes(dateinterval)
                strtime = BookingProcessUtility.ParsetimeToString(startTime)
                ddlControl.Items.Add(strtime)

            Loop While startTime <> endtime
            ddlControl.SelectedIndex = index
        Catch ex As Exception
            ''SetErrorMessage("SelectLocationBasedOnCodeAndUser -> " & ex.Message)
            Throw
        End Try
    End Sub


End Class

Public Class BookingProcessUtility

    Public Shared Function ParsetimeToString(ByVal starttime As DateTime) As String
        Dim strTime As String = (Trim(starttime.ToString.Remove(0, starttime.ToString.IndexOf(" ") + 1)))
        Dim index As Integer = strTime.LastIndexOf(":")
        strTime = strTime.Remove(index, 3)
        If strTime.Contains("a.m") Then
            strTime = strTime.Replace("a.m.", "AM")
        Else
            strTime = strTime.Replace("p.m.", "PM")
        End If
        Return strTime

    End Function

    Public Shared Function ParsetimeToString(ByVal starttime As String) As String
        Dim strtime As String
        strtime = starttime
        If strtime.Trim = "AM" Then Return ""
        If strtime.Trim = "PM" Then Return ""

        If strtime.Contains("AM") Then
            strtime = strtime.Replace("AM", "").Trim
        Else
            strtime = strtime.Replace("PM", "").Trim
        End If
        Return strtime
    End Function

    Public Shared Function IsValidIntegerValue(ByVal value As String) As Boolean
        Try
            If (CInt(value) = 0) Then Return True
            Dim temp As Integer = Integer.Parse(value)
            Return True

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Function ChangeRootElement(ByVal strxml As String) As String
        strxml = strxml.Replace("<data>", "<Data>")
        strxml = strxml.Replace("</data>", "</Data>")
        Return strxml
    End Function

    Public Shared Function GetAMPM(ByVal controldate As String) As String
        If controldate.Contains("AM") Then
            Return "AM"
        Else
            Return "PM"
        End If
    End Function

    Public Shared Function DateTimeSplitter(ByVal CKOCKIdate As DateTime, ByRef CKOCKItime As String) As String
        Dim CKOCKIdatestring As String = CKOCKIdate.ToShortDateString

        Dim CKOCKIarray() As String = CKOCKIdatestring.Split("/")
        If CKOCKIarray IsNot Nothing AndAlso (CKOCKIarray.Length = 3) Then
            If CKOCKIarray(0).Length = 1 Then
                CKOCKIarray(0) = "0" & CKOCKIarray(0)
            End If

            If CKOCKIarray(1).Length = 1 Then
                CKOCKIarray(1) = "0" & CKOCKIarray(1)
            End If
            CKOCKIdatestring = String.Concat(CKOCKIarray(0), "/", CKOCKIarray(1), "/", CKOCKIarray(2))
        End If

        CKOCKItime = CKOCKIdate.ToShortTimeString
        CKOCKItime = CKOCKItime.Replace("a.m.", "AM")
        CKOCKItime = CKOCKItime.Replace("p.m.", "PM")
        Return CKOCKIdatestring
    End Function

    Public Shared Function DateOldFormat(ByVal strdate As String) As String
        Try

            Dim aryDay As String() = strdate.Split("-")
            Return aryDay(2) & "/" & aryDay(1) & "/" & aryDay(0)
        Catch
        End Try
    End Function

    Public Shared Function DateNewFormat(ByVal strdate As String) As String
        Try

            Dim aryDay As String() = strdate.Split("-")
            Return aryDay(0) & "-" & aryDay(1) & "-" & aryDay(2)
        Catch
        End Try
    End Function

    Public Shared Function ParseDateAsDayWord(ByVal strDate As String) As String
        Try

            'Dim aryDay As String() = strDate.Split("-")
            ' ''Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            'Dim dt As New DateTime(CInt(aryDay(0)), CInt(aryDay(1)), CInt(aryDay(2)))
            'Return dt.ToString("dddd").Substring(0, 3)

            ''rev:mia 14July
            Return Aurora.Common.Utility.ParseDateTime(strDate, Aurora.Common.Utility.SystemCulture).ToString("dddd").Substring(0, 3)
        Catch

        End Try
    End Function

    Public Shared Function ParseRemainingDay(ByVal strdate As String) As String
        Try
            ''rev:mia 14July
            'Dim aryDay As String() = strdate.Split("-")
            ' ''Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            'Dim dt As New DateTime(CInt(aryDay(0)), CInt(aryDay(1)), CInt(aryDay(2)))
            ''Return DateDiff(DateInterval.Day, DateTime.Now, dt)

            Return DateDiff(DateInterval.Day, ParseDateTime(DateTime.Now, SystemCulture), ParseDateTime(strdate, SystemCulture))


        Catch
        End Try
    End Function

    Public Shared Function NewParseRemainingDay(ByVal strDate As String) As String
        Try
            Dim aryDay As String() = strDate.Split("-")
            ''Dim dt As New DateTime(CInt(aryDay(2)), CInt(aryDay(1)), CInt(aryDay(0)))
            Dim dt As New DateTime(CInt(aryDay(0)), CInt(aryDay(1)), CInt(aryDay(2)))
            Return dt.ToShortDateString
        Catch
        End Try
    End Function
    Public Shared Function Splitter(ByVal strToSplit As String) As String
        If strToSplit.Contains("-") Then
            Dim strArray As String() = strToSplit.Split("-")
            Return strArray(0).Trim
        Else
            Return strToSplit.Trim
        End If
    End Function

    Public Shared Function IsValidXMLstring(ByVal strXml As String) As Boolean
        Try
            Dim xmldoc As New System.Xml.XmlDocument
            xmldoc.LoadXml(strXml)
            xmldoc = Nothing
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Shared Sub BlankValueInDropdown(ByVal ddlControl As DropDownList, Optional ByVal controlname As String = "")
        Dim item As ListItem = Nothing
        If ddlControl.ID = "ddlContact" Then
            item = New ListItem(" ", 0)
        Else
            item = New ListItem(" ", 0)
        End If

        ddlControl.Items.Insert(0, item)
        ddlControl.SelectedIndex = 0
    End Sub

    Public Shared Function IsValidDropdownValue(ByVal ddlControl As DropDownList) As Boolean
        If String.IsNullOrEmpty(ddlControl.SelectedItem.Text) Then
            Return False
        End If
        If String.IsNullOrEmpty(ddlControl.SelectedValue.ToString) Then
            Return False
        End If
        Return True
    End Function

    Public Shared Function ParseEvalItemToBoolean(ByVal objCurr As Object, Optional ByVal currentrequest As Object = Nothing) As Boolean
        If objCurr Is DBNull.Value Then
            Return False
        End If
        If objCurr Is Nothing Then
            Return False
        End If
        If objCurr = 0 Then
            If CType(currentrequest, String).StartsWith("Current") And objCurr = 0 Then
                Return False
            End If
            Return True
        End If
        If objCurr = 1 Then

            Return False
        End If

    End Function

    Public Shared Function DispClientMessage(ByRef xmlErrorMsg As XmlDocument, ByVal msgcode As String, ByVal msgparam As String) As String
        Dim msg As String = String.Empty
        Dim retvalue As String = String.Empty
        Dim param As String = String.Empty
        Dim paramString() As String = Nothing
        Dim i As Integer

        Dim ctr As Integer = xmlErrorMsg.DocumentElement.ChildNodes(0).ChildNodes.Count - 1
        For i = 0 To ctr
            msg = xmlErrorMsg.DocumentElement.SelectSingleNode("Msg").InnerText ''Me.xmlErrorMsg.DocumentElement.ChildNodes(0).ChildNodes(i).ChildNodes(0).InnerText
            retvalue = msg.IndexOf(msgcode)
            If retvalue <> -1 Then
                Exit For
            End If
        Next

        If i = ctr Then
            Return String.Empty
        End If

        param = msgparam
        If Not String.IsNullOrEmpty(param) Then
            paramString = param.Split(",")
            For ii As Integer = 0 To paramString.Length - 1
                If Not String.IsNullOrEmpty(paramString(ii)) Then
                    msg = msg.Replace("xxx" & (ii + 1), paramString(ii))
                End If
            Next
        End If

        For iii As Integer = 0 To paramString.Length - 1
            msg = msg.Replace("xxx" & (iii + 1), "")
        Next

        Return msg
    End Function

    Public Shared Function AddIDinChosenVehicle(ByVal xmlstring As String, ByVal xpath As String, ByVal nodename As String) As String
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(xmlstring)
        Dim xmlatt As XmlAttribute
        Dim elem As XmlElement = xmldoc.DocumentElement

        Dim nodes As XmlNodeList = elem.SelectNodes(xpath)
        For Each node As XmlNode In nodes
            If node.Name = "SelectedVehicle" Then
                xmlatt = xmldoc.CreateAttribute(nodename)
                If Not node.ChildNodes(0).ChildNodes(0) Is Nothing Then
                    xmlatt.Value = node.ChildNodes(0).ChildNodes(0).InnerText
                Else
                    xmlatt.Value = ""
                End If
                node.Attributes.Append(xmlatt)
            End If
        Next
        Return xmldoc.OuterXml
    End Function
End Class
