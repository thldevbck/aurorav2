'--rev:mia july 4 2012 - start-- addition of 3rd party field

Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class Booking

    Public Shared Function GetSrcClassComboData(ByVal userCode As String) As DataTable
        'Return Aurora.Common.Data.GetComboData("SrcCLASS", "", "", userCode)
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetComboData("SrcCLASS", "", "", userCode)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function

    Public Shared Function GetBrandData(ByVal userCode As String) As DataTable
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetData("BRAND", "", "", userCode)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function
    Public Shared Function GetCountryData(ByVal userCode As String) As DataTable
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetData("COUNTRYWITHPRODUCT", "", "", userCode)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count = 1 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function

    Public Shared Function GetBookingStatusList() As DataTable
        Return Data.DataRepository.GetBookingStatusList()
    End Function

    '--rev:mia july 4 2012 - start-- addition of 3rd party field
    Public Shared Function SearchBookingRental(ByVal bookingStatus As String, ByVal oldBookingNumber As String, _
      ByVal rentalStatus As String, ByVal oldRentalNumber As String, ByVal brandCode As String, ByVal packageId As String, _
      ByVal bookingCity As String, ByVal pickupFromDate As DateTime, ByVal pickupToDate As DateTime, ByVal vehicleClass As String, _
      ByVal checkOutLocationCode As String, ByVal unitNumber As String, ByVal dropOffFromDate As DateTime, _
      ByVal dropOffToDate As DateTime, ByVal regoNumber As String, ByVal checkInLocationCode As String, _
      ByVal arrivalConnectionRef As String, ByVal departureConnectionRef As String, ByVal hirerLastName As String, _
      ByVal agentCode As String, ByVal hirerAddress As String, ByVal agentRef As String, ByVal hirerState As String, _
      ByVal voucherNumber As String, ByVal hirerTown As String, ByVal hirerCity As String, ByVal bookedFromDate As DateTime, _
      ByVal bookedToDate As DateTime, ByVal customerLastName As String, ByVal customerFirstName As String, _
      ByVal inError As String, ByVal agentType As String, ByVal debtorStatus As String, ByVal productShortName As String, _
      ByVal fleetModel As String, ByVal userCode As String, Optional ByVal thirdParty As String = "") As DataSet

        Return Data.DataRepository.SearchBookingRental(bookingStatus, oldBookingNumber, rentalStatus, oldRentalNumber, brandCode, packageId, bookingCity, _
            pickupFromDate, pickupToDate, vehicleClass, checkOutLocationCode, unitNumber, dropOffFromDate, _
            dropOffToDate, regoNumber, checkInLocationCode, arrivalConnectionRef, departureConnectionRef, _
            hirerLastName, agentCode, hirerAddress, agentRef, hirerState, voucherNumber, hirerTown, hirerCity, _
            bookedFromDate, bookedToDate, customerLastName, customerFirstName, inError, agentType, debtorStatus, _
            productShortName, fleetModel, userCode, thirdParty) '--rev:mia july 4 2012 - start-- addition of 3rd party field

    End Function

    Public Shared Function GetBookingDetail(ByVal bookingId As String, ByVal condition As String, ByVal rentalId As String, _
        ByVal bookingNumber As String, ByVal userCode As String) As DataSet

        Return Data.DataRepository.GetBookingDetail(bookingId, condition, rentalId, bookingNumber, userCode)

    End Function

    Public Shared Function GetMaxRentalNumber(ByVal bookingId As String) As Integer

        Dim maxRental As Integer = 0
        Dim ds As DataSet

        ds = Data.DataRepository.GetMaxRentalNumber(bookingId)
        maxRental = Utility.ParseInt(ds.Tables(0).Rows(0)("MaxRental"), 0)

        Return maxRental
    End Function

    Public Shared Function GetReferralType() As DataSet
        Return Aurora.Common.Data.GetCodecodetype("19", "")
    End Function

    Public Shared Function GetBookingDetail2(ByVal bookingId As String, ByVal userCode As String) As DataSet

        Return Data.DataRepository.GetBookingDetail2(bookingId, userCode)

    End Function

    ''rev:mia 15jan2014-addition of OPTIONALS for  supervisor override password
    Public Shared Function SaveBooking(ByVal bookingId As String, ByVal agentId As String, _
        ByVal firstName As String, ByVal lastName As String, ByVal title As String, ByVal refType As String, _
        ByVal oldAgentId As String, ByVal intNo As String, ByVal userCode As String, _
        Optional SlotId As String = "", _
        Optional SlotDescription As String = "", _
        Optional RentalId As String = "") As String

        Dim returnError As String = ""
        Dim returnMessage As String = ""
        Dim updateFleet As String = ""
        Dim blrIdList As String = ""

        Dim passIntegrityCheck As Boolean
        passIntegrityCheck = CheckIntegrity(bookingId, 0, intNo, "", "")

        If passIntegrityCheck Then

            Using transaction As New Aurora.Common.Data.DatabaseTransaction()

                ' Update product
                Data.DataRepository.ManageBookedProductModBoo(bookingId, agentId, userCode, "SAVE", returnError, returnMessage, updateFleet, blrIdList)

                If String.IsNullOrEmpty(returnError) Then

                    ' update booking
                    ''rev:mia 15jan2014-addition of SlotId, SlotDescription, RentalId
                    returnError = Data.DataRepository.ManageBooking(bookingId, firstName, lastName, title, refType, agentId, oldAgentId, intNo, userCode, "SAVE", SlotId, SlotDescription, RentalId)

                    'update booking int no
                    Aurora.Booking.Data.DataRepository.UpdateBookingIntegrityNumber(bookingId)

                    transaction.CommitTransaction()
                End If
                Return returnError
            End Using
        Else
            returnError = "This booking has been modified by another user. Please refresh the screen."
        End If
        Return returnError
    End Function

    Public Shared Function ValidateBooking(bookingId as String, rentalId as String, userCode as String) As String
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetPopUpData("VALIDATEBOOKING", bookingId, rentalId, userCode, "", "", userCode)
        Return ds.Tables(0).Rows(0)("Error")
        '<Error></Error><Wrn></Wrn>
    End Function

    Public Shared Sub ValidateBooking1(ByVal bookingId As String, ByVal rentalId As String, ByVal userCode As String, _
        ByRef errorMessage As String, ByRef warningMessage As String)
        Dim ds As DataSet
        ds = Aurora.Common.Data.GetPopUpData("VALIDATEBOOKING", bookingId, rentalId, userCode, "", "", userCode)
        errorMessage = ds.Tables(0).Rows(0)("Error")
        warningMessage = ds.Tables(0).Rows(0)("Wrn")
        '<Error></Error><Wrn></Wrn>
    End Sub

    Public Shared Function ShowBookingRequestAgent(ByVal agentCode As String) As DataSet

        Return Data.DataRepository.ShowBookingRequestAgent(agentCode)
    End Function

    Public Shared Function GetBookingRequestAgent(ByVal bookingRequestId As String, ByVal bookingId As String) As XmlDocument

        Return Data.DataRepository.GetBookingRequestAgent(bookingRequestId, bookingId)

    End Function

    Public Shared Function UpdateBookingRequestAgent(ByVal xmlDoc As XmlDocument, ByVal parentTable As String) As String

        Dim returnMessage As String = Data.DataRepository.UpdateBookingRequestAgent(xmlDoc, parentTable)
        returnMessage = returnMessage.Replace("<data>", "")
        returnMessage = returnMessage.Replace("</data>", "")
        Return returnMessage

    End Function

    ''' <summary>
    ''' This is the almighty Check Integrity function. If true, the data you have is current, go ahead and update.else ask the user to refresh the screen.
    ''' </summary>
    ''' <param name="RentalId">Optional - RentalId</param>
    ''' <param name="BPDId">Optional - Booked Product Id</param>
    ''' <param name="BooID">Booking Id</param>
    ''' <param name="ScreenRentalIntegrityNo">Rental Integrity Number from screen</param>
    ''' <param name="ScreenBookingIntegrityNo">Booking Integrity Number from screen</param>
    ''' <returns>True - Continue processing, False - Abort processing</returns>
    ''' <remarks></remarks>
    Public Shared Function CheckIntegrity(ByVal BooID As String, ByVal ScreenRentalIntegrityNo As Integer, _
                                          ByVal ScreenBookingIntegrityNo As Integer, Optional ByVal RentalId As String = "", _
                                          Optional ByVal BPDId As String = "") As Boolean

        Dim bDataIsCurrent As Boolean
        Dim nDataBaseRentalIntegrityNo As Integer
        Dim nDataBaseBooIntNo

        bDataIsCurrent = False

        'call db
        DataRepository.GetIntegrityNumber(nDataBaseRentalIntegrityNo, nDataBaseBooIntNo, RentalId, BPDId, BooID)

        If nDataBaseRentalIntegrityNo = ScreenRentalIntegrityNo Then
            ' if passed first check
            ' look at the booking integrity
            If nDataBaseBooIntNo = ScreenBookingIntegrityNo Then
                bDataIsCurrent = True
            Else
                bDataIsCurrent = False
            End If
        Else

            'failed very first check!
            bDataIsCurrent = False
        End If

        Return bDataIsCurrent

    End Function




End Class
