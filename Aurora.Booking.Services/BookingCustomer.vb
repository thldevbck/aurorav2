Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookingCustomer

    Public Shared Function GetCustomerSummary(ByVal thisRental As String, ByVal bookingId As String, ByVal rentalId As String) As DataTable
        Dim ds As DataSet
        ds = Data.DataRepository.GetCustomerSummary(thisRental, bookingId, rentalId)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 1 Then
            dt = ds.Tables(1)
        End If
        Return dt
    End Function

    Public Shared Function UpdateCustomerSummary(ByVal bookingId As String, ByVal rentalId As String, ByVal rentalCustomerDt As DataTable, _
        ByVal removeCustomerCol As Collection, ByVal msg As String, ByVal userCode As String, ByVal programName As String, Optional ByVal isBp2bb As Boolean = False) As String

        'Validations
        Dim noOfPriHirer As Integer = 0
        For Each rr As DataRow In rentalCustomerDt.Rows
            noOfPriHirer = noOfPriHirer + Utility.ParseInt(rr("PriHirer"), 0)
        Next
        If noOfPriHirer = 0 Then
            Return "GEN132"
        ElseIf noOfPriHirer > 1 Then
            Return "GEN133"
        End If


        Dim xmlDoc As XmlDocument = New XmlDocument
        xmlDoc.LoadXml("<Data><ThisRental></ThisRental><Remove></Remove></Data>")

        Dim root As XmlElement = xmlDoc.DocumentElement.FirstChild

        For Each rr As DataRow In rentalCustomerDt.Rows


            Dim rentalNode As XmlNode = xmlDoc.CreateNode("element", "Rental", "")

            Dim cusId As String = Utility.ParseString(rr("CusId"), "")
            Dim custIdNode As XmlNode = xmlDoc.CreateNode("element", "CusId", "")
            custIdNode.InnerText = cusId
            rentalNode.AppendChild(custIdNode)

            Dim name As String = Utility.ParseString(rr("CName"), "")
            Dim nameNode As XmlNode = xmlDoc.CreateNode("element", "CName", "")
            nameNode.InnerText = name
            rentalNode.AppendChild(nameNode)

            Dim address As String = Utility.ParseString(rr("Address"), "")
            Dim addressNode As XmlNode = xmlDoc.CreateNode("element", "Address", "")
            addressNode.InnerText = address
            rentalNode.AppendChild(addressNode)

            Dim hirer As String = Utility.ParseString(rr("Hirer"), "")
            Dim hirerNode As XmlNode = xmlDoc.CreateNode("element", "Hirer", "")
            hirerNode.InnerText = hirer
            rentalNode.AppendChild(hirerNode)

            Dim priHirer As String = Utility.ParseString(rr("PriHirer"), "")
            Dim priHirerNode As XmlNode = xmlDoc.CreateNode("element", "PriHirer", "")
            priHirerNode.InnerText = priHirer
            rentalNode.AppendChild(priHirerNode)

            Dim isDriver As String = Utility.ParseString(rr("IsDriver"), "")
            Dim isDriverNode As XmlNode = xmlDoc.CreateNode("element", "IsDriver", "")
            isDriverNode.InnerText = isDriver
            rentalNode.AppendChild(isDriverNode)

            Dim issueAuthority As String = Utility.ParseString(rr("IssueAuthority"), "")
            Dim issueAuthorityNode As XmlNode = xmlDoc.CreateNode("element", "IssueAuthority", "")
            issueAuthorityNode.InnerText = issueAuthority
            rentalNode.AppendChild(issueAuthorityNode)

            Dim dob As String = Utility.ParseString(rr("DOB"), "")
            Dim dobNode As XmlNode = xmlDoc.CreateNode("element", "DOB", "")
            dobNode.InnerText = dob
            rentalNode.AppendChild(dobNode)

            Dim licNo As String = Utility.ParseString(rr("LicNo"), "")
            Dim licNoNode As XmlNode = xmlDoc.CreateNode("element", "LicNo", "")
            licNoNode.InnerText = licNo
            rentalNode.AppendChild(licNoNode)

            Dim expiryDt As String = Utility.ParseString(rr("ExpiryDt"), "")
            Dim expiryDtNode As XmlNode = xmlDoc.CreateNode("element", "ExpiryDt", "")
            expiryDtNode.InnerText = expiryDt
            rentalNode.AppendChild(expiryDtNode)

            Dim check As String = Utility.ParseString(rr("Check"), "")
            Dim checkNode As XmlNode = xmlDoc.CreateNode("element", "Check", "")
            checkNode.InnerText = check
            rentalNode.AppendChild(checkNode)

            Dim addable As String = Utility.ParseString(rr("Addable"), "")
            Dim addableNode As XmlNode = xmlDoc.CreateNode("element", "Addable", "")
            addableNode.InnerText = addable
            rentalNode.AppendChild(addableNode)

            Dim rntId As String = Utility.ParseString(rr("RntId"), "")
            Dim rntIdNode As XmlNode = xmlDoc.CreateNode("element", "RntId", "")
            rntIdNode.InnerText = rntId
            rentalNode.AppendChild(rntIdNode)

            Dim passportNo As String = Utility.ParseString(rr("PassportNo"), "")
            Dim passportNoNode As XmlNode = xmlDoc.CreateNode("element", "PassportNo", "")
            passportNoNode.InnerText = passportNo
            rentalNode.AppendChild(passportNoNode)

            Dim passportCountry As String = Utility.ParseString(rr("PassportCountry"), "")
            Dim passportCountryNode As XmlNode = xmlDoc.CreateNode("element", "PassportCountry", "")
            passportCountryNode.InnerText = passportCountry
            rentalNode.AppendChild(passportCountryNode)

            Dim ageNode As XmlNode = xmlDoc.CreateNode("element", "Age", "")
            ageNode.InnerText = ""
            rentalNode.AppendChild(ageNode)

            ' Change by Shoel on 10.6.10 for Loyalty Card info
            Dim loyaltyCardNumber As String = Utility.ParseString(rr("LoyaltyCardNumber"), "")
            Dim loyaltyCardNumberNode As XmlNode = xmlDoc.CreateNode("element", "LoyaltyCardNumber", "")
            loyaltyCardNumberNode.InnerText = loyaltyCardNumber
            rentalNode.AppendChild(loyaltyCardNumberNode)
            ' Change by Shoel on 10.6.10 for Loyalty Card info

            If isDriver = "1" Then
                If String.IsNullOrEmpty(licNo) Then
                    Return "GEN134"
                ElseIf String.IsNullOrEmpty(dob) Then
                    Return "GEN135"
                ElseIf String.IsNullOrEmpty(issueAuthority) Then
                    Return "GEN136"
                ElseIf String.IsNullOrEmpty(expiryDt) Then
                    Return "GEN137"
                End If
            End If
            If Not String.IsNullOrEmpty(licNo) Then
                If String.IsNullOrEmpty(dob) Then
                    Return "GEN138"
                ElseIf String.IsNullOrEmpty(issueAuthority) Then
                    Return "GEN139"
                ElseIf String.IsNullOrEmpty(expiryDt) Then
                    Return "GEN140"
                End If
            End If

            root.AppendChild(rentalNode)

        Next


        Dim root1 As XmlElement = xmlDoc.DocumentElement.LastChild

        For Each s As String In removeCustomerCol

            Dim recoNode As XmlNode = xmlDoc.CreateNode("element", "Reco", "")

            Dim idNode As XmlNode = xmlDoc.CreateNode("element", "ID", "")
            idNode.InnerText = s
            recoNode.AppendChild(idNode)

            root1.AppendChild(recoNode)

        Next

        Dim root2 As XmlElement = xmlDoc.DocumentElement
        Dim msgNode As XmlNode = xmlDoc.CreateNode("element", "Msg", "")
        msgNode.InnerText = msg
        root2.AppendChild(msgNode)
        Dim addModUserIdNode As XmlNode = xmlDoc.CreateNode("element", "AddModUserId", "")
        addModUserIdNode.InnerText = userCode
        root2.AppendChild(addModUserIdNode)
        Dim addProgramNameNode As XmlNode = xmlDoc.CreateNode("element", "AddProgramName", "")
        addProgramNameNode.InnerText = programName
        root2.AppendChild(addProgramNameNode)


        Dim errorMessage As String
        ''rev:mia jan 21 2009 bypass bp2bb
        If Not isBp2bb Then
            errorMessage = Data.DataRepository.UpdateCustomerSummary(bookingId, rentalId, xmlDoc)
        Else
            errorMessage = Data.DataRepository.UpdateCustomerSummaryAndBypassBP2BB(bookingId, rentalId, xmlDoc)
        End If


        Return errorMessage

    End Function

    Public Shared Function GetDrivers(ByVal rentalId As String) As DataTable
        Dim ds As DataSet
        ds = Data.DataRepository.GetDrivers(rentalId)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 1 Then
            dt = ds.Tables(1)
        End If
        Return dt
    End Function

    Public Shared Function FindDriverByLicence(ByVal rentalId As String, ByVal licence As String, ByVal dob As Date) As DataTable
        Dim ds As DataSet
        ds = Data.DataRepository.FindDriverByLicence(rentalId, licence, dob)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 1 Then
            dt = ds.Tables(1)
        End If
        Return dt
    End Function

    Public Shared Function DeleteDriver(ByVal customerId As String, ByVal rentalId As String) As String

        Dim errorMessage As String = ""
        Data.DataRepository.DeleteDriver(customerId, rentalId, errorMessage)
        Return errorMessage
    
    End Function

    Public Shared Function ValidateDriver(ByVal rentalId As String, ByVal dob As Date, ByVal expDate As Date) As String
        Dim errorMessage As String = ""
        Data.DataRepository.ValidateDriver(rentalId, dob, expDate, errorMessage)
        Return errorMessage
    End Function

    Public Shared Function UpdateCustomer(ByVal customerId As String, ByVal firstName As String, _
        ByVal lastName As String, ByVal isVip As Boolean, ByVal fleetDriver As Boolean, _
        ByVal personType As String, ByVal title As String, ByVal dob As Date, ByVal licence As String, _
        ByVal expiryDate As Date, ByVal issuingAuthority As String, ByVal gender As String, _
        ByVal occupation As String, ByVal codPrefModOfCommId As String, ByVal receiveMailLists As Boolean, _
        ByVal receiveSurveys As Boolean, ByVal integrityNo As Integer, ByVal passportNo As String, _
        ByVal passportCountry As String, ByVal rentalId As String, ByVal addUserId As String, _
        ByVal modUserId As String, ByVal programName As String, ByVal loyaltyCardNumber As String) As String

        Return Data.DataRepository.UpdateCustomer(customerId, firstName, lastName, isVip, fleetDriver, _
            personType, title, dob, licence, expiryDate, issuingAuthority, gender, _
            occupation, codPrefModOfCommId, receiveMailLists, receiveSurveys, integrityNo, passportNo, _
            passportCountry, rentalId, addUserId, modUserId, programName, loyaltyCardNumber)

    End Function

    Public Shared Function UpdateDriver(ByVal customerId As String, ByVal firstName As String, ByVal lastName As String, _
        ByVal title As String, ByVal dob As Date, ByVal licence As String, ByVal expiryDate As Date, _
        ByVal issuingAuthority As String, ByVal integrityNo As Integer, ByVal addUserId As String, _
        ByVal modUserId As String, ByVal programName As String) As String

        Return Data.DataRepository.UpdateDriver(customerId, firstName, lastName, title, dob, licence, _
            expiryDate, issuingAuthority, integrityNo, addUserId, modUserId, programName)

    End Function

    Public Shared Function UpdateTraveller(ByVal customerId As String, ByVal rentalId As String, ByVal isDriver As Boolean, _
          ByVal isHirer As Boolean, ByVal isPrimaryHirer As Boolean, ByVal addUserId As String, ByVal modUserId As String, ByVal programName As String) As String

        Return Data.DataRepository.UpdateTraveller(customerId, rentalId, isDriver, isHirer, isPrimaryHirer, addUserId, modUserId, programName)

    End Function

    Public Shared Function FindCustomer(ByVal search As Integer, ByVal lastName As String, _
        ByVal firstName As String, ByVal dob As Date, ByVal isVip As Boolean, ByVal street As String, _
        ByVal city As String, ByVal state As String, ByVal country As String, ByVal licenceNo As String, _
        ByVal isFleet As Boolean, ByVal age As Integer, ByVal userCode As String) As DataTable

        Dim ds As DataSet
        ds = Data.DataRepository.FindCustomer(search, lastName, firstName, dob, _
            isVip, street, city, state, country, licenceNo, isFleet, age, userCode)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt
        'Return ds

    End Function

    Public Shared Function FleetDriverCheckBoxEnable(ByVal userCode As String) As Boolean

        Dim ds As DataSet
        ds = Data.DataRepository.FleetDriverCheckBoxEnable(userCode)
        'Dim dt As DataTable
        'dt = New DataTable
        If ds.Tables.Count > 0 Then
            'dt = ds.Tables(0)
            If (Convert.ToBoolean(ds.Tables(0).Rows(0)("fleetDrivercheckBoxEnable") = "YES")) Then
                Return True
            Else
                Return False
            End If
        End If
        Return False

    End Function

    Public Shared Function GetCustomerDetails(ByVal customerId As String) As DataSet

        Dim ds As DataSet
        ds = Data.DataRepository.GetCustomerDetails(customerId)
        Return ds

    End Function

    Public Shared Function ValidateAddress(ByVal action As String, ByVal tableName As String, ByVal agentId As String, _
           ByVal typeId As String, ByVal city As String, ByVal state As String) As String

        Dim returnError As String = ""
        Data.DataRepository.ValidateAddress(action, tableName, agentId, typeId, city, state, returnError)
        Return returnError

    End Function

    Public Shared Function UpdateAddress(ByVal queryAction As String, ByVal id As String, ByVal tableName As String, _
           ByVal address1 As String, ByVal typeId As String, ByVal prntId As String, ByVal address2 As String, _
           ByVal postCode As String, ByVal cityCode As String, ByVal address3 As String, ByVal stateCode As String, _
           ByVal integrityNo As Integer, ByVal userCode As String, ByVal programName As String) As String

        Dim returnError As String = ""
        Data.DataRepository.UpdateAddress(queryAction, id, tableName, address1, typeId, prntId, address2, _
            postCode, cityCode, address3, stateCode, integrityNo, userCode, programName, returnError)
        Return returnError

    End Function

    Public Shared Function ValidatePhoneNumber(ByVal action As String, ByVal tableName As String, ByVal agentId As String, _
        ByVal typeId As String, ByVal phoneNumber As String, ByVal email As String) As String

        Dim returnError As String = ""
        Data.DataRepository.ValidatePhoneNumber(action, tableName, agentId, typeId, phoneNumber, email, returnError)
        Return returnError

    End Function

    Public Shared Function UpdatePhoneNumber(ByVal queryAction As String, ByVal phnId As String, ByVal tableName As String, _
       ByVal phnPrnId As String, ByVal typeId As String, ByVal areaCode As String, ByVal countryCode As String, ByVal number As String, ByVal integrityNo As Integer, _
       ByVal email As String, ByVal userCode As String, ByVal programName As String) As String

        Dim returnError As String = ""
        Data.DataRepository.UpdatePhoneNumber(queryAction, phnId, tableName, phnPrnId, typeId, _
            areaCode, countryCode, number, integrityNo, email, userCode, programName, returnError)
        Return returnError

    End Function

    Public Shared Function UpdateCustomerBrochure(ByVal brochureId As String, ByVal customerId As String, ByVal qty As Integer, _
           ByVal addUserId As String, ByVal modUserId As String, ByVal programName As String) As String

        Dim returnError As String = ""
        Data.DataRepository.UpdateCustomerBrochure(brochureId, customerId, qty, addUserId, modUserId, programName)
        Return returnError

    End Function

    Public Shared Function ValidateDeletion(ByVal customerId As String, ByVal rentalId As String) As String

        Dim returnError As String = ""
        Data.DataRepository.ValidateDeletion(customerId, rentalId, returnError)
        Return returnError

    End Function

    Public Shared Function DeleteCustomer(ByVal customerId As String) As String

        Dim returnError As String = ""
        Data.DataRepository.DeleteCustomer(customerId, returnError)
        Return returnError

    End Function


#Region "rev:mia Sept.2 2013 - Customer Title from query"
    Public Shared Function GetCustomerTitle() As DataSet
        Return Data.DataRepository.GetCustomerTitle
    End Function

    Public Shared Function UpdateCustomerTitle(CusFirstName As String, CusLastName As String, CusTitle As String, CusId As String) As Boolean
        Return Data.DataRepository.UpdateCustomerTitle(CusFirstName.TrimEnd, CusLastName.TrimEnd, CusTitle.TrimEnd, CusId.TrimEnd)
    End Function

    Public Shared Function GetCustomerTitleAndName(CusId As String) As DataSet
        Return Data.DataRepository.GetCustomerTitleAndName(CusId.TrimEnd)
    End Function

#End Region


End Class
