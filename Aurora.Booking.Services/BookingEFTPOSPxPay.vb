﻿Imports System.Text
Imports System.Xml
Imports System.Web
Imports System.IO
Imports System.Net
Imports System.Configuration
Imports Aurora.Common.Logging


''called by EftposPXPayListener.aspx.vb 
Public Class BookingEFTPOSPxPay
    Private _WebServiceUrl As String
    Private _PxPayUserId As String


    Public ReadOnly Property PxPayUserId As String
        Get
            Return _PxPayUserId
        End Get
    End Property

    Private _PxPayKey As String
    Public ReadOnly Property PxPayKey As String
        Get
            Return _PxPayKey
        End Get
    End Property

    Public Property CountryCode As String

    Public Property UserName As String

    Public Property Password As String

    Sub New(ByVal country As String, ByVal id As String)
        Dim xml As New XmlDocument
        xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))

        _PxPayUserId = xml.SelectSingleNode("DpsBillingToken/Country[@code='" + country + "']/UserName").InnerText
        _PxPayKey = xml.SelectSingleNode("DpsBillingToken/Country[@code='" + country + "']/Password").InnerText
        _WebServiceUrl = ConfigurationManager.AppSettings("PaymentExpress.PxPay")
        CountryCode = country
        UserName = id
    End Sub


    Sub New(ByVal userCode As String, _
            ByVal rentalId As String, _
            ByVal country As String)

        Dim values As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_getDefValuesForUser", userCode, rentalId).OuterXml
        Dim xml As New XmlDocument
        xml.LoadXml(values)

        Dim brdcode As String = ""
        Dim userCurrency As String = ""
        Dim ComCode As String = ""

        Try


            If Not (xml.SelectSingleNode("data/UserInfo/BrdCode")) Is Nothing Then
                brdcode = xml.SelectSingleNode("data/UserInfo/BrdCode").InnerText
            End If

            If Not (xml.SelectSingleNode("data/UserInfo/BrdCode")) Is Nothing Then
                userCurrency = xml.SelectSingleNode("data/UserInfo/UsrCtyCode").InnerText
            End If


            ComCode = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getSettings", userCode).SelectSingleNode("data/UserInfo/ComCode").InnerText

            MapPasswordsForBrands(UserName, Password, ComCode, userCurrency, brdcode)
            _WebServiceUrl = ConfigurationManager.AppSettings("PaymentExpress.PxPay")

            _PxPayUserId = UserName
            _PxPayKey = Password

            _WebServiceUrl = ConfigurationManager.AppSettings("PaymentExpress.PxPay")


            LogInformation("(BILLING TOKEN)", String.Concat("PxPayUserId: ", _PxPayUserId, " , PxPayKey: ", _PxPayKey, " , COMCODE: ", ComCode, " , USERCURRENCY: ", userCurrency, " , BRDCODE: ", brdcode, " , USERCODE: ", userCode))

            CountryCode = country
            UserName = userCode
        Catch ex As Exception
            LogInformation("(BILLING TOKEN ERROR)", String.Concat("PxPayUserId: ", _PxPayUserId, " , PxPayKey: ", _PxPayKey, " , COMCODE: ", ComCode, " , USERCURRENCY: ", userCurrency, " , BRDCODE: ", brdcode, " , USERCODE: ", userCode, " EXCEPTION: ", ex.Message + ", TRACE: ", ex.StackTrace))
        End Try

    End Sub

    Sub MapPasswordsForBrands(ByRef username As String, _
                             ByRef password As String, _
                             ByVal _comCode As String, _
                             ByVal _currencyType As String, _
                             ByVal _brdCode As String)

        Dim xml As New XmlDocument
        xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))

        ' ''test
        '_brdCode = ConfigurationManager.AppSettings("BRDCodeTest")
        '_currencyType = ConfigurationManager.AppSettings("currencyTest")
        '_comCode = ConfigurationManager.AppSettings("comCodeTest")

        Dim path As String = "DpsBillingToken/Brands/"

        Try

        If _comCode.Equals("THL") = True Then
            If _currencyType.Equals("NZ") = True Then
                If _brdCode.Equals("B") = True Then
                    username = xml.SelectSingleNode(path + "britz_username").InnerText
                    password = xml.SelectSingleNode(path + "britz_password").InnerText
                Else ''maui
                    username = xml.SelectSingleNode(path + "maui_username").InnerText
                    password = xml.SelectSingleNode(path + "maui_password").InnerText

                End If
            Else ''rentals
                username = xml.SelectSingleNode(path + "rentals_username").InnerText
                password = xml.SelectSingleNode(path + "rentals_password").InnerText
            End If

        Else ''kxz ''------ If _comcode.Equals("THL") = True Then
            If _comCode.Equals("KXS") = True Then '------ If _comcode.Equals("KX") = True Then
                username = xml.SelectSingleNode(path + "kxs_username").InnerText
                password = xml.SelectSingleNode(path + "kxs_password").InnerText
            Else
                ' V2
                ' RKS MOD: 21-Sep-2009 for MAC.COM
                If _comCode.ToUpper().Equals("MAC") = True Then '------ If _comcode.Equals("MAC") = True Then

                    If _currencyType.Equals("NZ") = True Then '------ If _comcode.Equals("NZ") = True Then
                        username = xml.SelectSingleNode(path + "mac_nz_username").InnerText
                        password = xml.SelectSingleNode(path + "mac_nz_password").InnerText
                    Else
                        If _currencyType.Equals("AU") = True Then
                            username = xml.SelectSingleNode(path + "mac_au_username").InnerText
                            password = xml.SelectSingleNode(path + "mac_au_password").InnerText
                        End If
                    End If '------ If _comcode.Equals("NZ") = True Then
                End If
            End If '' '------   If _comcode.Equals("KX") = True Then
        End If ''''kxz ''------ If _comcode.Equals("THL") = True Then

        Catch ex As Exception
            LogError("(MapPasswordsForBrands)", String.Concat("INVALID CREDENTIALS: USERNAME: ", username, " , PASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode, " EXCEPTION: ", ex.Message + ", TRACE: ", ex.StackTrace))
        End Try

    End Sub
    Sub New()

    End Sub

    Public Function GetAllPXPAyToken(ByVal rentalId As String) As String
        Dim result As New DataSet
        Dim sb As New StringBuilder
        Try

        Aurora.Common.Data.ExecuteDataSetSP("BillingTokenSelect", result, rentalId)
            sb.Append("<div>")

        sb.Append("<table style='border-width: 1px; background-color: #FFF' border='1' width='100%'>")

        Dim thstyle As String = "style='border-bottom-width: 1px;margin-top: 3px;margin-bottom: 3px;'"
            sb.Append("<tr  valign='baseline'>")
        sb.AppendFormat("<th align='left' {0}>{1}</th>", thstyle, "Card Type")
        sb.AppendFormat("<th align='left' {0}>{1}</th>", thstyle, "Card Number")
        sb.AppendFormat("<th align='left' {0}>{1}</th>", thstyle, "CardHolder Name")
        sb.AppendFormat("<th align='left' {0}>{1}</th>", thstyle, "Expiry Date")

        sb.Append("</tr>")
        Dim i As Integer = 1
        Dim trstyle As String = ""
        Dim tdstyle As String = "style='font-size: small; font-weight: normal; font-style: normal; border-bottom-width: 1px; border-width: 1px;'"
        For Each rowItem As DataRow In result.Tables(0).Rows
            If i Mod 2 = 0 Then
                trstyle = "style='background-color: #FFF;'"
            Else
                trstyle = "style='background-color: #EEE;'"
            End If
            sb.AppendFormat("<tr {0}>", trstyle)
            sb.AppendFormat("<td {0}'>{1}</td>", tdstyle, ConvertIntoCamelCase(rowItem("PtmName").ToString))
            sb.AppendFormat("<td {0}'>{1}</td>", tdstyle, ConvertIntoCamelCase(rowItem("PmtBillCardNumber").ToString))
            sb.AppendFormat("<td {0}'>{1}</td>", tdstyle, ConvertIntoCamelCase(rowItem("PmtBillToCardName").ToString))
            sb.AppendFormat("<td {0}'>{1}</td>", tdstyle, ConvertIntoCamelCase(rowItem("PmtBillToCardExpDate").ToString.Insert(2, "/")))

            sb.Append("</tr>")
            i = i + 1
        Next
        sb.Append("</table>")
        sb.Append("</div>")


        Catch ex As Exception
            sb.Append("<div><table style='border-width: 1px; background-color: #FFF' border='1' width='100%'>")
            sb.Append("<tr>")
            sb.AppendFormat("<td>{0}</td>", ex.Message)
            sb.Append("</tr>")
            sb.Append("</table></div>")
            Return sb.ToString
        End Try

        Return sb.ToString
    End Function

    Private Function ConvertIntoCamelCase(ByVal value As String) As String
        Dim templower As String = value.ToLower
        Dim split As String() = templower.Split(" ")
        Dim sb As New StringBuilder
        Dim beginning As Char
        For Each wrd As String In split
            If Not (String.IsNullOrEmpty(wrd)) Then
                beginning = wrd.Substring(0, 1).ToUpper
                If split.Length = 0 Then
                    sb.Append(String.Concat(beginning, wrd.Remove(0, 1)))
                Else
                    sb.Append(String.Concat(beginning, wrd.Remove(0, 1), " "))
                End If
            End If

        Next
        Return sb.ToString
    End Function

    Private Function SubmitXml(ByVal InputXml As String) As String


        Dim webReq As HttpWebRequest = DirectCast(WebRequest.Create(_WebServiceUrl), HttpWebRequest)
        webReq.Method = "POST"
        Dim timeout As String = ConfigurationManager.AppSettings("PaymentXpress.TimeOut")
        Dim UseProxy As String = ConfigurationManager.AppSettings("UseProxy")
        Dim httpProxy As String = ConfigurationManager.AppSettings("httpProxy")

        If UseProxy Then
            Dim localProxy As New WebProxy
            localProxy.Address = New Uri(httpProxy)
            webReq.Proxy = localProxy
        End If

        If (String.IsNullOrEmpty(timeout)) Then timeout = 10000

        Try
            Dim reqBytes As Byte()

            reqBytes = System.Text.Encoding.UTF8.GetBytes(InputXml)
            webReq.ContentType = "text/xml"
            webReq.ContentLength = reqBytes.Length
            webReq.Timeout = CInt(timeout)
            Dim requestStream As Stream = webReq.GetRequestStream()
            requestStream.Write(reqBytes, 0, reqBytes.Length)
            requestStream.Close()

            Dim webResponse As HttpWebResponse = DirectCast(webReq.GetResponse(), HttpWebResponse)
            Using sr As New StreamReader(webResponse.GetResponseStream(), System.Text.Encoding.ASCII)
                Return sr.ReadToEnd()
            End Using
        Catch ex As Exception
            webReq = Nothing
            LogInformation("(BILLING TOKEN SUBMITXML)", String.Concat("EXCEPTION:", ex.Message, " STACKTRACE:", ex.StackTrace))
            Return "ERROR: " + ex.Message
        End Try

    End Function

    Public Function CaptureBillingToken(ByVal MerchantReference As String) As String
        Dim sw As New StringWriter()

        Dim settings As New XmlWriterSettings()
        settings.Indent = True
        settings.NewLineOnAttributes = False
        settings.OmitXmlDeclaration = True

        Dim amountInput As String = "1.00"

        Try
            If (Not System.Configuration.ConfigurationManager.AppSettings("CCA_TestBooking") Is Nothing) Then
                Dim Testvalue As String = System.Configuration.ConfigurationManager.AppSettings("CCA_TestBooking").ToString
                Dim TestBooking As String = Testvalue.Split(",")(0).TrimEnd
                If (MerchantReference.Equals(TestBooking) = True And Testvalue.Contains(",") = True) Then
                    amountInput = Testvalue.Split(",")(1).TrimEnd
                End If
            End If

        Catch ex As Exception
            amountInput = "1.00"
        End Try
        
        Using writer As XmlWriter = XmlWriter.Create(sw, settings)
            writer.WriteStartDocument()
            writer.WriteStartElement("GenerateRequest")
            writer.WriteElementString("PxPayUserId", _PxPayUserId)
            writer.WriteElementString("PxPayKey", _PxPayKey)
            writer.WriteElementString("AmountInput", amountInput)
            writer.WriteElementString("CurrencyInput", (CountryCode + "D").ToUpper)
            writer.WriteElementString("MerchantReference", MerchantReference)
            writer.WriteElementString("TxnType", "Auth")
            writer.WriteElementString("BillingId", MerchantReference)
            writer.WriteElementString("EnableAddBillCard", "1")
            writer.WriteElementString("UrlSuccess", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path))
            writer.WriteElementString("UrlFail", HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Path))
            writer.WriteEndElement()
            writer.WriteEndDocument()
            writer.Flush()
        End Using
        Return SubmitXml(sw.ToString)
    End Function

    Private Function ProcessResponseXml(ByVal result As String) As String

        Dim sw As New StringWriter()

        Dim settings As New XmlWriterSettings()
        settings.Indent = True
        settings.NewLineOnAttributes = False
        settings.OmitXmlDeclaration = True

        Using writer As XmlWriter = XmlWriter.Create(sw, settings)
            writer.WriteStartDocument()
            writer.WriteStartElement("ProcessResponse")
            writer.WriteElementString("PxPayUserId", _PxPayUserId)
            writer.WriteElementString("PxPayKey", _PxPayKey)
            writer.WriteElementString("Response", result)
            writer.WriteEndElement()
            writer.WriteEndDocument()
            writer.Flush()
        End Using

        Return sw.ToString()
    End Function


    Public Function ProcessResponse(ByVal result As String) As ResponseOutput
        Dim myResult As New ResponseOutput(SubmitXml(ProcessResponseXml(result)))

        If myResult.Success = 1 Then
            Dim value As String = InsertTokenObject(myResult.MerchantReference, _
                              myResult.CardName, _
                              myResult.CardHolderName, _
                              myResult.CardNumber, _
                              myResult.DateExpiry, _
                              myResult.DpsBillingId, _
                              UserName, UserName)
            If String.IsNullOrEmpty(value) Then
                Return myResult
            Else
                If (value.IndexOf("ERROR") = -1) Then
                    Return myResult
                End If
            End If
            
        Else
            Return Nothing
        End If
        ''Return myResult
    End Function

    Private Shared Function InsertTokenObject(ByVal PmtBillToRntId As String, _
                                             ByVal PmtBillToCardType As String, _
                                             ByVal PmtBillToCardName As String, _
                                             ByVal PmtBillCardNumber As String, _
                                             ByVal PmtBillToCardExpDate As String, _
                                             ByVal PmtBillToToken As String, _
                                             ByVal AddUsrId As String, _
                                             ByVal ModUsrId As String) As String

        Try
            Return Aurora.Common.Data.ExecuteScalarSP("BillingTokenInsert", PmtBillToRntId, _
                                                                        PmtBillToCardType, _
                                                                        PmtBillToCardName, _
                                                                        PmtBillCardNumber, _
                                                                        PmtBillToCardExpDate, _
                                                                        PmtBillToToken, _
                                                                        AddUsrId)

        Catch ex As Exception
            LogInformation("(BILLING TOKEN InsertTokenObject)", String.Concat("EXCEPTION:", ex.Message, " STACKTRACE:", ex.StackTrace))
            Return "ERROR: " + ex.Message
        End Try


    End Function
End Class

Public Class ResponseOutput
    '<Response valid="1">
    '<Success>1</Success>
    '<TxnType>Purchase</TxnType>
    '<CurrencyInput>NZD</CurrencyInput>
    '<MerchantReference>Test Transaction</MerchantReference>
    '<TxnData1>28 Grange Rd</TxnData1>
    '<TxnData2>Auckland</TxnData2>
    '<TxnData3>NZ</TxnData3>
    '<AuthCode>053646</AuthCode>
    '<CardName>Visa</CardName>
    '<CardHolderName>TEST</CardHolderName>
    '<CardNumber>411111........11</CardNumber>
    '<DateExpiry>1010</DateExpiry>
    '<CardHolderName>TEST</CardHolderName>

    '<ClientInfo>219.88.103.101</ClientInfo>
    '<TxnId>P777575CA3DDA78C</TxnId>
    '<EmailAddress></EmailAddress>
    '<DpsTxnRef>000000040119429b</DpsTxnRef>
    '<BillingId></BillingId>
    '<DpsBillingId></DpsBillingId>
    '<AmountSettlement>2.06</AmountSettlement>
    '<CurrencySettlement>NZD</CurrencySettlement>
    '<TxnMac>BD43E619</TxnMac>
    '<ResponseText>APPROVED</ResponseText>
    '<CardNumber2>9999900000000005</CardNumber2>
    '</Response>
    Public Sub New(ByVal Xml As String)
        _Xml = Xml
        '' SetProperty()
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(Xml)

        valid = xmldoc.SelectSingleNode("Response").Attributes("valid").Value
        Success = xmldoc.SelectSingleNode("Response/Success").InnerText
        TxnType = xmldoc.SelectSingleNode("Response/TxnType").InnerText
        CurrencyInput = xmldoc.SelectSingleNode("Response/CurrencyInput").InnerText
        MerchantReference = xmldoc.SelectSingleNode("Response/MerchantReference").InnerText
        TxnData1 = xmldoc.SelectSingleNode("Response/TxnData1").InnerText
        TxnData2 = xmldoc.SelectSingleNode("Response/TxnData2").InnerText
        TxnData3 = xmldoc.SelectSingleNode("Response/TxnData3").InnerText
        AuthCode = xmldoc.SelectSingleNode("Response/AuthCode").InnerText
        CardName = xmldoc.SelectSingleNode("Response/CardName").InnerText
        CardHolderName = xmldoc.SelectSingleNode("Response/CardHolderName").InnerText
        CardNumber = xmldoc.SelectSingleNode("Response/CardNumber").InnerText
        DateExpiry = xmldoc.SelectSingleNode("Response/DateExpiry").InnerText
        ClientInfo = xmldoc.SelectSingleNode("Response/ClientInfo").InnerText
        TxnId = xmldoc.SelectSingleNode("Response/TxnId").InnerText
        EmailAddress = xmldoc.SelectSingleNode("Response/EmailAddress").InnerText
        DpsTxnRef = xmldoc.SelectSingleNode("Response/DpsTxnRef").InnerText
        BillingId = xmldoc.SelectSingleNode("Response/BillingId").InnerText
        DpsBillingId = xmldoc.SelectSingleNode("Response/DpsBillingId").InnerText
        AmountSettlement = xmldoc.SelectSingleNode("Response/AmountSettlement").InnerText
        CurrencySettlement = xmldoc.SelectSingleNode("Response/CurrencySettlement").InnerText
        TxnMac = xmldoc.SelectSingleNode("Response/TxnMac").InnerText
        ResponseText = xmldoc.SelectSingleNode("Response/ResponseText").InnerText

    End Sub

    Private _valid As String
    Private _AmountSettlement As String
    Private _AuthCode As String
    Private _CardName As String
    Private _CardNumber As String
    Private _DateExpiry As String
    Private _DpsTxnRef As String
    Private _Success As String
    Private _ResponseText As String
    Private _DpsBillingId As String
    Private _CardHolderName As String
    Private _CurrencySettlement As String
    Private _TxnData1 As String
    Private _TxnData2 As String
    Private _TxnData3 As String
    Private _TxnType As String
    Private _CurrencyInput As String
    Private _MerchantReference As String
    Private _ClientInfo As String
    Private _TxnId As String
    Private _EmailAddress As String
    Private _BillingId As String
    Private _TxnMac As String

    Private _Xml As String

    Public Property valid() As String
        Get
            Return _valid
        End Get
        Set(ByVal value As String)
            _valid = value
        End Set
    End Property

    Public Property AmountSettlement() As String
        Get
            Return _AmountSettlement
        End Get
        Set(ByVal value As String)
            _AmountSettlement = value
        End Set
    End Property

    Public Property AuthCode() As String
        Get
            Return _AuthCode
        End Get
        Set(ByVal value As String)
            _AuthCode = value
        End Set
    End Property

    Public Property CardName() As String
        Get
            Return _CardName
        End Get
        Set(ByVal value As String)
            _CardName = value
        End Set
    End Property

    Public Property CardNumber() As String
        Get
            Return _CardNumber
        End Get
        Set(ByVal value As String)
            _CardNumber = value
        End Set
    End Property

    Public Property DateExpiry() As String
        Get
            Return _DateExpiry
        End Get
        Set(ByVal value As String)
            _DateExpiry = value
        End Set
    End Property

    Public Property DpsTxnRef() As String
        Get
            Return _DpsTxnRef
        End Get
        Set(ByVal value As String)
            _DpsTxnRef = value
        End Set
    End Property

    Public Property Success() As String
        Get
            Return _Success
        End Get
        Set(ByVal value As String)
            _Success = value
        End Set
    End Property

    Public Property ResponseText() As String
        Get
            Return _ResponseText
        End Get
        Set(ByVal value As String)
            _ResponseText = value
        End Set
    End Property

    Public Property DpsBillingId() As String
        Get
            Return _DpsBillingId
        End Get
        Set(ByVal value As String)
            _DpsBillingId = value
        End Set
    End Property

    Public Property CardHolderName() As String
        Get
            Return _CardHolderName
        End Get
        Set(ByVal value As String)
            _CardHolderName = value
        End Set
    End Property

    Public Property CurrencySettlement() As String
        Get
            Return _CurrencySettlement
        End Get
        Set(ByVal value As String)
            _CurrencySettlement = value
        End Set
    End Property

    Public Property TxnData1() As String
        Get
            Return _TxnData1
        End Get
        Set(ByVal value As String)
            _TxnData1 = value
        End Set
    End Property

    Public Property TxnData2() As String
        Get
            Return _TxnData2
        End Get
        Set(ByVal value As String)
            _TxnData2 = value
        End Set
    End Property

    Public Property TxnData3() As String
        Get
            Return _TxnData3
        End Get
        Set(ByVal value As String)
            _TxnData3 = value
        End Set
    End Property

    Public Property TxnType() As String
        Get
            Return _TxnType
        End Get
        Set(ByVal value As String)
            _TxnType = value
        End Set
    End Property

    Public Property CurrencyInput() As String
        Get
            Return _CurrencyInput
        End Get
        Set(ByVal value As String)
            _CurrencyInput = value
        End Set
    End Property


    Public Property MerchantReference() As String
        Get
            Return _MerchantReference
        End Get
        Set(ByVal value As String)
            _MerchantReference = value
        End Set
    End Property

    Public Property ClientInfo() As String
        Get
            Return _ClientInfo
        End Get
        Set(ByVal value As String)
            _ClientInfo = value
        End Set
    End Property

    Public Property TxnId() As String
        Get
            Return _TxnId
        End Get
        Set(ByVal value As String)
            _TxnId = value
        End Set
    End Property

    Public Property EmailAddress() As String
        Get
            Return _EmailAddress
        End Get
        Set(ByVal value As String)
            _EmailAddress = value
        End Set
    End Property

    Public Property BillingId() As String
        Get
            Return _BillingId
        End Get
        Set(ByVal value As String)
            _BillingId = value
        End Set
    End Property

    Public Property TxnMac() As String
        Get
            Return _TxnMac
        End Get
        Set(ByVal value As String)
            _TxnMac = value
        End Set
    End Property

    ' If there are any additional elements or attributes added to the output XML simply add a property of the same name. 

    'Private Sub SetProperty()

    '    Dim reader As XmlReader = XmlReader.Create(New StringReader(_Xml))

    '    While reader.Read()
    '        Dim prop As PropertyInfo
    '        If reader.NodeType = XmlNodeType.Element Then
    '            prop = Me.[GetType]().GetProperty(reader.Name)
    '            If prop IsNot Nothing Then
    '                Me.[GetType]().GetProperty(reader.Name).SetValue(Me, reader.ReadString(), System.Reflection.BindingFlags.[Default], Nothing, Nothing, Nothing)
    '            End If
    '            If reader.HasAttributes Then

    '                For count As Integer = 0 To reader.AttributeCount - 1
    '                    'Read the current attribute 
    '                    reader.MoveToAttribute(count)
    '                    prop = Me.[GetType]().GetProperty(reader.Name)
    '                    If prop IsNot Nothing Then
    '                        Me.[GetType]().GetProperty(reader.Name).SetValue(Me, reader.Value, System.Reflection.BindingFlags.[Default], Nothing, Nothing, Nothing)
    '                    End If
    '                Next
    '            End If
    '        End If

    '    End While
    'End Sub


End Class



Public Class PaymentExpressWSData
    Public Property Amount As String
    Public Property DpsBillingId As String
    Public Property DpsTxnRef As String
    Public Property TxnRef As String
    Public Property TxnType As String
End Class