Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class BookingPaymentUpdateSingleItem
    Private Shared Function GetErrorTextFromResource(ByVal bErrStatus As Boolean, _
                                         ByVal sErrNumber As String, _
                                         ByVal sErrType As String, _
                                         ByVal sErrDescription As String) As String

        Dim sXmlString As String
        If sErrDescription = "" Then
            sErrDescription = "Success"
        End If
        sXmlString = "<Error>"
        sXmlString = sXmlString & "<ErrStatus>" & bErrStatus & "</ErrStatus>"
        sXmlString = sXmlString & "<ErrNumber>" & sErrNumber & "</ErrNumber>"
        sXmlString = sXmlString & "<ErrType>" & sErrType & "</ErrType>"
        sXmlString = sXmlString & "<ErrDescription>" & sErrDescription & "</ErrDescription></Error>"
        Return sXmlString

    End Function

    Private Shared Function getMessageFromDB(ByVal scode As String) As String
        Dim params(5) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sCode", DbType.String, scode, Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("param1", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("param2", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("param3", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("param4", DbType.String, "", Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("oparam1", DbType.String, 400, Parameter.ParameterType.AddOutParameter)
        Aurora.Common.Data.ExecuteOutputSP("sp_get_ErrorString", params)
        Return IIf(String.IsNullOrEmpty(params(5).Value), "SUCCESS", params(5).Value)
    End Function

    Public Shared Function UpdateItem(ByVal sXml As String, _
                               ByVal sUserId As String, _
                               ByVal sProgramName As String, _
                               ByVal sFunction As String, _
                               ByVal sParam As String) As String

        Dim sXmlString As String = ""
        Dim sErrorString As String = ""
        Dim smessage As String = "U"

        Dim xmlTemp As New XmlDocument

        Try

            xmlTemp.LoadXml(sXml)
            If xmlTemp Is Nothing Then
                sXmlString = getMessageFromDB("GEN023")
                sErrorString = GetErrorTextFromResource(True, "", "Application", sXmlString)
                xmlTemp = Nothing
                Return "<Root>" & sErrorString & "<Data></Data></Root>"
            End If

            Dim root As XmlElement = xmlTemp.DocumentElement
            Dim itemA As String = root.SelectSingleNode("//Data/paramA").InnerText
            Dim itemB As String = root.SelectSingleNode("//Data/paramB").InnerText



            '@sBookingId  varchar(64),  
            '@sRentalId  varchar(64),  
            '@sParam   varchar(64),  
            '@sAddModuserId varchar(64),  
            '@sProgram  varchar(64),  
            '@sAction   varchar(10)  

            Dim forExtra As String = sParam
            sXmlString = Aurora.Common.Data.ExecuteScalarSP("paym_updatePaymentHistory", itemA, itemB, forExtra, sUserId, sProgramName, smessage)

            If sXmlString = "" Then
                sXmlString = getMessageFromDB("GEN046")
                sErrorString = GetErrorTextFromResource(False, "", "", sXmlString)
            Else
                Throw New Exception
            End If

            Return "<Root>" & sErrorString & "<Data></Data></Root>"

        Catch ex As Exception

            Dim sxmlstringarray() As String = sXmlString.Split("/")
            Dim serrorno As String = ""
            Dim sErrorDesc As String = ""
            If UBound(sxmlstringarray) > 1 Then
                serrorno = sXmlString(0)

                For i As Integer = 1 To UBound(sxmlstringarray)
                    If i = 1 Then
                        sErrorDesc = sxmlstringarray(i)
                    Else
                        sErrorDesc = sErrorDesc & "/" & sxmlstringarray(i)
                    End If
                Next

            Else
                serrorno = sxmlstringarray(0)
                sErrorDesc = sxmlstringarray(1)
            End If

            If serrorno = "GEN001" Or serrorno = "GEN008" Then
                sErrorString = GetErrorTextFromResource(True, serrorno, "Application", sErrorDesc)
                Return "<Root>" & sErrorString & "<Data></Data></Root>"
            End If

            sErrorString = GetErrorTextFromResource(True, serrorno, "Application", sErrorDesc)
            Return "<Root>" & sErrorString & "<Data></Data></Root>"
        End Try
    End Function

End Class
