''colAmntOuts
Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class BookingPaymentAmountOuts
    Public CustToPay As Object
    Public AmountAllocated As Object
    Public RptType As Object
    Public RptRntId As Object
    Public RptIsRefund As Boolean
    Public RptChargeCurrId As Object
End Class

Public Class BookingPaymentOutCollection
    Implements System.Collections.IEnumerable

    Private mcollection As New Collection

    Public Function Add(ByVal objBookingPaymentAmountOuts As BookingPaymentAmountOuts) As BookingPaymentAmountOuts
        Dim newBookingPaymentAmountOuts As New BookingPaymentAmountOuts
        With newBookingPaymentAmountOuts
            .CustToPay = objBookingPaymentAmountOuts.CustToPay
            .AmountAllocated = objBookingPaymentAmountOuts.AmountAllocated
            ''.RptCurrentPymtAmt = amItem.RptCurrentPymtAmt
            .RptType = objBookingPaymentAmountOuts.RptType
            .RptRntId = objBookingPaymentAmountOuts.RptRntId
            .RptIsRefund = objBookingPaymentAmountOuts.RptIsRefund
            .RptChargeCurrId = objBookingPaymentAmountOuts.RptChargeCurrId
        End With
        mcollection.Add(newBookingPaymentAmountOuts)
        Add = objBookingPaymentAmountOuts
    End Function

    Public Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        GetEnumerator = mcollection.GetEnumerator
    End Function

    Public Function Count() As Integer
        Return mcollection.Count
    End Function

    Public Sub Delete(ByVal index As Integer)
        mcollection.Remove(index)
    End Sub

    Public Function Item(ByVal index As Integer) As BookingPaymentAmountOuts
        Item = mcollection.Item(index)
    End Function

End Class
