Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports Aurora.Telematics.Service

Public Class BookingExchange

    Public Shared Function GetExchangeData(ByVal bookingId As String, ByVal rentalId As String, _
            ByVal userCode As String) As DataTable
        Dim ds As DataSet
        ds = Data.DataRepository.GetExchangeData(bookingId, rentalId, userCode)
        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt
    End Function

    Public Shared Function GetExchangeDataXmlDoc(ByVal bookingId As String, ByVal rentalId As String, _
               ByVal userCode As String) As XmlDocument

        Return Data.DataRepository.GetExchangeDataXmlDoc(bookingId, rentalId, userCode)

    End Function

    Public Shared Function GetAIMSVehicleName(ByVal rego As String, ByVal unit As String, _
        ByVal rentalId As String, ByVal requestType As String, ByVal userCode As String) As DataTable

        Dim xmlDoc As XmlDocument
        xmlDoc = Data.DataRepository.GetAIMSVehicleName(rego, unit, rentalId, requestType, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))

        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function

    ''rev:mia Oct.10
    Public Shared Function GetAIMSVehicleNameExchangeTab(ByVal rego As String, ByVal unit As String, _
            ByVal rentalId As String, ByVal requestType As String, ByVal userCode As String) As DataTable

        Dim xmlDoc As XmlDocument
        xmlDoc = Data.DataRepository.GetAIMSVehicleNameExchangeTab(rego, unit, rentalId, requestType, userCode)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc))

        Dim dt As DataTable
        dt = New DataTable
        If ds.Tables.Count > 0 Then
            dt = ds.Tables(0)
        End If
        Return dt

    End Function


    Public Shared Function GetUVIData(ByVal UserCode As String, ByVal roleCode As String) As XmlDocument
        'GEN_GetPopUpData @case = 'GETUVI', @param1 = 'sp7', @param2='ORID_AIMS'
        Return DataRepository.GetUVIData(UserCode, roleCode)
    End Function

    Public Shared Function ManageBPDExchange(ByVal xmlDoc As XmlDocument, _
        ByVal force As String, ByVal userCode As String, ByVal programName As String) As String

        Dim status As String = xmlDoc.SelectSingleNode("//data/Rental/RntStCode").InnerText
        Dim exchangeDate As DateTime
        exchangeDate = xmlDoc.SelectSingleNode("//data/Rental/CurrDt").InnerText & " " & xmlDoc.SelectSingleNode("//data/Rental/CurrTm").InnerText

        Dim todayDate As DateTime
        todayDate = xmlDoc.SelectSingleNode("//data/Rental/VCurrDt").InnerText & " " & xmlDoc.SelectSingleNode("//data/Rental/VCurrTm").InnerText

        Dim outDate As Date = xmlDoc.SelectSingleNode("//data/Rental/RntCkoWhen").InnerText
        Dim inDate As Date = xmlDoc.SelectSingleNode("//data/Rental/RntCkiWhen").InnerText

        Dim isCrossHire As Boolean = Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/CrossHire").InnerText, 0)

        Dim oldSapId As String = xmlDoc.SelectSingleNode("//data/Rental/OldSapId").InnerText
        Dim oldCrossHire As Boolean = Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/OldCrossHire").InnerText, 0)

        Dim rentalId As String = xmlDoc.SelectSingleNode("//data/Rental/RntId").InnerText
        Dim shortName As String = xmlDoc.SelectSingleNode("//data/Rental/RVehicle").InnerText
        shortName = shortName.Split("-")(0).Trim()

        Dim noOfExch As Integer = xmlDoc.SelectSingleNode("//data/Rental/NoOfExch").InnerText
        Dim booRntNum As String = xmlDoc.SelectSingleNode("//data/Rental/BooRntNum").InnerText
        Dim branch As String = xmlDoc.SelectSingleNode("//data/Rental/Branch").InnerText
        Dim oddoIn As Integer = Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/OdoIn").InnerText, 0)
        Dim oddoOut As Integer = Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/ROdoOut").InnerText, 0)

        Dim bpdId As String = xmlDoc.SelectSingleNode("//data/Rental/BpdId").InnerText

        Dim rate As Integer = Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/Rate").InnerText, 0)

        Dim noteText As String = xmlDoc.SelectSingleNode("//data/Rental/NoteText").InnerText
        Dim unitNumber As String = xmlDoc.SelectSingleNode("//data/Rental/RUnitNum").InnerText

        'Added by Nimesh on 08/July/2015 - https://thlonline.atlassian.net/browse/TELMAT-16
        Dim oldUnitNumber As String = xmlDoc.SelectSingleNode("//data/Rental/UnitNum").InnerText

        Dim oldRegNumber As String = xmlDoc.SelectSingleNode("//data/Rental/RegNum").InnerText

        'End Added by Nimesh on 08/July/2015 - https://thlonline.atlassian.net/browse/TELMAT-16
        Dim rRegNum As String = xmlDoc.SelectSingleNode("//data/Rental/RRegNum").InnerText
        Dim exchangeType As String = xmlDoc.SelectSingleNode("//data/Rental/ExchangeTyp").InnerText

        Dim cityCode As String = xmlDoc.SelectSingleNode("//data/Rental/CtyCode").InnerText
        'Dim rentalCheckInLocationCode As String = xmlDoc.SelectSingleNode("//data/Rental/RntCkiLocCode").InnerText

        Dim productId As String = ""
        Dim rentalCheckOutLoc As String = xmlDoc.SelectSingleNode("//data/Rental/CkoLoc").InnerText
        Dim rentalCheckOutDate As String = xmlDoc.SelectSingleNode("//data/Rental/RntCkoWhen").InnerText
        Dim checkOutDayPart As String = xmlDoc.SelectSingleNode("//data/Rental/RntCkoDatePart").InnerText

        Dim rentalCheckInLoc As String = xmlDoc.SelectSingleNode("//data/Rental/RntCkiLocCode").InnerText
        Dim rentalCheckInDate As String = xmlDoc.SelectSingleNode("//data/Rental/RntCkiWhen").InnerText
        Dim checkInDayPart As String = xmlDoc.SelectSingleNode("//data/Rental/RntCkiDatePart").InnerText
        Dim bookingId As String = xmlDoc.SelectSingleNode("//data/Rental/BooId").InnerText

        'output
        Dim amPm As String = ""
        Dim dVassSeqNum As String = ""
        Dim sapId As String = ""
        Dim returnError As String = ""
        Dim updateFleet As Boolean

        ' to handle AIMS return messages
        Dim sAimsMessage As String = ""

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                If status = "CO" Then
                    If exchangeDate <= outDate Then
                        Throw New Exception("Exchange date must be greater then check-out date.")
                    End If
                    If exchangeDate > inDate Then
                        Throw New Exception("Exchange date cannot be after the rental check-in date.")
                    End If
                    If exchangeDate > todayDate Then
                        Throw New Exception("Exchange date cannot be greater than today's date.")
                    End If
                End If

                '====================>> Try to Fetch NewSAPId For the Product ShortName  <<===============================
                If Not isCrossHire Then
                    'Call GEN_getSapIdForShortName
                    returnError = ""
                    Data.DataRepository.GetSapIdForShortName(rentalId, shortName, userCode, exchangeDate, _
                        amPm, dVassSeqNum, sapId, returnError, updateFleet)

                    'Dim err As String
                    'err = ds.Tables(0).Rows(0)(0)
                    productId = dVassSeqNum

                    If returnError.ToUpper() = "SUCCESS" Then
                        returnError = ""
                    End If

                    If returnError.Length <> 0 Then
                        Throw New Exception(returnError)
                    End If
                Else
                    sapId = oldSapId
                End If

                '=====================>> Call ManageBookedProductExchange with New SapId <<<======================




                '--------------------------------------------AuroraExchange-----------------------------------
                returnError = ""
                Data.DataRepository.VehicleExchange(bpdId, exchangeDate, branch, sapId, rate, noteText, unitNumber, _
                    rRegNum, oddoOut, oddoIn, userCode, programName, exchangeType, returnError)

                If Not String.IsNullOrEmpty(returnError) Then
                    Throw New Exception(returnError)
                End If


                '------------------------- START DVASS CALL BECAUSE STATUS IS KK OR NN -----------------------
                If (status = "KK" Or status = "NN") And updateFleet Then
                    'ErrorLog(Space(4) & "------ START DVASS CALL BECAUSE STATUS IS KK OR NN --------")

                    If cityCode.ToUpper() = "AU" Then
                        Aurora.Reservations.Services.AuroraDvass.selectCountry(0)
                    ElseIf cityCode.ToUpper() = "NZ" Then
                        Aurora.Reservations.Services.AuroraDvass.selectCountry(1)
                    End If

                    returnError = ""

                    returnError = Aurora.Reservations.Services.AuroraDvass.ModifyRental(booRntNum, productId, _
                        rentalCheckOutLoc, rentalCheckOutDate, convertStringDayPartToInteger(checkOutDayPart), _
                        rentalCheckInLoc, rentalCheckInDate, convertStringDayPartToInteger(checkInDayPart), _
                        0, 1, 10, "", 1)

                    If returnError = "SUCCESS" Then
                        returnError = ""
                    End If
                    If Not String.IsNullOrEmpty(returnError) Then
                        Throw New Exception(returnError)
                    End If

                    'ErrorLog(Space(4) & "------ END OF DVASS CALL  --------")

                End If


                '=====-===========>> CHECK-OUT Process for the New BpdId which ManageBookedProduct_Exch has returned <<======-======
                If status = "CO" And Not isCrossHire Then

                    'ErrorLog(Space(4) & "------ It's After Check-Out --------")

                    Dim externalRef As String
                    externalRef = booRntNum & "x" & (noOfExch + 1)

                    Dim aimsOjb As New AI.CMaster
                    Dim aimsReturnValue As Integer

                    Logging.LogInformation("AIMS CALL ", "FOR EXCHANGE : F6 ACTIVITY CREATE." & vbNewLine & _
                                                                "Params:" & vbNewLine & _
                                                                "______________________________________________" & vbNewLine & _
                                                                "ExternalRef   := " & externalRef & vbNewLine & _
                                                                "Unit No       := " & unitNumber & vbNewLine & _
                                                                "Branch        := " & branch & vbNewLine & _
                                                                "Exchange Date := " & exchangeDate & vbNewLine & _
                                                                "Odometer Out  := " & oddoOut & vbNewLine & _
                                                                "Check-In Locn := " & rentalCheckInLoc & vbNewLine & _
                                                                "Check-In Date := " & inDate & vbNewLine & _
                                                                "Odometer In   := -1" & vbNewLine & _
                                                                "Other Assets  :=   " & vbNewLine & _
                                                                "Force Flag    := <F9NotReq>" & vbNewLine & _
                                                                "______________________________________________" & vbNewLine)

                    aimsReturnValue = aimsOjb.F6_ActivityCreate("Rental", externalRef, unitNumber, branch, _
                        exchangeDate, oddoOut, rentalCheckInLoc, inDate, -1, "", "<F9NotReq>")


                    If aimsReturnValue <> 0 Then
                        Throw New Exception(BookingCheckInCheckOut.GetAimsMessage(aimsReturnValue.ToString().Trim()))
                        'sAimsMessage = BookingCheckInCheckOut.GetAimsMessage(aimsReturnValue.ToString().Trim())
                    End If

                    'ErrorLog(Space(4) & "------ END AIMS : F6_ActivityCreate --------")


                    '==========================-===========>> End of CHECK-OUT Process <<======-================
                End If

                ' Moved down
                If status = "CO" Then

                    If Not oldCrossHire Then
                        '=====-=========-===========-=======>> CALL CHECK-IN Process for OLD Vehicle <<========-=======-=========-=======-=

                        Dim externalRef As String = ""
                        If noOfExch = 0 Then
                            externalRef = booRntNum
                        Else
                            externalRef = booRntNum & "x" & noOfExch
                        End If

                        Dim aimsOjb As New AI.CMaster
                        Dim aimsReturnValue As Integer

                        Try
                            Logging.LogInformation("AIMS CALL ", "FOR EXCHANGE : F8 ACTIVITY COMPLETE." & vbNewLine & _
                                                                "Params:" & vbNewLine & _
                                                                "______________________________________________" & vbNewLine & _
                                                                "ExternalRef   := " & externalRef & vbNewLine & _
                                                                "Branch        := " & branch & vbNewLine & _
                                                                "Exchange Date := " & exchangeDate & vbNewLine & _
                                                                "Odometer In   := " & oddoIn & vbNewLine & _
                                                                "______________________________________________" & vbNewLine)
                        Catch
                        End Try

                        aimsReturnValue = aimsOjb.F8_ActivityComplete("Rental", externalRef, branch, exchangeDate, oddoIn)

                        If aimsReturnValue <> 0 Then
                            Throw New Exception(BookingCheckInCheckOut.GetAimsMessage(aimsReturnValue.ToString().Trim()))
                            'sAimsMessage = BookingCheckInCheckOut.GetAimsMessage(aimsReturnValue.ToString().Trim())
                        End If

                        'ErrorLog(Space(4) & "------ END AIMS : F8_ActivityComplete --------")

                        '------=============>>> End of AIMS Call <<<============================------------------
                    End If

                    '========-=============-==============>> CHECK IN PROCESS END <<===========-==============-===========-========-====
                End If
                ' Moved down

                '=====-=====================================>> RES_ssTransToFinance <<======-===========================

                If Not DataRepository.SSTransToFinance(bookingId, rentalId, sapId, bpdId) Then
                    Throw New Exception(Format(Now, "DD MMM YY dddd HH:MM:SS") & "Error in RES_ssTransToFinance Aborting the Transaction")
                End If

                



                'If Not DataRepository.UpdateRentalIntegrityNumber(rentalId, bpdId) Then
                '    Return "Failed to update integrity number for Rental Id=" & rentalId & ", BPDId =" & bpdId
                'End If
                DataRepository.UpdateRentalIntegrityNumber(rentalId, bpdId)


                ' Added By Nimesh on 06th Aug 2015
                ' Description: Initial IsServiceDue flag in Fleet Asset was being updated in Stored procedure RES_VehicleExchange, but it was timing out in AIMS Call because of RowLock and 
                ' AIMS was not able to update in between the transaction. 
                ' Created new  Stored procedure RES_UpdateUnitForServiceDue in the database
                ' Created new Sub in Aurora.Booking.Data> DataRepository, UpdateServiceDueFlag to Call Stored procedure RES_UpdateUnitForServiceDue
                ' Now Calling the same method in AURORA.Booking.Data.UpdateServiceDueFlag here
                DataRepository.UpdateServiceDueFlag(oldUnitNumber, returnError)

                If returnError.ToUpper() = "SUCCESS" Then
                    returnError = ""
                End If

                If returnError.Length <> 0 Then
                    Throw New Exception(returnError)
                End If
                'End Added by Nimesh on 06th Aug 2015

                ''rev:mia 25June2015 - Adding Imarda
                'BookingCheckInCheckOut.IMARDAtelematicsUpdate(True, rentalId, unitNumber)
                ''Nim to make it async 14th July 2015

                Dim tempTelematicsArgs As New TelematicsArgs
                tempTelematicsArgs.BookingID = rentalId
                tempTelematicsArgs.UnitNumber = unitNumber
                tempTelematicsArgs.IsCheckOut = True
                tempTelematicsArgs.CheckInOutDateTime = exchangeDate
                tempTelematicsArgs.RegoNumber = rRegNum
                System.Threading.ThreadPool.QueueUserWorkItem(AddressOf BookingCheckInCheckOut.IMARDAtelematicsUpdateASYNC, tempTelematicsArgs)

                'IMARDAtelematicsUpdate(True, sRentalId, sUnit)
                ''End Nim to make it async 14th July 2015

                ''Added by Nimesh on 8th July 2015 ref: https://thlonline.atlassian.net/browse/TELMAT-16
                'BookingCheckInCheckOut.IMARDAtelematicsUpdate(False, rentalId, oldUnitNumber)
                ''Nim to make it async 14th July 2015

                tempTelematicsArgs = New TelematicsArgs
                tempTelematicsArgs.BookingID = rentalId
                tempTelematicsArgs.UnitNumber = oldUnitNumber
                tempTelematicsArgs.IsCheckOut = False
                tempTelematicsArgs.CheckInOutDateTime = exchangeDate
                tempTelematicsArgs.RegoNumber = oldRegNumber
                System.Threading.ThreadPool.QueueUserWorkItem(AddressOf BookingCheckInCheckOut.IMARDAtelematicsUpdateASYNC, tempTelematicsArgs)

                'IMARDAtelematicsUpdate(True, sRentalId, sUnit)
                ''End Nim to make it async 14th July 2015
                ''End - Added by Nimesh on 8th July 2015 ref: https://thlonline.atlassian.net/browse/TELMAT-16

                oTransaction.CommitTransaction()
                oTransaction.Dispose()

                'If sAimsMessage.Trim() <> String.Empty Then
                '    Return "AIMSMSG/" & sAimsMessage.Trim()
                'Else
                Return ""
                'End If


            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Try
                    Logging.LogException("BOOKING_CHANGEOVER_EXCEPTION", ex)
                Catch
                End Try
                Return ex.Message
            End Try

        End Using

    End Function

    Private Shared Function convertStringDayPartToInteger(ByVal strDayPart As String) As Integer
        Select Case UCase(strDayPart)
            Case "AM"
                Return 0
            Case "PM"
                Return 1
            Case Else
                ' THIS will cause DVASS to fail
                Return -1
        End Select
    End Function

End Class
