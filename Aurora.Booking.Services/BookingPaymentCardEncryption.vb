Imports System
Imports System.io
Imports System.text
Imports Microsoft.VisualBasic
Imports System.Security
Imports System.Security.Cryptography



Public Class BookingPaymentCardEncryption
    Shared _protectKey As Boolean
    Shared _algoName As String

    Public Sub New(ByVal targetFile As String)
        GenerateKey(targetFile)
    End Sub

    Public Shared Property AlgorithmName() As String
        Get
            Return _algoName
        End Get
        Set(ByVal value As String)
            If value = "" Then
                _algoName = "DES"
            Else
                _algoName = value
            End If
        End Set
    End Property

    Public Shared Property ProtectKey() As Boolean
        Get
            Return _protectKey
        End Get
        Set(ByVal value As Boolean)
            _protectKey = value
        End Set
    End Property

    Private Shared Sub GenerateKey(ByVal targetFile As String)
        Try

            If targetFile <> "" Then

                If Not File.Exists(targetFile) Then

                    Dim algorithm As SymmetricAlgorithm = SymmetricAlgorithm.Create(AlgorithmName)
                    algorithm.GenerateKey()
                    Dim key As Byte() = algorithm.Key

                    If ProtectKey = True Then
                        key = ProtectedData.Protect(key, Nothing, DataProtectionScope.LocalMachine)
                    End If
                    Using fs As New FileStream(targetFile, FileMode.Create)
                        fs.Write(key, 0, key.Length)
                    End Using
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Shared Sub ReadKey(ByVal algorithm As SymmetricAlgorithm, ByVal keyfile As String)
        Dim key As Byte()

        ''read the key content
        Using fs As New FileStream(keyfile, FileMode.Open, FileAccess.Read, FileShare.Inheritable)
            key = New Byte(fs.Length - 1) {}
            fs.Read(key, 0, CInt(fs.Length))
        End Using

        If ProtectKey = True Then
            algorithm.Key = ProtectedData.Unprotect(key, Nothing, DataProtectionScope.LocalMachine)
        Else
            algorithm.Key = key
        End If
    End Sub

    Public Shared Function EncyrptCardNumber(ByVal cardNumber As String, ByVal keyfile As String) As Byte()
        ''convert creditcard numbers  into bytes
        Dim cardNumberBytes() As Byte = Encoding.UTF8.GetBytes(cardNumber)
        ''Dim cardNumberBytes() As Byte = Encoding.Unicode.GetBytes(cardNumber)

        ''call the algorithm class that will be use in the encryption
        Dim algorithm As SymmetricAlgorithm = SymmetricAlgorithm.Create(AlgorithmName)

        'generate extra numbers to be added in the encryption
        algorithm.GenerateIV()

        'read the key generated when in the constructor
        ReadKey(algorithm, keyfile)

        'create a pipeline
        Dim targetStream As New MemoryStream
        targetStream.Write(algorithm.IV, 0, algorithm.IV.Length)


        'wrap the encryption process with a stream
        Dim cs As New CryptoStream(targetStream, algorithm.CreateEncryptor(), CryptoStreamMode.Write)
        cs.Write(cardNumberBytes, 0, cardNumberBytes.Length)

        cs.FlushFinalBlock()

        ''return as byte
        Return targetStream.ToArray()
    End Function

    Public Shared Function DecryptCardNumber(ByVal cardnumberByte As Byte(), ByVal keyfile As String, Optional ByVal isRoleAllowedToView As Boolean = False) As String
        ''call the algorithm class that will be use in the decryption
        Dim algorithm As SymmetricAlgorithm = SymmetricAlgorithm.Create(AlgorithmName)

        'read the key generated when in the constructor
        ReadKey(algorithm, keyfile)

        'create a pipeline
        Dim targetStream As New MemoryStream

        Dim pos As Integer = 0
        Dim IVbytes() As Byte = New Byte(algorithm.IV.Length - 1) {}
        Array.Copy(cardnumberByte, IVbytes, IVbytes.Length)

        algorithm.IV = IVbytes
        pos += algorithm.IV.Length

        Dim cs As New CryptoStream(targetStream, algorithm.CreateDecryptor, CryptoStreamMode.Write)
        cs.Write(cardnumberByte, pos, cardnumberByte.Length - pos)
        cs.FlushFinalBlock()

        If isRoleAllowedToView = True Then
            Return Encoding.UTF8.GetString(targetStream.ToArray())
        Else
            Dim partialString As String = Encoding.UTF8.GetString(targetStream.ToArray())
            ''Return New String("*", partialString.Length - 1)
            Return partialString
        End If

    End Function

End Class
