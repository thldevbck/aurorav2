﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic

Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Aurora.Common

Imports Aurora.Booking.Data

Public Class EuropcarServiceLogEntry
    Private mTimeStamp As DateTime
    Private mAction As String
    Private mReferenceNumber As String
    Private mRequestXml As String
    Private mResponseXml As String

    Public Property TimeStamp As DateTime
        Get
            Return mTimeStamp
        End Get
        Set(ByVal value As DateTime)
            mTimeStamp = value
        End Set
    End Property


    Public Property Action As String
        Get
            Return mAction
        End Get
        Set(ByVal value As String)
            mAction = value
        End Set
    End Property

    Public Property ReferenceNumber As String
        Get
            Return mReferenceNumber
        End Get
        Set(ByVal value As String)
            mReferenceNumber = value
        End Set
    End Property

    Public Property RequestXml As String
        Get
            Return mRequestXml
        End Get
        Set(ByVal value As String)
            mRequestXml = value
        End Set
    End Property

    Public Property ResponseXml As String
        Get
            Return mResponseXml
        End Get
        Set(ByVal value As String)
            mResponseXml = value
        End Set
    End Property
End Class


Public Class EuropcarServiceRepository


    Public Overridable Function GetServiceLog( _
        ByVal rentalID As String) As List(Of EuropcarServiceLogEntry)

        Dim dataSet As New EuropcarLogDataSet()

        Data.DataRepository.GetEuropcarServiceLog(dataSet, rentalID)

        Return dataSet.ResultSet
    End Function

    ''' <summary>
    ''' Used to populate log entries from data reader.
    ''' </summary>
    ''' <remarks></remarks>
    Protected Class EuropcarLogDataSet
        Inherits ListDataSet(Of EuropcarServiceLogEntry)

        Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As EuropcarServiceLogEntry
            Dim entry As New EuropcarServiceLogEntry()
            entry.Action = GetDbValue(Of String)(reader, "ThPartySerEvent")
            entry.ReferenceNumber = GetDbValue(Of String)(reader, "ThPartySerRefNum")
            entry.RequestXml = GetDbValue(Of String)(reader, "ThPartySerRQXML")
            entry.ResponseXml = GetDbValue(Of String)(reader, "ThPartySerRSXML")
            entry.TimeStamp = GetDbValue(Of DateTime)(reader, "AddDateTime")


            Return entry
        End Function
    End Class

End Class
