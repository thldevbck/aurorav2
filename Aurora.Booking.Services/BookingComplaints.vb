'-- RKS : 18-Nov-2010
'-- Call 31092: Complaint tab enhancement
Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Reservations

Public Class BookingComplaints

    Public Shared Function GetComplaintSummary(ByVal RentalId As String) As DataTable
        Return DataRepository.GetComplaintSummary(RentalId)
    End Function

    'Public Shared Function GetComplaintData(ByVal ComplaintId As Int64, ByVal RentalId As String) As DataSet
    '    Return DataRepository.GetComplaintData(ComplaintId, RentalId)
    'End Function

    Public Shared Sub GetPageData(ByVal ComplaintId As Int64, ByVal RentalId As String, _
                                  ByRef ComplaintDataTable As DataTable, _
                                  ByRef CauseSubCauseMasterDataTable As DataTable, _
                                  ByRef OwnerGroupUsersMasterDataTable As DataTable, _
                                  ByRef PrimaryCauseDataTable As DataTable, _
                                  ByRef PrimarySubCauseDataTable As DataTable, _
                                  ByRef SecondaryCauseDataTable As DataTable, _
                                  ByRef SecondarySubCauseDataTable As DataTable, _
                                  ByRef TertiaryCauseDataTable As DataTable, _
                                  ByRef TertiarySubCauseDataTable As DataTable, _
                                  ByRef OwnerGroupDataTable As DataTable, _
                                  ByRef OwnerUsersDataTable As DataTable, _
                                  ByRef NotifiedOptionsDataTable As DataTable, _
                                  ByRef VehiclesDataTable As DataTable, _
                                  ByRef HeaderDataTable As DataTable)

        '-- Call 31092: Complaint tab enhancement
        '-- RKS : 18-Nov-2010
        '-- Added usercode for GerComplaintData to valiate role for editing
        Dim sUsrCode As String = UserSettings.Current.UsrCode
        '-- Changed FROM With DataRepository.GetComplaintData(ComplaintId, RentalId)
        '--         TO With DataRepository.GetComplaintData(ComplaintId, RentalId, sUsrCode)
        'With DataRepository.GetComplaintData(ComplaintId, RentalId) -- commented original line RKS : 18-Nov-2010
        With DataRepository.GetComplaintData(ComplaintId, RentalId, sUsrCode)
            If (.Tables.Count > 0) Then
                CauseSubCauseMasterDataTable = .Tables(0)
                OwnerGroupUsersMasterDataTable = .Tables(1)
                NotifiedOptionsDataTable = .Tables(2)
                ComplaintDataTable = .Tables(3)
                VehiclesDataTable = .Tables(4)
                HeaderDataTable = .Tables(5)

                SetCurrentCauseSubCauseTables(ComplaintDataTable, "", "", "", _
                                              CauseSubCauseMasterDataTable, _
                                              PrimaryCauseDataTable, PrimarySubCauseDataTable, _
                                              SecondaryCauseDataTable, SecondarySubCauseDataTable, _
                                              TertiaryCauseDataTable, TertiarySubCauseDataTable)

                SetCurrentOwnerGroupOwnerUserTables(ComplaintDataTable, _
                                                    OwnerGroupUsersMasterDataTable, _
                                                    "", OwnerGroupDataTable, _
                                                    OwnerUsersDataTable)

            Else
                ' big problem - DIE DIE DIE!
            End If
        End With

    End Sub

    ''' <summary>
    ''' This Subroutine populates the Cause and Subcause tables
    ''' </summary>
    ''' <param name="ComplaintDataTable">Pass this in on first load only</param>
    ''' <param name="PrimaryCauseId">Pass this in on subsequent load</param>
    ''' <param name="SecondaryCauseId">Pass this in on subsequent load</param>
    ''' <param name="TertiaryCauseId">Pass this in on subsequent load</param>
    ''' <param name="CauseSubCauseMasterDataTable">Always needed</param>
    ''' <param name="PrimaryCauseDataTable">Pass this in on first load only</param>
    ''' <param name="PrimarySubCauseDataTable">Always needed</param>
    ''' <param name="SecondaryCauseDataTable">Pass this in on first load only</param>
    ''' <param name="SecondarySubCauseDataTable">Always needed</param>
    ''' <param name="TertiaryCauseDataTable">Pass this in on first load only</param>
    ''' <param name="TertiarySubCauseDataTable">Always needed</param>
    ''' <remarks></remarks>
    Public Shared Sub SetCurrentCauseSubCauseTables(ByVal ComplaintDataTable As DataTable, _
                                                    ByVal PrimaryCauseId As String, _
                                                    ByVal SecondaryCauseId As String, _
                                                    ByVal TertiaryCauseId As String, _
                                                    ByVal CauseSubCauseMasterDataTable As DataTable, _
                                                    ByRef PrimaryCauseDataTable As DataTable, _
                                                    ByRef PrimarySubCauseDataTable As DataTable, _
                                                    ByRef SecondaryCauseDataTable As DataTable, _
                                                    ByRef SecondarySubCauseDataTable As DataTable, _
                                                    ByRef TertiaryCauseDataTable As DataTable, _
                                                    ByRef TertiarySubCauseDataTable As DataTable)

        Dim bDoPrimary, bDoSecondary, bDoTertiary As New Boolean

        If ComplaintDataTable IsNot Nothing AndAlso ComplaintDataTable.Rows.Count > 0 Then
            ' Pick up from dataset, if not specified
            If PrimaryCauseId = "" Then
                bDoPrimary = True
                If ReferenceEquals(ComplaintDataTable.Rows(0)("CmpPrimaryRootCause"), System.DBNull.Value) Then
                    PrimaryCauseId = Nothing
                Else
                    PrimaryCauseId = ComplaintDataTable.Rows(0)("CmpPrimaryRootCause").ToString()
                End If
            End If
            If SecondaryCauseId = "" Then
                bDoSecondary = True
                If ReferenceEquals(ComplaintDataTable.Rows(0)("CmpSecondaryRootCause"), System.DBNull.Value) Then
                    SecondaryCauseId = Nothing
                Else
                    SecondaryCauseId = ComplaintDataTable.Rows(0)("CmpSecondaryRootCause").ToString()
                End If

            End If
            If TertiaryCauseId = "" Then
                bDoTertiary = True
                If ReferenceEquals(ComplaintDataTable.Rows(0)("CmpTertiaryRootCause"), System.DBNull.Value) Then
                    TertiaryCauseId = Nothing
                Else
                    TertiaryCauseId = ComplaintDataTable.Rows(0)("CmpTertiaryRootCause").ToString()
                End If
            End If
        Else
            ' not page load
            If PrimaryCauseId <> "" Then
                bDoPrimary = True
            End If
            If SecondaryCauseId <> "" Then
                bDoSecondary = True
            End If
            If TertiaryCauseId <> "" Then
                bDoTertiary = True
            End If
        End If

        If Not bDoPrimary AndAlso Not bDoSecondary AndAlso Not bDoTertiary Then
            ' nothing at all?
            ' this is a new Complaint
            bDoPrimary = True
            PrimaryCauseId = Nothing
            bDoSecondary = True
            SecondaryCauseId = Nothing
            bDoTertiary = True
            TertiaryCauseId = Nothing
        End If

        Dim dvFilterView As New DataView(CauseSubCauseMasterDataTable)

        PrimaryCauseDataTable = dvFilterView.ToTable(True, New String() {"CauId", "CauDesc"})
        SecondaryCauseDataTable = dvFilterView.ToTable(True, New String() {"CauId", "CauDesc"})
        TertiaryCauseDataTable = dvFilterView.ToTable(True, New String() {"CauId", "CauDesc"})

        If bDoPrimary Then
            'If PrimaryCauseId Is Nothing Then
            '    PrimaryCauseId = PrimaryCauseDataTable.Rows(0)("CauId")
            'End If
            dvFilterView.RowFilter = "CauId='" & PrimaryCauseId & "'"
            PrimarySubCauseDataTable = dvFilterView.ToTable(True, New String() {"SbcId", "SbcDesc"})
        End If

        If bDoSecondary Then
            'If SecondaryCauseId Is Nothing Then
            '    SecondaryCauseId = SecondaryCauseDataTable.Rows(0)("CauId")
            'End If
            dvFilterView.RowFilter = "CauId='" & SecondaryCauseId & "'"
            SecondarySubCauseDataTable = dvFilterView.ToTable(True, New String() {"SbcId", "SbcDesc"})
        End If

        If bDoTertiary Then
            'If TertiaryCauseId Is Nothing Then
            '    TertiaryCauseId = TertiaryCauseDataTable.Rows(0)("CauId")
            'End If
            dvFilterView.RowFilter = "CauId='" & TertiaryCauseId & "'"
            TertiarySubCauseDataTable = dvFilterView.ToTable(True, New String() {"SbcId", "SbcDesc"})
        End If

    End Sub

    Public Shared Sub SetCurrentOwnerGroupOwnerUserTables(ByVal ComplaintDataTable As DataTable, _
                                                          ByVal OwnerGroupUsersMasterDataTable As DataTable, _
                                                          ByVal OwnerGroupId As String, _
                                                          ByRef OwnerGroupDataTable As DataTable, _
                                                          ByRef OwnerUsersDataTable As DataTable)
        If ComplaintDataTable IsNot Nothing AndAlso ComplaintDataTable.Rows.Count > 0 Then
            ' Pick up from dataset, if not specified
            OwnerGroupId = ComplaintDataTable.Rows(0)("CmpOwnerGroupRolId").ToString()
        End If

        Dim dvFilterView As New DataView(OwnerGroupUsersMasterDataTable)

        OwnerGroupDataTable = dvFilterView.ToTable(True, New String() {"RolId", "RolDesc"})

        If String.IsNullOrEmpty(OwnerGroupId) Then
            OwnerGroupId = OwnerGroupDataTable.Rows(0)("RolId").ToString()
        End If

        dvFilterView.RowFilter = "RolId='" & OwnerGroupId & "'"

        OwnerUsersDataTable = dvFilterView.ToTable(True, New String() {"UsrCode", "UsrName"})

    End Sub

    Public Shared Function CheckVehicleDataExists(ByVal RentalReference As String, ByVal UserCode As String) As Integer
        Dim iResult As Integer
        Dim oResult As Object = New Object

        Try
            oResult = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.Complaints_CheckVehicleDataExists('" & RentalReference & "','" & UserCode & "')")
            iResult = CInt(oResult)
        Catch
        End Try

        Return iResult
    End Function

End Class
