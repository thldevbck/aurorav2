﻿Imports System.Text
Imports System.Xml
Imports System.Web
Imports System.IO
Imports System.Net
Imports System.Configuration
Imports Aurora.Common.Logging

Public Class BookingEFTPOSBillingToken


#Region "In Javascript Usage"

    Public Function BillingTokenSelectPopulateCardInfo(ByVal rentalid As String, ByVal tokenid As String) As String
        Dim result As New DataSet
        Dim sb As New StringBuilder
        Try
            Aurora.Common.Data.ExecuteDataSetSP("BillingTokenSelectPopulateCardInfo", result, rentalid, tokenid)
            sb.AppendFormat("{0}|", CardTypeList)

            For Each rowItem As DataRow In result.Tables(0).Rows
                sb.AppendFormat("{0}|{1}|{2}|{3}", _
                                                   rowItem("PmtBillToCardType").ToString, _
                                                   rowItem("PmtBillCardNumber").ToString, _
                                                   rowItem("PmtBillToCardName").ToString, _
                                                   rowItem("PmtBillToCardExpDate").ToString)

            Next

            Return sb.ToString
        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try
        Return sb.ToString
    End Function

    Public Function GetBillingTokenForPayment(ByVal rentalID As String) As String
        Dim result As New DataSet
        Dim sb As New StringBuilder

        Aurora.Common.Data.ExecuteDataSetSP("BillingTokenSelectForPayment", result, rentalID)
        sb.AppendFormat("<option value='{0}'>{1}</option>", "", "")
        For Each rowItem As DataRow In result.Tables(0).Rows
            sb.AppendFormat("<option value='{0}'>{1}</option>", rowItem("PmtBillToToken").ToString, rowItem("TokenCard"))
        Next

        Return sb.ToString
    End Function

#End Region

#Region "Private Usage"
    Private Shared Function MaskCard(ByVal cardnumber As String) As StringBuilder
        Dim i As Integer
        Dim sbCard As New StringBuilder
        For Each c As Char In cardnumber
            If i > 3 And i < cardnumber.Length - 2 Then
                sbCard.Append("X")
            Else
                sbCard.Append(c)
            End If
            i = i + 1
        Next
        Return sbCard
    End Function


    Private Shared Function CreateRequestObject(ByVal countrycode As String, ByVal BillingTokenCustomerDataObject As BillingTokenCustomerData) As StringWriter
        Dim sw As New StringWriter()
        Dim xtw As New XmlTextWriter(sw)
        xtw.WriteStartElement("Txn")
        xtw.WriteElementString("PostUsername", BillingTokenCustomerDataObject.UserName)
        xtw.WriteElementString("PostPassword", BillingTokenCustomerDataObject.Password)
        xtw.WriteElementString("CardHolderName", BillingTokenCustomerDataObject.CardHolderName)
        xtw.WriteElementString("CardNumber", BillingTokenCustomerDataObject.CardNumber)
        xtw.WriteElementString("Amount", BillingTokenCustomerDataObject.Amount)
        xtw.WriteElementString("DateExpiry", BillingTokenCustomerDataObject.ExpiryData)
        xtw.WriteElementString("InputCurrency", (countrycode + "D").ToUpper)
        xtw.WriteElementString("TxnType", "Purchase")
        xtw.WriteElementString("BillingId", BillingTokenCustomerDataObject.BillingId)
        xtw.WriteElementString("EnableAddBillCard", Convert.ToInt16(BillingTokenCustomerDataObject.EnableAddBillCard))
        xtw.WriteEndElement()
        xtw.Close()
        Return sw
    End Function

    Private Shared Function InsertTokenObject(ByVal PmtBillToRntId As String, _
                                              ByVal PmtBillToCardType As String, _
                                              ByVal PmtBillToCardName As String, _
                                              ByVal PmtBillCardNumber As String, _
                                              ByVal PmtBillToCardExpDate As String, _
                                              ByVal PmtBillToToken As String, _
                                              ByVal AddUsrId As String, _
                                              ByVal ModUsrId As String) As String

        Try
            Return Aurora.Common.Data.ExecuteScalarSP("BillingTokenInsert", PmtBillToRntId, _
                                                                        PmtBillToCardType, _
                                                                        PmtBillToCardName, _
                                                                        PmtBillCardNumber, _
                                                                        PmtBillToCardExpDate, _
                                                                        PmtBillToToken, _
                                                                        AddUsrId)

        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try


    End Function

    Private Shared Sub NewBillingTokenExtracted(ByVal userId As String, ByVal BillingTokenCustomerDataObject As BillingTokenCustomerData, ByVal sb As StringBuilder, ByVal sw As StringWriter, ByVal dpsUrlService As String, ByVal UseProxy As String, ByVal httpProxy As String)
        Dim wrq As WebRequest = WebRequest.Create(dpsUrlService)
        wrq.Method = "POST"
        wrq.ContentType = "application/x-www-form-urlencoded"
        If UseProxy Then
            Dim localProxy As New WebProxy
            localProxy.Address = New Uri(httpProxy)
            wrq.Proxy = localProxy
        End If

        Dim b As Byte() = Encoding.ASCII.GetBytes(sw.ToString())
        wrq.ContentLength = b.Length

        Dim s As Stream = wrq.GetRequestStream()
        s.Write(b, 0, b.Length)
        s.Close()

        ' Check the response
        Dim wrs As WebResponse = wrq.GetResponse()
        If wrs IsNot Nothing Then
            Dim sr As New StreamReader(wrs.GetResponseStream())
            Dim xd As New XmlDocument()
            xd.LoadXml(sr.ReadToEnd().Trim())
            If xd.SelectSingleNode("/Txn/Success") IsNot Nothing Then


                If Convert.ToBoolean(CInt(xd.SelectSingleNode("/Txn/Success").InnerText)) = True Then
                    Dim cardnumber As String = BillingTokenCustomerDataObject.CardNumber
                    Dim sbCard As StringBuilder = MaskCard(cardnumber)
                    BillingTokenCustomerDataObject.CardNumber = sbCard.ToString
                    BillingTokenCustomerDataObject.ExpiryData = BillingTokenCustomerDataObject.ExpiryData.Insert(2, "/")
                    InsertTokenObject(BillingTokenCustomerDataObject.RentalID, _
                                    BillingTokenCustomerDataObject.CardType, _
                                    BillingTokenCustomerDataObject.CardHolderName, _
                                    BillingTokenCustomerDataObject.CardNumber, _
                                    BillingTokenCustomerDataObject.ExpiryData, _
                                    xd.SelectSingleNode("/Txn/Transaction/DpsBillingId").InnerText, _
                                    userId, _
                                    userId)
                End If


                With sb
                    .AppendFormat("{0}|", xd.SelectSingleNode("/Txn/Transaction/DpsBillingId").InnerText)
                    .AppendFormat("ResponseText: &nbsp;{0}", xd.SelectSingleNode("/Txn/ResponseText").InnerText)
                End With


                'With sb
                '    .Append("<table>")
                '    .AppendFormat("<tr><td>DPSBillingID</td> <td>{0}</td></tr>", xd.SelectSingleNode("/Txn/Transaction/DpsBillingId").InnerText)
                '    .AppendFormat("<tr><td>Authorized  </td> <td>{0}</td></tr>", Convert.ToBoolean(CInt(xd.SelectSingleNode("/Txn/Transaction/Authorized").InnerText)))
                '    .AppendFormat("<tr><td>Response    </td> <td>{0}</td></tr>", xd.SelectSingleNode("/Txn/ResponseText").InnerText)
                '    .AppendFormat("<tr><td>HelpText    </td> <td>{0}</td></tr>", xd.SelectSingleNode("/Txn/HelpText").InnerText)
                '    .AppendFormat("<tr><td>Success     </td> <td>{0}</td></tr>", Convert.ToBoolean(CInt(xd.SelectSingleNode("/Txn/Success").InnerText)))
                '    .Append("</table>")
                'End With
            End If
        End If
    End Sub

#End Region

#Region "Public Usage"

    Public Function BillingTokenSelectPopulate(ByVal rentalid As String, ByVal tokenid As String) As DataSet
        Dim result As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("BillingTokenSelectPopulateCardInfo", result, rentalid, tokenid)
        Catch ex As Exception
            Return Nothing
        End Try
        Return result
    End Function

    Public Function GetBillingToken(ByVal rentalID As String) As DataSet
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("BillingTokenSelectForPayment", result, rentalID)
        Return result
    End Function

    Public Function GetCreditCards() As DataSet
        Dim xmlString As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getMethodType", "00B1F2C5-0112-44A4-BDAE-D21561F8D911").OuterXml
        Dim dsLocal As New DataSet
        dsLocal.ReadXml(New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing))
        Return dsLocal
    End Function

    Public Function CardTypeList(Optional ByVal guidCard As String = "00B1F2C5-0112-44A4-BDAE-D21561F8D911") As String
        Dim xmlString As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getMethodType", guidCard).OuterXml
        Dim dsLocal As New DataSet
        dsLocal.ReadXml(New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing))

        Dim sb As New StringBuilder
        For Each rowItem As DataRow In dsLocal.Tables(0).Rows
            sb.AppendFormat("<option value='{0}'>{1}</option>", rowItem("id").ToString, rowItem("name"))
        Next

        Return sb.ToString
    End Function

    Public Function GetAllToken(ByVal rentalId As String) As String
        Dim result As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("BillingTokenSelect", result, rentalId)

        Dim sb As New StringBuilder
        sb.Append("<table class='dataTable'>")
        sb.Append("<tr>")
        sb.AppendFormat("<th style='width: 100px;'>{0}</th>", "Card Type")
        sb.AppendFormat("<th style='width: 150px;'>{0}</th>", "Card Number")
        sb.AppendFormat("<th style='width: 200px;'>{0}</th>", "CardHolder Name")
        sb.AppendFormat("<th>{0}</th>", "Expiry Date")

        sb.Append("</tr>")
        Dim i As Integer = 1
        For Each rowItem As DataRow In result.Tables(0).Rows
            If i Mod 2 = 0 Then
                sb.Append("<tr class='evenRow'>")
            Else
                sb.Append("<tr class='oddRow'>")
            End If

            sb.AppendFormat("<td>{0}</td>", rowItem("PtmName").ToString)
            sb.AppendFormat("<td>{0}</td>", rowItem("PmtBillCardNumber").ToString)
            sb.AppendFormat("<td>{0}</td>", rowItem("PmtBillToCardName").ToString)
            sb.AppendFormat("<td>{0}</td>", rowItem("PmtBillToCardExpDate").ToString)

            sb.Append("</tr>")
            i = i + 1
        Next
        sb.Append("</table>")
        Return sb.ToString
    End Function

    Public Function NewBillingToken(ByVal CardHolderName As String, _
         ByVal CardNumber As String, _
         ByVal Amount As String, _
         ByVal ExpiryData As String, _
         ByVal DPSBillingId As String, _
         ByVal EnableAddBillCard As Boolean, _
         ByVal countrycode As String, _
         ByVal MerchantReference As String, _
         ByVal rentalid As String, _
         ByVal cardtype As String, _
         ByVal userId As String) As String


        Dim BillingTokenCustomerDataObject As New BillingTokenCustomerData(userId, rentalid)
        ''Dim BillingTokenCustomerDataObject As New BillingTokenCustomerData(countrycode)
        Dim sb As New StringBuilder
        Try

            With BillingTokenCustomerDataObject
                .CardHolderName = CardHolderName
                .CardNumber = CardNumber
                .Amount = Amount
                .ExpiryData = ExpiryData
                .EnableAddBillCard = EnableAddBillCard
                .MerchantReference = MerchantReference
                .BillingId = MerchantReference
                .RentalID = rentalid
                .CardType = cardtype
            End With

            Dim sw As StringWriter = CreateRequestObject(countrycode, BillingTokenCustomerDataObject)

            ' Send the Xml message to PXPost
            Dim dpsUrlService As String = ConfigurationManager.AppSettings("DPSServiceURL").ToString
            Dim UseProxy As String = ConfigurationManager.AppSettings("UseProxy").ToString
            Dim httpProxy As String = ConfigurationManager.AppSettings("httpProxy").ToString



            NewBillingTokenExtracted(userId, BillingTokenCustomerDataObject, sb, sw, dpsUrlService, UseProxy, httpProxy)

        Catch ex As Exception
            Return "ERROR: " + ex.Message
        End Try

        Return sb.ToString
    End Function

#End Region

#Region "PXPAY Usages"

#End Region

    Public Sub New()

    End Sub
End Class

Public Class BillingTokenCustomerData
    Public Property CardType As String
    Public Property CardHolderName As String
    Public Property CardNumber As String
    Public Property Amount As String
    Public Property ExpiryData As String
    Public Property BillingId As String
    Public Property DPSBillingId As String
    Public Property EnableAddBillCard As Boolean
    Public Property MerchantReference As String
    Public Property RentalID As String

    Public UserName As String
    Public Password As String

    Sub New(ByVal country As String)
        Dim xml As New XmlDocument
        xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))

        UserName = xml.SelectSingleNode("DpsBillingToken/Country[@code='" + country + "']/UserName").InnerText
        Password = xml.SelectSingleNode("DpsBillingToken/Country[@code='" + country + "']/Password").InnerText

    End Sub

    Sub New(ByVal userCode As String, ByVal rentalId As String)

        Dim brdcode As String = ""
        Dim userCurrency As String = ""
        Dim ComCode As String = ""
        Try

            Dim values As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("GEN_getDefValuesForUser", userCode, rentalId).OuterXml  ''SelectSingleNode("UserInfo/BrdCode").InnerText
            Dim xml As New XmlDocument
            xml.LoadXml(values)

            
            If Not (xml.SelectSingleNode("data/UserInfo/BrdCode")) Is Nothing Then
                brdcode = xml.SelectSingleNode("data/UserInfo/BrdCode").InnerText
            End If

            If Not (xml.SelectSingleNode("data/UserInfo/BrdCode")) Is Nothing Then
                userCurrency = xml.SelectSingleNode("data/UserInfo/UsrCtyCode").InnerText
            End If


            ComCode = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getSettings", userCode).SelectSingleNode("data/UserInfo/ComCode").InnerText

            MapPasswordsForBrands(UserName, Password, ComCode, userCurrency, brdcode)
            LogInformation("BillingTokenCustomerData(BILLING TOKEN)", String.Concat("INFORMATION: USERNAME: ", UserName, " , PASSWORD: ", Password, " , COMCODE: ", ComCode, " , USERCURRENCY: ", userCurrency, " , BRDCODE: ", brdcode))

        Catch ex As Exception
            LogError("BillingTokenCustomerData(BILLING TOKEN)", String.Concat("INVALID CREDENTIALS: USERNAME: ", UserName, " , PASSWORD: ", Password, " , COMCODE: ", ComCode, " , USERCURRENCY: ", userCurrency, " , BRDCODE: ", brdcode, " EXCEPTION: ", ex.Message + ", TRACE: ", ex.StackTrace))
        End Try
        

    End Sub
    Sub MapPasswordsForBrands(ByRef username As String, _
                             ByRef password As String, _
                             ByVal _comCode As String, _
                             ByVal _currencyType As String, _
                             ByVal _brdCode As String)

        Dim xml As New XmlDocument
        xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))
        Dim path As String = "DpsBillingToken/Brands/"

        Try

        If _comCode.Equals("THL") = True Then
            If _currencyType.Equals("NZ") = True Then
                If _brdCode.Equals("B") = True Then
                    username = xml.SelectSingleNode(path + "britz_username").InnerText   ''"/ComCode/britz_username"
                    password = xml.SelectSingleNode(path + "britz_password").InnerText   ''"/ComCode/britz_password"
                Else ''maui
                    username = xml.SelectSingleNode(path + "maui_username").InnerText
                    password = xml.SelectSingleNode(path + "maui_password").InnerText

                End If
            Else ''rentals
                username = xml.SelectSingleNode(path + "rentals_username").InnerText
                password = xml.SelectSingleNode(path + "rentals_password").InnerText
            End If

        Else ''kxz ''------ If _comcode.Equals("THL") = True Then
            If _comCode.Equals("KXS") = True Then '------ If _comcode.Equals("KX") = True Then
                username = xml.SelectSingleNode(path + "kxs_username").InnerText
                password = xml.SelectSingleNode(path + "kxs_password").InnerText
            Else
                ' V2
                ' RKS MOD: 21-Sep-2009 for MAC.COM
                If _comCode.ToUpper().Equals("MAC") = True Then '------ If _comcode.Equals("MAC") = True Then

                    If _currencyType.Equals("NZ") = True Then '------ If _comcode.Equals("NZ") = True Then
                        username = xml.SelectSingleNode(path + "mac_nz_username").InnerText
                        password = xml.SelectSingleNode(path + "mac_nz_password").InnerText
                    Else
                        If _currencyType.Equals("AU") = True Then
                            username = xml.SelectSingleNode(path + "mac_au_username").InnerText
                            password = xml.SelectSingleNode(path + "mac_au_password").InnerText
                        End If
                    End If '------ If _comcode.Equals("NZ") = True Then
                End If
            End If '' '------   If _comcode.Equals("KX") = True Then
            End If ''''kxz ''------ If _comcode.Equals("THL") = True Then

            LogInformation("BOOKINGEFTPOSBILLINGTOKEN", String.Concat("BILLING TOKEN: INFORMATION: USERNAME: ", username, " , PASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode))
        Catch ex As Exception
            LogInformation("BOOKINGEFTPOSBILLINGTOKEN", String.Concat("BILLING TOKEN: ERROR: USERNAME: ", username, " , PASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode, " EXCEPTION: ", ex.Message + ", TRACE: ", ex.StackTrace))
        End Try

    End Sub
End Class


