Imports Aurora.Common.data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common

Public Class BookingPaymentItem
    Public pPdtId As Object
    Public pPmtPmId As Object
    Public pAccountName As Object
    Public pCurrency As Object
    Public pAmount As Object
    Public pLocalAmount As Double
    Public pApproval As Object
    Public pExpDate As Object
    Public pChequeNo As Object
    Public pBank As Object
    Public pBranch As Object
    Public pAccountNo As Object
    Public pIntegrity As Object
    Public pAuthCode As Object
    Public pMerchRef As Object
    Public pTxnRef As Object

    ''rev:mia PdtDpsEfTxnRef -- March 8 2010
    Public pPdtDpsEfTxnRef As String

    ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
    Public PdtEFTPOSCardMethodDontMatch As String
    Public PdtEFTPOSCardTypeSwiped As String
    Public PdtEFTPOSCardTypeSurchargeAdded As String
End Class

Public Class BookingPaymentItemsCollection
    Implements System.Collections.IEnumerable

    Private mcolCollection As New Collection
    Public Function Add(ByVal pItem As BookingPaymentItem) As BookingPaymentItem
        Dim itemNew As New BookingPaymentItem
        With itemNew
            .pPdtId = pItem.pPdtId
            .pAccountName = pItem.pAccountName
            .pPmtPmId = pItem.pPmtPmId
            .pApproval = pItem.pApproval
            .pExpDate = pItem.pExpDate
            .pChequeNo = pItem.pChequeNo
            .pBank = pItem.pBank
            .pBranch = pItem.pBranch
            .pAccountNo = pItem.pAccountNo
            .pAmount = pItem.pAmount
            .pIntegrity = pItem.pIntegrity
            .pAuthCode = pItem.pAuthCode
            .pMerchRef = pItem.pMerchRef
            .pTxnRef = pItem.pTxnRef

            ''rev:mia PdtDpsEfTxnRef -- March 8 2010
            .pPdtDpsEfTxnRef = pItem.pPdtDpsEfTxnRef

            ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
            .PdtEFTPOSCardMethodDontMatch = pItem.PdtEFTPOSCardMethodDontMatch
            .PdtEFTPOSCardTypeSwiped = pItem.PdtEFTPOSCardTypeSwiped
            .PdtEFTPOSCardTypeSurchargeAdded = pItem.PdtEFTPOSCardTypeSurchargeAdded
        End With
        mcolCollection.Add(itemNew)
        Add = itemNew
    End Function

    Public Function Count() As Long
        Count = mcolCollection.Count
    End Function

    Public Sub Delete(ByVal index As Object)
        mcolCollection.Remove(index)
    End Sub


    Public Function item(ByVal index As Object) As BookingPaymentItem
        item = mcolCollection.Item(index)
    End Function


    Function GetEnumerator() As System.Collections.IEnumerator Implements System.Collections.IEnumerable.GetEnumerator
        GetEnumerator = mcolCollection.GetEnumerator
    End Function




End Class
