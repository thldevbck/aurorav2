Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookingMatchProduct

    Public Shared Function GetMatchProducts(ByVal bookingId As String, ByVal rentalId As String) As DataSet

        Return Data.DataRepository.GetMatchProducts(bookingId, rentalId)

    End Function

    Public Shared Function UpdateMatchProducts(ByVal xmlDoc As XmlDocument) As String

        Return Data.DataRepository.UpdateMatchProducts(xmlDoc)

    End Function

End Class
