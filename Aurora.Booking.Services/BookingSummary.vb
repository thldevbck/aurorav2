Imports Aurora.Booking.Data
Imports Aurora.Common

Public Class BookingSummary

    Public Shared Function GetBookingummary(ByVal bookingId As String, ByVal rentalId As String, ByVal isAll As Boolean) As DataTable

        Dim ds As DataSet
        ds = DataRepository.GetBookingummary(bookingId, rentalId, isAll)

        Dim dt As DataTable = New DataTable
        ' Define the columns for the table.
        For j As Integer = 0 To ds.Tables(3).Rows.Count
            dt.Columns.Add(New DataColumn(j.ToString(), GetType(String)))
        Next j

        'dt.Columns.Add(New DataColumn("Title", GetType(String)))
        'dt.Columns.Add(New DataColumn("Customer", GetType(String)))
        'dt.Columns.Add(New DataColumn("Agent", GetType(String)))

        Dim dr As DataRow

        dr = dt.NewRow()
        If ds.Tables(3).Rows.Count = 4 Then
            dr(1) = ds.Tables(1).Rows(0)(0)
            dr(2) = ds.Tables(1).Rows(0)(0)
            dr(3) = ds.Tables(1).Rows(1)(0)
            dr(4) = ds.Tables(1).Rows(1)(0)
        ElseIf ds.Tables(1).Rows.Count = 2 And ds.Tables(3).Rows.Count = 2 Then
            dr(1) = ds.Tables(1).Rows(0)(0)
            dr(2) = ds.Tables(1).Rows(1)(0)
        ElseIf ds.Tables(1).Rows.Count = 1 And ds.Tables(3).Rows.Count = 2 Then
            dr(1) = ds.Tables(1).Rows(0)(0)
            dr(2) = ds.Tables(1).Rows(0)(0)
        ElseIf ds.Tables(1).Rows.Count = 1 And ds.Tables(3).Rows.Count = 1 Then
            dr(1) = ds.Tables(1).Rows(0)(0)
        End If

        dt.Rows.Add(dr)

        Dim title() As String = {"", "", "Gross", "Agent Discount", "Total Owing", "Total Paid", "Total Due", "Deposit Owing", "Deposit Paid", "Deposit Due", "Security Owing", "Security Paid", "Security Due", "GST Content"}

        For i As Integer = 3 To 27 Step 2
            dr = dt.NewRow()
            dr(0) = title((i - 1) / 2) 'ds.Tables(i).TableName
            'dr(1) = ds.Tables(i).Rows(0)(0)
            'dr(2) = ds.Tables(i).Rows(1)(0)
            For k As Integer = 0 To ds.Tables(3).Rows.Count - 1
                dr(k + 1) = ds.Tables(i).Rows(k)(0)
            Next k

            dt.Rows.Add(dr)

        Next

        Return dt

    End Function

End Class


