Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml

Public Class BookedProductSearch

    Public Shared Function GetProductTypeData() As DataTable
        Dim dstPrdTypes As DataSet
        dstPrdTypes = Aurora.Common.Data.GetData("TYPE4", "", "", "")
        Dim dtPrdTypes As DataTable = New DataTable
        If dstPrdTypes.Tables.Count = 1 Then
            dtPrdTypes = dstPrdTypes.Tables(0)
        End If
        Return dtPrdTypes
    End Function

    Public Shared Function DoBookedProductSearch(ByVal BookingId As String, ByVal RentalId As String, _
                         ByVal BookedProductStatus As String, ByVal BookedProductInitialStatus As String, _
                         ByVal ProductShortName As String, ByVal ProductClassDescription As String, _
                         ByVal TypeDescription As String, ByVal CheckOutDate As String, ByVal CheckOutLocationCode As String, _
                         ByVal CheckInDate As String, ByVal CheckInLocationCode As String, ByVal BookedProductCodeTypeId As String, _
                         ByVal AgencyReference As String, ByVal ChargeTo As String, ByVal IsCurrent As String, _
                         ByVal IsOriginal As String, ByVal IsFOCProduct As String, ByVal IsTransferredToFinance As String, _
                         ByVal RecordId As String, ByVal UserCode As String) As XmlDocument

        Return DataRepository.DoBookedProductSearch(BookingId, RentalId, BookedProductStatus, BookedProductInitialStatus, _
                                                    ProductShortName, ProductClassDescription, TypeDescription, _
                                                    CheckOutDate, CheckOutLocationCode, CheckInDate, CheckInLocationCode, _
                                                    BookedProductCodeTypeId, AgencyReference, ChargeTo, IsCurrent, IsOriginal, _
                                                    IsFOCProduct, IsTransferredToFinance, RecordId, UserCode)
    End Function


End Class
