''rev:mia november 22 2010
''        - addition of PdtEFTPOSCardMethodDontMatch,PdtEFTPOSCardTypeSwiped parameters in SAVEPAYMENT AND REFUNDPAYMENT and new property was added to the bookingpaymentItemsCollection
''        - changes on paym_managePaymentDetail Table
''        - changes on paym_GetPayment

Imports Aurora.Common.Data
Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common



Public Class BookingPaymentMaintenance

#Region " Constant"
    Private Const AURORA_XPATH_RENTALID = "//Root/Payment/RentalId"
    Private Const RPTADJUST_SUCCESS = "E041E0C9-B95E-486D-9B17-E12C6A7DFDED"
    Private Const AURORA_XPATH_USERLOCCODE = "//Root/Payment/UserLocCode"
    Private Const AURORA_XPATH_CURRENCY = "//Root/Payment/Currency"
    Private Const AURORA_XPATH_PAYMENTMETHOD = "//PaymentMethod"
    Private Const AURORA_XPATH_CONRATE = "//ConRate"
    Private Const AURORA_XPATH_ERRSTATUS = "//*/ErrStatus"
    Private Const AURORA_XPATH_BOOID = "//*/BookingId"
    Private Const AURORA_XPATH_RNTLID = "//*/RentalId"
    Private Const AURORA_XPATH_BOONUM = "//*/BookingNumber"

    Private Const AURORA_XPATH_AMOUNT = "//Root/PaymentEntryRoot/PaymentEntry/Amount"
    Private Const AURORA_XPATH_PDTAMOUNT = "//Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount"

    Private Const AURORA_XPATH_AMOUNTTOPAY = "//*/AmountToPay"
    Private Const AURORA_XPATH_PAYMENTRUNNINGTOTAL = "//*/PaymentRunningTotal"
    Private Const AURORA_XPATH_PAYMENTID2 = "//*/PaymentId"
    Private Const AURORA_XPATH_PAYMENTRUNNINGTOTAL2 = "//*/PaymentRunningTotal"
    Private Const AURORA_XPATH_PMTPAYMENTOUT = "//*/PmtPaymentOut"
    Private Const AURORA_XPATH_ENTRYPAYMENTID = "//*/PmEntryPaymentId"
    Private Const AURORA_XPATH_PAYMENTDATE = "//*/PaymentDate"
    Private Const AURORA_XPATH_STATUS = "//*/Status"
    Private Const AURORA_XPATH_PTMID = "//Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PmtPtmId"
    Private Const AURORA_XPATH_PDTID = "//Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtId"
    Private Const AURORA_XPATH_PAYMENTENTRYSTATUS = "//Root/PaymentEntryRoot/PaymentEntry/Status"
    Private Const AURORA_XPATH_PAYMENTENTRYORIGINALAMOUNT = "//Root/PaymentEntryRoot/PaymentEntry/OriginalAmt"

#End Region


    Private Structure RntPmtRecord
        Dim szId As String
        Dim szRntId As String
        Dim dAmount As Double
        Dim szType As String
        Dim szCurrency As String
        Dim IntegrityNo As Integer
    End Structure

    Private Structure RntPmtRecord2
        Dim RptRntId As Object
        Dim RptType As Object
        Dim RptChargeCurrId As Object
        Dim RptChargeAmt As Object
        Dim RptLocalCurrAmt As Object
        Dim RptIsRefund As Object
        Dim RptIsRefunded As Object
        Dim RptPmtXfrToRntNum As Object
        Dim RptPmtXfrFromRntNum As Object
    End Structure

#Region " Private Functions"

   

    Private Shared Function getSecondPayment(ByVal szParameter As String) As String
        Dim szErrorString As String = ""
        Dim szXmlString As String = ""
        Dim oPaymentDom As New System.Xml.XmlDocument
        Dim root As System.Xml.XmlElement = Nothing

        Try

            szXmlString = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_GetPayment", szParameter).OuterXml
            If InStr(1, szXmlString, "<Error><ErrStatus>") > 0 Then
                szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "", "Application", "No Records Found")
                Return "<Root>" & szErrorString & "<Data></Data>" & "</Root>"
            End If

            oPaymentDom.LoadXml(szXmlString)

            root = oPaymentDom.DocumentElement
            If root.SelectSingleNode("//PaymentEntryItems").ChildNodes.Count = 0 Then
                szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "", "Application", "No Records Found")
                Return "<Root>" & szErrorString & "<Data></Data>" & "</Root>"
            End If

            szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(False, "", "", "")
            oPaymentDom = Nothing

            Return szXmlString

        Catch ex As Exception
            oPaymentDom = Nothing
            Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "Error", "System", ex.Message + "," + ex.StackTrace) & "</Root>"
        End Try
    End Function

    Private Shared Function manageRentalPayment(ByVal bNewPayment As Boolean, _
                                           ByVal szRptId As Object, _
                                           ByVal szRptRntId As Object, _
                                           ByVal szPaymentId As String, _
                                           ByVal szRptType As String, _
                                           ByVal szRptChargeCurrId As String, _
                                           ByVal szRptChargeCurrencyName As String, _
                                           ByVal szRptChargeAmount As Object, _
                                           ByVal szPmtCurrencyId As String, _
                                           ByVal szPmtCurrencyName As String, _
                                           ByVal szRptLocalCurrAmount As Object, _
                                           ByVal bRptIsAmountRefunded As Object, _
                                           ByVal szRptPmtXfrToRntNum As Object, _
                                           ByVal szRptPmtXfrFromRntNum As Object, _
                                           ByVal nRptIntegrityNo As Integer, _
                                           ByVal szAddUsrId As Object, _
                                           ByVal szModUsrId As Object, _
                                           ByVal szAddPrgmName As Object, _
                                           ByVal szUserLocalCurrencyName As Object, _
                                           ByVal t1CustToPayAmt As Object, _
                                           ByRef varErrorString As Object, _
                                           ByVal sOriginalPaymentAmt As Object) As Boolean

        Dim params(15) As Aurora.Common.Data.Parameter

        Dim GUID As String
        Dim bRptIsRefund As Boolean


        Try
            If szRptChargeAmount = 0 Then
                manageRentalPayment = True
                Exit Function
            End If

            If bNewPayment Then
                GUID = Aurora.Common.Data.ExecuteScalarSP("sp_get_newId")
                GUID = Replace(GUID, "<Id>", String.Empty)
                GUID = Replace(GUID, "</Id>", String.Empty)
                GUID = Trim(GUID)
                szRptId = GUID
                nRptIntegrityNo = 0
            End If

            szRptLocalCurrAmount = getExchangeAmount(szRptChargeAmount, _
                                                    szUserLocalCurrencyName, _
                                                    szRptChargeCurrencyName, _
                                                    szUserLocalCurrencyName, _
                                                    varErrorString)

            If Not varErrorString.Equals(String.Empty) Then
                If Len(varErrorString) > 0 Then
                    Exit Function
                End If
            End If

            If Not t1CustToPayAmt.Equals(String.Empty) Then
                If t1CustToPayAmt < 0 And szRptType <> "O" Then
                    bRptIsRefund = True
                Else
                    bRptIsRefund = False
                End If
            End If


            ' params from DB
            ' @RptId					varchar	(64),
            'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTID, 200, 64, szRptId)
            params(0) = New Aurora.Common.Data.Parameter("RptId", DbType.String, szRptId, Parameter.ParameterType.AddInParameter)

            ' @RptRntId				varchar	(64),
            'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTRNTID, 200, 64, szRptRntId)
            params(1) = New Aurora.Common.Data.Parameter("RptRntId", DbType.String, Trim(szRptRntId), Parameter.ParameterType.AddInParameter)

            ' @RptPmtId				varchar	(64),
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTPMTID, 200, 64, szPaymentId)
            params(2) = New Aurora.Common.Data.Parameter("RptPmtId", DbType.String, szPaymentId, Parameter.ParameterType.AddInParameter)

            ' @RptType				varchar	(1),
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTTYPE, 200, 1, szRptType)
            params(3) = New Aurora.Common.Data.Parameter("RptType", DbType.String, szRptType, Parameter.ParameterType.AddInParameter)

            ' @RptChargeCurrId		varchar	(64),
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTCHARGECURRID, 200, 64, szRptChargeCurrId)
            params(4) = New Aurora.Common.Data.Parameter("RptChargeCurrId", DbType.String, szRptChargeCurrId, Parameter.ParameterType.AddInParameter)

            ' @RptChargeAmt			money,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTCHARGEAMT, 6, 8, szRptChargeAmount)
            params(5) = New Aurora.Common.Data.Parameter("RptChargeAmt", DbType.Currency, szRptChargeAmount, Parameter.ParameterType.AddInParameter)

            ' @RptLocalCurrAmt		money,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTLOCALCURRAMT, 6, 8, szRptLocalCurrAmount)
            params(6) = New Aurora.Common.Data.Parameter("RptLocalCurrAmt", DbType.Currency, szRptLocalCurrAmount, Parameter.ParameterType.AddInParameter)

            ' @RptIsRefund			bit	,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTISREFUND, 11, 1, bRptIsRefund)
            params(7) = New Aurora.Common.Data.Parameter("RptIsRefund", DbType.Boolean, bRptIsRefund, Parameter.ParameterType.AddInParameter)

            ' @RptIsRefunded			bit = 0	,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTISREFUNDED, 11, 1, bRptIsAmountRefunded)
            params(8) = New Aurora.Common.Data.Parameter("RptIsRefunded", DbType.Boolean, IIf(bRptIsAmountRefunded = Nothing, False, Convert.ToBoolean(bRptIsAmountRefunded)), Parameter.ParameterType.AddInParameter)

            ' @RptPmtXfrToRntNum		varchar(64) = NULL,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTPMTXFRTORNTNUM, 200, 64, szRptPmtXfrToRntNum)
            params(9) = New Aurora.Common.Data.Parameter("RptPmtXfrToRntNum", DbType.String, szRptPmtXfrToRntNum, Parameter.ParameterType.AddInParameter)

            ' @RptPmtXfrFromRntNum	varchar(64) = NULL,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTPMTXFRFROMRNTNUM, 200, 64, szRptPmtXfrFromRntNum)
            params(10) = New Aurora.Common.Data.Parameter("RptPmtXfrFromRntNum", DbType.String, szRptPmtXfrFromRntNum, Parameter.ParameterType.AddInParameter)

            ' @IntegrityNo			int = 0,
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTINTEGRITYNUMBER, 3, 4, nRptIntegrityNo)
            params(11) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, nRptIntegrityNo, Parameter.ParameterType.AddInParameter)

            ' @AddUsrId				varchar(64) = '',
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ADDUSERID, 200, 64, szAddUsrId)
            params(12) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, szAddUsrId, Parameter.ParameterType.AddInParameter)

            ' @ModUsrId				varchar(64) = '',
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_MODIFYUSERID, 200, 64, szModUsrId)
            params(13) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, szModUsrId, Parameter.ParameterType.AddInParameter)


            ' @AddPrgmName			varchar(64) = '',
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ADDPROGRAMNAME, 200, 64, szAddPrgmName)
            params(14) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, szAddPrgmName, Parameter.ParameterType.AddInParameter)


            ' @sOriginalPmtAmt		money		= NULL 
            ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ORIGINALPMTAMT, 6, 8, sOriginalPaymentAmt)
            params(15) = New Aurora.Common.Data.Parameter("sOriginalPmtAmt", DbType.Currency, IIf(sOriginalPaymentAmt = "", DBNull.Value, sOriginalPaymentAmt), Parameter.ParameterType.AddInParameter)

            ''varErrorString = oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMMANAGERENTALPAYMENT)
            varErrorString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_manageRentalPayment", params)

            If varErrorString <> "SUCCESS" Then
                Exit Function
            Else
                varErrorString = String.Empty
            End If

            manageRentalPayment = True
        Catch ex As Exception
            manageRentalPayment = False
            varErrorString = ex.Message
        End Try

    End Function

    Private Shared Function AdjustRentalPayment(ByVal szPaymentId As String, _
                                              ByVal dAmountToAllocate As Double, _
                                              ByVal szUserLocalCurrency As String, _
                                              ByVal szUserLocalCurrencyName As String, _
                                              ByRef varErrorString As Object)


        ''enumerate exsiting records for the payment at the reantalPayment
        ''and delete or modify those records
        Dim params(1) As Aurora.Common.Data.Parameter
        Dim paramsUpdateRentalPayment(3) As Aurora.Common.Data.Parameter

        Dim szRentalPaymentsXML As String
        Dim rpsNode As System.Xml.XmlNode
        Dim rpsNodeList As System.Xml.XmlNodeList
        Dim RptArray() As RntPmtRecord
        Dim dTotalAllocatedAmount As Double
        Dim dAllocationDifference As Double
        Dim vcount As Integer
        Dim vlength As Integer
        Dim tmpDom As New System.Xml.XmlDocument

        Dim RptChargeAmount As Double
        Dim RptLocalCurrAmount As Double


        Try
            ''paym_getRentalPayments
            ''szRentalPaymentsXML = oAuroraDAL.getRecord(AURORA_DBSTOREDPROC_PAYMGETRENTALPAYMENTS & " '" & szPaymentId & "'")
            szRentalPaymentsXML = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getRentalPayments", szPaymentId).OuterXml

            If Len(szRentalPaymentsXML) = 0 Then
                varErrorString = "paym_getCurrentAmount failed"
                Exit Function
            End If

            szRentalPaymentsXML = "<Root><Data>" & szRentalPaymentsXML & "</Data></Root>"

            Try
                tmpDom.LoadXml(szRentalPaymentsXML)
            Catch ex As Xml.XmlException
                varErrorString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                Exit Function
            End Try


            vlength = tmpDom.ChildNodes(0).ChildNodes(0).ChildNodes.Count
            If vlength <= 0 Then
                Exit Function
            End If

            ReDim RptArray(vlength - 1)

            rpsNodeList = tmpDom.SelectNodes("//RentalPayment")

            For Each rpsNode In rpsNodeList
                Dim szTmpRptAmount As String
                RptArray(vcount).szId = rpsNode.ChildNodes(0).InnerText
                szTmpRptAmount = rpsNode.ChildNodes(1).InnerText
                RptArray(vcount).dAmount = CDbl(szTmpRptAmount)
                RptArray(vcount).szCurrency = rpsNode.ChildNodes(2).InnerText
                RptArray(vcount).szType = rpsNode.ChildNodes(3).InnerText
                RptArray(vcount).IntegrityNo = CLng(rpsNode.ChildNodes(4).InnerText)
                dTotalAllocatedAmount = dTotalAllocatedAmount + RptArray(vcount).dAmount
                vcount = vcount + 1
            Next

            dAllocationDifference = dTotalAllocatedAmount - dAmountToAllocate
            If dAllocationDifference <= 0 Then
                Exit Function
            End If

            ''loop through on the rental paymnet records until the
            ''difference has not been deleted(modified)

            vcount = 0

            Do
                If RptArray(vcount).szType = "O" Then
                    If dAllocationDifference >= RptArray(vcount).dAmount Then

                        ''delete record
                        ''@sPdtId			varchar(64),
                        ' @IntegrityNo	int

                        params(0) = New Aurora.Common.Data.Parameter("sPdtId", DbType.String, RptArray(vcount).szId, Parameter.ParameterType.AddInParameter)
                        params(1) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, RptArray(vcount).IntegrityNo, Parameter.ParameterType.AddInParameter)
                        varErrorString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_deleteRentalPayment", params) ''oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMDELETERENTALPAYMENT)


                        If varErrorString <> "SUCCESS" Then
                            Exit Function
                        Else
                            varErrorString = String.Empty
                        End If

                        ''if deleted then call this function again
                        Call AdjustRentalPayment(szPaymentId, dAmountToAllocate, szUserLocalCurrency, szUserLocalCurrencyName, varErrorString)
                        If Not varErrorString.Equals(String.Empty) Then
                            If Len(varErrorString) > 0 Then
                                Exit Function
                            End If
                        End If

                    Else
                        ''modify
                        RptLocalCurrAmount = RptArray(vcount).dAmount - dAllocationDifference
                        If RptArray(vcount).szCurrency <> szUserLocalCurrency Then

                            RptChargeAmount = ConvertAmount(szUserLocalCurrencyName, _
                                                            szUserLocalCurrencyName, _
                                                            RptArray(vcount).szCurrency, _
                                                            RptLocalCurrAmount, _
                                                            varErrorString)
                            If Not varErrorString.Equals(String.Empty) Then
                                If Len(varErrorString) > 0 Then
                                    Exit Function
                                End If
                            End If

                        Else
                            RptChargeAmount = RptLocalCurrAmount
                        End If



                        
                        ' params from DB

                        ' @sPdtId			varchar(64),
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTID, 200, 64, RptArray(vcount).szId)
                        paramsUpdateRentalPayment(0) = New Aurora.Common.Data.Parameter("sPdtId", DbType.String, RptArray(vcount).szId, Parameter.ParameterType.AddInParameter)

                        ' @ChargeAmount	money,
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_CHARGEAMOUNT, 6, 8, RptChargeAmount)
                        paramsUpdateRentalPayment(1) = New Aurora.Common.Data.Parameter("ChargeAmount", DbType.Currency, RptChargeAmount, Parameter.ParameterType.AddInParameter)

                        ' @LocalAmount	money,	
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_LOCALAMOUNT, 6, 8, RptLocalCurrAmount)
                        paramsUpdateRentalPayment(2) = New Aurora.Common.Data.Parameter("LocalAmount", DbType.Currency, RptLocalCurrAmount, Parameter.ParameterType.AddInParameter)

                        '' @IntegrityNo	int	
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTINTEGRITYNUMBER, 3, 4, RptArray(vcount).IntegrityNo)
                        paramsUpdateRentalPayment(3) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, RptArray(vcount).IntegrityNo, Parameter.ParameterType.AddInParameter)

                        varErrorString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_updateRentalPayment", paramsUpdateRentalPayment) ''oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMUPDATERENTALPAYMENT)

                        If varErrorString <> "SUCCESS" Then
                            Exit Function
                        Else
                            varErrorString = RPTADJUST_SUCCESS
                            Exit Function
                        End If

                    End If
                End If
                vcount = vcount + 1
            Loop While ((dAllocationDifference > 0) And vcount < vlength)

            vcount = 0

            ''should not be any "O" type, but make sure
            Do
                If RptArray(vcount).szType <> "O" Then
                    If dAllocationDifference >= RptArray(vcount).dAmount Then

                        ''delete record
                        ' params from DB
                        ' @sPdtId			varchar(64),
                        ' @IntegrityNo	int
                        params = Nothing
                        params(0) = New Aurora.Common.Data.Parameter("sPdtId", DbType.String, RptArray(vcount).szId, Parameter.ParameterType.AddInParameter)
                        params(1) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, RptArray(vcount).IntegrityNo, Parameter.ParameterType.AddInParameter)
                        varErrorString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_deleteRentalPayment", params) ''oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMDELETERENTALPAYMENT)

                        'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTID, 200, 64, RptArray(vcount).szId)
                        'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTINTEGRITYNUMBER, 3, 4, RptArray(vcount).IntegrityNo)

                        'varErrorString = oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMDELETERENTALPAYMENT)

                        If varErrorString <> "SUCCESS" Then
                            Exit Function
                        Else
                            varErrorString = String.Empty
                        End If

                        ''if deleted then call this function again
                        Call AdjustRentalPayment(szPaymentId, dAmountToAllocate, szUserLocalCurrency, szUserLocalCurrencyName, varErrorString)
                        If Not varErrorString.Equals(String.Empty) Then
                            If Len(varErrorString) > 0 Then
                                Exit Function
                            End If
                        End If
                    Else
                        ''modify
                        RptLocalCurrAmount = RptArray(vcount).dAmount - dAllocationDifference
                        If RptArray(vcount).szCurrency <> szUserLocalCurrency Then

                            RptChargeAmount = ConvertAmount(szUserLocalCurrencyName, _
                                                            szUserLocalCurrencyName, _
                                                            RptArray(vcount).szCurrency, _
                                                            RptLocalCurrAmount, _
                                                            varErrorString)
                            If Not varErrorString.Equals(String.Empty) Then
                                If Len(varErrorString) > 0 Then
                                    Exit Function
                                End If
                            End If

                        Else
                            RptChargeAmount = RptLocalCurrAmount
                        End If

                        ' params from DB
                        ' @sPdtId			varchar(64),
                        ' @ChargeAmount	money,
                        ' @LocalAmount	money,	
                        ' @IntegrityNo	int	
                        'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTID, 200, 64, RptArray(vcount).szId)
                        'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_CHARGEAMOUNT, 6, 8, RptChargeAmount)
                        'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_LOCALAMOUNT, 6, 8, RptLocalCurrAmount)
                        'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTINTEGRITYNUMBER, 3, 4, RptArray(vcount).IntegrityNo)

                        'varErrorString = oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMUPDATERENTALPAYMENT)

                        ' params from DB
                        paramsUpdateRentalPayment = Nothing
                        ' @sPdtId			varchar(64),
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTID, 200, 64, RptArray(vcount).szId)
                        paramsUpdateRentalPayment(0) = New Aurora.Common.Data.Parameter("sPdtId", DbType.String, RptArray(vcount).szId, Parameter.ParameterType.AddInParameter)

                        ' @ChargeAmount	money,
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_CHARGEAMOUNT, 6, 8, RptChargeAmount)
                        paramsUpdateRentalPayment(1) = New Aurora.Common.Data.Parameter("ChargeAmount", DbType.Currency, RptChargeAmount, Parameter.ParameterType.AddInParameter)

                        ' @LocalAmount	money,	
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_LOCALAMOUNT, 6, 8, RptLocalCurrAmount)
                        paramsUpdateRentalPayment(2) = New Aurora.Common.Data.Parameter("LocalAmount", DbType.Currency, RptLocalCurrAmount, Parameter.ParameterType.AddInParameter)

                        '' @IntegrityNo	int	
                        ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PDTINTEGRITYNUMBER, 3, 4, RptArray(vcount).IntegrityNo)
                        paramsUpdateRentalPayment(3) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, RptArray(vcount).IntegrityNo, Parameter.ParameterType.AddInParameter)

                        varErrorString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_updateRentalPayment", paramsUpdateRentalPayment) ''oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMUPDATERENTALPAYMENT)

                        If varErrorString <> "SUCCESS" Then
                            Exit Function
                        Else
                            varErrorString = RPTADJUST_SUCCESS
                            ''exit from the function
                            Exit Function
                        End If

                    End If
                End If
                vcount = vcount + 1
            Loop While ((dAllocationDifference > 0) And vcount < vlength)

        Catch ex As Exception
            AdjustRentalPayment = 0
            varErrorString = ex.Message
        Finally
            paramsUpdateRentalPayment = Nothing
            params = Nothing
        End Try

    End Function

    Private Shared Function rentalPaymentReversal(ByVal szOrigPaymentId As String, _
                                           ByVal szNewPaymentId As String, _
                                           ByVal sOriginalPmtAmt As String, _
                                           ByVal sUsrId As String, _
                                           ByVal sProgname As String, _
                                           ByRef varErrorString As Object, _
                                           Optional ByVal isPaymentChange As Boolean = False, _
                                           Optional ByVal amtToPay As String = "0.00", _
                                           Optional ByVal localamtToPay As String = "0.00") As Integer
        Dim szRentalPaymentsXML As String
        Dim rpsNode As System.Xml.XmlNode
        Dim rpsNodeList As System.Xml.XmlNodeList
        Dim RptArray() As RntPmtRecord2
        Dim vcount As Integer
        Dim vlength As Integer


        Dim RptId As String
        Dim GUID As String
        Dim varEmptyParameter As Object

        Dim params(15) As Aurora.Common.Data.Parameter
        Dim tmpDom As New System.Xml.XmlDocument
        Try
            ''paym_getRentalPayments2

            ''szRentalPaymentsXML = oAuroraDAL.getRecord(AURORA_DBSTOREDPROC_PAYMGETRENTALPAYMENTS2 & " '" & szOrigPaymentId & "'")
            szRentalPaymentsXML = Aurora.Common.Data.ExecuteScalarSP("paym_getRentalPayments2", szOrigPaymentId)

            If InStr(1, szRentalPaymentsXML, "<Error><ErrStatus>") > 0 Then
                varErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", "No Records Found")
                Return 0
            End If

            If Len(szRentalPaymentsXML) = 0 Then
                ''error
                varErrorString = "rentalPaymentReversal() '" & szOrigPaymentId & "' failed"
                Return 0
            End If

            szRentalPaymentsXML = "<Root><Data>" & szRentalPaymentsXML & "</Data></Root>"


            Try

                tmpDom.LoadXml(szRentalPaymentsXML)
                ''rev:mia July 15 2010
                Dim RptChargeAmt As String = tmpDom.SelectSingleNode("Root/Data/RentalPayment/ChargeAmt").InnerText.TrimEnd
                Dim RptLocalCurrAmt As String = tmpDom.SelectSingleNode("Root/Data/RentalPayment/LocalCurrAmt").InnerText.TrimEnd

                If isPaymentChange Then
                    ''Dim temPayment As String = RptChargeAmt ''sOriginalPmtAmt.Replace("-", "").TrimEnd
                    ''tmpDom.SelectSingleNode("Root/Data/RentalPayment/ChargeAmt").InnerText = (CDec(sOriginalPmtAmt.TrimEnd) - CDec(RptChargeAmt)) + CDec(RptChargeAmt)
                    ''tmpDom.SelectSingleNode("Root/Data/RentalPayment/LocalCurrAmt").InnerText = (CDec(sOriginalPmtAmt.TrimEnd) - CDec(RptLocalCurrAmt)) + CDec(RptLocalCurrAmt)
                    If (CDec(RptChargeAmt) < 0) Then
                        tmpDom.SelectSingleNode("Root/Data/RentalPayment/ChargeAmt").InnerText = -1 * amtToPay
                    Else
                        amtToPay = amtToPay.Replace("-", "")
                        tmpDom.SelectSingleNode("Root/Data/RentalPayment/ChargeAmt").InnerText = amtToPay
                    End If

                    If (CDec(RptLocalCurrAmt) < 0) Then
                        tmpDom.SelectSingleNode("Root/Data/RentalPayment/LocalCurrAmt").InnerText = -1 * localamtToPay
                    Else
                        localamtToPay = localamtToPay.Replace("-", "")
                        tmpDom.SelectSingleNode("Root/Data/RentalPayment/LocalCurrAmt").InnerText = localamtToPay
                    End If
                Else
                    ''rev:mia March 22 2011- some unknow reason..these two variables are empty
                    If (String.IsNullOrEmpty(RptChargeAmt) AndAlso String.IsNullOrEmpty(RptLocalCurrAmt)) Then
                        tmpDom.SelectSingleNode("Root/Data/RentalPayment/ChargeAmt").InnerText = -1 * CDec(amtToPay)
                        tmpDom.SelectSingleNode("Root/Data/RentalPayment/LocalCurrAmt").InnerText = -1 * CDec(localamtToPay)
                        Logging.LogError("RENTALPAYMENTERVERSAL: ", "ChargeAmt AND RptLocalCurrAmt are empty.PaymentID is " & szOrigPaymentId)
                    End If

                End If


            Catch ex As System.Xml.XmlException
                varErrorString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                Return 0
            End Try

            vlength = tmpDom.ChildNodes(0).ChildNodes(0).ChildNodes.Count
            If vlength <= 0 Then
                Return 0
            End If

            ReDim RptArray(vlength - 1)

            rpsNodeList = tmpDom.SelectNodes("//RentalPayment")

            For Each rpsNode In rpsNodeList
                RptArray(vcount).RptRntId = rpsNode.ChildNodes(0).InnerText.Trim()
                RptArray(vcount).RptType = rpsNode.ChildNodes(1).InnerText.Trim()
                RptArray(vcount).RptChargeCurrId = rpsNode.ChildNodes(2).InnerText.Trim()

                ''rev:mia March 22 2011- some unknow reason..these two variables are empty
                ''----------------------------------------------------------------------
                ''i'm going to put fallback just incase these two fields are empty
                If String.IsNullOrEmpty(rpsNode.ChildNodes(3).InnerText) Then
                    RptArray(vcount).RptChargeAmt = 0
                    Logging.LogError("RENTALPAYMENTERVERSAL: ", "RptChargeAmt: doing fallback by setting to zero ")
                Else
                    RptArray(vcount).RptChargeAmt = 0 - CDbl(rpsNode.ChildNodes(3).InnerText)
                End If

                If String.IsNullOrEmpty(rpsNode.ChildNodes(4).InnerText) Then
                    RptArray(vcount).RptLocalCurrAmt = 0
                    Logging.LogError("RENTALPAYMENTERVERSAL: ", "RptLocalCurrAmt: doing fallback by setting to zero ")
                Else
                    RptArray(vcount).RptLocalCurrAmt = 0 - CDbl(rpsNode.ChildNodes(4).InnerText)
                End If
                ''----------------------------------------------------------------------

                RptArray(vcount).RptIsRefund = rpsNode.ChildNodes(5).InnerText.Trim()
                RptArray(vcount).RptIsRefunded = rpsNode.ChildNodes(6).InnerText.Trim()
                RptArray(vcount).RptPmtXfrToRntNum = rpsNode.ChildNodes(7).InnerText.Trim()
                RptArray(vcount).RptPmtXfrFromRntNum = rpsNode.ChildNodes(8).InnerText.Trim()

                vcount = vcount + 1
            Next

            ''loop through on the reantal paymnet records until the
            ''difference has not been deleted(modified)

            vcount = 0

            Do
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ''do manageRentalPayment
                GUID = Aurora.Common.Data.ExecuteScalarSP("sp_get_newId")
                GUID = Replace(GUID, "<Id>", String.Empty)
                GUID = Replace(GUID, "</Id>", String.Empty)
                GUID = Trim(GUID)
                RptId = GUID


                ' params from DB
                ' @RptId					varchar	(64),
                'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTID, 200, 64, RptId)
                params(0) = New Aurora.Common.Data.Parameter("RptId", DbType.String, RptId, Parameter.ParameterType.AddInParameter)

                ' @RptRntId				varchar	(64),
                'oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTRNTID, 200, 64, RptArray(vcount).RptRntId)
                params(1) = New Aurora.Common.Data.Parameter("RptRntId", DbType.String, RptArray(vcount).RptRntId, Parameter.ParameterType.AddInParameter)

                ' @RptPmtId				varchar	(64),
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTPMTID, 200, 64, szNewPaymentId)
                params(2) = New Aurora.Common.Data.Parameter("RptPmtId", DbType.String, szNewPaymentId, Parameter.ParameterType.AddInParameter)

                ' @RptType				varchar	(1),
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTTYPE, 200, 1, RptArray(vcount).RptType)
                params(3) = New Aurora.Common.Data.Parameter("RptType", DbType.String, RptArray(vcount).RptType, Parameter.ParameterType.AddInParameter)

                ' @RptChargeCurrId		varchar	(64),
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTCHARGECURRID, 200, 64, RptArray(vcount).RptChargeCurrId)
                params(4) = New Aurora.Common.Data.Parameter("RptChargeCurrId", DbType.String, RptArray(vcount).RptChargeCurrId, Parameter.ParameterType.AddInParameter)

                ' @RptChargeAmt			money,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTCHARGEAMT, 6, 8, szRptChargeAmount)
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTCHARGEAMT, 6, 8, RptArray(vcount).RptChargeAmt)
                params(5) = New Aurora.Common.Data.Parameter("RptChargeAmt", DbType.Currency, RptArray(vcount).RptChargeAmt, Parameter.ParameterType.AddInParameter)

                ' @RptLocalCurrAmt		money,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTLOCALCURRAMT, 6, 8, RptArray(vcount).RptLocalCurrAmt)
                params(6) = New Aurora.Common.Data.Parameter("RptLocalCurrAmt", DbType.Currency, RptArray(vcount).RptLocalCurrAmt, Parameter.ParameterType.AddInParameter)

                ' @RptIsRefund			bit	,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTISREFUND, 11, 1, RptArray(vcount).RptIsRefund)
                params(7) = New Aurora.Common.Data.Parameter("RptIsRefund", DbType.Boolean, CBool(RptArray(vcount).RptIsRefund), Parameter.ParameterType.AddInParameter)

                ' @RptIsRefunded			bit = 0	,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTISREFUNDED, 11, 1, CStr(varEmptyParameter))
                params(8) = New Aurora.Common.Data.Parameter("RptIsRefunded", DbType.Boolean, Not String.IsNullOrEmpty(varEmptyParameter), Parameter.ParameterType.AddInParameter)

                ' @RptPmtXfrToRntNum		varchar(64) = NULL,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTPMTXFRTORNTNUM, 200, 64, varEmptyParameter)
                params(9) = New Aurora.Common.Data.Parameter("RptPmtXfrToRntNum", DbType.String, varEmptyParameter, Parameter.ParameterType.AddInParameter)

                ' @RptPmtXfrFromRntNum	varchar(64) = NULL,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTPMTXFRFROMRNTNUM, 200, 64, varEmptyParameter)
                params(10) = New Aurora.Common.Data.Parameter("RptPmtXfrFromRntNum", DbType.String, varEmptyParameter, Parameter.ParameterType.AddInParameter)

                ' @IntegrityNo			int = 0,
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_RPTINTEGRITYNUMBER, 3, 4, 0)
                params(11) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, 0, Parameter.ParameterType.AddInParameter)

                ' @AddUsrId				varchar(64) = '',
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ADDUSERID, 200, 64, sUsrId)
                params(12) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)

                ' @ModUsrId				varchar(64) = '',
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_MODIFYUSERID, 200, 64, sUsrId)
                params(13) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)


                ' @AddPrgmName			varchar(64) = '',
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ADDPROGRAMNAME, 200, 64, "REV-PROCESS")
                params(14) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, "REV-PROCESS", Parameter.ParameterType.AddInParameter)


                ' @sOriginalPmtAmt		money		= NULL 
                ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ORIGINALPMTAMT, 6, 8, sOriginalPmtAmt)
                params(15) = New Aurora.Common.Data.Parameter("sOriginalPmtAmt", DbType.Currency, IIf(String.IsNullOrEmpty(sOriginalPmtAmt), DBNull.Value, sOriginalPmtAmt), Parameter.ParameterType.AddInParameter)

                ''varErrorString = oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMMANAGERENTALPAYMENT)
                varErrorString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_manageRentalPayment", params)

                If varErrorString <> "SUCCESS" Then
                    Exit Function
                Else
                    varErrorString = String.Empty
                End If

                vcount = vcount + 1

            Loop While (vcount < vlength)

            Return 1

        Catch ex As Exception
            varErrorString = ex.Message
            Return 0
        Finally
            params = Nothing
            tmpDom = Nothing
        End Try

    End Function

    Private Shared Function ConvertAmount(ByVal base_curr_code As String, _
                                   ByVal from_curr_code As String, _
                                   ByVal to_curr_code As String, _
                                   ByVal nAmount As Double, _
                                   ByRef varErrorString As Object) As Double

        Dim oExcDom As New System.Xml.XmlDocument
        Dim exchroot As System.Xml.XmlElement
        Dim exchRecord, varLocalErrorString, sErrorString As String

        Try
            If nAmount = 0 Then
                Exit Function
            End If

            If from_curr_code = to_curr_code Then
                Return nAmount
            End If

            ''base currency which associate with the local amount
            ''it must be the user local currency
            ''chargeCurrency = oAuroraDAL.getRecord("dbo.paym_GetCurrencyDesc '" & baseCurrency & "'")

            base_curr_code = Trim(base_curr_code)
            from_curr_code = Trim(from_curr_code)
            to_curr_code = Trim(to_curr_code)

            '@iAmount	float		= 0.00,
            '@base_Curr	varchar(12)	= '',
            '@from_Curr	varchar(12)	= '',
            '@to_curr	varchar(12) = ''

            ''exchRecord = oAuroraDAL.getRecord(AURORA_DBSTOREDPROC_ADMCURRENCYCONVERTER & " " & CStr(nAmount) & ", '" & base_curr_code & "', '" & from_curr_code & "', '" & to_curr_code & "'")
            exchRecord = Aurora.Common.Data.ExecuteScalarSP("ADM_CurrencyConverter", CStr(nAmount), base_curr_code, from_curr_code, to_curr_code)

            ' Check for valid xml
            Try

                oExcDom.LoadXml(exchRecord)
            Catch ex As System.Xml.XmlException
                varLocalErrorString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varLocalErrorString)
                varErrorString = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End Try

            exchroot = oExcDom.DocumentElement

            If exchroot.Name = "Error" Then
                varLocalErrorString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varLocalErrorString)
                varErrorString = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End If

            ConvertAmount = exchroot.SelectSingleNode("//ConRate").InnerText
            ConvertAmount = Math.Round(ConvertAmount, 2)


        Catch ex As Exception
            ConvertAmount = 0
            varErrorString = ex.Message

        Finally
            exchroot = Nothing
        End Try

    End Function

  

#End Region

#Region " Public Functions"

    Public Shared Function maintainPayment(ByVal xmlPayment As String, _
                                          ByVal sUsrId As String, _
                                          ByVal sProgname As String) As String
        Logging.LogDebug("maintainPayment", "entry xmlPayment : " & xmlPayment & " sUsrId : " & sUsrId & " sProgname : " & sProgname)

        Dim szRentalId As String
        Dim szBookingId As String
        Dim szBookingNumber As String
        Dim szPaymentDate As String
        Dim sOriginalPaymentAmt As String
        Dim szUserLocalCurrency As String
        Dim szUserLocalCurrencyName As String
        Dim szPaymentId As String
        Dim szPaymentCurrency As String
        Dim szPaymentCurrencyName As String
        Dim szUserLocCode As String
        Dim szPmtPmtId As String
        Dim szStatus As String
        Dim dAmountToPay As String
        Dim dLocalAmount As Double

        Dim PdtId As Object
        Dim PdtAccNum As Object
        Dim PdtAccName As Object
        Dim PdtApprovalRef As Object
        Dim PdtExpDate As Object
        Dim PdtBankNum As Object
        Dim PdtBranchNum As Object
        Dim PdtChequeNum As Object
        Dim PdtPmtId As Object
        Dim PdtAmount As Object
        Dim nIntegrity As Long
        Dim PdtIntegrity As Object

        ''Dim colp As colPayment
        Dim colp As BookingPaymentCollection

        ''Dim colaop As colAmntOuts
        Dim colaop As BookingPaymentOutCollection

        Dim varXmlString As String
        Dim saXmlString As String()
        Dim iCount As Long
        Dim sErrNo, sErrDesc, sErrorString, sError As String

        Dim oPaymentDom As New System.Xml.XmlDocument
        Dim root As System.Xml.XmlElement
        Dim tmpDom As System.Xml.XmlDocument
        Dim tmpRoot As System.Xml.XmlElement
        Dim processRetDom As System.Xml.XmlDocument
        Dim returnNode As System.Xml.XmlNode
        Dim returnXML As String
        Dim tmpreturnXML As String



        ''variables to walk through on the xml
        Dim tNodeList As System.Xml.XmlNodeList
        Dim tNode As System.Xml.XmlNode
        Dim pmEntryRoot As System.Xml.XmlNode

        ''Dim pmEntry As PaymentEntry
        Dim pmEntry As BookingPaymentEntry

        ''Dim pmItem As PmtItem
        Dim pmItem As BookingPaymentItem

        ''Dim amntItem As AmntOuts
        Dim amntItem As BookingPaymentAmountOuts

        Dim BallanceToAllocate As Double
        Dim BallanceCurrency As String
        Dim nPmEntryLength As Long

        ''Dim itemCol As colPmItems
        Dim itemCol As BookingPaymentItemsCollection

        Dim GUID As String
        Dim bNewPayment As Boolean
        Dim sMessage As String
        Dim i, j As Long
        Dim varEmptyParameter As Object
        Dim szPayMentMaintainXml As Object
        Dim nCurrentPaymentAmount As Double

        ''variable for the OutstandingAmount currency
        Dim t1CustToPayCurr As String
        Dim t1CustToPayCurrId As String
        Dim t1CustToPayAmt As Object
        Dim RptChargeAmt As Double
        Dim RptType As Object
        Dim RptRntId As Object
        Dim RptLocalCurrAmt As Object
        Dim RptId As Object
        Dim iExgRate As Double
        Dim sPmtPaymentOut, sDpsAuthCode, sDpsMerchRef, sDpsTxnRef As String


        Dim params(14) As Aurora.Common.Data.Parameter
        ''rev:mia march 8 2010 
        Dim sPdtDpsEfTxnRef As String = ""

        ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
        Dim sPdtEFTPOSCardMethodDontMatch As String = ""
        Dim sPdtEFTPOSCardTypeSwiped As String = ""
        Dim sPdtEFTPOSCardTypeSurchargeAdded As String = ""

        ''rev:mia march 12 2012 -- Added CVC2
        Dim paramNewPayment(21) As Aurora.Common.Data.Parameter
        Dim spNameManagePaymentDetail As String = "paym_managePaymentDetail"

        ' Check for valid xml
        Try
            oPaymentDom.LoadXml(xmlPayment)
        Catch ex As System.Xml.XmlException
            varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varXmlString)
            Logging.LogDebug("maintainPayment", "check : 1  " & sErrorString)
            Return "<Root>" & sErrorString & "<Data></Data></Root>"
        End Try

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction()
            Try
                root = oPaymentDom.DocumentElement
                szBookingId = root.SelectSingleNode("//Root/Payment/BookingId").InnerText

                'the payment must associate with booking, if it isn't there.... return
                If Len(szBookingId) = 0 Then
                    varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varXmlString)

                    ''reV:mia oct.23
                    ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                    Logging.LogDebug("maintainPayment", "check : 2  " & sErrorString)
                    Throw New Exception(sErrorString)
                End If

                szBookingNumber = root.SelectSingleNode("//Root/Payment/BookingNumber").InnerText

                ''get rental id if any
                szRentalId = root.SelectSingleNode(AURORA_XPATH_RENTALID).InnerText
                szUserLocCode = root.SelectSingleNode(AURORA_XPATH_USERLOCCODE).InnerText
                szUserLocalCurrency = root.SelectSingleNode(AURORA_XPATH_CURRENCY).InnerText
                szUserLocalCurrencyName = Aurora.Common.Data.ExecuteScalarSP("paym_GetCurrencyDesc", szUserLocalCurrency)
                szUserLocalCurrencyName = Trim(szUserLocalCurrencyName)

                ''initialize the return xml
                processRetDom = New System.Xml.XmlDocument
                returnXML = "<PaymentEntryRoot>"

                If root.ChildNodes(1).Name = "PaymentEntryRoot" Then
                    If root.ChildNodes(1).ChildNodes.Count > 0 Then
                        ''colp = New colPayment
                        colp = New BookingPaymentCollection
                        For i = 0 To root.ChildNodes(1).ChildNodes.Count - 1

                            pmEntry = New BookingPaymentEntry
                            With pmEntry
                                .pPaymentId = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(10).InnerText.Trim()
                                .pAmount = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(4).InnerText.Trim()
                                .pLocalAmount = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(5).InnerText.Trim()
                                .pCurrency = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(6).InnerText.Trim()
                                .pCurrencyName = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(7).InnerText.Trim()
                                ''.pIsRemove = IIf(root.childNodes(1).childNodes(i).childNodes.item(8).InnerText = "", string.empty, root.childNodes(1).childNodes(i).childNodes.item(8).InnerText)
                                .pMethod = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(0).InnerText.Trim()
                                .pSubMethod = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(1).InnerText.Trim()
                                .pIntegrity = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(9).InnerText.Trim()
                                .pStatus = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(12).InnerText.Trim()
                                .pPaymentDate = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(11).InnerText.Trim()

                                If (Not root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(15) Is Nothing) Then
                                    .pOriginalPaymentAmt = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(15).InnerText.Trim()
                                Else
                                    Logging.LogError("MaintainPayment", "OriginalPayment not exist in xml")
                                End If
                            End With

                            pmEntryRoot = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(13)
                            nPmEntryLength = pmEntryRoot.ChildNodes.Count
                            If nPmEntryLength > 0 Then
                                ''create collection for the payment details
                                ''itemCol = New colPmItems
                                ''BookingPaymentItemsCollection
                                itemCol = New BookingPaymentItemsCollection
                                For j = 0 To nPmEntryLength - 1

                                    pmItem = New BookingPaymentItem
                                    With pmItem
                                        .pAccountName = pmEntryRoot.ChildNodes(j).ChildNodes.Item(0).InnerText.Trim()
                                        .pPmtPmId = pmEntryRoot.ChildNodes(j).ChildNodes.Item(1).InnerText.Trim()
                                        .pApproval = pmEntryRoot.ChildNodes(j).ChildNodes.Item(2).InnerText.Trim()
                                        .pExpDate = pmEntryRoot.ChildNodes(j).ChildNodes.Item(3).InnerText.Trim()
                                        .pChequeNo = pmEntryRoot.ChildNodes(j).ChildNodes.Item(4).InnerText.Trim()
                                        .pBank = pmEntryRoot.ChildNodes(j).ChildNodes.Item(5).InnerText.Trim()
                                        .pBranch = pmEntryRoot.ChildNodes(j).ChildNodes.Item(6).InnerText.Trim()
                                        .pAccountNo = pmEntryRoot.ChildNodes(j).ChildNodes.Item(7).InnerText.Trim()

                                        .pAmount = pmEntryRoot.ChildNodes(j).ChildNodes.Item(8).InnerText.Trim()
                                        .pIntegrity = pmEntryRoot.ChildNodes(j).ChildNodes.Item(9).InnerText.Trim()
                                        .pPdtId = pmEntryRoot.ChildNodes(j).ChildNodes.Item(11).InnerText.Trim()
                                        .pAuthCode = pmEntryRoot.ChildNodes(j).SelectSingleNode("AuthCode").InnerText.Trim()
                                        .pMerchRef = pmEntryRoot.ChildNodes(j).SelectSingleNode("MerchRef").InnerText.Trim()
                                        .pTxnRef = pmEntryRoot.ChildNodes(j).SelectSingleNode("TxnRef").InnerText.Trim()

                                        ''rev:mia march 8 2010 
                                        If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtDpsEfTxnRef") Is Nothing Then
                                            .pPdtDpsEfTxnRef = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtDpsEfTxnRef").InnerText.Trim()
                                        Else
                                            .pPdtDpsEfTxnRef = String.Empty
                                        End If

                                        ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                                        If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardMethodDontMatch") Is Nothing Then
                                            .PdtEFTPOSCardMethodDontMatch = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardMethodDontMatch").InnerText.Trim()
                                        Else
                                            .PdtEFTPOSCardMethodDontMatch = String.Empty
                                        End If
                                        If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardTypeSwiped") Is Nothing Then
                                            .PdtEFTPOSCardTypeSwiped = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardTypeSwiped").InnerText.Trim()
                                        Else
                                            .PdtEFTPOSCardTypeSwiped = String.Empty
                                        End If
                                        If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardTypeSurchargeAdded") Is Nothing Then
                                            .PdtEFTPOSCardTypeSurchargeAdded = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardTypeSurchargeAdded").InnerText.Trim()
                                        Else
                                            .PdtEFTPOSCardTypeSurchargeAdded = String.Empty
                                        End If

                                    End With
                                    itemCol.Add(pmItem)
                                    pmItem = Nothing
                                Next
                            End If

                            pmEntry.pCollectionItems = itemCol

                            colp.Add(pmEntry)
                            pmEntry = Nothing
                        Next
                    End If
                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ''create the amountsoutstanding collection

                tNode = Nothing
                tNodeList = Nothing

                ''rev:mia oct.23
                If root.ChildNodes(2) IsNot Nothing Then
                    tNodeList = root.ChildNodes(2).ChildNodes
                    If tNodeList.Count > 0 Then
                        '' colaop = New colAmntOuts
                        colaop = New BookingPaymentOutCollection
                        For Each tNode In tNodeList
                            ''amntItem = New  AmntOuts
                            amntItem = New BookingPaymentAmountOuts
                            With amntItem
                                .CustToPay = CDbl(tNode.ChildNodes(4).InnerText)
                                .RptChargeCurrId = tNode.ChildNodes(2).InnerText
                                .RptIsRefund = False
                                .RptRntId = tNode.ChildNodes(0).InnerText
                                .RptType = tNode.ChildNodes(1).InnerText
                                .AmountAllocated = 0
                            End With
                            colaop.Add(amntItem)
                            amntItem = Nothing
                        Next
                    End If
                End If
                tNode = Nothing
                tNodeList = Nothing

                ''NEW: set the ballance to allocate to 0
                BallanceToAllocate = 0

                For Each pmEntry In colp

                    nIntegrity = 0
                    dAmountToPay = 0
                    dLocalAmount = 0
                    szPaymentCurrency = String.Empty
                    szPmtPmtId = String.Empty
                    PdtAccNum = String.Empty
                    PdtAccName = String.Empty
                    PdtApprovalRef = String.Empty
                    PdtExpDate = String.Empty
                    PdtAmount = String.Empty
                    szStatus = String.Empty
                    szPaymentDate = String.Empty
                    sOriginalPaymentAmt = String.Empty

                    If Len(pmEntry.pPaymentId) = 0 Then
                        ''insert the payment table
                        sMessage = "I"
                        ''sp_get_newId
                        GUID = Aurora.Common.Data.ExecuteScalarSP("sp_get_newId")
                        GUID = Replace(GUID, "<Id>", String.Empty)
                        GUID = Replace(GUID, "</Id>", String.Empty)
                        GUID = Trim(GUID)
                        szPaymentId = GUID
                        bNewPayment = True
                        nIntegrity = 0
                    Else
                        ''update the payment table
                        szPaymentId = pmEntry.pPaymentId
                        nIntegrity = pmEntry.pIntegrity
                    End If

                    With pmEntry
                        dAmountToPay = CDbl(.pAmount)
                        dLocalAmount = CDbl(.pLocalAmount)
                        szPaymentCurrency = .pCurrency
                        szPaymentCurrencyName = .pCurrencyName
                        szPmtPmtId = .pSubMethod
                        szStatus = .pStatus
                        szPaymentDate = .pPaymentDate
                        sOriginalPaymentAmt = .pOriginalPaymentAmt

                    End With


                    If Not bNewPayment Then
                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        ''  existing payment
                        Dim currentPaymentStr As String
                        Dim tmpXml As String

                        ''paym_getCurrentAmount
                        ''tmpXml = oAuroraDAL.GetRecord(AURORA_DBSTOREDPROC_PAYMGETCURRENTAMOUNT & " '" & szPaymentId & "'")
                        tmpXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getCurrentAmount", szPaymentId).OuterXml

                        If Len(tmpXml) > 0 Then
                            tmpDom = New System.Xml.XmlDocument
                            Try
                                tmpDom.LoadXml(tmpXml)
                            Catch ex As System.Xml.XmlException
                                varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                                ' Error Section
                                saXmlString = Split(varXmlString, "/")
                                If UBound(saXmlString) > 1 Then
                                    sErrNo = saXmlString(0)
                                    For iCount = 1 To UBound(saXmlString)
                                        If iCount = 1 Then
                                            sErrDesc = saXmlString(iCount)
                                        Else
                                            sErrDesc = CStr(sErrDesc) & "/" & saXmlString(iCount)
                                        End If
                                    Next
                                Else
                                    sErrNo = saXmlString(0)
                                    sErrDesc = saXmlString(1)
                                End If

                                If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)

                                    ''reV:mia oct.23
                                    ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                    Logging.LogDebug("maintainPayment", "check : 3  " & sErrorString)
                                    Throw New Exception(sErrorString)
                                End If

                                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                ''Return "<Root>" & sErrorString & "<Data>" & CStr(szPayMentMaintainXml) & "</Data></Root>"

                                ''reV:mia oct.23
                                ''Return "<Root>" & sErrorString & "<Data>" & CStr(szPayMentMaintainXml) & "</Data></Root>"
                                Logging.LogDebug("maintainPayment", "check : 4  " & szPayMentMaintainXml)
                                Throw New Exception(sErrorString & "<Data>" & CStr(szPayMentMaintainXml) & "</Data>")

                            End Try

                            tmpRoot = tmpDom.DocumentElement
                            currentPaymentStr = tmpRoot.SelectSingleNode("//Payment/CurrentAmount").InnerText
                            nCurrentPaymentAmount = CDbl(currentPaymentStr)

                        Else
                            ''error
                            tmpDom = New System.Xml.XmlDocument
                            Try
                                tmpDom.LoadXml(tmpXml)
                            Catch ex As System.Xml.XmlException
                                varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "GEN023", "Application", varXmlString)

                                ''reV:mia oct.23
                                ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                Logging.LogDebug("maintainPayment", "check : 5  " & sErrorString)
                                Throw New Exception(sErrorString)
                            End Try

                            ' Error Section
                            saXmlString = Split(CStr(varXmlString), "/")
                            If UBound(saXmlString) > 1 Then
                                sErrNo = saXmlString(0)
                                For iCount = 1 To UBound(saXmlString)
                                    If iCount = 1 Then
                                        sErrDesc = saXmlString(iCount)
                                    Else
                                        sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                    End If
                                Next
                            Else
                                sErrNo = saXmlString(0)
                                sErrDesc = saXmlString(1)
                            End If

                            If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                ''reV:mia oct.23
                                ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                Logging.LogDebug("maintainPayment", "check : 6  " & sErrorString)
                                Throw New Exception(sErrorString)
                            End If

                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                            ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                            ''reV:mia oct.23
                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                            Logging.LogDebug("maintainPayment", "check : 7  " & sErrorString)
                            Throw New Exception(sErrorString)
                        End If
                    Else
                        nCurrentPaymentAmount = 0
                    End If

                    '--- Issue 159 - Payments out processing.
                    If dLocalAmount > 0 Then sPmtPaymentOut = "F" Else sPmtPaymentOut = "T"

                    ''allocate the local amount at the rentalpayment table
                    ''REM NEW
                    ''dAmountToAllocate = dLocalAmount

                    ''first update payment
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ' params from db

                    ' @sPaymentId				varchar(64),
                    ' @sPmtPtmId				varchar(64),
                    ' @sBookingId				varchar(64),

                    ' @sPmtCurrId				varchar(64),
                    ' @PmtDateTime			datetime 	= NULL,
                    ' @dPmtAmt				money,

                    ' @dLocCurrAmt			money,
                    ' @sPmtStatus				varchar(64) = NULL,
                    ' @sPmtBranch				varchar(64) = NULL,


                    ' @sPmtReversalofPmtId	varchar(64)	= NULL ,		--- Issue 159 - Payments out processing.
                    ' @sPmtPaymentOut			varchar(12) = NULL,			--- Issue 159 - Payments out processing.
                    ' @iIntegrityNo			int  		= 0,


                    ' @AddUsrId				varchar (64)= '',
                    ' @ModUsrId				varchar (64)= '',
                    ' @AddPrgmName			varchar(64) = '' 



                    params(0) = New Aurora.Common.Data.Parameter("sPaymentId", DbType.String, szPaymentId, Parameter.ParameterType.AddInParameter)
                    params(1) = New Aurora.Common.Data.Parameter("sPmtPtmId", DbType.String, szPmtPmtId, Parameter.ParameterType.AddInParameter)
                    params(2) = New Aurora.Common.Data.Parameter("sBookingId", DbType.String, szBookingId, Parameter.ParameterType.AddInParameter)

                    params(3) = New Aurora.Common.Data.Parameter("sPmtCurrId", DbType.String, szPaymentCurrency, Parameter.ParameterType.AddInParameter)
                    params(4) = New Aurora.Common.Data.Parameter("PmtDateTime", DbType.DateTime, IIf(szPaymentDate = "", DBNull.Value, szPaymentDate), Parameter.ParameterType.AddInParameter)

                    params(5) = New Aurora.Common.Data.Parameter("dPmtAmt", DbType.Currency, dLocalAmount, Parameter.ParameterType.AddInParameter)

                    params(6) = New Aurora.Common.Data.Parameter("dLocCurrAmt", DbType.Currency, dAmountToPay, Parameter.ParameterType.AddInParameter)
                    params(7) = New Aurora.Common.Data.Parameter("sPmtStatus", DbType.String, IIf(szStatus = "", DBNull.Value, szStatus), Parameter.ParameterType.AddInParameter)
                    params(8) = New Aurora.Common.Data.Parameter("sPmtBranch", DbType.String, szUserLocCode, Parameter.ParameterType.AddInParameter)

                    params(9) = New Aurora.Common.Data.Parameter("sPmtReversalofPmtId", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)
                    params(10) = New Aurora.Common.Data.Parameter("sPmtPaymentOut", DbType.String, sPmtPaymentOut, Parameter.ParameterType.AddInParameter)
                    params(11) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int16, nIntegrity, Parameter.ParameterType.AddInParameter)

                    params(12) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                    params(13) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                    params(14) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)

                    varXmlString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_managePayment", params)
                    If varXmlString <> "SUCCESS" Then
                        ' Error Section
                        saXmlString = Split(varXmlString, "/")
                        If UBound(saXmlString) > 1 Then
                            sErrNo = saXmlString(0)
                            For iCount = 1 To UBound(saXmlString)
                                If iCount = 1 Then
                                    sErrDesc = saXmlString(iCount)
                                Else
                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                End If
                            Next
                        Else
                            sErrNo = saXmlString(0)
                            sErrDesc = saXmlString(1)
                        End If

                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                            ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                            ''reV:mia oct.23
                            ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                            Logging.LogDebug("maintainPayment", "check : 8  " & sErrorString)
                            Throw New Exception(sErrorString)
                        End If

                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"

                        ''reV:mia oct.23
                        ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                        Logging.LogDebug("maintainPayment", "check : 9  " & sErrorString)
                        Throw New Exception(sErrorString)
                    Else
                        varXmlString = String.Empty
                    End If

                    ''update payment details
                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                    PdtPmtId = szPaymentId

                    ''get payment details from the collection
                    For Each pmItem In pmEntry.pCollectionItems

                        BallanceToAllocate = 0
                        BallanceCurrency = String.Empty

                        PdtAccNum = String.Empty
                        PdtAccName = String.Empty
                        PdtBankNum = String.Empty
                        PdtBranchNum = String.Empty
                        PdtChequeNum = String.Empty
                        PdtApprovalRef = String.Empty
                        PdtExpDate = String.Empty
                        PdtIntegrity = String.Empty
                        PdtAmount = String.Empty

                        PdtAccNum = pmItem.pAccountNo
                        PdtAccName = pmItem.pAccountName
                        PdtBankNum = pmItem.pBank
                        PdtBranchNum = pmItem.pBranch
                        PdtChequeNum = pmItem.pChequeNo
                        PdtApprovalRef = pmItem.pApproval
                        PdtExpDate = pmItem.pExpDate
                        PdtIntegrity = pmItem.pIntegrity
                        PdtAmount = pmItem.pAmount
                        sDpsAuthCode = pmItem.pAuthCode
                        sDpsMerchRef = pmItem.pMerchRef
                        sDpsTxnRef = pmItem.pTxnRef
                        sPdtDpsEfTxnRef = pmItem.pPdtDpsEfTxnRef

                        ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                        sPdtEFTPOSCardMethodDontMatch = pmItem.PdtEFTPOSCardMethodDontMatch
                        sPdtEFTPOSCardTypeSwiped = pmItem.PdtEFTPOSCardTypeSwiped
                        sPdtEFTPOSCardTypeSurchargeAdded = pmItem.PdtEFTPOSCardTypeSurchargeAdded

                        If bNewPayment Then
                            ''insert
                            GUID = Aurora.Common.Data.ExecuteScalarSP("sp_get_newId")
                            GUID = Replace(GUID, "<Id>", String.Empty)
                            GUID = Replace(GUID, "</Id>", String.Empty)
                            GUID = Trim(GUID)
                            PdtId = GUID
                        Else
                            PdtId = pmItem.pPdtId
                        End If


                        paramNewPayment(0) = New Aurora.Common.Data.Parameter("sPdtId", DbType.String, PdtId, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(1) = New Aurora.Common.Data.Parameter("sPdtPmtId", DbType.String, PdtPmtId, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(2) = New Aurora.Common.Data.Parameter("sPdtBankNum", DbType.String, PdtBankNum, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(3) = New Aurora.Common.Data.Parameter("sPdtBranchNum", DbType.String, PdtBranchNum, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(4) = New Aurora.Common.Data.Parameter("sPdtChequeNum", DbType.String, PdtChequeNum, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(5) = New Aurora.Common.Data.Parameter("sPdtAccnum", DbType.String, PdtAccNum, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(6) = New Aurora.Common.Data.Parameter("sPdtAmount", DbType.String, PdtAmount, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(7) = New Aurora.Common.Data.Parameter("sPdtAccName", DbType.String, PdtAccName, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(8) = New Aurora.Common.Data.Parameter("sPdtApproval", DbType.String, PdtApprovalRef, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(9) = New Aurora.Common.Data.Parameter("sPdtExpiry", DbType.String, PdtExpDate, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(10) = New Aurora.Common.Data.Parameter("sDpsAuthCode", DbType.String, sDpsAuthCode, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(11) = New Aurora.Common.Data.Parameter("sDpsMerchRef", DbType.String, sDpsMerchRef, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(12) = New Aurora.Common.Data.Parameter("sDpsTxnRef", DbType.String, sDpsTxnRef, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(13) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, PdtIntegrity, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(14) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(15) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(16) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)
                        ''rev:mia march 8,2010
                        paramNewPayment(17) = New Aurora.Common.Data.Parameter("PdtDpsEfTxnRef", DbType.String, sPdtDpsEfTxnRef, Parameter.ParameterType.AddInParameter)

                        ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                        paramNewPayment(18) = New Aurora.Common.Data.Parameter("PdtEFTPOSCardMethodDontMatch", DbType.String, sPdtEFTPOSCardMethodDontMatch, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(19) = New Aurora.Common.Data.Parameter("PdtEFTPOSCardTypeSwiped", DbType.String, sPdtEFTPOSCardTypeSwiped, Parameter.ParameterType.AddInParameter)
                        paramNewPayment(20) = New Aurora.Common.Data.Parameter("PdtEFTPOSCardTypeSurchargeAdded", DbType.String, sPdtEFTPOSCardTypeSurchargeAdded, Parameter.ParameterType.AddInParameter)

                        ''rev:mia march 12 2012 -- Added CVC2"
                        Try
                            Dim strcvv2 As String = root.SelectSingleNode("//Root/cvv2").InnerText
                            paramNewPayment(21) = New Aurora.Common.Data.Parameter("PdtCVV2", DbType.String, strcvv2, Parameter.ParameterType.AddInParameter)
                        Catch ex As Exception
                            paramNewPayment(21) = New Aurora.Common.Data.Parameter("PdtCVV2", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)
                        End Try

                        varXmlString = Aurora.Common.Data.ExecuteOutputObjectSP(spNameManagePaymentDetail, paramNewPayment)


                        If varXmlString <> "SUCCESS" Then
                            ' Error Section
                            saXmlString = Split(varXmlString, "/")
                            If UBound(saXmlString) > 1 Then
                                sErrNo = saXmlString(0)
                                For iCount = 1 To UBound(saXmlString)
                                    If iCount = 1 Then
                                        sErrDesc = saXmlString(iCount)
                                    Else
                                        sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                    End If
                                Next
                            Else
                                sErrNo = saXmlString(0)
                                sErrDesc = saXmlString(1)
                            End If

                            If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                ''reV:mia oct.23
                                ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"

                                Logging.LogDebug("maintainPayment", "check : 10  " & sErrorString)
                                Throw New Exception(sErrorString)
                            End If

                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                            ''szPayMentMaintainXml = oAuroraDAL.getRecord(sQueryString)
                            ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                            ''reV:mia oct.23
                            ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                            Logging.LogDebug("maintainPayment", "check : 11  " & sErrorString)
                            Throw New Exception(sErrorString)

                        Else
                            varXmlString = String.Empty
                        End If

                    Next


                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ''create rental payment
                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ''  new payment
                    'oErrlog.WriteErrorToFile sError, string.empty, Now, bNewPayment
                    If bNewPayment Then
                        ''with new payment crate new reantal payment records
                        ''enumerate the collection of outstanding payments

                        '''''NEW
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        ''enumerates the amountOutstanding collection and
                        ''allocate the payments against the amount which equal =
                        ''documentation t1.OutstandingAmounts

                        ''reV:mia oct.23
                        If colaop IsNot Nothing Then
                            If colaop.Count = 0 Then
                                ''overpayment as no outstanding record available
                                BallanceToAllocate = dAmountToPay
                                BallanceCurrency = szUserLocalCurrency
                            End If
                        End If

                            For Each amntItem In colaop

                                t1CustToPayCurrId = amntItem.RptChargeCurrId

                                ''paym_GetCurrencyDesc
                                t1CustToPayCurr = Aurora.Common.Data.ExecuteScalarSP("paym_GetCurrencyDesc", amntItem.RptChargeCurrId)
                                t1CustToPayCurr = Trim(t1CustToPayCurr)

                                If BallanceToAllocate = 0 Then

                                    ''there is outstanding record available
                                    ''pass szUserLocalCurrencyName
                                    If dAmountToPay > 0 Then
                                        If (t1CustToPayCurr <> szUserLocalCurrencyName) And (szPaymentCurrencyName <> szUserLocalCurrencyName) Then
                                            ''
                                            Dim dtmpAmount As Double

                                            dtmpAmount = getExchangeAmount(dAmountToPay, _
                                                                            szUserLocalCurrencyName, _
                                                                            szPaymentCurrencyName, _
                                                                            t1CustToPayCurr, _
                                                                            varXmlString)

                                            If Not varXmlString.Equals(String.Empty) Then
                                                If Len(varXmlString) > 0 Then
                                                    ' Error Section
                                                    saXmlString = Split(varXmlString, "/")
                                                    If UBound(saXmlString) > 1 Then
                                                        sErrNo = saXmlString(0)
                                                        For iCount = 1 To UBound(saXmlString)
                                                            If iCount = 1 Then
                                                                sErrDesc = saXmlString(iCount)
                                                            Else
                                                                sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                                            End If
                                                        Next
                                                    Else
                                                        sErrNo = saXmlString(0)
                                                        sErrDesc = saXmlString(1)
                                                    End If

                                                    If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                                        ''reV:mia oct.23
                                                    ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                                                    Logging.LogDebug("maintainPayment", "check : 12  " & sErrorString)
                                                        Throw New Exception(sErrorString)
                                                Else
                                                    ''todo
                                                    Dim returnInvalidcurrencyMessage As String = InvalidcurrencyMessage(varXmlString)
                                                    If Not String.IsNullOrEmpty(returnInvalidcurrencyMessage) Then
                                                        Logging.LogDebug("maintainPayment", "check : 13  " & returnInvalidcurrencyMessage)
                                                        Throw New Exception(returnInvalidcurrencyMessage)
                                                    End If
                                                End If

                                                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                                    ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                                    ''reV:mia oct.23
                                                ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                                                Logging.LogDebug("maintainPayment", "check : 14  " & sErrorString)
                                                    Throw New Exception(sErrorString)

                                                End If
                                            End If


                                            Dim strParam1 As String
                                            Dim strParam2 As String
                                            Dim strParam3 As String

                                            strParam1 = szUserLocalCurrencyName
                                            strParam2 = szUserLocalCurrencyName
                                            strParam3 = t1CustToPayCurr

                                            If (strParam1 <> strParam2) And (strParam1 <> strParam3) Then
                                                dtmpAmount = getExchangeAmount(dtmpAmount, _
                                                                                strParam1, _
                                                                                strParam2, _
                                                                                strParam3, _
                                                                                varXmlString)

                                                If Not varXmlString.Equals(String.Empty) Then
                                                    If Len(varXmlString) > 0 Then
                                                        ' Error Section
                                                        saXmlString = Split(varXmlString, "/")
                                                        If UBound(saXmlString) > 1 Then
                                                            sErrNo = saXmlString(0)
                                                            For iCount = 1 To UBound(saXmlString)
                                                                If iCount = 1 Then
                                                                    sErrDesc = saXmlString(iCount)
                                                                Else
                                                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                                                End If
                                                            Next
                                                        Else
                                                            sErrNo = saXmlString(0)
                                                            sErrDesc = saXmlString(1)
                                                        End If

                                                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"

                                                        ''reV:mia oct.23
                                                        Logging.LogDebug("maintainPayment", "check : 15  " & sErrorString)
                                                            Throw New Exception(sErrorString)

                                                    Else
                                                        ''todo
                                                        Dim returnInvalidcurrencyMessage As String = InvalidcurrencyMessage(varXmlString)
                                                        If Not String.IsNullOrEmpty(returnInvalidcurrencyMessage) Then
                                                            Logging.LogDebug("maintainPayment", "check : 16  " & returnInvalidcurrencyMessage)

                                                            Throw New Exception(returnInvalidcurrencyMessage)
                                                        End If
                                                    End If

                                                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                                    ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                                    ''reV:mia oct.23
                                                    Logging.LogDebug("maintainPayment", "check : 17  " & sErrorString)
                                                    Throw New Exception(sErrorString)
                                                End If
                                                End If

                                            End If

                                            strParam1 = String.Empty
                                            strParam2 = String.Empty
                                            strParam3 = String.Empty

                                            BallanceToAllocate = dtmpAmount

                                        Else
                                            BallanceToAllocate = dAmountToPay
                                        End If

                                        BallanceCurrency = t1CustToPayCurr
                                    Else

                                        BallanceToAllocate = dAmountToPay
                                        BallanceCurrency = szPaymentCurrencyName

                                    End If
                                Else    '' BallanceToAllocate <> 0
                                    If BallanceCurrency <> t1CustToPayCurr And BallanceToAllocate > 0 Then

                                        BallanceToAllocate = getExchangeAmount(BallanceToAllocate, _
                                                                                szUserLocalCurrencyName, _
                                                                                BallanceCurrency, _
                                                                                t1CustToPayCurr, _
                                                                                varXmlString)
                                        If Not varXmlString.Equals(String.Empty) Then
                                            If Len(varXmlString) > 0 Then
                                                ' Error Section
                                                saXmlString = Split(varXmlString, "/")
                                                If UBound(saXmlString) > 1 Then
                                                    sErrNo = saXmlString(0)
                                                    For iCount = 1 To UBound(saXmlString)
                                                        If iCount = 1 Then
                                                            sErrDesc = saXmlString(iCount)
                                                        Else
                                                            sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                                        End If
                                                    Next
                                                Else
                                                    sErrNo = saXmlString(0)
                                                    sErrDesc = saXmlString(1)
                                                End If

                                                If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                                    ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                                ''reV:mia oct.23
                                                Logging.LogDebug("maintainPayment", "check : 18  " & sErrorString)
                                                Throw New Exception(sErrorString)
                                            Else

                                                ''todo
                                                Dim returnInvalidcurrencyMessage As String = InvalidcurrencyMessage(varXmlString)
                                                If Not String.IsNullOrEmpty(returnInvalidcurrencyMessage) Then
                                                    Logging.LogDebug("maintainPayment", "check : 19  " & returnInvalidcurrencyMessage)
                                                    Throw New Exception(returnInvalidcurrencyMessage)
                                                End If
                                            End If

                                                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                                ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                            ''reV:mia oct.23
                                            Logging.LogDebug("maintainPayment", "check : 20  " & sErrorString)
                                                Throw New Exception(sErrorString)
                                            End If
                                        End If

                                        If szUserLocalCurrencyName <> t1CustToPayCurr Then
                                            BallanceToAllocate = Math.Round((BallanceToAllocate / iExgRate), 2)
                                        Else
                                            BallanceToAllocate = Math.Round((BallanceToAllocate * iExgRate), 2)
                                        End If

                                        BallanceCurrency = t1CustToPayCurr

                                    End If

                                End If

                                Dim dAmountBallance As Double
                                dAmountBallance = amntItem.CustToPay - amntItem.AmountAllocated

                                ''  10/09/2002 modifyed this condition
                                ''  If BallanceToAllocate <= dAmountBallance And dAmountBallance > 0 Then
                                ''  15/10/2002 modifyed this condition
                                ''  If BallanceToAllocate <= dAmountBallance And (dAmountBallance > 0 Or amntItem.CustToPay < 0) Then

                                If (((BallanceToAllocate <= dAmountBallance)) Or ((BallanceToAllocate >= dAmountBallance))) Then
                                    RptChargeAmt = BallanceToAllocate

                                    RptType = amntItem.RptType
                                    RptRntId = amntItem.RptRntId

                                    RptType = "O"
                                    RptRntId = amntItem.RptRntId

                                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    ''do manageRentalPayment
                                    If manageRentalPayment(True, _
                                                            RptId, _
                                                            amntItem.RptRntId, _
                                                            szPaymentId, _
                                                            amntItem.RptType, _
                                                            t1CustToPayCurrId, _
                                                            t1CustToPayCurr, _
                                                            RptChargeAmt, _
                                                            szPaymentCurrency, _
                                                            szPaymentCurrencyName, _
                                                            RptLocalCurrAmt, _
                                                            varEmptyParameter, _
                                                            varEmptyParameter, _
                                                            varEmptyParameter, _
                                                            nIntegrity, _
                                                            sUsrId, _
                                                            sUsrId, _
                                                            sProgname, _
                                                            szUserLocalCurrencyName, _
                                                            amntItem.CustToPay, _
                                                            varXmlString, _
                                                            sOriginalPaymentAmt) = False Then
                                        ' Error Section
                                        saXmlString = Split(varXmlString, "/")
                                        If UBound(saXmlString) > 1 Then
                                            sErrNo = saXmlString(0)
                                            For iCount = 1 To UBound(saXmlString)
                                                If iCount = 1 Then
                                                    sErrDesc = saXmlString(iCount)
                                                Else
                                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                                End If
                                            Next
                                        Else
                                            sErrNo = saXmlString(0)
                                            sErrDesc = saXmlString(1)
                                        End If

                                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then

                                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                            ''maintainPayment = "<Root>" & sErrorString & "<Data></Data></Root>"
                                        ''reV:mia oct.23
                                        Logging.LogDebug("maintainPayment", "check : 21  " & sErrorString)
                                            Throw New Exception(sErrorString)
                                            ''Exit Function
                                        End If

                                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                    ''reV:mia oct.23
                                    Logging.LogDebug("maintainPayment", "check : 22  " & sErrorString)
                                        Throw New Exception(sErrorString)

                                        ' Error Section
                                    End If

                                    amntItem.AmountAllocated = amntItem.AmountAllocated + RptChargeAmt
                                    BallanceToAllocate = 0

                                ElseIf (BallanceToAllocate > dAmountBallance And dAmountBallance > 0) Or (BallanceToAllocate < dAmountBallance And dAmountBallance < 0) Then

                                    RptChargeAmt = amntItem.CustToPay - amntItem.AmountAllocated
                                    RptType = amntItem.RptType
                                    RptRntId = amntItem.RptRntId

                                    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                    ''do manageRentalPayment
                                    If manageRentalPayment(True, _
                                                              RptId, _
                                                              amntItem.RptRntId, _
                                                              szPaymentId, _
                                                              amntItem.RptType, _
                                                              t1CustToPayCurrId, _
                                                              t1CustToPayCurr, _
                                                              RptChargeAmt, _
                                                              szPaymentCurrency, _
                                                              szPaymentCurrencyName, _
                                                              RptLocalCurrAmt, _
                                                              varEmptyParameter, _
                                                              varEmptyParameter, _
                                                              varEmptyParameter, _
                                                              nIntegrity, _
                                                              sUsrId, _
                                                              sUsrId, _
                                                              sProgname, _
                                                              szUserLocalCurrencyName, _
                                                              amntItem.CustToPay, _
                                                              varXmlString, _
                                                              sOriginalPaymentAmt) = False Then
                                        ' Error Section
                                        saXmlString = Split(varXmlString, "/")
                                        If UBound(saXmlString) > 1 Then
                                            sErrNo = saXmlString(0)
                                            For iCount = 1 To UBound(saXmlString)
                                                If iCount = 1 Then
                                                    sErrDesc = saXmlString(iCount)
                                                Else
                                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                                End If
                                            Next
                                        Else
                                            sErrNo = saXmlString(0)
                                            sErrDesc = saXmlString(1)
                                        End If

                                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                        ''reV:mia oct.23
                                        Logging.LogDebug("maintainPayment", "check : 23  " & sErrorString)
                                            Throw New Exception(sErrorString)
                                        End If


                                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                    ''reV:mia oct.23
                                    Logging.LogDebug("maintainPayment", "check : 24  " & sErrorString)
                                        Throw New Exception(sErrorString)
                                    End If

                                    amntItem.AmountAllocated = amntItem.AmountAllocated + RptChargeAmt
                                    BallanceToAllocate = BallanceToAllocate - RptChargeAmt

                                End If

                                If BallanceToAllocate = 0 Then
                                    Exit For
                                End If

                                t1CustToPayAmt = amntItem.CustToPay
                                If RptRntId.Equals(String.Empty) Then
                                    RptRntId = amntItem.RptRntId
                                End If
                            Next

                        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        ''if it still bigger than the AmountOutstanding payment,
                        ''then create Payment record for that
                        ''create overpayment over ONLY $1 to avoid small amounts which are
                        ''coming from the exchange
                        'oErrlog.WriteErrorToFile sError, "", Now, "manageRentalPayment 3 -->>" & BallanceToAllocate
                        If BallanceToAllocate >= 1 Or BallanceToAllocate <= -1 Then

                            If manageRentalPayment(True, _
                                                    RptId, _
                                                    RptRntId, _
                                                    szPaymentId, _
                                                    "O", _
                                                    t1CustToPayCurrId, _
                                                    t1CustToPayCurr, _
                                                    BallanceToAllocate, _
                                                    szPaymentCurrency, _
                                                    szPaymentCurrencyName, _
                                                    RptLocalCurrAmt, _
                                                    varEmptyParameter, _
                                                    varEmptyParameter, _
                                                    varEmptyParameter, _
                                                    nIntegrity, _
                                                    sUsrId, _
                                                    sUsrId, _
                                                    sProgname, _
                                                    szUserLocalCurrencyName, _
                                                    t1CustToPayAmt, _
                                                    varXmlString, _
                                                    sOriginalPaymentAmt) = False Then
                                ' Error Section
                                saXmlString = Split(varXmlString, "/")
                                If UBound(saXmlString) > 1 Then
                                    sErrNo = saXmlString(0)
                                    For iCount = 1 To UBound(saXmlString)
                                        If iCount = 1 Then
                                            sErrDesc = saXmlString(iCount)
                                        Else
                                            sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                        End If
                                    Next
                                Else
                                    sErrNo = saXmlString(0)
                                    sErrDesc = saXmlString(1)
                                End If

                                If sErrNo = "GEN001" Or sErrNo = "GEN008" Then

                                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                    ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                    ''reV:mia oct.23
                                    Logging.LogDebug("maintainPayment", "check : 25  " & sErrorString)
                                    Throw New Exception(sErrorString)
                                End If

                                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                ''reV:mia oct.23
                                Logging.LogDebug("maintainPayment", "check : 26  " & sErrorString)
                                Throw New Exception(sErrorString)
                            End If
                        End If

                    Else  ''existing payment

                        BallanceToAllocate = dAmountToPay

                        If BallanceToAllocate <> nCurrentPaymentAmount Then
                            ''the amount is not equal, must modify the RentalPayment table

                            If BallanceToAllocate < nCurrentPaymentAmount Then
                                ''delete/modify rentalpayment records
                                Call AdjustRentalPayment(szPaymentId, BallanceToAllocate, szUserLocalCurrency, szUserLocalCurrencyName, varXmlString)
                                If Not varXmlString.Equals(String.Empty) Then
                                    If (Len(varXmlString) > 0) And (varXmlString <> RPTADJUST_SUCCESS) Then
                                        ' Error Section
                                        saXmlString = Split(varXmlString, "/")
                                        If UBound(saXmlString) > 1 Then
                                            sErrNo = saXmlString(0)
                                            For iCount = 1 To UBound(saXmlString)
                                                If iCount = 1 Then
                                                    sErrDesc = saXmlString(iCount)
                                                Else
                                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                                End If
                                            Next
                                        Else
                                            sErrNo = saXmlString(0)
                                            sErrDesc = saXmlString(1)
                                        End If

                                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                            ''reV:mia oct.23
                                            Logging.LogDebug("maintainPayment", "check : 27  " & sErrorString)
                                            Throw New Exception(sErrorString)
                                        End If

                                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                        ''reV:mia oct.23
                                        Logging.LogDebug("maintainPayment", "check : 28  " & sErrorString)
                                        Throw New Exception(sErrorString)
                                    End If
                                End If

                            ElseIf BallanceToAllocate > nCurrentPaymentAmount Then
                                ''refund the amount which more than nCurrentPayment
                                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                                BallanceToAllocate = BallanceToAllocate - nCurrentPaymentAmount


                                If manageRentalPayment(True, _
                                                        RptId, _
                                                        RptRntId, _
                                                        szPaymentId, _
                                                        "O", _
                                                        szPaymentCurrency, _
                                                        szUserLocalCurrencyName, _
                                                        BallanceToAllocate, _
                                                        szPaymentCurrency, _
                                                        szUserLocalCurrencyName, _
                                                        BallanceToAllocate, _
                                                        varEmptyParameter, _
                                                        varEmptyParameter, _
                                                        varEmptyParameter, _
                                                        nIntegrity, _
                                                        sUsrId, _
                                                        sUsrId, _
                                                        sProgname, _
                                                        szUserLocalCurrencyName, _
                                                        String.Empty, _
                                                        varXmlString, _
                                                        sOriginalPaymentAmt) = False Then
                                    ' Error Section
                                    saXmlString = Split(varXmlString, "/")
                                    If UBound(saXmlString) > 1 Then
                                        sErrNo = saXmlString(0)
                                        For iCount = 1 To UBound(saXmlString)
                                            If iCount = 1 Then
                                                sErrDesc = saXmlString(iCount)
                                            Else
                                                sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                            End If
                                        Next
                                    Else
                                        sErrNo = saXmlString(0)
                                        sErrDesc = saXmlString(1)
                                    End If

                                    If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                        ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                                        ''reV:mia oct.23
                                        Logging.LogDebug("maintainPayment", "check : 29  " & sErrorString)
                                        Throw New Exception(sErrorString)
                                    End If

                                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                                    ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                                    ''reV:mia oct.23
                                    Logging.LogDebug("maintainPayment", "check : 30  " & sErrorString)
                                    Throw New Exception(sErrorString)

                                End If

                            End If

                        End If

                    End If

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    ''process the return xml
                    tmpreturnXML = getSecondPayment(szPaymentId)

                    Try
                        processRetDom.LoadXml(tmpreturnXML)
                    Catch ex As System.Xml.XmlException
                        varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                        ' Error Section
                        saXmlString = Split(varXmlString, "/")
                        If UBound(saXmlString) > 1 Then
                            sErrNo = saXmlString(0)
                            For iCount = 1 To UBound(saXmlString)
                                If iCount = 1 Then
                                    sErrDesc = saXmlString(iCount)
                                Else
                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                End If
                            Next
                        Else
                            sErrNo = saXmlString(0)
                            sErrDesc = saXmlString(1)
                        End If

                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then

                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                            ''reV:mia oct.23
                            Logging.LogDebug("maintainPayment", "check : 31  " & sErrorString)
                            Throw New Exception(sErrorString)

                        End If
                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                        ''reV:mia oct.23
                        Logging.LogDebug("maintainPayment", "check : 32  " & sErrorString)
                        Throw New Exception(sErrorString)


                        ' Error Section
                    End Try

                    returnNode = processRetDom.SelectSingleNode("//PaymentEntry")
                    returnXML = returnXML & returnNode.InnerXml

                    ''empty the xml
                    ''processRetDom.LoadXml(String.Empty)
                    ''       End If ---  If Isremove Else

                Next

                returnXML = returnXML & "</PaymentEntryRoot>"

                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ''success
                If bNewPayment Then
                    varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN045")
                Else
                    varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN046")
                End If
                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(False, String.Empty, String.Empty, varXmlString)

                maintainPayment = "<Root>" & sErrorString & "<Data>" & returnXML & "</Data></Root>"
                Logging.LogDebug("maintainPayment", "check : 33  " & maintainPayment)
                oTransaction.CommitTransaction()
            Catch ex As Exception
                Logging.LogDebug("maintainPayment", "check : Final  " & ex.Message & vbCrLf & ex.StackTrace)
                oTransaction.RollbackTransaction()
                Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "Error", "System", ex.Message + "," + ex.StackTrace) & "</Root>"
            End Try
        End Using

    End Function

    Public Shared Function paymentReversal(ByVal xmlPayment As String, _
                                      ByVal sUsrId As String, _
                                      ByVal sProgname As String, _
                                      Optional ByVal IsPaymentChange As Boolean = False) As String

        Dim paramupdatePayment(5) As Aurora.Common.Data.Parameter
        Dim params(14) As Aurora.Common.Data.Parameter

        ''rev:mia March 8, 2010
        ''Dim paramNewPayment(17) As Aurora.Common.Data.Parameter
        ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010


        ''rev:mia march 12 2012 -- Added CVC2
        Dim paramNewPayment(21) As Aurora.Common.Data.Parameter
        Dim spNameManagePaymentDetail As String = "paym_managePaymentDetail"


        Dim szRentalId, szBookingId, szBookingNumber, szPaymentDate As String
        Dim dAmountToPay, dLocalAmount As Double
        Dim szUserLocalCurrency, szUserLocalCurrencyName As String
        Dim szPaymentId, szPaymentCurrency, szUserLocCode, szPmtPmtId, szStatus As String
        Dim PdtId As Object
        Dim PdtAccNum As Object
        Dim PdtAccName As Object
        Dim PdtApprovalRef As Object
        Dim PdtExpDate As Object
        Dim PdtBankNum As Object
        Dim PdtBranchNum As Object
        Dim PdtChequeNum As Object
        Dim PdtPmtId As Object
        Dim PdtAmount As Object
        Dim nIntegrity As Long
        Dim PdtIntegrity As Object
        ''
        Dim colp As BookingPaymentCollection

        Dim varXmlString As String
        Dim saXmlString As String()
        Dim iCount As Long
        Dim sErrNo As String
        Dim sErrDesc As String
        Dim sErrorString As String
        Dim sError As String

        ''
        Dim oPaymentDom As New System.Xml.XmlDocument
        Dim root As System.Xml.XmlElement
        ''
        Dim processRetDom As System.Xml.XmlDocument
        Dim returnNode As System.Xml.XmlNode

        Dim returnXML As String
        Dim tmpreturnXML As String



        ''varaibles to walk through on the xml
        Dim tNodeList As System.Xml.XmlNodeList
        Dim tNode As System.Xml.XmlNode
        Dim pmEntryRoot As System.Xml.XmlNode

        Dim pmEntry As BookingPaymentEntry
        Dim pmItem As BookingPaymentItem

        Dim dCurrentPayment As Double
        Dim dAmountToAllocate As Double

        Dim nPmEntryLength As Long
        Dim itemCol As BookingPaymentItemsCollection

        ''
        Dim GUID As String

        Dim i, j As Long

        Dim szPayMentMaintainXml As Object

        Dim szOriginalPaymentId As String
        Dim varErrorString As Object
        Dim sPmtReversalofPmtId As Object
        Dim sPmtPaymentOut As Object
        Dim szOriginalPmtAmt As Object
        Dim sDpsAuthCode As String, sDpsMerchRef As String, sDpsTxnRef As String
        ''rev:mia March 8, 2010
        Dim sPdtDpsEfTxnRef As String = ""
        ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
        Dim sPdtEFTPOSCardMethodDontMatch As String = ""
        Dim sPdtEFTPOSCardTypeSwiped As String = ""
        Dim sPdtEFTPOSCardTypeSurchargeAdded As String = ""
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        Try
            oPaymentDom.LoadXml(xmlPayment)
        Catch ex As System.Xml.XmlException
            varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varXmlString)
            Return "<Root>" & sErrorString & "<Data></Data></Root>"
        End Try


        Try
            root = oPaymentDom.DocumentElement
            szBookingId = root.SelectSingleNode("//Root/Payment/BookingId").InnerText
            'the payment must associate with booking, if it isn't there return
            If Len(szBookingId) = 0 Then
                varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varXmlString)
                ''rev:mia oct.23
                ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                Throw New Exception(sErrorString)
            End If

            szBookingNumber = root.SelectSingleNode("//Root/Payment/BookingNumber").InnerText
            ''get rental id if any
            szRentalId = root.SelectSingleNode("//Root/Payment/RentalId").InnerText
            szUserLocCode = root.SelectSingleNode("//Root/Payment/UserLocCode").InnerText
            szUserLocalCurrency = root.SelectSingleNode("//Root/Payment/Currency").InnerText

            '--- Issue 159 - Payments out processing.
            sPmtReversalofPmtId = root.SelectSingleNode("//Root/Payment/PaymentId").InnerText
            sPmtPaymentOut = root.SelectSingleNode("//Root/Payment/PmtPaymentOut").InnerText

            ''paym_GetCurrencyDesc
            szUserLocalCurrencyName = Aurora.Common.Data.ExecuteScalarSP("paym_GetCurrencyDesc", szUserLocalCurrency) '' oAuroraDAL.getRecord(AURORA_DBSTOREDPROC_PAYMGETCURRENCYDESCRIPTION & " '" & szUserLocalCurrency & "'")

            ''initialize the return xml
            processRetDom = New System.Xml.XmlDocument
            returnXML = "<PaymentEntryRoot>"

            If root.ChildNodes(1).Name = "PaymentEntryRoot" Then
                If root.ChildNodes(1).ChildNodes.Count > 0 Then
                    colp = New BookingPaymentCollection
                    For i = 0 To root.ChildNodes(1).ChildNodes.Count - 1

                        pmEntry = New BookingPaymentEntry
                        With pmEntry
                            .pPaymentId = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(10).InnerText.Trim()
                            .pAmount = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(4).InnerText.Trim()
                            .pLocalAmount = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(5).InnerText.Trim()
                            .pCurrency = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(6).InnerText.Trim()
                            .pIsRemove = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(8).InnerText.Trim()
                            .pMethod = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(0).InnerText.Trim()
                            .pSubMethod = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(1).InnerText.Trim()
                            .pIntegrity = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(9).InnerText.Trim()
                            .pStatus = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(12).InnerText.Trim()
                            .pPaymentDate = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(11).InnerText.Trim()

                            ''rev:mia - dEC 14 2010
                            If (Not root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(15) Is Nothing) Then
                                .pOriginalPaymentAmt = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(15).InnerText.Trim()
                            Else
                                Logging.LogError("PaymentReversal", "OriginalPayment not exist in xml")
                            End If

                        End With

                        pmEntryRoot = root.ChildNodes(1).ChildNodes(i).ChildNodes.Item(13)
                        nPmEntryLength = pmEntryRoot.ChildNodes.Count
                        If nPmEntryLength > 0 Then
                            ''create collection for the payment details
                            itemCol = New BookingPaymentItemsCollection
                            For j = 0 To nPmEntryLength - 1
                                pmItem = New BookingPaymentItem
                                With pmItem
                                    .pAccountName = pmEntryRoot.ChildNodes(j).ChildNodes.Item(0).InnerText.Trim()
                                    .pPmtPmId = pmEntryRoot.ChildNodes(j).ChildNodes.Item(1).InnerText.Trim()
                                    .pApproval = pmEntryRoot.ChildNodes(j).ChildNodes.Item(2).InnerText.Trim()
                                    .pExpDate = pmEntryRoot.ChildNodes(j).ChildNodes.Item(3).InnerText.Trim()
                                    .pChequeNo = pmEntryRoot.ChildNodes(j).ChildNodes.Item(4).InnerText.Trim()
                                    .pBank = pmEntryRoot.ChildNodes(j).ChildNodes.Item(5).InnerText.Trim()
                                    .pBranch = pmEntryRoot.ChildNodes(j).ChildNodes.Item(6).InnerText.Trim()
                                    .pAccountNo = pmEntryRoot.ChildNodes(j).ChildNodes.Item(7).InnerText.Trim()
                                    .pAmount = pmEntryRoot.ChildNodes(j).ChildNodes.Item(8).InnerText.Trim()
                                    .pIntegrity = pmEntryRoot.ChildNodes(j).ChildNodes.Item(9).InnerText.Trim()
                                    .pPdtId = pmEntryRoot.ChildNodes(j).ChildNodes.Item(11).InnerText.Trim()
                                    .pAuthCode = pmEntryRoot.ChildNodes(j).SelectSingleNode("AuthCode").InnerText.Trim()
                                    .pMerchRef = pmEntryRoot.ChildNodes(j).SelectSingleNode("MerchRef").InnerText.Trim()
                                    .pTxnRef = pmEntryRoot.ChildNodes(j).SelectSingleNode("TxnRef").InnerText.Trim()

                                    ''rev:mia march 8, 2010
                                    If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtDpsEfTxnRef") Is Nothing Then
                                        .pPdtDpsEfTxnRef = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtDpsEfTxnRef").InnerText.Trim()
                                    Else
                                        .pPdtDpsEfTxnRef = String.Empty
                                    End If

                                    ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                                    If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardMethodDontMatch") Is Nothing Then
                                        .PdtEFTPOSCardMethodDontMatch = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardMethodDontMatch").InnerText.Trim()
                                    Else
                                        .PdtEFTPOSCardMethodDontMatch = String.Empty
                                    End If

                                    If Not pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardTypeSurchargeAdded") Is Nothing Then
                                        .PdtEFTPOSCardTypeSurchargeAdded = pmEntryRoot.ChildNodes(j).SelectSingleNode("PdtEFTPOSCardTypeSurchargeAdded").InnerText.Trim()
                                    Else
                                        .PdtEFTPOSCardTypeSurchargeAdded = String.Empty
                                    End If


                                End With
                                itemCol.Add(pmItem)
                                pmItem = Nothing
                            Next
                        End If

                        pmEntry.pCollectionItems = itemCol

                        colp.Add(pmEntry)
                        pmEntry = Nothing
                    Next
                End If
            End If

            tNode = Nothing
            tNodeList = Nothing

            For Each pmEntry In colp

                nIntegrity = 0
                dAmountToPay = 0
                dLocalAmount = 0
                szPaymentCurrency = String.Empty
                szPmtPmtId = String.Empty
                PdtAccNum = String.Empty
                PdtAccName = String.Empty
                PdtApprovalRef = String.Empty
                PdtExpDate = String.Empty
                PdtAmount = 0 ''rev:mia Feb 29 2012 - last day of Feb. Leap year
                szStatus = String.Empty
                szPaymentDate = String.Empty

                dCurrentPayment = 0     '' for allocate rental payments and store the remainder
                '' within the rental allocation loop

                dAmountToAllocate = 0

                ''DOC: If this is a Bond Imprint reverseal, need to update status ...
                Dim bIsposted As Boolean
                bIsposted = getPtmIsPostedToFinance(pmEntry.pPaymentId, varErrorString)

                If String.IsNullOrEmpty(varErrorString) = False Then
                    If Not varErrorString.Equals(String.Empty) Then
                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varErrorString)
                        ''rev:mia oct.23
                        ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                        Throw New Exception(sErrorString)
                    End If
                End If


                If bIsposted = False Then

                    nIntegrity = pmEntry.pIntegrity
                    ' params from DB

                    ' @sPaymentId		varchar(64),
                    ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PAYMENTID, 200, 64, pmEntry.pPaymentId)
                    paramupdatePayment(0) = New Aurora.Common.Data.Parameter("sPaymentId", DbType.String, pmEntry.pPaymentId, Parameter.ParameterType.AddInParameter)

                    ' @sPmtStatus		varchar(64) = NULL,
                    ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_PMTSTATUS, 200, 64, "Closed")
                    paramupdatePayment(1) = New Aurora.Common.Data.Parameter("sPmtStatus", DbType.String, "Closed", Parameter.ParameterType.AddInParameter)

                    ' @iIntegrityNo	int  = 0,
                    ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_INTEGRITYNUMBER, 3, 4, nIntegrity)
                    paramupdatePayment(2) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int16, nIntegrity, Parameter.ParameterType.AddInParameter)

                    ' @AddUsrId		varchar (64) = '',
                    ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ADDUSERID, 200, 64, sUsrId)
                    paramupdatePayment(3) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)

                    ' @ModUsrId		varchar (64) = '',
                    ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_MODIFYUSERID, 200, 64, sUsrId)
                    paramupdatePayment(4) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)

                    ' @AddPrgmName	varchar(64) = '' 
                    ''oAuroraDAL.appendParameterIn(AURORA_DBPARAM_ADDPROGRAMNAME, 200, 64, sProgname)
                    paramupdatePayment(5) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)

                    ''paym_updatePaymentStatus
                    varXmlString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_updatePaymentStatus", paramupdatePayment) ''oAuroraDAL.UpdateRecords(AURORA_DBSTOREDPROC_PAYMUPDATEPAYMENTSTATUS)

                    If varXmlString <> "SUCCESS" Then
                        ' Error Section
                        saXmlString = Split(varXmlString, "/")
                        If UBound(saXmlString) > 1 Then
                            sErrNo = saXmlString(0)
                            For iCount = 1 To UBound(saXmlString)
                                If iCount = 1 Then
                                    sErrDesc = saXmlString(iCount)
                                Else
                                    sErrDesc = CStr(sErrDesc) & "/" & saXmlString(iCount)
                                End If
                            Next
                        Else
                            sErrNo = 0
                            sErrDesc = saXmlString(0)
                        End If

                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then

                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                            ''rev:mia oct.23
                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                            Throw New Exception(sErrorString)

                        End If

                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)

                        ''rev:mia oct.23
                        ''Return "<Root>" & sErrorString & "<Data>" & CStr(szPayMentMaintainXml) & "</Data></Root>"
                        Throw New Exception(sErrorString)


                    Else
                        varXmlString = String.Empty
                    End If

                    nIntegrity = nIntegrity + 1

                End If


                GUID = Aurora.Common.Data.ExecuteScalarSP("sp_get_newId")
                GUID = Replace(GUID, "<Id>", String.Empty)
                GUID = Replace(GUID, "</Id>", String.Empty)
                GUID = Trim(GUID)
                szPaymentId = GUID
                nIntegrity = 0


                With pmEntry
                    szOriginalPaymentId = .pPaymentId

                    ''---------------------------------------------------------
                    ''rev:mia Feb 29 2012 - last day of Feb. Leap year
                    ''trap zero or if its empty
                    ''---------------------------------------------------------
                    If (String.IsNullOrEmpty(.pAmount)) Then
                        dAmountToPay = 0
                    Else
                        dAmountToPay = 0 - CDbl(.pAmount)
                    End If

                    If (String.IsNullOrEmpty(.pLocalAmount)) Then
                        dLocalAmount = 0
                    Else
                        dLocalAmount = 0 - CDbl(.pLocalAmount)
                    End If

                    If (String.IsNullOrEmpty(.pOriginalPaymentAmt)) Then
                        szOriginalPmtAmt = 0
                    Else
                        szOriginalPmtAmt = .pOriginalPaymentAmt
                    End If

                    ''---------------------------------------------------------

                    szPaymentCurrency = .pCurrency
                    szPmtPmtId = .pSubMethod
                    szStatus = .pStatus
                    szPaymentDate = .pPaymentDate

                    
                End With


                If bIsposted = True Then
                    If szStatus <> "Suspended" Then
                        szStatus = "Open"
                    End If
                Else
                    szStatus = "Closed"
                End If

                ''allocate the local amount at the rentalpayment table
                dAmountToAllocate = dLocalAmount

                ''first update payment
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ' db params
                ' @sPaymentId				varchar(64),
                ' @sPmtPtmId				varchar(64),
                ' @sBookingId				varchar(64),
                ' @sPmtCurrId				varchar(64),
                ' @PmtDateTime			datetime 	= NULL,
                ' @dPmtAmt				money,
                ' @dLocCurrAmt			money,
                ' @sPmtStatus				varchar(64) = NULL,
                ' @sPmtBranch				varchar(64) = NULL,
                ' @sPmtReversalofPmtId	varchar(64)	= NULL ,		--- Issue 159 - Payments out processing.
                ' @sPmtPaymentOut			varchar(12) = NULL,			--- Issue 159 - Payments out processing.
                ' @iIntegrityNo			int  		= 0,
                ' @AddUsrId				varchar (64)= '',
                ' @ModUsrId				varchar (64)= '',
                ' @AddPrgmName			varchar(64) = '' 

                params(0) = New Aurora.Common.Data.Parameter("sPaymentId", DbType.String, szPaymentId, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("sPmtPtmId", DbType.String, szPmtPmtId, Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("sBookingId", DbType.String, szBookingId, Parameter.ParameterType.AddInParameter)

                params(3) = New Aurora.Common.Data.Parameter("sPmtCurrId", DbType.String, szPaymentCurrency, Parameter.ParameterType.AddInParameter)
                params(4) = New Aurora.Common.Data.Parameter("PmtDateTime", DbType.DateTime, szPaymentDate, Parameter.ParameterType.AddInParameter)
                params(5) = New Aurora.Common.Data.Parameter("dPmtAmt", DbType.Currency, dLocalAmount, Parameter.ParameterType.AddInParameter)

                params(6) = New Aurora.Common.Data.Parameter("dLocCurrAmt", DbType.Currency, dAmountToPay, Parameter.ParameterType.AddInParameter)
                params(7) = New Aurora.Common.Data.Parameter("sPmtStatus", DbType.String, IIf(szStatus = "", DBNull.Value, szStatus), Parameter.ParameterType.AddInParameter)
                params(8) = New Aurora.Common.Data.Parameter("sPmtBranch", DbType.String, szUserLocCode, Parameter.ParameterType.AddInParameter)

                ''reV:mia oct21 - change string.empty to sPmtReversalofPmtId
                params(9) = New Aurora.Common.Data.Parameter("sPmtReversalofPmtId", DbType.String, sPmtReversalofPmtId, Parameter.ParameterType.AddInParameter)
                params(10) = New Aurora.Common.Data.Parameter("sPmtPaymentOut", DbType.String, sPmtPaymentOut, Parameter.ParameterType.AddInParameter)
                params(11) = New Aurora.Common.Data.Parameter("iIntegrityNo", DbType.Int16, nIntegrity, Parameter.ParameterType.AddInParameter)

                params(12) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                params(13) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                params(14) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)

                varXmlString = Aurora.Common.Data.ExecuteOutputObjectSP("paym_managePayment", params)

                If varXmlString <> "SUCCESS" Then
                    ' Error Section
                    saXmlString = Split(varXmlString, "/")
                    If UBound(saXmlString) > 1 Then
                        sErrNo = saXmlString(0)
                        For iCount = 1 To UBound(saXmlString)
                            If iCount = 1 Then
                                sErrDesc = saXmlString(iCount)
                            Else
                                sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                            End If
                        Next
                    Else
                        sErrNo = 0
                        sErrDesc = saXmlString(0)
                    End If

                    If sErrNo = "GEN001" Or sErrNo = "GEN008" Then
                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                        Return "<Root>" & sErrorString & "<Data></Data></Root>"
                    End If

                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                    ''rev:mia oct.23
                    ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                    Throw New Exception(sErrorString)
                Else
                    varXmlString = String.Empty
                End If

                ''update payment details
                '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                PdtPmtId = szPaymentId


                ''get payment details from the collection
                For Each pmItem In pmEntry.pCollectionItems

                    PdtAccNum = String.Empty
                    PdtAccName = String.Empty
                    PdtBankNum = String.Empty
                    PdtBranchNum = String.Empty
                    PdtChequeNum = String.Empty
                    PdtApprovalRef = String.Empty
                    PdtExpDate = String.Empty
                    PdtIntegrity = String.Empty
                    PdtAmount = 0 ''rev:mia Feb 29 2012 - last day of Feb. Leap year

                    PdtAccNum = pmItem.pAccountNo
                    PdtAccName = pmItem.pAccountName
                    PdtBankNum = pmItem.pBank
                    PdtBranchNum = pmItem.pBranch
                    PdtChequeNum = pmItem.pChequeNo
                    PdtApprovalRef = pmItem.pApproval
                    PdtExpDate = pmItem.pExpDate
                    PdtIntegrity = pmItem.pIntegrity

                    ''---------------------------------------------------------
                    ''rev:mia Feb 29 2012 - last day of Feb. Leap year
                    ''trap zero or if its empty
                    ''---------------------------------------------------------
                    If (String.IsNullOrEmpty(pmItem.pAmount)) Then
                        PdtAmount = 0
                    Else
                        PdtAmount = 0 - pmItem.pAmount
                    End If
                    ''---------------------------------------------------------

                    sDpsAuthCode = pmItem.pAuthCode
                    sDpsMerchRef = pmItem.pMerchRef
                    sDpsTxnRef = pmItem.pTxnRef
                    ''rev:mia march 8 2010
                    sPdtDpsEfTxnRef = pmItem.pPdtDpsEfTxnRef
                    ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                    sPdtEFTPOSCardMethodDontMatch = pmItem.PdtEFTPOSCardMethodDontMatch
                    sPdtEFTPOSCardTypeSwiped = pmItem.PdtEFTPOSCardTypeSwiped
                    sPdtEFTPOSCardTypeSurchargeAdded = pmItem.PdtEFTPOSCardTypeSurchargeAdded

                    GUID = Aurora.Common.Data.ExecuteScalarSP("sp_get_newId")
                    GUID = Replace(GUID, "<Id>", String.Empty)
                    GUID = Replace(GUID, "</Id>", String.Empty)
                    GUID = Trim(GUID)
                    PdtId = GUID

                    ' params from db
                    ' @sPdtId				VARCHAR	(64) 	= NULL,
                    ' @sPdtPmtId			VARCHAR	(64) 	= NULL,
                    ' @sPdtBankNum		VARCHAR	(24) 	= NULL,
                    ' @sPdtBranchNum		VARCHAR	(24) 	= NULL,
                    ' @sPdtChequeNum		VARCHAR	(24) 	= NULL,
                    ' @sPdtAccnum			VARCHAR	(24) 	= NULL,
                    ' @sPdtAmount			VARCHAR	(18) 	= NULL,
                    ' @sPdtAccName		VARCHAR	(256) 	= NULL,
                    ' @sPdtApproval		VARCHAR	(256) 	= NULL,
                    ' @sPdtExpiry			VARCHAR	(16) 	= NULL,
                    ' @sDpsAuthCode		VARCHAR	(256)	= NULL ,
                    ' @sDpsMerchRef		VARCHAR	(64)	= NULL ,
                    ' @sDpsTxnRef			VARCHAR	(256)	= NULL ,
                    ' @IntegrityNo		INT  			= 0,
                    ' @AddUsrId			VARCHAR (64) 	= '',
                    ' @ModUsrId			VARCHAR (64) 	= '',
                    ' @AddPrgmName		VARCHAR (64) 	= ''


                    paramNewPayment(0) = New Aurora.Common.Data.Parameter("sPdtId", DbType.String, PdtId, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(1) = New Aurora.Common.Data.Parameter("sPdtPmtId", DbType.String, PdtPmtId, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(2) = New Aurora.Common.Data.Parameter("sPdtBankNum", DbType.String, PdtBankNum, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(3) = New Aurora.Common.Data.Parameter("sPdtBranchNum", DbType.String, PdtBranchNum, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(4) = New Aurora.Common.Data.Parameter("sPdtChequeNum", DbType.String, PdtChequeNum, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(5) = New Aurora.Common.Data.Parameter("sPdtAccnum", DbType.String, PdtAccNum, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(6) = New Aurora.Common.Data.Parameter("sPdtAmount", DbType.String, PdtAmount, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(7) = New Aurora.Common.Data.Parameter("sPdtAccName", DbType.String, PdtAccName, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(8) = New Aurora.Common.Data.Parameter("sPdtApproval", DbType.String, PdtApprovalRef, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(9) = New Aurora.Common.Data.Parameter("sPdtExpiry", DbType.String, PdtExpDate, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(10) = New Aurora.Common.Data.Parameter("sDpsAuthCode", DbType.String, sDpsAuthCode, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(11) = New Aurora.Common.Data.Parameter("sDpsMerchRef", DbType.String, sDpsMerchRef, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(12) = New Aurora.Common.Data.Parameter("sDpsTxnRef", DbType.String, sDpsTxnRef, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(13) = New Aurora.Common.Data.Parameter("IntegrityNo", DbType.Int16, PdtIntegrity, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(14) = New Aurora.Common.Data.Parameter("AddUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(15) = New Aurora.Common.Data.Parameter("ModUsrId", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(16) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)

                    ''rev:mia march 8,2010
                    paramNewPayment(17) = New Aurora.Common.Data.Parameter("PdtDpsEfTxnRef", DbType.String, sPdtDpsEfTxnRef, Parameter.ParameterType.AddInParameter)
                    ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                    paramNewPayment(18) = New Aurora.Common.Data.Parameter("PdtEFTPOSCardMethodDontMatch", DbType.String, sPdtEFTPOSCardMethodDontMatch, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(19) = New Aurora.Common.Data.Parameter("PdtEFTPOSCardTypeSwiped", DbType.String, sPdtEFTPOSCardTypeSwiped, Parameter.ParameterType.AddInParameter)
                    paramNewPayment(20) = New Aurora.Common.Data.Parameter("PdtEFTPOSCardTypeSurchargeAdded", DbType.String, sPdtEFTPOSCardTypeSurchargeAdded, Parameter.ParameterType.AddInParameter)

                    ''rev:mia march 12 2012 -- Added CVC2"
                    Try
                        Dim strcvv2 As String = root.SelectSingleNode("//Root/cvv2").InnerText
                        paramNewPayment(21) = New Aurora.Common.Data.Parameter("PdtCVV2", DbType.String, strcvv2, Parameter.ParameterType.AddInParameter)
                    Catch ex As Exception
                        paramNewPayment(21) = New Aurora.Common.Data.Parameter("PdtCVV2", DbType.String, String.Empty, Parameter.ParameterType.AddInParameter)
                    End Try
                    varXmlString = Aurora.Common.Data.ExecuteOutputObjectSP(spNameManagePaymentDetail, paramNewPayment)


                    If varXmlString <> "SUCCESS" Then
                        ' Error Section
                        saXmlString = Split(varXmlString, "/")
                        If UBound(saXmlString) > 1 Then
                            sErrNo = saXmlString(0)
                            For iCount = 1 To UBound(saXmlString)
                                If iCount = 1 Then
                                    sErrDesc = saXmlString(iCount)
                                Else
                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                End If
                            Next
                        Else
                            sErrNo = 0
                            sErrDesc = saXmlString(0)
                        End If

                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then

                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)

                            ''rev:mia oct.23
                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                            Throw New Exception(sErrorString)

                        End If

                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)

                        ''rev:mia oct.23
                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                        Throw New Exception(sErrorString)

                        ' Error Section
                    Else
                        varXmlString = String.Empty
                    End If

                Next

                ''rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                If (Not String.IsNullOrEmpty(sPdtEFTPOSCardMethodDontMatch) AndAlso Not IsPaymentChange) Then
                    IsPaymentChange = True
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ''create rental payment reversal
                Call rentalPaymentReversal(szOriginalPaymentId, _
                                            szPaymentId, _
                                            szOriginalPmtAmt, _
                                            sUsrId, _
                                            sProgname, _
                                            varXmlString, _
                                            IsPaymentChange, _
                                             dAmountToPay, _
                                             dLocalAmount)

                If Not varXmlString.Equals(String.Empty) Then
                    If (Len(varXmlString) > 0) And (varXmlString <> RPTADJUST_SUCCESS) Then
                        ' Error Section
                        saXmlString = Split(varXmlString, "/")
                        If UBound(saXmlString) > 1 Then
                            sErrNo = saXmlString(0)
                            For iCount = 1 To UBound(saXmlString)
                                If iCount = 1 Then
                                    sErrDesc = saXmlString(iCount)
                                Else
                                    sErrDesc = sErrDesc & "/" & saXmlString(iCount)
                                End If
                            Next
                        Else
                            sErrNo = 0
                            sErrDesc = saXmlString(0)
                        End If

                        If sErrNo = "GEN001" Or sErrNo = "GEN008" Then

                            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                            ''rev:mia oct.23
                            ''Return "<Root>" & sErrorString & "<Data></Data></Root>"
                            Throw New Exception(sErrorString)

                        End If

                        sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrNo, "Application", sErrDesc)
                        ''rev:mia oct.23
                        ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                        Throw New Exception(sErrorString)

                    End If
                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                ''process the return xml
                tmpreturnXML = getSecondPayment(szPaymentId)


                Try

                    processRetDom.LoadXml(tmpreturnXML)

                Catch ex As System.Xml.XmlException
                    varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                    sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varXmlString)
                    paymentReversal = "<Root>" & sErrorString & "<Data></Data></Root>"
                    ''rev:mia oct.23
                    ''Return "<Root>" & sErrorString & "<Data>" & szPayMentMaintainXml & "</Data></Root>"
                    Throw New Exception(paymentReversal)

                End Try

                returnNode = processRetDom.SelectSingleNode("//PaymentEntry")
                returnXML = returnXML & returnNode.InnerXml

                ''empty the xml
                '' processRetDom.LoadXml(String.Empty)

            Next

            returnXML = returnXML & "</PaymentEntryRoot>"

            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            ''success
            varXmlString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN046")
            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(False, String.Empty, String.Empty, varXmlString)
            Return "<Root>" & sErrorString & "<Data>" & returnXML & "</Data></Root>"

            colp = Nothing
            oPaymentDom = Nothing
            root = Nothing
            processRetDom = Nothing

        Catch ex As Exception
            Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "Error", "System", ex.Message + "," + ex.StackTrace + "," + ex.StackTrace) & "</Root>"
        Finally
            paramupdatePayment = Nothing
            params = Nothing
            paramNewPayment = Nothing

        End Try

    End Function

    Public Shared Function getPtmIsPostedToFinance(ByVal szParameter As String, _
                                           ByRef varErrorString As Object) As Boolean

        Dim szErrorString As String
        Dim szXmlString As String
        Dim oPaymentDom As New System.Xml.XmlDocument
        Dim root As System.Xml.XmlElement

        Try

            ''paym_getPmtIsPostedToFinance

            szXmlString = Aurora.Common.Data.ExecuteScalarSP("paym_getPmtIsPostedToFinance", szParameter)

            If InStr(1, szXmlString, "<Error><ErrStatus>") > 0 Then
                szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", "No Records Found")
                varErrorString = "<Root>" & szErrorString & "<Data></Data>" & "</Root>"
                Return False
            End If

            Try
                oPaymentDom.LoadXml(szXmlString)
            Catch ex As System.Xml.XmlException
                szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", "Failed to load XML document")
                varErrorString = "<Root>" & szErrorString & "<Data></Data>" & "</Root>"
                Return False
            End Try

            root = oPaymentDom.DocumentElement
            If root.SelectSingleNode("//PaymentMethod").ChildNodes.Count = 0 Then
                Return False
            End If

            Return CBool(root.SelectSingleNode("//PaymentMethod").ChildNodes.Item(0).InnerText)

        Catch ex As Exception
            varErrorString = "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "Error", "System", ex.Message + "," + ex.StackTrace) & "</Root>"
            Return False
        Finally
            oPaymentDom = Nothing
        End Try

    End Function

    Public Shared Function PaymentTransfer(ByVal xmlPayment As String, _
                                   ByVal BookingRef As String, _
                                   ByVal sUsrId As String, _
                                   ByVal sProgname As String) As String

        Dim params(14) As Aurora.Common.Data.Parameter
        Dim paramnewPayment(9) As Aurora.Common.Data.Parameter
        Dim paramsrentalhistory(9) As Aurora.Common.Data.Parameter

        Dim xmlPaymentObj As New System.Xml.XmlDocument
        Dim XMLRetErr As New System.Xml.XmlDocument

        Dim sRntId As String
        Dim sBooId As String
        Dim iTotalAmtList As Decimal
        Dim sNewBooNum As String
        Dim sNewRntNum As String
        Dim sNewBooId As String
        Dim sNewRntId As String
        Dim sErrorNo As String = "System"
        Dim sErrorString As String
        Dim sBookingNumber As String
        Dim sExRntNum As String
        Dim sNoteType As String
        Dim sAudTypeId As String
        Dim strError As String
        Dim NewString As String

        Dim iTotalPaidLocal As Decimal
        Dim iTotalPaidOrg As Decimal
        Dim sCurrLocal As String
        Dim sSecondCurrCode As String
        Dim sFirstCurrCode As String

        Try
            xmlPaymentObj.LoadXml(xmlPayment)
        Catch ex As Xml.XmlException
            sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "GEN023", "Application", xmlPayment)
            Return "<Root>" & sErrorString & "<Data></Data></Root>"
        End Try


        '' Using oTransaction As New Aurora.Common.Data.DatabaseTransaction()


        Try



            sBooId = xmlPaymentObj.DocumentElement.SelectSingleNode("//*/BookingId").InnerText
            sRntId = xmlPaymentObj.DocumentElement.SelectSingleNode("//*/RentalId").InnerText
            sBookingNumber = xmlPaymentObj.DocumentElement.SelectSingleNode("//*/BookingNumber").InnerText
            sNewBooNum = Split(BookingRef, "/")(0)
            sNewRntNum = Split(BookingRef, "/")(1)

            '*** Get Data to Add Note
            params(0) = New Aurora.Common.Data.Parameter("psRntId", DbType.String, sRntId, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("psBooNum", DbType.String, sNewBooNum, Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("psRntNum", DbType.String, sNewRntNum, Parameter.ParameterType.AddInParameter)
            params(3) = New Aurora.Common.Data.Parameter("pPmtId", DbType.String, xmlPaymentObj.DocumentElement.SelectSingleNode("//*/PaymentId").InnerText, Parameter.ParameterType.AddInParameter)
            params(4) = New Aurora.Common.Data.Parameter("pRntNo", DbType.String, 200, Parameter.ParameterType.AddOutParameter)
            params(5) = New Aurora.Common.Data.Parameter("pNteCodTypId", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(6) = New Aurora.Common.Data.Parameter("pNteCodAudTypId", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(7) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(8) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(9) = New Aurora.Common.Data.Parameter("iTotalAmtList", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(10) = New Aurora.Common.Data.Parameter("iTotalPaidLocal", DbType.Currency, 64, Parameter.ParameterType.AddOutParameter)
            params(11) = New Aurora.Common.Data.Parameter("iTotalPaidOrg", DbType.Currency, 64, Parameter.ParameterType.AddOutParameter)
            params(12) = New Aurora.Common.Data.Parameter("sCurrLocal", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(13) = New Aurora.Common.Data.Parameter("sScondCurrCode", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            params(14) = New Aurora.Common.Data.Parameter("sFirstCurrCode", DbType.String, 64, Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("getNoteDetailForPayment", params)

            sExRntNum = params(4).Value
            sNoteType = params(5).Value
            sAudTypeId = params(6).Value
            sNewBooId = params(7).Value
            sNewRntId = params(8).Value
            iTotalAmtList = params(9).Value
            iTotalPaidLocal = params(10).Value
            iTotalPaidOrg = params(11).Value
            sCurrLocal = params(12).Value
            sSecondCurrCode = params(13).Value
            sFirstCurrCode = params(14).Value


            xmlPaymentObj.DocumentElement.SelectSingleNode("//Root/PaymentEntryRoot/PaymentEntry/Amount").InnerText = String.Format("{0:N}", iTotalPaidOrg)
            xmlPaymentObj.DocumentElement.SelectSingleNode("//Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount").InnerText = String.Format("{0:N}", iTotalPaidOrg)


            If Len(Trim(sNewBooId)) = 0 Then
                xmlPaymentObj = Nothing
                ''Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, CStr(sErrorNo), "Application", "Specified Booking Number " & sNewBooNum & " is not valid!!") & "<Data/></Root>"
                Throw New Exception("Specified Booking Number " & sNewBooNum & " is not valid!!")
            End If

            If Len(Trim(sNewRntId)) = 0 Then
                xmlPaymentObj = Nothing
                ''Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrorNo, "Application", "Specified Rental Number " & sNewRntNum & " is not valid!!") & "<Data/></Root>"
                Throw New Exception("Specified Rental Number " & sNewRntNum & " is not valid!!")
            End If

            If sFirstCurrCode <> sSecondCurrCode Then
                xmlPaymentObj = Nothing
                ''Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrorNo, "Application", "Sorry, Bond Transfer is not possible between two different country rental") & "<Data/></Root>"
                Throw New Exception("Sorry, Bond Transfer is not possible between two different country rental")
            End If

            If Math.Round(iTotalAmtList) < Math.Round(iTotalPaidLocal) Then
                xmlPaymentObj = Nothing
                ''Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, sErrorNo, "Application", "Transfer Invalid - The sum of security products on booking " & BookingRef & " is less than the Payment amount") & "<Data/></Root>"
                Throw New Exception("Transfer Invalid - The sum of security products on booking " & BookingRef & " is less than the Payment amount")
            End If
            '**** Reverse the Payment
            strError = paymentReversal(xmlPayment, sUsrId, "REV-PROCESS")

            Call XMLRetErr.LoadXml(strError)

            If Len(XMLRetErr.InnerXml) <> 0 Then
                If XMLRetErr.DocumentElement.SelectSingleNode("//*/ErrStatus").InnerText <> "False" Then
                    xmlPaymentObj = Nothing
                    Throw New Exception(XMLRetErr.InnerXml)
                    ''Return XMLRetErr.InnerXml
                End If
            End If

            '**** ManageNote about Payment Transfer


            strError = Aurora.Common.Data.ExecuteScalarSP("ManageNote", "", sBooId, sBookingNumber, sExRntNum, sNoteType, "Bond Payment Transferred to Booking " & BookingRef, sAudTypeId, 3, 1, 1, sUsrId, sProgname)

            If InStr(strError, "GEN045") <> 1 Then
                xmlPaymentObj = Nothing
                Throw New Exception(strError)
                ''Return strError
            End If

            '*** Rental History for the New Payment

            ''RES_checkRentalHistory
            paramnewPayment(0) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, sBooId, Parameter.ParameterType.AddInParameter)
            paramnewPayment(1) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, sRntId, Parameter.ParameterType.AddInParameter)

            paramnewPayment(2) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, "", Parameter.ParameterType.AddInParameter)
            paramnewPayment(3) = New Aurora.Common.Data.Parameter("rRnhId", DbType.String, "", Parameter.ParameterType.AddInParameter)

            paramnewPayment(4) = New Aurora.Common.Data.Parameter("sChangeEvent", DbType.String, "REV-PAYMENT", Parameter.ParameterType.AddInParameter)
            paramnewPayment(5) = New Aurora.Common.Data.Parameter("sDataType", DbType.String, "RPT", Parameter.ParameterType.AddInParameter)

            paramnewPayment(6) = New Aurora.Common.Data.Parameter("sBphChangedEvent", DbType.String, "", Parameter.ParameterType.AddInParameter)
            paramnewPayment(7) = New Aurora.Common.Data.Parameter("sVehSapId", DbType.String, "", Parameter.ParameterType.AddInParameter)

            paramnewPayment(8) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
            paramnewPayment(9) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)
            strError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_checkRentalHistory", paramnewPayment)

            ''strError = Aurora.Common.Data.ExecuteScalarSP("RES_checkRentalHistory", sBooId, sRntId, "", "", "REV-PAYMENT", "RPT", "", "", sUsrId, sProgname)
            If strError <> "SUCCESS" Then
                xmlPaymentObj = Nothing
                Throw New Exception(strError)
                ''Return strError
            End If


            '**** Change the XML Structure to process Add Payment for the Nominated Booking
            xmlPaymentObj.DocumentElement.SelectSingleNode("//*/BookingId").InnerText = sNewBooId
            xmlPaymentObj.DocumentElement.SelectSingleNode("//*/BookingNumber").InnerText = sNewBooNum
            xmlPaymentObj.DocumentElement.SelectSingleNode("//*/AmountToPay").InnerText = iTotalAmtList
            xmlPaymentObj.DocumentElement.SelectSingleNode("//*/PaymentRunningTotal").InnerText = iTotalAmtList

            xmlPaymentObj.DocumentElement.SelectSingleNode("//*/PaymentId").InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode("//*/AmountToPay").InnerText = xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_AMOUNT).InnerText

            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PAYMENTRUNNINGTOTAL2).InnerText = xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_AMOUNT).InnerText
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_AMOUNT).InnerText = iTotalPaidOrg
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PMTPAYMENTOUT).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_ENTRYPAYMENTID).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PAYMENTDATE).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_STATUS).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PTMID).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PDTID).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PAYMENTENTRYSTATUS).InnerText = String.Empty
            xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PAYMENTENTRYORIGINALAMOUNT).InnerText = -1 * CDbl(xmlPaymentObj.DocumentElement.SelectSingleNode(AURORA_XPATH_PAYMENTENTRYORIGINALAMOUNT).InnerText)



            xmlPayment = xmlPaymentObj.InnerXml
            NewString = "<AmountsOutstandingRoot><AmountsOutstanding><RntId>" & sNewRntId & "</RntId><Type>B</Type><Currency>" & sCurrLocal & "</Currency>"
            NewString = NewString & "<CurrCode>" & sSecondCurrCode & "</CurrCode><AmountOwing>" & iTotalPaidLocal & "</AmountOwing><CustToPay>" & iTotalAmtList & "</CustToPay><CustToPay>" & iTotalAmtList & "</CustToPay>"
            NewString = NewString & "</AmountsOutstanding></AmountsOutstandingRoot>"

            xmlPayment = Replace(xmlPayment, "<AmountsOutstandingRoot></AmountsOutstandingRoot>", NewString)

            Call xmlPaymentObj.LoadXml(xmlPayment)

            '**** Add Bond on to New Booking/rental
            'oErrlog.WriteErrorToFile "", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(25) & xmlPayment

            strError = maintainPayment(xmlPayment, _
                                            sUsrId, _
                                            "REV-PROCESS-TRANS")
            'oErrlog.WriteErrorToFile "", "", Format(Now, "DD MMM YY dddd HH:MM:SS"), Space(25) & "Return value from maintain Payment for other rental.." & strError
            Call XMLRetErr.LoadXml(strError)
            If Len(XMLRetErr.InnerXml) <> 0 Then
                If XMLRetErr.DocumentElement.SelectSingleNode(AURORA_XPATH_ERRSTATUS).InnerText <> "False" Then
                    PaymentTransfer = XMLRetErr.InnerXml
                    xmlPaymentObj = Nothing
                    Throw New Exception("Error processing your payment")
                End If
            End If


            strError = Aurora.Common.Data.ExecuteScalarSP("ManageNote", "", sNewBooNum, sNewBooNum, sNewRntNum, sNoteType, "Bond Payment Transferred from Booking " & sBookingNumber & "/" & sExRntNum, sAudTypeId, 3, 1, 0, sUsrId, sProgname)
            If InStr(strError, "GEN045") <> 1 Then
                xmlPaymentObj = Nothing
            End If

            '*** Rental History for the New Payment


            paramsrentalhistory(0) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, sNewBooId, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(1) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, sRntId, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(2) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(3) = New Aurora.Common.Data.Parameter("rRnhId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(4) = New Aurora.Common.Data.Parameter("sChangeEvent", DbType.String, "PAYMENT", Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(5) = New Aurora.Common.Data.Parameter("sDataType", DbType.String, "RPT", Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(6) = New Aurora.Common.Data.Parameter("sBphChangedEvent", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(7) = New Aurora.Common.Data.Parameter("sVehSapId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(8) = New Aurora.Common.Data.Parameter("sUserCode", DbType.String, sUsrId, Parameter.ParameterType.AddInParameter)
            paramsrentalhistory(9) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, sProgname, Parameter.ParameterType.AddInParameter)
            strError = Aurora.Common.Data.ExecuteOutputObjectSP("RES_checkRentalHistory", paramsrentalhistory)


            If strError <> "SUCCESS" Then
                xmlPaymentObj = Nothing
            End If

            xmlPaymentObj = Nothing
            '' oTransaction.CommitTransaction()
            Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(False, "", "", "Bond Payment Transferred Successfully") & "<Data/></Root>"


        Catch ex As Exception
            PaymentTransfer = "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "Error", "System", ex.Message + "," + ex.StackTrace) & "<Data/></Root>"
            '' oTransaction.RollbackTransaction()
        Finally
            params = Nothing
            paramnewPayment = Nothing
            paramsrentalhistory = Nothing
            xmlPaymentObj = Nothing
            XMLRetErr = Nothing
        End Try
        '' End Using
    End Function

    Public Shared Function getPayment(ByVal szParameter As String) As String

        Dim szErrorString As String = ""
        Dim szXmlString As String = ""
        Dim oPaymentDom As New System.Xml.XmlDocument
        Dim root As System.Xml.XmlElement = Nothing

        Try

            szXmlString = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_GetPayment", szParameter).OuterXml
            If InStr(1, szXmlString, "<Error><ErrStatus>") > 0 Then
                szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "", "Application", "No Records Found")
                Return "<Root>" & szErrorString & "<Data></Data>" & "</Root>"
            End If

            oPaymentDom.LoadXml(szXmlString)

            root = oPaymentDom.DocumentElement
            If root.SelectSingleNode("//PaymentEntryItems").ChildNodes.Count = 0 Then
                szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "", "Application", "No Records Found")
                Return "<Root>" & szErrorString & "<Data></Data>" & "</Root>"
            End If

            szErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(False, "", "", "")
            oPaymentDom = Nothing
            Return "<Root>" & szErrorString & "<Data>" & szXmlString & "</Data></Root>"

        Catch ex As Exception
            oPaymentDom = Nothing
            Return "<Root>" & Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, "Error", "System", ex.Message + "," + ex.StackTrace) & "</Root>"
        End Try

    End Function

#End Region

#Region "Public procedure for Adding History"

    Public Shared Sub AddDecryptHistory(ByVal sRntId As String, ByVal sUserCode As String, Optional ByVal BpdID As String = "", Optional ByVal changeEvent As String = "")
        '@sRntId                           = '00FC6F1F-D437-4C86-A506-5D0E0B914DFC',
        '@sDataType            = 'BPD',
        '@sChangeEvent                 = 'DECRYPT',
        '@sUserCode            = 'rs3',
        '@sAddPrgmName                = 'backend'

        Try
            Aurora.Common.Data.ExecuteScalarSP("REs_CheckRentalHistory", "", _
                                                                        sRntId, _
                                                                        BpdID, _
                                                                        "", _
                                                                        "DECRYPT", _
                                                                        "BPD", _
                                                                        changeEvent, _
                                                                        "", _
                                                                        sUserCode, _
                                                                        "BookingPaymentMaintenance.AddDecryptHistory")
        Catch ex As Exception
            ''do nothing
        End Try
    End Sub

    Public Shared Sub AddDecryptHistoryNoTooltip(ByVal sRntId As String, ByVal sUserCode As String)
        '@sRntId                           = '00FC6F1F-D437-4C86-A506-5D0E0B914DFC',
        '@sDataType            = 'BPD',
        '@sChangeEvent                 = 'DECRYPT',
        '@sUserCode            = 'rs3',
        '@sAddPrgmName                = 'backend'

        Try
            Aurora.Common.Data.ExecuteScalarSP("REs_CheckRentalHistory", "", _
                                                                        sRntId, _
                                                                        "", _
                                                                        "", _
                                                                        "DECRYPT", _
                                                                        "BPD", _
                                                                        "", _
                                                                        "", _
                                                                        sUserCode, _
                                                                        "BookingPaymentMaintenance.AddDecryptHistory")
        Catch ex As Exception
            ''do nothing
        End Try
    End Sub


#End Region

#Region "REV:MIA DEC22"
    Shared Function InvalidcurrencyMessage(ByVal xmlstring As String) As String
        Dim tempXML As New XmlDocument
        Dim tempReturn As String = ""
        Try
            tempXML.LoadXml(xmlstring)
            If tempXML.SelectSingleNode("Root/Data") IsNot Nothing Then
                tempReturn = tempXML.SelectSingleNode("Root/Data").InnerText
            End If
        Catch ex As Exception
            Return ""
        End Try
        Return tempReturn
    End Function

    Private Shared Function getExchangeAmount(ByVal dAmount As Double, _
                                        ByVal base_curr_code As String, _
                                        ByVal from_curr_code As String, _
                                        ByVal to_curr_code As String, _
                                        ByRef varErrorString As Object) As Double

        Dim oExcDom As New System.Xml.XmlDocument
        Dim exchroot As System.Xml.XmlElement
        Dim exchRecord As String
        Dim exchAmount As Double

        Dim varLocalErrorString
        Dim sErrorString

        Try
            If dAmount = 0 Then
                getExchangeAmount = 0
                Exit Function
            End If


            If from_curr_code = to_curr_code Then
                Return dAmount
            End If

            ''base currency which associate with the local amount
            ''it must be the user local currency
            ''chargeCurrency = oAuroraDAL.getRecord("dbo.paym_GetCurrencyDesc '" & baseCurrency & "'")
            base_curr_code = Trim(base_curr_code)
            from_curr_code = Trim(from_curr_code)
            to_curr_code = Trim(to_curr_code)
            ''exchRecord = oAuroraDAL.getRecord(AURORA_DBSTOREDPROC_ADMCURRENCYCONVERTER & " " & CStr(dAmount) & ", '" & base_curr_code & "', '" & from_curr_code & "', '" & to_curr_code & "'")
            exchRecord = Aurora.Common.Data.ExecuteScalarSP("ADM_CurrencyConverter", CStr(dAmount), base_curr_code, from_curr_code, to_curr_code)

            ' Check for valid xml
            Try
                oExcDom.LoadXml(exchRecord)
            Catch ex As Xml.XmlException
                varLocalErrorString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varLocalErrorString)
                varErrorString = "<Root>" & sErrorString & "<Data></Data></Root>"
                Exit Function
            End Try

            exchroot = oExcDom.DocumentElement

            If exchroot.Name = "Error" Then
                varLocalErrorString = Aurora.Booking.Services.BookingPaymentUtility.getMessageFromDB("GEN023")
                sErrorString = Aurora.Booking.Services.BookingPaymentUtility.GetErrorTextFromResource(True, String.Empty, "Application", varLocalErrorString)
                varErrorString = "<Root>" & sErrorString & "<Data>" & oExcDom.SelectSingleNode("Error/ErrDesc").InnerText & "</Data></Root>"
                Exit Function
            End If

            exchAmount = CDbl(exchroot.SelectSingleNode("//ConRate").InnerText)
            getExchangeAmount = exchAmount
        Catch ex As Exception
            getExchangeAmount = 0
            varErrorString = ex.Message
        Finally
            exchroot = Nothing
        End Try

    End Function

#End Region

#Region "rev:mia Dec 6 2010 - Manual payment source History and Payment tab identification."
    Public Shared Function UpdatePaymentToStatusToOpen(ByVal paymentid As String, ByVal userid As String) As Boolean
        ''rev:mia Dec 6 2010 - Manual payment source History and Payment tab identification.
        Try
            Dim result As Object = Aurora.Common.Data.ExecuteScalarSP("EFTPOS_updatePaymentToStatusToOpen", paymentid, userid)
            Return CBool(result)
        Catch ex As Exception
            Logging.LogError("PAYMENT TAB CONTROL SCREEN: UpdatePaymentToStatusToOpen: ", ex.Message & vbCrLf & ex.StackTrace)
            Return False
        End Try
    End Function
#End Region
End Class
