Imports Aurora.Booking.Data
Imports Aurora.Common

Public Class Agent

    Public Shared Function GetAgentContact(ByVal agentId As String) As DataSet
        Dim ds As DataSet = New DataSet
        ds = DataRepository.GetAgentContact(agentId)
        Return ds

    End Function


    Public Shared Function UpdateAgentContacts(ByVal agentId As String, ByVal userCode As String, ByVal programName As String, ByVal col As Collection) As String


        For Each l As ArrayList In col

            Dim contactId As String = l.Item(0)
            Dim typeId As String = l.Item(1)
            Dim lName As String = l.Item(2)
            Dim fName As String = l.Item(3)
            Dim title As String = l.Item(4)
            Dim position As String = l.Item(5)
            Dim comment As String = l.Item(6)
            Dim integrityNo As Integer = Utility.ParseInt(l.Item(7), 0)
            Dim remove As Boolean = Convert.ToBoolean(l.Item(8))
            Dim emailPhnId As String = l.Item(9)
            Dim emailId As String = l.Item(10)
            Dim email As String = l.Item(11)
            Dim emailIntegrityNo As Integer = Utility.ParseInt(l.Item(12), 0)
            Dim faxPhnId As String = l.Item(13)
            Dim faxId As String = l.Item(14)
            Dim faxACode As String = l.Item(15)
            Dim faxCCode As String = l.Item(16)
            Dim faxNo As String = l.Item(17)
            Dim faxIntegrityNo As Integer = Utility.ParseInt(l.Item(18), 0)
            Dim phonePhnId As String = l.Item(19)
            Dim phoneId As String = l.Item(20)
            Dim phoneACode As String = l.Item(21)
            Dim phoneCCode As String = l.Item(22)
            Dim phoneNo As String = l.Item(23)
            Dim phoneIntegrityNo As Integer = Utility.ParseInt(l.Item(24), 0)

            'Dim contactId As String = l.Item(0)
            'Dim phoneId As String = l.Item(1)
            'Dim typeId As String = l.Item(2)
            'Dim phoneNumber As String = l.Item(3)
            'Dim email As String = l.Item(4)
            'Dim faxId As String = l.Item(5)
            'Dim faxNumber As String = l.Item(6)
            'Dim emailIntegrityNo As Integer = Utility.ParseInt(l.Item(7), 0)
            'Dim isRemove As Boolean = Convert.ToBoolean(l.Item(8))


            '0<ContactId>B8117C033AD040F0BC78F27FEB0C88DC</ContactId>
            '1<TypeId>53EB5FA6BA564964B520BE458F6594E1</TypeId>
            '2<LName>Leong</LName>
            '3<FName>Jack</FName>
            '4<Title>Mr</Title>
            '5<Position></Position>
            '6<Comment></Comment>
            '7<IntegrityNo>2</IntegrityNo>
            '8<Remove>0</Remove
            '9<EmailPhnId>A5FB77E275B847AFA00C0456DDAA96B8</EmailPhnId>
            '10<EmailId>A2634A50296946A4BFAAA77056AEB634</EmailId>
            '11<Email></Email>
            '12<EmailIntegrityNo>3</EmailIntegrityNo>
            '13<FaxPhnId>A51EC9BF9FDB482490CC11D36A93BEC6</FaxPhnId>
            '14<FaxId>E17DA52692AE4488B5E562EAC43D2D29</FaxId>
            '15<FaxACode></FaxACode>
            '16<FaxCCode></FaxCCode>
            '17<FaxNo></FaxNo>
            '18<FaxIntegrityNo>3</FaxIntegrityNo>
            '19<PhonePhnId>9D27472F18B44283BFF15B622B8FEAB0</PhonePhnId>
            '20<PhoneId>4AF1D888FF0C4538BA1465374D294308</PhoneId>
            '21<PhoneACode>1</PhoneACode>
            '22<PhoneCCode>2</PhoneCCode>
            '23<PhoneNo>3</PhoneNo>
            '24<PhoneIntegrityNo>3</PhoneIntegrityNo>

            'Dim title As String
            'Dim fName As String
            'Dim lName As String
            'Dim position As String
            'Dim comment As String
            'Dim integrityNo As Integer
            'Dim programName As String
            'Dim userCode As String
            Dim returnError As String = ""

            Dim action As String = "M"
            If String.IsNullOrEmpty(contactId) Then
                action = "I"
            ElseIf remove Then
                action = "D"
            End If

            'JL The validation of the current Aurora is not working.
            'DataRepository.ValidatePhoneNumber(action, "Contact", agentId, typeId, phoneNumber, email, returnError)
            'DataRepository.ValidatePhoneNumber(action, "Contact", agentId, faxId, faxNumber, email, returnError)
            'DataRepository.ValidatePhoneNumber(action, "Contact", agentId, email, "", emailIntegrityNo, returnError)

            'Save contact
            DataRepository.UpdateContact(action, contactId, "Agent", agentId, typeId, title, fName, lName, position, _
                comment, integrityNo, userCode, programName, returnError)

            If Not String.IsNullOrEmpty(returnError) Then
                If returnError.Contains("GEN000/") And String.IsNullOrEmpty(contactId) Then
                    contactId = returnError.Substring(7)
                ElseIf returnError <> "SUCCESS" Then
                    Return returnError
                End If
            End If

            'Save phone
            DataRepository.UpdatePhoneNumber(action, phonePhnId, "Contact", contactId, phoneId, phoneACode, _
                phoneCCode, phoneNo, phoneIntegrityNo, "", userCode, programName, returnError)

            If Not String.IsNullOrEmpty(returnError) Then
                Return returnError
            End If

            'Save fax
            DataRepository.UpdatePhoneNumber(action, faxPhnId, "Contact", contactId, faxId, faxACode, _
               faxCCode, faxNo, faxIntegrityNo, "", userCode, programName, returnError)

            If Not String.IsNullOrEmpty(returnError) Then
                Return returnError
            End If
            'Save email
            DataRepository.UpdatePhoneNumber(action, emailPhnId, "Contact", contactId, emailId, "", "", _
               "", emailIntegrityNo, email, userCode, programName, returnError)
            If Not String.IsNullOrEmpty(returnError) Then
                Return returnError
            End If



        Next

        Return ""





    End Function



End Class
