﻿''rev:mia Oct 16 2014 - addition of PromoCode
Imports Aurora.Reservations.Services
Imports Aurora.Booking.Data
Imports Aurora.Common
Imports System.Xml
Imports System.Text
Imports Aurora.Common.Data

<Serializable()>
Public Class QuickAvailParameter
    Public branchOut As String
    Public branchIn As String
    Public dteCKO As String
    Public dteCKI As String
    Public product As String
    Public countrytext As String
    Public agentId As String
    Public bkrid As String
    Public hirePeriod As String
    Public Booid As String
    Public userCode As String
    Public Newvhrid As String
    Public Newbkrid As String
    Public programName As String
End Class

Friend Class BookingQuickAvailDataRepository

    Public Shared Function GetBkrID(BookingID As String, BookingNumber As String) As String
        Dim bkrid As String = String.Empty
        Try

            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("booid", DbType.String, BookingID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("boonum", DbType.String, IIf(String.IsNullOrEmpty(BookingNumber), Nothing, BookingNumber), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(2) = New Aurora.Common.Data.Parameter("BkrId", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

            Aurora.Common.Data.ExecuteOutputSP("BookingQuickAvail_GetBkrID", params)
            bkrid = params(2).Value

        Catch ex As Exception
            bkrid = String.Empty
        End Try

        Return bkrid
    End Function

End Class

Public Class BookingQuickAvail

    Private _xmlstringAvailable As String


    Private _GetWarningMessage As String

    Public ReadOnly Property WarningMessage As String
        Get
            Return _GetWarningMessage
        End Get
    End Property

    Private _GetAllMessages As String
    Public ReadOnly Property GetAllMessages As String
        Get
            Return _GetAllMessages
        End Get
    End Property

    Private _ErrorMessages As String
    Public ReadOnly Property ErrorMessages As String
        Get
            Return _ErrorMessages
        End Get
    End Property

    Private ReadOnly Property GetCountry(branchout As String) As String
        Get
            Dim qrytext As String = "select dbo.getCountryforlocation('" & branchout & "',NULL,NULL)"
            Dim countrytext As String = Aurora.Common.Data.ExecuteScalarSQL(qrytext)
            Return countrytext
        End Get
    End Property

    Private _DisplayOtherVehicleOption As Boolean
    Public Property DisplayOtherVehicleOption As Boolean
        Get
            Return _DisplayOtherVehicleOption
        End Get
        Set(value As Boolean)
            _DisplayOtherVehicleOption = value
        End Set
    End Property


    Public Function CheckAvailability() As Boolean
        Dim xmlAvail As New XmlDocument
        CheckAvailability = True
        Dim GetAllMessages As New StringBuilder

        Dim blockingmessage As String = WarningMessage

        If Not String.IsNullOrEmpty(blockingmessage) Then
            blockingmessage = String.Concat("<br/>Message: <b>", blockingmessage)
         End If

        Try

            xmlAvail.LoadXml(_xmlstringAvailable)

            Dim avs As String = xmlAvail.SelectSingleNode("//Data/AvailableVehicle/NumberOfVehicles").InnerText

            If avs.Equals("BYPASS") = True Then
                GetAllMessages.Append("Available: <b>YES</b></br>This vehicle is available as a <b><u>BYPASS</u></b>")
                CheckAvailability = True
            Else
                ''if avs is empty then possible that there is no record
                If String.IsNullOrEmpty(avs) Then
                    If (_DisplayOtherVehicleOption = True) Then
                        GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage, "</br></br> Do you want to display other vehicles?"))
                    Else
                        GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage))
                    End If

                    CheckAvailability = False
                End If

                ''check if its a valid number
                Try
                    Dim intAvail As Integer = CInt(avs)
                    If intAvail > 1 Then
                        GetAllMessages.Append(String.Concat("Available: <b>YES</b></br>There are <b>", avs, "</b> available vehicles.", blockingmessage))
                        CheckAvailability = True

                    ElseIf intAvail = 1 Then
                        GetAllMessages.Append(String.Concat("Available: <b>YES</b></br>There is <b>", avs, "</b> available vehicle.", blockingmessage))
                        CheckAvailability = True
                    Else
                        If (_DisplayOtherVehicleOption = True) Then
                            GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage, "</br></br> Do you want to display other vehicles?"))
                        Else
                            GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage))
                        End If

                        CheckAvailability = False
                    End If
                Catch ex As Exception
                    If (_DisplayOtherVehicleOption = True) Then
                        GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage, "</br></br> Do you want to display other vehicles?"))
                    Else
                        GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage))
                    End If
                    CheckAvailability = False
                End Try
            End If


        Catch ex As Exception
            If (_DisplayOtherVehicleOption = True) Then
                GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", blockingmessage, "</br></br> Do you want to display other vehicles?"))
            Else
                blockingmessage = String.Concat("<br/>Message: <b>", ex.Message, "</b>")
                GetAllMessages.Append(String.Concat("Available: <b>NO</b></br>", ""))
            End If
            CheckAvailability = False
            Logging.LogInformation("CheckAvailability ERROR: ", _xmlstringAvailable & ", ERROR " & blockingmessage)
        End Try

        If (_DisplayOtherVehicleOption = True) Then
            _GetAllMessages = GetAllMessages.ToString
        Else
            _GetAllMessages = GetAllMessages.ToString.Insert(0, "<br/>")
        End If


        Logging.LogInformation("CheckAvailability: ", _xmlstringAvailable & vbCrLf & vbCrLf & _GetAllMessages)
        Return CheckAvailability
    End Function

    Sub New(oQuickAvailParameter As QuickAvailParameter)
        Me.New(oQuickAvailParameter.bkrid, _
               oQuickAvailParameter.Newvhrid, _
               oQuickAvailParameter.branchOut, _
               oQuickAvailParameter.branchIn, _
               oQuickAvailParameter.dteCKO, _
               oQuickAvailParameter.dteCKI, _
               oQuickAvailParameter.hirePeriod, _
               oQuickAvailParameter.product, _
               oQuickAvailParameter.Booid.Split("-")(0).Trim, _
               oQuickAvailParameter.Booid.Split("-")(1).Trim, _
               oQuickAvailParameter.userCode, _
               oQuickAvailParameter.programName, _
               oQuickAvailParameter.agentId)
    End Sub

    Private _bkrid As String
    Public ReadOnly Property BkrIdCurrent As String
        Get
            Return _bkrid
        End Get
    End Property

    Private _vhrid As String
    Public ReadOnly Property VhrId As String
        Get
            Return _vhrid
        End Get
    End Property

    Private _countrytext As String
    Public ReadOnly Property CountryToSearch As String
        Get
            Return _countrytext
        End Get
    End Property

    Private _product As String
    Public ReadOnly Property ProductId As String
        Get
            Return _product
        End Get
    End Property




    Sub New(bkrid As String, _
            vhrid As String, _
            branchOut As String, _
            branchIn As String, _
            dteCKO As String, _
            dteCKI As String,
            hireperiod As String, _
            vehicle As String, _
            booid As String, _
            boonumber As String, _
            usercode As String, _
            programname As String, _
            agentId As String)


        _countrytext = Me.GetCountry(branchOut)
        _product = Aurora.Booking.Services.BookingProcess.GetProductID(vehicle, usercode)

        Dim newrequest As String = NewRequestXml(vhrid, _
                                                 bkrid, _
                                                 agentId, _
                                                 _product, _
                                                 branchOut, _
                                                 dteCKO, _
                                                 branchIn, _
                                                 dteCKI, _
                                                 hireperiod, _
                                                 usercode)

        Dim strResult As String = Aurora.Booking.Services.BookingRequest.ManageBookingRequest(newrequest)

        If Not String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnError) Then
            _ErrorMessages = Aurora.Booking.Services.BookingRequest.ReturnError
        End If

        If Not String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnWarning) Then
            _GetWarningMessage = Aurora.Booking.Services.BookingRequest.ReturnWarning
        End If

        Dim xpath As String = "Op/D/Root/NewBookingRequest/"
        Dim xmlLoadNewResult As New XmlDocument
        xmlLoadNewResult.LoadXml(strResult)

        _bkrid = xmlLoadNewResult.SelectSingleNode(xpath & "BkrId").InnerText
        _vhrid = xmlLoadNewResult.SelectSingleNode(xpath & "VhrId").InnerText


        _xmlstringAvailable = ManageAvailability.GetAvailabilityWithCost _
                                                 (_bkrid, _
                                                  branchOut, _
                                                  dteCKO, _
                                                  "PM", _
                                                  branchIn, _
                                                  dteCKI, _
                                                  "PM", _
                                                  hireperiod, _
                                                  _product, _
                                                  "0", _
                                                  booid, _
                                                   "", _
                                                   String.Empty, _
                                                   String.Empty, _
                                                  String.Empty, _
                                                  String.Empty, _
                                                  usercode, _
                                                  programname, _
                                                  _countrytext, _
                                                   String.Empty, _
                                                  True)
        Logging.LogDebug("BookingQuickAvail constructor", _xmlstringAvailable)

    End Sub


    Private Function NewRequestXml(bkrId As String, _
                                   vhrid As String, _
                                   AgnCode As String, _
                                   ProductId As String, _
                                   CKOLocation As String, _
                                   CKODate As String, _
                                   CKILocation As String, _
                                   CKIDate As String, _
                                   HirePeriod As String, _
                                   CurrentUserCode As String) As String

        Dim sbNewRequest As New StringBuilder
        Try

            With sbNewRequest
                .Append("<Root>")
                .Append("<NewBookingRequest>")
                .AppendFormat("<BooId>{0}</BooId>", String.Empty)
                .AppendFormat("<BkrId>{0}</BkrId>", bkrId)
                .AppendFormat("<VhrId>{0}</VhrId>", vhrid)
                .AppendFormat("<AgnCode>{0}</AgnCode>", AgnCode)
                .AppendFormat("<ConCode>{0}</ConCode>", String.Empty)
                .AppendFormat("<VhrAgnRef>{0}</VhrAgnRef>", String.Empty)
                .AppendFormat("<VhrThirdPartyRef>{0}</VhrThirdPartyRef>", String.Empty)
                .Append("<SapId/>")
                .AppendFormat("<PrdId>{0}</PrdId>", ProductId)
                .AppendFormat("<BrdCode>{0}</BrdCode>", String.Empty)
                .AppendFormat("<ClaId>{0}</ClaId>", String.Empty)
                .AppendFormat("<PkgCode>{0}</PkgCode>", String.Empty)
                .AppendFormat("<PkgId>{0}</PkgId>", String.Empty)
                .AppendFormat("<TypeId>{0}</TypeId>", String.Empty)
                .AppendFormat("<TypeId>{0}</TypeId>", String.Empty)
                .AppendFormat("<IntegrityNo>{0}</IntegrityNo>", String.Empty)
                .AppendFormat("<MiscAgentName>{0}</MiscAgentName>", String.Empty)
                .AppendFormat("<MiscAgentContact>{0}</MiscAgentContact>", String.Empty)
                .AppendFormat("<NumberOfVehicles>{0}</NumberOfVehicles>", 1)
                .AppendFormat("<NumberOfAdults>{0}</NumberOfAdults>", 1)
                .AppendFormat("<NumberOfChildren>{0}</NumberOfChildren>", 0)
                .AppendFormat("<NumberOfInfants>{0}</NumberOfInfants>", 0)
                .AppendFormat("<ckoLocation>{0}</ckoLocation>", CKOLocation)
                .AppendFormat("<ckoDate>{0}</ckoDate>", CKODate)
                .AppendFormat("<ckoDayPart>{0}</ckoDayPart>", "PM")
                .AppendFormat("<ckiLocation>{0}</ckiLocation>", CKILocation)
                .AppendFormat("<ckiDate>{0}</ckiDate>", CKIDate)
                .AppendFormat("<ckiDayPart>{0}</ckiDayPart>", "PM")
                .AppendFormat("<HirePeriod>{0}</HirePeriod>", HirePeriod)
                .AppendFormat("<HireUOM>{0}</HireUOM>", 1)
                .AppendFormat("<LastName>{0}</LastName>", "QuickAvail")
                .AppendFormat("<FirstName>{0}</FirstName>", "")
                .AppendFormat("<Title>{0}</Title>", "")
                .AppendFormat("<RequestSource>{0}</RequestSource>", String.Empty)
                .AppendFormat("<ButClick>{0}</ButClick>", "Display Availability With Cost")
                ''rev:mia Oct 16 2014 - addition of PromoCode
                .AppendFormat("<PromoCode>{0}</PromoCode>", String.Empty)

                .Append("</NewBookingRequest>")
                .AppendFormat("<Aurora><AddModUserId>{0}</AddModUserId></Aurora>", CurrentUserCode)
                .AppendFormat("<Aurora><AddProgName>{0}</AddProgName></Aurora>", "Booking.aspx")
                .Append("</Root>")

            End With



        Catch ex As Exception
            Logging.LogException("NewRequestXml", ex)
        End Try
        Return sbNewRequest.ToString
    End Function

End Class
