﻿Imports System.Xml
Imports System.Text
Imports Aurora.Common
Imports PhoenixWebService
Imports System.Configuration

Namespace Aurora.Booking.Alternate

    Public Class AvailabilityVehicles
        Implements IGetAlternateAvailability

        Public Function InitializedEmptyAlternateVehicle() As String Implements IGetAlternateAvailability.InitializedEmptyAlternateVehicle
            Return ""
        End Function

        Private Sub LogInformation(ByVal functionName As String, ByVal TextToLog As String)
            Aurora.Common.Logging.LogInformation(vbCrLf & vbCrLf & "AvailabilityVehicles inside aurora".ToUpper, String.Concat(vbCrLf, vbCrLf, MessageInformationFormat(functionName, TextToLog)))
        End Sub

        Private Function MessageInformationFormat(ByVal methodName As String, ByVal ParamArray strmessage As String()) As String
            Dim sb As New StringBuilder
            Dim skipfirstHeaderInlog As Boolean = False

            Try
                If (strmessage.Length = 1) Then
                    skipfirstHeaderInlog = True
                End If
            Catch ex As Exception
                skipfirstHeaderInlog = False
            End Try


            If (Not skipfirstHeaderInlog) Then
                sb.AppendFormat("{0}{1}{2}{3}{4}{5}", vbCrLf, vbCrLf, New String("-", 30), "STARTING", New String("-", 30), vbCrLf)
                sb.Append(methodName.ToUpper)
                sb.AppendFormat("{0}{1}{2}", vbCrLf, New String("-", 68), vbCrLf & vbCrLf)

            End If

            Dim ctr As Integer = 0
            For Each strvalue As String In strmessage
                If Not String.IsNullOrEmpty(strvalue) Then
                    strvalue = strvalue.Trim
                    strvalue = strvalue.PadRight(30)
                    If ctr = 0 Then
                        sb.AppendFormat("{0}:", strvalue)
                        ctr = ctr + 1
                    Else
                        sb.AppendFormat("{0}{1}", strvalue, vbCrLf)
                        ctr = 0
                    End If
                End If
            Next
            If (Not skipfirstHeaderInlog) Then
                ''sb.AppendFormat("{0}{1}{2}", vbCrLf, "----------------END-------------", vbCrLf & vbCrLf)
                sb.AppendFormat("{0}{1}{2}{3}{4}{5}{6}{7}", vbCrLf, vbCrLf, New String("-", 30), "END HERE", New String("-", 30), vbCrLf, vbCrLf, vbCrLf)
            End If

            Return sb.ToString
        End Function

#Region "AURORA ALTERNATE AVAILABILITY"
        Private Function AlternateVehicleInterface(xml As System.Xml.XmlDocument) As String
            Return ProcessAlternateVehicleList(xml)
        End Function


        Function ProcessAlternateVehicleList(ByVal vehicle As XmlDocument) As String

            LogInformation("Aurora.GetAlternateAvailabilities", MessageInformationFormat("ProcessAlternateVehicleList", _
                                                                                    "XML", vehicle.OuterXml))


            Const MSG_EMPTY As String = "ERROR: Your request doesn't return any data. Please try again."
            Const MSG_ERROR As String = "ERROR: Error was encountered. Please try again."
            Const MSG_NOFOUND As String = "<h3><center><font color='red'>No record of alternate availability.You might change the date/time/locations</font></center></h3>"
            ''Const TD_ELEMENT As String = "<td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td>"
            Const TD_ELEMENT As String = "<td>{0}</td><td>{1}</td><td>{2}</td>"
            Const TD_HEADER As String = "<td><b>{0}</b></td>"
            Const TABLE_HEADER As String = "<table border='1' width='100%;'><tr><td><b>{0}</b></td>"
            ''Const TD_STYLE As String = "border-right-width: 0px; width: 100px; border-top-width: 1px; border-bottom-width: 1px; border-left-width: 0px;"
            Const TD_OLDHEADER As String = "<tr><td width='100'>{0}</td><td width='150'>{1}</td><td width='100' align='right'>{2}</td><td width='100'>{3}</td><td width='100' align='right'>{4}</td><td width='100'>{5}</td></tr>"
            Const TD_OLDGAP As String = "<tr><td height='10'/></tr>"

            Dim ckoOrig As String = String.Concat("<b><label id='OriginalCheckOutLocationandDate'>", vehicle.DocumentElement.Attributes("cko").Value, "</label></b>")
            Dim ckiOrig As String = String.Concat("<b><label id='OriginalCheckInLocationandDate'>", vehicle.DocumentElement.Attributes("cki").Value, "</label></b>")

            Dim sbRoot As New StringBuilder
            ''sbRoot.Append("<table bgColor='#ddffdd'>")
            sbRoot.Append("<table>")
            sbRoot.Append(TD_OLDGAP)
            sbRoot.AppendFormat(TD_OLDHEADER, "Initial Search:".ToUpper, "<b><label id='PreviousPattern' /></b>", "FROM:", ckoOrig, "TO:", ckiOrig)
            
            sbRoot.Append(TD_OLDGAP)
            sbRoot.Append("</table>")
            sbRoot.AppendFormat(TABLE_HEADER, "Search Pattern".ToUpper)
            sbRoot.AppendFormat(TD_HEADER, "Availability".ToUpper)
            'sbRoot.AppendFormat(TD_HEADER, "Message".ToUpper)
            sbRoot.AppendFormat(TD_HEADER, "Select".ToUpper)
            sbRoot.Append("</tr>")

            Dim sb As New StringBuilder
            Dim hasFound As Boolean = False
            Try
                If vehicle.OuterXml.Equals("<data/>") = True Then
                    Return HtmlError(MSG_EMPTY)
                End If

                If vehicle.ChildNodes.Count = 0 Then
                    Return HtmlError(MSG_EMPTY)
                End If

                Dim renamed As String = ""

                If vehicle.SelectSingleNode("data").ChildNodes.Count - 1 >= 0 Then


                    Dim availablerate As XmlElement = Nothing
                    For Each availablerate In vehicle.SelectSingleNode("data").ChildNodes

                        sbRoot.Append("<tr>")
                        For Each content As XmlElement In availablerate.ChildNodes
                            If (content.Name = "Package") Then

                                If Not availablerate.Attributes("searchPattern") Is Nothing AndAlso Not availablerate.Attributes("rqGuid") Is Nothing Then
                                    renamed = availablerate.Attributes("searchPattern").InnerText
                                    Select Case availablerate.Attributes("searchPattern").InnerText
                                        Case "MoveForward"
                                            renamed = "Later PickUp Date"
                                            Exit Select
                                        Case "MoveBackWard"
                                            renamed = "Earlier PickUp Date"
                                            Exit Select
                                        Case "SwitchLocation"
                                            renamed = "Reverse Route"
                                            Exit Select
                                        Case "PickUpBased"
                                            renamed = "Fixed PickUp Date"
                                            Exit Select
                                        Case "DropOffBased"
                                            renamed = "Fixed DropOff Date"
                                            Exit Select
                                        Case "OnewaySwitchLocationOnPickup"
                                            renamed = "Return to PickUp Location"
                                            Exit Select
                                    End Select
                                End If ''If Not availablerate.Attributes("searchPattern") Is Nothing AndAlso Not availablerate.Attributes("rqGuid") Is Nothing Then

                                Dim AvpId As String = String.Empty
                                Dim PickupDate As String = String.Empty
                                Dim DropOffDate As String = String.Empty

                                If Not availablerate("Package").Attributes("AvpId") Is Nothing Then
                                    AvpId = availablerate("Package").Attributes("AvpId").Value
                                    If Not String.IsNullOrEmpty(AvpId) Then
                                        hasFound = True
                                        PickupDate = Convert.ToDateTime(availablerate("Package").Attributes("PickupDate").Value).ToString("dd/MM/yyyy")
                                        DropOffDate = Convert.ToDateTime(availablerate("Package").Attributes("DropOffDate").Value).ToString("dd/MM/yyyy")

                                        Dim checkbox As String = "&nbsp;"
                                        Dim combinedDate As String = String.Concat(PickupDate, "|", DropOffDate)
                                        checkbox = String.Concat("<input type='radio' id='chk", renamed.Replace(" ", ""), "' value='", combinedDate, "' class='alternateavailbility' name='alternate'/>")
                                        '' sbRoot.AppendFormat(TD_ELEMENT, renamed, PickupDate & " to " & DropOffDate, "FOUND", checkbox)
                                        sbRoot.AppendFormat(TD_ELEMENT, renamed, PickupDate & " to " & DropOffDate, checkbox)
                                    End If

                                End If

                            End If

                        Next
                        sbRoot.Append("</tr>")

                    Next

                End If
                sbRoot.Append("</table>")

                'If (sbRoot.ToString.Contains("FOUND") = False) Then
                '    sbRoot = New StringBuilder
                '    sbRoot.Append(MSG_NOFOUND)
                'End If

                If (Not hasFound) Then
                    sbRoot = New StringBuilder
                    sbRoot.Append(MSG_NOFOUND)
                End If
            Catch ex As Exception
                Logging.LogException("ProcessAlternateVehicleList", ex)
                Return HtmlError(MSG_ERROR)
            End Try

            LogInformation("Aurora.GetAlternateAvailabilities", MessageInformationFormat("ProcessAlternateVehicleList", _
                                                                                   "HTML", sbRoot.ToString))
            Return sbRoot.ToString

        End Function


        Public Function GetAlternateAvailabilities(VehInfo As Aurora.Booking.Alternate.VehicleInformation, _
                                                   PasInfo As Aurora.Booking.Alternate.PassengerInformation, _
                                                   DisplayMode As String) As String Implements IGetAlternateAvailability.GetAlternateAvailabilities



            If String.IsNullOrEmpty(VehInfo.Brand) Then
                VehInfo.Brand = GetBrand(VehInfo.VehicleCode, VehInfo.CountryCode)
            End If

            'LogInformation("Aurora.GetAlternateAvailabilities", MessageInformationFormat("GetAlternateAvailabilities RQ", _
            '                                                                         "VehicleCode", IIf(String.IsNullOrEmpty(VehInfo.VehicleCode), "NIL", VehInfo.VehicleCode), _
            '                                                                         "CountryCode", IIf(String.IsNullOrEmpty(VehInfo.CountryCode), "NIL", VehInfo.CountryCode), _
            '                                                                         "Brand", IIf(String.IsNullOrEmpty(VehInfo.Brand), "NIL", VehInfo.Brand), _
            '                                                                         "CheckOutZoneCode", IIf(String.IsNullOrEmpty(VehInfo.CheckOutZoneCode), "NIL", VehInfo.CheckOutZoneCode), _
            '                                                                         "CheckOutDateTime", IIf(String.IsNullOrEmpty(VehInfo.CheckOutDateTime), "NIL", VehInfo.CheckOutDateTime), _
            '                                                                         "CheckInZoneCode", IIf(String.IsNullOrEmpty(VehInfo.CheckInZoneCode), "NIL", VehInfo.CheckInZoneCode), _
            '                                                                         "CheckInDateTime", IIf(String.IsNullOrEmpty(VehInfo.CheckInDateTime), "NIL", VehInfo.CheckInDateTime), _
            '                                                                         "AgentCode", IIf(String.IsNullOrEmpty(VehInfo.AgentCode), "NIL", VehInfo.AgentCode), _
            '                                                                         "PackageCode", IIf(String.IsNullOrEmpty(VehInfo.PackageCode), "NIL", VehInfo.PackageCode), _
            '                                                                         "IsVan", IIf(String.IsNullOrEmpty(VehInfo.IsVan), "NIL", VehInfo.IsVan), _
            '                                                                         "DisplayMode", DisplayMode))

            Dim Returnxml As String = String.Empty
            Dim xmlresult As New XmlDocument

            Try
                ''Debug.WriteLine("start:" & DateTime.Now.ToLongTimeString)
                Dim phoenix As New PhoenixWebService
                ''phoenix.Timeout = 120000 ''two mintes
                phoenix.Timeout = CInt(ConfigurationManager.AppSettings("PhoenixWSTimeout").ToString) * 60000
                Returnxml = phoenix.GetAlternateAvailability(VehInfo.VehicleCode, _
                                                                                VehInfo.CountryCode, _
                                                                                VehInfo.Brand, _
                                                                                VehInfo.CheckOutZoneCode, _
                                                                                VehInfo.CheckOutDateTime, _
                                                                                VehInfo.CheckInZoneCode, _
                                                                                VehInfo.CheckInDateTime, _
                                                                                VehInfo.AgentCode, _
                                                                                VehInfo.PackageCode, _
                                                                                VehInfo.IsVan, _
                                                                                PasInfo.NoOfAdults, _
                                                                                PasInfo.NoOfChildren, _
                                                                                False, _
                                                                                PasInfo.CountryOfResidence, _
                                                                                DisplayMode, _
                                                                                False, _
                                                                                False).OuterXml

                ''Debug.WriteLine("end:" & DateTime.Now.ToLongTimeString)
                phoenix = Nothing
                xmlresult.LoadXml(Returnxml)

                Dim attr As XmlAttribute = xmlresult.CreateAttribute("cki")
                attr.Value = VehInfo.CheckInZoneCode & " - " & VehInfo.CheckInDateTime.ToShortDateString
                xmlresult.DocumentElement.Attributes.Append(attr)

                attr = xmlresult.CreateAttribute("cko")
                attr.Value = VehInfo.CheckOutZoneCode & " - " & VehInfo.CheckOutDateTime.ToShortDateString
                xmlresult.DocumentElement.Attributes.Append(attr)

                'LogInformation("Aurora.GetAlternateAvailabilities", MessageInformationFormat("GetAlternateAvailabilities RS", "Returnxml", Returnxml))

            Catch ex As Exception
                xmlresult.LoadXml("<data/>")
                Logging.LogException("Aurora.GetAlternateAvailabilities EXCEPTION", ex)
            End Try




            Return AlternateVehicleInterface(xmlresult)
        End Function


        Function GetBrand(Prdshortname As String, PkgCtyCode As String) As String
            Return Aurora.Common.Data.ExecuteScalarSP("Alternate_GetBrandCodeForVehicle", Prdshortname, PkgCtyCode)
        End Function

#End Region

#Region "BASED ON DAYS"
        Private Function HtmlError(errormsg As String) As String
            Dim sbError As New StringBuilder
            sbError.Append("<table border='1' width='100%'>")
            sbError.AppendFormat("<tr><td width='175px;'>Other Error</td><td width='850px;'  style='color:Red;' >{0}</td></tr>", errormsg)
            sbError.Append("</table>")
            Return sbError.ToString
        End Function

        Public Function GetAvailabilitiesBasedOnDays(VehInfo As Aurora.Booking.Alternate.VehicleInformation, _
                                                   PasInfo As Aurora.Booking.Alternate.PassengerInformation _
                                                   ) As String
            If String.IsNullOrEmpty(VehInfo.Brand) Then
                VehInfo.Brand = GetBrand(VehInfo.VehicleCode, VehInfo.CountryCode)
            End If


            LogInformation("Aurora.GetAvailabilitiesBasedOnDays", MessageInformationFormat("GetAlternateAvailabilities RQ", _
                                                                                     "VehicleCode", IIf(String.IsNullOrEmpty(VehInfo.VehicleCode), "NIL", VehInfo.VehicleCode), _
                                                                                     "CountryCode", IIf(String.IsNullOrEmpty(VehInfo.CountryCode), "NIL", VehInfo.CountryCode), _
                                                                                     "Brand", IIf(String.IsNullOrEmpty(VehInfo.Brand), "NIL", VehInfo.Brand), _
                                                                                     "CheckOutZoneCode", IIf(String.IsNullOrEmpty(VehInfo.CheckOutZoneCode), "NIL", VehInfo.CheckOutZoneCode), _
                                                                                     "CheckOutDateTime", IIf(String.IsNullOrEmpty(VehInfo.CheckOutDateTime), "NIL", VehInfo.CheckOutDateTime), _
                                                                                     "CheckInZoneCode", IIf(String.IsNullOrEmpty(VehInfo.CheckInZoneCode), "NIL", VehInfo.CheckInZoneCode), _
                                                                                     "CheckInDateTime", IIf(String.IsNullOrEmpty(VehInfo.CheckInDateTime), "NIL", VehInfo.CheckInDateTime), _
                                                                                     "AgentCode", IIf(String.IsNullOrEmpty(VehInfo.AgentCode), "NIL", VehInfo.AgentCode), _
                                                                                     "PackageCode", IIf(String.IsNullOrEmpty(VehInfo.PackageCode), "NIL", VehInfo.PackageCode), _
                                                                                     "IsVan", IIf(String.IsNullOrEmpty(VehInfo.IsVan), "NIL", VehInfo.IsVan) _
                                                                                     ))

            Dim Returnxml As String = String.Empty
            Dim xmlresult As New XmlDocument
            Try
                Dim phoenix As New PhoenixWebService
                phoenix.Timeout = CInt(ConfigurationManager.AppSettings("PhoenixWSTimeout").ToString) * 60000
                Returnxml = phoenix.GetAvailabilitiesBasedOnDays(VehInfo.Brand, _
                                                                                VehInfo.CountryCode, _
                                                                                VehInfo.VehicleCode, _
                                                                                VehInfo.CheckOutZoneCode, _
                                                                                VehInfo.CheckOutDateTime, _
                                                                                VehInfo.CheckInZoneCode, _
                                                                                VehInfo.CheckInDateTime, _
                                                                                PasInfo.NoOfAdults, _
                                                                                PasInfo.NoOfChildren, _
                                                                                VehInfo.AgentCode, _
                                                                                VehInfo.PackageCode, _
                                                                                VehInfo.IsVan, _
                                                                                True, _
                                                                                VehInfo.CountryCode, _
                                                                                VehInfo.IsTestMode, _
                                                                                VehInfo.IsGross).OuterXml

                xmlresult.LoadXml(Returnxml)
                LogInformation("Aurora.GetAvailabilitiesBasedOnDays", MessageInformationFormat("GetAvailabilitiesBasedOnDays RS", "Returnxml", Returnxml))

            Catch ex As Exception
                xmlresult.LoadXml("<data/>")
                Logging.LogException("Aurora.GetAlternateAvailabilities EXCEPTION", ex)
            End Try

            Return AvailabilityBasedOnDays(xmlresult)
        End Function

        Function AvailabilityBasedOnDays(xml As XmlDocument) As String

            If xml Is Nothing Then Return "No result"
            Const xpathvehiclecode As String = "data/vehiclecode"
            Const AVAILROWS As String = "<td class='listofAvailabilities' align='center' style='border-style: solid;border-width: 1px;border-color: #BBBBBB;background-color:{0}' title='{1}'>{2}<br/>{3}<br/>{4}</td>"
            Const MONTHROWNOCOUNTER As String = "<td><font size='3'>{0}</font></td><td><font size='3'>{1}</font></font></td></tr><tr>"
            Const MONTHROW As String = "<tr><td><font size='3'>{0}</font></td><td><font size='3'>{1}</font></font></td></tr><tr>"
            Const XN_COLOR As String = "#dd9999"
            Const YF_COLOR As String = "#ccffcc"
            Const R_COLOR As String = "#ffeeee"
            Const DEF_COLOR As String = "white"
            Const N_COLOR As String = "#C8C8C8"
            Const TABLE_ELEMENT As String = "<table border='1' width='100%;'><tr>"
            Const TABLE_COLOR_LEGEND As String = "<table style='border-right-width: 1px; margin-top: 5px; width: 780px; border-top-width: 1px; border-bottom-width: 1px; border-left-width: 1px;'><tr align='center'><td style='width: 20%;'>KEY COLOR:</td><td style='width: 15%; background-color:{0}'>Available</td><td style='width: 15%; background-color:{1}'>Block</td><td style='width: 15%; background-color:{2}'>Unavailable</td><td style='width: 15%; background-color:{3}'>Pickup / Drop-Off</td></tr></table>"
            Const NBSP As String = "&nbsp;"

            Dim vehiclecode As String = xml.SelectSingleNode(xpathvehiclecode).Attributes("code").Value
            Dim locfrom As String = xml.SelectSingleNode(xpathvehiclecode).Attributes("from").Value
            Dim locto As String = xml.SelectSingleNode(xpathvehiclecode).Attributes("to").Value
            Dim origDatefrom As String = Convert.ToDateTime(xml.SelectSingleNode(xpathvehiclecode).Attributes("origDateFrom").Value).ToString("dd/MM/yyyy")
            Dim origDateto As String = Convert.ToDateTime(xml.SelectSingleNode(xpathvehiclecode).Attributes("origDateTo").Value).ToString("dd/MM/yyyy")
            Dim noofDays As String = xml.SelectSingleNode(xpathvehiclecode).Attributes("noofdays").Value


            Dim availabilitiesNodes As XmlNodeList = xml.SelectNodes(xpathvehiclecode & "/availability")


            Dim datefrom As String = String.Empty
            Dim dateto As String = String.Empty
            Dim mycolor As String = String.Empty
            Dim errormsg As String = NBSP
            Dim noofvehicle As String = "0"
            Dim avail As String = String.Empty

            Dim sb As New StringBuilder
            sb.AppendFormat(TABLE_COLOR_LEGEND, YF_COLOR, XN_COLOR, N_COLOR, DEF_COLOR)
            sb.Append(TABLE_ELEMENT)

            Dim optSelect As String = NBSP
            Dim counterForNewrow As Integer = 0
            Dim diffyear As Boolean = False
            Dim mos As String = String.Empty
            Dim yr As String = String.Empty
            Dim textToDisplay As String = String.Empty
            Dim blockingRuleMessage As String = String.Empty
            Dim sameData As Boolean = False

            For Each item As XmlNode In availabilitiesNodes

                datefrom = Convert.ToDateTime(item.Attributes("fr").Value).ToString("dd/MM/yyyy")

                avail = item.InnerText
                Select Case avail
                    Case "X", "N", ""
                        mycolor = XN_COLOR
                    Case "Y", "F"
                        mycolor = YF_COLOR
                    Case "R"
                        mycolor = R_COLOR
                End Select

                If (datefrom = origDatefrom Or datefrom = origDateto) Then
                    mycolor = DEF_COLOR
                    sameData = True
                Else
                    sameData = False
                End If


                If (String.IsNullOrEmpty(mos) Or mos <> Convert.ToDateTime(datefrom).ToString("MMM").ToUpper) Then
                    mos = Convert.ToDateTime(datefrom).ToString("MMM").ToUpper
                    yr = Convert.ToDateTime(datefrom).ToString("yyyy").ToUpper
                    sb.AppendFormat(IIf(counterForNewrow = 0, MONTHROWNOCOUNTER, MONTHROW), mos, yr)
                    counterForNewrow = 0
                ElseIf mos = Convert.ToDateTime(datefrom).ToString("MMM").ToUpper And counterForNewrow > 14 Then
                    sb.Append("</tr><tr>")
                    counterForNewrow = 0
                End If

                If (Not item.Attributes("no") Is Nothing) Then
                    noofvehicle = item.Attributes("no").Value
                End If

                If (Not item.Attributes("rule") Is Nothing) Then
                    blockingRuleMessage = item.Attributes("rule").Value
                End If

                If (counterForNewrow <= 14) Then

                    If (avail <> "N" And avail.Length > 0) Then
                        Dim combinedDate As String = String.Concat(datefrom, "|", dateto)
                        optSelect = String.Concat("<input type='radio' id='chk", avail, "' value='", combinedDate, "' class='listofAvailabilities' name='vehAvailabilities'/>")
                    Else
                        noofvehicle = "0"
                        optSelect = NBSP
                    End If

                    textToDisplay = Convert.ToDateTime(datefrom).Day
                    If (mycolor = DEF_COLOR) Then
                        textToDisplay = String.Concat("<b><font size='3' color='blue'>", textToDisplay, "</font></b>")
                    End If

                    If (Not String.IsNullOrEmpty(blockingRuleMessage)) Then
                        If (blockingRuleMessage.Contains("BLOCKINGRULE") = True And sameData = False) Then
                            mycolor = XN_COLOR
                        ElseIf blockingRuleMessage.Contains("TANDC") = True And sameData = False Then
                            If avail <> "N" Then
                                mycolor = YF_COLOR
                            Else
                                mycolor = N_COLOR
                            End If

                        End If
                        datefrom = String.Concat(datefrom, "|", blockingRuleMessage)
                    Else
                        datefrom = String.Concat(datefrom, "|", blockingRuleMessage)
                    End If

                    sb.AppendFormat(AVAILROWS, _
                                    mycolor, _
                                    datefrom, _
                                    optSelect, _
                                    textToDisplay, _
                                    IIf(noofvehicle <> "0", String.Concat("<font size='1'>", noofvehicle, " found</font>"), NBSP))
                    counterForNewrow = counterForNewrow + 1
                   

                Else
                    sb.Append("</tr><tr>")
                    counterForNewrow = 0
                End If
                blockingRuleMessage = String.Empty

            Next
            If (sb.ToString.EndsWith("</tr>") = False) Then
                sb.Append("</tr>")
            End If

            sb.Append("</table>")

            Return sb.ToString
        End Function
#End Region

      
    End Class

End Namespace
