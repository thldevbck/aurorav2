﻿Imports System.Xml
Namespace Aurora.Booking.Alternate

    Public Interface IGetAlternateAvailability
        Function GetAlternateAvailabilities(VehInfo As VehicleInformation, _
                                            PasInfo As PassengerInformation, _
                                            DisplayMode As String _
                                            ) As String
        Function InitializedEmptyAlternateVehicle() As String
        '' Function AlternateVehicleInterface(xml As XmlDocument) As String

    End Interface

End Namespace
