Imports System.Data.SqlClient

Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

''' <summary>
''' A <c>Package</c> holds information about the packages available for a specific booking week product.  
''' </summary>
Public Class Package

    Private _weekProductPackagesRow As WeekProductPackagesRow
    Public ReadOnly Property WeekProductPackagesRow() As WeekProductPackagesRow
        Get
            Return _weekProductPackagesRow
        End Get
    End Property

    Private _flexLevelDataTable As FlexLevelDataTable
    Public ReadOnly Property FlexLevelDataTable() As FlexLevelDataTable
        Get
            Return _flexLevelDataTable
        End Get
    End Property

    Private Sub Load()
        '' System.Diagnostics.Debug.WriteLine("PkgId: " & WeekProductPackagesRow.PkgId & ", SapId: " & WeekProductPackagesRow.SapId & ", TravelYear: " & WeekProductPackagesRow.TravelYear)
        _flexLevelDataTable = DataRepository.GetFlexLevels(WeekProductPackagesRow.PkgId, WeekProductPackagesRow.SapId, WeekProductPackagesRow.TravelYear)
    End Sub

    Public Sub New(ByVal weekProductPackagesRow As WeekProductPackagesRow)
        If weekProductPackagesRow Is Nothing Then
            Throw New ArgumentNullException()
        End If

        _weekProductPackagesRow = weekProductPackagesRow
        Load()

    End Sub

    Public Sub New(ByVal comCode As String, ByVal flwId As Integer, ByVal flpId As Integer, ByVal pkgId As String)
        If String.IsNullOrEmpty(comCode) OrElse String.IsNullOrEmpty(pkgId) Then
            Throw New ArgumentNullException()
        End If

        Dim weekProductPackagesDataTable As WeekProductPackagesDataTable = DataRepository.GetWeekProductPackage(comCode, flwId, flpId, pkgId)
        If weekProductPackagesDataTable Is Nothing Or weekProductPackagesDataTable.Rows.Count = 0 Then
            Throw New Exception("Package does not exist")
        End If

        _weekProductPackagesRow = weekProductPackagesDataTable(0)
        Load()
    End Sub

End Class
