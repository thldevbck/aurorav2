Imports System.Data.SqlClient
Imports System.Drawing

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

''' <summary>
''' A <c>BookingWeeks</c> aggregates travel years (2001,2002,2003 etc) and flex booking weeks for a 
''' specific company, and mocks non-existent FlexBookingWeekRows (status = "none") for the current and next 
''' booking period if they dont already exist.
''' </summary>
Public Class BookingWeeks
    Private _comCode As String
    Public ReadOnly Property ComCode() As String
        Get
            Return _comCode
        End Get
    End Property

    Private _currentBookStart As DateTime
    Public ReadOnly Property CurrentBookStart() As DateTime
        Get
            Return _currentBookStart
        End Get
    End Property

    Private _nextBookStart As DateTime
    Public ReadOnly Property NextBookStart() As DateTime
        Get
            Return _nextBookStart
        End Get
    End Property

    Private _currentFbwId As Integer
    Public ReadOnly Property CurrentFbwId() As Integer
        Get
            Return _currentFbwId
        End Get
    End Property

    Private _travelYearsDataTable As TravelYearsDataTable
    Public ReadOnly Property TravelYearsDataTable() As TravelYearsDataTable
        Get
            Return _travelYearsDataTable
        End Get
    End Property

    Private _flexBookingWeekDataTable As FlexBookingWeekDataTable
    Public ReadOnly Property FlexBookingWeekDataTable() As FlexBookingWeekDataTable
        Get
            Return _flexBookingWeekDataTable
        End Get
    End Property

    Public Sub New(ByVal comCode As String)
        If String.IsNullOrEmpty(comCode) Then
            Throw New ArgumentNullException()
        End If

        Dim today = Date.Now.Date

        ' get the travel years and booking dates
        _travelYearsDataTable = DataRepository.GetTravelYears()
        _flexBookingWeekDataTable = DataRepository.GetBookingWeeks(comCode)

        ' calculate the current booking start and next booking start from todays date
        _currentBookStart = today
        While CurrentBookStart.DayOfWeek <> DayOfWeek.Monday
            _currentBookStart = _currentBookStart - New TimeSpan(1, 0, 0, 0)
        End While
        _nextBookStart = _currentBookStart + New TimeSpan(7, 0, 0, 0)

        ' if they dont exist already, create fake booking week rows for the current and next booking weeks
        ' for all current and future travel years
        For Each bookStart As Date In New Date() {_currentBookStart, _nextBookStart}
            Dim found = False
            For Each bookingWeeksRow As FlexBookingWeekRow In _flexBookingWeekDataTable
                found = found OrElse bookingWeeksRow.FbwBookStart = bookStart
            Next
            If found Then Continue For ' booking week already exists

            ' calculate the travelYear start and travelYear end for the new booking week
            Dim travelYearStart As Date = Date.MinValue
            Dim travelYearEnd As Date = Date.MinValue
            For Each travelYearsRow As TravelYearsRow In _travelYearsDataTable
                ' ignore if the end of this travel year is before the end of this booking week e.g.
                ' this travel year is too old
                If travelYearsRow.TravelYear.AddYears(1) < bookStart.AddDays(7) Then Continue For

                If travelYearStart = Date.MinValue Then
                    travelYearStart = travelYearsRow.TravelYear
                End If
                travelYearEnd = travelYearsRow.TravelYear.AddYears(1).AddDays(-1)
            Next
            If travelYearStart = Date.MinValue Then Continue For ' yikes, no travel years found at all

            Dim newBookingWeeksRow As FlexBookingWeekRow = _flexBookingWeekDataTable.NewFlexBookingWeekRow()
            newBookingWeeksRow.FbwComCode = comCode
            newBookingWeeksRow.FbwId = IIf(bookStart = NextBookStart, -2, -1)
            newBookingWeeksRow.FbwBookStart = bookStart
            newBookingWeeksRow.FbwTravelYearStart = travelYearStart
            newBookingWeeksRow.FbwTravelYearEnd = travelYearEnd
            newBookingWeeksRow.FbwVersion = 0
            newBookingWeeksRow.FbwStatus = "none"
            newBookingWeeksRow.IntegrityNo = 0
            newBookingWeeksRow.AddUsrId = "none"
            newBookingWeeksRow.AddPrgmName = "none"
            newBookingWeeksRow.AddDateTime = DateTime.Now
            _flexBookingWeekDataTable.Rows.InsertAt(newBookingWeeksRow, 0)
        Next

        ' get the id for the current booking week
        _currentFbwId = -1
        For Each bookingWeeksRow As FlexBookingWeekRow In _flexBookingWeekDataTable
            If bookingWeeksRow.FbwBookStart <= today _
             AndAlso bookingWeeksRow.FbwBookStart.AddDays(7) > today Then
                _currentFbwId = bookingWeeksRow.FbwId
            End If
        Next

    End Sub

End Class
