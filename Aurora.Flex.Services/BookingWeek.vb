Imports System.Data.SqlClient
Imports System.Drawing

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

''' <summary>
''' A <c>BookingWeek</c> holds information about a specific booking week, and the products, exports, and brands relevant for it.
''' Products can be indexed by country and brand using the WeekProductsByCountryBrandCode property.
''' </summary>
Public Class BookingWeek

    Private _bookingWeekRow As FlexBookingWeekRow
    Public ReadOnly Property BookingWeekRow() As FlexBookingWeekRow
        Get
            Return _bookingWeekRow
        End Get
    End Property

    Private _countryBrandYearsDataTable As New CountryBrandYearsDataTable
    Public ReadOnly Property CountryBrandYearsDataTable() As CountryBrandYearsDataTable
        Get
            Return _countryBrandYearsDataTable
        End Get
    End Property

    Private _weekProductsDataTable As WeekProductsDataTable
    Public ReadOnly Property WeekProductsDataTable() As WeekProductsDataTable
        Get
            Return _weekProductsDataTable
        End Get
    End Property

    ''' <summary>
    ''' Get all the weekly products for a brdCode
    ''' </summary>
    Public Function GetWeekProductsForBrandCode(ByVal brdCode As String) As WeekProductsRow()
        Dim result As New List(Of WeekProductsRow)

        For Each weekProductsRow As WeekProductsRow In Me.WeekProductsDataTable
            If brdCode Is Nothing OrElse weekProductsRow.BrdCode = brdCode Then
                result.Add(weekProductsRow)
            End If
        Next

        Return result.ToArray()
    End Function

    Private _weekExportsDataTable As WeekExportsDataTable
    Public ReadOnly Property WeekExportsDataTable() As WeekExportsDataTable
        Get
            Return _weekExportsDataTable
        End Get
    End Property

    ''' <summary>
    ''' Returns export for a specific brand (brdCode isnot nothing)
    ''' or exports that do not have a brand (brdCode is nothing) 
    ''' </summary>
    Public Function GetWeekExportsForBrandCode(ByVal brdCode As String) As WeekExportsRow()
        Dim result As New List(Of WeekExportsRow)
        For Each weekExportsRow As WeekExportsRow In Me.WeekExportsDataTable
            If (weekExportsRow.IsFlxBrdCodeNull AndAlso brdCode Is Nothing) _
             OrElse (Not weekExportsRow.IsFlxBrdCodeNull AndAlso brdCode IsNot Nothing AndAlso weekExportsRow.FlxBrdCode = brdCode) Then
                result.Add(weekExportsRow)
            End If
        Next
        Return result.ToArray()
    End Function

    Private _flexBookingWeekBrandDataTable As FlexBookingWeekBrandDataTable
    Public ReadOnly Property FlexBookingWeekBrandDataTable() As FlexBookingWeekBrandDataTable
        Get
            Return _flexBookingWeekBrandDataTable
        End Get
    End Property

    Public ReadOnly Property FlexBookingWeekBrandRow(ByVal brdCode As String) As FlexBookingWeekBrandRow
        Get
            For Each result As FlexBookingWeekBrandRow In _flexBookingWeekBrandDataTable
                If result.FbbBrdCode = brdCode Then
                    Return result
                End If
            Next
            Return Nothing
        End Get
    End Property

    Private _weekProductsByCountryBrandCode As New Dictionary(Of String, List(Of WeekProductsRow))
    Public ReadOnly Property WeekProductsByCountryBrandCode(ByVal ctyBrdCode As String) As List(Of WeekProductsRow)
        Get
            Return _weekProductsByCountryBrandCode(ctyBrdCode)
        End Get
    End Property

    Public ReadOnly Property AreExportsSent(ByVal brdCode As String) As Boolean
        Get
            For Each weekExportsRow As WeekExportsRow In GetWeekExportsForBrandCode(brdCode)
                If weekExportsRow.ProductCount <> 0 AndAlso Not weekExportsRow.IsSent Then
                    Return False
                End If
            Next
            Return True
        End Get
    End Property

    Public ReadOnly Property AreAllExportsSent() As Boolean
        Get
            For Each weekExportsRow As WeekExportsRow In _weekExportsDataTable
                If weekExportsRow.ProductCount <> 0 AndAlso Not weekExportsRow.IsSent Then
                    Return False
                End If
            Next
            Return True
        End Get
    End Property

    Private Sub Load()
        _weekProductsDataTable = DataRepository.GetWeekProducts(BookingWeekRow.FbwComCode, BookingWeekRow.FbwId)
        _weekExportsDataTable = DataRepository.GetWeekExports(BookingWeekRow.FbwComCode, BookingWeekRow.FbwId)

        For Each weekProductRow As WeekProductsRow In _weekProductsDataTable
            Dim ctyBrdYrCode = weekProductRow.CtyCode + "," + weekProductRow.BrdCode + "," + weekProductRow.TravelYear.Year.ToString()
            If _countryBrandYearsDataTable.FindByCtyBrdYrCode(ctyBrdYrCode) Is Nothing Then
                Dim countryBrandsRow As CountryBrandYearsRow = _countryBrandYearsDataTable.NewCountryBrandYearsRow()
                countryBrandsRow.CtyBrdYrCode = ctyBrdYrCode
                countryBrandsRow.CtyBrdYrName = weekProductRow.CtyName + " - " + weekProductRow.BrdName + " for " + weekProductRow.TravelYear.Year.ToString()
                countryBrandsRow.CtyCode = weekProductRow.CtyCode
                countryBrandsRow.BrdCode = weekProductRow.BrdCode
                countryBrandsRow.TravelYear = weekProductRow.TravelYear
                _countryBrandYearsDataTable.Rows.Add(countryBrandsRow)
                _weekProductsByCountryBrandCode.Add(ctyBrdYrCode, New List(Of WeekProductsRow))
            End If
            _weekProductsByCountryBrandCode(ctyBrdYrCode).Add(weekProductRow)
        Next

        _flexBookingWeekBrandDataTable = DataRepository.GetBookingWeekBrands(BookingWeekRow.FbwComCode, BookingWeekRow.FbwId)
    End Sub

    Public Sub New(ByVal bookingWeekRow As FlexBookingWeekRow)
        If bookingWeekRow Is Nothing Then
            Throw New ArgumentNullException()
        End If

        _bookingWeekRow = bookingWeekRow
        Load()
    End Sub

    Public Sub New(ByVal comCode As String, ByVal fbwId As Integer)
        If String.IsNullOrEmpty(comCode) Then
            Throw New ArgumentNullException()
        End If

        Dim flexBookingWeekDataTable As FlexBookingWeekDataTable = DataRepository.GetBookingWeek(comCode, fbwId)
        If flexBookingWeekDataTable Is Nothing Or flexBookingWeekDataTable.Rows.Count = 0 Then
            Throw New Exception("Booking Week does not exist")
        End If

        _bookingWeekRow = flexBookingWeekDataTable(0)
        Load()
    End Sub

#Region "Booking Week Functions"

    ''' <summary>
    ''' Create the booking week.  Can throw a Validation or Save Exception
    ''' </summary>
    Public Function CreateBookingWeek(ByVal usrId As String, ByVal prgmName As String, ByVal WithDomesticFlex As Boolean) As FlexBookingWeekRow
        If _bookingWeekRow.FbwId > 0 Then
            Throw New ValidationException("Invalid Booking Week: Already exists")
        End If

        Dim bookingWeekResult As FlexBookingWeekDataTable = DataRepository.CreateBookingWeek(_bookingWeekRow.FbwComCode,
                                                                                             _bookingWeekRow.FbwBookStart,
                                                                                             _bookingWeekRow.FbwTravelYearStart,
                                                                                             _bookingWeekRow.FbwTravelYearEnd,
                                                                                             usrId,
                                                                                             prgmName, _
                                                                                             WithDomesticFlex)
        If bookingWeekResult Is Nothing OrElse bookingWeekResult.Count = 0 Then
            Throw New SaveException("Create Error: There was a problem creating the booking week.")
        End If

        Return bookingWeekResult(0)
    End Function

    Public ReadOnly Property IsReadOnlyBookingWeek() As Boolean
        Get
            Return _bookingWeekRow.FbwId <= 0 OrElse _bookingWeekRow.IsReadonly
        End Get
    End Property

    Public ReadOnly Property CanApproveBookingWeek() As Boolean
        Get
            If _bookingWeekRow.FbwId <= 0 OrElse Not _bookingWeekRow.CanApprove Then
                Return False
            End If

            For Each flexBookingWeekBrandRow As FlexBookingWeekBrandRow In Me.FlexBookingWeekBrandDataTable
                If Not (flexBookingWeekBrandRow.IsApproved OrElse flexBookingWeekBrandRow.IsSent) Then
                    Return False
                End If
            Next

            Return True
        End Get
    End Property

    Public ReadOnly Property CanRejectBookingWeek() As Boolean
        Get
            Return _bookingWeekRow.FbwId > 0 AndAlso _bookingWeekRow.CanReject
        End Get
    End Property

    Public ReadOnly Property CanFinalizeBookingWeek() As Boolean
        Get
            If _bookingWeekRow.FbwId <= 0 OrElse Not _bookingWeekRow.CanFinalize Then
                Return False
            End If

            Return True
        End Get
    End Property

    ''' <summary>
    ''' Approve the booking week.  Can throw a Validation or Save Exception
    ''' </summary>
    Public Function ApproveBookingWeek(ByVal emailSubject As String, ByVal emailText As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekRow
        If _bookingWeekRow.FbwId <= 0 Then
            Throw New ValidationException("Invalid Booking Week: Does not exist")
        ElseIf Not CanApproveBookingWeek() Then
            Throw New ValidationException("Invalid Booking Week: Invalid status, cannot approve")
        ElseIf _bookingWeekRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Booking Week: Has already been updated by another user")
        End If

        Dim bookingWeekResult As FlexBookingWeekDataTable = DataRepository.ApproveBookingWeek( _
             _bookingWeekRow.FbwId, _
             emailSubject, _
             emailText, _
             _bookingWeekRow.IntegrityNo, _
             usrId)
        If bookingWeekResult Is Nothing OrElse bookingWeekResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem approving the booking week, most likely caused by it being updated already.")
        End If

        _bookingWeekRow.FbwVersion = bookingWeekResult(0).FbwVersion
        _bookingWeekRow.FbwStatus = bookingWeekResult(0).FbwStatus
        _bookingWeekRow.FbwEmailSubject = bookingWeekResult(0).FbwEmailSubject
        _bookingWeekRow.FbwEmailText = bookingWeekResult(0).FbwEmailText
        _bookingWeekRow.IntegrityNo = bookingWeekResult(0).IntegrityNo
        _bookingWeekRow.ModUsrId = bookingWeekResult(0).ModUsrId
        _bookingWeekRow.ModDateTime = bookingWeekResult(0).ModDateTime

        For Each weekExportsRow As WeekExportsRow In GetWeekExportsForBrandCode(Nothing)
            weekExportsRow.FwxStatus = FlexConstants.BookingWeekStatus_Approved
            weekExportsRow.ModUsrId = usrId
            weekExportsRow.ModDateTime = Date.Now
        Next

        Return bookingWeekResult(0)
    End Function

    ''' <summary>
    ''' Reject the booking week.  Can throw a Validation or Save Exception
    ''' </summary>
    Public Function RejectBookingWeek(ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekRow
        If _bookingWeekRow.FbwId <= 0 Then
            Throw New ValidationException("Invalid Booking Week: Does not exist")
        ElseIf Not CanRejectBookingWeek() Then
            Throw New ValidationException("Invalid Booking Week: Invalid status, cannot reject")
        ElseIf _bookingWeekRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Booking Week: Has already been updated by another user")
        End If

        Dim bookingWeekResult As FlexBookingWeekDataTable = DataRepository.RejectBookingWeek( _
             _bookingWeekRow.FbwId, _
             _bookingWeekRow.IntegrityNo, _
             usrId)
        If bookingWeekResult Is Nothing OrElse bookingWeekResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem rejecting the booking week, most likely caused by it being updated already.")
        End If

        _bookingWeekRow.FbwStatus = bookingWeekResult(0).FbwStatus
        _bookingWeekRow.IntegrityNo = bookingWeekResult(0).IntegrityNo
        _bookingWeekRow.ModUsrId = bookingWeekResult(0).ModUsrId
        _bookingWeekRow.ModDateTime = bookingWeekResult(0).ModDateTime

        For Each weekExportsRow As WeekExportsRow In GetWeekExportsForBrandCode(Nothing)
            weekExportsRow.FwxStatus = FlexConstants.BookingWeekStatus_Rejected
            weekExportsRow.ModUsrId = usrId
            weekExportsRow.ModDateTime = Date.Now
        Next

        Return bookingWeekResult(0)
    End Function

    ''' <summary>
    ''' Finalize the booking week.  
    ''' </summary>
    Public Function FinalizeBookingWeek(ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekRow
        If _bookingWeekRow.FbwId <= 0 Then
            Throw New ValidationException("Invalid Booking Week: Does not exist")
        ElseIf Not CanFinalizeBookingWeek() Then
            Throw New ValidationException("Invalid Booking Week: Cannot finalize")
        ElseIf _bookingWeekRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Booking Week: Has already been updated by another user")
        End If

        Dim bookingWeekResult As FlexBookingWeekDataTable = DataRepository.FinalizeBookingWeek( _
             _bookingWeekRow.FbwId, _
             _bookingWeekRow.IntegrityNo, _
             usrId)
        If bookingWeekResult Is Nothing OrElse bookingWeekResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem finalizing the booking week, most likely caused by it being updated already.")
        End If

        _bookingWeekRow.FbwStatus = bookingWeekResult(0).FbwStatus
        _bookingWeekRow.IntegrityNo = bookingWeekResult(0).IntegrityNo
        _bookingWeekRow.ModUsrId = bookingWeekResult(0).ModUsrId
        _bookingWeekRow.ModDateTime = bookingWeekResult(0).ModDateTime

        Return bookingWeekResult(0)
    End Function

#End Region

#Region "Booking Week Brand Functions"

    Public ReadOnly Property IsReadOnlyBookingWeekBrand(ByVal brdCode As String) As Boolean
        Get
            Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
            Return flexBookingWeekBrandRow Is Nothing _
             OrElse flexBookingWeekBrandRow.IsReadonly _
             OrElse Me.BookingWeekRow.IsReadonly
        End Get
    End Property

    Public ReadOnly Property CanApproveBookingWeekBrand(ByVal brdCode As String) As Boolean
        Get
            Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
            Return flexBookingWeekBrandRow IsNot Nothing _
             AndAlso flexBookingWeekBrandRow.CanApprove _
             AndAlso Not Me.BookingWeekRow.IsReadOnly
        End Get
    End Property

    Public ReadOnly Property CanRejectBookingWeekBrand(ByVal brdCode As String) As Boolean
        Get
            Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
            Return flexBookingWeekBrandRow IsNot Nothing _
             AndAlso flexBookingWeekBrandRow.CanReject _
             AndAlso Not Me.BookingWeekRow.IsReadOnly
        End Get
    End Property

    Public ReadOnly Property CanFinalizeBookingWeekBrand(ByVal brdCode As String) As Boolean
        Get
            Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
            Return flexBookingWeekBrandRow IsNot Nothing _
             AndAlso flexBookingWeekBrandRow.CanFinalize
        End Get
    End Property

    ''' <summary>
    ''' Approve the brand.  Can throw a Validation or Save Exception
    ''' </summary>
    Public Function ApproveBookingWeekBrand(ByVal brdCode As String, ByVal emailSubject As String, ByVal emailText As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekBrandRow
        If Not CanApproveBookingWeekBrand(brdCode) Then
            Throw New ValidationException("Invalid Brand: Invalid status, cannot approve")
        End If

        Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
        If flexBookingWeekBrandRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Brand: Has already been updated by another user")
        End If

        Dim bookingWeekBrandResult As FlexBookingWeekBrandDataTable = DataRepository.ApproveBookingWeekBrand( _
             flexBookingWeekBrandRow.FbbId, _
             emailSubject, _
             emailText, _
             flexBookingWeekBrandRow.IntegrityNo, _
             usrId)
        If bookingWeekBrandResult Is Nothing OrElse bookingWeekBrandResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem approving the brand, most likely caused by it being updated already.")
        End If

        flexBookingWeekBrandRow.FbbVersion = bookingWeekBrandResult(0).FbbVersion
        flexBookingWeekBrandRow.FbbStatus = bookingWeekBrandResult(0).FbbStatus
        flexBookingWeekBrandRow.FbbEmailSubject = bookingWeekBrandResult(0).FbbEmailSubject
        flexBookingWeekBrandRow.FbbEmailText = bookingWeekBrandResult(0).FbbEmailText
        flexBookingWeekBrandRow.IntegrityNo = bookingWeekBrandResult(0).IntegrityNo
        flexBookingWeekBrandRow.ModUsrId = bookingWeekBrandResult(0).ModUsrId
        flexBookingWeekBrandRow.ModDateTime = bookingWeekBrandResult(0).ModDateTime

        For Each weekExportsRow As WeekExportsRow In GetWeekExportsForBrandCode(brdCode)
            weekExportsRow.FwxStatus = FlexConstants.BookingWeekStatus_Approved
            weekExportsRow.ModUsrId = usrId
            weekExportsRow.ModDateTime = Date.Now
        Next

        Return bookingWeekBrandResult(0)
    End Function

    ''' <summary>
    ''' Reject the brand.  Can throw a Validation or Save Exception
    ''' </summary>
    Public Function RejectBookingWeekBrand(ByVal brdCode As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekBrandRow
        If Not CanRejectBookingWeekBrand(brdCode) Then
            Throw New ValidationException("Invalid Brand: Invalid status, cannot reject")
        End If

        Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
        If flexBookingWeekBrandRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Brand: Has already been updated by another user")
        End If

        Dim bookingWeekBrandResult As FlexBookingWeekBrandDataTable = DataRepository.RejectBookingWeekBrand( _
             flexBookingWeekBrandRow.FbbId, _
             flexBookingWeekBrandRow.IntegrityNo, _
             usrId)
        If bookingWeekBrandResult Is Nothing OrElse bookingWeekBrandResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem rejecting the brand, most likely caused by it being updated already.")
        End If

        flexBookingWeekBrandRow.FbbStatus = bookingWeekBrandResult(0).FbbStatus
        flexBookingWeekBrandRow.IntegrityNo = bookingWeekBrandResult(0).IntegrityNo
        flexBookingWeekBrandRow.ModUsrId = bookingWeekBrandResult(0).ModUsrId
        flexBookingWeekBrandRow.ModDateTime = bookingWeekBrandResult(0).ModDateTime

        For Each weekExportsRow As WeekExportsRow In GetWeekExportsForBrandCode(brdCode)
            weekExportsRow.FwxStatus = FlexConstants.BookingWeekStatus_Rejected
            weekExportsRow.ModUsrId = usrId
            weekExportsRow.ModDateTime = Date.Now
        Next

        Return bookingWeekBrandResult(0)
    End Function

    ''' <summary>
    ''' Finalize the brand.  
    ''' </summary>
    Public Function FinalizeBookingWeekBrand(ByVal brdCode As String, ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekBrandRow
        If Not CanFinalizeBookingWeekBrand(brdCode) Then
            Throw New ValidationException("Invalid Brand: Invalid status, cannot finalize")
        End If

        Dim flexBookingWeekBrandRow As FlexBookingWeekBrandRow = Me.FlexBookingWeekBrandRow(brdCode)
        If flexBookingWeekBrandRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Brand: Has already been updated by another user")
        End If

        Dim bookingWeekBrandResult As FlexBookingWeekBrandDataTable = DataRepository.FinalizeBookingWeekBrand( _
             flexBookingWeekBrandRow.FbbId, _
             flexBookingWeekBrandRow.IntegrityNo, _
             usrId)
        If bookingWeekBrandResult Is Nothing OrElse bookingWeekBrandResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem finalizing the brand, most likely caused by it being updated already.")
        End If

        flexBookingWeekBrandRow.FbbStatus = bookingWeekBrandResult(0).FbbStatus
        flexBookingWeekBrandRow.IntegrityNo = bookingWeekBrandResult(0).IntegrityNo
        flexBookingWeekBrandRow.ModUsrId = bookingWeekBrandResult(0).ModUsrId
        flexBookingWeekBrandRow.ModDateTime = bookingWeekBrandResult(0).ModDateTime

        Return bookingWeekBrandResult(0)
    End Function

#End Region

End Class
