Imports System.Text

Imports System.Data.SqlClient

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports System.Globalization
''' <summary>
''' A <c>WeekExport</c> holds information about a export (<c>FlexBookingWeekExport</c>) and all the information
''' it needs to generate the file/text for it (contacts, products, locations, weeks, vehicle mappings).
''' </summary>
Public Class Export

    Private _weekExportRow As WeekExportsRow
    Public ReadOnly Property WeekExportRow() As WeekExportsRow
        Get
            Return _weekExportRow
        End Get
    End Property

    Private _exportContactsDataTable As ExportContactsDataTable
    Public ReadOnly Property ExportContactsDataTable() As ExportContactsDataTable
        Get
            Return _exportContactsDataTable
        End Get
    End Property

    Public ReadOnly Property ContactsDescription() As String
        Get
            If _weekExportRow.FwxStatus = FlexConstants.BookingWeekStatus_Sent Then
                If Not _weekExportRow.IsFwxContactsDescriptionNull Then
                    Return _weekExportRow.FwxContactsDescription
                Else
                    Return ""
                End If
            Else
                Dim result As New StringBuilder()
                Dim first As Boolean = True
                For Each exportContactsRow As ExportContactsRow In _exportContactsDataTable
                    If first Then
                        first = False
                    Else
                        result.Append(";")
                    End If
                    result.Append(exportContactsRow.Description)
                Next
                Return result.ToString()
            End If
        End Get
    End Property

    Private _weekProductsDataTable As WeekProductsDataTable
    Public ReadOnly Property WeekProductsDataTable() As WeekProductsDataTable
        Get
            Return _weekProductsDataTable
        End Get
    End Property

    Private _flexWeekNumberDataTable As FlexWeekNumberDataTable
    Public ReadOnly Property FlexWeekNumberDataTable() As FlexWeekNumberDataTable
        Get
            Return _flexWeekNumberDataTable
        End Get
    End Property

    Public ReadOnly Property FlexWeekNumberRowsByTravelYear(ByVal travelYear As Date) As List(Of FlexWeekNumberRow)
        Get
            Dim result As New List(Of FlexWeekNumberRow)
            For Each flexWeekNumberRow As FlexWeekNumberRow In _flexWeekNumberDataTable
                If flexWeekNumberRow.FwnTrvYearStart = travelYear Then
                    result.Add(flexWeekNumberRow)
                End If
            Next
            Return result
        End Get
    End Property

    Public ReadOnly Property FlexWeekNumberCurrentByTravelYear(ByVal travelYear As Date) As FlexWeekNumberRow
        Get
            For Each flexWeekNumberRow As FlexWeekNumberRow In FlexWeekNumberRowsByTravelYear(travelYear)
                If flexWeekNumberRow.IsCurrent(_weekExportRow.FbwBookStart) _
                 OrElse flexWeekNumberRow.IsFuture(_weekExportRow.FbwBookStart) Then
                    Return flexWeekNumberRow
                End If
            Next
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property CurrentWkNoByTravelYear(ByVal travelYear As Date) As Integer
        Get
            Dim result As Integer
            If Not FlexWeekNumberCurrentByTravelYear(travelYear) Is Nothing Then
                result = FlexWeekNumberCurrentByTravelYear(travelYear).FwnWkNo
            Else
                result = 1
            End If
            If result > 1 Then result -= 1
            Return result
        End Get
    End Property

    Private _products As New List(Of Product)
    Public ReadOnly Property Products() As List(Of Product)
        Get
            Return _products
        End Get
    End Property

    Private _locationsDataTable As LocationsDataTable
    Public ReadOnly Property LocationsDataTable() As LocationsDataTable
        Get
            Return _locationsDataTable
        End Get
    End Property

    Public Function IsProductAndLocationValid(ByVal product As Product, ByVal locCode As String) As Boolean
        Return product.HasLocation(locCode) _
         AndAlso (_weekExportRow.IsFwxTravelYearNull() OrElse product.WeekProductRow.TravelYear = _weekExportRow.FwxTravelYear) _
         AndAlso Not product.IsLocationExcluded(locCode)
    End Function

    Public Function IsLocationValid(ByVal locCode As String) As Boolean
        For Each product As Product In _products
            If IsProductAndLocationValid(product, locCode) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public Function IsProductValid(ByVal product As Product) As Boolean
        For Each locationsRow As LocationsRow In _locationsDataTable
            If IsProductAndLocationValid(product, locationsRow.LocCode) Then
                Return True
            End If
        Next
        Return False
    End Function

    Public ReadOnly Property FirstValidProduct() As Product
        Get
            For Each product As Product In _products
                If IsProductValid(product) Then
                    Return product
                End If
            Next
            Return Nothing
        End Get
    End Property

    Public Function GetValidProductByNameLocationTravelYear(ByVal prdShortName As String, ByVal locCode As String, ByVal travelDate As Date) As Product
        For Each product As Product In _products
            If IsProductAndLocationValid(product, locCode) _
             AndAlso product.WeekProductRow.PrdShortName = prdShortName _
             AndAlso product.WeekProductRow.TravelYear = travelDate Then
                Return product
            End If
        Next
        Return Nothing
    End Function

    Private _flexProductDataTable As FlexProductDataTable
    Public ReadOnly Property FlexProductDataTable() As FlexProductDataTable
        Get
            Return _flexProductDataTable
        End Get
    End Property

    Public Function GetFlexProduct(ByVal ctyCode As String, ByVal brdCode As String, ByVal prdId As String) As FlexProductRow
        For Each flexProductRow As FlexProductRow In _flexProductDataTable
            If (flexProductRow.FxpComCode = WeekExportRow.FbwComCode) _
             AndAlso (flexProductRow.FxpCtyCode = ctyCode) _
             AndAlso (flexProductRow.FxpBrdCode = brdCode) _
             AndAlso (flexProductRow.FxpPrdId = prdId) _
             AndAlso (flexProductRow.FxpIsActive) _
             AndAlso (flexProductRow.IsFxpParentIdNull) _
             Then Return flexProductRow
        Next
        Return Nothing
    End Function

    Private _text As String = Nothing
    Public ReadOnly Property Text()
        Get
            If _text Is Nothing Then
                If _weekExportRow.FwxStatus = FlexConstants.BookingWeekStatus_Sent Then
                    If Not _weekExportRow.IsFwxTextNull Then
                        _text = _weekExportRow.FwxText
                    Else
                        _text = ""
                    End If
                Else
                    _text = ExportGenerator.Generate(Me)
                End If
            End If
            Return _text
        End Get
    End Property

    Private Sub Load()
        _exportContactsDataTable = DataRepository.GetExportContacts(_weekExportRow.FbwComCode, _weekExportRow.FlxId)
        _weekProductsDataTable = DataRepository.GetWeekProductsExport(_weekExportRow.FbwComCode, _weekExportRow.FwxId)

        Dim tempArray As ArrayList = ProductsExcludeText
        For Each weekProductsRow As WeekProductsRow In _weekProductsDataTable

            ''-----------------------------------------------------------------------------
            ''REV:MIA JULY 15 2013 - NEW VEHICLE TO BE EXCLUDE AND AVAILABLE ONLY IN AURORA
            '' ALL OTHER FILES HAS NO ACCESS TO THESE VEHICLE
            ''-----------------------------------------------------------------------------
            Try
                If Not tempArray Is Nothing Then
                    If tempArray.Count > 0 Then
                        If (Me.WeekExportRow.FlxCode.ToUpper() = "AURORA") Then
                            Logging.LogWarning("EXPORT to FLXTOAURORA", "Products to include in " & Me.WeekExportRow.FlxCode.ToUpper() & " :  " & weekProductsRow.PrdShortName)
                            _products.Add(New Product(weekProductsRow))
                        Else
                            If (tempArray.IndexOf(weekProductsRow.PrdShortName) <> -1) Then
                                Logging.LogWarning("EXPORT", "Products to exclude in " & Me.WeekExportRow.FlxCode.ToUpper() & " :  " & weekProductsRow.PrdShortName)
                                _productsExclude.Add(New Product(weekProductsRow))
                            Else
                                Logging.LogWarning("EXPORT(Inner Else Block)", "Products to include in " & Me.WeekExportRow.FlxCode.ToUpper() & " :  " & weekProductsRow.PrdShortName)
                                _products.Add(New Product(weekProductsRow))
                            End If
                        End If
                    End If
                Else
                    ''there is no exclusion
                    Logging.LogWarning("EXPORT(Outer Else Block)", "Products to include in " & Me.WeekExportRow.FlxCode.ToUpper() & " :  " & weekProductsRow.PrdShortName)
                    _products.Add(New Product(weekProductsRow))
                End If
            Catch ex As Exception
                Logging.LogWarning("EXPORT(Catch Block)", "Products to include in " & Me.WeekExportRow.FlxCode.ToUpper() & " :  " & weekProductsRow.PrdShortName)
                _products.Add(New Product(weekProductsRow))
            End Try
            
            ''---------------------------------------------------------------------------
        Next
        _flexWeekNumberDataTable = DataRepository.GetFlexWeekNumbersBetween(_weekExportRow.FbwTravelYearStart, _weekExportRow.FbwTravelYearEnd)
        _locationsDataTable = DataRepository.GetLocationsExport(_weekExportRow.FbwComCode, _weekExportRow.FwxId)
        _flexProductDataTable = DataRepository.GetFlexProducts(_weekExportRow.FbwComCode)
    End Sub

    Public Sub New(ByVal weekExportRow As WeekExportsRow)
        If weekExportRow Is Nothing Then
            Throw New ArgumentNullException()
        End If

        _weekExportRow = weekExportRow
        Load()
    End Sub

    Public Sub New(ByVal comCode As String, ByVal fwxId As Integer)
        If String.IsNullOrEmpty(comCode) Then
            Throw New ArgumentNullException()
        End If

        Dim weekExportsDataTable As WeekExportsDataTable = DataRepository.GetWeekExport(comCode, fwxId)
        If weekExportsDataTable Is Nothing Or weekExportsDataTable.Rows.Count = 0 Then
            Throw New Exception("Export does not exist")
        End If

        _weekExportRow = weekExportsDataTable(0)
        Load()
    End Sub

    ''' <summary>
    ''' Save the export.  Can throw a Save Exception
    ''' </summary>
    Public Sub Save(ByVal scheduledTime As Date, ByVal integrityNo As Integer, ByVal usrId As String)
        If _weekExportRow.IsReadonly _
         OrElse _weekExportRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Export: Has already been updated by another user", _weekExportRow)
        End If

        Dim weekExportResult As FlexBookingWeekExportDataTable = DataRepository.UpdateWeekExport( _
             _weekExportRow.FwxId, _
             scheduledTime, _
             integrityNo, _
             usrId)
        If weekExportResult Is Nothing OrElse weekExportResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem updating the file, most likely caused by it being updated already.")
        End If

        _weekExportRow.FwxScheduledTime = weekExportResult(0).FwxScheduledTime
        _weekExportRow.IntegrityNo = weekExportResult(0).IntegrityNo
        _weekExportRow.ModUsrId = weekExportResult(0).ModUsrId
        _weekExportRow.ModDateTime = weekExportResult(0).ModDateTime
    End Sub

    Public ReadOnly Property CanFinalizeExport() As Boolean
        Get
            Return _weekExportRow.CanFinalize
        End Get
    End Property

    ''' <summary>
    ''' Finalize the export
    ''' </summary>
    Public Function FinalizeExport(ByVal integrityNo As Integer, ByVal usrId As String) As FlexBookingWeekExportRow
        If Not Me.CanFinalizeExport() _
         OrElse _weekExportRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Export: Cannot finalize")
        End If

        Dim weekExportResult As FlexBookingWeekExportDataTable = DataRepository.FinalizeWeekExport( _
             _weekExportRow.FwxId, _
             Text, _
             ContactsDescription, _
             integrityNo, _
             usrId)
        If weekExportResult Is Nothing OrElse weekExportResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem finalizing the booking week, most likely caused by it being updated already.")
        End If

        _weekExportRow.FwxStatus = weekExportResult(0).FwxStatus
        _weekExportRow.FwxText = weekExportResult(0).FwxText
        _weekExportRow.FwxContactsDescription = weekExportResult(0).FwxContactsDescription
        _weekExportRow.IntegrityNo = weekExportResult(0).IntegrityNo
        _weekExportRow.ModUsrId = weekExportResult(0).ModUsrId
        _weekExportRow.ModDateTime = weekExportResult(0).ModDateTime

        Return weekExportResult(0)
    End Function

#Region "REV:MIA JULY 15 2013 - NEW VEHICLE TO BE EXCLUDE AND AVAILABLE ONLY IN SQL"
    Private _productsExclude As New List(Of Product)
    Public ReadOnly Property productsExclude() As List(Of Product)
        Get
            Return _productsExclude
        End Get
    End Property

    Private _productsExcludeText As ArrayList
    Private ReadOnly Property ProductsExcludeText As ArrayList
        Get

            Dim productsList As String
            Try
                productsList = System.Configuration.ConfigurationManager.AppSettings("newProductFlexExclusion").ToString
                If (String.IsNullOrEmpty(productsList)) Then
                    _productsExcludeText = Nothing
                Else
                    Dim productsListTemp As String() = productsList.Split(","c)
                    _productsExcludeText = New ArrayList
                    _productsExcludeText.AddRange(productsListTemp)
                End If
                

            Catch ex As Exception
                Return Nothing
            End Try
            Return _productsExcludeText

        End Get
    End Property
#End Region

#Region "REV:MIA FEB.13 2014 CHECKING OF WEEK NUMBER"
    'Public Shared Function GetIso8601WeekOfYear(time As DateTime) As Integer
    '    ' Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
    '    ' be the same week# as whatever Thursday, Friday or Saturday are,
    '    ' and we always get those right
    '    Dim day As DayOfWeek = CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(time)
    '    If day >= DayOfWeek.Monday AndAlso day <= DayOfWeek.Wednesday Then
    '        time = time.AddDays(3)
    '    End If

    '    ' Return the week of our adjusted day
    '    Return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(time, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday)
    'End Function
#End Region

#Region "REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718"
    Private Shared Function IsAprilFlexRecordActive() As Boolean
        Return CBool(System.Configuration.ConfigurationManager.AppSettings("displayAprilFlexRecordActive").ToString())
    End Function

    Private Shared Function getAprilFlexRecord() As String()
        Return System.Configuration.ConfigurationManager.AppSettings("displayAprilFlexRecord").ToString().Split(",")
    End Function

    Public Shared Function DisplayAprilFlexRecord(brand As String, year As String) As Boolean
        If (Not IsAprilFlexRecordActive()) Then Return False
        Dim aprilflexrecord() As String = getAprilFlexRecord()
        For Each item As String In aprilflexrecord
            If (Not String.IsNullOrEmpty(item)) Then
                If (item.Contains("-") = True) Then
                    Dim splitdata() As String = item.Split("-")
                    If (splitdata(0) = brand And splitdata(1) = year) Then
                        Return True
                    End If
                End If
            End If
        Next
        Return False
    End Function

    Public Shared Function GetMonday(ByVal d As DayOfWeek, ByVal StartDate As Date) As Date
        For p As Integer = 1 To 7
            If StartDate.AddDays(p).DayOfWeek = d Then Return StartDate.AddDays(p)
        Next
    End Function
#End Region

End Class
