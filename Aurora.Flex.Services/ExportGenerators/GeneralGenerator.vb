﻿Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


<ExportGeneratorAttribute("GENERAL")> _
Public Class GeneralGenerator
    Inherits ExportGenerator

    Public Shared ReadOnly InititialBlanks As String = New String(",", 15)
    Public Shared ReadOnly WeekRanges()() As Integer = {New Integer() {1, 13}, New Integer() {14, 26}, New Integer() {27, 39}, New Integer() {40, CInt(System.Configuration.ConfigurationManager.AppSettings("WeekCount").ToString())}}

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Dim pageCount As Integer = 0
        Dim firstValidProduct As Product = export.FirstValidProduct
        Dim currentWkNo As Integer = export.CurrentWkNoByTravelYear(export.WeekExportRow.FwxTravelYear)

        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
        Dim displayAprilFlexRecord As Boolean = export.DisplayAprilFlexRecord(export.Products(0).WeekProductRow.BrdCode, export.Products(0).WeekProductRow.TravelYear.Year)

        Dim result As New StringBuilder
        For Each locationsRow As LocationsRow In export.LocationsDataTable
            If Not export.IsLocationValid(locationsRow.LocCode) Then Continue For

            pageCount += 1

            result.AppendLine(export.WeekExportRow.FbwBookStart.ToString("dd/MM/yyyy") + New String(" ", 28) + firstValidProduct.WeekProductRow.BrdName + " Flex Rates" + New String(" ", 13) + "Page: " & CStr(pageCount))
            result.AppendLine("Effective for bookings made in the week commencing " + export.WeekExportRow.FbwBookStart.ToString("dd MMMM, yyyy"))
            result.AppendLine("Departure from: " + locationsRow.LocName + ", booked after " + export.WeekExportRow.FbwBookStart.ToString("dd MMMM, yyyy"))
            result.AppendLine()

            For Each weekRange() As Integer In WeekRanges
                If currentWkNo > weekRange(1) Then Continue For

                ' sub-header months
                result.Append("Ex-" + locationsRow.LocName.PadRight(19))
                For wkNo As Integer = weekRange(0) To weekRange(1)
                    If wkNo < currentWkNo Then
                        result.Append(New String(" ", 4))
                    Else
                        For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(export.WeekExportRow.FwxTravelYear)
                            If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                                ''rev:mia nov 09 2015 -  Aurora(-432) General Flex file error
                                Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                                   " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                                   " Does Not Fall On Monday")
                                ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                                If (Not displayAprilFlexRecord) Then
                                    Continue For
                                End If
                            End If
                            If flexWeekNumberRow.FwnWkNo = wkNo Then
                                result.Append(flexWeekNumberRow.FwnTrvWkStart.ToString("MMM "))
                            End If
                        Next
                    End If
                Next
                result.AppendLine()

                ' sub-header days
                result.Append("  Pick Up week->      ")
                For wkNo As Integer = weekRange(0) To weekRange(1)
                    If wkNo < currentWkNo Then
                        result.Append(New String(" ", 4))
                    Else
                        For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(export.WeekExportRow.FwxTravelYear)
                            If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                                ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                                Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                                   " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                                   " Does Not Fall On Monday")
                                ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                                If (Not displayAprilFlexRecord) Then
                                    Continue For
                                End If
                            End If

                            If flexWeekNumberRow.FwnWkNo = wkNo Then
                                result.Append(flexWeekNumberRow.FwnTrvWkStart.ToString(" dd "))
                            End If
                        Next
                    End If
                Next
                result.AppendLine()


                For Each product As Product In export.Products
                    If Not export.IsProductAndLocationValid(product, locationsRow.LocCode) Then Continue For

                    ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
                    Dim itemFlexProductRow As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                    Dim displayVehicleInAuroraExportOnly As Boolean = CBool(itemFlexProductRow.FxpAuroraExportOnly)
                    If (displayVehicleInAuroraExportOnly) Then
                        Logging.LogWarning("GenerateText: ", "product.WeekProductRow.PrdId :" & product.WeekProductRow.PrdId & ",displayVehicleInAuroraExportOnly:" & displayVehicleInAuroraExportOnly)
                        Continue For
                    End If

                    result.Append(product.WeekProductRow.PrdName.PadRight(22, " "))

                    For wkNo As Integer = weekRange(0) To weekRange(1)
                        If wkNo < currentWkNo Then
                            result.Append(New String(" ", 4))
                        Else

                            For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(export.WeekExportRow.FwxTravelYear)
                                If flexWeekNumberRow.FwnWkNo = wkNo Then

                                    If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                                        ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                                        Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                                           " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                                           " Does Not Fall On Monday")
                                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                                        If (Not displayAprilFlexRecord) Then
                                            Continue For
                                        End If
                                    End If

                                    Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())

                                    ''rev:mia March 23 2012

                                    Dim flexBookingWeekRateRow As FlexBookingWeekRateRow = product.FlexBookingWeekRateByWkNoForINTL_DOM(flexWeekNumberRow.FwnWkNo, IIf(filename.Contains("DOM"), True, False))
                                    Dim rate As String = ""
                                    Dim type As String = Trim(flexBookingWeekRateRow.FlrFbwType.TrimEnd())


                                    rate = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False)))

                                    If (filename.Contains("DOM") = True And flexBookingWeekRateRow.FlrFbwType.Trim.Equals("D") = True) Then
                                        result.Append(rate.PadLeft(3, " ") + IIf(flexBookingWeekRateRow.FlrChanged, "*", " "))
                                    ElseIf (filename.Contains("DOM") = False And flexBookingWeekRateRow.FlrFbwType.Trim.Equals("I") = True) Then
                                        result.Append(rate.PadLeft(3, " ") + IIf(flexBookingWeekRateRow.FlrChanged, "*", " "))
                                    Else
                                        result.Append(rate.PadLeft(3, " ") + " ")
                                    End If


                                End If
                            Next
                        End If
                    Next
                    result.AppendLine()
                Next


                result.AppendLine()
            Next
        Next
        result.AppendLine("End of Report")
        Return result.ToString()
    End Function

End Class
