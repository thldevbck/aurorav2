Imports System.Text
Imports System.IO
Imports System.Xml

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

<ExportGeneratorAttribute("FLEXUPDATE")> _
Public Class FlexUpdateGenerator
    Inherits ExportGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String

        If export.Products.Count = 0 Then Return ""

        Using result As New StringWriterUTF8()
            Using w As New System.Xml.XmlTextWriter(result)
                w.Formatting = Formatting.Indented
                w.Indentation = 2

                w.WriteStartDocument(True)
                w.WriteComment("Flex Rate Update " + export.WeekExportRow.FileName)
                w.WriteStartElement("FlexUpdate")

                w.WriteStartElement("FlexRates")
                For Each product As Product In export.Products
                    w.WriteStartElement("flrtout")

                    w.WriteElementString("country", product.WeekProductRow.CtyCode)
                    w.WriteElementString("brand", "BR")
                    w.WriteElementString("start_on", product.WeekProductRow.TravelYear.ToString("dd/MM/yyyy"))
                    w.WriteElementString("book_start", product.WeekProductRow.FbwBookStart.ToString("dd/MM/yyyy"))
                    w.WriteElementString("vehic_type", product.WeekProductRow.PrdName)

                    For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateDataTable
                        Dim rate As String = FlexConstants.FlexNumToRate(flexBookingWeekRateRow.FlrFlexNum)

                        w.WriteElementString("d" + flexBookingWeekRateRow.FlrWkNo.ToString().PadLeft(2, "0"), flexBookingWeekRateRow.FlrTravelFrom.ToString("dd/MM/yyyy"))
                        w.WriteElementString("w" + flexBookingWeekRateRow.FlrWkNo.ToString().PadLeft(2, "0"), rate)
                    Next

                    w.WriteEndElement()                 ' flrtout
                Next
                w.WriteEndElement()                 ' FlexRates

                w.WriteStartElement("Exceptions")
                For Each product As Product In export.Products
                    For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In product.FlexBookingWeekExceptionDataTable
                        If (flexBookingWeekExceptionRow.IsFleIdNull()) Then Continue For

                        w.WriteStartElement("deltaout")

                        w.WriteElementString("BookingStart", flexBookingWeekExceptionRow.FleBookStart.ToString("yyyy-MM-dd") + "T00:00:00")
                        w.WriteElementString("VEHIC_NAME", product.WeekProductRow.PrdName)
                        w.WriteElementString("FromTo", flexBookingWeekExceptionRow.FromTo)
                        w.WriteElementString("City", flexBookingWeekExceptionRow.LocationCode)
                        w.WriteElementString("TravelStart", flexBookingWeekExceptionRow.FleTravelStart.ToString("yyyy-MM-dd") + "T00:00:00")
                        w.WriteElementString("TravelEnd", flexBookingWeekExceptionRow.FleTravelEnd.ToString("yyyy-MM-dd") + "T00:00:00")
                        w.WriteElementString("Change", flexBookingWeekExceptionRow.FleDelta.ToString())
                        w.WriteElementString("Brand", "BR")
                        w.WriteElementString("Country", product.WeekProductRow.CtyCode)

                        w.WriteEndElement()                 ' deltaout
                    Next
                Next
                w.WriteEndElement()                 ' Exceptions

                w.WriteEndElement()                 ' FlexUpdate
                w.WriteEndDocument()
            End Using

            Return result.ToString()
        End Using
    End Function


End Class
