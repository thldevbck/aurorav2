Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Flex.Data.FlexDataset

''' <summary>
''' The Aurora Export Generator.  Generates the sql which to insert the week rates into the import 
''' tables in Aurora.  This export must be valid SQL.
''' </summary>

<ExportGeneratorAttribute("AURORA")> _
Public Class AuroraGenerator
    Inherits ExportGenerator

    Private Sub GenerateTextProduct(ByVal export As Export, ByVal product As Product, ByVal result As StringBuilder, ByVal flexSubProduct As FlexProductRow)

        Dim ctyCode As String
        Dim brdCode As String
        Dim prdId As String
        Dim prdShortName As String


        
        If flexSubProduct Is Nothing Then
            ctyCode = product.WeekProductRow.CtyCode
            brdCode = product.WeekProductRow.BrdCode
            prdId = product.WeekProductRow.PrdId
            prdShortName = product.WeekProductRow.PrdShortName

        Else
            ctyCode = flexSubProduct.FxpCtyCode
            brdCode = flexSubProduct.FxpBrdCode
            prdId = flexSubProduct.FxpPrdId
            prdShortName = flexSubProduct.PrdShortName
        End If

        result.AppendLine("-- ======================================================================")
        result.AppendLine(Utility.EscapeSqlSingleLineComment(brdCode + " - " + ctyCode + " - " + prdShortName))

        ' book dates (product)
        result.AppendLine("SET @id = NEWID()")
        result.AppendLine("INSERT INTO FlexBookedDate (FbdId, FbdCtyCode, FbdBrand, FbdEffDate, FbdBookFrom, FbdPrdId, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime,AddPrgmName)")
        result.Append("VALUES (")
        result.Append("@id, ") ' (<FbdId, varchar(64),> _
        result.Append(Utility.EscapeSqlString(ctyCode) & ", ") ' ,<FbdCtyCode, varchar(3),>
        result.Append(Utility.EscapeSqlString(brdCode) & ", ") ' ,<FbdBrand, varchar(64),>
        result.Append("'" & product.WeekProductRow.TravelYear.ToString("dd/MM/yyyy") & "', ") ' ,<FbdEffDate, datetime,>
        result.Append("'" & product.WeekProductRow.FbwBookStart.ToString("dd/MM/yyyy") & "', ") ' ,<FbdBookFrom, datetime,>
        result.Append(Utility.EscapeSqlString(prdShortName) & ", ") ' ,<FbdPrdId, varchar(64),>
        result.Append("1, ") ' ,<IntegrityNo, int,>
        result.Append("2, ") ' ,<AddUsrId, varchar(64),>
        result.Append("NULL, ") ' ,<ModUsrId, varchar(64),>
        result.Append("GETDATE(), ") ' ,<AddDateTime, datetime,>
        result.Append("NULL, ") ' ,<ModDateTime, datetime,>
        result.Append("'FlxToAurora')") ' ,<AddPrgmName, varchar(256),>)
        result.AppendLine()

        'result.Append("INSERT INTO FlexTrigger (FltId, FltTableName, FltRecordId, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime, AddPrgmName) ")
        'result.Append("VALUES (NEWID(), 'FlexBookedDate', @id, 1, 2, NULL, GETDATE(), NULL, 'FlxToAurora')")
        'result.AppendLine()

        result.AppendLine()

        ''------------------------------------------------------------------------------
        '' REV:MIA FEB12012 MOVE BELOW
        ''added FbcType in the insert
        ''------------------------------------------------------------------------------

        ' book date child (product rates)
        For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateDataTable
            result.AppendLine(Utility.EscapeSqlSingleLineComment("Week No: " + flexBookingWeekRateRow.FlrWkNo.ToString() + " - " + flexBookingWeekRateRow.FlrTravelFrom.ToString("dd/MM/yyyy")))
            result.AppendLine("SET @idchild = NEWID()")
            result.AppendLine("INSERT INTO FlexBookedDateChild (FbcId, FbcFbdId, FbcTravelFrom, FbcFlexNum, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime, AddPrgmName,FbcType) ")
            result.Append("VALUES (")
            result.Append("@idchild, ") ' (<FbcId, varchar(64),>
            result.Append("@id, ") ' ,<FbcFbdId, varchar(64),>
            result.Append("'" & flexBookingWeekRateRow.FlrTravelFrom.ToString("dd/MM/yyyy") & "', ") ' ,<FbcTravelFrom, datetime,>
            result.Append(flexBookingWeekRateRow.FlrFlexNum.ToString() & ", ") ' ,<FbcFlexNum, int,>
            result.Append("1, ") ' ,<IntegrityNo, int,>
            result.Append("2, ") ' ,<AddUsrId, varchar(64),>
            result.Append("NULL, ") ' ,<ModUsrId, varchar(64),>
            result.Append("GETDATE(), ") ' ,<AddDateTime, datetime,>
            result.Append("NULL, ") ' ,<ModDateTime, datetime,>
            result.Append("'FlxToAurora', ") ' ,<AddPrgmName, varchar(256),>)

            ''rev:mia Jan31 2012 
            ''-------------------
            result.Append(IIf(String.IsNullOrEmpty(flexBookingWeekRateRow.FlrFbwType), "'I'", "'" & flexBookingWeekRateRow.FlrFbwType & "'"))
            result.Append(" )")
            ''-------------------

            result.AppendLine()

            'result.Append("INSERT INTO FlexTrigger (FltId, FltTableName, FltRecordId, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime, AddPrgmName) ")
            'result.AppendLine("VALUES (NEWID(), 'FlexBookedDateChild', @idchild, 1, 2, NULL, GETDATE(), NULL, 'FlxToAurora')")
        Next
        result.AppendLine()

        ' exceptions
        For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In product.FlexBookingWeekExceptionDataTable
            If (flexBookingWeekExceptionRow.IsFleIdNull()) Then Continue For

            result.AppendLine("-- exception")
            result.AppendLine("SET @id = NEWID()")
            result.AppendLine("INSERT INTO FlexException (fexId,FexCtyCode,FexBookStart,FexBookEnd,FexTravelStart,FexTravelEnd,FexBrand,IntegrityNo,AddUsrId,ModUsrId,AddDateTime,ModDateTime,AddPrgmName)")
            result.Append("VALUES (")
            result.Append("@id, ") ' (<fexId, varchar(64),>
            result.Append(Utility.EscapeSqlString(ctyCode) & ", ") ' ,<FexCtyCode, varchar(3),>
            result.Append("'" & flexBookingWeekExceptionRow.FleBookStart.ToString("dd/MM/yyyy") & "', ") ' ,<FexBookStart, datetime,>
            result.Append("'" & flexBookingWeekExceptionRow.FleBookEnd.ToString("dd/MM/yyyy") & "', ") ' ,<FexBookEnd, datetime,>
            result.Append("'" & flexBookingWeekExceptionRow.FleTravelStart.ToString("dd/MM/yyyy") & "', ") ' ,<FexTravelStart, datetime,>
            result.Append("'" & flexBookingWeekExceptionRow.FleTravelEnd.ToString("dd/MM/yyyy") & "', ") ' ,<FexTravelEnd, datetime,>
            result.Append(Utility.EscapeSqlString(brdCode) & ", ") ' ,<FexBrand, varchar(64),>
            result.Append("1, ") ' ,<IntegrityNo, int,>
            result.Append("2, ") ' ,<AddUsrId, varchar(64),>
            result.Append("NULL, ") ' ,<ModUsrId, varchar(64),>
            result.Append("GETDATE(), ") ' ,<AddDateTime, datetime,>
            result.Append("NULL, ") ' ,<ModDateTime, datetime,>
            result.Append("'FlxToAurora')") ' ,<AddPrgmName, varchar(256),>)
            result.AppendLine()

            'result.Append("INSERT INTO FlexTrigger (FltId, FltTableName, FltRecordId, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime, AddPrgmName) ")
            'result.Append("VALUES (NEWID(), 'FlexException', @id, 1, 2, NULL, GETDATE(), NULL, 'FlxToAurora')")
            'result.AppendLine()

            result.AppendLine("SET @idchild = NEWID()")
            result.AppendLine("INSERT INTO FlexExceptionChild (FecId, FecfexId, FecClaId, FecPrdShortName, FecFromCity, FecToCity, FecDelta, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime, AddPrgmName) ")
            result.Append("VALUES (")
            result.Append("@idchild, ") ' (<FecId, varchar(64),>
            result.Append("@id, ") ' ,<FecfexId, varchar(64),>
            result.Append("'', ") ' ,<FecClaId, varchar(64),>
            result.Append(Utility.EscapeSqlString(prdShortName) & ", ") ' ,<FecPrdShortName, varchar(8),>
            If Not flexBookingWeekExceptionRow.IsFleLocCodeFromNull Then
                result.Append(Utility.EscapeSqlString(flexBookingWeekExceptionRow.FleLocCodeFrom) & ", ") ' ,<FecPrdShortName, varchar(8),>
            Else
                result.Append("NULL, ") ' ,<FecFromCity, varchar(3),>
            End If
            If Not flexBookingWeekExceptionRow.IsFleLocCodeToNull Then
                result.Append(Utility.EscapeSqlString(flexBookingWeekExceptionRow.FleLocCodeTo) & ", ") ' ,<FecPrdShortName, varchar(8),>
            Else
                result.Append("NULL, ") ' ,<FecToCity, varchar(3),>
            End If
            result.Append(flexBookingWeekExceptionRow.FleDelta.ToString() & ", ") ' ,<FecDelta, int,>
            result.Append("1, ") ' ,<IntegrityNo, int,>
            result.Append("2, ") ' ,<AddUsrId, varchar(64),>
            result.Append("NULL, ") ' ,<ModUsrId, varchar(64),>
            result.Append("GETDATE(), ") ' ,<AddDateTime, datetime,>
            result.Append("NULL, ") ' ,<ModDateTime, datetime,>
            result.Append("'FlxToAurora')") ' ,<AddPrgmName, varchar(256),>)
            result.AppendLine()

            'result.Append("INSERT INTO FlexTrigger (FltId, FltTableName, FltRecordId, IntegrityNo, AddUsrId, ModUsrId, AddDateTime, ModDateTime, AddPrgmName) ")
            'result.AppendLine("VALUES (NEWID(), 'FlexExceptionChild', @idchild, 1, 2, NULL, GETDATE(), NULL, 'FlxToAurora')")
            'result.AppendLine()
        Next
        result.AppendLine()
    End Sub

    Public Overrides Function GenerateText(ByVal export As Export) As String

        If export.Products.Count = 0 Then Return ""

        Dim result As New StringBuilder
        result.AppendLine(Utility.EscapeSqlSingleLineComment(export.WeekExportRow.BookingWeekDescription))
        result.AppendLine("EXEC flex_prepareflextables")
        result.AppendLine("DECLARE @id AS UniqueIdentifier")
        result.AppendLine("DECLARE @idchild AS UniqueIdentifier")
        result.AppendLine()

        For Each product As Product In export.Products
            GenerateTextProduct(export, product, result, Nothing)

            ' do flex sub-products in this group that are also set to get these flex rates
            Dim flexProduct As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
            If flexProduct Is Nothing Then Continue For

            For Each flexSubProduct As FlexProductRow In export.FlexProductDataTable
                If Not flexSubProduct.IsFxpParentIdNull _
                 AndAlso flexSubProduct.FxpId <> flexProduct.FxpId _
                 AndAlso flexSubProduct.FxpParentId = flexProduct.FxpId _
                 AndAlso flexSubProduct.FxpIsActive Then
                    GenerateTextProduct(export, product, result, flexSubProduct)
                End If
            Next
        Next

        Return result.ToString()
    End Function

End Class
