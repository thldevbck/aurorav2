Imports System.Text
Imports System.IO
Imports System.Xml

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

<ExportGeneratorAttribute("SPECIALXML")> _
Public Class SpecialXmlGenerator
    Inherits ExportGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String

        If export.Products.Count = 0 _
         OrElse export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Dim currentWkNo As Integer = export.CurrentWkNoByTravelYear(export.WeekExportRow.FwxTravelYear)

        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
        Dim displayAprilFlexRecord As Boolean = export.DisplayAprilFlexRecord(export.Products(0).WeekProductRow.BrdCode, export.Products(0).WeekProductRow.TravelYear.Year)

        Using result As New StringWriterUTF8()
            Using w As New XmlTextWriter(result)
                w.Formatting = Formatting.Indented
                w.Indentation = 2

                w.WriteStartDocument(True)
                w.WriteComment("Flex Rate Update " + export.WeekExportRow.FileName)
                w.WriteStartElement("FlexRates")

                Dim firstProduct = True

                If export.Products.Count > 0 _
                 AndAlso Not export.WeekExportRow.IsFwxTravelYearNull() Then

                    For Each product As Product In export.Products
                        If Not export.IsProductValid(product) Then Continue For

                        ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
                        Dim itemFlexProductRow As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                        Dim displayVehicleInAuroraExportOnly As Boolean = CBool(itemFlexProductRow.FxpAuroraExportOnly)
                        If (displayVehicleInAuroraExportOnly) Then
                            Logging.LogWarning("GenerateText: ", "product.WeekProductRow.PrdId :" & product.WeekProductRow.PrdId & ",displayVehicleInAuroraExportOnly:" & displayVehicleInAuroraExportOnly)
                            Continue For
                        End If

                        If firstProduct Then
                            w.WriteElementString("BookStart", product.WeekProductRow.FbwBookStart.ToString)
                            w.WriteElementString("TrvYear", product.WeekProductRow.TravelYear.ToString)
                            w.WriteElementString("Country", product.WeekProductRow.CtyName)
                            w.WriteElementString("Brand", product.WeekProductRow.BrdName)
                            w.WriteStartElement("Vehicles")
                            firstProduct = False
                        End If

                        w.WriteStartElement("Vehicle")
                        w.WriteElementString("Code", product.WeekProductRow.PrdShortName)
                        w.WriteStartElement("Dates")

                        For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(export.WeekExportRow.FwxTravelYear)
                            If flexWeekNumberRow.FwnWkNo < currentWkNo Then Continue For

                            If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                                ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                                Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                                   " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                                   " Does Not Fall On Monday")
                                ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                                If (Not displayAprilFlexRecord) Then
                                    Continue For
                                End If
                            End If

                            w.WriteStartElement("TravelStart")
                            w.WriteElementString("From", flexWeekNumberRow.FwnTrvWkStart.ToString())
                            ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                            If (Not displayAprilFlexRecord) Then
                                w.WriteElementString("To", flexWeekNumberRow.FwnTrvWkEnd.ToString())
                            Else
                                Dim nextMonday As Date = export.GetMonday(DayOfWeek.Sunday, flexWeekNumberRow.FwnTrvWkStart)
                                w.WriteElementString("To", nextMonday)
                            End If

                            w.WriteStartElement("Cities")

                            For Each locationsRow As LocationsRow In export.LocationsDataTable
                                If Not export.IsProductAndLocationValid(product, locationsRow.LocCode) Then Continue For

                                ''------------------------------------------------------------------------
                                ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                                Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                                Dim rate As String = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False)))
                                ''------------------------------------------------------------------------

                                ''Dim rate As String = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode))

                                w.WriteStartElement("City")
                                w.WriteElementString("Name", locationsRow.LocCode)
                                w.WriteElementString("FlexRate", rate)
                                w.WriteEndElement()             ' Close City
                            Next

                            w.WriteEndElement()                 ' Cities
                            w.WriteEndElement()                 ' TravelStart
                        Next

                        w.WriteEndElement()                 ' Dates
                        w.WriteEndElement()                 ' Vehicle
                    Next

                End If

                If Not firstProduct Then
                    w.WriteEndElement()                 ' Vehicles
                    firstProduct = False
                End If

                w.WriteEndElement()                 ' flexrates
                w.WriteEndDocument()
            End Using

            Return result.ToString()
        End Using
    End Function


End Class
