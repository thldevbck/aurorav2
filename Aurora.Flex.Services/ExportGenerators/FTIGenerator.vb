Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


<ExportGeneratorAttribute("FTI")> _
Public Class FTIGenerator
    Inherits ExportGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 OrElse Not export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Dim travelYears As New List(Of Date)
        For Each product As Product In export.Products
            If Not travelYears.Contains(product.WeekProductRow.TravelYear) Then travelYears.Add(product.WeekProductRow.TravelYear)
        Next
        travelYears.Sort()
        If travelYears.Count = 0 Then Return ""

        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
        Dim displayAprilFlexRecord As Boolean = export.DisplayAprilFlexRecord(export.Products(0).WeekProductRow.BrdCode, export.Products(0).WeekProductRow.TravelYear.Year)

        Dim result As New StringBuilder
        For Each travelYear As Date In travelYears
            For Each locationsRow As LocationsRow In export.LocationsDataTable
                Dim isValidLocation = False
                For Each product As Product In export.Products
                    If product.WeekProductRow.TravelYear <> travelYear _
                     OrElse Not export.IsProductAndLocationValid(product, locationsRow.LocCode) Then Continue For
                    isValidLocation = True
                Next
                If Not isValidLocation Then Continue For

                result.AppendLine("Ex-" + locationsRow.LocName)
                result.Append("  Pick Up Week->")

                For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(travelYear)
                    System.Diagnostics.Debug.WriteLine("Travel Year: " & travelYear & ", flexWeekNumberRow.FwnTrvWkStart: " & flexWeekNumberRow.FwnTrvWkStart & ", flexWeekNumberRow.FwnTrvWkStart: " & flexWeekNumberRow.FwnTrvWkStart.DayOfWeek)
                    ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                    If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                        System.Diagnostics.Debug.WriteLine("Travel Year: " & travelYear & ", flexWeekNumberRow.FwnTrvWkStart: " & flexWeekNumberRow.FwnTrvWkStart & ", flexWeekNumberRow.FwnTrvWkStart: " & flexWeekNumberRow.FwnTrvWkStart.DayOfWeek & " SKIP")
                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                        If (Not displayAprilFlexRecord) Then
                            Continue For
                        End If
                    End If
                    result.Append(";" + flexWeekNumberRow.FwnTrvWkStart.ToString("dd/MM/yy"))
                Next
                result.AppendLine()

                For Each product As Product In export.Products
                    If product.WeekProductRow.TravelYear <> travelYear _
                     OrElse Not export.IsProductAndLocationValid(product, locationsRow.LocCode) Then Continue For

                    ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
                    Dim itemFlexProductRow As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                    Dim displayVehicleInAuroraExportOnly As Boolean = CBool(itemFlexProductRow.FxpAuroraExportOnly)
                    If (displayVehicleInAuroraExportOnly) Then
                        Logging.LogWarning("GenerateText: ", "product.WeekProductRow.PrdId :" & product.WeekProductRow.PrdId & ",displayVehicleInAuroraExportOnly:" & displayVehicleInAuroraExportOnly)
                        Continue For
                    End If

                    Dim flexProduct As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                    If flexProduct Is Nothing OrElse flexProduct.IsFxpFTICodeNull OrElse String.IsNullOrEmpty(flexProduct.FxpFTICode) Then Continue For

                    result.Append(flexProduct.FxpFTICode)

                    For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(travelYear)
                        If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                            ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                            Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                               " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                               " Does Not Fall On Monday")
                            ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                            If (Not displayAprilFlexRecord) Then
                                Continue For
                            End If
                        End If

                        ''------------------------------------------------------------------------
                        ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                        Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                        Dim rate As String = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False)))
                        ''------------------------------------------------------------------------

                        Dim fleetStatus As String = product.FleetStatusByWkNo(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode)
                        If fleetStatus = FlexConstants.FleetStatus_None Then
                            fleetStatus = FlexConstants.FleetStatus_Unavailable
                        ElseIf FlexExportConfiguration.Instance.IsAlwayRequestLocation(locationsRow.LocCode) Then
                            fleetStatus = FlexConstants.FleetStatus_Request
                        End If

                        result.Append(";" + fleetStatus + rate + " ")
                    Next
                    result.AppendLine()
                Next
                result.AppendLine()
            Next
        Next

        result.AppendLine("End of Report")
        Return result.ToString()
    End Function

End Class
