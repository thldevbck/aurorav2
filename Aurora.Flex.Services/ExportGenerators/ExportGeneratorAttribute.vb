Imports Microsoft.VisualBasic

<AttributeUsage(AttributeTargets.Class)> Public Class ExportGeneratorAttribute
    Inherits Attribute

    Private _code As String

    Public ReadOnly Property Code() As String
        Get
            Return _code
        End Get
    End Property

    Sub New(ByVal code As String)
        _code = code
    End Sub

End Class