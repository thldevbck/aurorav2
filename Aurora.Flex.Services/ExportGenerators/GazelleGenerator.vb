Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


<ExportGeneratorAttribute("GAZELLE")> _
Public Class GazelleGenerator
    Inherits ExportGenerator

    Public Shared ReadOnly InititialBlanks As String = New String(",", 16)

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse Not export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Dim travelYears As New List(Of Date)
        For Each product As Product In export.Products
            If Not travelYears.Contains(product.WeekProductRow.TravelYear) Then travelYears.Add(product.WeekProductRow.TravelYear)
        Next
        travelYears.Sort()
        If travelYears.Count = 0 Then Return ""

        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
        Dim displayAprilFlexRecord As Boolean = export.DisplayAprilFlexRecord(export.Products(0).WeekProductRow.BrdCode, export.Products(0).WeekProductRow.TravelYear.Year)

        Dim result As New StringBuilder
        ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        Dim gazelleproduct As String = FlexExportConfiguration.Instance.GazelleProductColumns
        ''result.AppendLine("beg_date,end_date,location" + InititialBlanks + FlexExportConfiguration.Instance.GazelleProductColumns)

        For Each locationsRow As LocationsRow In export.LocationsDataTable
            If Not export.IsLocationValid(locationsRow.LocCode) Then Continue For

            For Each travelYear As Date In travelYears
                For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(travelYear)
                    ''System.Diagnostics.Debug.Assert(flexWeekNumberRow.FwnTrvWkStart.ToString("MM/dd/yyyy") <> "03/21/2016", flexWeekNumberRow.FwnTrvWkEnd)
                    If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                        ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                        Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                           " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                           " Does Not Fall On Monday")

                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                        If (Not displayAprilFlexRecord) Then
                            Continue For
                        End If

                    End If

                    If flexWeekNumberRow.IsPast(export.WeekExportRow.FbwBookStart.AddDays(-7)) _
                     OrElse flexWeekNumberRow.FwnTrvYearStart > export.WeekExportRow.FbwTravelYearEnd Then Continue For

                    result.Append(flexWeekNumberRow.FwnTrvWkStart.ToString("MM/dd/yyyy") + ",")

                    ''-------------------------------------------------------------------------------------
                    ''rev:mia 23Sept2015- hack. check of there's another week before ending for flex year
                    ''-------------------------------------------------------------------------------------
                    If (flexWeekNumberRow.FwnWkNo >= 52) Then
                        If (flexWeekNumberRow.FwnTrvWkStart.AddDays(7) <= flexWeekNumberRow.FwnTrvWkEnd) Then
                            result.Append(flexWeekNumberRow.FwnTrvWkStart.AddDays(6).ToString("MM/dd/yyyy") + ",")
                        Else
                            result.Append(flexWeekNumberRow.FwnTrvWkEnd.ToString("MM/dd/yyyy") + ",")
                        End If
                    Else
                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                        If (Not displayAprilFlexRecord) Then
                            result.Append(flexWeekNumberRow.FwnTrvWkEnd.ToString("MM/dd/yyyy") + ",")
                        Else
                            Dim nextMonday As Date = export.GetMonday(DayOfWeek.Sunday, flexWeekNumberRow.FwnTrvWkStart)
                            result.Append(nextMonday.ToString("MM/dd/yyyy") + ",")
                        End If
                    End If
                    ''-------------------------------------------------------------------------------------


                    result.Append(FlexExportConfiguration.Instance.GetIndustryLocationCode(locationsRow.LocCode))
                    result.Append(InititialBlanks)

                    Dim first As Boolean = True
                    For Each productShortName As String In FlexExportConfiguration.Instance.GazelleProductColumns.Split(",")
                        If first Then first = False Else result.Append(",")

                        productShortName = productShortName.Trim().ToUpper()
                        If String.IsNullOrEmpty(productShortName) Then Continue For

                        Dim product As Product = export.GetValidProductByNameLocationTravelYear(productShortName, locationsRow.LocCode, flexWeekNumberRow.FwnTrvYearStart)
                        If product Is Nothing Then Continue For

                        ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
                        Dim itemFlexProductRow As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                        Dim displayVehicleInAuroraExportOnly As Boolean = CBool(itemFlexProductRow.FxpAuroraExportOnly)
                        If (displayVehicleInAuroraExportOnly) Then
                            gazelleproduct = gazelleproduct.Replace(product.WeekProductRow.PrdShortName, ",")
                            Logging.LogWarning("GenerateText: ", "product.WeekProductRow.PrdId :" & product.WeekProductRow.PrdId & ",displayVehicleInAuroraExportOnly:" & displayVehicleInAuroraExportOnly)
                            Continue For
                        End If

                        ''------------------------------------------------------------------------
                        ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                        Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                        Dim rate As String = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False)))
                        ''------------------------------------------------------------------------


                        Dim fleetStatus As String = product.FleetStatusByWkNo(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode)

                        If fleetStatus = FlexConstants.FleetStatus_None _
                         OrElse fleetStatus = FlexConstants.FleetStatus_Unavailable Then
                            result.Append("NA")
                        ElseIf FlexExportConfiguration.Instance.IsAlwayRequestLocation(locationsRow.LocCode) _
                         OrElse fleetStatus = FlexConstants.FleetStatus_Request Then
                            result.Append("RQ" + rate)
                        Else
                            result.Append(rate)
                        End If
                    Next

                    result.AppendLine()
                Next
            Next
        Next
        ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
        result.Insert(0, "beg_date,end_date,location" + InititialBlanks + gazelleproduct)
        Return result.ToString()
    End Function

End Class
