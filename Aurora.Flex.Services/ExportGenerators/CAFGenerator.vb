Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


<ExportGeneratorAttribute("CAF")> _
Public Class CAFGenerator
    Inherits ExportGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
        Dim displayAprilFlexRecord As Boolean = export.DisplayAprilFlexRecord(export.Products(0).WeekProductRow.BrdCode, export.Products(0).WeekProductRow.TravelYear.Year)

        Dim result As New StringBuilder
        For Each locationsRow As LocationsRow In export.LocationsDataTable
            If Not export.IsLocationValid(locationsRow.LocCode) Then Continue For

            result.AppendLine("Ex-" + locationsRow.LocName)
            result.Append("  Pick Up Week->")

            For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(export.WeekExportRow.FwxTravelYear)
                result.Append(";" + flexWeekNumberRow.FwnTrvWkStart.ToString("dd/MM/yy"))
            Next
            result.AppendLine()

            For Each product As Product In export.Products
                If Not export.IsProductAndLocationValid(product, locationsRow.LocCode) Then Continue For

                ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
                Dim itemFlexProductRow As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                Dim displayVehicleInAuroraExportOnly As Boolean = CBool(itemFlexProductRow.FxpAuroraExportOnly)
                If (displayVehicleInAuroraExportOnly) Then
                    Logging.LogWarning("GenerateText: ", "product.WeekProductRow.PrdId :" & product.WeekProductRow.PrdId & ",displayVehicleInAuroraExportOnly:" & displayVehicleInAuroraExportOnly)
                    Continue For
                End If


                Dim flexProduct As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                If flexProduct Is Nothing OrElse flexProduct.IsFxpCAFCodeNull OrElse String.IsNullOrEmpty(flexProduct.FxpCAFCode) Then Continue For

                result.Append(flexProduct.FxpCAFCode)

                For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(export.WeekExportRow.FwxTravelYear)

                    If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                        ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                        Logging.LogWarning("GenerateText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                           " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                           " Does Not Fall On Monday")

                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                        If (Not displayAprilFlexRecord) Then
                            Continue For
                        End If
                    End If

                    ''------------------------------------------------------------------------
                    ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                    Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                    Dim rate As String = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False)))
                    ''------------------------------------------------------------------------

                    ''Dim rate As String = FlexConstants.FlexNumToRate(product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode))
                    result.Append("; " + rate + " ")
                Next
                result.AppendLine()
            Next
            result.AppendLine()
        Next

        result.AppendLine("End of Report")
        Return result.ToString()
    End Function

End Class
