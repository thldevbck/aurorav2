Imports System.Xml
Imports System.Xml.Xpath
Imports System.Xml.Serialization
Imports System.Configuration


Public Class FlexExportConfiguration

    Public GazelleProductColumns As String = "2B4WDB,2BB,2BBXS,2BTSB,4B4WDB,4BB,6BB,PFMR,4BBXS,,,,,,,,,,"
    Public AuroraToIndustryLocationCodes As String = "HBT:HBA"
    Public AlwaysRequestLocations As String = "WLG,ZQN"

    Public DefaultBookingWeekText As String = "The flex rates are attached for your reference. Please do not hesitate to contact your dedicated account manager should you have any queries. To unsubscribe to the Flex updates please reply to this email with 'Unsubscribe' in the subject line." & vbCrLf & _
"" & vbCrLf & _
"Yours Sincerely," & vbCrLf & _
"Britz flex rate team"


    ''' <summary>
    ''' Is the fleet status of this aurora location code always "request"?
    ''' </summary>
    Public Function IsAlwayRequestLocation(ByVal locCode As String) As Boolean
        Return New List(Of String)(AlwaysRequestLocations.ToUpper().Split(",")) _
            .Contains(("" & locCode).Trim().ToUpper())
    End Function

    ''' <summary>
    ''' Get the industry standard location code for external or agent reports
    ''' </summary>
    Public Function GetIndustryLocationCode(ByVal locCode As String) As String
        For Each pair As String In AuroraToIndustryLocationCodes.Split(",")
            Dim pairValues() As String = pair.Split(":")
            If pairValues.Length <> 2 Then Continue For
            If pairValues(0) = locCode Then Return pairValues(1)
        Next
        Return locCode
    End Function

    Private Shared _instance As FlexExportConfiguration
    Public Shared ReadOnly Property Instance() As FlexExportConfiguration
        Get
            If _instance Is Nothing Then _instance = CType(ConfigurationManager.GetSection("FlexExportConfiguration"), FlexExportConfiguration)
            If _instance Is Nothing Then _instance = New FlexExportConfiguration()
            Return _instance
        End Get
    End Property

End Class


Public Class FlexExportConfigurationSectionHandler
    Implements IConfigurationSectionHandler

    Public Function Create(ByVal parent As Object, ByVal configContext As Object, _
        ByVal section As System.Xml.XmlNode) As Object _
        Implements System.Configuration.IConfigurationSectionHandler.Create

        Dim xs As XmlSerializer = New XmlSerializer(GetType(FlexExportConfiguration))
        Return xs.Deserialize(New XmlNodeReader(section))
    End Function

End Class
