Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset

Public Class BaseDERGenerator
    Inherits ExportGenerator

    Public Function GenerateDERText(ByVal export As Export, ByVal writeAuroraCodes As Boolean, ByVal writeFlexNum As Boolean, ByVal writeIndustryLocations As Boolean, ByVal fleetStatusQuotes As Boolean, ByVal alwaysRQ As Boolean, ByVal travelYear As DateTime, ByVal writeBookStart As Boolean) As String
        Dim currentWkNo As Integer = export.CurrentWkNoByTravelYear(travelYear)

        Dim result As New StringBuilder
        If writeBookStart Then result.AppendLine("""FIRST-BOOK-DATE"",""" + export.WeekExportRow.FbwBookStart.ToString("MM/dd/yyyy") + """")

        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
        Dim displayAprilFlexRecord As Boolean = export.DisplayAprilFlexRecord(export.Products(0).WeekProductRow.BrdCode, export.Products(0).WeekProductRow.TravelYear.Year)

        For Each locationsRow As LocationsRow In export.LocationsDataTable
            If Not export.IsLocationValid(locationsRow.LocCode) Then Continue For

            For Each product As Product In export.Products
                If (product.WeekProductRow.TravelYear <> travelYear) _
                  OrElse Not export.IsProductAndLocationValid(product, locationsRow.LocCode) Then Continue For

                ''rev:mia 16-Nov-2016 - https://thlonline.atlassian.net/browse/AURORA-1107
                Dim itemFlexProductRow As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                Dim displayVehicleInAuroraExportOnly As Boolean = CBool(itemFlexProductRow.FxpAuroraExportOnly)
                If (displayVehicleInAuroraExportOnly) Then
                    Logging.LogWarning("GenerateText: ", "product.WeekProductRow.PrdId :" & product.WeekProductRow.PrdId & ",displayVehicleInAuroraExportOnly:" & displayVehicleInAuroraExportOnly)
                    Continue For
                End If


                For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberRowsByTravelYear(travelYear)
                    If flexWeekNumberRow.FwnWkNo < currentWkNo Then Continue For

                    If (flexWeekNumberRow.FwnTrvWkStart.DayOfWeek <> DayOfWeek.Monday) Then
                        ''rev:mia sept 14 2015 - Block 01-Apr from ALL flex files if it's not Monday
                        Logging.LogWarning("GenerateDERText: ", "FlexWeekNumberRow.FwnWkNo :" & flexWeekNumberRow.FwnWkNo & _
                                           " FlexWeekNumberRow.FwnTrvWkStart :" & flexWeekNumberRow.FwnTrvWkStart & _
                                           " Does Not Fall On Monday")

                        
                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                        If (Not displayAprilFlexRecord) Then
                            Continue For
                        End If

                    End If


                    Dim line As New StringBuilder
                    If writeIndustryLocations Then
                        line.Append("""" + FlexExportConfiguration.Instance.GetIndustryLocationCode(locationsRow.LocCode) + """,")
                    Else
                        line.Append("""" + locationsRow.LocCode + """,")
                    End If

                    If writeAuroraCodes Then
                        line.Append("""" + product.WeekProductRow.PrdShortName + """,")
                    Else
                        Dim flexProduct As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
                        If flexProduct Is Nothing OrElse flexProduct.IsFxpDERCodeNull OrElse String.IsNullOrEmpty(flexProduct.FxpDERCode) Then Continue For

                        line.Append("""" + (flexProduct.FxpDERCode + New String(" ", 8)).Substring(0, 8) + """,")
                    End If
                    line.Append("""" + (product.WeekProductRow.PrdName + New String(" ", 8)).Substring(0, 8) + """,")

                    line.Append("""" + flexWeekNumberRow.FwnTrvWkStart.ToString("MM/dd/yyyy") + """,")




                    ''-------------------------------------------------------------------------------------
                    ''rev:mia 23Sept2015- hack. check of there's another week before ending for flex year
                    ''-------------------------------------------------------------------------------------
                    If (flexWeekNumberRow.FwnWkNo >= 52) Then
                        ''System.Diagnostics.Debug.Assert(flexWeekNumberRow.FwnWkNo < 52, " week no is equal or greater than 52")
                        If (flexWeekNumberRow.FwnTrvWkStart.AddDays(7) <= flexWeekNumberRow.FwnTrvWkEnd) Then
                            line.Append("""" + flexWeekNumberRow.FwnTrvWkStart.AddDays(6).ToString("MM/dd/yyyy") + """,")
                        Else
                            ''rev:mia 04NOV2015 - exception to the rules
                            ''DER Flex Rate needs to be fixed to accommodate our latest change (53 weeks)
                            If (flexWeekNumberRow.FwnTrvWkStart.AddDays(7) > flexWeekNumberRow.FwnTrvWkEnd) Then
                                line.Append("""" + flexWeekNumberRow.FwnTrvWkStart.AddDays(6).ToString("MM/dd/yyyy") + """,")
                            Else
                                line.Append("""" + flexWeekNumberRow.FwnTrvWkEnd.ToString("MM/dd/yyyy") + """,")
                            End If

                        End If



                    Else
                        ''REV:MIA JULY 06 2013 - https://thlonline.atlassian.net/browse/AURORA-1009 01 April 2017 rate in maui Flex file for 1718
                        If (Not displayAprilFlexRecord) Then
                            line.Append("""" + flexWeekNumberRow.FwnTrvWkEnd.ToString("MM/dd/yyyy") + """,")
                        Else
                            Dim nextMonday As Date = export.GetMonday(DayOfWeek.Sunday, flexWeekNumberRow.FwnTrvWkStart)
                            line.Append("""" + nextMonday.ToString("MM/dd/yyyy") + """,")
                        End If
                    End If
                    ''-------------------------------------------------------------------------------------


                    ''------------------------------------------------------------------------
                    ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                    Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                    Dim flexNum As Integer = product.GetAdjustedFlexNumByWkNoLocCode(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False))
                    ''------------------------------------------------------------------------


                    Dim rate As String = FlexConstants.FlexNumToRate(flexNum)

                    If writeFlexNum Then
                        line.Append("""" + flexNum.ToString() + """,")
                    Else
                        line.Append("""" + rate + """,")
                    End If

                    Dim fleetStatus As String = product.FleetStatusByWkNo(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode)
                    If alwaysRQ Then
                        fleetStatus = "RQ"
                    ElseIf fleetStatus = FlexConstants.FleetStatus_None _
                     OrElse fleetStatus = FlexConstants.FleetStatus_Unavailable Then
                        fleetStatus = "NA"
                    ElseIf FlexExportConfiguration.Instance.IsAlwayRequestLocation(locationsRow.LocCode) _
                     OrElse fleetStatus = FlexConstants.FleetStatus_Request Then
                        fleetStatus = "RQ"
                    End If

                    If fleetStatusQuotes Then
                        line.Append("""" + fleetStatus + """")
                    Else
                        line.Append(fleetStatus)
                    End If

                    result.AppendLine(line.ToString())
                Next
            Next
        Next

        Return result.ToString()
    End Function

End Class


<ExportGeneratorAttribute("DER")> _
Public Class DERGenerator
    Inherits BaseDERGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Return GenerateDERText(export, writeAuroraCodes:=False, writeFlexNum:=True, writeIndustryLocations:=False, fleetStatusQuotes:=False, alwaysRQ:=True, travelYear:=export.WeekExportRow.FwxTravelYear, writeBookStart:=True) _
            & "END OF FILE" & vbCrLf
    End Function

End Class


<ExportGeneratorAttribute("DER2")> _
Public Class DER2Generator
    Inherits BaseDERGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse Not export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Dim travelYears As New List(Of Date)
        For Each product As Product In export.Products
            If Not travelYears.Contains(product.WeekProductRow.TravelYear) Then travelYears.Add(product.WeekProductRow.TravelYear)
        Next
        travelYears.Sort()
        If travelYears.Count = 0 Then Return ""

        Dim result As New StringBuilder
        Dim writeBookStart As Boolean = True
        For Each travelYear As Date In travelYears
            result.Append(GenerateDERText(export, writeAuroraCodes:=False, writeFlexNum:=True, writeIndustryLocations:=True, fleetStatusQuotes:=True, alwaysRQ:=True, travelYear:=travelYear, writeBookStart:=writeBookStart))
            writeBookStart = False
        Next
        Return result.ToString() & "END OF FILE" & vbCrLf
    End Function

End Class


<ExportGeneratorAttribute("DER3")> _
Public Class DER3Generator
    Inherits BaseDERGenerator

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        Return GenerateDERText(export, writeAuroraCodes:=True, writeFlexNum:=False, writeIndustryLocations:=True, fleetStatusQuotes:=False, alwaysRQ:=True, travelYear:=export.WeekExportRow.FwxTravelYear, writeBookStart:=True) _
            & "END OF FILE" & vbCrLf
    End Function

End Class
