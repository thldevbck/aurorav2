Imports System.IO
Imports System.Text

'''<summary>
''' Implements a TextWriter for writing information to a string in UTF8, rather then the default UTF16 .NET
''' uses internally for its string representation
''' </summary>
Public Class StringWriterUTF8
    Inherits StringWriter

    Public Sub New()
        MyBase.New()
    End Sub

    Public Overrides ReadOnly Property Encoding() As Encoding
        Get
            Return System.Text.Encoding.UTF8
        End Get
    End Property

End Class

