Imports System.Reflection

Public Class ExportGenerator

    Public Overridable Function GenerateText(ByVal export As Export) As String
        Throw New NotImplementedException()
    End Function

    Private Shared Function GetGeneratorType(ByVal code As String) As Type
        Dim assembly As Assembly = System.Reflection.Assembly.GetAssembly(GetType(ExportGenerator))
        For Each type As Type In assembly.GetTypes()
            Dim attributes As Object() = type.GetCustomAttributes(False)
            For Each attribute As Attribute In attributes
                If TypeOf attribute Is ExportGeneratorAttribute _
                 AndAlso CType(attribute, ExportGeneratorAttribute).Code = code Then
                    Return type
                End If
            Next
        Next

        Return Nothing
    End Function

    Public Shared Function Generate(ByVal export As Export) As String
        If export Is Nothing Then
            Throw New ArgumentNullException()
        End If

        Dim generatorType As Type = GetGeneratorType(export.WeekExportRow.FlxCode)
        If generatorType Is Nothing Then
            Return ""
        End If

        Dim generator As ExportGenerator = Activator.CreateInstance(generatorType)
        Return generator.GenerateText(export)
    End Function

End Class
