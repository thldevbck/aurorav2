Imports System.Text
Imports System.IO
Imports System.Xml

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


''' <summary>
''' Produces Xml for a product summary which can be consumed by a stylesheet for the email that the scheduler sends out.
''' One of the more complicated export types due to the fact it works across travel years and it uses a very customized 
''' format thats different from the structure of the data.
''' </summary>
<ExportGeneratorAttribute("SUMMARY")> _
Public Class SummaryGenerator
    Inherits ExportGenerator

    Public Class ProductWeekSummary
        Public Product As Product
        Public FlexBookingWeekRateRow As FlexBookingWeekRateRow
    End Class

    Public Const MaxWeeks As Integer = 8

    Public Const MaxLines As Integer = 30
    Public Const MaxHeaderLines As Integer = 7
    Public Const StartLines As Integer = 4

    Public Class ProductSummary
        Public Code As String
        Public ProductShortName As String
        Public Name As String
        Public BrandCode As String
        Public CountryCode As String
        Public CountryName As String

        Public Package As Package
        Public LocationNameList As New List(Of String)
        Public LocationRowList As New Dictionary(Of String, LocationsRow)
        Public ProductWeekSummaryList(MaxWeeks) As ProductWeekSummary
    End Class

    Public Overrides Function GenerateText(ByVal export As Export) As String
        If export.Products.Count = 0 _
         OrElse export.WeekExportRow.IsFlxBrdCodeNull _
         OrElse Not export.WeekExportRow.IsFwxTravelYearNull() Then Return ""

        ' Get the week numbers to use (across travel years if need be)
        'Dim _flexWeekNumberDataTable As FlexWeekNumberDataTable = DataRepository.GetFlexWeekNumbers(export.WeekExportRow.FbwTravelYearEnd)
        'Dim maxrow As DataRow() = _flexWeekNumberDataTable.Select("max(FwnWkNo)")
        'Dim intmaxyear As Integer = Convert.ToInt16(maxrow(0)("FwnWkNo"))
        Dim isYearChange As Boolean = False
        Dim FwnTrvYearStart As New Date(1900, 1, 1)
        Dim FwnTrvWkStart As New Date(1900, 1, 1)

        Dim flexWeekNumberList As New List(Of FlexWeekNumberRow)
        For Each flexWeekNumberRow As FlexWeekNumberRow In export.FlexWeekNumberDataTable
            If flexWeekNumberList.Count < MaxWeeks _
             AndAlso (flexWeekNumberRow.IsCurrent(export.WeekExportRow.FbwBookStart) OrElse flexWeekNumberRow.IsFuture(export.WeekExportRow.FbwBookStart)) Then

                'System.Diagnostics.Debug.WriteLine("flexWeekNumberRow.FwnTrvYearStart : " & flexWeekNumberRow.FwnTrvYearStart _
                '                                   & vbCrLf & "flexWeekNumberRow.FwnTrvWkStart : " & flexWeekNumberRow.FwnTrvWkStart _
                '                                   & vbCrLf & "flexWeekNumberRow.FwnTrvWkEnd : " & flexWeekNumberRow.FwnTrvWkEnd _
                '                                   & vbCrLf & "isYearChange : " & isYearChange & vbCrLf)

                ''rev:mia 22July2014 - code to check if there is a change in flex rate year. 
                '                      Change year changes can be possibly     because it reach the April 1,xxxx rate and will read the next year flex or..
                '                      a new rate was introduced in the middle of the current flex rate year.
                '                      if isYearChange became true, then these to scenarios was found.

                If (isYearChange = False) Then
                    If (FwnTrvYearStart.Equals(New Date(1900, 1, 1))) Then
                        FwnTrvYearStart = flexWeekNumberRow.FwnTrvYearStart
                        FwnTrvWkStart = flexWeekNumberRow.FwnTrvWkStart
                        isYearChange = False
                    Else
                        If (flexWeekNumberRow.FwnTrvYearStart.Equals(FwnTrvYearStart)) Then
                            FwnTrvWkStart = flexWeekNumberRow.FwnTrvWkStart
                            ''do nothing
                        Else
                            If (flexWeekNumberRow.FwnTrvYearStart.Equals(flexWeekNumberRow.FwnTrvWkStart)) Then
                                FwnTrvYearStart = flexWeekNumberRow.FwnTrvYearStart
                                isYearChange = True
                            End If
                        End If
                    End If
                End If

                flexWeekNumberList.Add(flexWeekNumberRow)

            End If
        Next

        ' Build up a product summary list (might include several products per summary across travel years)
        ' Summary includes product details, locations, and weekly rates
        Dim productCodeList As New List(Of String)
        Dim productSummaryList As New Dictionary(Of String, ProductSummary)

        ''rev:mia Sept 3 2012 - Problem happen when brand has two products with the same name like were being returnd by the query.
        ''                    - The old codes overwritten the first product by the second one thus producing incorrect rate of 2013 or 
        ''                    - it's pulling the next year...eg:2013 will be pulled intead of 2012
        ''                    - Today in history (Sept 4 2012, my friend jerome and his family is going back to manila to
        ''                      seek medications for his ailing wife. May God Bless them.
        ''                     #1 - will hold the default date.  
        Dim travelyear As Date = New Date(1900, 1, 1)
        Dim aprilOneFlpIds As New Hashtable
        Dim key As String = ""
        Dim keyvalue As String = String.Empty
        Dim executeNewAprilRate As Boolean = False
        For Each product As Product In export.Products


            ' hack by shoel to fix backpackers woes! 11.11.8
            ''REV:MIA JULY152013 ADD 'Y' IN THIS CONDITION
            If export.WeekExportRow.FlxCode.ToUpper().Trim() = "SUMMARY" And _
               (export.WeekExportRow.FlxBrdCode.ToUpper().Trim() = "P" Or export.WeekExportRow.FlxBrdCode.ToUpper().Trim() = "Y") Then


                key = String.Empty
                Dim isDOM As Boolean = IIf(export.WeekExportRow.FileName.ToUpper.TrimEnd().Contains("DOM"), True, False)
                Dim value As String = product.FlexBookingWeekRateDataTable(0)("FlrFlpId").ToString

                'Dim temp As Aurora.Flex.Data.FlexDataset.FlexBookingWeekRateRow = product.FlexBookingWeekRateByWkNoForINTL_DOM(1)
                'System.Diagnostics.Debug.WriteLine("inside loop : " & vbCrLf & value & vbCrLf & product.FlexBookingWeekRateDataTable(0)("FlrFlpId").ToString & " - " & _
                '                                  product.FlexBookingWeekRateDataTable(0)("FlrTravelFrom").ToString & " - " & _
                '                                  product.FlexBookingWeekRateDataTable(0)("FlrWkNo").ToString & " - " &
                '                                  product.FlexBookingWeekRateDataTable(0)("FlrFlexNum").ToString & " - " & _
                '                                  temp.FlrTravelTo & " - " & _
                '                                  temp.FlrFlexNum & " - " & isDOM)


                If (isDOM) Then
                    If (isDOM = True And product.FlexBookingWeekRateDataTable(0)("FlrFbwType").ToString = "D") Then
                        key = String.Concat(product.WeekProductRow.PrdShortName, "-", _
                                                  product.WeekProductRow.CtyCode, "-", _
                                                  "D")
                    End If
                Else
                    key = ""
                    If (isDOM = False) Then
                        If (isDOM = False And product.FlexBookingWeekRateDataTable(0)("FlrFbwType").ToString = "I") Then
                            key = String.Concat(product.WeekProductRow.PrdShortName, "-", _
                                                      product.WeekProductRow.CtyCode, "-", _
                                                      "I")
                        End If
                    End If
                End If


                Dim travelfrom As Date = product.FlexBookingWeekRateDataTable(0)("FlrTravelFrom")

                If (Date.Compare(product.WeekProductRow.TravelYear, product.WeekProductRow.TravelYearStart) = 0 And _
                    Date.Compare(travelfrom, product.WeekProductRow.TravelYearStart) = 0 And Not String.IsNullOrEmpty(key)) Then

                    If (aprilOneFlpIds.Contains(key) = False) Then
                        aprilOneFlpIds.Add(key, value)
                    Else
                        keyvalue = aprilOneFlpIds(key)
                        keyvalue = String.Concat(keyvalue, "~", value)
                        aprilOneFlpIds(key) = keyvalue
                    End If
                End If




                Dim dtThisBookingWeekStart As Date = export.WeekExportRow.FbwBookStart
                Dim dtCurrentTravelYearStart As Date = New Date(dtThisBookingWeekStart.Year, 4, 1)
                ' try 1 Apr of the year

                If dtThisBookingWeekStart < dtCurrentTravelYearStart Then
                    ' past travel year
                    dtCurrentTravelYearStart = dtCurrentTravelYearStart.AddYears(-1)
                End If

                ''----------------------------------------------
                ''REV:MIA FEB162012 add comment in this condition
                ''REV:MIA JULY152013 REMOVE comment in this condition
                ''If (product.WeekProductRow.TravelYear <> dtCurrentTravelYearStart) Then Continue For
            End If
            ' hack by shoel to fix backpackers woes! 11.11.8

            ' find our flex product defintion
            Dim flexProduct As FlexProductRow = export.GetFlexProduct(product.WeekProductRow.CtyCode, product.WeekProductRow.BrdCode, product.WeekProductRow.PrdId)
            If flexProduct Is Nothing OrElse flexProduct.IsFxpPkgCodeNull OrElse String.IsNullOrEmpty(flexProduct.FxpPkgCode) Then Continue For

            ' loop through the packages valid for this week product and get the package with the matching package code in the flex product
            Dim weekProductPackagesRow As WeekProductPackagesRow = Nothing
            For Each weekProductPackagesRow0 As WeekProductPackagesRow In product.WeekProductPackagesDataTable
                If weekProductPackagesRow0.PkgCode = flexProduct.FxpPkgCode Then
                    weekProductPackagesRow = weekProductPackagesRow0
                End If
            Next
            If weekProductPackagesRow Is Nothing Then Continue For

            Dim productSummary As ProductSummary

            ''rev:mia Sept 3 2012: stored traveller year here if its in default date
            If (travelyear.Equals(New Date(1900, 1, 1))) Then
                travelyear = weekProductPackagesRow.TravelYear
            End If

            Dim code As String = String.Concat(product.WeekProductRow.BrdCode, "-", product.WeekProductRow.CtyCode, "-", product.WeekProductRow.PrdShortName)

            ''rev:mia Sept 3 2012 - #2: candidate for fix for 8 week errors..whoah..just add another text in the keycode .
            ''                           make fix simple to avoid fixing other dlls related to this summary
            Dim debug As New StringBuilder
            debug.AppendFormat("FlrFlpId:{0}{1}", product.FlexBookingWeekRateDataTable(0)("FlrFlpId").ToString, vbCrLf)
            debug.AppendFormat("FlrTravelFrom:{0}{1}", Convert.ToDateTime(product.FlexBookingWeekRateDataTable(0)("FlrTravelFrom").ToString).ToString("dd/MM/yyyy"), vbCrLf)
            debug.AppendFormat("FlrWkNo:{0}{1}", product.FlexBookingWeekRateDataTable(0)("FlrWkNo").ToString, vbCrLf)
            debug.AppendFormat("FlrFlexNum:{0}{1}", product.FlexBookingWeekRateDataTable(0)("FlrFlexNum").ToString, vbCrLf)
            debug.AppendFormat("Travelyear:{0}{1}", travelyear.ToString("dd/MM/yyyy"), vbCrLf)
            debug.AppendFormat("Week Product Packages TravelYear:{0}{1}", weekProductPackagesRow.TravelYear.ToString("dd/MM/yyyy"), vbCrLf)


            If (productCodeList.Contains(code) = True) Then

                If (travelyear.Equals(weekProductPackagesRow.TravelYear)) Then
                    code = String.Concat(product.WeekProductRow.BrdCode, "-", product.WeekProductRow.CtyCode, "-", product.WeekProductRow.PrdShortName, "-", weekProductPackagesRow.FlpId)
                    debug.AppendFormat("Both Travel Year IS equal {0}-{1}{2}", weekProductPackagesRow.TravelYear.Year, travelyear.ToString("yyyy"), vbCrLf)
                    'System.Diagnostics.Debug.WriteLine(code & " IS NOT UNIQUE (1). " & vbCrLf & debug.ToString)
                Else
                    ''reV:mia Feb.12 2013- today in history. Pope Benedict resigned from his office citing his old age
                    ''                     preventing him to do his holy work.
                    ''                     his last day will be on feb.28 2013

                    If (Date.Compare(travelyear, weekProductPackagesRow.TravelYear) = -1) Then
                        debug.AppendFormat("***Both Travel Year are NOT equal***{0}", vbCrLf)
                        'code = String.Concat(product.WeekProductRow.BrdCode, "-", product.WeekProductRow.CtyCode, "-", product.WeekProductRow.PrdShortName, "-", weekProductPackagesRow.FlpId)


                        ''rev:mia - 22July2014 - Filter only year equal to current year and disregard future year
                        Dim currentYear As Int32 = DateTime.Now.Year
                        If (weekProductPackagesRow.TravelYear.Year.Equals(currentYear) And isYearChange = False) Then
                            debug.AppendFormat("Both Travel Year IS EQUAL TO CURRENT YEAR {0}-{1}{2}", weekProductPackagesRow.TravelYear.Year, currentYear, vbCrLf)
                            code = String.Concat(product.WeekProductRow.BrdCode, "-", product.WeekProductRow.CtyCode, "-", product.WeekProductRow.PrdShortName, "-", weekProductPackagesRow.FlpId)
                            '' System.Diagnostics.Debug.WriteLine(code & " IS NOT UNIQUE (2)." & vbCrLf & debug.ToString)
                        ElseIf (Not weekProductPackagesRow.TravelYear.Year.Equals(currentYear) And isYearChange = True) Then
                            debug.AppendFormat("Both Travel Year IS NOT EQUAL TO CURRENT YEAR BUT THERE IS OVERLAPPING IN FLEX RATE YEAR {0}-{1}{2}", weekProductPackagesRow.TravelYear.Year, currentYear, vbCrLf)
                            code = String.Concat(product.WeekProductRow.BrdCode, "-", product.WeekProductRow.CtyCode, "-", product.WeekProductRow.PrdShortName, "-", weekProductPackagesRow.FlpId)
                            ''System.Diagnostics.Debug.WriteLine(code & " IS NOT UNIQUE (3)." & vbCrLf & debug.ToString)
                        ElseIf isYearChange = True Then
                            debug.AppendFormat("Both Travel Year IS NOT EQUAL TO CURRENT YEAR BUT ISYEARCHANGE = TRUE {0}-{1}{2}", weekProductPackagesRow.TravelYear.Year, currentYear, vbCrLf)
                            code = String.Concat(product.WeekProductRow.BrdCode, "-", product.WeekProductRow.CtyCode, "-", product.WeekProductRow.PrdShortName, "-", weekProductPackagesRow.FlpId)
                            ''System.Diagnostics.Debug.WriteLine(code & " IS NOT UNIQUE (4)." & vbCrLf & debug.ToString)
                        Else
                            debug.AppendFormat("Both Travel Year IS NOT EQUAL TO CURRENT YEAR {0}-{1}{2}", weekProductPackagesRow.TravelYear.Year, currentYear, vbCrLf)
                            ''System.Diagnostics.Debug.WriteLine(code & " IS NOT UNIQUE (5)." & vbCrLf & debug.ToString)
                            Continue For
                        End If

                    Else
                        ''just do nothing and exit
                        Exit For
                    End If
                End If
            Else
                ''System.Diagnostics.Debug.WriteLine(code & " IS UNIQUE " & vbCrLf & debug.ToString)
                debug.AppendFormat("{0}{1}{2}", code, " IS UNIQUE ", vbCrLf)
            End If
            Logging.LogDebug("SUMMARY GENERATOR", vbCrLf & vbCrLf & debug.ToString())

            '' do we have a summary for this basic product already?
            '' If Not productSummaryList.ContainsKey(IIf(flpIdIsadded = True, code.Substring(code.LastIndexOf("-")), code)) Then
            If Not productSummaryList.ContainsKey(code) Then
                productSummary = New ProductSummary()
                ' set basic attributes
                productSummary.Code = code
                productSummary.ProductShortName = product.WeekProductRow.PrdShortName
                productSummary.Name = product.WeekProductRow.PrdName
                productSummary.BrandCode = product.WeekProductRow.BrdCode
                productSummary.CountryCode = product.WeekProductRow.CtyCode
                productSummary.CountryName = product.WeekProductRow.CtyName
                productSummary.Package = New Package(weekProductPackagesRow) ' load the package

                productSummaryList.Add(code, productSummary)
                productCodeList.Add(code)
            Else
                productSummary = productSummaryList(code)
            End If

            ' find locations
            For Each locationsRow As LocationsRow In export.LocationsDataTable
                If export.IsProductAndLocationValid(product, locationsRow.LocCode) _
                 AndAlso Not productSummary.LocationNameList.Contains(locationsRow.LocName) Then
                    productSummary.LocationNameList.Add(locationsRow.LocName)
                    productSummary.LocationRowList.Add(locationsRow.LocName, locationsRow)
                End If
            Next
            productSummary.LocationNameList.Sort()

            ' find weekly rates
            Dim index As Integer = 0


            ' Another weird hack by Shoel - This is to prevent a situation wherein there are less than 7 weeks worth of data in the mailer

            Dim alAddedItemDetails As ArrayList = New ArrayList


            For Each flexWeekNumberRow As FlexWeekNumberRow In flexWeekNumberList
                For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateDataTable
                    If flexWeekNumberRow.FwnTrvYearStart <> product.WeekProductRow.FbwTravelYearStart _
                     OrElse flexWeekNumberRow.FwnWkNo <> flexBookingWeekRateRow.FlrWkNo Then Continue For


                    ''----------------------------------------------
                    ''REV:MIA FEB162012 add this condition to check
                    If index > productSummary.ProductWeekSummaryList.Length - 1 Then
                        Continue For
                    End If

                    Dim productWeekSummary As New ProductWeekSummary()
                    productWeekSummary.Product = product
                    productWeekSummary.FlexBookingWeekRateRow = flexBookingWeekRateRow
                    productSummary.ProductWeekSummaryList(index) = productWeekSummary

                    ''rev:mia Sept 3 2012: - #3 reenlist the code to get the latest and recent
                    ''                          summary of products                                                  
                    If (index = productSummary.ProductWeekSummaryList.Length - 1) Then
                        productSummaryList.Remove(code)
                        productSummaryList.Add(code, productSummary)
                    End If

                    ' add the details of this thingummajig so that we don't add it again in part2!
                    alAddedItemDetails.Add(flexWeekNumberRow.FwnTrvWkStart.ToShortDateString() & " TO " & flexWeekNumberRow.FwnTrvWkEnd.ToShortDateString())
                    ' found a thingummajig, ergo we increment the counter
                    index += 1
                Next
                ' increment only if u found a valid thingummajig!
                'index += 1
            Next


            ' missing some stuff - we aint got 8 weeks worth of data!!!! - A.k.a - Part2
            If index < 8 Then
                ' go over the whole list again, because its in no discernable order :'(
                For Each flexWeekNumberRow As FlexWeekNumberRow In flexWeekNumberList
                    For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In product.FlexBookingWeekRateDataTable

                        ' this has to be a crossover period, ignore the year check 
                        If flexWeekNumberRow.FwnWkNo <> flexBookingWeekRateRow.FlrWkNo Then Continue For

                        ' make sure this thingummajig wasn't added already!
                        If alAddedItemDetails.Contains(flexWeekNumberRow.FwnTrvWkStart.ToShortDateString() & " TO " & flexWeekNumberRow.FwnTrvWkEnd.ToShortDateString()) Then Continue For

                        ' gotten till here, impressive! this must be a thingummajig to fill in the missing spaces post current travel year!
                        Dim productWeekSummary As New ProductWeekSummary()
                        productWeekSummary.Product = product
                        productWeekSummary.FlexBookingWeekRateRow = flexBookingWeekRateRow
                        productSummary.ProductWeekSummaryList(index) = productWeekSummary

                        ''rev:mia Sept 3 2012: - #3 reenlist the code to get the latest and recent
                        ''                          summary of products                                                  
                        If (index = productSummary.ProductWeekSummaryList.Length - 1) Then
                            productSummaryList.Remove(code)
                            productSummaryList.Add(code, productSummary)
                        End If

                        alAddedItemDetails.Add(flexWeekNumberRow.FwnTrvWkStart.ToShortDateString() & " TO " & flexWeekNumberRow.FwnTrvWkEnd.ToShortDateString())
                        index += 1
                        ' got 8 thngummajigs? beat it!
                        If index >= 8 Then Exit For
                    Next
                    ' got 8 thngummajigs? beat it!
                    If index >= 8 Then Exit For
                Next
            End If
            ' missing some stuff - we aint got 8 weeks worth of data!!!! - A.k.a - Part2

            ' Another weird hack by Shoel - This is to prevent a situation wherein there are less than 7 weeks worth of data in the mailer

        Next
        keyvalue = String.Empty
        productCodeList.Sort()

        Dim lines As Integer = StartLines

        ' We now have our summary information, lets write it out to Xml
        Using result As New StringWriterUTF8()
            Using w As New System.Xml.XmlTextWriter(result)
                w.Formatting = Formatting.Indented
                w.Indentation = 2



                Dim flexnumForNextProduct As String = ""
                Dim brandStop As Boolean = False

                System.Diagnostics.Debug.WriteLine("---looping to productlist---")
                For Each productCode As String In productCodeList
                    keyvalue = String.Empty
                    executeNewAprilRate = False
                    If (Not String.IsNullOrEmpty(flexnumForNextProduct)) Then
                        If (productCode.Contains(flexnumForNextProduct) = True) Then Continue For
                    End If

                   

                    Dim productSummary As ProductSummary = productSummaryList(productCode)

                    Dim pageBreak As String
                    Dim productLines As Integer = MaxHeaderLines + productSummary.LocationNameList.Count
                    If (lines + productLines) > MaxLines Then
                        pageBreak = "always"
                        lines = 0
                    Else
                        pageBreak = "auto"
                        lines += productLines
                    End If

                    ''------------------------------------------------------------------------
                    ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                    Dim filenameHeader As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                    ''------------------------------------------------------------------------

                    w.WriteStartElement("Product")
                    w.WriteAttributeString("ProductShortName", productSummary.ProductShortName)
                    w.WriteAttributeString("Name", productSummary.Name)
                    w.WriteAttributeString("BrandCode", productSummary.BrandCode)
                    w.WriteAttributeString("CountryCode", productSummary.CountryCode)

                    ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                    w.WriteAttributeString("CountryName", productSummary.CountryName & IIf(filenameHeader.Contains("DOM"), "-Domestic", "-International"))

                    w.WriteAttributeString("PageBreak", pageBreak)

                    w.WriteStartElement("Headers")
                    Dim index As Integer = 0
                    Dim lastWeekProcess As Integer = -1
                    brandStop = False

                    For Each flexWeekNumberRow As FlexWeekNumberRow In flexWeekNumberList
                        w.WriteStartElement("Header")
                        w.WriteAttributeString("Index", index.ToString())
                        w.WriteAttributeString("Day", flexWeekNumberRow.FwnTrvWkStart.Day.ToString())
                        w.WriteAttributeString("Month", flexWeekNumberRow.FwnTrvWkStart.ToString("MMM"))
                        w.WriteAttributeString("Year", flexWeekNumberRow.FwnTrvWkStart.Year.ToString())
                        w.WriteEndElement() ' Header
                        index += 1
                    Next
                    w.WriteEndElement() ' Headers

                    w.WriteStartElement("Locations")
                    For Each locationName As String In productSummary.LocationNameList
                        ''System.Diagnostics.Debug.Assert(locationName <> "Alice Springs")
                        keyvalue = String.Empty
                        executeNewAprilRate = False
                        Dim locationsRow As LocationsRow = productSummary.LocationRowList(locationName)

                        w.WriteStartElement("Location")
                        w.WriteAttributeString("LocationCode", locationsRow.LocCode)
                        w.WriteAttributeString("Name", locationsRow.LocName)

                        w.WriteStartElement("Rates")
                        index = 0
                        For Each flexWeekNumberRow As FlexWeekNumberRow In flexWeekNumberList


                            Dim flexNum As Integer = 1
                            Dim amount As Integer = 0

                            Dim productWeekSummary As ProductWeekSummary = productSummary.ProductWeekSummaryList(index)
                            If productWeekSummary IsNot Nothing Then
                                ''------------------------------------------------------------------------
                                ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
                                Dim filename As String = Trim(export.WeekExportRow.FileName.ToUpper.TrimEnd())
                                ''------------------------------------------------------------------------

                                ''System.Diagnostics.Debug.Assert(index < 7, "Time to trace")
                                'System.Diagnostics.Debug.WriteLine("WEEK NO: " & flexWeekNumberRow.FwnWkNo & _
                                '                                   ", TRAVEL YEAR: " & travelyear & _
                                '                                   ", WEEKSTART:  " & flexWeekNumberRow.FwnTrvWkStart & _
                                '                                   ", WEEKEND: " & flexWeekNumberRow.FwnTrvWkEnd & _
                                '                                   ", YEAR END: " & flexWeekNumberRow.FwnTrvYearStart)
                                ''REV:MIA FEB.13 2014 - TRAPPED THE 01-APRIL-XXXX WRONG VALUE
                                If (flexWeekNumberRow.FwnWkNo = 1 And Date.Compare(travelyear, flexWeekNumberRow.FwnTrvYearStart) = -1) Then
                                    If (flexWeekNumberRow.FwnTrvWkStart.Equals(New Date(Year(flexWeekNumberRow.FwnTrvYearStart), 4, 1))) Then
                                        '' System.Diagnostics.Debug.Assert(productWeekSummary.Product.WeekProductRow.PrdShortName <> "4Y")
                                        ''System.Diagnostics.Debug.WriteLine(productWeekSummary.Product.WeekProductRow.FlpId & " - " & productWeekSummary.Product.WeekProductRow.PrdShortName)
                                        key = String.Concat(productWeekSummary.Product.WeekProductRow.PrdShortName, "-", _
                                             productWeekSummary.Product.WeekProductRow.CtyCode, "-", _
                                             IIf(filename.Contains("DOM"), "D", "I"))

                                        keyvalue = aprilOneFlpIds(key)
                                        If (Not String.IsNullOrEmpty(keyvalue)) Then
                                            If (keyvalue.Contains("~") = True) Then
                                                keyvalue = keyvalue.Split("~")(1)
                                                keyvalue = keyvalue.Trim
                                            Else
                                                keyvalue = keyvalue.Trim
                                            End If
                                        Else
                                            System.Diagnostics.Debug.WriteLine("KEY NOT FOUND: " & key)
                                        End If
                                        executeNewAprilRate = True
                                    End If
                                End If

                             

                                '' System.Diagnostics.Debug.Assert(productWeekSummary.Product.WeekProductRow.PrdShortName <> "4Y" And locationsRow.LocCode <> "CNS" And flexWeekNumberRow.FwnWkNo <> 2)
                                If (Not String.IsNullOrEmpty(keyvalue)) Then
                                    Logging.LogInformation("GenerateText: ", vbCrLf & " ------------------------------------------------ " & vbCrLf & _
                                                                         "FlpId: " & keyvalue & vbCrLf & _
                                                                         "Travel Year: " & travelyear & vbCrLf & _
                                                                         "Travel Week Start: " & flexWeekNumberRow.FwnTrvWkStart & vbCrLf & _
                                                                         "Travel Year Start: " & flexWeekNumberRow.FwnTrvYearStart & vbCrLf & _
                                                                         "Week: " & flexWeekNumberRow.FwnWkNo & " for " & productWeekSummary.Product.WeekProductRow.PrdShortName & " TO " & locationsRow.LocCode & " (" & IIf(filename.Contains("DOM"), "DOMESTIC)", "INTERNATIONAL)") & vbCrLf)
                                    flexNum = productWeekSummary.Product.GetAdjustedFlexNumByWkNoLocCodeV2(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False), -1, True, Convert.ToInt32(keyvalue))
                                Else
                                    Logging.LogInformation("GenerateText: ", vbCrLf & " ------------------------------------------------ " & vbCrLf & _
                                                                        "Travel Year: " & travelyear & vbCrLf & _
                                                                         "Travel Week Start: " & flexWeekNumberRow.FwnTrvWkStart & vbCrLf & _
                                                                         "Travel Year Start: " & flexWeekNumberRow.FwnTrvYearStart & vbCrLf & _
                                                                         "Week: " & flexWeekNumberRow.FwnWkNo & " for " & productWeekSummary.Product.WeekProductRow.PrdShortName & " TO " & locationsRow.LocCode & " (" & IIf(filename.Contains("DOM"), "DOMESTIC)", "INTERNATIONAL)") & vbCrLf)
                                    flexNum = productWeekSummary.Product.GetAdjustedFlexNumByWkNoLocCodeV2(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False))
                                End If

                                ''System.Diagnostics.Debug.WriteLine("WEEK NO: " & flexWeekNumberRow.FwnWkNo & " : " & flexNum)
                                'System.Diagnostics.Debug.WriteLine(productCode & " -  " & _
                                '                                   productWeekSummary.Product.WeekProductRow.TravelYear & ", " & _
                                '                                   productWeekSummary.Product.WeekProductRow.FlpId & _
                                '                                   " : Weekno: " & flexWeekNumberRow.FwnWkNo & _
                                '                                   ", Flex:  " & flexNum & _
                                '                                   ", FwnTrvYearStart:  " & flexWeekNumberRow.FwnTrvYearStart)

                                ''rev:mia feb. 12 2013 - this is a new booking year w/c means it is april 1 20XX onward
                                If (Date.Compare(productWeekSummary.Product.WeekProductRow.TravelYear, flexWeekNumberRow.FwnTrvYearStart) = -1 And _
                                                flexWeekNumberRow.FwnWkNo >= 1 And _
                                                export.WeekExportRow.FlxBrdCode.ToUpper().Trim() <> "P") Then


                                    Dim indexOf As Integer = productCodeList.IndexOf(productCode) + 1
                                    Dim tempProductcode As String = ""

                                    For i As Integer = indexOf To productCodeList.Count - 1
                                        tempProductcode = productCodeList(i)

                                        If (tempProductcode.Split("-").Length = 4) Then
                                            If (tempProductcode.Substring(0, tempProductcode.LastIndexOf("-")).Trim = productCode.Trim) Then
                                                flexnumForNextProduct = tempProductcode.Split("-")(3)
                                                Exit For
                                            End If
                                        End If
                                    Next

                                    If (String.IsNullOrEmpty(flexnumForNextProduct)) Then
                                        flexnumForNextProduct = productWeekSummary.Product.WeekProductRow.FlpId
                                    End If


                                    ''reV:mia Feb 20.2014 - trapping part
                                    If (Not String.IsNullOrEmpty(flexnumForNextProduct)) Then
                                        If (Not String.IsNullOrEmpty(keyvalue)) Then
                                            If ((keyvalue.TrimEnd <> flexnumForNextProduct.TrimEnd) AndAlso executeNewAprilRate = True) Then
                                                keyvalue = flexnumForNextProduct
                                                flexNum = productWeekSummary.Product.GetAdjustedFlexNumByWkNoLocCodeV2(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False), Convert.ToInt32(flexnumForNextProduct))
                                            End If
                                        Else
                                            flexNum = productWeekSummary.Product.GetAdjustedFlexNumByWkNoLocCodeV2(flexWeekNumberRow.FwnWkNo, locationsRow.LocCode, IIf(filename.Contains("DOM"), True, False), Convert.ToInt32(flexnumForNextProduct))
                                        End If

                                    End If

                                Else
                                    ''REV:MIA feb 13 2013 - backpacker will be remove starting april 1
                                    If (Date.Compare(productWeekSummary.Product.WeekProductRow.TravelYear, flexWeekNumberRow.FwnTrvYearStart) = -1 And _
                                                flexWeekNumberRow.FwnWkNo = 1 And _
                                                export.WeekExportRow.FlxBrdCode.ToUpper().Trim() = "P") Then
                                        brandStop = True
                                        amount = 0

                                    ElseIf (Date.Compare(New Date(2013, 4, 1), flexWeekNumberRow.FwnTrvWkStart) = -1 And _
                                                flexWeekNumberRow.FwnWkNo >= 1 And _
                                                export.WeekExportRow.FlxBrdCode.ToUpper().Trim() = "P") Then
                                        brandStop = True
                                        amount = 0
                                    Else
                                        brandStop = False
                                    End If
                                End If

                                If (Not brandStop) Then
                                    ''rev:mia feb.18,2014 - call the DataRepository.GetFlexLevels(WeekProductPackagesRow.PkgId, WeekProductPackagesRow.SapId, WeekProductPackagesRow.TravelYear)
                                    Try
                                        If (Not String.IsNullOrEmpty(keyvalue) Or executeNewAprilRate = True) Then
                                            Dim PkgIdForNextYear As String = productSummary.Package.WeekProductPackagesRow.PkgId
                                            Dim SapIdForNextYear As String = productSummary.Package.WeekProductPackagesRow.SapId
                                            Dim TravelYearForNextYear As Date = DateAdd(DateInterval.Year, 1, productSummary.Package.WeekProductPackagesRow.TravelYear)
                                            Dim _flexLevelDataTableNexdtYear As FlexLevelDataTable = DataRepository.GetFlexLevels(PkgIdForNextYear, SapIdForNextYear, TravelYearForNextYear)

                                            ''hardcode this 6Y since its breaking all logic
                                            If (_flexLevelDataTableNexdtYear.Rows.Count - 1 = -1) Then
                                                _flexLevelDataTableNexdtYear = DataRepository.GetFlexLevels(PkgIdForNextYear, SapIdForNextYear, productSummary.Package.WeekProductPackagesRow.TravelYear)
                                            End If

                                            For Each flexLevelRowForNextYear As FlexLevelRow In _flexLevelDataTableNexdtYear
                                                If CInt(flexLevelRowForNextYear.FlxNum) = flexNum Then
                                                    amount = CInt(flexLevelRowForNextYear.FlxRate)
                                                    Exit For
                                                End If
                                            Next
                                            ''REV:MIA FEB 18, 2014-no need this one..
                                            'For Each flexLevelRow As FlexLevelRow In productSummary.Package.FlexLevelDataTable
                                            '    If CInt(flexLevelRow.FlxNum) = flexNum Then
                                            '        ''System.Diagnostics.Debug.WriteLine("orig: " & CInt(flexLevelRow.FlxRate) & ", updated: " & CInt(amount))
                                            '        Exit For
                                            '    End If
                                            'Next

                                        Else
                                            For Each flexLevelRow As FlexLevelRow In productSummary.Package.FlexLevelDataTable
                                                If CInt(flexLevelRow.FlxNum) = flexNum Then
                                                    amount = CInt(flexLevelRow.FlxRate)
                                                    Exit For
                                                End If
                                            Next
                                        End If
                                    Catch ex As Exception
                                        For Each flexLevelRow As FlexLevelRow In productSummary.Package.FlexLevelDataTable
                                            If CInt(flexLevelRow.FlxNum) = flexNum Then
                                                amount = CInt(flexLevelRow.FlxRate)
                                                Exit For
                                            End If
                                        Next
                                    End Try


                                End If
                                If (executeNewAprilRate = False) Then
                                    keyvalue = String.Empty
                                End If

                            End If

                            w.WriteStartElement("Rate")
                            w.WriteAttributeString("Index", index.ToString())
                            If (amount > 0) Then
                                w.WriteAttributeString("Amount", "$" & amount.ToString())
                                ''w.WriteAttributeString("Amount", "" & amount.ToString())
                            Else
                                If (brandStop = True) Then
                                    w.WriteAttributeString("Amount", "N/A")
                                Else
                                    w.WriteAttributeString("Amount", " ")
                                End If
                            End If
                            w.WriteEndElement() ' Rate

                            index += 1
                        Next

                        w.WriteEndElement() ' Rates
                        w.WriteEndElement() ' Location
                    Next

                    w.WriteEndElement() ' Locations
                    w.WriteEndElement() ' Product


                Next
            End Using

            Return result.ToString()
        End Using


    End Function

  
  

End Class
