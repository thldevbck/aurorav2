Imports System.Data.SqlClient

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset


''' <summary>
''' A <c>WeekProduct</c> holds information about a product (<c>FlexBookingWeekProduct</c>) for a specific 
''' booking week, including locations relevant for it, fleet status, packages, and its flex rates and 
''' exceptions.  If the booking week is current, the product may not physically exist in the database yet.
''' The product always holds 52 product rates (enforced by the database) and 20 exceptions (actual and mock,
''' enforced by the code)
''' The object also supports data validation and the ability to save the product, its rates and exceptions.  
''' </summary>
Public Class Product

    Private _weekProductRow As WeekProductsRow
    Public ReadOnly Property WeekProductRow() As WeekProductsRow
        Get
            Return _weekProductRow
        End Get
    End Property

    Private _locationsDataTable As LocationsDataTable
    Public ReadOnly Property LocationsDataTable() As LocationsDataTable
        Get
            Return _locationsDataTable
        End Get
    End Property

    Private _weekProductLocationAvailabilityDataTable As WeekProductLocationAvailabilityDataTable
    Public ReadOnly Property WeekProductLocationAvailabilityDataTable() As WeekProductLocationAvailabilityDataTable
        Get
            Return _weekProductLocationAvailabilityDataTable
        End Get
    End Property

    Private _fleetStatusDataTable As FleetStatusDataTable
    Public ReadOnly Property FleetStatusDataTable() As FleetStatusDataTable
        Get
            Return _fleetStatusDataTable
        End Get
    End Property

    Private _fleetStatusRow As FleetStatusRow
    Public ReadOnly Property FleetStatusRow() As FleetStatusRow
        Get
            Return _fleetStatusRow
        End Get
    End Property

    Private _weekProductPackagesDataTable As WeekProductPackagesDataTable
    Public ReadOnly Property WeekProductPackagesDataTable() As WeekProductPackagesDataTable
        Get
            Return _weekProductPackagesDataTable
        End Get
    End Property

    Private _flexBookingWeekRateDataTable As FlexBookingWeekRateDataTable
    Public ReadOnly Property FlexBookingWeekRateDataTable() As FlexBookingWeekRateDataTable
        Get
            Return _flexBookingWeekRateDataTable
        End Get
    End Property

    '''------------------------------------------------------------------------------
    ''' REV:MIA FEB12012 ByVal WithDomesticFlex As Boolean.this is a new function
    '''------------------------------------------------------------------------------
    Public ReadOnly Property Flex2_Flex2_GetDomesticNumber
        Get

        End Get
    End Property

    Private _HasDomesticRecord As Boolean
    Public ReadOnly Property HasDomesticRecord As Boolean
        Get
            Return _HasDomesticRecord
        End Get
    End Property

    '''------------------------------------------------------------------------------
    ''' REV:MIA FEB12012 ByVal WithDomesticFlex As Boolean.this is a new function
    '''------------------------------------------------------------------------------
    Public ReadOnly Property FlexBookingWeekRateRowsForINTL_DOM(Optional ByVal WithDomesticFlex As Boolean = False) As FlexBookingWeekRateRow()
        Get
            Dim flexBookingWeekRateRow() As FlexBookingWeekRateRow

            If (WithDomesticFlex = True) Then
                _HasDomesticRecord = True
                flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'D'")
                If (flexBookingWeekRateRow.Length - 1 = -1) Then
                    flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'I'")
                    _HasDomesticRecord = False
                End If
            Else
                flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'I'")

            End If
            Return flexBookingWeekRateRow
        End Get
    End Property

    Public ReadOnly Property FlexBookingWeekRateByWkNo(ByVal wkNo As Integer) As FlexBookingWeekRateRow
        Get
            For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable
                If flexBookingWeekRateRow.FlrWkNo = wkNo Then
                    Return flexBookingWeekRateRow
                End If
            Next
            Return Nothing
        End Get
    End Property

    '''------------------------------------------------------------------------------
    ''' REV:MIA FEB12012 ByVal WithDomesticFlex As Boolean
    '''------------------------------------------------------------------------------
    Public ReadOnly Property FlexBookingWeekRateByWkNoForINTL_DOM(ByVal wkNo As Integer, Optional ByVal WithDomesticFlex As Boolean = False) As FlexBookingWeekRateRow
        Get
            'For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable
            '    If flexBookingWeekRateRow.FlrWkNo = wkNo Then
            '        Return flexBookingWeekRateRow
            '    End If
            'Next
            'Return Nothing

            ''------------------------------------------------------------------------
            ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
            ''------------------------------------------------------------------------
            Dim flexBookingWeekRateRow() As FlexBookingWeekRateRow

            If (WithDomesticFlex = True) Then
                flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'D' and FlrWkNo = " & wkNo)
            Else
                flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'I' and FlrWkNo = " & wkNo)
            End If

            For Each item As FlexBookingWeekRateRow In flexBookingWeekRateRow
                If item.FlrWkNo = wkNo Then
                    Return item
                End If

            Next

            Return Nothing
        End Get
    End Property


    '''------------------------------------------------------------------------------
    ''' REV:MIA FEB12012 ByVal WithDomesticFlex As Boolean
    '''------------------------------------------------------------------------------
    Public ReadOnly Property GetAdjustedFlexNumByWkNoLocCode(ByVal wkNo As Integer, ByVal locCode As String, Optional ByVal WithDomesticFlex As Boolean = False) As Integer
        Get
            Dim result As FlexBookingWeekRateRow = Nothing

            ''REV:MIA FEB12012 commented this code
            'For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable
            '    Dim type As String = Trim(flexBookingWeekRateRow.FlrFbwType.TrimEnd())

            '    If flexBookingWeekRateRow.FlrWkNo = wkNo Then
            '        result = flexBookingWeekRateRow
            '    End If

            'Next



            ''------------------------------------------------------------------------
            ''REV:MIA FEB162012 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
            ''------------------------------------------------------------------------
            Dim flexBookingWeekRateRow(2) As FlexBookingWeekRateRow

            If (WithDomesticFlex = True) Then
                flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'D' and FlrWkNo = " & wkNo)
            Else
                flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'I' and FlrWkNo = " & wkNo)
                ''_flexBookingWeekRateDataTable.Select("FlrFbwType = 'I' and FlrWkNo = " & wkNo)(0)("FlrFlexNum")
                ''_flexBookingWeekRateDataTable.Select("FlrFbwType = 'I' and FlrWkNo = " & wkNo)
            End If

            For Each item As FlexBookingWeekRateRow In flexBookingWeekRateRow
                If item.FlrWkNo = wkNo Then
                    result = item
                End If
            Next
            ''------------------------------------------------------------------------


            If result Is Nothing Then Return FlexConstants.FlexNumDefault

            For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
                If flexBookingWeekExceptionRow.LocationCode = locCode _
                 AndAlso flexBookingWeekExceptionRow.FleTravelStart <= result.FlrTravelFrom _
                 AndAlso flexBookingWeekExceptionRow.FleTravelEnd >= result.FlrTravelFrom Then
                    Try
                        Dim flexInt As Integer = FlexConstants.FlexNumToInt(result.FlrFlexNum) + flexBookingWeekExceptionRow.FleDelta
                        If flexInt < FlexConstants.FlexIntMin Then flexInt = FlexConstants.FlexIntMin
                        If flexInt > FlexConstants.FlexIntMax Then flexInt = FlexConstants.FlexIntMax
                        Return FlexConstants.FlexIntToFlexNum(flexInt)
                    Catch ex As Exception
                        Return result.FlrFlexNum
                    End Try
                End If
            Next

            Return result.FlrFlexNum
        End Get
    End Property

 
    Private _flexBookingWeekExceptionDataTable As FlexBookingWeekExceptionDataTable
    Public ReadOnly Property FlexBookingWeekExceptionDataTable() As FlexBookingWeekExceptionDataTable
        Get
            Return _flexBookingWeekExceptionDataTable
        End Get
    End Property

    Public ReadOnly Property FleetStatusByWkNo(ByVal wkNo As Integer, Optional ByVal LocationCode As String = "") As String
        Get

            Dim sFleetStatus As String = ""

            If _weekProductRow.FbwStatus = FlexConstants.BookingWeekStatus_Sent Then
                If Not Me.FlexBookingWeekRateByWkNo(wkNo).IsFlrFleetStatusNull Then
                    sFleetStatus = Me.FlexBookingWeekRateByWkNo(wkNo).FlrFleetStatus
                Else
                    sFleetStatus = FlexConstants.FleetStatus_None
                End If
            Else
                ''rev:mia 10sept2015-I have created new travel date as 28-Mar-2015 week number 53 and it's not working on staging
                If Not _fleetStatusRow Is Nothing _
                 AndAlso wkNo >= FlexConstants.WeekNoMin _
                 AndAlso wkNo <= FlexConstants.GlobalWeekNoMax _
                 AndAlso Not _fleetStatusRow.IsNull("FstFleetStatus" + wkNo.ToString()) Then
                    If LocationCode.Trim().Equals(String.Empty) Then
                        sFleetStatus = _fleetStatusRow("FstFleetStatus" + wkNo.ToString())
                    Else
                        ' we have a loc code
                        ' look 4 exceptions
                        If Not _fleetStatusRow.ExceptionList.Trim().Equals(String.Empty) Then
                            Dim saExceptions As String()
                            saExceptions = _fleetStatusRow.ExceptionList.Trim().Split(","c)
                            For i As Integer = 0 To saExceptions.Length - 1
                                If String.IsNullOrEmpty(saExceptions(i)) OrElse String.IsNullOrEmpty(saExceptions(i).Trim()) Then
                                    Continue For
                                End If
                                ' we have a valid exception
                                ' format is WeekNo-LocCode-FleetStatus
                                Dim saException As String()
                                saException = saExceptions(i).Split("-"c)
                                If CInt(saException(0)) = wkNo AndAlso saException(1) = LocationCode Then
                                    sFleetStatus = saException(2)
                                    Exit For
                                End If
                            Next
                        End If
                    End If
                    If sFleetStatus.Trim().Equals(String.Empty) Then
                        ' couldnt find any exception :'(
                        sFleetStatus = _fleetStatusRow("FstFleetStatus" + wkNo.ToString())
                    End If
                Else
                    sFleetStatus = FlexConstants.FleetStatus_None
                End If
            End If

            Return sFleetStatus
        End Get
    End Property


    ''' <summary>
    ''' Is the specified product rate is the past?  (the travel date is before the booking week)
    ''' </summary>
    Public ReadOnly Property IsRatePast(ByVal flexBookingWeekRateRow As FlexBookingWeekRateRow) As Boolean
        Get
            Return flexBookingWeekRateRow.IsPast(WeekProductRow.FbwBookStart)
        End Get
    End Property

    ''' <summary>
    ''' Is the specified product rate current?  (the travel date is within the booking week)
    ''' </summary>
    Public ReadOnly Property IsRateCurrent(ByVal flexBookingWeekRateRow As FlexBookingWeekRateRow) As Boolean
        Get
            Return flexBookingWeekRateRow.IsCurrent(WeekProductRow.FbwBookStart)
        End Get
    End Property

    ''' <summary>
    ''' Is the specified product rate in the future?  (the travel date is after the booking week)
    ''' </summary>
    Public ReadOnly Property IsRateFuture(ByVal flexBookingWeekRateRow As FlexBookingWeekRateRow) As Boolean
        Get
            Return flexBookingWeekRateRow.IsFuture(WeekProductRow.FbwBookStart)
        End Get
    End Property

    ''' <summary>
    ''' Similar to IsRatePast/Current/Future, but just returned the current week number
    ''' </summary>
    Public ReadOnly Property CurrentWkNo() As Integer
        Get
            For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable
                If flexBookingWeekRateRow.IsCurrent(_weekProductRow.FbwBookStart) Then
                    Return flexBookingWeekRateRow.FlrWkNo
                End If
            Next
            Return Integer.MinValue
        End Get
    End Property

    Public Function HasLocation(ByVal locCode As String) As Boolean
        For Each locationsRow As LocationsRow In LocationsDataTable
            If locationsRow.LocCode = locCode Then
                Return True
            End If
        Next
        Return False
    End Function

    ''' <summary>
    ''' Is a location available for pickup?
    ''' </summary>
    Private Function IsLocationIncludedExcluded(ByVal locCode As String) As Boolean
        locCode = locCode.Trim().ToLower()

        ' loop thought the location availability rows
        For Each locAvailRow As WeekProductLocationAvailabilityRow In _weekProductLocationAvailabilityDataTable
            ' we are concerned about 3 things in the location availability record for this product;
            ' from location, to location, and whether available or not
            Dim lcaFromLocCode As String = ""
            Dim lcaToLocCode As String = ""
            Dim lcaIsNotAvailable As Boolean = False
            If Not locAvailRow.IsLcaFromLocCodeNull Then lcaFromLocCode = locAvailRow.LcaFromLocCode.Trim().ToLower()
            If Not locAvailRow.IsLcaToLocCodeNull Then lcaToLocCode = locAvailRow.LcaToLocCode.Trim().ToLower()
            If Not locAvailRow.IsLcaIsNotAvailableNull Then lcaIsNotAvailable = locAvailRow.LcaIsNotAvailable

            If lcaFromLocCode = locCode AndAlso lcaToLocCode = "" Then
                ' this from location matches and has been specifically mentioned as being availabe 
                ' or not availabe for all to locations, so return with the result immediately
                Return Not lcaIsNotAvailable
            End If
        Next

        ' we didnt find any "from" availability records that specifically applied to this location
        ' so we assume the location is available for pickup
        Return True
    End Function

    Public Function IsLocationIncluded(ByVal locCode As String) As Boolean
        Return HasLocation(locCode) AndAlso IsLocationIncludedExcluded(locCode)
    End Function

    Public Function IsLocationExcluded(ByVal locCode As String) As Boolean
        Return HasLocation(locCode) AndAlso Not IsLocationIncludedExcluded(locCode)
    End Function

    Public Function IsLocationException(ByVal locCode As String) As Boolean
        For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
            If flexBookingWeekExceptionRow.LocationCode = locCode Then
                Return True
            End If
        Next
        Return False
    End Function

    Private _locationsIncluded As String() = Nothing
    Public ReadOnly Property LocationsIncludedDescription() As String
        Get
            If _weekProductRow.FbwStatus = FlexConstants.BookingWeekStatus_Sent Then
                If Not _weekProductRow.IsFlpLocationsIncludeNull Then
                    Return _weekProductRow.FlpLocationsInclude
                Else
                    Return ""
                End If
            Else
                Return String.Join(",", _locationsIncluded)
            End If
        End Get
    End Property

    Private _locationsExcluded As String() = Nothing
    Public ReadOnly Property LocationsExcludedDescription() As String
        Get
            If _weekProductRow.FbwStatus = FlexConstants.BookingWeekStatus_Sent Then
                If Not _weekProductRow.IsFlpLocationsExcludeNull Then
                    Return _weekProductRow.FlpLocationsExclude
                Else
                    Return ""
                End If
            Else
                Return String.Join(",", _locationsExcluded)
            End If
        End Get
    End Property

    Private _flexWeekNumberDataTable As FlexWeekNumberDataTable
    Public ReadOnly Property FlexWeekNumberDataTable() As FlexWeekNumberDataTable
        Get
            Return _flexWeekNumberDataTable
        End Get
    End Property

    Private Sub Load()
        _locationsDataTable = DataRepository.GetLocations(WeekProductRow.ComCode, WeekProductRow.CtyCode)
        _weekProductLocationAvailabilityDataTable = DataRepository.GetLocationAvailability(WeekProductRow.ComCode, WeekProductRow.FlpId)
        _fleetStatusDataTable = DataRepository.GetFleetStatus(WeekProductRow.ComCode, WeekProductRow.CtyCode, WeekProductRow.BrdCode, WeekProductRow.TravelYear, WeekProductRow.PrdId)
        If Not _fleetStatusDataTable Is Nothing And _fleetStatusDataTable.Count > 0 Then
            _fleetStatusRow = _fleetStatusDataTable.Rows(0)
        End If


        _weekProductPackagesDataTable = DataRepository.GetWeekProductPackages(WeekProductRow.ComCode, WeekProductRow.FlpId)
        _flexBookingWeekRateDataTable = DataRepository.GetProductRates(WeekProductRow.FlpId)

        _flexBookingWeekExceptionDataTable = DataRepository.GetProductExceptions(WeekProductRow.FlpId)
        While _flexBookingWeekExceptionDataTable.Count < FlexConstants.ExceptionMax
            Dim flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow = _flexBookingWeekExceptionDataTable.NewFlexBookingWeekExceptionRow()
            flexBookingWeekExceptionRow.SetFleIdNull()
            flexBookingWeekExceptionRow.SetFleFlpIdNull()
            flexBookingWeekExceptionRow.SetFleLocCodeFromNull()
            flexBookingWeekExceptionRow.SetFleLocCodeToNull()
            flexBookingWeekExceptionRow.FleBookStart = Date.MinValue
            flexBookingWeekExceptionRow.FleBookEnd = Date.MinValue
            flexBookingWeekExceptionRow.FleTravelStart = Date.MinValue
            flexBookingWeekExceptionRow.FleTravelEnd = Date.MinValue
            flexBookingWeekExceptionRow.FleDelta = 0
            _flexBookingWeekExceptionDataTable.AddFlexBookingWeekExceptionRow(flexBookingWeekExceptionRow)
        End While

        Dim locationsIncluded As New List(Of String)
        Dim locationsExcluded As New List(Of String)
        For Each locationsRow As LocationsRow In LocationsDataTable
            If IsLocationIncluded(locationsRow.LocCode) Then locationsIncluded.Add(locationsRow.LocCode)
            If IsLocationExcluded(locationsRow.LocCode) Then locationsExcluded.Add(locationsRow.LocCode)
        Next
        _locationsIncluded = locationsIncluded.ToArray()
        _locationsExcluded = locationsExcluded.ToArray()
        _flexWeekNumberDataTable = DataRepository.GetFlexWeekNumbers(_weekProductRow.TravelYear)
    End Sub

    Public Sub New(ByVal weekProductRow As WeekProductsRow)
        If weekProductRow Is Nothing Then
            Throw New ArgumentNullException()
        End If

        _weekProductRow = weekProductRow
        Load()
    End Sub

    Public Sub New(ByVal comCode As String, ByVal flwId As Integer, ByVal flpId As Integer)
        If String.IsNullOrEmpty(comCode) Then
            Throw New ArgumentNullException()
        End If

        Dim weekProductsDataTable As WeekProductsDataTable = DataRepository.GetWeekProduct(comCode, flwId, flpId)
        If weekProductsDataTable Is Nothing Or weekProductsDataTable.Rows.Count = 0 Then
            Throw New Exception("Product does not exist")
        End If

        _weekProductRow = weekProductsDataTable(0)
        Load()
    End Sub

    ''' <summary>
    ''' Validate a specified product rate in isolation
    ''' </summary>
    Public Sub ValidateRate(ByVal flexBookingWeekRateRow As FlexBookingWeekRateRow)
        If Not FlexConstants.ValidateFlexNum(flexBookingWeekRateRow.FlrFlexNum) Then
            Throw New ValidationException("Invalid Rate", flexBookingWeekRateRow)
        End If
    End Sub

    Private Sub ValidateRates()
        For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable
            ValidateRate(flexBookingWeekRateRow)
        Next
    End Sub

    ''' <summary>
    ''' Validate a specified product exception in isolation
    ''' </summary>
    Public Sub ValidateException(ByVal flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow)
        If String.IsNullOrEmpty(flexBookingWeekExceptionRow.LocationCode) Then
            If flexBookingWeekExceptionRow.FleBookStart <> Date.MinValue _
             OrElse flexBookingWeekExceptionRow.FleBookEnd <> Date.MinValue Then
                Throw New ValidationException("Invalid Exception: Location is blank but booking dates are specified", flexBookingWeekExceptionRow)
            End If

            If flexBookingWeekExceptionRow.FleTravelStart <> Date.MinValue _
             OrElse flexBookingWeekExceptionRow.FleTravelEnd <> Date.MinValue Then
                Throw New ValidationException("Invalid Exception: Location is blank but travel dates are specified", flexBookingWeekExceptionRow)
            End If

            If flexBookingWeekExceptionRow.FleDelta <> 0 Then
                Throw New ValidationException("Invalid Exception: Location is blank but a change is specified", flexBookingWeekExceptionRow)
            End If
        Else
            If flexBookingWeekExceptionRow.FleBookStart = Date.MinValue _
             OrElse flexBookingWeekExceptionRow.FleBookEnd = Date.MinValue _
             OrElse flexBookingWeekExceptionRow.FleBookStart > flexBookingWeekExceptionRow.FleBookEnd Then
                Throw New ValidationException("Invalid Exception: Booking Dates are incorrect", flexBookingWeekExceptionRow)
            End If

            If flexBookingWeekExceptionRow.FleBookStart.DayOfWeek <> DayOfWeek.Monday _
             OrElse flexBookingWeekExceptionRow.FleBookEnd.DayOfWeek <> DayOfWeek.Sunday Then
                Throw New ValidationException("Invalid Exception: Booking Dates must start on a monday and end on a sunday", flexBookingWeekExceptionRow)
            End If

            If Not (_weekProductRow.FbwBookStart <= flexBookingWeekExceptionRow.FleBookEnd _
             AndAlso _weekProductRow.FbwBookEnd >= flexBookingWeekExceptionRow.FleBookStart) Then
                Throw New ValidationException("Invalid Exception: Booking Dates do not overlap with the current week", flexBookingWeekExceptionRow)
            End If

            If flexBookingWeekExceptionRow.FleTravelStart = Date.MinValue _
             OrElse flexBookingWeekExceptionRow.FleTravelEnd = Date.MinValue _
             OrElse flexBookingWeekExceptionRow.FleTravelStart > flexBookingWeekExceptionRow.FleTravelEnd Then
                Throw New ValidationException("Invalid Exception: Travel Dates are incorrect", flexBookingWeekExceptionRow)
            End If
            'Nyt: 13th Oct 2015 Ref:https://thlonline.atlassian.net/browse/AURORA-318
            'Comments : Existing code was throwing exception as while building week list from validateTravelStart and ValidateTravelEnd which was calling CalculateTravelYearDates to build list
            '           of weeks, it used a logic of week being minimum of 4 days thus for year starting 1st April 2016, 1st date was taken as begening of week and next week was 11th April 2016, thus
            '           week starting from 4th April was failing. As per discussion with rajesh, since we have week list in FlexBookingWeekRateDataTable, we are picking up week start from that data. 
            '           Existing logic was commented and new one was added
            'Change Starts
            'If Not FlexConstants.ValidateTravelStart(flexBookingWeekExceptionRow.FleTravelStart) _
            ' OrElse Not FlexConstants.ValidateTravelEnd(flexBookingWeekExceptionRow.FleTravelEnd) Then
            '    Throw New ValidationException("Invalid Exception: Travel Dates must start and end on a valid travel week (usually a monday and sunday except at the start and end of the travel year)", flexBookingWeekExceptionRow)
            'End If

            If Not FlexBookingWeekRateDataTable.Select("FlrTravelFrom = '" + flexBookingWeekExceptionRow.FleTravelStart + "'").Length > 0 _
                   OrElse Not FlexBookingWeekRateDataTable.Select("FlrTravelFrom = '" + flexBookingWeekExceptionRow.FleTravelEnd.AddDays(1) + "'").Length > 0 Then
                'Additional Validation to check if travel end date is equal to end of travel year as end of travel year is not returned in flexbookingweekratedatatable
                Dim EndOfTravelYearDate As DateTime = New Date(FlexBookingWeekRateDataTable.Rows(0)("FlrTravelFrom").Year + 1, 4, 1)
                If Not EndOfTravelYearDate.Equals(flexBookingWeekExceptionRow.FleTravelEnd.AddDays(1)) Then
                    Throw New ValidationException("Invalid Exception: Travel Dates must start and end on a valid travel week (usually a monday and sunday except at the start and end of the travel year)", flexBookingWeekExceptionRow)
                End If

            End If
            'Change Ends Nyt:13th Oct 2015  Ref:https://thlonline.atlassian.net/browse/AURORA-318
            If Not (_weekProductRow.FbwBookStart <= flexBookingWeekExceptionRow.FleTravelEnd _
             AndAlso _weekProductRow.TravelYearEnd >= flexBookingWeekExceptionRow.FleTravelStart) Then
                Throw New ValidationException("Invalid Exception: Travel Dates do not overlap with the current travel year (starting from the booking date)", flexBookingWeekExceptionRow)
            End If

            If flexBookingWeekExceptionRow.FleDelta < FlexConstants.FlexChangeMin _
             OrElse flexBookingWeekExceptionRow.FleDelta > FlexConstants.FlexChangeMax _
             OrElse flexBookingWeekExceptionRow.FleDelta = 0 Then
                Throw New ValidationException("Invalid Exception: Change not been specified", flexBookingWeekExceptionRow)
            End If
        End If

    End Sub

    Private Sub ValidateExceptions()
        For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
            ValidateException(flexBookingWeekExceptionRow)
        Next

        ' Ensure exception booking and travel periods dont collide with another booking or travel period for the
        ' same location
        For Each flexBookingWeekExceptionRow0 As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
            For Each flexBookingWeekExceptionRow1 As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
                If flexBookingWeekExceptionRow0 Is flexBookingWeekExceptionRow1 _
                 OrElse String.IsNullOrEmpty(flexBookingWeekExceptionRow0.LocationCode) _
                 OrElse String.IsNullOrEmpty(flexBookingWeekExceptionRow1.LocationCode) _
                 OrElse flexBookingWeekExceptionRow0.LocationCode <> flexBookingWeekExceptionRow1.LocationCode Then
                    Continue For
                End If

                'If Utility.DoesDateRangeIntersect( _
                ' flexBookingWeekExceptionRow0.FleBookStart, _
                ' flexBookingWeekExceptionRow0.FleBookEnd, _
                ' flexBookingWeekExceptionRow1.FleBookStart, _
                ' flexBookingWeekExceptionRow1.FleBookEnd) Then
                '    Throw New ValidationException("Invalid Exception: Booking Periods collide with another exception for the same location", flexBookingWeekExceptionRow0)
                'End If

                If Utility.DoesDateRangeIntersect( _
                 flexBookingWeekExceptionRow0.FleTravelStart, _
                 flexBookingWeekExceptionRow0.FleTravelEnd, _
                 flexBookingWeekExceptionRow1.FleTravelStart, _
                 flexBookingWeekExceptionRow1.FleTravelEnd) Then
                    Throw New ValidationException("Invalid Exception: Travel Periods collide with another exception for the same location", flexBookingWeekExceptionRow0)
                End If
            Next
        Next
    End Sub

    ''' <summary>
    ''' Approve the product.  Should only be called when the object (rates and exceptions have been *fully* 
    ''' updated from the UI.  Can throw a Validate or Save Exception.
    ''' Does *not* refresh the data structure.  Product must be reloaded.
    '''------------------------------------------------------------------------------
    ''' REV:MIA FEB12012 ByVal WithDomesticFlex As Boolean
    '''------------------------------------------------------------------------------
    ''' </summary>
    Public Sub ApproveProduct(ByVal integrityNo As Integer, ByVal usrId As String, ByVal WithDomesticFlex As Boolean)
        If _weekProductRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Product: Has already been updated by another user")
        End If

        ValidateRates()
        ValidateExceptions()

        ''------------------------------------------------------------------------------
        '' REV:MIA FEB12012 MOVE BELOW AFTER THE FOR EACH
        ''------------------------------------------------------------------------------
        'Dim productResult As FlexBookingWeekProductDataTable = DataRepository.ApproveWeekProduct( _
        '     _weekProductRow.FlpId, _
        '     _weekProductRow.IntegrityNo, _
        '     usrId)
        'If productResult Is Nothing OrElse productResult.Count = 0 Then
        '    Throw New SaveException("Save Error: There was a problem approving the product, most likely caused by it being updated already.")
        'End If

        'Dim flpId As Integer = productResult(0).FlpId

        Dim flexType As String = ""
        Dim flexTypeStatusUnset As Boolean
        For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable

            flexType = flexBookingWeekRateRow.FlrFbwType

            If (Not String.IsNullOrEmpty(flexType)) Then
                flexType = flexType.Trim().ToUpper()
            Else
                flexType = "I"
            End If

            flexTypeStatusUnset = IIf(String.IsNullOrEmpty(flexBookingWeekRateRow.FlrFlpStatus), True, False)

            ''------------------------------------------------------------------------------
            '' REV:MIA FEB12012 
            ''------------------------------------------------------------------------------
            Dim rateResult As FlexBookingWeekRateDataTable = Nothing
            If (WithDomesticFlex = True) Then
                ''System.Diagnostics.Debug.WriteLine(flexType & " " & flexTypeStatusUnset)
                If (flexType.Equals("D")) Then '' And flexTypeStatusUnset) Then
                    rateResult = DataRepository.UpdateProductRate(flexBookingWeekRateRow.FlrId, flexBookingWeekRateRow.FlrFlexNum, flexBookingWeekRateRow.FlrChanged)
                    If rateResult Is Nothing OrElse rateResult.Count = 0 Then
                        Throw New SaveException("Save Error: There was a problem saving the product rate.")
                    End If
                End If
            Else
                If (flexType.Equals("I")) Then '' And flexTypeStatusUnset) Then
                    rateResult = DataRepository.UpdateProductRate(flexBookingWeekRateRow.FlrId, flexBookingWeekRateRow.FlrFlexNum, flexBookingWeekRateRow.FlrChanged)
                    If rateResult Is Nothing OrElse rateResult.Count = 0 Then
                        Throw New SaveException("Save Error: There was a problem saving the product rate.")
                    End If
                End If
            End If
            ''------------------------------------------------------------------------------
        Next

        ''------------------------------------------------------------------------------
        '' REV:MIA FEB12012 MOVE BELOW
        ''------------------------------------------------------------------------------
        Dim productResult As FlexBookingWeekProductDataTable = DataRepository.ApproveWeekProduct( _
            _weekProductRow.FlpId, _
            _weekProductRow.IntegrityNo, _
            usrId, _
            IIf(WithDomesticFlex = True, "D", "I"))
        If productResult Is Nothing OrElse productResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem approving the product, most likely caused by it being updated already.")
        End If

        Dim flpId As Integer = productResult(0).FlpId


        For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
            If String.IsNullOrEmpty(flexBookingWeekExceptionRow.LocationCode) AndAlso Not flexBookingWeekExceptionRow.IsFleIdNull() Then
                DataRepository.DeleteProductException(flexBookingWeekExceptionRow.FleId) ' delete
            ElseIf Not String.IsNullOrEmpty(flexBookingWeekExceptionRow.LocationCode) Then
                Dim fleId As Nullable(Of Integer)
                If Not flexBookingWeekExceptionRow.IsFleIdNull Then
                    fleId = flexBookingWeekExceptionRow.FleId ' update
                Else
                    fleId = Nothing ' create
                End If

                Dim locCodeFrom As String = Nothing
                If Not flexBookingWeekExceptionRow.IsFleLocCodeFromNull Then
                    locCodeFrom = flexBookingWeekExceptionRow.FleLocCodeFrom
                End If

                Dim locCodeTo As String = Nothing
                If Not flexBookingWeekExceptionRow.IsFleLocCodeToNull Then
                    locCodeTo = flexBookingWeekExceptionRow.FleLocCodeTo
                End If

                Dim exceptionResult As FlexBookingWeekExceptionDataTable = DataRepository.CreateUpdateProductException( _
                 fleId, _
                 flpId, _
                 flexBookingWeekExceptionRow.FleBookStart, _
                 flexBookingWeekExceptionRow.FleBookEnd, _
                 flexBookingWeekExceptionRow.FleTravelStart, _
                 flexBookingWeekExceptionRow.FleTravelEnd, _
                 locCodeFrom, _
                 locCodeTo, _
                 flexBookingWeekExceptionRow.FleDelta)
                If exceptionResult Is Nothing OrElse exceptionResult.Count = 0 Then
                    Throw New SaveException("Save Error: There was a problem saving the product exception.")
                End If
            End If
        Next
    End Sub


    ''' <summary>
    ''' Reject the product.  Can throw a Validation or Save Exception.  Does *not* refresh the data structure.  Product must be reloaded.
    ''' </summary>
    Public Sub RejectProduct(ByVal integrityNo As Integer, ByVal usrId As String, ByVal WithDomesticFlex As Boolean)
        If _weekProductRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Product: Has already been updated by another user")
        End If

        ''------------------------------------------------------------------------------
        '' REV:MIA FEB12012 -- ByVal WithDomesticFlex As Boolean
        ''------------------------------------------------------------------------------
        Dim productResult As FlexBookingWeekProductDataTable = DataRepository.RejectWeekProduct( _
             _weekProductRow.FlpId, _
             _weekProductRow.IntegrityNo, _
             usrId, _
             WithDomesticFlex)

        If productResult Is Nothing OrElse productResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem rejecting the product, most likely caused by it being updated already.")
        End If
    End Sub

    ''' <summary>
    ''' Finalize the product.  Can throw a Validation or Save Exception
    ''' </summary>
    Public Sub FinalizeProduct(ByVal integrityNo As Integer, ByVal usrId As String)
        If _weekProductRow.IntegrityNo <> integrityNo Then
            Throw New ValidationException("Invalid Product: Has already been updated by another user")
        End If

        For Each flexBookingWeekRateRow As FlexBookingWeekRateRow In _flexBookingWeekRateDataTable
            Dim productRateResult As FlexBookingWeekRateDataTable = DataRepository.FinalizeProductRate( _
                 flexBookingWeekRateRow.FlrId, _
                 FleetStatusByWkNo(flexBookingWeekRateRow.FlrWkNo))
            If productRateResult Is Nothing OrElse productRateResult.Count = 0 Then Continue For

            flexBookingWeekRateRow.FlrFleetStatus = productRateResult(0).FlrFleetStatus
        Next

        Dim productResult As FlexBookingWeekProductDataTable = DataRepository.FinalizeWeekProduct( _
             _weekProductRow.FlpId, _
             LocationsIncludedDescription, _
             LocationsExcludedDescription, _
             _weekProductRow.IntegrityNo, _
             usrId)
        If productResult Is Nothing OrElse productResult.Count = 0 Then
            Throw New SaveException("Save Error: There was a problem finalizing the product, most likely caused by it being updated already.")
        End If

        _weekProductRow.FlpLocationsInclude = productResult(0).FlpLocationsInclude
        _weekProductRow.FlpLocationsExclude = productResult(0).FlpLocationsExclude
        _weekProductRow.IntegrityNo = productResult(0).IntegrityNo
        _weekProductRow.ModUsrId = productResult(0).ModUsrId
        _weekProductRow.ModDateTime = productResult(0).ModDateTime
    End Sub


#Region "rev:mia feb.12 2013 - contains fixes for mighty rates"
    'Private _flexBookingWeekRateDataTableV2 As FlexBookingWeekRateDataTable
    Private MightyDataset As DataSet
    'Public Property FlexBookingWeekRateDataTableV2 As FlexBookingWeekRateDataTable
    '    Get
    '        Return _flexBookingWeekRateDataTableV2
    '    End Get
    '    Set(value As FlexBookingWeekRateDataTable)
    '        _flexBookingWeekRateDataTableV2 = value
    '    End Set
    'End Property

    ''reV:mia Feb.12 2013- today in history. Pope Benedict resigned from his office citing his old age preventing him to do his holy work.
    ''                     his last day will be on feb.28 2013
    Public ReadOnly Property GetAdjustedFlexNumByWkNoLocCodeV2(ByVal wkNo As Integer, _
                                                               ByVal locCode As String, _
                                                               Optional ByVal WithDomesticFlex As Boolean = False, _
                                                               Optional flpId As Int32 = -1, _
                                                               Optional getWeekForNexYear As Boolean = False, _
                                                               Optional flpIdforNextYear As Int32 = -1 _
                                                               ) As Integer
        Get
            Dim result As FlexBookingWeekRateRow = Nothing
            Dim _mightyflexBookingWeekRateDataTable As FlexBookingWeekRateDataTable

            ''------------------------------------------------------------------------
            ''REV:MIA FEB112013 ADDED CONDITION TO CHECK IF ITS DOM OR INTERNATIONAL
            ''------------------------------------------------------------------------
            Dim flexBookingWeekRateRow() As FlexBookingWeekRateRow = Nothing

            If (flpId <> -1) Then
                _mightyflexBookingWeekRateDataTable = DataRepository.GetProductRates(flpId)
                If (WithDomesticFlex = True) Then
                    flexBookingWeekRateRow = _mightyflexBookingWeekRateDataTable.Select("FlrFbwType = 'D' and FlrWkNo = " & wkNo)
                Else
                    flexBookingWeekRateRow = _mightyflexBookingWeekRateDataTable.Select("FlrFbwType = 'I' and FlrWkNo = " & wkNo)
                End If
            Else


                ''------------------------------------------------------------------------
                ''REV:MIA FEB172014 ADDED CONDITION TO CHECK IF FLPID = -1 AND WKNO = 1
                ''AND getWeekForNexYear = TRUE AND flpIdforNextYear <> -1 
                ''THEN CONTINUE GET THE VALUE FOR NEXT YEAR
                ''------------------------------------------------------------------------
                If (getWeekForNexYear = False And flpIdforNextYear = -1) Then
                    flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = " & IIf(WithDomesticFlex = True, "'D'", "'I'") & " and FlrWkNo = " & wkNo)
                Else
                    flexBookingWeekRateRow = DataRepository.GetProductRates(flpIdforNextYear).Select("FlrFbwType = " & IIf(WithDomesticFlex = True, "'D'", "'I'") & " and FlrWkNo = " & wkNo)
                End If


                ''------------------------------------------------------------------------
                ''REV:MIA FEB132013 ADDED CONDITION TO CHECK IF FLPID = -1 AND WKNO = 1
                ''------------------------------------------------------------------------
                'If (WithDomesticFlex = True) Then
                '    flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'D' and FlrWkNo = " & wkNo)
                'Else
                '    flexBookingWeekRateRow = _flexBookingWeekRateDataTable.Select("FlrFbwType = 'I' and FlrWkNo = " & wkNo)
                'End If

            End If



            For Each item As FlexBookingWeekRateRow In flexBookingWeekRateRow
                If (Not item Is Nothing) Then
                    If item.FlrWkNo = wkNo Then
                        result = item
                    End If
                End If
            Next

            ''------------------------------------------------------------------------


            If result Is Nothing Then Return FlexConstants.FlexNumDefault
            If (getWeekForNexYear = False And flpIdforNextYear = -1) Then
                For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In _flexBookingWeekExceptionDataTable
                    If flexBookingWeekExceptionRow.LocationCode = locCode _
                     AndAlso flexBookingWeekExceptionRow.FleTravelStart <= result.FlrTravelFrom _
                     AndAlso flexBookingWeekExceptionRow.FleTravelEnd >= result.FlrTravelFrom Then
                        Try
                            Dim flexInt As Integer = FlexConstants.FlexNumToInt(result.FlrFlexNum) + flexBookingWeekExceptionRow.FleDelta
                            If flexInt < FlexConstants.FlexIntMin Then flexInt = FlexConstants.FlexIntMin
                            If flexInt > FlexConstants.FlexIntMax Then flexInt = FlexConstants.FlexIntMax
                            Logging.LogInformation("1.GetAdjustedFlexNumByWkNoLocCodeV2:", vbCrLf & "FLEX NUM: " & result.FlrFlexNum & _
                                                   vbCrLf & "FlexConstants.FlexNumToInt(result.FlrFlexNum): " & FlexConstants.FlexNumToInt(result.FlrFlexNum) & _
                                                   vbCrLf & "flexBookingWeekExceptionRow.FlexDelta        : " & flexBookingWeekExceptionRow.FleDelta & _
                                                   vbCrLf & "flexint                                      : " & flexInt & " flexint=FlexConstants.FlexNumToInt(result.FlrFlexNum) + flexBookingWeekExceptionRow.FleDelta" & _
                                                   vbCrLf & "flexInt < FlexConstants.FlexIntMin           : " & CBool(flexInt < FlexConstants.FlexIntMin) & " if true assign FlexConstants.FlexIntMin value (" & FlexConstants.FlexIntMin & ") to flexint" & _
                                                   vbCrLf & "flexInt > FlexConstants.FlexIntMax           : " & CBool(flexInt > FlexConstants.FlexIntMax) & " if true assign FlexConstants.FlexIntMax value (" & FlexConstants.FlexIntMin & ") to flexint" & _
                                                   vbCrLf & "RETURN FlexConstants.FlexIntToFlexNum(" & flexInt & ") : " & FlexConstants.FlexIntToFlexNum(flexInt) & vbCrLf & " ------------------------------------------------ ")
                            Return FlexConstants.FlexIntToFlexNum(flexInt)
                        Catch ex As Exception
                            Return result.FlrFlexNum
                        End Try
                    End If
                Next
            Else
                For Each flexBookingWeekExceptionRow As FlexBookingWeekExceptionRow In ReloadExceptionForNextYear(flpIdforNextYear)
                    If flexBookingWeekExceptionRow.LocationCode = locCode _
                     AndAlso flexBookingWeekExceptionRow.FleTravelStart <= result.FlrTravelFrom _
                     AndAlso flexBookingWeekExceptionRow.FleTravelEnd >= result.FlrTravelFrom Then
                        Try
                            Dim flexInt As Integer = FlexConstants.FlexNumToInt(result.FlrFlexNum) + flexBookingWeekExceptionRow.FleDelta
                            If flexInt < FlexConstants.FlexIntMin Then flexInt = FlexConstants.FlexIntMin
                            If flexInt > FlexConstants.FlexIntMax Then flexInt = FlexConstants.FlexIntMax
                            Logging.LogInformation("2.GetAdjustedFlexNumByWkNoLocCodeV2:", vbCrLf & "FLEX NUM: " & result.FlrFlexNum & _
                                                   vbCrLf & "FlexConstants.FlexNumToInt(result.FlrFlexNum): " & FlexConstants.FlexNumToInt(result.FlrFlexNum) & _
                                                   vbCrLf & "flexBookingWeekExceptionRow.FlexDelta        : " & flexBookingWeekExceptionRow.FleDelta & _
                                                   vbCrLf & "flexint                                      : " & flexInt & " flexint=FlexConstants.FlexNumToInt(result.FlrFlexNum) + flexBookingWeekExceptionRow.FleDelta" & _
                                                   vbCrLf & "flexInt < FlexConstants.FlexIntMin           : " & CBool(flexInt < FlexConstants.FlexIntMin) & " if true assign FlexConstants.FlexIntMin value (" & FlexConstants.FlexIntMin & ") to flexint" & _
                                                   vbCrLf & "flexInt > FlexConstants.FlexIntMax           : " & CBool(flexInt > FlexConstants.FlexIntMax) & " if true assign FlexConstants.FlexIntMax value (" & FlexConstants.FlexIntMin & ") to flexint" & _
                                                   vbCrLf & "RETURN FlexConstants.FlexIntToFlexNum(" & flexInt & ") : " & FlexConstants.FlexIntToFlexNum(flexInt) & vbCrLf & " ------------------------------------------------ ")
                            Return FlexConstants.FlexIntToFlexNum(flexInt)
                        Catch ex As Exception
                            Return result.FlrFlexNum
                        End Try
                    End If
                Next

            End If
            
            Logging.LogInformation("3.GetAdjustedFlexNumByWkNoLocCodeV2:", vbCrLf & "RETURN FLEX NUM: " & result.FlrFlexNum  & vbCrLf & " ------------------------------------------------ ")
            Return result.FlrFlexNum
        End Get
    End Property
    ''REV:MIA FEB.13 2014 - TRAPPED THE 01-APRIL-XXXX WRONG VALUE
    Private Function ReloadExceptionForNextYear(flpid As Int32) As FlexBookingWeekExceptionDataTable
        Dim _flexBookingWeekExceptionDataTableForNextYear As FlexBookingWeekExceptionDataTable = DataRepository.GetProductExceptions(flpid)
        While _flexBookingWeekExceptionDataTableForNextYear.Count < FlexConstants.ExceptionMax
            Dim flexBookingWeekExceptionRowNextYear As FlexBookingWeekExceptionRow = _flexBookingWeekExceptionDataTableForNextYear.NewFlexBookingWeekExceptionRow()
            flexBookingWeekExceptionRowNextYear.SetFleIdNull()
            flexBookingWeekExceptionRowNextYear.SetFleFlpIdNull()
            flexBookingWeekExceptionRowNextYear.SetFleLocCodeFromNull()
            flexBookingWeekExceptionRowNextYear.SetFleLocCodeToNull()
            flexBookingWeekExceptionRowNextYear.FleBookStart = Date.MinValue
            flexBookingWeekExceptionRowNextYear.FleBookEnd = Date.MinValue
            flexBookingWeekExceptionRowNextYear.FleTravelStart = Date.MinValue
            flexBookingWeekExceptionRowNextYear.FleTravelEnd = Date.MinValue
            flexBookingWeekExceptionRowNextYear.FleDelta = 0
            _flexBookingWeekExceptionDataTableForNextYear.AddFlexBookingWeekExceptionRow(flexBookingWeekExceptionRowNextYear)
        End While
        Return _flexBookingWeekExceptionDataTableForNextYear
    End Function
#End Region

    
End Class
