﻿Imports System.Xml
Imports System.Text
Imports System.IO
Imports System.Xml.Serialization
Imports System.Configuration
Imports System.Reflection
Imports Aurora.Common.Logging

Public Structure DPSstructure
    Public Comment As String
    Public DPStime As String
    Public COMcode As String
    Public BRDcode As String
    Public Curtype As String
    Public Cardnumber As String
    Public CardHolder As String
    Public Expirydate As String
    Public Amount As String
    Public TransactionType As String
    Public DPSreference As String
    Public TransactionMessage As String
    Public Username As String
    Public Password As String
    Public WSLink As String
End Structure




Public Class WSPaymentExpress

#Region "Variables"

    Private _DPStransactionRef As String
    Private _comcode As String
    Private _username As String
    Private _password As String
    Private _currencytype As String
    Private _brdcode As String
    Private _LogDescription As New StringBuilder
    Private _LogType As EventLogEntryType = EventLogEntryType.Information
    Private Const LOG_NAME As String = "DPS"
    Private completeErrorMsg As String
    Private WS_LINK As String = ConfigurationManager.AppSettings("WSLINK")

    Dim dpsPath As String = ConfigurationManager.AppSettings("DPS")
    Dim folderpath As String = ConfigurationManager.AppSettings("path")


#End Region

#Region "Properties"

    Public Property CurrencyType() As String
        Get
            Return _currencytype
        End Get
        Set(ByVal value As String)
            _currencytype = value
        End Set
    End Property
    Public Property BRDcode() As String
        Get
            Return _brdcode
        End Get
        Set(ByVal value As String)
            _brdcode = value
        End Set
    End Property
    Public ReadOnly Property GetPassword() As String
        Get
            Return _password
        End Get
    End Property
    Public ReadOnly Property GetUserName() As String
        Get
            Return _username
        End Get
    End Property
    Public Property ComCode() As String
        Get
            ComCode = _comcode
        End Get
        Set(ByVal value As String)
            _comcode = value
        End Set
    End Property
    Public ReadOnly Property DPStransactionRef() As String
        Get
            Return _DPStransactionRef
        End Get
    End Property
#End Region

#Region "Public Functions"

    Public Function RetrieveCredential() As String
        RetrieveCredential = Nothing
        Dim pathnodeUsername As String = String.Empty
        Dim pathnodePassword As String = String.Empty

        Dim dom As XmlDocument = New XmlDocument
    

        Dim mypath As String = folderpath & "\" & dpsPath & "\ComCode.xml"

        Dim assemblyfolder As String = Assembly.GetExecutingAssembly.Location
        assemblyfolder = assemblyfolder.Substring(0, assemblyfolder.LastIndexOf("\"))
        Dim foldertocreate As String = String.Concat(assemblyfolder, "\", dpsPath)

        Dim xmlPATH As String = mypath

        Dim sXMLusernamePath As String = "", sXMLpasswordPath As String = ""

        dom.Load(xmlPATH)
        If _comcode.Equals("THL") = True Then
            If _currencytype.Equals("NZ") = True Then
                If _brdcode.Equals("B") = True Then
                    pathnodeUsername = "/ComCode/britz_username"
                    pathnodePassword = "/ComCode/britz_password"
                Else ''maui
                    pathnodeUsername = "/ComCode/maui_username"
                    pathnodePassword = "/ComCode/maui_password  "
                End If
            Else ''rentals
                pathnodeUsername = "/ComCode/rentals_username"
                pathnodePassword = "/ComCode/rentals_password"
            End If

        Else ''kxz ''------ If _comcode.Equals("THL") = True Then
            If _comcode.Equals("KXS") = True Then '------ If _comcode.Equals("KX") = True Then
                pathnodeUsername = "/ComCode/kxs_username"
                pathnodePassword = "/ComCode/kxs_password"
            Else
                ' V2
                ' RKS MOD: 21-Sep-2009 for MAC.COM
                If _comcode.ToUpper().Equals("MAC") = True Then '------ If _comcode.Equals("MAC") = True Then

                    If _currencytype.Equals("NZ") = True Then '------ If _comcode.Equals("NZ") = True Then
                        sXMLusernamePath = "/ComCode/mac_nz_username"
                        sXMLpasswordPath = "/ComCode/mac_nz_password"
                    Else
                        If _currencytype.Equals("AU") = True Then
                            sXMLusernamePath = "/ComCode/mac_au_username"
                            sXMLpasswordPath = "/ComCode/mac_au_password"
                        End If
                    End If '------ If _comcode.Equals("NZ") = True Then
                    pathnodeUsername = sXMLusernamePath
                    pathnodePassword = sXMLpasswordPath
                End If
            End If '' '------   If _comcode.Equals("KX") = True Then
        End If ''''kxz ''------ If _comcode.Equals("THL") = True Then

        
        LogInformation(LOG_NAME, "RetrieveCredential " & _
                                "[XML LOCATION(s):" & xmlPATH & "][USERNAME:" & _
                                pathnodeUsername & " PASSWORD: " & pathnodePassword & "]")

        Try
            Dim xmlnodeUsername As XmlElement = (dom.SelectSingleNode(pathnodeUsername))
            If Not xmlnodeUsername Is Nothing Then
                _username = xmlnodeUsername.InnerText
            End If
            Dim xmlnodePassword As XmlElement = (dom.SelectSingleNode(pathnodePassword))
            If Not xmlnodePassword Is Nothing Then
                _password = xmlnodePassword.InnerText
            End If
            TransactionXMLResult("SUCCESS", "RetrieveCredential Function", "", "")


        Catch ex As Exception
            completeErrorMsg = String.Concat("SOURCE: " & ex.Source, ", STACKTRACE: " & ex.StackTrace, ", MESSAGE: " & ex.Message)
            RetrieveCredential = TransactionXMLResult("ERROR", "RetrieveCredential Function", "", ex.Message)
        Finally

            LogInformation(LOG_NAME, "RetrieveCredential " & _
                               "[XML LOCATION:" & xmlPATH & "][USERNAMEs:" & _
                                _username & " PASSWORDs: " & _password & "][WSLINK: " & WS_LINK & "]")

            completeErrorMsg = String.Empty
        End Try

        Return RetrieveCredential
    End Function

    Public Function CurrentStatus(ByVal username As String, ByVal password As String, Optional ByVal Txref As String = Nothing) As String
        Return GetStatus(username, password, Txref)
    End Function

    ''rev:mia March 12 2012 - added CVC2
    Public Function Save(ByVal sCardnumber As String, _
                    ByVal sCardHolderName As String, _
                    ByVal sDateExpiry As String, _
                    ByVal sAmt As String, _
                    ByVal sMerchantInformation As String, _
                    ByVal inputCurrency As String, _
                    ByVal username As String, _
                    ByVal password As String, _
                    Optional ByVal CVC2 As String = "") As String


        Dim objectdetails As New PaymentExpress.TransactionDetails


        Dim paymentX As New PaymentExpress.PaymentExpressWS
        paymentX.Url = WS_LINK
        paymentX.Proxy = Nothing


        Dim paymentResult As New PaymentExpress.TransactionResult2

        Dim retTxRef As String = String.Empty
        Dim message As String = String.Empty

        Try

            With objectdetails
                .cardNumber = sCardnumber
                .cardHolderName = sCardHolderName
                .dateExpiry = sDateExpiry
                .amount = sAmt
                .merchantReference = sMerchantInformation
                .txnType = "Purchase"
                .inputCurrency = inputCurrency
                .cvc2 = CVC2
            End With

            paymentResult = paymentX.SubmitTransaction(username, password, objectdetails)

            retTxRef = paymentResult.dpsTxnRef
            _DPStransactionRef = paymentResult.dpsTxnRef

            If retTxRef.Equals("") = False And paymentResult.authorized = 1 Then
                retTxRef = TransactionXMLResult("SUCCESS", paymentResult.authCode, paymentResult.dpsTxnRef, "")
                message = TransactionMessage("SUCCESS", Nothing, paymentResult.authCode)
                '_LogType = EventLogEntryType.Information
            Else
                retTxRef = TransactionXMLResult("ERROR", paymentResult.authCode, paymentResult.dpsTxnRef, paymentResult.cardHolderHelpText)
                message = TransactionMessage("ERROR", paymentResult.cardHolderHelpText, paymentResult.authCode)
                '_LogType = EventLogEntryType.Error
            End If

        Catch ex As Exception
            completeErrorMsg = String.Concat("SAVE FUNCTION,", " SOURCE: " & ex.Source, ", STACKTRACE: " & ex.StackTrace, ", MESSAGE: " & ex.Message)
            retTxRef = TransactionXMLResult("ERROR", paymentResult.authCode, paymentResult.dpsTxnRef, paymentResult.cardHolderHelpText & " " & ex.Message)
            message = TransactionMessage("ERROR", paymentResult.cardHolderHelpText & " " & completeErrorMsg, paymentResult.authCode)
            '_LogType = EventLogEntryType.Error

        Finally
            WriteLogs(ComCode, BRDcode, CurrencyType, sCardnumber, sCardHolderName, sDateExpiry, sAmt, "Purchase", _
            _DPStransactionRef, message, _LogType, username, password, paymentX.Url)
            completeErrorMsg = String.Empty
        End Try

        Return retTxRef
    End Function

    ''rev:mia March 12 2012 - added CVC2
    Public Function Reverse(ByVal DPStransactionRef As String, _
            ByVal sAmt As String, _
            ByVal sMerchantInformation As String, _
            ByVal inputCurrency As String, _
            ByVal username As String, _
            ByVal password As String, _
            Optional ByVal CVC2 As String = "") As String

        Dim objectdetails As New PaymentExpress.TransactionDetails


        Dim paymentX As New PaymentExpress.PaymentExpressWS
        paymentX.Url = WS_LINK
        paymentX.Proxy = Nothing

        Dim paymentResult As New PaymentExpress.TransactionResult2

        Dim retTxRef As String = String.Empty
        Dim message As String = String.Empty
        Try

            With objectdetails
                .dpsTxnRef = DPStransactionRef
                .amount = sAmt
                .merchantReference = sMerchantInformation
                .txnType = "Refund"
                .inputCurrency = inputCurrency
                .cvc2 = CVC2
            End With

            paymentResult = paymentX.SubmitTransaction(username, password, objectdetails)

            retTxRef = paymentResult.dpsTxnRef
            _DPStransactionRef = paymentResult.dpsTxnRef

            If retTxRef.Equals("") = False And paymentResult.authorized = 1 Then
                retTxRef = TransactionXMLResult("SUCCESS", paymentResult.authCode, paymentResult.dpsTxnRef, "")
                message = TransactionMessage("SUCCESS", Nothing, paymentResult.authCode)
                ' _LogType = EventLogEntryType.Information
            Else
                retTxRef = TransactionXMLResult("ERROR", paymentResult.authCode, paymentResult.dpsTxnRef, paymentResult.cardHolderHelpText)
                message = TransactionMessage("ERROR", paymentResult.cardHolderHelpText, paymentResult.authCode)
                '_LogType = EventLogEntryType.Error
            End If

        Catch ex As Exception
            completeErrorMsg = String.Concat("REVERSE FUNCTION,", " SOURCE: " & ex.Source, ", STACKTRACE: " & ex.StackTrace, ", MESSAGE: " & ex.Message)

            retTxRef = TransactionXMLResult("ERROR", paymentResult.authCode, paymentResult.dpsTxnRef, paymentResult.cardHolderHelpText & " " & ex.Message)
            message = TransactionMessage("ERROR", paymentResult.cardHolderHelpText & " " & completeErrorMsg, paymentResult.authCode)
            '_LogType = EventLogEntryType.Error

        Finally
            WriteLogs(ComCode, BRDcode, CurrencyType, "", "", "", sAmt, "Reverse", DPStransactionRef, message, _LogType, username, password, paymentX.Url)
            completeErrorMsg = String.Empty
        End Try

        Return retTxRef
    End Function

#End Region
#Region "Private Functions"

    Private Function TransactionXMLResult(ByVal status As String, ByVal Authcode As String, ByVal DPStransactionRef As String, ByVal message As String) As String
        Dim sb As New StringBuilder
        With sb
            .Append("<root>")
            .AppendFormat("<status>{0}</status>", status)
            .AppendFormat("<msg>{0}</msg>", message)
            .AppendFormat("<AuthCode>{0}</AuthCode>", Authcode)
            .AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", DPStransactionRef)
            .Append("</root>")
        End With
        Return sb.ToString
    End Function
    Private Function GetStatus(Optional ByVal vusername As String = Nothing, Optional ByVal vpassword As String = Nothing, Optional ByVal Txref As String = Nothing) As String
        Dim paymentX As New PaymentExpress.PaymentExpressWS
        Dim paymentStatus As New PaymentExpress.TransactionResult2


        Dim retStatus As String = String.Empty
        paymentStatus = paymentX.GetStatus(vusername, vpassword, Txref)

        If paymentStatus.authorized = 1 Then ''successful
            retStatus = TransactionXMLResult("SUCCESS", paymentStatus.authCode, paymentStatus.dpsTxnRef, "")
        Else    ''declined/failed
            retStatus = String.Concat("Card Name:" & paymentStatus.cardName, " Error Message: ", paymentStatus.cardHolderResponseText, " - ", paymentStatus.cardHolderResponseDescription)
            retStatus = TransactionXMLResult("ERROR", paymentStatus.authCode, paymentStatus.dpsTxnRef, retStatus)
        End If

        Return retStatus
    End Function
    Private Function TransactionMessage(ByVal vstatus As String, ByVal vmessage As String, ByVal vAuthcode As String) As String
        If Not _LogDescription Is Nothing Then
            _LogDescription = Nothing
            _LogDescription = New StringBuilder
        End If
        With _LogDescription
            .AppendFormat("Status: [{0}], ", vstatus)
            .AppendFormat("Message: [{0}],", IIf(Not vmessage Is Nothing Or vmessage <> "", vmessage, "empty"))
            .AppendFormat("AuthCode: [{0}]", vAuthcode)
        End With
        Return _LogDescription.ToString
    End Function

    Private Sub WriteLogs(ByVal vCOMcode As String,
                          ByVal vBRDcode As String,
                          ByVal vCurtype As String, _
                          ByVal vCardnumber As String,
                          ByVal vCardHolder As String,
                          ByVal vExpirydate As String,
                          ByVal vAmount As String,
                          ByVal vTransactionType As String, _
                          ByVal vDPSreference As String,
                          ByVal vTransactionMessage As String,
                          Optional ByVal vEventlog As EventLogEntryType = EventLogEntryType.Information,
                          Optional ByVal vusername As String = "",
                          Optional ByVal vpassword As String = "",
                          Optional ByVal wslink As String = "")

        Dim DPSstruct As New DPSstructure
        Dim sb As New StringBuilder
        Dim tw As New StringWriter(sb)

        Dim xmlserializer As New XmlSerializer(GetType(DPSstructure))

        With DPSstruct
            .DPStime = Now
            .COMcode = vCOMcode
            .BRDcode = vBRDcode
            .Curtype = vCurtype
            .Cardnumber = vCardnumber
            .CardHolder = vCardHolder
            .Expirydate = vExpirydate
            .Amount = vAmount
            .TransactionType = vTransactionType
            .DPSreference = vDPSreference
            .TransactionMessage = vTransactionMessage
            .Comment = "Please change the prolog of the XML from UTF-16 to UTF-8"
            .Username = vusername
            .Password = vpassword
            .WSLink = wslink
        End With

        Try

            xmlserializer.Serialize(tw, DPSstruct)
            ''EventLog.WriteEntry(LOG_NAME, sb.ToString, vEventlog)
            LogInformation("WSPaymentExpress v2", sb.ToString)

        Catch

        End Try
    End Sub

    Private Sub CreateXML(ByVal path As String)
        Dim xw As New XmlTextWriter(path, Encoding.UTF8)
        With xw
            .WriteStartDocument(True)
            .Formatting = Formatting.Indented
            .WriteComment("This is the default xml containing username and password for KXS,BRITZ,MAUI,RENTALS")
            .WriteStartElement("ComCode")

            .WriteElementString("kxs_username", kxs_username)
            .WriteElementString("kxs_password", kxs_password)

            .WriteElementString("britz_username", britz_username)
            .WriteElementString("britz_password", britz_password)


            .WriteElementString("maui_username", maui_username)
            .WriteElementString("maui_password", maui_password)


            .WriteElementString("rentals_username", rentals_username)
            .WriteElementString("rentals_password", rentals_password)

            .WriteEndElement()
            .Flush()
            .Close()

        End With
    End Sub

#End Region

#Region "Constructors"
    Public Sub New()

        'Try
        '    If Not EventLog.SourceExists(LOG_NAME) Then
        '        EventLog.CreateEventSource(LOG_NAME, LOG_NAME)
        '    End If
        'Catch ex As Exception
        'End Try
        

        Dim assemblyfolder As String = Assembly.GetExecutingAssembly.Location
        assemblyfolder = assemblyfolder.Substring(0, assemblyfolder.LastIndexOf("\"))

        ''rev:mia 12.10.07 - override assemblyfolder
        assemblyfolder = folderpath
        Dim foldertocreate As String
        If assemblyfolder.EndsWith("\") Then
            foldertocreate = String.Concat(assemblyfolder, dpsPath)
        Else
            foldertocreate = String.Concat(assemblyfolder, "\", dpsPath)
        End If


        Dim xmlPATH As String = foldertocreate & "\ComCode.xml"

        Dim haserror As Boolean = False
        Dim errormsg As String = String.Empty
        Try

            If Directory.Exists(foldertocreate) = False Then
                Directory.CreateDirectory(foldertocreate)
                LogInformation(LOG_NAME, "[NEW FOLDER CREATED:" & foldertocreate & "]")
            End If
            If File.Exists(xmlPATH) = False Then
                Me.CreateXML(xmlPATH)
                LogInformation(LOG_NAME, "[NEW XML CREATED:" & xmlPATH & "]")
            End If

        Catch ex As Exception
            haserror = True
            errormsg = ex.Message
            LogException(LOG_NAME, ex)
        Finally

            If haserror = True Then
            End If

        End Try

    End Sub
#End Region

#Region "AppSettings"

    Private ReadOnly Property kxs_username As String
        Get
            Return ConfigurationManager.AppSettings("kxs_username")
        End Get
    End Property

    Private ReadOnly Property kxs_password As String
        Get
            Return ConfigurationManager.AppSettings("kxs_password")
        End Get
    End Property

    Private ReadOnly Property britz_username As String
        Get
            Return ConfigurationManager.AppSettings("britz_username")
        End Get
    End Property

    Private ReadOnly Property britz_password As String
        Get
            Return ConfigurationManager.AppSettings("britz_password")
        End Get
    End Property

    Private ReadOnly Property maui_username As String
        Get
            Return ConfigurationManager.AppSettings("maui_username")
        End Get
    End Property

    Private ReadOnly Property maui_password As String
        Get
            Return ConfigurationManager.AppSettings("maui_password")
        End Get
    End Property

    Private ReadOnly Property rentals_username As String
        Get
            Return ConfigurationManager.AppSettings("rentals_username")
        End Get
    End Property

    Private ReadOnly Property rentals_password As String
        Get
            Return ConfigurationManager.AppSettings("rentals_password")
        End Get
    End Property

#End Region


End Class
