<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="Default.aspx.vb" Inherits="Reports_Default" Title="Untitled Page" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\CollapsiblePanel\CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="divMain" style="position:absolute;left:10px" >
                <uc1:CollapsiblePanel runat="server" Title="Parameters" TargetControlID="parameterPanel"
                    ID="parameterCollapsiblePanel" Visible="False" Width="100%" />
                <asp:Panel ID="parameterPanel" runat="server" Style="padding: 5px; width: 100%" Visible="False">
                    <asp:Table ID="parameterTable" runat="Server" EnableViewState="False" Style="border: 0px;
                        width: 100%; margin-bottom: 5px;">
                    </asp:Table>
                    <div style="float: right; margin-right: 10px">
                        <asp:Button ID="resetButton" runat="Server" CssClass="Button_Standard Button_Reset"
                            Text="Reset" />
                        <asp:Button ID="viewButton" runat="Server" CssClass="Button_Standard Button_View"
                            Text="View" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="reportPanel" runat="Server" Visible="False">
                    <hr />
                    <asp:Literal ID="reportLiteral" runat="server"></asp:Literal>
                    <iframe id="reportIFrame" runat="server" width="100%" height="600px" frameborder="0"
                        style="overflow: hidden; background-color: #FFF"></iframe>
                </asp:Panel>
            </div>
             <script type="text/javascript" language="javascript">
                try{divMain.style.width= document.body.clientWidth - 20;}catch(err){}
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
