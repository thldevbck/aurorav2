Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Reports.Data
Imports Aurora.Reports.Data.ReportDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data

<AuroraFunctionCodeAttribute("REPORTMAINT2")> _
<AuroraPageTitleAttribute("Report Maintenance Import/Export")> _
Partial Class Reports_ImportExport
    Inherits AuroraPage

    Private _functionDataTable As New ReportDataSet.LookupDataTable
    Private _selectedFunction As New ReportDataSet

    Private Sub BuildFunctions(ByVal useViewState As Boolean)
        Dim functionDropDown_selectedValue As String = functionDropDown.SelectedValue
        functionDropDown.SelectedValue = Nothing

        Aurora.Common.Data.ExecuteTableSql("SELECT FunCode AS Value, FunCode AS Name, FunCode + ' ' + FunTitle AS Description FROM Functions ORDER BY FunCode", _functionDataTable)

        functionDropDown.DataSource = _functionDataTable
        functionDropDown.DataValueField = "Value"
        functionDropDown.DataTextField = "Description"
        functionDropDown.DataBind()

        If useViewState AndAlso functionDropDown.Items.FindByValue(functionDropDown_selectedValue) IsNot Nothing Then functionDropDown.SelectedValue = functionDropDown_selectedValue

        If Not String.IsNullOrEmpty(functionDropDown.SelectedValue) Then
            DataRepository.GetReportByFunCode(_selectedFunction, functionDropDown.SelectedValue)
        End If
    End Sub


    Private Sub BuildImportExport(ByVal useViewState As Boolean)
        If _selectedFunction.Report.Count > 0 Then
            xmlTextBox.ReadOnly = True
            importButton.Enabled = False
            deleteButton.Enabled = True

            Using writer As New System.IO.StringWriter()
                _selectedFunction.WriteXml(writer)
                xmlTextBox.Text = writer.ToString()
            End Using
        Else
            xmlTextBox.ReadOnly = False
            importButton.Enabled = True
            deleteButton.Enabled = False
            xmlTextBox.Text = ""
        End If
    End Sub


    Private Sub BuildForm(ByVal useViewState As Boolean)
        BuildFunctions(useViewState)
        BuildImportExport(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Return
        End If

        BuildForm(False)
    End Sub

    Protected Sub functionDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles functionDropDown.SelectedIndexChanged
        BuildForm(True)
    End Sub

    Protected Sub importButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles importButton.Click
        If String.IsNullOrEmpty(functionDropDown.SelectedValue) Then
            BuildForm(True)
            Return
        End If

        DataRepository.GetReportByFunCode(_selectedFunction, functionDropDown.SelectedValue)
        If _selectedFunction.Report.Count > 0 Then
            BuildForm(True)
            AddErrorMessage("Function already has a report definition")
            Return
        End If

        Dim reportDataSet As New ReportDataSet()
        Try
            Using stringReader As New System.IO.StringReader(xmlTextBox.Text)
                reportDataSet.ReadXml(stringReader)
            End Using

            If reportDataSet.Report.Count <> 1 Then Throw New Exception()
        Catch
            BuildForm(True)
            AddErrorMessage("Invalid Xml")
            Return
        End Try

        Try
            For Each reportRow As ReportRow In reportDataSet.Report
                reportRow.repFunCode = functionDropDown.SelectedValue
                reportRow.repId = Aurora.Common.Data.GetNewId()
                Aurora.Common.Data.ExecuteDataRow(reportRow)

                For Each reportParamRow As ReportParamRow In reportRow.GetReportParamRows()
                    reportParamRow.rppId = Aurora.Common.Data.GetNewId()
                    Aurora.Common.Data.ExecuteDataRow(reportParamRow)

                    For Each reportParamListItemRow As ReportParamListItemRow In reportParamRow.GetReportParamListItemRows()
                        Aurora.Common.Data.ExecuteDataRow(reportParamListItemRow)
                    Next
                Next
            Next
        Catch ex As Exception
            BuildForm(True)
            AddErrorMessage("Error importing report definition:<br/>" & Server.HtmlEncode(ex.Message))
            Return
        End Try

        BuildForm(True)
        AddInformationMessage("Import Successful")
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If String.IsNullOrEmpty(functionDropDown.SelectedValue) Then
            BuildForm(True)
            Return
        End If

        Dim deleteFunction As New ReportDataSet
        DataRepository.GetReportByFunCode(deleteFunction, functionDropDown.SelectedValue)
        If deleteFunction.Report.Count <> 1 Then
            BuildForm(True)
            AddErrorMessage("Function does not have a report definition")
            Return
        End If

        Try
            DataRepository.DeleteReport(deleteFunction.Report(0).repId)
        Catch ex As Exception
            BuildForm(True)
            AddErrorMessage("Error deleting report definition:<br/>" & Server.HtmlEncode(ex.Message))
            Return
        End Try

        BuildForm(True)
        AddInformationMessage("Report definition deleted")
    End Sub
End Class
