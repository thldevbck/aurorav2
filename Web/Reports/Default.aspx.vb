Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Reports.Data
Imports Aurora.Reports.Data.ReportDataSet
Imports Aurora.Reports.Services

Partial Class Reports_Default
    Inherits AuroraPage

    Public Const QueryStringDateFormat As String = "dd/MM/yyyy"

    Private _reportDataSet As New ReportDataSet
    Private _reportDataSetValidationException As Exception

    Public Overrides ReadOnly Property FunctionCode() As String
        Get
            If _reportDataSet IsNot Nothing Then
                Return _reportDataSet.RootReport.repFunCode
            Else
                Return AuroraFunctionCodeAttribute.General
            End If
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If _reportDataSet IsNot Nothing Then
                Return _reportDataSet.RootReport.repName
            Else
                Return "Report"
            End If
        End Get
    End Property

    Public ReadOnly Property BaseUrl() As String
        Get
            Return "Default.aspx" _
             & "?funCode=" & Server.UrlEncode(_reportDataSet.RootReport.repFunCode) _
             & IIf(String.IsNullOrEmpty(Request.QueryString("popup")), "", "&popup=True")
        End Get
    End Property

    Public Function InitListItems(ByVal reportParamRow As ReportParamRow, ByVal isCheckBox As Boolean) As ListItem()
        Dim result As New List(Of ListItem)
        Dim resultValues As New List(Of String)

        If reportParamRow.GetReportParamListItemRows().Length > 0 Then
            For Each reportParamListItemRow As ReportParamListItemRow In reportParamRow.GetReportParamListItemRows()
                If Not resultValues.Contains(reportParamListItemRow.rpiValue) Then
                    Dim li As New ListItem()
                    li.Text = reportParamListItemRow.rpiDescription
                    li.Value = reportParamListItemRow.rpiValue
                    li.Attributes.Add("name", reportParamListItemRow.rpiName)
                    result.Add(li)
                    resultValues.Add(li.Value)
                End If
            Next
        End If

        If reportParamRow.LookupDataTable IsNot Nothing Then
            For Each lookupRow As LookupRow In reportParamRow.LookupDataTable
                If Not resultValues.Contains(lookupRow.Value) Then
                    Dim li As New ListItem()
                    li.Text = lookupRow.Description
                    li.Value = lookupRow.Value
                    li.Attributes.Add("name", lookupRow.Name)
                    result.Add(li)
                    resultValues.Add(li.Value)
                End If
            Next
        End If

        Return result.ToArray()
    End Function

    Private Sub InitForm()
        Dim index As Integer = 0
        For Each reportParamRow As ReportParamRow In _reportDataSet.RootReport.GetReportParamRows()
            Dim tableRow As New TableRow()
            parameterTable.Rows.Add(tableRow)

            Dim nameCell As New TableCell
            nameCell.Width = Unit.Parse("150px")
            nameCell.Text = Server.HtmlEncode(reportParamRow.rppDescription) & ":"
            tableRow.Cells.Add(nameCell)

            Dim valueCell As New TableCell
            tableRow.Cells.Add(valueCell)

            If reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_RADIO Then
                Dim radioBoxList As New RadioButtonList
                radioBoxList.Items.AddRange(InitListItems(reportParamRow, False))
                radioBoxList.ID = "reportParam" & CStr(index)
                radioBoxList.RepeatLayout = RepeatLayout.Table
                radioBoxList.RepeatColumns = 4
                radioBoxList.RepeatDirection = RepeatDirection.Horizontal
                radioBoxList.Width = Unit.Parse("600px")
                radioBoxList.AutoPostBack = reportParamRow.rppIsAutoPostBack
                valueCell.Controls.Add(radioBoxList)

                If Me.IsPostBack _
                 AndAlso radioBoxList.Items.FindByValue(Request(radioBoxList.ClientID.Replace("_", "$"))) IsNot Nothing Then
                    radioBoxList.SelectedValue = Request(radioBoxList.ClientID.Replace("_", "$"))
                ElseIf Request.QueryString(reportParamRow.rppName) IsNot Nothing _
                 AndAlso radioBoxList.Items.FindByValue(Request.QueryString(reportParamRow.rppName)) IsNot Nothing Then
                    radioBoxList.SelectedValue = Request.QueryString(reportParamRow.rppName)
                ElseIf reportParamRow.DefaultValue IsNot Nothing _
                 AndAlso radioBoxList.Items.FindByValue(Aurora.Common.Utility.ObjectValueToString(reportParamRow.DefaultValue)) IsNot Nothing Then
                    radioBoxList.SelectedValue = Aurora.Common.Utility.ObjectValueToString(reportParamRow.DefaultValue)
                End If

                reportParamRow.Tag = radioBoxList
                If Not String.IsNullOrEmpty(radioBoxList.SelectedValue) Then
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, radioBoxList.SelectedValue)
                Else
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, DBNull.Value)
                End If
            ElseIf reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_DROPDOWN Then
                Dim dropDownList As New DropDownList
                If reportParamRow.rppNullable Then dropDownList.Items.Add(New ListItem("", ""))
                dropDownList.Items.AddRange(InitListItems(reportParamRow, False))
                dropDownList.ID = "reportParam" & CStr(index)
                If Not reportParamRow.IsrppWidthNull AndAlso Not String.IsNullOrEmpty(reportParamRow.rppWidth) Then
                    dropDownList.Width = Unit.Parse(reportParamRow.rppWidth)
                Else
                    dropDownList.Width = Unit.Parse("600px")
                End If
                dropDownList.AutoPostBack = reportParamRow.rppIsAutoPostBack
                valueCell.Controls.Add(dropDownList)

                If Me.IsPostBack _
                 AndAlso dropDownList.Items.FindByValue(Request(dropDownList.ClientID.Replace("_", "$"))) IsNot Nothing Then
                    dropDownList.SelectedValue = Request(dropDownList.ClientID.Replace("_", "$"))
                ElseIf Request.QueryString(reportParamRow.rppName) IsNot Nothing _
                 AndAlso dropDownList.Items.FindByValue(Request.QueryString(reportParamRow.rppName)) IsNot Nothing Then
                    dropDownList.SelectedValue = Request.QueryString(reportParamRow.rppName)
                ElseIf reportParamRow.DefaultValue IsNot Nothing _
                 AndAlso dropDownList.Items.FindByValue(Aurora.Common.Utility.ObjectValueToString(reportParamRow.DefaultValue)) IsNot Nothing Then
                    dropDownList.SelectedValue = Aurora.Common.Utility.ObjectValueToString(reportParamRow.DefaultValue)
                End If

                reportParamRow.Tag = dropDownList
                If Not String.IsNullOrEmpty(dropDownList.SelectedValue) Then
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, dropDownList.SelectedValue)
                Else
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, DBNull.Value)
                End If
            ElseIf reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_CHECKBOX Then
                Dim checkBoxList As New CheckBoxList
                checkBoxList.Items.AddRange(InitListItems(reportParamRow, True))
                checkBoxList.ID = "reportParam" & CStr(index)
                checkBoxList.RepeatLayout = RepeatLayout.Table
                checkBoxList.RepeatColumns = 4
                checkBoxList.RepeatDirection = RepeatDirection.Horizontal
                checkBoxList.Width = Unit.Parse("600px")
                checkBoxList.AutoPostBack = reportParamRow.rppIsAutoPostBack
                valueCell.Controls.Add(checkBoxList)

                For i As Integer = 0 To checkBoxList.Items.Count - 1
                    Dim listItem As ListItem = checkBoxList.Items(i)
                    If Me.IsPostBack Then
                        listItem.Selected = Not String.IsNullOrEmpty(Request(checkBoxList.ClientID.Replace("_", "$") & "$" & CStr(i)))
                    Else
                        listItem.Selected = Not String.IsNullOrEmpty(Request.QueryString(listItem.Attributes("name")))
                    End If
                Next

                reportParamRow.Tag = checkBoxList
                For Each listItem As ListItem In checkBoxList.Items
                    If listItem.Selected Then
                        _reportDataSet.EnvironmentParams.Add("@" + listItem.Attributes("name"), listItem.Value)
                    Else
                        _reportDataSet.EnvironmentParams.Add("@" + listItem.Attributes("name"), DBNull.Value)
                    End If
                Next
            ElseIf reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_DATE Then
                Dim dateControl As UserControls_DateControl = Me.LoadControl("~\UserControls\DateControl\DateControl.ascx")
                dateControl.ID = "reportParam" & CStr(index)
                dateControl.Nullable = reportParamRow.rppNullable
                dateControl.AutoPostBack = reportParamRow.rppIsAutoPostBack
                valueCell.Controls.Add(dateControl)
                If Not reportParamRow.rppNullable Then
                    valueCell.Controls.Add(New LiteralControl("*"))
                End If

                If Me.IsPostBack Then
                    Try
                        dateControl.Date = Date.ParseExact("" & Request(dateControl.ClientID.Replace("_", "$")), QueryStringDateFormat, System.Globalization.CultureInfo.CurrentCulture)
                    Catch
                        dateControl.Text = "" & Request(dateControl.ClientID.Replace("_", "$"))
                    End Try
                ElseIf Request.QueryString(reportParamRow.rppName) IsNot Nothing Then
                    Try
                        dateControl.Date = Date.ParseExact(Request.QueryString(reportParamRow.rppName), QueryStringDateFormat, System.Globalization.CultureInfo.CurrentCulture)
                    Catch
                        dateControl.Text = Request.QueryString(reportParamRow.rppName)
                    End Try
                ElseIf reportParamRow.DefaultValue IsNot Nothing Then
                    Try
                        dateControl.Date = CType(reportParamRow.DefaultValue, Date)
                    Catch
                        dateControl.Text = Aurora.Common.Utility.ObjectValueToString(reportParamRow.DefaultValue)
                    End Try
                End If

                reportParamRow.Tag = dateControl
                If dateControl.Date <> Date.MinValue Then
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, dateControl.Date)
                Else
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, DBNull.Value)
                End If
            Else
                Dim textBox As UserControls_RegExTextBox = Me.LoadControl("~\UserControls\RegExTextBox\RegExTextBox.ascx")
                textBox.Width = Unit.Parse("600px")
                textBox.ID = "reportParam" & CStr(index)
                If Not reportParamRow.IsrppSettingsNull Then
                    textBox.RegExPattern = reportParamRow.rppSettings
                    textBox.ErrorMessage = "Enter a valid " & reportParamRow.rppName
                End If
                If Not reportParamRow.IsrppWidthNull AndAlso Not String.IsNullOrEmpty(reportParamRow.rppWidth) Then textBox.Width = Unit.Parse(reportParamRow.rppWidth)
                textBox.Enabled = reportParamRow.rppEnabled
                textBox.Nullable = reportParamRow.rppNullable
                textBox.AutoPostBack = reportParamRow.rppIsAutoPostBack
                valueCell.Controls.Add(textBox)
                If Not reportParamRow.rppNullable Then
                    valueCell.Controls.Add(New LiteralControl("*"))
                End If

                If Me.IsPostBack Then
                    textBox.Text = "" & Request(textBox.ClientID.Replace("_", "$"))
                ElseIf Request.QueryString(reportParamRow.rppName) IsNot Nothing Then
                    textBox.Text = Request.QueryString(reportParamRow.rppName)
                ElseIf reportParamRow.DefaultValue IsNot Nothing Then
                    textBox.Text = Aurora.Common.Utility.ObjectValueToString(reportParamRow.DefaultValue)
                End If

                reportParamRow.Tag = textBox
                If Not String.IsNullOrEmpty(textBox.Text) Then
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, textBox.Text)
                Else
                    _reportDataSet.EnvironmentParams.Add("@" + reportParamRow.rppName, DBNull.Value)
                End If
            End If

            index += 1
        Next
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            _reportDataSet.LoadFromParams(Request.QueryString)
        Catch ex As Exception
            _reportDataSet = Nothing
            _reportDataSetValidationException = ex
        End Try

        MyBase.OnInit(e)

        If _reportDataSetValidationException Is Nothing AndAlso _reportDataSet IsNot Nothing Then
            _reportDataSet.EnvironmentParams.Add("@Environment_UserId", UserSettings.Current.UsrId)
            _reportDataSet.EnvironmentParams.Add("@Environment_UserCode", UserSettings.Current.UsrCode)
            _reportDataSet.EnvironmentParams.Add("@Environment_UserName", UserSettings.Current.UsrName)
            _reportDataSet.EnvironmentParams.Add("@Environment_ComCode", UserSettings.Current.ComCode)
            _reportDataSet.EnvironmentParams.Add("@Environment_ComName", UserSettings.Current.ComName)
            _reportDataSet.EnvironmentParams.Add("@Environment_CountryCode", UserSettings.Current.CtyCode)
            _reportDataSet.EnvironmentParams.Add("@Environment_CountryName", UserSettings.Current.CtyName)
            _reportDataSet.EnvironmentParams.Add("@Environment_LocationCode", UserSettings.Current.LocCode)
            _reportDataSet.EnvironmentParams.Add("@Environment_LocationName", UserSettings.Current.LocName)
            _reportDataSet.EnvironmentParams.Add("@Environment_TownCityCode", UserSettings.Current.TctCode)
            _reportDataSet.EnvironmentParams.Add("@Environment_TownCityName", UserSettings.Current.TctName)
            _reportDataSet.EnvironmentParams.Add("@Environment_PrgmName", UserSettings.Current.PrgmName)

            Try
                InitForm()
            Catch ex As Exception
                _reportDataSetValidationException = ex
            End Try
        End If

        If _reportDataSetValidationException IsNot Nothing Then
            Me.AddErrorMessage(_reportDataSetValidationException.Message)
        End If
    End Sub

    Function ValidateReportParameters(ByRef params As String, ByVal showMessage As Boolean) As Boolean
        Dim result As Boolean = True
        Dim sErrorString As String = ""

        For Each reportParamRow As ReportParamRow In _reportDataSet.RootReport.GetReportParamRows()
            If reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_RADIO Then
                Dim radioBoxList As RadioButtonList = reportParamRow.Tag
                params &= "&" & Server.UrlEncode(reportParamRow.rppName) & "=" & Server.UrlEncode(radioBoxList.SelectedValue)
            ElseIf reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_DROPDOWN Then
                Dim dropDownList As DropDownList = reportParamRow.Tag
                params &= "&" & Server.UrlEncode(reportParamRow.rppName) & "=" & Server.UrlEncode(dropDownList.SelectedValue)
            ElseIf reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_CHECKBOX Then
                Dim checkBoxList As CheckBoxList = reportParamRow.Tag
                For Each listItem As ListItem In checkBoxList.Items
                    params &= "&" & Server.UrlEncode(listItem.Attributes("name")) & "="
                    If listItem.Selected Then params &= Server.UrlEncode(listItem.Value)
                Next
            ElseIf reportParamRow.rppTypeCode = ReportsConstants.ReportParamType_DATE Then
                Dim dateControl As UserControls_DateControl = reportParamRow.Tag
                If dateControl.IsValid Then
                    If dateControl.Date <> Date.MinValue Then
                        params &= "&" & Server.UrlEncode(reportParamRow.rppName) & "=" & dateControl.Date.ToString(QueryStringDateFormat)
                    End If
                Else
                    'If showMessage Then Me.AddWarningMessage("Enter a valid " & reportParamRow.rppDescription)
                    If showMessage Then sErrorString = sErrorString & """" & reportParamRow.rppDescription & """ is invalid<br/>" 'Me.AddWarningMessage("""" & reportParamRow.rppDescription & """ is invalid")
                    result = False
                End If
            Else
                Dim textBox As UserControls_RegExTextBox = reportParamRow.Tag
                If textBox.IsTextValid Then
                    params &= "&" & Server.UrlEncode(reportParamRow.rppName) & "="
                    If Not String.IsNullOrEmpty(textBox.Text.Trim()) Then
                        params &= Server.UrlEncode(textBox.Text.Trim())
                    End If
                Else
                    If showMessage Then Me.AddWarningMessage(textBox.ErrorMessage)
                    result = False
                End If
            End If
        Next
        If Not sErrorString.Equals(String.Empty) Then
            Me.AddWarningMessage(sErrorString)
        End If
        Return result
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then
            parameterCollapsiblePanel.Collapsed = False
            reportPanel.Visible = False
            Return
        End If
        If _reportDataSetValidationException IsNot Nothing Then Return
        If _reportDataSet Is Nothing Then Return

        parameterPanel.Visible = True
        parameterCollapsiblePanel.Visible = True
        parameterCollapsiblePanel.Collapsed = True

        Dim params As String = ""
        If Not String.IsNullOrEmpty(Request.QueryString("reportreset")) OrElse Not ValidateReportParameters(params, False) Then
            parameterCollapsiblePanel.Collapsed = False
            reportPanel.Visible = False
            Return
        End If

        reportPanel.Visible = True

        'Dim iframeUrl As String = ReportsConfiguration.Instance.ReportsUrl _
        ' & "?/" & Server.UrlPathEncode(Me.CompanyName & _reportDataSet.RootReport.repReportName) _
        ' & params & "&rs:Command=Render&rc:Parameters=false" & "&rc:LinkTarget=" & reportIFrame.ClientID

        Dim iframeUrl As String = ReportsConfiguration.Instance.ReportsUrl _
         & "?" & Server.UrlPathEncode(_reportDataSet.RootReport.repReportName) _
         & params & "&rs:Command=Render&rc:Parameters=false" & "&rc:LinkTarget=" & reportIFrame.ClientID

        reportIFrame.Attributes.Add("src", iframeUrl)
        'reportIFrame.Attributes.Add("width", ReportsConfiguration.Instance.ReportsIFrameWidth)
        'reportIFrame.Attributes.Add("height", ReportsConfiguration.Instance.ReportsIFrameHeight)
    End Sub

    Protected Sub viewButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles viewButton.Click
        If _reportDataSet Is Nothing Then Return
        If _reportDataSetValidationException IsNot Nothing Then Return

        parameterPanel.Visible = True
        parameterCollapsiblePanel.Visible = True

        Dim params As String = ""
        If Not ValidateReportParameters(params, True) Then
            parameterCollapsiblePanel.Collapsed = False
            reportPanel.Visible = False
            Return
        End If

        Response.Redirect(BaseUrl & params)
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        Response.Redirect(BaseUrl & "&reportreset=true")
    End Sub

End Class
