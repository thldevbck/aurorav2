<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FerryItemsUserControl.ascx.vb"
    Inherits="SalesAndMarketing_Controls_FerryItemsUserControl" %>
<table style="width: 100%" cellpadding="2" cellspacing="0" border="1">
    <tr>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 210px" colspan="7">
            <b>Ferry Items::</b>
        </td>
    </tr>
    <tr>
        <%--<td style="border-top-width: 1px; border-top-style: dotted; width: 250px">
            Ferry Items:
        </td>--%>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px">
            <asp:DropDownList ID="ferryItemsDropdown" runat="server" Width="350">
            </asp:DropDownList>
        </td>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px">
            <asp:Button ID="addButton" runat="server" Text="Add" CssClass="Button_Standard Button_Add" />
        </td>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px">
            &nbsp;&nbsp;
            <asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="Button_Standard Button_Remove" />
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <asp:UpdatePanel ID="FerryItemsUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="FerryItemsGridView" runat="server" CssClass="dataTableGrid" AutoGenerateColumns="False"
                        Width="100%" DataKeyNames="ProductID" ShowHeader ="true" AllowSorting="true">
                        <RowStyle CssClass="evenRow" />
                        <AlternatingRowStyle CssClass="oddRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Product Code" 
                                               ItemStyle-VerticalAlign="Middle" 
                                               ItemStyle-Width="450">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="productcodeCheckbox" Text='<%# eval("ProductCode") %>'   AutoPostBack = "true"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Display When Condition" 
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="100">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" ID="displayConditionCheckboxlist" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" RepeatColumns="1" Width="100%" AutoPostBack="true">
                                    </asp:CheckBoxList>
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Display When
                                    <asp:DropDownList runat="server" ID="displayConditiondropdown" OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                        AutoPostBack="true" Width="150" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Don't Display When Condition" ItemStyle-VerticalAlign="Top">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" ID="dontDisplayConditionCheckboxlist" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" RepeatColumns="1" Width="100%" AutoPostBack="true">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                    Don't Display
                                    <asp:DropDownList runat="server" ID="dontDisplayConditionDropdown" OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                        AutoPostBack="true" Width="150" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Calculate Condition" ItemStyle-VerticalAlign="Top">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" ID="CalculateDisplayConditionCheckboxlist" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" RepeatColumns="1" Width="100%" AutoPostBack="true">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                  Also  Calculate When <asp:DropDownList runat="server" ID="CalculateDisplayConditionDropdown"
                                        OnSelectedIndexChanged="dropdown_SelectedIndexChanged" AutoPostBack="true" Width="150" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField HeaderText="Order" ItemStyle-VerticalAlign="Top" ItemStyle-Width="50" SortExpression="OrderNumber" ItemStyle-HorizontalAlign="Center" >
                                <ItemTemplate>
                                    <asp:TextBox ID="ordernumbertextbox" runat="server" MaxLength="2" Text='<%# eval("OrderNumber") %>'
                                        Width="40" OnTextChanged="textbox_TextChanged"  AutoPostBack="false"/>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width: 100%" cellpadding="2" cellspacing="0" class="dataTableGrid">
                                <thead>
                                    <tr class="oddRow">
                                        <td>
                                            Product Code
                                        </td>
                                        <td>
                                            Display When Condition
                                        </td>
                                        <td>
                                            Don't Display When Condition
                                        </td>
                                        <td>
                                            Calculate Condition
                                        </td>
                                        <td>
                                            Order Number
                                        </td>
                                    </tr>
                                </thead>
                                <tr class="evenRow">
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
