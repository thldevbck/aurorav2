<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InsurancesUserControl.ascx.vb"
    Inherits="SalesAndMarketing_Controls_InsurancesUserControl" %>
<table style="width: 100%" cellpadding="2" cellspacing="0" border="1">
    <tr>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 210px" colspan="7" >
         <b>   Insurance Option:</b>
        </td>
    </tr>
    <tr>
    <%--    <td style="border-top-width: 1px; border-top-style: dotted; width: 210px">
            Insurance Option:
        </td>--%>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 350px">
            <asp:DropDownList ID="InsuranceDropdown" runat="server" Width="350">
            </asp:DropDownList>
        </td>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px">
            <asp:Button ID="addButton" runat="server" Text="Add" CssClass="Button_Standard Button_Add" />
        </td>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px">
            &nbsp;&nbsp;
            <asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="Button_Standard Button_Remove" />
        </td>
    </tr>
    <tr>
        <td colspan="7">
            <asp:UpdatePanel ID="InsuranceUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="InsuranceGridView" runat="server" CssClass="dataTableGrid" AutoGenerateColumns="False"
                        Width="100%" DataKeyNames="ProductID" ShowHeader ="true" AllowSorting="true">
                        <RowStyle CssClass="evenRow" />
                        <AlternatingRowStyle CssClass="oddRow" />
                        
                        <Columns>
                            
                            <asp:TemplateField HeaderText="Product Code"  
                                               ItemStyle-Width="90" 
                                               HeaderStyle-VerticalAlign="Middle" 
                                               ItemStyle-VerticalAlign="Top" 
                                               ItemStyle-Font-Size="Small"
                                               HeaderStyle-Font-Size="Small">
                                               
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="productcodeCheckbox" Text='<%# eval("ProductCode") %>'  AutoPostBack="true" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Display When Condition" 
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="70" 
                                               HeaderStyle-VerticalAlign="Bottom" 
                                               HeaderStyle-HorizontalAlign=Left
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" 
                                                      ID="displayConditionCheckboxlist" 
                                                      RepeatDirection="Horizontal"
                                                      RepeatLayout="Flow" 
                                                      RepeatColumns="1" 
                                                      Width="65%" 
                                                      AutoPostBack="true" 
                                                      Font-Size="Smaller">
                                    </asp:CheckBoxList>
                                </ItemTemplate>
                                <HeaderTemplate>
                                 Display When<asp:DropDownList runat="server" 
                                                               ID="displayConditiondropdown" 
                                                               OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                                               AutoPostBack="true" 
                                                               Width="100" 
                                                               Font-Size="Small" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Don't Display When Condition" 
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="70" 
                                               HeaderStyle-VerticalAlign="Bottom" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" 
                                                ID="dontDisplayConditionCheckboxlist" 
                                                RepeatDirection="Horizontal"
                                                RepeatLayout="Flow" 
                                                RepeatColumns="1" 
                                                Width="65%" 
                                                AutoPostBack="true"
                                                Font-Size="Smaller">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate>
                                   Don't Display<asp:DropDownList runat="server" ID="dontDisplayConditionDropdown" OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                        AutoPostBack="true" Width="110"  />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Calculate Condition" 
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="70" 
                                               HeaderStyle-VerticalAlign="Bottom" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" 
                                                      ID="CalculateDisplayConditionCheckboxlist" 
                                                      RepeatDirection="Horizontal"
                                                      RepeatLayout="Flow" 
                                                      RepeatColumns="1" 
                                                      Width="65%" 
                                                      AutoPostBack="true"
                                                      Font-Size="Smaller">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate >
                                  Also Calculate When<asp:DropDownList runat="server" ID="CalculateDisplayConditionDropdown" OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                        AutoPostBack="true" Width="110" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                          
                            <asp:TemplateField HeaderText="Bond" 
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="70" 
                                               HeaderStyle-VerticalAlign="Bottom" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" 
                                                      ID="BondCheckboxlist" 
                                                      RepeatDirection="Horizontal"
                                                      RepeatLayout="Flow" 
                                                      RepeatColumns="1" 
                                                      Width="65%" 
                                                      AutoPostBack="true"
                                                      Font-Size="Smaller">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate >
                                  Bond<asp:DropDownList runat="server" ID="BondDropdown" OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Imprint" 
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="70" 
                                               HeaderStyle-VerticalAlign="Bottom" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" 
                                                      ID="ImprintCheckboxlist" 
                                                      RepeatDirection="Horizontal"
                                                      RepeatLayout="Flow" 
                                                      RepeatColumns="1" 
                                                      Width="65%" 
                                                      AutoPostBack="true"
                                                      Font-Size="Smaller">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate >
                                  Imprint<asp:DropDownList runat="server" ID="ImprintDropdown" OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                        AutoPostBack="true" Width="100" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                               
                            <asp:TemplateField HeaderText="Rating" 
                                               ItemStyle-VerticalAlign="Top" 
                                               ItemStyle-Width="30" 
                                               ItemStyle-HorizontalAlign="Center" 
                                               HeaderStyle-VerticalAlign="Middle" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:TextBox ID="Ratingtextbox" runat="server" MaxLength="2" Text='<%# eval("Rating") %>'
                                        Width="20" OnTextChanged="textbox_TextChanged"  AutoPostBack="false" Font-Size="X-Small"/>
                                </ItemTemplate>
                            </asp:TemplateField>                 
                            
                            <asp:TemplateField HeaderText="Order" 
                                               ItemStyle-VerticalAlign="Top" 
                                               ItemStyle-Width="30" 
                                               SortExpression="OrderNumber" 
                                               ItemStyle-HorizontalAlign="Center" 
                                               HeaderStyle-VerticalAlign="Middle" 
                                               HeaderStyle-HorizontalAlign="Center" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:TextBox ID="ordernumbertextbox" runat="server" MaxLength="2" Text='<%# eval("OrderNumber") %>'
                                        Width="20" OnTextChanged="textbox_TextChanged"  AutoPostBack="false" Font-Size="X-Small"/>
                                </ItemTemplate>
                            </asp:TemplateField>             

                            <asp:TemplateField HeaderText="TargetSite"
                                               ItemStyle-VerticalAlign="Top" 
                                               HeaderStyle-Width="70" 
                                               HeaderStyle-VerticalAlign="Bottom" 
                                               ItemStyle-Font-Size="Small" 
                                               HeaderStyle-Font-Size="Small">
                                <ItemTemplate>
                                    <asp:CheckBoxList runat="server" 
                                                     ID="targetSiteCheckboxlist" 
                                                     RepeatDirection="Horizontal"
                                                     RepeatLayout="Flow" 
                                                     RepeatColumns="1" 
                                                     Width="100%" 
                                                     AutoPostBack="true"
                                                     Font-Size="Smaller">
                                    </asp:CheckBoxList>
                                    &nbsp;
                                </ItemTemplate>
                                <HeaderTemplate >
                                  TargetSite<asp:DropDownList runat="server"
                                                              ID="TargetSiteDropdown" 
                                                              OnSelectedIndexChanged="dropdown_SelectedIndexChanged"
                                                              AutoPostBack="true" 
                                                              Width="100"  />
                                </HeaderTemplate>
                            </asp:TemplateField>

                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width: 100%" cellpadding="2" cellspacing="0" class="dataTableGrid">
                                <thead>
                                    <tr class="oddRow">
                                        <td>
                                            Product Code
                                        </td>
                                        <td>
                                            Display When Condition
                                        </td>
                                        <td>
                                            Don't Display When Condition
                                        </td>
                                        <td>
                                            Calculate Condition
                                        </td>
                                        
                                        <%--start added row--%>
                                        <td>
                                            Bond
                                        </td>
                                        <td>
                                            Imprint
                                        </td>
                                        <td>
                                            Rating
                                        </td>
                                        <%--end added row--%>
                                        
                                        <td>
                                            Order Number
                                        </td>
                                        <%-- TargetSite --%>
                                        <td>
                                            TargetSite
                                        </td>

                                    </tr>
                                </thead>
                                <tr class="evenRow">
                                    <td>&nbsp;</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <%--added row--%>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <%-- TargetSite --%>
                                    <td></td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
