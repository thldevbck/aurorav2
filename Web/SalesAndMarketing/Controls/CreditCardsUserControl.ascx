<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CreditCardsUserControl.ascx.vb"
    Inherits="SalesAndMarketing_Controls_CreditCardsUserControl" %>
<table style="width: 100%" cellpadding="2" cellspacing="0" border="1">
<tr>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 210px" colspan="7" >
         <b>    Credit Card Type:</b>
        </td>
    </tr>
    <tr>
        <%--<td style="border-top-width: 1px; border-top-style: dotted; width: 250px">
            Credit Card Type:
        </td>--%>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px">
            <asp:DropDownList ID="CreditCardDropdown" runat="server" Width="350">
            </asp:DropDownList>
        </td>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 250px" />
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px">
            <asp:Button ID="addButton" runat="server" Text="Add" CssClass="Button_Standard Button_Add" />
        </td>
        <td style="border-top-width: 1px; border-top-style: dotted; width: 150px">
            &nbsp;&nbsp;<asp:Button ID="RemoveButton" runat="server" Text="Remove" CssClass="Button_Standard Button_Remove" />
        </td>
    </tr>
    <tr>
        <td colspan="3"> 
            <asp:UpdatePanel ID="CreditCardUpdatePanel" runat="server">
                <ContentTemplate>
                    <asp:GridView ID="CreditCardGridView" runat="server" CssClass="dataTableGrid" AutoGenerateColumns="False"
                        Width="80%" DataKeyNames="PtmID" ShowHeader ="true">
                        <RowStyle CssClass="evenRow" />
                        <AlternatingRowStyle CssClass="oddRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Card type" ItemStyle-VerticalAlign="Top">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="creditcardCheckbox" Text='<%# eval("PtmName") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <table style="width: 100%" cellpadding="2" cellspacing="0" class="dataTableGrid">
                                <thead>
                                    <tr class="oddRow">
                                        <td>
                                            Card type
                                        </td>
                                    </tr>
                                </thead>
                                <tr class="evenRow">
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </ContentTemplate>
            </asp:UpdatePanel>
        </td>
    </tr>
</table>
