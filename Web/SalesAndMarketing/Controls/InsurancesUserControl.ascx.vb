
Imports System.io
Imports System.Xml
Imports System.data
Imports aurora.SalesAndMarketing
Imports System.Collections.Generic

Partial Class SalesAndMarketing_Controls_InsurancesUserControl
    Inherits AuroraUserControl

#Region "constant"
    Const NEW_FLAG As String = "<font color='red'><sup><b>&nbsp;NEW</b></sup></font>"
    Const ORDER_ERROR As String = "The value you have entered is not a number.This will be converted to Zero as the default."
    Const DUPLICATE_WHEN_PRODUCT As String = " is already assigned to this "
    Const DUPLICATE_ROW As String = "This XXX has already been assigned to this group. "
    Const CHECK_ERROR_MSG As String = "Please tick Insurance product you wish to modify!"


    Const COLUMN_COUNT As Integer = 18
    Const DUPLICATE_WHEN_ALL As String = "The vehicle you selected was already added in '"
#End Region
    '
#Region "variables"

    Private tableInsurance As System.Data.DataTable
  
#End Region

#Region "public properties"

    Public Property Country() As String
        Get
            Return CStr(ViewState("CountryParam"))
        End Get
        Set(ByVal value As String)
            ViewState("CountryParam") = value
        End Set
    End Property

    Public Property Brand() As String
        Get
            Return CStr(ViewState("BrandParam"))
        End Get
        Set(ByVal value As String)
            ViewState("BrandParam") = value
        End Set
    End Property

    Public Property VehicleType() As String
        Get
            Return CStr(ViewState("VehicleTypeParam"))
        End Get
        Set(ByVal value As String)
            ViewState("VehicleTypeParam") = value
        End Set
    End Property

    Public ReadOnly Property InsuranceTable() As DataTable
        Get
            If Not CheckDataTableIfExist() Then CreateTableColumns()
            XMLtoDatatable()
            RowIndexInsurance = ""
            SortInsurance = ""
            Return tableInsurance
        End Get
    End Property

    Public Property ReturnAllImprintVehicle() As String
        Get
            Return ViewState("ReturnAllImprintVehicle")
        End Get
        Set(ByVal value As String)
            ViewState("ReturnAllImprintVehicle") = value
        End Set
    End Property

    Public Property ReturnAllBondVehicle() As String
        Get
            Return ViewState("ReturnAllBondVehicle")
        End Get
        Set(ByVal value As String)
            ViewState("ReturnAllBondVehicle") = value
        End Set
    End Property

#End Region

#Region "private properties"

    Private Property RowIndexInsurance() As String
        Get
            Return ViewState("RowIndexInsurance")
        End Get
        Set(ByVal value As String)
            ViewState("RowIndexInsurance") = value
        End Set
    End Property

    Private Property tableInsuranceXML() As String
        Get
            Return CStr(ViewState("tableInsuranceXML"))
        End Get
        Set(ByVal value As String)
            ViewState("tableInsuranceXML") = value
        End Set
    End Property

    Private Property SortInsurance() As String
        Get
            Return ViewState("SortInsurance")
        End Get
        Set(ByVal value As String)
            ViewState("SortInsurance") = value
        End Set
    End Property
#End Region

#Region "public procedures"

    Public Sub UnBind()
        Me.InsuranceDropdown.Items.Clear()
        Me.InsuranceGridView.DataSource = Nothing
        Me.InsuranceGridView.DataBind()
    End Sub

    Public Sub BindDropDown()
        Dim view As DataView = Data.DataRepository.Get_WebProductList("INS", Country, Brand, VehicleType).Tables(0).DefaultView
        view.Sort = "Description Asc"
        Me.InsuranceDropdown.DataSource = Nothing
        Me.InsuranceDropdown.DataBind()

        Me.InsuranceDropdown.Items.Add("(Select)")
        Me.InsuranceDropdown.AppendDataBoundItems = True
        Me.InsuranceDropdown.DataSource = view
        Me.InsuranceDropdown.DataValueField = "PrdId"
        Me.InsuranceDropdown.DataTextField = "Description"
        Me.InsuranceDropdown.DataBind()
    End Sub

    Public Sub ResetXMLs()
        tableInsuranceXML = ""
        RowIndexInsurance = ""
        SortInsurance = ""
    End Sub

#End Region

#Region "private procedures"

    Sub BindGridView(ByVal sortedCondition As String)
        XMLtoDatatable()
        Dim filteredView As DataView = tableInsurance.DefaultView
        filteredView.Sort = sortedCondition
        filteredView.RowFilter = "ParentDeletedBoolean = 'False'"
        Me.InsuranceGridView.DataSource = filteredView
        Me.InsuranceGridView.DataBind()
        RemoveButton.Enabled = IIf(filteredView.Count > 0, True, False)
    End Sub

    Sub BindGridView()
        Dim filteredView As DataView = NewRecord.DefaultView
        filteredView.RowFilter = "ParentDeletedBoolean = 'False'"
        Me.InsuranceGridView.DataSource = filteredView ''NewRecord()
        Me.InsuranceGridView.DataBind()
        RemoveButton.Enabled = IIf(filteredView.Count > 0, True, False)
    End Sub

    Sub BindGridView(ByVal tablename As DataTable)
        Dim filteredView As DataView = tablename.DefaultView
        If String.IsNullOrEmpty(SortInsurance) Then SortInsurance = "OrderNumber ASC"
        filteredView.RowFilter = "ParentDeletedBoolean = 'False'"
        Me.InsuranceGridView.DataSource = filteredView ''tablename
        Me.InsuranceGridView.DataBind()
        RemoveButton.Enabled = IIf(filteredView.Count > 0, True, False)
    End Sub

    Sub CreateTableColumns()
        If tableInsurance Is Nothing Then tableInsurance = New DataTable("Insurance")
        If tableInsurance.Columns.Count = COLUMN_COUNT Then Exit Sub

        Dim dcProductID As New DataColumn("ProductID", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcProductID)

        Dim dcProductcode As New DataColumn("ProductCode", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcProductcode)

        Dim dcCodeDisplayCondition As New DataColumn("CodeDisplayCondition", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcCodeDisplayCondition)

        Dim dcCodeDontDisplayCondition As New DataColumn("CodeDontDisplayCondition", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcCodeDontDisplayCondition)

        Dim dcCalculateCondition As New DataColumn("CalculateCondition", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcCalculateCondition)

        Dim dcOrderNumber As New DataColumn("OrderNumber", System.Type.GetType("System.Int32"))
        tableInsurance.Columns.Add(dcOrderNumber)

        ''added for ids 
        Dim dcCodeDisplayConditionID As New DataColumn("CodeDisplayConditionID", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcCodeDisplayConditionID)

        Dim dcCodeDontDisplayConditionID As New DataColumn("CodeDontDisplayConditionID", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcCodeDontDisplayConditionID)

        Dim dcCalculateConditionID As New DataColumn("CalculateConditionID", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcCalculateConditionID)

        ''------- CONTAINS ID'S TO BE DELETED-----------------
        Dim dcParentDeletedBoolean As New DataColumn("ParentDeletedBoolean", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcParentDeletedBoolean)

        Dim dcChildDeletedID As New DataColumn("ChildDeletedID", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcChildDeletedID)

        Dim dcChildDeletedAction As New DataColumn("ChildDeletedAction", System.Type.GetType("System.String"))
        tableInsurance.Columns.Add(dcChildDeletedAction)

        ''add datetime for sorting
        Dim dcAddDatetime As New DataColumn("AddDateTime", System.Type.GetType("System.DateTime"))
        tableInsurance.Columns.Add(dcAddDatetime)

        Dim key(1) As System.Data.DataColumn
        key(0) = tableInsurance.Columns("ProductID")
        tableInsurance.PrimaryKey = key

        ''rev:mia sept 4, 2009
        BondImprintAndRating(tableInsurance)

        ''rev:mia March 8 2011
        TargetSite(tableInsurance)
    End Sub

    Sub XMLtoDatatable()

        If Not String.IsNullOrEmpty(tableInsuranceXML) Then
            Dim xmlReader As New XmlTextReader(tableInsuranceXML, System.Xml.XmlNodeType.Document, Nothing)
            xmlReader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(xmlReader)
            tableInsurance = ds.Tables(0)

            Dim key(1) As System.Data.DataColumn
            key(0) = tableInsurance.Columns("ProductID")
            tableInsurance.PrimaryKey = key
        End If
    End Sub

    Sub RemoveItemsInCheckBoxList(ByRef row As DataRow, ByVal COLUMN_NAME As String, ByVal itemToRemove As String, ByVal itemToRemoveValue As String, ByVal childActionID As String)

        ''rev:mia sept 4, 2009
        Dim tempAry() As String = Nothing
        If row(COLUMN_NAME) IsNot Nothing Then
            Dim textrow As String = row(COLUMN_NAME).ToString
            If textrow.Contains("All") Then
                row(COLUMN_NAME) = ""
                row(COLUMN_NAME & "ID") = ""
            Else
                If textrow.Contains(itemToRemove) Then
                    Dim ary() As String = textrow.Split(",")
                    textrow = ""
                    For Each item As String In ary
                        If Not String.IsNullOrEmpty(item) Then
                            If item <> itemToRemove.TrimEnd Then
                                textrow = textrow & item & ","
                            End If
                        End If
                    Next

                    
                    If textrow.EndsWith(",") Then
                        textrow = textrow.Remove(textrow.Length - 1, 1)
                    End If
                    If textrow.StartsWith(",") Then
                        textrow = textrow.Remove(0, 1)
                    End If
                    row(COLUMN_NAME) = textrow
                End If

                If row(COLUMN_NAME & "ID") IsNot Nothing Then
                    Dim textrowIDs As String = row(COLUMN_NAME & "ID").ToString
                    Dim aryTemp() As String = textrowIDs.Split(",")
                    textrowIDs = ""
                    ''rev:mia sept 7, 2009

                    For Each item As String In aryTemp
                        If Not String.IsNullOrEmpty(item) Then
                            If item <> itemToRemoveValue.TrimEnd Then
                                textrowIDs = textrowIDs & item & ","
                            End If
                        End If
                    Next

                    If textrowIDs.Equals(",") Then
                        row(COLUMN_NAME & "ID") = ""
                    Else
                        row(COLUMN_NAME & "ID") = textrowIDs & ","
                    End If
                End If

            End If
            

            If row("ChildDeletedID") IsNot Nothing And ((childActionID <> "4" And childActionID <> "5" And Not textrow.Contains("All"))) Then
                Dim textrowID As String = row("ChildDeletedID").ToString
                textrowID = textrowID & itemToRemoveValue & ","
                If textrowID.Equals(",") Then
                    textrowID = ""
                End If
                row("ChildDeletedID") = textrowID

                Dim txtchildActionID As String = row("ChildDeletedAction")
                row("ChildDeletedAction") = txtchildActionID & childActionID & ","
            End If

        End If
        
    End Sub

#End Region

#Region "private functions"
    Private Function ItemsIsCheck() As Boolean
        Dim retValue As Boolean = False
        For Each row As GridViewRow In Me.InsuranceGridView.Rows
            Dim productcodeCheckbox As CheckBox = CType(row.FindControl("productcodeCheckbox"), CheckBox)
            If productcodeCheckbox IsNot Nothing Then
                retValue = productcodeCheckbox.Checked
                If retValue = True Then Return retValue
            End If
        Next
        Return False
    End Function
    ''RowIndexInsurance
    Private Sub CheckRowIndexAndAppend(ByVal index As String)
        If String.IsNullOrEmpty(RowIndexInsurance) Then
            RowIndexInsurance = String.Concat(RowIndexInsurance, index & ",")
        Else
            Dim tempAry() As String = RowIndexInsurance.Split(",")
            Dim isExist As Boolean = False
            For Each temp As String In tempAry
                If temp.Equals(index) Then
                    isExist = True
                    Exit For
                End If
            Next
            If Not isExist Then
                RowIndexInsurance = String.Concat(RowIndexInsurance, index & ",")
            End If
        End If
    End Sub

    Private Sub CheckRowIndexAndRemove(ByVal index As String)
        If String.IsNullOrEmpty(RowIndexInsurance) Then
            Exit Sub
        Else
            Dim tempAry() As String = RowIndexInsurance.Split(",")
            Dim tempRowIndexInsurance As String = ""
            For Each temp As String In tempAry
                If Not temp.Equals(index) And Not String.IsNullOrEmpty(temp) Then
                    tempRowIndexInsurance = String.Concat(tempRowIndexInsurance, temp & ",")
                End If
            Next
            RowIndexInsurance = tempRowIndexInsurance
        End If
    End Sub


    Private Function ConvertOrderNumber(ByVal ordernumber As String) As String
        Dim retvalue As String = "0"
        If String.IsNullOrEmpty(ordernumber) Then
            Return retvalue
        End If
        Try
            retvalue = Convert.ToInt16(CInt(ordernumber))
        Catch ex As Exception
            MyBase.CurrentPage.SetWarningShortMessage(ORDER_ERROR)
        End Try
        Return retvalue
    End Function

    Function StreamXMLDatatable() As String
        If tableInsurance.Rows.Count - 1 = -1 Then Return ""
        Dim stream As New MemoryStream
        tableInsurance.WriteXml(stream, XmlWriteMode.WriteSchema)
        stream.Position = 0
        Dim ds As New DataSet
        ds.ReadXml(stream)
        Return ds.GetXml
    End Function

    Function CheckDataTableIfExist() As Boolean
        Return IIf(tableInsurance Is Nothing, False, True)
    End Function

    Function IsDuplicateRecord() As Boolean

        For Each row As GridViewRow In Me.InsuranceGridView.Rows
            If row.RowType = DataControlRowType.DataRow Then
                If Me.InsuranceGridView.DataKeys(row.RowIndex).Value.ToString.Equals(Me.InsuranceDropdown.SelectedValue) Then
                    Return True
                Else
                    Dim key As String = Me.InsuranceGridView.DataKeys(row.RowIndex).Value
                    If key.Contains("|") Then
                        key = key.Remove(key.IndexOf("|"))
                        If key.Equals(Me.InsuranceDropdown.SelectedValue) Then
                            Return True
                        End If
                    End If
                End If

            End If
        Next

        Return False

    End Function

    Function NewRecord() As DataTable
        If (Me.InsuranceGridView.Rows.Count = 0) Then tableInsuranceXML = ""
        XMLtoDatatable()

        If Not Me.InsuranceDropdown.SelectedItem.Text = "(Select)" Then

            Dim addrow As DataRow = tableInsurance.NewRow()

            addrow("ProductID") = Me.InsuranceDropdown.SelectedValue
            addrow("ProductCode") = Me.InsuranceDropdown.SelectedItem.Text.Split("-")(0) & NEW_FLAG
            addrow("CodeDisplayCondition") = ""
            addrow("CodeDisplayConditionID") = ""
            addrow("CodeDontDisplayCondition") = ""
            addrow("CodeDontDisplayConditionID") = ""
            addrow("CalculateCondition") = ""
            addrow("CalculateConditionID") = ""
            addrow("OrderNumber") = 0
            addrow("ParentDeletedBoolean") = "False"  ''delete if true. Default value is false
            addrow("ChildDeletedID") = "" ''this will hold all dependencies that will be deleted
            addrow("ChildDeletedAction") = "" ''this will hold the action id 
            addrow("AddDateTime") = Now.ToString

            ''rev:mia sept 4 2009
            NewRecordBondImprintAndRating(addrow)

            ''rev: mia March 8 2011
            NewRecordTargetSite(addrow)

            If IsDuplicateRecord() Then
                Dim tempMsg As String = DUPLICATE_ROW
                tempMsg = tempMsg.Replace("XXX", " '" & Me.InsuranceDropdown.SelectedItem.Text & "' ")
                MyBase.CurrentPage.SetWarningShortMessage(tempMsg)
            Else
                tableInsurance.Rows.Add(addrow)
            End If

        End If

        tableInsuranceXML = StreamXMLDatatable()
        Return tableInsurance
    End Function

    Function RemoveRecord() As DataTable
        XMLtoDatatable()
        For Each row As GridViewRow In Me.InsuranceGridView.Rows
            Dim productcodeCheckbox As CheckBox = CType(row.FindControl("productcodeCheckbox"), CheckBox)
            Dim key As String = Me.InsuranceGridView.DataKeys(row.RowIndex).Value

            If productcodeCheckbox IsNot Nothing Then
                If productcodeCheckbox.Checked Then
                    Dim removeRow As DataRow = tableInsurance.Rows.Find(key)
                    If removeRow IsNot Nothing Then
                        removeRow("ParentDeletedBoolean") = "True"
                    End If
                Else
                    ''rev:mia june 29, 2009 - problem when removing the checkbox value
                    CheckRowIndexAndRemove(row.RowIndex.ToString)
                End If
            End If

            Dim displayConditionCheckboxlist As CheckBoxList = CType(row.FindControl("displayConditionCheckboxlist"), CheckBoxList)
            Dim dontDisplayConditionCheckboxlist As CheckBoxList = CType(row.FindControl("dontDisplayConditionCheckboxlist"), CheckBoxList)
            Dim CalculateDisplayConditionCheckboxlist As CheckBoxList = CType(row.FindControl("CalculateDisplayConditionCheckboxlist"), CheckBoxList)
            ''rev:mia sept 4 2009
            Dim BondCheckboxlist As CheckBoxList = CType(row.FindControl("BondCheckboxlist"), CheckBoxList)
            Dim ImprintCheckboxlist As CheckBoxList = CType(row.FindControl("ImprintCheckboxlist"), CheckBoxList)

            ''rev:mia March 10 2011
            Dim targetSiteCheckboxlist As CheckBoxList = CType(row.FindControl("targetSiteCheckboxlist"), CheckBoxList)

            Dim editrow As DataRow = tableInsurance.Rows.Find(key)
            If editrow IsNot Nothing Then
                Dim COLUMN_NAME As String = ""

                If displayConditionCheckboxlist IsNot Nothing Then
                    For Each item As ListItem In displayConditionCheckboxlist.Items
                        If item.Selected Then
                            COLUMN_NAME = "CodeDisplayCondition"
                            RemoveItemsInCheckBoxList(editrow, COLUMN_NAME, item.Text.TrimEnd, item.Value, 1)
                        End If
                    Next
                End If

                If dontDisplayConditionCheckboxlist IsNot Nothing Then
                    For Each item As ListItem In dontDisplayConditionCheckboxlist.Items
                        If item.Selected Then
                            COLUMN_NAME = "CodeDontDisplayCondition"
                            RemoveItemsInCheckBoxList(editrow, COLUMN_NAME, item.Text.TrimEnd, item.Value, 2)
                        End If
                    Next
                End If

                If CalculateDisplayConditionCheckboxlist IsNot Nothing Then
                    For Each item As ListItem In CalculateDisplayConditionCheckboxlist.Items
                        If item.Selected Then
                            COLUMN_NAME = "CalculateCondition"
                            RemoveItemsInCheckBoxList(editrow, COLUMN_NAME, item.Text.TrimEnd, item.Value, 3)
                        End If
                    Next
                End If

                ''rev:mia sept 4 2009
                If BondCheckboxlist IsNot Nothing Then
                    For Each item As ListItem In BondCheckboxlist.Items
                        If item.Selected Then
                            COLUMN_NAME = "Bond"
                            RemoveItemsInCheckBoxList(editrow, COLUMN_NAME, item.Text.TrimEnd, item.Value, 4)
                        End If
                    Next
                End If

                If ImprintCheckboxlist IsNot Nothing Then
                    For Each item As ListItem In ImprintCheckboxlist.Items
                        If item.Selected Then
                            COLUMN_NAME = "Imprint"
                            RemoveItemsInCheckBoxList(editrow, COLUMN_NAME, item.Text.TrimEnd, item.Value, 5)
                        End If
                    Next
                End If

                ''rev:mia March 10 2011
                If targetSiteCheckboxlist IsNot Nothing Then
                    For Each item As ListItem In targetSiteCheckboxlist.Items
                        If item.Selected Then
                            COLUMN_NAME = "TargetSite"
                            RemoveItemsInCheckBoxList(editrow, COLUMN_NAME, item.Text.TrimEnd, item.Value, 6)
                        End If
                    Next
                End If
            End If
        Next
        tableInsuranceXML = StreamXMLDatatable()
        Return tableInsurance
    End Function

    Function EditRecord(ByVal dropdown As DropDownList, Optional ByVal textBox As TextBox = Nothing) As DataTable
        XMLtoDatatable()
        For Each row As GridViewRow In Me.InsuranceGridView.Rows
            Dim productcodeCheckbox As CheckBox = CType(row.FindControl("productcodeCheckbox"), CheckBox)
            Dim key As String = Me.InsuranceGridView.DataKeys(row.RowIndex).Value

            Dim editrow As DataRow = tableInsurance.Rows.Find(key)

            If productcodeCheckbox IsNot Nothing Then
                If productcodeCheckbox.Checked Or Me.InsuranceGridView.Rows.Count = 1 Then
                    If productcodeCheckbox.Checked Then
                        CheckRowIndexAndAppend(row.RowIndex.ToString)
                    Else
                        CheckRowIndexAndRemove(row.RowIndex.ToString)
                    End If

                    If editrow IsNot Nothing Then
                        If dropdown IsNot Nothing Then
                            If dropdown.SelectedItem.Text = "(Select)" Then Return Nothing
                            Dim COLUMN_NAME As String = ""
                            Dim COLUMN_NAME_CHECK As String = ""
                            Select Case dropdown.ID
                                Case "displayConditiondropdown"
                                    COLUMN_NAME = "CodeDisplayCondition"
                                    COLUMN_NAME_CHECK = "CodeDontDisplayCondition"
                                Case "dontDisplayConditionDropdown"
                                    COLUMN_NAME = "CodeDontDisplayCondition"
                                    COLUMN_NAME_CHECK = "CodeDisplayCondition"
                                Case "CalculateDisplayConditionDropdown"
                                    COLUMN_NAME = "CalculateCondition"

                                    ''rev:mia sept. 4, 2009
                                Case "BondDropdown"
                                    COLUMN_NAME = "Bond"
                                Case "ImprintDropdown"
                                    COLUMN_NAME = "Imprint"

                                Case "TargetSiteDropdown"
                                    COLUMN_NAME = "TargetSite"
                            End Select

                            Dim isDuplicate As Boolean = False
                            Dim dropdowntext As String = dropdown.SelectedItem.Text.ToUpper.Trim
                            If editrow(COLUMN_NAME) IsNot Nothing Then
                                Dim textrow As String = editrow(COLUMN_NAME).ToString
                                Dim textrowcheck As String = ""
                                If Not String.IsNullOrEmpty(COLUMN_NAME_CHECK) Then
                                    textrowcheck = editrow(COLUMN_NAME_CHECK).ToString
                                End If

                                Dim temp As String = "'" & dropdowntext & "'" & DUPLICATE_WHEN_PRODUCT & "'" & editrow("ProductCode") & "' product code"
                                Dim tempAry() As String = Nothing
                                If textrow.Contains(dropdowntext) Then
                                    tempAry = textrow.Split(",")
                                    For i As Integer = 0 To tempAry.Length - 1
                                        If tempAry(i) = dropdowntext Then
                                            MyBase.CurrentPage.SetWarningShortMessage(temp)
                                            isDuplicate = True
                                        ElseIf tempAry(i) = dropdowntext & NEW_FLAG Then
                                            MyBase.CurrentPage.SetWarningShortMessage(temp)
                                            isDuplicate = True
                                        End If
                                    Next

                                ElseIf textrowcheck.Contains(dropdowntext) Then
                                    tempAry = textrowcheck.Split(",")
                                    For i As Integer = 0 To tempAry.Length - 1
                                        If tempAry(i) = dropdowntext Then
                                            MyBase.CurrentPage.SetWarningShortMessage(temp)
                                            isDuplicate = True
                                        ElseIf tempAry(i) = dropdowntext & NEW_FLAG Then
                                            MyBase.CurrentPage.SetWarningShortMessage(temp)
                                            isDuplicate = True
                                        End If
                                    Next

                                ElseIf editrow(COLUMN_NAME).ToString.Contains("All") Then
                                    MyBase.CurrentPage.SetWarningShortMessage(DUPLICATE_WHEN_ALL & editrow("ProductCode") & "' product code")
                                    isDuplicate = True
                                End If

                                If COLUMN_NAME = "Bond" And Not Me.CheckBondAllIsActive Then
                                    ReturnAllBondVehicle = ""
                                ElseIf COLUMN_NAME = "Imprint" And Not Me.CheckImprintAllIsActive Then
                                    ReturnAllImprintVehicle = ""
                                End If

                                If isDuplicate = False Then
                                    ''rev:mia sept. 4, 2009
                                    If dropdown.SelectedItem.Text <> "(All)" Then
                                        editrow(COLUMN_NAME) = dropdowntext & NEW_FLAG & "," & textrow
                                        editrow(COLUMN_NAME & "ID") = editrow(COLUMN_NAME & "ID") & "," & dropdown.SelectedValue
                                    Else
                                        editrow(COLUMN_NAME) = "All" & NEW_FLAG

                                        Dim tempAllID As String = ""
                                        Dim tempAllName As String = ""
                                        For Each item As ListItem In dropdown.Items
                                            If item.Text <> "(All)" And item.Text <> "(Select)" Then
                                                tempAllID = tempAllID & "," & item.Value
                                                tempAllName = tempAllName & "," & item.Text
                                            End If
                                        Next
                                        If tempAllID.StartsWith(",") Then
                                            tempAllID = tempAllID.Remove(0, 1)
                                            tempAllName = tempAllName.Remove(0, 1)
                                        End If
                                        editrow(COLUMN_NAME & "ID") = tempAllID

                                        If COLUMN_NAME = "Bond" Then
                                            ReturnAllBondVehicle = tempAllName
                                        ElseIf COLUMN_NAME = "Imprint" Then
                                            ReturnAllImprintVehicle = tempAllName
                                        End If

                                        ''End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                Else
                    ''rev:mia june 23, 2009 - problem when removing the checkbox value
                    CheckRowIndexAndRemove(row.RowIndex.ToString)
                    ''rev:mia june 29, 2009 - adding message of there are no selection in the checkbox grid
                    If textBox Is Nothing Then
                        If Not ItemsIsCheck() Then
                            MyBase.CurrentPage.SetInformationShortMessage(CHECK_ERROR_MSG)
                        End If
                    End If

                End If
                If editrow IsNot Nothing Then
                    Dim ordernumbertextbox As TextBox = CType(row.FindControl("ordernumbertextbox"), TextBox)
                    editrow("OrderNumber") = ConvertOrderNumber(ordernumbertextbox.Text)

                    ''rev:mia sept. 4, 2009
                    Dim ratingtextbox As TextBox = CType(row.FindControl("Ratingtextbox"), TextBox)
                    editrow("Rating") = ConvertOrderNumber(ratingtextbox.Text)
                End If
            End If
        Next
        tableInsuranceXML = StreamXMLDatatable()
        Return tableInsurance
    End Function

#End Region

#Region "Initialize Routine"

    Function IsValidArraySize(ByVal aryA As Array, ByVal aryB As Array) As Boolean
        Return aryA.Length = aryB.Length
    End Function

    Sub CurrentRecord(ByVal HreItmPrdId As String, _
                      ByVal PrdShortName As String, _
                      ByVal HreItmDispOrder As String, _
                      ByVal DependencyProductId As String, _
                      ByVal DependencyPrdShortName As String, _
                      ByVal DependencyAction As String, _
                      ByVal ActionId As String, _
                      ByVal AddDateTime As DateTime, _
                      Optional ByVal ID As String = "", _
                      Optional ByVal Rating As String = "0", _
                      Optional ByVal HreItemID As String = "", _
                      Optional ByVal SiteId As String = "")

        XMLtoDatatable()
        Dim addrow As DataRow = tableInsurance.NewRow()

        addrow("ProductID") = HreItmPrdId & ID
        addrow("ProductCode") = PrdShortName

        Dim aryAction() As String = ActionId.Split(",")
        Dim aryDependencyPrdShortName() As String = DependencyPrdShortName.Split(",")
        Dim aryDependencyProductId() As String = DependencyProductId.Split(",")

        If Not IsValidArraySize(aryDependencyPrdShortName, aryDependencyProductId) Then Exit Sub
        If Not IsValidArraySize(aryAction, aryDependencyProductId) Then Exit Sub
        Dim ctr As Integer = 0
        For Each action As String In aryAction
            If action = "" Then
                addrow("CalculateCondition") = ""
                addrow("CalculateConditionID") = ""

                addrow("CodeDontDisplayCondition") = ""
                addrow("CodeDontDisplayConditionID") = ""

                addrow("CodeDisplayCondition") = ""
                addrow("CodeDisplayConditionID") = ""


                ''rev:sept 4, 2009
                addrow("Imprint") = ""
                addrow("ImprintID") = ""

                addrow("Bond") = ""
                addrow("BondID") = ""

                addrow("TargetSiteID") = ""
                addrow("TargetSite") = ""

                Exit For
            End If

            Select Case action
                Case "1" ''1st dropdown on gridview

                    addrow("CodeDisplayCondition") = addrow("CodeDisplayCondition") & aryDependencyPrdShortName(ctr) & ","
                    addrow("CodeDisplayConditionID") = addrow("CodeDisplayConditionID") & aryDependencyProductId(ctr) & ","

                    If Not addrow("CodeDontDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayCondition")) Then
                            addrow("CodeDontDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDontDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayConditionID")) Then
                            addrow("CodeDontDisplayConditionID") = ""
                        End If
                    End If

                    ''-------  
                    If Not addrow("CalculateCondition") Is Nothing Then
                        If IsDBNull(addrow("CalculateCondition")) Then
                            addrow("CalculateCondition") = ""
                        End If
                    End If

                    If Not addrow("CalculateConditionID") Is Nothing Then
                        If IsDBNull(addrow("CalculateConditionID")) Then
                            addrow("CalculateConditionID") = ""
                        End If
                    End If


                    ''rev:sept 4, 2009
                    If Not addrow("Bond") Is Nothing Then
                        If IsDBNull(addrow("Bond")) Then
                            addrow("Bond") = ""
                        End If
                    End If

                    If Not addrow("BondID") Is Nothing Then
                        If IsDBNull(addrow("BondID")) Then
                            addrow("BondID") = ""
                        End If
                    End If


                    If Not addrow("Imprint") Is Nothing Then
                        If IsDBNull(addrow("Imprint")) Then
                            addrow("Imprint") = ""
                        End If
                    End If

                    If Not addrow("ImprintID") Is Nothing Then
                        If IsDBNull(addrow("ImprintID")) Then
                            addrow("ImprintID") = ""
                        End If
                    End If

                Case "2" ''2nd dropdown on gridview

                    addrow("CodeDontDisplayCondition") = addrow("CodeDontDisplayCondition") & aryDependencyPrdShortName(ctr) & ","
                    addrow("CodeDontDisplayConditionID") = addrow("CodeDontDisplayConditionID") & aryDependencyProductId(ctr) & ","

                    If Not addrow("CodeDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayCondition")) Then
                            addrow("CodeDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayConditionID")) Then
                            addrow("CodeDisplayConditionID") = ""
                        End If
                    End If
                    ''-------------
                    If Not addrow("CalculateCondition") Is Nothing Then
                        If IsDBNull(addrow("CalculateCondition")) Then
                            addrow("CalculateCondition") = ""
                        End If
                    End If

                    If Not addrow("CalculateConditionID") Is Nothing Then
                        If IsDBNull(addrow("CalculateConditionID")) Then
                            addrow("CalculateConditionID") = ""
                        End If
                    End If

                    ''rev:sept 4, 2009
                    If Not addrow("Bond") Is Nothing Then
                        If IsDBNull(addrow("Bond")) Then
                            addrow("Bond") = ""
                        End If
                    End If

                    If Not addrow("BondID") Is Nothing Then
                        If IsDBNull(addrow("BondID")) Then
                            addrow("BondID") = ""
                        End If
                    End If


                    If Not addrow("Imprint") Is Nothing Then
                        If IsDBNull(addrow("Imprint")) Then
                            addrow("Imprint") = ""
                        End If
                    End If

                    If Not addrow("ImprintID") Is Nothing Then
                        If IsDBNull(addrow("ImprintID")) Then
                            addrow("ImprintID") = ""
                        End If
                    End If

                Case "3" ''3rd dropdown on gridview

                    addrow("CalculateCondition") = addrow("CalculateCondition") & aryDependencyPrdShortName(ctr) & ","
                    addrow("CalculateConditionID") = addrow("CalculateConditionID") & aryDependencyProductId(ctr) & ","

                    If Not addrow("CodeDontDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayCondition")) Then
                            addrow("CodeDontDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDontDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayConditionID")) Then
                            addrow("CodeDontDisplayConditionID") = ""
                        End If
                    End If
                    ''-------------
                    If Not addrow("CodeDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayCondition")) Then
                            addrow("CodeDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayConditionID")) Then
                            addrow("CodeDisplayConditionID") = ""
                        End If
                    End If

                    If Not addrow("CalculateCondition") Is Nothing Then
                        If IsDBNull(addrow("CalculateCondition")) Then
                            addrow("CalculateCondition") = ""
                        End If
                    End If

                    If Not addrow("CalculateConditionID") Is Nothing Then
                        If IsDBNull(addrow("CalculateConditionID")) Then
                            addrow("CalculateConditionID") = ""
                        End If
                    End If

                    ''rev:sept 4, 2009
                    If Not addrow("Bond") Is Nothing Then
                        If IsDBNull(addrow("Bond")) Then
                            addrow("Bond") = ""
                        End If
                    End If

                    If Not addrow("BondID") Is Nothing Then
                        If IsDBNull(addrow("BondID")) Then
                            addrow("BondID") = ""
                        End If
                    End If


                    If Not addrow("Imprint") Is Nothing Then
                        If IsDBNull(addrow("Imprint")) Then
                            addrow("Imprint") = ""
                        End If
                    End If

                    If Not addrow("ImprintID") Is Nothing Then
                        If IsDBNull(addrow("ImprintID")) Then
                            addrow("ImprintID") = ""
                        End If
                    End If


                    ''rev:mia sept 4 ,2009
                Case 4 ''bond
                    addrow("Bond") = addrow("Bond") & aryDependencyPrdShortName(ctr) & ","
                    addrow("BondID") = addrow("BondID") & aryDependencyProductId(ctr) & ","

                    If Not addrow("CodeDontDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayCondition")) Then
                            addrow("CodeDontDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDontDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayConditionID")) Then
                            addrow("CodeDontDisplayConditionID") = ""
                        End If
                    End If

                    If Not addrow("CalculateCondition") Is Nothing Then
                        If IsDBNull(addrow("CalculateCondition")) Then
                            addrow("CalculateCondition") = ""
                        End If
                    End If

                    If Not addrow("CalculateConditionID") Is Nothing Then
                        If IsDBNull(addrow("CalculateConditionID")) Then
                            addrow("CalculateConditionID") = ""
                        End If
                    End If

                    If Not addrow("CodeDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayCondition")) Then
                            addrow("CodeDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayConditionID")) Then
                            addrow("CodeDisplayConditionID") = ""
                        End If
                    End If

                    If Not addrow("Imprint") Is Nothing Then
                        If IsDBNull(addrow("Imprint")) Then
                            addrow("Imprint") = ""
                        End If
                    End If

                    If Not addrow("ImprintID") Is Nothing Then
                        If IsDBNull(addrow("ImprintID")) Then
                            addrow("ImprintID") = ""
                        End If
                    End If

                Case 5 ''imprint

                    addrow("Imprint") = addrow("Imprint") & aryDependencyPrdShortName(ctr) & ","
                    addrow("ImprintID") = addrow("ImprintID") & aryDependencyProductId(ctr) & ","

                    If Not addrow("CodeDontDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayCondition")) Then
                            addrow("CodeDontDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDontDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDontDisplayConditionID")) Then
                            addrow("CodeDontDisplayConditionID") = ""
                        End If
                    End If

                    If Not addrow("CalculateCondition") Is Nothing Then
                        If IsDBNull(addrow("CalculateCondition")) Then
                            addrow("CalculateCondition") = ""
                        End If
                    End If

                    If Not addrow("CalculateConditionID") Is Nothing Then
                        If IsDBNull(addrow("CalculateConditionID")) Then
                            addrow("CalculateConditionID") = ""
                        End If
                    End If

                    If Not addrow("CodeDisplayCondition") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayCondition")) Then
                            addrow("CodeDisplayCondition") = ""
                        End If
                    End If

                    If Not addrow("CodeDisplayConditionID") Is Nothing Then
                        If IsDBNull(addrow("CodeDisplayConditionID")) Then
                            addrow("CodeDisplayConditionID") = ""
                        End If
                    End If

                    If Not addrow("Bond") Is Nothing Then
                        If IsDBNull(addrow("Bond")) Then
                            addrow("Bond") = ""
                        End If
                    End If

                    If Not addrow("BondID") Is Nothing Then
                        If IsDBNull(addrow("BondID")) Then
                            addrow("BondID") = ""
                        End If
                    End If
                Case Else
                    addrow("CalculateCondition") = ""
                    addrow("CalculateConditionID") = ""

                    addrow("CodeDontDisplayCondition") = ""
                    addrow("CodeDontDisplayConditionID") = ""

                    addrow("CodeDisplayCondition") = ""
                    addrow("CodeDisplayConditionID") = ""

            End Select
            ctr = ctr + 1
        Next

        addrow("OrderNumber") = HreItmDispOrder
        addrow("ParentDeletedBoolean") = "False"  ''delete if true. Default value is false
        addrow("ChildDeletedID") = "" ''this will hold all dependencies that will be deleted
        addrow("ChildDeletedAction") = "" ''this will hold the action id
        addrow("AddDateTime") = AddDateTime
        addrow("Rating") = Rating
        addrow("HreItmId") = HreItemID

        addrow("TargetSiteID") = GetTargetID(SiteId)
        addrow("TargetSite") = GetTargetLinkName(SiteId)

        tableInsurance.Rows.Add(addrow)
        tableInsuranceXML = StreamXMLDatatable()
    End Sub

    Public Sub DisplayInsurance(ByVal nodes As XmlNodeList, Optional ByVal resetTable As Boolean = False)


        Dim items As New List(Of Data.WebNonVehItems)

        Dim HreItmPrdId As String = ""
        Dim PrdShortName As String = ""
        Dim HreItmDispOrder As String = ""
        Dim DependencyProductId As String = ""
        Dim DependencyPrdShortName As String = ""
        Dim DependencyAction As String = ""
        Dim ActionId As String = ""
        Dim Adddatetime As DateTime = Nothing
        Dim HreItmId As String = ""
        Dim HreItmIdNew As String = ""
        Dim SiteIds As String = ""

        Dim Rating As String = ""
        RemoveButton.Enabled = IIf(nodes.Count > 0, True, False)
        If nodes IsNot Nothing Then
            For Each mynode As XmlNode In nodes
                If mynode.Attributes("ClaCode").Value.ToUpper = "INSURANCE" Or _
                   mynode.Attributes("ClaCode").Value.ToUpper = "INSURENCE" Then

                    ''HreItmId = "|" & mynode.Attributes("HreItmId").Value.ToUpper
                    HreItmId = "|" & System.Guid.NewGuid.ToString("N")
                    HreItmIdNew = mynode.Attributes("HreItmId").Value.ToUpper

                    HreItmPrdId = mynode.Attributes("HreItmPrdId").Value
                    HreItmDispOrder = mynode.Attributes("HreItmDispOrder").Value
                    PrdShortName = mynode.SelectSingleNode("Product").Attributes("PrdShortName").Value
                    Adddatetime = Convert.ToDateTime(mynode.Attributes("AddDateTime").Value)

                    ''rev:mia sept 4, 2009
                    If Not mynode.Attributes("Rating") Is Nothing Then
                        Rating = IIf(String.IsNullOrEmpty(mynode.Attributes("Rating").Value), "0", mynode.Attributes("Rating").Value)
                    Else
                        Rating = "0"
                    End If

                    ''REV:MIA MARCH 8 2011
                    If mynode.SelectSingleNode("Product").Attributes.Count = 2 Then
                        If Not mynode.SelectSingleNode("Product").Attributes("AvailableOnSite") Is Nothing Then
                            SiteIds = SiteIds & mynode.SelectSingleNode("Product").Attributes("AvailableOnSite").InnerText
                        End If
                    End If


                    If mynode.SelectSingleNode("Product").HasChildNodes Then

                        For Each productchild As XmlNode In mynode.SelectNodes("Product/Dependency")

                            If productchild.Attributes("ActionId") IsNot Nothing Then
                                ActionId = ActionId & productchild.Attributes("ActionId").Value & ","
                            End If

                            If productchild.Attributes("DependencyProductId") IsNot Nothing Then
                                DependencyProductId = DependencyProductId & productchild.Attributes("DependencyProductId").Value & ","
                            End If

                            If productchild.Attributes("PrdShortName") IsNot Nothing Then
                                DependencyPrdShortName = DependencyPrdShortName & productchild.Attributes("PrdShortName").Value & ","
                            End If

                            If productchild.Attributes("DependencyAction") IsNot Nothing Then
                                DependencyAction = DependencyAction & productchild.Attributes("DependencyAction").Value & ","
                            End If


                        Next

                    End If

                    If Not String.IsNullOrEmpty(DependencyProductId) Then
                        If DependencyProductId.LastIndexOf(",") <> -1 Then
                            DependencyProductId = DependencyProductId.Remove(DependencyProductId.Length - 1, 1)
                        End If
                    End If

                    If Not String.IsNullOrEmpty(DependencyPrdShortName) Then
                        If DependencyPrdShortName.LastIndexOf(",") <> -1 Then
                            DependencyPrdShortName = DependencyPrdShortName.Remove(DependencyPrdShortName.Length - 1, 1)
                        End If
                    End If

                    If Not String.IsNullOrEmpty(DependencyAction) Then
                        If DependencyAction.LastIndexOf(",") <> -1 Then
                            DependencyAction = DependencyAction.Remove(DependencyAction.Length - 1, 1)
                        End If
                    End If

                    If Not String.IsNullOrEmpty(ActionId) Then
                        If ActionId.LastIndexOf(",") <> -1 Then
                            ActionId = ActionId.Remove(ActionId.Length - 1, 1)
                        End If
                    End If

                    Dim webItems As New Data.WebNonVehItems(HreItmPrdId, _
                                                            PrdShortName, _
                                                            HreItmDispOrder, _
                                                            DependencyProductId, _
                                                            DependencyPrdShortName, _
                                                            DependencyAction, _
                                                            ActionId, _
                                                            Adddatetime, _
                                                            HreItmId, _
                                                            Rating, _
                                                            HreItmIdNew, _
                                                            SiteIds)


                    items.Add(webItems)

                    ''reset to empty
                    DependencyProductId = ""
                    DependencyPrdShortName = ""
                    DependencyAction = ""
                    ActionId = ""
                    Rating = ""
                    SiteIds = ""
                End If
            Next
        End If

        If items.Count - 1 <> -1 Then
            If resetTable Then
                tableInsurance = Nothing
                tableInsuranceXML = Nothing
            End If
            If Not CheckDataTableIfExist() Then CreateTableColumns()
            Dim i As Integer = 0
            For Each myitem As Data.WebNonVehItems In items

                Me.CurrentRecord(myitem.HreItmPrdId, _
                                myitem.PrdShortName, _
                                myitem.HreItmDispOrder, _
                                myitem.DependencyProductId, _
                                myitem.DependencyPrdShortName, _
                                myitem.DependencyAction, _
                                myitem.ActionId, _
                                myitem.AddDatetime, _
                                myitem.HreItmId, _
                                myitem.Rating, _
                                myitem.HreItmIdNew, _
                                myitem.SiteId)
            Next
            Me.BindGridView(tableInsurance)
        End If
    End Sub
#End Region

#Region "Load Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ResetXMLs()
            CreateTableColumns()
            RowIndexInsurance = ""
            ''SortInsurance = "OrderNumber ASC"
            Me.RemoveButton.Enabled = False
        End If
    End Sub
#End Region

#Region "Control Events"

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Me.InsuranceDropdown.Items.Count = 0 Then Return
        If Me.InsuranceDropdown.SelectedItem.Text = "(Select)" Then
            MyBase.CurrentPage.SetWarningShortMessage("This is not a valid Insurance product")
            Exit Sub
        End If
        If Not CheckDataTableIfExist() Then CreateTableColumns()
        BindGridView()
    End Sub

    Protected Sub RemoveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RemoveButton.Click
        BindGridView(RemoveRecord)
    End Sub

    Protected Sub InsuranceGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles InsuranceGridView.RowCreated
        Dim col As DataControlField = Nothing
        Dim img As HtmlImage = Nothing
        If e.Row.RowType = DataControlRowType.Header Then
            For i As Integer = 0 To Me.InsuranceGridView.Columns.Count - 1
                col = Me.InsuranceGridView.Columns(i)

                img = New HtmlImage
                If col.HeaderText.Contains("Order") Then
                    If SortInsurance.Contains("ASC") Then
                        img.Src = "~/images/sort_ascending.gif"
                        ''e.Row.Cells(i).Controls.Add(img)
                    Else
                        img.Src = "~/images/sort_descending.gif"
                        ''e.Row.Cells(i).Controls.Add(img)
                    End If
                    e.Row.Font.Size = FontSize.Small
                    col.HeaderText = "Order"
                End If
            Next
        End If

    End Sub

    Protected Sub InsuranceGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles InsuranceGridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            ''RowIndex
            Dim productcodeCheckbox As CheckBox = CType(e.Row.FindControl("productcodeCheckbox"), CheckBox)
            If productcodeCheckbox IsNot Nothing Then
                If Not String.IsNullOrEmpty(RowIndexInsurance) Then
                    Dim RowIndexInsuranceAry As String() = RowIndexInsurance.Split(",")
                    For i As Integer = 0 To RowIndexInsuranceAry.Length - 1
                        If Not String.IsNullOrEmpty(RowIndexInsuranceAry(i)) Then
                            If e.Row.RowIndex = RowIndexInsuranceAry(i) Then
                                productcodeCheckbox.Checked = True
                            End If
                        End If
                    Next
                End If
            End If

            Dim displayConditionCheckboxlist As CheckBoxList = CType(e.Row.FindControl("displayConditionCheckboxlist"), CheckBoxList)
            Dim dontDisplayConditionCheckboxlist As CheckBoxList = CType(e.Row.FindControl("dontDisplayConditionCheckboxlist"), CheckBoxList)
            Dim CalculateDisplayConditionCheckboxlist As CheckBoxList = CType(e.Row.FindControl("CalculateDisplayConditionCheckboxlist"), CheckBoxList)

            Dim textvalue As String = ""
            Dim textID As String = ""
            Dim textIDAry As String() = Nothing
            Dim ctr As Integer = 0
            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            If displayConditionCheckboxlist IsNot Nothing Then
                textvalue = rowview("CodeDisplayCondition").ToString
                If Not String.IsNullOrEmpty(textvalue) Then
                    If textvalue.LastIndexOf(",") = textvalue.Length - 1 Then
                        textvalue = textvalue.Remove(textvalue.Length - 1, 1)
                    End If
                    Dim arytextdisplayCondition() As String = textvalue.Split(",")
                    displayConditionCheckboxlist.DataSource = arytextdisplayCondition
                    displayConditionCheckboxlist.DataBind()

                    textID = rowview("CodeDisplayConditionID").ToString.TrimEnd
                    textIDAry = textID.Split(",")
                    If textID.LastIndexOf(",") = textID.Length - 1 Then
                        textID = textID.Remove(textID.Length - 1, 1)
                    End If

                    For Each chkItem As ListItem In displayConditionCheckboxlist.Items
                        If ctr <= textIDAry.Length - 1 Then
                            chkItem.Value = textIDAry(ctr)
                            ctr = ctr + 1
                        End If
                    Next

                End If
            End If


            If dontDisplayConditionCheckboxlist IsNot Nothing Then
                textvalue = rowview("CodeDontDisplayCondition").ToString
                If Not String.IsNullOrEmpty(textvalue) Then
                    If textvalue.LastIndexOf(",") = textvalue.Length - 1 Then
                        textvalue = textvalue.Remove(textvalue.Length - 1, 1)
                    End If

                    Dim arytextdisplayCondition() As String = textvalue.Split(",")
                    dontDisplayConditionCheckboxlist.DataSource = arytextdisplayCondition
                    dontDisplayConditionCheckboxlist.DataBind()

                    textID = rowview("CodeDontDisplayConditionID").ToString
                    textIDAry = textID.Split(",")
                    If textID.LastIndexOf(",") = textID.Length - 1 Then
                        textID = textID.Remove(textID.Length - 1, 1)
                    End If

                    ctr = 0
                    For Each chkItem As ListItem In dontDisplayConditionCheckboxlist.Items
                        If ctr <= textIDAry.Length - 1 Then
                            chkItem.Value = textIDAry(ctr)
                            ctr = ctr + 1
                        End If
                    Next

                End If

            End If

            If CalculateDisplayConditionCheckboxlist IsNot Nothing Then
                textvalue = rowview("CalculateCondition").ToString
                If Not String.IsNullOrEmpty(textvalue) Then
                    If textvalue.LastIndexOf(",") = textvalue.Length - 1 Then
                        textvalue = textvalue.Remove(textvalue.Length - 1, 1)
                    End If

                    Dim arytextdisplayCondition() As String = textvalue.Split(",")
                    CalculateDisplayConditionCheckboxlist.DataSource = arytextdisplayCondition
                    CalculateDisplayConditionCheckboxlist.DataBind()

                    textID = rowview("CalculateConditionID").ToString
                    textIDAry = textID.Split(",")
                    If textID.LastIndexOf(",") = textID.Length - 1 Then
                        textID = textID.Remove(textID.Length - 1, 1)
                    End If

                    ctr = 0
                    For Each chkItem As ListItem In CalculateDisplayConditionCheckboxlist.Items
                        If ctr <= textIDAry.Length - 1 Then
                            chkItem.Value = textIDAry(ctr)
                            ctr = ctr + 1
                        End If
                    Next

                End If
            End If

            ''rev:mia sept 4 2009
            Dim BondCheckboxlist As CheckBoxList = CType(e.Row.FindControl("BondCheckboxlist"), CheckBoxList)
            Dim ImprintCheckboxlist As CheckBoxList = CType(e.Row.FindControl("ImprintCheckboxlist"), CheckBoxList)

            If BondCheckboxlist IsNot Nothing Then
                textvalue = rowview("Bond").ToString
                If Not String.IsNullOrEmpty(textvalue) Then
                    If textvalue.LastIndexOf(",") = textvalue.Length - 1 Then
                        textvalue = textvalue.Remove(textvalue.Length - 1, 1)
                    End If

                    Dim arytextdisplayCondition() As String = textvalue.Split(",")
                    BondCheckboxlist.DataSource = arytextdisplayCondition
                    BondCheckboxlist.DataBind()

                    textID = rowview("BondID").ToString
                    textIDAry = textID.Split(",")
                    If textID.LastIndexOf(",") = textID.Length - 1 Then
                        textID = textID.Remove(textID.Length - 1, 1)
                    End If

                    ctr = 0
                    For Each chkItem As ListItem In BondCheckboxlist.Items
                        If ctr <= textIDAry.Length - 1 Then
                            chkItem.Value = textIDAry(ctr)
                            ctr = ctr + 1
                        End If
                    Next
                End If
            End If

            If ImprintCheckboxlist IsNot Nothing Then
                textvalue = rowview("Imprint").ToString
                If Not String.IsNullOrEmpty(textvalue) Then
                    If textvalue.LastIndexOf(",") = textvalue.Length - 1 Then
                        textvalue = textvalue.Remove(textvalue.Length - 1, 1)
                    End If

                    Dim arytextdisplayCondition() As String = textvalue.Split(",")
                    ImprintCheckboxlist.DataSource = arytextdisplayCondition
                    ImprintCheckboxlist.DataBind()

                    textID = rowview("ImprintID").ToString
                    textIDAry = textID.Split(",")
                    If textID.LastIndexOf(",") = textID.Length - 1 Then
                        textID = textID.Remove(textID.Length - 1, 1)
                    End If

                    ctr = 0
                    For Each chkItem As ListItem In ImprintCheckboxlist.Items
                        If ctr <= textIDAry.Length - 1 Then
                            chkItem.Value = textIDAry(ctr)
                            ctr = ctr + 1
                        End If
                    Next
                End If
            End If

            ''rev:mia MARCH 8 2011
            Dim targetSiteCheckboxlist As CheckBoxList = CType(e.Row.FindControl("targetSiteCheckboxlist"), CheckBoxList)
            If Not targetSiteCheckboxlist Is Nothing Then
                textvalue = rowview("TargetSite").ToString
                If Not String.IsNullOrEmpty(textvalue) Then
                    If textvalue.LastIndexOf(",") = textvalue.Length - 1 Then
                        textvalue = textvalue.Remove(textvalue.Length - 1, 1)
                    End If

                    Dim arytextdisplayCondition() As String = textvalue.Split(",")
                    targetSiteCheckboxlist.DataSource = arytextdisplayCondition
                    targetSiteCheckboxlist.DataBind()

                    textID = rowview("TargetSiteID").ToString
                    textIDAry = textID.Split(",")
                    If textID.LastIndexOf(",") = textID.Length - 1 Then
                        textID = textID.Remove(textID.Length - 1, 1)
                    End If

                    ctr = 0
                    For Each chkItem As ListItem In targetSiteCheckboxlist.Items
                        If ctr <= textIDAry.Length - 1 Then
                            chkItem.Value = textIDAry(ctr)
                            ctr = ctr + 1
                        End If
                    Next
                End If
            End If
            
        Else
            If e.Row.RowType = DataControlRowType.Header Then
                Dim displayConditiondropdown As DropDownList = CType(e.Row.FindControl("displayConditiondropdown"), DropDownList)
                Dim dontDisplayConditionDropdown As DropDownList = CType(e.Row.FindControl("dontDisplayConditionDropdown"), DropDownList)
                Dim CalculateDisplayConditionDropdown As DropDownList = CType(e.Row.FindControl("CalculateDisplayConditionDropdown"), DropDownList)

                Dim view As DataView = Data.DataRepository.Get_WebProductList("OTHERS", Country, Brand, VehicleType).Tables(0).DefaultView
                view.Sort = "PrdShortName asc"
                If displayConditiondropdown IsNot Nothing Then
                    displayConditiondropdown.Items.Add("(Select)")
                    displayConditiondropdown.AppendDataBoundItems = True
                    displayConditiondropdown.DataTextField = "PrdShortName"
                    displayConditiondropdown.DataValueField = "PrdId"
                    displayConditiondropdown.DataSource = view
                    displayConditiondropdown.DataBind()
                End If

                If dontDisplayConditionDropdown IsNot Nothing Then
                    dontDisplayConditionDropdown.Items.Add("(Select)")
                    dontDisplayConditionDropdown.AppendDataBoundItems = True
                    dontDisplayConditionDropdown.DataTextField = "PrdShortName"
                    dontDisplayConditionDropdown.DataValueField = "PrdId"
                    dontDisplayConditionDropdown.DataSource = view
                    dontDisplayConditionDropdown.DataBind()
                End If

                If CalculateDisplayConditionDropdown IsNot Nothing Then
                    CalculateDisplayConditionDropdown.Items.Add("(Select)")
                    CalculateDisplayConditionDropdown.AppendDataBoundItems = True
                    CalculateDisplayConditionDropdown.DataTextField = "PrdShortName"
                    CalculateDisplayConditionDropdown.DataValueField = "PrdId"
                    CalculateDisplayConditionDropdown.DataSource = view
                    CalculateDisplayConditionDropdown.DataBind()
                End If

                ''rev:mia Sept 2 , 2009
                Dim imprintDropdown As DropDownList = CType(e.Row.FindControl("imprintDropdown"), DropDownList)
                Dim bondDropdown As DropDownList = CType(e.Row.FindControl("bondDropdown"), DropDownList)
                view.RowFilter = "PrdIsVehicle = 1"
                If imprintDropdown IsNot Nothing Then
                    imprintDropdown.Items.Add("(Select)")
                    imprintDropdown.Items.Add("(All)")
                    imprintDropdown.AppendDataBoundItems = True
                    imprintDropdown.DataTextField = "PrdShortName"
                    imprintDropdown.DataValueField = "PrdId"
                    imprintDropdown.DataSource = view
                    imprintDropdown.DataBind()
                End If

                If bondDropdown IsNot Nothing Then
                    bondDropdown.Items.Add("(Select)")
                    bondDropdown.Items.Add("(All)")
                    bondDropdown.AppendDataBoundItems = True
                    bondDropdown.DataTextField = "PrdShortName"
                    bondDropdown.DataValueField = "PrdId"
                    bondDropdown.DataSource = view
                    bondDropdown.DataBind()
                End If

                ''rev:mia MARCH 8 2011
                Try
                    view = Data.DataRepository.Get_WebProductListWithTarget(Country, Brand, VehicleType).Tables(0).DefaultView
                Catch ex As Exception

                End Try

                Dim TargetSiteDropdown As DropDownList = CType(e.Row.FindControl("TargetSiteDropdown"), DropDownList)
                If Not TargetSiteDropdown Is Nothing Then
                    TargetSiteDropdown.Items.Add("(Select)")
                    TargetSiteDropdown.Items.Add("(All)")
                    TargetSiteDropdown.AppendDataBoundItems = True
                    TargetSiteDropdown.DataTextField = "WebTargetSiteName"
                    TargetSiteDropdown.DataValueField = "WebTargetSiteID"
                    TargetSiteDropdown.DataSource = view
                    TargetSiteDropdown.DataBind()
                End If
            End If
        End If
    End Sub

    ''' <summary>
    ''' this is a dropdown inside the grid
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub dropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindGridView(EditRecord(sender))
    End Sub

    ''' <summary>
    ''' this is a textbox inside the grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub textbox_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindGridView(EditRecord(Nothing, sender))
    End Sub

    Protected Sub InsuranceGridView_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles InsuranceGridView.Sorting
        If e.SortExpression = "OrderNumber" Then
            If SortInsurance.Contains("ASC") Then
                SortInsurance = "OrderNumber DESC"
            Else
                If String.IsNullOrEmpty(SortInsurance) Then
                    SortInsurance = "OrderNumber ASC"
                Else
                    SortInsurance = "OrderNumber ASC"
                End If
            End If
        End If
        BindGridView(SortInsurance)
    End Sub

#End Region

#Region "Additions of Bond and Imprint to the insurance"
    '' lets try to create new functions so previous code will not break
    Sub BondImprintAndRating(ByRef table As DataTable)
        Dim dcBondID As New DataColumn("BondID", Type.GetType("System.String"))
        Dim dcImprintID As New DataColumn("ImprintID", Type.GetType("System.String"))
        Dim dcRating As New DataColumn("Rating", Type.GetType("System.String"))


        Dim dcBond As New DataColumn("Bond", Type.GetType("System.String"))
        Dim dcImprint As New DataColumn("Imprint", Type.GetType("System.String"))


        table.Columns.Add(dcBond)
        table.Columns.Add(dcImprint)
        table.Columns.Add(dcBondID)
        table.Columns.Add(dcImprintID)
        table.Columns.Add(dcRating)

        ''REV:JUNE 28 2010
        Dim dcHreItmId As New DataColumn("HreItmId", Type.GetType("System.String"))
        table.Columns.Add(dcHreItmId)
    End Sub

    Sub NewRecordBondImprintAndRating(ByRef row As DataRow)
        row("Bond") = ""
        row("Imprint") = ""
        row("BondID") = ""
        row("ImprintID") = ""
        row("Rating") = "0"
        row("HreItmId") = "0"

    End Sub

    Private Function CheckImprintAllIsActive() As Boolean
        CheckImprintAllIsActive = False
        For Each row As GridViewRow In Me.InsuranceGridView.Rows
            Dim key As String = Me.InsuranceGridView.DataKeys(row.RowIndex).Value
            Dim editrow As DataRow = tableInsurance.Rows.Find(key)
            If editrow("Imprint").ToString.Contains("All") Then Return True
        Next
        Return CheckImprintAllIsActive
    End Function

    Private Function CheckBondAllIsActive() As Boolean
        CheckBondAllIsActive = False
        For Each row As GridViewRow In Me.InsuranceGridView.Rows
            Dim key As String = Me.InsuranceGridView.DataKeys(row.RowIndex).Value
            Dim editrow As DataRow = tableInsurance.Rows.Find(key)
            If editrow("Bond").ToString.Contains("All") Then Return True
        Next
        Return CheckBondAllIsActive
    End Function
#End Region

#Region "TargetSite"
    Sub TargetSite(ByRef table As DataTable)
        Dim dcTargetSiteID As New DataColumn("TargetSiteID", Type.GetType("System.String"))
        Dim dcTargetSite As New DataColumn("TargetSite", Type.GetType("System.String"))


        table.Columns.Add(dcTargetSiteID)
        table.Columns.Add(dcTargetSite)
    End Sub

    Sub NewRecordTargetSite(ByRef row As DataRow)
        row("TargetSiteID") = ""
        row("TargetSite") = ""
    End Sub

    Function GetTargetID(ByVal sites As String) As String
        Dim commasplit As String() = sites.Split(",")

        Dim ids As String = ""
        For Each commas As String In commasplit
            If commas.Contains("-") = True Then
                ids = ids & commas.Substring(0, commas.IndexOf("-")) & ","
            End If
        Next
        If ids.EndsWith(",") = True Then
            ids = ids.Remove(ids.LastIndexOf(","))
        End If
        Return ids
    End Function

    Function GetTargetLinkName(ByVal sites As String) As String
        Dim commasplit As String() = sites.Split(",")

        Dim ids As String = ""
        For Each commas As String In commasplit
            If commas.Contains("-") = True Then
                ids = ids & commas.Substring(commas.IndexOf("-") + 1) & ","
            End If
        Next
        If ids.EndsWith(",") = True Then
            ids = ids.Remove(ids.LastIndexOf(","))
        End If
        Return ids
    End Function
#End Region

#Region "rev:mia 20-Oct-2016 https://thlonline.atlassian.net/browse/AURORA-1106"
    Public Sub EnableRemoveButton(flag As Boolean)
        RemoveButton.Enabled = flag
    End Sub
#End Region
End Class
