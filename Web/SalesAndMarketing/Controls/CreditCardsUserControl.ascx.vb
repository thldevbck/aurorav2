Imports System.io
Imports System.Xml
Imports System.data
Imports aurora.SalesAndMarketing
Imports System.Collections.Generic

Partial Class SalesAndMarketing_Controls_CreditCardsUserControl
    Inherits AuroraUserControl

#Region "constant"
    Const NEW_FLAG As String = "<font color='red'><sup><b>&nbsp;NEW</b></sup></font>"
    Const DUPLICATE_WHEN_PRODUCT As String = " is already assigned to this "
    Const DUPLICATE_ROW As String = "This XXX has already been assigned to this group. "
#End Region

#Region "variables"
    
    Private creditCardTable As System.Data.DataTable

#End Region

#Region "public properties"

    Public Property Country() As String
        Get
            Return CStr(ViewState("CountryParam"))
        End Get
        Set(ByVal value As String)
            ViewState("CountryParam") = value
        End Set
    End Property

    Public Property Brand() As String
        Get
            Return CStr(ViewState("BrandParam"))
        End Get
        Set(ByVal value As String)
            ViewState("BrandParam") = value
        End Set
    End Property

    Public Property VehicleType() As String
        Get
            Return CStr(ViewState("VehicleTypeParam"))
        End Get
        Set(ByVal value As String)
            ViewState("VehicleTypeParam") = value
        End Set
    End Property

    Public ReadOnly Property CardTypeTable() As DataTable
        Get
            If Not CheckDataTableIfExist() Then CreateTableColumns()
            XMLtoDatatable()
            Return creditCardTable
        End Get
    End Property

#End Region

#Region "private properties"

    Private Property tableCreditCardXML() As String
        Get
            Return CStr(ViewState("tableCreditCardXML"))
        End Get
        Set(ByVal value As String)
            ViewState("tableCreditCardXML") = value
        End Set
    End Property

#End Region

#Region "public procedures"

    Public Sub UnBind()
        Me.CreditCardDropdown.Items.Clear()
        Me.CreditCardGridView.DataSource = Nothing
        Me.CreditCardGridView.DataBind()
    End Sub

    Public Sub BindDropDown()
        Dim view As DataView = Data.DataRepository.Get_WebProductList("CARD", Country, Brand, VehicleType).Tables(0).DefaultView
        view.Sort = "PtmName Asc"
        Me.CreditCardDropdown.DataSource = Nothing
        Me.CreditCardDropdown.DataBind()

        Me.CreditCardDropdown.Items.Add("(Select)")
        Me.CreditCardDropdown.AppendDataBoundItems = True
        Me.CreditCardDropdown.DataSource = view
        Me.CreditCardDropdown.DataValueField = "PtmId"
        Me.CreditCardDropdown.DataTextField = "PtmName"
        Me.CreditCardDropdown.DataBind()
    End Sub

    Public Sub ResetXMLs()
        tableCreditCardXML = ""
    End Sub

#End Region

#Region "private procedures"

    Sub BindGridView()
        Dim filteredView As DataView = NewRecord.DefaultView
        '' filteredView.Sort = "PtmId Asc"
        filteredView.RowFilter = "Displayed = 'True'"
        Me.CreditCardGridView.DataSource = filteredView
        Me.CreditCardGridView.DataBind()
        RemoveButton.Enabled = IIf(filteredView.Count > 0, True, False)
    End Sub

    Sub BindGridView(ByVal tablename As DataTable)
        Dim filteredView As DataView = tablename.DefaultView
        filteredView.Sort = "AddDateTime ASC"
        filteredView.RowFilter = "Displayed = 'True'"
        Me.CreditCardGridView.DataSource = filteredView
        Me.CreditCardGridView.DataBind()
        RemoveButton.Enabled = IIf(filteredView.Count > 0, True, False)
    End Sub

    Sub CreateTableColumns()
        If creditCardTable Is Nothing Then creditCardTable = New DataTable("CreditCard")
        If creditCardTable.Columns.Count = 6 Then Exit Sub

        Dim dcProductID As New DataColumn("PtmId", System.Type.GetType("System.String"))
        creditCardTable.Columns.Add(dcProductID)

        Dim dcProductcode As New DataColumn("PtmName", System.Type.GetType("System.String"))
        creditCardTable.Columns.Add(dcProductcode)

        Dim dcOrderNumber As New DataColumn("OrderNumber", System.Type.GetType("System.String"))
        creditCardTable.Columns.Add(dcOrderNumber)

        Dim dcDeletedCard As New DataColumn("DeletedCard", System.Type.GetType("System.String"))
        creditCardTable.Columns.Add(dcDeletedCard)

        Dim dcDisplayed As New DataColumn("Displayed", System.Type.GetType("System.String"))
        creditCardTable.Columns.Add(dcDisplayed)

        ''add datetime for sorting
        Dim dcAddDatetime As New DataColumn("AddDatetime", System.Type.GetType("System.DateTime"))
        creditCardTable.Columns.Add(dcAddDatetime)


        Dim key(1) As System.Data.DataColumn
        key(0) = creditCardTable.Columns("PtmId")
        creditCardTable.PrimaryKey = key
    End Sub

    Sub XMLtoDatatable()
        If Not String.IsNullOrEmpty(tableCreditCardXML) Then
            Dim xmlReader As New XmlTextReader(tableCreditCardXML, System.Xml.XmlNodeType.Document, Nothing)
            xmlReader.ReadOuterXml()
            Dim ds As New DataSet
            ds.ReadXml(xmlReader)
            creditCardTable = ds.Tables(0)
            Dim key(1) As System.Data.DataColumn
            key(0) = creditCardTable.Columns("PtmId")
            creditCardTable.PrimaryKey = key
        End If
    End Sub

    Sub RemoveItemsInCheckBoxList(ByRef row As DataRow, ByVal COLUMN_NAME As String, ByVal itemToRemove As String)
        If row(COLUMN_NAME) IsNot Nothing Then
            Dim textrow As String = row(COLUMN_NAME).ToString
            If textrow.Contains(itemToRemove) Then
                textrow = textrow.Replace(itemToRemove, "")
                Dim ary() As String = textrow.Split(",")
                textrow = ""
                For Each item As String In ary
                    If Not String.IsNullOrEmpty(item) Then
                        textrow = textrow & item & ","
                    End If
                Next
                If textrow.EndsWith(",") Then
                    textrow = textrow.Remove(textrow.Length - 1, 1)
                End If
                row(COLUMN_NAME) = textrow
            End If
        End If

    End Sub

#End Region

#Region "private functions"

    Function StreamXMLDatatable() As String
        If creditCardTable.Rows.Count - 1 = -1 Then Return ""
        Dim stream As New MemoryStream
        creditCardTable.WriteXml(stream, XmlWriteMode.WriteSchema)
        stream.Position = 0
        Dim ds As New DataSet
        ds.ReadXml(stream)
        Return ds.GetXml
    End Function

    Function CheckDataTableIfExist() As Boolean
        Return IIf(creditCardTable Is Nothing, False, True)
    End Function

    Function IsDuplicateRecord() As Boolean
        Dim duplicaterow As DataRow = creditCardTable.Rows.Find(Me.CreditCardDropdown.SelectedValue)
        If duplicaterow IsNot Nothing Then
            Return True
        Else
            Return False
        End If
    End Function

    Function NewRecord() As DataTable
        If (Me.CreditCardGridView.Rows.Count = 0) Then tableCreditCardXML = ""
        XMLtoDatatable()
        Dim addrow As DataRow = creditCardTable.NewRow()
        addrow("PtmId") = Me.CreditCardDropdown.SelectedValue
        addrow("PtmName") = Me.CreditCardDropdown.SelectedItem.Text & NEW_FLAG
        addrow("OrderNumber") = ""
        addrow("DeletedCard") = ""
        addrow("Displayed") = "True"
        addrow("AddDateTime") = Now.ToString

        If IsDuplicateRecord() Then
            Dim tempMsg As String = DUPLICATE_ROW
            tempMsg = tempMsg.Replace("XXX", " '" & Me.CreditCardDropdown.SelectedItem.Text & "' ")
            MyBase.CurrentPage.SetWarningShortMessage(tempMsg)
        Else
            creditCardTable.Rows.Add(addrow)
        End If
        tableCreditCardXML = StreamXMLDatatable()

        Return creditCardTable
    End Function

    Function RemoveRecord() As DataTable
        XMLtoDatatable()
        For Each row As GridViewRow In Me.CreditCardGridView.Rows
            Dim creditcardCheckbox As CheckBox = CType(row.FindControl("creditcardCheckbox"), CheckBox)
            Dim key As String = Me.CreditCardGridView.DataKeys(row.RowIndex).Value
            If creditcardCheckbox IsNot Nothing Then
                If creditcardCheckbox.Checked Then
                    Dim removeRow As DataRow = creditCardTable.Rows.Find(key)
                    If removeRow IsNot Nothing Then
                        '' creditCardTable.Rows.Remove(removeRow)
                        removeRow("Displayed") = "False"
                    End If
                End If
            End If
        Next
        tableCreditCardXML = StreamXMLDatatable()
        Return creditCardTable
    End Function

    Function EditRecord(ByVal dropdown As DropDownList, Optional ByVal textBox As TextBox = Nothing) As DataTable
        XMLtoDatatable()
        For Each row As GridViewRow In Me.CreditCardGridView.Rows
            Dim creditcardCheckbox As CheckBox = CType(row.FindControl("creditcardCheckbox"), CheckBox)
            Dim key As String = Me.CreditCardGridView.DataKeys(row.RowIndex).Value
            Dim editrow As DataRow = creditCardTable.Rows.Find(key)
            If creditcardCheckbox IsNot Nothing Then
                If editrow IsNot Nothing Then
                    Dim ordernumbertextbox As TextBox = CType(row.FindControl("ordernumbertextbox"), TextBox)
                    editrow("OrderNumber") = ordernumbertextbox.Text
                End If
            End If
        Next
        tableCreditCardXML = StreamXMLDatatable()
        Return creditCardTable
    End Function

#End Region

#Region "Initialize Routine"

    Sub CurrentRecord(ByVal WbCrdTypSPtmId As String, ByVal PmtName As String, ByVal AddDateTime As DateTime)
        XMLtoDatatable()
        Dim addrow As DataRow = creditCardTable.NewRow()
        addrow("PtmId") = WbCrdTypSPtmId
        addrow("PtmName") = PmtName
        addrow("OrderNumber") = ""
        addrow("DeletedCard") = ""
        addrow("Displayed") = "True"
        addrow("AddDatetime") = AddDateTime

        creditCardTable.Rows.Add(addrow)
        tableCreditCardXML = StreamXMLDatatable()
    End Sub

    Public Sub DisplayCreditCards(ByVal nodes As XmlNodeList, Optional ByVal resetTable As Boolean = False)
        Dim items As New List(Of Data.WebCreditCardItems)

        Dim WbCrdTypSPtmId As String = ""
        Dim PmtName As String = ""
        RemoveButton.Enabled = IIf(nodes.Count > 0, True, False)
        Dim AddDateTime As DateTime = Nothing
        For Each carditem As XmlNode In nodes
            If carditem.Attributes("WbCrdTypSPtmId") IsNot Nothing Then
                WbCrdTypSPtmId = carditem.Attributes("WbCrdTypSPtmId").Value.ToString
            End If

            If carditem.Attributes("PmtName") IsNot Nothing Then
                PmtName = carditem.Attributes("PmtName").Value.ToString
            End If

            If carditem.Attributes("AddDateTime") IsNot Nothing Then
                AddDateTime = carditem.Attributes("AddDateTime").Value.ToString
            End If


            Dim creditcards As New Data.WebCreditCardItems(WbCrdTypSPtmId, PmtName, AddDateTime)
            items.Add(creditcards)
        Next

        If items.Count - 1 <> -1 Then
            If resetTable Then
                creditCardTable = Nothing
                tableCreditCardXML = Nothing
            End If
            If Not CheckDataTableIfExist() Then CreateTableColumns()
            For Each myitem As Data.WebCreditCardItems In items
                Me.CurrentRecord(myitem.WbCrdTypSPtmID, _
                                myitem.PmtName, _
                                myitem.AddDateTime)
            Next
            Me.BindGridView(creditCardTable)
        End If
    End Sub
#End Region

#Region "Load Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ResetXMLs()
            CreateTableColumns()
            Me.RemoveButton.Enabled = False
        End If
    End Sub
#End Region

#Region "Control Events"

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Me.CreditCardDropdown.Items.Count = 0 Then Return
        If Me.CreditCardDropdown.SelectedItem.Text = "(Select)" Then
            MyBase.CurrentPage.SetWarningShortMessage("This is not a valid Credit Card")
            Exit Sub
        End If
        If Not CheckDataTableIfExist() Then CreateTableColumns()
        BindGridView()
    End Sub

    Protected Sub RemoveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RemoveButton.Click
        BindGridView(RemoveRecord)
    End Sub

    Protected Sub InsuranceGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles CreditCardGridView.RowDataBound

        If e.Row.RowType = DataControlRowType.Header Then
            Dim displayConditiondropdown As DropDownList = CType(e.Row.FindControl("displayConditiondropdown"), DropDownList)
            Dim dontDisplayConditionDropdown As DropDownList = CType(e.Row.FindControl("dontDisplayConditionDropdown"), DropDownList)
            Dim CalculateDisplayConditionDropdown As DropDownList = CType(e.Row.FindControl("CalculateDisplayConditionDropdown"), DropDownList)

            Dim view As DataView = Data.DataRepository.Get_WebProductList("OTHERS", Country, Brand, VehicleType).Tables(0).DefaultView
            view.Sort = "PrdShortName asc"
            If displayConditiondropdown IsNot Nothing Then
                displayConditiondropdown.Items.Add("(Select)")
                displayConditiondropdown.AppendDataBoundItems = True
                displayConditiondropdown.DataTextField = "PrdShortName"
                displayConditiondropdown.DataValueField = "PrdId"
                displayConditiondropdown.DataSource = view
                displayConditiondropdown.DataBind()
            End If

            If dontDisplayConditionDropdown IsNot Nothing Then
                dontDisplayConditionDropdown.Items.Add("(Select)")
                dontDisplayConditionDropdown.AppendDataBoundItems = True
                dontDisplayConditionDropdown.DataTextField = "PrdShortName"
                dontDisplayConditionDropdown.DataValueField = "PrdId"
                dontDisplayConditionDropdown.DataSource = view
                dontDisplayConditionDropdown.DataBind()
            End If

            If CalculateDisplayConditionDropdown IsNot Nothing Then
                CalculateDisplayConditionDropdown.Items.Add("(Select)")
                CalculateDisplayConditionDropdown.AppendDataBoundItems = True
                CalculateDisplayConditionDropdown.DataTextField = "PrdShortName"
                CalculateDisplayConditionDropdown.DataValueField = "PrdId"
                CalculateDisplayConditionDropdown.DataSource = view
                CalculateDisplayConditionDropdown.DataBind()
            End If
        End If
    End Sub

    ''' <summary>
    ''' this is a dropdown inside the grid
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub dropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindGridView(EditRecord(sender))
    End Sub

    ''' <summary>
    ''' this is a textbox inside the grid
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub textbox_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        BindGridView(EditRecord(Nothing, sender))
    End Sub
#End Region

#Region "rev:mia 20-Oct-2016 https://thlonline.atlassian.net/browse/AURORA-1106"
    Public Sub EnableRemoveButton(flag As Boolean)
        RemoveButton.Enabled = flag
    End Sub
#End Region
End Class
