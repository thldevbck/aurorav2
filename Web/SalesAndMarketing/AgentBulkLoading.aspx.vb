'' Change log!
'' 17.6.8 - Shoel : Changed the way the page behaves when we enter it thru menu items Pkg/Prd
''  ID4   - Shoel : date format thingie added !
'' 3.10.8 - Shoel : Fix for squish # 625
'' 30.10.8- Shoel : Fix for no records found error message messing up show avlbl products click + fix for function code bug
'' 3.5.10 - Shoel : Setting Ads Effective date , fix for Help Desk Call 11267

''rev:mia DEc.24 2012 Call : 37671 - Change request - Section to search for agents to exclude from the bulk allocation in Aurora
''rev:mia jan 07 2013 Call : 37671 - Change request - Section to search for agents to exclude from the bulk allocation in Aurora

Imports System.Xml
Imports System.Data
Imports Aurora.Common.Utility
Imports Aurora.Common

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.AgentBulkProductMaintainence)> _
Partial Class Agent_AgentBulkLoading
    Inherits AuroraPage

    Public Const VIEWSTATE_SELECTED_AGENT_DETAILS As String = "ViewState_AgnPrdBlkLod_SelectedAgentDetails"
    Public Const VIEWSTATE_SEARCH_RESULTS As String = "ViewState_AgnPrdBlkLod_SearchResults"
    Public Const AGENT_BULK_LOADING As String = "AgentBulkLoading.aspx"

    Public ReadOnly Property IsPackageBulkLoading() As Boolean
        Get
            Return CStr(Request("Option")) = "PKG"
        End Get
    End Property

    Public ReadOnly Property IsProductBulkLoading() As Boolean
        Get
            Return Not IsPackageBulkLoading
        End Get
    End Property

    Public Overrides ReadOnly Property FunctionCode() As String
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("AgnId")) _
             AndAlso Not String.IsNullOrEmpty(Request.QueryString("Text")) Then
                Return AuroraFunctionCodeAttribute.AgentEnquiry
            Else
                If IsPackageBulkLoading() Then
                    Return AuroraFunctionCodeAttribute.AgentBulkPackageMaintainence
                Else
                    Return AuroraFunctionCodeAttribute.AgentBulkProductMaintainence
                End If
            End If
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If Not String.IsNullOrEmpty(Request.QueryString("AgnId")) _
             AndAlso Not String.IsNullOrEmpty(Request.QueryString("Text")) Then
                If IsPackageBulkLoading() Then
                    Return "Add Agent Packages"
                Else
                    Return "Add Agent Product Discounts"
                End If
            Else
                If IsPackageBulkLoading() Then
                    Return "Agent Package Bulk Loading"
                Else
                    Return "Agent Product Bulk Loading"
                End If
            End If
        End Get
    End Property

    Sub BuildBulkLoading()
        LoadCountries()
        LoadMarketCode()
        ''LoadPackages()

        CollapseAllDivs()
        LoadAgentTypes()
        LoadCategories()
        LoadClassCombo()
        LoadBrandCombo()
        ViewState(VIEWSTATE_SEARCH_RESULTS) = Nothing
        ViewState(VIEWSTATE_SELECTED_AGENT_DETAILS) = Nothing
        LoadDummyGridData()
        btnAddToSelectedAgents.Enabled = False
        ClearText()
        'package specific stuff
        LoadPackageTypesCombo()
        LoadPackageCountriesCombo()
        'package specific stuff
        LoadDummyPackageSearchResultsGrid()

        If Not String.IsNullOrEmpty(Request.QueryString("AgnId")) _
         AndAlso Not String.IsNullOrEmpty(Request.QueryString("Text")) Then
            btnBulkAssign.Text = "Add"
            ' required for non-bulk loading, else it wont save agent discs!!!
            radCreateWithATP.Checked = True
        Else
            btnBulkAssign.Text = "Bulk Assign"
        End If
    End Sub

    Protected Sub Agent_AgentBulkLoading_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        MultipleSelectionAllowed(True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''rev:mia dec.15
        ConfirmationBoxControl_ShowAvailable.LeftButton_Text = "Continue"
        If Not Page.IsPostBack Then ContinueAssigning = False

        packageBulkLoadingPanel.Visible = IsPackageBulkLoading
        productBulkLoadingPanel.Visible = IsProductBulkLoading

        LoadPackages()
        If IsPostBack Then Return

        BuildBulkLoading()

    End Sub

    Sub ClearText()
        ''pkrAgentCountry.DataId = Nothing
        ''pkrAgentCountry.Text = ""


        pkrAgentGroup.DataId = Nothing
        pkrAgentGroup.Text = ""
        ''pkrMarketCode.DataId = Nothing
        ''pkrMarketCode.Text = ""

        pkrOtherProduct.DataId = Nothing
        pkrOtherProduct.Text = ""
        pkrProduct.DataId = Nothing
        pkrProduct.Text = ""
        pkrType.DataId = Nothing
        pkrType.Text = ""
    End Sub

    Sub CollapseAllDivs()
        cpnlAgentSelectionFilter.Collapsed = True
        cpnlSearchResultAgentList.Collapsed = True
        cpnlShowSelectedAgents.Collapsed = True
        cpnlShowAddProducts.Collapsed = True
        cpnlShowAvailableProducts.Collapsed = True
        cpnlShowModelAgentDiscount.Collapsed = True
        cpnlShowProductSelectionFilter.Collapsed = True

        If Not String.IsNullOrEmpty(Request.QueryString("AgnId")) _
         AndAlso Not String.IsNullOrEmpty(Request.QueryString("Text")) Then
            cpnlShowModelAgentDiscount.Visible = False
            pnlShowModelAgentDiscount.Visible = False
        End If
    End Sub

    Protected Sub gwSearchResultAgentList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwSearchResultAgentList.RowCreated
        Try
            e.Row.Cells(1).Visible = False
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub gwSearchResultAgentList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwSearchResultAgentList.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            CType(e.Row.Cells(0).FindControl("chkSelectAllAgent"), CheckBox).Attributes.Add("onclick", "return ToggleTableColumnCheckboxes(0, event);")
        End If
    End Sub

    Protected Sub btnAddToSelectedAgents_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToSelectedAgents.Click


        Dim xmlSearchResults As XmlElement
        'xmlSearchResults = ViewState(VIEWSTATE_SEARCH_RESULTS)

        'If xmlSearchResults Is Nothing Then
        If ViewState(VIEWSTATE_SEARCH_RESULTS) Is Nothing Then
            'had been cleared , this has to be an empty row
            SetInformationShortMessage("No search results specified, please search for Agents first")
            Return
        End If


        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(CStr(ViewState(VIEWSTATE_SEARCH_RESULTS)))
        xmlSearchResults = xmlDoc.DocumentElement.SelectSingleNode("Root")

        If (xmlSearchResults.ChildNodes.Count < 200) Then
            labelNumberOfAgent.Visible = False
            labelNumberOfAgent.Text = ""
            AllAgentsRecords = ""
            ContinueAssigning = False
        End If



        Dim xmlPriorSelectedAgents As New XmlDocument
        Dim xmlSelectedAgents As New XmlDocument
        Dim sbXMLString As New StringBuilder

        'sbXMLString.Append("<Root>")

        'set it to the session stuff, if it existed

        '  xmlPriorSelectedAgents = ViewState(VIEWSTATE_SELECTED_AGENT_DETAILS)

        ' If Not xmlPriorSelectedAgents Is Nothing Then
        If Not ViewState(VIEWSTATE_SELECTED_AGENT_DETAILS) Is Nothing Then
            xmlPriorSelectedAgents.LoadXml(CStr(ViewState(VIEWSTATE_SELECTED_AGENT_DETAILS)))
            sbXMLString.Append(xmlPriorSelectedAgents.DocumentElement.InnerXml)
        Else
            'initialise it
            xmlPriorSelectedAgents.LoadXml("<Root/>")
        End If

        ' fix to ensure something is selected
        Dim iSelectedAgentsCount As Integer = 0
        Dim hascheck As Boolean = False
        For i As Integer = 0 To gwSearchResultAgentList.Rows.Count - 1
            If CType(gwSearchResultAgentList.Rows(i).Cells(0).FindControl("chkAgnIsSel"), CheckBox).Checked Then
                hascheck = True
                If Not xmlPriorSelectedAgents.DocumentElement.InnerXml.Contains(xmlSearchResults.ChildNodes(i).OuterXml) Then
                    sbXMLString.Append(xmlSearchResults.ChildNodes(i).OuterXml)
                    xmlPriorSelectedAgents.DocumentElement.InnerXml = xmlPriorSelectedAgents.DocumentElement.InnerXml & xmlSearchResults.ChildNodes(i).OuterXml
                    iSelectedAgentsCount += 1
                End If
            End If
        Next

        If iSelectedAgentsCount = 0 And hascheck = False Then
            ' nothing was selected :(
            SetInformationShortMessage("Please tick the Agent(s) to be added")
            Exit Sub
        ElseIf iSelectedAgentsCount = 0 And hascheck = True Then
            SetInformationShortMessage("You have selected Agent(s) that is already been added in the 'Agents Selected' Grid")
            Exit Sub
        End If

        'save back to session
        ViewState(VIEWSTATE_SELECTED_AGENT_DETAILS) = xmlPriorSelectedAgents.OuterXml

        sbXMLString.Insert(0, "<Root>")
        sbXMLString.Append("</Root>")
        xmlSelectedAgents.LoadXml(sbXMLString.ToString())

        LoadSelectedAgentsList(xmlSelectedAgents)
        ClearSearchResultsAgentList()

    End Sub

    Protected Sub gwSelectedAgents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwSelectedAgents.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim chkCell As CheckBox
            chkCell = CType(e.Row.Cells(0).FindControl("chkSelected"), CheckBox)
            chkCell.Checked = True
        End If

        e.Row.Cells(1).Visible = False
        e.Row.Cells(3).Visible = False
        e.Row.Cells(4).Visible = False

        If e.Row.RowType = DataControlRowType.Header Then
            CType(e.Row.Cells(0).FindControl("chkSelectAllAgent"), CheckBox).Attributes.Add("onclick", "return ToggleTableColumnCheckboxes(0, event);")
        End If
    End Sub

    Sub LoadSearchResultsAgentList(ByVal XmlSearchResults As XmlElement)

        Dim dstSearchResultsAgentList As New DataSet
        Dim dtSearchResultsAgentList As DataTable
        If Not XmlSearchResults Is Nothing Then
            dstSearchResultsAgentList.ReadXml(New XmlNodeReader(XmlSearchResults))
            If dstSearchResultsAgentList.Tables.Count > 0 Then
                dtSearchResultsAgentList = dstSearchResultsAgentList.Tables(0)
            Else
                dtSearchResultsAgentList = New DataTable
            End If

            gwSearchResultAgentList.DataSource = dtSearchResultsAgentList
            gwSearchResultAgentList.DataBind()

            cpnlSearchResultAgentList.Collapsed = False
        End If

    End Sub

    Sub ClearSearchResultsAgentList()
        Dim dstSearchResultsAgentList As New DataSet
        Dim dtSearchResultsAgentList As DataTable
        Dim XmlSearchResults As New XmlDocument
        XmlSearchResults.LoadXml("<Root>" & _
                                    "<Agent>" & _
                                        "<AgnIsSel>0</AgnIsSel>" & _
                                        "<AgnId/>" & _
                                        "<AgentName/>" & _
                                        "<BillingCityTown />" & _
                                        "<BillingCountry/>" & _
                                        "<AgentType/>" & _
                                    "</Agent>" & _
                                  "</Root>")

        If Not XmlSearchResults Is Nothing Then
            dstSearchResultsAgentList.ReadXml(New XmlNodeReader(XmlSearchResults))
            If dstSearchResultsAgentList.Tables.Count > 0 Then
                dtSearchResultsAgentList = dstSearchResultsAgentList.Tables(0)
            Else
                dtSearchResultsAgentList = New DataTable
            End If

            gwSearchResultAgentList.DataSource = dtSearchResultsAgentList
            gwSearchResultAgentList.DataBind()

            cpnlSearchResultAgentList.Collapsed = False
        End If

        ViewState(VIEWSTATE_SEARCH_RESULTS) = Nothing
        btnAddToSelectedAgents.Enabled = False
    End Sub

    Sub LoadSelectedAgentsList(ByVal XmlSelectedAgentsList As XmlDocument)
        If XmlSelectedAgentsList Is Nothing Then Return

        Dim dstSelectedAgentList As New DataSet
        Dim dtSelectedAgentList As DataTable
        dstSelectedAgentList.ReadXml(New XmlNodeReader(XmlSelectedAgentsList))
        If dstSelectedAgentList.Tables.Count > 0 Then
            dtSelectedAgentList = dstSelectedAgentList.Tables(0)
        Else
            dtSelectedAgentList = New DataTable
        End If

        Dim table2000 As New DataTable

        Dim dummyagents As String = ""

        If (XmlSelectedAgentsList.DocumentElement.ChildNodes.Count > 200) Then
            ''table2000 = GetTop200(dtSelectedAgentList, XmlSelectedAgentsList.DocumentElement.SelectSingleNode("noRec").InnerText, 1)
            table2000 = GetTop200(dtSelectedAgentList, 199, 0)
            gwSelectedAgents.DataSource = table2000
            gwSelectedAgents.DataBind()
            dummyagents = GetAllRecords(dtSelectedAgentList)
        Else
            gwSelectedAgents.DataSource = dtSelectedAgentList
            gwSelectedAgents.DataBind()
        End If

        AllAgentsRecords = IIf(XmlSelectedAgentsList.DocumentElement.ChildNodes.Count > 200, dummyagents, "")
        labelNumberOfAgent.Visible = IIf(String.IsNullOrEmpty(AllAgentsRecords) Or Len(AllAgentsRecords) = 0, False, True)



        gwSelectedAgents.Enabled = Not HasMoreThan200Records ''gwSelectedAgents.Rows.Count < 199
        cpnlShowSelectedAgents.Collapsed = False
    End Sub

    Sub LoadClassCombo()
        cmbClass.DataSource = Aurora.SalesAndMarketing.Services.Agent.GetClassTypes()
        cmbClass.DataTextField = "DESCRIPTION"
        cmbClass.DataValueField = "ID"
        cmbClass.DataBind()
        cmbClass.Items.Insert(0, "")

    End Sub

    Sub LoadBrandCombo()
        Dim dtBrands As DataTable
        dtBrands = Aurora.SalesAndMarketing.Services.Agent.GetBrands(UserCode)
        cmbBrand.DataSource = dtBrands
        cmbBrand.DataTextField = "BrdName"
        cmbBrand.DataValueField = "BrdCode"
        cmbBrand.DataBind()
        cmbBrand.Items.Insert(0, "")

        'cmbPackageBrand 
        cmbPackageBrand.DataSource = dtBrands
        cmbPackageBrand.DataTextField = "BrdName"
        cmbPackageBrand.DataValueField = "BrdCode"
        cmbPackageBrand.DataBind()
        cmbPackageBrand.Items.Insert(0, "")

    End Sub

    Sub LoadProductsGrid(ByVal ProductsData As XmlElement)
        Dim dstProducts As New DataSet
        Dim dtProducts As DataTable
        dstProducts.ReadXml(New XmlNodeReader(ProductsData))
        If dstProducts.Tables.Count > 0 Then
            dtProducts = dstProducts.Tables(0)
        Else
            dtProducts = New DataTable
        End If

        gwAvailableProducts.DataSource = dtProducts
        gwAvailableProducts.DataBind()
        cpnlShowAvailableProducts.Collapsed = False
    End Sub

    Protected Sub btnShowAvailableProducts_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAvailableProducts.Click

        cpnlShowProductSelectionFilter.Collapsed = False
        'If cmbBrand.SelectedValue = "" And _
        '    cmbClass.SelectedValue = "" And _
        '    (cblAgentType.SelectedIndex = -1 Or String.IsNullOrEmpty(AgentTypeContent)) Then ''cmbAgentType.SelectedValue = "" Then
        '    SetInformationShortMessage("Please specify the criteria for 'Product Selection'")
        '    Return
        'End If

        If cmbBrand.SelectedValue = "" And _
            cmbClass.SelectedValue = "" Then
            SetInformationShortMessage("Please specify the criteria for 'Product Selection'")
            Return
        End If

        ''---------------------------------------------------------------------------
        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        ''cmbAgentType.SelectedValue was change to AgentTypeContent
        Dim xmlProductSearchResults As XmlDocument
        xmlProductSearchResults = Aurora.SalesAndMarketing.Services.Agent.GetBulkProduct(False, "", _
                                                                                         cmbBrand.SelectedValue, _
                                                                                         cmbClass.SelectedValue, _
                                                                                         "", _
                                                                                         UserCode)
        If xmlProductSearchResults.DocumentElement.ChildNodes(0).SelectSingleNode("Error") IsNot Nothing Then
            SetErrorMessage(xmlProductSearchResults.DocumentElement.ChildNodes(0).SelectSingleNode("Error").InnerText)
        Else
            LoadProductsGrid(xmlProductSearchResults.DocumentElement.ChildNodes(0))
        End If

    End Sub

    Protected Sub gwAvailableProducts_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwAvailableProducts.RowCreated
        e.Row.Cells(1).Visible = False
    End Sub

    Protected Sub gwAvailableProducts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwAvailableProducts.RowDataBound
        If e.Row.RowType = DataControlRowType.Header Then
            CType(e.Row.Cells(0).FindControl("chkSelectAllProducts"), CheckBox).Attributes.Add("onclick", "return ToggleTableColumnCheckboxes(0, event);")
        End If
    End Sub

    Protected Sub btnAddProductToList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddProductToList.Click
        If pkrProduct.DataId <> "" Then
            'its a valid id entered

            '<Product>
            '  <Check1>0</Check1>
            '  <PrdId>05B9BECD-036F-49A4-A1B2-2BEC573F7232</PrdId>
            '  <ProductName>CCMR - CCMR Britz</ProductName>
            '</Product>

            Dim xmlProductEntered As New XmlDocument
            xmlProductEntered.LoadXml("<Root><Product>" & _
                                        "<Check1>0</Check1>" & _
                                        "<PrdId>" & pkrProduct.DataId & "</PrdId>" & _
                                        "<ProductName>" & pkrProduct.Text & "</ProductName>" & _
                                      "</Product></Root>")
            LoadProductsGrid(xmlProductEntered.DocumentElement)
            cpnlShowAddProducts.Collapsed = False
        End If
    End Sub

    Sub LoadDummyGridData()
        'product grid
        Dim xmlDummy As New XmlDocument
        xmlDummy.LoadXml("<Root><Product>" & _
                                    "<Check1>0</Check1>" & _
                                    "<PrdId/>" & _
                                    "<ProductName/>" & _
                                  "</Product></Root>")
        LoadProductsGrid(xmlDummy.DocumentElement)
        'done with product grid

        'search result agent list
        xmlDummy.LoadXml("<Root><Agent>" & _
                                    "<AgnIsSel>0</AgnIsSel>" & _
                                    "<AgnId/>" & _
                                    "<AgentName/>" & _
                                    "<BillingCityTown/>" & _
                                    "<BillingCountry/>" & _
                                    "<AgentType/>" & _
                                  "</Agent></Root>")
        LoadSearchResultsAgentList(xmlDummy.DocumentElement)

        If Not String.IsNullOrEmpty(Request.QueryString("AgnId")) _
         AndAlso Not String.IsNullOrEmpty(Request.QueryString("Text")) Then
            xmlDummy.LoadXml("<Root><Agent>" & _
                                        "<AgnIsSel>0</AgnIsSel>" & _
                                        "<AgnId>" & HttpUtility.HtmlEncode(Request.QueryString("AgnId")) & "</AgnId>" & _
                                        "<AgentName>" & HttpUtility.HtmlEncode(Request.QueryString("Text")) & "</AgentName>" & _
                                        "<BillingCityTown/>" & _
                                        "<BillingCountry/>" & _
                                        "<AgentType/>" & _
                                      "</Agent></Root>")
            LoadSelectedAgentsList(xmlDummy)

            cpnlAgentSelectionFilter.Collapsed = True
            cpnlAgentSelectionFilter.Visible = False
            pnlAgentSelectionFilter.Visible = False

            cpnlShowAvailableProducts.Collapsed = True
            'cpnlShowAvailableProducts.Visible = False
            'pnlShowAvailableProducts.Visible = False

            cpnlSearchResultAgentList.Collapsed = True
            cpnlSearchResultAgentList.Visible = False
            pnlSearchResultAgentList.Visible = False

            cpnlShowSelectedAgents.Collapsed = False
            pnlShowSelectedAgents.Enabled = False
        Else
            LoadSelectedAgentsList(xmlDummy)

            cpnlAgentSelectionFilter.Collapsed = False
            cpnlShowAvailableProducts.Collapsed = True
            cpnlSearchResultAgentList.Collapsed = True
            cpnlShowSelectedAgents.Collapsed = False
        End If

        btnBack.Visible = Not String.IsNullOrEmpty(Me.Request.QueryString("BackUrl"))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        AllAgentsRecords = ""
        BuildBulkLoading()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        AllAgentsRecords = ""
        If String.IsNullOrEmpty(Me.Request.QueryString("BackUrl")) Then Return

        Response.Redirect(Me.Request.QueryString("BackUrl"))
    End Sub

    Protected Sub btnBulkAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBulkAssign.Click
        'the main stuff!

        'common validation
        Dim bAtleast1Agent As Boolean
        bAtleast1Agent = chkSelectAllAgents.Checked

        If Not bAtleast1Agent Then
            For i As Integer = 0 To gwSelectedAgents.Rows.Count - 1
                If CType(gwSelectedAgents.Rows(i).Cells(0).FindControl("chkSelected"), CheckBox).Checked Then
                    If gwSelectedAgents.Rows(i).Cells(1).Text <> "&nbsp;" Then
                        bAtleast1Agent = True
                        Exit For
                    End If
                End If
            Next

            ''rev:mia dec15
            If Not ContinueAssigning Then
                If Not bAtleast1Agent Then
                    SetErrorShortMessage("Please select at least one agent before selecting 'Bulk Assign'")
                    '''' HideProcessingIndicator()
                    Exit Sub
                End If

            End If
        End If
        'end common validation

        If IsProductBulkLoading Then
            'product
            If String.IsNullOrEmpty(Request.QueryString("AgnId")) _
            Or String.IsNullOrEmpty(Request.QueryString("Text")) Then
                ' only check if Bulk Loading
                'If dtcBookedFrom.Text.Trim() = "" Or dtcTravelFrom.Text.Trim() = "" Then
                If dtcEffectiveFrom.Text.Trim() = "" Then
                    SetWarningShortMessage("Booked From and Travel From are mandatory.")
                    Return
                End If
            End If

            'check that atleast 1 product and 1 agent is selected!
            Dim bAtleast1Product As Boolean = False

            For i As Integer = 0 To gwAvailableProducts.Rows.Count - 1
                If CType(gwAvailableProducts.Rows(i).Cells(0).FindControl("chkSelected"), CheckBox).Checked Then
                    bAtleast1Product = True
                    Exit For
                End If
            Next

            If Not (bAtleast1Product) Then
                SetWarningShortMessage("Please select at least one Product before selecting 'Bulk Assign'")
                Exit Sub
            End If

            'trying to save it!
            Dim xmlResult As XmlDocument
            xmlResult = Aurora.SalesAndMarketing.Services.Agent.SaveBulkAgentProducts(BuildScreenDataForProductSave(), UserCode, AGENT_BULK_LOADING)
            If xmlResult.DocumentElement.SelectSingleNode("ErrorType").InnerText.Trim().Equals("GEN106") Then
                Dim txtError As String = xmlResult.DocumentElement.SelectSingleNode("ErrorMessage").InnerText.Trim()
                If (txtError.Contains("GEN106") = True) Then
                    If (txtError.IndexOf("GEN106") > 1) Then
                        txtError = txtError.Substring(txtError.IndexOf("GEN106"))
                    End If
                End If
                SetInformationShortMessage(txtError)
            Else
                SetErrorMessage(xmlResult.DocumentElement.SelectSingleNode("ErrorMessage").InnerText.Trim())

            End If
            '''' HideProcessingIndicator()

            'end product
        Else
            'package

            'check that atleast 1 package is selected!
            Dim bAtleast1Package As Boolean

            bAtleast1Package = False

            For i As Integer = 0 To rptPackageSearchResults.Items.Count - 1
                If CType(rptPackageSearchResults.Items(i).FindControl("chkSelected"), CheckBox).Checked Then
                    bAtleast1Package = True
                    Exit For
                End If
            Next

            If Not (bAtleast1Package) Then
                SetWarningShortMessage("Please select at least one Package before selecting 'Bulk Assign'")
                '''' HideProcessingIndicator()
                Exit Sub
            End If

            'trying to save it!
            Dim xmlScreenData, xmlResult As XmlDocument

            Try
                xmlScreenData = BuildScreenDataForPackageSave()
            Catch ex As Exception
                SetErrorShortMessage(ex.Message)
                Exit Sub

            End Try


            xmlResult = Aurora.SalesAndMarketing.Services.Agent.SaveBulkAgentPackages(xmlScreenData)
            If xmlResult.DocumentElement.SelectSingleNode("ErrorType").InnerText.Trim().Equals("GEN106") Then
                SetInformationShortMessage(xmlResult.DocumentElement.SelectSingleNode("ErrorMessage").InnerText.Trim())
            Else
                SetErrorShortMessage(xmlResult.DocumentElement.SelectSingleNode("ErrorMessage").InnerText.Trim())
            End If

          
        End If
        '' AllAgentsRecords = IIf(Not ContinueAssigning, "", AllAgentsRecords)
    End Sub

    Function BuildScreenDataForProductSave() As XmlDocument
        Dim xmlScreenData As New XmlDocument
        Dim sbScreenDataXml As New StringBuilder

        Dim sModelFr As String = ""
        Dim sCreateDontCreate As String = ""

        If radCurrent.Checked Then
            sModelFr = "Current"
        ElseIf radLast.Checked Then
            sModelFr = "LastOfEffDt"
        ElseIf radLatest.Checked Then
            sModelFr = "LatestAdd"
        End If

        If radCreateWithATP.Checked Then
            sCreateDontCreate = "Create"
        ElseIf radDontCreate.Checked Then
            sCreateDontCreate = "DontCreate"
        End If

        sbScreenDataXml.Append("<Root><PD><Pds Id='" & Trim(pkrOtherProduct.DataId) & "' MFr='" + sModelFr & "' Cr='" & sCreateDontCreate & "' /></PD>")
        'sbScreenDataXml.Append("<CrDt Bf='" & dtcBookedFrom.Text & "' Tf='" & dtcTravelFrom.Text & "' />")

        If Not String.IsNullOrEmpty(Request.QueryString("AgnId")) _
                AndAlso Not String.IsNullOrEmpty(Request.QueryString("Text")) Then
            ' bulk loading
            sbScreenDataXml.Append("<CrDt Bf='" & _
                                            DateTimeToString(Now, SystemCulture).Split(" "c)(0) & "' Tf='" & _
                                            DateTimeToString(Now, SystemCulture).Split(" "c)(0) & "' />")
        Else
            ' not bulk loading
            sbScreenDataXml.Append("<CrDt Bf='" & _
                                            DateTimeToString(dtcEffectiveFrom.Date, SystemCulture).Split(" "c)(0) & "' Tf='" & _
                                            DateTimeToString(dtcEffectiveFrom.Date, SystemCulture).Split(" "c)(0) & "' />")
        End If

        Dim hasmore200records As Boolean = HasMoreThan200Records

        sbScreenDataXml.Append("<AgentDetail>")

        If chkSelectAllAgents.Checked Then
            sbScreenDataXml.Append("<AgnId id ='ALL'/>")
        Else
            If hasmore200records = True Then
                sbScreenDataXml.Append(AllAgentsRecords)
            Else
                For i As Integer = 0 To gwSelectedAgents.Rows.Count - 1
                    If CType(gwSelectedAgents.Rows(i).Cells(0).FindControl("chkSelected"), CheckBox).Checked Then
                        sbScreenDataXml.Append("<AgnId id ='" & Trim(gwSelectedAgents.Rows(i).Cells(1).Text) & "'/>")
                    End If
                Next
            End If
            
        End If
        sbScreenDataXml.Append("</AgentDetail>")

        sbScreenDataXml.Append("<ProductDetail>")
        For i As Integer = 0 To gwAvailableProducts.Rows.Count - 1
            If CType(gwAvailableProducts.Rows(i).Cells(0).FindControl("chkSelected"), CheckBox).Checked Then
                sbScreenDataXml.Append("<PrdId id ='" + Trim(gwAvailableProducts.Rows(i).Cells(1).Text) + "'/>")
            End If
        Next
        sbScreenDataXml.Append("</ProductDetail>")
        sbScreenDataXml.Append("</Root>")


        xmlScreenData.LoadXml(sbScreenDataXml.ToString())
        Return xmlScreenData

    End Function

#Region "PACKAGE STUFF"

    Protected Sub btnShowAvailablePackages_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAvailablePackages.Click
        If cmbPackageBrand.SelectedValue.Trim() = "" And _
            cmbPackageCountry.SelectedValue.Trim() = "" And _
            cmbPackageType.SelectedValue.Trim() = "" And _
            dtcPackageTravelFrom.Text.Trim() = "" And _
            dtcPackageTravelTo.Text.Trim() = "" And _
            dtcPackageBookingFrom.Text = "" And _
            dtcPackageBookingTo.Text.Trim() = "" And _
            PickerControlAgentPackageSelection.DataId = "" Then

            SetInformationShortMessage("Please enter your selection criteria!")
            Exit Sub
        End If

        Dim xmlPackages As XmlDocument
        xmlPackages = Aurora.SalesAndMarketing.Services.Agent.GetBulkPackages(False, "", _
                                cmbPackageCountry.SelectedValue, cmbPackageBrand.SelectedValue, cmbPackageType.SelectedValue, _
                                IIf(dtcPackageTravelFrom.Text.Trim().Equals(String.Empty), "", DateTimeToString(dtcPackageTravelFrom.Date, SystemCulture).Split(" "c)(0)), _
                                IIf(dtcPackageTravelTo.Text.Trim().Equals(String.Empty), "", DateTimeToString(dtcPackageTravelTo.Date, SystemCulture).Split(" "c)(0)), _
                                IIf(dtcPackageBookingFrom.Text.Trim().Equals(String.Empty), "", DateTimeToString(dtcPackageBookingFrom.Date, SystemCulture).Split(" "c)(0)), _
                                IIf(dtcPackageBookingTo.Text.Trim().Equals(String.Empty), "", DateTimeToString(dtcPackageBookingTo.Date, SystemCulture).Split(" "c)(0)), _
                                UserCode, _
                                PickerControlAgentPackageSelection.Text)
        LoadSearchResultPackagesGrid(xmlPackages.DocumentElement.SelectSingleNode("Packages"))

    End Sub

    Sub LoadPackageTypesCombo()
        Dim dtTypes As DataTable
        dtTypes = Aurora.SalesAndMarketing.Services.Agent.GetPackageTypes()
        cmbPackageType.DataSource = dtTypes
        cmbPackageType.DataTextField = "DESCRIPTION"
        cmbPackageType.DataValueField = "ID"
        cmbPackageType.DataBind()
        cmbPackageType.Items.Insert(0, "")
    End Sub

    Sub LoadPackageCountriesCombo()
        Dim dtCountries As DataTable
        dtCountries = Aurora.SalesAndMarketing.Services.Agent.GetCountriesOfOperation()
        cmbPackageCountry.DataSource = dtCountries
        cmbPackageCountry.DataTextField = "NAME"
        cmbPackageCountry.DataValueField = "CODE"
        cmbPackageCountry.DataBind()
        cmbPackageCountry.Items.Insert(0, "")
    End Sub

    Sub LoadSearchResultPackagesGrid(ByVal SearchData As XmlElement)
        Dim dstResults As New DataSet
        Dim dtResults As New DataTable
        dstResults.ReadXml(New XmlNodeReader(SearchData))
        If dstResults.Tables.Count > 0 Then
            dtResults = dstResults.Tables(0)
        End If

        ' hack to format the data
        For Each oRow As DataRow In dtResults.Rows
            Dim dtTemp As Date
            If Not CStr(oRow(2)).Trim().Equals(String.Empty) Then
                dtTemp = ParseDateTime(oRow(2), SystemCulture)
                oRow(2) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If

            If Not CStr(oRow(3)).Trim().Equals(String.Empty) Then
                dtTemp = ParseDateTime(oRow(3), SystemCulture)
                oRow(3) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If

            If Not CStr(oRow(4)).Trim().Equals(String.Empty) Then
                dtTemp = ParseDateTime(oRow(4), SystemCulture)
                oRow(4) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If

            If Not CStr(oRow(5)).Trim().Equals(String.Empty) Then
                dtTemp = ParseDateTime(oRow(5), SystemCulture)
                oRow(5) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If
        Next
        ' hack to format the data

        rptPackageSearchResults.DataSource = dtResults
        rptPackageSearchResults.DataBind()

        cpnlFilterPackageResults.Collapsed = False
        cpnlPackageSelectionFilter.Collapsed = False
        cpnlIdentifyPackages.Collapsed = True
    End Sub

    Protected Sub btnAddPackage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPackage.Click
        If String.IsNullOrEmpty(pkrPackage.DataId) Then Return

        Dim xmlPackageDetails As XmlDocument = Aurora.SalesAndMarketing.Services.Agent.GetPackageDetails(pkrPackage.DataId, UserCode)
        LoadSearchResultPackagesGrid(xmlPackageDetails.DocumentElement)
        cpnlFilterPackageResults.Collapsed = False
        cpnlIdentifyPackages.Collapsed = False
        cpnlPackageSelectionFilter.Collapsed = True
        pkrPackage.DataId = Nothing
        pkrPackage.Text = ""
    End Sub

    Function BuildScreenDataForPackageSave(Optional processOnDb As Boolean = False) As XmlDocument


        ' reqd Format
        '<Root>
        '	<AgentDetail>
        '		<AgnId id ='2EC4D68D-28EE-4AC0-A063-CF7240303BB4'/>
        '	</AgentDetail>
        '	<AgentPackages>
        '		<PkgId id='8748A7CE-8284-4F19-931F-75221CDB1AAB'/>
        '	</AgentPackages>
        '	<AgnSelection>Selected</AgnSelection>
        '	<AddModUserId>sp7</AddModUserId>
        '	<AddProgramName>AgentPackageBulkMgt.asp</AddProgramName>
        '</Root>
        Dim xmlScreenData As New XmlDocument
        Dim sbScreenDataXml As New StringBuilder

        sbScreenDataXml.Append("<Root>")
        sbScreenDataXml.Append("<AgentDetail>")

        Dim hasmore200records As Boolean = HasMoreThan200Records ''(Not AllAgentsRecords Is Nothing And AllAgentsRecords.Trim.Length > 0 And gwSelectedAgents.Rows.Count > 199) ''False
        If chkSelectAllAgents.Checked Then
            sbScreenDataXml.Append("<AgnId id ='ALL'/>")
        Else

            If hasmore200records = True Then
                sbScreenDataXml.Append(AllAgentsRecords)
                '' System.Diagnostics.Debug.Assert(Len(AllAgentsRecords) > 0, "all agents records is empty")
            Else
                For i As Integer = 0 To gwSelectedAgents.Rows.Count - 1
                    If CType(gwSelectedAgents.Rows(i).Cells(0).FindControl("chkSelected"), CheckBox).Checked Then
                        If Not ContinueAssigning Then ''confirmation box doesnt display
                            sbScreenDataXml.Append("<AgnId id ='" & Trim(gwSelectedAgents.Rows(i).Cells(1).Text) & "'/>")
                        Else
                            ''Criterias
                            sbScreenDataXml.Append(Criterias)
                        End If
                    End If
                Next
            End If

        End If
        sbScreenDataXml.Append("</AgentDetail>")

        sbScreenDataXml.Append("<AgentPackages>")
        For i As Integer = 0 To rptPackageSearchResults.Items.Count - 1
            If CType(rptPackageSearchResults.Items(i).FindControl("chkSelected"), CheckBox).Checked Then
                If CType(rptPackageSearchResults.Items(i).FindControl("chkSelected"), CheckBox).Attributes("PackageID") = "" Then
                    Throw New Exception("Select a valid package")
                End If
                sbScreenDataXml.Append("<PkgId id ='" + Trim(CType(rptPackageSearchResults.Items(i).FindControl("chkSelected"), CheckBox).Attributes("PackageID")) + "'/>")
            End If
        Next
        sbScreenDataXml.Append("</AgentPackages>")

        ''rev:mia dec16
        If Not ContinueAssigning Then
            
            sbScreenDataXml.Append("<AgnSelection>Selected</AgnSelection>")
        Else
            If (hasmore200records) Then
                sbScreenDataXml.Append("<AgnSelection>Selected</AgnSelection>")
            Else
                sbScreenDataXml.Append("<AgnSelection>CriteriaMatch</AgnSelection>")
            End If

        End If

        sbScreenDataXml.Append("<AddModUserId>" & UserCode & "</AddModUserId>")
        sbScreenDataXml.Append("<AddProgramName>" & AGENT_BULK_LOADING & "</AddProgramName>")

        sbScreenDataXml.Append("</Root>")

        xmlScreenData.LoadXml(sbScreenDataXml.ToString())
        Return xmlScreenData
    End Function

    Sub LoadDummyPackageSearchResultsGrid()
        'product grid
        Dim xmlDummy As New XmlDocument
        xmlDummy.LoadXml("<Root><Package>" & _
                                    "<Remove>0</Remove>" & _
                                    "<PkgId/>" & _
                                    "<PkgDesc/>" & _
                                     "<BkFr/>" & _
                                    "<BkTo/>" & _
                                     "<TrFr/>" & _
                                    "<TrTo/>" & _
                                  "</Package></Root>")


        Dim dstResults As New DataSet
        Dim dtResults As New DataTable
        dstResults.ReadXml(New XmlNodeReader(xmlDummy))
        If dstResults.Tables.Count > 0 Then
            dtResults = dstResults.Tables(0)
        End If

        rptPackageSearchResults.DataSource = dtResults
        rptPackageSearchResults.DataBind()
    End Sub

#End Region


#Region "REV:MIA    DECEMBER 15,2008"
    Private Function AvailablityMessage(ByVal noAgent As String, ByVal noAgentFound As String) As String
        Dim sb As New StringBuilder
        With sb
            .AppendFormat("There are more than <b>{0}</b> agents that meet your criteria.{1}", noAgent, "<br><br>")
            .AppendFormat("You can <b>'Continue'</b> to create Agent/Package for <b>{0}</b> agents found.{1}", noAgentFound, "<br><br>")
            .AppendFormat("Or you <b>'Cancel'</b> to modify your selection.(A result of less than 200 agents means {0}", "<br><br>")
            .AppendFormat("you can review the list and un-select any you do not want){0}", "<br><br>")
        End With
        Return sb.ToString
    End Function

    Protected Sub btnShowAvailableAgents_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAvailableAgents.Click


        ''---------------------------------------------------------------------------
        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        ''cmbAgentType.SelectedItem.Text was change to AgentTypeContent
        ''cmbCategory.SelectedItem.Text was change to CategoryTypeContent
        ''pkrAgentCountry.DataId was change to CountryContent
        ''pkrMarketCode.DataId was change to marketcodeContent
        ''packagescontent was change to PickerControlForPackage.DataId
        If String.IsNullOrEmpty(CountryContent) _
         AndAlso String.IsNullOrEmpty(pkrAgentGroup.DataId) _
         AndAlso String.IsNullOrEmpty(MarketcodeContent) _
         AndAlso String.IsNullOrEmpty(AgentTypeContent) _
         AndAlso String.IsNullOrEmpty(CategoryTypeContent) _
         AndAlso String.IsNullOrEmpty(PickerControlForAgentSelection.DataId) _
         AndAlso String.IsNullOrEmpty(PackagesContent) _
         Then
            SetWarningShortMessage("Agent selection filter parameters must be specified")
            Return
        End If


        ''---------------------------------------------------------------------------
        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        ''cmbAgentType.SelectedValue was change to AgentTypeContent
        ''cmbCategory.SelectedValue was change to CategoryTypeContent
        ''pkrAgentCountry.DataId was change to CountryContent
        ''pkrMarketCode.DataId was change to marketcodeContent
        ''packagescontent was change to PickerControlForPackage.DataId
        Dim xmlReturnDoc As XmlDocument = Aurora.SalesAndMarketing.Services.Agent.FindBulkAgent( _
            CountryContent, _
            pkrAgentGroup.DataId, _
            MarketcodeContent, _
            AgentTypeContent, _
            CategoryTypeContent, _
            PickerControlForAgentSelection.Text, _
            PackagesContent)


        '<data>
        '    <Msg>GEN049 - No Records Found</Msg> 
        '    <b200OrLess>1</b200OrLess> 
        '    <noRec>0</noRec> 
        '    <Root /> 
        '</data>

        'or

        ' <data>
        '    <Msg>GEN022 - Search Criteris has more than 200 records</Msg> 
        '    <b200OrLess>0</b200OrLess> 
        '    <noRec>1094</noRec> 
        '    <Root>
        '        <Agent>
        '            <AgnIsSel>0</AgnIsSel> 
        '            <AgnId>00C424F6-F4C5-4393-B6DC-621566056242</AgnId> 
        '            <AgentName>HOLSPCHC - HOLIDAY SHOPPE-ATLANTIC AND PACI</AgentName> 
        '            <BillingCityTown /> 
        '            <BillingCountry>NZ</BillingCountry> 
        '            <AgentType>Retail</AgentType> 
        '        </Agent>
        '        <Agent>
        '            <AgnIsSel>0</AgnIsSel> 
        '            <AgnId>02B94F99-12F8-48B9-B14E-EB4F5ADF9D1B</AgnId> 
        '            <AgentName>ASIAPCHC - ASIA PACIFIC WORLD TRAVEL</AgentName> 
        '            <BillingCityTown /> 
        '            <BillingCountry>NZ</BillingCountry> 
        '            <AgentType>Retail</AgentType> 
        '        </Agent>
        '    </Root>
        '</data>
        Dim blnDisplayZero As Boolean = False
        If (Not xmlReturnDoc.DocumentElement.SelectSingleNode("Msg") Is Nothing) And (Not xmlReturnDoc.DocumentElement.SelectSingleNode("noRec") Is Nothing) Then
            If xmlReturnDoc.DocumentElement.SelectSingleNode("noRec").InnerText = "0" Then
                SetErrorShortMessage(xmlReturnDoc.DocumentElement.SelectSingleNode("Msg").InnerText)
                ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
                LoadEmptyGridwhenSearchIsEmpty()
                LoadEmptyGridwhenSearchIsemptyForAgent()
                Return
            End If
            'more than 0 records
            If Not xmlReturnDoc.DocumentElement.SelectSingleNode("b200OrLess") Is Nothing Then
                If xmlReturnDoc.DocumentElement.SelectSingleNode("b200OrLess").InnerText = "0" Then
                    SetInformationShortMessage(xmlReturnDoc.DocumentElement.SelectSingleNode("Msg").InnerText)
                    With ConfirmationBoxControl_ShowAvailable
                        ConfirmationBoxControl_ShowAvailable.Text = AvailablityMessage("200", xmlReturnDoc.DocumentElement.SelectSingleNode("noRec").InnerText)
                        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
                        labelNumberOfAgent.Text = "Displaying <b>'200'</b> agents out of <b>'" & xmlReturnDoc.DocumentElement.SelectSingleNode("noRec").InnerText & "'</b> found."

                        ConfirmationBoxControl_ShowAvailable.Param = xmlReturnDoc.OuterXml
                        ConfirmationBoxControl_ShowAvailable.Show()
                    End With
                Else
                    ''ContinueAssigning = False ''this one will force to do selection
                    LoadSearchResultsAgentList(xmlReturnDoc.DocumentElement.SelectSingleNode("Root"))
                    btnAddToSelectedAgents.Enabled = True
                    gwSelectedAgents.Enabled = gwSelectedAgents.Rows.Count < 199

                End If

            End If

            ViewState(VIEWSTATE_SEARCH_RESULTS) = xmlReturnDoc.OuterXml
        End If
        btnAddToSelectedAgents.Enabled = True
    End Sub

    Public Property ContinueAssigning() As Boolean
        Get
            Return ViewState("ContinueAssigning")
        End Get
        Set(ByVal value As Boolean)
            ViewState("ContinueAssigning") = value
        End Set
    End Property

    Protected Sub ConfirmationBoxControl_ShowAvailable_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl_ShowAvailable.PostBack
        If leftButton Then
            ''do nothing

            ContinueAssigning = True
            LoadEmptyGridwhenSearchIsEmpty()

            Dim xmlAgentContent As New XmlDocument
            xmlAgentContent.LoadXml(param)
            xmlAgentContent.LoadXml(xmlAgentContent.DocumentElement.SelectSingleNode("Root").OuterXml)
            LoadSelectedAgentsList(xmlAgentContent)

            gwSelectedAgents.Enabled = False
            labelNumberOfAgent.Visible = True

        Else

            ContinueAssigning = False
        End If
    End Sub

    Private Function Criterias() As String
        'SET	@sCountryCode = dbo.getSplitedData(dbo.getSplitedData(@Criteria, ',', 1), '-', 1)
        'SET	@sGroupCode = dbo.getSplitedData(dbo.getSplitedData(@Criteria, ',', 2), '-', 1)
        'SET	@MktRgnId = dbo.getCodeId(10, dbo.getSplitedData(dbo.getSplitedData(@Criteria, ',', 3), '-', 1))
        'SET	@sAgentTypeId = dbo.getSplitedData(@Criteria, ',', 4)
        'SET	@sCategoryId = dbo.getSplitedData(@Criteria, ',', 5)
        'SET	@BillingCodeId = dbo.getCodeId(1, 'Billing')
        Dim criteria As String = "<AgnId Criteria='@1,@2,@3,@4,'/>" '' "<AgnId Criteria='AU,,,EF2C8354-0EAC-425E-9BCC-B2B702218582,'/>"

        Dim ctycontent As String = CountryContent()
        Dim sCountryCode As String = "" ''CountryContent() ''pkrAgentCountry.Text.Split("-")(0)
        If (ctycontent.Contains(",")) Then
            sCountryCode = ctycontent.Replace(",", "|")
        Else
            sCountryCode = ctycontent
        End If

        Dim sGroupCode As String = pkrAgentGroup.DataId & ""
        If (sGroupCode.Contains(",")) Then
            sGroupCode = sGroupCode.Replace(",", "|")
        End If



        Dim marketcontent As String = MarketcodeContent()
        Dim MktRgnId As String = "" ''pkrMarketCode.DataId & ""
        If (marketcontent.Contains(",")) Then
            MktRgnId = marketcontent.Replace(",", "|")
        Else
            MktRgnId = marketcontent
        End If

        ''---------------------------------------------------------------------------
        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        ''cmbAgentType.SelectedValue was change to AgentTypeContent
        Dim sAgentTypeId As String
        Dim agenttypeidcontent As String = AgentTypeContent()
        If (agenttypeidcontent.Contains(",")) Then
            sAgentTypeId = agenttypeidcontent.Replace(",", "|")
        Else
            sAgentTypeId = agenttypeidcontent
        End If

        Dim sCategoryId As String = ""
        criteria = criteria.Replace("@1", sCountryCode)
        criteria = criteria.Replace("@2", sGroupCode)
        criteria = criteria.Replace("@3", MktRgnId)
        criteria = criteria.Replace("@4", sAgentTypeId)
        Return criteria
    End Function
#End Region


#Region "rev:mia Jan. 14 2013 - addition of a field to allow multiselection"

    Function PackagesContent() As String
        Dim sbitem As New StringBuilder

        For Each item As ListItem In chkpackagebulk.Items
            If item.Selected = True And item.Text.Contains("(ALL)") = False Then
                sbitem.AppendFormat("{0},", item.Value)
            End If
        Next
        If (chkpackagebulk.SelectedIndex > -1) Then
            Return sbitem.ToString.Remove(sbitem.ToString.LastIndexOf(","), 1)
        Else
            Return ""
        End If
    End Function

    Function MarketcodeContent() As String
        Dim sbitem As New StringBuilder

        For Each item As ListItem In chkMarketCode.Items
            If item.Selected = True And item.Text.Contains("(ALL)") = False Then
                sbitem.AppendFormat("{0},", item.Value)
            End If
        Next
        If (chkMarketCode.SelectedIndex > -1) Then
            Return sbitem.ToString.Remove(sbitem.ToString.LastIndexOf(","), 1)
        Else
            Return ""
        End If
    End Function

    Function CountryContent() As String
        Dim sbitem As New StringBuilder

        For Each item As ListItem In chkCountries.Items
            If item.Selected = True And item.Text.Contains("(ALL)") = False Then
                sbitem.AppendFormat("{0},", item.Value)
            End If
        Next
        If (chkCountries.SelectedIndex > -1) Then
            Return sbitem.ToString.Remove(sbitem.ToString.LastIndexOf(","), 1)
        Else
            Return ""
        End If
    End Function

    Function AgentTypeContent() As String
        Dim sbitem As New StringBuilder

        For Each item As ListItem In cblAgentType.Items
            If item.Selected = True And item.Text.Contains("(ALL)") = False Then
                sbitem.AppendFormat("{0},", item.Value)
            End If
        Next
        If (cblAgentType.SelectedIndex > -1) Then
            Return sbitem.ToString.Remove(sbitem.ToString.LastIndexOf(","), 1)
        Else
            Return ""
        End If

    End Function

    Function CategoryTypeContent() As String
        Dim sbitem As New StringBuilder

        For Each item As ListItem In cblCategory.Items
            If item.Selected = True And item.Text.Contains("(ALL)") = False Then
                sbitem.AppendFormat("{0},", item.Value)
            End If
        Next
        If (cblCategory.SelectedIndex > -1) Then
            Return sbitem.ToString.Remove(sbitem.ToString.LastIndexOf(","), 1)
        Else
            Return ""
        End If

    End Function

    Sub LoadEmptyGridwhenSearchIsEmpty()

        Dim xmlDummy As New XmlDocument
        xmlDummy.LoadXml("<Root><Agent>" & _
                                    "<AgnIsSel>0</AgnIsSel>" & _
                                    "<AgnId/>" & _
                                    "<AgentName/>" & _
                                    "<BillingCityTown/>" & _
                                    "<BillingCountry/>" & _
                                    "<AgentType/>" & _
                                  "</Agent></Root>")
        LoadSearchResultsAgentList(xmlDummy.DocumentElement)
        btnAddToSelectedAgents.Enabled = False
    End Sub

    Sub LoadEmptyGridwhenSearchIsemptyForAgent()
        Dim xmlDummy As New XmlDocument
        xmlDummy.LoadXml("<Root><Agent>" & _
                                       "<AgnIsSel>0</AgnIsSel>" & _
                                       "<AgnId>" & HttpUtility.HtmlEncode(Request.QueryString("AgnId")) & "</AgnId>" & _
                                       "<AgentName>" & HttpUtility.HtmlEncode(Request.QueryString("Text")) & "</AgentName>" & _
                                       "<BillingCityTown/>" & _
                                       "<BillingCountry/>" & _
                                       "<AgentType/>" & _
                                     "</Agent></Root>")
        LoadSelectedAgentsList(xmlDummy)
    End Sub

    Sub MultipleSelectionAllowed(allowed As Boolean)
        pkrAgentGroup.AllowedMultipleSelection = allowed
        PickerControlForAgentSelection.AllowedMultipleSelection = allowed
    End Sub



    Function GetTop200(dt As DataTable, RowCount As Integer, _Start As Integer) As DataTable
        Dim _table As DataTable
        _table = dt.Clone()
        For i As Integer = _Start To _Start + RowCount
            If i >= dt.Rows.Count Then
                Exit For
            Else
                _table.ImportRow(dt.Rows(i))
            End If
        Next
        Return _table
    End Function

    Function GetAllRecords(dt As DataTable) As String
        ''<AgnId id ='" & Trim(gwSelectedAgents.Rows(i).Cells(1).Text) & "'/>"

        Dim sb As New StringBuilder
        For i As Integer = 0 To dt.Rows.Count
            If i >= dt.Rows.Count Then
                Exit For
            Else
                sb.Append("<AgnId id ='" & dt.Rows(i)(1).ToString.Trim & "'/>")
            End If
        Next

        Return sb.ToString
    End Function

    Private Property AllAgentsRecords As String
        Get
            Return ViewState("AllAgentsRecords")
        End Get
        Set(value As String)
            ViewState("AllAgentsRecords") = value
        End Set
    End Property

    Private ReadOnly Property HasMoreThan200Records As Boolean
        Get
            Return ((AllAgentsRecords.Trim.Length > 0 And Not String.IsNullOrEmpty(AllAgentsRecords)) And gwSelectedAgents.Rows.Count > 199 And ContinueAssigning) ''False
        End Get

    End Property

    Function HighlightAUandNZ(input As String) As String


    End Function

    Function MakeUppercaseTheFirstLetter(input As String) As String
        If (String.IsNullOrEmpty(input)) Then
            Return String.Empty
        End If
        Dim arrayLetter() As Char = input.ToCharArray
        arrayLetter(0) = Char.ToUpper(arrayLetter(0))

        Return New String(arrayLetter)

    End Function

    Sub LoadCountries()

        Dim dsCountries As New DataSet
        dsCountries = Aurora.SalesAndMarketing.Services.Agent.GetPopUpDataForAgentBulkloading("AGNENTCOUNTRY", UserCode)
        Dim item As ListItem
        Dim id As String
        Dim text As String
        For Each row As DataRow In dsCountries.Tables(0).Rows
            id = row("CODE")
            text = row("DESCRIPTION")
            item = New ListItem(MakeUppercaseTheFirstLetter(text.ToLower), id)
            item.Attributes.Add("title", row("DESCRIPTION"))
            chkCountries.Items.Add(item)
        Next
        chkCountries.Items.Insert(0, New ListItem("(ALL)", "LoadCountries"))
    End Sub

    Sub LoadMarketCode()
        Dim dsMarketcode As New DataSet
        dsMarketcode = Aurora.SalesAndMarketing.Services.Agent.GetPopUpDataForAgentBulkloading("MARKETCODE", UserCode)
        Dim item As ListItem
        Dim id As String
        Dim text As String
        For Each row As DataRow In dsMarketcode.Tables(0).Rows
            id = row("CODE")
            text = row("DESCRIPTION")
            item = New ListItem(MakeUppercaseTheFirstLetter(text.ToLower), id)
            item.Attributes.Add("title", row("DESCRIPTION"))
            chkMarketCode.Items.Add(item)
        Next
        chkMarketCode.Items.Insert(0, New ListItem("(ALL)", "LoadMarketCode"))
    End Sub

    Sub LoadPackages()
        Dim dsPackages As New DataSet
        dsPackages = Aurora.SalesAndMarketing.Services.Agent.GetPopUpDataForAgentBulkloading("PACKAGEBULK", UserCode)

        Dim item As ListItem
        Dim id As String
        Dim text As String
        Dim tempItem As ListItem = Nothing

        For Each row As DataRow In dsPackages.Tables(0).Rows
            id = row("ID")
            text = row("CODE")
            tempItem = chkpackagebulk.Items.FindByValue(id)
            If (Not tempItem Is Nothing) Then
                tempItem.Attributes.Add("title", row("DESCRIPTION"))
            Else
                item = New ListItem(MakeUppercaseTheFirstLetter(text), id)
                item.Attributes.Add("title", row("DESCRIPTION"))
                chkpackagebulk.Items.Add(item)
            End If
        Next
        tempItem = chkpackagebulk.Items.FindByValue("LoadPackages")
        'If (tempItem Is Nothing) Then
        '    chkpackagebulk.Items.Insert(0, New ListItem("(ALL)", "LoadPackages"))
        'End If

    End Sub

    Sub LoadAgentTypes()
        Dim dstAgentTypes As DataSet
        dstAgentTypes = Aurora.Common.Data.GetComboData("AgentType", "", "", "")
        Dim dtAgentTypes As DataTable
        If Not dstAgentTypes.Tables(0) Is Nothing Then
            dtAgentTypes = dstAgentTypes.Tables(0)
        Else
            dtAgentTypes = New DataTable
        End If

        ''---------------------------------------------------------------------------
        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        'cmbAgentType.DataSource = dtAgentTypes
        'cmbAgentType.DataTextField = "DESCRIPTION"
        'cmbAgentType.DataValueField = "ID"
        'cmbAgentType.DataBind()
        'cmbAgentType.Items.Insert(0, "")
        'cblAgentType.DataSource = dtAgentTypes
        'cblAgentType.DataTextField = "DESCRIPTION"
        'cblAgentType.DataValueField = "ID"
        'cblAgentType.DataBind()
        Dim item As ListItem
        Dim id As String
        Dim text As String
        For Each row As DataRow In dtAgentTypes.Rows
            id = row("ID")
            text = row("DESCRIPTION")
            item = New ListItem(MakeUppercaseTheFirstLetter(text.ToLower), id)
            item.Attributes.Add("title", row("DESCRIPTION"))
            cblAgentType.Items.Add(item)
        Next
        cblAgentType.Items.Insert(0, New ListItem("(ALL)", "LoadAgentTypes"))
        ''---------------------------------------------------------------------------
    End Sub

    Sub LoadCategories()
        Dim dstCategories As DataSet = Aurora.Common.Data.GetCodecodetype(35, "")
        Dim dtCategories As DataTable

        If Not dstCategories.Tables(0) Is Nothing Then
            dtCategories = dstCategories.Tables(0)
        Else
            dtCategories = New DataTable
        End If
        ''---------------------------------------------------------------------------
        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        'cmbCategory.DataSource = dtCategories
        'cmbCategory.DataTextField = "DESCRIPTION"
        'cmbCategory.DataValueField = "ID"
        'cmbCategory.DataBind()
        'cmbCategory.Items.Insert(0, "")
        Dim item As ListItem
        Dim id As String
        Dim text As String
        For Each row As DataRow In dtCategories.Rows
            id = row("ID")
            text = row("DESCRIPTION")
            item = New ListItem(MakeUppercaseTheFirstLetter(text), id)
            item.Attributes.Add("title", row("DESCRIPTION"))
            cblCategory.Items.Add(item)
        Next
        cblCategory.Items.Insert(0, New ListItem("(ALL)", "LoadCategories"))
        ''---------------------------------------------------------------------------
    End Sub

#End Region

End Class
