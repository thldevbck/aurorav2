<%@ Page Language="VB" AutoEventWireup="false" CodeFile="WebNonVehicle.aspx.vb" Inherits="SalesAndMarketing_Default"
    MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="Controls/CreditCardsUserControl.ascx" TagName="CreditCardsUserControl"
    TagPrefix="uc4" %>
<%@ Register Src="Controls/FerryItemsUserControl.ascx" TagName="FerryItemsUserControl"
    TagPrefix="uc3" %>
<%@ Register Src="Controls/ExtraHireItemsUserControl.ascx" TagName="ExtraHireItemsUserControl"
    TagPrefix="uc2" %>
<%@ Register Src="Controls/InsurancesUserControl.ascx" TagName="InsurancesUserControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:UpdatePanel ID="MainUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="WebNonVehPanel" runat="Server" Width="850px" DefaultButton="showDetailsButton">
                <span style="width: 100%; float: left;">
                    <table style="width: 100%" cellpadding="2" cellspacing="0">
                        <tr>
                            <td style="width: 150px">
                                Country:
                            </td>
                            <td style="width: 250px">
                                <asp:DropDownList ID="countryDropdown" runat="server" Width="150">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 150px">
                                Brand:
                            </td>
                            <td style="width: 250px">
                                <asp:DropDownList ID="brandDropDown" runat="server" Width="150">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 250px">
                                Vehicle Type:
                            </td>
                            <td style="width: 250px">
                                <asp:DropDownList ID="VehicleTypeDropDown" runat="server" Width="150">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 150px">
                                <asp:Button ID="showDetailsButton" runat="server" Text="Show Details" CssClass="Button_Standard Button_Search" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 150px">
                                Category:
                            </td>
                            <td style="width: 250px">
                                <asp:DropDownList ID="VehMgtTypeDropDown" runat="server" Width="150">
                                </asp:DropDownList>
                            </td>
                        </tr>

                        <tr>
                            <td rowspan="1">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <uc1:InsurancesUserControl ID="InsurancesUserControl1" runat="server" />
                        </tr>
                        <tr>
                            <td rowspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <uc2:ExtraHireItemsUserControl ID="ExtraHireItemsUserControl1" runat="server" />
                        </tr>
                        <tr>
                            <td rowspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <uc3:FerryItemsUserControl ID="FerryItemsUserControl1" runat="server" />
                        </tr>
                        <tr>
                            <td rowspan="2">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <uc4:CreditCardsUserControl ID="CreditCardsUserControl1" runat="server" />
                        </tr>
                        <tr>
                            <td rowspan="2">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </span>
                <table style="width: 100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td colspan="7" align="right" style="border-top-width: 1px; border-top-style: solid">
                            <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="HidCountryValue" runat="server" Visible="False" />
                <asp:HiddenField ID="hidBrandValue" runat="server" Visible="False" />
                <asp:HiddenField ID="hidVehicleType" runat="server" Visible="False" />
                <asp:HiddenField ID="hidVehMgtType" runat="server" Visible="false" />
             </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
