''-----------------
''-SP needed
''  Admin_getComCode
''  WEBP_getHreItmWebSetupId
''  WEBP_getForWebExtraHireSetup
''  Products_WEbGetLocationsAndBrands
''  webp_getproductlist
''  WEBP_InsertUpdateWebNonVehicleItems
''  WEBP_InsertUpdateNonVehItems
''  WEBP_getForWebExtraHireSetupExtraHireItems
''  WEBP_getForWebExtraHireSetupFerry
''-----------------
Imports System.xml
Imports System.Data
Imports System.Data.SqlClient
Imports Aurora.SalesAndMarketing

#Region "Notes: June 25 2010 - fixes for duplicate products"

''added Aurora.SalesAndMarketing.Data.DataRepository.Get_WebSetupIDPlusInsuranceData for InsuranceControl only
''modified GetStoredData(Optional ByVal resetTable As Boolean = False)
''modified InsuranceUserControl.ascx - width of all checkbox list

''SP changes 
''WEBP_InsertUpdateWebNonVehicleItems
''added WEBP_getForWebExtraHireSetupInsurance
#End Region

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.WebNonVehicleProductDisplay)> _
Partial Class SalesAndMarketing_Default
    Inherits AuroraPage

#Region "Enum"

    Public Enum Options
        InsuranceItem = 1
        ExtraHireItems = 2
        FerryItem = 3
        CreditCardItem = 4
    End Enum

#End Region

#Region "property"
    Private Property FirstLoad() As Boolean
        Get
            Return ViewState("FirstLoad")
        End Get
        Set(ByVal value As Boolean)
            ViewState("FirstLoad") = value
        End Set
    End Property

    Private Property BrandSelected() As String
        Get
            Return ViewState("BrandSelected")
        End Get
        Set(ByVal value As String)
            ViewState("BrandSelected") = value
        End Set
    End Property

    Private Property VehicleTypeSelected() As String
        Get
            Return ViewState("VehicleTypeSelected")
        End Get
        Set(ByVal value As String)
            ViewState("VehicleTypeSelected") = value
        End Set
    End Property
#End Region

#Region "Load Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            FirstLoad = True
            InitCountriesBrandAndVehicleTypes()
            Me.SaveButton.Enabled = False
        End If
    End Sub
#End Region

#Region "private procedures"

    Sub InitCountriesBrandAndVehicleTypes()

        Dim ds As DataSet = Data.DataRepository.Get_WebLocationsAndBrands(Me.UserCode)

        Me.countryDropdown.Items.Add("(Select)")
        Me.countryDropdown.AppendDataBoundItems = True
        Me.countryDropdown.DataSource = ds.Tables(0).DefaultView
        Me.countryDropdown.DataTextField = "Countries"
        Me.countryDropdown.DataValueField = "CtyCode"
        Me.countryDropdown.AutoPostBack = True

        Me.brandDropDown.Items.Add("(Select)")
        Me.brandDropDown.AppendDataBoundItems = True
        Me.brandDropDown.AutoPostBack = True

        Me.VehicleTypeDropDown.Items.Add("(Select)")
        Me.VehicleTypeDropDown.AppendDataBoundItems = True
        Me.VehicleTypeDropDown.AutoPostBack = True

        Me.VehMgtTypeDropDown.DataSource = ds.Tables(3).DefaultView
        Me.VehMgtTypeDropDown.DataTextField = "CodCode"
        Me.VehMgtTypeDropDown.DataValueField = "CodId"
        Me.VehMgtTypeDropDown.AutoPostBack = True
        Me.VehMgtTypeDropDown.SelectedIndex = 0
        Page.DataBind()
    End Sub

    Sub UpdateHiddenfield()
        Me.HidCountryValue.Value = Me.countryDropdown.SelectedValue
        Me.hidBrandValue.Value = Me.brandDropDown.SelectedValue
        Me.hidVehicleType.Value = Me.VehicleTypeDropDown.SelectedValue
        Me.hidVehMgtType.Value = Me.VehMgtTypeDropDown.SelectedValue
    End Sub

    Sub ClearHiddenfield()
        Me.HidCountryValue.Value = ""
        Me.hidBrandValue.Value = ""
        Me.hidVehicleType.Value = ""
        Me.hidVehMgtType.Value = ""
    End Sub

    Sub PassParameterToControls()

        Me.CreditCardsUserControl1.ResetXMLs()
        Me.CreditCardsUserControl1.UnBind()
        Me.CreditCardsUserControl1.Country = Me.HidCountryValue.Value.TrimEnd
        Me.CreditCardsUserControl1.Brand = Me.hidBrandValue.Value.TrimEnd
        Me.CreditCardsUserControl1.VehicleType = Me.hidVehicleType.Value.TrimEnd
        Me.CreditCardsUserControl1.BindDropDown()

        Me.InsurancesUserControl1.ResetXMLs()
        Me.InsurancesUserControl1.UnBind()
        Me.InsurancesUserControl1.Country = Me.HidCountryValue.Value.TrimEnd
        Me.InsurancesUserControl1.Brand = Me.hidBrandValue.Value.TrimEnd
        Me.InsurancesUserControl1.VehicleType = Me.hidVehicleType.Value.TrimEnd
        Me.InsurancesUserControl1.BindDropDown()

        Me.FerryItemsUserControl1.ResetXMLs()
        Me.FerryItemsUserControl1.UnBind()
        Me.FerryItemsUserControl1.Country = Me.HidCountryValue.Value.TrimEnd
        Me.FerryItemsUserControl1.Brand = Me.hidBrandValue.Value.TrimEnd
        Me.FerryItemsUserControl1.VehicleType = Me.hidVehicleType.Value.TrimEnd
        Me.FerryItemsUserControl1.BindDropDown()

        Me.ExtraHireItemsUserControl1.ResetXMLs()
        Me.ExtraHireItemsUserControl1.UnBind()
        Me.ExtraHireItemsUserControl1.Country = Me.HidCountryValue.Value.TrimEnd
        Me.ExtraHireItemsUserControl1.Brand = Me.hidBrandValue.Value.TrimEnd
        Me.ExtraHireItemsUserControl1.VehicleType = Me.hidVehicleType.Value.TrimEnd
        Me.ExtraHireItemsUserControl1.BindDropDown()
    End Sub

    Sub GetStoredData(Optional ByVal resetTable As Boolean = False)
        Dim message As String = ""
        FirstLoad = False
        Dim xmldoc As XmlDocument = Nothing
        Try
            xmldoc = Aurora.SalesAndMarketing.Data.DataRepository.Get_WebSetupIDPlusData(Me.HidCountryValue.Value.TrimEnd, _
                                                                                               Me.hidBrandValue.Value.TrimEnd, _
                                                                                               Me.hidVehicleType.Value.TrimEnd, _
                                                                                               Me.hidVehMgtType.Value.TrimEnd)

            ''June 28 2010 separation of Insurance from the rest
            Dim insurance As XmlDocument
            insurance = Aurora.SalesAndMarketing.Data.DataRepository.Get_WebSetupIDPlusInsuranceData(Me.HidCountryValue.Value.TrimEnd, _
                                                                                               Me.hidBrandValue.Value.TrimEnd, _
                                                                                               Me.hidVehicleType.Value.TrimEnd, _
                                                                                               Me.hidVehMgtType.Value.TrimEnd)


            ''July 12 2010 separation of ferry from the rest
            Dim ferry As XmlDocument
            ferry = Aurora.SalesAndMarketing.Data.DataRepository.Get_WebSetupIDPlusFerryData(Me.HidCountryValue.Value.TrimEnd, _
                                                                                               Me.hidBrandValue.Value.TrimEnd, _
                                                                                               Me.hidVehicleType.Value.TrimEnd, _
                                                                                               Me.hidVehMgtType.Value.TrimEnd)

            If xmldoc IsNot Nothing Then
                ''xpath for insurance
                Dim insuranceNodelist As XmlNodeList = insurance.SelectNodes("data/WebSetup/ExtraHireItems/HireItems[@ClaCode = 'Insurence']")
                If (insuranceNodelist.Count <> 0) Then
                    Me.InsurancesUserControl1.DisplayInsurance(insuranceNodelist, resetTable)
                Else
                    Me.InsurancesUserControl1.EnableRemoveButton(False)
                    message = "<li>No Insurance List found</li>"
                End If


                ''xpath for extrahire
                Dim extraNodeList As XmlNodeList = xmldoc.SelectNodes("data/WebSetup/ExtraHireItems/HireItems[@ClaCode = 'Extra Hire Items']")
                If (extraNodeList.Count <> 0) Then
                    Me.ExtraHireItemsUserControl1.DisplayExtraHireItems(extraNodeList, resetTable)
                Else
                    Me.ExtraHireItemsUserControl1.EnableRemoveButton(False)
                    message = message + "<li>No ExtraHire Items List found</li>"
                End If


                ''xpath for ferry
                Dim ferryNodeList As XmlNodeList = ferry.SelectNodes("data/WebSetup/ExtraHireItems/HireItems[@ClaCode = 'Ferry crossing']")
                If (ferryNodeList.Count <> 0) Then
                    Me.FerryItemsUserControl1.DisplayFerry(ferryNodeList, resetTable)
                Else
                    Me.FerryItemsUserControl1.EnableRemoveButton(False)
                    message = message + "<li>No Ferry Items List found</li>"
                End If


                ''xpath for Creditcard
                Dim creditNodeList As XmlNodeList = xmldoc.SelectNodes("data/WebSetup/CardTypes/CardType")
                If (creditNodeList.Count <> 0) Then
                    Me.CreditCardsUserControl1.DisplayCreditCards(creditNodeList, resetTable)
                Else
                    Me.CreditCardsUserControl1.EnableRemoveButton(False)
                    message = message + "<li>No CreditCard List found</li>"
                End If


                ''mia:rev 17-Oct-2016 -Add new Category in WebNonVehicle for Relocation and General Category   
                Dim catId As String = xmldoc.SelectSingleNode("data/WebSetup/CatId").InnerText
                If (Not String.IsNullOrEmpty(catId)) Then
                    VehMgtTypeDropDown.SelectedValue = catId
                    hidVehMgtType.Value = catId
                Else
                    VehMgtTypeDropDown.SelectedIndex = -1
                    hidVehMgtType.Value = String.Empty
                End If

                If (Not String.IsNullOrEmpty(message) And Not resetTable) Then
                    Me.SetInformationMessage("Records result: <ul>" & message & "</ul>")
                End If
                Me.SaveButton.Enabled = True
            Else
                Me.SaveButton.Enabled = False
                Throw New Exception
            End If

        Catch ex As Exception
            Me.SetWarningMessage(ex.Message)
        End Try


    End Sub

    Sub SaveData()
        Dim dsWebItems As DataSet = New DataSet("WebHireItems")
        dsWebItems.Tables.Add(Me.InsurancesUserControl1.InsuranceTable.Copy)
        dsWebItems.Tables.Add(Me.FerryItemsUserControl1.FerryTable.Copy)
        dsWebItems.Tables.Add(Me.ExtraHireItemsUserControl1.ExtraHireItemTable.Copy)
        dsWebItems.Tables.Add(Me.CreditCardsUserControl1.CardTypeTable.Copy)

        Dim xmltemp As New XmlDocument
        xmltemp.LoadXml(dsWebItems.GetXml)
        For Each elem As XmlElement In xmltemp.DocumentElement
            If elem.Name = "Insurance" Or elem.Name = "ExtraHire" Or elem.Name = "Ferry" Then
                If elem.ChildNodes.Count > 0 Then
                    If elem.ChildNodes(0).Name = "ProductID" Then
                        Dim key As String = elem.ChildNodes(0).InnerText
                        If key.Contains("|") Then
                            key = key.Remove(key.IndexOf("|"))
                            elem.ChildNodes(0).InnerText = key
                        End If
                    End If
                    If elem.Name = "Insurance" Then
                        If elem.ChildNodes(13).Name = "Bond" Then
                            If elem.ChildNodes(13).InnerText.Contains("All") Then
                                elem.ChildNodes(13).InnerText = Me.InsurancesUserControl1.ReturnAllBondVehicle
                            End If
                        End If
                        If elem.ChildNodes(15).Name = "Imprint" Then
                            If elem.ChildNodes(15).InnerText.Contains("All") Then
                                elem.ChildNodes(15).InnerText = Me.InsurancesUserControl1.ReturnAllImprintVehicle
                            End If
                        End If
                    End If
                    
                    
                End If
            End If

        Next
        Using db As New Aurora.Common.Data.DatabaseTransaction
            Try
                Dim retvalue As String = Aurora.SalesAndMarketing.Data.DataRepository.InsertUpdateWebNonVehicleItems(Me.HidCountryValue.Value.TrimEnd, _
                                                               Me.hidBrandValue.Value.TrimEnd, _
                                                               Me.VehicleTypeDropDown.SelectedItem.Value, _
                                                               Me.UserCode, _
                                                               xmltemp.OuterXml, _
                                                               IIf(String.IsNullOrEmpty(Me.hidVehMgtType.Value), Me.VehMgtTypeDropDown.SelectedValue, Me.hidVehMgtType.Value))

                If retvalue.Contains("ERROR") Then Throw New Exception("Failed to save record")
                Me.SetInformationMessage(retvalue.Replace("OK:", "GEN046 - Updated Successfully"))
                db.CommitTransaction()
                ''db.RollbackTransaction()
            Catch ex As Exception
                Me.SetErrorMessage(ex.Message)
                db.RollbackTransaction()
            End Try
        End Using
    End Sub

    Sub WebBrandsFiltered(ByVal cty As String)
        Dim ds As DataSet = Data.DataRepository.Get_WebBrandsFiltered(cty, Me.UserCode)
        Me.brandDropDown.Items.Clear()
        Me.brandDropDown.Items.Add("(Select)")
        Me.brandDropDown.AppendDataBoundItems = True
        Me.brandDropDown.DataSource = ds.Tables(0).DefaultView
        Me.brandDropDown.DataTextField = "BrdName"
        Me.brandDropDown.DataValueField = "BrdCode"
        Me.brandDropDown.DataBind()
    End Sub

    Sub WebClassFiltered(ByVal cty As String, ByVal brdcode As String)
        Dim ds As DataSet = Data.DataRepository.Get_WebClassFiltered(cty, brdcode)
        Me.VehicleTypeDropDown.Items.Clear()
        Me.VehicleTypeDropDown.Items.Add("(Select)")
        Me.VehicleTypeDropDown.AppendDataBoundItems = True
        Me.VehicleTypeDropDown.DataSource = ds.Tables(0).DefaultView
        Me.VehicleTypeDropDown.DataTextField = "ClaDesc"
        Me.VehicleTypeDropDown.DataValueField = "ClaCode"
        Me.VehicleTypeDropDown.DataBind()
    End Sub
#End Region

#Region "private function"
    Function IsHiddenFieldValid() As Boolean
        Dim invalidvalue As String = "(Select)"
        Return Me.HidCountryValue.Value <> invalidvalue _
               AndAlso Me.hidBrandValue.Value <> invalidvalue _
               AndAlso Me.hidVehicleType.Value <> invalidvalue
        ''AndAlso Me.hidVehMgtType.Value <> invalidvalue
    End Function
#End Region

#Region "Controls Events"

    Protected Sub showDetailsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles showDetailsButton.Click
        UpdateHiddenfield()
        If Not IsHiddenFieldValid() Then
            Me.SetWarningShortMessage("Please enter valid search criteria!")
            ClearHiddenfield()
            Exit Sub
        End If

        PassParameterToControls()
        GetStoredData()
        If Me.SaveButton.Enabled = False Then Me.SaveButton.Enabled = True
    End Sub

    Protected Sub SaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        SaveData()
        Me.InsurancesUserControl1.ResetXMLs()
        Me.FerryItemsUserControl1.ResetXMLs()
        Me.ExtraHireItemsUserControl1.ResetXMLs()
        Me.CreditCardsUserControl1.ResetXMLs()
        GetStoredData(True)
    End Sub

    Protected Sub countryDropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles countryDropdown.SelectedIndexChanged
        If countryDropdown.SelectedValue Is Nothing Then Return
        WebBrandsFiltered(countryDropdown.SelectedValue)
        If FirstLoad Then Return

        Dim list As ListItem = brandDropDown.Items.FindByValue(Me.BrandSelected)
        If list IsNot Nothing Then
            brandDropDown.SelectedValue = Me.BrandSelected
        End If
        ''Me.SetInformationMessage("The search criteria has been changed. Please select  'Brand' and 'Vehicle Type' and click 'Show Details' button to refresh the content.")
        Me.SetInformationMessage(REFRESH_PAGE)
        Me.SaveButton.Enabled = False
    End Sub

    Protected Sub brandDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles brandDropDown.SelectedIndexChanged
        If countryDropdown.SelectedValue Is Nothing Then Return
        If brandDropDown.SelectedValue Is Nothing Then Return
        WebClassFiltered(countryDropdown.SelectedValue, brandDropDown.SelectedValue)
        Me.BrandSelected = brandDropDown.SelectedValue

        If FirstLoad Then Return
        Dim list As ListItem = VehicleTypeDropDown.Items.FindByValue(Me.VehicleTypeSelected)
        If list IsNot Nothing Then
            VehicleTypeDropDown.SelectedValue = Me.VehicleTypeSelected
        End If
        ''Me.SetInformationMessage("The search criteria has been changed. Please select 'Vehicle Type' and click 'Show Details' button to refresh the content.")
        Me.SetInformationMessage(REFRESH_PAGE)
        Me.SaveButton.Enabled = False
    End Sub

    Protected Sub VehicleTypeDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VehicleTypeDropDown.SelectedIndexChanged
        If countryDropdown.SelectedValue Is Nothing Then Return
        If brandDropDown.SelectedValue Is Nothing Then Return
        Me.VehicleTypeSelected = VehicleTypeDropDown.SelectedValue
        If FirstLoad Then Return

        Dim list As ListItem = VehicleTypeDropDown.Items.FindByValue(Me.VehicleTypeSelected)
        If list IsNot Nothing Then
            VehicleTypeDropDown.SelectedValue = Me.VehicleTypeSelected
        End If
        ''Me.SetInformationMessage("The search criteria has been changed. Please select  'Brand', 'Vehicle Type' and click 'Show Details' button to refresh the content.")
        Me.SetInformationMessage(REFRESH_PAGE)
        Me.SaveButton.Enabled = False
    End Sub

   
#End Region


#Region "mia:rev 17-Oct-2016 - Add new Category in WebNonVehicle for Relocation and General Category"
    ''Private Const REFRESH_PAGE As String = "The search criteria has been changed. Please select  'Country', 'Brand', 'Vehicle Type', 'Category' and click 'Show Details' button to refresh the content."
    Private Const REFRESH_PAGE As String = "The search criteria has been changed. Please click 'Show Details' button to refresh the content."
    Private Property VehicleMgtTypeSelected() As String
        Get
            Return ViewState("VehicleMgtTypeSelected")
        End Get
        Set(ByVal value As String)
            ViewState("VehicleMgtTypeSelected") = value
        End Set
    End Property

    Protected Sub VehMgtTypeDropDown_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles VehMgtTypeDropDown.SelectedIndexChanged
        UpdateHiddenfield()
        If countryDropdown.SelectedValue Is Nothing Then Return
        If brandDropDown.SelectedValue Is Nothing Then Return
        If VehicleTypeDropDown.SelectedValue Is Nothing Then Return
        Me.VehicleMgtTypeSelected = VehMgtTypeDropDown.SelectedValue

        If FirstLoad Then Return


        Dim list As ListItem = VehMgtTypeDropDown.Items.FindByValue(Me.VehicleMgtTypeSelected)
        If list IsNot Nothing Then
            VehMgtTypeDropDown.SelectedValue = Me.VehicleMgtTypeSelected
        End If
        Me.SetInformationMessage(REFRESH_PAGE)
        '' Me.SaveButton.Enabled = False
    End Sub
#End Region
    
End Class
