﻿//rev:mia Jan. 14 2013 - addition of a field to allow multiselection
//rev:mia Feb. 14 2013 - Valentine's day fixing

$(document).ready(function () {
    var panel = $('[id$=pnlAgentType]')
    panel.css('margin-top', 10)

    panel = $('[id$=PanelCategory]')
    panel.css('margin-top', 10)

    panel = $('[id$=pnlCountry]')
    panel.css('margin-top', 10).css('margin-left', 260);

    panel = $('[id$=pnlMarektCode]')
    panel.css('margin-top', 10).css('margin-left', 190);

    panel = $('[id$=pnlpackagebulk]')
    panel.css('margin-top', 10).css('margin-left', 470)


    //    $('[id$=_chkCountries]').find('span[title="Australia"]').css('color', 'blue').css('font-weight', 'bold')
    //    $('[id$=_chkCountries]').find('span[title="New Zealand"]').css('color', 'blue').css('font-weight', 'bold')

    var txtboxId = '';
    var currentItemID = ''

    $('.bulkdisplay input').click(function (e) {

        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();

        var text = $(this).parent().children('label').text()
        var objmode = (this.checked ? 'checked' : false)
        //txtboxId = $(this).parent().parent().parent().parent().parent().parent().parent().find("input[type='text']").attr("id")
        txtboxId = $(this).attr("id")

      
        if (txtboxId.indexOf('ctl00_ContentPlaceHolder_cblAgentType') > -1) {
            txtboxId = 'ctl00_ContentPlaceHolder_txtAgentType'
        }
        else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_chkpackagebulk') > -1) {
            txtboxId = 'ctl00_ContentPlaceHolder_txtpackagebulk'
        }
        else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_cblCategory') > -1) {
            txtboxId = 'ctl00_ContentPlaceHolder_txtCategory'
        }
        else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_chkMarketCode') > -1) {
            txtboxId = 'ctl00_ContentPlaceHolder_txtMarketcode'
        }
        else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_chkCountries') > -1) {
            txtboxId = 'ctl00_ContentPlaceHolder_txtCountry'
        }
       

        var ispackage = ((txtboxId.indexOf('ctl00_ContentPlaceHolder_txtpackagebulk') > -1) ? true : false)

        var alltext = ''
        var totalnumber = 0

        if (text == "(ALL)") {
            $(this).parent().parent().parent().parent().find('input[type="checkbox"]').attr('checked', objmode);
            alltext = text
        }
        else {
            if (ispackage) {
                currentItemID = $(this).parent().children('input[type="checkbox"]:').attr("id").replace("ctl00_ContentPlaceHolder_chkpackagebulk_", "")
            }
            $(this).parent().parent().parent().parent().find('input[type="checkbox"]:checked').each(function () {
                var text = $(this).parent().children('label').text()
                if (text != '(ALL)') {
                    alltext = alltext + text + ','

                    if (txtboxId.indexOf('ctl00_ContentPlaceHolder_txtpackagebulk') > -1) {
                        totalnumber = $(this).parent().parent().parent().parent().find('input[type="checkbox"]:checked').length
                        if (totalnumber > 7) {
                            setInformationShortMessage("Only seven packages is allowed for filtering.")
                            if (currentItemID == $(this).parent().children('input[type="checkbox"]').attr("id").replace("ctl00_ContentPlaceHolder_chkpackagebulk_", "")) {
                                $(this).attr('checked', false);
                                alltext = alltext.replace(text + ',', '')
                            }
                        }
                        else {
                            //alltext = alltext + text + ','
                            clearShortMessage()
                        }
                    }
                    else {
                        //alltext = alltext + text + ','
                        clearShortMessage()
                    }
                }
                else {
                    $(this).attr('checked', false);
                    clearShortMessage()
                }

            });


            var lastIndex = alltext.lastIndexOf(',')
            if (lastIndex != -1) {
                alltext = alltext.substring(0, lastIndex)
            }
        } //if (text == "(ALL)") 


        if (alltext.indexOf('(ALL)') != -1) alltext = '(ALL)'

        if (alltext == '') {
            alltext = txtboxId.replace('ctl00_ContentPlaceHolder_txt', '').toUpperCase()
            alltext = '[' + alltext + ']'

        }
        $('[id=' + txtboxId + ']').attr("value", alltext)

    });

    $('.bulkdisplaytext').blur(function () {
        var text = $(this).val()
        var txtboxId = $(this).attr("id")
        var checkboxlistId = ''
        if (text == '') {
            text = txtboxId.replace('ctl00_ContentPlaceHolder_txt', '').toUpperCase()
            text = '[' + text + ']'

            $('[id=' + txtboxId + ']').attr("value", text)

            if (txtboxId == 'ctl00_ContentPlaceHolder_txtCountry') {
                checkboxlistId = '_chkCountries'
            }
            else if (txtboxId == 'ctl00_ContentPlaceHolder_txtMarketcode') {
                checkboxlistId = '_chkMarketCode'
            }
            else if (txtboxId == 'ctl00_ContentPlaceHolder_txtCategory') {
                checkboxlistId = '_cblCategory'
            }
            else if (txtboxId == 'ctl00_ContentPlaceHolder_txtpackagebulk') {
                checkboxlistId = '_chkpackagebulk'
            }
            else if (txtboxId == 'ctl00_ContentPlaceHolder_txtAgentType') {
                checkboxlistId = '_cblAgentType'
            }

            $('[id$=' + checkboxlistId + '] input').each(function (e) {
                if (this.checked) {
                    this.checked = false
                }
            });
        }

    });

});

//end
    
    
