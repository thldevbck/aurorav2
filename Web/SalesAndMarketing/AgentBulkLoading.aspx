<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="AgentBulkLoading.aspx.vb" Inherits="Agent_AgentBulkLoading" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc3" %>
<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JS/AgentBulkLoadingJS.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
      

        function ToggleSearchResultVisibility(divSearchResultId) {
            var divSearchResult = document.getElementById(divSearchResultId);
            if (divSearchResult.style.display == "none")
                divSearchResult.style.display = "block";
            else
                divSearchResult.style.display = "none";
            return false;
        }

        function ToggleTableColumnCheckboxes(col, evt) {
            if (!evt) evt = window.event;
            if (!evt) return false;

            var chkAll = evt.srcElement ? evt.srcElement : evt.target;
            if (!chkAll) return false;

            var tbody = chkAll;
            while (tbody && (tbody.tagName != 'TBODY'))
                tbody = tbody.parentNode;
            if (!tbody) return false;

            var tableRows = tbody.getElementsByTagName("TR");
            for (var i = 0; i < tableRows.length; i++) {
                var tableCell = tableRows[i].getElementsByTagName("TD")[col];
                if (!tableCell) continue;

                var inputElements = tableCell.getElementsByTagName("INPUT");
                for (var j = 0; j < inputElements.length; j++) {
                    var inputElement = inputElements[j];
                    if (inputElement.type == 'checkbox')
                        inputElement.checked = chkAll.checked;
                }
            }

            return true;
        }

       
           
    </script>
    
</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
     
     <script  type="text/javascript">
         var pbControl = null;
         var prm = Sys.WebForms.PageRequestManager.getInstance();

         prm.add_beginRequest(BeginRequestHandler);
         prm.add_endRequest(EndRequestHandler);

         
         Sys.Application.add_load(loadhandler);
         function loadhandler() { }

         function BeginRequestHandler(sender, args) {
             pbControl = args.get_postBackElement();
         }

         function EndRequestHandler(sender, args) {
             var panel = $('[id$=pnlAgentType]')
             panel.css('margin-top', 10)

             panel = $('[id$=PanelCategory]')
             panel.css('margin-top', 10)//.

             panel = $('[id$=pnlCountry]')
             panel.css('margin-top', 10).css('margin-left', 260);

             panel = $('[id$=pnlMarektCode]')
             panel.css('margin-top', 10).css('margin-left', 190);

             panel = $('[id$=pnlpackagebulk]')
             panel.css('margin-top', 10).css('margin-left', 470)

//             $('[id$=_chkCountries]').find('span[title="Australia"]').css('color', 'blue').css('font-weight', 'bold')
//             $('[id$=_chkCountries]').find('span[title="New Zealand"]').css('color', 'blue').css('font-weight', 'bold')


             if (pbControl.id.indexOf('ctl00_ContentPlaceHolder_btn') > -1 || pbControl.id.indexOf('leftButton') > -1) {
    
                 var txtboxId = '';

                 $('.bulkdisplay input').click(function (e) {

                     if (!e) var e = window.event;
                     e.cancelBubble = true;
                     if (e.stopPropagation) e.stopPropagation();

                     var text = $(this).parent().children('label').text()
                     var objmode = (this.checked ? 'checked' : false)
                     //txtboxId = $(this).parent().parent().parent().parent().parent().parent().parent().find("input[type='text']").attr("id")
                     txtboxId = $(this).attr("id")


                     if (txtboxId.indexOf('ctl00_ContentPlaceHolder_cblAgentType') > -1) {
                         txtboxId = 'ctl00_ContentPlaceHolder_txtAgentType'
                     }
                     else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_chkpackagebulk') > -1) {
                         txtboxId = 'ctl00_ContentPlaceHolder_txtpackagebulk'
                     }
                     else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_cblCategory') > -1) {
                         txtboxId = 'ctl00_ContentPlaceHolder_txtCategory'
                     }
                     else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_chkMarketCode') > -1) {
                         txtboxId = 'ctl00_ContentPlaceHolder_txtMarketcode'
                     }
                     else if (txtboxId.indexOf('ctl00_ContentPlaceHolder_chkCountries') > -1) {
                         txtboxId = 'ctl00_ContentPlaceHolder_txtCountry'
                     }


                     var ispackage = ((txtboxId.indexOf('ctl00_ContentPlaceHolder_txtpackagebulk') > -1) ? true : false)

                     var alltext = ''
                     var totalnumber = 0

                     if (text == "(ALL)") {
                         $(this).parent().parent().parent().parent().find('input[type="checkbox"]').attr('checked', objmode);
                         alltext = text
                     }
                     else {
                         if (ispackage) {
                             currentItemID = $(this).parent().children('input[type="checkbox"]:').attr("id").replace("ctl00_ContentPlaceHolder_chkpackagebulk_", "")
                         }
                         $(this).parent().parent().parent().parent().find('input[type="checkbox"]:checked').each(function () {
                             var text = $(this).parent().children('label').text()
                             if (text != '(ALL)') {
                                 alltext = alltext + text + ','

                                 if (txtboxId.indexOf('ctl00_ContentPlaceHolder_txtpackagebulk') > -1) {
                                     totalnumber = $(this).parent().parent().parent().parent().find('input[type="checkbox"]:checked').length
                                     if (totalnumber > 7) {
                                         setInformationShortMessage("Only seven packages is allowed for filtering.")
                                         if (currentItemID == $(this).parent().children('input[type="checkbox"]').attr("id").replace("ctl00_ContentPlaceHolder_chkpackagebulk_", "")) {
                                             $(this).attr('checked', false);
                                             alltext = alltext.replace(text + ',', '')
                                         }
                                     }
                                     else {
                                         //alltext = alltext + text + ','
                                         clearShortMessage()
                                     }
                                 }
                                 else {
                                     //alltext = alltext + text + ','
                                     clearShortMessage()
                                 }
                             }
                             else {
                                 $(this).attr('checked', false);
                                 clearShortMessage()
                             }

                         });


                         var lastIndex = alltext.lastIndexOf(',')
                         if (lastIndex != -1) {
                             alltext = alltext.substring(0, lastIndex)
                         }
                     } //if (text == "(ALL)") 


                     if (alltext.indexOf('(ALL)') != -1) alltext = '(ALL)'

                     if (alltext == '') {
                         alltext = txtboxId.replace('ctl00_ContentPlaceHolder_txt', '').toUpperCase()
                         alltext = '[' + alltext + ']'

                     }
                     $('[id=' + txtboxId + ']').attr("value", alltext)

                 });


                 $('.bulkdisplaytext').blur(function () {
                     var text = $(this).val()
                     var txtboxId = $(this).attr("id")
                     var checkboxlistId = ''
                     if (text == '') {
                         text = txtboxId.replace('ctl00_ContentPlaceHolder_txt', '').toUpperCase()
                         text = '[' + text + ']'

                         $('[id=' + txtboxId + ']').attr("value", text)

                         if (txtboxId == 'ctl00_ContentPlaceHolder_txtCountry') {
                             checkboxlistId = '_chkCountries'
                         }
                         else if (txtboxId == 'ctl00_ContentPlaceHolder_txtMarketcode') {
                             checkboxlistId = '_chkMarketCode'
                         }
                         else if (txtboxId == 'ctl00_ContentPlaceHolder_txtCategory') {
                             checkboxlistId = '_cblCategory'
                         }
                         else if (txtboxId == 'ctl00_ContentPlaceHolder_txtpackagebulk') {
                             checkboxlistId = '_chkpackagebulk'
                         }
                         else if (txtboxId == 'ctl00_ContentPlaceHolder_txtAgentType') {
                             checkboxlistId = '_cblAgentType'
                         }

                         $('[id$=' + checkboxlistId + '] input').each(function (e) {
                             if (this.checked) {
                                 this.checked = false
                             }
                         });
                     }

                 });



             } //if (pbControl.id.indexOf('ctl00_ContentPlaceHolder_btn') > -1 || pbControl.id.indexOf('leftButton') > -1) {

         }

     </script>

    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <b>Identify Agent(s):</b>
            <uc1:CollapsiblePanel ID="cpnlAgentSelectionFilter" runat="server" Title="Agent Selection Filter"
                TargetControlID="pnlAgentSelectionFilter" />
            <asp:Panel ID="pnlAgentSelectionFilter" runat="Server">
                <table cellspacing="0" cellpadding="2" width="100%" border="0">
                    <tr>
                        <td width="150">
                            Country:
                        </td>
                        <td width="250">
                            
                            <%-- rev:mia Jan 17 2013
                            if AllowedMultipleSelection = false, the MaxReturnRecord will be default to 1
                            if AllowedMultipleSelection = true, the MaxReturnRecord will not be greater to   3
                            if AllowedMultipleSelection = true and MaxReturnRecord is empty , then  default is 2
                            
                            <uc1:PickerControl ID="pkrAgentCountry" 
                                               runat="server" 
                                               PopupType="AGENTCOUNTRYMULTIPLE"
                                               Width="200px" 
                                               AppendDescription="true"                                                 
                                               AllowedMultipleSelection="true"
                                               MaxReturnRecord ="2" />
                            --%>
                            <input type="text" runat="server"  value="[COUNTRY]" id="txtCountry" style="text-align: center; width:213px;"  class="bulkdisplaytext" />
                                        <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="DDECountry"  
                                                                      TargetControlID="txtCountry"   
                                                                      DropDownControlID="pnlCountry"  
                                                                       />  

                                        <asp:Panel ID="pnlCountry" 
                                                   runat="server" 
                                                   BorderColor="Gray" 
                                                   BackColor="White"
                                                   BorderWidth="1" style='visibility:hidden; display:none;' >

                                                <asp:CheckBoxList ID="chkCountries" runat="server" 
                                                                  RepeatColumns="4"
                                                                  RepeatDirection="Vertical" 
                                                                  RepeatLayout="table" 
                                                                  CellPadding = "1" 
                                                                  CellSpacing = "1"
                                                                  TextAlign ="right" 
                                                                  CssClass="bulkdisplay">

                                                    
                                                    
                                                </asp:CheckBoxList>

                                        </asp:Panel>
                        </td>
                        <td width="150">
                            Group:
                        </td>
                        <td>
                            <uc1:PickerControl ID="pkrAgentGroup" 
                                               runat="server" 
                                               PopupType="AGENTGROUP" 
                                               Width="190px"
                                               AllowedMultipleSelection="true"
                                               MaxReturnRecord ="2" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Market Code:
                        </td>
                        <td>
                            <%--<uc1:PickerControl ID="pkrMarketCode" 
                                               runat="server" 
                                               PopupType="MARKETCODE" Width="200px" 
                                               AllowedMultipleSelection="true"
                                               MaxReturnRecord ="2" />--%>
                          <input type="text" runat="server"  value="[MARKETCODE]" id="txtMarketcode" style="text-align: center; width:213px;" class="bulkdisplaytext"/>
                                        <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="ddemarketcode"  
                                                                      TargetControlID="txtMarketcode"   
                                                                      DropDownControlID="pnlMarektCode"  
                                                                       />  

                                        <asp:Panel ID="pnlMarektCode" 
                                                   runat="server" 
                                                   BorderColor="Gray" 
                                                   BackColor="White"
                                                   BorderWidth="1" style='visibility:hidden; display:none;' >

                                                <asp:CheckBoxList ID="chkMarketCode" runat="server" 
                                                                  RepeatColumns="4"
                                                                  RepeatDirection="Vertical" 
                                                                  RepeatLayout="table" 
                                                                  CellPadding = "1" 
                                                                  CellSpacing = "1"
                                                                  TextAlign ="right"
                                                                  CssClass="bulkdisplay">

                                                    
                                                    
                                                </asp:CheckBoxList>

                                        </asp:Panel>
                        </td>
                        <td>
                            Agent Type:
                        </td>
                        <td>
                            <%--<asp:DropDownList ID="cmbAgentType" 
                                              runat="server" 
                                              CssClass="Dropdown_Standard" 
                                              Width="215px"
                                                />--%>
                            <input type="text" runat="server"  value="[AGENTTYPE]" id="txtAgentType" style="text-align: center; width:213px;"  class="bulkdisplaytext"/>
                                        <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="DDE"  
                                                                      TargetControlID="txtAgentType"   
                                                                      DropDownControlID="pnlAgentType"  
                                                                       />  

                                        <asp:Panel ID="pnlAgentType" 
                                                   runat="server" 
                                                   BorderColor="Gray" 
                                                   BackColor="White"
                                                   BorderWidth="1" style='visibility:hidden; display:none; ' >

                                                <asp:CheckBoxList ID="cblAgentType" runat="server" 
                                                                  RepeatColumns="1" 
                                                                  RepeatDirection="Vertical" 
                                                                  RepeatLayout="table" 
                                                                  CellPadding = "3" 
                                                                  CellSpacing = "3"
                                                                  TextAlign ="right"
                                                                  CssClass="bulkdisplay">

                                                    
                                                    
                                                </asp:CheckBoxList>

                                        </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Category:
                        </td>
                        <td>
                            <%--<asp:DropDownList ID="cmbCategory" runat="server" CssClass="Dropdown_Standard" Width="224px" />--%>
                                        <input type="text" runat="server"  value="[CATEGORY]" id="txtCategory" style="text-align: center; width:213px;" class="bulkdisplaytext"/>
                                        <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="DDEcategory"  
                                                                      TargetControlID="txtCategory"   
                                                                      DropDownControlID="PanelCategory"  
                                                                       />  

                                        <asp:Panel ID="PanelCategory" 
                                                   runat="server" 
                                                   BorderColor="Gray" 
                                                   BackColor="White"
                                                   BorderWidth="1" style='visibility:hidden; display:none;' >

                                                <asp:CheckBoxList ID="cblCategory" runat="server" 
                                                                  RepeatColumns="1" 
                                                                  RepeatDirection="Vertical" 
                                                                  RepeatLayout="table" 
                                                                  CellPadding = "3" 
                                                                  CellSpacing = "3"
                                                                  TextAlign ="right"
                                                                  CssClass="bulkdisplay">

                                                    
                                                    
                                                </asp:CheckBoxList>

                                        </asp:Panel>
                        </td>
                        <td>
                             Agent:
                        </td>
                        <td>
                             <uc1:PickerControl 
                                        ID="PickerControlForAgentSelection" 
                                        runat="server" 
                                        EnableViewState="false"
                                        Width="190" 
                                        PopupType="AGENT" 
                                        AppendDescription="true" 
                                        AutoPostBack="false" 
                                        AllowedMultipleSelection="true"
                                        MaxReturnRecord ="2" />
                        
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Package:
                        </td>
                        <td>
                            
                             <%--<uc1:PickerControl 
                                                    ID="PickerControlForPackage" 
                                                    runat="server" 
                                                    Width="199" 
                                                    PopupType="PACKAGEBULK"
                                                    AppendDescription="TRUE" 
                                                    AutoPostBack="false"
                                                    AllowedMultipleSelection="false"
                                                    MaxReturnRecord ="1" />--%>

                                        <input type="text" runat="server"  value="[PACKAGE]" id="txtpackagebulk" style="text-align: center; width:213px;" class="bulkdisplaytext"/>
                                        <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="ddepackagebulk"  
                                                                      TargetControlID="txtpackagebulk"   
                                                                      DropDownControlID="pnlpackagebulk"  
                                                                       />  

                                        <asp:Panel ID="pnlpackagebulk" 
                                                   runat="server" 
                                                   BorderColor="Gray" 
                                                   BackColor="White"
                                                   BorderWidth="1" style='visibility:hidden; display:none; ' >

                                                <asp:CheckBoxList ID="chkpackagebulk" runat="server" 
                                                                  RepeatColumns="7" 
                                                                  RepeatDirection="Vertical" 
                                                                  RepeatLayout="table" 
                                                                  CellPadding = "2" 
                                                                  CellSpacing = "1"
                                                                  TextAlign ="right"
                                                                  CssClass="bulkdisplay">

                                                    
                                                    
                                                </asp:CheckBoxList>

                                        </asp:Panel>
                        </td>
                       
                        
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:Button ID="btnShowAvailableAgents" runat="server" Text="Show Available" CssClass="Button_Standard Button_Search" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <%--..................................................................................................................--%>
            <!--Filter Results-->
            <uc1:CollapsiblePanel ID="cpnlSearchResultAgentList" runat="server" Title="Agent Filter Results"
                TargetControlID="pnlSearchResultAgentList" />
            <asp:Panel ID="pnlSearchResultAgentList" runat="server">
                <asp:GridView ID="gwSearchResultAgentList" runat="server" AutoGenerateColumns="False"
                    Width="100%" CssClass="dataTableGrid">
                    <RowStyle CssClass="evenRow" />
                    <AlternatingRowStyle CssClass="oddRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Add">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkAgnIsSel" runat="server" Checked='<%# Bind("AgnIsSel") %>' />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllAgent" runat="server" />
                            </HeaderTemplate>
                            <HeaderStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="AgnId" HeaderText="AgnId" />
                        <asp:BoundField DataField="AgentName" HeaderText="Agent" />
                        <asp:BoundField DataField="BillingCountry" HeaderText="Billing Country" />
                        <asp:BoundField DataField="AgentType" HeaderText="Agent Type" />
                    </Columns>
                   
                </asp:GridView>
                <asp:Button ID="btnAddToSelectedAgents" runat="server" Text="Add To Selected" CssClass="Button_Standard Button_Add"
                    Style="float: right" />
            </asp:Panel>
            <!--Filter Results-->
            <%--..................................................................................................................--%>
            <!--Selected Agents-->
            <uc1:CollapsiblePanel ID="cpnlShowSelectedAgents" runat="server" Title="Agents Selected"
                TargetControlID="pnlShowSelectedAgents" />
            <asp:Panel ID="pnlShowSelectedAgents" runat="server">
                <center> <asp:Label runat="server" Text="" Visible="false" id="labelNumberOfAgent"/></center>
                <asp:GridView ID="gwSelectedAgents" runat="server" AutoGenerateColumns="False" Width="100%"
                    CssClass="dataTableGrid" BorderStyle="none">
                    <RowStyle CssClass="evenRow" />
                    <AlternatingRowStyle CssClass="oddRow" />
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelected" runat="server" />
                            </ItemTemplate>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAllAgent" runat="server" />
                            </HeaderTemplate>
                            <ItemStyle Width="20px" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="AgnId" HeaderText="AgnId" />
                        <asp:BoundField DataField="AgentName" HeaderText="Agent" />
                        <asp:BoundField DataField="BillingCountry" HeaderText="Billing Country" />
                        <asp:BoundField DataField="AgentType" HeaderText="Agent Type" />
                    </Columns>
                </asp:GridView>
                <table cellspacing="0" cellpadding="2" width="100%" border="0" style="visibility:hidden;">
                    <td align="right">
                        <asp:CheckBox ID="chkSelectAllAgents" runat="server" AutoPostBack="True" Text="All Agents" />
                    </td>
                </table>
            </asp:Panel>
            <!--Selected Agents-->
            <hr />
            <%----------------------------------------------------------------------------------------------------------------------%>
            <!-- Product Stuff-->
            <asp:Panel ID="productBulkLoadingPanel" runat="server">
                <b>Identify Product(s):</b>
                <!--Add Products-->
                <uc1:CollapsiblePanel ID="cpnlShowAddProducts" runat="server" Title="Add Products"
                    TargetControlID="pnlShowAddProducts" />
                <asp:Panel ID="pnlShowAddProducts" runat="server">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Product:
                            </td>
                            <td width="250">
                                <uc1:PickerControl ID="pkrProduct" PopupType="PRODUCT" runat="server" />
                            </td>
                            <td align="right">
                                <asp:Button ID="btnAddProductToList" Text="Add" runat="server" CssClass="Button_Standard Button_Add" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!--Add Products-->
                <%--..................................................................................................................--%>
                <!--Product Selection Filter-->
                <uc1:CollapsiblePanel ID="cpnlShowProductSelectionFilter" runat="server" Title="Product Selection Filter"
                    TargetControlID="pnlShowProductSelectionFilter" />
                <asp:Panel ID="pnlShowProductSelectionFilter" runat="server">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Brand:
                            </td>
                            <td width="250">
                                <asp:DropDownList ID="cmbBrand" runat="server" Width="200px" />
                            </td>
                            <td width="150">
                                Class:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbClass" runat="server" Width="200px" />
                            </td>
                        </tr>
                        <tr style="visibility:hidden">
                            <td>
                                Type:
                            </td>
                            <td colspan="3">
                                <uc1:PickerControl ID="pkrType" runat="server" PopupType="CLASSTYPEBULK" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button ID="btnShowAvailableProducts" runat="server" Text="Show Available" CssClass="Button_Standard Button_Search" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!--Product Selection Filter-->
                <%--..................................................................................................................--%>
                <!--Available Products-->
                <uc1:CollapsiblePanel ID="cpnlShowAvailableProducts" runat="server" Title="Products Selected"
                    TargetControlID="pnlShowAvailableProducts" />
                <asp:Panel ID="pnlShowAvailableProducts" runat="server">
                    <asp:GridView ID="gwAvailableProducts" runat="server" AutoGenerateColumns="false"
                        CssClass="dataTableGrid" Width="100%" BorderStyle="none">
                        <RowStyle CssClass="evenRow" />
                        <AlternatingRowStyle CssClass="oddRow" />
                        <HeaderStyle />
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelected" runat="server" Checked='<%# Bind("Check1") %>' />
                                </ItemTemplate>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAllProducts" runat="server" />
                                </HeaderTemplate>
                                <HeaderStyle Width="20px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="PrdId" HeaderText="PrdId" />
                            <asp:BoundField DataField="ProductName" HeaderText="Product" />
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <!--Available Products-->
                <%--..................................................................................................................--%>
                <!--Agent Product Discount-->
                <uc1:CollapsiblePanel ID="cpnlShowModelAgentDiscount" runat="server" Title="Agent Product Discount"
                    TargetControlID="pnlShowModelAgentDiscount" />
                <asp:Panel ID="pnlShowModelAgentDiscount" runat="server">
                    <table cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td width="150">
                                Set Disc% to same% as :
                            </td>
                            <td colspan="3">
                                <uc1:PickerControl ID="pkrOtherProduct" runat="server" PopupType="PRODUCT" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Model From :&nbsp;
                            </td>
                            <td colspan="3">
                                <asp:RadioButton ID="radCurrent" runat="server" Text="Current %" GroupName="rdgModelFrom"
                                    Checked="True" />
                                &nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="radLast" runat="server" Text="% of Last Effective Date" GroupName="rdgModelFrom" />
                                &nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="radLatest" runat="server" Text="Latest Added %" GroupName="rdgModelFrom" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                If model from product not on Agent:
                            </td>
                            <td colspan="3">
                                <asp:RadioButton ID="radDontCreate" runat="server" Text="Don't Create" GroupName="rdgCreate"
                                    Checked="True" />
                                &nbsp;&nbsp;&nbsp;
                                <asp:RadioButton ID="radCreateWithATP" runat="server" Text="Create with Agent Type Percentage"
                                    GroupName="rdgCreate" />
                            </td>
                        </tr>
                        <tr>
                            <%-- <td width="150">Booked From:</td>
                            <td width="250">
                                <uc1:DateControl ID="dtcBookedFrom" runat="server" />*
                            </td>
                            <td width="150">Travel From:</td>
                            <td>
                                <uc1:DateControl ID="dtcTravelFrom" runat="server" />*
                            </td>--%>
                            <td width="150">
                                Effective Date:
                            </td>
                            <td>
                                <uc1:DateControl ID="dtcEffectiveFrom" runat="server" />
                                *
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!--Agent Product Discount-->
                <%--..................................................................................................................--%>
            </asp:Panel>
            <!-- Product Stuff-->
            <%----------------------------------------------------------------------------------------------------------------------%>
            <!--Package Stuff-->
            <asp:Panel ID="packageBulkLoadingPanel" runat="server">
                <b>Identify Package(s):</b>
                <!--Identify Packages-->
                <uc1:CollapsiblePanel ID="cpnlIdentifyPackages" runat="server" Title="Add Packages"
                    TargetControlID="pnlIdentifyPackages" />
                <asp:Panel ID="pnlIdentifyPackages" runat="server">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Package:
                            </td>
                            <td>
                                <uc1:PickerControl ID="pkrPackage" PopupType="PACKAGEBULK" runat="server" Width="400"
                                    AppendDescription="true" />
                            </td>
                            <td align="right">
                                <asp:Button ID="btnAddPackage" Text="Add" runat="server" CssClass="Button_Standard Button_Add" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!--Identify Packages-->
                <%--..................................................................................................................--%>
                <!--Package Selection Filter-->
                <uc1:CollapsiblePanel ID="cpnlPackageSelectionFilter" runat="server" Title="Package Selection Filter"
                    TargetControlID="pnlPackageSelectionFilter" />
                <asp:Panel ID="pnlPackageSelectionFilter" runat="server">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Country:
                            </td>
                            <td width="250">
                                <asp:DropDownList ID="cmbPackageCountry" runat="server" Width="224px" />
                            </td>
                            <td width="150">
                                Brand:
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbPackageBrand" runat="server" Width="224px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Package Type:
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="cmbPackageType" runat="server" Width="224px" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Travel From:
                            </td>
                            <td>
                                <uc1:DateControl ID="dtcPackageTravelFrom" runat="server" />
                            </td>
                            <td>
                                To:
                            </td>
                            <td>
                                <uc1:DateControl ID="dtcPackageTravelTo" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Booking From:
                            </td>
                            <td>
                                <uc1:DateControl ID="dtcPackageBookingFrom" runat="server" />
                            </td>
                            <td>
                                To:
                            </td>
                            <td>
                                <uc1:DateControl ID="dtcPackageBookingTo" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Agent:
                            </td>
                            <td>
                                <uc1:PickerControl 
                                        ID="PickerControlAgentPackageSelection" 
                                        runat="server" 
                                        EnableViewState="false"
                                        Width="199" 
                                        PopupType="AGENT" 
                                        AppendDescription="true" 
                                        AutoPostBack="false"  />
                        </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button ID="btnShowAvailablePackages" runat="server" Text="Show Available" CssClass="Button_Standard Button_Search" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <!--Package Selection Filter-->
                <%--..................................................................................................................--%>
                <!--Filter Results-->
                <uc1:CollapsiblePanel ID="cpnlFilterPackageResults" runat="server" Title="Packages Selected"
                    TargetControlID="pnlFilterPackageResults" />
                <asp:Panel ID="pnlFilterPackageResults" runat="server">
                    <asp:Repeater ID="rptPackageSearchResults" runat="server">
                        <HeaderTemplate>
                            <table width="100%" border="0" cellpadding="2" cellspacing="0" class="dataTableGrid">
                                <tr>
                                    <th width="20" rowspan="2" valign="baseline">
                                        <input type="checkbox" id="chkSelectAllPackages" onclick="return ToggleTableColumnCheckboxes(0, event);" />
                                    </th>
                                    <th rowspan="2" valign="baseline">
                                        Package
                                    </th>
                                    <th colspan="2" style="text-align: center;">
                                        Booked
                                    </th>
                                    <th colspan="2" style="text-align: center;">
                                        Travel
                                    </th>
                                </tr>
                                <tr>
                                    <th style="width: 100px; text-align: center;">
                                        From
                                    </th>
                                    <th style="width: 100px; text-align: center;">
                                        To
                                    </th>
                                    <th style="width: 100px; text-align: center;">
                                        From
                                    </th>
                                    <th style="width: 100px; text-align: center;">
                                        To
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="evenRow">
                                <td>
                                    <asp:CheckBox ID="chkSelected" runat="server" Checked='<%#eval("Remove") %>' PackageID='<%#Container.DataItem("PkgId")%>' />
                                </td>
                                <td>
                                    <%#Container.DataItem("PkgDesc")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("BkFr")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("BkTo")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("TrFr")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("TrTo")%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="oddRow">
                                <td style="text-align: center">
                                    <asp:CheckBox ID="chkSelected" runat="server" Checked='<%#eval("Remove") %>' PackageID='<%#Container.DataItem("PkgId")%>' />
                                </td>
                                <td>
                                    <%#Container.DataItem("PkgDesc")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("BkFr")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("BkTo")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("TrFr")%>
                                </td>
                                <td style="text-align: center">
                                    <%#Container.DataItem("TrTo")%>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
                <!--Filter Results-->
            </asp:Panel>
            <%----------------------------------------------------------------------------------------------------------------------%>
            <br />
            <p style="float: right">
                <asp:Button ID="btnBulkAssign" runat="server" Text="Bulk Assign" CssClass="Button_Standard Button_Add" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </p>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updatePanelShowAvailability" runat="server">
        <ContentTemplate>
            <uc3:ConfirmationBoxControl ID="ConfirmationBoxControl_ShowAvailable" runat="server"
                Title="Agent Package Bulk Loading Availability" Text="" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
