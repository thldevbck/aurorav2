Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data
Imports System.Web.Script.Serialization

Imports Aurora.Common

<AuroraPageTitleAttribute("Currency Exchange")> _
<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class Include_CurrencyExchange
    Inherits AuroraPage

    Private _currentCurrencyRatesTable As DataTable

    Public Class CurrencyRate
        Public fromCodCode As String
        Public fromCodDesc As String
        Public toCodCode As String
        Public toCodDesc As String
        Public rateOfExchange As Decimal
    End Class

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        _currentCurrencyRatesTable = Aurora.Common.Data.GetCurrentCurrencyRates()

        Dim selectedTo As String = toDropDown.SelectedValue
        Dim selectedFrom As String = fromDropDown.SelectedValue

        Dim currencyRatesList As New List(Of CurrencyRate)
        For Each dataRow As DataRow In _currentCurrencyRatesTable.Rows
            Dim currencyRate As New CurrencyRate()
            currencyRate.fromCodCode = dataRow("BaseCodCode").ToString()
            currencyRate.fromCodDesc = dataRow("BaseCodDesc").ToString()
            currencyRate.toCodCode = dataRow("TransCodCode").ToString()
            currencyRate.toCodDesc = dataRow("TransCodDesc").ToString()
            currencyRate.rateOfExchange = CType(dataRow("RateOfExchange"), Decimal)

            currencyRatesList.Add(currencyRate)
        Next

        toDropDown.Items.Clear()
        For Each currencyRate As CurrencyRate In currencyRatesList
            If toDropDown.Items.FindByValue(currencyRate.toCodCode) Is Nothing Then
                Dim li As New ListItem
                li.Text = currencyRate.toCodCode & " - " & currencyRate.toCodDesc
                li.Value = currencyRate.toCodCode
                toDropDown.Items.Add(li)
            End If
        Next

        If Not Me.IsPostBack _
         AndAlso Not String.IsNullOrEmpty(Me.Request.QueryString("currency")) _
         AndAlso toDropDown.Items.FindByValue(Me.Request.QueryString("currency")) IsNot Nothing Then
            toDropDown.SelectedValue = Me.Request.QueryString("currency")
        ElseIf Me.IsPostBack AndAlso toDropDown.Items.FindByValue(selectedTo) IsNot Nothing Then
            toDropDown.SelectedValue = selectedTo
        ElseIf toDropDown.Items.FindByValue(UserSettings.Current.CtyDefCurrCode) IsNot Nothing Then
            toDropDown.SelectedValue = UserSettings.Current.CtyDefCurrCode
        Else
            toDropDown.SelectedIndex = 0
        End If
        toImage.ImageUrl = "~\Images\Currency\" + toDropDown.SelectedValue + ".gif"
        rateFromTableHeader.InnerHtml = "Rate from<br/>(1 " + toDropDown.SelectedValue + ")"
        rateToTableHeader.InnerHtml = "Rate to<br/>(in " + toDropDown.SelectedValue + ")"

        fromDropDown.Items.Clear()
        rateTable.Rows.Clear()

        For Each currencyRate As CurrencyRate In currencyRatesList
            If toDropDown.SelectedValue = currencyRate.toCodCode Then
                Dim li As New ListItem
                li.Text = currencyRate.fromCodCode & " - " & currencyRate.fromCodDesc
                li.Value = currencyRate.fromCodCode
                fromDropDown.Items.Add(li)
            End If
        Next

        If fromDropDown.Items.FindByValue(selectedFrom) IsNot Nothing Then
            fromDropDown.SelectedValue = selectedFrom
        Else
            fromDropDown.SelectedIndex = 0
        End If
        fromImage.ImageUrl = "~\Images\Currency\" & fromDropDown.SelectedValue & ".gif"

        'fromAmountLabel.Text = ""
        For Each currencyRate As CurrencyRate In currencyRatesList
            If toDropDown.SelectedValue = currencyRate.toCodCode AndAlso fromDropDown.SelectedValue = currencyRate.fromCodCode Then
                'fromAmountLabel.Text = currencyRate.rateOfExchange.ToString("0.00")
                'toAmountLabel.Text = (1.0 / currencyRate.rateOfExchange).ToString("0.00")
                rateOfExchangeHiddenField.Value = currencyRate.rateOfExchange.ToString()
            End If
        Next

        Dim count As Integer = 0
        For Each currencyRate As CurrencyRate In currencyRatesList
            If toDropDown.SelectedValue = currencyRate.toCodCode Then
                Dim tableRow As New TableRow()
                If currencyRate.fromCodCode = fromDropDown.SelectedValue Then
                    tableRow.Attributes.Add("class", "selectedRow")
                Else
                    tableRow.Attributes.Add("class", IIf(count Mod 2 = 0, "evenRow", "oddRow"))
                End If
                tableRow.Attributes.Add("style", "cursor: pointer")
                rateTable.Rows.Add(tableRow)

                tableRow.Attributes.Add("onclick", "document.getElementById ('" & fromDropDown.ClientID & "').selectedIndex=" & count & "; setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder$fromDropDown\',\'\')', 0)")
                tableRow.Attributes.Add("onmouseover", "currency_onmouseover(this);")
                tableRow.Attributes.Add("onmouseout", "currency_onmouseout(this);")

                Dim tableCell As TableCell

                tableCell = New TableCell()
                tableCell.Attributes.Add("style", "width: 60px")
                tableRow.Controls.Add(tableCell)

                Dim img As New System.Web.UI.WebControls.Image()
                img.ImageUrl = "~\Images\Currency\" + currencyRate.fromCodCode + ".gif"
                tableCell.Controls.Add(img)

                tableCell = New TableCell()
                tableCell.Text = Server.HtmlEncode(currencyRate.fromCodCode & " - " & currencyRate.fromCodDesc)
                tableRow.Controls.Add(tableCell)

                tableCell = New TableCell()
                tableCell.Attributes.Add("style", "width: 70px; text-align: right")
                tableCell.Text = Server.HtmlEncode((1.0 / currencyRate.rateOfExchange).ToString("0.00"))
                tableRow.Controls.Add(tableCell)

                tableCell = New TableCell()
                tableCell.Attributes.Add("style", "width: 70px; text-align: right")
                tableCell.Text = Server.HtmlEncode(currencyRate.rateOfExchange.ToString("0.00"))
                tableRow.Controls.Add(tableCell)

                count += 1
            End If
        Next
    End Sub
End Class
