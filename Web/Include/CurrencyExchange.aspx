<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CurrencyExchange.aspx.vb" Inherits="Include_CurrencyExchange" MasterPageFile="~/Include/PopupHeader.master" validateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
    <script type="text/javascript">
var rateOfExchangeHiddenFieldId = '<%= Me.rateOfExchangeHiddenField.ClientID %>';
var rateOfExchangeHiddenField;

var lastChangedHiddenFieldId = '<%= Me.lastChangedHiddenField.ClientID %>';
var lastChangedHiddenField;
    
var fromAmountTextBoxId = '<%= Me.fromAmountTextBox.ClientID %>';
var fromAmountTextBox;

var toAmountTextBoxId = '<%= Me.toAmountTextBox.ClientID %>';
var toAmountTextBox;

function currency_onmouseover(row)
{
    row.style.backgroundColor='#ADF';
}

function currency_onmouseout(row)
{
    row.style.backgroundColor='';
}

function currencyRefreshToCalculation()
{
    var rateOfExchange = parseFloat (rateOfExchangeHiddenField.value);
    var fromAmount = parseFloat (fromAmountTextBox.value);
    var regEx = new RegExp ("^\d+(\.\d+)?$");
    var isValidFromAmount = !isNaN (fromAmount);
    
    if (!isValidFromAmount)
    {
        fromAmountTextBox.style.textDecoration = 'underline';
        fromAmountTextBox.style.color = '#F00';
        MessagePanelControl.setMessagePanelWarning (null, "Specify a valid amount");
    }
    else
    {
        fromAmountTextBox.style.textDecoration = 'none';
        fromAmountTextBox.style.color = '#000';
        MessagePanelControl.clearMessagePanel (null);
    }
    
    if (!isValidFromAmount)
        toAmountTextBox.value = "";
    else
        toAmountTextBox.value = (fromAmount * rateOfExchange).toFixed(2);
        
    lastChangedHiddenField.value = "from";
}

function currencyRefreshFromCalculation()
{
    var rateOfExchange = parseFloat (rateOfExchangeHiddenField.value);
    var toAmount = parseFloat (toAmountTextBox.value);
    var regEx = new RegExp ("^\d+(\.\d+)?$");
    var isValidToAmount = !isNaN (toAmount);
    
    if (!isValidToAmount)
    {
        toAmountTextBox.style.textDecoration = 'underline';
        toAmountTextBox.style.color = '#F00';
        MessagePanelControl.setMessagePanelWarning (null, "Specify a valid amount");
    }
    else
    {
        toAmountTextBox.style.textDecoration = 'none';
        toAmountTextBox.style.color = '#000';
        MessagePanelControl.clearMessagePanel (null);
    }
    
    if (!isValidToAmount)
        fromAmountTextBox.value = "";
    else
        fromAmountTextBox.value = (toAmount / rateOfExchange).toFixed(2);

    lastChangedHiddenField.value = "to";
}

function currencyLoad()
{
    rateOfExchangeHiddenField = document.getElementById (rateOfExchangeHiddenFieldId);
    lastChangedHiddenField = document.getElementById (lastChangedHiddenFieldId);
    fromAmountTextBox = document.getElementById (fromAmountTextBoxId);
    toAmountTextBox = document.getElementById (toAmountTextBoxId);
    
    addEvent (fromAmountTextBox, "change", currencyRefreshToCalculation);
    addEvent (fromAmountTextBox, "keyup", currencyRefreshToCalculation);
    addEvent (toAmountTextBox, "change", currencyRefreshFromCalculation);
    addEvent (toAmountTextBox, "keyup", currencyRefreshFromCalculation);
    
    if (lastChangedHiddenField.value == "to")
    {
        currencyRefreshFromCalculation();
        fromAmountTextBox.focus();
    }
    else
    {
        currencyRefreshToCalculation();
        fromAmountTextBox.focus();
    }
}

addEvent (window, "load", currencyLoad);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(currencyLoad);
    </script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:UpdatePanel ID="messageUpdatePanel" runat="server" RenderMode="Inline">
        <ContentTemplate>
            <asp:HiddenField ID="rateOfExchangeHiddenField" runat="server" Value="1.0" />
            <asp:HiddenField ID="lastChangedHiddenField" runat="server" Value="from" />
            
            <div> <!-- style="min-height:350px; height:auto !important; height:350px;" -->
            
            <table cellpadding="2" cellspacing="0" class="dataTable" style="width: 420px;">
                <tr>
                    <th colspan="3">Base Currency:</th>
                    <th style="text-align:right ">
                        <img src="../images/exchangeIcon.gif" width="16" height="16" alt="" align="top" />
                    </th>
                </tr>
                <tr>
                    <td style="width: 60px">                        
                        <asp:Image  
                            ID="toImage"
                            runat="server" 
                            Height="27" 
                            Width="53" 
                            ImageUrl="~/Images/Currency/none.gif" 
                            ImageAlign="AbsMiddle" />
                    </td>
                    <td>                        
                        <asp:DropDownList 
                            ID="toDropDown"
                            runat="server"
                            Width="200px"
                            AutoPostBack="true" />
                    </td>
                    <td colspan="2" align="right">
<%--
                        <asp:Label runat="server" Font-Size="10px" Font-Italic="true" Text="x " />
                        <asp:Label
                            ID="toAmountLabel"
                            runat="server" />
                        <asp:Label runat="server" Font-Size="10px" Font-Italic="true" Text="= " />
--%>
                        <asp:TextBox 
                            ID="toAmountTextBox" 
                            runat="server" 
                            Width="65px" 
                            Text="0" 
                            Style="text-align: right" />
                    </td>
                </tr>
                <tr>
                    <th colspan="2">Conversion Rates:</th>
                    <th style="width: 70px; font-weight: normal; text-align: right; font-size: 10px;" runat="server" id="rateFromTableHeader">Rate from</th>
                    <th style="width: 70px; font-weight: normal; text-align: right; font-size: 10px;" runat="server" id="rateToTableHeader">Rate to</th>
                </tr>
                <tr>
                    <td colspan="4" style="padding: 0px; border-top-width: 1px">
                        <asp:Table ID="rateTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableInner" Style="width: 100%">
                        </asp:Table>
                    </td>
                </tr>
                <tr>
                    <th colspan="3">Calculator:</th>
                    <th style="text-align:right ">
                        <img src="../images/calc.gif" width="16" height="16" alt="" align="top" />
                    </th>
                </tr>
                <tr>
                    <td>                        
                        <asp:Image  
                            ID="fromImage"
                            runat="server" 
                            Height="27" 
                            Width="53" 
                            ImageUrl="~/Images/Currency/none.gif" 
                            ImageAlign="AbsMiddle" />
                    </td>
                    <td>
                        <asp:DropDownList 
                            ID="fromDropDown"
                            runat="server"
                            Width="200px" 
                            AutoPostBack="true" />
                    </td>
                    <td colspan="2" align="right">
<%--
                        <asp:Label runat="server" Font-Size="10px" Font-Italic="true" Text="x " />
                        <asp:Label
                            ID="fromAmountLabel"
                            runat="server" />
                        <asp:Label runat="server" Font-Size="10px" Font-Italic="true" Text="= " />
--%>                    
                        <asp:TextBox 
                            ID="fromAmountTextBox" 
                            runat="server" 
                            Width="65px" 
                            Text="0.00" 
                            Style="text-align: right" />
                    </td>
                </tr>
            </table>
            
            </div>
            
            <div style="width: 420px; margin-top: 5px">
                <input type="button" id="closeButton" value="Close" class="Button_Standard Button_Close" onclick="window.close(); return false;" style="float: right" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>