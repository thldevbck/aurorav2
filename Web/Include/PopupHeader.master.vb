Imports System.Data
Imports System.Configuration
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO

Imports Aurora.Common

Partial Class PopupHeader
    Inherits AuroraMasterPage

    Protected AuroraPage As AuroraPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.AuroraPage = CType(Me.Page, AuroraPage)

        Me.title.Text = Server.HtmlEncode(AuroraPage.PageTitle)
        Me.titleLabel.Text = Server.HtmlEncode(AuroraPage.PageTitle)

        If Me.AuroraPage.HasHelp Then
            Me.helpButton.Attributes.Add("onclick", "openHelp(" & New JavaScriptSerializer().Serialize(Me.AuroraPage.HelpUrl) & ")")
            Me.helpButton.Src = "~/images/helpButton.gif"
            Me.helpButton.Alt = "Help"
            Me.helpButton.Attributes.Add("title", "Help")
            Me.helpButton.Style.Remove("display")
        Else
            Me.helpButton.Attributes.Remove("onclick")
            Me.helpButton.Src = "~/images/helpDisabledButton.gif"
            Me.helpButton.Alt = ""
            Me.helpButton.Attributes.Remove("title")
            Me.helpButton.Style.Add("display", "none")
        End If

        systemInfo.Text = Server.HtmlEncode(AuroraPage.PageTitle _
            & " - " & UserSettings.Current.SystemIdentifier & " - " & Me.AuroraPage.FunctionCode _
            & IIf(Me.AuroraPage.MenuFunctionCode <> Me.AuroraPage.FunctionCode, "/" & Me.AuroraPage.MenuFunctionCode, ""))

        Me.Page.ClientScript.RegisterClientScriptInclude("AuroraHeader", Me.ResolveUrl("~/Include/AuroraHeader.js"))
        Me.Page.ClientScript.RegisterClientScriptInclude("Date", Me.ResolveUrl("~/Include/Date.js"))
        Me.Page.ClientScript.RegisterClientScriptInclude("Validation", Me.ResolveUrl("~/Include/Validation.js"))
    End Sub

    Public Overrides Sub ClearMessages()
        messagePanel.ClearMessagePanel()
    End Sub

    Public Overrides Sub AddMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        messagePanel.AddMessagePanel(messageType, text)
    End Sub

    Public Overrides Sub ClearShortMessage()
        shortMessagePanel.ClearMessagePanel()
    End Sub

    Public Overrides Sub SetShortMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        shortMessagePanel.SetMessagePanel(messageType, text)
    End Sub
End Class

