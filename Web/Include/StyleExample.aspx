<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StyleExample.aspx.vb" Inherits="Include_StyleExample" MasterPageFile="~/Include/AuroraHeader.master" validateRequest="false" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\ConfirmationBox\ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\SupervisorOverrideControl\SupervisorOverrideControl.ascx" TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\CollapsiblePanel\CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>


<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
blockquote
{
    background:url(../images/A-cap.gif) no-repeat;
    background-position: top left;
    background-color: #F6F6FF;
    border: solid 1px #CCF;
    padding-left: 45px;
    padding-top: 5px;
    margin-left: 0px;
    background-color: #F6F6FF;
    min-height:45px; 
    height:auto !important; 
    height:45px;
} 

blockquote.techie
{
    background:url(../images/hacker.gif) no-repeat;
    background-position: top left;
    background-color: #F6F6FF;
}

    </style>
</asp:Content>


<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <div style="width: 780px;">

    <ul>
        <li><a href="#WebBrowser">Web Browser</a></li>
        <li><a href="#FormElements">Form Elements</a></li>
        <li><a href="#StatusColors">Status Colors</a></li>
        <li><a href="#DataTables">Data Tables</a></li>
        <li><a href="#Forms">Forms</a></li>
    </ul>

    <!------------------------------------------------------------------------------------>
    <a name="WebBrowser"></a><h2>Web Browser</h2>

    <blockquote><p>The standard web browser for Aurora is Internet Explorer 6, and the styles should work optimally in that.</p></blockquote>  
    
    <blockquote class="techie"><p>If your using standard HTML/CSS, and take some care, your forms
    should work in FireFox/Safari/Opera too.  This is not a critical requirement, but it "future-proofs" 
    the code (future versions of IE will be less tolerant of non-standard html), and Aurora or your forms
    may not always be used just internally in THL.</p></blockquote>

    <blockquote class="techie"><p>Important....  Do *not* add styles to the global stylesheet unless you 
    are certain that it is needed by everyone, and it is approved.  The global stylesheet defines the 
    style standard documented below and adding to it, means you need a feature not already described.</p>
    <p>This document only uses all the classes described (very few are needed), and it only uses custom styles for 
    layout (width, height, float etc) or for status colors.
    </p></blockquote>

    <!------------------------------------------------------------------------------------>
    <a name="FormElements"></a><h2>Form Elements</h2>
    
    <h3>Standard Html Elements</h3>
    
    <blockquote><p>The standard elements used in a form.</p></blockquote>
    
    <ul>
        <li>Labels: 
            <ul>
                <li>Label 1</li>
                <li>Label 2</li>
                <li>Label 3</li>
            </ul>
         </li>
        <li>Text Box: <br />
            <asp:TextBox runat="server" /></li>
        <li>Drop Down List: <br />
            <asp:DropDownList runat="server">
                <asp:ListItem>Option 1</asp:ListItem>
                <asp:ListItem>Option 2</asp:ListItem>
                <asp:ListItem>Option 3</asp:ListItem>
            </asp:DropDownList>
        </li>
        <li>Check Box:  <br />
            <asp:CheckBox runat="server" Text="Text" />
        </li>
        <li>Check Box List:  <br />
            <asp:CheckBoxList runat="server" RepeatLayout="Table" RepeatColumns="3">
                <asp:ListItem>Option 1</asp:ListItem>
                <asp:ListItem>Option 2</asp:ListItem>
                <asp:ListItem>Option 3</asp:ListItem>
            </asp:CheckBoxList>
        </li>
        <li>Radio Button List:  <br />
            <asp:RadioButtonList runat="server" RepeatLayout="Table" RepeatColumns="3">
                <asp:ListItem>Option 1</asp:ListItem>
                <asp:ListItem>Option 2</asp:ListItem>
                <asp:ListItem>Option 3</asp:ListItem>
            </asp:RadioButtonList>
        </li>
        <li>Button:  <br />
            <asp:Button runat="server" Text="Small" CssClass="Button_Small" />
            <asp:Button runat="server" Text="Medium" CssClass="Button_Medium" />
            <asp:Button runat="server" Text="Standard" CssClass="Button_Standard" />
        </li>
        <li>Date Control: <br />
            <uc1:DateControl runat="server" />
        </li>
        <li>Time Control: <br />
            <uc1:TimeControl runat="server" />
        </li>
        <li>Picker Control: <br />
            <uc1:PickerControl runat="server" PopupType="LOCATION" Param2="NZ" AppendDescription="true" />
        </li>
        <li>&quot;RegEx Text Box&quot; Control: <br />
            Email: <uc1:RegExTextBox runat="server" RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" ErrorMessage="Enter a valid email" Text="joe@win.com" />
        </li>
        <li>Readonly/Disabled:<br />
            <asp:Button runat="server" Enabled="false" Text="Button" CssClass="Button_Standard" />
            <asp:TextBox runat="server" ReadOnly="true" />
            <asp:DropDownList runat="server" Enabled="false">
                <asp:ListItem>Option 1</asp:ListItem>
                <asp:ListItem>Option 2</asp:ListItem>
                <asp:ListItem>Option 3</asp:ListItem>
            </asp:DropDownList>
            <asp:CheckBoxList runat="server" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                <asp:ListItem>Option 1</asp:ListItem>
                <asp:ListItem Selected="true">Option 2</asp:ListItem>
                <asp:ListItem>Option 3</asp:ListItem>
            </asp:CheckBoxList>
            <asp:RadioButtonList runat="server" RepeatLayout="Table" RepeatColumns="3" Enabled="false">
                <asp:ListItem Selected="true">Option 1</asp:ListItem>
                <asp:ListItem>Option 2</asp:ListItem>
                <asp:ListItem>Option 3</asp:ListItem>
            </asp:RadioButtonList>
        </li>
    </ul>

    <blockquote><p>The width, and size of these controls is determined requirements of the data.  Generally, size's of all inputs should follow multiple's of 50 pixels.  The standard size of a textbox containing a textual data should be 200px.</p></blockquote>
    <blockquote class="techie">
        <ul>
            <li>Button CSS classes are:  <b>Button_Small</b>, <b>Button_Medium</b>, <b>Button_Standard</b> (use this by default)</li>
            <li>A CSS class that can be used to &quot;pad&quot; out radio/checkbox lists is <b>repeatLabelTable</b>.</li>
            <li>A CSS class for a &quot;standard size&quot; textbox is <b>TextBox_Standard</b>.</li>
            <li>Except for the above, do not use CSS classes with these controls.</li>
            <li>The &quot;readonly&quot; and &quot;disabled&quot; CSS classes are automatically added to the form element
            by a javascript when the page loads based on the "readonly" or "disabled" attribute.  You should never have to 
            declare this CSS Style server side.</li>
        </ul>
    </blockquote>


    <h3>Messages</h3>

    <uc1:MessagePanelControl ID="MessagePanelControl1" RunAt="server" Text="Information Message" MessageType="Information" CssClass="messagePanel" />
    <uc1:MessagePanelControl ID="MessagePanelControl2" RunAt="server" Text="Warning Message" MessageType="Warning" CssClass="messagePanel" />
    <uc1:MessagePanelControl ID="MessagePanelControl3" RunAt="server" Text="Error Message" MessageType="Error" CssClass="messagePanel" />

    <blockquote>
        <p>In Olso we have three different types of messages:</p>
        <ul>
            <li>Information Message - Should generally be used to tell the user something has happened successfully e.g. a successful update to the database.</li>
            <li>Warning Message - Should generally be used to tell the user something we detected something is wrong e.g. validation problems, duplicate data etc.</li>
            <li>Error Message - Something really bad has happened, that we don�t know about and should never happen e.g. database or system error.</li>
        </ul>

        <p>Furthermore, they can be shown in different ways:</p>
        
        <ul>
            <li>A big message - These apear just below the header.  These should only be shown on "big" events, like on a form load or submit.  </li>
            <li>A short message - These appear inside the header.  These are very short messages and should only be shown on "little" events, typically in response to something that effects a single bit of information e.g. a field validation message</li>
            <li>A popup message - These are messages that apear in the popup dialogs, and the same rule applies to them as the short messages.</li>
        </ul>
    </blockquote>

    <blockquote class="techie">
        <p>Standard methods exist on the base page to set and add messages to the top of the page.  Custom Controls
        like the Date Control will display minor messages in the template.  They can be told to display their
        message in a custom MessagePanelControl by specifying the MessagePanelControlID property on them.
        </p>
    </blockquote>

    <!------------------------------------------------------------------------------------>
    <a name="StatusColors"></a><h2>Status Colors</h2>

    <blockquote><p>Standard colors used to indicate the status of items or data.</p></blockquote>

    <h3>Active/Inactive</h3>

    <div style="height:60px; width:200px; background-color: #DDFFDD; text-align: center; vertical-align: middle; float: left">
        <b>Active</b><br />
        <i>#DDFFDD</i><br/>
        Items that are active
    </div>

    <div style="height:60px; width:200px; background-color: #DDDDDD; text-align: center; vertical-align: middle; float: left">
        <b>Inactive</b><br />
        <i>#DDDDDD</i><br/>
        Items that are inactive
    </div>

    <div style="height:60px; width:200px; background-color: #FFDDDD; text-align: center; vertical-align: middle; float: left">
        <b>Error</b><br />
        <i>#FFDDDD</i><br/>
        Items that are in error
    </div>
    
    <div style="clear: both" /><br />


    <h3>Time based</h3>
    
    <div style="height:60px; width:200px; background-color: #DDFFDD; text-align: center; vertical-align: middle; float: left">
        <b>Current</b><br />
        <i>#DDFFDD</i><br/>
        Items that are current and active
    </div>

    <div style="height:60px; width:200px; background-color: #DDDDFF; text-align: center; vertical-align: middle; float: left">
        <b>Past</b><br />
        <i>#DDDDFF</i><br/>
        Items that are active but have expired
    </div>

    <div style="height:60px; width:200px; background-color: #FFFFDD; text-align: center; vertical-align: middle; float: left">
        <b>Future</b><br />
        <i>#FFFFDD</i><br/>
        Items that are active but apply to the future
    </div>

    <div style="clear: both" /><br />
    
    <!------------------------------------------------------------------------------------>
    <a name="DataTables"></a><h2>Data Tables</h2>
    
    <blockquote><p>The different table layouts used to display record sets or tabular data.  Tables should generally 
    be a fixed width and fit within 780 pixels unless the requirements force it to be larger e.g. large reports with lots
    of columns.</p>
    </blockquote>

    <blockquote class="techie">
        <ul>
            <li>Use the following CSS classes *only* for displaying tables. </li>
            <li>Table cells are by default left-aligned.</li>
            <li>Set width's manually in the header, it will vary according to data requirements.</li>
        </ul>
    </blockquote>


    <h3>Alternate-row color coding</h3>
    
    <table cellpadding="2" cellspacing="0" class="dataTable">
        <tbody>
            <tr>
                <th style="width:150px">Type</th>
                <th style="width:50px">Qty</th>
                <th style="width:100px">Uom</th>
                <th style="width:50px">Amount</th>
                <th style="width:200px">Description</th>
            </tr>
            <tr class="evenRow">
                <td><a href="#">Free of Charge</a></td>
                <td>7</td>
                <td>Calendar Days</td>
                <td>&nbsp;</td>
                <td>1 Calendar Day of 2PMN Free</td>
            </tr>
            <tr class="oddRow">
                <td><a href="#">Long Hire Discount</a></td>
                <td>21</td>
                <td>Calendar Days</td>
                <td>$5.00</td>
                <td>&nbsp;</td>
            </tr>
            <tr class="evenRow">
                <td ><a href="#">Long Hire Discount</a></td>
                <td>35</td>
                <td></td>
                <td>$5.00</td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>

    <blockquote class="techie">
        <ul>
            <li>Table CSS Class: <b>dataTable</b></li>
            <li>Row CSS Classes: <b>evenRow</b> / <b>oddRow</b></li>
        </ul>
    </blockquote>

    <h3>Color status coding</h3>

    <table class="dataTableColor" cellpadding="2" cellspacing="0">
		<tbody>
		    <tr>
    			<th style="width: 200px;">Name</th>
    			<th style="width: 100px;">Brand</th>
    			<th style="width: 100px;">Country</th>
    			<th style="width: 150px;">Booked</th>
    			<th style="width: 150px;">Travel</th>
    			<th style="width: 50px;">Status</th>
    		</tr>
    		<tr style="background-color: #DDFFDD;">
			    <td><a href="#">10000BCB - 10,000 BC OFFER JUNE 06</a></td><td>B - Britz</td><td>NZ - New Zealand</td><td>05/06/2006 - 30/06/2009</td><td>05/06/2006 - 15/07/2009</td><td>Active</td>
		    </tr>
		    <tr style="background-color: #DDDDFF;">
			    <td><a href="#">25ANNIAU - 25TH ANNIVERSARY SPECIAL FOR MEIERS</a></td><td>B - Britz</td><td>AU - Australia</td><td>01/11/2004 - 14/03/2006</td><td>01/04/2005 - 31/03/2006</td><td>Active</td>
		    </tr>
		    <tr style="background-color: #DDDDDD;">
			    <td><a href="#">2DAYBREAK - 3 Day Britz Escape</a></td><td>B - Britz</td><td>AU - Australia</td><td>08/02/2004 - 29/02/2004</td><td>08/02/2004 - 30/06/2004</td><td>Inactive</td>
		    </tr>
		    <tr style="background-color: #DDDDFF;">
			    <td><a href="#">3DAYBREAK - 3 Day Britz Break</a></td><td>B - Britz</td><td>AU - Australia</td><td>05/02/2004 - 28/06/2004</td><td>08/02/2004 - 30/06/2004</td><td>Active</td>
            </tr>
        </tbody>
    </table>

    <blockquote class="techie">
        <ul>
            <li>Table CSS Class: <b>dataTableColor</b></li>
        </ul>
    </blockquote>

    <h3>Grid data</h3>

    <table class="dataTableGrid" cellpadding="2" cellspacing="0">
		<tbody>
		    <tr>
    			<th style="width: 100px;">A</th>
    			<th style="width: 100px;">B</th>
    			<th style="width: 100px;">C</th>
    			<th style="width: 100px;">D</th>
    		</tr>
    		<tr>
			    <td>1</td><td>2</td><td>3</td><td>4</td>
		    </tr>
		    <tr>
			    <td>5</td><td>6</td><td>7</td><td>8</td>
		    </tr>
		    <tr>
			    <td>9</td><td>10</td><td>11</td><td>12</td>
		    </tr>
		    <tr>
			    <td>13</td><td>14</td><td>15</td><td>16</td>
            </tr>
        </tbody>
    </table>

    <blockquote class="techie">
        <ul>
            <li>Table CSS Class: <b>dataTableGrid</b></li>
            <li>Row CSS Classes (optional): <b>evenRow</b> / <b>oddRow</b></li>
        </ul>
    </blockquote>
    
    <blockquote class="techie">
        <ul>
            <li>Most tables should only need the basic style and layouts demonstrated above.  However, if you need to show a table within a &quot;dataTable&quot; (e.g. more complicated report layouts), you can
             override the CSS styles inherited from the parent tables to your internal table by using the CSS class <b>dataTableInternal</b></li>
            <li>If you need to override border style's and colors, dont define the *full* border style, just override border-width, and border-style.
              This is a good rule with CSS in general, override only what you need to, and inherit as much as possible.</li>
        </ul>
    </blockquote>
             

    <!------------------------------------------------------------------------------------>
    <a name="Forms"></a><h2>Forms</h2>

    <h3>General Layout</h3>        

    <blockquote>
        <ul>
            <li>The general four column layout for simple forms.</li>
            <li>Column widths are 150,250,150,* pixels (last column has no width to make it stretch to fit).  This is not set in concrete and 
            can vary according to data requirements, though an attempt should be made to follow this standard.</li>
            <li>All data is displayed in form elements, not as text.</li>
            <li>Field labels are followed by a colon.</li>
            <li>Mandatory form elements are followed by a asterix.</li>
            <li>Buttons are right aligned to the form.  Active buttons (update, save, delete etc) are placed before neutral buttons (cancel, back, reset etc)</li>
            <li>Ok is spelled &quot;OK&quot;</li>
            <li>Standard buttons names are:
                <ul>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_OK" Text="OK" />
                        &nbsp;&nbsp;<i>&quot;Button_OK&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Cancel" Text="Cancel" />
                        &nbsp;&nbsp;<i>&quot;Button_Cancel&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Update" Text="Update" />,
                        <asp:Button runat="server" CssClass="Button_Standard Button_Edit" Text="Edit" />
                        &nbsp;&nbsp;<i>&quot;Button_Update&quot;</i>, <i>&quot;Button_Edit&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Save" Text="Save" />
                        &nbsp;&nbsp;<i>&quot;Button_Save&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Create" Text="Create" />,
                        <asp:Button runat="server" CssClass="Button_Standard Button_Add" Text="Add" />,
                        <asp:Button runat="server" CssClass="Button_Standard Button_New" Text="New" />
                        <br /><i>&quot;Button_Create&quot;</i>, <i>&quot;Button_Add&quot;</i>, <i>&quot;Button_New&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Delete" Text="Delete" />,
                        <asp:Button runat="server" CssClass="Button_Standard Button_Remove" Text="Remove" />
                        &nbsp;&nbsp;<i>&quot;Button_Delete&quot;</i>, <i>&quot;Button_Remove&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Reset" Text="Reset" />
                        &nbsp;&nbsp;<i>&quot;Button_Reset&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Refresh" Text="Refresh" />
                        &nbsp;&nbsp;<i>&quot;Button_Refresh&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Prev" Text="Previous" />,
                        <asp:Button runat="server" CssClass="Button_Standard Button_Back" Text="Back" />
                        <asp:Button runat="server" CssClass="Button_Standard Button_Close" Text="Close" />
                        <br />&nbsp;&nbsp;<i>&quot;Button_Prev&quot;</i>, <i>&quot;Button_Back&quot;</i>, <i>&quot;Button_Close&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Next" Text="Next" />
                        &nbsp;&nbsp;<i>&quot;Button_Next&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Search" Text="Search" />
                        &nbsp;&nbsp;<i>&quot;Button_Search&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Calculate" Text="Calculate" />
                        &nbsp;&nbsp;<i>&quot;Button_Calculate&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Print" Text="Print" />
                        &nbsp;&nbsp;<i>&quot;Button_Print&quot;</i>
                    </li>
                    <li>
                        <asp:Button runat="server" CssClass="Button_Standard Button_Report" Text="Report" />
                        &nbsp;&nbsp;<i>&quot;Button_Report&quot;</i>
                    </li>
                </ul>
            </li>
        </ul>
    </blockquote>
    
    <table cellpadding="2" cellspacing="0">
        <tr>
            <td style="width:150px">Field Label 1:</td>
            <td style="width:250px"><asp:TextBox runat="server" /></td>
            <td style="width:150px">Field Label 2:</td>
            <td style="width:250px"><asp:TextBox runat="server" /></td>
        </tr>
        <tr>
            <td>Field Label 3:</td>
            <td><asp:DropDownList runat="server" /></td>
            <td>Field Label 4:</td>
            <td>
                <uc1:DateControl runat="server" />
                <uc1:TimeControl runat="server" />
            </td>
        </tr>
        <tr>
            <td>Field Label 5:</td>
            <td colspan="3"><asp:TextBox runat="server" Width="500px" />*</td>
        </tr>
        <tr>
            <td>Field Label 6:</td>
            <td><uc1:PickerControl runat="server" PopupType="LOCATION" Param2="NZ" AppendDescription="true" /></td>
            <td>Field Label 7:</td>
            <td><asp:TextBox runat="server" /></td>
        </tr>
        <tr>
            <td>Field Label 5</td>
            <td colspan="3">
                <asp:CheckBoxList runat="server" RepeatLayout="Table" RepeatColumns="3">
                    <asp:ListItem>Option 1</asp:ListItem>
                    <asp:ListItem>Option 2</asp:ListItem>
                    <asp:ListItem>Option 3</asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="4">
                <asp:Button runat="Server" Text="Active Action" class="Button_Standard" />
                <asp:Button runat="Server" Text="Neutral Action" class="Button_Standard" />
            </td>
        </tr>
    </table>

    <blockquote class="techie">
        <p>No globally defined CSS classes should be used in making a form *layout*.  Should 
        be custom to your application.  Generally, the only custom styles that should need to be defined are 
        widths and heights.</p>
    </blockquote>
    
    <blockquote class="techie">
        <p>Use the extra button CSS classes in addition to Button_Small, Button_Medium, and Button_Standard e.g. class=&quot;Button_Standard Button_OK&quot;</p>  
   
    </blockquote>

    <h3>Optional Sections</h3>

    <blockquote><p>Optional sections (also called Collapsable Panel) are areas in a larger form that contain information 
    or sections that not always valid for the current action the user is performing.</p></blockquote>

    <uc1:CollapsiblePanel runat="server" Title="Optional Section (click to show or hide)" TargetControlID="showHidePanel" />
    <asp:Panel ID="showHidePanel" runat="server" Style="padding: 5px;">

        <table class="dataTableGrid" cellpadding="2" cellspacing="0">
		    <tbody>
		        <tr>
    			    <th style="width: 100px;">A</th>
    			    <th style="width: 100px;">B</th>
    			    <th style="width: 100px;">C</th>
    			    <th style="width: 100px;">D</th>
    		    </tr>
    		    <tr>
			        <td>1</td><td>2</td><td>3</td><td>4</td>
		        </tr>
		        <tr>
			        <td>5</td><td>6</td><td>7</td><td>8</td>
		        </tr>
		        <tr>
			        <td>9</td><td>10</td><td>11</td><td>12</td>
		        </tr>
		        <tr>
			        <td>13</td><td>14</td><td>15</td><td>16</td>
                </tr>
            </tbody>
        </table>

    </asp:Panel>

    <blockquote class="techie">
        <p>No custom styles or CSS classes should be needed in making content for a Colapsable Panel, with possibly 
        the exception of some &quot;padding&quot;.  
        The Collapsable panel automatically wraps the content of the &quot;TargetControl&quot; it will display.</p>
    </blockquote>


    <h3>Tabbed Layouts</h3>

    <blockquote>
        <p>Tabbed layouts are used with forms that display a lot of information.  They should in conjunction with
        an always visible area above it that shows identifying information and critical fields.</p>
    </blockquote>

    <ajaxToolkit:TabContainer runat="server" ActiveTabIndex="0" Width="100%">
        <ajaxToolkit:TabPanel runat="server" HeaderText="Saleable Products">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Agents">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Market Codes">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Non-Availability">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Locations">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Weekly Profile">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
        <ajaxToolkit:TabPanel runat="server" HeaderText="Notes">
            <ContentTemplate>
            </ContentTemplate>
        </ajaxToolkit:TabPanel>
    </ajaxToolkit:TabContainer>

    <blockquote class="techie">
        <p>Shown in this example is the default style for Tab Containers.  Likely, for each form, there will have to be some
        customization e.g. tab sizes, min-height etc.  Do not define CSS classes, but override the following CSS classes in your
        own form; <b>ajax__tab_container</b>, <b>ajax__tab_panel</b>, and <b>ajax__tab_inner span</b>.
        </p>
    </blockquote>


    <h3>Popups</h3>

    <blockquote>
        <p>Popup's are user interface elements that popup to allow users to edit details without leaving the larger 
        context of the main form.  They are laid out like normal forms although their width and height should 
        not exceed 75% of the screen, approx 600 x 500 pixels.</p>
    </blockquote>

    <asp:Panel runat="server" CssClass="modalPopup" Width="500px">
        <asp:Panel runat="Server" CssClass="modalPopupTitle">
            <asp:Label runat="Server" Text="Popup Title" />
        </asp:Panel>
        
        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td>Label 1:</td>
                <td><asp:DropDownList runat="server" /></td>
                <td>Label 2:</td>
                <td>
                    <asp:DropDownList runat="server">
                        <asp:ListItem Text="1" />
                        <asp:ListItem Text="2" />
                        <asp:ListItem Text="3" />
                    </asp:DropDownList>
                </td>
                <td>Label 3:</td>
                <td><asp:CheckBox runat="server" /></td>
            </tr>
            <tr>
                <td colspan="6"><asp:TextBox runat="server" TextMode="MultiLine" Rows="6" Width="100%"></asp:TextBox></td>
            </tr>                    
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td align="right" colspan="6">
                    <asp:Button runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                    <asp:Button runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <blockquote class="techie">
        <ul>
            <li>Forms in a popup should generally be layed out similar to the four column standard for the main forms, though
        adjusted for the smaller size.</li>
            <li>CSS classes used: <b>modalPopup</b> (for the popup Panel), and <b>modalPopupTitle</b> (for the top panel containing 
        the title label).</li>
        </ul>
    </blockquote>
    

    <h3>Standard Popup: Confirmation Box</h3>
    
    <asp:Panel runat="server" CssClass="modalPopup" ID="confirmationPanel" Width="500px" >
        <asp:Panel runat="Server" CssClass="modalPopupTitle">
            <asp:Label runat="Server" Text="Confirmation Box" />
        </asp:Panel>

        <table width="100%">
            <tr>
                <td style="vertical-align:top;" align="left">
                    <asp:Image runat="Server" ImageUrl="~/Images/informat2.gif" />
                </td>
                <td style="vertical-align:top;" align="left" width="100%">
                    <br />
                    Hello....
                    <br />
                </td>
            </tr>            
            <tr>
                <td colspan="2" align="right" width="100%">&nbsp;
                    <asp:Button runat="server" Text="OK" CssClass="Button_Standard Button_OK" />
                    <asp:Button runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                </td>
            </tr>
        </table>
        
    </asp:Panel>


    <h3>Standard Popup: Supervisor Override</h3>

    <asp:Panel runat="server" CssClass="modalPopup" Width="500px">
        
        <asp:Panel runat="Server" CssClass="modalPopupTitle">
            <asp:Label runat="Server" Text="Supervisor Override Required" />
        </asp:Panel>

        <table width="100%">
            <tr>
                <td style="vertical-align:top;" align="left">
                    <asp:Image runat="Server" ImageUrl="~/UserControls/SupervisorOverrideControl/SupervisorOverrideControl.gif" />
                </td>
                <td style="vertical-align:top;" align="left" width="100%">
                    <table width="100%">
                        <tr id="messageTableRow" runat="server" style="visibility:hidden" class="supervisorMessageTableRow">
                            <td colspan="2"><asp:Label ID="textLabel" runat="server" /></td>
                        </tr>
                        <tr>
                            <td><label id="logonLabel" for="logonTextBox">Logon:</label></td>
                            <td><asp:TextBox ID="logonTextBox" runat="Server" Width="200px"></asp:TextBox>*</td>
                        </tr>
                        <tr>
                            <td><label id="passwordLabel">Password:</label></td>
                            <td><asp:TextBox ID="passwordTextBox" runat="Server" TextMode="Password" Width="200px" />*</td>
                        </tr>
                        <tr id="errorTableRow" runat="Server" style="visibility:hidden" class="supervisorMessageTableRow">
                            <td colspan="2"><asp:Image ID="errorImage" runat="server" ImageUrl="../../images/error.gif" />&nbsp;<asp:Label ID="errorLabel" runat="server" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td style="vertical-align: bottom"><asp:Label runat="server" Style="color:Gray; font-family:Verdana; font-size:10px" Text="ROLE"/>&nbsp;</td>
                <td align="right" width="100%">&nbsp;
                    <asp:Button runat="server" Text="OK" CssClass="Button_Standard Button_OK" />
                    <asp:Button runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                </td>
            </tr>
        </table>

    </asp:Panel>


    </div>

</asp:Content>

