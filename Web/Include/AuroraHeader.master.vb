Imports System.Data
Imports System.Configuration
Imports System.Web.Script.Serialization
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO

Imports Aurora.Common
Imports Aurora.Booking.Services

Partial Class AuroraHeader
    Inherits AuroraMasterPage

    Protected AuroraPage As AuroraPage

    Property UserAllowedToToggleCompany() As Boolean
        Get
            If ViewState("Master_User_Allowed_To_Toggle") IsNot Nothing Then
                Return CBool(ViewState("Master_User_Allowed_To_Toggle"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Master_User_Allowed_To_Toggle") = value
        End Set
    End Property

    Private Sub BuildMenu()
        Dim selectedMenuModule As AuroraPageMenuModule = Me.AuroraPage.AuroraPageMenu.GetMenuModuleByFunctionId(Me.AuroraPage.MenuFunctionId)
        Dim selectedMenuFunction As AuroraPageMenuFunction = Me.AuroraPage.AuroraPageMenu.GetMenuFunctionByFunctionId(Me.AuroraPage.MenuFunctionId)

        ' populate menu module dropdown
        Me.menuModuleDropDown.Items.Clear()
        For Each menuModule As AuroraPageMenuModule In Me.AuroraPage.AuroraPageMenu.Items
            Dim li As New ListItem(menuModule.MenLabel, menuModule.MenId)
            li.Selected = selectedMenuModule Is menuModule
            Me.menuModuleDropDown.Items.Add(li)
        Next

        ' populate menu function dropdown
        Me.menuFunctionDropDown.Items.Clear()
        If selectedMenuModule IsNot Nothing Then
            For Each menuFunction As AuroraPageMenuFunction In selectedMenuModule.Items
                Dim li As New ListItem(menuFunction.MenLabel, menuFunction.MenId)
                li.Selected = selectedMenuFunction Is menuFunction
                Me.menuFunctionDropDown.Items.Add(li)
            Next
        End If

        ' set menu json 
        Dim js As New JavaScriptSerializer()
        menuJavascriptLiteral.Text = _
            vbTab & "<script type=""text/javascript"">" & vbCrLf & _
            "var functionId = " & js.Serialize(Me.AuroraPage.FunctionId) & ";" & vbCrLf & _
            "var functionCode = " & js.Serialize(Me.AuroraPage.FunctionCode) & ";" & vbCrLf & _
            "var functionFileName = " & js.Serialize(Me.AuroraPage.FunctionFileName) & ";" & vbCrLf & _
            "var menuFunctionId = " & js.Serialize(Me.AuroraPage.MenuFunctionId) & ";" & vbCrLf & _
            "var menuFunctionCode = " & js.Serialize(Me.AuroraPage.MenuFunctionCode) & ";" & vbCrLf & _
            "var menuFunctionFileName = " & js.Serialize(Me.AuroraPage.MenuFunctionFileName) & ";" & vbCrLf & _
            "var auroraBaseUrl = " & js.Serialize(Me.ResolveUrl("~/../")) & ";" & vbCrLf & _
            "var auroraPageMenu = " & js.Serialize(Me.AuroraPage.AuroraPageMenu) & ";" & vbCrLf & _
            vbTab & "</script>" & vbCrLf
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Me.AuroraPage = CType(Me.Page, AuroraPage)

        userNameLabel.Text = AuroraPage.UserName

        BuildMenu()

        Me.title.Text = Server.HtmlEncode(AuroraPage.PageTitle & " - " & UserSettings.Current.SystemIdentifier)
       

        ShowLabels()

        Me.titlelabel.InnerText = Server.HtmlEncode(AuroraPage.PageTitle)

        If Me.AuroraPage.HasHelp Then
            Me.helpButton.Attributes.Add("onclick", "openHelp(" & New JavaScriptSerializer().Serialize(Me.AuroraPage.HelpUrl) & ")")
            Me.helpButton.Src = "~/images/helpButton.gif"
            Me.helpButton.Alt = "Help"
            Me.helpButton.Attributes.Add("title", "Help")
        Else
            Me.helpButton.Attributes.Remove("onclick")
            Me.helpButton.Src = "~/images/helpDisabledButton.gif"
            Me.helpButton.Alt = "No help for this function"
            Me.helpButton.Attributes.Add("title", "No help for this function")
        End If

        systemInfo.Text = Server.HtmlEncode(AuroraPage.PageTitle _
            & " - " & UserSettings.Current.SystemIdentifier & " - " & Me.AuroraPage.FunctionCode _
            & IIf(Me.AuroraPage.MenuFunctionCode <> Me.AuroraPage.FunctionCode, "/" & Me.AuroraPage.MenuFunctionCode, ""))

        Me.Page.ClientScript.RegisterClientScriptInclude("AuroraHeader", Me.ResolveUrl("~/Include/AuroraHeader.js"))
        Me.Page.ClientScript.RegisterClientScriptInclude("Date", Me.ResolveUrl("~/Include/Date.js"))
        Me.Page.ClientScript.RegisterClientScriptInclude("Validation", Me.ResolveUrl("~/Include/Validation.js"))

        ''rev:mia dec.16
        Dim blnIsInBookingProcess As Boolean = Request.RawUrl.ToUpper.Contains("/Web/Booking/BookingProcess.aspx".ToUpper)
        Dim blnURLContaintoggle As Boolean = IIf(Request.QueryString("toggleon") IsNot Nothing, True, False)

        If blnIsInBookingProcess Then
            MessagingAndRedirector(True)
        Else
            If blnURLContaintoggle Then
                If Request.QueryString("toggleon") = "0" Then
                    MessagingAndRedirector(False)
                End If
            Else
                lnkToggleCompany.Visible = False
                welcomeLabel.Visible = True
                Session("PostBackValuetoggle") = ""
            End If
        End If

        
        If Not Page.IsPostBack Then
            DisplayPrinters()
        Else
            Dim sc As ScriptManager = ScriptManager.GetCurrent(Me.Page)
            If sc IsNot Nothing Then
                If sc.AsyncPostBackSourceElementID.IndexOf("btnTest") <> -1 Or sc.AsyncPostBackSourceElementID.IndexOf("btnAssigned") <> -1 Then
                    DisplayPrinters()
                Else
                    DisplayPrinters()
                End If
            End If
        End If
    End Sub

    Public Overrides Sub ClearMessages()
        messagePanel.ClearMessagePanel()
    End Sub

    Public Overrides Sub AddMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        messagePanel.AddMessagePanel(messageType, text)
    End Sub

    Public Overrides Sub ClearShortMessage()
        shortMessagePanel.ClearMessagePanel()
    End Sub

    Public Overrides Sub SetShortMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        shortMessagePanel.SetMessagePanel(messageType, text)
    End Sub

    Public Sub SwitchUser(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSwitchUser.ServerClick
        Me.AuroraPage = CType(Me.Page, AuroraPage)
        If ("" & Me.AuroraPage.CookieValue("refreshUser")) = "" Then
            Session.Abandon()
            System.Web.Security.FormsAuthentication.SignOut()

            Response.Status = "401 Unauthorized"
            Response.AddHeader("WWW-Authenticate", "Digest")
            Me.AuroraPage.CookieValue("refreshUser") = "true"
            Response.End()
        Else
            Me.AuroraPage.CookieValue("refreshUser") = ""
            Response.Redirect("../../initial.asp")
        End If
    End Sub

    Public Sub ToggleCompany(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkToggleCompany.ServerClick
        If UserAllowedToToggleCompany Then
            Dim sRetMsg As String
            sRetMsg = Data.ToggleUserCompany(AuroraPage.UserCode)
            If sRetMsg.IndexOf("SUCCESS") > -1 Then
                ' Success
                Aurora.Common.UserSettings.Load(AuroraPage.UserCode)
                ShowLabels()
                'Page_Load(New Object, New EventArgs)
                Me.shortMessagePanel.AddMessagePanelInformation("Your location and company have been updated successfully")
            Else
                Me.shortMessagePanel.AddMessagePanelError(sRetMsg)
            End If
            ' run the toggle sp!
        End If
    End Sub

    Public Sub ShowLabels()
        Me.welcomeLabel.Text = Server.HtmlEncode(AuroraPage.CompanyName)
        lnkToggleCompany.InnerText = Server.HtmlEncode(AuroraPage.CompanyName)
        ' check user's roles
        UserAllowedToToggleCompany = Data.UserAllowedToToggleLocation(AuroraPage.UserCode)
        If Not UserAllowedToToggleCompany Then
            lnkToggleCompany.Visible = False
            welcomeLabel.Visible = True
        Else
            lnkToggleCompany.Visible = True
            welcomeLabel.Visible = False
        End If
    End Sub

#Region " REV:MIA DEC.10,2008"

    Private ReadOnly Property GetURL() As String
        Get
            Dim server As String = Request.ServerVariables("HTTP_HOST")
            Dim cookieValue As String 
            cookieValue = GetDefaultUrl()
            If Not cookieValue.ToUpper.Contains("BOOKINGPROCESS") Then
                ''  Return ""
            End If

            Dim intLocation As Integer = -1

            If cookieValue.Contains("..") Then
                intLocation = cookieValue.IndexOf("..")
                cookieValue = cookieValue.Remove(intLocation, 2)
            End If

            If cookieValue.Contains("~") Then
                intLocation = cookieValue.IndexOf("~")
                cookieValue = cookieValue.Remove(intLocation, 1)
            End If

            cookieValue = cookieValue.Replace("""", "")
            Dim path As String = ""
            If Request.ServerVariables("HTTP_HOST").Contains("localhost") = False Then
                path = "/Aurora"
            End If
            Return "http://" & String.Concat(server, path, cookieValue)
        End Get
    End Property

    Private Function GetDefaultUrl() As String
        Dim _javascriptSerializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Return _javascriptSerializer.Serialize(UserSettings.Current.FunFileName)
    End Function

    Public Sub GotoDefaultPage(Optional ByVal indextoload As String = "1")
        Dim bookingprocessurl As String = "/WEB/BOOKING/BOOKINGPROCESS.ASPX"
        Dim path As String = ""
        Dim server As String = Request.ServerVariables("HTTP_HOST")
        Dim myurl As String = GetURL
        If String.IsNullOrEmpty(myurl) Then
            Exit Sub
        End If

        If Request.ServerVariables("HTTP_HOST").Contains("localhost") = False Then
            path = "/Aurora"
        End If
        Dim completeURL As String = "http://" & String.Concat(server, path, bookingprocessurl)
        Response.Redirect(completeURL.ToLower & "?toggleon=" & indextoload)

    End Sub

    Public Function TogglecompanyAction(ByVal currentpage As String) As Boolean
        TogglecompanyAction = False
        Dim pathPage As String = "BookingProcess.aspx".ToUpper

        Try
            If Not String.IsNullOrEmpty(currentpage) Then
                If currentpage.ToUpper.Contains(pathPage.ToUpper) Then
                    '' Return True
                    Return False ''rev:mia dec11 - change scope
                End If
            End If
            Return TogglecompanyAction
        Catch ex As Exception
        End Try

    End Function

    Public Sub ToggleCompanyInServer(ByVal usercode As String, ByVal companyname As String)
        Dim sRetMsg As String
        sRetMsg = Data.ToggleUserCompany(usercode)
        If sRetMsg.IndexOf("SUCCESS") > -1 Then
            ' Success
            Aurora.Common.UserSettings.Load(usercode)
            ShowLabels(companyname, usercode)
            'Page_Load(New Object, New EventArgs)
            Me.shortMessagePanel.AddMessagePanelInformation("Your location and company have been updated successfully")
        Else
            Me.shortMessagePanel.AddMessagePanelError(sRetMsg)
        End If
    End Sub
    Public Sub ToggleCompanyInServer()
        Dim sRetMsg As String
        sRetMsg = Data.ToggleUserCompany(AuroraPage.UserCode)
        If sRetMsg.IndexOf("SUCCESS") > -1 Then
            Aurora.Common.UserSettings.Load(AuroraPage.UserCode)
            ShowtoggleLabels()
            Me.shortMessagePanel.AddMessagePanelInformation("Your location and company have been updated successfully")
        Else
            Me.shortMessagePanel.AddMessagePanelError(sRetMsg)
        End If
    End Sub

    Public Sub ShowLabels(ByVal companyname As String, ByVal usercode As String)
        Me.welcomeLabel.Text = Server.HtmlEncode(companyname)
        lnkToggleCompany.InnerText = Server.HtmlEncode(companyname)
        ' check user's roles
        UserAllowedToToggleCompany = Data.UserAllowedToToggleLocation(usercode)
        If Not UserAllowedToToggleCompany Then
            lnkToggleCompany.Visible = False
            welcomeLabel.Visible = True
        Else
            lnkToggleCompany.Visible = True
            welcomeLabel.Visible = False
        End If
       
    End Sub

    Sub MessagingAndRedirector(ByVal isInBookingProcess As Boolean)
        Dim sc As ScriptManager
        sc = ScriptManager.GetCurrent(Me.Page)

        If isInBookingProcess Then
            If sc.AsyncPostBackSourceElementID.Equals("ctl00$lnkToggleCompany") Then
                GotoDefaultPage("0")
            Else
                UserAllowedToToggleCompany = Data.UserAllowedToToggleLocation(AuroraPage.UserCode)
                If UserAllowedToToggleCompany Then
                    lnkToggleCompany.Disabled = False
                    lnkToggleCompany.Visible = True
                    welcomeLabel.Visible = False
                Else
                    lnkToggleCompany.Disabled = True
                    lnkToggleCompany.Visible = False
                    welcomeLabel.Visible = True
                End If
            End If

        Else
            ToggleCompanyInServer()
        End If
    End Sub

    Public Sub ShowtoggleLabels()
        Me.welcomeLabel.Text = Server.HtmlEncode(AuroraPage.CompanyName)
        lnkToggleCompany.InnerText = Server.HtmlEncode(AuroraPage.CompanyName)
        UserAllowedToToggleCompany = Data.UserAllowedToToggleLocation(AuroraPage.UserCode)
        lnkToggleCompany.Attributes.Add("onclick", "href=''")
        lnkToggleCompany.Visible = False
        welcomeLabel.Visible = True
        
    End Sub
#End Region

#Region "UpdatePanel will be trigger in the SetPrinterPage"

    Protected Sub ImagePrinters_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ImagePrinters.Click

    End Sub

    Sub DisplayPrinters()
        Try

            Dim EFTPOS As New BookingPaymentEFTPOSMaintenance
            Dim dsPrinters As DataSet = EFTPOS.GetPrintersAssignedToBranchAndUser(Me.AuroraPage.UserCode)

            UserCodeAndPrinter = dsPrinters.Tables(0).Rows(0)("UserPrinter")
            gridPrinters.DataSource = dsPrinters.Tables(1)

            Dim arykey(0) As String
            arykey(0) = "LocPrnID"
            gridPrinters.DataKeyNames = arykey
            gridPrinters.DataBind()
            ImagePrinters.Enabled = True
        Catch ex As Exception
            Me.AuroraPage.SetErrorShortMessage("Error getting list of printers")
            ImagePrinters.Enabled = False
        End Try
        
    End Sub

    Protected Sub btnTest_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim btn As Button = CType(sender, Button)
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim txtPrinterName As TextBox = CType(row.FindControl("txtPrinterName"), TextBox)
            If Not txtPrinterName Is Nothing Then
                PrinterSelection.PrintRaw(txtPrinterName.Text, Now.ToUniversalTime.ToString + ": Check printer > " + txtPrinterName.Text)
            End If

        Catch ex As Exception
            Me.AuroraPage.SetErrorShortMessage("Printer testing error")
        End Try
        


    End Sub

    Protected Sub btnAssigned_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim retvalue As String
        Dim txtPrinterName As TextBox
        Try
            Dim btn As Button = CType(sender, Button)
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim id As Integer = gridPrinters.DataKeys(row.DataItemIndex).Value.ToString
            Dim EFTPOS As New BookingPaymentEFTPOSMaintenance
            retvalue = EFTPOS.SetPrintersAssignedToBranchAndUser(Me.AuroraPage.UserCode, id)
            txtPrinterName = CType(row.FindControl("txtPrinterName"), TextBox)
            Me.AuroraPage.SetInformationShortMessage(String.Concat("'", txtPrinterName.Text, "'", " ", retvalue, ". Please refresh your page."))
            DisplayPrinters()

        Catch ex As Exception
            Me.AuroraPage.SetErrorShortMessage("Error mapping your profile to a printer")
        End Try


    End Sub

    Protected Sub gridPrinters_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridPrinters.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)

            Dim img As Image = CType(e.Row.FindControl("imgDefault"), Image)
            If Not img Is Nothing Then
                img.Visible = IIf(rowview("DefaultPrinterID") > 0 And UserCodeAndPrinter = rowview("DefaultPrinterID"), True, False)
            End If
            
        End If
    End Sub

    Protected Sub gridPrinters_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gridPrinters.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)

            Dim img As Image = CType(e.Row.FindControl("imgDefault"), Image)
            If Not img Is Nothing Then
                img.Visible = IIf(rowview("DefaultPrinterID") > 0 And UserCodeAndPrinter = rowview("DefaultPrinterID"), True, False)
            End If

            
        End If
    End Sub

    Private Property UserCodeAndPrinter() As Integer
        Get
            Return CInt(ViewState("UserCodeAndPrinter"))
        End Get
        Set(ByVal value As Integer)
            ViewState("UserCodeAndPrinter") = value
        End Set
    End Property
#End Region
End Class

