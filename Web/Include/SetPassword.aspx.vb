Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Data
Imports System.Web.Script.Serialization

Imports Aurora.Common

<AuroraPageTitleAttribute("Set Password")> _
<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class Include_SetPassword
    Inherits AuroraPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        userTextBox.Text = UserSettings.Current.UsrCode & " - " & UserSettings.Current.UsrName

        passwordTextBox.Attributes.Add("value", passwordTextBox.Text)
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Try
            Aurora.Common.Data.UpdateUserPassword(UserSettings.Current.UsrCode, passwordTextBox.Text, UserSettings.Current.UsrCode)
            Me.SetShortMessage(AuroraHeaderMessageType.Information, "Password updated")
        Catch ex As Exception
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "Error updating password")
        End Try
    End Sub
End Class
