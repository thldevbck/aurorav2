﻿function trimSpace(str){ 
    return str.replace(/^\s+/g, '').replace(/\s+$/g, '');
}

function ValidateText(mtText){
    mtText = trimSpace(mtText)
    var TextArray = mtText.split(" ")
	
    // Check if there is only one word
    if (TextArray.length == 1){
        //MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', "Please enter more than one word.");
        //alert('Please enter more than one word.')
        return "Please enter more than one word."
    }
    // Check second word
    if (TextArray[0].length == 1){
        if (TextArray[1].length == 1){
            //MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', "Invalid text - please re-enter.");
	        //alert('Invalid text - please re-enter.')
	        return "Invalid text - please re-enter."
        }
        if (chkSamLtrInWrd(TextArray[1]) == 1){
            //MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', "Invalid text - please re-enter.");
	        //alert('Invalid text - please re-enter.')
	        return "Invalid text - please re-enter."
        }
    }
    // Check first word for same character
    if (TextArray[0].length > 1){
        if (chkSamLtrInWrd(TextArray[0]) == 1){
            //MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', "Invalid text - please re-enter.");
	        //alert('Invalid text - please re-enter.')
	        return "Invalid text - please re-enter."
        }
    }	
    return ""
}

// return 1 if it is same character
function chkSamLtrInWrd(myWord){
    var first = myWord.substring(0,1)
    for(i=0; i<myWord.length; i++){
        if (first != myWord.substring(i,i+1))
	        return 0
    }
    return 1
}

function keyStrokeInt(){ 
	var nVal = window.event.keyCode
	if (nVal < 48 || nVal > 58){ 
		//if(document.selection.type!="Text")
			window.event.keyCode = 8
	}
}

function removeSpace(obj){ 
    s=obj.value
    String.prototype.trim = function() { return this.replace(/(^\s*)|(\s*$)/g, "");}
    sTrim = s.trim()
    obj.value=sTrim
}

function keyStrokeDecimal(){ 
    var nVal = window.event.keyCode
    if ((nVal == 58 || nVal != 46 && nVal < 48 || nVal > 58) && nVal != 45){ 
        window.event.keyCode = 8
    }   
}

function trimDecimal(intData,noOfDec){ 
	var MinusFlag = false
	intData = "" + intData + ""
	if(intData.substring(0,1)=="-")
		MinusFlag = true
	inumber=intData
	number=intData
	intData=""+number+""
	number=""+number+""
	numberArray=number.split(".")
	if(intData=="-."){ return "-0.00"
	}
	if(intData=="-"){ return "-0.00"
	}
	if(intData=="."){ return "0.00"
	}
	if(numberArray.length==1){ if(inumber=="")
		inumber=0
		return ""+inumber+""+".00"
	}
	sInt=numberArray[0]
	sFraction=""+numberArray[1].substring(0,noOfDec)+""
	if(sFraction==""){ return ""+sInt+""+".00"
	}
	if(sInt.length ==1 ){ if(sInt=="-"){ if(sFraction.length==1)
	sFraction= sFraction + '0'
	if(sFraction=="")
	return "-0.00"
	return "-0." + sFraction
	}
	}
	if(sInt==""){ if(sFraction.length==1)
	sFraction= sFraction + '0'
	return "0." + sFraction
	}
	if(sInt==0){ if(sFraction.length==1)
	sFraction= sFraction + 0
	return sInt + "." + sFraction
	}
	if(sFraction.length==1){ return sResult=sInt +"."+ sFraction+"0"
	}
	sResult=sInt +"."+ sFraction
	return ""+sResult+""
}