﻿/***************************************************************************************/
/* Utility Functions */
/***************************************************************************************/

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };
String.prototype.ltrim = function() { return this.replace(/^\s+/,""); }
String.prototype.rtrim = function() { return this.replace(/\s+$/,""); }

function clearDropDown (dropDown)
{ 
    if (dropDown.options)
        dropDown.options.length = 0;
    else
        for (var i= dropDown.length; i >= 0; i--)
            dropDown.remove(0);
}

function addDropDownOption (dropDown, text, value)
{ 
    if (dropDown.options)
        dropDown.options[dropDown.options.length] = new Option(text, value);
    else
        dropDown.add(Option.create(text,value));
}

function addEvent(obj, evType, fn)
{ 
    if (obj == null) 
        return false
    else if (obj.addEventListener)
    { 
        obj.addEventListener(evType, fn, false); 
        return true; 
    } 
    else if (obj.attachEvent)
        return obj.attachEvent("on"+evType, fn); 
    else 
        return false; 
}

function cancelEvent (e)
{
    e = e ? e : window.event;
    if(e.stopPropagation) e.stopPropagation();
    if(e.preventDefault) e.preventDefault();
    e.cancelBubble = true;
    e.cancel = true;
    e.returnValue = false;
    return false;    
}

function toggleTableColumnCheckboxes(col, evt)
{
    if (!evt) evt = window.event;
    if (!evt) return false;

    var chkAll = evt.srcElement ? evt.srcElement : evt.target;
    if (!chkAll) return false;
    
    var tbody = chkAll;
    while (tbody && (tbody.tagName != 'TBODY'))
        tbody = tbody.parentNode;
    if (!tbody) return false;
    
    var tableRows = tbody.getElementsByTagName("TR");
    for (var i = 0; i < tableRows.length; i++)
    {
        var tableCell = tableRows[i].getElementsByTagName ("TD")[col];
        if (!tableCell) continue;
        
        var inputElements = tableCell.getElementsByTagName ("INPUT");
        for (var j = 0; j < inputElements.length; j++)
        {
            var inputElement = inputElements[j];
            if ((inputElement.type == 'checkbox') && !inputElement.disabled)
                inputElement.checked = chkAll.checked;
        }
    }
    
    return true;
}

function toggleCheckboxesList (id, all)
{
    var checkBoxList = document.getElementById (id);
    if (!checkBoxList) return;
    
    var inputs = checkBoxList.getElementsByTagName('input');
    for (var j = 0; j < inputs.length; j++)
    {
        var input = inputs[j];
        input.checked = all;
    }
}

/***************************************************************************************/
/* Application Functionality */
/***************************************************************************************/

function getErrorMessage(errorCode, param1, param2, param3, param4, param5, param6)
{  
    var id = -1;         
    var text = "";
  
    for(var i = 0; i < errorMessage.ErrorMessages.length; i++)
        if (errorMessage.ErrorMessages[i].Text == errorCode)
            text = errorMessage.ErrorMessages[i].Value;

    text = text.replace('xxx1', param1);
    text = text.replace('xxx2', param2);
    text = text.replace('xxx3', param3);
    text = text.replace('xxx4', param4);
    text = text.replace('xxx5', param5);
    text = text.replace('xxx6', param6);
    
    for(var i=1; i<=6; i++)
        text = text.replace('xxx' + i, '');
        
    text = errorCode + ' - ' + text;

    return text 
}

function getSelectedMenuModule()
{
    var menuModuleDropDown = document.getElementById ("ctl00_menuModuleDropDown");
    
    for (var i = 0; i < auroraPageMenu.Items.length; i++)
    {
        var menuModule = auroraPageMenu.Items[i];
        if (menuModule.MenId == menuModuleDropDown.value)
            return menuModule;
    }
    return null;
}

function getSelectedMenuFunction(selectedMenuModule)
{
    var menuFunctionDropDown = document.getElementById ("ctl00_menuFunctionDropDown");
    for (var i = 0; i < selectedMenuModule.Items.length; i++)
    {
        var menuFunction = selectedMenuModule.Items[i];
        if (menuFunction.MenId == menuFunctionDropDown.value)
            return menuFunction;
    }
    return null;
}

/***************************************************************************************/
/* Application Event Handlers */
/***************************************************************************************/

function GoToAuroraAspUrl (funId, funCode, funFileName, searchParam, params)
{
    document.getElementById("htm_hid_funId").value = funId;
    document.getElementById("hid_htm_funCode").value = funCode;
    document.getElementById("htm_hid_funFileName").value = funFileName;
    document.getElementById("selectedCombo1").value = funFileName;
    document.getElementById("sTxtSearch").value = searchParam;

    document.forms[0].action = auroraBaseUrl + "asp/" + funFileName + params
    document.forms[0].method = "POST";
    document.forms[0].submit();
}

function Search()
{
    var searchParam = ('' + document.getElementById("ctl00_searchTextBox").value).trim();
    
    if (window.onPageSearch != null) 
    {
        window.onPageSearch(searchParam)
    }
    else
    {
        if ((menuFunctionId != '') && (menuFunctionFileName != '') && (searchParam != ''))
        {
            var url = auroraBaseUrl + "Web/" + menuFunctionFileName;
            url += (url.indexOf('?') >= 0 ? "&" : "?") + "sTxtSearch=" + escape (searchParam);
            window.location = url;
        } 
    }
}

function GoToBooking()
{
 var searchParam = ('' + document.getElementById("ctl00_searchTextBox").value).trim();

    if (searchParam != '')
        // GoToAuroraAspUrl ('A112', 'RS-VEHREQMGT', 'Booking.asp', searchParam, '?sTxtSearch=" + escape (searchParam) + "&hdRentalId=&hdBookingId=');
	    window.location = auroraBaseUrl + "Web/Booking/Booking.aspx?sTxtSearch=" + escape (searchParam) + "&hdRentalId=&hdBookingId="; 
    else
	    alert('Please enter the booking no.');
}

function menuModuleDropDown_onchange()
{
    var menuModuleDropDown = document.getElementById ("ctl00_menuModuleDropDown");
    var menuFunctionDropDown = document.getElementById ("ctl00_menuFunctionDropDown");
    
    var selectedMenuModule = getSelectedMenuModule();
    if (!selectedMenuModule) return;

    clearDropDown (menuFunctionDropDown);
    addDropDownOption (menuFunctionDropDown, "Select", "");
    for (var i = 0; i < selectedMenuModule.Items.length; i++)
    {
        var menuFunction = selectedMenuModule.Items[i];
        addDropDownOption (menuFunctionDropDown, menuFunction.MenLabel, menuFunction.MenId);
    }
}

function menuFunctionDropDown_onchange()
{ 
    var menuModuleDropDown = document.getElementById ("ctl00_menuModuleDropDown");
    var menuFunctionDropDown = document.getElementById ("ctl00_menuFunctionDropDown");
    
    var selectedMenuModule = getSelectedMenuModule();
    if (!selectedMenuModule) return;
    
    var selectedMenuFunction = getSelectedMenuFunction (selectedMenuModule);
    if (!selectedMenuFunction) return;

    var funId = ('' + selectedMenuFunction.MenFunId).trim();
    var funCode = ('' + selectedMenuFunction.FunCode).trim();
    var funFileName = ('' + selectedMenuFunction.FunFileName).trim();
    var searchParam = ('' + document.getElementById("ctl00_searchTextBox").value).trim();
    
    if ((funId != '') && (funFileName != ''))
    {
    	if (new RegExp("aspx$").test(funFileName))
    	{
    	    var url = auroraBaseUrl + "Web/" + funFileName;
    	    if (searchParam != '') 
    	        url += (url.indexOf('?') >= 0 ? "&" : "?") + "sTxtSearch=" + escape (searchParam);
    	    window.location = url;
    	}
    	else
    	    GoToAuroraAspUrl (funId, funCode, funFileName, searchParam, '');
    }
}

function changeUserCode(url)
{ 
    if (!url || (url == '')) url = "ini1.asp";
    document.forms[0].action= auroraBaseUrl + url;
    document.forms[0].method="POST";
    document.forms[0].submit();
}

function openSetPasswordWindow()
{ 
    window.showModalDialog
    (
        auroraBaseUrl + "Web/Include/SetPassword.aspx", 
        null, 
        "dialogHeight:250px;dialogWidth:475px;resizable:yes;status:No;"
    ); 
}

function openCurrencyExchangeWindow(currency)
{
    var url = auroraBaseUrl + "Web/Include/CurrencyExchange.aspx";
    if (currency) url += "?currency=" + escape ("" + currency);
    
    var w = window.open
    (
        url, 
        "currencyExchange", 
        "width=450,height=475,resizable=yes"
    );
    if (w) w.focus();
}

function openHelp(url)
{ 
    var w = window.open
    (
        url, 
        "help", 
        "width=750,height=550,resizable=yes,scrollbars=yes"
    );
    if (w) w.focus();
}

/***************************************************************************************/
/* ASP.NET / IE CheckBox and Radio List Hack */
/***************************************************************************************/

function fixDisabledTableSpanLabels()
{
    var tableArray = document.getElementsByTagName('table');
    for (var k = 0; k < tableArray.length; k++)
    {
        var table = tableArray[k];
        if (!table.disabled) continue;
        table.disabled = false;

        var spanArray = document.getElementsByTagName('span');
        for (var i = 0; i < spanArray.length; i++)
        {
            var span = spanArray[i];
            if (!span.disabled) continue;
            span.disabled = false;

            var labelArray = span.getElementsByTagName('label');
            for (var j = 0; j < labelArray.length; j++)
            {
                var label = labelArray[j];
                if (!label.disabled) continue;
                label.disabled = false;
            }
        }
    }
}

function fixReadonlyDisabledStyles()
{
    var tagNames = ['input', 'select', 'textarea', 'label'];
    for (var i = 0; i < tagNames.length; i++)
    {
        var tagName = tagNames[i];
        var elements = document.getElementsByTagName(tagName);
        for (var j = 0; j < elements.length; j++)
        {
            var element = elements[j];
            var className;
            // if (element.style.backgroundColor) continue;
            
            if (element.disabled)
            {
                if (tagName == 'input')
                    className = element.type + 'disabled';
                else
                    className = tagName + 'disabled';
                
                if (element.className) 
                    element.className = element.className + ' ' + className;
                else
                    element.className = className;
            }
            else if (element.readOnly)
            {
                if (tagName == 'input') 
                    className = element.type + 'readonly';
                else
                    className = tagName + 'readonly';

                if (element.className) 
                    element.className = element.className + ' ' + className;
                else
                    element.className = className;
            }
        }
    }
}

function onloadHacks()
{
    fixDisabledTableSpanLabels();
    fixReadonlyDisabledStyles();
}

/***********************************************/
//rev:mia nov21 added function for New Navigation
/***********************************************/
function WindowNavigate(page,qs)
{
    if (page != '')
	    window.location = auroraBaseUrl + "Web/" + page + "?" + qs; 
}