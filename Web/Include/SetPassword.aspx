<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SetPassword.aspx.vb" Inherits="Include_SetPassword" MasterPageFile="~/Include/PopupHeader.master" validateRequest="false" %>

<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
    <script type="text/javascript">
var passwordTextBoxId = '<%= Me.passwordTextBox.ClientID %>';
var passwordTextBox;

function passwordLoad()
{
    passwordTextBox = document.getElementById (passwordTextBoxId);

    passwordTextBox.focus();
}

addEvent (window, "load", passwordLoad);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(passwordLoad);
    </script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:UpdatePanel runat="Server">
        <ContentTemplate>

            <table cellpadding="2" cellspacing="0">
                <tr>
                    <td style="width:150px">User:</td>
                    <td>
                        <asp:TextBox 
                            ID="userTextBox" 
                            runat="server" 
                            Width="250px" 
                            ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td style="width:150px">Password:</td>
                    <td>
                        <asp:TextBox 
                            ID="passwordTextBox" 
                            TextMode="Password"
                            runat="server" 
                            Width="250px" />
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                        <input type="button" id="closeButton" value="Close" class="Button_Standard Button_Close" onclick="window.close(); return false;" />
                    </td>
                </tr>
            </table>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>