﻿<%@ Page Title="Edit Vehicle Plan" Language="C#" MasterPageFile="~/Include/AuroraHeader.master"
    AutoEventWireup="true" CodeFile="EditVehiclePlan.aspx.cs" Inherits="AIMS_EditVehiclePlan"
    EnableEventValidation="false" %>

<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>


<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <link rel="Stylesheet" type="text/css" href="Styles/common.css" />
    <link rel="Stylesheet" type="text/css" href="Styles/EditVehiclePlan/StyleSheet.css" />
    <style>
        label.error
        {
            color: red;
        }
        .highlight
        {
            background-color: #E6E6FA;
        }
        
        input[readonly],select[readonly],textarea[readonly] {
             background-color: #ccc;
        }
        
        #txthistory{
             background-color: #ccc;
        }
        
        .modalPopupConfirmation
        {
            border-color: Black;
            border-width: 2px;
            position: absolute;
            margin-left: auto;
            margin-right: auto;
            width: 700px;
            padding: 30px;
            background-color: rgb(246,246,255);
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JqueryLatest/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery-ui-1.10.2.js" type="text/javascript"></script>
    <script src="JS/EditVehiclePlan/JScript.js" type="text/javascript"></script>
   
   <%--<script src="JS/EditVehiclePlan/EditVehiclePlan.js" type="text/javascript"></script>
   <script src="JqueryLatest/AIMSvalidator.js" type="text/javascript"></script>
   --%> 
    <script src="JS/AIMSdrag.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery.validate.min.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="vehicleSearchPanel" runat="Server" CssClass="packageSearch" DefaultButton="searchButton">
        <table style="width: 100%;" cellpadding="2" cellspacing="0">
            <tr>
                <td class="packageSearchTD1">
                    Fleet Model Code:
                </td>
                <td class="packageSearchTD2">
                    <asp:DropDownList ID="ddlFleetModelCode" CssClass="ddlFleetModelCode" runat="server">
                    </asp:DropDownList>
                     <span id="invalidFleetModel" style="display: none">Please select a valid item</span>
                </td>
            </tr>
            <tr>
                <td>
                    From Unit Number:
                </td>
                <td>
                    <asp:TextBox ID="txbFromUnitNumber" runat="server" Style="width: 147px" MaxLength="10" />
                </td>
            </tr>
            <tr>
                <td>
                    To Unit Number:
                </td>
                <td>
                    <asp:TextBox ID="txbToUnitNumber" runat="server" Style="width: 147px" MaxLength="10" />
                </td>
            </tr>
            <tr>
                <td>
                    Registration Number:
                </td>
                <td>
                    <asp:TextBox ID="registrationTextBox" runat="server" Style="width: 147px" MaxLength="10" />
                </td>
            </tr>
            <tr>
                <td>
                    Select View
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rblNewVehicle" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Enquiry" Value="NewVehicle"></asp:ListItem>
                        <asp:ListItem Text="Recoding" Value="Rebranding"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <asp:CustomValidator runat="server" ID="valSearchKeywords" OnServerValidate="valSearchKeywords_ServerVAlidate"
            ErrorMessage="Please either choose a valid Fleet Model Code or limit unit number range."
            ValidationGroup="searchValidation" Enabled="false"></asp:CustomValidator>
        <table class="buttonTable">
            <tr>
                <td class="rightAlign">
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search"
                        OnClick="searchButton_Click" ValidationGroup="searchValidation" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset"
                        OnClick="resetButton_Click" />
                    <asp:Button ID="createButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                        OnClick="backButton_Click" />
                </td>
            </tr>
        </table>
        <br />
        <asp:Panel runat="server" ID="pnlSearchResult">
            <table class="buttonTable">
                <tr>
                    <td class="rightAlign">
                        <input type="button" value="Bulk Unit(s) Edit" class="Button_Standard Button_Edit" 
                            id="ButtonTopUnitEdit" />
                    </td>
                </tr>
            </table>
            <asp:Repeater runat="server" ID="resultrepeater" OnItemDataBound="resultrepeater_ItemDataBound">
                <HeaderTemplate>
                    <table class='mrrDataTable' width='100%' id="resultrepeater_table">
                        <tr>
                            <th>
                            </th>
                            <th style="text-align: left;">
                                <asp:CheckBox ID="chkSelectAll" runat="server" />
                            </th>
                            <th>
                                Unit
                                <br />
                                Number
                            </th>
                            <th>
                                Fleet
                                <br />
                                Model
                                <br />
                                Code
                            </th>
                            
                            <th>
                                Reg.Number
                            </th>

                            <th>
                                Start
                                <br />
                                Location
                            </th>
                            <th>
                                Delivery Date
                            </th>
                            <th>
                                On Fleet Date
                            </th>
                            <th>
                                Receive Date
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Plan Status
                            </th>
                           
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr runat="server" id="tr" name='<%#DataBinder.Eval(Container, "DataItem.FleetAssetId")%>'>
                        <td>
                            <a href="#" runat="server" id="linkEdit">Edit</a>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSelectUni" runat="server" />
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.UnitNumber")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.FleetModelCode")%>
                        </td>
                         <td>
                            <%#DataBinder.Eval(Container, "DataItem.RegistrationNumber")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PurLocationOnFleet")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PurProposedDelivDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.FirstOnFleetDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PurReceiptDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.Status")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PlanStatus")%>
                        </td>
                       
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr runat="server" id="tr" name='<%#DataBinder.Eval(Container, "DataItem.FleetAssetId")%>'>
                        <td>
                            <a href="#" runat="server" id="linkEdit">Edit</a>
                        </td>
                        <td>
                            <asp:CheckBox ID="chkSelectUni" runat="server" />
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.UnitNumber")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.FleetModelCode")%>
                        </td>
                         <td>
                            <%#DataBinder.Eval(Container, "DataItem.RegistrationNumber")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PurLocationOnFleet")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PurProposedDelivDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.FirstOnFleetDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PurReceiptDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.Status")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.PlanStatus")%>
                        </td>
                        
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <table class="buttonTable">
                <tr>
                    <td class="rightAlign">
                        <input type="button" value="Bulk Unit(s) Edit" class="Button_Standard Button_Edit"
                            id="ButtonBottomUnitEdit" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlRebrandingSearchResult">
            <table class="buttonTable">
                <tr>
                    <td class="rightAlign">
                        <input type="button" value="Bulk Edit" class="Button_Standard Button_Edit" id="topbuttonRebrandingEdit"  />
                    </td>
                </tr>
            </table>
            <asp:Repeater runat="server" ID="rebrandingrepeater" OnItemDataBound="rebrandingrepeater_ItemDataBound">
                <HeaderTemplate>
                    <table class='mrrDataTable' width='100%' id="rebrandingrepeater_table">
                        <tr>
                            <th style="text-align: left;">
                                <asp:CheckBox ID="chkRebrandingSelectAll" runat="server" />
                            </th>
                            <th>
                                Unit Number
                            </th>
                            <th>
                                Registration
                            </th>
                            <th>
                                Fleet Model Code
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Effective Date
                            </th>
                            <th>
                                Acq Status
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr runat="server" id="tr">
                        <td>
                            <asp:CheckBox ID="chkRebrandingSelectUni" runat="server" />
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.OriginalUnitNum")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.Registration")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.ModelCode")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.GeneralStatus")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.EffectiveDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.AcqDispStatus")%>
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr runat="server" id="tr">
                        <td>
                            <asp:CheckBox ID="chkRebrandingSelectUni" runat="server" />
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.OriginalUnitNum")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.Registration")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.ModelCode")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.GeneralStatus")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.EffectiveDate")%>
                        </td>
                        <td>
                            <%#DataBinder.Eval(Container, "DataItem.AcqDispStatus")%>
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
            <br />
            <table class="buttonTable">
                <tr>
                    <td class="rightAlign">
                        <input type="button" value="Bulk Edit" class="Button_Standard Button_Edit" id="bottombuttonRebrandingEdit" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <asp:Panel runat="server" ID="pnlEditBulk" CssClass="modalPopup pnlEditBulk moveable"
            Style="border-color: Black; border-width: 2px; display: none; width: 450px">
            <table style="width: 450px;" cellpadding="2" cellspacing="0">
                <tr>
                    <td colspan="2">
                        <h3>
                            Bulk Unit Edit (New Vehicle)
                        </h3>
                    </td>
                    <td style="text-align: right;">
                        <a id="A2" href="" runat="server" class="ClosePopup">Close</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        Fleet Model Code:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBulkRebrandingDropDown" runat="server" Style="width: 250px;">
                        </asp:DropDownList>
                        
                        
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        Start Location:
                    </td>
                    <td style="width: 250px">
                        <asp:DropDownList ID="ddlBulkStartLocation" runat="server" CssClass="dropDownList">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        Delivery Date:
                    </td>
                    <td style="width: 250px">
                        <uc1:DateControl ID="dateBulkDeliveryDate" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        On Fleet Date:
                    </td>
                    <td style="width: 250px">
                        <uc1:DateControl ID="dateBulkFleetDate" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        Received Date:
                    </td>
                    <td style="width: 250px">
                        <uc1:DateControl ID="dateBulkReceivedDate" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnBulkSave" CssClass="Button_Standard Button_Save"
                            OnClick="btnBulkSave_Click" Text="Save" Style="width: 80px;" />&nbsp;
                        <input id="btnBulkEditCancel" type="button" value="Cancel" class="Button_Standard Button_Cancel"
                            style="width: 80px;" />
                    </td>
                </tr>
            </table>
            <p>
                <asp:Label runat="server" ID="lblBulkNote" Text="NOTE: field that is blank will not be updated."></asp:Label>
            </p>
            <p><label id="lblErrorMessage" class="error" value=""></label></p>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlEditBulkRebranding" CssClass="modalPopup pnlEditBulk moveable"
            Style="border-color: Black; border-width: 2px; display: none; width: 450px;">
            <table style="width: 450px;" cellpadding="2" cellspacing="0">
                <tr>
                    <td style="width: 100px;">
                        <h3>
                            Bulk Unit Edit
                        </h3>
                    </td>
                    <td style="text-align: left;">
                        (Rebranding)
                    </td>
                    <td style="text-align: right;">
                        <a id="A3" href="" runat="server" class="ClosePopup">Close</a>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px;">
                        Action
                    </td>
                    <td style="width: 200px; vertical-align: top">
                        <asp:RadioButtonList runat="server" ID="rblBulkAction">
                            <asp:ListItem Value="insert" Text="Insert for selected unit(s)" Selected="True"></asp:ListItem>
                            <asp:ListItem Value="update" Text="Update selected record(s)"></asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px;">
                        Schedule Code:
                    </td>
                    <td style="width: 200px;">
                        <asp:DropDownList runat="server" ID="ddlBulkScheduleCode" Width="150">
                        </asp:DropDownList>
                        &nbsp;*
                    </td>
                    <td style="text-align: left;">
                        <label id="errorForddlBulkScheduleCode" class="error">
                        </label>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">
                        Effective Date:
                    </td>
                    <td style="width: 200px">
                        <uc1:DateControl ID="dateRebrandingEffectiveDate" runat="server" AutoPostBack="false" />
                        &nbsp;*
                    </td>
                    <td style="text-align: left;">
                        <label id="errorFordateRebrandingEffectiveDate" class="error">
                        </label>
                    </td>
                </tr>
            </table>
            <div style='text-align: right;'>
                <asp:Button runat="server" ID="btnBulkRebrandingSave" CssClass="Button_Standard Button_Save"
                    OnClick="btnBulkRebrandingSave_Click" Text="Save" ValidationGroup="rebrandingValidation"
                    Style="width: 80px;" />&nbsp;
                <input id="btnBulkRebrandingCancel" type="button" value="Cancel" class="Button_Standard Button_Cancel"
                    style="width: 80px;" />
            </div>
        </asp:Panel>
        <br />
        <asp:Panel runat="server" ID="pnlEditUnit" CssClass="modalPopup pnlEditUnit" Style="width: 840px;
            height: 755px; border-color: Black; border-width: 2px; display: none;">
            <h2>
                Edit Vehicle
            </h2>
            <table style="width: 650px;" cellpadding="0" cellspacing="0" id="editvehicletable">
                <tr>
                    <td>
                        Unit Number:
                    </td>
                    <td>
                        <asp:TextBox ID="txbUnit" runat="server" ReadOnly="true" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td style="width: 150px;">
                   
                    </td>
                    <td></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Fleet Model Code:
                    </td>
                     <td colspan="2">
                        <asp:DropDownList ID="RebrandingDropDown" runat="server" Style="width: 250px;">
                        </asp:DropDownList>
                    </td>
                  
               <%-- </tr>
                <tr>--%>
                    <td>
                        Registration #:
                    </td>
                    <td>
                          <asp:TextBox runat="server" ID="txbEditRegistraionNumber"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <!--rev:mia end march 13,2014 -added-->
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <b>VEHICLE INFORMATION</b>
                    </td>
                </tr>
              
                <tr>
                    <td>
                        Name:
                    </td>
                    <td>
                        <asp:DropDownList ID="ManufacturerDropDown" runat="server">
                        </asp:DropDownList>
                        &nbsp;*
                    </td>
                    <td>
                        <label id="errorForManufacturerDropDown" class="error">
                        </label>
                    </td>
                    <td>
                        Code:
                    </td>
                    <td>
                        <asp:DropDownList ID="modelCodeDropDown" runat="server" Width="120">
                        </asp:DropDownList>
                        &nbsp;*
                    </td>
                    <td>
                        <label id="errorFormodelCodeDropDown" class="error" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Description:
                    </td>
                    <td>
                        <b>
                            <asp:TextBox ID="modeldescription" runat="server" ReadOnly="true" Width="150" />
                        </b>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Last Check-In Date:
                    </td>
                    <td>
                        <asp:TextBox ID="lastCheckindateLabel" runat="server" ReadOnly="true" />
                    </td>
                   
                </tr>
                <tr>
                    <td>
                        Fuel Type:
                    </td>
                    <td>
                        <asp:DropDownList ID="FuelTypeDropDown" runat="server">
                        </asp:DropDownList>
                        &nbsp;*
                    </td>
                    <td>
                        <label id="errorForFuelTypeDropDown" class="error">
                        </label>
                    </td>
                   <td>
                        Last Check-In Location:
                    </td>
                    <td>
                        <asp:TextBox ID="lastcheckinlocationlabel" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Transmission:
                    </td>
                    <td>
                        <asp:DropDownList ID="TransmissionDropDown" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Last Check-In Odometer:
                    </td>
                    <td>
                        <asp:TextBox ID="lastcheckinOdometerLabel" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Service Schedule:
                    </td>
                    <td>
                        <asp:DropDownList ID="ServiceScheduleDropDown" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Disposal Date:
                    </td>
                    <td>
                          <asp:TextBox ID="disposalDate" runat="server" ReadOnly="true" />
                    </td>
                     
                </tr>
                <!--rev:mia end march 13,2014 -added-->
                <tr>
                    <td>
                        Start Location:
                    </td>
                    <td style="text-align: left;">
                        <asp:DropDownList ID="ddlEditStartLocation" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Delivery Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="dateEditDeliveryDate" runat="server" AutoPostBack="false" />
                        &nbsp;*
                    </td>
                    <td>
                        <label for="dateEditDeliveryDate" class="error" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Chassis #:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txbChassisNumber"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        On Fleet Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="dateEditOnFleetDate" runat="server" AutoPostBack="false" />
                        &nbsp;*
                    </td>
                    <td>
                        <label id="errorFordateEditOnFleetDate" class="error" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Engine Number:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txbEditEngineNumber"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                     <td>
                        First Registration Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="dateEditRegistrationDate" runat="server" AutoPostBack="false" />
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        Comp. Plate:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txbCompliancePlateYear" Width="40" MaxLength="4"></asp:TextBox>
                        /
                        <asp:TextBox runat="server" ID="txbCompliancePlateMonth" Width="20" MaxLength="2"></asp:TextBox>
                        (YYYY/mm)
                        <asp:RegularExpressionValidator runat="server" ID="RegularExpressionValidator1" ErrorMessage="Please input a 4 digits year"
                            ValidationExpression="\d{4}" ControlToValidate="txbCompliancePlateYear" EnableClientScript="false"></asp:RegularExpressionValidator>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                  <td>
                        Warranty Expiry Date:
                    </td>
                    <td>
                        <%--<uc1:DateControl ID="RUCRenewDateControl" runat="server" AutoPostBack="false" />--%>
                        <uc1:DateControl ID="WarrantyExpiryDateControl" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Self-Contain Expiry Date:
                    </td>
                    <td>
                      <uc1:DateControl ID="SelfContainmentDateControl" runat="server" AutoPostBack="false" />
                    </td>
                    <td>
                        &nbsp;
                    </td>

                    <td>
                        Received Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="dateEditReceivedDate" runat="server" AutoPostBack="false" />
                    </td>
                     
                    
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                       <b> REG / KM'S / RUC</b>
                    </td>
                </tr>
                <tr>
                    <td>
                      RUC Paid to (KM):
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="RUCPaidToTextBox" MaxLength="6"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        COF Expiry Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="dateCofDate" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        E-Tag:
                    </td>
                    <td>
                       <asp:TextBox runat="server" ID="txbETag"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                
                    <td>
                        Registration Expiry Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="RegExpiryDataDateControl" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
               
                <tr>
                    <td>
                       EPIRB:
                    </td>
                    <td>
                         <asp:TextBox runat="server" ID="txbEPurb"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Telematics Device:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTelematicsDevice" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <%--<td>
                     Telematics Tablet:
                    </td>
                    <td>
                     <asp:TextBox runat="server" ID="txtTelematicsTablet" MaxLength="30"></asp:TextBox>
                    </td>--%>
                    <td>
                     Telematics Tablet Date:
                    </td>
                    <td>
                      <uc1:DateControl ID="TelematicsTabletDateDateControl" runat="server" AutoPostBack="false" />
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Telematics InstallDate:
                    </td>
                    <td>
                        <uc1:DateControl ID="DateControlTelematicsInstallDate" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <%--<td>
                      Telematics GUID:
                    </td>
                    <td>
                        
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtTelematicsGUID" Style="width: 250px" ></asp:TextBox>
                       <%--<asp:Label runat="server" ID="lblTelematicsGUID" Text=""></asp:Label>-- %>
                    </td>--%>
                    <td>Telematics Tablet Installer :</td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTelematicsTabletInstaller" MaxLength="64"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Telematics Installer:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txttelematicsInstaller" MaxLength="30"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    
                    <td></td>
                    <td></td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                      Telematics GUID:
                    </td>
                    <td>
                        
                        <asp:TextBox runat="server" ReadOnly="true" ID="txtTelematicsGUID" Style="width: 250px" ></asp:TextBox>
                       <%--<asp:Label runat="server" ID="lblTelematicsGUID" Text=""></asp:Label>--%>
                    </td>
                </tr>
                <%--<tr>
                    <td>
                        <asp:Button runat="server" ID="updGUID" Text="Update GUID" Style="width: 100px"  OnClick="btnUpdateGUID_Click"/>
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                </tr>--%>
                <tr>
                    <td>
                        Comments
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <textarea id="txtcomments" cols="10" rows="5" style="width: 830px; " runat="server"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        History
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <textarea id="txthistory" cols="10" rows="5" style="width: 830px; "></textarea>
                    </td>
                </tr>

            </table>
            <br />
         
            <div style="text-align: right;">
                <input id="cancelbutton" type="button" value="Cancel" class="Button_Standard Button_Cancel"
                    style="width: 100px;" />
                <asp:Button runat="server" ID="btnSaveSingleVehicle" CssClass="Button_Standard Button_Save"
                    OnClick="btnSaveSingleVehicle_Click" Text="Save" Width="100" />
            </div>
        </asp:Panel>
    </asp:Panel>
       <table style="width: 700px;" cellpadding="2" cellspacing="2" id="ructable">
                <tr>
                    <td style="display: none">
                        Ignition Key
                    </td>
                    <td style="display: none">
                        <asp:TextBox ID="txbIgnitionKey" runat="server" />
                    </td>
                </tr>
                <!--rev:mia start march 13,2014 -added-->
                <tr style="display: none;">
                    <td>
                        iFleetAssetId
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txbEditFleetAssetId" EnableViewState="true"></asp:TextBox>
                    </td>
                </tr>
            </table>
    <asp:Panel ID="panHiddenValues" runat="server" Visible="false">
        <asp:Label runat="server" ID="lblFleetModelId" Text=""></asp:Label>
        <asp:Label runat="server" ID="lblManufacturerId" Text=""></asp:Label>
        <asp:Label runat="server" ID="lblCountryId" Text=""></asp:Label>
    </asp:Panel>
    <!--rev:mia added march 13,2014 -added ..used by RebrandingDropDown-->
    <asp:HiddenField runat="server" ID="hiddenOptions" EnableViewState="true" />
    <!--rev:mia added march 13,2014 -added ..used by History-->
    <asp:HiddenField runat="server" ID="HiddenHistory" EnableViewState="true" />
    <asp:HiddenField runat="server" ID="HiddenCountryId" EnableViewState="true" />
    <asp:HiddenField runat="server" ID="HiddenFieldFleetAssetIds" EnableViewState="true" />
    <asp:HiddenField runat="server" ID="HiddenFieldUserCode" EnableViewState="true" />
    <!-- -->
    <asp:HiddenField runat="server" ID="HiddenHasVehOnFleetChecked" Value="false" EnableViewState="true" />

        <uc2:ConfirmationBoxControl ID="ConfirmationBoxControl1"
                                    runat="server" 
                                    LeftButton_Visible="true"
                                    LeftButton_AutoPostback="false"
                                    />
    


     <script src="JS/EditVehiclePlan/EditVehiclePlan.js" type="text/javascript"></script>
    <script src="JqueryLatest/AIMSvalidator.js" type="text/javascript"></script>
</asp:Content>
