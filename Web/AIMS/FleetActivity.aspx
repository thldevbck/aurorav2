﻿<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="true"
    Title="Fleet Activity" CodeFile="FleetActivity.aspx.vb" Inherits="AIMS_FleetActivity" %>

<%@ Register Src="../UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<asp:Content ID="cphStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <link rel="Stylesheet" type="text/css" href="Styles/common.css" />
    <link rel="Stylesheet" type="text/css" href="Styles/FleetActivity/StyleSheet.css" />
    <link rel="stylesheet" type="text/css" href="Styles/FleetActivity/StyleSheetIE7.css" />
</asp:Content>
<asp:Content ID="cphScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JqueryLatest/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery-ui-1.10.2.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery.validate.min.js" type="text/javascript"></script>
    <script src="JqueryLatest/knockout-3.0.0.js" type="text/javascript"></script>
    <script src="JS/GenericAIMSAccess.js" type="text/javascript"></script>
    <script src="JS/FleetActivity/Models.js" type="text/javascript"></script>
    <script src="JS/AIMSCommon.js" type="text/javascript"></script>
    <script src="JS/FleetActivity/Common.js" type="text/javascript"></script>
    <script src="JS/FleetActivity/Request.js" type="text/javascript"></script>
    <script type="text/javascript">
        var isFullAccess = false;
        var isPartialAccess = false;
        var isViewAccess = false;
    </script>
</asp:Content>
<asp:Content ID="cphContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div id="screen">
    </div>
    <div id="divSearchPanel">
        <table id="tableSearch">
            <tr>
                <td>
                    Country:
                </td>
                <td>
                    <select id="selSearchCountry" name="selSearchCountry" data-bind="options: $root.AllSearchCountries, optionsValue: 'countryId', optionsText: 'displayText', optionsCaption: '--please select--'">
                    </select>&nbsp;*
                    <%-- <label for="selSearchCountry" class="error">
                </label>--%>
                </td>
            </tr>
            <tr>
                <td>
                    Unit Number:
                </td>
                <td>
                    <input type="text" id="inputFromUnitNumber" />
                </td>
                <td>
                    Registration Number:
                </td>
                <td>
                    <input type="text" id="inputRegistrationNumber" />
                </td>
            </tr>
        </table>    
        <div class="row">
            <table class="buttonTable">
                <tr>
                    <td class="rightAlign">
                        <button data-bind="click: $root.ActivityHistorySearch.bind($data, true)" class="Button_Standard Button_Search">
                            Search</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="divFleetDetailPanel">
        <table id="tableFleetDetailPanel">
            <tr>
                <td>
                    Model Code:
                </td>
                <td>
                   <input type="text" id="inputModelCode" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.ModelCode()" />
                </td>
                <td>
                    Model Description:
                </td>
                <td>
                    <input type="text" id="inputModelDescription" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.ModelDescription()" />
                </td>
            </tr>
            <tr>
                <td>
                     Manufacturer:
                </td>
                <td>
                    <input type="text" id="inputManufacturer" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.Manufacturer()" />
                </td>
                <td>
                     Manufacturer Model Code:
                </td>
                <td>
                    <input type="text" id="inputManufacturerModelCode" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.manufacturerModelCode()" />
                </td>
            </tr>
            <tr>
                <td>
                     Manufacturer Model Name:
                </td>
                <td>
                 <input type="text" id="inputManufacturerModelName" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.ManufacturerModel()" />
                </td>
            </tr>
        </table>
   <%--     <div class="row">
            <div class="titleCol">
                Model Code
            </div>
            <div class="inputCol">
                <input type="text" id="inputModelCode" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.ModelCode()" />
            </div>
            <div class="titleCol">
                Model Description
            </div>
            <div class="inputCol">
                <input type="text" id="inputModelDescription" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.ModelDescription()" />
            </div>
        </div>
        <div class="row">
            <div class="titleCol">
                Manufacturer
            </div>
            <div class="inputCol">
                <input type="text" id="inputManufacturer" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.Manufacturer()" />
            </div>
            <div class="titleCol">
                Manufacturer Model Code
            </div>
            <div class="inputCol">
                <input type="text" id="inputManufacturerModelCode" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.manufacturerModelCode()" />
            </div>
        </div>
        <div class="row">
            <div class="titleCol">
                Manufacturer Model Name
            </div>
            <div class="inputCol">
                <input type="text" id="inputManufacturerModelName" disabled="disabled" data-bind="value: $root.ThisFleetRelatedInformation.ManufacturerModel()" />
            </div>
        </div>--%>
    </div>

    <table id="tableFleetActivitiesHistory" class="divResult">
        <thead>
            <tr>
                <th>
                </th>
                <th>
                    External Ref
                </th>
                <th>
                    Activity Type
                </th>
                <th>
                    Completed
                </th>
                <th>
                    Start Odo
                </th>
                <th>
                    End Odo
                </th>
                <th>
                    Start Date
                </th>
                <th>
                    End Date
                </th>
                <th>
                    Start Branch
                </th>
                <th>
                    End Branch
                </th>
                <th>
                    Modified Date
                </th>
                <th>
                    Modified By
                </th>
            </tr>
        </thead>
         <%--<tbody data-bind="foreach: AllFleetActivityHistory().sort(function (l, r) { return l.StartDate() < r.StartDate() ? -1 : -1 })">--%>
        <tbody data-bind="foreach: AllFleetActivityHistory">
            <tr>
                <td>
                    <%--<input type="checkbox" data-bind="checked: SelectedItem" />--%>
                    <input type="radio" data-bind="value: ActivityId, checked: $root.SelectedFleetActivityHistory"
                        name="fleetActivity" />
                </td>
                <td>
                    <a data-bind="text: ExternalRef, click: $root.ExternalRef_Click, css: { disableLink: ActivityType() != 'Rental' }">
                    </a>
                </td>
                <td>
                    <span data-bind="text: ActivityType"></span>
                </td>
                <td>
                    <span data-bind="text: Completed"></span>
                </td>
                <td>
                    <span data-bind="text: StartOdometer"></span>
                </td>
                <td>
                    <span data-bind="text: EndOdometer"></span>
                </td>
                <td>
                    <span data-bind="text: StartDate"></span>
                </td>
                <td>
                    <span data-bind="text: EndDate"></span>
                </td>
                <td>
                    <span data-bind="text: StartLocation"></span>
                </td>
                <td>
                    <span data-bind="text: EndLocation"></span>
                </td>
                <td>
                    <span data-bind="text: ModifiedDate"></span>
                </td>
                <td>
                    <span data-bind="text: ModifiedBy"></span>
                </td>
            </tr>
        </tbody>
    </table>
    <table class="buttonTable">
        <tr>
            <td class="rightAlign">
                <button id="FleetActivityRecordsBulkEdit" class="Button_Standard Button_Edit" data-bind="click: $root.FleetActivityEdit">
                    Edit</button>
                <button id="FleetActivityRecordAdd" class="Button_Standard Button_Add" data-bind="click: $root.FleetActivityAdd">
                    Add</button>
            </td>
        </tr>
    </table>
    <div id="editFleetAtivityPanel" class="modalPopup">
        <h3>
            <span id="popupTitle"></span>Activities
        </h3>
        <table id="tableEditActivities"  cellspacing="0" cellpadding="0" style="width:100%;border-collapse:collapse;">
            <tr>
                <td>
                    Activity Type:
                </td>
                <td>
                    <select id="editFleetAtivityPanel_ActivityType" data-bind="options: $root.AllActivityType, optionsValue: 'ActivityTypeValue', optionsText: 'ActivityTypeText', optionsCaption: '--please select--'">
                    </select>&nbsp;*
                </td>
                <td>
                    External Ref:
                </td>
                <td>
                    <input type="text" id="editFleetAtivityPanel_ExternalRef" />
                </td>
            </tr>
            <tr>
                <td>
                    Model code:
                </td>
                <td>
                    <input id="editFleetAtivityPanel_ModelCode" size="15" disabled="disabled" />
                </td>
                <td>
                    Model Description:
                </td>
                <td>
                    <input id="editFleetAtivityPanel_ModelDescription" disabled="disabled" />
                </td>
            </tr>
            <tr>
                <td>
                    Completed:
                </td>
                <td>
                    <input type="checkbox" id="editFleetAtivityPanel_Completed" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <b>Start</b>
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <b>End</b>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    Date:
                </td>
                <td>
                    <uc1:DateControl ID="editFleetAtivityPanel_StartDate" runat="server" AutoPostBack="false" />
                    &nbsp;
                    <uc1:TimeControl ID="editFleetAtivityPanel_StartTime" runat="server" AutoPostBack="false" />
                    &nbsp;*
                </td>
                <td>
                    Date:
                </td>
                <td>
                    <uc1:DateControl ID="editFleetAtivityPanel_EndDate" runat="server" AutoPostBack="false" />
                    &nbsp;
                    <uc1:TimeControl ID="editFleetAtivityPanel_EndTime" runat="server" AutoPostBack="false" />
                    &nbsp;*
                </td>
            </tr>
            <tr>
                <td>
                    Branch:
                </td>
                <td>
                    <select id="editFleetAtivityPanel_StartBranch" data-bind="options: $root.AllLocation, optionsValue: 'LocationValue', optionsText: 'LocationText', optionsCaption: '--please select--'">
                    </select>
                    &nbsp;*
                </td>
                <td>
                    Branch:
                </td>
                <td>
                    <select id="editFleetAtivityPanel_EndBranch" data-bind="options: $root.AllLocation, optionsValue: 'LocationValue', optionsText: 'LocationText', optionsCaption: '--please select--'">
                    </select>
                    &nbsp;*
                </td>
            </tr>
            <tr>
                <td>
                    Odometer:
                </td>
                <td>
                    <input type="text" id="editFleetAtivityPanel_StartOddometer" />
                    &nbsp;*
                </td>
                <td>
                    Odometer:
                </td>
                <td>
                    <input type="text" id="editFleetAtivityPanel_EndOddometer" />
                    &nbsp;*
                </td>
            </tr>
        </table>
        <table id="tableDescription">
            <tr>
                <td valign="top">
                    Description:
                </td>
                <td style="width: 552px;">
                    <textarea id="editFleetAtivityPanel_Desciption" cols="100" rows="5"></textarea>
                </td>
            </tr>
        </table>
        <table id="tableButton">
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <button class="Button_Standard Button_OK" data-bind="click: $root.FleetActivityEdit_OK"
                        style="width: 80px;">
                        Apply</button>&nbsp;
                    <button class="Button_Standard Button_Cancel" data-bind="click: $root.FleetActivityEdit_Cancel"
                        style="width: 80px;">
                        Cancel</button>
                </td>
            </tr>
        </table>
    </div>
    <asp:HiddenField Value="" runat="server" ID="ManufacturerIDhidden" />
    <asp:HiddenField Value="FleetActivity" runat="server" ID="hiddenPageIdentity" />
    <asp:HiddenField Value="" runat="server" ID="AIMSListenerPath" />
</asp:Content>
