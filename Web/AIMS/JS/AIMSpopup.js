﻿
//----POPUP FOR MANUFACTURER MODEL SECTION WHEN CLICKING EDIT AND ADD----

function centerPopupNewEditManufacturerModel() {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelNewEditManufacturerModel]").height();
    popupWidth = $("[id$=_panelNewEditManufacturerModel]").width();


    $("[id$=_panelNewEditManufacturerModel]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });

    $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").attr("disabled", "disabled")
    $("#ManufacturerModelresultV2").hide()
    $("[id$=_pnlModel]").hide()
}

function UnloadPopupAddNewManufacturingModel() {
    clearShortMessage()
    $("[id$=_panelNewEditManufacturerModel]").hide();
    $("label.error").text("")
    $("#EditManufacturerModelTable input[type=text]").val("")
    $("#EditManufacturerModelTable select").val("-1")
    $("[name='ManufacturerId_CheckBoxes']").prop("checked", false);
   
}

function LoadPopupAddNewManufacturingModel() {
    centerPopupNewEditManufacturerModel()
    $("[id$=_panelNewEditManufacturerModel]").show();

}

//----POPUP FOR MANUFACTURER MODEL SECTION WHEN LINK-----

function centerPopupLinkManufacturingModel() {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelLinkManufacturingModel]").height();
    popupWidth = $("[id$=_panelLinkManufacturingModel]").width();


    $("[id$=_panelLinkManufacturingModel]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
}

function UnloadPopupLinkManufacturingModel() {
    $("[id$=_panelLinkManufacturingModel]").hide();
    $("[name='linkManufacturerId_CheckBoxes']").prop("checked", false);
    $("#FleetModelsuggestionResultV2").empty();
    clearShortMessage()
}

function LoadPopupLinkManufacturingModel() {
    centerPopupLinkManufacturingModel()
    $("[id$=_panelLinkManufacturingModel]").show();

}


//----POPUP FOR MANUFACTURER MODEL SECTION WHEN SEARCH CLICK-----
function centerPopupLinkDisplayUnitNumber() {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelDisplayUnitNumber]").height();
    popupWidth = $("[id$=_panelDisplayUnitNumber]").width();


    $("[id$=_panelDisplayUnitNumber]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
}

function UnloadPopupDisplayUnitNumber() {
    $("[id$=_panelDisplayUnitNumber]").hide();
    clearShortMessage()
}

function LoadPopupDisplayUnitNumber() {
    $("[id$=_unitnumberfilteronModel]").val("")
    $("#beginAssetfilteronModel").val("")
    centerPopupLinkDisplayUnitNumber()
    $("[id$=_panelDisplayUnitNumber]").show();
}

//----POPUP FOR MANUFACTURER MODEL SECTION WHEN ALLOCATE UNIT NUMBER CLICK-----
function centerPopupAllocateUnitNumber() {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelAllocateUnitNumber]").height();
    popupWidth = $("[id$=_panelAllocateUnitNumber]").width();


    $("[id$=_panelAllocateUnitNumber]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
}

function UnloadPopupAllocateUnitNumber() {
    $("[id$=_panelAllocateUnitNumber]").hide();
    clearShortMessage()
}

function LoadPopupAllocateUnitNumber() {
    centerPopupAllocateUnitNumber()
    $("[id$=_panelAllocateUnitNumber]").show();
}









