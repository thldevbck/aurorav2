﻿

(function ($, window, document, undefined) {
    jQuery(function ($) {
        $(".modalPopup").hide();

        //Load URL paremeters
        $("#inputFromUnitNumber").val(getParameterByName("inputFromUnitNumber"));
        $("#inputToUnitNumber").val(getParameterByName("inputToUnitNumber"));
        $("#inputRegNum").val(getParameterByName("inputRegNum"));
        //$("[id*=calSalePlanStartDate]").val(getParameterByName("calSalePlanStartDate"));
        //$("[id*=calSalePlanEndDate]").val(getParameterByName("calSalePlanEndDate"));

        GetDropdownForCountries();

        var currentCountry = getParameterByName("iCountryId");

        //get user current country
        //user current country has higher priority than url parameter country
        $.ajax({
            type: "POST",
            url: CONST_URL_FLEETMODELMGT + "GetDefaultCountry",
            data: { UserCode: $("[id$=_ManufacturerIDhidden]").val() },
            success: function (data) {
                //use current user country
                $("#selSearchCountry").val($("#selSearchCountry option:contains('" + data + "')").val());
            },
            async: false
        });

        //only load the all dropdownlists when there is country selected.
        if ($("#selSearchCountry").val() != "") {
            GetDropdownForSalePlan($("#selSearchCountry").val());
        }

        //disable FleetModelCodeTextbox if no country selected in #selSearchCountry      
        $("#selSearchCountry").change(function () {
            $("#FleetModelCodeTextbox").val("");

            if (this.value == "-1") {
                $("#FleetModelCodeTextbox").prop("disabled", true).addClass("textdisabled");
            } else {
                $("#FleetModelCodeTextbox").prop("disabled", false).removeClass("textdisabled");
            }

            GetDropdownForSalePlan($("#selSearchCountry").val());
        });

        $("#singleEditCurrentPlan_customerType").change(function (data) {
            if ($(this).val().indexOf("Dealer") >= 0) {
                $("#mandatoryDealerNameIndicator").show();
            } else {
                $("#mandatoryDealerNameIndicator").hide();
            }
        });

        //MVVM data bind search result. Overall viewmodel for this screen.
        function AllVehicleSaleRecordsViewModel() {
            //Data
            var self = this;
            //self.SearchCriteria = new SearchCriteriaDetail({});

            //manually load data rather than data-bind
            self.SearchCriteria = new SearchCriteriaDetail({});

            self.VehicleSaleRecords = ko.observableArray([]);
            self.CurrentPlanRecords = ko.observableArray([]);
            self.ProcessedSteps = ko.observableArray([]);

            self.CurrentEditingPlanRecord;

            self.AllVehicleSaleRecords = ko.computed(function () {
                var checked = true;

                var countChecked = 0;

                for (var item in self.VehicleSaleRecords()) {
                    if (self.VehicleSaleRecords()[item].SelectedItem())
                        countChecked++;
                }

                if (countChecked == 0) {
                    checked = false;
                }

                return checked;
            });

            self.AllCurrentPlans = ko.computed(function () {
                var checked = true;

                var countChecked = 0;

                for (var item in self.CurrentPlanRecords()) {
                    if (self.CurrentPlanRecords()[item].SelectedItem())
                        countChecked++;
                }

                if (countChecked == 0) {
                    checked = false;
                }

                return checked;
            });

            // Operations/behaviour
            self.search = function (isNewSearch) {
                setInformationShortMessage("");

                //SearchValidation
                if ($("#selSearchCountry").val() == "-1") {
                    $("#selSearchCountry").addClass("ErrorV2")
                    self.VehicleSaleRecords.removeAll();
                    self.CurrentPlanRecords.removeAll();
                    return false;
                } else if (($("#FleetModelCodeTextbox").val() + $("#inputFromUnitNumber").val() + $("#inputToUnitNumber").val() + $("#inputRegNum").val()) == "") {
                    //PivotalTracker 72236034: Do not allow blank searches. 
                    //Only allow a search if there is a record in Fleet Model Code or Unit Number or Registration number. 
                    //alert("Do not allow blank searches. Only allow a search if there is a record in Fleet Model Code or Unit Number or Registration number. ");
                    setInformationShortMessage(CONST_NO_BLANK_SEARCH);
                    $("#selSearchCountry").removeClass("ErrorV2");
                    return false;
                } else {
                    $("#selSearchCountry").removeClass("ErrorV2");
                }

                //PivotalTracker 72236034: On exiting the from unit number auto populate the To unit number with the from unit number details.
                if ($("#inputFromUnitNumber").val() != "" && +$("#inputToUnitNumber").val() == "") {
                    $("#inputToUnitNumber").val($("#inputFromUnitNumber").val());
                }

                if ($("#inputFromUnitNumber").val() == "" && +$("#inputToUnitNumber").val() != "") {
                    $("#inputFromUnitNumber").val($("#inputToUnitNumber").val());
                }

                //give warnning if any vehicle sale has been modified but not created
                /*for (var item in self.VehicleSaleRecords()){
                if (self.VehicleSaleRecords()[item].Modified()){
                if (!confirm("There are some vehicle sale items haven't been created. Would you like to clear them?"))
                return false;
                }
                }*/

                //store search criteria
                if (isNewSearch) {
                    self.SearchCriteria = new SearchCriteriaDetail({
                        iFleetModelId: ($("#FleetModelIdTextbox").val() === "") ? undefined : $("#FleetModelIdTextbox").val(),
                        iCountryId: $("#selSearchCountry").val(),
                        w_unit_from: ($("#inputFromUnitNumber").val() === "") ? undefined : $("#inputFromUnitNumber").val(),
                        w_unit_to: ($("#inputToUnitNumber").val() === "") ? undefined : $("#inputToUnitNumber").val(),
                        sRegoNum: ($("#inputRegNum").val() === "") ? undefined : $("#inputRegNum").val()
                    });
                }

                $.post(CONST_URL + "GetVehicleSaleRecords", {
                    iFleetModelId: self.SearchCriteria.iFleetModelId(),
                    iCountryId: self.SearchCriteria.iCountryId(),
                    w_unit_from: self.SearchCriteria.w_unit_from(),
                    w_unit_to: self.SearchCriteria.w_unit_to(),
                    sRegoNum: self.SearchCriteria.sRegoNum(),
                    sUserCode: $.trim($("[id$=_ManufacturerIDhidden]").val())
                }, function (data) {
                    //Vehicle Sale table
                    self.VehicleSaleRecords($.map(data.VehicleSaleRecords, function (item) {
                        return new VehicleSaleRecord(item)
                    }));

                    //Current Plan table
                    self.CurrentPlanRecords($.map(data.CurrentPlans, function (item) {
                        return new CurrentPlanRecord(item)
                    }));

                    //change result style
                    $('#VehicleSaleRecords tbody tr:odd').addClass("oddBG");
                    $('#VehicleSaleRecords tbody tr:even').addClass("evenBG");

                    if (self.VehicleSaleRecords().length == 0 && self.CurrentPlanRecords().length == 0) {
                        setInformationShortMessage(CONST_NO_RESULT);
                    } else {
                        setInformationShortMessage(CONST_MODEL_WAS_FOUND);
                    }
                }, "json");
            };

            self.VehicleSaleRecordsBulkEdit = function () {
                //buttons for bulkEditVehicleSalePanel
                var selectedVehicleSaleRecordsCount = 0;

                for (var item in self.VehicleSaleRecords()) {
                    if (self.VehicleSaleRecords()[item].SelectedItem())
                        selectedVehicleSaleRecordsCount++;
                }

                if (selectedVehicleSaleRecordsCount == 0) {
                    //no selection in VehicleSaleRecords table
                    alert("Please select a unit.");
                } else {
                    $("#bulkEditVehicleSalePanel").show();
                    ShowPopBox();
                }
            }

            //vehicle sale panel
            self.bulkEditVehicleSale_OK = function () {
                //only change the local row
                //no POST to server

                var validCreate = true;

                if ($("[id*=calVehicleSaleOffFleetDate]").val() == "") {
                    $("[id*=calVehicleSaleOffFleetDate]").addClass("ErrorV2");
                    validCreate = false;
                } else {
                    $("[id*=calVehicleSaleOffFleetDate]").removeClass("ErrorV2");
                }

                if ($("#bulkEditVehicleSale_offFleetLocation").val() == -1) {
                    $("#bulkEditVehicleSale_offFleetLocation").addClass("ErrorV2");
                    validCreate = false;
                } else {
                    $("#bulkEditVehicleSale_offFleetLocation").removeClass("ErrorV2");
                }


                if (!validCreate) {
                    return false;
                }

                var that = this;

                $("#VehicleSaleRecords tbody input:checkbox").each(function (index, value) {
                    //update each modified item in self.VehicleSaleRecords
                    if (this.checked) {
                        if ($("input[id*='calVehicleSaleOffFleetDate']").val() != '')
                            self.VehicleSaleRecords()[index].OffFleetDate($("input[id*='calVehicleSaleOffFleetDate']").val());

                        if ($("#bulkEditVehicleSale_offFleetLocation").val() != -1)
                            self.VehicleSaleRecords()[index].SaleLocation($("#bulkEditVehicleSale_offFleetLocation option:selected").val());

                        if ($("#bulkEditVehicleSale_saleDealer").val() != -1) {
                            self.VehicleSaleRecords()[index].DealerId($("#bulkEditVehicleSale_saleDealer option:selected").val());
                            self.VehicleSaleRecords()[index].DealerName($("#bulkEditVehicleSale_saleDealer option:selected").text());
                        }
                        else {
                            self.VehicleSaleRecords()[index].DealerId("");
                            self.VehicleSaleRecords()[index].DealerName("");
                        }

                        self.VehicleSaleRecords()[index].Modified(true);
                    }
                });

                ResetBulkEditVehicleSale();

                $("#bulkEditVehicleSalePanel").hide();
                HidePopBox();
            };

            self.bulkEditVehicleSale_Reset = function () {
                ResetBulkEditVehicleSale();
            };

            self.bulkEditVehicleSale_Cancel = function () {
                $("#bulkEditVehicleSalePanel").hide();
                HidePopBox();
            };

            self.createSalePlan_OK = function () {
                //validate
                //count how many has been selected.
                var selectedVehicleSaleRecordsCount = 0;
                var _selectedItem;
                var validator = true;

                for (var item in self.VehicleSaleRecords()) {
                    _selectedItem = self.VehicleSaleRecords()[item];

                    if (_selectedItem.SelectedItem()) {
                        selectedVehicleSaleRecordsCount++;

                        if (_selectedItem.OffFleetDate() == "" ||
                         _selectedItem.SaleLocation() == "") {
                            validator = false;
                        }
                    }
                }

                if (selectedVehicleSaleRecordsCount == 0) {
                    //no selection in VehicleSaleRecords table
                    setErrorShortMessage("Please select a unit.");
                    window.scrollTo(0, 0);
                    return;
                }

                if (!validator) {
                    //Off fleet date, off fleet locaion and sale dealer are mandatory fields
                    setErrorShortMessage("Off Fleet Date and Off Fleet Location are mandatory.");
                    window.scrollTo(0, 0);
                    return;
                }

                //Step 1 of 3: invoke Fleet_CreateSalePlan
                $.post(CONST_URL + "CreateSalePlan", {
                    iCountryId: self.SearchCriteria.iCountryId(),
                    iFromUnitNum: self.SearchCriteria.w_unit_from(),
                    iToUnitNum: self.SearchCriteria.w_unit_to(),
                    iFleetModelID: self.SearchCriteria.iFleetModelId(),
                    dStartDate: undefined, //("[id*=calSalePlanStartDate]").val(),
                    dEndDate: undefined, //$("[id*=calSalePlanEndDate]").val(),
                    iplanquantity: selectedVehicleSaleRecordsCount,
                    sUserCode: $.trim($("[id$=_ManufacturerIDhidden]").val())
                }, function (data) {
                    //the "data" is the output iPlanId from Fleet_CreateSalePlan

                    //Step 2 of 3: invoke Fleet_CreateSalPlanLine
                    for (var item in self.VehicleSaleRecords()) {
                        if (self.VehicleSaleRecords()[item].SelectedItem()) {
                            $.ajax({
                                type: "POST",
                                url: CONST_URL + "CreateSalPlanLine",
                                data: {
                                    iCountryId: self.SearchCriteria.iCountryId(),
                                    iUnitNumber: self.VehicleSaleRecords()[item].UnitNumber(),
                                    iPlanId: data,
                                    sOffFleetLication: self.VehicleSaleRecords()[item].SaleLocation(),
                                    dOffFleetDate: self.VehicleSaleRecords()[item].OffFleetDate(),
                                    p_dealer: self.VehicleSaleRecords()[item].DealerId(),
                                    sUserCode: $.trim($("[id$=_ManufacturerIDhidden]").val())
                                },
                                datatype: "json",
                                async: false
                            });
                        }
                    }

                    //Step 3 of 3: invoke Fleet_GetVehicleSaleRecords after all CreateSalPlanLine
                    self.search(false);
                }, 'json');

            }

            self.createSalePlan_Reset = function () {
                //$("#createSalePlanPanel input[id*='calSalePlanStartDate']").val('');
                //$("#createSalePlanPanel input[id*='calSalePlanEndDate']").val('');
                if (confirm("All of your vehicle sale changes will be lost. Are you sure you want to continue?")) {
                    self.search(true);
                }

            }

            self.selectAllVehicleSaleRecords_click = function (thisItem) {
                var thisValue = !(self.AllVehicleSaleRecords());

                for (var item in self.VehicleSaleRecords()) {
                    self.VehicleSaleRecords()[item].SelectedItem(thisValue);
                }

                return true; //Allowing the default click action
            }

            //current plan panel
            self.selectAllCurrentPlans_click = function (thisItem) {
                var thisValue = !(self.AllCurrentPlans());

                for (var item in self.CurrentPlanRecords()) {
                    if (self.CurrentPlanRecords()[item].AllowSelection()) {
                        self.CurrentPlanRecords()[item].SelectedItem(thisValue);
                    }
                }

                return true; //Allowing the default click action
            }

            //buttons for bulkEditCurrentPlanPanel
            self.bulkEditCurrentPlan_Edit = function () {
                if ($("#CurrentPlanRecords tbody input:checkbox:checked").length == 0) {
                    //no selection in VehicleSaleRecords table
                    alert("Please select a unit.");
                } else {
                    $("#bulkEditCurrentPlanPanel").show();
                    ShowPopBox();
                }
            }

            self.bulkEditCurrentPlan_Delete = function () {
                //only created status can be deleted
                var selectedCurrentPlanRecordsCount = 0;

                for (var item in self.CurrentPlanRecords()) {
                    if (self.CurrentPlanRecords()[item].SelectedItem())
                        selectedCurrentPlanRecordsCount++;
                }

                if (selectedCurrentPlanRecordsCount == 0) {
                    alert("Please select a unit.");
                } else {
                    if (confirm("Only plans with created status will be deleted. The status of plans will be changed to 'Disposal-Cancelled'. Do you want to continue?")) {
                        for (var item in self.CurrentPlanRecords()) {
                            var thisItem = self.CurrentPlanRecords()[item];

                            if (thisItem.SelectedItem() && thisItem.Status() == "Created") {
                                $.post(CONST_URL + "DeleteSalePlanRecord", {
                                    iFleetAssetId: thisItem.FleetAssetId,
                                    iSaleId: thisItem.SaleId,
                                    sUsrCode: $.trim($("[id$=_ManufacturerIDhidden]").val())
                                }, function (data) {
                                }, 'json');

                                self.CurrentPlanRecords.remove(thisItem); //_destroy may not work
                            }
                        }

                        //refresh search.
                        self.search(false);
                    }
                }
            };

            self.bulkEditCurrentPlan_OK = function () {
                var that = this;

                var _calCurrentPlanOffFleetDate;
                var _offFleetLocation;
                var _saleDealer;

                for (var item in self.CurrentPlanRecords()) {
                    var thisItem = self.CurrentPlanRecords()[item];

                    if (thisItem.SelectedItem()) {
                        if ($("input[id*='calCurrentPlanOffFleetDate']").val() != '')
                            _calCurrentPlanOffFleetDate = $("input[id*='calCurrentPlanOffFleetDate']").val();

                        if ($("#bulkEditCurrentPlan_offFleetLocation").val() != -1)
                            _offFleetLocation = $("#bulkEditCurrentPlan_offFleetLocation option:selected").val();

                        if ($("#bulkEditCurrentPlan_saleDealer").val() != -1)
                            _saleDealer = $("#bulkEditCurrentPlan_saleDealer option:selected").val();

                        $.ajax({
                            type: "POST",
                            url: CONST_URL + "UpdateSaleRecord",
                            data: {
                                iSaleId: thisItem.SaleId(),
                                dOffFleetDate: _calCurrentPlanOffFleetDate,
                                sSaleLocation: _offFleetLocation,
                                iSaleDealerId: _saleDealer,
                                sUsrCode: $.trim($("[id$=_ManufacturerIDhidden]").val())
                            },
                            datatype: "json",
                            async: false
                        });
                    }
                };

                self.search(false);

                ResetBulkEditCurrentPlan();

                $("#bulkEditCurrentPlanPanel").hide();
                HidePopBox();
            };

            self.bulkEditCurrentPlan_Reset = function () {
                ResetBulkEditCurrentPlan();
            };

            self.bulkEditCurrentPlan_Cancel = function () {
                $("#bulkEditCurrentPlanPanel").hide();
                HidePopBox();
            };

            self.singleEditCurrentPlan_item = function () {
                var that = this;

                if (that.UnitNumber() != null) {
                    $("#singleEditCurrentPlan_unitNumber").val(that.UnitNumber());
                }

                if (that.RegistrationNumber() != null) {
                    $("#singleEditCurrentPlan_registrationNumber").val(that.RegistrationNumber());
                }

                if (that.ActualLocation() != null && that.ActualLocation() != "") {
                    $("#singleEditCurrentPlan_actualLocation").val(that.ActualLocation());
                }
                if (that.CurrentLocation() != null && that.CurrentLocation() != "") {
                    $("#singleEditCurrentPlan_currentLocation").val(that.CurrentLocation());
                }

                if (that.DateSold() != null) {
                    $("input[id*='singleEditCurrentPlan_calDateSold']").val(ParseDateFormat(that.DateSold()));
                }

                if (that.PickupDate() != null) {
                    $("input[id*='singleEditCurrentPlan_calPickupDate']").val(ParseDateFormat(that.PickupDate()));
                }

                if (that.CustomerType() != null && that.CustomerType() != "") {
                    $("#singleEditCurrentPlan_customerType").val(that.CustomerType()); //TODO
                }

                if (that.CustomerName() != null && that.CustomerName() != "") {
                    $("#singleEditCurrentPlan_customerName").val(that.CustomerName());
                }

                if (that.DealerId() != null && that.DealerId() != 0) {
                    $("#singleEditCurrentPlan_dealerName").val(that.DealerId());
                }

                if (that.SettleMethod() != null && that.SettleMethod() != "") {
                    $("#singleEditCurrentPlan_paymentMethod").val(that.SettleMethod()); //TODO
                }

                if (that.SettleBankDate() != null) {
                    $("input[id*='singleEditCurrentPlan_calBankDate']").val(ParseDateFormat(that.SettleBankDate()));
                }

                if (that.SettleLocation() != null && that.SettleLocation() != "") {
                    $("#singleEditCurrentPlan_settleLocation").val(that.SettleLocation());
                }

                //rev:mia 14nov2014 start - added RRP --------
                if (that.RRP() != null && that.RRP() != "") {
                    $("#singleEditCurrentPlan_rrp").val(that.RRP())
                }
                //rev:mia 14nov2014 end   - added RRP --------		


                $("#singleEditCurrentPlan_comments").val(that.SaleComment());

                //load Processed Steps
                $.ajax({
                    type: "POST",
                    url: CONST_URL + "GetProcessedSteps",
                    data: {
                        iFleetAssetId: that.FleetAssetId()
                    },
                    success: function (data) {
                        self.ProcessedSteps($.map(data.ProcessSteps, function (item) {
                            return new ProcessedStep(item)
                        }));

                        $('#ProcessStepsTable tbody tr:odd').addClass("oddBG");
                        $('#ProcessStepsTable tbody tr:even').addClass("evenBG");

                        //display history
                        var historyText = "";

                        for (var hText in data.HistoryItems) {
                            historyText += data.HistoryItems[hText].History + "\r\n\r\n";
                        }

                        $("#singleEditCurrentPlan_history").val(historyText);
                    },
                    dataType: "json",
                    async: false
                });

                //enable or disable save and release buttons
                if (that.Status().indexOf("Disp") >= 0 || that.Status().indexOf("Released") >= 0) {
                    $("#butSingleEditCurrentPlan_Save").addClass("buttondisabled disabled").attr("disabled", "disabled");
                } else {
                    if (isPartialAccess == true) {
                        $("#butSingleEditCurrentPlan_Save").removeClass("buttondisabled").removeClass('disabled').removeAttr('disabled');
                    }
                    else if (isViewAccess == true) {
                        $("#butSingleEditCurrentPlan_Save").addClass("buttondisabled disabled").attr("disabled", "disabled");
                    }
                    else {
                        $("#butSingleEditCurrentPlan_Save").removeClass("buttondisabled").removeClass('disabled').removeAttr('disabled');
                    }
                }

                if (that.Status().indexOf("Released") >= 0) {
                    $("#butSingleEditCurrentPlan_OK").addClass("buttondisabled").attr("disabled", "disabled");
                } else {
                    if (isPartialAccess == true || isViewAccess == true) {
                        $("#butSingleEditCurrentPlan_OK").addClass("buttondisabled disabled").attr("disabled", "disabled");
                    }
                    else {
                        $("#butSingleEditCurrentPlan_OK").removeClass('disabled');
                    }
                    //$("#butSingleEditCurrentPlan_OK").removeClass('disabled');
                }

                //show hide mandatory icon for dealer name
                if (that.CustomerType() != null && that.CustomerType().indexOf("Dealer") >= 0) {
                    $("#mandatoryDealerNameIndicator").show();
                } else {
                    $("#mandatoryDealerNameIndicator").hide();
                }

                CurrentEditingPlanRecord = that;

                $("#singleEditCurrentPlanPanel").show();
                ShowPopBox();
            };

            self.singleEditCurrentPlan_Save = function (thisPlan) {
                //Save button
                //bRelease = 0
                if ($("#selSearchCountry").val() == "" || $("#selSearchCountry").val() == undefined) {
                    setErrorShortMessage("Country parameter is missing.");
                    return false
                }

                //rev:mia 14nov2014 start - added RRP --------
                var rrp = 0;
                if ($("#singleEditCurrentPlan_rrp").val() != '') {
                    rrp = $("#singleEditCurrentPlan_rrp").val();
                    if (!IsNumeric(rrp)) {
                        $("#singleEditCurrentPlan_rrp").addClass("ErrorV2")
                        return false;
                    }
                    else
                        $("#singleEditCurrentPlan_rrp").removeClass("ErrorV2")
                }
                //rev:mia 14nov2014 end   - added RRP --------	

                RemoveBorder()

                UpdateSalesPlan(self, 0);
            };

            self.singleEditCurrentPlan_OK = function (thisPlan) {
                //Release button
                //bRelease = 1
                var validCreate = true;

                if ($("#singleEditCurrentPlan_currentLocation").val() == "-1") {
                    $("#singleEditCurrentPlan_currentLocation").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_currentLocation").removeClass("ErrorV2")
                }

                if ($("#singleEditCurrentPlan_actualLocation").val() == "-1") {
                    $("#singleEditCurrentPlan_actualLocation").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_actualLocation").removeClass("ErrorV2")
                }

                if ($("[id*=singleEditCurrentPlan_calDateSold]").val() == "") {
                    //$("[id*=singleEditCurrentPlan_calDateSold]").parent().next("label").show();
                    $("[id*=singleEditCurrentPlan_calDateSold]").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("[id*=singleEditCurrentPlan_calDateSold]").removeClass("ErrorV2")
                }

                if ($("[id*=singleEditCurrentPlan_calPickupDate]").val() == "") {
                    //$("[id*=singleEditCurrentPlan_calPickupDate]").parent().next("label").show();
                    $("[id*=singleEditCurrentPlan_calPickupDate]").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("[id*=singleEditCurrentPlan_calPickupDate]").removeClass("ErrorV2")
                }

                if ($("#singleEditCurrentPlan_customerType").val() == "-1") {
                    //$("#singleEditCurrentPlan_customerType").next("label").show();
                    $("#singleEditCurrentPlan_customerType").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_customerType").removeClass("ErrorV2")
                }

                if ($("#singleEditCurrentPlan_customerName").val() == "") {
                    //$("#singleEditCurrentPlan_customerName").next("label").show();
                    $("#singleEditCurrentPlan_customerName").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_customerName").removeClass("ErrorV2")
                }

                if ($("#singleEditCurrentPlan_customerType").val().indexOf("Dealer") >= 0 &&
                    $("#singleEditCurrentPlan_dealerName").val() == "-1") {
                    $("#singleEditCurrentPlan_dealerName").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_dealerName").removeClass("ErrorV2")
                }

                if ($("#singleEditCurrentPlan_paymentMethod").val() == "-1") {
                    $("#singleEditCurrentPlan_paymentMethod").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_paymentMethod").removeClass("ErrorV2")
                }

                if ($("[id*=singleEditCurrentPlan_calBankDate]").val() == "") {
                    //$("[id*=singleEditCurrentPlan_calBankDate]").parent().next("label").show();
                    $("[id*=singleEditCurrentPlan_calBankDate]").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("[id*=singleEditCurrentPlan_calBankDate]").removeClass("ErrorV2")
                }

                if ($("#singleEditCurrentPlan_settleLocation").val() == "-1") {
                    $("#singleEditCurrentPlan_settleLocation").addClass("ErrorV2")
                    validCreate = false;
                } else {
                    $("#singleEditCurrentPlan_settleLocation").removeClass("ErrorV2")
                }

                //rev:mia 14nov2014 start - added RRP --------
                var rrp = 0;
                if ($("#singleEditCurrentPlan_rrp").val() != '') {
                    rrp = $("#singleEditCurrentPlan_rrp").val();
                    if (!IsNumeric(rrp)) {
                        $("#singleEditCurrentPlan_rrp").addClass("ErrorV2")
                        validCreate = false;
                    }
                    else
                        $("#singleEditCurrentPlan_rrp").removeClass("ErrorV2")
                }
                //rev:mia 14nov2014 end   - added RRP --------	


                //dealer is mandatory if customer tpe is "Dealer"

                if (!validCreate) {
                    return false;
                }


                RemoveBorder()

                if (confirm("This will irreversibly remove vehicle from Fleet. Do you wish to proceed?")) {
                    UpdateSalesPlan(self, 1);
                }
            };

            self.singleEditCurrentPlan_Reset = function () {
                clearSingleEditCurrentPlanFields();
                RemoveBorder()
            };

            self.singleEditCurrentPlan_Cancel = function () {
                clearSingleEditCurrentPlanFields();
                RemoveBorder()
                $("#singleEditCurrentPlanPanel").hide();
                HidePopBox();
            };

            //self.search(false);
        }
        //[END] MVVM

        ko.applyBindings(new AllVehicleSaleRecordsViewModel());
    });
} (jQuery, this, this.document));


function RemoveBorder() {
    $("#singleEditCurrentPlan_currentLocation").removeClass("ErrorV2")
    $("#singleEditCurrentPlan_actualLocation").removeClass("ErrorV2")
    $("[id*=singleEditCurrentPlan_calDateSold]").removeClass("ErrorV2")
    $("[id*=singleEditCurrentPlan_calPickupDate]").removeClass("ErrorV2")
    $("#singleEditCurrentPlan_customerType").removeClass("ErrorV2")
    $("#singleEditCurrentPlan_customerName").removeClass("ErrorV2")
    $("#singleEditCurrentPlan_dealerName").removeClass("ErrorV2")
    $("#singleEditCurrentPlan_paymentMethod").removeClass("ErrorV2")
    $("[id*=singleEditCurrentPlan_calBankDate]").removeClass("ErrorV2")
    $("#singleEditCurrentPlan_settleLocation").removeClass("ErrorV2")
    
    //rev:mia 14nov2014 start - added RRP --------
    $("#singleEditCurrentPlan_rrp").removeClass("ErrorV2")
    //rev:mia 14nov2014 end   - added RRP --------		
}

//rev:mia 14nov2014 start - added RRP --------
function IsNumeric (obj) {
    return !jQuery.isArray(obj) && (obj - parseFloat(obj) + 1) >= 0;
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
    j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
//rev:mia 14nov2014 end   - added RRP --------		

