﻿var CONST_URL = "AIMSListener/FleetSaleDisposalPlanListener.aspx?MODE="; // "AIMSListener/FleetSaleDisposalPlan/";
var CONST_URL_FLEETMODELMGT = "AIMSListener/AIMSListener.aspx?MODE=";
var validator;
var CONST_MODEL_WAS_FOUND = 'AIMS Information was loaded.';
var CONST_NO_BLANK_SEARCH = 'Please input an valid unit number, registration number or fleet model code.';
var CONST_NO_RESULT = 'No Results Found';

//auto complete for fleet model code
function FleetModelCodeAutoComplete(fleetModelInfoList) {
    var _source = $.map(fleetModelInfoList, function (item) {
        return {
            label: item.FleetInformation,
            value: item.FleetModelId
        }
    });


    $("#FleetModelCodeTextbox").autocomplete({
        //source: GetFleetModelCodesAutoSuggest,
        source: _source,
        messages: {
            noResults: '',
            results: function () { }
        },
        select: function (event, ui) {
            if (ui.item) {
                $("#FleetModelIdTextbox").val(ui.item.value);
                this.value = ui.item.label.split('-')[0];
                return false;
            }
        },
        change: function (event, ui) {
            if (!ui.item) {
                //no selection
                $("#FleetModelIdTextbox").val("");
                this.value = "";
            }
        }, focus: function (event, ui) {
            event.preventDefault();
            this.value = ui.item.label.split('-')[0];
        }
    });
}

function LoadOffFleetLocation(locationList) {
    $(".offFleetLocation").html('');
    $(".offFleetLocation").append("<option value= '-1'>--please select--</option>");

    $.each(locationList, function (index, item) {
        $(".offFleetLocation").append("<option value= '" + item.LocationCode + "'>" + item.LocationCode + " - " + item.LocationName + "</option>");
    });
}

function LoadSaleDealer(dealerList) {
    $(".saleDealer").html('')
    $(".saleDealer").append("<option value= '-1'>--please select--</option>");

    $.each(dealerList, function (index, item) {
        //$(".saleDealer").append("<option value= '" + item.DealerId + "'>" + item.DealerCode + " - " + item.DealerName + "</option>");
        //Updated by Nimesh on 15th June 2015 to remove dealercode from the display - https://thlonline.atlassian.net/browse/DSD-782
        $(".saleDealer").append("<option value= '" + item.DealerId + "'>" + item.DealerName + "</option>");
    });
}

function LoadPaymentMethod(paymentMethods) {
    $(".paymentMethod").html("");
    $(".paymentMethod").append("<option value= '-1'>--please select--</option>");

    $.each(paymentMethods, function (index, item) {
        $(".paymentMethod").append("<option value= '" + item.ReferenceCode + "'>" + item.ReferenceDescription + "</option>");
    });
}

function LoadCustomerType(customerTypes) {
    $(".customerType").html("");
    $(".customerType").append("<option value= '-1'>--please select--</option>");

    $.each(customerTypes, function (index, item) {
        $(".customerType").append("<option value= '" + item.CustomerTypeCode + "'>" + item.CustomerTypeCode + " - " + item.CustomerTypeDesc + "</option>");
    });
}

//
function LoadCurrentLocationList(currentLocationList) {
    $("#singleEditCurrentPlan_currentLocation").html("");
    $("#singleEditCurrentPlan_currentLocation").append("<option value= '-1'>--please select--</option>");

    $.each(currentLocationList, function (index, item) {
        $("#singleEditCurrentPlan_currentLocation").append("<option value= '" + item.LocationId + "'>" + item.LocationCode + " - " + item.LocationName + "</option>");
    });
}

function LoadActualLocationList(actualLocationList) {
    $("#singleEditCurrentPlan_actualLocation").html("");
    $("#singleEditCurrentPlan_actualLocation").append("<option value= '-1'>--please select--</option>");

    $.each(actualLocationList, function (index, item) {
        $("#singleEditCurrentPlan_actualLocation").append("<option value= '" + item.PreSaleActualLocId + "'>" + item.LocationCode + " - " + item.Description + "</option>");
    });
}

//this function should use KO option binding
function GetDropdownForCountries() {
    $.ajax({
        type: "POST",
        url: CONST_URL + "GetDropdownForSalePlan",
        data: "",
        success: function (data) {
            if (data.CountryList !== undefined) {
                $("#selSearchCountry").append("<option value= '-1'>--please select--</option>");

                $.each(data.CountryList, function (index, country) {
                    $("#selSearchCountry").append("<option value= '" + country.CountryId + "'>" + country.CountryCode + " - " + country.CountryDescription + "</option>");
                });
            }
        },
        dataType: "json",
        async: false
    });
}

function GetDropdownForSalePlan(_iCountryId) {
    $.ajax({
        type: "POST",
        url: CONST_URL + "GetDropdownForSalePlan",
        data: { iCountryId: _iCountryId },
        success: function (data) {
            if (data.FleetModelInfoList !== undefined)
                FleetModelCodeAutoComplete(data.FleetModelInfoList);

            if (data.LocationList !== undefined)
                LoadOffFleetLocation(data.LocationList);

            if (data.DealerList !== undefined)
                LoadSaleDealer(data.DealerList);

            if (data.PaymentMethodList !== undefined)
                LoadPaymentMethod(data.ReferenceList); //PaymentMethodList

            if (data.CustomerTypeList !== undefined)
                LoadCustomerType(data.CustomerTypeList);

            if (data.CurrentLocationList !== undefined)
                LoadCurrentLocationList(data.CurrentLocationList);

            if (data.ActualLocationList !== undefined)
                LoadActualLocationList(data.ActualLocationList);
        },
        dataType: "json",
        async: false
    });
}

function ResetBulkEditVehicleSale() {
    $("input[id*='calVehicleSaleOffFleetDate']").val('');
    $("[id*=calVehicleSaleOffFleetDate]").parent().next("label").hide();

    $("#bulkEditVehicleSale_offFleetLocation").val('-1');
    $("#bulkEditVehicleSale_offFleetLocation").next("label").hide();

    $("#bulkEditVehicleSale_saleDealer").val('-1');
    $("#bulkEditVehicleSale_saleDealer").next("label").hide();

}

function ResetBulkEditCurrentPlan() {
    $("input[id*='calCurrentPlanOffFleetDate']").val('');
    $("#bulkEditCurrentPlan_offFleetLocation").val('-1');
    $("#bulkEditCurrentPlan_saleDealer").val('-1');
}

function clearSingleEditCurrentPlanFields() {
    $("#singleEditCurrentPlanPanel input").val("");
    $("#singleEditCurrentPlanPanel select").val("-1");
    $("#singleEditCurrentPlanPanel label").hide();
}

function UpdateSalesPlan(self, _bRelease) {
    $.ajax({
        type: "POST",
        url: CONST_URL + "UpdateSalesPlan",
        data: {
            iCountryId: self.SearchCriteria.iCountryId(),
            p_fleet_asset_id: CurrentEditingPlanRecord.FleetAssetId(),
            dDateSold: $("[id*=singleEditCurrentPlan_calDateSold]").val(), //CurrentEditingPlanRecord.DateSold(),
            dPickupDate: $("[id*=singleEditCurrentPlan_calPickupDate]").val(), //CurrentEditingPlanRecord.PickupDate(),
            sCustomerType: $("#singleEditCurrentPlan_customerType").val(), //CurrentEditingPlanRecord.CustomerType(),
            iSalePrice: '000',
            iSalePriceIncTax: '000',
            iSalesTaxPercent: '000',
            iDealerId: $("#singleEditCurrentPlan_dealerName").val(), //CurrentEditingPlanRecord.DealerId(),
            sCustomerName: $("#singleEditCurrentPlan_customerName").val(), //CurrentEditingPlanRecord.CustomerName(),
            sSettleMethod: $("#singleEditCurrentPlan_paymentMethod").val(), //CurrentEditingPlanRecord.SettleMethod(),
            dSettleBankDate: $("[id*=singleEditCurrentPlan_calBankDate]").val(), //CurrentEditingPlanRecord.SettleBankDate(),
            sSettleLocation: $("#singleEditCurrentPlan_settleLocation").val(), //CurrentEditingPlanRecord.SettleLocation(),
            sSaleComment: $("#singleEditCurrentPlan_comments").val(), //CurrentEditingPlanRecord.SaleComment(),
            bRelease: _bRelease,
            iPreSaleActualLocId: $("#singleEditCurrentPlan_actualLocation").val(),
            iCurrentLocationId: $("#singleEditCurrentPlan_currentLocation").val(),
            dModifiedDate: (new Date()).format("dd/MM/yyyy"), //CurrentEditingPlanRecord.ModifiedDate(),
            sModifiedBy: $.trim($("[id$=_ManufacturerIDhidden]").val()), //CurrentEditingPlanRecord.ModifiedBy(), 
            iSaleId: CurrentEditingPlanRecord.SaleId()
            //rev:mia 14nov2014 start - added RRP --------
            , rrp: $.trim($("#singleEditCurrentPlan_rrp").val())
            //rev:mia 14nov2014 end   - added RRP --------	
        },
        dataType: "application/json",
        success: function (data) {
        },
        async: true
    });

    $.each(self.ProcessedSteps(), function(index,item){
        if (item.Completed() != item.OriginalCompletedValue()) {
            $.ajax({
                type: "POST",
                url: CONST_URL + "SaveProcessSteps",
                data:{
                    iProcessStepsId: (item.ProcessStepsId() == null) ? undefined : item.ProcessStepsId(),
                    iFleetAssetId : CurrentEditingPlanRecord.FleetAssetId(),
                    iProcessId: item.ProcessId(),
                    sAddUserCode : $.trim($("[id$=_ManufacturerIDhidden]").val()),
                    bCompleted: (item.Completed() === "" || !item.Completed()) ? 0 : 1
                },
                dataType: "json",
                success: function (data){
                },
                async: false
            });
        }
        if (item.ProcessedDate() != item.OriginalProcessedDate()) {
            $.ajax({
                type: "POST",
                url: CONST_URL + "SaveProcessStepsDate",
                data: {
                    iProcessStepsId: (item.ProcessStepsId() == null) ? undefined : item.ProcessStepsId(),
                    iFleetAssetId: CurrentEditingPlanRecord.FleetAssetId(),
                    iProcessId: item.ProcessId(),
                    sAddUserCode: $.trim($("[id$=_ManufacturerIDhidden]").val()),
                    dProcessedDate: item.ProcessedDate()
                },
                dataType: "json",
                success: function (data) {
                },

                async: false
            });
            //alert('Save this value nimesh' + item.ProcessId() + ' oldvalue ' + item.OriginalProcessedDate() + ' with new value ' + item.ProcessedDate());
        }
    });

    clearSingleEditCurrentPlanFields();

    self.search(false);

    $("#singleEditCurrentPlanPanel").hide();
    HidePopBox();
}