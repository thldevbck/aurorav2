﻿// Class to represent a row in the Vehicle Sale table
function VehicleSaleRecord(data) {
    var self = this;

    self.SelectedItem = ko.observable(false);
    self.CountryId = ko.observable(data.CountryId);
    self.FleetAssetId = ko.observable(data.FleetAssetId);
    self.UnitNumber = ko.observable(data.UnitNumber);
    self.RegistrationNumber = ko.observable(data.RegistrationNumber);
    self.FleetModelCode = ko.observable(data.FleetModelCode); //Fleet model code
    self.IdCompliancePlateMonth = ko.observable(data.IdCompliancePlateMonth);
    self.IdCompliancePlateYear = ko.observable(data.IdCompliancePlateYear);
    self.FleetModelId = ko.observable(data.FleetModelId);
    self.Manufacturer = ko.observable(data.Manufacturer); //Manufacture
    self.ManufacturerId = ko.observable(data.ManufacturerId);
    self.OffFleetDate = ko.observable(FormatDate(data.OffFleetDate));
    self.SaleLocation = ko.observable(data.SaleLocation);
    self.DealerName = ko.observable(data.DealerName);
    self.DealerId = ko.observable(data.DealerId);
    self.Odometer = ko.observable(data.Odometer);
    self.AssetId = ko.observable(data.AssetId);
    self.AssetComponentNr = ko.observable(data.AssetComponentNr);
    self.CompanyId = ko.observable(data.CompanyId);
    self.Modified = ko.observable(false);
    
    //rev:mia 11Aug2014 - display if isPartialAccess = true or isViewAccess = true
    self.isVisible = ko.observable((isPartialAccess == true || isViewAccess == true ? false : true));
}

//Class to represent a row in the Current Plan table
function CurrentPlanRecord(data) {
    var self = this;

    self.SelectedItem = ko.observable(false);
    self.SaleId = ko.observable(data.SaleId);
    self.UnitNumber = ko.observable(data.UnitNumber);
    self.RegistrationNumber = ko.observable(data.RegistrationNumber);
    self.FleetModelCode = ko.observable(data.FleetModelCode);
    self.Manufacturer = ko.observable(data.Manufacturer);
    self.OffFleetDate = ko.observable(FormatDate(data.OffFleetDate));
    self.SaleLocation = ko.observable(data.SaleLocation);
    self.DealerName = ko.observable(data.DealerName);
    self.DealerId = ko.observable(data.DealerId);
    self.PlanNumber = ko.observable(data.PlanNumber);
    self.Status = ko.observable(data.Status);
    self.ModifiedBy = ko.observable(data.ModifiedBy);
    self.ModifiedDate = ko.observable(FormatDate(data.ModifiedDate));
    self.DateSold = ko.observable(data.DateSold);
    self.PickupDate = ko.observable(data.PickupDate);
    self.CustomerType = ko.observable(data.CustomerType);
    self.CustomerName = ko.observable(data.CustomerName);
    self.SettleMethod = ko.observable(data.SettleMethod);
    self.SettleBankDate = ko.observable(data.SettleBankDate);
    self.SettleLocation = ko.observable(data.SettleLocation);
    self.SaleComment = ko.observable(data.SaleComment);
    self.FleetAssetId = ko.observable(data.FleetAssetId);
    self.ActualLocation = ko.observable(data.ActualLocation);
    self.CurrentLocation = ko.observable(data.CurrentLocation);
    //rev:mia 14nov2014 start - added RRP --------
    self.RRP   = ko.observable(data.RRP);
    //rev:mia 14nov2014 end   - added RRP --------		


    self.ModifiedCurrentPlanRowColor = ko.computed(function () {
        //{ modifiedCurrentPlanRow_Gray: Status() == 'Disp' || Status() == 'Released', modifiedCurrentPlanRow_Yellow: Status() == 'Created', modifiedCurrentPlanRow_Green: Status() == 'Created' }
        if (self.Status().indexOf("Disp") >= 0 || self.Status().indexOf("Released") >= 0) {
            return "modifiedCurrentPlanRow_Gray";
        } else if (self.Status().indexOf("Created") >= 0) {

            if (self.OffFleetDate() != null) {
                var dateStringArray = self.OffFleetDate().split('/'); //24/04/2014

                if (dateStringArray.length >= 3 &&
                    new Date(dateStringArray[2] + "/" + dateStringArray[1] + "/" + dateStringArray[0]) > new Date()) {
                    return "modifiedCurrentPlanRow_Yellow";
                }
            }
            return "modifiedCurrentPlanRow_Green";
        }
    });

    self.AllowSelection = ko.computed(function () {
        //rev:mia 11Aug2014 - disabled if isPartialAccess = false
        if (isFullAccess == false && ( isPartialAccess == true || isViewAccess == true)) {
            return false;
        }

        if (self.Status().indexOf("Disp") >= 0 || self.Status().indexOf("Released") >= 0) {
            return false;
        }

        return true;
    });
}

function ProcessedStep(data) {
    var self = this;
    ko.validation.rules['simpleDate'] = {
        validator: function (val) {
            if (!val.match(/^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.](201)[2-9]{1})$/)) {
                self.ShowProcessedDate(self.OriginalProcessedDate());
                return false;
            }
        },
        message: 'Invalid date'
    };
    var dateValidator = function (val) {
        //if ($(".validationMessage").length > 0)
        //    $(".validationMessage").each(function () {
        //        var a = $(this)[0].outerHTML;
        //    });
        $(".validationMessage").css('color', 'red');
        if(val != null)
            //if (!val.match(/^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.](201)[2-9]{1})$/)) {
            //if (!val.match(/((([0][1-9]|[12][\d])|[3][01])[-/]([0][13578]|[1][02])[-/][1-9]\d\d\d)|((([0][1-9]|[12][\d])|[3][0])[-/]([0][13456789]|[1][012])[-/][1-9]\d\d\d)|(([0][1-9]|[12][\d])[-/][0][2][-/][1-9]\d([02468][048]|[13579][26]))|(([0][1-9]|[12][0-8])[-/][0][2][-/][1-9]\d\d\d)/)) {
            if (!val.match(/^([0]?[1-9]|[1|2][0-9]|[3][0|1])[./-]([0]?[1-9]|[1][0-2])[./-]([0-9]{4}|[0-9]{2})$/)) {
                //self.ProcessedDate(self.OriginalProcess
                //edDate());
                $("#butSingleEditCurrentPlan_Save").addClass("buttondisabled disabled").attr("disabled", "disabled");
                validationMessage = ' Invalid Date';
                return false;
            }
            else {
                //var rightnow = new Date();
                //var valSplit = val.split('/');
                //var valDate = new Date(valSplit[2], valSplit[1] -1, valSplit[0]);
                //if (valDate > rightnow) {
                //    $("#butSingleEditCurrentPlan_Save").addClass("buttondisabled disabled").attr("disabled", "disabled");
                //    validationMessage = 'Date can not be in future';
                //    return false;
                //}
                //else {
                    $("#butSingleEditCurrentPlan_Save").removeClass("buttondisabled disabled").removeAttr("disabled");
                    return true;
                //}
            }

    }
    var validationMessage = '';
    self.ProcessId = ko.observable(data.ProcessId);
    self.Description = ko.observable(data.Description);
    self.ProcessedBy = ko.observable(data.ProcessedBy);
    
    self.ProcessedDate = ko.observable(FormatDate(data.ProcessedDate));
    self.Completed = ko.observable(data.Completed);
    self.ProcessStepsId = ko.observable(data.ProcessStepsId);
    self.OriginalCompletedValue = ko.observable(data.Completed);
    //rev:mia 11Aug2014 - display if isPartialAccess = true or isViewAccess = true
    self.isEnable = ko.observable((isViewAccess == true ? false : true));
    //rev:nyt 3rdNov2015 - display if Completed
    self.Display = ko.observable(isFullAccess == true);
    self.OriginalProcessedDate = ko.observable(FormatDate(data.ProcessedDate));
    self.ShowProcessedDate = ko.computed(function () {
        var len = 0;
        var retValue = false;
        if (typeof self.OriginalProcessedDate() === 'string')
            //return 0;
            len = self.OriginalProcessedDate().length;
        if (len > 0)
            retValue = true;
        return retValue;
    });
    //self.ProcessedDate = ko.observable();
    //self.ProcessedDate = ko.computed({
    //    read: function () { return FormatDate(data.ProcessedDate); },
    //    write: function (val) {
    //        if (!val.match(/^(0[1-9]|[12][0-9]|3[01])[\- \/.](?:(0[1-9]|1[012])[\- \/.](201)[2-9]{1})$/)) {
               
    //            self._ProcessedDate = self.OriginalProcessedDate();
    //            //self.ProcessedDate(self.OriginalProcessedDate());
    //            //ko.utils.arrayPushAll(self.Notifications(), itemModels);
                
    //            return;
    //        }
            
    //        // other code here
    //    }
    //});
    //ko.validation.rules.pattern.message = 'Invalid.';

    //ko.validation.configure({
    //    registerExtenders: true,
    //    messagesOnModified: true,
    //    insertMessages: true,
    //    parseInputAttributes: true,
    //    messageTemplate: null
    //});
    var validationOptions = {
        insertMessages: true,
        decorateElement: false,
        errorElementClass: 'errorFill'
    };
    ko.validation.init(validationOptions);
    //self.ProcessedDate.extend({ required: true });
    self.ProcessedDate.extend({ validation: { validator: dateValidator, message: function () { return validationMessage; } } });
    ko.validation.registerExtenders();
}

function SearchCriteriaDetail(data) {
    var self = this;

    self.iFleetModelId = ko.observable(data.iFleetModelId);
    self.iCountryId = ko.observable(data.iCountryId);
    self.w_unit_from = ko.observable(data.w_unit_from);
    self.w_unit_to = ko.observable(data.w_unit_to);
    self.sRegoNum = ko.observable(data.sRegoNum);
}