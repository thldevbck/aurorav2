﻿//----CLEAR FleetModelDiv----
function clearFleetModelDiv(mode) {
    
    $("#FleetModelDiv input[type=text]").val("")
    $("#FleetModelDiv input[type=checkbox]").removeAttr("checked");


    switch (mode) {
        case Mode.ClearAll:
            $("[id$=_FleetModelCodeTextbox]").val("")
            $("#vehiclePlans").empty()
            $("#AllocatedUnitNumber").empty()
            $("#ManufacturerModel").empty()
            $("#FleetModelsuggestionResultV2").empty()
            $("#FleetModelDiv").hide()
            $("#VTCodelabel").html("")
            $("#FleetModelChangeLabel").html("")

            $("[id$=_UnitNumberhidden]").val("")
            $("[id$=_ModelCodeshidden]").val("")
            break;

        case Mode.Add:
            clearMessages()
            isAdd = true;
            $("#FleetModeltable").removeAttr("style")
            $("[id$=_FleetModelCodeTextbox]").val("")
            $("#FleetModelDiv").show()
            
            $("#vehiclePlans").empty()
            $("#AllocatedUnitNumber").empty()
            $("#ManufacturerModel").empty()
            $("#FleetModelsuggestionResultV2").empty()
            $("#VTCodelabel").html("")
            $("#FleetModelChangeLabel").html("")
            ButtonGroupingStatus(false)
            break;

        case Mode.Change:
            $("#FleetModeltable").attr("style", "visibility:none")
            $("#vehiclePlans").empty()
            $("#AllocatedUnitNumber").empty()
            $("#ManufacturerModel").empty()
            $("#FleetModelDiv").hide()
           
            break;

        case Mode.Cancel:
            clearMessages()
            $("#FleetModeltable").attr("style", "visibility:none")
            $("#FleetModelDiv").hide()
            break;

        case Mode.AddManufacturerModel:
            $("#EditManufacturerModelTable input[type=text]").val("")
            $("#EditManufacturerModelTable select").val("-1")
            break;

        case Mode.Load:
            isAdd = false;
            $("#FleetModeltable").removeAttr("style")
            $("#ManufacturerModel").empty();
            $("#AllocatedUnitNumber").empty();
            $("#vehiclePlans").empty();
            $("#FleetModelDiv").show()
            break;

    }
}


//CALLED BY GetSingleFleetModelCode AND GetFleetModelDetails AJAX REQUEST
function fxn_GetSingleFleetModelCode(fleetmodel, ctr) {
    if (fleetmodel.length == 0) {
        clearFleetModelDiv(Mode.Add);//empty everything
        return false;
    }

    fleetModelId = fleetmodel[ctr].FleetModelId
        
    if (fleetmodel[ctr].VTCode != null) {
        $("#VTCodelabel").html("<b>" + fleetmodel[ctr].VTCode + "</b>")
    }
    else
        $("#VTCodelabel").html("")


    if (fleetmodel[ctr].FleetModelChange != null) {
        $("#FleetModelChangeLabel").html(fleetmodel[ctr].FleetModelChange + "&nbsp;")
    }
    else
        $("#FleetModelChangeLabel").html("")

    $("#FleetModelCodeDataTextbox").val(fleetmodel[ctr].FleetModelCode);

    $("#FleetModelDescriptionDataTextbox").val(htmlDecode(fleetmodel[ctr].FleetModelDescription));

    if (fleetmodel[ctr].ConversionIndicator == "1") {
        $("#ConversionRequiredCheckbox").prop('checked', true)
    }
    else {
        $("#ConversionRequiredCheckbox").prop('checked', false)
    }

    if (fleetmodel[ctr].IsLocal == "1") {
        $("#isLocalcheckbox").prop('checked', true)
    }
    else {
        $("#isLocalcheckbox").prop('checked', false)
    }
    
    $("#DailyRunningCosttextBox").val(fxn_currencyformatted(fleetmodel[ctr].DailyRunningCost));
    $("#DailyDelaytextBox").val(fxn_currencyformatted(fleetmodel[ctr].DailyDelayCost));

    $("#ctl00_ContentPlaceHolder_inactiveDatePicker_dateTextBox").val(fleetmodel[ctr].InActiveDate)
}


//CALLED BY LoadDataForManufacturerModel AJAX REQUEST
function NewEditManufacturerGrid(manufacturers,
                                countries,
                                manufacturermodels,
                                exteriors,
                                fuels,
                                transmissions,
                                schedules) {

    option = "";
    //not working inside citrix so use the old javascript way
    if ($("#CountrySelect option").length - 1 == 0) {
        $.each(countries, function (index, country) {
            //$("#CountrySelect").append($('<option>', { value: country.CountryId, text: country.CountryDescription }));
            option = option + AddOptionItem(country.CountryId, country.CountryDescription)
        });
        $("#CountrySelect").append(option);
    }
    CountryId = $("#CountryDropdown option:selected").val();
    $("#CountrySelect").val(parseInt(CountryId))

    option = "";
    if ($("#ManufacturerSelect option").length - 1 == 0) {
        $.each(manufacturers, function (index, manufacturer) {
            option = option + AddOptionItem(manufacturer.ManufacturerId, manufacturer.Manufacturer)
        });
        $("#ManufacturerSelect").append(option);
    }

    option = "";
    if ( $("#fueltypeSelect option").length - 1 == 0) {
        $.each(fuels, function (index, fuel) {
            option = option + AddOptionItem(fuel.ReferenceCode, fuel.ReferenceDescription)
        });
        $("#fueltypeSelect").append(option)
    }

    option = "";
    if ( $("#TransmissionSelect option").length - 1 == 0) {
        $.each(transmissions, function (index, transmission) {
            option = option + AddOptionItem(transmission.ReferenceCode,transmission.ReferenceDescription)
        });
        $("#TransmissionSelect").append(option);
    }

    option = "";
    if ($("#ServiceScheduleSelect option").length - 1 == 0) {
        $.each(schedules, function (index, schedule) {
            option = option + AddOptionItem(schedule.serviceScheduleId, schedule.Name)
        });
        $("#ServiceScheduleSelect").append(option);
    }
    option = "";
}

//CALLED BY GetFleetModelDetails AJAX REQUEST
function fxn_vehiclePlans(vehiclePlans) {
    if (vehiclePlans.length == 0) {
        $("#vehiclePlans").empty();
        return false;
    }

    var itemrow = "<table class='mrrDataTable' width='100%'>"
    itemrow += "<tr VALIGN='TOP'>";
    itemrow += "<td width='30px;'>Plan #.</td>";
    itemrow += "<td width='20px;'>Plan Date</td>";
    itemrow += "<td width='30px;'>Plate</BR>Year</td>";
    itemrow += "<td width='20px;'>Status</td>";
    itemrow += "<td width='20px;'>Qty</td>";
    itemrow += "<td width='30px;'>From</br>Unit</td>";
    itemrow += "<td width='30px;'>To</br>Unit</td>";
    itemrow += "<td width='40px;'>Delivery</br>from</br>date</td>";
    itemrow += "<td width='40px;'>Fleet</br>Model</td>";
    itemrow += "<td width='40px;'>Fleet</br>model</br>desc</td>";
    itemrow += "<td width='40px;'>Manu-</br>facturer</td>";
    itemrow += "<td width='40px;'>Code</td>";
    itemrow += "<td width='40px;'>Description</td>";
    itemrow += "</tr>";
    var classrow = "evenRow"
    $.each(vehiclePlans, function (index, vehiclePlan) {
        var rowid = 'row_' + vehiclePlan.ReqId;
        var ctyvalue = $("#CountryDropdown option:selected").val()
        var href = "<a href='EditVehiclePlan.aspx?iFleetModelId=" + fleetModelId + "&iCountryId=" + ctyvalue + "&iUnitFromNumber=" + $.trim(vehiclePlan.FromUnit) + "&iUnitToNumber=" + $.trim(vehiclePlan.ToUnit) + "'>" + vehiclePlan.PlanNumber + "</a>" 
        itemrow += "<tr id='" + rowid + "' class='" + classrow + "'>"
        itemrow += "<td>" + href + "</td>"
        itemrow += "<td>" + vehiclePlan.PlanDate + "</td>"
        itemrow += "<td>" + vehiclePlan.CompliancePlateYear + "</td>"
        itemrow += "<td>" + vehiclePlan.Status + "</td>"
        itemrow += "<td>" + vehiclePlan.Quantity + "</td>"
        itemrow += "<td>" + vehiclePlan.FromUnit + "</td>"
        itemrow += "<td>" + vehiclePlan.ToUnit + "</td>"
        itemrow += "<td>" + vehiclePlan.LTDealerDeliveryFromDate + "</td>"
        itemrow += "<td>" + vehiclePlan.FleetModelCode + "</td>"
        itemrow += "<td>" + vehiclePlan.FleetModelDescription + "</td>"
        itemrow += "<td>" + vehiclePlan.Manufacturer + "</td>"
        itemrow += "<td>" + vehiclePlan.ManufacturerModelCode + "</td>"
        itemrow += "<td>" + vehiclePlan.ManufacturerModel + "</td>"
        itemrow += "</tr>"
        classrow = (classrow == "oddRow" ? "evenRow" : "oddRow")
    });

    itemrow += "</table>"
    $("#vehiclePlans").html(itemrow);
}

//CALLED BY GetFleetModelDetails AJAX REQUEST
function fxn_regions(regions) {
    $("#regionSelect").html("")
    $.each(regions, function (index, region) {
        $("#regionSelect").append($('<option>', { value: region.RegionId, text: region.Region }));
    });
}

//CALLED BY GetFleetModelDetails AJAX REQUEST
function fxn_allocatedUnitNumbers(allocatedUnitNumbers) {
    if (allocatedUnitNumbers.length == 0) {
        $("#AllocatedUnitNumber").empty()
        $("#AllocatedUnitNumberEditButton").addClass("buttondisabled").attr("disabled", "disabled");
        return false;
    }
    else {
        $("#AllocatedUnitNumberbuttonGroup :button").removeAttr("disabled").addClass("Button_Standard").css("width", "140px").removeClass("buttondisabled")
    }
    
    var itemrow = "<table class='mrrDataTable' width='100%'>"
    itemrow += "<tr><td width='20px;'></td><td  width='100px;'>Country</td><td  width='100px;'>Manufacturer</td><td  width='100px;'>Begin Unit #</td><td  width='100px;'>End Unit #</td><td  width='100px;'>Last Unit Used</td>";
    var classrow = "evenRow"
    $.each(allocatedUnitNumbers, function (index, allocatedUnitNumber) {
        var rowid = 'row_' + allocatedUnitNumber.FleetNumberId;
        var name  =  'country_' + allocatedUnitNumber.CountryId

        classrow = classrow + " manufacturerId_" + allocatedUnitNumber.ManufacturerId

        itemrow += "<tr id='" + rowid + "' class='" + classrow + "' name='" + name + "'>"
        itemrow += "<td><input class='allocatedUnitNumbers' name='allocatedUnitNumbers_CheckBoxes' type='checkbox' id='allocatedUnitNumbers_" + allocatedUnitNumber.FleetNumberId + "'/></td>"
        itemrow += "<td>" + allocatedUnitNumber.CountryCode + "</td>"
        itemrow += "<td>" + allocatedUnitNumber.Manufacturer + "</td>"
        itemrow += "<td>" + allocatedUnitNumber.BeginAssetNum + "</td>"
        itemrow += "<td>" + allocatedUnitNumber.EndAssetNum + "</td>"
        itemrow += "<td>" + allocatedUnitNumber.LastAssetNumUsed + "</td>"
        itemrow += "</tr>"
        classrow = (classrow == "oddRow" ? "evenRow" : "oddRow")
    });

    itemrow += "</table>"
    $("#AllocatedUnitNumber").html(itemrow);

    $("[name='allocatedUnitNumbers_CheckBoxes']").click(function () {
        clearShortMessage();
        var name = $(this).attr("name");
        $("[name = " + name + "]").prop("checked", false);
        $(this).prop("checked", true);
        var id = $(this).attr("id")

        $("#hiddenFleetNumberId").val(id.split("_")[1])

        CountryId = $(this).parent().parent().attr("name").split("_")[1]
        $("#AllocateUnitNumberCountrySelect option:selected").val(CountryId)

        ManufacturerID = $(this).parent().parent().attr("class").split("_")[1]
        $("[id$=_AllocateUnitNumberManufacturerTextbox]").attr("class", ManufacturerID);
        $("[id$=_AllocateUnitNumberManufacturerTextbox]").val($(this).parent().parent().find("td").eq(2).text())

        $("[id$=AllocateUnitNumberBeginUnitNumberTextbox]").val($(this).parent().parent().find("td").eq(3).text())
        $("[id$=AllocateUnitNumberEndUnitNumberTextbox]").val($(this).parent().parent().find("td").eq(4).text())

        $("[id$=_AllocateUnitNumberFleetModelTextbox]").val($("[id$=_FleetModelCodeTextbox]").val())
        $("[id$=_AllocateUnitNumberFleetModelTextbox]").attr("name", "fleetmodel_" + fleetModelId);
    });
}


//CALLED BY GetFleetModelDetails AJAX REQUEST
function fxn_manufacturerModels(manufacturermodels) {
    if (manufacturermodels.length == 0) {
        $("#ManufacturerModelButtonsGroup  [id!= 'ManufacturerModelAddButton']").addClass("buttondisabled").attr("disabled", "disabled");
        $("#ManufacturerModelButtonsGroup  [id= 'ManufacturerModelAddButton']").removeAttr("disabled").addClass("Button_Standard").css("width", "130px").removeClass("buttondisabled")
        $("#ManufacturerModel").empty();
        return false;
    }
    else {
        $("#ManufacturerModelButtonsGroup :button").removeAttr("disabled").addClass("Button_Standard").css("width", "130px").removeClass("buttondisabled")
    }
    
   
    var itemrow = "<table class='mrrDataTable' width='100%'>"
    itemrow += "<tr><td width='10px;'></td><td  width='100px;'>Country</td><td  width='200px;'>Manufacturer</td><td  width='250px;'>Code</td><td  width='300px;'>Description</td>";
    var classrow = "evenRow"
    $.each(manufacturermodels, function (index, manufacturer) {
        var rowid = 'row_' + manufacturer.ManufacturerModelId;
        rowid = rowid + "_" + manufacturer.ManufacturerModelCode
        rowid = rowid + "_" + manufacturer.FuelType
        rowid = rowid + "_" + manufacturer.Transmission
        rowid = rowid + "_" + manufacturer.ServiceScheduleId
        
        rowid = rowid + "_" + manufacturer.ChassisCost
        rowid = rowid + "_" + manufacturer.ChassisCostId
        rowid = rowid + "_" + manufacturer.RegionModelId
        rowid = rowid + "_" + manufacturer.FleetModelManufacturerModelId
        rowid = rowid + "_" + manufacturer.RegionId
        
        itemrow += "<tr id='" + rowid + "' class='" + classrow + "'>"
        itemrow += "<td><input class='manufacturermodels' name='ManufacturerId_CheckBoxes' type='checkbox' id='ManufacturerId_" + manufacturer.ManufacturerId + "'/></td>"
        itemrow += "<td>" + manufacturer.CountryCode + "</td>"
        itemrow += "<td class='" + manufacturer.ManufacturerId + "'>" + manufacturer.Manufacturer + "</td>"
        itemrow += "<td>" + manufacturer.ManufacturerModelCode + "</td>"
        itemrow += "<td>" + manufacturer.ManufacturerModel + "</td>"
        itemrow += "</tr>"

        classrow = (classrow == "oddRow"? "evenRow" : "oddRow")
    });
    itemrow += "</table>"

    $("#ManufacturerModel").html(itemrow);


    $("[name='ManufacturerId_CheckBoxes']").click(function () {
        clearShortMessage();
        var name = $(this).attr("name");
        $("[name = " + name + "]").prop("checked", false);
        $(this).prop("checked", true);

        var id = $(this).attr("id").split("_")[1]
        var ManufacturerModelId = $(this).parent().parent().attr("id").split("_")[1]

        var FuelType = $(this).parent().parent().attr("id").split("_")[3]
        var Transmission = $(this).parent().parent().attr("id").split("_")[4]
        var ServiceScheduleId = $(this).parent().parent().attr("id").split("_")[5]
        
        var ChassisCost = $(this).parent().parent().attr("id").split("_")[6]
        var ChassisCostId = $(this).parent().parent().attr("id").split("_")[7]
        var RegionModelId = $(this).parent().parent().attr("id").split("_")[8]
        var FleetModelManufacturerModelId = $(this).parent().parent().attr("id").split("_")[9]
        var RegionId = $(this).parent().parent().attr("id").split("_")[10]
                                
        var modelcodelTextboxInEditManufacturerModel = $(this).parent().parent().find("td").eq(3).text()
        var ManufacturerModelTextBoxInEditManufacturerModel = $(this).parent().parent().find("td").eq(4).text()

        carmodelPrevious = id
        carmodelCurrent = id
        $("#ManufacturerSelect").val(id)
        $("[id$=_modelcodelTextboxInEditManufacturerModel]").val(modelcodelTextboxInEditManufacturerModel)
        $("[id$=_modelcodelTextboxInEditManufacturerModel]").removeAttr("class").attr("class", ManufacturerModelId)

        $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val(ManufacturerModelTextBoxInEditManufacturerModel) //aka description
        $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").removeAttr("class").attr("class", modelcodelTextboxInEditManufacturerModel)

        $("#CountrySelect").val(CountryId)

        $("#fueltypeSelect").val(FuelType)
        $("#TransmissionSelect").val(Transmission)
        $("#ServiceScheduleSelect").val(ServiceScheduleId)
        
        $("#ChassisCostText").val(ChassisCost)
        $("#ChassisCostId").val(ChassisCostId);
        $("#RegionModelId").val(RegionModelId);
        $("#FleetModelManufacturerModelId").val(FleetModelManufacturerModelId);
        $("#regionSelect").val(RegionId)        
        
        $("#EditManufacturerModelTable").attr("name", "EditManufacturerModelTable_" + ManufacturerModelId)

        var ManufacturerId = $(this).parent().parent().find("td").eq(2).attr("class")
        var Manufacturer = $(this).parent().parent().find("td").eq(2).text()
        var FleetModelCode = $("[id$=_FleetModelCodeTextbox]").val()

        $("[id$=_AllocateUnitNumberFleetModelTextbox]").val(FleetModelCode);
        $("[id$=_AllocateUnitNumberManufacturerTextbox]").val(Manufacturer);
        $("[id$=_AllocateUnitNumberManufacturerTextbox]").attr("class",ManufacturerId)

    });
}

//CALLED BY GetManufacturerModeLCodeAutoSuggest AJAX REQUEST
function fxn_manufacturerModelCodes(ManufacturerModeLCodes) {
    
   if (ManufacturerModeLCodes.length == 0) {
       setErrorShortMessage(CONST_MODEL_WAS_NOT_FOUND)
       $("#modelcodelresultV2").empty();
       $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val("")
        return;
    }
  
    
    var itemrow = ""
    var name = "";
    var itemrowtemp = '';
    var modelcode = '';
    var modelcodeitem = ""
    var modelcodeDescription = ""
    $.each(ManufacturerModeLCodes, function (index, ManufacturerModeLCode) {
        name = $.trim(ManufacturerModeLCode.ManufacturerModeLCode);
        modelcode = $.trim(ManufacturerModeLCode.ManufacturerModeLCode);
        modelcodeDescription = ManufacturerModeLCode.ManufacturerModeL
        if (itemrowtemp == "" && name.split("~")[0] != "") {
            itemrowtemp = itemrowtemp + "<tr><td class='" + modelcodeDescription + "'><input style='width:10px;' class='modelcodelinks' name='" + name + "' type='radio' id='" + name + "'/>" + $.trim(modelcode.split("~")[1]) + "</td></tr>"
            itemrow = itemrow + itemrowtemp;
            itemrowtemp = ""
        }
    });

    $("#modelcodelresultV2").html("");
   
    if (itemrow != "") {
        $("#modelcodelresultV2").html("<table>" + itemrow + "</table>");

      
        var modelcodelength = $("#modelcodelresultV2 .modelcodelinks").filter(function () {
            var myid = $(this).attr("id").toLowerCase()
            if ($.trim(myid.split("~")[1]) != "") {
                if ($.trim(myid.split("~")[1]) == $.trim($("[id$=_modelcodelTextboxInEditManufacturerModel]").val().toLowerCase())) {
                     modelcodeitem = $(this).attr("id")
                     modelcodeDescription = $(this).parent().attr("class")
                     $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val($.trim(modelcodeDescription.split("~")[1]))
                     $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").removeAttr("class").attr("class", $.trim(modelcodeDescription.split("~")[0]))
                     return $.trim(myid.split("~")[1]) == $.trim($("[id$=_modelcodelTextboxInEditManufacturerModel]").val().toLowerCase());
                }
            }
        });

        if (modelcodelength.length - 1 != -1 && modelcodeitem != "") {
            $("#modelcodelresultV2 [id='" + $.trim(modelcodeitem) + "']").prop("checked", true).show();
           clearShortMessage()
        }
        else {
            $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val("")
        }
    }

    $('.modelcodelinks').bind("click", function (e) {
        modelcode = $(this).attr("id");
        modelcodeDescription = $(this).parent().attr("class")
        $("[id$=_modelcodelTextboxInEditManufacturerModel]").val($.trim(modelcode.split("~")[1]));
        $("[id$=_modelcodelTextboxInEditManufacturerModel]").removeAttr("class").attr("class", $.trim(modelcode.split("~")[0]));
        $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val($.trim(modelcodeDescription.split("~")[1]))
        $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").removeAttr("class").attr("class", $.trim(modelcodeDescription.split("~")[0]))
        
        $(".modelcodelinks").prop("checked", false);
        $(this).prop("checked", true);
        
    });
}


//CALLED BY GetFleetModelDetails AJAX REQUEST
function fxn_linkmanufacturerModels(manufacturermodels) {
    if (manufacturermodels.length == 0) {
        $("#LinkManufacturing").empty()
        return false;
    }

    var itemrow = "<table class='mrrDataTable' width='300px;'>"
    itemrow += "<tr><td width='30px;'></td><td  width='100px;'>Country</td><td  width='100px;'>Manufacturer</td><td  width='150px;'>Code</td>";
    var classrow = "evenRow"
    $.each(manufacturermodels, function (index, manufacturer) {
        var rowid = 'row_' + manufacturer.ManufacturerModelId;
        rowid = rowid + "_" + manufacturer.ManufacturerModelCode
     

        itemrow += "<tr id='" + rowid + "' class='" + classrow + "'>"
        itemrow += "<td><input class='linkmanufacturermodels' name='linkManufacturerId_CheckBoxes' type='checkbox' id='linkManufacturerId_" + manufacturer.ManufacturerId + "'/></td>"
        itemrow += "<td>" + manufacturer.CountryCode + "</td>"
        itemrow += "<td>" + manufacturer.Manufacturer + "</td>"
        itemrow += "<td>" + manufacturer.ManufacturerModelCode + "</td>"
        
        itemrow += "</tr>"

        classrow = (classrow == "oddRow" ? "evenRow" : "oddRow")
    });
    itemrow += "</table>"

    $("#LinkManufacturing").html(itemrow);


    $("[name='linkManufacturerId_CheckBoxes']").click(function () {
        clearShortMessage();
    });
}

//CALLED BY GetNumberRules AJAX REQUEST
function fxn_numberrules(numberrules) {
    if (numberrules.length == 0) {
        $("#UnitNumber").empty();
        return false;
    }
    
    var itemrow = "<table class='mrrDataTable' width='780px;'>"
    itemrow = itemrow + "<tr>";
    itemrow = itemrow + "<td  width='7%'>Cty</td>"
    itemrow = itemrow + "<td  width='10%;'>Model</td>"
    itemrow = itemrow + "<td  width='20%'>Description</td>"
    itemrow = itemrow + "<td  width='10%;'>Manu-</br>facturer</td>"
    itemrow = itemrow + "<td  width='7%;'>Begin</br>Asset #</td>"
    itemrow = itemrow + "<td  width='7%;'>End</br>Asset #</td>"
    itemrow = itemrow + "<td  width='5%;'>Seq #</td>"
    itemrow = itemrow + "<td  width='5%;'>Last</br> Asset #</td>"
    itemrow = itemrow + "<tr>";

    var classrow = "evenRow"
  
    $.each(numberrules, function (index, numberrule) {
        var rowid = 'row_' + numberrule.FleetNumberId;
        
        itemrow += "<tr id='" + rowid + "' class='" + classrow + "'>"
        itemrow += "<td>" + numberrule.CountryCode + "</td>"
        itemrow += "<td>" + $.trim(numberrule.FleetModelCode) + "</td>"
        itemrow += "<td>" + $.trim(numberrule.FleetModelDescription) + "</td>"
        itemrow += "<td>" + $.trim(numberrule.Manufacturer) + "</td>"
        itemrow += "<td>" + numberrule.BeginAssetNum + "</td>"
        itemrow += "<td>" + numberrule.EndAssetNum + "</td>"
        itemrow += "<td>" + numberrule.RuleSeqNum + "</td>"
        itemrow += "<td>" + numberrule.LastAssetNumUsed + "</td>"

        itemrow += "</tr>"
        
        modelcodesarray.push($.trim(numberrule.FleetModelCode))
        beginassetarray.push(numberrule.BeginAssetNum)

        classrow = (classrow == "oddRow" ? "evenRow" : "oddRow")
    });
    itemrow += "</table>"

    $("#UnitNumber").html(itemrow);
    $("#UnitNumber").show()

    if ($("#FleetModelCodeDataTextbox").val() != "") {
        setInformationShortMessage(CONST_MODEL_WAS_FOUND);
    }
    else {
        setInformationShortMessage(CONST_FLEET_IS_EMPTY);
    }

    modelcodesarray = fxn_eliminateDuplicates(modelcodesarray)
    beginassetarray = fxn_eliminateDuplicates(beginassetarray)
}



//CALLED BY GetManufacturerAutoSuggest AJAX REQUEST
function fxn_manufacturernames(manufacturernames) {
    if (manufacturernames.length == 0) {
        $("#AllocateUnitNumberManufacturerTextboxV2").empty()
        return false;
    }

    var itemrow = ""
    var itemrowtemp = "";

    $.each(manufacturernames, function (index, manufacturername) {
        if (itemrowtemp == "") {
            var id = manufacturername.ManufacturerId;
            var name = $.trim(manufacturername.Manufacturer);

            itemrowtemp = itemrowtemp + "<tr><td><input style='width:10px;' class='manufacturernamelinks' name='" + name + "' type='radio' id='" + id + "'/>" + name + "</td></tr>"
            itemrow = itemrow + itemrowtemp;
            itemrowtemp = ""
        }
    });

    $("#AllocateUnitNumberManufacturerSuggestion").html("");
    if (itemrow != "") {
        $("#AllocateUnitNumberManufacturerTextboxV2").html(itemrow);
    }
  
    $(".manufacturernamelinks").click(function (e) {
        var text = $(this).attr("name");
        var id = $(this).attr("id");

        $("[id$=_AllocateUnitNumberManufacturerTextbox]").val($.trim(text));
        $("[id$=_AllocateUnitNumberManufacturerTextbox]").removeAttr("class").attr("class", id)

        $("#AllocateUnitNumberSaveButton").removeAttr("disabled").addClass("Button_Standard").css("width", "70px").removeClass("buttondisabled")

        $(".manufacturernamelinks").prop("checked", false);
        $(this).prop("checked", true);

        EventPropagation()
        
    });

}

function fxn_currencyformatted(amount) {
    var i = parseFloat(amount);
    if (isNaN(i)) { i = 0.00; }
    var minus = '';
    if (i < 0) { minus = '-'; }
    i = Math.abs(i);
    i = parseInt((i + .005) * 100);
    i = i / 100;
    s = new String(i);
    if (s.indexOf('.') < 0) { s += '.00'; }
    if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
    s = minus + s;
    return s;
}

function AddOptionItem(id, text) {
    return "<option value= '" + id + "'>" + text + "</option>"
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function ButtonGroupingStatus(mode) {

    if (mode == false) {
        $("#ManufacturerModelButtonsGroup :button").addClass("buttondisabled").attr("disabled", "disabled");
        $("#AllocatedUnitNumberbuttonGroup :button").addClass("buttondisabled").attr("disabled", "disabled");
        $("#VehiclePlanButtonGroup :button").addClass("buttondisabled").attr("disabled", "disabled");
        $("#loadButton").addClass("buttondisabled").attr("disabled", "disabled")
    }
    else {
        $("#ManufacturerModelButtonsGroup :button").removeAttr("disabled").addClass("Button_Standard").css("width", "130px").removeClass("buttondisabled")
        $("#AllocatedUnitNumberbuttonGroup :button").removeAttr("disabled").addClass("Button_Standard").css("width", "130px").removeClass("buttondisabled")
        $("#VehiclePlanButtonGroup :button").removeAttr("disabled").addClass("Button_Standard").css("width", "130px").removeClass("buttondisabled")
    }

}

function EnablingModeForEditManufacturerModelTable(mode) {
    if (mode == false) {
        $("#EditManufacturerModelTable tr:gt(1) ").attr("disabled", "disabled");
        $("#EditManufacturerModelTable tr:gt(-2) ").removeAttr("disabled");
        $("[id$=TextboxInEditManufacturerModel]").attr("disabled", "disabled");

        $("[id$=_modelcodelTextboxInEditManufacturerModel]").attr("disabled", "disabled")
        $("#modelcodelresultV2").hide()
        $("[id$=_pnlModelCodes]").hide().attr("disabled", "disabled")

    }
    else {
        $("#EditManufacturerModelTable select,#EditManufacturerModelTable input").removeAttr("disabled");
        $("#EditManufacturerModelTable tr:gt(1)").removeAttr("disabled");

        $("[id$=_modelcodelTextboxInEditManufacturerModel]").removeAttr("disabled")
        $("#modelcodelresultV2").show()
        $("[id$=_pnlModelCodes]").show().removeAttr("disabled")
    }

    $("#CountrySelect").attr("disabled", "disabled");
    $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").attr("disabled", "disabled")
    $("#ManufacturerModelresultV2").hide()
    $("[id$=_pnlModel]").hide()
}


function fxn_eliminateDuplicates(arr) {
    var i,
      len = arr.length,
      out = [],
      obj = {};

    for (i = 0; i < len; i++) {
        obj[arr[i]] = 0;
    }
    for (i in obj) {
        out.push(i);
    }
    return out;
}


function fxn_ReloadUnitNumberByCountry() {
    var unitNumbersJSON;
    unitNumbersJSON = $("[id$=_UnitNumberhidden]").val()
    unitNumbersJSON = $.parseJSON(unitNumbersJSON)
    unitNumbersJSON.NumberRules = $.grep(unitNumbersJSON.NumberRules, function (element, index) {
        return (element.CountryCode == (CountryId == "8" ? "NZL" : "AUS"));
    });

    fxn_numberrules(unitNumbersJSON.NumberRules)
}

function fxn_ReloadUnitNumberByCodeInKeyPress(tempmodelcodesarray) {
    var content = '';
    var tdcounter = 0;
    var tempcontent = "";
    $.each(tempmodelcodesarray, function (i, elem) {

        if (tdcounter <= 5) {
            content = content + "<td width='100px;'><input class='unitnumber' name='" + elem + "' type='radio' id='" + elem + "'>" + elem + "</input></td>"
            tdcounter++;
        }
        else {
            tempcontent = tempcontent + "<tr>" + content + "</tr>"
            content = "<td  width='100px;'><input class='unitnumber' name='" + elem + "' type='radio' id='" + elem + "'>" + elem + "</input></td>"
            tdcounter = 1;
        }
    });
    if (tempmodelcodesarray.length - 1 <= 5) tempcontent = "<tr>" + content + "</tr>"
    $("#unitnumberfilteronModelResult").html("").html("<table width='100%'>" + tempcontent + "</table>")

}

function fxn_ReloadUnitNumberByBeginAsset(assetnumber,itemcode) {
    var unitNumbersJSON;
    var item;

    unitNumbersJSON = $("[id$=_UnitNumberhidden]").val()
    unitNumbersJSON = $.parseJSON(unitNumbersJSON)
    unitNumbersJSON.NumberRules = $.grep(unitNumbersJSON.NumberRules, function (element, index) {
        var inputtext = element.BeginAssetNum.toString().substring(0, assetnumber.length)
        if (itemcode != '') {
            item = element.FleetModelCode.toLowerCase().substring(0, itemcode.length)
            return (inputtext == assetnumber && item.toLowerCase() == itemcode.toLowerCase())
        }
        else
        return inputtext == assetnumber
    });
    fxn_numberrules(unitNumbersJSON.NumberRules)
}

function fxn_ReloadUnitNumberByCode(itemcode, assetnumber) {
    var unitNumbersJSON;
    var asset;
    unitNumbersJSON = $("[id$=_UnitNumberhidden]").val()
    unitNumbersJSON = $.parseJSON(unitNumbersJSON)
    unitNumbersJSON.NumberRules = $.grep(unitNumbersJSON.NumberRules, function (element, index) {
        var inputtext = element.FleetModelCode.toLowerCase().substring(0, itemcode.length)
        if (assetnumber != '') {
            asset = element.BeginAssetNum.toString().substring(0, assetnumber.length)
            return (inputtext.toLowerCase() == itemcode.toLowerCase() && asset == assetnumber)
        }
        else
            return inputtext.toLowerCase() == itemcode.toLowerCase()
    });
    fxn_numberrules(unitNumbersJSON.NumberRules)
}

function htmlEncode(value) {
    return $('<div/>').text(value).html();
}

function htmlDecode(value) {
    return $('<div/>').html(value).text();
}

function fxn_ReloadFleetModels(model) {
    var fleetmodels = $("[id$=_HiddenFieldGetFleetModelCodesAutoSuggest]").val()
    fleetmodels = $.parseJSON(fleetmodels)

    if (model != null) {
        fleetmodels.FleetModels = $.grep(fleetmodels.FleetModels, function (element, index) {
            model = $.trim(model)
            var modelcode = element.FleetInformation.split("-")[0]
            if (element.FleetInformation == CONST_XHIRE) {
                modelcode = 'X-HIRE'
            }

            var inputtext = modelcode.toLowerCase().substring(0, model.length)
            return inputtext.toLowerCase() == model.toLowerCase()
        });
    }
    fxn_FleetModels(fleetmodels.FleetModels)
}


function fxn_FleetModels(fleetmodels) {
    var content = '';
    var tempcontent = '';

       var tdcounter = 0
       $.each(fleetmodels, function (index, fleetmodel) {
           var modelcode = ""
           var modeldesc = ""
           if (fleetmodel.FleetInformation.split("-").length - 1 == 2) {
               if (fleetmodel.FleetInformation == CONST_XHIRE) {
                   modelcode = 'X-HIRE'
                   modeldesc = 'CROSS HIRE'
               }
               else {
                   modelcode = fleetmodel.FleetInformation.split("-")[0]
                   modeldesc = fleetmodel.FleetInformation.split("-")[1]
                   modeldesc = modeldesc + '-' + fleetmodel.FleetInformation.split("-")[2]
               }
           }
           else {
               modelcode = fleetmodel.FleetInformation.split("-")[0]
               modeldesc = fleetmodel.FleetInformation.split("-")[1]
           }
           modelcode = $.trim(modelcode.toUpperCase()) + "-" + $.trim(modeldesc.toLowerCase())
           if (modelcode.length > 30) {
               modelcode = modelcode.substring(0, 30) + "..."
           }

           if (tdcounter <= 2) {
               content = content + "<td  width='300px;'><input class='cblFleetModels' name='" + fleetmodel.FleetModelId + "' type='radio' id='" + $.trim(fleetmodel.FleetModelCode.toUpperCase()) + "'>" + $.trim(modelcode) + "</input></td>"
               tdcounter++;
           }
           else {
               tempcontent = tempcontent + "<tr>" + content + "</tr>"
               content = "<td  width='300px;'><input class='cblFleetModels' name='" + fleetmodel.FleetModelId + "' type='radio' id='" + $.trim(fleetmodel.FleetModelCode.toUpperCase()) + "'>" + $.trim(modelcode) + "</input></td>"
               tdcounter = 1;
           }

           if (fleetmodels.length - 1 <= 2 && tdcounter <= 2) tempcontent = "<tr>" + content + "</tr>"
       });

        $("#FleetModelsuggestionResultV2").html("").hide();
        if (content != "") {
            $("#FleetModelsuggestionResultV2").html("<table>" + tempcontent + "</table>")
            var itemcode = $.trim($("[id$=_FleetModelCodeTextbox]").val());
            var radio = $("#FleetModelsuggestionResultV2 [id='" + itemcode.toUpperCase() + "']")
            if ($(radio) != null) {
                $(radio).prop("checked", true);
                $(radio).parent().parent().css("background-color", "lightyellow")
            }
            else {
                $("#FleetModelsuggestionResultV2 .cblFleetModels]").parent().parent().css("background-color", "#FFFFFF")
            }

            $("#FleetModelsuggestionResultV2").show()
        }

        $('.cblFleetModels').bind("click", function (e) {
            var text = $(this).attr("id");
            fleetModelId = $(this).attr("name");
            $("[id$=_FleetModelCodeTextbox]").val($.trim(text));

            $(".cblFleetModels").prop("checked", false);
            $(this).prop("checked", true);

            $("#loadButton").removeAttr("disabled").addClass("Button_Standard").removeClass("buttondisabled")
            GetSingleFleeDetail(text)

            addedThruRadioClicking = true;
        });
}