﻿/* REV: MIA 28-FEB-2014 - today in THL history, last day of Chloe and Ian */
var windowWidth;
var windowHeight;
var popupHeight;
var popupWidth;

var CONST_INSERTUPDATEFLEETMODELCODE            = "InsertUpdateFleetModelCode"
var CONST_GETFLEETMODELCODESAUTOSUGGEST         = "GetFleetModelCodesAutoSuggest"
var CONST_GETSINGLEFLEETMODELCODE               = "GetSingleFleetModelCode"
var CONST_GETDEFAULTCOUNTRY                     = "GetDefaultCountry"
var CONST_LOADDATAFORMANUFACTURERMODEL          = "LoadDataForManufacturerModel"
var CONST_INSERTUPDATEFLEETMANUFACTURERMODEL    = "InsertUpdateFleetManufacturerModel"
var CONST_GETFLEETMODELDETAILS                  = "GetFleetModelDetails"
var CONST_GETMANUFACTURERMODELCODEAUTOSUGGEST   = "GetManufacturerModeLCodeAutoSuggest"
var CONST_GETNUMBERRULES                        = "GetNumberRules"
var CONST_LINKFLEETMODELMANUFACTURERMODEL       = "LinkFleetModelManufacturerModel"
var CONST_GETMANUFACTURERAUTOSUGGEST            = "GetManufacturerAutoSuggest"
var CONST_ALLOCATEEDITNUMBER                    = "AllocateEditUnitNumber"

var CONST_GETCOUNTRIES = 'GetCountries'
var CONST_URL                                   = "AIMSListener/AIMSListener.aspx?MODE="
var CONST_FLEET_IS_EMPTY                        = 'Fleet Model Code is empty.'
var CONST_TICK_AN_ITEM                          = 'Please tick an item from the Manufacturing Model.'
var CONST_FLEET_IS_EMPTY_TRY                    = 'Fleet Information does not exist. Please try again.'
var CONST_SELECT_MODE_CODE                      = 'Please select Fleet Model Code.'
var CONST_TICK_ALLOCATED_UNIT_NUMBER            = 'Please tick an item from the Allocated Unit Number.'
var CONST_REQUIRED_ELEMENT                      = '&nbsp;<b>Required</b>'
var CONST_FLEET_MODEL_UPDATED                   = 'Fleet Model was updated.'
var CONST_LINK_FLEET_MODEL_UPDATED              = 'Link Fleet Model was updated.'
var CONST_MANUFACTURING_MODEL_UPDATED           = 'Edit Manufacturing Model was updated.'
var CONST_PLEASE_SELECT                         = '--please select--'
var CONST_ALLOCATE_NUMBER_UPDATED               = 'Allocate Number was updated.'
var CONST_MANUFACTURING_MODEL_WAS_NOT_FOUND     = 'Manufacturer Model Code not found. Please change your model.'
var CONST_MODEL_WAS_NOT_FOUND                   = 'Model Code not found. Please change your code.'
var CONST_MODEL_WAS_FOUND                       = 'AIMS Information was loaded.'
var CONST_XHIRE                                 = 'X-HIRE - CROSS HIRE'

var Mode = {
    Add: 1,
    Change: 2,
    Cancel: 3,
    AddManufacturerModel: 4,
    Load: 5,
    ClearAll: 6

}

var fleetModelId = -1;
var isAdd = false;
var parameter = {};

var addedThruRadioClicking = false;
var loadFleetModel = false;

var fleetnumberid = "";
var FleetNumberId;

var option = "";
var ManufacturerID = ""
var FleetManufacturerModelID = ""
var ManufacturerModelCode = ""
var CountryId = ""
var FuelType = ""
var Transmission = ""
var ManufacturerModel = ""
var ServiceScheduleId = ""
var ChassisCost = ""
var UserCode = ""

var FleetModelCodeDataTextbox = ""
var FleetModelDescriptionDataTextbox = ""
var ConversionRequiredCheckbox = ""
var isLocalcheckbox = ""
var DailyRunningCosttextBox = ""
var DailyDelaytextBox = ""
var inactiveDate = ""

var FleetModelCodeTextbox;
var unitNumbersJSON = ''
var modelcodesarray = []
var beginassetarray = []

//we need to avoid multiple postback to database
//im using this compare if we need to call dtabase or just skip
var carmodelPrevious = '';
var carmodelCurrent = '';

var countryPrevious = '';
var countryCurrent = '';


$(document).ready(function () {


    $("#FleetModelDiv").hide()
    $("#FleetModeltable").attr("style", "visibility:none")
    //disable buttons during first load
    ButtonGroupingStatus(false)

    //RETRIEVE COUNTRIES
    RequestAjaxCall(CONST_GETCOUNTRIES, {})



    //if there are some query string from referrers, execute these codes
    //to load AIMS data
    CountryId = unescape(getUrlVars()["iCountryId"])
    FleetModelCodeTextbox = unescape(getUrlVars()["iFleetModelCode"])

    if (CountryId != "undefined" && FleetModelCodeTextbox != "undefined") {
        $("[id$=_FleetModelCodeTextbox]").val($.trim(FleetModelCodeTextbox))
        GetSingleFleeDetail(FleetModelCodeTextbox)
        $("#loadButton").removeAttr("disabled").addClass("Button_Standard").removeClass("buttondisabled")
        loadFleetModel = true;
        addedThruRadioClicking = false;
        $("#FleetModelDiv").show()
        $("#FleetModeltable").removeAttr("style")
    }


    $("#CountryDropdown").change(function () {
        clearShortMessage()
        clearFleetModelDiv(Mode.ClearAll)
        ButtonGroupingStatus(false)
        countryCurrent = $("#CountryDropdown option:selected").val()
    });


    //---------------------------------------------------------------------------------
    //-----------------START - BUTTONS IN LOADING AND SEARCHING SECTION-------------------
    //---------------------------------------------------------------------------------

    $("#addButton").click(function () {
        clearFleetModelDiv(Mode.Add);
        $("#ConversionRequiredCheckbox").prop('checked', true)

    });

    $("#loadButton").click(function () {
        FleetModelCodeTextbox = $.trim($("[id$=_FleetModelCodeTextbox]").val());
        if (FleetModelCodeTextbox == "") {
            clearFleetModelDiv(Mode.Change)
            setErrorShortMessage(CONST_FLEET_IS_EMPTY_TRY);
            return;
        }
        else {
            clearFleetModelDiv(Mode.Load)
        }
        $("[id$=_FleetModelCodeTextbox]").val(FleetModelCodeTextbox.toUpperCase())
        //addedThruRadioClicking is to identify if radio button in the dynamic popup
        //was click. If then, we dont need to reload the "GetSingleFleetModelCode" because it will be executed
        //in the "GetFleetModelDetails" responses
        if (addedThruRadioClicking) {
            //reload fleetmodel details
            GetFleetModelDetails()
            addedThruRadioClicking = false;

            //if this is true, then execute the "GetFleetModelDetails" inside "GetSingleFleetModelCode"
            loadFleetModel = false;
        }
        else {
            GetSingleFleeDetail(FleetModelCodeTextbox)
            loadFleetModel = true;
            addedThruRadioClicking = false;
        }
    });

    //---------------------------------------------------------------------------------
    //-----------------END - BUTTONS IN LOADING AND SEARCHING SECTION-------------------
    //---------------------------------------------------------------------------------



    //---------------------------------------------------------------------------------
    //-----------------START - BUTTONS IN MANUFACTURER MODEL SECTION-------------------
    //---------------------------------------------------------------------------------
    $("#ManufacturerModelEditButton").click(function () {
        clearShortMessage();
        var length = $("[name='ManufacturerId_CheckBoxes']:checked").length;
        if (length == 1) {
            $("#ManufacturerModelTitle").text("Edit Manufacturer Model".toUpperCase())
            $("#ManufacturerModel input:checked").trigger("click");
            LoadPopupAddNewManufacturingModel();
        }
        else
            setErrorShortMessage(CONST_TICK_AN_ITEM);

        $("#CountrySelect").attr("disabled", "disabled");
    });

    $("#ManufacturerModelAddButton").click(function () {
        UnloadPopupAddNewManufacturingModel();
        $("#ManufacturerModelTitle").text("Add Manufacturer Model".toUpperCase())
        $("#EditManufacturerModelTable").attr("name", "EditManufacturerModelTable_Add")
        EnablingModeForEditManufacturerModelTable(false)
        LoadPopupAddNewManufacturingModel();
        CountryId = $("#CountryDropdown option:selected").val();
        $("#CountrySelect").val(parseInt(CountryId))
    });

    $('#ManufacturerModelLinkButton').click(function () {
        LoadPopupLinkManufacturingModel()
    });

    $('#ManufacturerModelSearchUnitNumberButton').click(function () {
        LoadPopupDisplayUnitNumber()
        fxn_ReloadUnitNumberByCountry()
    });

    $('#ManufacturerModelAllocateUnitNumberButton').click(function () {
        clearShortMessage();
        var length = $("[name='ManufacturerId_CheckBoxes']:checked").length;
        if (length == 1) {
            $("#AllocateUnitNumbeTable").attr("name", "AllocateUnitNumbeTable_0")
            $("#hiddenFleetNumberId").val("0")
            $("#AllocateUnitNumberBeginUnitNumberTextbox").val("")
            $("#AllocateUnitNumberEndUnitNumberTextbox").val("")
            LoadPopupAllocateUnitNumber()
        }
        else
            setErrorShortMessage(CONST_TICK_AN_ITEM);
    });

    $("#LinkManufacturingModelSaveButton").click(function () {
        var text = ""
        $("#LinkManufacturing table tr td input:checked").each(function () {
            var ManufacturerModelId = $(this).parent().parent().attr("id").split("_")[1]
            text = text + ManufacturerModelId + ":"
        });
        text = text.substr(0, text.length - 1)
        text = $.trim(text);

        UserCode = $.trim($("[id$=_ManufacturerIDhidden]").val())
        parameter = { FleetModelId: fleetModelId, UserCode: UserCode, Links: text }
        RequestAjaxCall(CONST_LINKFLEETMODELMANUFACTURERMODEL, parameter)
    });


    //---------------------------------------------------------------------------------
    //-----------------END - BUTTONS IN MANUFACTURER MODEL SECTION-------------------
    //---------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------
    //-----------------START - UNLOAD POPUP--------------------------------------------
    //---------------------------------------------------------------------------------

    $("a.ClosePopup, .Button_Cancel").click(function () {
        UnloadPopupLinkManufacturingModel();
        UnloadPopupAddNewManufacturingModel();
        UnloadPopupDisplayUnitNumber();
        UnloadPopupAllocateUnitNumber();
    });

    //---------------------------------------------------------------------------------
    //-----------------END - UNLOAD POPUP----------------------------------------------
    //---------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------
    //-----------------FILTER IN DISPLAY UNIT NUMBER-----------------------------------
    //---------------------------------------------------------------------------------
    $("#beginAssetfilteronModel").keyup(function (e) {
        var content = "";
        var beginAssetfilteronModel = $.trim($(this).val());
        var unitnumberfilteronModel = $.trim($("[id$=_unitnumberfilteronModel]").val())

        if (beginAssetfilteronModel == "" && unitnumberfilteronModel == "") {
            fxn_ReloadUnitNumberByCountry()
            return false;
        }
        if (beginAssetfilteronModel == "" && unitnumberfilteronModel != "") {
            fxn_ReloadUnitNumberByCode(unitnumberfilteronModel, beginAssetfilteronModel)
            return false;
        }

        fxn_ReloadUnitNumberByBeginAsset(beginAssetfilteronModel, unitnumberfilteronModel)

    }).focus(function () {

        var unitnumberfilteronModel = $.trim($("[id$=_unitnumberfilteronModel]").val())
        if ($(this).val() == "" && unitnumberfilteronModel == "") {
            //does not call database. it just requery the json in the in hidden field
            fxn_ReloadUnitNumberByCountry()
            return false;
        }

    })


    $("[id$=_unitnumberfilteronModel]").keyup(function (e) {
        var content = ""
        var unitnumberfilteronModel = $.trim($(this).val())
        var beginAssetfilteronModel = $.trim($('#beginAssetfilteronModel').val());

        if (unitnumberfilteronModel == "" && beginAssetfilteronModel == "") {
            fxn_ReloadUnitNumberByCountry()
            return false;
        }

        if (beginAssetfilteronModel != "" && unitnumberfilteronModel == "") {
            fxn_ReloadUnitNumberByBeginAsset(beginAssetfilteronModel, unitnumberfilteronModel)
            return;
        }

        var tempmodelcodesarray = modelcodesarray
        tempmodelcodesarray = $.grep(tempmodelcodesarray, function (element, index) {
            if (unitnumberfilteronModel != "") {
                var inputtext = element.toLowerCase().substring(0, unitnumberfilteronModel.length)
                return inputtext.toLowerCase() == unitnumberfilteronModel.toLowerCase()
            }
        });

        $(".unitnumber").prop("checked", false);
        $("#unitnumberfilteronModelResult .unitnumber").parent().parent().css("background-color", "#FFFFFF")

        if (tempmodelcodesarray.length - 1 != -1) {
            var itemcode = $.trim($(this).val());

            fxn_ReloadUnitNumberByCodeInKeyPress(tempmodelcodesarray)
            fxn_ReloadUnitNumberByCode(itemcode, beginAssetfilteronModel)


            $('.unitnumber').bind("click", function (e) {
                var text = $(this).attr("id");
                //fleetModelId = $(this).attr("name");
                var fleetmodelcode = $(this).attr("name")
                $("[id$=_unitnumberfilteronModel]").val($.trim(text));

                $(".unitnumber").prop("checked", false);
                $(this).prop("checked", true);

                fxn_ReloadUnitNumberByCode(text, '')
            });

            var radio = $("#unitnumberfilteronModelResult [id='" + itemcode.toUpperCase() + "']")
            if ($(radio) != null) {
                $(radio).prop("checked", true);
                $(radio).parent().parent().css("background-color", "lightyellow")
            }
            else {
                $("#unitnumberfilteronModelResult .unitnumber").parent().parent().css("background-color", "#FFFFFF")
            }

            $("#UnitNumber").show()
            $(this).val(itemcode.toUpperCase())
        }
        else {
            $("#UnitNumber").html("").hide()
        }
    }).keypress(function (e) {
        if (e.which == 13) { $("#unitnumberfilteronModelResult").hide(); }
    }).focus(function () {

        var beginAssetfilteronModel = $.trim($('#beginAssetfilteronModel').val());
        if ($(this).val() == "" && beginAssetfilteronModel == "") {
            //does not call database. it just requery the json in the in hidden field
            fxn_ReloadUnitNumberByCountry()
            return false;
        }

    }).click(function () {
        $(this).click();
    });




    //---------------------------------------------------------------------------------
    //-----------------START - AUTO SUGGESTION TEXBOXES--------------------------------
    //---------------------------------------------------------------------------------

    $("[id$=_FleetModelCodeTextbox]").keyup(function (e) {
        clearShortMessage();
        ButtonGroupingStatus(false)
        var isModelCodeAddedInDiv = ($("[id$=_HiddenFieldGetFleetModelCodesAutoSuggest]").val() == "" ? false : true)
        FleetModelCodeTextbox = $.trim($(this).val());

        if (FleetModelCodeTextbox == "") {
            $("#loadButton").attr("disabled", "disabled").addClass("buttondisabled")
            clearFleetModelDiv(Mode.ClearAll);

            $("#FleetModelsuggestionResultV2").html("").show()
            if (isModelCodeAddedInDiv == true) fxn_ReloadFleetModels(null)
            else fxn_GetFleetModelCodesAutoSuggest(null)
            EventPropagation(e)
        }
        else {

            $("#loadButton").removeAttr("disabled").addClass("Button_Standard").removeClass("buttondisabled")
            if (e.which == 13) {  // detect the enter key
                $('#loadButton').click(); // fire a sample click,  you can do anything
            }
            else {
                if (isModelCodeAddedInDiv == true) fxn_ReloadFleetModels(FleetModelCodeTextbox)
                else fxn_GetFleetModelCodesAutoSuggest(FleetModelCodeTextbox)
            }
        }
        $(this).val(FleetModelCodeTextbox.toUpperCase())
    }).focus(function (e) {
        $(this).click()
        $("#FleetModelsuggestionResultV2").show()
    }).keypress(function (e) {
        if (e.which == 13) { $("#FleetModelsuggestionResultV2").hide(); }
    }).click(function () {
        $(this).click()
    }).hover(function (e) {
        FleetModelCodeTextbox = $.trim($(this).val());
        if (FleetModelCodeTextbox == '') {
            if ($.trim($("#FleetModelsuggestionResultV2").html()) == '') {
                fxn_GetFleetModelCodesAutoSuggest(null)
            }
            else {
                if (countryCurrent != countryPrevious) {
                    fxn_GetFleetModelCodesAutoSuggest(null)
                    countryPrevious = countryCurrent
                }
            }
        }
        else {
            if (countryCurrent != countryPrevious) {
                fxn_GetFleetModelCodesAutoSuggest(null)
                countryPrevious = countryCurrent
            }
        }
    });


    $("#ManufacturerSelect").change(function () {
        $("[id$=_modelcodelTextboxInEditManufacturerModel]").val("")
        $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val("")
        if (carmodelPrevious == '' && carmodelCurrent == '') {
            carmodelPrevious = $("#ManufacturerSelect option:selected").val();
            carmodelCurrent = $("#ManufacturerSelect option:selected").val();
        }
        else {
            carmodelCurrent = $("#ManufacturerSelect option:selected").val();
        }
    });


    $("#trmodelcode").hover(function () {
        if (carmodelCurrent == '' && carmodelPrevious == '') return;
        if (carmodelCurrent != carmodelPrevious) {
            if ($("[id$=_modelcodelTextboxInEditManufacturerModel]").val() == "") {
                GetManufacturerModeLCodeAutoSuggest(null)
                $("#modelcodelresultV2").show();
            }
            carmodelPrevious = carmodelCurrent
        }
    });

    $("[id$=_modelcodelTextboxInEditManufacturerModel]").keyup(function (e) {
        var modelcodelTextboxInEditManufacturerModel = $.trim($(this).val())
        clearShortMessage();

        if (modelcodelTextboxInEditManufacturerModel == "") {
            $("#modelcodelresultV2").empty();
            $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val("")
            //empty all validations messages.
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper]").parent().next().html("")

            GetManufacturerModeLCodeAutoSuggest(null)
            $("#modelcodelresultV2").show();
            return false;
        }

        GetManufacturerModeLCodeAutoSuggest(modelcodelTextboxInEditManufacturerModel)
        EventPropagation(e)

    }).focus(function () {
    }).keypress(function (e) {
        if (e.which == 13) {
            $("#modelcodelresultV2").hide();
        }
    });


    //---------------------------------------------------------------------------------
    //-----------------END - AUTO SUGGESTION TEXBOXES--------------------------------
    //---------------------------------------------------------------------------------



    //---------------------------------------------------------------------------------
    //-----------START - SAVE FXN INSIDE POPUP FOR MANUFACTURER MODEL SECTION----------
    //---------------------------------------------------------------------------------

    $("#ManufacturerSelect").change(function () {
        ManufacturerID = $("#ManufacturerSelect option:selected").val();
        if (ManufacturerID != "-1") EnablingModeForEditManufacturerModelTable(true)
        else EnablingModeForEditManufacturerModelTable(false)

        GetManufacturerModeLCodeAutoSuggest(null)
        $("#modelcodelresultV2").show();
    });

    $("#NewEditmanufacturerModelsaveButton").click(function () {


        validator.element("#ManufacturerSelect");
        if (!validator.valid()) return false;


        validator.element("[id$=_modelcodelTextboxInEditManufacturerModel]");
        if (!validator.valid()) {
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper]").parent().next().html(CONST_REQUIRED_ELEMENT)
            return false;
        }
        else {
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper]").parent().next().html("")
        }
        if ($("[id$=_modelcodelTextboxInEditManufacturerModel]").val() == "") {
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_modelcodelTextboxInEditManufacturerModel_dropWrapper]").parent().next().html(CONST_REQUIRED_ELEMENT)
            return false;

        }

        validator.element("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]");
        if (!validator.valid()) {
            $("[id$=ctl00_ContentPlaceHolder_ManufacturerModelTextBoxInEditManufacturerModel_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_ManufacturerModelTextBoxInEditManufacturerModel_dropWrapper]").parent().next().html(CONST_REQUIRED_ELEMENT)
            return false;
        }
        else {
            $("[id$=ctl00_ContentPlaceHolder_ManufacturerModelTextBoxInEditManufacturerModel_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_ManufacturerModelTextBoxInEditManufacturerModel_dropWrapper]").parent().next().html("")
        }

        validator.element("#CountrySelect");
        if (!validator.valid()) return false;

        validator.element("#regionSelect");
        if (!validator.valid()) return false;

        validator.element("#fueltypeSelect");
        if (!validator.valid()) return false;

        validator.element("#TransmissionSelect");
        if (!validator.valid()) return false;

        validator.element("#ServiceScheduleSelect");
        if (!validator.valid()) return false;

        validator.element("#ChassisCostText");
        if (!validator.valid()) return false;


        clearShortMessage();
        var name = $("#EditManufacturerModelTable").attr("name")
        var isadd = (name.indexOf("Add") != -1 ? true : false);


        ManufacturerID = $("#ManufacturerSelect option:selected").val();                                  //4

        if (!isadd) {
            FleetManufacturerModelID = $("[id$=_modelcodelTextboxInEditManufacturerModel]").attr("class") /* 421         AIMSProd..FleetModelManufacturerModel.ManufacturerModelId*/
            FleetManufacturerModelID = $.trim(FleetManufacturerModelID.replace("valid", ""))
        } else { FleetManufacturerModelID = 0 }


        ManufacturerModelCode = $("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").attr("class") /* CorollaConq AIMSProd..FleetManufacturerModel.ManufacturerModelCode*/
        ManufacturerModelCode = $.trim(ManufacturerModelCode.replace("valid", ""))


        CountryId = $("#CountrySelect option:selected").val();
        FuelType = $("#fueltypeSelect option:selected").val()
        Transmission = $("#TransmissionSelect option:selected").val()
        ManufacturerModel = $.trim($("[id$=_ManufacturerModelTextBoxInEditManufacturerModel]").val())     /*Corolla Conquest auto  AIMSProd..FleetManufacturerModel.ManufacturerModel	*/
        ServiceScheduleId = $("#ServiceScheduleSelect option:selected").val();
        ChassisCost = $("#ChassisCostText").val();

        iChassisCostId = $("#ChassisCostId").val();
        iRegionModelId = $("#RegionModelId").val();
        iFleetModelManufacturerModelId = $("#FleetModelManufacturerModelId").val();
        iRegionId = $("#regionSelect option:selected").val();

        UserCode = $("[id$=_ManufacturerIDhidden]").val()

        //edit part
        if ((fleetModelId == -1 || fleetModelId == null))                                                   //994
        {
            setErrorShortMessage(CONST_SELECT_MODE_CODE)
            return;
        }

        parameter = {
            FleetModelId: fleetModelId,
            FleetManufacturerModelID: FleetManufacturerModelID,
            ManufacturerModelCode: ManufacturerModelCode,
            CountryId: CountryId,
            FuelType: FuelType,
            Transmission: Transmission,
            ManufacturerModel: ManufacturerModel,
            ServiceScheduleId: ServiceScheduleId,
            ChassisCost: ChassisCost,
            iChassisCostId: iChassisCostId,
            iRegionModelId: iRegionModelId,
            iFleetModelManufacturerModelId: iFleetModelManufacturerModelId,
            iRegionId: iRegionId,
            UserCode: UserCode,
            ManufacturerID: ManufacturerID
        }

        RequestAjaxCall(CONST_INSERTUPDATEFLEETMANUFACTURERMODEL, parameter)

    });

    //---------------------------------------------------------------------------------
    //-----------END - SAVE FXN INSIDE POPUP FOR MANUFACTURER MODEL SECTION----------
    //---------------------------------------------------------------------------------


    //---------------------------------------------------------------------------------
    //-----------START - SAVE FXN INSIDE FLEET MODEL SECTION---------------------------
    //---------------------------------------------------------------------------------

    $("#SaveFleetModelButton").click(function () {
        validator.element("#FleetModelCodeDataTextbox");
        if (!validator.valid()) return false;

        validator.element("#FleetModelDescriptionDataTextbox");
        if (!validator.valid()) return false;


        validator.element("#DailyRunningCosttextBox");
        if (!validator.valid()) return false;

        validator.element("#DailyDelaytextBox");
        if (!validator.valid()) return false;

        //alert('todo: call and save to db');
        FleetModelCodeDataTextbox = $.trim($("#FleetModelCodeDataTextbox").val())
        FleetModelDescriptionDataTextbox = $.trim($("#FleetModelDescriptionDataTextbox").val())
        ConversionRequiredCheckbox = 1 //($("#ConversionRequiredCheckbox").is(":checked") ? 1 : 0)
        isLocalcheckbox = ($("#isLocalcheckbox").is(":checked") ? 1 : 0)
        DailyRunningCosttextBox = $("#DailyRunningCosttextBox").val()
        DailyDelaytextBox = $("#DailyDelaytextBox").val()
        UserCode = $.trim($("[id$=_ManufacturerIDhidden]").val())
        inactiveDate = $("#ctl00_ContentPlaceHolder_inactiveDatePicker_dateTextBox").val()

        parameter = { FleetModelId: (isAdd == false ? fleetModelId : "-1"), FleetModelCode: FleetModelCodeDataTextbox, FleetModelDescription: FleetModelDescriptionDataTextbox, ConversionIndicator: ConversionRequiredCheckbox, IsLocal: isLocalcheckbox, DailyRunningCost: DailyRunningCosttextBox, DailyDelayCost: DailyDelaytextBox, UserCode: UserCode, InactiveDate: inactiveDate }
        RequestAjaxCall(CONST_INSERTUPDATEFLEETMODELCODE, parameter)
    });
    //FLEET MODEL SECTION: display this block when clicking Button or if Search has found

    //---------------------------------------------------------------------------------
    //-----------END - SAVE FXN INSIDE FLEET MODEL SECTION---------------------------
    //---------------------------------------------------------------------------------


    $("#CancelFleetModelButton").click(function () {
        clearShortMessage();
        clearFleetModelDiv(Mode.Cancel);
        clearFleetModelDiv(Mode.ClearAll);
        ButtonGroupingStatus(false)
        isAdd = false;
    });


    //---------------------------------------------------------------------------------
    //-----------START - ALLOCATE UNIT NUMBER------------------------------------------
    //---------------------------------------------------------------------------------
    $("#AllocatedUnitNumberEditButton").click(function () {
        clearShortMessage();

        fleetnumberid = $("#hiddenFleetNumberId").val()
        if (fleetnumberid == "0") {
            setErrorShortMessage(CONST_TICK_ALLOCATED_UNIT_NUMBER)
            return false;
        }
        LoadPopupAllocateUnitNumber()
    });

    $("#AllocateUnitNumberSaveButton").click(function () {
        validator.element("[id$=_AllocateUnitNumberFleetModelTextbox]");
        if (!validator.valid()) {
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberFleetModelTextbox_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberFleetModelTextbox_dropWrapper]").parent().next().html(CONST_REQUIRED_ELEMENT)
            return false;
        }
        else {
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberFleetModelTextbox_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberFleetModelTextbox_dropWrapper]").parent().next().html("")
        }

        validator.element("[id$=_AllocateUnitNumberManufacturerTextbox]");
        if (!validator.valid()) {
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberManufacturerTextbox_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberManufacturerTextbox_dropWrapper]").parent().next().html(CONST_REQUIRED_ELEMENT)
            return false;
        }
        else {
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberManufacturerTextbox_dropWrapper] label.error").text("")
            $("[id$=ctl00_ContentPlaceHolder_AllocateUnitNumberManufacturerTextbox_dropWrapper]").parent().next().html("")
        }


        validator.element("#AllocateUnitNumberBeginUnitNumberTextbox");
        if (!validator.valid()) return false;

        validator.element("#AllocateUnitNumberEndUnitNumberTextbox");
        if (!validator.valid()) return false;

        ManufacturerID = $("[id$=_AllocateUnitNumberManufacturerTextbox]").attr("class")
        ManufacturerID = $.trim(ManufacturerID.replace("valid", ""))
        CountryId = $("#CountryDropdown option:selected").val()
        UserCode = $.trim($("[id$=_ManufacturerIDhidden]").val())

        var beginNumber = $.trim($("#AllocateUnitNumberBeginUnitNumberTextbox").val())
        var endNumber = $.trim($("#AllocateUnitNumberEndUnitNumberTextbox").val())
        fleetnumberid = $("#hiddenFleetNumberId").val()

        parameter = { FleetModelId: fleetModelId,
            UserCode: UserCode,
            ManufacturerID: ManufacturerID,
            CountryId: CountryId,
            BeginNumber: beginNumber,
            EndNumber: endNumber,
            FleetNumberId: fleetnumberid
        }

        RequestAjaxCall(CONST_ALLOCATEEDITNUMBER, parameter)
    });

    $("#AllocateUnitNumberCancelButton").click(function () {
        $("[name='allocatedUnitNumbers_CheckBoxes'],[name='ManufacturerId_CheckBoxes']").prop("checked", false);
    });

    //---------------------------------------------------------------------------------
    //-----------END - ALLOCATE UNIT NUMBER------------------------------------------
    //---------------------------------------------------------------------------------

    $("#beginAssetfilteronModel,#DailyRunningCosttextBox, #DailyDelaytextBox, #ChassisCostText, #AllocateUnitNumberBeginUnitNumberTextbox, #AllocateUnitNumberEndUnitNumberTextbox").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    $("#DailyRunningCosttextBox, #DailyDelaytextBox, #ChassisCostText").blur(function () {
        this.value = fxn_currencyformatted(this.value)
    });

});

function RequestAjaxCall(mode, parameter) {
    var fleetmodels = null;
    var content = ""
    $.post(CONST_URL + mode, parameter, function (result) {

        switch (mode) {
            case CONST_INSERTUPDATEFLEETMODELCODE:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1)
                    setErrorMessage(result);
                else {
                    setInformationShortMessage(CONST_FLEET_MODEL_UPDATED);

                    if (fleetModelId == "-1")
                        isAdd = false;
                    else $("#FleetModelsuggestionResultV2").empty();

                    fleetModelId = result
                    $("#ctl00_ContentPlaceHolder_FleetModelCodeTextbox").val($("#FleetModelCodeDataTextbox").val())
                    $("#loadButton").removeAttr("disabled").addClass("Button_Standard").removeClass("buttondisabled")
                    GetFleetModelDetails()
                }
                break;
            
            case CONST_GETFLEETMODELCODESAUTOSUGGEST:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }

                $("[id$=_HiddenFieldGetFleetModelCodesAutoSuggest]").val(result)

  
                fleetmodels = jQuery.parseJSON(result);
                fxn_FleetModels(fleetmodels.FleetModels)
                break;

            case CONST_GETSINGLEFLEETMODELCODE:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }

                fleetmodels = jQuery.parseJSON(result);
                if (fleetmodels.FleetModels.length == 0) {
                    setWarningShortMessage(CONST_FLEET_IS_EMPTY)
                    $("#FleetModelDiv").hide()
                    ButtonGroupingStatus(false)
                    return;
                }

                content = "";
                $.each(fleetmodels, function (index, fleetmodel) {
                    var fleetmodellength = fleetmodel.length - 1
                    for (var ctr = 0; ctr <= fleetmodellength; ctr++) {
                        fxn_GetSingleFleetModelCode(fleetmodel, ctr);
                    }
                });

                if (loadFleetModel) {
                    //reload fleetmodel details
                    GetFleetModelDetails()
                    loadFleetModel = false;
                }
                break;

            case CONST_GETDEFAULTCOUNTRY:
                clearMessages()
                var country = -1
                if (result == "AU") {
                    $("#CountryDropdown, #AllocateUnitNumberCountrySelect").val(1)
                    countryCurrent = 1
                }
                else {
                    $("#CountryDropdown, #AllocateUnitNumberCountrySelect").val(8)
                    countryCurrent = 8
                }

                if (countryPrevious == '') countryPrevious = countryCurrent
                break;

            case CONST_GETCOUNTRIES:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }
                
                var countries = jQuery.parseJSON(result);
                $.each(countries, function (index, country) {
                    var countrylength = country.length - 1
                    for (var ctr = 0; ctr <= countrylength; ctr++) {
                        option = option + AddOptionItem(country[ctr].CountryId, country[ctr].CountryDescription)
                    }
                    $("#CountryDropdown, #AllocateUnitNumberCountrySelect").html(option);
                    option = "";
                });

                parameter = { UserCode: $("[id$=_ManufacturerIDhidden]").val() }
                RequestAjaxCall(CONST_GETDEFAULTCOUNTRY, parameter)
                break;

            case CONST_LOADDATAFORMANUFACTURERMODEL:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }
                
                var completeData = jQuery.parseJSON(result);
                //---load data and create table for MANUFACTURER MODEL
                var manufacturermodels = completeData.ManufacturerModels;
                manufacturermodels = $.grep(manufacturermodels, function (element, index) {
                    return element.CountryId == CountryId;
                });
                

                //---load data and create table NEW/EDIT MANUFACTURER MODEL
                var manufacturers = completeData.Manufacturers;
                manufacturers = $.grep(manufacturers, function (element, index) {
                    return element.CountryId == CountryId;
                });

                var countries = completeData.Countries;
                var exteriors = completeData.ExteriorInteriors;
                var fuels = completeData.Fuels;
                var transmissions = completeData.Transmission;
                var schedules = completeData.ServiceSchedule;

                NewEditManufacturerGrid(manufacturers,
                                        countries,
                                        manufacturermodels,
                                        exteriors,
                                        fuels,
                                        transmissions,
                                        schedules);


                break;

            case CONST_INSERTUPDATEFLEETMANUFACTURERMODEL:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1)
                    setErrorMessage(result);
                else {
                    //reload fleetmodel details
                    GetFleetModelDetails()
                    UnloadPopupAddNewManufacturingModel()
                    setInformationShortMessage(CONST_MANUFACTURING_MODEL_UPDATED);
                }
                break;

            //--------------------------------------------------------------------------//                                   
            //trigger by $("#loadButton") and $("#NewEditmanufacturerModelsaveButton")                                     
            //--------------------------------------------------------------------------//                                    
            case CONST_GETFLEETMODELDETAILS:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }
                $("#AllocatedUnitNumberAddButton").click(function () {
                    var length = $("[name='allocatedUnitNumbers_CheckBoxes']:checked").length;
                    if (length == 1) {
                        CountryId = $("#CountryDropdown option:selected").val();
                        fleetnumberid = $("#hiddenFleetNumberId").val();
                        window.location = "LoadVehiclePlan.aspx?iFleetNumberId=" + fleetnumberid + "&iCountryId=" + CountryId;
                    }
                    else
                        setErrorShortMessage(CONST_TICK_ALLOCATED_UNIT_NUMBER);

                });

                if ($("#ManufacturerSelect option").length - 1 == -1) {
                    option = option + AddOptionItem("-1", CONST_PLEASE_SELECT)
                    $("#EditManufacturerModelTable select").html(option)
                }

                fleetmodels = jQuery.parseJSON(result);
                fxn_GetSingleFleetModelCode(fleetmodels.FleetModelDetails, 0)

                var manufacturerModels = fleetmodels.ManufacturerModels;
                fxn_manufacturerModels(manufacturerModels)

                var availableModels = fleetmodels.AvailableModels

                var allocatedUnitNumbers = fleetmodels.AllocatedUnitNumbers
                fxn_allocatedUnitNumbers(allocatedUnitNumbers)

                var vehiclePlans = fleetmodels.VehiclePlans
                fxn_vehiclePlans(vehiclePlans)

                var regions = fleetmodels.Regions
                fxn_regions(regions)

                var links = fleetmodels.AvailableModels
                fxn_linkmanufacturerModels(links)

                RequestAjaxCall(CONST_LOADDATAFORMANUFACTURERMODEL, {})
                RequestAjaxCall(CONST_GETNUMBERRULES, {})


                break;

            case CONST_GETMANUFACTURERMODELCODEAUTOSUGGEST:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }

                var currentOption = $("#ManufacturerSelect option:selected").val();
                var ManufacturerModeLCodes = jQuery.parseJSON(result);
                ManufacturerModeLCodes.ManufacturerModeLCode = $.grep(ManufacturerModeLCodes.ManufacturerModeLCode, function (element, index) {
                    return (element.CountryId == CountryId && element.ManufacturerId == currentOption);
                });
                if (ManufacturerModeLCodes.ManufacturerModeLCode != 0) {
                    fxn_manufacturerModelCodes(ManufacturerModeLCodes.ManufacturerModeLCode)
                }

                break;

            case CONST_GETNUMBERRULES:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }
                $("[id$=_UnitNumberhidden]").val(result);

                fxn_ReloadUnitNumberByCountry()

                $("#UnitNumber").show()

                modelcodesarray = $.unique(modelcodesarray);
                $("[id$=_ModelCodeshidden]").val(modelcodesarray.sort())

                beginassetarray = $.unique(beginassetarray);
                $("[id$=_BeginAssehidden]").val(beginassetarray.sort())

                fxn_ReloadUnitNumberByCodeInKeyPress(modelcodesarray)

                $('.unitnumber').bind("click", function (e) {
                    var text = $(this).attr("id");
                    //fleetModelId = $(this).attr("name");
                    var fleetmodelcode = $(this).attr("name")
                    $("[id$=_unitnumberfilteronModel]").val($.trim(fleetmodelcode));

                    $(".unitnumber").prop("checked", false);
                    $(this).prop("checked", true);
                    fxn_ReloadUnitNumberByCode(text, '')
                    
                });

                break;

            case CONST_LINKFLEETMODELMANUFACTURERMODEL:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }
                
                if (result != null && result.indexOf("ERROR") != -1)
                    setInformationShortMessage(result);
                else {
                    //reload fleetmodel details
                    GetFleetModelDetails()
                    UnloadPopupLinkManufacturingModel()
                    setInformationShortMessage(CONST_LINK_FLEET_MODEL_UPDATED);
                }
                break;
            case CONST_GETMANUFACTURERAUTOSUGGEST:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1) {
                    setErrorMessage(result);
                    return;
                }
                
                var manufacturernames = jQuery.parseJSON(result);
                fxn_manufacturernames(manufacturernames.ManufacturerNames)
                break;

            case CONST_ALLOCATEEDITNUMBER:
                clearMessages()
                if (result != null && result.indexOf("ERROR") != -1)
                    setErrorMessage(result);
                else {
                    //reload fleetmodel details
                    GetFleetModelDetails()
                    UnloadPopupAllocateUnitNumber()
                    setInformationShortMessage(CONST_ALLOCATE_NUMBER_UPDATED);
                }
                break;

        }
    });
}

function fxn_GetFleetModelCodesAutoSuggest(param) {
    CountryId = $("#CountryDropdown option:selected").val();
    parameter = { FleetModel: param, CountryId: CountryId }
    RequestAjaxCall(CONST_GETFLEETMODELCODESAUTOSUGGEST, parameter)
}


function GetFleetModelDetails() {
    CountryId = $("#CountryDropdown option:selected").val();
    if (window.location.href.indexOf("iCountryId") != -1) {
        if (unescape(getUrlVars()["iCountryId"]) != "undefined") {
            CountryId = unescape(getUrlVars()["iCountryId"])

        }
    }
    if (CountryId == -1) {
        CountryId = $("#CountryDropdown option:selected").val();
    }
    parameter = { FleetModelId: fleetModelId, CountryId: CountryId }
    RequestAjaxCall(CONST_GETFLEETMODELDETAILS, parameter)
}

function GetManufacturerModeLCodeAutoSuggest(param) {
    var currentOption = $("#ManufacturerSelect option:selected").val();
    CountryId = $("#CountryDropdown option:selected").val();
    parameter = { ManufacturerModeLCode: param, CountryId: CountryId, ManufacturerID: currentOption }
    RequestAjaxCall(CONST_GETMANUFACTURERMODELCODEAUTOSUGGEST, parameter)
}

function GetSingleFleeDetail(param) {
    CountryId = $("#CountryDropdown option:selected").val();
    parameter = { FleetModel: param, CountryId: CountryId }
    RequestAjaxCall(CONST_GETSINGLEFLEETMODELCODE, parameter)
}

function EventPropagation(e) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
}

