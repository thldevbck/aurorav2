﻿function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function FormatDate(_date) {
    if (_date == null)
        return _date;

    var dateValue = _date.substr(0, 10).split('-');

    if (dateValue.length == 3)
        return dateValue[2] + '/' + dateValue[1] + '/' + dateValue[0];
    else
        return _date;
}

function ShowPopBox() {
    var scrOfY = 0;
    if (typeof (window.pageYOffset) == 'number') {
        //Netscape compliant
        scrOfY = window.pageYOffset;
    } else if (document.body && (document.body.scrollLeft || document.body.scrollTop)) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
    } else if (document.documentElement && (document.documentElement.scrollLeft || document.documentElement.scrollTop)) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
    }

    $(".modalPopup").each(function () {
        //popup position
        $(this).css({
            "top": scrOfY + document.documentElement.clientHeight / 2 - $(this).height() / 2,
            "left": document.documentElement.clientWidth / 2 - $(this).width() / 2
        });
    });

    $('#screen').css({ "display": "block", opacity: 0.7, "width": $(document).width(), "height": $(document).height() });
    //$('body').css({ "overflow": "hidden" });
    // $('#box').css({ "display": "block" });
}

function HidePopBox() {
    $('#screen').css("display", "none");
   // $('body').css({ "overflow": "auto" });
    // $('#box').css("display", "none");
}

function ParseDateFormat(originalDate) {
    if (originalDate != null) {
        var splitDate = originalDate.split('-');

        return splitDate[2].substring(0, 2) + '/' + splitDate[1] + '/' + splitDate[0];
    }
    return originalDate;
}