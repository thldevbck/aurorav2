﻿$(document).ready(function () {
    var $dragging = null;
    var x = 0;
    var y = 0;

    $(".moveableDisplayUnitNumber, .moveable").on("mousedown", function (e) {
        $dragging = $(this)
        var offset = $(this).offset();
        x = e.pageX - offset.left;
        y = e.pageY - offset.top;
    });


    $("#UnitNumber").scroll(function (e) {
        $dragging = null;
    });

    
    $(".moveable input[type='text'], .moveable .Button_Standard, .moveable select, .moveable select, .moveable input[type='radio']").scroll(function (e) {
        $dragging = null;
    });
    

    $("pnlEditBulk.table tr").scroll(function (e) {
        $dragging = null;
    });


    $("pnlEditBulk.table tr, .moveable input[type='text'], .moveable .Button_Standard, .moveable select, .moveable input[type='radio'], #beginAssetfilteronModel, .moveable radio, [id$=_unitnumberfilteronModel],#AllocateUnitNumberBeginUnitNumberTextbox,#AllocateUnitNumberEndUnitNumberTextbox").focus(function (e) 
    {
        $dragging = null;
    }).mousedown(function (e) 
    {
        $dragging = null;
    }).mousemove(function (e)
    {
        $dragging = null;
    }).click(function (e) 
    {
        $dragging = null;
    }).hover(function (e)
    {
        $dragging = null;
    }).keypress(function (e) 
    {
        $dragging = null;
    }).keyup(function (e) 
    {
        $dragging = null;
    });

   

    $(document.body).on("mouseup", function (e) {
        $dragging = null;
    });

    $(document.body).on("mousemove", function (e) {
        if ($dragging) {
            $dragging.offset({
                top: e.pageY - y,
                left: e.pageX - x
            });
        }
    });

});
