﻿(function ($, window, document, undefined) {
    $.widget("custom.combobox", {
        _create: function () {
            this.wrapper = $("<span>")
            // .addClass("custom-combobox")
            .insertAfter(this.element);
            this.element.hide();
            this._createAutocomplete();
        },

        _createAutocomplete: function () {
            var selected = this.element.children(":selected"), value = selected.val() ? selected.text() : "";

            this.input = $("<input>")
            .appendTo(this.wrapper)
            .val(value)
            .attr("title", "")
            //.addClass("custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left")
            .autocomplete({
                delay: 0,
                minLength: 0,
                source: $.proxy(this, "_source"),
                messages: {
                    noResults: '',
                    results: function () { }
                }
            });
            this._on(this.input, {
                autocompleteselect: function (event, ui) {
                    ui.item.option.selected = true;
                    this._trigger("select", event, {
                        item: ui.item.option
                    });
                },
                autocompletechange: "_removeIfInvalid"
            });
        },

        _source: function (request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            response(this.element.children("option").map(function () {
                var text = $(this).text();
                if (this.value && (!request.term || matcher.test(text)))
                    return {
                        label: text,
                        value: text,
                        option: this
                    };
            }));
        },

        _removeIfInvalid: function (event, ui) {
            $("#invalidFleetModel").hide();

            // Selected an item, nothing to do
            if (ui.item) {
                return;
            }
            // Search for a match (case-insensitive)
            var value = this.input.val(),
            valueLowerCase = value.toLowerCase(),
            valid = false;

            this.element.children("option").each(function () {
                if ($(this).text().toLowerCase() === valueLowerCase) {
                    $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
                    this.selected = valid = true;
                    return false;
                }
            });
            // Found a match, nothing to do
            if (valid) {
                return;
            }

            // Remove invalid value
            this.element.val("");

            this.input.data("ui-autocomplete").term = "";
            $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", "disabled");
            if ($("[id$=_txbFromUnitNumber]").val() == "" && $("[id$=_txbToUnitNumber]").val() == "" && this.element.val() == ""  && $("[id$=_registrationTextBox]").val() == "") {
                $("#invalidFleetModel").show();
                return false;
            }
            else {
                $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
            }

            
        },

        _destroy: function () {
            this.wrapper.remove();
            this.element.show();
        }
    });

    //if any one of the individual unit checkbox ticked, check the chkSelectAll
    //    function ToggleBulkCheckboxes(uniCheckboxes, allCheckbox) {
    //        uniCheckboxes.click(function () {
    //            allCheckbox.prop("checked", false);

    //            uniCheckboxes.each(function () {                
    //                if ($(this).prop("checked")) {
    //                    allCheckbox.prop("checked", true);
    //                }
    //            });

    //        });        
    //    }

    $(function () {
        var windowWidth = document.documentElement.clientWidth;
        var windowHeight = document.documentElement.clientHeight;


        //implement autocomplete to "Fleet Model Code" drop down list
        $(".ddlFleetModelCode").combobox();

        //bulk edit checkbox in "new vehicle" search
        $("input[id*=chkSelectUni]").click(function () {
            //if any one of the individual unit checkbox ticked, check the chkSelectAll
            //$("input[id*=chkSelectAll]").prop("checked", ($("#resultrepeater_table [id$='_chkSelectUni']:checked").length > 0 ? true : false));


            var fleetassetId = '';
            $("#resultrepeater_table tr[id$=_tr]").each(function () {
                var ischecked = $(this).find("td :checked").prop("checked");
                if (ischecked)
                    fleetassetId += $(this).attr("name") + ","
            });

            if (fleetassetId != '') {
                $("[id$=_HiddenFieldFleetAssetIds]").empty().val(fleetassetId)
            }
        });

        $("input[id*=chkSelectAll]").click(function () {
            //$("#ctl00_ContentPlaceHolder_resultrepeater_ctl00_chkSelectAll").click(function(){
            var that = $(this);

            $("#resultrepeater_table [id$='_chkSelectUni']").prop("checked", that.prop("checked"));

            var fleetassetId = '';
            $("#resultrepeater_table tr[id$=_tr]").each(function () {
                /*Added by Nimesh to filter the rows which has checkbox and selected only not all*/
                /*It was selecting fleet assets where even the checkbox was not visible*/
                if ($(this).has(':checkbox:checked').length === 1)
                /*End - Added by Nimesh to filter the rows which has checkbox and selected only not all*/

                if ($(this).attr("name") != "")
                    fleetassetId += $(this).attr("name") + ","
            });

            if (fleetassetId != '') {
                $("[id$=_HiddenFieldFleetAssetIds]").empty().val(fleetassetId)
            }

        });

        //"rebranding" search result checkbox

        $("input[id*=chkRebrandingSelectUni]").click(function () {
            var that = $(this);


            //if unit number is same, only one can be checked
            //var unitNumber = that.parent().next().html();
            //that.parent().parent().siblings().children("td:contains('" + $.trim(unitNumber) + "')").prev().children("input").prop("checked", false);
            that.closest("tr").attr("class", "selected")
            var rowindexclick = that.closest("span").attr("name")
            var unitNumber = $.trim(that.closest("tr").find('td').eq(1).html())
            var ischeck = that.prop("checked")

            
            if (ischeck) {
                $("#rebrandingrepeater_table td:contains('" + unitNumber + "')").closest('td').prev().find("span[name != '" + rowindexclick + "']").each(function () {
                    var $this = $(this)
                    $this.find("input[type='checkbox']").prop('checked', false)
                    $this.find("input[type='checkbox']").css("visibility", 'hidden')
                });
            }
            else {
                $("#rebrandingrepeater_table td:contains('" + unitNumber + "')").closest('td').prev().find("span").find("input[type='checkbox']").prop('checked', false).css("visibility", 'visible').removeAttr('disabled')
            }

            /*
            var id = $.trim(that.closest("tr").attr("name"))
            var fleetmodelcode = $.trim(that.closest("tr").find('td').eq(3).html())
            var ticked = false;
            
            $("#rebrandingrepeater_table tr[id$=_tr] td:contains('" + unitNumber + "')").each(function () {

            var $this = $(this)
            var rowindexcurrent = $this.closest("span").attr("name")
            var insideId = $this.closest('tr').attr("name");
            var insidefleetmodelcode = $.trim($this.closest("tr").find('td').eq(3).html())

            if (rowindexcurrent ==  rowindexclick && id == insideId && !ticked && ischeck && fleetmodelcode == insidefleetmodelcode) {
            $this.closest('tr').find('td').eq(0).find("span").find("input[type='checkbox']").prop('checked', true)
            ticked = true;
            }
            else {

            var disabledcheckbox = $this.closest('tr').find('td').eq(0).find("span").find("input[type='checkbox']")
            if (ischeck) {
            $(disabledcheckbox).prop('checked', false)
            $(disabledcheckbox).css("visibility", 'hidden')
            $(disabledcheckbox).removeAttr('disabled')
            }
            else {
            $(disabledcheckbox).prop('checked', false)
            $(disabledcheckbox).css("visibility", 'visible')
            $(disabledcheckbox).removeAttr('disabled')
            }
            }
            });
            */

            var fleetassetId = '';
            $("#rebrandingrepeater_table tr[id$=_tr] :checked").each(function () {
                rowindexclick = $(this).closest("span").attr("name")
                fleetassetId += $(this).closest('tr').attr("name") + "-" + rowindexclick.substring(9) + ","
            });

            if (fleetassetId != '') {
                $("[id$=_HiddenFieldFleetAssetIds]").empty().val(fleetassetId)
            }

        });


        $("input[id*=chkRebrandingSelectAll]").click(function () {
            var that = $(this);
            var fleetassetId = '';
            var temp = '';
            var current = '';
            var trname = '';
            var rowindexclick = that.closest("span").attr("name")

            if (!that.prop("checked")) {
                $("input[id*=chkRebrandingSelectUni]").prop("checked", false).removeAttr('disabled').css('visibility', 'visible')
                return;
            }

            $("input[id*=chkRebrandingSelectUni]").prop("checked", false).prop('disabled','disabled').css('visibility', 'visible')
            $("#rebrandingrepeater_table tr[id$=_tr]").each(function () {
                if ($(this).attr("name") != undefined && $(this).attr("name") != "") {

                    current = $.trim($(this).find('td')[1].innerHTML);
                    trname = $(this).attr("name");

                    if (temp == '' || temp != current) {
                        temp = current;
                        rowindexclick = $(this).find('td').eq(0).find('span').attr("name")
                        $(this).find(':checkbox').prop("checked", that.prop("checked"));
                        fleetassetId += trname  +"-" + rowindexclick.substring(9) + ","
                    }
                    else {

                        $(this).find(':checkbox').prop("checked", !that.prop("checked"));
                        $(this).find(':checkbox').prop("disabled", 'disabled');
                        $(this).find(':checkbox').css("visibility", "hidden")
                    }
                }
            });

            if (fleetassetId != '') {
                $("[id$=_HiddenFieldFleetAssetIds]").empty().val(fleetassetId)
            }
        });

    });
} (jQuery, this, this.document));

