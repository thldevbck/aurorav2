﻿//rev:mia 22Aug2014 - revisions
//  - add new fields to the AimsProd..FleetAsset
//  WarrantyExpDate    DATETIME 
//  SelfContainExpDate DATETIME 
//  TelematicsDevice   NVARCHAR(50) 
//  TelematicsTablet   NVARCHAR(50)
//  - modify Fleet_getUnitNumberDetails
//  - modify Fleet_UpdateFleetDetails

//rev:mia added march 13,2014 -added
var CONST_URL = "AIMSListener/AIMSListener.aspx?MODE="
var CONST_GETDEFAULTCOUNTRY = "GetDefaultCountry"
var CONST_GETSINGLEUNINUMBER = "GetSingleUnitNumber"
var CONST_TICKONEITEM = "Please tick at least one item.";
var CONST_PLEASE_SELECT = '--please select--'
var CONST_REQUIRED_ELEMENT = 'Required'

var option = '';
var assetid = '';

//--------popup--------------
var windowWidth;
var windowHeight;
var popupHeight;
var popupWidth;

var unit = "";
var fleetModelCodel = "";
var startlocation = "";
var deliverydate = "";
var onfleetdate = "";
var onreceivedate = "";
var status = "";
var planstatus = "";

var isFullAccess = false;
var isPartialAccess = false;
var isViewAccess = false;
var isAccessVerified = false;

function ValidateAndSave() {
    return false;
}

$(document).ready(function () {

    if ($("[id$=_HiddenFieldUserCode]").val() != null) {
        RequestAjaxCall('CheckUserRoleForFullAccess', { usercode: $("[id$=_HiddenFieldUserCode]").val(), role: 'AIMSFULL' });
    }

    $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1_confirmationPanel").removeClass("modalPopup").addClass("modalPopupConfirmation")
    CenterConfirmationPopup()
    $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1_rightButton").css("display", "none");


    var ManufacturerDropDown = $("[id$=_ManufacturerDropDown]").attr("id");
    var FuelTypeDropDown = $("[id$=_FuelTypeDropDown]").attr("id");
    var modelCodeDropDown = $("[id$=_modelCodeDropDown]").attr("id");

    $("[id$=_ManufacturerDropDown]").attr("name", ManufacturerDropDown);
    $("[id$=_FuelTypeDropDown]").attr("name", FuelTypeDropDown);
    $("[id$=_modelCodeDropDown]").attr("name", modelCodeDropDown);


    $("#errorForManufacturerDropDown").attr("for", ManufacturerDropDown);
    $("#errorFormodelCodeDropDown").attr("for", modelCodeDropDown);
    $("#errorForFuelTypeDropDown").attr("for", FuelTypeDropDown);
    $("#errorFordateEditDeliveryDate").attr("for", "ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateTextBox");
    $("#ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateTextBox").attr("name", "ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateTextBox");

    $("#errorFordateEditOnFleetDate").attr("for", "ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateTextBox");
    $("#ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateTextBox").attr("name", "ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateTextBox");

    $("#ctl00_ContentPlaceHolder_ddlBulkScheduleCode").addClass("required")

    //$("#errorFordateRebrandingEffectiveDate").attr("for", "ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox");
    $("#ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox").attr("name", "ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox")//.addClass("required")
    $("label[for='ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox']").text("").hide();
    $("#ctl00_ContentPlaceHolder_ddlBulkScheduleCode").addClass("valueNotEquals")


    var to = $("[id$=_txbToUnitNumber]").val()
    var selectedvalue = $("[id$=_hiddenModelCodeId]").val()//$("#ctl00_ContentPlaceHolder_ddlFleetModelCode option:selected").val()
    var from = $("[id$=_txbFromUnitNumber]").val()
    var reg = $('[id$=_registrationTextBox]').val()
    if (selectedvalue == "" && to == "" && from == "" && reg == "") {
      //  $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", "disabled");
    }


    $("[id$=_txbFromUnitNumber]").blur(function () {
        to = $("[id$=_txbToUnitNumber]").val()
        //selectedvalue = $("#ctl00_ContentPlaceHolder_ddlFleetModelCode option:selected").val()
        selectedvalue = $("[id$=_hiddenModelCodeId]").val()

        if ($("[id$=_txbFromUnitNumber]").val() == "" && $("[id$=_txbToUnitNumber]").val() == "" && $("[id$=_registrationTextBox]").val() == "" && selectedvalue == "") {
            $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", true)
        }
        else {
            if ($(this).val() != "") {
                $("[id$=_txbToUnitNumber]").val($(this).val())
                $("#invalidFleetModel").hide();
                $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
            }
            else if ($("[id$=_txbFromUnitNumber]").val() != "" || $("[id$=_txbToUnitNumber]").val() != "" || $("[id$=_registrationTextBox]").val() != "" || selectedvalue != "") {
                $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
            }
            else {
                $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", true)
            }
        }


    });
    $("[id$=_txbToUnitNumber],[id$=_registrationTextBox]").blur(function () {
        from = $("[id$=_txbFromUnitNumber]").val()
        //selectedvalue = $("#ctl00_ContentPlaceHolder_ddlFleetModelCode option:selected").val()
        selectedvalue = $("[id$=_hiddenModelCodeId]").val()

        if ($("[id$=_txbFromUnitNumber]").val() == "" && $("[id$=_txbToUnitNumber]").val() == "" && $("[id$=_registrationTextBox]").val() == "" && selectedvalue == "") {
            $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", true)
        }
        else if ($("[id$=_txbFromUnitNumber]").val() != "" || $("[id$=_txbToUnitNumber]").val() != "" || $("[id$=_registrationTextBox]").val() != "" || selectedvalue != "") {
            $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
        }
        else {
            $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", "disabled");
        }

    });

    $("[id$=_registrationTextBox]").keyup(function () {
        afrom = $("[id$=_txbFromUnitNumber]").val()
        //selectedvalue = $("#ctl00_ContentPlaceHolder_ddlFleetModelCode option:selected").val()
        selectedvalue = $("[id$=_hiddenModelCodeId]").val()

        if ($("[id$=_txbFromUnitNumber]").val() == "" && $("[id$=_txbToUnitNumber]").val() == "" && $("[id$=_registrationTextBox]").val() == "" && selectedvalue == "") {
            $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", true)
        }
        else if ($("[id$=_txbFromUnitNumber]").val() != "" || $("[id$=_txbToUnitNumber]").val() != "" || $("[id$=_registrationTextBox]").val() != "" || selectedvalue != "") {
            $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
        }
        else {
            $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", "disabled");
        }

    });
    //$("[id$=_txbToUnitNumber],[id$=_registrationTextBox],[id$=_txbFromUnitNumber]").keyup(function () {
    //    from = $("[id$=_txbFromUnitNumber]").val()
    //    //selectedvalue = $("#ctl00_ContentPlaceHolder_ddlFleetModelCode option:selected").val()
    //    selectedvalue = $("[id$=_hiddenModelCodeId]").val()

    //    if ($("[id$=_txbFromUnitNumber]").val() == "" && $("[id$=_txbToUnitNumber]").val() == "" && $("[id$=_registrationTextBox]").val() == "" && selectedvalue == "") {
    //        $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", true)
    //    }
    //    else if ($("[id$=_txbFromUnitNumber]").val() != "" || $("[id$=_txbToUnitNumber]").val() != "" || $("[id$=_registrationTextBox]").val() != "" || selectedvalue != "") {
    //        $("#ctl00_ContentPlaceHolder_searchButton").removeAttr("disabled").addClass("Button_Standard").removeClass("submitdisabled");
    //    }
    //    else {
    //        $("#ctl00_ContentPlaceHolder_searchButton").addClass("submitdisabled").attr("disabled", "disabled");
    //    }

    //});



    //$("[id$=_txbToUnitNumber], [id$=_txbFromUnitNumber]").keyup(function () {
    //    this.value = this.value.replace(/[^0-9\.]/g, '');
    //});

    $('[id$=_btnBulkSave]').click(function () {
        //if ($("[id$=_HiddenHasVehOnFleetChecked]").val().trim() === "true") {
        //    $("[id$=_HiddenHasVehOnFleetChecked]").val("false");
        //    return true;
        //}
        if ($("[id$=_ddlBulkRebrandingDropDown]").val() == "" || $("[id$=_ddlBulkRebrandingDropDown]").val() == null)
            return true;
        var assetIdList = $("[id$=_HiddenFieldFleetAssetIds]").val();
        //RequestAjaxCall('CheckIfVehiclesAreOnFleet', { FleetAssetList: assetIdList });
        var reqData = '{ FleetAssetList: ' + assetIdList + ' }'
        //return false;
        var retValue = true;
        $.ajax({
            url: CONST_URL + 'CheckIfVehiclesAreOnFleet',
            type: 'POST',
            datatype: "json",
            data: { FleetAssetList: assetIdList },
            async: false,
            cache: false,
            timeout: 30000,
            error: function () {
                return false;
            },
            success: function (result) {
                if (result.replace("\"\"", "").trim().length > 0) {
                    $("#lblErrorMessage").show();
                    $("#lblErrorMessage").text(result + " Changes not permitted on fleet model code");
                        //$('[id$=_btnBulkSave]').hide();
                        retValue = false;
                    } 
                }
        });
       // $.post(CONST_URL + 'CheckIfVehiclesAreOnFleet', { FleetAssetList: assetIdList }, function (result) {
       //     var message = jQuery.parseJSON(result);
       //     if (message.trim().length > 0) {
       //         $("#lblError").text(message + " Can Not Save");
       //         $('[id$=_btnBulkSave]').hide();
       //         retValue= false;
       //     }
       //     return retValue;
       // });
        return retValue;
    });
    $('[id$=_btnBulkRebrandingSave]').click(function () {
        $("label.error").show();

        $("#ctl00_ContentPlaceHolder_ddlBulkScheduleCode").addClass("valueNotEquals")
        validator.element("#ctl00_ContentPlaceHolder_ddlBulkScheduleCode");

        if (!validator.valid()) {
            $("label[for='ctl00_ContentPlaceHolder_ddlBulkScheduleCode']").text("").hide();
            $("#errorForddlBulkScheduleCode").html(CONST_REQUIRED_ELEMENT)
            $("#ctl00_ContentPlaceHolder_ddlBulkScheduleCode").removeClass("valueNotEquals").removeClass("required")
            return false;
        }
        else {
            //$("label[for='ctl00_ContentPlaceHolder_ddlBulkScheduleCode']").html("").hide();
            $("#errorForddlBulkScheduleCode").text("")
        }


        $("#ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox").addClass("required")
        validator.element("#ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox");
        if (!validator.valid()) {
            $("label[for='ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox']").text("").hide();
            $("#errorFordateRebrandingEffectiveDate").html(CONST_REQUIRED_ELEMENT)
            $("#ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox").removeClass("required")
            return false;
        }
        else {
            // $("label[for='ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox']").html("").hide();
            $("#errorFordateRebrandingEffectiveDate").text("")
        }


    });

    $("[id$=_ManufacturerDropDown]").change(function (e) {
        var item = $(this);
        $("#ctl00_ContentPlaceHolder_modeldescription").val("")
        if ($(item).val() != "-1") {
            $("[id$=_modelCodeDropDown]").removeAttr("disabled");
            var result = jQuery.parseJSON($("[id$=_HiddenHistory]").val());
            var CountryId = $("[id$=_HiddenCountryId]").val();
            loadDataTomodelCodeDropDown("_modelCodeDropDown", CountryId, result, $(item).find("option:selected").text());

        }
    });

    $("[id$=_modelCodeDropDown]").change(function (e) {
        var item = $(this);
        $("#ctl00_ContentPlaceHolder_modeldescription").val("")
        if ($(item).val() != "-1") {
            var result = jQuery.parseJSON($("[id$=_HiddenHistory]").val());
            var CountryId = $("[id$=_HiddenCountryId]").val();
            var manufacturername = $("[id$=_ManufacturerDropDown] option:selected").text()
            loadDataTomodeldescriptionLabel(CountryId, result, manufacturername, $(item).find("option:selected").text())
        }
    });
    $("#topbuttonRebrandingEdit, #bottombuttonRebrandingEdit").click(function (e) {
        clearMessages()
        var ischeck = ($("#rebrandingrepeater_table tr[id$=_tr]").find(":checked").length - 1 != -1 ? true : false)
        if (ischeck) {
            LoadPopup("_pnlEditBulkRebranding");
            $('[id$=_ddlBulkScheduleCode]').val("0").focus()
            $("#ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox").val("");
        }
        else {
            setErrorMessage(CONST_TICKONEITEM);
        }
    });
    $("#ButtonTopUnitEdit, #ButtonBottomUnitEdit").click(function (e) {
        clearMessages()
        var ischeck = ($("#resultrepeater_table tr[id$=_tr]").find(":checked").length - 1 != -1 ? true : false)
        if (ischeck) {
            LoadPopup("_pnlEditBulk");
            $('[id$=_ddlBulkStartLocation]').val("0").focus()
            $('[id$=_ddlBulkRebrandingDropDown]').val("0").focus()
            $("#ctl00_ContentPlaceHolder_dateBulkDeliveryDate_dateTextBox").val("");
            $("#ctl00_ContentPlaceHolder_dateBulkFleetDate_dateTextBox").val("");
            $("#ctl00_ContentPlaceHolder_dateBulkReceivedDate_dateTextBox").val("");
        }
        else {
            setErrorMessage(CONST_TICKONEITEM);
        }
    });


    $(".Button_Cancel, .ClosePopup").click(function () {
        $(".error").html("")
        UnloadPopup();
    });


    $("[id*=_linkUnit]").click(function (e) {

        if (!isFullAccess && !isPartialAccess && !isViewAccess) {
                return false;
        }

        assetid = $.trim($(this).parent().parent().attr("name"))
        unit = $.trim($(this).parent().parent().find("td").eq(2).text())
        fleetModelCodel = $.trim($(this).parent().parent().find("td").eq(3).text())
        startlocation = $.trim($(this).parent().parent().find("td").eq(5).text())
        deliverydate = $.trim($(this).parent().parent().find("td").eq(6).text())
        onfleetdate = $.trim($(this).parent().parent().find("td").eq(7).text())
        onreceivedate = $.trim($(this).parent().parent().find("td").eq(8).text());
        status = $.trim($(this).parent().parent().find("td").eq(9).text());
        planstatus = $.trim($(this).parent().parent().find("td").eq(10).text())

        if ($(this).html() == "View") {
            $("[id$=_btnSaveSingleVehicle]").attr("disabled", "disabled").addClass("submitdisabled")
        }
        else {

            if (isFullAccess || isPartialAccess) {
                $("[id$=_btnSaveSingleVehicle]").removeAttr("disabled")
            }
            if (isViewAccess) {
                $("[id$=_btnSaveSingleVehicle]").attr("disabled", "disabled").addClass("submitdisabled")
            }

        }

        if (assetid != '') {
            RequestAjaxCall('GetUnitNumberDetails', { fleetassetid: assetid });
        }
    });


    $(".dataTableColor a").hover(function (e) {
        //get the fleetassetid
        assetid = $.trim($(this).parent().parent().find("td").eq(8).text())
        $("[id$=_txbEditFleetAssetId]").text($.trim(assetid));
    });

    $("[id$=_RUCPaidToTextBox]").keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });




});

function RequestAjaxCall(mode, parameter) {

    $.post(CONST_URL + mode, parameter, function (result) {
        if (mode == 'CheckUserRoleForFullAccess') {
            if (result == 'True') {
                isFullAccess = true;
                isPartialAccess = false;
                isViewAccess = false;
                AIMSAccess('CheckUserRoleForFullAccess');
            }
            else {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = false;
                RequestAjaxCall('CheckUserRoleForPartialAccess', { usercode: $("[id$=_HiddenFieldUserCode]").val(), role: 'AIMSPARTIAL' });
            }
        }

        if (mode == 'CheckUserRoleForPartialAccess') {
            if (result == 'True') {
                isFullAccess = false;
                isPartialAccess = true;
                isViewAccess = false;
                AIMSAccess('CheckUserRoleForPartialAccess');
            }
            else {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = false;
                RequestAjaxCall('CheckUserRoleForViewAccess', { usercode: $("[id$=_HiddenFieldUserCode]").val(), role: 'AIMSVIEWONLY' });
            }
        }

        if (mode == 'CheckUserRoleForViewAccess') {
            if (result == 'True') {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = true;
                AIMSAccess('CheckUserRoleForViewAccess');
            }
            else {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = false;
                AIMSAccess('CheckUserRoleForNoaccess');
            }
        }


        if (mode == 'CheckUserRoles') {


            var hidecontrol = '';
            var disablecontrol = '';
            var labelcolor = '';

            hidecontrol = (result == 'True' ? 'visible' : 'hidden')
            disablecontrol = (result == 'True' ? '' : 'disable')
            labelcolor = (result == 'True' ? 'black' : 'black')

            $('[id$=_rblNewVehicle] :radio').eq(1).css("visibility", hidecontrol).next().css("visibility", hidecontrol)


            if (result == 'True')
                $("#editvehicletable input, #editvehicletable textarea,#editvehicletable select").removeAttr("disabled")
            else {
                $("#editvehicletable input, #editvehicletable textarea,#editvehicletable select").removeAttr("disabled")
            }

            $("#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox").removeAttr('disabled').closest('td').prev('td').css("color", labelcolor)
            $("#ctl00_ContentPlaceHolder_dateCofDate_dateTextBox").removeAttr('disabled').closest('td').prev('td').css("color", labelcolor)
            $("#ctl00_ContentPlaceHolder_RUCPaidToTextBox").removeAttr('disabled').closest('td').prev('td').css("color", labelcolor)
            $("#ctl00_ContentPlaceHolder_txtcomments").removeAttr('disabled')
            $("#ctl00_ContentPlaceHolder_txtcomments").css("color", labelcolor)

            $('#txthistory').on('focus mousedown click mouseover', function (e) {
                if (e.stopPropagation) {
                    e.stopPropagation();
                    e.preventDefault();
                }
                else {
                    e.cancelBubble = true;
                    e.returnValue = false;
                }
                this.blur();
                window.focus();

            });


            if (result == 'False') {
                $("#editvehicletable input, #editvehicletable textarea, #editvehicletable select").attr("readonly", "readonly")
                $("#ctl00_ContentPlaceHolder_RUCPaidToTextBox").removeAttr("readonly")
                $("#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox").removeAttr("readonly")
                $("#ctl00_ContentPlaceHolder_dateCofDate_dateTextBox").removeAttr("readonly")
                $("#ctl00_ContentPlaceHolder_txtcomments").removeAttr("readonly")



                $("#ctl00_ContentPlaceHolder_dateEditReceivedDate_dateImage").prop("onclick", null)
                //rev:mia 22Aug2014
                //$("#ctl00_ContentPlaceHolder_RUCRenewDateControl_dateImage").prop("onclick", null)
                $("#ctl00_ContentPlaceHolder_SelfContainmentDateControl_dateTextBox").prop("onclick", null)
                $("#ctl00_ContentPlaceHolder_WarrantyExpiryDateControl_dateImage").prop("onclick", null)
                $("#ctl00_ContentPlaceHolder_dateEditRegistrationDate_dateImage").prop("onclick", null)
                $("#ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateImage").prop("onclick", null)
                $("#ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateImage").prop("onclick", null)


                $('#editvehicletable select, #editvehicletable input, #editvehicletable #txthistory').on('focus mousedown click mouseover', function (e) {
                    var myid = $(this).attr("id");
                    if (myid != 'ctl00_ContentPlaceHolder_txtcomments' &&
                       myid != 'ctl00_ContentPlaceHolder_RUCPaidToTextBox'
                       && myid != 'ctl00_ContentPlaceHolder_dateCofDate_dateTextBox'
                       && myid != 'ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox'
                       ) {
                        if (e.stopPropagation) {
                            e.stopPropagation();
                            e.preventDefault();
                        }
                        else {
                            e.cancelBubble = true;
                            e.returnValue = false;
                        }
                        this.blur();
                        window.focus();
                    }
                });
            }
        }

        if (mode == 'GetUnitNumberDetails') {
            clearMessages()
            if (result != null && result.indexOf("ERROR") != -1) {
                setErrorMessage(result);
                return;
            }
            $("[id$=_HiddenHistory]").val(result);

            result = jQuery.parseJSON(result);
            option = "";
            var fleetModelCodes = result.FleetModeLCode;
            $("[id$=_RebrandingDropDown]").html("");
            $.each(fleetModelCodes, function (index, fleetModelCode) {
                $("[id$=_RebrandingDropDown]").append(AddOptionItem(fleetModelCode.FleetModelCode, fleetModelCode.FleetModelCode + "&nbsp;-&nbsp;{" + $.trim(fleetModelCode.EffectiveDate) + "}&nbsp;" + "&nbsp;-&nbsp;{" + $.trim(fleetModelCode.ModifiedBy) + "}"))
            });

            $("[id$=_hiddenOptions]").val(option)

            var histories = result.History;
            if (histories != null) {
                option = "<ol>"
                $.each(histories, function (index, history) {
                    option = option + "<li>" + history.History
                });
                option = option + "</ol>"
                $("#txthistory").append(option)
            }
            else {
                $("#txthistory").empty();
            }

            var CountryId = $("[id$=_HiddenCountryId]").val();
            var manufactureId = $("[id$=_ManufacturerDropDown] option:selected").text();

            loadDataToManufacturerDropDown("_ManufacturerDropDown", CountryId, result);
            loadDataTomodelCodeDropDown("_modelCodeDropDown", CountryId, result, "");
            loadDataTo("_FuelTypeDropDown", result.Fuels);
            loadDataToServiceScheduleDropDown("_ServiceScheduleDropDown", result.ServiceSchedule);
            loadDataTo("_TransmissionDropDown", result.Transmission);
            $("[id$=_modelCodeDropDown]").attr("disabled", "disabled");

            var manufacturerEtc = result.ManufacturerEtc;
            if (manufacturerEtc[0].WarrantyExpDate != null)
                $("#ctl00_ContentPlaceHolder_WarrantyExpiryDateControl_dateTextBox").val(manufacturerEtc[0].WarrantyExpDate);
            else
                $("#ctl00_ContentPlaceHolder_WarrantyExpiryDateControl_dateTextBox").val("");

            if (manufacturerEtc[0].SelfContainExpDate != null)
                $("#ctl00_ContentPlaceHolder_SelfContainmentDateControl_dateTextBox").val(manufacturerEtc[0].SelfContainExpDate);
            else
                $("#ctl00_ContentPlaceHolder_SelfContainmentDateControl_dateTextBox").val("");

            if (manufacturerEtc[0].TelematicsDevice != null)
                $("#ctl00_ContentPlaceHolder_txtTelematicsDevice").val(manufacturerEtc[0].TelematicsDevice);
            else
                $("#ctl00_ContentPlaceHolder_txtTelematicsDevice").val("");

            if (manufacturerEtc[0].TelematicsTablet != null)
                $("#ctl00_ContentPlaceHolder_txtTelematicsTablet").val(manufacturerEtc[0].TelematicsTablet);
            else
                $("#ctl00_ContentPlaceHolder_txtTelematicsTablet").val("");

            //Added by Nimesh on 1stJuly 2015 to display TelematicsGUID
            if (manufacturerEtc[0].TelematicsTablet != null)
                $("#ctl00_ContentPlaceHolder_txtTelematicsGUID").val(manufacturerEtc[0].TelematicsGUID);
            else
                $("#ctl00_ContentPlaceHolder_txtTelematicsGUID").val("");
            //End Added by Nimesh on 1stJuly 2015 to display TelematicsGUID

            //Added by Nimesh on 16th July 2015 to display TelematicsTablet Installer and TelematicsTabletInstallDate
            if (manufacturerEtc[0].TelematicsTabletInstaller != null)
                $("#ctl00_ContentPlaceHolder_txtTelematicsTabletInstaller").val(manufacturerEtc[0].TelematicsTabletInstaller);
            else
                $("#ctl00_ContentPlaceHolder_txtTelematicsTabletInstaller").val("");

            if (manufacturerEtc[0].TelematicsTabletInstallDate != null)
                $("#ctl00_ContentPlaceHolder_TelematicsTabletDateDateControl_dateTextBox").val(manufacturerEtc[0].TelematicsTabletInstallDate);
            else
                $("#ctl00_ContentPlaceHolder_TelematicsTabletDateDateControl_dateTextBox").val("");

            //Added by Nimesh on 16th July 2015 to display TelematicsTablet Installer and TelematicsTabletInstallDate
            //rev:mia 14nov2014 start- added TelematicsInstallDate and TelematicsInstaller
            if (manufacturerEtc[0].TelematicsInstallDate != null)
                $("#ctl00_ContentPlaceHolder_DateControlTelematicsInstallDate_dateTextBox").val(manufacturerEtc[0].TelematicsInstallDate);
            else
                $("#ctl00_ContentPlaceHolder_DateControlTelematicsInstallDate_dateTextBox").val("");

            if (manufacturerEtc[0].TelematicsInstaller != null)
                $("#ctl00_ContentPlaceHolder_txttelematicsInstaller").val(manufacturerEtc[0].TelematicsInstaller);
            else
                $("#ctl00_ContentPlaceHolder_txttelematicsInstaller").val("");
            //rev:mia 14nov2014 end-------------------------------------------------------
            
            RequestAjaxCall(CONST_GETSINGLEUNINUMBER, { fleetassetid: assetid });
        }
        if (mode == 'GetDefaultCountry') {
            if (result == "AU") {
                $("[id$=_lblCountryId]").text(1)
            }
            else {
                $("[id$=_lblCountryId]").text(8)
            }
        }

        /*Added by Nimesh on 12th Aug 2015*/
        //if (mode == 'CheckIfVehiclesAreOnFleet') {
        //    var message = jQuery.parseJSON(result);
        //    if (message.trim().length > 0)
        //        $("#lblError").text(message + " click save again to continue");
        //    $("[id$=_HiddenHasVehOnFleetChecked]").val("true");
        //}
        /*End Added by Nimesh on 12th Aug 2015*/
        if (mode == 'GetSingleUnitNumber') {
            var fleetUnitNumbers = jQuery.parseJSON(result);



            $("[id$=_txbUnit]").val($.trim(unit));
            $("[id$=_ddlEditStartLocation]").val($.trim(startlocation));
            $("#ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateTextBox").val($.trim(deliverydate));
            $("#ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateTextBox").val($.trim(onfleetdate));
            $("#ctl00_ContentPlaceHolder_dateEditReceivedDate_dateTextBox").val($.trim(onreceivedate));
            $("[id$=_txbEditFleetAssetId]").val($.trim(assetid));

            $("[id$=_txbEditEngineNumber]").val(fleetUnitNumbers.FleetModelCode[0].IdEngineNumber);
            $("[id$=_txbChassisNumber]").val(fleetUnitNumbers.FleetModelCode[0].IdChassisNumber);
            $("[id$=_txbCompliancePlateYear]").val(fleetUnitNumbers.FleetModelCode[0].IdCompliancePlateYear);
            $("[id$=_txbCompliancePlateMonth]").val(fleetUnitNumbers.FleetModelCode[0].IdCompliancePlateMonth);
            $("[id$=_txbIgnitionKey]").val(fleetUnitNumbers.FleetModelCode[0].KeyNrIgnition);
            $("[id$=_txbEditRegistraionNumber]").val(fleetUnitNumbers.FleetModelCode[0].RegistrationNumber);
            $("#ctl00_ContentPlaceHolder_dateEditRegistrationDate_dateTextBox").val(fleetUnitNumbers.FleetModelCode[0].RegFirstDate);
            $("#ctl00_ContentPlaceHolder_dateCofDate_dateTextBox").val(fleetUnitNumbers.FleetModelCode[0].RCCofEndDate);
            $("[id$=_txbETag]").val(fleetUnitNumbers.FleetModelCode[0].ETag);
            $("[id$=_txbEPurb]").val(fleetUnitNumbers.FleetModelCode[0].EPurb);
            $("[id$=_RUCPaidToTextBox]").val(fleetUnitNumbers.FleetModelCode[0].RUCPaidToKm);

            //rev:mia 22Aug2014
            //$("#ctl00_ContentPlaceHolder_RUCRenewDateControl_dateTextBox").val(fleetUnitNumbers.FleetModelCode[0].RUCRenewDate);
            if (fleetUnitNumbers.FleetModelCode[0].ManufacturerId != null) {
                $("[id$=_ManufacturerDropDown]").val(fleetUnitNumbers.FleetModelCode[0].ManufacturerId);
                $("[id$=_modelCodeDropDown]").val(fleetUnitNumbers.FleetModelCode[0].ManufacturerModelId);
            }
            else {
                $("[id$=_ManufacturerDropDown]").val("-1");
                $("[id$=_modelCodeDropDown]").val("-1");
            }


            if (fleetUnitNumbers.FleetModelCode[0].MdTransmission != null)
                $("[id$=_TransmissionDropDown]").val(fleetUnitNumbers.FleetModelCode[0].MdTransmission);
            else
                $("[id$=_TransmissionDropDown]").val("-1");

            if (fleetUnitNumbers.FleetModelCode[0].NextServiceId != null)
                $("[id$=_ServiceScheduleDropDown]").val(fleetUnitNumbers.FleetModelCode[0].NextServiceId);
            else
                $("[id$=_ServiceScheduleDropDown]").val("-1");

            if (fleetUnitNumbers.FleetModelCode[0].MdFuelType != null)
                $("[id$=_FuelTypeDropDown]").val(fleetUnitNumbers.FleetModelCode[0].MdFuelType);
            else
                $("[id$=_FuelTypeDropDown]").val("-1");

            if (fleetUnitNumbers.FleetModelCode[0].ManufacturerModel != null)
                $("#ctl00_ContentPlaceHolder_modeldescription").val(fleetUnitNumbers.FleetModelCode[0].ManufacturerModel)
            else
                $("#ctl00_ContentPlaceHolder_modeldescription").val("")


            $("[id$=_lastCheckindateLabel]").val(fleetUnitNumbers.FleetModelCode[0].LastCheckInDatetime)
            $("[id$=_lastcheckinlocationlabel]").val(fleetUnitNumbers.FleetModelCode[0].LastCheckInLocation)
            $("[id$=_lastcheckinOdometerLabel]").val(fleetUnitNumbers.FleetModelCode[0].LastCheckInOdometer)
            $("[id$=_disposalDate]").val(fleetUnitNumbers.FleetModelCode[0].DisposalDate)
            $("#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox").val(fleetUnitNumbers.FleetModelCode[0].RegistrationExpiryDate);
            $("[id$=_txtcomments]").val(fleetUnitNumbers.FleetModelCode[0].FleetComments)


            LoadPopup("_pnlEditUnit");
        }

    });
}

function AddOptionItem(id, text) {
    return "<option value= '" + id + "'>" + text + "</option>"
}

function Reset() {
    $(".dataTableColor input[type='text']").val("");
    DisableCheckBox('NO')
}

function LoadPopup(panelname) {
    DisableCheckBox('YES')
    centerPopup(panelname);
}

function centerPopup(panelname) {
    windowWidth  = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight  = $("[id$=" + panelname + "]").height();
    popupWidth   = $("[id$=" + panelname + "]").width();

    $("[id$=" + panelname + "]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    }).show();
}

function DisableCheckBox(mode) {
    $("#resultrepeater_table a, #resultrepeater_table input").prop("disabled", (mode == 'YES' ? true : false));
    $("#rebrandingrepeater_table a,#rebrandingrepeater_table input").prop("disabled", (mode == 'YES' ? true : false));
}

function UnloadPopup() {
    clearShortMessage()
    Reset();
    $("[id$=_pnlEditUnit]").hide();
    $("[id$=_pnlEditBulk]").hide();
    $("[id$=_pnlEditBulkRebranding]").hide();

    $("input[id*=chkRebrandingSelectUni]").prop("checked", false).removeAttr('disabled').css('visibility', 'visible')
    $("input[id*=chkRebrandingSelectAll]").prop("checked", false)

    $("input[id*=chkSelectUni]").prop("checked", false)//.removeAttr('disabled').css('visibility', 'visible')
    $("input[id*=chkSelectAll]").prop("checked", false)


    $("[id$=_txtcomments],[id$=_lastCheckindateLabel],[id$=_lastcheckinlocationlabel],[id$=_lastcheckinOdometerLabel],[id$=_disposalDate],#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox").val("")
    $("label.error").hide();

    if (!isFullAccess) {
        $("[id*=_chkSelectUni]").attr("disabled", true)
        $("[id*=_chkSelectAll]").attr("disabled", true)
    }
}


//load data to ManufacturerDropDown
function loadDataToManufacturerDropDown(elementname, countryid,result) {
    var manufacturers = result.Manufacturers;
    var option = "";
    var manufacturer;
    var item = $("[id$=" + elementname + "]");
    manufacturers = $.grep(manufacturers, function (element, index) {
        return element.CountryId == countryid;
    });

    if ($(item).find("option").length - 1 == -1) {
        option = option + AddOptionItem("-1", CONST_PLEASE_SELECT)
        $.each(manufacturers, function (index, manufacturer) {
            option = option + AddOptionItem(manufacturer.ManufacturerId, manufacturer.Manufacturer)
        });
        $(item).append(option);
        $(item).val("-1")
    }
}

//load data to modelCodeDropDown
function loadDataTomodelCodeDropDown(elementname, countryid, result, manufacturername) {
    var manufactureId = $("[id$=_ManufacturerDropDown] option:selected").val();
    var manufacturers = result.ManufacturerModels;
    var option = "";
    var manufacturer;
    var item = $("[id$=" + elementname + "]");
    manufacturers = $.grep(manufacturers, function (element, index) {
        if(manufacturername == "")
            return element.CountryId == countryid //&& element.ManufacturerModelCode == $("[id$=_ddlFleetModelCode] option:selected").text();
        else
           return element.CountryId == countryid && element.Manufacturer == manufacturername
    });

    if (manufacturername != "") $(item).html("");
    
    if ($(item).find("option").length - 1 == -1 ) {
        
        $.each(manufacturers, function (index, manufacturer) {
            option = option + AddOptionItem(manufacturer.ManufacturerModelId, manufacturer.ManufacturerModelCode)
        });

        if (manufacturers.length -1 > 0) {
            option = AddOptionItem("-1", CONST_PLEASE_SELECT) + option
            $(item).append(option);
            $(item).val("-1")
        }
        else {
            $(item).append(option);
            $("#ctl00_ContentPlaceHolder_modeldescription").text(manufacturers[0].ManufacturerModel)
        }
        
    }
}


//load data to modeldescription
function loadDataTomodeldescriptionLabel(countryid, result, manufacturername, code) {
    var manufactureId = $("[id$=_ManufacturerDropDown] option:selected").val();
    var manufacturers = result.ManufacturerModels;
    
  
    manufacturers = $.grep(manufacturers, function (element, index) {
            return element.CountryId == countryid && element.Manufacturer == manufacturername && element.ManufacturerModelCode == code
    });

        if (manufacturers.length - 1 != -1) {
            $("#ctl00_ContentPlaceHolder_modeldescription").val(manufacturers[0].ManufacturerModel)
    }
}

//load data to ServiceScheduleDropDown
function loadDataToServiceScheduleDropDown(elementname, result) {
    var manufacturers = result;
    var option = "";
    var manufacturer;
    var item = $("[id$=" + elementname + "]");
    
    if ($(item).find("option").length - 1 == -1) {
        option = option + AddOptionItem("-1", CONST_PLEASE_SELECT)
        /*
        Following was changed by Nimesh on 11th may 15 to get serviceScheduleServiceID
        $.each(manufacturers, function (index, manufacturer) {
            option = option + AddOptionItem(manufacturer.serviceScheduleId, manufacturer.Name)
        });
        */
        $.each(manufacturers, function (index, manufacturer) {
            option = option + AddOptionItem(manufacturer.serviceScheduleServiceId, manufacturer.Name)
        });
        $(item).append(option);
        $(item).val("-1")
    }
}


//load data to FuelTypeDropDown,TransmissionDropDown,ServiceScheduleDropDown
function loadDataTo(elementname, result) {
    var manufacturers = result;
    var option = "";
    var manufacturer;
    var item = $("[id$=" + elementname + "]");

    if ($(item).find("option").length - 1 == -1) {
        option = option + AddOptionItem("-1", CONST_PLEASE_SELECT)
        $.each(manufacturers, function (index, manufacturer) {
            option = option + AddOptionItem(manufacturer.ReferenceCode, manufacturer.ReferenceDescription)
        });
        $(item).append(option);
        $(item).val("-1")
    }
}


function EventPropagation(e) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();
}


function CenterConfirmationPopup() {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1_confirmationPanel").height();
    popupWidth = $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1_confirmationPanel").width();

    /*
    $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1Behaviour_foregroundElement").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
   */
    $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1Behaviour_foregroundElement").css("left", Math.round(windowWidth / 2 - popupWidth / 2));
    $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1Behaviour_foregroundElement").css("top", Math.round(windowHeight / 2 - popupHeight / 2));
    $("#ctl00_ContentPlaceHolder_ConfirmationBoxControl1Behaviour_foregroundElement").css("position", "absolute");
}


function AIMSAccess(result) 
{
    
    
    var labelcolor = 'black';

    $('[id$=_rblNewVehicle] :radio').eq(1).removeAttr("disabled")


    if (result == 'CheckUserRoleForFullAccess') {
        $("#editvehicletable input, #editvehicletable textarea,#editvehicletable select").removeAttr("disabled")
        result = 'FullAccess';
        $("#ButtonTopUnitEdit").removeAttr('disabled').removeClass('submitdisabled').addClass('Button_Standard Button_Edit')
        $("#ButtonBottomUnitEdit").removeAttr('disabled').removeClass('submitdisabled').addClass('Button_Standard Button_Edit')
    }
    else if (result == 'CheckUserRoleForPartialAccess' || result == 'CheckUserRoleForViewAccess' || result == 'CheckUserRoleForNoaccess') 
    {
        if (result == 'CheckUserRoleForPartialAccess')
            result = 'PartialAccess';
        else if (result == 'CheckUserRoleForViewAccess')
            result = 'ViewAccess';
        else {
            result = 'NoAccess';
            $("[id*=_linkUnit]").attr("disabled", true).css('color', 'gray');
        }

        $("[id*=_chkSelectUni]").attr("disabled", true)
        $("[id*=_chkSelectAll]").attr("disabled", true)
        $('[id$=_rblNewVehicle] :radio').eq(1).attr("disabled", true)
        $("#ButtonTopUnitEdit").addClass("submitdisabled").attr("disabled", true)
        $("#ButtonBottomUnitEdit").addClass("submitdisabled").attr("disabled", true)

    }

    $("#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox").removeAttr('disabled').closest('td').prev('td').css("color", labelcolor)
    $("#ctl00_ContentPlaceHolder_dateCofDate_dateTextBox").removeAttr('disabled').closest('td').prev('td').css("color", labelcolor)
    $("#ctl00_ContentPlaceHolder_RUCPaidToTextBox").removeAttr('disabled').closest('td').prev('td').css("color", labelcolor)
    $("#ctl00_ContentPlaceHolder_txtcomments").removeAttr('disabled')
    $("#ctl00_ContentPlaceHolder_txtcomments").css("color", labelcolor)

    $('#txthistory').on('focus mousedown click mouseover', function (e) {
        if (e.stopPropagation) {
            e.stopPropagation();
            e.preventDefault();
        }
        else {
            e.cancelBubble = true;
            e.returnValue = false;
        }
        this.blur();
        window.focus();

    });

    if (result == 'PartialAccess' || result == 'ViewAccess') {
        $("#editvehicletable input, #editvehicletable textarea, #editvehicletable select").attr("readonly", "readonly")
        $("[id$=_RebrandingDropDown]").removeAttr("disabled").removeAttr("readonly")
        if (result == 'PartialAccess') {
            $("#ctl00_ContentPlaceHolder_RUCPaidToTextBox").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_dateCofDate_dateTextBox").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_txtcomments").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_ServiceScheduleDropDown").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_txbETag").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_txbEPurb").removeAttr("readonly")

            $("#ctl00_ContentPlaceHolder_txtTelematicsDevice").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_txtTelematicsTablet").removeAttr("readonly")

            //rev:mia 14nov2014 start- added TelematicsInstallDate and TelematicsInstaller
            $("#ctl00_ContentPlaceHolder_txttelematicsInstaller").removeAttr("readonly")
            $("#ctl00_ContentPlaceHolder_DateControlTelematicsInstallDate_dateTextBox").removeAttr("readonly")
            //rev:mia 14nov2014 end-------------------------------------------------------
        }

        if (result == 'ViewAccess') {
            $("#ctl00_ContentPlaceHolder_dateCofDate_dateImage").prop("onclick", null)
            $("#ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateImage").prop("onclick", null)
        }

        $("#ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateImage").prop("onclick", null)
        $("#ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateImage").prop("onclick", null)
        $("#ctl00_ContentPlaceHolder_dateEditRegistrationDate_dateImage").prop("onclick", null)

        //rev:mia 22Aug2014
        //$("#ctl00_ContentPlaceHolder_RUCRenewDateControl_dateImage").prop("onclick", null)
        $("#ctl00_ContentPlaceHolder_WarrantyExpiryDateControl_dateImage").prop("onclick", null)
        $("#ctl00_ContentPlaceHolder_SelfContainmentDateControl_dateTextBox").prop("onclick", null)

        $("#ctl00_ContentPlaceHolder_dateEditReceivedDate_dateImage").prop("onclick", null)


        $('#editvehicletable select, #editvehicletable input, #editvehicletable #txthistory').on('focus mousedown click mouseover', function (e) {
            var myid = $(this).attr("id");

            if (result == 'PartialAccess') {
                if (myid != 'ctl00_ContentPlaceHolder_txtcomments' &&
                       myid != 'ctl00_ContentPlaceHolder_RUCPaidToTextBox'
                       && myid != 'ctl00_ContentPlaceHolder_dateCofDate_dateTextBox'
                       && myid != 'ctl00_ContentPlaceHolder_RegExpiryDataDateControl_dateTextBox'
                       && myid != 'ctl00_ContentPlaceHolder_RebrandingDropDown'
                       && myid != 'ctl00_ContentPlaceHolder_ServiceScheduleDropDown'
                       && myid != 'ctl00_ContentPlaceHolder_txbETag'
                       && myid != 'ctl00_ContentPlaceHolder_txtcomments'
                       && myid != 'ctl00_ContentPlaceHolder_txtTelematicsDevice'
                       && myid != 'ctl00_ContentPlaceHolder_txtTelematicsTablet'
                //rev:mia 14nov2014 start- added TelematicsInstallDate and TelematicsInstaller
                       && myid != 'ctl00_ContentPlaceHolder_txttelematicsInstaller'
                       && myid != 'ctl00_ContentPlaceHolder_DateControlTelematicsInstallDate_dateTextBox'
                //rev:mia 14nov2014 end-------------------------------------------------------
                       ) {
                    if (e.stopPropagation) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else {
                        e.cancelBubble = true;
                        e.returnValue = false;
                    }
                    this.blur();
                    window.focus();
                }
            }
            else {
                if (myid != 'ctl00_ContentPlaceHolder_RebrandingDropDown') {
                    if (e.stopPropagation) {
                        e.stopPropagation();
                        e.preventDefault();
                    }
                    else {
                        e.cancelBubble = true;
                        e.returnValue = false;
                    }
                    this.blur();
                    window.focus();
                }
            }

        });

    }
    else {

        if (result == 'FullAccess') {
            //DO NOTHING
        }
    }

  
}