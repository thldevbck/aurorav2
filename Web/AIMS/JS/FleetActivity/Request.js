﻿(function ($, window, document, undefined) {
    jQuery(function ($) {
        $(".modalPopup").hide();

        function FleetActivityViewModel() {
            var self = this;

            //variables
            self.AllSearchCountries = GetCountryList();
            self.AllActivityType = ko.observableArray([]);
            self.AllLocation = ko.observableArray([]);

            self.ThisFleetRelatedInformation = new FleetRelatedInformation({});
            self.AllFleetActivityHistory = ko.observableArray([]);

            self.SelectedFleetActivityHistory = ko.observable();
            self.SearchCriteria = new SearchCriteriaDetail({});

            //operations
            self.ActivityHistorySearch = function (isNewSearch) {
                self.AllFleetActivityHistory.removeAll();

                self.SearchCriteria = new SearchCriteriaDetail({
                    Country: $("#selSearchCountry").val(),
                    UnitNumber: ($("#inputFromUnitNumber").val() === "") ? undefined : $("#inputFromUnitNumber").val(),
                    RegistrationNumber: ($("#inputRegistrationNumber").val() === "") ? undefined : $("#inputRegistrationNumber").val()
                });

                if (self.SearchCriteria.UnitNumber === undefined && self.SearchCriteria.RegistrationNumber === undefined) {
                    setInformationShortMessage(CONST_INVALID_SEARCH_CRITERIA);
                    return;
                }

                $.ajax({
                    type: "POST",
                    url: FLEET_ACTIVITY_CONST_URL + "GetFleetActivityHistory",
                    data: {
                        iCountryId: $("#selSearchCountry").val(),
                        iUnitNumber: (self.SearchCriteria.UnitNumber === "") ? undefined : self.SearchCriteria.UnitNumber,
                        sRegoNum: (self.SearchCriteria.RegistrationNumber === "") ? undefined : self.SearchCriteria.RegistrationNumber
                    },
                    datatype: "json",
                    success: function (returnData) {
                        if (returnData.toLowerCase().indexOf("error") != -1) {
                            setInformationShortMessage(CONST_UNIT_WAS_NOT_FOUND); // setInformationShortMessage(returnData);
                            return;
                        }

                        var _thisFleetRelatedInformation = jQuery.parseJSON(returnData).ThisFleetRelatedInformation[0];

                        if (_thisFleetRelatedInformation !== undefined) {
                            self.ThisFleetRelatedInformation.ModelCode(_thisFleetRelatedInformation.ModelCode);
                            self.ThisFleetRelatedInformation.ModelDescription(_thisFleetRelatedInformation.ModelDescription);
                            self.ThisFleetRelatedInformation.Manufacturer(_thisFleetRelatedInformation.Manufacturer);
                            self.ThisFleetRelatedInformation.manufacturerModelCode(_thisFleetRelatedInformation.manufacturerModelCode);
                            self.ThisFleetRelatedInformation.ManufacturerModel(_thisFleetRelatedInformation.ManufacturerModel);

                            self.AllFleetActivityHistory($.map(jQuery.parseJSON(returnData).AllFleetActivityHistory, function (item) {
                                return new FleetActivityHistory(item)
                            }));

                            //Pivotal Tracker 75487692: Aims Derisking - Unit and rego search message display and field auto populate conditions
                            if ($("#inputFromUnitNumber").val() == "") { //527056
                                if (self.AllFleetActivityHistory().length > 0) {
                                    $("#inputFromUnitNumber").val(self.AllFleetActivityHistory()[0].UnitNumber());
                                }
                            }

                            if ($("#inputRegistrationNumber").val() == "") { //AJU661
                                if (self.AllFleetActivityHistory().length > 0) {
                                    $("#inputRegistrationNumber").val(self.AllFleetActivityHistory()[0].RegistrationNumber());
                                }
                            }
                            if (isViewAccess == true || isPartialAccess == true) {
                                $("input[type='radio']").hide()
                            }

                            //change result style
                            $('.divResult tbody tr:odd').addClass("oddBG");
                            $('.divResult tbody tr:even').addClass("evenBG");

                            
                            if (self.AllFleetActivityHistory().length > 0) {
                                setInformationShortMessage(CONST_MODEL_WAS_FOUND);
                            } else {
                                setInformationShortMessage(CONST_UNIT_WAS_NOT_FOUND);
                            }
                        } else {
                            setInformationShortMessage(CONST_UNIT_WAS_NOT_FOUND);
                        }
                    },
                    async: false
                });

                //load drop down list

                $.ajax({
                    type: "POST",
                    url: FLEET_ACTIVITY_CONST_URL + "GetDropDownForFleetActivity",
                    data: { iCountryId: $("#selSearchCountry").val() },
                    success: function (data) {
                        if (data.ActivityTypeList !== undefined) {
                            self.AllActivityType($.map(data.ActivityTypeList, function (item) {
                                return { ActivityTypeValue: item.ActivityType, ActivityTypeText: item.ActivityType }
                            }));
                        }

                        if (data.LocationList !== undefined) {
                            self.AllLocation($.map(data.LocationList, function (item) {
                                return { LocationValue: item.LocationId, LocationText: item.LocationCode + " - " + item.LocationName }
                            }));
                        }
                    },
                    dataType: "json",
                    async: false
                });
            };

            //click: $root.ExternalRef_Click
            self.ExternalRef_Click = function () {
                var that = this;

                if (that.ActivityType() == "Rental") {
                    //console.log(that.RentalRef());
                    //http://cor-app-002/Aurora/Web/Booking/Booking.aspx?hdRentalId=B6955166-3
                    window.open("../Booking/Booking.aspx?hdRentalId=" + that.RentalRef().replace('/', '-'), "", "height=650,width=810,resizable=yes,scrollbars=yes");
                }
            };

            self.FleetActivityEdit = function () {
                var that = this;

                if ($("input[name='fleetActivity']:checked").length == 0) {
                    setInformationShortMessage(CONST_INVALID_SELECT_AN_ACTIVITY);
                    window.scrollTo(0, 0);
                    return;
                }

                $("#editFleetAtivityPanel #popupTitle").html(CONST_POPUP_TITLE_TEXT_EDIT); //editFleetAtivityPanel

                //load item info
                ClearEditFleetAtivityPanel();

                var item;
                var isComplete = true;
                var thisActivityType = "";

                for (var itemIndex in self.AllFleetActivityHistory()) {
                    item = self.AllFleetActivityHistory()[itemIndex];

                    if (self.SelectedFleetActivityHistory() == item.ActivityId()) {
                        thisActivityType = item.ActivityType();

                        $("#editFleetAtivityPanel_ActivityType").val(thisActivityType);

                        $("#editFleetAtivityPanel_ExternalRef").val(item.ExternalRef());

                        //PivotalTracker 70988130: Aims - Activity Not Completed - Edit Screen Model Code and Description Changes
                        $("#editFleetAtivityPanel_ModelCode").val(item.ModelCode());
                        $("#editFleetAtivityPanel_ModelDescription").val(item.ModelDescription());

                        isComplete = (item.Completed().toLowerCase() == "yes") ? true : false;

                        $("#editFleetAtivityPanel_Completed").prop("checked", isComplete);

                        $("input[id*='editFleetAtivityPanel_StartDate']").val(item.StartDate());
                        $("input[id*='editFleetAtivityPanel_StartTime']").val(item.StartTime());
                        $("#editFleetAtivityPanel_StartBranch>option:contains(" + item.StartLocation() + ")").prop('selected', true);
                        $("#editFleetAtivityPanel_StartOddometer").val(item.StartOdometer());
                        $("input[id*='editFleetAtivityPanel_EndDate']").val(item.EndDate());
                        $("input[id*='editFleetAtivityPanel_EndTime']").val(item.EndTime());
                        $("#editFleetAtivityPanel_EndBranch>option:contains(" + item.EndLocation() + ")").prop('selected', true);
                        $("#editFleetAtivityPanel_EndOddometer").val(item.EndOdometer());

                        $("#editFleetAtivityPanel_Desciption").val(item.ActivityDescription());

                    }
                }
                

                //apply display logic
                FleetActivityEditDisplayLogic(isComplete, thisActivityType);

                $("#editFleetAtivityPanel").show();
                ShowPopBox();
            };

            self.FleetActivityAdd = function (item) {
                //Only available when user did a valid search
                if (self.ThisFleetRelatedInformation.ModelCode() !== undefined) {
                    $("#editFleetAtivityPanel #popupTitle").html(CONST_POPUP_TITLE_TEXT_ADD); //editFleetAtivityPanel

                    ClearEditFleetAtivityPanel();

                    //Pivotal Tracker 75170554: Aims Derisking - When adding activity carry odometer over to the start and end boxes
                    //find out the highest end odometer of completed activities.
                    var highestOdometer = 0;

                    for (var itemIndex in self.AllFleetActivityHistory()) {
                        if (highestOdometer < self.AllFleetActivityHistory()[itemIndex].EndOdometer() &&
                        self.AllFleetActivityHistory()[itemIndex].Completed().toLowerCase() == "yes") {
                            highestOdometer = self.AllFleetActivityHistory()[itemIndex].EndOdometer();
                        }
                    }

                    //assign the highest odometer to start and end odometer fields.
                    $("#editFleetAtivityPanel_StartOddometer").val(highestOdometer);
                    $("#editFleetAtivityPanel_EndOddometer").val(highestOdometer);

                    FleetActivityAddDisplayLogic();

                    $("#editFleetAtivityPanel").show();
                    ShowPopBox();
                } else {
                    setInformationShortMessage(CONST_INVALID_FLEET_ACTIVITY);
                }
            };

            self.FleetActivityEdit_OK = function (data, event) {
                var validCreate = true;

                if ($("#editFleetAtivityPanel_ActivityType").val() == "") {
                    $("#editFleetAtivityPanel_ActivityType").addClass("ErrorV2");
                    //$("#editFleetAtivityPanel_ActivityType_validation").show();
                    validCreate = false;
                } else {
                    $("#editFleetAtivityPanel_ActivityType").removeClass("ErrorV2");
                    //$("#editFleetAtivityPanel_ActivityType_validation").hide();
                }

                //                if ($("#editFleetAtivityPanel_ExternalRef").val() == "") {
                //                    $("#editFleetAtivityPanel_ExternalRef_validation").show();
                //                    validCreate = false;
                //                } else {
                //                    $("#editFleetAtivityPanel_ExternalRef_validation").hide();
                //                }

                if ($("[id*=StartDate_dateTextBox]").val() == "" ||
                    $("[id*=StartTime_timeTextBox]").val() == "") {
                    $("[id*=StartDate_dateTextBox]").parent().nextAll("#val_StartDate").show();
                    validCreate = false;
                } else {
                    $("[id*=StartDate_dateTextBox]").parent().nextAll("#val_StartDate").hide();
                }

                if ($("[id*='StartTime_timeTextBox']").css('textDecoration').indexOf("underline") > -1 &&
                    $(".Warning").html() !== undefined &&
                    $(".Warning").html().indexOf("Enter time in hh:mm format") > -1) {
                    $("[id*=StartDate_dateTextBox]").parent().nextAll("#val_StartTime").html($(".Warning").html()).show();
                    validCreate = false;
                } else {
                    $("[id*=StartDate_dateTextBox]").parent().nextAll("#val_StartTime").hide();
                }

                if ($("[id*=EndDate_dateTextBox]").val() == "" ||
                    $("[id*=EndTime_timeTextBox]").val() == "") {
                    $("[id*=EndDate_dateTextBox]").parent().nextAll("#val_EndDate").show();
                    validCreate = false;
                } else {
                    $("[id*=EndDate_dateTextBox]").parent().nextAll("#val_EndDate").hide();
                }

                if ($("[id*='EndTime_timeTextBox']").css('textDecoration').indexOf("underline") > -1 &&
                    $(".Warning").html() !== undefined &&
                    $(".Warning").html().indexOf("Enter time in hh:mm format") > -1) {
                    $("[id*=EndDate_dateTextBox]").parent().nextAll("#val_EndTime").html($(".Warning").html()).show();
                    validCreate = false;
                } else {
                    $("[id*=EndDate_dateTextBox]").parent().nextAll("#val_EndTime").hide();
                }

                //Pivotal Tracker 75170946: Aims Derisking - Message Popups (please refer to screenshots on how to display messages)
                //End date can not be less than start date.
                try {
                    var startDateArray = $("[id*=StartDate_dateTextBox]").val().split('/');
                    var endDateArray = $("[id*=EndDate_dateTextBox]").val().split('/');

                    var startDateObject = new Date(startDateArray[2], startDateArray[1] - 1, startDateArray[0]); // January = 0
                    var endDateObject = new Date(endDateArray[2], endDateArray[1] - 1, endDateArray[0]); // January = 0


                    var startTime = parseInt($("[id*=StartTime_timeTextBox]").val().split(':')[0]) + parseInt($("[id*=StartTime_timeTextBox]").val().split(':')[1]) / 60;
                    var endTime = parseInt($("[id*=EndTime_timeTextBox]").val().split(':')[0]) + parseInt($("[id*=EndTime_timeTextBox]").val().split(':')[1]) / 60;

                    if (endDateObject < startDateObject ||
                    (endDateObject.valueOf() == startDateObject.valueOf() && endTime < startTime)
                    ) {
                        validCreate = false;
                        alert(CONST_END_DATE_NOT_LESS_THAN_START_DATE);
                    }
                } catch (e) {

                }

                if ($("#editFleetAtivityPanel_StartBranch").val() == "") {
                    $("#editFleetAtivityPanel_StartBranch").next("label").show();
                    validCreate = false;
                } else {
                    $("#editFleetAtivityPanel_StartBranch").next("label").hide();
                }

                if ($("#editFleetAtivityPanel_EndBranch").val() == "") {
                    $("#editFleetAtivityPanel_EndBranch").next("label").show();
                    validCreate = false;
                } else {
                    $("#editFleetAtivityPanel_EndBranch").next("label").hide();
                }

                if ($("#editFleetAtivityPanel_StartOddometer").val() == "") {
                    $("#editFleetAtivityPanel_StartOddometer").next("label").show();
                    validCreate = false;
                } else {
                    $("#editFleetAtivityPanel_StartOddometer").next("label").hide();
                }

                if ($("#editFleetAtivityPanel_EndOddometer").val() == "") {
                    $("#editFleetAtivityPanel_EndOddometer").next("label").show();
                    validCreate = false;
                } else {
                    $("#editFleetAtivityPanel_EndOddometer").next("label").hide();

                    //odo end should be larger than or equal to odo start
                    if (parseInt($("#editFleetAtivityPanel_EndOddometer").val()) < parseInt($("#editFleetAtivityPanel_StartOddometer").val())) {
                        alert("End odometer should be larger than start odometer.");
                        validCreate = false;
                    }
                }

                if (!$(event.target).hasClass("buttondisabled") && validCreate) {
                    if ($("#popupTitle").html() == CONST_POPUP_TITLE_TEXT_ADD) {
                        CreateNewActivity(self, false);
                    } else if ($("#popupTitle").html() == CONST_POPUP_TITLE_TEXT_EDIT) {
                        UpdateExistingActivity(self, false);
                    }

                    $("#editFleetAtivityPanel").hide();
                    HidePopBox();
                } else {
                    //return false;
                }
            }

            self.FleetActivityEdit_Cancel = function () {
                $("#editFleetAtivityPanel").hide();
                HidePopBox();
            }

            return self;
        } //[END] MVVM

        var FleetActivityVM = new FleetActivityViewModel();
        ko.applyBindings(FleetActivityVM);

        //load search criteria
        //get user current country
        $.ajax({
            type: "POST",
            url: FLEETMODELMGT_CONST_URL + "GetDefaultCountry",
            data: { UserCode: $("[id$=_ManufacturerIDhidden]").val() },
            success: function (data) {
                //use current user country
                $("#selSearchCountry").val($("#selSearchCountry option:contains('" + data + "')").val());
            },
            async: false
        });

        $("#inputFromUnitNumber").val(getParameterByName("inputFromUnitNumber"));
        $("#inputRegistrationNumber").val(getParameterByName("inputRegistrationNumber"));

        if (getParameterByName("doSearch") == 1) {
            FleetActivityVM.ActivityHistorySearch(true);
        }
    });
} (jQuery, this, this.document));