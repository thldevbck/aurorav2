﻿function FleetRelatedInformation(data) {
    var self = this;

    self.ModelCode = ko.observable(data.ModelCode);
    self.ModelDescription = ko.observable(data.ModelDescription);
    self.Manufacturer = ko.observable(data.Manufacturer);
    self.manufacturerModelCode = ko.observable(data.manufacturerModelCode);
    self.ManufacturerModel = ko.observable(data.ManufacturerModel);
}

function FleetActivityHistory(data) {
    var self = this;

    //self.SelectedItem = ko.observable(false);
    self.ActivityId = ko.observable(data.ActivityId);
    self.FleetAssetId = ko.observable(data.FleetAssetId);
    self.UnitNumber = ko.observable(data.UnitNumber);
    self.RegistrationNumber = ko.observable(data.RegistrationNumber);
    self.ActivityType = ko.observable(data.ActivityType);
    self.ExternalRef = ko.observable(data.ExternalRef);
    self.StartOdometer = ko.observable(data.StartOdometer);
    self.EndOdometer = ko.observable(data.EndOdometer);
    self.CompanyId = ko.observable(data.CompanyId);
    self.StartDate = ko.observable(FormatDate(data.StartDate));
    self.StartTime = ko.observable(data.StartTime);
    self.EndDate = ko.observable(FormatDate(data.EndDate));
    self.EndTime = ko.observable(data.EndTime);
    self.Completed = ko.observable(data.Completed);
    self.StartLocation = ko.observable(data.StartLocation);
    self.EndLocation = ko.observable(data.EndLocation);
    self.ModelCode = ko.observable(data.ModelCode);
    self.ModelDescription = ko.observable(data.ModelDescription);
    self.ModifiedDate = ko.observable(FormatDate(data.ModifiedDate));
    self.ModifiedBy = ko.observable(data.ModifiedBy);
    self.RentalRef = ko.observable(data.RentalRef);
    self.ActivityDescription = ko.observable(data.ActivityDescription);
}

function SearchCriteriaDetail(data) {
    var self = this;

    self.Country = data.Country;
    self.UnitNumber = data.UnitNumber;
    self.RegistrationNumber = data.RegistrationNumber;
}