﻿var FLEET_ACTIVITY_CONST_URL = "AIMSListener/FleetActivityListener.aspx?MODE=";
var FLEET_SALE_CONST_URL = "AIMSListener/FleetSaleDisposalPlanListener.aspx?MODE=";
var FLEETMODELMGT_CONST_URL = "AIMSListener/AIMSListener.aspx?MODE=";
var CONST_MODEL_WAS_FOUND = 'AIMS Information was loaded.';
var CONST_UNIT_WAS_NOT_FOUND = "The unit and/or registration doesn't exist in the database!";
var CONST_INVALID_SEARCH_CRITERIA = "Please input a valid unit number or registration number.";
var CONST_INVALID_SELECT_AN_ACTIVITY = "Please select a fleet activity.";
var CONST_INVALID_FLEET_ACTIVITY = "Please search a valid fleet activity before adding a activity history.";
var CONST_END_DATE_NOT_LESS_THAN_START_DATE = "End Date can not be less than Start Date!";
var CONST_ADD_NEW_ACTIVITY = "New Activity Added Successfully!";
var CONST_POPUP_TITLE_TEXT_ADD = "Add";
var CONST_POPUP_TITLE_TEXT_EDIT = "Edit";
var validator;

function GetCountryList() {
    var countryList = new Array();

    $.ajax({
        type: "POST",
        url: FLEET_SALE_CONST_URL + "GetDropdownForSalePlan",
        data: "",
        success: function (data) {
            if (data.CountryList !== undefined) {
                //countryList.push({ countryId: -1, displayText: "--please select--" });

                $.each(data.CountryList, function (index, country) {
                    countryList.push({ countryId: country.CountryId, displayText: country.CountryCode + " - " + country.CountryDescription });
                });
            }

        },
        dataType: "json",
        async: false
    });

    return countryList;
}

function ClearEditFleetAtivityPanel() {
    LinkEndBlockWithStartBlock(false);

    $("#editFleetAtivityPanel_ActivityType").prop("disabled", false);
    $("#editFleetAtivityPanel_ActivityType option:contains('please')").attr('selected', true);

    $("#editFleetAtivityPanel_ExternalRef").val("").prop("disabled", false);

    $("#editFleetAtivityPanel_Completed").attr('checked', false).prop("disabled", false);

    $("#editFleetAtivityPanel_ModelCode").val("");
    $("#editFleetAtivityPanel_ModelDescription").val("");

    $("#editFleetAtivityPanel input[id*='Start']").val("").prop("disabled", false);
    $("#editFleetAtivityPanel select[id*='Start']").val("").prop("disabled", false);

    $("#editFleetAtivityPanel input[id*='End']").val("").prop("disabled", false);
    $("#editFleetAtivityPanel select[id*='End']").val("").prop("disabled", false);

    $("#editFleetAtivityPanel_Desciption").val("").prop("disabled", false);

    $(".bulkEditVehicleSalePanelButtons .Button_OK").removeClass("buttondisabled");
}

function FleetActivityAddDisplayLogic() {
    //Pivotal Tracker 70995356: Aims - When adding incident activity, external ref should not be mandatory and greyed out
    //Pivotal Tracker 75404022: Aims - When adding an activity, completed Checkbox default conditions
    $("#editFleetAtivityPanel_ActivityType").change(function () {
        if (this.value == "Incident") {
            $("#editFleetAtivityPanel_ExternalRef").prop("disabled", "disabled");
            $("#editFleetAtivityPanel_Completed").prop('checked', true).prop("disabled", "disabled");

            //Pivotal Tracker 75169768: Aims Derisking - Add Incident Activity
            $("#editFleetAtivityPanel input[id*='End']").prop("disabled", "disabled");
            $("#editFleetAtivityPanel select[id*='End']").prop("disabled", "disabled");

            //pass start block value to end blcok
            LinkEndBlockWithStartBlock(true);
        } else {
            $("#editFleetAtivityPanel_ExternalRef").prop("disabled", false);
            $("#editFleetAtivityPanel_Completed").prop('checked', false).prop("disabled", false);

            //Pivotal Tracker 75169768: Aims Derisking - Add Incident Activity
            $("#editFleetAtivityPanel input[id*='End']").prop("disabled", false);
            $("#editFleetAtivityPanel select[id*='End']").prop("disabled", false);

            LinkEndBlockWithStartBlock(false);
        }

    });
}

//TODO: should split this into domain layer
function FleetActivityEditDisplayLogic(isComplete, thisActivityType) {
    $("#editFleetAtivityPanel_ActivityType").prop("disabled", "disabled"); //disable activity type

    //Pivotal Tracker 70995356: When Editing any activity type, 
    //regardless if activity is completed or not the external reference is always greyed out.
    $("#editFleetAtivityPanel_ExternalRef").prop("disabled", "disabled");

    if (isComplete) {
        $(".bulkEditVehicleSalePanelButtons .Button_OK").addClass("buttondisabled");
    } else {
        //disable start block
        $("#editFleetAtivityPanel input[id*='Start']").prop("disabled", "disabled");
        $("#editFleetAtivityPanel select[id*='Start']").prop("disabled", "disabled");
    }

    if (thisActivityType == "Incident") {
        $(".bulkEditVehicleSalePanelButtons .Button_OK").removeClass("buttondisabled"); //TODO: seriously, this logic need to be tidy up.

        //disable end block
        $("#editFleetAtivityPanel input[id*='End']").prop("disabled", "disabled");
        $("#editFleetAtivityPanel select[id*='End']").prop("disabled", "disabled");

        //pass start block value to end blcok
        LinkEndBlockWithStartBlock(true);

        //set completed flag to true
        $("#editFleetAtivityPanel_Completed").prop("disabled", true).prop("checked", true);
    }

    //PivotalTracker 75169848: Aims Derisking - Edit Completed Rental Activity, start odometer should be editable
    if (thisActivityType == "Rental") {
        if (isComplete) {
            $(".bulkEditVehicleSalePanelButtons .Button_OK").removeClass("buttondisabled");

            $("#editFleetAtivityPanel_Completed").prop("disabled", "disabled");

            //disable start block
            $("#editFleetAtivityPanel input[id*='Start']").prop("disabled", "disabled");
            $("#editFleetAtivityPanel select[id*='Start']").prop("disabled", "disabled");

            //disable end block
            $("#editFleetAtivityPanel input[id*='End']").prop("disabled", "disabled");
            $("#editFleetAtivityPanel select[id*='End']").prop("disabled", "disabled");
        }

        //Pivotal Tracker 75169916: Aims Derisking - Edit Incomplete Rental Activity
        //Pivotal Tracker 75169848: Aims Derisking - Edit Completed Rental Activity
        $("#editFleetAtivityPanel_StartOddometer").prop("disabled", false);
        
    }

    //PivotalTracker 75170272: Aims Derisking - Edit Incomplete Stock Adjustment Activity
    //PivotalTracker 75403140: Aims Derisking - Edit Completed  Stock Adjustment Activity field control.
    if (thisActivityType == "StockAdjustment") {
        $(".bulkEditVehicleSalePanelButtons .Button_OK").removeClass("buttondisabled");

        if (isComplete) {
            $("#editFleetAtivityPanel_Completed").prop("disabled", "disabled");

            $("#editFleetAtivityPanel input[id*='Start']").prop("disabled", "disabled");
            $("#editFleetAtivityPanel select[id*='Start']").prop("disabled", "disabled");

            $("#editFleetAtivityPanel input[id*='End']").prop("disabled", "disabled");
            $("#editFleetAtivityPanel select[id*='End']").prop("disabled", "disabled");
        }

        $("#editFleetAtivityPanel_StartOddometer").prop("disabled", false);

    }

}

function LinkEndBlockWithStartBlock(isLink) {
    if (isLink) {
        $("input[id*='editFleetAtivityPanel_StartDate']").focusout(function () {
            var that = this;
            $("input[id*='editFleetAtivityPanel_EndDate']").val($(that).val());
        });

        $("input[id*='editFleetAtivityPanel_StartTime']").focusout(function () {
            var that = this;
            $("input[id*='editFleetAtivityPanel_EndTime']").val($(that).val());
        });

        $("#editFleetAtivityPanel_StartBranch").change(function () {
            var that = this;
            $("#editFleetAtivityPanel_EndBranch").val($(that).val());
        });

        $("#editFleetAtivityPanel_StartOddometer").change(function () {
            var that = this;
            $("#editFleetAtivityPanel_EndOddometer").val($(that).val());
        });
    } else {
        $("input[id*='editFleetAtivityPanel_StartDate']").off("focusout");
        $("input[id*='editFleetAtivityPanel_StartTime']").off("focusout");

        $("#editFleetAtivityPanel_StartBranch").off("change");
        $("#editFleetAtivityPanel_StartOddometer").off("change");
    }
}

function CreateNewActivity(self, _bForce) {
    $.ajax({
        type: "POST",
        url: FLEET_ACTIVITY_CONST_URL + "CreateNewActivity",
        data: { iFleetAssetId: self.AllFleetActivityHistory()[0].FleetAssetId(),
            sActivityType: $("#editFleetAtivityPanel_ActivityType").val(),
            sExternalRef: $("#editFleetAtivityPanel_ExternalRef").val(),
            dStartDate: $("input[id*=editFleetAtivityPanel_StartDate]").val() + " " + $("input[id*=editFleetAtivityPanel_StartTime]").val(),
            dEndDate: $("input[id*=editFleetAtivityPanel_EndDate]").val() + " " + $("input[id*=editFleetAtivityPanel_EndTime]").val(),
            iStartLocation: ($("#editFleetAtivityPanel_StartBranch").val() === "") ? undefined : $("#editFleetAtivityPanel_StartBranch").val(),
            iEndLocation: ($("#editFleetAtivityPanel_EndBranch").val() === "") ? undefined : $("#editFleetAtivityPanel_EndBranch").val(),
            iStartOddometer: $("#editFleetAtivityPanel_StartOddometer").val(),
            iEndOddometer: $("#editFleetAtivityPanel_EndOddometer").val(),
            bCompleted: $("#editFleetAtivityPanel_Completed").prop("checked"),
            sModifiedBy: $.trim($("[id$=_ManufacturerIDhidden]").val()),
            sdescription: $("#editFleetAtivityPanel_Desciption").val(),
            bForce: _bForce
        },
        success: function (data) {
            if (data[0] == "") {
                self.ActivityHistorySearch(false);
                setInformationShortMessage(CONST_ADD_NEW_ACTIVITY);
            } else if (data[0].indexOf("Confirm") > -1) {
                if (confirm(data[0])) {
                    CreateNewActivity(self, true);
                }
            } else if (data[0].indexOf("Error") > -1) {
                setErrorShortMessage(data[0]);
                window.scrollTo(0, 0);
            }
        },
        dataType: "json", async: false
    });
}

function UpdateExistingActivity(self, _bForce) {
    $.ajax({
        type: "POST",
        url: FLEET_ACTIVITY_CONST_URL + "UpdateActivity",
        data: { iFleetActivityId: self.SelectedFleetActivityHistory(),
            iFleetAssetId: self.AllFleetActivityHistory()[0].FleetAssetId(),
            sActivityType: $("#editFleetAtivityPanel_ActivityType").val(),
            sExternalRef: $("#editFleetAtivityPanel_ExternalRef").val(),
            dStartDate: $("input[id*=editFleetAtivityPanel_StartDate]").val() + " " + $("input[id*=editFleetAtivityPanel_StartTime]").val(),
            dEndDate: $("input[id*=editFleetAtivityPanel_EndDate]").val() + " " + $("input[id*=editFleetAtivityPanel_EndTime]").val(),
            iStartLocation: ($("#editFleetAtivityPanel_StartBranch").val() === "") ? undefined : $("#editFleetAtivityPanel_StartBranch").val(),
            iEndLocation: ($("#editFleetAtivityPanel_EndBranch").val() === "") ? undefined : $("#editFleetAtivityPanel_EndBranch").val(),
            iStartOddometer: $("#editFleetAtivityPanel_StartOddometer").val(),
            iEndOddometer: $("#editFleetAtivityPanel_EndOddometer").val(),
            bCompleted: $("#editFleetAtivityPanel_Completed").prop("checked"),
            sModifiedBy: $.trim($("[id$=_ManufacturerIDhidden]").val()),
            sdescription: $("#editFleetAtivityPanel_Desciption").val(),
            bForce: _bForce
        },
        success: function (data) {
            if (data[0] == "") {
                self.ActivityHistorySearch(false);
            } else if (data[0].toLowerCase().indexOf("confirm") > -1) {
                if (confirm(data[0])) {
                    UpdateExistingActivity(self, true);
                }
            } else if (data[0].toLowerCase().indexOf("error") > -1) {
                setErrorShortMessage(data[0]);
                window.scrollTo(0, 0);
            }
        },
        dataType: "json", async: false
    });
}