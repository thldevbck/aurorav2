﻿var CONST_URL_GENERICACCCESS = "AIMSListener/AIMSListener.aspx?MODE="


$(document).ready(function () {
    if ($("[id$=_ManufacturerIDhidden]").val() != null) {
        RequestAjaxAccessCall('CheckUserRoleForFullAccess', { usercode: $("[id$=_ManufacturerIDhidden]").val(), role: 'AIMSFULL' });
        RequestAjaxAccessCall('CheckUserRole', { usercode: $("[id$=_ManufacturerIDhidden]").val(), role: 'OLMAN' });
    }
});

function RequestAjaxAccessCall(mode, parameter) {
    var ListenerPath = $("[id$=_AIMSListenerPath]").val()
    if (ListenerPath != "") {
        ListenerPath = ListenerPath + CONST_URL_GENERICACCCESS;
    }
    else {
        ListenerPath = CONST_URL_GENERICACCCESS;
    }

    $.post(ListenerPath + mode, parameter, function (result) {
        if (mode == 'CheckUserRoleForFullAccess') {
            if (result == 'True') {
                isFullAccess = true;
                isPartialAccess = false;
                isViewAccess = false;
                AIMSAccess('CheckUserRoleForFullAccess');
            }
            else {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = false;
                RequestAjaxAccessCall('CheckUserRoleForPartialAccess', { usercode: $("[id$=_ManufacturerIDhidden]").val(), role: 'AIMSPARTIAL' });
            }
        }

        if (mode == 'CheckUserRoleForPartialAccess') {
            if (result == 'True') {
                isFullAccess = false;
                isPartialAccess = true;
                isViewAccess = false;
                AIMSAccess('CheckUserRoleForPartialAccess');
            }
            else {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = false;
                RequestAjaxAccessCall('CheckUserRoleForViewAccess', { usercode: $("[id$=_ManufacturerIDhidden]").val(), role: 'AIMSVIEWONLY' });
            }
        }

        if (mode == 'CheckUserRoleForViewAccess') {
            if (result == 'True') {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = true;
                AIMSAccess('CheckUserRoleForViewAccess');
            }
            else {
                isFullAccess = false;
                isPartialAccess = false;
                isViewAccess = false;
                AIMSAccess('CheckUserRoleForNoaccess');
            }
        }
        if (mode == 'CheckUserRole') {
            if (result == 'True') {
                isFleetManager = true;
            }
            else {
                isFleetManager = false;
            }
        }


    });
}


function AIMSAccess(result) 
{
    

    if (result == 'CheckUserRoleForFullAccess') {
        result = 'FullAccess';
    }
    else if (result == 'CheckUserRoleForPartialAccess' || result == 'CheckUserRoleForViewAccess' || result == 'CheckUserRoleForNoaccess') {
        if (result == 'CheckUserRoleForPartialAccess')
            result = 'PartialAccess';
        else if (result == 'CheckUserRoleForViewAccess')
            result = 'ViewAccess';
        else {
            result = 'NoAccess';

        }
    }
    

    if (result == 'PartialAccess' || result == 'ViewAccess') 
    {
        var hiddenPageIdentity = $("[id$=_hiddenPageIdentity]").val()

        if (hiddenPageIdentity == "FleetSaleDisposalPlan") {
            $("#VehicleSaleRecordsBulkEdit").addClass("buttondisabled disabled").attr("disabled", "disabled");
            $("#buttonCreateSalePlan").addClass("buttondisabled disabled").attr("disabled", "disabled");
            $("#buttonbulkEditCurrentPlan_Edit").addClass("buttondisabled disabled").attr("disabled", "disabled");
            $("#buttonbulkEditCurrentPlan_Delete").addClass("buttondisabled disabled").attr("disabled", "disabled");

            //$("#VehicleSaleRecords input[type='checkbox']").attr("disabled", "disabled")

            $("#butSingleEditCurrentPlan_OK").addClass("buttondisabled disabled").attr("disabled", "disabled"); //Release
            $("#buttonsingleEditCurrentPlan_Reset").addClass("buttondisabled disabled").attr("disabled", "disabled");

            $("#singleEditCurrentPlan_comments").attr("disabled", "disabled");

            if (result == 'PartialAccess') {
                $("#singleEditCurrentPlan_currentLocation").attr("disabled", "disabled");
                $("#tableSaleInformation, #tablePayment").find("input,button,textarea,select").attr("disabled", "disabled");
                $("#butSingleEditCurrentPlan_Save").removeAttr("disabled").removeClass("buttondisabled").addClass("Button_Standard")
            }
            if (result == 'ViewAccess') {
                $("#singleEditCurrentPlan_actualLocation,#singleEditCurrentPlan_currentLocation").attr("disabled", "disabled");
                $("#butSingleEditCurrentPlan_Save").addClass("buttondisabled disabled").attr("disabled", "disabled"); //Save
                $("#tableSaleInformation, #tablePayment, #ProcessStepsTable").find("input,button,textarea,select").attr("disabled", "disabled");
            }
        } else if (hiddenPageIdentity == "FleetActivity") {
            if (result == 'ViewAccess' || result == 'PartialAccess') {
                $("#FleetActivityRecordsBulkEdit, #FleetActivityRecordAdd").addClass("buttondisabled disabled").attr("disabled", "disabled");
            }
        } else if (hiddenPageIdentity == "FleetRepairManagement") {
            if (result == 'ViewAccess' || result == 'PartialAccess') {
                $("#ctl00_ContentPlaceHolder_btnCreateUpdate, #ctl00_ContentPlaceHolder_btnComplete, #ctl00_ContentPlaceHolder_btnNew, #ctl00_ContentPlaceHolder_btnCancel, #ctl00_ContentPlaceHolder_btnDelete").addClass("buttondisabled disabled").attr("disabled", "disabled");
                $("#tableUnitAndDescription, #tableSetup, #tableCompletion").find("input,button,textarea,select").attr("disabled", "disabled");
            }
        }
            
            


        } //if (result == 'PartialAccess' || result == 'ViewAccess') 
    } //function AIMSAccess(result) 
