﻿
	
 
<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="FleetModelMGT.aspx.vb" Inherits="AIMS_FleetModelMGT" %>

<%@ Register Src="../UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>

<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        #FleetModeltable #CountriesTable #DisplayUnitNumberTable
        {
            width: 100%;
        }
        
        #FleetModeltable > tbody > tr:first-child > td:first-child
        {
            width: 20%;
        }
        
        #FleetModeltable > tbody > tr > td:first-child + td
        {
            width: 80%;
        }
        
        #CountriesTable > tbody > tr:first-child > td:first-child
        {
            width: 20%;
        }
        
        #CountriesTable > tbody > tr > td:first-child + td
        {
            width: 80%;
        }
        
        input[type='text']
        {
            width: 100px;
        }
        
        input[type='button']
        {
            width: 100px;
        }
        
        .allocateunitnumbersbuttons
        {
            text-align: right;
        }
        
        tr > td
        {
            border: 1;
            padding-bottom: 0.5em;
        }
        
        label.error
        {
            font-weight: bold;
        }
        .highlight
        {
            background-color: #E6E6FA;
        }
        
        #EditManufacturerModelTable select
        {
            width: 180px;
        }
        
        #EditManufacturerModelTable input
        {
            width: 175px;
        }
        .mrrDataTable tr:first-child
        {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JqueryLatest/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="JqueryLatest/AIMSvalidator.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery.validate.min.js" type="text/javascript"></script>
    <script src="JS/AIMSRequest.js" type="text/javascript"></script>
    <script src="JS/AIMSfxns.js" type="text/javascript"></script>
    <script src="JS/AIMSpopup.js" type="text/javascript"></script>
    <script src="JS/AIMSdrag.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <!--TOP SEARCHING-->
    <asp:UpdatePanel ID="updCountries" runat="server">
        <ContentTemplate>
            <table id="CountriesTable">
                <thead>
                    <tr>
                        <td>
                            <h3>
                                Search
                            </h3>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="width: 160px;">
                            Country:
                        </td>
                        <td>
                            <select id="CountryDropdown" style="width: 120px;">
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fleet Model Code:
                        </td>
                        <td>
                            <input type="text" id="FleetModelCodeTextbox" runat="server" />
                            <ajaxtoolkit:dropdownextender runat="server" id="DDE" targetcontrolid="FleetModelCodeTextbox"
                                dropdowncontrolid="pnlFleetModels" />
                            <asp:Panel ID="pnlFleetModels" runat="server" BorderColor="Gray" BackColor="White"
                                BorderWidth="1" Style="margin-top: 1px; position: absolute; width: 800px; margin-left: 695px;">
                                <div id="FleetModelsuggestionResultV2" style="width: 800px;">
                                </div>
                            </asp:Panel>
                            <input type="button" value="Load" class="Button_Standard" style="width: 70px;" id="loadButton"/>
                            <input type="button" value="Add" class="Button_Standard Button_Add" style="width: 70px;"
                                id="addButton" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--FLEET MODEL SECTION: display this block when clicking Button or if Search has found-->
    <div id="FleetModelDiv" style="display: none;">
        <table style="width: 100%; visibility: hidden;" id="FleetModeltable" width="100%">
            <tr>
                <td>
                    <h3>
                        Fleet Model
                    </h3>
                </td>
            </tr>
            <tr>
                <td>
                    VT Code:
                </td>
                <td>
                    <label id="VTCodelabel">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    Fleet Model Change:
                </td>
                <td>
                    <label id="FleetModelChangeLabel">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    Fleet Model Code:
                </td>
                <td>
                    <input type="text" id="FleetModelCodeDataTextbox" name="FleetModelCodeDataTextbox"
                        class="required" maxlength="10" />
                    &nbsp; *
                    <label for="FleetModelCodeDataTextbox" class="error">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    Fleet Model Description:
                </td>
                <td>
                    <input type="text" id="FleetModelDescriptionDataTextbox" style="width: 350px;" name="FleetModelDescriptionDataTextbox"
                        class="required" maxlength="128" />
                    &nbsp; *
                    <label for="FleetModelDescriptionDataTextbox" class="error">
                    </label>
                </td>
            </tr>
            <!--
            <tr>
                <td>
                    Conversion Required:
                </td>
                <td>
                    <input type="checkbox" id="ConversionRequiredCheckbox" class="required checkbox"
                        name="ConversionRequiredCheckbox" checked="checked" />
                    *
                    <label for="ConversionRequiredCheckbox" class="error">
                    </label>
                </td>
            </tr>
            -->
            <tr>
                <td>
                    Is Local:
                </td>
                <td>
                    <input type="checkbox" id="isLocalcheckbox" style="text-align: right" />
                </td>
            </tr>
            <tr>
                <td>
                    Daily Running Cost:
                </td>
                <td>
                    <input type="text" id="DailyRunningCosttextBox" name="DailyRunningCosttextBox" class="required" />
                    &nbsp; *
                    <label for="DailyRunningCosttextBox" class="error">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    Daily Delay Cost:
                </td>
                <td>
                    <input type="text" id="DailyDelaytextBox" name="DailyDelaytextBox" class="required" />&nbsp;
                    *
                    <label for="DailyDelaytextBox" class="error">
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    InActive Date:
                </td>
                <td>
                  <uc1:DateControl ID="inactiveDatePicker" runat="server" AutoPostBack="false" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td style="text-align: right; width: 20%;">
                    <input type="button" id="SaveFleetModelButton" value="Save" class="Button_Save Button_Standard" />&nbsp;
                    <input type="button" id="CancelFleetModelButton" value="Cancel" class="Button_Cancel Button_Standard" />&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <!--MANUFACTURER MODEL SECTION-->
    <asp:UpdatePanel ID="UpdatePanelManufacturerModel" runat="server">
        <ContentTemplate>
            <uc1:CollapsiblePanel ID="CollapsiblePanelManufacturerModel" Title="Manufacturer Model"
                Collapsed="false" runat="server" Width="100%" TargetControlID="panelManufacturerModel"
                Visible="true" />
            <asp:Panel Style="overflow: hidden" ID="panelManufacturerModel" runat="server" Width="100%"
                Visible="true">
                <div id="ManufacturerModel">
                </div>
                <div style="text-align: right" id="ManufacturerModelButtonsGroup">
                    <input id="ManufacturerModelEditButton" value="Edit" class="Button_Edit Button_Standard"
                        type="button" style="width: 70px;" />&nbsp;
                    <input id="ManufacturerModelAddButton" value="Add" class="Button_Add Button_Standard"
                        type="button" style="width: 70px;" />&nbsp;
                    <input id="ManufacturerModelLinkButton" value="Link" class="Button_Remove Button_Standard"
                        type="button" style="width: 70px;" />&nbsp;
                    <input id="ManufacturerModelAllocateUnitNumberButton" value="Allocate Unit #" class="Button_Standard"
                        type="button" />&nbsp;
                    <input id="ManufacturerModelSearchUnitNumberButton" value="Search Unit #" class="Button_Standard  Button_Search"
                        type="button" style="width: 120px;" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--ALLOCATED UNIT SECTION-->
    <asp:UpdatePanel ID="UpdatePanelAllocatedUnitNumber" runat="server">
        <ContentTemplate>
            <uc1:CollapsiblePanel ID="CollapsiblePanelAllocatedUnitNumber" Title="Allocated Unit Number"
                Collapsed="false" runat="server" Width="100%" TargetControlID="panelAllocatedUnitNumber"
                Visible="true" />
            <asp:Panel Style="overflow: hidden" ID="panelAllocatedUnitNumber" runat="server"
                Width="100%" Visible="true">
                <div id="AllocatedUnitNumber">
                </div>
                <div style="text-align: right" id="AllocatedUnitNumberbuttonGroup">
                    <input id="AllocatedUnitNumberEditButton" value="Edit" class="Button_Standard Button_Edit"
                        type="button" />&nbsp;
                    <input id="AllocatedUnitNumberAddButton" value="Add Vehicle Plan" class="Button_Standard  Button_Add"
                        type="button" style="width: 200px;" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--VEHICLE PLAN SECTION-->
    <asp:UpdatePanel ID="UpdatePanelVehiclePlan" runat="server">
        <ContentTemplate>
            <uc1:CollapsiblePanel ID="CollapsiblePanelVehiclePlan" Title="Vehicle Plan" runat="server"
                Collapsed="false" Width="100%" TargetControlID="panelVehiclePlan" Visible="true" />
            <asp:Panel Style="overflow: hidden" ID="panelVehiclePlan" runat="server" Width="100%"
                Visible="true">
                <div id="vehiclePlans">
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--POPUP FOR MANUFACTURER MODEL SECTION WHEN CLICKING EDIT AND ADD-->
    <asp:Panel ID="panelNewEditManufacturerModel" runat="server" CssClass="modalPopup"
        Style="display: none; width: 430px; height: 380px; border-color: Black; border-width: 2px;">
        <asp:UpdatePanel ID="updManufacturerModel" runat="server">
            <ContentTemplate>
                <table id="EditManufacturerModelTable" style="margin-left: 10px;" class="EditManufacturerModelTable">
                    <tr>
                        <td colspan="2">
                            <h3 id="ManufacturerModelTitle">
                            </h3>
                        </td>
                        <td style="width: 60px;">
                        </td>
                        <td style="text-align: right;">
                            <a id="A1" href="" runat="server" class="ClosePopup">Close</a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Manufacturer:
                        </td>
                        <td>
                            <select id="ManufacturerSelect" name="ManufacturerSelect">
                            </select>
                            &nbsp; *
                        </td>
                        <td>
                            <label for="ManufacturerSelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr id="trmodelcode">
                        <td>
                            Model Code:
                        </td>
                        <td>
                            <input type="text" id="modelcodelTextboxInEditManufacturerModel" name="modelcodelTextboxInEditManufacturerModel"
                                runat="server" class="required" />
                            &nbsp; *
                            <ajaxtoolkit:dropdownextender runat="server" id="DDE_modelcodelTextboxInEditManufacturerModel"
                                targetcontrolid="modelcodelTextboxInEditManufacturerModel" dropdowncontrolid="pnlModelCodes" />
                            <asp:Panel ID="pnlModelCodes" runat="server" BorderColor="Gray" BackColor="White"
                                BorderWidth="1" Style="margin-top: -1px; position: absolute; width: 180px; margin-left: -3px;">
                                <div id="modelcodelresultV2" style="width: 180px;">
                                </div>
                            </asp:Panel>
                        </td>
                        <td>
                            <label for="modelcodelTextboxInEditManufacturerModel" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Manufacturer Model:
                        </td>
                        <td style='disabled: true;'>
                            <input type="text" id="ManufacturerModelTextBoxInEditManufacturerModel" name="ManufacturerModelTextBoxInEditManufacturerModel"
                                runat="server" class="required" disabled='disabled' />
                            &nbsp; *
                            <ajaxtoolkit:dropdownextender runat="server" id="DDE_ManufacturerModelTextBoxInEditManufacturerModel"
                                targetcontrolid="ManufacturerModelTextBoxInEditManufacturerModel" dropdowncontrolid="pnlModel"
                                enabled='false' />
                            <asp:Panel ID="pnlModel" runat="server" BorderColor="Gray" BackColor="White" BorderWidth="1"
                                Style="margin-top: -1px; position: absolute; width: 180px; margin-left: -3px;"
                                Enabled='false'>
                                <div id="ManufacturerModelresultV2" style="width: 180px;">
                                </div>
                            </asp:Panel>
                        </td>
                        <td>
                            <label for="ManufacturerModelTextBoxInEditManufacturerModel" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Country:
                        </td>
                        <td>
                            <select id="CountrySelect" name="CountrySelect">
                            </select>
                            &nbsp; *
                        </td>
                        <td>
                            <label for="CountrySelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Region:
                        </td>
                        <td>
                            <select id="regionSelect" name="regionSelect">
                            </select>
                            &nbsp; *
                        </td>
                        <td>
                            <label for="regionSelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fuel Type:
                        </td>
                        <td>
                            <select id="fueltypeSelect" name="fueltypeSelect">
                            </select>
                            &nbsp; *
                        </td>
                        <td>
                            <label for="fueltypeSelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Transmission:
                        </td>
                        <td>
                            <select id="TransmissionSelect" name="TransmissionSelect">
                            </select>
                            &nbsp; *
                        </td>
                        <td>
                            <label for="TransmissionSelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Service Schedule:
                        </td>
                        <td>
                            <select id="ServiceScheduleSelect" name="ServiceScheduleSelect">
                            </select>
                            &nbsp; *
                        </td>
                        <td>
                            <label for="ServiceScheduleSelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Chassis Cost:
                        </td>
                        <td>
                            <input type="text" id="ChassisCostText" name="ChassisCostText" />
                            &nbsp; *
                        </td>
                        <td>
                            <label for="ChassisCostText" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="display: none">
                            <input type="text" id="ChassisCostId" name="ChassisCostId" />
                            <input type="text" id="RegionModelId" name="RegionModelId" />
                            <input type="text" id="FleetModelManufacturerModelId" name="FleetModelManufacturerModelId" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input type="button" value="Cancel" id="NewEditmanufacturerModelcancelButton" class="Button_Cancel Button_Standard ClosePopup"
                                style="width: 80px" />
                            &nbsp;
                            <input type="button" value="Save" id="NewEditmanufacturerModelsaveButton" class="Button_Save Button_Standard"
                                style="width: 80px" />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!--POPUP FOR MANUFACTURER MODEL SECTION WHEN CLICKING LINK-->
    <asp:Panel ID="panelLinkManufacturingModel" runat="server" CssClass="modalPopup"
        Style="display: none; width: 330px; height: 470px; border-color: Black; border-width: 2px;">
        <table id="LinkManufacturingTable">
            <tr>
                <td style="width: 400px;">
                    <h3>
                        Link Manufacturer Model
                    </h3>
                </td>
                <td style="text-align: right">
                    <a href="" runat="server" class="ClosePopup">Close</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div id="LinkManufacturing" style="overflow-y: scroll; height: 350px;" />
                </td>
                <td>
                </td>
            </tr>
        </table>
        <div style="text-align: right" id="LinkManufacturingModelButton">
            <input type="button" value="Save" id="LinkManufacturingModelSaveButton" class="Button_Save Button_Standard"
                style="width: 80px;" />
            &nbsp;
            <input type="button" value="Cancel" id="LinkManufacturingModelCancelButton" class="Button_Cancel Button_Standard"
                style="width: 80px;" />
        </div>
    </asp:Panel>
    <!--POPUP FOR MANUFACTURER MODEL SECTION WHEN SEARCH CLICK-->
    <asp:Panel ID="panelDisplayUnitNumber" runat="server" CssClass="modalPopup moveableDisplayUnitNumber" Style="display: none;
        width: 800px; height: 620px; border-color: Black; border-width: 2px;">
    
        <asp:UpdatePanel ID="updatePanelDisplayUnit" runat="server">
            <ContentTemplate>
                <table id="DisplayUnitNumberTable">
                    <tr>
                        <td style="width:100%;">
                            <h3>
                                Display Unit Number
                            </h3>
                        </td>
                        <td style="text-align: right">
                            <a href="" runat="server" class="ClosePopup">Close</a>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 50px;">
                            Filter by:
                        </td>
                        <td><b>Model Code</b></td>
                        <td style="text-align: left">
                            <input type="text" 
                                   id="unitnumberfilteronModel" 
                                   name="unitnumberfilteronModel" 
                                   runat="server"  />
                                  
                            <ajaxtoolkit:dropdownextender 
                                        runat="server" 
                                        id="DDE_unitnumberfilteronModel" 
                                        targetcontrolid="unitnumberfilteronModel"
                                        dropdowncontrolid="panelunitnumberfilteronModel" />
                            
                                    <asp:Panel ID="panelunitnumberfilteronModel" 
                                               runat="server" 
                                               BorderColor="Gray" 
                                               BackColor="White"
                                               BorderWidth="1" 
                                               Style="margin-top: -1px; position: absolute; width: 90%; margin-left: 620px;">
                                        
                                                <div id="unitnumberfilteronModelResult" style="height: 100%; width:100%;" >
                                                </div>
                                    </asp:Panel>
                        </td>
                        <td style="width: 150px;text-align:center;">AND</td>
                        <td><b>Begin Asset #</b></td>
                        <td>
                        <input type="text" 
                                   id="beginAssetfilteronModel" 
                                   name="beginAssetfilteronModel" 
                                    />
                        </td>
                    </tr>
                </table>
                <table  width='99%'>
                     <td>
                        <td colspan="2">
                            <div id="UnitNumber" style="overflow-y: scroll; height: 540px; width:800px;"  />
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    
    </asp:Panel>
    <!--POPUP FOR MANUFACTURER MODEL SECTION WHEN ALLOCATE UNIT NUMBER CLICK-->
    <asp:Panel ID="panelAllocateUnitNumber" runat="server" CssClass="modalPopup moveable" Style="display: none;
        width: 470px; height: 250px; border-color: Black; border-width: 2px;">
        <asp:UpdatePanel runat="server" ID="upAllocateUnitNumber">
            <ContentTemplate>
                <table id="AllocateUnitNumbeTable" name="AllocateUnitNumbeTable_0">
                    <tr>
                        <td colspan="2">
                            <h3>
                                Allocate Unit Numbers
                            </h3>
                        </td>
                        <td width="60px;">
                        </td>
                        <td style="text-align: right">
                            <a id="A2" href="" runat="server" class="ClosePopup">Close</a>
                        </td>
                    </tr>
                    <tr>
                        <td width="150px;">
                            Country:
                        </td>
                        <td>
                            <select id="AllocateUnitNumberCountrySelect" style="width: 110px;" name="AllocateUnitNumberCountrySelect"
                                disabled="disabled">
                            </select>
                            &nbsp;*
                        </td>
                        <td>
                            <label for="AllocateUnitNumberCountrySelect" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Fleet Model Code:
                        </td>
                        <td>
                            <input type="text" id="AllocateUnitNumberFleetModelTextbox" name="AllocateUnitNumberFleetModelTextbox"
                                runat="server" style="width: 180px;" class="required" disabled="disabled" />
                            &nbsp;*
                            <ajaxtoolkit:dropdownextender runat="server" id="DDE_AllocateUnitNumberFleetModelTextbox"
                                targetcontrolid="AllocateUnitNumberFleetModelTextbox" dropdowncontrolid="pnlAllocateUnitNumberFleetModelTextbox"
                                enabled="false" />
                            <asp:Panel ID="pnlAllocateUnitNumberFleetModelTextbox" runat="server" BorderColor="Gray"
                                BackColor="White" BorderWidth="1" Style="margin-top: -3px; position: absolute;
                                width: 180px; margin-left: -8px; display: none;">
                                <div id="AllocateUnitNumberFleetModelV2" style="width: 180px;">
                                </div>
                            </asp:Panel>
                        </td>
                        <td>
                            <label for="AllocateUnitNumberFleetModelTextbox" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Manufacturer:
                        </td>
                        <td>
                            <input type="text" id="AllocateUnitNumberManufacturerTextbox" name="AllocateUnitNumberManufacturerTextbox"
                                runat="server" style="width: 180px;" class="required" disabled="disabled" />
                            &nbsp;*
                            <ajaxtoolkit:dropdownextender runat="server" id="DDE_AllocateUnitNumberManufacturerTextbox"
                                targetcontrolid="AllocateUnitNumberManufacturerTextbox" dropdowncontrolid="pnlAllocateUnitNumberManufacturerTextbox"
                                enabled="false" />
                            <asp:Panel ID="pnlAllocateUnitNumberManufacturerTextbox" runat="server" BorderColor="Gray"
                                BackColor="White" BorderWidth="1" Style="margin-top: -3px; position: absolute;
                                width: 180px; margin-left: -8px; display: none;">
                                <div id="AllocateUnitNumberManufacturerTextboxV2" style="width: 180px;">
                                </div>
                            </asp:Panel>
                        </td>
                        <td>
                            <label for="AllocateUnitNumberManufacturerTextbox" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Begin Unit Number:
                        </td>
                        <td>
                            <input type="text" id="AllocateUnitNumberBeginUnitNumberTextbox" name="AllocateUnitNumberBeginUnitNumberTextbox" />
                            &nbsp;*
                        </td>
                        <td>
                            <label for="AllocateUnitNumberManufacturerTextbox" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            End Unit Number:
                        </td>
                        <td>
                            <input type="text" id="AllocateUnitNumberEndUnitNumberTextbox" name="AllocateUnitNumberEndUnitNumberTextbox" />
                            &nbsp;*
                        </td>
                        <td>
                            <label for="AllocateUnitNumberManufacturerTextbox" class="error">
                            </label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <div style="text-align: right" id="AllocateUnitNumberButtonGroup">
                                <input type="button" value="Save" id="AllocateUnitNumberSaveButton" class="Button_Save Button_Standard"
                                    style="width: 80px;" />
                                &nbsp;
                                <input type="button" value="Cancel" id="AllocateUnitNumberCancelButton" class="Button_Cancel Button_Standard"
                                    style="width: 80px;" />
                            </div>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <input type="hidden" value="0" id="hiddenFleetNumberId" />
    </asp:Panel>
    <asp:HiddenField Value="" runat="server" ID="ManufacturerIDhidden" />
    <asp:HiddenField Value="" runat="server" ID="UnitNumberhidden" />
    <asp:HiddenField Value="" runat="server" ID="ModelCodeshidden" />
    <asp:HiddenField Value="" runat="server" ID="HiddenFieldGetFleetModelCodesAutoSuggest" />
    <asp:HiddenField Value="" runat="server" ID="BeginAssehidden" />
</asp:Content>
