﻿Imports Aurora.AIMS.Services
Imports Aurora.Common.Utility
Imports System
Imports Aurora.Common

Partial Class AIMS_AIMSlistener_FleetSaleDisposalPlanListener
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim result As String = String.Empty

        Try
            Select Case Request.QueryString("MODE")
                Case "GetDropdownForSalePlan"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))

                    result = FleetSaleDisposalPlan.GetDropdownForSalePlan(iCountryId)
                Case "CreateSalePlan"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))
                    Dim iFromUnitNum As Integer = EncodeText(Request("iFromUnitNum"))
                    Dim iToUnitNum As Integer = EncodeText(Request("iToUnitNum"))
                    Dim iFleetModelID As Integer = EncodeText(Request("iFleetModelID"))

                    Dim dStartDate As Nullable(Of DateTime)

                    If (Not Request("dStartDate") Is Nothing) Then
                        If (Not EncodeText(Request("dStartDate")) Is "") Then
                            dStartDate = EncodeText(Request("dStartDate"))
                        End If
                    End If

                    Dim dEndDate As Nullable(Of DateTime)

                    If (Not Request("dEndDate") Is Nothing) Then
                        If (Not EncodeText(Request("dEndDate")) Is "") Then
                            dEndDate = EncodeText(Request("dEndDate"))
                        End If
                    End If

                    Dim iplanquantity As Integer = EncodeText(Request("iplanquantity"))
                    Dim sUserCode As String = EncodeText(Request("sUserCode"))

                    result = FleetSaleDisposalPlan.CreateSalePlan(iCountryId, iFromUnitNum, iToUnitNum, iFleetModelID, dStartDate, dEndDate, iplanquantity, sUserCode)
                Case "CreateSalPlanLine"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))
                    Dim iUnitNumber As Integer = EncodeText(Request("iUnitNumber"))
                    Dim iPlanId As Integer = EncodeText(Request("iPlanId"))
                    Dim sOffFleetLication As String = EncodeText(Request("sOffFleetLication"))

                    Dim dOffFleetDate As Nullable(Of DateTime)

                    If (Not Request("dOffFleetDate") Is Nothing) Then
                        If (Not EncodeText(Request("dOffFleetDate")) Is "") Then
                            dOffFleetDate = EncodeText(Request("dOffFleetDate"))
                        End If
                    End If
                    Dim p_dealer As Integer
                    If (Not String.IsNullOrEmpty(EncodeText(Request("p_dealer")))) Then
                        p_dealer = EncodeText(Request("p_dealer"))
                    End If

                    Dim sUserCode As String = EncodeText(Request("sUserCode"))

                    result = FleetSaleDisposalPlan.CreateSalPlanLine(iCountryId, iUnitNumber, iPlanId, sOffFleetLication, dOffFleetDate, p_dealer, sUserCode)
                Case "DeleteSalePlanRecord"
                    Dim iFleetAssetId As Integer = EncodeText(Request("iFleetAssetId"))
                    Dim iSaleId As Integer = EncodeText(Request("iSaleId"))
                    Dim sUserCode As String = EncodeText(Request("sUserCode"))

                    result = FleetSaleDisposalPlan.DeleteSalePlanRecord(iFleetAssetId, iSaleId, sUserCode)
                Case "GetCountries"
                    result = FleetModel.GetCountries
                Case "GetFleetModelCodesAutoSuggest"
                    Dim _fleetModel As String = EncodeText(Request("FleetModel"))
                    Dim _ICountryId As Integer = EncodeText(Request("ICountryId"))

                    result = FleetModel.GetFleetModelCodesAutoSuggest(_fleetModel, _ICountryId)
                Case "GetVehicleSaleRecords"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))
                    Dim iFleetModelId As String = EncodeText(Request("iFleetModelId"))
                    Dim w_unit_from As Integer = EncodeText(Request("w_unit_from"))
                    Dim w_unit_to As Nullable(Of Integer)
                    Dim sRegoNum As String = EncodeText(Request("sRegoNum"))
                    Dim sUserCode As String = Request("UserCode")

                    If EncodeText(Request("w_unit_to")) = 0 Then
                        w_unit_to = Nothing
                    Else
                        w_unit_to = EncodeText(Request("w_unit_to"))
                    End If

                    result = (FleetSaleDisposalPlan.GetVehicleSaleRecords( _
                                   iCountryId, _
                                   iFleetModelId, _
                                   w_unit_from, _
                                   w_unit_to, _
                                   sRegoNum, _
                                   sUserCode))
                Case "UpdateSalesPlan"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))
                    Dim p_fleet_asset_id As Integer = EncodeText(Request("p_fleet_asset_id"))

                    Dim dDateSold As Nullable(Of DateTime)
                    If (Not Request("dDateSold") Is Nothing) Then
                        If (Not String.IsNullOrEmpty(EncodeText(Request("dDateSold")))) Then
                            dDateSold = EncodeText(Request("dDateSold"))
                        End If
                    End If

                    Dim dPickupDate As Nullable(Of DateTime)
                    If (Not Request("dPickupDate") Is Nothing) Then
                        If (Not String.IsNullOrEmpty(EncodeText(Request("dPickupDate")))) Then
                            dPickupDate = EncodeText(Request("dPickupDate"))
                        End If
                    End If

                    Dim sCustomerType As String = EncodeText(Request("sCustomerType"))
                    If (String.IsNullOrEmpty(sCustomerType)) Then
                        sCustomerType = Nothing
                    ElseIf sCustomerType = "-1" Then
                        sCustomerType = Nothing
                    End If

                    Dim iDealerId As Nullable(Of Integer)
                    If (Not String.IsNullOrEmpty(EncodeText(Request("iDealerId")))) Then
                        If (EncodeText(Request("iDealerId")) <> "-1") Then
                            iDealerId = CInt(EncodeText(Request("iDealerId")))
                        Else
                            iDealerId = -1
                        End If
                    End If


                    Dim iSalePrice As Integer = EncodeText(Request("iSalePrice"))
                    Dim iSalePriceIncTax As Integer = EncodeText(Request("iSalePriceIncTax"))
                    Dim iSalesTaxPercent As Integer = EncodeText(Request("iSalesTaxPercent"))


                    Dim sCustomerName As String = EncodeText(Request("sCustomerName"))
                    If (String.IsNullOrEmpty(sCustomerName)) Then
                        sCustomerName = Nothing
                    End If

                    Dim sSettleMethod As String = EncodeText(Request("sSettleMethod"))
                    If (String.IsNullOrEmpty(sSettleMethod)) Then
                        sSettleMethod = Nothing
                    ElseIf sSettleMethod = "-1" Then
                        sSettleMethod = Nothing
                    End If

                    Dim dSettleBankDate As Nullable(Of DateTime)
                    If (Not Request("dSettleBankDate") Is Nothing) Then
                        If (Not String.IsNullOrEmpty(EncodeText(Request("dSettleBankDate")))) Then
                            dSettleBankDate = EncodeText(Request("dSettleBankDate"))
                        End If
                    End If

                    Dim sSettleLocation As String = EncodeText(Request("sSettleLocation"))
                    If (String.IsNullOrEmpty(sSettleLocation)) Then
                        sSettleLocation = Nothing
                    ElseIf sSettleLocation = "-1" Then
                        sSettleLocation = Nothing
                    End If

                    Dim sSaleComment As String = EncodeText(Request("sSaleComment"))
                    If (String.IsNullOrEmpty(sSaleComment)) Then
                        sSaleComment = Nothing
                    End If

                    Dim bRelease As String = EncodeText(Request("bRelease"))
                    If (String.IsNullOrEmpty(bRelease)) Then
                        bRelease = Nothing
                    End If

                    Dim iPreSaleActualLocId As String = EncodeText(Request("iPreSaleActualLocId"))
                    If (String.IsNullOrEmpty(iPreSaleActualLocId)) Then
                        iPreSaleActualLocId = Nothing
                    End If

                    Dim iCurrentLocationId As String = EncodeText(Request("iCurrentLocationId"))
                    If (String.IsNullOrEmpty(iCurrentLocationId)) Then
                        iCurrentLocationId = Nothing
                    End If

                    Dim dModifiedDate As Nullable(Of DateTime)

                    If (Not Request("dModifiedDate") Is Nothing) Then
                        If (Not String.IsNullOrEmpty(EncodeText(Request("dModifiedDate")))) Then
                            dModifiedDate = EncodeText(Request("dModifiedDate"))
                        End If
                    End If

                    Dim sModifiedBy As String = EncodeText(Request("sModifiedBy"))
                    Dim iSaleId As Integer = EncodeText(Request("iSaleId"))

                    ''rev:mia 14nov2014 start - added RRP --------
                    Dim rrp As String = EncodeText(Request("rrp"))
                    If (String.IsNullOrEmpty(rrp)) Then
                        rrp = Nothing
                    End If
                    ''rev:mia 14nov2014 end   - added RRP --------		

                    result = (FleetSaleDisposalPlan.UpdateSalesPlan(iCountryId, _
                                                                     p_fleet_asset_id, dDateSold, dPickupDate, _
                                                                     sCustomerType, iSalePrice, iSalePriceIncTax, iSalesTaxPercent, _
                                                                     iDealerId, sCustomerName, sSettleMethod, dSettleBankDate, sSettleLocation, _
                                                                     sSaleComment, bRelease, iPreSaleActualLocId, iCurrentLocationId, _
                                                                     dModifiedDate, sModifiedBy, iSaleId, rrp))
                Case "SaveProcessSteps"
                    Dim iProcessStepsId As Integer = EncodeText(Request("iProcessStepsId"))

                    Dim iFleetAssetId As Integer = EncodeText(Request("iFleetAssetId"))
                    Dim iProcessId As Integer = EncodeText(Request("iProcessId"))
                    Dim sAddUserCode As String = EncodeText(Request("sAddUserCode"))
                    Dim bCompleted As Integer = EncodeText(Request("bCompleted"))

                    If (iProcessStepsId.Equals(0)) Then
                        result = (FleetSaleDisposalPlan.SaveProcessSteps(Nothing, _
                                                        iFleetAssetId, _
                                                        iProcessId, _
                                                        sAddUserCode, _
                                                        bCompleted))
                    Else
                        result = (FleetSaleDisposalPlan.SaveProcessSteps(iProcessStepsId, _
                                                                                iFleetAssetId, _
                                                                                iProcessId, _
                                                                                sAddUserCode, _
                                                                                bCompleted))
                    End If

                Case "SaveProcessStepsDate"
                    Dim iProcessStepsId As Integer = EncodeText(Request("iProcessStepsId"))

                    Dim iFleetAssetId As Integer = EncodeText(Request("iFleetAssetId"))
                    Dim iProcessId As Integer = EncodeText(Request("iProcessId"))
                    Dim sAddUserCode As String = EncodeText(Request("sAddUserCode"))
                    Dim dProcessedDate As Date = EncodeText(Request("dProcessedDate"))
                    Dim testString As String = "Nimesh Remove This Later"
                    'If (iProcessStepsId.Equals(0)) Then
                    '    result = (FleetSaleDisposalPlan.SaveProcessSteps(Nothing, _
                    '                                    iFleetAssetId, _
                    '                                    iProcessId, _
                    '                                    sAddUserCode, _
                    '                                    bCompleted))
                    'Else
                    '    result = (FleetSaleDisposalPlan.SaveProcessSteps(iProcessStepsId, _
                    '                                                            iFleetAssetId, _
                    '                                                            iProcessId, _
                    '                                                            sAddUserCode, _
                    '                                                            bCompleted))
                    'End If
                    result = FleetSaleDisposalPlan.SaveProcessStepsDate(iProcessStepsId, iFleetAssetId, iProcessId, sAddUserCode, dProcessedDate)


                Case "GetProcessedSteps"
                    Dim iFleetAssetId As Integer = EncodeText(Request("iFleetAssetId"))
                    result = (FleetSaleDisposalPlan.GetProcessedSteps(iFleetAssetId))
                Case "UpdateSaleRecord"
                    Dim iSaleId As Integer = EncodeText(Request("iSaleId"))
                    Dim dOffFleetDate As Nullable(Of DateTime)

                    If (Not Request("dOffFleetDate") Is Nothing) Then
                        If (Not EncodeText(Request("dOffFleetDate")) Is "") Then
                            dOffFleetDate = EncodeText(Request("dOffFleetDate"))
                        End If
                    End If

                    Dim sSaleLocation As String = EncodeText(Request("sSaleLocation"))
                    Dim iSaleDealerId As Integer = EncodeText(Request("iSaleDealerId"))
                    Dim sUserCode As String = EncodeText(Request("sUsrCode"))

                    result = (FleetSaleDisposalPlan.UpdateSaleRecord(iSaleId, dOffFleetDate, sSaleLocation, iSaleDealerId, sUserCode))
                Case Else

            End Select

        Catch ex As Exception
            result = "ERROR:" & ex.Message
            Logging.LogError("AIMS_AIMSlistener_FleetSaleDisposalPlan", ex.Message & vbCrLf & ex.Source)
        End Try

        Response.Write(result)
        Response.End()

    End Sub
End Class
