﻿Imports System
Imports Aurora.AIMS.Services
Imports Aurora.Common.Utility
Imports Aurora.Common

Partial Class AIMS_AIMSlistener_AIMSListener
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim qs As String = Request.QueryString("MODE")
        Dim result As String = String.Empty
        Dim parameter As String = ""
        Try
            Select Case qs
                Case "CheckUserRole", "CheckUserRoleForFullAccess", "CheckUserRoleForPartialAccess", "CheckUserRoleForViewAccess"
                    Dim usercode As String = Request("usercode")
                    Dim role As String = Request("role")
                    result = EditVehiclePlan.CheckUserRole(usercode, role)

                    ''rev:mia 2oct2014 - more generic aims role.. Kcenia's bday yesteday
                    ''if result is empty, default it to AimsViewOnly
                Case "CheckGenericUserRole"

                    ''Dim usercode As String = Request("usercode")
                    ''Dim RoleMappingActualPage As String = Request("RoleMappingActualPage")
                    ''result = EditVehiclePlan.CheckGenericUserRole(usercode, RoleMappingActualPage)

                Case "GetSingleUnitNumber"
                    Dim ifleetassetid As Integer = Request("fleetassetid")
                    result = EditVehiclePlan.GetSingleUnitNumber(ifleetassetid)
                Case "GetUnitNumberDetails"
                    Dim ifleetassetid As Integer = Request("fleetassetid")
                    result = EditVehiclePlan.GetUnitNumberDetails(ifleetassetid)
                Case "GetDefaultCountry"
                    Dim sUserCode As String = Request("UserCode")
                    result = FleetModel.GetDefaultCountry(sUserCode)
                Case "GetCountries"
                    result = FleetModel.GetCountries
                Case "GetFleetModelCodes"
                    result = FleetModel.GetFleetModelCodes
                Case "GetFleetModelCodesAllocate"
                    result = FleetModel.GetFleetModelCodes
                Case "GetFleetModelCodesAutoSuggest", "GetFleetModelCodesAutoSuggestForUnitNumber"
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    parameter = EncodeText(Request("FleetModel"))
                    result = FleetModel.GetFleetModelCodesAutoSuggest(parameter, CountryId)
                Case "GetFleetModelCodesForAllocateAutoSuggest"
                    parameter = EncodeText(Request("FleetModel"))
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    result = FleetModel.GetFleetModelCodesAutoSuggest(parameter, CountryId)
                Case "GetSingleFleetModelCode"
                    parameter = EncodeText(Request("FleetModel"))
                    ''rev:mia 18-Oct-2016 Issues recoding NZ Fleet 
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    result = FleetModel.GetSingleFleetModelCode(parameter, CountryId)
                Case "InsertUpdateFleetModelCode"

                    Dim iFleetModelId As String = CInt(Request("FleetModelId"))
                    Dim sFleetModelCode As String = EncodeText(Request("FleetModelCode"))
                    Dim sFleetModelDescription As String = EncodeText(Request("FleetModelDescription"))
                    Dim bConversionIndicator As Boolean = CBool(Request("ConversionIndicator"))
                    Dim mDailyRunningCost As Decimal = Convert.ToDecimal(Request("DailyRunningCost"))
                    Dim mDailyDelayCost As Decimal = Convert.ToDecimal(Request("DailyDelayCost"))
                    Dim bIsLocal As Boolean = CBool(Request("IsLocal"))
                    Dim sUserCode As String = Request("UserCode")
                    Dim inactiveDate As String = Request("InactiveDate")


                    result = FleetModel.InsertUpdateFleetModelCode( _
                                                                       CInt(iFleetModelId), _
                                                                       sFleetModelCode, _
                                                                       sFleetModelDescription, _
                                                                       bConversionIndicator, _
                                                                       mDailyRunningCost, _
                                                                       mDailyDelayCost, _
                                                                       bIsLocal, _
                                                                        sUserCode, _
                                                                        inactiveDate _
                                                                        )
                Case "LoadDataForManufacturerModel"
                    result = FleetModel.LoadDataForManufacturerModel()

                Case "InsertUpdateFleetManufacturerModel"
                    Dim FleetModelId As Integer = CInt(Request("FleetModelId"))
                    Dim ManufacturerID As Integer = CInt(Request("ManufacturerID"))
                    Dim FleetManufacturerModelID As Integer = CInt(Request("FleetManufacturerModelID"))
                    Dim ManufacturerModelCode As String = EncodeText(Request("ManufacturerModelCode"))
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    Dim Region As Integer = CInt(Request("Region"))
                    Dim FuelType As String = EncodeText(Request("FuelType"))
                    Dim Transmission As String = Request("Transmission")
                    Dim ManufacturerModel As String = EncodeText(Request("ManufacturerModel"))
                    Dim ServiceScheduleId As Integer = CInt(Request("ServiceScheduleId"))
                    Dim ChassisCost As Decimal = Convert.ToDecimal(Request("ChassisCost"))

                    Dim iChassisCostId As Integer
                    If (Request("iChassisCostId").Equals(String.Empty) Or Request("iChassisCostId") = "null") Then
                        iChassisCostId = Nothing
                    Else
                        iChassisCostId = Math.Round(CInt(Request("iChassisCostId")))
                    End If

                    Dim iRegionModelId As Integer
                    If (Request("iRegionModelId").Equals(String.Empty) Or Request("iRegionModelId") = "null") Then
                        iRegionModelId = -1
                    Else
                        If (String.IsNullOrEmpty(Request("iRegionModelId"))) Then
                            iRegionModelId = -1
                        Else
                            iRegionModelId = CInt(Request("iRegionModelId"))
                        End If


                    End If

                    Dim iFleetModelManufacturerModelId As Integer
                    If (Request("iFleetModelManufacturerModelId").Equals(String.Empty)) Then
                        iFleetModelManufacturerModelId = Nothing
                    Else
                        iFleetModelManufacturerModelId = CInt(Request("iFleetModelManufacturerModelId"))
                    End If

                    Dim iRegionId As Integer = CInt(Request("iRegionId"))

                    Dim UserCode As String = Request("UserCode")

                    result = FleetModel.InsertUpdateFleetManufacturerModel(FleetModelId, _
                                                                           ManufacturerID, _
                                                                           FleetManufacturerModelID, _
                                                                           ManufacturerModelCode, _
                                                                           CountryId, _
                                                                           FuelType, _
                                                                           Transmission, _
                                                                           ManufacturerModel, _
                                                                           ServiceScheduleId, _
                                                                           ChassisCost, _
                                                                           iChassisCostId, _
                                                                           iRegionModelId, _
                                                                           iFleetModelManufacturerModelId, _
                                                                           iRegionId, _
                                                                           UserCode)







                Case "GetFleetModelDetails"
                    Dim FleetModelId As Integer = CInt(Request("FleetModelId"))
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    result = FleetModel.GetFleetModelDetails(FleetModelId, CountryId)

                Case "GetManufacturerModeLCodeAutoSuggest"
                    Dim ManufacturerID As Integer = CInt(Request("ManufacturerID"))
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    Dim ManufacturerModeLCode As String = EncodeText(Request("ManufacturerModeLCode"))
                    result = FleetModel.GetManufacturerModeLCodeAutoSuggest(ManufacturerModeLCode, CountryId, ManufacturerID)

                Case "GetManufacturerModelDescriptionAutoSuggest"

                    Dim ManufacturerModeL As String = EncodeText(Request("ManufacturerModeL"))
                    result = FleetModel.GetManufacturerModelDescriptionAutoSuggest(ManufacturerModeL)

                Case "GetNumberRules"
                    result = FleetModel.GetNumberRules

                Case "LinkFleetModelManufacturerModel"
                    Dim Links As String = EncodeText(Request("Links"))
                    Dim FleetModelId As Integer = CInt(Request("FleetModelId"))
                    Dim UserCode As String = Request("UserCode")
                    result = FleetModel.LinkFleetModelManufacturerModel(Links, FleetModelId, UserCode)

                Case "GetManufacturerAutoSuggest"
                    Dim Manufacturernames As String = EncodeText(Request("ManufacturerNames"))
                    result = FleetModel.GetManufacturerAutoSuggest(Manufacturernames)

                Case "AllocateEditUnitNumber"
                    Dim FleetModelId As Integer = CInt(Request("FleetModelId"))
                    Dim UserCode As String = Request("UserCode")
                    Dim ManufacturerID As Integer = CInt(Request("ManufacturerID"))
                    Dim CountryId As Integer = CInt(Request("CountryId"))
                    Dim FleetNumberId As Integer = CInt(Request("FleetNumberId"))

                    Dim BeginNumber As String = EncodeText(Request("BeginNumber").Split(".")(0))
                    Dim EndNumber As String = EncodeText(Request("EndNumber").Split(".")(0))

                    result = FleetModel.InsUpNumberRules(FleetNumberId, _
                                                         CountryId, _
                                                         FleetModelId, _
                                                         ManufacturerID, _
                                                         Convert.ToInt64(BeginNumber), _
                                                         Convert.ToInt64(EndNumber), _
                                                         UserCode)


                Case "CheckIfVehiclesAreOnFleet"
                    Dim FleetAssetList As String = Request("FleetAssetList")
                    result = FleetModel.CheckIfVehiclesAreOnFleet(FleetAssetList)
                Case Else

            End Select

        Catch ex As Exception
            result = "ERROR:" & ex.Message
            Logging.LogError("AIMS_AIMSlistener_AIMSListener", ex.Message & vbcrlf & ex.Source)
        End Try
        
        Response.Write(result)
        Response.End()

    End Sub

End Class
