﻿Imports Aurora.AIMS.Services
Imports Aurora.Common.Utility
Imports System
Imports Aurora.Common

Partial Class AIMS_AIMSlistener_FleetActivityListener
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        Dim result As String = String.Empty

        Try
            Select Case Request.QueryString("MODE")
                Case "GetDropDownForFleetActivity"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))

                    result = FleetActivity.GetDropDownForFleetActivity(iCountryId)

                Case "GetFleetActivityHistory"
                    Dim iCountryId As Integer = EncodeText(Request("iCountryId"))
                    Dim iUnitNumber As Integer = EncodeText(Request("iUnitNumber"))
                    Dim sRegoNum As String = EncodeText(Request("sRegoNum"))

                    result = FleetActivity.GetFleetActivityHistory(iCountryId, iUnitNumber, sRegoNum)
                Case "CreateNewActivity"
                    Dim iFleetAssetId As Integer = EncodeText(Request("iFleetAssetId"))
                    Dim sActivityType As String = EncodeText(Request("sActivityType"))
                    Dim sExternalRef As String = EncodeText(Request("sExternalRef"))
                    Dim dStartDate As String = EncodeText(Request("dStartDate"))
                    Dim dEndDate As String = EncodeText(Request("dEndDate"))
                    Dim iStartLocation As String = EncodeText(Request("iStartLocation"))
                    Dim iEndLocation As String = EncodeText(Request("iEndLocation"))
                    Dim iStartOddometer As String = EncodeText(Request("iStartOddometer"))
                    Dim iEndOddometer As String = EncodeText(Request("iEndOddometer"))
                    Dim bCompleted As String = EncodeText(Request("bCompleted"))
                    Dim sModifiedBy As String = EncodeText(Request("sModifiedBy"))
                    Dim sdescription As String = EncodeText(Request("sdescription"))
                    Dim bForce As String = EncodeText(Request("bForce"))

                    result = FleetActivity.CreateNewActivity(iFleetAssetId, sActivityType, sExternalRef, dStartDate, dEndDate, iStartLocation, iEndLocation, iStartOddometer, iEndOddometer, bCompleted, sModifiedBy, sdescription, bForce)
                Case "UpdateActivity"
                    Dim iFleetActivityId As Integer = EncodeText(Request("iFleetActivityId"))
                    Dim iFleetAssetId As Integer = EncodeText(Request("iFleetAssetId"))
                    Dim sActivityType As String = EncodeText(Request("sActivityType"))
                    Dim sExternalRef As String = EncodeText(Request("sExternalRef"))
                    Dim dStartDate As String = EncodeText(Request("dStartDate"))
                    Dim dEndDate As String = EncodeText(Request("dEndDate"))
                    Dim iStartLocation As String = EncodeText(Request("iStartLocation"))
                    Dim iEndLocation As String = EncodeText(Request("iEndLocation"))
                    Dim iStartOddometer As String = EncodeText(Request("iStartOddometer"))
                    Dim iEndOddometer As String = EncodeText(Request("iEndOddometer"))
                    Dim bCompleted As String = EncodeText(Request("bCompleted"))
                    Dim sModifiedBy As String = EncodeText(Request("sModifiedBy"))
                    Dim sdescription As String = EncodeText(Request("sdescription"))
                    Dim bForce As String = EncodeText(Request("bForce"))

                    result = FleetActivity.UpdateActivity(iFleetActivityId, iFleetAssetId, sActivityType, sExternalRef, dStartDate, dEndDate, iStartLocation, iEndLocation, iStartOddometer, iEndOddometer, bCompleted, sModifiedBy, sdescription, bForce)
                Case Else

            End Select

        Catch ex As Exception
            result = "ERROR:" & ex.Message
            Logging.LogError("AIMS_AIMSlistener_FleetActivity", ex.Message & vbCrLf & ex.Source)
        End Try

        Response.Write(result)
        Response.End()

    End Sub
End Class
