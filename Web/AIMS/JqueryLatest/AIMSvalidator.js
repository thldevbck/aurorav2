﻿//--------dynamic validations--------
//--------first draft- manny--------
var validator;
$(function () {
    $.validator.addMethod("valueNotEquals", function (value, element, arg) {
        return arg != value;
    }, "Invalid Value");


    validator = $("#aspnetForm").validate({
        rules: {
            DailyRunningCosttextBox:
                    {
                        required: true,
                        number: true
                    },
            DailyDelaytextBox:
                    {
                        required: true,
                        number: true
                    },
            //POPUP FOR MANUFACTURER MODEL SECTION WHEN CLICKING EDIT AND ADD
            ManufacturerSelect: { valueNotEquals: "-1" },
            
            CountrySelect: { valueNotEquals: "-1" },
            fueltypeSelect: { valueNotEquals: "-1" },
            TransmissionSelect: { valueNotEquals: "-1" },
            regionSelect: { valueNotEquals: "-1" },
            ServiceScheduleSelect: { valueNotEquals: "-1" },
            ChassisCostText: { required: true, valueNotEquals: "0.00", number: true },

            //POPUP FOR MANUFACTURER MODEL SECTION WHEN ALLOCATE UNIT NUMBER CLICK
            AllocateUnitNumberFleetModelTextbox : { required: true },
            AllocateUnitNumberManufacturerTextbox: { required: true },
            AllocateUnitNumberBeginUnitNumberTextbox: { required: true, number: true },
            AllocateUnitNumberEndUnitNumberTextbox: { required: true, number: true }

            //Fleet Asset page
            ,ctl00_ContentPlaceHolder_ManufacturerDropDown: { valueNotEquals: "-1" },
            ctl00_ContentPlaceHolder_modelCodeDropDown: { valueNotEquals: "-1" },
            ctl00_ContentPlaceHolder_FuelTypeDropDown: { valueNotEquals: "-1" },
            ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateTextBox: { required: true },
            ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateTextBox: { required: true }
        },
        messages: {
            FleetModelCodeDataTextbox: "Required",
            FleetModelDescriptionDataTextbox: "Required",
            ConversionRequiredCheckbox: "Required",
            DailyRunningCosttextBox:
                        {
                            required: "Required",
                            number: "Number Only"
                        },
            DailyDelaytextBox:
                        {
                            required: "Required",
                            number: "Number Only"
                        },
            //POPUP FOR MANUFACTURER MODEL SECTION WHEN CLICKING EDIT AND ADD
            ManufacturerSelect: { valueNotEquals: "Required" },
            
            
            CountrySelect: { valueNotEquals: "Required" },
            fueltypeSelect: { valueNotEquals: "Required" },
            TransmissionSelect: { valueNotEquals: "Required" },
            regionSelect: { valueNotEquals: "Required" },
            ServiceScheduleSelect: { valueNotEquals: "Required" },
            ChassisCostText:{required: "Required",  number: "Number Only"},
            //POPUP FOR MANUFACTURER MODEL SECTION WHEN ALLOCATE UNIT NUMBER CLICK
            AllocateUnitNumberFleetModelTextbox: { required: "Required" },
            AllocateUnitNumberManufacturerTextbox: { required: "Required" },
            AllocateUnitNumberBeginUnitNumberTextbox: { required: "Required", number: "Number Only" },
            AllocateUnitNumberEndUnitNumberTextbox: { required: "Required", number: "Number Only" }

             //Fleet Asset page
            ,ctl00_ContentPlaceHolder_ManufacturerDropDown: { valueNotEquals: "Required" },
            ctl00_ContentPlaceHolder_modelCodeDropDown: { valueNotEquals: "Required" },
            ctl00_ContentPlaceHolder_FuelTypeDropDown: { valueNotEquals: "Required" },
            ctl00_ContentPlaceHolder_dateEditDeliveryDate_dateTextBox: { required: "Required" },
            ctl00_ContentPlaceHolder_dateEditOnFleetDate_dateTextBox: { required: "Required" }
        }
    });
});
