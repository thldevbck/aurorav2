﻿<%@ Page Title="Load Vehicle Plan" Language="C#" MasterPageFile="~/Include/AuroraHeader.master"
    AutoEventWireup="true" CodeFile="LoadVehiclePlan.aspx.cs" Inherits="AIMS_LoadVehiclePlan" %>

<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <link rel="Stylesheet" type="text/css" href="Styles/common.css" />
    <link rel="Stylesheet" type="text/css" href="Styles/LoadVehiclePlan/StyleSheet.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
 
    <script type="text/javascript" language="javascript"></script>
    <asp:Panel runat="server" ID="pnlVehiclePlan">
        <table style="width: 100%;" cellpadding="2" cellspacing="0">
            <tr>
                <td class="packageSearchTD1">
                    Country
                </td>
                <td class="packageSearchTD2">
                    <asp:TextBox ID="txbCountry" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Fleet Model Code
                </td>
                <td>
                    <asp:TextBox ID="txbFleetModelCode" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Manufacturer
                </td>
                <td>
                    <asp:TextBox ID="txbManufacturer" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Begin Unit Number
                </td>
                <td>
                    <asp:TextBox ID="txbBeginUnitNum" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    End Unit Number
                </td>
                <td>
                    <asp:TextBox ID="txbEndUnitNum" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Manufacturer Model *
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlManufacturerModel">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="valManufacturerModel" runat="server" ControlToValidate="ddlManufacturerModel"
                        ErrorMessage="Please choose a Manufacturer Model"  Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Request Quantity *
                </td>
                <td>
                    <asp:TextBox ID="txbRequestQuantity" runat="server" />
                    <asp:RequiredFieldValidator ID="valRequireRequestQuantity" runat="server" ControlToValidate="txbRequestQuantity"
                        ErrorMessage="Please put a request quantity" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valRequestQuantity" ErrorMessage="Number only"
                        ValidationExpression="\d+" ControlToValidate="txbRequestQuantity" Display="Dynamic"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Delivery From Date *
                </td>
                <td>
                    <uc1:DateControl ID="dateDeliveryFromDate" runat="server" AutoPostBack="false" />
                    <asp:CustomValidator runat="server" ID="valDeliveryDate" ErrorMessage="Please input a valid date" Display="Dynamic"
                        OnServerValidate="dateDeliveryFromDate_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Compliance Plate Year *
                </td>
                <td>
                    <asp:TextBox ID="txbComplianceYear" runat="server" />
                    <asp:RequiredFieldValidator ID="valRequiredPlateYear" runat="server" ControlToValidate="txbComplianceYear" Display="Dynamic"
                        ErrorMessage="Please put a year value" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valComplianceYear" ErrorMessage="Please input a 4 digits year" Display="Dynamic"
                        ValidationExpression="\d{4}" ControlToValidate="txbComplianceYear" ></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Dealer *
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlDealer">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="valDealer" runat="server" ControlToValidate="ddlDealer" Display="Dynamic"
                        ErrorMessage="Please choose a dealer" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Region *
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlRegion">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="valRequiredRegion" runat="server" ControlToValidate="ddlRegion" Display="Dynamic"
                        ErrorMessage="Please choose a region" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    On Fleet Location *
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlFleetLocation">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="valRequiredFleetLocation" runat="server" ControlToValidate="ddlFleetLocation" Display="Dynamic"
                        ErrorMessage="Please choose a fleet location" ></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    On Fleet Date
                </td>
                <td>
                    <uc1:DateControl ID="dateOnFleetDate" runat="server" AutoPostBack="false" />
                    <asp:CustomValidator runat="server" ID="valOnFleetDate" ErrorMessage="Please input a valid date" Display="Dynamic"
                        OnServerValidate="dateOnFleetDate_ServerValidate"></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Lead Time</b>
                </td>
            </tr>
            <tr>
                <td>
                    How many? *
                </td>
                <td>
                    <asp:TextBox ID="txbHowMany" runat="server" />
                    <asp:RequiredFieldValidator ID="valRequiredHowMany" runat="server" ControlToValidate="txbHowMany" Display="Dynamic"
                        ErrorMessage="Please input a value" ></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator runat="server" ID="valHowMany" ErrorMessage="Number only" Display="Dynamic"
                        ValidationExpression="\d+" ControlToValidate="txbHowMany" ></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Frequency *
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlLeadTimeFrequency">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="valRequiredLeadTimeFrequency" runat="server" ControlToValidate="ddlLeadTimeFrequency" Display="Dynamic"
                        ErrorMessage="Please choose a frequency" ></asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
        <br />
        <table class="buttonTable">
            <tr>
                <td class="rightAlign">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Save Button_Standard" OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="Button_Reset Button_Standard"
                        OnClick="btnCancel_Click"  ValidationGroup="none"/>
                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Back Button_Standard" OnClick="btnBack_Click" ValidationGroup="none" />
                </td>
            </tr>
        </table>
        <asp:Panel ID="panHiddenValues" runat="server" Visible="false">
            <asp:Label runat="server" ID="lblFleetModelId" Text=""></asp:Label>
            <asp:Label runat="server" ID="lblManufacturerId" Text=""></asp:Label>
            <asp:Label runat="server" ID="lblCountryId" Text=""></asp:Label>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
