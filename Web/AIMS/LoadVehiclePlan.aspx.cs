﻿using Aurora.AIMS.Data;
using Aurora.AIMS.Services;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using Aurora.AIMS.Data;

[AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.AIMSFleet)]
public partial class AIMS_LoadVehiclePlan : AuroraPage
{
    private double iFleetNumberId;
    private double iCountryId;

    private const string FLEET_MODEL_MGT_URL = "./FleetModelMGT.aspx";//Sorry, this url value should be in web.config. But the config file is locked.
    private const string INITIAL_DROPDOWNLIST_ITEM = "-- Please select --";
    private const string INVALID_MESSAGE = "Sorry, you have provided some invalid information.";

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!Double.TryParse(Request.QueryString["iFleetNumberId"], out iFleetNumberId) ||
                !Double.TryParse(Request.QueryString["iCountryId"], out iCountryId))
            {
                //invalid query string
                pnlVehiclePlan.Visible = false;
                AddErrorMessage(INVALID_MESSAGE);
                return;
            }

            valManufacturerModel.InitialValue = INITIAL_DROPDOWNLIST_ITEM;
            valDealer.InitialValue = INITIAL_DROPDOWNLIST_ITEM;
            valRequiredRegion.InitialValue = INITIAL_DROPDOWNLIST_ITEM;
            valRequiredFleetLocation.InitialValue = INITIAL_DROPDOWNLIST_ITEM;
            valRequiredLeadTimeFrequency.InitialValue = INITIAL_DROPDOWNLIST_ITEM;

            if (!Page.IsPostBack)
            {
                try
                {
                    DataSet dsLoadVehiclePlan = LoadVehicle.GetDropDownDataForLoadVehiclePlan(iFleetNumberId, iCountryId);

                    if (dsLoadVehiclePlan == null)
                    {
                        //invalid query string
                        pnlVehiclePlan.Visible = false;
                        AddErrorMessage(INVALID_MESSAGE);
                    }

                    //basic vehicle plan
                    lblFleetModelId.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["FleetModelId"].ToString();
                    txbCountry.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["CountryDescription"].ToString();
                    txbFleetModelCode.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["FleetModelCode"].ToString();
                    txbManufacturer.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["Manufacturer"].ToString();

                    lblManufacturerId.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["ManufacturerId"].ToString();
                    lblCountryId.Text = iCountryId.ToString();
                    txbBeginUnitNum.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["BeginAssetNum"].ToString();
                    txbEndUnitNum.Text = dsLoadVehiclePlan.Tables[0].Rows[0]["EndAssetNum"].ToString();


                    LoadDropDownList(dsLoadVehiclePlan, ddlManufacturerModel, 1, "ManufacturerModel", "ManufacturerModelId");//manufacturer model
                    LoadDropDownList(dsLoadVehiclePlan, ddlDealer, 2, "DealerName", "DealerId");//Dealer table
                    LoadDropDownList(dsLoadVehiclePlan, ddlRegion, 3, "Region", "RegionId");                //region
                    LoadDropDownList(dsLoadVehiclePlan, ddlFleetLocation, 4, "LocationName", "LocationCode");  //on fleet location
                    LoadDropDownList(dsLoadVehiclePlan, ddlLeadTimeFrequency, 5, "ReferenceDescription", "ReferenceCode"); //Lead time frequency
                }
                catch (Exception)
                {

                    //invalid query string
                    pnlVehiclePlan.Visible = false;
                    AddErrorMessage(INVALID_MESSAGE);
                    return;
                }

            }
        }
        catch (Exception ex)
        {
            this.SetShortMessage(AuroraHeaderMessageType.Error, "Error " + ex.ToString());
        }
    }

    private void LoadDropDownList(DataSet dataset, DropDownList ddl, int dataTableIndex, string textColumnName, string dataColumnName)
    {
        //ClearDropDownList
        ddl.Items.Clear();
        ddl.Items.Add(new ListItem(INITIAL_DROPDOWNLIST_ITEM, INITIAL_DROPDOWNLIST_ITEM));

        foreach (DataRow dr in dataset.Tables[dataTableIndex].Rows)
        {
            if (ddl.Equals(ddlFleetLocation))
            {
                //Loc code - Loc Nmae
                ddl.Items.Add(new ListItem( dr[dataColumnName].ToString() + " - " + dr[textColumnName].ToString(), dr[dataColumnName].ToString()));
            }
            else
            {
                ddl.Items.Add(new ListItem(dr[textColumnName].ToString(), dr[dataColumnName].ToString()));
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (!Page.IsValid)
            return;

        object result;

        try
        {
            result = LoadVehicle.CreateVehiclePlan(
               iCountryId,//@iCountryId					BIGINT,
               int.Parse(lblFleetModelId.Text),//@iFleetModelId				BIGINT,
               int.Parse(lblManufacturerId.Text),//@iManufacturerId			BIGINT,
               ddlManufacturerModel.SelectedValue,//@iManufacturerModelId		BIGINT,
               int.Parse(txbRequestQuantity.Text),//@iFleetRequisitionQuantity	BIGINT,
               dateDeliveryFromDate.Date,//@dLTDealerDeliveryFromDate	DATETIME,
               txbComplianceYear.Text,//@sCompliancePlateYear		VARCHAR(10),
               ddlDealer.SelectedValue,//@iDealerId					BIGINT,
               ddlRegion.SelectedValue, //@iRegionId					BIGINT,
               ddlFleetLocation.SelectedValue, //@sPurLocationOnFleet		VARCHAR(12),
               int.Parse(txbHowMany.Text), //@iLTRateDelivered			BIGINT,
               ddlLeadTimeFrequency.SelectedValue,//@sLTRateDeliveryFrequency	VARCHAR(12),
               dateOnFleetDate.Date, //@dLT1stOnFleetDate			DATETIME,
               UserCode//@sUserCode		VARCHAR(64)
            );

            if (result != null && result.ToString().Contains("ERROR"))
            {
                throw new Exception(result.ToString());
            }

            Response.Redirect(FLEET_MODEL_MGT_URL + "?iFleetModelCode=" + txbFleetModelCode.Text + "&iCountryId=" + lblCountryId.Text, false);

        }
        catch (Exception ex)
        {
            //invalid query string
            //pnlVehiclePlan.Visible = false;

            LogError(INVALID_MESSAGE + "  " + ex.ToString());

            AddErrorMessage(INVALID_MESSAGE + "  " + ex.ToString());

            return;
        }


    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.Url.ToString());
    }

    protected void btnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect(FLEET_MODEL_MGT_URL + "?iFleetModelCode=" + txbFleetModelCode.Text + "&iCountryId=" + lblCountryId.Text);
    }


    protected void dateDeliveryFromDate_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = false;
        DateTime returnDate;


        if (!DateTime.TryParse(dateDeliveryFromDate.Text, out returnDate))
        {
            return;
        }


        e.IsValid = true;
    }

    protected void dateOnFleetDate_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = false;
        DateTime returnDate;


        if (!DateTime.TryParse(dateOnFleetDate.Text, out returnDate))
        {
            return;
        }


        e.IsValid = true;
    }
}