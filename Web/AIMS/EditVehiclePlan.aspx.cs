﻿using Aurora.AIMS.Data;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using System.Text;
using Aurora.AIMS.Services;
using System.Web.UI;
using Newtonsoft.Json;
using Newtonsoft;
using System.Web.UI.HtmlControls;
using Aurora.Telematics.Service.Services;

[AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.FleetAsset)]
public partial class AIMS_EditVehiclePlan : AuroraPage
{
    private const string FLEET_MODEL_MGT_URL = "./FleetModelMGT.aspx";//Sorry, this url value should be in web.config. But the config file is locked.

   
    private string CONST_OFFFLEET = "Off Fleet";
    private string CONST_ONFLEET = "On Fleet";
    private string CONST_SOLD = "SOLD";
    private string CONST_SEARCH_ERROR = "Invalid search values. ";
    private string CONST_UPDATE_ERROR = "Failed to update this unit. ";
    private string CONST_BULKSAVE_ERROR = "Failed to save Bulk Unit. ";
    private string CONST_BULKREBRANDINGSAVE_SUCCESS = "Bulk Rebranding was successfully saved. ";
    private string CONST_BULKREBRANDINGSAVE_ERROR = "Failed to save Bulk Rebranding. ";
    private string CONST_BULKSAVE_SUCCESS = "Bulk Unit was successfully saved. ";
    private string CONST_UPDATE_SUCCESS = "Unit was successfully updated. ";
    private string CONST_PLS_SELECT = "-- Please select --";


    private int manufacturerId = -1;
    private int manufacturerModelId = -1;
    private string mdFuelType = null;
    private string mdTransmission = null;
    private int NextServiceId = -1;
    private int newFleetModelId = -1;
    #region load event

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            

            rblNewVehicle.SelectedIndex = 0;
            HiddenFieldUserCode.Value = UserCode;
            //rev:mia March 14,2014 -add to get default country 
            lblCountryId.Text = (FleetModel.GetDefaultCountry(UserCode) == "AU" ? 1 : 8).ToString();
            HiddenCountryId.Value = lblCountryId.Text;
            ResetPage();

            if (LoadQueryString())
            {
                SearchVehiclePlanUnits();
            }
        }
        ConfirmationBoxControl1.LeftButton_AutoPostBack = false;
        
    }

    #endregion
    
    #region functions
    
    private void ResetPage()
    {
        try
        {
            pnlSearchResult.Visible = false;
            pnlRebrandingSearchResult.Visible = false;

            
            //ClearDropDownList
            ddlFleetModelCode.Items.Clear();
            ddlFleetModelCode.Items.Add(new ListItem(CONST_PLS_SELECT, string.Empty));


            ddlBulkScheduleCode.Items.Clear();
            ddlBulkScheduleCode.Items.Add(new ListItem(CONST_PLS_SELECT, "-1"));

            ddlBulkRebrandingDropDown.Items.Clear();
            ddlBulkRebrandingDropDown.Items.Add(new ListItem(CONST_PLS_SELECT, string.Empty));

            DataTable ds = EditVehiclePlan.GetFleetModelCodes("").Tables[0];

            foreach (DataRow dr in ds.Rows)
            {
                ListItem _lt = new ListItem(dr["FleetModelCode"].ToString().Trim(), dr["FleetModelid"].ToString().Trim());

                ddlFleetModelCode.Items.Add(_lt);
                ddlBulkScheduleCode.Items.Add(_lt);
                ddlBulkRebrandingDropDown.Items.Add(_lt);
            }


            DataTable locationList = new DataTable();

            if (Request.QueryString["iCountryId"] != null)
            {
                locationList = EditVehiclePlan.GetLocationList(Request.QueryString["iCountryId"]).Tables[0];
                lblCountryId.Text = Request.QueryString["iCountryId"];
            }
            else
            {
                //-1 indicate return all locations.
                //rev:mia March 14,2014 -modified to get default country 
                locationList = EditVehiclePlan.GetLocationList(Convert.ToInt16(lblCountryId.Text)).Tables[0];
            }


            //rev:mia March 14,2014 -modified to get default country  and location
            ddlEditStartLocation.DataSource = locationList;
            ddlEditStartLocation.DataTextField = "FullLocation";
            ddlEditStartLocation.DataValueField = "LocationCode";
            ddlEditStartLocation.DataBind();

            ddlBulkStartLocation.DataSource = locationList;
            ddlBulkStartLocation.DataTextField = "FullLocation";
            ddlBulkStartLocation.DataValueField = "LocationCode";
            ddlBulkStartLocation.DataBind();

            ddlBulkStartLocation.Items.Insert(0, new ListItem("", ""));

            txbFromUnitNumber.Text = "";
            txbToUnitNumber.Text = "";
            registrationTextBox.Text = "";

        }
        catch (Exception ex)
        {
            LogException(ex);

            AddErrorMessage(string.Concat(CONST_SEARCH_ERROR, ex.ToString()));
        }
    }

    private bool LoadQueryString()
    {
        bool result = false;

        //load query string
        if (Request.QueryString["iFleetModelId"] != null &&
            ddlFleetModelCode.Items.FindByValue(Request.QueryString["iFleetModelId"].ToString()) != null)
        {
            ddlFleetModelCode.SelectedIndex = ddlFleetModelCode.Items.IndexOf(ddlFleetModelCode.Items.FindByValue(Request.QueryString["iFleetModelId"].ToString()));
            result = true;
        }

        //load query string "from" and "to" unit number values
        if (Request.QueryString["iUnitFromNumber"] != null && txbFromUnitNumber.Text.Equals(string.Empty))
        {
            txbFromUnitNumber.Text = Request.QueryString["iUnitFromNumber"];
            result = true;
        }

        if (Request.QueryString["iUnitToNumber"] != null && txbToUnitNumber.Text.Equals(string.Empty))
        {
            txbToUnitNumber.Text = Request.QueryString["iUnitToNumber"];
            result = true;
        }

        return result;
    }
    
    private void SearchVehiclePlanUnits()
    {
        DataTable dtUnits = new DataTable();

        resultrepeater.DataSource = null;
        resultrepeater.DataBind();

        rebrandingrepeater.DataSource = null;
        rebrandingrepeater.DataBind();

        try
        {

            if (rblNewVehicle.SelectedValue.Equals("NewVehicle"))
            {
                dtUnits = EditVehiclePlan.GetUnitNumber(
                    (Request.QueryString["iRequitionId"] == null) ? null : Request.QueryString["iRequitionId"],
                    (ddlFleetModelCode.SelectedValue.Equals(string.Empty)) ? null : (int?)(int.Parse(ddlFleetModelCode.SelectedValue)),
                    (txbFromUnitNumber.Text.Equals(string.Empty)) ? null : (int?)(int.Parse(txbFromUnitNumber.Text)),
                    (txbToUnitNumber.Text.Equals(string.Empty)) ? null : (int?)(int.Parse(txbToUnitNumber.Text))
                    ,(registrationTextBox.Text.Equals(string.Empty) ? null :  registrationTextBox.Text.Trim())).Tables[0];

              
                resultrepeater.DataSource = dtUnits;
                resultrepeater.DataBind();

                
                pnlSearchResult.Visible = true;
                pnlRebrandingSearchResult.Visible = false;
            }
            else if (rblNewVehicle.SelectedValue.Equals("Rebranding"))
            {
                dtUnits = EditVehiclePlan.GetScheduledChangeFleetModelRules(
                    (ddlFleetModelCode.SelectedValue.Equals(string.Empty)) ? null : (int?)(int.Parse(ddlFleetModelCode.SelectedValue)),
                    (txbFromUnitNumber.Text.Equals(string.Empty)) ? null : (int?)(int.Parse(txbFromUnitNumber.Text)),
                    (txbToUnitNumber.Text.Equals(string.Empty)) ? null : (int?)(int.Parse(txbToUnitNumber.Text)), UserCode
                    , (registrationTextBox.Text.Equals(string.Empty) ? null : registrationTextBox.Text.Trim())).Tables[0];

               
                rebrandingrepeater.DataSource = dtUnits;
                rebrandingrepeater.DataBind();

                pnlSearchResult.Visible = false;
                pnlRebrandingSearchResult.Visible = true;

            }
        }
        catch (Exception ex)
        {
            LogException(ex);
            AddErrorMessage(string.Concat(CONST_SEARCH_ERROR, ex.ToString()));
        }
    }

    private void RowColorCoding(DataRow row, HtmlTableRow tr, string statusfieldname, string datefieldname, System.Web.UI.HtmlControls.HtmlAnchor linkEdit)
    {
        DateTime _temp = new DateTime();
        if (row[statusfieldname] is DBNull == false)
        {
            string status;
            if (linkEdit != null)
            {
                status = row["PlanStatus"].ToString();
                if (status.ToUpper() == CONST_SOLD)
                {
                    linkEdit.InnerText = "View";
                }
            }
            status = row[statusfieldname].ToString();
            if (status == CONST_OFFFLEET)
            {
                tr.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#DDDDDD");
            }
            else
            {
                string fleetdate = row[datefieldname].ToString();
                if (CONST_ONFLEET == status)
                {
                    if (DateTime.TryParse(fleetdate, out _temp))
                    {
                        tr.Style.Add(HtmlTextWriterStyle.BackgroundColor, (_temp <= DateTime.Now ? "#DDFFDD" : "#FFFFDD"));
                    }
                }
                else
                {
                    tr.Style.Add(HtmlTextWriterStyle.BackgroundColor, "#FFFFFF");
                    
                }
               
            }
        }
    }

    //reV:mia March 18,2014 - since data is retrieve using Jquery. Use Request.Form to get all data
    void EnumerateFormCollection()
    {
        foreach (string key in Request.Form)
        {
            if (key.Contains("ddlEditStartLocation") == true)
                ddlEditStartLocation.SelectedValue = Request.Form[key];
            else if (key.Contains("dateEditDeliveryDate") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    dateEditDeliveryDate.Date = Convert.ToDateTime(Request.Form[key]);
                else
                    dateEditDeliveryDate.Text = "";
            }
            else if (key.Contains("dateEditOnFleetDate") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    dateEditOnFleetDate.Date = Convert.ToDateTime(Request.Form[key]);
                else
                    dateEditOnFleetDate.Text = "";
            }
            else if (key.Contains("dateEditReceivedDate") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    dateEditReceivedDate.Date = Convert.ToDateTime(Request.Form[key]);
                else
                    dateEditReceivedDate.Text = "";
            }
            else if (key.Contains("txbEditEngineNumber") == true)
                txbEditEngineNumber.Text = Request.Form[key];
            else if (key.Contains("txbChassisNumber") == true)
                txbChassisNumber.Text = Request.Form[key];
            else if (key.Contains("txbCompliancePlateYear") == true)
                txbCompliancePlateYear.Text = Request.Form[key];
                /*Added by nimesh on 1st july*/
            else if (key.Contains("txbUnit") == true)
                txbUnit.Text = Request.Form[key];
                /*End added by nimesh on 1st july*/
            else if (key.Contains("txbCompliancePlateMonth") == true)
                txbCompliancePlateMonth.Text = Request.Form[key];
            else if (key.Contains("txbEditRegistraionNumber") == true)
                txbEditRegistraionNumber.Text = Request.Form[key];
            else if (key.Contains("dateEditRegistrationDate") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    dateEditRegistrationDate.Date = Convert.ToDateTime(Request.Form[key]);
                else
                    dateEditRegistrationDate.Text = "";
            }
            else if (key.Contains("dateCofDate") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    dateCofDate.Date = Convert.ToDateTime(Request.Form[key]);
                else
                    dateCofDate.Text = "";
            }
            else if (key.Contains("RUCRenewDateControl") == true)
            {
                //rev:mia 22Aug2014
                //if (!string.IsNullOrEmpty(Request.Form[key]))
                //    RUCRenewDateControl.Date = Convert.ToDateTime(Request.Form[key]);
                //else
                //    RUCRenewDateControl.Text = "";
            }
            else if (key.Contains("RUCPaidToTextBox") == true)
                RUCPaidToTextBox.Text = Request.Form[key];
            else if (key.Contains("txbETag") == true)
                txbETag.Text = Request.Form[key];
            else if (key.Contains("txbEPurb") == true)
                txbEPurb.Text = Request.Form[key];
            else if (key.Contains("txbEditFleetAssetId") == true)
                txbEditFleetAssetId.Text = Request.Form[key];
            else if (key.Contains("ManufacturerDropDown") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    manufacturerId = Convert.ToInt32(Request.Form[key].ToString());
            }
            else if (key.Contains("FuelTypeDropDown") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    mdFuelType = Request.Form[key].ToString();
            }
            else if (key.Contains("modelCodeDropDown") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    manufacturerModelId = Convert.ToInt32(Request.Form[key].ToString());
            }
            else if (key.Contains("TransmissionDropDown") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    mdTransmission = Request.Form[key].ToString();
            }
            else if (key.Contains("ServiceScheduleDropDown") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    NextServiceId = Convert.ToInt32(Request.Form[key].ToString());
            }
            /*Added by Nimesh Ref Aurora-41*/
            else if (key.Contains("ddlBulkScheduleCode") == true)
            {
                if (!string.IsNullOrEmpty(Request.Form[key]))
                    newFleetModelId = Convert.ToInt32(Request.Form[key].ToString());
            }
            /*End Added by Nimesh Ref Aurora-41*/
        }
    }

    #endregion
    
    #region buttons event

    protected void backButton_Click(object sender, EventArgs e)
    {
        //rev:mia March 14,2014 - added trapping here for missing parameter
        if (ddlFleetModelCode.SelectedItem.Text.Contains("select") && lblCountryId.Text == "-1")
            Response.Redirect(FLEET_MODEL_MGT_URL);
        else
        {
            if (lblCountryId.Text == "-1" && ddlFleetModelCode.SelectedItem.Text.Contains("select") == false)
                Response.Redirect(FLEET_MODEL_MGT_URL + "?iFleetModelCode=" + ddlFleetModelCode.SelectedItem.Text);
            else if (ddlFleetModelCode.SelectedItem.Text.Contains("select") == true && lblCountryId.Text != "-1")
                Response.Redirect(FLEET_MODEL_MGT_URL + "?iCountryId=" + lblCountryId.Text);
            else
                Response.Redirect(FLEET_MODEL_MGT_URL + "?iFleetModelCode=" + ddlFleetModelCode.SelectedItem.Text + "&iCountryId=" + lblCountryId.Text);
        }
    }

    protected void resetButton_Click(object sender, EventArgs e)
    {
        ResetPage();
        LoadQueryString();

    }

    protected void btnBulkRebrandingSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            bool hasEffectiveDateIncomplete = false;
            bool isInsert  = rblBulkAction.SelectedValue.Equals("insert");

            DataTable dt = new DataTable();

            //mapping to bound fields on GridView
            dt.Columns.Add("UnitNumber");
            dt.Columns.Add("Registration");
            dt.Columns.Add("ModelCode");
            dt.Columns.Add("GeneralStatus");
            dt.Columns.Add("EffectiveDate");

            List<string> iFleetAssetIds = new List<string>();
            GetFleetAssetIds(iFleetAssetIds);
            int countertogetout = -1;
            int counterforrow = -1;

            string UnitNumber = "";
            string Registration = "";
            string ModelCode = "";
            string GeneralStatus = "";
            string EffectiveDate = "";

            foreach (RepeaterItem ri in rebrandingrepeater.Items)
            {
                HtmlTableRow tr = ri.FindControl("tr") as HtmlTableRow;
                string nameattribute = tr.Attributes["name"];
                counterforrow += 1;

                foreach (string fleetassetid in iFleetAssetIds)
                {
                    string finalstring = "";
                    string pos = "";
                   
                        finalstring = fleetassetid.Remove(fleetassetid.IndexOf('-'));
                        pos = fleetassetid.Substring(fleetassetid.IndexOf('-') + 1);
                   
                   
                        if (string.Compare(nameattribute, finalstring) == 0 && pos == counterforrow.ToString())
                        {
                            //System.Diagnostics.Debug.WriteLine(pos + ", " + counterforrow.ToString());
                            UnitNumber = tr.Cells[1].InnerText.TrimEnd().TrimStart();
                            Registration = tr.Cells[2].InnerText.TrimEnd().TrimStart();
                            ModelCode = tr.Cells[3].InnerText.TrimEnd().TrimStart();
                            GeneralStatus = tr.Cells[4].InnerText.TrimEnd().TrimStart();
                            EffectiveDate = "";

                            if (!string.IsNullOrEmpty(tr.Cells[5].InnerText.TrimStart()))
                            {
                                EffectiveDate = tr.Cells[5].InnerText.TrimEnd().TrimStart();
                            }
                            else
                            {
                                hasEffectiveDateIncomplete = true;
                            }
                            dt.Rows.Add(UnitNumber, Registration, ModelCode, GeneralStatus, EffectiveDate);
                            countertogetout++;
                        }
                    
                }

                if (countertogetout == (iFleetAssetIds.Count - 1)) break;
            }
            if (hasEffectiveDateIncomplete && !isInsert)
            {
                ConfirmationBoxControl1.Text = "Can't update original model code for unit " + UnitNumber + ". <br/> Can only update schedule Model codes.";
                ConfirmationBoxControl1.Show();
              
            }
            else
            {
                string rebrandingdate = Request.Form["ctl00_ContentPlaceHolder_dateRebrandingEffectiveDate_dateTextBox"].ToString();

                string result = EditVehiclePlan.InsUpFleetModelChange(
                    dt,
                    isInsert,
                    ddlBulkScheduleCode.SelectedValue, //@iNewFleetModelId
                    Convert.ToDateTime(rebrandingdate),
                    UserCode);


                if (result.Contains("ERROR"))
                {
                    AddErrorMessage(string.Concat(CONST_BULKREBRANDINGSAVE_ERROR, result));
                }
                else
                {
                    //result is empty string
                    AddInformationMessage(CONST_BULKREBRANDINGSAVE_SUCCESS);
                    SearchVehiclePlanUnits();//refresh search result
                }
            }
            
        

            HiddenFieldFleetAssetIds.Value = "";
        }

    }
    
    protected void searchButton_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SearchVehiclePlanUnits();
        }
    }
    protected void btnUpdateGUID_Click(object sender, EventArgs e)
    {

        
        ConnectionService conSer = new ConnectionService();
        FleetService fleetSer = new FleetService();
        string unitNumber = Request.Form["ctl00$ContentPlaceHolder$txbUnit"];
        string userName = ConfigurationManager.AppSettings["UserName"].ToString();
        string userPassword = ConfigurationManager.AppSettings["Password"].ToString();
        string message=string.Empty;
        if(string.IsNullOrEmpty(unitNumber.Trim()))
        {
            AddInformationMessage("Unable to retrive unit number");
            return;
        }
        try
        {

             ConnectedSessionID = conSer.Login(userName, userPassword);
             if (ConnectedSessionID.Equals(Guid.Empty))
             {
                 message = "Your Login to TELEMATICS was Unsuccessful";
                 AddErrorMessage(message);
             }
             else
             {
                 txtTelematicsGUID.Text =  fleetSer.UpdateFleetGUID(connectedSessionID, unitNumber);
                 AddInformationMessage("GUID Updated Successfully please refresh");
                 ResetPage();
             }
       
        }
        catch (Exception)
        {
            
            throw;
        }
        

    }
    private string CheckIfVehiclesAreOnFleet()
    {
        string retMessage = string.Empty;
        string listToPass = HiddenFieldFleetAssetIds.Value.Substring(0, HiddenFieldFleetAssetIds.Value.Length - 1);
        retMessage = FleetModel.CheckIfVehiclesAreOnFleet(listToPass);

        //if (HiddenFieldFleetAssetIds.Value.Length > 1)
        //{
        //    string[] fleetIds = HiddenFieldFleetAssetIds.Value.Split(',');

        //    foreach (string fleetId in fleetIds)
        //    {
        //        if (!string.IsNullOrEmpty(fleetId) && fleetId != ",")
        //        {
        //            listToPass
        //        }
        //    }
        //}
        return retMessage;
    }

    protected void btnBulkSave_Click(object sender, EventArgs e)
    {
        List<string> iFleetAssetIds = new List<string>();

        GetFleetAssetIds(iFleetAssetIds);

        //string retMessage = CheckIfVehiclesAreOnFleet();
        //if (!string.IsNullOrEmpty(retMessage.Replace("\"\"", "")))
        //return;
        
        //string updateResult = EditVehiclePlan.UpdateFleetDetails(
        //              iFleetAssetIds, //fleetAssetId,
        //               (dateBulkDeliveryDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateBulkDeliveryDate.Date, // Delivery Date 
        //               (dateBulkFleetDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateBulkFleetDate.Date, // On Fleet Date 
        //               (ddlBulkStartLocation.SelectedValue.Trim().Equals(string.Empty)) ? null : ddlBulkStartLocation.SelectedValue.Trim(), // Start Location 
        //               (dateBulkReceivedDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateBulkReceivedDate.Date, // Received Date 
        //               null,                             - ok sIdEngineNumber As String,
        //               null,                             - ok sIdChassisNumber As String,
        //               null,                             - ok sKeyNrIgnition As String,
        //               null,                             - ok sIdCompliancePlateYear As Integer?,
        //               null,                             - ok sIdCompliancePlateMonth As Integer?,
        //               null,                             - ok sRegistrationNumber As String
        //               null,                             - ok dRegFirstDate As DateTime? 
        //               null,                             - ok dRCCofEndDate As DateTime?, 
        //               null,                             - ok sEtag
        //               null,                             - ok sepurb 
        //               UserCode,
        //               -1,   //rucPaidTo                 - ok
        //               -1,   //manufacturerId            - ok
        //               -1,   //manufacturerModelId       - ok
        //               null, //mdFuelType                - ok 
        //               null, //mdTransmission            - ok
        //               -1    //NextServiceId             - ok
        //              , null //@dRegoEndDate             - ok
        //              , null  //@sComment                - ok
        //              , null //@dWarrantyExpDate         - ok
        //              , null //@dSelfContainExpDate      - ok
        //              , null //@sTelematicsDevice        - ok
        //              , null //@sTelematicsTablet        - ok
        //              , null // @dTelematicsInstallDate  - ok 
        //              , null // @sTelematicsInstaller	   - ok  
        //           );

        string sComment =  null;
        string sTelematicsDevice = null;
        string sTelematicsTablet = null;
        string sPurLocationOnFleet = (!string.IsNullOrEmpty(ddlBulkStartLocation.SelectedValue) ? ddlBulkStartLocation.SelectedValue : null);
        string sIdEngineNumber = null;
        string sIdChassisNumber = null;
        string sKeyNrIgnition = null;
        string sRegistrationNumber = null;
        string sETag = null;
        string sEPurb = null;
        string sUserCode = UserCode;
        string sMdFuelType = null;
        string sMdTransmission = null;
        string sTelematicsInstaller = null;

        int iRUCPaidToKms = -1;
        int iManufacturerId = -1;      
        int iManufacturerModelId = -1;
        int iIdCompliancePlateYear = -1;
        int iIdCompliancePlateMonth = -1;
        int iNextServiceId = -1;

        DateTime dPurProposedDelivDate  = (dateBulkDeliveryDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateBulkDeliveryDate.Date;
        DateTime dFirstOnFleetDate      = (dateBulkFleetDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateBulkFleetDate.Date;
        DateTime dPurReceiptDate        = (dateBulkReceivedDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateBulkReceivedDate.Date;
        DateTime dRegFirstDate          = DateTime.MinValue;
        DateTime dRCCofEndDate          = DateTime.MinValue;
        DateTime dRegoEndDate           = DateTime.MinValue;
        DateTime dWarrantyExpDate       = DateTime.MinValue;
        DateTime dSelfContainExpDate    = DateTime.MinValue;
        DateTime dTelematicsInstallDate = DateTime.MinValue;
        DateTime dTelematicsTabletInstallDate = DateTime.MinValue;
        string sTelematicsTabletInstaller = null;

        int newFleetModelId = (!string.IsNullOrEmpty(ddlBulkRebrandingDropDown.SelectedValue) ? int.Parse(ddlBulkRebrandingDropDown.SelectedValue) : 0);
        //int.Parse(ddlBulkScheduleCode.SelectedItem.Value);

        string updateResult = EditVehiclePlan.UpdateFleetDetailsV2(iFleetAssetIds,
                         iRUCPaidToKms,
                         iManufacturerId,
                         iNextServiceId,
                         iManufacturerModelId,
                         iIdCompliancePlateYear,
                         iIdCompliancePlateMonth,
                         dPurProposedDelivDate,
                         dFirstOnFleetDate,
                         dPurReceiptDate,
                         dRegFirstDate,
                         dRCCofEndDate,
                         dRegoEndDate,
                         dWarrantyExpDate,
                         dSelfContainExpDate,
                         dTelematicsInstallDate,
                         sComment,
                         sTelematicsDevice,
                         sTelematicsTablet,
                         sPurLocationOnFleet,
                         sIdEngineNumber,
                         sIdChassisNumber,
                         sKeyNrIgnition,
                         sRegistrationNumber,
                         sETag,
                         sEPurb,
                         sUserCode,
                         sMdFuelType,
                         sMdTransmission,
                         sTelematicsInstaller,
                         dTelematicsTabletInstallDate, 
                         sTelematicsTabletInstaller,
                         newFleetModelId
                         );

        
        if (updateResult.Contains("ERROR"))
        {
            AddErrorMessage(string.Concat(CONST_BULKSAVE_ERROR, updateResult));
        }
        else
        {
            //result is empty string
            AddInformationMessage(CONST_BULKSAVE_SUCCESS);
            SearchVehiclePlanUnits();//refresh search result
        }
        HiddenFieldFleetAssetIds.Value = "";
    }

    

  
    private void GetFleetAssetIds(List<string> iFleetAssetIds)
    {
        if (HiddenFieldFleetAssetIds.Value.Length > 1)
        {
            string[] fleetIds = HiddenFieldFleetAssetIds.Value.Split(',');

            foreach (string fleetId in fleetIds)
            {
                if (!string.IsNullOrEmpty(fleetId) && fleetId != ",")
                {
                    iFleetAssetIds.Add(fleetId);
                }
            }
        }
    }

    protected void btnSaveSingleVehicle_Click(object sender, EventArgs e)
    {
        string updateResult;

        int yearNum, monthNum;

        //reV:mia March 18,2014 - since data is retrieve using Jquery. Use Request.Form to get all data
        EnumerateFormCollection();


        List<string> iFleetAssetIds = new List<string>();
        iFleetAssetIds.Add(txbEditFleetAssetId.Text);

        //int _manufacturerId = (manufacturerId.ToString() != "-1" ? manufacturerId : -1);
        //int _manufacturerModelId = (manufacturerModelId.ToString() != "-1" ? manufacturerModelId : -1);
        //string _mdFuelType = (mdFuelType != "-1" ? mdFuelType : null);
        //string _mdTransmission = (mdTransmission != "-1" ? mdTransmission : null);
        //int _NextServiceId = (NextServiceId.ToString() != "-1" ? Convert.ToInt32(NextServiceId) : -1);

 
        // rev:mia  August 22 ,2014 WarrantyExpDate,SelfContainExpDate,TelematicsDevice,TelematicsTablet

        //updateResult = EditVehiclePlan.UpdateFleetDetails(
        //1       iFleetAssetIds,
        //2       (dateEditDeliveryDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateEditDeliveryDate.Date,
        //3       (dateEditOnFleetDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateEditOnFleetDate.Date,
        //4       ddlEditStartLocation.SelectedValue,
        //5       (dateEditReceivedDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateEditReceivedDate.Date,
        //6       txbEditEngineNumber.Text,
        //7       txbChassisNumber.Text,
        //8       txbIgnitionKey.Text,
        //9       (int.TryParse(txbCompliancePlateYear.Text, out yearNum)) ? (int?)yearNum : null,
        //10       (int.TryParse(txbCompliancePlateMonth.Text, out monthNum)) ? (int?)monthNum : null,
        //11       txbEditRegistraionNumber.Text,
        //12       (dateEditRegistrationDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateEditRegistrationDate.Date,
        //13       (dateCofDate.Date.Equals(DateTime.MinValue)) ? null : (DateTime?)dateCofDate.Date,
        //14       txbETag.Text,
        //15       txbEPurb.Text,
        //16       UserCode,
        //17       (string.IsNullOrEmpty(RUCPaidToTextBox.Text) ? -1 : Convert.ToInt32(RUCPaidToTextBox.Text)),
        //18       _manufacturerId,
        //19       _manufacturerModelId,
        //20       _mdFuelType,
        //21       _mdTransmission,
        //22       _NextServiceId, 
        //23       txtcomments.Value.TrimEnd(),
        //24       RegExpiryDataDateControl.Text,
        //25       WarrantyExpiryDateControl.Text,
        //26       SelfContainmentDateControl.Text,
        //27       txtTelematicsDevice.Text,
        //28       txtTelematicsTablet.Text,
        //29       (DateControlTelematicsInstallDate.Date.Equals(DateTime.MinValue)) ? null : DateControlTelematicsInstallDate.Text,
        //30       txttelematicsInstaller.Text
                
        //   );


        int iRUCPaidToKms           = (string.IsNullOrEmpty(RUCPaidToTextBox.Text) ? -1 : Convert.ToInt32(RUCPaidToTextBox.Text));
        int iManufacturerId         = (manufacturerId.ToString() != "-1" ? manufacturerId : -1);
        int iNextServiceId          = (NextServiceId.ToString() != "-1" ? Convert.ToInt32(NextServiceId) : -1);
        int iManufacturerModelId    = (manufacturerModelId.ToString() != "-1" ? manufacturerModelId : -1);
        int iIdCompliancePlateYear  = (int.TryParse(txbCompliancePlateYear.Text, out yearNum)) ? yearNum : -1;
        int iIdCompliancePlateMonth = (int.TryParse(txbCompliancePlateMonth.Text, out monthNum)) ? monthNum : -1;

        DateTime dPurProposedDelivDate  = (dateEditDeliveryDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateEditDeliveryDate.Date;
        DateTime dFirstOnFleetDate      = (dateEditOnFleetDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateEditOnFleetDate.Date;
        DateTime dPurReceiptDate        = (dateEditReceivedDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateEditReceivedDate.Date;
        DateTime dRegFirstDate          = (dateEditRegistrationDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateEditRegistrationDate.Date;
        DateTime dRCCofEndDate          = (dateCofDate.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)dateCofDate.Date;
        DateTime dRegoEndDate           = (RegExpiryDataDateControl.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)RegExpiryDataDateControl.Date;
        DateTime dWarrantyExpDate       = (WarrantyExpiryDateControl.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)WarrantyExpiryDateControl.Date;
        DateTime dSelfContainExpDate    = (SelfContainmentDateControl.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)SelfContainmentDateControl.Date;
        DateTime dTelematicsInstallDate = (DateControlTelematicsInstallDate.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)DateControlTelematicsInstallDate.Date;

        string sComment             = (!string.IsNullOrEmpty(txtcomments.Value)? txtcomments.Value : null);
        string sTelematicsDevice    = (!string.IsNullOrEmpty(txtTelematicsDevice.Text) ? txtTelematicsDevice.Text : null);
        //changed by nimesh DSD-865
        //string sTelematicsTablet    = (!string.IsNullOrEmpty(txtTelematicsTablet.Text) ? txtTelematicsTablet.Text : null);
        string sTelematicsTablet = string.Empty;
        DateTime dTelematicsTabletDate = (TelematicsTabletDateDateControl.Date.Equals(DateTime.MinValue)) ? DateTime.MinValue : (DateTime)TelematicsTabletDateDateControl.Date;
        string sTelematicsTabletInstaller = (!string.IsNullOrEmpty(txtTelematicsTabletInstaller.Text) ? txtTelematicsTabletInstaller.Text : null);
        // End Changed by nimesh DSD-865
        string sPurLocationOnFleet  = (!string.IsNullOrEmpty(ddlEditStartLocation.SelectedValue) ? ddlEditStartLocation.SelectedValue : null);
        
        string sIdEngineNumber      = (!string.IsNullOrEmpty(txbEditEngineNumber.Text) ? txbEditEngineNumber.Text : null);
        string sIdChassisNumber     = (!string.IsNullOrEmpty(txbChassisNumber.Text) ? txbChassisNumber.Text : null);
        string sKeyNrIgnition       = (!string.IsNullOrEmpty(txbIgnitionKey.Text) ? txbIgnitionKey.Text : null);
        string sRegistrationNumber  = (!string.IsNullOrEmpty(txbEditRegistraionNumber.Text) ? txbEditRegistraionNumber.Text : null);
        string sETag                = (!string.IsNullOrEmpty(txbETag.Text) ? txbETag.Text : null);
        string sEPurb               = (!string.IsNullOrEmpty(txbEPurb.Text) ? txbEPurb.Text : null);
        string sUserCode            = UserCode;
        string sMdFuelType          = (mdFuelType != "-1" ? mdFuelType : null);
        string sMdTransmission      = (mdTransmission != "-1" ? mdTransmission : null);
        string sTelematicsInstaller = (!string.IsNullOrEmpty(txttelematicsInstaller.Text) ? txttelematicsInstaller.Text : null);

        updateResult = EditVehiclePlan.UpdateFleetDetailsV2(iFleetAssetIds,
                 iRUCPaidToKms,
                 iManufacturerId,
                 iNextServiceId,
                 iManufacturerModelId,
                 iIdCompliancePlateYear,
                 iIdCompliancePlateMonth,
                 dPurProposedDelivDate,
                 dFirstOnFleetDate,
                 dPurReceiptDate,
                 dRegFirstDate,
                 dRCCofEndDate,
                 dRegoEndDate,
                 dWarrantyExpDate,
                 dSelfContainExpDate,
                 dTelematicsInstallDate,
                 sComment,
                 sTelematicsDevice,
                 sTelematicsTablet,
                 sPurLocationOnFleet,
                 sIdEngineNumber,
                 sIdChassisNumber,
                 sKeyNrIgnition,
                 sRegistrationNumber,
                 sETag,
                 sEPurb,
                 sUserCode,
                 sMdFuelType,
                 sMdTransmission,
                 sTelematicsInstaller, 
                 dTelematicsTabletDate, 
                 sTelematicsTabletInstaller,0);
 

        if (updateResult.Contains("ERROR"))
        {
            AddErrorMessage(string.Concat(CONST_UPDATE_ERROR, updateResult));
        }
        else
        {
            //refresh search result
            SearchVehiclePlanUnits();
            AddInformationMessage(CONST_UPDATE_SUCCESS);
        }

    }

    #endregion

    #region repeater event
    protected void rebrandingrepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem ri = e.Item;

        switch (ri.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                {
                    DataRow row = ((DataRowView)ri.DataItem).Row;
                    HtmlTableRow tr = ri.FindControl("tr") as HtmlTableRow;
                    if (tr != null) RowColorCoding(row, tr, "GeneralStatus", "EffectiveDate",  null);


                    CheckBox chkRebrandingSelectUni = (CheckBox)tr.FindControl("chkRebrandingSelectUni");
                    chkRebrandingSelectUni.Visible = (chkRebrandingSelectUni != null && row["AcqDispStatus"].ToString() != "Sold");
                    chkRebrandingSelectUni.Attributes.Add("name", "rowindex_" + ri.ItemIndex.ToString());

                    if (row["AcqDispStatus"].ToString() == "Sold")
                    {
                        tr.Attributes.Remove("name");
                    }
                    else
                    {
                        tr.Attributes.Add("name", row["FleetAssetId"].ToString());
                    }
                    break;
                }
        }
    }
    protected void resultrepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        RepeaterItem ri = e.Item;

        switch (ri.ItemType)
        {
            case ListItemType.Item:
            case ListItemType.AlternatingItem:
                {
                    DataRow row = ((DataRowView)ri.DataItem).Row;
                    HtmlTableRow tr = ri.FindControl("tr") as HtmlTableRow;
                    if (tr != null)
                    {

                        System.Web.UI.HtmlControls.HtmlAnchor linkEdit = (System.Web.UI.HtmlControls.HtmlAnchor)tr.FindControl("linkEdit");
                        if (linkEdit != null)
                        {
                            if (row["UnitNumber"] is DBNull == false)
                                linkEdit.ID = "linkUnit" + row["UnitNumber"].ToString();
                        }

                        RowColorCoding(row, tr, "Status", "FirstOnFleetDate", linkEdit);


                        CheckBox chkSelectUni = (CheckBox)tr.FindControl("chkSelectUni");
                        chkSelectUni.Visible = (chkSelectUni != null && row["PlanStatus"].ToString() != "Sold");
                        chkSelectUni.Attributes.Add("name", "Sold");
                       
                        
                    }

                    break;
                }
        }
    }

    #endregion
    
    #region validate
    /// <summary>
    /// Fleet model code is mandatory or from unit number and to unit number is mandatory.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void valSearchKeywords_ServerVAlidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = (ddlFleetModelCode.SelectedValue.Equals(string.Empty) && (txbFromUnitNumber.Text.Equals(string.Empty) && txbToUnitNumber.Text.Equals(string.Empty)) ? false : true);
    }
        
    protected void valRebrandingEffectiveDate_ServerValidate(object sender, ServerValidateEventArgs e)
    {
        e.IsValid = (dateRebrandingEffectiveDate.Date.Equals(DateTime.MinValue) ? false : true);
    }

    #endregion



    private Guid connectedSessionID;

    public Guid ConnectedSessionID
    {
        get { return connectedSessionID; }
        set { connectedSessionID = value; }
    }
}

