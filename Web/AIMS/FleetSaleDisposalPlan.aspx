﻿<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="true"
    Title="Fleet Sale and Disposal Plan" CodeFile="FleetSaleDisposalPlan.aspx.vb"
    Inherits="AIMS_FleetSaleDisposalPlan" %>

<%@ Register Src="../UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<asp:Content ID="cphStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <link rel="Stylesheet" type="text/css" href="Styles/common.css" />
    <link rel="Stylesheet" type="text/css" href="Styles/FleetSaleDisposalPlan/StyleSheet.css" />
    
</asp:Content>
<asp:Content ID="cphScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JqueryLatest/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery-ui-1.10.2.js" type="text/javascript"></script>
    <script src="JqueryLatest/jquery.validate.min.js" type="text/javascript"></script>
    <script src="JqueryLatest/knockout-3.0.0.js" type="text/javascript"></script>
    <script src="JqueryLatest/knockout-validation.js"  type="text/javascript"></script>
    <script src="JS/FleetSaleDisposalPlan/Models.js" type="text/javascript"></script>
    
    <script src="JS/AIMSCommon.js" type="text/javascript"></script>
    <script src="JS/GenericAIMSAccess.js" type="text/javascript"></script>
    <script src="JS/FleetSaleDisposalPlan/Common.js" type="text/javascript"></script>
    <script src="JS/FleetSaleDisposalPlan/Request.js" type="text/javascript"></script>
    
    <script type="text/javascript">
        var isFullAccess = false;
        var isPartialAccess = false;
        var isViewAccess = false;
        var isFleetManager = false;
    </script>
 
</asp:Content>
<asp:Content ID="cphContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    
    <div id="screen">
    </div>
    <div id="disposalPlanSearchPanel">
        <div class="row">
            <table id="tableFleetSalesDisposalData">
                <tr>
                    <td>
                        Country:
                    </td>
                    <td>
                        <select id="selSearchCountry">
                        </select>&nbsp;*
                    </td>
                    <td>
                    </td>
                    <td>
                        Fleet Model Code:
                    </td>
                    <td>
                        <input type="text" id="FleetModelCodeTextbox" />
                        <input type="text" id="FleetModelIdTextbox" />
                    </td>
                </tr>
                <tr>
                    <td>
                        From Unit Number:
                    </td>
                    <td>
                        <input type="text" id="inputFromUnitNumber" />
                    </td>
                    <td>
                    </td>
                    <td>
                        To Unit Number:
                    </td>
                    <td>
                        <input type="text" id="inputToUnitNumber" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Registration Number:
                    </td>
                    <td>
                        <input type="text" id="inputRegNum" />
                    </td>
                </tr>
            </table>
     
        </div>
        <div class="row">
            <table class="buttonTable">
                <tr>
                    <td class="rightAlign">
                        <button data-bind="click: $root.search.bind($data, true)" class="Button_Standard Button_Search">
                            Search</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <!--SECTION-->
    <uc1:CollapsiblePanel ID="CollapsiblePanelUnitPlan" Title="Vehicle Sale" Collapsed="false"
        runat="server" Width="100%" TargetControlID="panelUnitPlan" Visible="true" />
    <asp:Panel Style="overflow: hidden" ID="panelUnitPlan" runat="server" Width="100%"
        Visible="true">
        <table id="VehicleSaleRecords" class="divResult " cellspacing="0" cellpadding="0" style="width:100%;border-collapse:collapse; border-color: #999;border-style: solid;">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" data-bind="checked: AllVehicleSaleRecords, click: $root.selectAllVehicleSaleRecords_click" />
                    </th>
                    <th>
                        Unit
                    </th>
                    <th>
                        Registration
                    </th>
                    <th>
                        Model Code
                    </th>
                    <th>
                        Comp Plate mm
                    </th>
                    <th>
                        Comp Plate YYYY
                    </th>
                    <th>
                        Manufacture
                    </th>
                    <th>
                        Odometer
                    </th>
                    <th>
                        Off Fleet Date *
                    </th>
                    <th>
                        Off Fleet Location *
                    </th>
                    <th>
                        Sale Dealer
                    </th>
                </tr>
            </thead>
            <tbody data-bind="foreach: VehicleSaleRecords().sort(function (l, r) { return l.UnitNumber() < r.UnitNumber() ? -1 : 1 })">
                <tr data-bind="css: { modifiedVehicleSaleRow: Modified() }">
                    <td>
                        <input type="checkbox" data-bind="checked: SelectedItem , visible: isVisible" />
                    </td>
                    <td>
                        <span data-bind="text: UnitNumber"></span>
                    </td>
                    <td>
                        <span data-bind="text: RegistrationNumber"></span>
                    </td>
                    <td>
                        <span data-bind="text: FleetModelCode"></span>
                    </td>
                    <td>
                        <span data-bind="text: IdCompliancePlateMonth"></span>
                    </td>
                    <td>
                        <span data-bind="text: IdCompliancePlateYear"></span>
                    </td>
                    <td>
                        <span data-bind="text: Manufacturer"></span>
                    </td>
                    <td>
                        <span data-bind="text: Odometer"></span>
                    </td>
                    <td>
                        <span data-bind="text: OffFleetDate"></span>
                    </td>
                    <td>
                        <span data-bind="text: SaleLocation"></span>
                    </td>
                    <td>
                        <span data-bind="text: DealerName"></span>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="buttonTable">
            <tr>
                <td class="rightAlign">
                    <button id="VehicleSaleRecordsBulkEdit" class="Button_Standard Button_Edit" data-bind="click: $root.VehicleSaleRecordsBulkEdit">
                        Bulk Unit(s) Edit</button>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <div id="bulkEditVehicleSalePanel" class="modalPopup">
        <h3>Update Unit Defaults</h3>
        <table id="tableUpdateUnitDefault">
            <tr>
                <td>
                    Off Fleet Date:
                </td>
                <td>
                    <uc1:DateControl ID="calVehicleSaleOffFleetDate" runat="server" AutoPostBack="false" />&nbsp;*
                </td>
            </tr>
            <tr>
                <td>
                    Off Fleet Location:
                </td>
                <td>
                     <select id="bulkEditVehicleSale_offFleetLocation" class="offFleetLocation">
                     </select>&nbsp;*
                </td>
            </tr>
            <tr>
                <td>
                    Sale Dealer:
                </td>
                <td>
                    <select id="bulkEditVehicleSale_saleDealer" class="saleDealer">
                    </select>
                </td>
            </tr>
        </table>
        <div class="divBulkButtonsV1 bulkEditVehicleSalePanelButtons">
            <button class="Button_Standard Button_OK" data-bind="click: $root.bulkEditVehicleSale_OK">
                Apply</button>&nbsp;
            <button class="Button_Standard Button_Reset" data-bind="click: $root.bulkEditVehicleSale_Reset">
                Reset</button>&nbsp;
            <button class="Button_Standard Button_Cancel" data-bind="click: $root.bulkEditVehicleSale_Cancel">
                Cancel</button>
        </div>
    </div>
    <div id="createSalePlanPanel" class="">
    
        <div class="divBulkButtonsV1 createSalePlanPanelButtons">
             <button class="Button_Standard Button_OK" id="buttonCreateSalePlan" data-bind="click: $root.createSalePlan_OK">
                Create</button>
            <button class="Button_Standard Button_Reset" data-bind="click: $root.createSalePlan_Reset">
                Reset</button>
        </div>
    </div>
    <uc1:CollapsiblePanel ID="CollapsibleCurrentPlan" Title="Current Plan" Collapsed="false"
        runat="server" Width="100%" TargetControlID="panelCurrentPlan" Visible="true" />
    <asp:Panel Style="overflow: hidden" ID="panelCurrentPlan" runat="server" Width="100%"
        Visible="true">
        <table id="CurrentPlanRecords" class="divResult">
            <thead>
                <tr>
                    <th>
                        <input type="checkbox" data-bind="checked: AllCurrentPlans, click: $root.selectAllCurrentPlans_click" />
                    </th>
                    <th>
                        Unit
                    </th>
                    <th>
                        Registration
                    </th>
                    <th>
                        Model Code
                    </th>
                    <th>
                        Manufacture
                    </th>
                    <th>
                        Off Fleet Date
                    </th>
                    <th>
                        Off Fleet Locaion
                    </th>
                    <th>
                        Sale Dealer
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Create Date
                    </th>
                    <th>
                        Added by
                    </th>
                </tr>
            </thead>
            <tbody data-bind="foreach: CurrentPlanRecords().sort(function (l, r) { return l.UnitNumber() < r.UnitNumber() ? -1 : 1 })">
                <tr data-bind="css: ModifiedCurrentPlanRowColor">
                    <td>
                        <input type="checkbox" data-bind="checked: SelectedItem, style : {display: AllowSelection() ? 'block' : 'none'}" />
                    </td>
                    <td>
                        <a data-bind="text: UnitNumber, attr:{title: PlanNumber}, click: $root.singleEditCurrentPlan_item">
                        </a>
                    </td>
                    <td>
                        <span data-bind="text: RegistrationNumber"></span>
                    </td>
                    <td>
                        <span data-bind="text: FleetModelCode"></span>
                    </td>
                    <td>
                        <span data-bind="text: Manufacturer"></span>
                    </td>
                    <td>
                        <span data-bind="text: OffFleetDate"></span>
                    </td>
                    <td>
                        <span data-bind="text: SaleLocation"></span>
                    </td>
                    <td>
                        <span data-bind="text: DealerName"></span>
                    </td>
                    <td>
                        <span data-bind="text: Status"></span>
                    </td>
                    <td>
                        <span data-bind="text: ModifiedDate"></span>
                    </td>
                    <td>
                        <span data-bind="text: ModifiedBy"></span><span data-bind="text: FleetAssetId" style="display: none">
                        </span><span data-bind="text: SaleId" style="color: #0f0; display: none"></span>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="buttonTable">
            <tr>
                <td class="rightAlign">
                    <button data-bind="click: $root.bulkEditCurrentPlan_Edit" id="buttonbulkEditCurrentPlan_Edit" class="Button_Standard Button_Edit">
                        Bulk Unit(s) Edit</button>&nbsp;
                    <button class="Button_Standard Button_Delete" id="buttonbulkEditCurrentPlan_Delete" data-bind="click: $root.bulkEditCurrentPlan_Delete">
                        Delete</button>
                </td>
            </tr>
        </table>
        <div id="bulkEditCurrentPlanPanel" class="modalPopup">
            <h3> Update Plan Details</h3>
            <table id="tableUpdatePlanDetails">
                <tr>
                    <td>
                        Off Fleet Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="calCurrentPlanOffFleetDate" runat="server" AutoPostBack="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Off Fleet Location:
                    </td>
                    <td>
                        <select id="bulkEditCurrentPlan_offFleetLocation" class="offFleetLocation">
                    </select>
                    </td>
                </tr>
                <tr>
                    <td>
                        Sale Dealer:
                    </td>
                    <td>
                        <select id="bulkEditCurrentPlan_saleDealer" class="saleDealer">
                        </select>
                    </td>
                </tr>
            </table>
            <div class="divBulkButtonsV1 bulkEditVehicleSalePanelButtons">
                <button class="Button_Standard Button_OK" data-bind="click: $root.bulkEditCurrentPlan_OK">
                    Apply</button>&nbsp;
                <button class="Button_Standard Button_Reset" data-bind="click: $root.bulkEditCurrentPlan_Reset">
                    Reset</button>&nbsp;
                <button class="Button_Standard Button_Cancel" data-bind="click: $root.bulkEditCurrentPlan_Cancel">
                    Cancel</button>
            </div>
        </div>
        <div id="singleEditCurrentPlanPanel" class="modalPopup">
            <h3>
                Edit Plan</h3>
            <table id="tableForUnitNumber">
                <tr>
                    <td>
                        Unit Number:
                    </td>
                    <td>
                        <input type="text" id="singleEditCurrentPlan_unitNumber" disabled="disabled" class="shortTextbox" />
                    </td>
                    <td>
                        Registration Number:
                    </td>
                    <td>
                        <input type="text" id="singleEditCurrentPlan_registrationNumber" disabled="disabled"
                            class="shortTextbox" />
                    </td>
                </tr>
                 <tr>
                    <td>
                        Current Location:
                    </td>
                    <td>
                        <select id="singleEditCurrentPlan_currentLocation">
                        </select>&nbsp;*
                    </td>
                    <td>
                        Actual Location:
                    </td>
                    <td>
                        <select id="singleEditCurrentPlan_actualLocation">
                        </select>&nbsp;*
                    </td>
                </tr>
            </table>
            
            <table id="ProcessStepsTable" class="divResult dataTableColor" cellspacing="0" cellpadding="0" style="width:100%;border-collapse:collapse;">
                <thead>
                    <tr>
                        <th>
                            Process
                        </th>
                        <th>
                            Processed by
                        </th>
                        <th>
                            Processed Date
                        </th>
                        <th>
                            Completed
                        </th>
                    </tr>
                </thead>
                <tbody data-bind="foreach: ProcessedSteps()">
                    <tr>
                        <td>
                            <span data-bind="text: Description"></span>
                        </td>
                        <td>
                            <span data-bind="text: ProcessedBy, attr:{id:$index}"></span>
                        </td>
                        <td>
                            <%--<span data-bind="text: ProcessedDate"></span>--%>
                            <!-- ko if: isFleetManager === true -->
                            <input data-bind="value: ProcessedDate, visible: ShowProcessedDate" style="width:80px"></input>
                            
                            <!-- /ko -->

                            <!-- ko if: isFleetManager !== true -->
                            <span data-bind="text: ProcessedDate"></span>
                            <!-- /ko -->
                            <%--<uc1:DateControl   data-bind="attr: { id: $index }"   runat="server" AutoPostBack="false" />--%>
                        </td>
                        <td>
                            <input type="checkbox" data-bind="checked: Completed, enable: isEnable" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <br />
            <b>Sale Information</b>
            <table id="tableSaleInformation">
                <tr>
                    <td>
                        Date Sold:
                    </td>
                    <td>
                        <uc1:DateControl ID="singleEditCurrentPlan_calDateSold" runat="server" AutoPostBack="false" />
                        &nbsp;*
                    </td>
                    <td>
                        Pickup Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="singleEditCurrentPlan_calPickupDate" runat="server" AutoPostBack="false" />
                        &nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                        Customer Type:
                    </td>
                    <td>
                        <select id="singleEditCurrentPlan_customerType" class="customerType" />&nbsp;*
                    </td>
                    <td>
                        Customer Name:
                    </td>
                    <td>
                        <input id="singleEditCurrentPlan_customerName" />&nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                        Dealer Name:
                    </td>
                    <td>
                        <select id="singleEditCurrentPlan_dealerName" class="saleDealer">
                        </select>&nbsp;*
                        
                    </td>
                </tr>
            </table>
            <br />
            <b>Payment</b>
            <table id="tablePayment">
                <tr>
                    <td>
                        Payment Method:
                    </td>
                    <td>
                        <select id="singleEditCurrentPlan_paymentMethod" class="paymentMethod" />&nbsp;*
                    </td>
                    <td>
                        Bank Date:
                    </td>
                    <td>
                        <uc1:DateControl ID="singleEditCurrentPlan_calBankDate" runat="server" AutoPostBack="false" />
                        &nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                        Settle Location:
                    </td>
                    <td>
                        <select id="singleEditCurrentPlan_settleLocation" class="offFleetLocation">
                        </select>&nbsp;*
                    </td>
                    <td>
                        RRP:
                    </td>
                    <td>
                        <input id="singleEditCurrentPlan_rrp" style="width:50px" maxlength="8" />
                    </td>

                </tr>
            </table>
            <br />
            <table id="tableCommentsAndHistory">
                <tr>
                    <td>
                        Comments:
                    </td>
                    <td>
                        <textarea id="singleEditCurrentPlan_comments" rows="3" cols="96"></textarea>
                    </td>
                </tr>
                <tr>
                    <td>
                        History:
                    </td>
                    <td>
                        <textarea id="singleEditCurrentPlan_history" readonly="readonly" rows="3" cols="96"></textarea>
                    </td>
                </tr>
            </table>
            <div class="divBulkButtons singleEditVehicleSalePanelButtons">
                <button id="butSingleEditCurrentPlan_Save" class="Button_Standard Button_Save" data-bind="click: $root.singleEditCurrentPlan_Save">
                    Save</button>&nbsp;
                <button id="butSingleEditCurrentPlan_OK" class="Button_Standard Button_OK" data-bind="click: $root.singleEditCurrentPlan_OK">
                    Release</button>&nbsp;
                <button class="Button_Standard Button_Reset" id="buttonsingleEditCurrentPlan_Reset" data-bind="click: $root.singleEditCurrentPlan_Reset">
                    Reset</button>&nbsp;
                <button class="Button_Standard Button_Cancel" data-bind="click: $root.singleEditCurrentPlan_Cancel">
                    Cancel</button>
            </div>
        </div>
    </asp:Panel>
    <asp:HiddenField Value="" runat="server" ID="ManufacturerIDhidden" />
    <asp:HiddenField Value="FleetSaleDisposalPlan" runat="server" ID="hiddenPageIdentity" />
    <asp:HiddenField Value="" runat="server" ID="AIMSListenerPath" />
     <script type="text/javascript">
         $(document).ready(function () {
             $("#inputFromUnitNumber").blur(function () {
                 if ($(this).val() != "") {
                     $("#inputToUnitNumber").val($(this).val())
                 }
             });
         });
     </script>
</asp:Content>
