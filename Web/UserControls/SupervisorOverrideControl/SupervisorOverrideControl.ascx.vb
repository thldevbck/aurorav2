Imports Aurora.Common
Imports System.Data

Partial Class UserControls_SupervisorOverrideUserControl
    Inherits AuroraUserControl

    Public Delegate Sub SupervisorOverrideUserControlEventHandler(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal param As String)

    Public Event PostBack As SupervisorOverrideUserControlEventHandler

#Region "REV:MIA Added new button functionality for CreditCard no decryption - changes after releases!!!"
    Public Delegate Sub SupervisorContinueOverrideUserControlEventHandler(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal isContinue As Boolean, ByVal param As String)
    Public Event PostBackContinue As SupervisorContinueOverrideUserControlEventHandler

    Public Property ContinueButtonText() As String
        Get
            Return BtnContinue.Text
        End Get
        Set(ByVal value As String)
            BtnContinue.Text = value
        End Set
    End Property

    Public Property ContinueButtonEnabled() As Boolean
        Get
            Return BtnContinue.Enabled
        End Get
        Set(ByVal value As Boolean)
            BtnContinue.Enabled = value
        End Set
    End Property

    Public Property ContinueButtonVisible() As Boolean
        Get
            Return BtnContinue.Visible
        End Get
        Set(ByVal value As Boolean)
            BtnContinue.Visible = value
            btnContinueOK.Visible = value
            BtnContinue.Enabled = value
            btnContinueOK.Enabled = value

            okButton.Visible = Not value
        End Set
    End Property

    Public WriteOnly Property ContinueButtonWidth() As Integer
        Set(ByVal value As Integer)
            BtnContinue.Width = Unit.Pixel(value)
        End Set
    End Property

    Protected Sub BtnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnContinue.Click
        programmaticSupervisorOverride.Hide()
        RaiseEvent PostBackContinue(Me, False, True, Param)
    End Sub

    Protected Sub btnContinueOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinueOK.Click
        Dim errorMessage As String = ""
        If Not String.IsNullOrEmpty(Role) _
         AndAlso Not Aurora.Common.Service.CheckUser(Login, Password, Role, errorMessage) Then
            Me.errorLabel.Text = Server.HtmlEncode(errorMessage)
            programmaticSupervisorOverride.Show()
        Else
            programmaticSupervisorOverride.Hide()
            RaiseEvent PostBackContinue(Me, True, False, Param)
        End If
    End Sub

#End Region

#Region "REV:MIA March 4,2010 addition of Reason in Supervisor override"

    Private Sub RefreshList()

        Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance

        If (Not String.IsNullOrEmpty(Me.DisplayType)) Then
            If (Me.DisplayType.ToUpper = "REFUND") Then
                Me.ddlReason.AppendDataBoundItems = True
                Me.ddlReason.Items.Add("Please select reason")
            Else
                trReasonOther.Visible = True
                ReasonOtherTextBox.Enabled = True
            End If
        Else
            Me.DisplayType = "PAYMENT"
        End If

        

        Me.ddlReason.DataSource = objEFTPOS.RetrieveEftposReasons(DisplayType)
        Me.ddlReason.DataValueField = "PmtReasonID"
        Me.ddlReason.DataTextField = "PmtReasonText"
        Me.ddlReason.DataBind()
        objEFTPOS = Nothing
        Me.ddlReason.SelectedIndex = -1

    End Sub

    Private _displaytype As String
    Public Property DisplayType As String
        Get
            If (String.IsNullOrEmpty(_displaytype)) Then
                _displaytype = "PAYMENT"
            End If
            Return _displaytype
        End Get
        Set(ByVal value As String)
            _displaytype = value
        End Set
    End Property

    Private _viewreason As Boolean
    Public Property ViewReason() As Boolean
        Get
            Return trReason.Visible
        End Get
        Set(ByVal value As Boolean)
            trReason.Visible = value
        End Set
    End Property

   

    Public WriteOnly Property SetReason() As String
        Set(ByVal value As String)
            Me.ddlReason.SelectedValue = CInt(value)
        End Set
    End Property

    Public ReadOnly Property Reason() As String
        Get
            Return Me.ddlReason.SelectedValue
        End Get
    End Property

    Private _paymenttype As String
    Public Property PaymentType() As String
        Get
            Return _paymenttype
        End Get
        Set(ByVal value As String)
            _paymenttype = value
        End Set
    End Property

    Public ReadOnly Property OthersReasons() As String
        Get
            Return ReasonOtherTextBox.Text.TrimEnd
        End Get
    End Property
#End Region

    Public Property Title() As String
        Get
            Return titleLabel.Text
        End Get
        Set(ByVal value As String)
            titleLabel.Text = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return textLabel.Text
        End Get
        Set(ByVal value As String)
            textLabel.Text = value
        End Set
    End Property

    Public Property Role() As String
        Get
            Return roleHiddenField.Value
        End Get
        Set(ByVal value As String)
            roleHiddenField.Value = value
            roleLabel.Text = "" & value
        End Set
    End Property

    Public ReadOnly Property ErrorMessage() As String
        Get
            Return errorLabel.Text
        End Get
    End Property

    Public ReadOnly Property Login() As String
        Get
            Return logonTextBox.Text
        End Get
    End Property

    Public ReadOnly Property Password() As String
        Get
            Return passwordTextBox.Text
        End Get
    End Property

    Public ReadOnly Property Param() As String
        Get
            Return paramHiddenField.Value
        End Get
    End Property

    Private _btnCancel_AutoPostBack As Boolean = True
    Public Property btnCancel_AutoPostBack() As Boolean
        Get
            Return _btnCancel_AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            _btnCancel_AutoPostBack = value
        End Set
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        programmaticSupervisorOverride.BehaviorID = Me.ClientID & "Behaviour"
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("SupervisorOverrideControl.js") Then
            Me.Page.ClientScript.RegisterClientScriptInclude("SupervisorOverrideControl.js", Me.ResolveUrl("SupervisorOverrideControl.js"))
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("SupervisorOverrideControl.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "SupervisorOverrideControl.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("SupervisorOverrideControl.css") & """ />")
        End If

        If ViewReason And Not Page.IsPostBack Then
            RefreshList()
        Else
            System.Diagnostics.Debug.Write("postback")
        End If
    End Sub

    Public Sub Show(Optional ByVal Text As String = Nothing, Optional ByVal Role As String = Nothing, Optional ByVal ErrorMessage As String = Nothing, Optional ByVal Param As String = Nothing)

        If Not String.IsNullOrEmpty(Text) Then Me.Text = Text
        If Not String.IsNullOrEmpty(Role) Then Me.Role = Role
        If Not String.IsNullOrEmpty(ErrorMessage) Then Me.errorLabel.Text = ErrorMessage Else Me.errorLabel.Text = ""
        If Not String.IsNullOrEmpty(Param) Then Me.paramHiddenField.Value = Param Else Me.paramHiddenField.Value = ""
        Me.logonTextBox.Text = ""
        Me.passwordTextBox.Text = ""

        programmaticSupervisorOverride.Show()
    End Sub

    Public Sub Hide()
        programmaticSupervisorOverride.Hide()
    End Sub

    Protected Sub okButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles okButton.Click

        Dim errorMessage As String = ""
        If Not String.IsNullOrEmpty(Role) _
         AndAlso Not Aurora.Common.Service.CheckUser(Login, Password, Role, errorMessage) Then
            Me.errorLabel.Text = Server.HtmlEncode(errorMessage)
            programmaticSupervisorOverride.Show()
        Else
            programmaticSupervisorOverride.Hide()
            RaiseEvent PostBack(Me, True, Param)
        End If

    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        programmaticSupervisorOverride.Hide()

        RaiseEvent PostBack(Me, False, Param)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        errorTableRow.Style.Add("visibility", IIf(String.IsNullOrEmpty(ErrorMessage), "hidden", "visible"))
        messageTableRow.Style.Add("visibility", IIf(String.IsNullOrEmpty(Text), "hidden", "visible"))

        If btnCancel_AutoPostBack Then
            cancelButton.Attributes.Add("onclick", "return hideConfirmationBoxViaClient('" & programmaticSupervisorOverride.BehaviorID & "');")
        Else
            cancelButton.Attributes.Remove("onclick")
        End If

        ' ''eftpos other list
        Dim item As ListItem = ddlReason.Items.FindByText("Other")
        If Not (item Is Nothing) Then
            trReasonOther.Visible = True
            ReasonOtherTextBox.Enabled = False
        End If
    End Sub
End Class

