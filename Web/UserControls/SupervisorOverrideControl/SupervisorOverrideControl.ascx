<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SupervisorOverrideControl.ascx.vb" Inherits="UserControls_SupervisorOverrideUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:HiddenField ID="paramHiddenField" runat="server" Value="" />
<asp:HiddenField ID="roleHiddenField" runat="server" Value="" />

<asp:Button 
    runat="server" 
    ID="hiddenTargetControlForSupervisorOverride" 
    Style="display:none" />
    
<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="programmaticSupervisorOverride"
    BehaviorID="programmaticSupervisorOverrideBehavior" 
    TargetControlID="hiddenTargetControlForSupervisorOverride"
    PopupControlID="programmaticSupervisorOverridePanel" 
    BackgroundCssClass="modalBackground"
    DropShadow="True" 
    PopupDragHandleControlID="programmaticSupervisorOverrideDragHandle" />

<asp:Panel 
    runat="server" 
    CssClass="modalPopup" 
    ID="programmaticSupervisorOverridePanel" 
    Style="display: none; padding: 10px;" 
    DefaultButton="okButton"
    Width="500px">
    
    <asp:Panel 
        runat="Server" ID="programmaticSupervisorOverrideDragHandle"
        CssClass="modalPopupTitle">
        <asp:Label ID="titleLabel" runat="Server" Text="Supervisor Override Required" />
    </asp:Panel>

    
    <table width="100%">
        <tr>
            <td style="vertical-align:top;" align="left">
                <asp:Image Id="image" runat="Server" ImageUrl="SupervisorOverrideControl.gif" />
            </td>
            <td style="vertical-align:top;" align="left" width="100%">
                <table width="100%">
                    <tr id="messageTableRow" runat="server" style="visibility:hidden" class="supervisorMessageTableRow">
                        <td colspan="2"><asp:Label ID="textLabel" runat="server" /></td>
                    </tr>
                    <tr>
                        <td><label id="logonLabel" for="logonTextBox">Logon:</label></td>
                        <td><asp:TextBox ID="logonTextBox" runat="Server" Width="200px" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td><label id="passwordLabel">Password:</label></td>
                        <td><asp:TextBox ID="passwordTextBox" runat="Server" TextMode="Password" Width="200px" />&nbsp;*</td>
                    </tr>
                    <tr id="trReason" runat="server" visible="false" >
                        <td><label id="ReasonLabel">Reason:</label></td>
                        <td>
                            <asp:DropDownList ID="ddlReason" runat="server"  Width="300px" >
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trReasonOther" runat="server" visible="false" >
                        <td>Other:</td>
                        <td>
                            <asp:TextBox ID="ReasonOtherTextBox" runat="Server" Width="292px" TextMode="MultiLine"  Enabled="false" />&nbsp;*
                        </td>
                    </tr>
                    <tr id="errorTableRow" runat="Server" style="visibility:hidden" class="supervisorMessageTableRow">
                        <td colspan="2"><asp:Image ID="errorImage" runat="server" ImageUrl="../../images/error.gif" />&nbsp;<asp:Label ID="errorLabel" runat="server" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td style="vertical-align: bottom"><asp:Label ID="roleLabel" runat="server" Style="color:Gray; font-family:Verdana; font-size:10px" />&nbsp;</td>
            <td align="right" width="100%">
                
                <asp:Button ID="BtnContinue" runat="server" CssClass="Button_Standard Button_OK"
                    Text="Reverse Payment Only" Visible="False" Enabled="False" Width="200" />
                <asp:Button ID="btnContinueOK" runat="server" CssClass="Button_Standard Button_OK"
                    Text="Ok" Enabled="False" Visible="False" />&nbsp;
                <asp:Button ID="okButton" runat="server" Text="OK" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="cancelButton" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />&nbsp;
            </td>
        </tr>
    </table>

</asp:Panel>
