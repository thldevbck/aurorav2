﻿function showSupervisorOverrideViaClient(id) 
{ 
    var supervisorOverrideBehavior = $find(id);
    supervisorOverrideBehavior.show();
}

function hideSupervisorOverrideViaClient(id) 
{   
    var supervisorOverrideBehavior = $find(id);
    supervisorOverrideBehavior.hide();
    return false;
}
