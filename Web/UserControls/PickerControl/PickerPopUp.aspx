<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PickerPopUp.aspx.vb" Inherits="PickerControl_PickerPopUp" MasterPageFile="~/Include/PopupHeader.master" %> 

<asp:Content runat="Server" ContentPlaceHolderID="StylePlaceHolder">
    <link rel="stylesheet" type="text/css" href="PickerPopup.css" />
</asp:Content>

<asp:Content runat="Server" ContentPlaceHolderID="ScriptPlaceHolder">
    <script type="text/javascript" src="PickerPopup.js"></script>
    <script type="text/javascript">
addEvent (window, "keydown", popup_onkeydown);
    </script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <table cellpadding="0" cellspacing="0" width="100%">
        <!-- start data -->
        <tr>
            <td colspan="2">
                <asp:Table runat="server" ID="headerTable" CellPadding="1" CellSpacing="0" CssClass="popupHeaderTable" Width="100%" EnableViewState="False">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell Width="125px">Code</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Description</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>

                <asp:Panel ID="dataPanel" runat="server" CssClass="dataPanel" Visible="false" EnableViewState="False">
                    <table id="popupTable" cellpadding="1" cellspacing="0" class="popupTable" width="95%" >
                        <asp:Literal ID="popupTableLiteral" runat="server" />
                    </table>
                </asp:Panel>                        

                <asp:Panel ID="warningPanel" runat="server" CssClass="warningPanel" Visible="false" EnableViewState="False">
                    <asp:Label ID="warningLabel" runat="server" Text="Warning" EnableViewState="False" />
                </asp:Panel>                        

                <asp:Panel ID="errorPanel" runat="server" CssClass="errorPanel" Visible="true" EnableViewState="False">
                    <asp:Label ID="errorLabel" runat="server" Text="Error" EnableViewState="False" />
                </asp:Panel>                        

            </td>
        </tr>
        <!-- end data -->
        
        <!-- start footer -->
        <tr>
            <td align="left">
                <asp:Label ID="resultCountLabel" runat="server" Text="" />
            </td>
            <td align="right">
                <input type="button" class="Button_Standard Button_Close" style="margin-right: 5px; margin-top: 5px" onclick="window.close()" value="Close" />
            </td>
        </tr>
        <!-- end footer -->
    </table>
        
    <input type="hidden" id="pickerTextBoxIdHidden" runat="server" value="" />
    <input type="hidden" id="idEnabledHidden" runat="server" value="false" />
    <input type="hidden" id="appendDescHidden" runat="server" value="false" />
    <%--rev:mia Jan. 14 2013 - addition of a field to allow multiselection--%>
    <input type="hidden" id="isMultipleHidden" runat="server" value="false" />
</asp:Content>
