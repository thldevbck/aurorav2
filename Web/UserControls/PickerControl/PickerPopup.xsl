<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<xsl:apply-templates select="/data/POPUP">
			<xsl:sort select="CODE" order="ascending" />
			<xsl:sort select="DESCRIPTION" order="ascending" />
		</xsl:apply-templates>
	</xsl:template>
	<xsl:template match="/data/POPUP">
		<tr onmouseover="popupTableRow_onmouseover(this);" onmouseout="popupTableRow_onmouseout(this);" onclick="popupTableRow_onclick(this);">
			<xsl:if test="position() mod 2 != 1" />
			<xsl:attribute name="class">rowalt</xsl:attribute>
			<td style="display:none" />
			<td width="150px">
				<xsl:value-of select="CODE" />
			</td>
			<td>
				<xsl:value-of select="DESCRIPTION" />
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>