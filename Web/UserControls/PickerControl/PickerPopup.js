function popup_onkeydown(ev)
{
	ev || (ev = window.event);
    var keynum = 0;
    if (ev && ev.keyCode) keynum = ev.keyCode; // IE
    else if (ev && ev.which) keynum = ev.which; // Netscape/Firefox/Opera

    if (keynum == 27)
    {
        window.close();
        return true;
    }
    else
        return false;
}


function popupTableRow_onmouseover(row)
{
    row.style.backgroundColor='#ADF';
}

function popupTableRow_onmouseout(row)
{
    row.style.backgroundColor='';
}

function popupTableRow_onclick(row)
{
    var cells = row.getElementsByTagName('td');
    
    var id = cells[0].innerText ? cells[0].innerText : cells[0].textContent;
    var code = cells[1].innerText ? cells[1].innerText : cells[1].textContent;
    var desc = cells[2].innerText ? cells[2].innerText : cells[2].textContent;
    
    var pickerTextBoxId = document.getElementById('ctl00_ContentPlaceHolder_pickerTextBoxIdHidden').value.trim();
    var idEnabled = /true/i.test(document.getElementById('ctl00_ContentPlaceHolder_idEnabledHidden').value.trim());
    var appendDesc = /true/i.test(document.getElementById('ctl00_ContentPlaceHolder_appendDescHidden').value.trim());
    
    if (window.opener)
    {
        var response = {};
        response.dataId = idEnabled ? id : code;
        response.text = !appendDesc ? code : code + ' - ' + desc;
        window.opener.PickerControl.clientCallbackResult (pickerTextBoxId, response);
        window.opener.document.getElementById(pickerTextBoxId).focus();

        //rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        var isMultipleSelection = /true/i.test(document.getElementById('ctl00_ContentPlaceHolder_isMultipleHidden').value.trim());

        if (isMultipleSelection == false) window.close()
        else window.focus()

        
    }
}
