<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PickerControl.ascx.vb" Inherits="UserControls_PickerControl" %>


<span class="PickerControl">

<asp:HiddenField ID="pickerHidden" runat="server" />

<asp:HiddenField ID="pickerParam1Hidden" runat="server" />
<asp:HiddenField ID="pickerParam2Hidden" runat="server" />
<asp:HiddenField ID="pickerParam3Hidden" runat="server" />
<asp:HiddenField ID="pickerParam4Hidden" runat="server" />
<asp:HiddenField ID="pickerParam5Hidden" runat="server" />

<asp:TextBox ID="pickerTextBox" runat="server" Width="100px" />

<asp:Image ID="pickerImage" runat="server" ImageUrl="~/UserControls/PickerControl/PickerControl.gif" ImageAlign="AbsMiddle" Width="18" Height="15" />

</span>