//rev:mia Jan. 14 2013 - addition of a field to allow multiselection
var isRequestFromPopup = false;
var popupDef = '';

PickerControl = function () {}

PickerControl.setParam = function(pickerTextBoxId, index, value) 
{
    var pickerTextBox = document.getElementById (pickerTextBoxId);
    var idprefix = pickerTextBox.getAttribute('idprefix');

    document.getElementById (idprefix + '_pickerParam' + index + 'Hidden').value = value;
}

PickerControl.getParam = function(pickerTextBoxId, index) 
{
    var pickerTextBox = document.getElementById (pickerTextBoxId);
    var idprefix = pickerTextBox.getAttribute('idprefix');

    return document.getElementById (idprefix + '_pickerParam' + index + 'Hidden').value;
}

PickerControl.getUrl = function (pickerTextBoxId) {
    var pickerTextBox = document.getElementById(pickerTextBoxId);

    var popuptype = pickerTextBox.getAttribute('popuptype');
    var idprefix = pickerTextBox.getAttribute('idprefix');
    var url = pickerTextBox.getAttribute('url');
    var appenddesc = pickerTextBox.getAttribute('appenddesc');
    var caption = pickerTextBox.getAttribute('caption');
    var text = pickerTextBox.value;

    url += "?popuptype=" + escape(popuptype);
    url += "&param1=" + escape(this.getParam(pickerTextBoxId, 1));
    url += "&param2=" + escape(this.getParam(pickerTextBoxId, 2));
    url += "&param3=" + escape(this.getParam(pickerTextBoxId, 3));
    url += "&param4=" + escape(this.getParam(pickerTextBoxId, 4));
    url += "&param5=" + escape(this.getParam(pickerTextBoxId, 5));
    url += "&pickertextboxid=" + escape(pickerTextBoxId);
    url += "&appenddesc=" + escape(appenddesc);
    url += "&caption=" + escape(caption);
    url += '&text=' + escape(text);

    //rev:mia Jan. 14 2013 - addition of a field to allow multiselection
    popupDef = escape(popuptype)
    var MultipleSelection = pickerTextBox.getAttribute('MultipleSelection')
    var MaxReturnRecord = pickerTextBox.getAttribute('MaxReturnRecord')
    if (MultipleSelection == true || MultipleSelection == 'True' ) {
        if (MaxReturnRecord == '') {
            MaxReturnRecord = 2
        }
        else if (parseInt(MaxReturnRecord) > 3)
        {
            MaxReturnRecord = 3
        }

    }
    else {
        if (parseInt(MaxReturnRecord) > 1) {
            MaxReturnRecord = 1
        }
    }
    url += '&MultipleSelection=' + MultipleSelection + '&MaxReturnRecord=' + MaxReturnRecord;

    return url;
}


PickerControl.openPicker = function (pickerTextBoxId) {
    //rev:mia Jan. 14 2013 - addition of a field to allow multiselection
    isRequestFromPopup = true
    window.open (this.getUrl (pickerTextBoxId), 'popup', 'height = 540, width = 500, status = no, toolbar = no, menubar = no, location = no, resizable = yes');
}

PickerControl.pickerTextBox_onkeyup = function (pickerTextBoxId) {
    

    var pickerTextBox = document.getElementById(pickerTextBoxId);
    var idprefix = pickerTextBox.getAttribute('idprefix');

    var text = pickerTextBox.value;
    var oldtext = pickerTextBox.getAttribute('oldvalue');

    var pickerHiddenId = idprefix + '_pickerHidden';
    var pickerHidden = document.getElementById(pickerHiddenId);

    if (text != oldtext) {
        pickerHidden.value = '';
        pickerTextBox.style.textDecoration = 'none';
        pickerTextBox.style.color = '#000';

        pickerTextBox.setAttribute('oldvalue', text);
    }
}

PickerControl.pickerTextBox_onblur = function(pickerTextBoxId) {
    isRequestFromPopup = false

    var pickerTextBox = document.getElementById (pickerTextBoxId);
    var idprefix = pickerTextBox.getAttribute('idprefix');

    var text = pickerTextBox.value;
    var nullable = pickerTextBox.getAttribute('nullable') == 'true';
    var errorMessage = pickerTextBox.getAttribute('errorMessage');
    var messagePanelControlID = pickerTextBox.getAttribute ('messagePanelControlID')

    if (text == '')
    {
        if (!nullable)
            MessagePanelControl.setMessagePanelWarning (messagePanelControlID, errorMessage);
    }
}

PickerControl.pickerTextBox_onchange = function (pickerTextBoxId) {
    //rev:mia Jan. 14 2013 - addition of a field to allow multiselection
    isRequestFromPopup = false

    var pickerTextBox = document.getElementById (pickerTextBoxId);
    var idprefix = pickerTextBox.getAttribute('idprefix');

    var text = pickerTextBox.value;
    var nullable = pickerTextBox.getAttribute('nullable') == 'true';
    var errorMessage = pickerTextBox.getAttribute('errorMessage');
    var messagePanelControlID = pickerTextBox.getAttribute('messagePanelControlID')

    MessagePanelControl.clearMessagePanel(messagePanelControlID);

    if (text == '')
    {
        if (!nullable)
            MessagePanelControl.setMessagePanelWarning (messagePanelControlID, errorMessage);
    }
    else
    {
        pickerTextBox.style.textDecoration = 'underline';
        pickerTextBox.style.color = '#F00';

        this.ajax (pickerTextBoxId, PickerControl.clientCallbackAjax);
    }
}

PickerControl.ajax = function(pickerTextBoxId, callbackFunction)
{
    var request = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("MSXML2.XMLHTTP.3.0");
    request.open("POST", this.getUrl (pickerTextBoxId) + '&ajax=true', true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded"); 
    request.onreadystatechange = function()
    {
        if (request.readyState == 4 && request.status == 200) 
            if (request.responseText)
                callbackFunction(pickerTextBoxId, request.responseText);
    };
    request.send('');
}

PickerControl.clientCallbackResult = function (pickerTextBoxId, result) {
    var pickerTextBox = document.getElementById(pickerTextBoxId);
    var idprefix = pickerTextBox.getAttribute('idprefix');

    var pickerHiddenId = idprefix + '_pickerHidden';
    var pickerHidden = document.getElementById(pickerHiddenId);

    var messagePanelControlID = pickerTextBox.getAttribute('messagePanelControlID')
    var autoPostBack = pickerTextBox.getAttribute('autoPostBack') == 'true';

    if (result && result.dataId) {
        //rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        MultipleSelection = pickerTextBox.getAttribute('MultipleSelection')
        if (MultipleSelection == true || MultipleSelection == 'True') {
            var oldvalue = pickerTextBox.value
            var newvalue
            if (oldvalue.indexOf('-') > 0 || (popupDef == 'MARKETCODE' || popupDef == 'AGENTGROUP')) {
                if (isRequestFromPopup == true) {
                    if (oldvalue.indexOf(result.text) == -1) {
                        newvalue = (oldvalue == '' ? result.text : oldvalue + ',' + result.text)
                    }
                    else {
                        newvalue = oldvalue
                    }
                }
                else {
                    newvalue = (oldvalue == '' ? result.text : result.text)
                }
            }
            else {
                newvalue = (oldvalue == '' ? result.text : result.text)
            }

            pickerTextBox.value = newvalue;
            pickerTextBox.setAttribute('oldvalue', newvalue);

            oldvalue = pickerHidden.value
            if (oldvalue.indexOf(result.dataId) == -1) {
                newvalue = newvalue = (oldvalue == '' ? result.dataId : oldvalue + ',' + result.dataId)
                pickerHidden.value = newvalue;
            }
            else {
                pickerHidden.value = oldvalue
            }


        }
        else {
            pickerTextBox.value = result.text;
            pickerTextBox.setAttribute('oldvalue', result.text);
            pickerHidden.value = result.dataId;
        } //if (MultipleSelection == true || MultipleSelection == 'True') {


        //        pickerTextBox.value = result.text;
        //        pickerTextBox.setAttribute('oldvalue', result.text);
        //        pickerHidden.value = result.dataId;

        pickerTextBox.style.textDecoration = 'none';
        pickerTextBox.style.color = '#000';

        MessagePanelControl.clearMessagePanel(messagePanelControlID);
    }
    else if (result && (result.warningMessage || result.errorMessage)) {
        pickerTextBox.style.textDecoration = 'underline';
        pickerTextBox.style.color = '#F00';
        if (result.errorMessage)
            MessagePanelControl.setMessagePanelError(messagePanelControlID, result.errorMessage);
        else if (result.warningMessage)
            MessagePanelControl.setMessagePanelWarning(messagePanelControlID, result.warningMessage);
    }

    if (autoPostBack)
        setTimeout('__doPostBack(\'' + pickerTextBoxId + '\',\'\')', 0);
}

PickerControl.clientCallbackAjax = function(pickerTextBoxId, response) 
{
    var result;
    eval ("result = " + response);
    
    if (result) PickerControl.clientCallbackResult (pickerTextBoxId, result);
}
