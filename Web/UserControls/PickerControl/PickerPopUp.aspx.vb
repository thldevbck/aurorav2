Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.Reflection
Imports System.Threading
Imports System.Web.Script.Serialization

Imports Aurora.Common.Data
Imports Aurora.Popups.Data

''' <summary>
''' The form shown in the "popup" for the picker control.  Takes the PopupInformation to the popup type specified in the query string and executes
''' GEN_GetPopupData with the supplied parameters.  It then dynamically constructs a stylesheet based on the information in PopupInformation and uses
''' this to build the table data to display.
''' 
''' Its a slightly clumsy way of doing it.  We need the xslt because the Xml returned needs to be sorted.  The Xslt might be better off in a 
''' configuration file, but having it in code means PopupInformation is much easier to inspect and use for other purposes.
''' 
''' If "ajax=true" is specified in the query string, the response is a JSON encoded result.  This is used by the picker to validate the entered code.
''' </summary>
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.General)> _
Partial Class PickerControl_PickerPopUp
    Inherits AuroraPage

    Public Class PopupWarningException
        Inherits Exception

        Public Sub New(ByVal message As String)
            MyBase.New(message)
        End Sub
    End Class

    Public Structure AjaxResult
        Public dataId As String
        Public text As String
        Public warningMessage As String
        Public errorMessage As String
    End Structure

    Public Const TooManyErrorCode As String = "GEN022"

    Public Const PopupTypeName As String = "popuptype"

    Public Const TextName As String = "text"
    Public Const Param1Name As String = "param1"
    Public Const Param2Name As String = "param2"
    Public Const Param3Name As String = "param3"
    Public Const Param4Name As String = "param4"
    Public Const Param5Name As String = "param5"

    Public Const pickerTextBoxIdName As String = "pickertextboxid"

    Public Const CaptionName As String = "caption"
    Public Const AppendDescName As String = "appenddesc"
    Public Const AjaxName As String = "ajax"

    Private _popupType As PopupType = PopupType.Undefined
    Private _popupInformation As PopupInformation

    Private _caption As String
    Private _text As String
    Private _param1 As String
    Private _param2 As String
    Private _param3 As String
    Private _param4 As String
    Private _param5 As String

    Private _appendDesc As Boolean
    Private _ajax As Boolean

    Private _popupXml As XmlDocument
    Private _popupXmlNodes As XmlNodeList

    ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
    Private multipleSelection As Boolean
    Private MaxReturnRecord As String

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If Not String.IsNullOrEmpty(_caption) Then
                Return Server.HtmlEncode(_caption)
            ElseIf _popupInformation IsNot Nothing Then
                Return Server.HtmlEncode("Select " & _popupInformation.Name)
            Else
                Return "Picker"
            End If
        End Get
    End Property

    ''' <summary>
    ''' Load and initialize all the parameters
    ''' </summary>
    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            _popupType = [Enum].Parse(GetType(PopupType), Request.QueryString(PopupTypeName))
        Catch
            Throw New Exception("Invalid Popup Type")
        End Try
        If _popupType = PopupType.Undefined Then
            Throw New Exception("Invalid Popup Type")
        End If

        _popupInformation = PopupInformation.GetInformationByType(_popupType)
        If _popupInformation Is Nothing Then
            Throw New Exception("Invalid Popup Type")
        End If

        _caption = Request.QueryString(CaptionName)

        pickerTextBoxIdHidden.Value = Request.QueryString(pickerTextBoxIdName)
        idEnabledHidden.Value = _popupInformation.IdEnabled.ToString().ToLower()
        Try
            _appendDesc = Boolean.Parse(Request.QueryString(AppendDescName))
        Catch
            _appendDesc = False
        End Try
        appendDescHidden.Value = _appendDesc.ToString().ToLower()


        _text = "" & Request.QueryString(TextName)
        'rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        multipleSelection = Request.QueryString("MultipleSelection")
        MaxReturnRecord = Request.QueryString("MaxReturnRecord")

        isMultipleHidden.Value = multipleSelection

        If Not String.IsNullOrEmpty(_text) AndAlso _appendDesc AndAlso _text.IndexOf("-") <> -1 Then
            If (multipleSelection) Then
                ''some text are appended in the textbox
                Dim aryText() As String = _text.Split(",")
                _text = ""
                For Each item As String In aryText
                    _text = _text & item.Split("-")(0).TrimEnd() & ","
                Next
                _text = _text.Remove(_text.LastIndexOf(","), 1)
            Else
                _text = _text.Split("-")(0).TrimEnd()
            End If

        End If

        _param1 = "" & Request.QueryString(Param1Name)
        _param2 = "" & Request.QueryString(Param2Name)
        _param3 = "" & Request.QueryString(Param3Name)
        _param4 = "" & Request.QueryString(Param4Name)
        _param5 = "" & Request.QueryString(Param5Name)

        
        Try
            _ajax = Boolean.Parse(Request.QueryString(AjaxName))
        Catch
            _ajax = False
        End Try
    End Sub

    Private Sub BuildHeader()
        Dim tableHeaderRow As TableHeaderRow = headerTable.Rows(0)
        tableHeaderRow.Controls.Clear()
        For Each pic As PopupInformationColumn In _popupInformation.AllColumns
            Dim tableHeaderCell As New TableHeaderCell()
            tableHeaderCell.Text = pic.Caption
            tableHeaderCell.Width = pic.Width
            If Not pic.Visible Then tableHeaderCell.Style.Add(HtmlTextWriterStyle.Display, "none")
            tableHeaderCell.HorizontalAlign = pic.HorizontalAlign
            tableHeaderRow.Controls.Add(tableHeaderCell)
        Next
    End Sub

    Private Shared _xsltCache As New Dictionary(Of PopupType, XslCompiledTransform)
    Private Function BuildXsltStyleSheet() As XslCompiledTransform
        Monitor.Enter(_xsltCache)
        Try
            If _xsltCache.ContainsKey(_popupType) Then Return _xsltCache(_popupType)
        Finally
            Monitor.Exit(_xsltCache)
        End Try

        ' create xslt stylesheet
        Dim xsltDoc As New XmlDocument()

        '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
        Dim styleSheetElement As XmlElement = xsltDoc.CreateElement("xsl", "stylesheet", "http://www.w3.org/1999/XSL/Transform")
        styleSheetElement.SetAttribute("version", "1.0")
        xsltDoc.AppendChild(styleSheetElement)

        '  <xsl:template match="/">
        Dim sortTemplateElement As XmlElement = xsltDoc.CreateElement("xsl", "template", "http://www.w3.org/1999/XSL/Transform")
        sortTemplateElement.SetAttribute("match", "/")
        styleSheetElement.AppendChild(sortTemplateElement)

        '    <xsl:apply-templates select="/data/POPUP">
        Dim applyTemplatesElement As XmlElement = xsltDoc.CreateElement("xsl", "apply-templates", "http://www.w3.org/1999/XSL/Transform")
        applyTemplatesElement.SetAttribute("select", _popupInformation.DataXPath)
        sortTemplateElement.AppendChild(applyTemplatesElement)

        '      <xsl:sort select="CODE" order="ascending"/>
        Dim sortCodeElement As XmlElement = xsltDoc.CreateElement("xsl", "sort", "http://www.w3.org/1999/XSL/Transform")
        sortCodeElement.SetAttribute("select", _popupInformation.CodeColumn.XPath)
        sortCodeElement.SetAttribute("order", "ascending")
        applyTemplatesElement.AppendChild(sortCodeElement)

        '      <xsl:sort select="DESC" order="ascending"/>
        If _popupInformation.DescColumn IsNot Nothing AndAlso _popupInformation.DescColumn.XPath IsNot Nothing Then
            Dim sortDescElement As XmlElement = xsltDoc.CreateElement("xsl", "sort", "http://www.w3.org/1999/XSL/Transform")
            sortDescElement.SetAttribute("select", _popupInformation.DescColumn.XPath)
            sortDescElement.SetAttribute("order", "ascending")
            applyTemplatesElement.AppendChild(sortDescElement)
        End If

        '  <xsl:template match="/data/POPUP">
        Dim templateElement As XmlElement = xsltDoc.CreateElement("xsl", "template", "http://www.w3.org/1999/XSL/Transform")
        templateElement.SetAttribute("match", _popupInformation.DataXPath)
        styleSheetElement.AppendChild(templateElement)

        '    <tr onmouseover="popupTableRow_onmouseover(this);" onmouseout="popupTableRow_onmouseout(this);" onclick="popupTableRow_onclick(this);">
        Dim trElement As XmlElement = xsltDoc.CreateElement("tr")
        trElement.SetAttribute("onmouseover", "popupTableRow_onmouseover(this);")
        trElement.SetAttribute("onmouseout", "popupTableRow_onmouseout(this);")
        trElement.SetAttribute("onclick", "popupTableRow_onclick(this);")
        templateElement.AppendChild(trElement)

        '      <xsl:if test="position() mod 2 != 1">
        Dim trIfElement As XmlElement = xsltDoc.CreateElement("xsl", "if", "http://www.w3.org/1999/XSL/Transform")
        trIfElement.SetAttribute("test", "position() mod 2 != 1")
        trElement.AppendChild(trIfElement)

        '        <xsl:attribute name="class">rowalt</xsl:attribute>
        Dim trAttributeElement As XmlElement = xsltDoc.CreateElement("xsl", "attribute", "http://www.w3.org/1999/XSL/Transform")
        trAttributeElement.SetAttribute("name", "class")
        trAttributeElement.InnerText = "rowalt"
        trIfElement.AppendChild(trAttributeElement)

        '	       <td><xsl:value-of select="CODE" />
        For Each pic As PopupInformationColumn In _popupInformation.AllColumns
            Dim tdElement As XmlElement = xsltDoc.CreateElement("td")
            If pic.Width <> Unit.Empty Then tdElement.SetAttribute("width", pic.Width.ToString())
            If pic.HorizontalAlign <> HorizontalAlign.NotSet Then tdElement.SetAttribute("align", pic.HorizontalAlign.ToString())
            If Not pic.Visible Then tdElement.SetAttribute("style", "display:none")
            trElement.AppendChild(tdElement)
            If pic.XPath IsNot Nothing Then
                Dim valueOfElement As XmlElement = xsltDoc.CreateElement("xsl", "value-of", "http://www.w3.org/1999/XSL/Transform")
                valueOfElement.SetAttribute("select", pic.XPath)
                tdElement.AppendChild(valueOfElement)
            End If
        Next

        ' load the xslt stylesheet
        Dim xslt As New XslCompiledTransform
        Using xmlReader As New XmlNodeReader(xsltDoc)
            xslt.Load(xmlReader)
        End Using

        ' cache it
        Monitor.Enter(_xsltCache)
        Try
            If Not _xsltCache.ContainsKey(_popupType) Then _xsltCache.Add(_popupType, xslt)
        Finally
            Monitor.Exit(_xsltCache)
        End Try

        Return xslt
    End Function


    Private Sub BuildResults()
        _popupXml = _popupInformation.GetPopUpData(_param1, _param2, _param3, _param4, _param5, Me.UserCode, _text)
        Dim errorStatus As String = Nothing
        Dim errorCode As String = Nothing
        Dim errorDesc As String = Nothing
        If HasSqlXmlSPDocError(_popupXml, errorStatus, errorCode, errorDesc) Then
            If errorCode = TooManyErrorCode Then
                Throw New PopupWarningException("Found too many " & _popupInformation.PluralName)
            ElseIf errorDesc IsNot Nothing Then
                Throw New Exception(errorDesc)
            Else
                Throw New Exception("Error loading popup data")
            End If
        End If

        _popupXmlNodes = _popupXml.SelectNodes(_popupInformation.DataXPath)
        If _popupXmlNodes.Count = 0 Then Throw New PopupWarningException("Found no " & _popupInformation.PluralName)

        Dim xslt As XslCompiledTransform = BuildXsltStyleSheet()

        ' do the transform
        Using writer As StringWriter = New StringWriter
            Using xmlReader As XmlReader = New XmlNodeReader(_popupXml)
                Using xmlWriter As XmlWriter = New XmlTextWriter(writer)
                    xslt.Transform(xmlReader, xmlWriter)
                End Using
            End Using

            popupTableLiteral.Text = writer.ToString()
        End Using

        resultCountLabel.Text = _popupXmlNodes.Count.ToString & " result" & IIf(_popupXmlNodes.Count > 1, "s", "") & " found"
    End Sub

    Private Sub LoadForm()
        Try
            BuildHeader()
            BuildResults()

            dataPanel.Visible = True
            warningPanel.Visible = False
            errorPanel.Visible = False
        Catch ex As PopupWarningException

            dataPanel.Visible = False
            errorPanel.Visible = False

            warningPanel.Visible = True
            warningLabel.Text = Server.HtmlEncode(ex.Message)

        Catch ex As Exception
            dataPanel.Visible = False
            warningPanel.Visible = False

            errorPanel.Visible = True
            errorLabel.Text = Server.HtmlEncode(ex.Message)
        End Try
    End Sub

    Private Sub LoadAjaxCheck()
        Response.ContentType = "text/plain"
        Dim result As New AjaxResult
        Try
            ' load from database
            _popupXml = _popupInformation.GetPopUpData(_param1, _param2, _param3, _param4, _param5, Me.UserCode, _text)
            Dim errorStatus As String = Nothing
            Dim errorCode As String = Nothing
            Dim errorDesc As String = Nothing
            If HasSqlXmlSPDocError(_popupXml, errorStatus, errorCode, errorDesc) Then
                If errorCode = TooManyErrorCode Then
                    Throw New PopupWarningException("Found too many " & _popupInformation.PluralName & ", refine your selection")
                ElseIf errorDesc IsNot Nothing Then
                    Throw New PopupWarningException(errorDesc)
                Else
                    Throw New Exception("Error loading popup data")
                End If
            End If

            _popupXmlNodes = _popupXml.SelectNodes(_popupInformation.DataXPath)

            Dim node As XmlNode = Nothing
            Dim count As Integer = 0
            For Each node0 As XmlNode In _popupXmlNodes
                Dim code As String = node0.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText
                If code.ToLower() = _text.ToLower() Then
                    node = node0

                    If (multipleSelection = False) Then
                        count = 1
                    Else
                        count = _popupXmlNodes.Count - 1
                    End If


                    Exit For
                    ' fix by Shoel - to prevent incorrect selection of codes
                    'ElseIf code.ToLower().Length > _text.ToLower().Length _
                    ' AndAlso code.Substring(0, _text.ToLower().Length).ToLower() = _text.ToLower() Then
                    '    If node Is Nothing Then node = node0
                    '    count += 1
                    ' fix by Shoel - to prevent incorrect selection of codes
                End If
            Next

            ' fix by shoel for CAMPERBOERSE issue
            If _popupXmlNodes.Count = 1 And count = 0 Then
                ' theres just one result, and its not selected
                node = _popupXmlNodes(0)
                count = 1
            End If

            ' tell the UI how many matches are found
            If count = 0 Then
                count = _popupXmlNodes.Count
            End If
            ' fix by shoel for CAMPERBOERSE issue

            If count = 0 Then
                Throw New PopupWarningException("Found no match for " & _popupInformation.Name & ", refine your selection")
            ElseIf count > 1 Then
                '' 'rev:mia Jan. 14 2013 - addition of a field to allow multiselection
                If (multipleSelection = False) Then
                    Throw New PopupWarningException("Found too many " & _popupInformation.PluralName & ", refine your selection")
                Else
                    If (count > MaxReturnRecord) Then
                        Throw New PopupWarningException("Found too many " & _popupInformation.PluralName & ", refine your selection or select the popup")
                    End If
                    ''do nothing
                End If

            End If

            '' 'rev:mia Jan. 14 2013 - addition of a field to allow multiselection
            If (multipleSelection = False) Then
                If _popupInformation.IdEnabled Then
                    result.dataId = node.SelectSingleNode(_popupInformation.IdColumn.XPath).InnerText
                Else
                    result.dataId = node.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText
                End If

                If _appendDesc Then
                    result.text = node.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText & " - " & node.SelectSingleNode(_popupInformation.DescColumn.XPath).InnerText
                Else
                    result.text = node.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText
                End If
            Else
                '' 'rev:mia Jan. 14 2013 - addition of a field to allow multiselection
                Dim sbDataid As New StringBuilder
                Dim sbText As New StringBuilder
                _popupXmlNodes = _popupXml.SelectNodes(_popupInformation.DataXPath)

                Dim ctr As Integer = 1
                For Each node0 As XmlNode In _popupXmlNodes
                    If ctr > MaxReturnRecord Then Exit For

                    If _popupInformation.IdEnabled Then
                        sbDataid.Append(node0.SelectSingleNode(_popupInformation.IdColumn.XPath).InnerText & ",")
                    Else
                        sbDataid.Append(node0.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText & ",")
                    End If

                    If _appendDesc Then
                        sbText.Append(node0.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText & " - " & node0.SelectSingleNode(_popupInformation.DescColumn.XPath).InnerText & ",")
                    Else
                        sbText.Append(node0.SelectSingleNode(_popupInformation.CodeColumn.XPath).InnerText & ",")
                    End If

                    ctr = ctr + 1
                Next

                If (sbDataid.ToString.EndsWith(",") = True) Then
                    result.dataId = sbDataid.ToString.Remove(sbDataid.ToString.LastIndexOf(","), 1)
                Else
                    result.dataId = sbDataid.ToString
                End If

                If (sbText.ToString.EndsWith(",") = True) Then
                    result.text = sbText.ToString.Remove(sbText.ToString.LastIndexOf(","), 1)
                Else
                    result.text = sbText.ToString
                End If

            End If
            

            result.warningMessage = Nothing
            result.errorMessage = Nothing

        Catch ex As PopupWarningException
            result.dataId = Nothing
            result.text = Nothing
            result.warningMessage = ex.Message
            result.errorMessage = Nothing
        Catch ex As Exception
            result.dataId = Nothing
            result.text = Nothing
            result.warningMessage = Nothing
            result.errorMessage = "Error validating " & _popupInformation.Name
        End Try

        Dim js As New JavaScriptSerializer()
        Response.Write(js.Serialize(result))
        Response.End()
    End Sub

    ''' <summary>
    ''' Load the form, showing a warning or error if any exceptions were thrown
    ''' </summary>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not _ajax Then
            LoadForm()
        Else
            LoadAjaxCheck()
        End If
    End Sub
End Class
