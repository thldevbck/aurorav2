Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.IO
Imports System.Web
Imports System.Xml
Imports System.Xml.XPath
Imports System.Xml.Xsl
Imports System.Web.Script.Serialization
Imports Aurora.Common
Imports Aurora.Popups.Data

Partial Class UserControls_PickerControl
    Inherits AuroraUserControl

    Public Property DataId() As String
        Get
            If String.IsNullOrEmpty(pickerHidden.Value) AndAlso Not String.IsNullOrEmpty(Text) Then
                Dim text As String = Me.Text
                If AppendDescription AndAlso text.IndexOf("-") <> -1 Then
                    text = text.Split("-")(0).TrimEnd()
                End If
                pickerHidden.Value = "" & CachedPopupInformation.ValidatePopUpDataText(Param1, Param2, Param3, Param4, Param5, Me.CurrentPage.UserCode, text)
            End If
            Return pickerHidden.Value
        End Get
        Set(ByVal value As String)
            pickerHidden.Value = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return pickerTextBox.Text
        End Get
        Set(ByVal value As String)
            pickerTextBox.Text = value
        End Set
    End Property

    Public ReadOnly Property IsValid() As Boolean
        Get
            Return Not String.IsNullOrEmpty(DataId) OrElse (Nullable AndAlso String.IsNullOrEmpty(Text.Trim()))
        End Get
    End Property

    Public ReadOnly Property ErrorMessage() As String
        Get
            If IsValid Then
                Return ""
            Else
                Return "Specify a valid " & CachedPopupInformation.Name
            End If
        End Get
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("AutoPostBack")) Then
                Return CBool(ViewState("AutoPostBack"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("AutoPostBack") = value
        End Set
    End Property

    Public Property AppendDescription() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("AppendDescription")) Then
                Return CBool(ViewState("AppendDescription"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("AppendDescription") = value
        End Set
    End Property

    Public Property Nullable() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("Nullable")) Then
                Return CBool(ViewState("Nullable"))
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Nullable") = value
        End Set
    End Property

    Public Property [ReadOnly]() As Boolean
        Get
            Return pickerTextBox.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            pickerTextBox.ReadOnly = value
        End Set
    End Property

    Public Property Width() As Unit
        Get
            Return pickerTextBox.Width
        End Get
        Set(ByVal value As Unit)
            pickerTextBox.Width = value
        End Set
    End Property

    Public Property PopupType() As PopupType
        Get
            Try
                If String.IsNullOrEmpty(ViewState("PopupType")) Then
                    Return PopupType.PRODUCT
                Else
                    Return [Enum].Parse(GetType(PopupType), ViewState("PopupType"))
                End If
            Catch
                Return PopupType.PRODUCT
            End Try
        End Get
        Set(ByVal value As PopupType)
            ViewState("PopupType") = value.ToString()
        End Set
    End Property

    Private _popupInformation As PopupInformation = Nothing
    Private ReadOnly Property CachedPopupInformation() As PopupInformation
        Get
            If _popupInformation Is Nothing OrElse _popupInformation.PopupType <> PopupType Then
                _popupInformation = PopupInformation.GetInformationByType(PopupType)
            End If
            Return _popupInformation
        End Get
    End Property

    Public Property Param1() As String
        Get
            Return pickerParam1Hidden.Value
        End Get
        Set(ByVal value As String)
            pickerParam1Hidden.Value = value
        End Set
    End Property

    Public Property Param2() As String
        Get
            Return pickerParam2Hidden.Value
        End Get
        Set(ByVal value As String)
            pickerParam2Hidden.Value = value
        End Set
    End Property

    Public Property Param3() As String
        Get
            Return pickerParam3Hidden.Value
        End Get
        Set(ByVal value As String)
            pickerParam3Hidden.Value = value
        End Set
    End Property

    Public Property Param4() As String
        Get
            Return pickerParam4Hidden.Value
        End Get
        Set(ByVal value As String)
            pickerParam4Hidden.Value = value
        End Set
    End Property

    Public Property Param5() As String
        Get
            Return pickerParam5Hidden.Value
        End Get
        Set(ByVal value As String)
            pickerParam5Hidden.Value = value
        End Set
    End Property

    Public Property Caption() As String
        Get
            Return ViewState("Caption")
        End Get
        Set(ByVal value As String)
            ViewState("Caption") = value
        End Set
    End Property

    Public Property MessagePanelControlID() As String
        Get
            Return "" & ViewState("MessagePanelControlID")
        End Get
        Set(ByVal value As String)
            ViewState("MessagePanelControlID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("PickerControl.js") Then
            Me.Page.ClientScript.RegisterClientScriptInclude("PickerControl.js", Me.ResolveUrl("PickerControl.js"))
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("PickerControl.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "PickerControl.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("PickerControl.css") & """ />")
        End If


        
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        pickerTextBox.Attributes.Add("popuptype", Me.PopupType.ToString())
        pickerTextBox.Attributes.Add("url", "" & Me.ResolveUrl("PickerPopUp.aspx"))
        pickerTextBox.Attributes.Add("caption", "" & Me.Caption)
        pickerTextBox.Attributes.Add("idprefix", "" & MyBase.ClientID)
        pickerTextBox.Attributes.Add("appenddesc", "" & Me.AppendDescription.ToString().ToLower())
        pickerTextBox.Attributes.Add("nullable", Me.Nullable.ToString().ToLower())
        pickerTextBox.Attributes.Add("autoPostBack", Me.AutoPostBack.ToString().ToLower())
        pickerTextBox.Attributes.Add("oldvalue", pickerTextBox.Text)

        ''rev:mia Jan. 14 2013 - addition of a field to allow multiselection
        pickerTextBox.Attributes.Add("MultipleSelection", AllowedMultipleSelection)
        pickerTextBox.Attributes.Add("MaxReturnRecord", MaxReturnRecord)

        pickerTextBox.Attributes.Add("onchange", "PickerControl.pickerTextBox_onchange('" & Me.ClientID & "')")
        pickerTextBox.Attributes.Add("onkeyup", "PickerControl.pickerTextBox_onkeyup('" & Me.ClientID & "')")
        If Not Nullable Then pickerTextBox.Attributes.Add("onblur", "PickerControl.pickerTextBox_onblur('" & Me.ClientID & "')")
        If Not Nullable Then pickerTextBox.Attributes.Add("errorMessage", IIf(Not String.IsNullOrEmpty(Caption), Caption, CachedPopupInformation.Name) + " must be specified")

        pickerTextBox.Style.Add(HtmlTextWriterStyle.TextDecoration, IIf(String.IsNullOrEmpty(DataId) And Not String.IsNullOrEmpty(Text), "underline", "none"))
        pickerTextBox.Style.Add(HtmlTextWriterStyle.Color, IIf(String.IsNullOrEmpty(DataId) And Not String.IsNullOrEmpty(Text), "#F00", "#000"))
        pickerTextBox.ToolTip = IIf(Not String.IsNullOrEmpty(Caption), Caption, CachedPopupInformation.Name)

        Dim c As Control = Me.Parent.FindControl(MessagePanelControlID)
        If c IsNot Nothing Then
            pickerTextBox.Attributes.Add("messagePanelControlID", c.ClientID)
        Else
            pickerTextBox.Attributes.Add("messagePanelControlID", "")
        End If

        pickerImage.Attributes.Add("onclick", "PickerControl.openPicker('" & Me.ClientID & "')")
        pickerImage.Style.Add("display", IIf(Not Me.ReadOnly, "", "none"))
        pickerImage.ToolTip = IIf(Not String.IsNullOrEmpty(Caption), Caption, CachedPopupInformation.Name)
    End Sub

    Public Overrides ReadOnly Property ClientID() As String
        Get
            Return pickerTextBox.ClientID
        End Get
    End Property

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Protected Sub pickerTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pickerTextBox.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub


#Region "rev:mia Jan. 14 2013 - addition of a field to allow multiselection"

    Public Property AllowedMultipleSelection As Boolean
        Get
            Return ViewState("AllowedMultipleSelection")
        End Get
        Set(value As Boolean)
            ViewState("AllowedMultipleSelection") = value
        End Set
    End Property

    Public Property MaxReturnRecord As String
        Get
            Return ViewState("MaxReturnRecord")
        End Get
        Set(value As String)

            If (String.IsNullOrEmpty(value)) Then
                value = IIf(AllowedMultipleSelection = True, 2, 1)
            Else
                value = IIf(AllowedMultipleSelection = True, value, 1)
            End If
            
            ViewState("MaxReturnRecord") = value
        End Set
    End Property
#End Region


End Class
