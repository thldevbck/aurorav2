Imports System.Xml
'' Change log
'' 9.6.8 - Shoel - Z:index fixed so this thingie now pops up and above the modal popup!

''' <summary>
''' DateControl has a Date (MinValue if no value), Readonly, and Nullable property (date can be set to no value)
''' </summary>
''' <remarks></remarks>
Partial Class UserControls_DateControl
    '' Inherits System.Web.UI.UserControl
    Inherits AuroraUserControl

    ''' <summary>
    ''' The default date format, based on the user settings
    ''' </summary>
    Public Shared ReadOnly Property DefaultDateFormat() As String
        Get
            Return Aurora.Common.UserSettings.Current.ComDateFormat
        End Get
    End Property

    ''' <summary>
    ''' The default date format used by the javascript, converted from the .NET default date format
    ''' </summary>
    Public Shared ReadOnly Property DefaultJavascriptDateFormat() As String
        Get
            Dim result As String = DefaultDateFormat

            If result.IndexOf("ddd") <> -1 Then
                result = result.Replace("ddd", "d")
            ElseIf result.IndexOf("dd") <> -1 Then
                result = result.Replace("dd", "d")
            ElseIf result.IndexOf("d") <> -1 Then
                result = result.Replace("d", "d")
            End If

            If result.IndexOf("MMM") <> -1 Then
                result = result.Replace("MMM", "M")
            ElseIf result.IndexOf("MM") <> -1 Then
                result = result.Replace("MM", "m")
            ElseIf result.IndexOf("M") <> -1 Then
                result = result.Replace("M", "m")
            End If

            If result.IndexOf("yyyy") <> -1 Then
                result = result.Replace("yyyy", "Y")
            ElseIf result.IndexOf("yyy") <> -1 Then
                result = result.Replace("yyy", "Y")
            ElseIf result.IndexOf("yy") <> -1 Then
                result = result.Replace("yy", "y")
            ElseIf result.IndexOf("y") <> -1 Then
                result = result.Replace("y", "y")
            End If

            Return result
        End Get
    End Property

    Private _dateFormat As String = Nothing
    Public Property DateFormat() As String
        Get
            If _dateFormat Is Nothing Then
                _dateFormat = DefaultDateFormat
            End If
            Return _dateFormat
        End Get
        Set(ByVal value As String)
            _dateFormat = value
        End Set
    End Property

    Private _javascriptDateFormat As String = Nothing
    Public Property JavascriptDateFormat() As String
        Get
            If _javascriptDateFormat Is Nothing Then
                _javascriptDateFormat = DefaultJavascriptDateFormat
            End If
            Return _javascriptDateFormat
        End Get
        Set(ByVal value As String)
            _javascriptDateFormat = value
        End Set
    End Property

    ''' <summary>
    ''' The text value
    ''' </summary>
    Public Property Text() As String
        Get
            Return dateTextBox.Text
        End Get
        Set(ByVal value As String)
            dateTextBox.Text = value
        End Set
    End Property

    Public ReadOnly Property IsValid() As Boolean
        Get
            Return (Text.Trim() = "" AndAlso Nullable) OrElse Me.Date <> Date.MinValue
        End Get
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("AutoPostBack")) Then
                Return CBool(ViewState("AutoPostBack"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("AutoPostBack") = value
        End Set
    End Property

    Public Property MessagePanelControlID() As String
        Get
            Return "" & ViewState("MessagePanelControlID")
        End Get
        Set(ByVal value As String)
            ViewState("MessagePanelControlID") = value
        End Set
    End Property

    ''' <summary>
    ''' The date value.  Date.MinValue if empty
    ''' </summary>
    Public Property [Date]() As Date
        Get
            Try
                If String.IsNullOrEmpty(dateTextBox.Text.Trim()) Then
                    Return Date.MinValue
                Else
                    Return Date.ParseExact(dateTextBox.Text, DateFormat, System.Globalization.CultureInfo.CurrentCulture)
                End If
            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Get
        Set(ByVal value As DateTime)
            If value <> Date.MinValue Then
                dateTextBox.Text = value.ToString(DateFormat)
            Else
                dateTextBox.Text = ""
            End If
        End Set
    End Property

    Public Property [ReadOnly]() As Boolean
        Get
            Return dateTextBox.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            dateTextBox.ReadOnly = value
        End Set
    End Property

    Public Property TabIndex() As Short
        Get
            Return dateTextBox.TabIndex
        End Get
        Set(ByVal value As Short)
            dateTextBox.TabIndex = value
        End Set
    End Property

    Public Property Nullable() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("Nullable")) Then
                Return CBool(ViewState("Nullable"))
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Nullable") = value.ToString()
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return dateTextBox.Enabled
        End Get
        Set(ByVal value As Boolean)
            dateTextBox.Enabled = value
        End Set
    End Property

    Public Property CssClass() As String
        Get
            Return dateTextBox.CssClass
        End Get
        Set(ByVal value As String)
            dateTextBox.CssClass = value
        End Set
    End Property

    Public ReadOnly Property Style() As CssStyleCollection
        Get
            Return dateTextBox.Style
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        For Each scriptFileName As String In New String() {"DateControl.js", "CalendarPopup.js"}
            If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered(scriptFileName) Then
                Me.Page.ClientScript.RegisterClientScriptInclude(scriptFileName, Me.ResolveUrl(scriptFileName))
            End If
        Next

        For Each styleFileName As String In New String() {"DateControl.css", "CalendarPopup.css"}
            If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(styleFileName) Then
                Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), styleFileName, "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl(styleFileName) & """ />")
            End If
        Next

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("CalendarInlineScript") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CalendarInlineScript", "<DIV ID='PopupCalendarDiv' STYLE='position:absolute;visibility:hidden;background-color:white;layer-background-color:white;z-index:100002'></DIV><SCRIPT LANGUAGE='JavaScript' ID='jscal1x'>var cal1x = new CalendarPopup('PopupCalendarDiv');cal1x.setCssPrefix('CalendarPopup');</SCRIPT>")
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        dateTextBox.MaxLength = New Date(2000, 12, 12).ToString(Aurora.Common.UserSettings.Current.ComDateFormat).Length
        dateTextBox.Attributes.Add("onkeyup", "DateControl.dateTextBox_onkeyup('" & dateTextBox.ClientID & "')")
        If String.IsNullOrEmpty(Me.Attributes("onchange")) Then
            dateTextBox.Attributes.Add("onchange", "DateControl.dateTextBox_onchange('" & dateTextBox.ClientID & "')")
        Else
            dateTextBox.Attributes.Add("onchange", "DateControl.dateTextBox_onchange('" & dateTextBox.ClientID & "');" & Me.Attributes("onchange"))
        End If
        dateTextBox.Attributes.Add("dateFormat", Me.DateFormat)
        dateTextBox.Attributes.Add("javascriptDateFormat", Me.JavascriptDateFormat)
        dateTextBox.Attributes.Add("nullable", Me.Nullable.ToString().ToLower())
        dateTextBox.Attributes.Add("autoPostBack", Me.AutoPostBack.ToString().ToLower())
        If Me.ReadOnly OrElse Not Me.Enabled Then
            dateTextBox.CssClass = "inputreadonly"
        Else
            dateTextBox.CssClass = ""
        End If

        If Me.Enabled And Not Me.ReadOnly Then dateImage.Attributes.Add("onclick", "cal1x.select(document.getElementById('" & dateTextBox.ClientID & "'),'" & dateImage.ClientID & "','" & Me.JavascriptDateFormat & "'); return false;")

        Dim c As Control = Me.Parent.FindControl(MessagePanelControlID)
        If c IsNot Nothing Then
            dateTextBox.Attributes.Add("messagePanelControlID", c.ClientID)
        Else
            dateTextBox.Attributes.Add("messagePanelControlID", "")
        End If

        If Not String.IsNullOrEmpty(Text.Trim()) AndAlso Me.Date = Date.MinValue Then
            dateTextBox.Style.Add("text-decoration", "underline")
            dateTextBox.Style.Add("color", "#F00")
        Else
            dateTextBox.Style.Add("text-decoration", "none")
            dateTextBox.Style.Add("color", "#000")
        End If
    End Sub

    Public Overrides ReadOnly Property ClientID() As String
        Get
            Return dateTextBox.ClientID
        End Get
    End Property

    Public Event DateChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Protected Sub dateTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dateTextBox.TextChanged
        RaiseEvent DateChanged(sender, e)
    End Sub

End Class
