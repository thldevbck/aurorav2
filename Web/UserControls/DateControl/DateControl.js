DateControl = function () 
{
}

DateControl.dateTextBox_onkeyup = function(dateTextBoxId) 
{
	var dateTextBox = document.getElementById (dateTextBoxId);
    var text = dateTextBox.value;
    var oldtext = "" + dateTextBox.getAttribute('oldvalue');

    if (!oldtext || (text != oldtext))
    {
        dateTextBox.style.textDecoration = 'none';
        dateTextBox.style.color = '#000';
    }
}

DateControl.dateTextBox_onchange = function(dateTextBoxId) 
{
	var dateTextBox = document.getElementById (dateTextBoxId);
	var dateFormat = dateTextBox.getAttribute ('dateFormat');
	var javascriptDateFormat = dateTextBox.getAttribute ('javascriptDateFormat');
	var nullable = dateTextBox.getAttribute ('nullable') == 'true';
    var autoPostBack = dateTextBox.getAttribute('autoPostBack') == 'true';
	var messagePanelControlID = dateTextBox.getAttribute ('messagePanelControlID');
    var text = dateTextBox.value.trim();
    var oldtext = "" + dateTextBox.getAttribute('oldvalue');
    
   //Issue: same value enteres was not properly converted into correct date format.
   // if (text == oldtext) return; 

    dateTextBox.setAttribute ('oldvalue', text);

    if ((text != '') || !nullable)
    {
        var text = this.validateDateString (text, javascriptDateFormat); 
        if (text) 
        {
            dateTextBox.value = text;
            MessagePanelControl.clearMessagePanel(messagePanelControlID);
            dateTextBox.style.textDecoration = 'none';
            dateTextBox.style.color = '#000';
        }
        else
        {
            MessagePanelControl.setMessagePanelWarning (messagePanelControlID, 'Enter date in ' + dateFormat + ' format');
            dateTextBox.style.textDecoration = 'underline';
            dateTextBox.style.color = '#F00';
        }
    }
    else if (!nullable)
    {
        MessagePanelControl.setMessagePanelWarning (messagePanelControlID, 'Enter a date');
        dateTextBox.style.textDecoration = 'none';
        dateTextBox.style.color = '#000';
    }
    else
    {
        dateTextBox.value = '';
        MessagePanelControl.clearMessagePanel(messagePanelControlID);
        dateTextBox.style.textDecoration = 'none';
        dateTextBox.style.color = '#000';
    }

    if (autoPostBack)
        setTimeout('__doPostBack(\'' + dateTextBoxId + '\',\'\')', 0);
};

DateControl.validateDateString = function (text, javascriptDateFormat)
{
    var date = this.convert68CharDate (text); // try parsing a 6 or 8 char date first
    if (!date) date = Date.parseDate (text, javascriptDateFormat); // otherwise use the standard date format
    if (date)
        return date.dateFormat(javascriptDateFormat);
    else
        return null;
}

// Convert a 6 or digit date (ddmmyy or ddmmyyyy) into a Date object.  Returns null if invalid.
DateControl.convert68CharDate = function (str)
{
    if (!str) 
        return null;
        
    // trim and length check
    str = ('' + str).trim();
    if ((str.length != 6) && (str.length != 8))
        return null;
        
    // check for valid chars
    var validChars = "0123456789";
    for (var i = 0; i < str.length; i++)
        if (validChars.indexOf(str.charAt(i)) == -1)
            return null;
      
    // break it up into constituent parts
    var day = parseInt (str.substring(0,2), 10);
    var month = parseInt (str.substring(2,4), 10) - 1;

    var year;
    if (str.length == 6)
        year = 2000 + parseInt (str.substring(4,6), 10);
    else
        year = parseInt (str.substring(4,8), 10);

    // return the date
    var result = new Date(year,month,day);
    
    return result;
}

