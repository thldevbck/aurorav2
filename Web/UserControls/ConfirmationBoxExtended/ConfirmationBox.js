﻿function showConfirmationBoxViaClient(id) 
{      
    var confirmationBoxBehavior = $find(id);
    confirmationBoxBehavior.show();
}

function hideConfirmationBoxViaClient(id) 
{    
    var confirmationBoxBehavior = $find(id);
    confirmationBoxBehavior.hide();
    return false;
}
