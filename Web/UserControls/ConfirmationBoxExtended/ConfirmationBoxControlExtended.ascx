<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfirmationBoxControlExtended.ascx.vb" Inherits="UserControls_ConfimationBoxControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:HiddenField ID="paramHiddenField" runat="server" />
<asp:HiddenField ID="redirectUrlHiddenField" runat="server" />

<asp:Button 
 runat="server" 
 ID="hiddenTargetControlForConfirmationBox" 
 Style="display: none" />

<ajaxToolkit:ModalPopupExtender 
 runat="server" 
 ID="confirmationBox" 
 BehaviorID="confirmationBoxBehavior"
 TargetControlID="hiddenTargetControlForConfirmationBox" 
 PopupControlID="confirmationPanel"
 BackgroundCssClass="modalBackground" 
 DropShadow="True" 
 PopupDragHandleControlID="confirmationBoxDragHandle" />

<asp:Panel 
 runat="server" 
 CssClass="modalPopup" 
 ID="confirmationPanel" 
 Style="display: none; padding: 10px;" Width="500px" >

    <asp:Panel 
        runat="Server" 
        ID="confirmationBoxDragHandle" 
        CssClass="modalPopupTitle"><asp:Label ID="titleLabel" runat="Server" Text="Confirm" />
    </asp:Panel>
    <br />
    <table width="100%">
        <tr>
            <td style="vertical-align:top;" align="left">
                <asp:Image Id="messageTypeImage" runat="Server" ImageUrl="~/Images/informat2.gif" />
            </td>
            <td style="vertical-align:top;" align="left" width="100%">
                <asp:Label ID="textLabel" runat="server" />
                <br />
                <br />
            </td>
        </tr>            
        <tr>
            <td colspan="2" align="right" width="100%">
                <asp:Button ID="leftButton" runat="server" Text="Yes" CssClass="Button_Standard Button_OK" />&nbsp;
                <asp:Button ID="centerButton" runat="server" Text="No" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="rightButton" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
            </td>
        </tr>
        <tr>
            <td style="vertical-align:top;" align="left" width="100%" colspan="2">
                <asp:Label ID="noteLabel" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
    </asp:Panel>
