<%@ Control 
    Language="VB" 
    AutoEventWireup="false" 
    CodeFile="CollapsiblePanel.ascx.vb"
    Inherits="UserControls_CollapsiblePanel" %>


<table cellpadding="0" cellspacing="0" class="collapsibleTable" id="collapsibleTable" runat="server" width="100%" style="margin-bottom: 0; padding-bottom: 0; margin-top: 10px;">
    <tr>
        <th width="100%">
            <asp:Panel ID="collapsiblePanel" runat="server" Width="100%">
                <asp:Image ID="collapsibleImage" runat="server" ImageUrl="~/Images/sectionShow.gif" AlternateText="" ImageAlign="right"/>
                <asp:Label ID="titleLabel" runat="server" />
            </asp:Panel>
        </th>
    </tr>
</table>
    
<ajaxToolkit:CollapsiblePanelExtender 
    ID="collapsiblePanelExtender" 
    runat="Server" 
    ExpandControlID="collapsiblePanel"
    CollapseControlID="collapsiblePanel" 
    Collapsed="True" 
    ImageControlID="collapsibleImage"
    ExpandedImage="~/images/sectionHide.gif" 
    ExpandedText="Hide"
    CollapsedImage="~/images/sectionShow.gif"
    CollapsedText="Show"
    SuppressPostBack="true" 
    TargetControlID=""
    />

