
Partial Class UserControls_CollapsiblePanel
    Inherits System.Web.UI.UserControl

    Public Property TargetControlID() As String
        Get
            Return ViewState("TargetControlID")
        End Get
        Set(ByVal value As String)
            ViewState("TargetControlID") = value
        End Set
    End Property

    Public Property Title() As String
        Get
            Return titleLabel.Text
        End Get
        Set(ByVal value As String)
            titleLabel.Text = value
        End Set
    End Property

    Public Property Width() As Unit
        Get
            Return Unit.Parse(collapsibleTable.Width)
        End Get
        Set(ByVal value As Unit)
            collapsibleTable.Width = value.ToString()
        End Set
    End Property

    Public Property Collapsed() As Boolean
        Get
            Return Me.collapsiblePanelExtender.Collapsed
        End Get
        Set(ByVal value As Boolean)
            Me.collapsiblePanelExtender.Collapsed = value

            ' don't remove this line, coz it'll kill my code - Shoel
            Me.collapsiblePanelExtender.ClientState = value.ToString()
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("CollapsiblePanel.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CollapsiblePanel.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("CollapsiblePanel.css") & """ />")
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim c As Panel = CType(Me.Parent.FindControl(Me.TargetControlID), Panel)
        If c IsNot Nothing Then
            collapsiblePanelExtender.TargetControlID = Me.TargetControlID
            c.CssClass = (c.CssClass & " collapsibleTablePanel").Trim()
            If Me.Collapsed Then c.Style.Add(HtmlTextWriterStyle.Height, "0px")
        End If
    End Sub

End Class
