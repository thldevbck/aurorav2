<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Example.aspx.vb" Inherits="UserControls_Example" MasterPageFile="~/Include/AuroraHeader.master" validateRequest="false" %>

<%@ Register Src="DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="ConfirmationBox\ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl" TagPrefix="uc1" %>
<%@ Register Src="SupervisorOverrideControl\SupervisorOverrideControl.ascx" TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<%@ Register Src="MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>
<%@ Register Src="RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="WizardControl\WizardControl.ascx" TagName="WizardControl" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style>
        .pickerExample { border-top: dotted 1px black; border-left: dotted 1px black;}
        .pickerExample td { border-bottom: dotted 1px black; border-right: dotted 1px black;}
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <h1>WizardControl</h1>

    <uc1:WizardControl Id="wizardControl" runat="server" SelectedIndex="1">
        <Items>
            <asp:ListItem>Booking Request</asp:ListItem>
            <asp:ListItem>Duplicate Rentals</asp:ListItem>
            <asp:ListItem>Display Vehicles</asp:ListItem>
            <asp:ListItem>Summary</asp:ListItem>
        </Items>
    </uc1:WizardControl>
    
    <hr />
    
    Selected Index: <asp:Label ID="selectedIndexLabel" runat="server" Text="1" /><br />
    
    <p>
The Macintosh Classic is a personal computer that was manufactured by Apple Computer from October 15, 1990 to September 14, 1992.
    </p>
    
    <asp:Button ID="nextButton" runat="server" Text="Next" CssClass="Button_Standard" />

    <h1>RegExTextBox</h1>
    
    <p>Positive Decimal (required): <uc1:RegExTextBox 
        ID="regExTextBox1" 
        runat="server" 
        Nullable="false"
        ErrorMessage="Positive decimal only"
        RegExPattern="^\d+$" /></p>
    <p>Positive Decimal (nullable):  <uc1:RegExTextBox 
        ID="regExTextBox2" 
        runat="server" 
        Nullable="true"
        ErrorMessage="Positive decimal only"
        RegExPattern="^\d+$" /></p>

    <h1>DateControl</h1>
    
    <p>date2: (required)<uc1:DateControl ID="dateControl1" runat="server" Nullable="false" Text="2001-12-12" /></p>
    <p>date1: (nullable)<uc1:DateControl ID="dateControl" runat="server" Nullable="true" Text="2001-12-12" /></p>

    <p>
        <b>DateControl &amp; AutoPostBack test</b>
    
        <asp:UpdatePanel ID="UpdatePanel2" runat="Server">
            <ContentTemplate>
            
                <uc1:DateControl ID="dateControl2" runat="server" Nullable="true" Text="2001-12-12" AutoPostBack="true" />
                
                <br />
                
                <asp:Label ID="dateControl2Label" runat="Server"></asp:Label>
            
            </ContentTemplate>
        </asp:UpdatePanel>
    </p>

    
    <h1>TimeControl</h1>

    <p>time 1: (required)<uc1:TimeControl ID="timeControl1" runat="server" Nullable="false" Text="01:02"/></p>
    <p>time 2: (nullable) : <uc1:TimeControl ID="timeControl2" runat="server" Nullable="true" Text="23:24"/></p>

    <p>
        <b>TimeControl &amp; AutoPostBack test</b>
    
        <asp:UpdatePanel ID="UpdatePanel1" runat="Server">
            <ContentTemplate>
            
                <uc1:TimeControl ID="timeControl3" runat="server" Nullable="false" Text="01:02" AutoPostBack="true" />
                
                <br />
                
                <asp:Label ID="timeControl3Label" runat="Server"></asp:Label>
            
            </ContentTemplate>
        </asp:UpdatePanel>
    </p>


    <h1>MessagePanelControl</h1>
    
    <uc1:MessagePanelControl RunAt="server" ID="testMessagePanelControl" />
   
    <br />

    <input type="button" value="Clear" onclick="MessagePanelControl.clearMessagePanel('<%= testMessagePanelControl.ClientID %>')" />
    <input type="button" value="Info" onclick="MessagePanelControl.setMessagePanelInformation('<%= testMessagePanelControl.ClientID %>', 'hello, world')" />
    
    <br />
    
    <p>Email (required): <uc1:RegExTextBox 
        ID="regExTextBox3" 
        runat="server" 
        Nullable="false"
        MessagePanelControlID="testMessagePanelControl"
        ErrorMessage="Enter a valid email"
        RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" 
        Text="wilfred.verkley@thlonline.com" Width="300px" /></p>


    <h1>PickerControl</h1>

    <table width="100%" class="pickerExample" cellpadding="2" cellspacing="0">
        <tr>
            <td>Location (required):</td>        
            <td><uc1:PickerControl ID="pickerControl" runat="server" PopupType="LOCATION" Width="40px" Nullable="false" /></td>        
            <td>Package:</td>        
            <td><uc1:PickerControl ID="pickerControl2" runat="server" PopupType="PACKAGE" /></td>        
            <td>Country (Append Desc):</td>        
            <td><uc1:PickerControl ID="pickerControl3" runat="server" PopupType="POPUPCOUNTRY" AppendDescription="true" Width="200px" Caption="Pick me, pick me!" /></td>
        </tr>
        <tr>
            <td>Sub Group:</td>        
            <td><uc1:PickerControl ID="pickerControl1" runat="server" PopupType="SUBGROUP" AppendDescription="true"  Width="200px" /></td>        
        </tr>
        </table>
        
    <p>
        <b>PickedControl &amp; AutoPostBack test</b>
    
        <asp:UpdatePanel runat="Server">
            <ContentTemplate>
            
                <uc1:PickerControl 
                    ID="pickerControl4" 
                    runat="server" 
                    PopupType="LOCATION" 
                    Width="200px" 
                    Nullable="false" 
                    AppendDescription="true" 
                    AutoPostBack="true" />
                
                <br />
                
                <asp:Label ID="pickerControl4Label" runat="Server"></asp:Label>
            
            
            </ContentTemplate>
        </asp:UpdatePanel>
        
    </p>
    
    
    <p>
        <b>Manny AGENTPACKAGE4BOOKINGREQ test</b>
        
        <br />
    
        <uc1:PickerControl 
            ID="pickerControl5" 
            runat="server" 
            PopupType="AGENTPACKAGE4BOOKINGREQ" 
            Width="200px" 
            Nullable="false" 
            AppendDescription="true" 
            AutoPostBack="true" 
            Param2=""
            Param3=""
        />
        
        <input type="button" value="Set params" class="Button_Standard" onclick="
            PickerControl.setParam ('ctl00_ContentPlaceHolder_pickerControl5_pickerTextBox', 2, 'flexb'); 
            PickerControl.setParam ('ctl00_ContentPlaceHolder_pickerControl5_pickerTextBox', 3, 'FDDB6E5C-16A4-48DB-905C-98C2405F03AB,C72688F8-4325-4D50-9FA6-EB3B6BC30EC5,08/08/2008 8:00 AM,AKL ,AKL ,1,0,0,9/09/2008 07:00:00'); 
        "/>
    
    </p>

    
    <h1>Confirmation Box </h1>

    <asp:Button ID="confirmButton" runat="Server" Text="Confirm" CssClass="Button_Standard" />
    <uc1:ConfirmationBoxControl 
        ID="confirmationBoxControl1" 
        runat="server" 
        Text="Confirmation Box 1" />
    <uc1:ConfirmationBoxControl 
        ID="confirmationBoxControl2" 
        runat="server" 
        Text="Confirmation Box 2" />
        
    <p>&nbsp;</p>

    <h1>Supervisor Override Control</h1>        

    <asp:Button ID="supervisorOverrideButton" runat="Server" Text="Override" CssClass="Button_Standard" />
    <uc1:SupervisorOverrideControl 
        ID="supervisorOverrideControl" 
        runat="server" 
        Role="GENERALADMIN"
        Title="Can we fix it?"
        Text="Yes we can!" />

    <p>&nbsp;</p>

    <asp:Button ID="submitButton" runat="server" Text="Submit" CssClass="Button_Standard" /> 


</asp:Content>

