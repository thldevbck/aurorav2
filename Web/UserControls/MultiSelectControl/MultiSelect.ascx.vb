﻿Option Strict On
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Common

Public Enum MultiSelectType
    Locations = 0
    FleetModels = 1
    VehicleProducts = 2
End Enum

Partial Class UserControls_SelectControl_MultiSelect
    Inherits System.Web.UI.UserControl

    Protected Shared mControlKey As String = "_" + Guid.NewGuid().ToString().Replace("-", "")
    Protected Shared mJsKey As String = mControlKey + ".js"
    'Protected Shared mCssKey As String = mControlKey + ".css"

    Protected mSelectType As MultiSelectType
    Protected mDisplayKeys As Boolean

    Protected mDisplayValue As String

    Protected mItems As IEnumerable(Of MultiSelectItem)

    Protected mHasChanged As Boolean

    Protected mCountryCode As String
    Protected mCompanyCode As String

    Protected mPopupName As String

    Protected mIsReadOnly As Boolean

    Protected mRetainValuesOnPostback As Boolean = True


    Public Shared ReadOnly Property ClientScriptReference As String
        Get
            Return mControlKey
        End Get
    End Property

    Public ReadOnly Property HasChanged As Boolean
        Get
            Return mHasChanged
        End Get
    End Property

    Public Property CountryCode As String
        Get
            Return mCountryCode
        End Get
        Set(ByVal value As String)
            mCountryCode = value
        End Set
    End Property

    Public Property PopupName As String
        Get
            Return mPopupName
        End Get
        Set(ByVal value As String)
            mPopupName = value
        End Set
    End Property

    Public Property IsReadOnly As Boolean
        Get
            Return mIsReadOnly
        End Get
        Set(ByVal value As Boolean)
            mIsReadOnly = value
        End Set
    End Property

    Public Property RetainValuesOnPostback As Boolean
        Get
            Return mRetainValuesOnPostback
        End Get
        Set(ByVal value As Boolean)
            mRetainValuesOnPostback = value
        End Set
    End Property

    Public Property Items As IEnumerable(Of MultiSelectItem)
        Get
            If mItems Is Nothing Then
                mItems = New List(Of MultiSelectItem)
            End If

            Return mItems
        End Get
        Set(ByVal value As IEnumerable(Of MultiSelectItem))
            mItems = value
        End Set
    End Property


    'Protected mMaximumCharactersPerLine As Integer

    ' ''' <summary>
    ' ''' Indicates how many characters 
    ' ''' </summary>
    ' ''' <value></value>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Public Property MaximumCharactersPerLine As Integer
    '    Get
    '        Return mMaximumCharactersPerLine
    '    End Get
    '    Set(ByVal value As Integer)
    '        mMaximumCharactersPerLine = value
    '    End Set
    'End Property

    Public Property SelectType As MultiSelectType
        Get
            Return mSelectType
        End Get
        Set(ByVal value As MultiSelectType)
            mSelectType = value
        End Set
    End Property

    Protected ReadOnly Property IsThreePartDataItemType As Boolean
        Get
            Return SelectType = MultiSelectType.FleetModels OrElse SelectType = MultiSelectType.VehicleProducts
        End Get
    End Property

    Public Property DisplayKeys As Boolean
        Get
            Return mDisplayKeys
        End Get
        Set(ByVal value As Boolean)
            mDisplayKeys = value
        End Set
    End Property

    ''' <summary>
    ''' Creates and returns a string that contains comma-separated item keys.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function GetKeysAsCsv() As String
        If mItems Is Nothing Then
            Return Nothing
        End If

        Return GetSeparatedValueString(mItems, True, False)
    End Function

    'Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
    '    MyBase.OnInit(e)
    'End Sub

    'Protected Overrides Sub LoadViewState(ByVal savedState As Object)
    '    MyBase.LoadViewState(savedState)

    '    RestoreStateFromString(hidData.Value)
    'End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        RestoreStateFromString(hidData.Value)

        MyBase.OnLoad(e)

        mCompanyCode = DirectCast(Page, AuroraPage).CompanyCode


        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered(Me.Page.GetType(), mJsKey) Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.Page.GetType(), mJsKey, RenderControlToString(plhInitControl), False)
        End If

        InitImageButton()
    End Sub

    Protected Overrides Sub LoadViewState(ByVal savedState As Object)
        Dim state As MultiSelectControlState = DirectCast(savedState, MultiSelectControlState)
        mDisplayKeys = state.DisplayKeys
        mCountryCode = state.CountryCode

        MyBase.LoadViewState(state.BaseState)
    End Sub

    Protected Overrides Function SaveViewState() As Object
        Dim baseState As Object = MyBase.SaveViewState()

        Return New MultiSelectControlState(mDisplayKeys, mCountryCode, baseState)
    End Function

    Protected Sub InitImageButton()
        'btnShowPopup.Attributes.Add("onclick", "return " & mControlKey & ".open(this);")
    End Sub

    'Public Overrides Sub DataBind()
    '    'MyBase.DataBind())

    'End Sub

    Public Shared Function RenderControlToString(ByVal control As Control) As String
        Dim controlHidden As Boolean = True
        If Not control.Visible Then
            controlHidden = True
            control.Visible = True
        End If

        Dim objStringWriter As System.IO.StringWriter = Nothing
        Dim objHtmlWriter As HtmlTextWriter = Nothing
        Try
            objStringWriter = New System.IO.StringWriter()
            objHtmlWriter = New HtmlTextWriter(objStringWriter)
            control.RenderControl(objHtmlWriter)
            Return objStringWriter.ToString()
        Catch

        Finally
            If objHtmlWriter IsNot Nothing Then
                objHtmlWriter.Close()
            End If

            If objStringWriter IsNot Nothing Then
                objStringWriter.Close()
            End If

            If controlHidden Then
                control.Visible = False
            End If
        End Try

        Return Nothing
    End Function

    Protected Sub RestoreStateFromString(ByVal stateValue As String)
        If String.IsNullOrEmpty(stateValue) Then
            Return
        End If

        Dim index As Integer = stateValue.LastIndexOf(","c) + 1

        If index < 1 Then
            If stateValue.Length > 1 Then
                Return
            End If
        End If

        Dim strHasChanged As String = stateValue.Substring(index)

        If strHasChanged = "1" Then
            mHasChanged = True
        ElseIf strHasChanged = "0" Then
            mHasChanged = False
        Else
            Return
        End If

        If index > 0 Then
            'mItems = GetItems(stateValue.Substring(0, index - 1), IsThreePartDataItemType)
            mItems = GetItems(stateValue.Substring(0, index - 1), False)
        End If
    End Sub

    Public Function GetScript() As String
        Dim script As String

        script = RenderControlToString(plhInitControl)

        Return script
    End Function

    'Public Shared Function GetControlScript() As String
    '    Dim page As New Page
    '    Dim control As New UserControls_SelectControl_MultiSelect()

    '    'Dim control As UserControls_SelectControl_MultiSelect = _
    '    '    DirectCast(page.LoadControl(GetType(UserControls_SelectControl_MultiSelect), New Object() {}), UserControls_SelectControl_MultiSelect)

    '    page.Controls.Add(control)



    '    'control.EnsureChildControls()

    '    'control.Construct()
    '    'control.CreateControlCollection()
    '    'control.CreateChildControls()
    '    'control.EnsureChildControls()
    '    'control.OnInit(New EventArgs())

    '    Dim stringWriter As New System.IO.StringWriter()


    '    Try
    '        'HttpContext.Current.Server.Execute(page, stringWriter, False)
    '        HttpServerUtility()
    '    Catch ex As Exception
    '    Finally
    '        stringWriter.Close()
    '    End Try


    '    Return control.GetScript()
    'End Function

    Public Shared Function GetCsv(ByVal items As IEnumerable(Of String)) As String
        If items Is Nothing Then
            Return ""
        End If

        Dim builder As New StringBuilder()

        For Each item As String In items
            If builder.Length > 0 Then
                builder.Append(",")
            End If
            If item IsNot Nothing Then
                builder.Append(item.Replace("\", "\\").Replace(",", "\,"))
            End If
        Next

        Return builder.ToString()
    End Function

    Public Shared Function GetSeparatedValueString(ByVal items As IEnumerable(Of MultiSelectItem), ByVal addKeys As Boolean, ByVal addValues As Boolean) As String
        Return GetSeparatedValueString(items, addKeys, addValues, True, False)
    End Function

    Public Shared Function GetSeparatedValueString(ByVal items As IEnumerable(Of MultiSelectItem), ByVal addKeys As Boolean, ByVal addValues As Boolean, ByVal encode As Boolean) As String
        Return GetSeparatedValueString(items, addKeys, addValues, encode, False)
    End Function

    Public Shared Function GetSeparatedValueString(ByVal items As IEnumerable(Of MultiSelectItem), ByVal addKeys As Boolean, ByVal addValues As Boolean, ByVal encode As Boolean, ByVal useDescription As Boolean) As String
        If items Is Nothing Then
            Return Nothing
        End If

        Dim builder As New System.Text.StringBuilder()

        Dim bolAddBoth As Boolean = addValues AndAlso addKeys

        For Each item As MultiSelectItem In items
            If item IsNot Nothing Then
                If builder.Length > 0 Then
                    If bolAddBoth Then
                        builder.Append(","c)
                    Else
                        builder.Append(", ")
                    End If
                End If

                Dim strDescription As String = item.Description
                Dim objValue As Object = item.Value
                Dim strValue As String = Nothing

                If addKeys Then
                    Dim objKey As Object = item.Key

                    If objKey IsNot Nothing Then
                        Dim strKey As String = objKey.ToString()
                        If encode Then
                            strKey = strKey.Replace("\", "\\").Replace(",", "\,")
                        End If

                        If useDescription AndAlso objValue IsNot Nothing Then
                            strValue = objValue.ToString()
                            If encode Then
                                strValue = strValue.Replace("\", "\\").Replace(",", "\,")
                            End If

                            strKey = String.Concat(strKey, ",", strValue)

                            If encode Then
                                strKey = strKey.Replace("\", "\\").Replace(",", "\,")
                            End If
                        End If

                        builder.Append(strKey)
                    End If
                End If

                If addValues Then
                    If addKeys Then
                        builder.Append(","c)
                    End If

                    If objValue IsNot Nothing Then
                        If strValue Is Nothing Then
                            strValue = objValue.ToString()
                            If encode Then
                                strValue = strValue.Replace("\", "\\").Replace(",", "\,")
                            End If
                        End If

                        builder.Append(strValue)

                        If useDescription Then
                            If strDescription IsNot Nothing AndAlso encode Then
                                strDescription = strDescription.Replace("\", "\\").Replace(",", "\,")
                            End If

                            builder.Append(" - ")
                            builder.Append(strDescription)
                        End If
                    End If
                End If
            End If
        Next

        Return builder.ToString()
    End Function

    Public Shared Function GetItems(ByVal values As String, ByVal keysOnly As Boolean) As List(Of MultiSelectItem)
        Dim strSelectedItems As String = values.Trim()

        If strSelectedItems.Length = 0 Then
            Return New List(Of MultiSelectItem)
        End If

        Dim lstValues As List(Of String) = GetValuesFromCsv(strSelectedItems)
        Dim lstSelectedItems As New List(Of MultiSelectItem)

        Dim intLen As Integer = lstValues.Count

        If keysOnly Then
            For i As Integer = 0 To intLen - 1
                Dim item As New MultiSelectItem(lstValues(i), lstValues(i))

                lstSelectedItems.Add(item)
            Next
        Else
            If intLen Mod 2 = 1 Then
                Throw New Exception("Invalid item data received from client.")
            End If

            For i As Integer = 0 To intLen - 1 Step 2
                Dim item As New MultiSelectItem(lstValues(i), lstValues(i + 1))

                lstSelectedItems.Add(item)
            Next
        End If


        Return lstSelectedItems
    End Function

    Public Shared Function GetValuesFromCsv(ByVal value As String) As List(Of String)
        Dim lstValues As New List(Of String)

        If value Is Nothing Then
            Return lstValues
        End If

        Dim strValue As String
        Dim intPrevIndex As Integer = 0
        Dim intIndex As Integer = 0
        Dim intLen As Integer = value.Length

        Dim intBacktrackIndex As Integer

        While intIndex < intLen
            intIndex = value.IndexOf(","c, intIndex)

            If intIndex > -1 Then
                'Count the number of escape slashes
                intBacktrackIndex = intIndex - 1

                While intBacktrackIndex >= 0 AndAlso value(intBacktrackIndex) = "\"c
                    intBacktrackIndex = intBacktrackIndex - 1
                End While

                If (intIndex - intBacktrackIndex - 1) Mod 2 = 1 Then
                    'This is an escaped comma - move on
                Else
                    strValue = value.Substring(intPrevIndex, intIndex - intPrevIndex)
                    'Unescape
                    strValue = strValue.Replace("\\", "\").Replace("\,", ",").Trim()
                    lstValues.Add(strValue)

                    intPrevIndex = intIndex + 1
                End If
            Else
                Exit While
            End If

            intIndex = intIndex + 1
        End While

        If intPrevIndex < intLen Then
            strValue = value.Substring(intPrevIndex, intLen - intPrevIndex)
            'Unescape
            strValue = strValue.Replace("\\", "\").Replace("\,", ",").Trim()
            lstValues.Add(strValue)
        ElseIf intPrevIndex = intLen Then
            lstValues.Add("")
        End If


        Return lstValues
    End Function

    Protected Function GetPopupName() As String
        If Not String.IsNullOrEmpty(mPopupName) Then
            Return mPopupName
        End If

        Select Case mSelectType
            Case MultiSelectType.Locations
                Return "Locations"
            Case MultiSelectType.FleetModels
                Return "Fleet Models"
        End Select

        Return Nothing
    End Function

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)

        mDisplayValue = GetSeparatedValueString(mItems, mDisplayKeys, Not mDisplayKeys, False, False)

        If mDisplayValue = "" Then
            mDisplayValue = Nothing
        End If

        'Dim strStateValue As String = GetSeparatedValueString(mItems, True, Not mUseKeysOnly, True)
        Dim strStateValue As String

        'strStateValue = GetSeparatedValueString(mItems, True, Not IsThreePartDataItemType)
        strStateValue = GetSeparatedValueString(mItems, True, RetainValuesOnPostback)


        If Not String.IsNullOrEmpty(strStateValue) Then
            strStateValue = strStateValue + "," + IIFX(mHasChanged, "1", "0")
        Else
            strStateValue = IIFX(mHasChanged, "1", "0")
        End If

        hidData.Value = strStateValue
    End Sub

    <System.Serializable()> _
    Protected Class MultiSelectControlState
        Private mDisplayKeys As Boolean
        Private mCountryCode As String
        Private mBaseState As Object
        Private mName As String
        'Private mCompanyCode As String

        Public Property DisplayKeys As Boolean
            Get
                Return mDisplayKeys
            End Get
            Set(ByVal value As Boolean)
                mDisplayKeys = value
            End Set
        End Property
        Public Property CountryCode As String
            Get
                Return mCountryCode
            End Get
            Set(ByVal value As String)
                mCountryCode = value
            End Set
        End Property

        Public Property BaseState As Object
            Get
                Return mBaseState
            End Get
            Set(ByVal value As Object)
                mBaseState = value
            End Set
        End Property

        'Public Property Name As String
        '    Get
        '        Return mName
        '    End Get
        '    Set(ByVal value As String)
        '        mName = value
        '    End Set
        'End Property

        'Public Property CompanyCode As String
        '    Get
        '        Return mCompanyCode
        '    End Get
        '    Set(ByVal value As String)
        '        mCompanyCode = value
        '    End Set
        'End Property

        Public Sub New(ByVal displayKeys As Boolean, ByVal countryCode As String, ByVal baseState As Object) ', ByVal companyCode As String)
            mDisplayKeys = displayKeys
            mCountryCode = countryCode
            'mName = name
            mBaseState = baseState
            'mCompanyCode = companyCode
        End Sub
    End Class

End Class

Public Class MultiSelectItemRepository

    Protected mItemType As MultiSelectType

    Public Sub New(ByVal itemType As MultiSelectType)
        mItemType = itemType
    End Sub

    Public Function GetItems(ByVal partialValue As String, ByVal country As String, ByVal companyCode As String) As List(Of MultiSelectItem)
        Return GetItems(partialValue, country, companyCode, Nothing)
    End Function

    Public Function GetItems(ByVal partialValue As String, ByVal country As String, ByVal companyCode As String, ByVal itemIdsCsv As String) As List(Of MultiSelectItem)
        Dim dataSet As New SelectItemDataSet()
        Data.ExecuteListSP("GetMultiSelectDialogItems", dataSet, New ParamInfo(CInt(mItemType), "SelectTypeID"), New ParamInfo(partialValue, "PartialValue"), New ParamInfo(country, "Country"), New ParamInfo(companyCode, "CompanyCode"), New ParamInfo(itemIdsCsv, "ItemIdsCsv"))

        Return dataSet.ResultSet
    End Function

    Protected Class SelectItemDataSet
        Inherits ListDataSet(Of MultiSelectItem)

        Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As MultiSelectItem
            Dim item As New MultiSelectItem(reader("ItemKey"), reader("ItemValue"), GetDbValue(Of String)(reader("ItemDescription")))

            Return item
        End Function

    End Class
End Class

Public Class MultiSelectItem
    Private mKey As Object
    Private mValue As Object
    Private mDescription As String

    Public Property Key As Object
        Get
            Return mKey
        End Get
        Set(ByVal value As Object)
            mKey = value
        End Set
    End Property

    Public Property Value As Object
        Get
            Return mValue
        End Get
        Set(ByVal value As Object)
            mValue = value
        End Set
    End Property

    Public Property Description As String
        Get
            Return mDescription
        End Get
        Set(ByVal value As String)
            mDescription = value
        End Set
    End Property

    Public Sub New()
        mKey = key
        mValue = value
    End Sub

    Public Sub New(ByVal key As Object, ByVal value As Object)
        mKey = key
        mValue = value
    End Sub

    Public Sub New(ByVal key As Object, ByVal value As Object, ByVal description As String)
        mKey = key
        mValue = value
        mDescription = description
    End Sub

End Class
