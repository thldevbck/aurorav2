﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MultiSelect.ascx.vb" Inherits="UserControls_SelectControl_MultiSelect" %>

<span class="MultiSelectControl">
    <%--<input type="text" readonly="readonly" />--%>
    <%--<asp:Image ID="btnShowPopup" runat="server" ImageUrl="~/UserControls/PickerControl/PickerControl.gif" ImageAlign="AbsMiddle" Width="18" Height="15" />--%>
    <%--<span>AKL,&nbsp;ACL,&nbsp;CHC,&nbsp;MLB,&nbsp;ADE, ADL, SYD, AKI, BLAH, YADA</span>--%>
    
    <%="" %> <%--Fix for ASP.NET bug: '__o' is not declared.--%>

    <% If IsReadOnly Then%>
    <span><%=mDisplayValue %></span>
    <% Else%>
    <a href="" onclick="return <%=mControlKey%>.open(this);"><%=mDisplayValue %><img <%= Aurora.Common.IIFX(mDisplayValue isnot nothing, "style=""display:none;""", "") %> src="<%=ResolveUrl("~/UserControls/PickerControl/PickerControl.gif")%>" alt="ADD" /></a>
    <%End If%>
    <asp:HiddenField ID="hidData" runat="server" />
</span>

<asp:PlaceHolder ID="plhInitControl" runat="server" Visible="false">
    <script type="text/javascript">
        var <%=mControlKey%> = new function(){
            var init = function()
            {
                var oCss = document.createElement('link');
                oCss.type = 'text/css';
                oCss.rel = 'stylesheet';
                oCss.href = '<%=ResolveUrl("MultiSelectControl.css")%>';
                document.getElementsByTagName('head')[0].appendChild(oCss);
            },
            getValuesFromCsv = function (source) {
                var 
                aVals = [],
                sVal,
                iPrevIndex = 0,
                iIndex = 0,
                iLen = source.length,
                iBackIndex;

                while (iIndex < iLen) {
                    iIndex = source.indexOf(',', iIndex);

                    if (iIndex > -1) {
                        iBackIndex = iIndex - 1;

                        while (iBackIndex >= 0 && source.charAt(iBackIndex) == '\\') {
                            iBackIndex = iBackIndex - 1;
                        }

                        if ((iIndex - iBackIndex - 1) % 2 == 0) {
                            sVal = source.substr(iPrevIndex, iIndex - iPrevIndex);
                            sVal = sVal.replace(/\\\\/g, '\\').replace(/\\,/g, ',');
                            aVals.push(sVal);

                            iPrevIndex = iIndex + 1;
                        }
                    }
                    else break;

                    iIndex = iIndex + 1;
                }

                if (iPrevIndex < iLen) {
                    sVal = source.substr(iPrevIndex, iLen - iPrevIndex);
                    sVal = sVal.replace(/\\\\/g, '\\').replace(/\\,/g, ',');
                    aVals.push(sVal);
                }
                else if (iPrevIndex == iLen)
                    aVals.push('');


                return aVals;
            };

            this.getValuesFromCsv = getValuesFromCsv;

            this.toCsv = function(values){
                var i=-1, sResult='';

                while(++i < values.length){
                    if(sResult)
                        sResult += ',';

                    sResult += values[i] ? values[i].replace(/\\/gi, '\\\\').replace(/,/gi, '\\,') : '';
                }

                return sResult;
            }

            this.open = function(caller, saveCallback, getCallback){
                var sRnd = Math.random() * 100000000000000000,
                sSaveHandlerRef = 'a'+ sRnd,
                sGetHandlerRef = 'b'+ sRnd,
                sCsvHandlerRef = 'c'+ sRnd,
                oWin = null,
                onSave,
                oStateField = caller ? (caller.nextSibling.nodeType == 1 ? caller.nextSibling : caller.nextSibling.nextSibling) : null,
                handler = function(){
                    if(oWin){
                        oWin.focus();
                        return false;
                    }

                    oWin = window.open('<%=ResolveUrl("MultiSelectDialog.aspx") & "?itemTypeId=" & CInt(mSelectType)%>' + '&targetId=' + sRnd + '<%=Aurora.Common.IIFX(mDisplayKeys, "&displayKeys=1", "") + Aurora.Common.IIFX(mCountryCode isnot nothing, "&countryCode=" & mCountryCode, "") + Aurora.Common.IIFX(mCompanyCode isnot nothing, "&companyCode=" & mCompanyCode, "") + Aurora.Common.IIFX(mRetainValuesOnPostback, "&retainValues=1", "")%>',
                    '',
                    'directories=0,width=800,height=450,resizable=1,location=0,menubar=0,scrollbars=0,status=0,toolbar=0'
                    );

                    return false;
                };
                
                onSave = function(displayValues, stateValues){
                    oWin = null;
                    if(typeof displayValues === 'undefined')
                        return;

                    if(saveCallback){
                        saveCallback(displayValues, stateValues);
                        return;
                    }

                    if(caller.firstChild.nodeType != 3)
                        caller.insertBefore(document.createTextNode(''), caller.firstChild);

                    caller.firstChild.nextSibling.style.display = displayValues ? 'none' : '';
                    
                    caller.firstChild.nodeValue = displayValues;
                    if(oStateField)
                        oStateField.value = stateValues + (stateValues ? ',1' : '1');
                };


                if(!getCallback)
                    getCallback = function(){
                        if(!oStateField)
                            return '';

                        var sCsv = oStateField.value,
                        i = sCsv.lastIndexOf(',');

                        sCsv = i > -1 ? sCsv.substr(0, i) : '';

                        return sCsv;
                    };

                window[sSaveHandlerRef] = onSave;
                window[sGetHandlerRef] = getCallback;
                window[sCsvHandlerRef] = getValuesFromCsv;
                if(caller)
                    caller.onclick = handler;
                handler();
                return false;
            }

            init();
        }
    </script>
</asp:PlaceHolder>
