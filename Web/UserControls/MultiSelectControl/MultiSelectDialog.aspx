﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="MultiSelectDialog.aspx.vb" Inherits="UserControls_MultiSelectControl_MultiSelectDialog" %>

<%@ Register Src="~/UserControls/MultiSelectControl/MultiSelect.ascx" TagName="DateControl" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../../Include/AuroraHeader.css" />
    <link rel="stylesheet" type="text/css" href="MultiSelectDialog.css" />    
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        

        <div>
            <table id="multiselTable">
                <tr>
                    <td>
                        <b>Available Items</b> <br /> <br />
                        Search: <input id="txtItemSearch" type="text" EnableViewState="false" runat="server" />
                        <input id="btnMultiselSearch" type="button" value="Search" onclick="__doPostBack('<%= pnlItemsList.ClientID %>', '');return false;" />
                        <asp:UpdatePanel ID="pnlItemsList" UpdateMode="Conditional" ChildrenAsTriggers="false" RenderMode="Block" EnableViewState="false" runat="server">
                            <ContentTemplate>
                                <asp:HiddenField ID="hidSelectedItems" runat="server" />
                                <select id="lbxMultiselItems" multiple="multiple" class="multiselItems" >
                                    <%
                                        If mItems IsNot Nothing Then
                                            Dim lastIndex As Integer = mItems.Count - 1
                                            For i As Integer = 0 To lastIndex
                                        %>
                                                  
                                            <option value="<%= GetItemKey(mItems(i)) %>"><%= GetItemValue(mItems(i))%></option>
                                        <%
                                        Next
                                    End If
                                    %>
                                </select>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                    <td class="multiselArrows">
                        <input value="&gt;" id="multiselItemsAdd" type="button" /><br /><br />
                        <input  value="&lt;" id="multiselItemsRemove" type="button" />
                    </td>
                    <td>                
                        <b>Selected Items</b> <br /> <br />
                        <select id="lbxMultiselSelectedItems" multiple="multiple" class="multiselItemsSelected" >
                   
                        </select>
                    </td>            
                </tr>
                <tr>
                    <td colspan="3" class="multiselButtons">
                        <br />                        
                        <input id="btnMultiselSave" class="Button_Back Button_Medium" type="button" value="Ok" />
                        <input id="btnMultiselClose" class="Button_Back Button_Medium" type="button" value="Cancel" />
                    </td>
                </tr>
            </table>

            <script type="text/javascript">
                (function (sourceListId, targetListId, addButtonId, removeButtonId, saveButtonId, closeButtonId, selectedItemsInputId, searchButtonId, isThreePartDataType) {
                    var 
                    mAddButton = document.getElementById(addButtonId),
                    mRemoveButton = document.getElementById(removeButtonId),
                    mSaveButton = document.getElementById(saveButtonId),
                    mCloseButton = document.getElementById(closeButtonId),
                    //mSelectedItemsInput = document.getElementById(selectedItemsInputId),
                    mSearchButton = document.getElementById(searchButtonId),
                    mTemp,
                    sCallbackRef,
                    sSaveCallbackRef,
                    sGetCallbackRef,
                    sCsvCallbackRef,
                    bDisplayKeys,
                    sDisplayValues,
                    getSelectedItemsInput = function(){
                        return document.getElementById(selectedItemsInputId);
                    },
                    moveItems = function (sourceList, targetList) {
                        while (sourceList.selectedIndex > -1) {
                            var oOption = sourceList.options[sourceList.selectedIndex];
                            sourceList.remove(sourceList.selectedIndex);
                            targetList.add(oOption, 99999);
                        }
                    },
                    addItems = function () {
                        moveItems(
                            document.getElementById(sourceListId),
                            document.getElementById(targetListId)
                        );
                    },
                    removeItems = function () {
                        moveItems(
                            document.getElementById(targetListId),
                            document.getElementById(sourceListId)
                        );
                    },
                    getUrlQueryParam = function (param) {
                        var sQuery = location.href.split('?')[1], i, j, sParam = '&' + param.toLowerCase() + '=';
                        if (sQuery) {
                            sQuery = sQuery.toLowerCase();
                            sQuery = (i = sQuery.indexOf(sParam)) > -1 ? sQuery.substring(i + sParam.length, (sQuery.indexOf('&', (i + sParam.length)) + 1 || sQuery.length + 1) - 1) : '';
                        }
                        return sQuery || '';
                    },
                    getValuesFromCsv,
                    getSelectedItems = function(itemsCsv){
                        var oReqManager,
                        //i = itemsCsv.lastIndexOf(','),
                        callback = function(sender, args){
                            var strValue = getSelectedItemsInput().value;

                            if(strValue)
                                populateSelectedItems(getValuesFromCsv(strValue));

                            if(oReqManager)
                                oReqManager.remove_endRequest(callback);

                            mSearchButton.disabled = false;
                        };
                        
                        //itemsCsv = i > -1 ? itemsCsv.substr(0, i) : '';
                        
                        getSelectedItemsInput().value = itemsCsv;

                        if(isThreePartDataType){
                            oReqManager = Sys.WebForms.PageRequestManager.getInstance();
                            oReqManager.add_endRequest(callback);

                            mSearchButton.disabled = true;

                            setTimeout(function(){__doPostBack(selectedItemsInputId, '');}, 100);
                        }
                        else callback();
                    },
                    populateSelectedItems = function (items) {                        
                        var i = 0,
                        //iIncr = bDisplayKeys ? 1 : 2,
                        iIncr = 2,
                        iLen = items.length,
                        oList = document.getElementById(targetListId),
                        oOption;
                        while (i < iLen) {
                            oOption = document.createElement('option');
                            oOption.value = items[i];
                            //oOption.text = bDisplayKeys ? items[i] : items[i + 1];
                            oOption.text = items[i + 1];
                            oList.add(oOption, 99999);
                            i += iIncr;
                        }
                    },
                    getSortedOptions = function (list) {
                        var aOptions = [], aSource = list.options, comparer, i = -1;

                        while (++i < aSource.length)
                            aOptions.push(aSource[i]);

                        if (bDisplayKeys)
                            comparer = function (a, b) { return a.value > b.value ? 1 : a.value < b.value ? -1 : 0; };
                        else
                            comparer = function (a, b) { return a.text > b.text ? 1 : a.text < b.text ? -1 : 0; };

                        aOptions.sort(comparer);

                        return aOptions;
                    },
                    onSave = function (isRequest) {
                        var oList = document.getElementById(targetListId),
                        i = -1,
                        oOptions = getSortedOptions(oList.options),
                        oOption,
                        aValueSegments,
                        sValue,
                        sText,
                        oItemsInput = getSelectedItemsInput(),
                        sResult = '';

                        sDisplayValues = '';

                        while (oOption = oOptions[++i]) {
                            if (sResult) {
                                sResult += ',';
                                sDisplayValues += ', ';
                            }

                            sValue = oOption.value;

                            if(isThreePartDataType){
                                aValueSegments = getValuesFromCsv(sValue);
                                sValue = aValueSegments[0];
                                sText = aValueSegments[1];
                            }
                            else sText = oOption.text;

                            sResult += sValue.replace(/\\/gi, '\\\\').replace(/,/gi, '\\,');
                            //if (!bDisplayKeys)
                            if(!isRequest)// && !isThreePartDataType)
                                sResult += ',' + sText.replace(/\\/gi, '\\\\').replace(/,/gi, '\\,');

                            sDisplayValues += bDisplayKeys ? sValue : sText;
                        }

                        return (oItemsInput.value = sResult);
                    };

                    sCallbackRef = getUrlQueryParam('targetId');
                    sSaveCallbackRef = 'a' + sCallbackRef;
                    sGetCallbackRef = 'b' + sCallbackRef;
                    sCsvCallbackRef = 'c' + sCallbackRef;

                    getValuesFromCsv = window.opener[sCsvCallbackRef];

                    bDisplayKeys = (getUrlQueryParam('displayKeys') || 0) * 1;

                    getSelectedItems(window.opener[sGetCallbackRef]());

                    mAddButton.onclick = addItems;
                    mRemoveButton.onclick = removeItems;
                    mSaveButton.onclick = function () {
                        var sResult = onSave();
                        window.opener[sSaveCallbackRef](sDisplayValues, sResult);
                        window.close();
                    };
                    mCloseButton.onclick = function () { window.close(); };

                    window.onunload = function () {
                        window.opener[sSaveCallbackRef]();
                    }

                    mTemp = mSearchButton.onclick;
                    mSearchButton.onclick = function () { onSave(true); mTemp(); };
                })(
                    'lbxMultiselItems',
                    'lbxMultiselSelectedItems',
                    'multiselItemsAdd',
                    'multiselItemsRemove',
                    'btnMultiselSave',
                    'btnMultiselClose',
                    '<%=hidSelectedItems.ClientID %>',
                    'btnMultiselSearch',
                    <%=IsThreePartDataItemType.ToString().ToLower() %>
                );

            </script>
        </div>
    </form>
</body>
</html>
