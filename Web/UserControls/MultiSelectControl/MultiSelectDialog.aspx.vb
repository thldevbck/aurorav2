﻿Option Strict On
Option Explicit On

Imports System
Imports System.Collections.Generic
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Common

Imports ASP.usercontrols_multiselectcontrol_multiselect_ascx

Partial Class UserControls_MultiSelectControl_MultiSelectDialog
    Inherits System.Web.UI.Page

    Protected mItems As New List(Of MultiSelectItem)
    Protected mSelectedItems As New List(Of MultiSelectItem)

    Protected mDisplayKeys As Nullable(Of Boolean)
    Protected mCountryCode As String = ""
    Protected mCompanyCode As String = ""

    Protected mSelectType As Nullable(Of MultiSelectType)

    Protected mRetainValuesOnPostback As Nullable(Of Boolean)

    Protected ReadOnly Property SelectType As MultiSelectType
        Get
            If Not mSelectType.HasValue Then
                Dim itemTypeValue As String = Request.QueryString("itemTypeId")
                Dim itemType As MultiSelectType

                If Not String.IsNullOrEmpty(itemTypeValue) Then
                    Try
                        itemType = CType(itemTypeValue, MultiSelectType)
                    Catch ex As Exception
                    End Try
                End If

                mSelectType = itemType
            End If

            Return mSelectType.Value
        End Get
    End Property

    Protected ReadOnly Property IsThreePartDataItemType As Boolean
        Get
            Return SelectType = MultiSelectType.FleetModels OrElse SelectType = MultiSelectType.VehicleProducts
        End Get
    End Property

    Protected ReadOnly Property DisplayKeys As Boolean
        Get
            If Not mDisplayKeys.HasValue Then
                Dim strDisplayKeys As String = Me.Request.QueryString("displayKeys")
                Dim bolDisplayKeys As Boolean = False
                Boolean.TryParse(strDisplayKeys, bolDisplayKeys)
                mDisplayKeys = bolDisplayKeys OrElse strDisplayKeys = "1"
            End If
            Return mDisplayKeys.Value
        End Get
    End Property

    Protected ReadOnly Property RetainValuesOnPostback As Boolean
        Get
            If Not mRetainValuesOnPostback.HasValue Then
                Dim strValue As String = Me.Request.QueryString("retainValues")
                Dim bolValue As Boolean = False
                Boolean.TryParse(strValue, bolValue)
                mRetainValuesOnPostback = bolValue OrElse strValue = "1"
            End If
            Return mRetainValuesOnPostback.Value
        End Get
    End Property

    Protected ReadOnly Property CountryCode As String
        Get
            If mCountryCode = "" Then
                mCountryCode = Me.Request.QueryString("countryCode")
                If String.IsNullOrEmpty(mCountryCode) Then
                    mCountryCode = Nothing
                End If
            End If
            Return mCountryCode
        End Get
    End Property

    Protected ReadOnly Property CompanyCode As String
        Get
            If mCompanyCode = "" Then
                mCompanyCode = Me.Request.QueryString("companyCode")
                If String.IsNullOrEmpty(mCompanyCode) Then
                    mCompanyCode = Nothing
                End If
            End If
            Return mCompanyCode
        End Get
    End Property


    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        Page.Title = GetPopupName()

        Response.AddHeader("X-UA-Compatible", "IE=7")

        If Page.IsPostBack Then
            HandleAsyncPostback()
        End If
    End Sub

    Protected Function GetPopupName() As String
        Select Case SelectType
            Case MultiSelectType.Locations
                Return "Locations"
            Case MultiSelectType.FleetModels
                Return "Fleet Models"
            Case MultiSelectType.VehicleProducts
                Return "Products - Vehicles"
        End Select

        Return Nothing
    End Function

    ''' <summary>
    ''' Returns true if the postback was asynchronous.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function HandleAsyncPostback() As Boolean
        Dim targetControl As String = Request.Form("__EVENTTARGET")
        If String.Equals(targetControl, pnlItemsList.ClientID) Then
            SearchItems(txtItemSearch.Value, SelectType)
            MoveSelectedItems(False)

            pnlItemsList.Update()

            Return True
        ElseIf String.Equals(targetControl, hidSelectedItems.ClientID) Then
            GetSelectedItemsCsv()
            pnlItemsList.Update()
        End If

        Return False
    End Function

    Protected Sub GetSelectedItemsCsv()
        Dim selectedItems As String = hidSelectedItems.Value

        If String.IsNullOrEmpty(selectedItems) Then
            selectedItems = ""
        ElseIf RetainValuesOnPostback Then
            Dim lstSelectedItems As List(Of String) = UserControls_SelectControl_MultiSelect.GetValuesFromCsv(selectedItems)

            Dim i As Integer = lstSelectedItems.Count - 1
            While i >= 0
                If i Mod 2 = 1 Then
                    lstSelectedItems.RemoveAt(i)
                End If

                i -= 1
            End While

            selectedItems = UserControls_SelectControl_MultiSelect.GetCsv(lstSelectedItems)
        End If

        Dim repository As New MultiSelectItemRepository(SelectType)
        Dim lstItems As List(Of MultiSelectItem) = repository.GetItems("", CountryCode, CompanyCode, selectedItems)

        hidSelectedItems.Value = UserControls_SelectControl_MultiSelect.GetSeparatedValueString(lstItems, True, True, True, True)
    End Sub

    Protected Sub SearchItems(ByVal partialValue As String, ByVal itemType As MultiSelectType)
        'If String.IsNullOrEmpty(partialValue) Then
        '    Return
        'End If

        If partialValue Is Nothing Then
            partialValue = ""
        Else
            partialValue = partialValue.Trim()
        End If

        'If partialValue.Length = 0 Then
        '    Return
        'End If

        Dim repository As New MultiSelectItemRepository(itemType)

        Dim lstItems As List(Of MultiSelectItem) = repository.GetItems(partialValue, CountryCode, CompanyCode)

        mItems = lstItems
    End Sub



    Protected Sub MoveSelectedItems(ByVal populateTargetList As Boolean)
        'Dim lstSelectedItems As List(Of MultiSelectItem) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetItems(hidSelectedItems.Value, UseKeysOnly)
        Dim lstSelectedItems As List(Of MultiSelectItem) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetItems(hidSelectedItems.Value, True)

        For Each selectedItem As MultiSelectItem In lstSelectedItems

            If mItems IsNot Nothing Then
                Dim intItemCount As Integer = mItems.Count
                Dim intItemIndex As Integer = 0

                While intItemIndex < intItemCount
                    Dim item As MultiSelectItem = mItems(intItemIndex)

                    If item.Key IsNot Nothing AndAlso selectedItem.Key IsNot Nothing Then
                        Dim type As Type = item.Key.GetType()
                        If type IsNot selectedItem.Key.GetType() Then
                            selectedItem.Key = Convert.ChangeType(selectedItem.Key, type)
                        End If
                    End If


                    If Object.Equals(item.Key, selectedItem.Key) Then
                        mItems.RemoveAt(intItemIndex)

                        intItemCount = mItems.Count
                    Else
                        intItemIndex = intItemIndex + 1
                    End If
                End While
            End If

            If populateTargetList Then
                mSelectedItems.Add(selectedItem)
            End If
        Next

    End Sub

    
    Protected Function GetItemKey(ByVal item As MultiSelectItem) As String
        Dim key As Object = item.Key

        If Not IsThreePartDataItemType OrElse key Is Nothing OrElse item.Value Is Nothing Then
            If key Is Nothing Then
                Return Nothing
            End If
            Return key.ToString()
        End If

        Dim pair(1) As String

        pair(0) = key.ToString()
        pair(1) = item.Value.ToString()

        Return UserControls_SelectControl_MultiSelect.GetCsv(pair)
    End Function

    Protected Function GetItemValue(ByVal item As MultiSelectItem) As String
        If Not IsThreePartDataItemType Then
            Return CStr(item.Value)
        End If

        Return CStr(item.Value) + " - " + item.Description
    End Function
End Class
