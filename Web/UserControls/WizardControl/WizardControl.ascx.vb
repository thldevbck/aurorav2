Imports System.ComponentModel
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.Design
Imports System.Drawing.Design

Partial Class UserControls_WizardControl
    Inherits System.Web.UI.UserControl

    <DefaultValue(CType(Nothing, String)), _
     Category("Default"), _
     PersistenceMode(PersistenceMode.InnerProperty), _
     Description("The collection of items in the list."), _
     Editor("System.Web.UI.Design.WebControls.ListItemsCollectionEditor,System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", GetType(UITypeEditor)), _
     MergableProperty(False)> _
    Public ReadOnly Property Items() As ListItemCollection
        Get
            Return itemDropDownList.Items
        End Get
    End Property

    Public Property SelectedIndex() As Integer
        Get
            If TypeOf ViewState("SelectedIndex") Is Integer Then
                Return CType(ViewState("SelectedIndex"), Integer)
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("SelectedIndex") = value
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            If TypeOf ViewState("Enabled") Is Boolean Then
                Return CType(ViewState("Enabled"), Boolean)
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Enabled") = value
        End Set
    End Property

    Public Event SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Protected Sub itemDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles itemDropDownList.SelectedIndexChanged
        SelectedIndex = itemDropDownList.SelectedIndex
        RaiseEvent SelectedIndexChanged(sender, e)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("WizardControl.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "WizardControl.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("WizardControl.css") & """ />")
        End If

        itemDropDownList.SelectedIndex = SelectedIndex

        Me.wizardPanel.Controls.Clear()
        Dim index As Integer = 0
        For Each listItem As ListItem In Me.Items
            If index > 0 Then
                Me.wizardPanel.Controls.Add(New LiteralControl("<span class=""seperator"">&nbsp;&nbsp;&gt;&nbsp;&nbsp;</span>"))
            End If

            Dim image As New Image()
            If Me.SelectedIndex = index Then
                image.ImageUrl = (index + 1).ToString() + "_on.gif"
            Else
                image.ImageUrl = (index + 1).ToString() + "_off.gif"
            End If
            Me.wizardPanel.Controls.Add(image)

            Dim textLabel As New Label
            textLabel.Text = Server.HtmlEncode(listItem.Text)
            If index = Me.SelectedIndex Then
                textLabel.CssClass = "selected"
            ElseIf index > Me.SelectedIndex Then
                textLabel.CssClass = "next"
            End If

            If Me.Enabled AndAlso Me.SelectedIndex > index Then
                Dim hyperLink As New HyperLink()
                hyperLink.ID = index
                hyperLink.Attributes.Add("href", "#")
                hyperLink.Attributes.Add("onclick", "document.getElementById ('" & itemDropDownList.ClientID & "').selectedIndex = " & index & "; javascript:setTimeout('__doPostBack(\'" & itemDropDownList.ClientID & "\',\'\')', 0)")
                Me.wizardPanel.Controls.Add(hyperLink)

                hyperLink.Controls.Add(textLabel)
            Else
                Me.wizardPanel.Controls.Add(textLabel)
            End If

            index += 1
        Next
    End Sub

End Class
