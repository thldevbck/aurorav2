﻿function SelectMeOnly(SourceElement)
{
    var parentTBODY = SourceElement.parentNode.parentNode.parentNode;
    
    var sourceElementCheckBox = SourceElement.parentNode.parentNode.children[1].children[0];

    if(sourceElementCheckBox.type=="checkbox")
    {
        if(sourceElementCheckBox.checked)
        {
            for (var i = 0;i<parentTBODY.children.length;i++)
            {
                if (parentTBODY.children[i].children[0].tagName == "TH")
                {
                    continue;
                }
                var oRadio = parentTBODY.children[i].children[0].children[0];
                var oCheckBox = parentTBODY.children[i].children[1].children[0];
                
                if(oRadio.type=="radio")
                {
                    if(oRadio.id == SourceElement.id)
                    {
                        if(oCheckBox.type=="checkbox")
                        {
                            oRadio.checked = oCheckBox.checked;
                        }
                    }
                    else
                    { 
                        oRadio.checked = false;
                    }
                }
            }
        }
        else
        {
            SourceElement.checked = false;
        }
    }
    

}

function ClearMyRadio(SourceElement)
{
    var oTR = SourceElement.parentNode.parentNode;
    var oTD = oTR.children[0];
    var oRadio = oTD.children[0];
    
    if(oRadio.type=="radio")
    {
        if(!SourceElement.checked)
        {
            oRadio.checked = false;
        }
    }
}
