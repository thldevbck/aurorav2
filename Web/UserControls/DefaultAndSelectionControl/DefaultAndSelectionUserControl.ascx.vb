﻿Imports System.Text
Imports System.Data

Partial Public Class DefaultAndSelectionUserControl
    Inherits System.Web.UI.UserControl

    'Protected WithEvents grdMain As Global.System.Web.UI.WebControls.GridView

    'Protected WithEvents hdnCollectionName As Global.System.Web.UI.WebControls.HiddenField

    'Protected WithEvents hdnShowHeader As Global.System.Web.UI.WebControls.HiddenField


    Public Property CollectionName() As String
        Get
            Return hdnCollectionName.Value
        End Get
        Set(ByVal value As String)
            hdnCollectionName.Value = value
        End Set
    End Property

    Public Property DataSource() As DataTable
        Get
            Return CType(ViewState(Me.ClientID & "DataSet"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            ViewState(Me.ClientID & "DataSet") = value
        End Set
    End Property

    Public Property ShowHeader() As Boolean
        Get
            If hdnShowHeader.Value.Trim().Equals(String.Empty) Then
                Return False
            Else
                Return CBool(hdnShowHeader.Value)
            End If
        End Get
        Set(ByVal value As Boolean)
            hdnShowHeader.Value = CStr(value)
        End Set
    End Property

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    If Not IsPostBack Then
    '        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("DefaultAndSelection.js") Then
    '            Me.Page.ClientScript.RegisterClientScriptInclude("DefaultAndSelection.js", ".." & Me.ResolveUrl("DefaultAndSelection.js"))
    '        End If
    '    End If
    'End Sub

    Public Overrides Sub DataBind()
        grdMain.ShowHeader = ShowHeader
        If DataSource Is Nothing Then
            grdMain.DataSource = DataSource
        Else
            Dim dvSort As New DataView(DataSource)
            dvSort.Sort = "ItemCode"
            grdMain.DataSource = dvSort.ToTable()
        End If

        grdMain.DataBind()
    End Sub

    Protected Sub grdMain_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdMain.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim radDefault As RadioButton
            Dim chkSelected As CheckBox

            radDefault = CType(e.Row.FindControl("radDefault"), RadioButton)

            chkSelected = CType(e.Row.FindControl("chkSelected"), CheckBox)

            Dim celDefault, celSelected As System.Web.UI.WebControls.TableCell

            celDefault = e.Row.Cells(0)
            celSelected = e.Row.Cells(1)

            If celDefault.Text.Trim().Equals(String.Empty) Then
                radDefault.Checked = False
            Else
                radDefault.Checked = CBool(celDefault.Text.Trim())
            End If

            If celSelected.Text.Trim().Equals(String.Empty) Then
                chkSelected.Checked = False
            Else
                chkSelected.Checked = CBool(celSelected.Text.Trim())
            End If

            Dim hdnItemCode As HiddenField
            hdnItemCode = CType(e.Row.FindControl("hdnItemCode"), HiddenField)
            hdnItemCode.Value = e.Row.Cells(5).Text

            'radDefault.Enabled = chkSelected.Checked

        ElseIf e.Row.RowType = DataControlRowType.Header Then
            e.Row.Cells(6).Text = CollectionName
        End If

        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
        e.Row.Cells(4).Visible = False
        e.Row.Cells(5).Visible = False
    End Sub

    Public Function GetCurrentData() As DataTable
        Dim dtResult As New DataTable
        dtResult.Columns.Add("ItemCode")
        dtResult.Columns.Add("ItemName")
        dtResult.Columns.Add("Default")
        dtResult.Columns.Add("Selected")
        For Each oRow As GridViewRow In grdMain.Rows
            dtResult.Rows.Add(oRow.Cells(5).Text, _
                              oRow.Cells(4).Text, _
                              CType(oRow.FindControl("radDefault"), RadioButton).Checked, _
                              CType(oRow.FindControl("chkSelected"), CheckBox).Checked)
        Next
        Return dtResult
    End Function

    Public Function HasChanged() As Boolean
        Dim bHasChanged As Boolean = False
        Dim dtNewData As DataTable = GetCurrentData()

        For i As Integer = 0 To DataSource.Rows.Count - 1
            If DataSource.Rows(i)("ItemCode").ToString() = dtNewData.Rows(i)("ItemCode").ToString() Then
                If DataSource.Rows(i)("Default").ToString() <> dtNewData.Rows(i)("Default").ToString() _
                   OrElse _
                   DataSource.Rows(i)("Selected").ToString() <> dtNewData.Rows(i)("Selected").ToString() _
                Then
                    bHasChanged = True
                    Exit For
                End If
            End If
        Next

        Return bHasChanged
    End Function

    Public Function GetCSVSelectedList() As String
        Dim sbCSVList As New StringBuilder
        Dim dtNewData As DataTable = GetCurrentData()

        For Each oRow As DataRow In dtNewData.Rows
            If CBool(oRow("Selected")) Then
                sbCSVList.Append(oRow("ItemCode").ToString() & ",")
            End If
        Next

        Return sbCSVList.ToString().TrimEnd(","c)
        
    End Function

    Public Function GetDefault() As String
        Dim dtNewData As DataTable = GetCurrentData()

        For Each oRow As DataRow In dtNewData.Rows
            If CBool(oRow("Selected")) AndAlso CBool(oRow("Default")) Then
                Return oRow("ItemCode").ToString()
            End If
        Next

        ' couldnt find anything?
        Return String.Empty
    End Function

    Public Function IsValid(ByRef ErrorMessage As String) As Boolean
        Dim dtNewData As DataTable = GetCurrentData()

        Dim bSelectionMade As Boolean = False
        Dim bDefaultChosen As Boolean = False
        For Each oRow As DataRow In dtNewData.Rows
            If CBool(oRow("Selected")) Then
                bSelectionMade = True
                If CBool(oRow("Default")) Then
                    bDefaultChosen = True
                End If
            Else
                If CBool(oRow("Default")) Then
                    ' default chose in a non-selected row
                    ErrorMessage = "Default must be chosen from selected currencies"
                    Return False
                End If
            End If
        Next

        If bSelectionMade Then
            If bDefaultChosen Then
                Return True
            Else
                ErrorMessage = "Please select a default currency"
                Return False
            End If
        Else
            If bDefaultChosen Then
                ErrorMessage = "Please select a currency before assigning a default"
                Return False
            Else
                Return True
            End If
        End If

        Return False
    End Function
End Class