﻿function showSlotUserControlViaClient(id) 
{ 
    var slotBehavior = $find(id);
    slotBehavior.show();
}

function hideSlotUserControlViaClient(id) 
{
    var slotBehavior = $find(id);
    slotBehavior.hide();
    return false;
}
