﻿Imports Aurora.Common
Imports Aurora.Booking.Services.Rental
Imports System.Data

Partial Class UserControls_SlotAvailableControl_SlotAvailableControl
    Inherits AuroraUserControl

    Public Delegate Sub SlotUserControlEventHandler(sender As Object, isSaveButton As Boolean, param As String)

    Public Event PostBack As SlotUserControlEventHandler

    Private Const JS_name As String = "SlotAvailableControl.js"

    Private _NoOfBulkBookings As Integer
    Public Property NoOfBulkBookings As Integer
        Get
            Return _NoOfBulkBookings
        End Get
        Set(value As Integer)
            _NoOfBulkBookings = value
        End Set
    End Property

    Public WriteOnly Property SaveButtonText As String
        Set(value As String)
            SaveButton.Text = value
        End Set
    End Property

    Public WriteOnly Property SkipButtonText As String
        Set(value As String)
            SkipButton.Text = value
        End Set
    End Property

    Public ReadOnly Property Param() As String
        Get
            Return paramHiddenField.Value
        End Get
    End Property

    Private _SlotId As String
    Private ReadOnly Property SlotId As String
        Get
            Return _SlotId
        End Get
    End Property

    Private _SlotDescription As String
    Private ReadOnly Property SlotDescription As String
        Get
            Return _SlotDescription
        End Get
    End Property

    Public Property Title() As String
        Get
            Return titleLabel.Text
        End Get
        Set(ByVal value As String)
            titleLabel.Text = value
        End Set
    End Property

    Private _btnCancel_AutoPostBack As Boolean = True
    Public Property btnCancel_AutoPostBack() As Boolean
        Get
            Return _btnCancel_AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            _btnCancel_AutoPostBack = value
        End Set
    End Property

    Protected Sub UserControls_SlotAvailableControl_SlotAvailableControl_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        programmaticSlot.BehaviorID = Me.ClientID & "Behaviour"
    End Sub

    Protected Sub UserControls_SlotAvailableControl_SlotAvailableControl_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered(JS_name) Then
            Me.Page.ClientScript.RegisterClientScriptInclude(JS_name, Me.ResolveUrl(JS_name))
        End If
    End Sub

    Public Sub Hide()
        programmaticSlot.Hide()
    End Sub

    Public Sub Show(ByVal AvpId As String, _
                    Optional RentalId As String = "", _
                    Optional BpdId As String = "", _
                    Optional PickupDate As String = "", _
                    Optional PickupLocCode As String = "", _
                    Optional AgnId As String = "", _
                    Optional SapId As String = "", _
                    Optional PkgId As String = "", _
                    Optional ByRef Display As Boolean = True, _
                    Optional Status As String = "")


        Me.AvpId = AvpId
        Me.RentalId = RentalId
        Me.BpdId = BpdId
        Me.PickupDate = PickupDate
        Me.PickupLocCode = PickupLocCode
        Me.AgnId = AgnId
        Me.SapId = SapId
        Me.PkgId = PkgId
        Me.RntStatus = Status



        Me.errorLabel.Text = ""
        If Not String.IsNullOrEmpty(AvpId) Then
            Me.paramHiddenField.Value = AvpId
            SlotRentalId = ""
        Else
            Me.paramHiddenField.Value = ""
            SlotRentalId = RentalId
        End If


        If (HasDataToPopulateAvailableSlot(AvpId, SlotRentalId, BpdId, PickupDate, PickupLocCode, AgnId, SapId, PkgId, Status) = True) Then
            programmaticSlot.Show()
            _IsThereAnySlot = True
        Else
            programmaticSlot.Hide()
            _IsThereAnySlot = False
            Display = False
        End If

    End Sub

    Private _IsThereAnySlot As Boolean
    Public Property IsThereAnySlot As Boolean
        Get
            Return _IsThereAnySlot
        End Get
        Set(value As Boolean)
            _IsThereAnySlot = value
        End Set
    End Property

    Private Function HasDataToPopulateAvailableSlot(AvpId As String, _
                                                    Optional RentalId As String = "", _
                                                    Optional BpdId As String = "", _
                                                    Optional PickupDate As String = "", _
                                                    Optional PickupLocCode As String = "", _
                                                    Optional AgnId As String = "", _
                                                    Optional SapId As String = "", _
                                                    Optional PkgId As String = "", _
                                                    Optional newRntStatus As String = "") As Boolean

        Try
            Dim result As DataSet


            If (String.IsNullOrEmpty(AvpId) And Not String.IsNullOrEmpty(RentalId)) Then
                Dim newCheckOutDate As Date = Date.MinValue
                If (Not String.IsNullOrEmpty(PickupDate)) Then
                    newCheckOutDate = Aurora.Common.Utility.ParseDateTime(PickupDate, Aurora.Common.Utility.SystemCulture)
                End If
                
                result = GetAndValidateAvailableSlot(RentalId, Nothing, BpdId, "0", newCheckOutDate, PickupLocCode, AgnId, SapId, PkgId, newRntStatus)
            Else
                result = GetAvailableSlot(Nothing, AvpId, Nothing, "0")
            End If


            If ((result.Tables.Count - 1) = -1) Then
                ddlArrivalTime.Enabled = False
                Return False
            End If

            If (result.Tables(0).Rows.Count - 1 = -1) Then
                ddlArrivalTime.Enabled = False
                Return False
            End If


            ddlArrivalTime.Enabled = True


            ddlArrivalTime.Items.Clear()
            ddlArrivalTime.DataSource = result
            ddlArrivalTime.DataTextField = "SlotDesc"
            ddlArrivalTime.DataValueField = "SlotId"
            ddlArrivalTime.DataBind()




            Dim selectedindex As Integer = -1

            For Each row As DataRow In result.Tables(0).Rows
                Dim item As New ListItem
                Dim slotid As Integer = Convert.ToInt16(row("SlotId"))
                Dim allocated As Integer = 0
                Dim booked As Integer = 0
                Dim available As Integer = 0
                Dim isSlotAvailable As Integer = -1
                Dim isDefault As Integer = -1
                Dim isAllDaySlot As Integer = 1

                If (Not IsDBNull(row("IsAllDaySlot"))) Then
                    isAllDaySlot = Convert.ToInt16(row("IsAllDaySlot"))
                End If

                If (Not IsDBNull(row("AllocatedNumbers"))) Then
                    allocated = Convert.ToInt16(row("AllocatedNumbers"))
                End If

                If (Not IsDBNull(row("BookedNumbers"))) Then
                    booked = Convert.ToInt16(row("BookedNumbers"))
                End If

                If (Not IsDBNull(row("AvailableNumbers"))) Then
                    available = Convert.ToInt16(row("AvailableNumbers"))
                End If

                If (Not IsDBNull(row("IsSelected"))) Then
                    isDefault = Convert.ToInt16(row("IsSelected"))
                End If

                If (Not IsDBNull(row("IsSlotAvailable"))) Then
                    isSlotAvailable = Convert.ToInt16(row("isSlotAvailable"))
                End If


                If (CInt(row("IsSelected") = 1) And NoOfBulkBookings <= available) Then
                    item = ddlArrivalTime.Items.FindByValue(slotid)
                    item.Selected = True
                    item.Attributes.Add("default", "Y")
                End If


                If (isAllDaySlot = 0) Then
                    If (CInt(row("IsSlotAvailable") = 0) Or NoOfBulkBookings > available) Then
                        item = ddlArrivalTime.Items.FindByValue(slotid)

                        ''rev:mia 13jan2014-addition of supervisor override password
                        If (OverrideIsOK = False) Then
                            item.Attributes.Add("disabled", "disabled")
                        End If
                        overrideCheckbox.Enabled = IIf(OverrideIsOK = False, True, False)
                    End If
                End If


                item = ddlArrivalTime.Items.FindByValue(slotid)
                Dim tooltip As String = String.Format("Allocated: {0}, Booked: {1}, Available: {2}, BookingSummary: {3}, IsSlotAvailable: {4}, IsSelected: {5}", allocated, booked, available, NoOfBulkBookings, isSlotAvailable, isDefault)
                item.Attributes.Add("title", tooltip)

                ddlArrivalTime.Attributes.Add("onchange", "MarkOptionAsDisabledPageLoad('" & selectedindex & "')")
            Next

            Dim SelectedRows() As DataRow = result.Tables(0).Select("IsSelected = 1 and AvailableNumbers >= " & NoOfBulkBookings)
            If (SelectedRows.Length - 1 = -1) Then
                ddlArrivalTime.Items.Insert(0, New ListItem("--(Please select slot)--", -1))
            End If

            Dim NoOfSlotsAvailable() As DataRow = result.Tables(0).Select("IsSlotAvailable = 0")
            If (NoOfSlotsAvailable.Length - 1 = -1) Then
                ''if items are available. there is no need to activate the override password
                overrideCheckbox.Enabled = False
            End If
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function

    Protected Sub SkipButton_Click(sender As Object, e As System.EventArgs) Handles SkipButton.Click
        programmaticSlot.Hide()
        RaiseEvent PostBack(Me, False, Param)
    End Sub

    Protected Sub SaveButton_Click(sender As Object, e As System.EventArgs) Handles SaveButton.Click
        Dim avpid As String = Me.paramHiddenField.Value
        If (Not String.IsNullOrEmpty(avpid)) Then
            If (ddlArrivalTime.SelectedValue = -1) Then
                Show(avpid)
                Me.errorLabel.Text = "Time Slot is mandatory"
                Return
            End If
        Else
            If (ddlArrivalTime.SelectedValue = -1) Then
                Show(String.Empty, SlotRentalId)
                Me.errorLabel.Text = "Time Slot is mandatory"
                Return
            End If
        End If


        If (Not String.IsNullOrEmpty(ddlArrivalTime.SelectedValue)) Then
            _SlotId = ddlArrivalTime.SelectedValue
            _SlotDescription = ddlArrivalTime.SelectedItem.Text
            Me.paramHiddenField.Value = String.Concat(avpid, "|", _SlotId, "|", _SlotDescription, "|", SlotRentalId)
            RaiseEvent PostBack(Me, True, Param)

        Else
            Me.errorLabel.Text = "Time Slot is mandatory"
            programmaticSlot.Show()
        End If

    End Sub

    Protected Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        errorTableRow.Style.Add("visibility", IIf(String.IsNullOrEmpty(errorLabel.Text), "hidden", "visible"))

        If btnCancel_AutoPostBack Then
            SkipButton.Attributes.Add("onclick", "return hideConfirmationBoxViaClient('" & programmaticSlot.BehaviorID & "');")
        Else
            SkipButton.Attributes.Remove("onclick")
        End If
    End Sub

    Public Function IsSlotRequired(avpid As String, _
                                   Optional RentalId As String = "", _
                                   Optional BpdId As String = "", _
                                   Optional PickupDate As String = "", _
                                   Optional PickupLocCode As String = "", _
                                   Optional AgnId As String = "", _
                                   Optional SapId As String = "", _
                                   Optional PkgId As String = "", _
                                   Optional RntStatus As String = "") As Boolean

        ''Dim result As DataSet = GetAvailableSlot(Nothing, avpid, Nothing, "0")
        Dim result As DataSet
        If (String.IsNullOrEmpty(avpid) And Not String.IsNullOrEmpty(RentalId)) Then
            Dim newCheckOutDate As Date = Date.MinValue
            If (Not String.IsNullOrEmpty(PickupDate)) Then
                newCheckOutDate = Aurora.Common.Utility.ParseDateTime(PickupDate, Aurora.Common.Utility.SystemCulture)
            End If
            result = GetAndValidateAvailableSlot(RentalId, Nothing, BpdId, "0", newCheckOutDate, PickupLocCode, AgnId, SapId, PkgId, RntStatus)

        Else
            result = GetAvailableSlot(Nothing, avpid, Nothing, "0")
        End If


        IsThereAnySlot = False
        If ((result.Tables.Count - 1) = -1) Then
            Return False
        End If

        If (result.Tables(0).Rows.Count - 1 = -1) Then
            Return False
        End If
        IsThereAnySlot = True
        Return True
    End Function


#Region "rev:mia 13jan2014-addition of supervisor override password"

    Private Property OverrideIsOK As Boolean
        Get
            Return CBool(ViewState("OverrideIsOK"))
        End Get
        Set(value As Boolean)
            ViewState("OverrideIsOK") = value
        End Set
    End Property

    Protected Sub overrideCheckbox_CheckedChanged(sender As Object, e As System.EventArgs) Handles overrideCheckbox.CheckedChanged

        If (overrideCheckbox.Checked) Then
            SlotSupervisorOverrideControl.Show()
        Else
            SlotSupervisorOverrideControl.Hide()
        End If
    End Sub

    Protected Sub Page_Unload(sender As Object, e As System.EventArgs) Handles Me.Unload
        overrideCheckbox.Checked = False
    End Sub

    Public Property SupervisorOverride As Boolean
        Get
            Return overrideCheckbox.Checked
        End Get
        Set(value As Boolean)
            overrideCheckbox.Checked = value
        End Set
    End Property

    Protected Sub SlotSupervisorOverrideControl_PostBack(sender As Object, isOkButton As Boolean, param As String) Handles SlotSupervisorOverrideControl.PostBack
        If isOkButton Then
            Dim sReturnMsg As String = ""
            sReturnMsg = Aurora.Common.Service.CheckUser(SlotSupervisorOverrideControl.Login, SlotSupervisorOverrideControl.Password, "OLMAN")

            If (sReturnMsg = "True") Then
                OverrideIsOK = True
                SlotSupervisorOverrideControl.Hide()

                If (Not String.IsNullOrEmpty(Me.paramHiddenField.Value) And String.IsNullOrEmpty(SlotRentalId)) Then
                    Show(Me.paramHiddenField.Value)
                Else
                    Show(Nothing, SlotRentalId, Me.BpdId, Me.PickupDate, Me.PickupLocCode, Me.AgnId, Me.SapId, Me.PkgId, True, Me.RntStatus)
                End If
            Else
                SlotSupervisorOverrideControl.Show()
                MyBase.CurrentPage.SetErrorShortMessage(sReturnMsg)
            End If
        End If
        OverrideIsOK = False
    End Sub

    Private Property SlotRentalId As String
        Get
            Return ViewState("RentalId")
        End Get
        Set(value As String)
            ViewState("RentalId") = value
        End Set
    End Property

    property CacheSlotResult() As DataSet
        Get
            Return ViewState("CacheSlotResult")
        End Get
        Set(value As DataSet)
            ViewState("CacheSlotResult") = value
        End Set
    End Property

    Property AvpId As String
        Get
            Return ViewState("AvpId")
        End Get
        Set(value As String)
            ViewState("AvpId") = value
        End Set
    End Property

    Property RentalId As String
        Get
            Return ViewState("RentalId")
        End Get
        Set(value As String)
            ViewState("RentalId") = value
        End Set
    End Property

    Property BpdId As String
        Get
            Return ViewState("BpdId")
        End Get
        Set(value As String)
            ViewState("BpdId") = value
        End Set
    End Property

    Property PickupDate As String
        Get
            Return ViewState("PickupDate")
        End Get
        Set(value As String)
            ViewState("PickupDate") = value
        End Set
    End Property

    Property PickupLocCode As String
        Get
            Return ViewState("PickupLocCode")
        End Get
        Set(value As String)
            ViewState("PickupLocCode") = value
        End Set
    End Property

    Property AgnId As String
        Get
            Return ViewState("AgnId")
        End Get
        Set(value As String)
            ViewState("AgnId") = value
        End Set
    End Property

    Property SapId As String
        Get
            Return ViewState("SapId")
        End Get
        Set(value As String)
            ViewState("SapId") = value
        End Set
    End Property

    Property PkgId As String
        Get
            Return ViewState("PkgId")
        End Get
        Set(value As String)
            ViewState("PkgId") = value
        End Set
    End Property

    Property RntStatus As String
        Get
            Return ViewState("RntStatus")
        End Get
        Set(value As String)
            ViewState("RntStatus") = value
        End Set
    End Property

#End Region




End Class
