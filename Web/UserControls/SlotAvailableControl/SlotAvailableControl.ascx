﻿<%@ Control Language="VB" 
            AutoEventWireup="false" 
            CodeFile="SlotAvailableControl.ascx.vb" 
            Inherits="UserControls_SlotAvailableControl_SlotAvailableControl" %>



<%@ Register src="../SupervisorOverrideControl/SupervisorOverrideControl.ascx" tagname="SupervisorOverrideControl" tagprefix="uc1" %>


<asp:HiddenField ID="paramHiddenField" runat="server" Value="" />

<asp:Button 
    runat="server" 
    ID="hiddenTargetControlForSlot" 
    Style="display:none"    />
    <%--useSubmitBehavior="false"--%>

<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="programmaticSlot"
    BehaviorID="programmaticSlotBehavior" 
    TargetControlID="hiddenTargetControlForSlot"
    PopupControlID="programmaticSlotPanel" 
    BackgroundCssClass="modalBackground"
    DropShadow="True" 
    PopupDragHandleControlID="programmaticSlotDragHandle" />

<%----%>
<asp:Panel 
    runat="server" 
    CssClass="modalPopup" 
    ID="programmaticSlotPanel" 
    Style="display:none; padding: 10px;"
    DefaultButton="SaveButton"
    Width="500px">

     <asp:Panel 
        runat="Server" ID="programmaticSlotDragHandle"
        CssClass="modalPopupTitle">
        <asp:Label ID="titleLabel" runat="Server" Text="Slot Availability" />
    </asp:Panel>

     <br />
    <table width="100%">
        <tr>
            <td style="width:150px;" align="left">
              Override Slot:
            </td>
            <td>
                <asp:CheckBox runat="server" ID="overrideCheckbox" AutoPostBack="true" />
            </td>
        </tr>
        <tr>
            <td style="width:150px;" align="left">
                Current Available:
            </td>
            <td>
                <asp:DropDownList ID="ddlArrivalTime" runat="server" EnableViewState="true" Style="width: 170px" autopostback="false"></asp:DropDownList>
            </td>
        </tr>
        <tr id="errorTableRow" runat="Server" style="visibility:hidden">
               <td colspan="2"><asp:Image ID="errorImage" runat="server" ImageUrl="../../images/error.gif" />&nbsp;<asp:Label ID="errorLabel" runat="server" /></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="right" width="100%">
                <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="Button_Standard Button_OK" />&nbsp;
                <asp:Button ID="SkipButton" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                
            </td>
        </tr>
    </table>
  </asp:Panel>

<uc1:SupervisorOverrideControl ID="SlotSupervisorOverrideControl" runat="server"  />