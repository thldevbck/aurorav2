''' <summary>
''' A simple panel for holding messages.  Can be used client or server side.  Multiple instances must each be given a unique "Code".
''' </summary>
''' <remarks></remarks>
Partial Class UserControls_MessagePanelControl
    Inherits System.Web.UI.UserControl

    Public Overrides ReadOnly Property ClientID() As String
        Get
            Return messagePanel.ClientID
        End Get
    End Property

    Public Property CssClass() As String
        Get
            Return messagePanel.CssClass
        End Get
        Set(ByVal value As String)
            messagePanel.CssClass = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return CType(ViewState("Text"), String)
        End Get
        Set(ByVal value As String)
            ViewState("Text") = value
        End Set
    End Property

    Public Property MessageType() As AuroraHeaderMessageType
        Get
            If ViewState("MessageType") IsNot Nothing Then
                Return CType(ViewState("MessageType"), AuroraHeaderMessageType)
            Else
                Return AuroraHeaderMessageType.Information
            End If
        End Get
        Set(ByVal value As AuroraHeaderMessageType)
            ViewState("MessageType") = value
        End Set
    End Property

    Public Sub ClearMessagePanel()
        messagePanel.Controls.Clear()
        messagePanel.Style.Add("display", "none")
    End Sub

    Public Sub AddMessagePanel(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        messagePanel.Controls.Add(New LiteralControl("<div class=""" + messageType.ToString() + """>" + text + "</div>"))
        messagePanel.Style.Add("display", "")
    End Sub

    Public Sub AddMessagePanelInformation(ByVal text As String)
        AddMessagePanel(AuroraHeaderMessageType.Information, text)
    End Sub

    Public Sub AddMessagePanelError(ByVal text As String)
        AddMessagePanel(AuroraHeaderMessageType.Error, text)
    End Sub

    Public Sub AddMessagePanelWarning(ByVal text As String)
        AddMessagePanel(AuroraHeaderMessageType.Warning, text)
    End Sub

    Public Sub SetMessagePanel(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        ClearMessagePanel()
        AddMessagePanel(messageType, text)
    End Sub

    Public Sub SetMessagePanelInformation(ByVal text As String)
        ClearMessagePanel()
        AddMessagePanelInformation(text)
    End Sub

    Public Sub SetMessagePanelError(ByVal text As String)
        ClearMessagePanel()
        AddMessagePanelError(text)
    End Sub

    Public Sub SetMessagePanelWarning(ByVal text As String)
        ClearMessagePanel()
        AddMessagePanelWarning(text)
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        MyBase.OnPreRender(e)

        If Not String.IsNullOrEmpty(Me.Text) Then
            AddMessagePanel(Me.MessageType, Me.Text)
        End If

        For Each scriptName As String In New String() {"MessagePanelControl.js"}
            If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered(scriptName) Then
                Me.Page.ClientScript.RegisterClientScriptInclude(scriptName, Me.ResolveUrl(scriptName))
            End If
        Next
    End Sub

End Class
