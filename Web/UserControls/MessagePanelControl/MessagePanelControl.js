﻿MessagePanelControl = function () 
{
}

MessagePanelControl.clearMessagePanel = function (messagePanelId) 
{ 
    var messagePanel = null;
    if (messagePanelId && (messagePanelId != "")) messagePanel = document.getElementById (messagePanelId);
    if (!messagePanel) messagePanel = shortMessagePanel;
    if (!messagePanel) return;
    
    messagePanel.innerHTML = ''; 
    messagePanel.style.display = 'none'; 
}

MessagePanelControl.addMessagePanel = function (messagePanelId, messageType, text) 
{ 
    var messagePanel = null;
    if (messagePanelId && (messagePanelId != "")) messagePanel = document.getElementById (messagePanelId);
    if (!messagePanel) messagePanel = shortMessagePanel;
    if (!messagePanel) return;
    
    messagePanel.innerHTML += '<div class="' + messageType + '">' + text + '</div>'; 
    messagePanel.style.display = '';  
}
MessagePanelControl.addMessagePanelInformation = function (messagePanelId, text) { this.addMessagePanel (messagePanelId, 'Information', text); }
MessagePanelControl.addMessagePanelError = function (messagePanelId, text) { this.addMessagePanel (messagePanelId, 'Error', text); }
MessagePanelControl.addMessagePanelWarning = function (messagePanelId, text) { this.addMessagePanel (messagePanelId, 'Warning', text); }

MessagePanelControl.setMessagePanel = function (messagePanelId, messageType, text) { this.clearMessagePanel (messagePanelId); this.addMessagePanel (messagePanelId, messageType, text); }
MessagePanelControl.setMessagePanelInformation = function (messagePanelId, text) { this.clearMessagePanel (messagePanelId); this.addMessagePanel (messagePanelId, 'Information', text); }
MessagePanelControl.setMessagePanelError = function (messagePanelId, text) { this.clearMessagePanel (messagePanelId); this.addMessagePanel (messagePanelId, 'Error', text); }
MessagePanelControl.setMessagePanelWarning = function (messagePanelId, text) { this.clearMessagePanel (messagePanelId); this.addMessagePanel (messagePanelId, 'Warning', text); }
