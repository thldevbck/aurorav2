
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class UserControls_LoadingIndicatorControl
    Inherits AuroraUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("LoadingIndicator.js") Then
            Me.Page.ClientScript.RegisterClientScriptInclude("LoadingIndicator.js", Me.ResolveUrl("LoadingIndicator.js"))
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("LoadingIndicator.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "LoadingIndicator.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("LoadingIndicator.css") & """ />")
        End If
    End Sub

End Class

