<%@ Control Language="VB" AutoEventWireup="false" 
    CodeFile="LoadingIndicatorControl.ascx.vb"
    Inherits="UserControls_LoadingIndicatorControl" %>

<style type="text/css">
* html #loadingIndicator_overlay
{
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="<%= Me.ResolveUrl("overlay.png") %>", sizingMethod="scale");
}
</style>

<div id="loadingIndicator_overlay" style="display:none;">
    <img 
        id="loadingIndicator_loadingImage" 
        src="<%= Me.ResolveUrl("loading.gif") %>" 
        alt="Loading" />
</div>

