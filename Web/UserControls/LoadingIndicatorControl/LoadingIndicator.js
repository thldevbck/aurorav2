﻿var loadingIndicatorTimeoutDelay = 750;
var loadingIndicatorTimeoutId = null;

//
// getLoadingIndicatorPageScroll()
// Returns array with x,y page scroll values.
// Core code from - quirksmode.org
//
function getLoadingIndicatorPageScroll()
{
	var yScroll;
	if (self.pageYOffset) 
		yScroll = self.pageYOffset;
	else if (document.documentElement && document.documentElement.scrollTop)	 // Explorer 6 Strict
		yScroll = document.documentElement.scrollTop;
	else if (document.body) // all other Explorers
		yScroll = document.body.scrollTop;
	
	return new Array('',yScroll) 
}



//
// getLoadingIndicatorPageSize()
// Returns array with page width, height and window width, height
// Core code from - quirksmode.org
// Edit for Firefox by pHaez
//
function getLoadingIndicatorPageSize()
{
	var xScroll, yScroll;
	if (window.innerHeight && window.scrollMaxY) 
	{	
		xScroll = document.body.scrollWidth;
		yScroll = window.innerHeight + window.scrollMaxY;
	} 
	else if (document.body.scrollHeight > document.body.offsetHeight) // all but Explorer Mac
	{
		xScroll = document.body.scrollWidth;
		yScroll = document.body.scrollHeight;
	} 
	else // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
	{
		xScroll = document.body.offsetWidth;
		yScroll = document.body.offsetHeight;
	}
	
	var windowWidth, windowHeight;
	if (self.innerHeight) // all except Explorer
	{
		windowWidth = self.innerWidth;
		windowHeight = self.innerHeight;
	} 
	else if (document.documentElement && document.documentElement.clientHeight) // Explorer 6 Strict Mode
	{
		windowWidth = document.documentElement.clientWidth;
		windowHeight = document.documentElement.clientHeight;
	} 
	else if (document.body) // other Explorers
	{
		windowWidth = document.body.clientWidth;
		windowHeight = document.body.clientHeight;
	}	
	
	// for small pages with total height less then height of the viewport
	if(yScroll < windowHeight)
		pageHeight = windowHeight;
	else 
		pageHeight = yScroll;

	// for small pages with total width less then width of the viewport
	if(xScroll < windowWidth)	
		pageWidth = windowWidth;
	else 
		pageWidth = xScroll;

	return new Array(pageWidth,pageHeight,windowWidth,windowHeight) 
}


//
// showLoadingIndicator()
// Preloads images. Pleaces new image in loadingIndicator then centers and displays.
//
function showLoadingIndicator()
{
return;
	// prep objects
	var objOverlay = document.getElementById('loadingIndicator_overlay');
	var objLoadingImage = document.getElementById('loadingIndicator_loadingImage');

    //animation hack
    setTimeout('document.images["' + objLoadingImage.id +'"].src="' + objLoadingImage.src + '"', 200); 

	var arrayPageSize = getLoadingIndicatorPageSize();
	var arrayPageScroll = getLoadingIndicatorPageScroll();

	objLoadingImage.style.top = (arrayPageScroll[1] + ((arrayPageSize[3] - 35 - objLoadingImage.height) / 2) + 'px');
	objLoadingImage.style.left = (((arrayPageSize[0] - 20 - objLoadingImage.width) / 2) + 'px');
	objLoadingImage.style.display = 'block';

	// set height of Overlay to take up whole page and show
	objOverlay.style.height = (arrayPageSize[1] + 'px');
	objOverlay.style.display = 'block';

	// make select boxes visible
	selects = document.getElementsByTagName("select");
    for (i = 0; i != selects.length; i++)
        if (selects[i].style.visibility != "hidden")
        {
		    selects[i].setAttribute ("oldVisibility", selects[i].style.visibility);
		    selects[i].style.visibility = "hidden";
        }
}

function showLoadingIndicatorTimed()
{
return;
    if (loadingIndicatorTimeoutId != null)
    {
        clearTimeout (loadingIndicatorTimeoutId);
        loadingIndicatorTimeoutId = null;
    }
    loadingIndicatorTimeoutId = setTimeout ("showLoadingIndicator();", loadingIndicatorTimeoutDelay);
}

//
// hideLoadingIndicator()
//
function hideLoadingIndicator()
{
return;
    if (loadingIndicatorTimeoutId != null)
    {
        clearTimeout (loadingIndicatorTimeoutId);
        loadingIndicatorTimeoutId = null;
    }

	// get objects
	objOverlay = document.getElementById('loadingIndicator_overlay');

	// hide loadingIndicator_overlay
	objOverlay.style.display = 'none';

	// make select boxes visible
	selects = document.getElementsByTagName("select");
    for (i = 0; i < selects.length; i++) 
		if (selects[i].getAttribute ("oldVisibility") != null)
		{
		    selects[i].style.visibility = selects[i].getAttribute ("oldVisibility");
    		selects[i].removeAttribute ("oldVisibility");
		}
}

//
// initLoadingIndicator()
// This function inserts html markup at the top of the page which will be used as a
// container for the loadingIndicator_overlay pattern and the inline image.
//
function initLoadingIndicator()
{
    if (Sys.Browser.agent == Sys.Browser.InternetExplorer && Sys.Browser.version < 7) 
        return;

	addEvent (document.getElementById ("aspnetForm"), "submit", function() { showLoadingIndicator(); return true; });
    Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(showLoadingIndicatorTimed);

    addEvent (window, "load", hideLoadingIndicator);
    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(hideLoadingIndicator);
}

//addEvent (window, "load", initLoadingIndicator);

