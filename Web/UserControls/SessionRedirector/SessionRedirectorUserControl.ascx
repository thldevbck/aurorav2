<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SessionRedirectorUserControl.ascx.vb" Inherits="UserControls_SessionRedirector_SessionRedirectorUserControl" %>
<%@ Register Src="../ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc1" %>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <asp:Timer ID="ajaxtimer" runat="server">
        </asp:Timer>
    </ContentTemplate>
</asp:UpdatePanel>
<uc1:ConfirmationBoxControl ID="ConfirmationBoxControl1" 
        runat="server" 
        RightButton_Visible ="false" 
        LeftButton_AutoPostBack="true" 
        Text ="Your session has timed out.The application will redirect you to your default page."
        Title="Time-Out Confirmation" />
