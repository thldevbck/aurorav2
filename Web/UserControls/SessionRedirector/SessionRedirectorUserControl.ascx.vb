
Partial Class UserControls_SessionRedirector_SessionRedirectorUserControl
    Inherits System.Web.UI.UserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim intMilliseconds As Integer = 60000
        ajaxTimer.Interval = Session.Timeout * intMilliseconds
    End Sub

    
    Protected Sub ajaxTimer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles ajaxtimer.Tick
        Me.ConfirmationBoxControl1.Show()
    End Sub

    Protected Sub ConfirmationBoxControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl1.PostBack
        If leftButton Then
            Dim path As String = VirtualPathUtility.ToAppRelative("/Web/Usercontrols/SessionRedirector/SessionRedirector.aspx")
            If Request.ServerVariables("HTTP_HOST").Contains("localhost") = False Then
                path = "/Aurora" & path
            End If
            Response.Redirect(path)
        End If
    End Sub
End Class
