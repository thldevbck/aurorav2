Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.UserDataSet
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class UserControls_SessionRedirector_SessionRedirector
    ''Inherits System.Web.UI.Page
    Inherits AuroraPage

    Public ReadOnly Property GetURL() As String
        Get
            Dim server As String = Request.ServerVariables("HTTP_HOST")
            Dim cookieValue As String ''= (Request.Cookies("defaultUserPage").Value)
            'If String.IsNullOrEmpty(cookieValue) Then
            '    cookieValue = GetDefaultUrl()
            'End If
            cookieValue = GetDefaultUrl()
            Dim intLocation As Integer = -1

            If cookieValue.Contains("..") Then
                intLocation = cookieValue.IndexOf("..")
                cookieValue = cookieValue.Remove(intLocation, 2)
            End If

            If cookieValue.Contains("~") Then
                intLocation = cookieValue.IndexOf("~")
                cookieValue = cookieValue.Remove(intLocation, 1)
            End If

            cookieValue = cookieValue.Replace("""", "")
            Dim path As String = ""
            If Request.ServerVariables("HTTP_HOST").Contains("localhost") = False Then
                path = "/Aurora"
            End If
            Return "http://" & String.Concat(server, path, cookieValue)
        End Get
    End Property
    Private Function GetDefaultUrl() As String
        Dim _javascriptSerializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        Return _javascriptSerializer.Serialize(UserSettings.Current.FunFileName)
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        
        response.redirect(GetURL)
    End Sub
End Class
