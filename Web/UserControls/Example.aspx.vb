Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Flex.Data
Imports Aurora.Flex.Data.FlexDataset
Imports Aurora.Flex.Services

<AuroraPageTitleAttribute("User Control Examples")> _
<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class UserControls_Example
    Inherits AuroraPage


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Protected Sub confirmButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles confirmButton.Click
        confirmationBoxControl1.Show()
    End Sub

    Protected Sub confirmationBoxControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confirmationBoxControl1.PostBack
        confirmationBoxControl2.Show()
    End Sub

    Protected Sub supervisorOverrideButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles supervisorOverrideButton.Click
        supervisorOverrideControl.Show()
    End Sub

    Protected Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal param As String) Handles supervisorOverrideControl.PostBack
        If isOkButton Then
            Response.Write("zuit suit riot<br/>")
        End If
    End Sub

    Protected Sub wizardControl_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wizardControl.SelectedIndexChanged
        selectedIndexLabel.Text = wizardControl.SelectedIndex.ToString()
    End Sub

    Protected Sub nextButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles nextButton.Click
        If (wizardControl.SelectedIndex + 1) < wizardControl.Items.Count Then
            wizardControl.SelectedIndex += 1
        End If
    End Sub

    Protected Sub pickerControl4_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles pickerControl4.TextChanged
        pickerControl4Label.Text = Server.HtmlEncode("pickerControl4_TextChanged : " & Date.Now.ToLongTimeString())
    End Sub

    Protected Sub timeControl3_TimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles timeControl3.TimeChanged
        timeControl3Label.Text = Server.HtmlEncode("timeControl3_TimeChanged : " & Date.Now.ToLongTimeString())

    End Sub

    Protected Sub dateControl2_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dateControl2.DateChanged
        dateControl2Label.Text = Server.HtmlEncode("dateControl2_DateChanged : " & Date.Now.ToLongTimeString())

    End Sub
End Class
