Imports Aurora.Common
Imports System.Data

Partial Class UserControls_ConfimationBoxControl
    Inherits AuroraUserControl

    Public Delegate Sub AuroraConfirmationBoxControlEventHandler(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String)

    Public Event PostBack As AuroraConfirmationBoxControlEventHandler

    Public Property Title() As String
        Get
            Return titleLabel.Text
        End Get
        Set(ByVal value As String)
            titleLabel.Text = value
        End Set
    End Property

    Public Property Text() As String
        Get
            Return textLabel.Text
        End Get
        Set(ByVal value As String)
            textLabel.Text = value
        End Set
    End Property

    Public Property LeftButton_Text() As String
        Get
            Return leftButton.Text
        End Get
        Set(ByVal value As String)
            leftButton.Text = value
        End Set
    End Property

    Public Property RightButton_Text() As String
        Get
            Return rightButton.Text
        End Get
        Set(ByVal value As String)
            rightButton.Text = value
        End Set
    End Property

    Public Property LeftButton_Visible() As Boolean
        Get
            Return leftButton.Visible
        End Get
        Set(ByVal value As Boolean)
            leftButton.Visible = value
        End Set
    End Property

    Public Property RightButton_Visible() As Boolean
        Get
            Return rightButton.Visible
        End Get
        Set(ByVal value As Boolean)
            rightButton.Visible = value
        End Set
    End Property

    Public ReadOnly Property Left_Button() As Button
        Get
            Return leftButton
        End Get
    End Property

    Public ReadOnly Property Right_Button() As Button
        Get
            Return rightButton
        End Get
    End Property

    Public Property Param() As String
        Get
            Return paramHiddenField.Value
        End Get
        Set(ByVal value As String)
            paramHiddenField.Value = value
        End Set
    End Property

    Public Property RedirectUrl() As String
        Get
            Return redirectUrlHiddenField.Value
        End Get
        Set(ByVal value As String)
            redirectUrlHiddenField.Value = value
        End Set
    End Property

    Private _leftButton_AutoPostBack As Boolean = True
    Public Property LeftButton_AutoPostBack() As Boolean
        Get
            Return _leftButton_AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            _leftButton_AutoPostBack = value
        End Set
    End Property

    Private _rightButton_AutoPostBack As Boolean = False
    Public Property RightButton_AutoPostBack() As Boolean
        Get
            Return _rightButton_AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            _rightButton_AutoPostBack = value
        End Set
    End Property

    Private _messageType As AuroraHeaderMessageType = AuroraHeaderMessageType.Warning
    Public Property MessageType() As AuroraHeaderMessageType
        Get
            Return _messageType
        End Get
        Set(ByVal value As AuroraHeaderMessageType)
            _messageType = value
        End Set
    End Property


    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        confirmationBox.BehaviorID = Me.ClientID & "Behaviour"
        ''------------------------------------------------------------------------------------------------------
        ''rev:mia July 3, 2013 - fixing problem on the mixing up of quick avail and other confirmation popup
        ''------------------------------------------------------------------------------------------------------
        HideOKButtonAndDisplaySearch(False)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("ConfirmationBox.js") Then
            Me.Page.ClientScript.RegisterClientScriptInclude("ConfirmationBox.js", Me.ResolveUrl("ConfirmationBox.js"))
        End If
        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("ConfirmationBox.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "ConfirmationBox.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("ConfirmationBox.css") & """ />")
        End If

    End Sub

    Public Sub Show()
        confirmationBox.Show()
    End Sub

    Public Sub Hide()
        confirmationBox.Hide()
    End Sub

    Protected Sub leftButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles leftButton.Click
        confirmationBox.Hide()
        RaiseEvent PostBack(Me, True, False, Param)
    End Sub

    Protected Sub rightButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles rightButton.Click
        confirmationBox.Hide()
        RaiseEvent PostBack(Me, False, True, Param)
        If Not String.IsNullOrEmpty(RedirectUrl) Then
            Response.Redirect(RedirectUrl)
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Const TIMED_OUT_MSG As String = "Your session has timed out.The application will redirect you to your default page"

        If MessageType = AuroraHeaderMessageType.Information Then
            messageTypeImage.ImageUrl = Me.ResolveUrl("~/Images/informat2.gif")
        ElseIf MessageType = AuroraHeaderMessageType.Error Then
            messageTypeImage.ImageUrl = Me.ResolveUrl("~/Images/error2.gif")
        Else
            messageTypeImage.ImageUrl = Me.ResolveUrl("~/Images/warning2.gif")
        End If

        If Not _leftButton_AutoPostBack Then
            leftButton.Attributes.Add("onclick", "return hideConfirmationBoxViaClient('" & confirmationBox.BehaviorID & "');")
        Else
            leftButton.Attributes.Remove("onclick")
        End If

        If Not _rightButton_AutoPostBack Then
            rightButton.Attributes.Add("onclick", "return hideConfirmationBoxViaClient('" & confirmationBox.BehaviorID & "');")
        Else
            rightButton.Attributes.Remove("onclick")
        End If

        ''------------------------------------------------------------------------------------------------------
        ''rev:mia July 3, 2013 - fixing problem on the mixing up of quick avail and other confirmation popup
        ''------------------------------------------------------------------------------------------------------
        'If (Me.Parent.Page.ToString.IndexOf("bookingprocess") = -1 And Me.Parent.Page.ToString.IndexOf("booking") = -1) Or Text.IndexOf(TIMED_OUT_MSG) <> -1 Then
        '    HideOKButtonAndDisplaySearch(False)
        'End If
        If ((Me.Parent.Page.Title.ToString.IndexOf("Maintain Payment") = -1 And _
             Me.Parent.Page.Title.ToString.IndexOf("Booking Request") = -1 And _
             Me.Parent.Page.ToString.IndexOf("booking.aspx") = -1) Or _
            Text.IndexOf(TIMED_OUT_MSG) <> -1) Then
            HideOKButtonAndDisplaySearch(False)
        End If
        ''------------------------------------------------------------------------------------------------------
    End Sub



#Region "public procedures"
    Public Sub HideOKButtonAndDisplaySearch(ByVal hideBoolean As Boolean)
        If hideBoolean = True Then
            divButtonForSearch.Visible = True
            divButtonForSearch.Disabled = False
            leftButton.Visible = False
        Else
            divButtonForSearch.Visible = False
            divButtonForSearch.Disabled = True
            leftButton.Visible = True
        End If
    End Sub

#End Region

#Region "Public properties"

    Public ReadOnly Property ConfirmationBehaviorID As String
        Get
            Return confirmationBox.BehaviorID
        End Get
    End Property

#End Region



End Class

