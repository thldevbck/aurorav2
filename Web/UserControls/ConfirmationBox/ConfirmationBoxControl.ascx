<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ConfirmationBoxControl.ascx.vb"
    Inherits="UserControls_ConfimationBoxControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:HiddenField ID="paramHiddenField" runat="server" />
<asp:HiddenField ID="redirectUrlHiddenField" runat="server" />
<asp:Button runat="server" ID="hiddenTargetControlForConfirmationBox" Style="display: none" />

<ajaxToolkit:ModalPopupExtender runat="server" ID="confirmationBox" BehaviorID="confirmationBoxBehavior"
    TargetControlID="hiddenTargetControlForConfirmationBox" PopupControlID="confirmationPanel"
    BackgroundCssClass="modalBackground" DropShadow="True" PopupDragHandleControlID="confirmationBoxDragHandle" />
<asp:Panel runat="server" CssClass="modalPopup" ID="confirmationPanel" Style="display: none;
    padding: 10px;" Width="500px">
    <asp:Panel runat="Server" ID="confirmationBoxDragHandle" CssClass="modalPopupTitle">
        <asp:Label ID="titleLabel" runat="Server" Text="Confirm" />
    </asp:Panel>
    <br />
    <table width="100%">
        <tr>
            <td style="vertical-align: top;" align="left">
                <asp:Image ID="messageTypeImage" runat="Server" ImageUrl="~/Images/informat2.gif" />
            </td>
            <td style="vertical-align: top;" align="left" width="100%">
                <asp:Label ID="textLabel" runat="server" />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td></td>
            <td align="right" colspan="2">
            <table>
                <tr>
                    <td>
                        <div id="divButtonForSearch" runat="server">
                             <table>
                                <tr>
                                    <td>Search:</td>
                                    <td id="controltest">
                                        
                                        <input type="text" runat="server"  value="all brands" id="txtBrands" style="text-align: center"/>
                                        <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="DDE"  
                                                                      TargetControlID="txtBrands"   
                                                                      DropDownControlID="pnlBrands"  
                                                                       />  

                                        <asp:Panel ID="pnlBrands" 
                                                   runat="server" 
                                                   BorderColor="Gray" 
                                                   BackColor="White"
                                                   BorderWidth="1">

                                                <asp:CheckBoxList ID="cblBrands" runat="server" 
                                                                  RepeatColumns="1" 
                                                                  RepeatDirection="Vertical" 
                                                                  RepeatLayout="table" 
                                                                  CellPadding = "3" 
                                                                  CellSpacing = "3"
                                                                  TextAlign ="Left">

                                                    <asp:ListItem Text="Alpha"       Value="A" alt='A' />
                                                    <asp:ListItem Text="Britz"       Value="B" alt='B' />
                                                    <asp:ListItem Text="BackPacker"  Value="P" alt='P' />
                                                    <asp:ListItem Text="Econo"       Value="E" alt='E' />
                                                    <asp:ListItem Text="Kea"         Value="Q" alt='Q' />
                                                    <asp:ListItem Text="Maui"        Value="M" alt='M' />
                                                    <asp:ListItem Text="Mighty"      Value="Y" alt='Y' />
                                                    <asp:ListItem Text="United"      Value="U" alt='U' />
                                                    
                                                    
                                                </asp:CheckBoxList>

                                        </asp:Panel>

                                    </td>
                                    <td>
                                        <input id="SearchButton" type="button" value="Search" class="Button_Standard Button_OK" />
                                    </td>
                                </tr>
                             </table>
                            
                        </div>
                    </td>
                    <td>
                        <asp:Button ID="leftButton" runat="server" Text="OK" CssClass="Button_Standard Button_OK" />
                    </td>
                    <td>
                        <asp:Button ID="rightButton" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel"  Width="100"/>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
</asp:Panel>

