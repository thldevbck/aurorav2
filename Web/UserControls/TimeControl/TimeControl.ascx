<%@ Control Language="VB" AutoEventWireup="false" CodeFile="TimeControl.ascx.vb" Inherits="UserControls_TimeControl" %>

<span class="DateControl">

<asp:TextBox 
    ID="timeTextBox" 
    runat="server" 
    MaxLength="5" 
    CssClass="TimeControl" 
    Width="35px" />

<asp:Image ID="timeImage" runat="server" ImageUrl="~/UserControls/TimeControl/TimeControl.gif"  ImageAlign="AbsMiddle" Width="18" Height="15" EnableViewState="false" />

</span>

