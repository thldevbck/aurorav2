﻿TimeControl = function () 
{
}

TimeControl.timeTextBox_onkeyup = function(timeTextBoxId) 
{
	var timeTextBox = document.getElementById (timeTextBoxId);
    var text = timeTextBox.value.trim();
    var oldtext = "" + timeTextBox.getAttribute('oldvalue');

    if (text != oldtext)
    {
        timeTextBox.style.textDecoration = 'none';
        timeTextBox.style.color = '#000';
    }
}

TimeControl.timeTextBox_onchange = function(timeTextBoxId) 
{
	var timeTextBox = document.getElementById (timeTextBoxId);
	var nullable = timeTextBox.getAttribute ('nullable') == 'true';
    var autoPostBack = timeTextBox.getAttribute('autoPostBack') == 'true';
	var messagePanelControlID = timeTextBox.getAttribute ('messagePanelControlID');
    var text = timeTextBox.value.trim();
    var oldtext = "" + timeTextBox.getAttribute('oldvalue');
    if (text == oldtext) return;

    timeTextBox.setAttribute ('oldvalue', text);
    
    if ((text != '') || !nullable)
    {
        var text = this.validateTimeString (text); 
        if (text) 
        {
            timeTextBox.value = text;
            MessagePanelControl.clearMessagePanel();
            timeTextBox.style.textDecoration = 'none';
            timeTextBox.style.color = '#000';
        }
        else
        {
            MessagePanelControl.setMessagePanelWarning (messagePanelControlID, 'Enter time in hh:mm format');
            timeTextBox.style.textDecoration = 'underline';
            timeTextBox.style.color = '#F00';
        }
    }
    else if (!nullable)
    {
        MessagePanelControl.setMessagePanelWarning (messagePanelControlID, 'Enter a time');
        timeTextBox.style.textDecoration = 'none';
        timeTextBox.style.color = '#000';
    }
    else
    {
        timeTextBox.value = '';
        MessagePanelControl.clearMessagePanel();
        timeTextBox.style.textDecoration = 'none';
        timeTextBox.style.color = '#000';
    }

    if (autoPostBack)
        setTimeout('__doPostBack(\'' + timeTextBoxId + '\',\'\')', 0);
};

TimeControl.validateTimeString = function (text)
{
    text = ("" + text).trim();
    
    // check colon position
    switch (text.indexOf(':'))
    {
        case -1: // no colon, may be a case of auto completion
            switch(text.length)
            {
                case 1: text = "0" + text + ":00"; break;
                case 2: text = text + ":00"; break;
                case 3: text = "0" + text.substring(0,1) + ":" + text.substring(1,3) ;break;
                default: text = text.substring(0,2) + ":" + text.substring(2,4); break;
            }
            break; 
        case 0: text = "00:" + text.substring(1,3); break; // : in front 
        case 1: text = "0" + text.substring(0,4); break; // : after some char
        case 2: text = text.substring(0,5); break; // : after 2 chars
        case 3: return null; 
        default: return null; 
    }
    
    //check length, if short, pad at end, should be 5
    switch (text.length)
    {
        case 3: text += "00"; break;
        case 4: text += "0"; break;
        default: text = text.substring(0,5); break;
    }
    
    //string in HH:MM form now check if its valid!
    var hours = parseInt (text.split(':')[0]);
    var minutes = parseInt (text.split(':')[1]);
    if ((hours>=0) && (hours<=23) && (minutes>=0) && (minutes<=59))
        return text;
    else
        return null;
}
