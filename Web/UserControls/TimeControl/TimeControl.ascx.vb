''' <summary>
''' A server side user control which encaptulates the time input
'''
''' </summary>
''' <remarks></remarks>
Partial Class UserControls_TimeControl
    Inherits System.Web.UI.UserControl

    ''' <summary>
    ''' The text value of the time
    ''' </summary>
    Public Property Text() As String
        Get
            Return timeTextBox.Text
        End Get
        Set(ByVal value As String)
            timeTextBox.Text = value
        End Set
    End Property

    ''' <summary>
    ''' The time value. MinValue if empty or error
    ''' </summary>
    Public Property [Time]() As TimeSpan
        Get
            Try
                Dim text As String = timeTextBox.Text.Trim()
                If String.IsNullOrEmpty(text) _
                 OrElse text.IndexOf(":"c) = -1 Then Throw New Exception()

                Dim resultArray As String() = text.Split(":")
                If resultArray.Length <> 2 Then Throw New Exception()

                Dim result As New TimeSpan(Integer.Parse(resultArray(0)), Integer.Parse(resultArray(1)), 0)
                If result.Ticks < 0 Or result.Ticks >= TimeSpan.TicksPerDay Then Throw New Exception()

                Return result
            Catch
                Return TimeSpan.MinValue
            End Try
        End Get
        Set(ByVal value As TimeSpan)
            If value <> TimeSpan.MinValue Then
                timeTextBox.Text = value.Hours.ToString().PadLeft(2, "0") & ":" & value.Minutes.ToString().PadLeft(2, "0")
            Else
                timeTextBox.Text = ""
            End If
        End Set
    End Property

    Public ReadOnly Property IsValid() As Boolean
        Get
            Return (Text.Trim() = "" AndAlso Nullable) OrElse Me.Time <> TimeSpan.MinValue
        End Get
    End Property

    Public Property [ReadOnly]() As Boolean
        Get
            Return timeTextBox.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            timeTextBox.ReadOnly = value
        End Set
    End Property

    Public Property Nullable() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("Nullable")) Then
                Return CBool(ViewState("Nullable"))
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Nullable") = value.ToString()
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return timeTextBox.Enabled
        End Get
        Set(ByVal value As Boolean)
            timeTextBox.Enabled = value
        End Set
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("AutoPostBack")) Then
                Return CBool(ViewState("AutoPostBack"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("AutoPostBack") = value
        End Set
    End Property

    Public Property MessagePanelControlID() As String
        Get
            Return "" & ViewState("MessagePanelControlID")
        End Get
        Set(ByVal value As String)
            ViewState("MessagePanelControlID") = value
        End Set
    End Property

    Public Property CssClass() As String
        Get
            Return timeTextBox.CssClass
        End Get
        Set(ByVal value As String)
            timeTextBox.CssClass = value
        End Set
    End Property

    Public ReadOnly Property Style() As CssStyleCollection
        Get
            Return timeTextBox.Style
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("TimeControl.js") Then
            Me.Page.ClientScript.RegisterClientScriptInclude("TimeControl.js", Me.ResolveUrl("TimeControl.js"))
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("TimeControl.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "TimeControl.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("TimeControl.css") & """ />")
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        timeTextBox.Attributes.Add("onkeyup", "TimeControl.timeTextBox_onkeyup('" & timeTextBox.ClientID & "')")
        timeTextBox.Attributes.Add("onchange", "TimeControl.timeTextBox_onchange('" & timeTextBox.ClientID & "');")
        timeTextBox.Attributes.Add("nullable", Me.Nullable.ToString().ToLower())
        timeTextBox.Attributes.Add("autoPostBack", Me.AutoPostBack.ToString().ToLower())

        Dim c As Control = Me.Parent.FindControl(MessagePanelControlID)
        If c IsNot Nothing Then
            timeTextBox.Attributes.Add("messagePanelControlID", c.ClientID)
        Else
            timeTextBox.Attributes.Add("messagePanelControlID", "")
        End If

        If Not String.IsNullOrEmpty(Text.Trim()) AndAlso Me.Time = TimeSpan.MinValue Then
            timeTextBox.Style.Add("text-decoration", "underline")
            timeTextBox.Style.Add("color", "#F00")
        Else
            timeTextBox.Style.Add("text-decoration", "none")
            timeTextBox.Style.Add("color", "#000")
        End If
    End Sub

    Public Overrides ReadOnly Property ClientID() As String
        Get
            Return timeTextBox.ClientID
        End Get
    End Property

    Public Event TimeChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    Protected Sub timeTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles timeTextBox.TextChanged
        RaiseEvent TimeChanged(sender, e)
    End Sub

End Class
