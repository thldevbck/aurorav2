RegExTextBox = function () 
{
}

RegExTextBox.regExTextBox_onkeyup = function(regExTextBoxId) 
{
	var regExTextBox = document.getElementById (regExTextBoxId);
    var text = regExTextBox.value;
    var oldtext = regExTextBox.getAttribute('oldvalue');

    if (!oldtext || (text != oldtext))
    {
        regExTextBox.style.textDecoration = 'none';
        regExTextBox.style.color = '#000';
        regExTextBox.setAttribute ('oldvalue', text);
    }
}

RegExTextBox.regExTextBox_onchange = function(regExTextBoxId) 
{
	var regExTextBox = document.getElementById (regExTextBoxId);

	var nullable = regExTextBox.getAttribute ('nullable') == 'true';
	var messagePanelControlID = regExTextBox.getAttribute ('messagePanelControlID');
	var regExPattern = regExTextBox.getAttribute ('regExPattern');
	var errorMessage = regExTextBox.getAttribute ('errorMessage');
    var text = regExTextBox.value.trim();

    if ((text != '') || !nullable)
    {
        if (this.isTextValid (text, regExPattern)) 
        {
            MessagePanelControl.clearMessagePanel(messagePanelControlID);
            regExTextBox.style.textDecoration = 'none';
            regExTextBox.style.color = '#000';
        }
        else
        {
            MessagePanelControl.setMessagePanelWarning (messagePanelControlID, errorMessage);
            regExTextBox.style.textDecoration = 'underline';
            regExTextBox.style.color = '#F00';
        }
    }
    else if (!nullable)
    {
        MessagePanelControl.setMessagePanelWarning (messagePanelControlID, errorMessage);
        regExTextBox.style.textDecoration = 'none';
        regExTextBox.style.color = '#000';
    }
    else
    {
        MessagePanelControl.clearMessagePanel(messagePanelControlID);
        regExTextBox.style.textDecoration = 'none';
        regExTextBox.style.color = '#000';
    }
};

RegExTextBox.isTextValid = function (text, pattern)
{
    if (pattern == '') 
        return true;
    else
    {
        var regExp = new RegExp (pattern);
        return regExp.test(text);
    }
}
