Imports System.Text.RegularExpressions

''' <summary>
''' RegExTextBox is a simple Aurora-friendly TextBox control with regular expression validation
''' </summary>
''' <remarks></remarks>
Partial Class UserControls_RegExTextBox
    Inherits System.Web.UI.UserControl

    Public Property Text() As String
        Get
            Return regExTextBox.Text
        End Get
        Set(ByVal value As String)
            regExTextBox.Text = value
        End Set
    End Property

    Public ReadOnly Property IsTextValid() As Boolean
        Get
            If String.IsNullOrEmpty(RegExPattern) Then Return True
            If Nullable AndAlso String.IsNullOrEmpty(Text.Trim()) Then Return True

            Return System.Text.RegularExpressions.Regex.IsMatch(Text, RegExPattern)
        End Get
    End Property

    Public Property AutoPostBack() As Boolean
        Get
            Return regExTextBox.AutoPostBack
        End Get
        Set(ByVal value As Boolean)
            regExTextBox.AutoPostBack = value
        End Set
    End Property

    Public Property RegExPattern() As String
        Get
            Return "" & ViewState("RegExPattern")
        End Get
        Set(ByVal value As String)
            ViewState("RegExPattern") = value
        End Set
    End Property

    Public Property ErrorMessage() As String
        Get
            Return "" & ViewState("ErrorMessage")
        End Get
        Set(ByVal value As String)
            ViewState("ErrorMessage") = value
        End Set
    End Property

    Public Property MessagePanelControlID() As String
        Get
            Return "" & ViewState("MessagePanelControlID")
        End Get
        Set(ByVal value As String)
            ViewState("MessagePanelControlID") = value
        End Set
    End Property

    Public Property [ReadOnly]() As Boolean
        Get
            Return regExTextBox.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            regExTextBox.ReadOnly = value
        End Set
    End Property

    Public Property Nullable() As Boolean
        Get
            If Not String.IsNullOrEmpty(ViewState("Nullable")) Then
                Return CBool(ViewState("Nullable"))
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Nullable") = value.ToString()
        End Set
    End Property

    Public Property Enabled() As Boolean
        Get
            Return regExTextBox.Enabled
        End Get
        Set(ByVal value As Boolean)
            regExTextBox.Enabled = value
        End Set
    End Property

    Public Property MaxLength() As Integer
        Get
            Return regExTextBox.MaxLength
        End Get
        Set(ByVal value As Integer)
            regExTextBox.MaxLength = value
        End Set
    End Property

    Public Property CssClass() As String
        Get
            Return regExTextBox.CssClass
        End Get
        Set(ByVal value As String)
            regExTextBox.CssClass = value
        End Set
    End Property

    Public ReadOnly Property Style() As CssStyleCollection
        Get
            Return regExTextBox.Style
        End Get
    End Property

    Public Property Width() As Unit
        Get
            Return regExTextBox.Width
        End Get
        Set(ByVal value As Unit)
            regExTextBox.Width = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.Page.ClientScript.IsClientScriptIncludeRegistered("RegExTextBox.js") Then
            Me.Page.ClientScript.RegisterClientScriptInclude("RegExTextBox.js", Me.ResolveUrl("RegExTextBox.js"))
        End If

        If Not Me.Page.ClientScript.IsClientScriptBlockRegistered("RegExTextBox.css") Then
            Me.Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "RegExTextBox.css", "<link rel=""stylesheet"" type=""text/css"" href=""" & Me.ResolveUrl("RegExTextBox.css") & """ />")
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        regExTextBox.Attributes.Add("onkeyup", "RegExTextBox.regExTextBox_onkeyup('" & regExTextBox.ClientID & "')")
        regExTextBox.Attributes.Add("onblur", "RegExTextBox.regExTextBox_onchange('" & regExTextBox.ClientID & "');")
        If Not String.IsNullOrEmpty(Me.Attributes("onchange")) Then regExTextBox.Attributes.Add("onchange", Me.Attributes("onchange"))
        If Not String.IsNullOrEmpty(Me.Attributes("onkeypress")) Then regExTextBox.Attributes.Add("onkeypress", Me.Attributes("onchange"))

        regExTextBox.Attributes.Add("nullable", Me.Nullable.ToString().ToLower())
        regExTextBox.Attributes.Add("regExPattern", RegExPattern)
        regExTextBox.Attributes.Add("errorMessage", ErrorMessage)

        Dim c As Control = Me.Parent.FindControl(MessagePanelControlID)
        If c IsNot Nothing Then
            regExTextBox.Attributes.Add("messagePanelControlID", c.ClientID)
        Else
            regExTextBox.Attributes.Add("messagePanelControlID", "")
        End If

        If Not IsTextValid() Then
            regExTextBox.Style.Add("text-decoration", "underline")
            regExTextBox.Style.Add("color", "#F00")
        Else
            regExTextBox.Style.Add("text-decoration", "none")
            regExTextBox.Style.Add("color", "#000")
        End If
    End Sub

    Public Overrides ReadOnly Property ClientID() As String
        Get
            Return regExTextBox.ClientID
        End Get
    End Property

    Protected Sub regExTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles regExTextBox.TextChanged
        RaiseEvent TextChanged(sender, e)
    End Sub

    Public Event TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)

End Class
