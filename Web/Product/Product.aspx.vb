Imports System.Collections.Generic
Imports System.Data

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet
Imports System.Globalization

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductEnquiry)> _
Partial Class Product_Product
    Inherits AuroraPage

    ''added manny
    Delegate Sub BuildProductDelegate(ByVal udpate As Boolean)
    Private _initException As Exception

    Private _prdId As String = Nothing
    Private _productDataSet As New ProductDataSet()
    Private _productRow As ProductRow = Nothing

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.ProductMaintenance)
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If String.IsNullOrEmpty(_prdId) Then
                Return "New Product"
            ElseIf CanMaintain Then
                Return "Edit Product"
            Else
                Return "View Product"
            End If
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("prdId")) Then
                _prdId = Request.QueryString("prdId").Trim()
            End If

            TankSizeTextBox.Attributes.Add("onkeyup", "IsValidSized(this);")
            ''TankSizeTextBox.Attributes.Add("onmouseup", "IsValidSized(this);")
            TankSizeTextBox.Attributes.Add("onblur", "CurrencyFormatted(this);")


            MyBase.OnInit(e)

            _productDataSet.EnforceConstraints = False

            DataRepository.GetProductLookups(_productDataSet, Me.CompanyCode)
            DataRepository.GetSaleableProductLookups(_productDataSet, Me.CompanyCode)

            If Not String.IsNullOrEmpty(_prdId) Then
                DataRepository.GetProduct(_productDataSet, Me.CompanyCode, _prdId)

                If _productDataSet.Product.Count = 1 Then
                    _productRow = _productDataSet.Product(0)
                ElseIf _productDataSet.Product.Count <= 0 Then
                    Throw New Exception("No product found")
                Else
                    Throw New Exception("Unexpected product result")
                End If


            End If

            brandDropDown.Items.Clear()
            For Each brandRow As BrandRow In _productDataSet.Brand
                brandDropDown.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
            Next

            typeDropDown.Items.Clear()
            For Each typeRow As TypeRow In _productDataSet.Type
                typeDropDown.Items.Add(New ListItem(typeRow.TypCode & " - " & typeRow.TypDesc, typeRow.TypId))
            Next

            UpdateIsVehicle()

            featuresCheckBoxList.Items.Clear()
            For Each featureRow As FeatureRow In _productDataSet.Feature
                featuresCheckBoxList.Items.Add(New ListItem(featureRow.FtrDesc, featureRow.FtrId))
            Next

            For Each productUserControl As ProductUserControl In New ProductUserControl() { _
             nonAvailabilityControl}
                productUserControl.InitProductUserControl(_productDataSet, Nothing, _productRow)
            Next
        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Private Sub BuildProduct(ByVal useViewState As Boolean)
        Dim currentDate As Date = Date.Now

        If _productRow Is Nothing Then
            If Not useViewState Then
                Try
                    If Not String.IsNullOrEmpty(CookieValue("Product_prdTypId")) Then
                        typeDropDown.SelectedValue = CookieValue("Product_prdTypId")
                    End If
                Catch
                End Try

                Try
                    If Not String.IsNullOrEmpty(CookieValue("Product_prdBrdCode")) Then
                        brandDropDown.SelectedValue = CookieValue("Product_prdBrdCode")
                    End If
                Catch
                End Try

                shortNameTextBox.Text = "" & CookieValue("Product_prdShortName")
                nameTextBox.Text = "" & CookieValue("Product_prdName")

                For i As Integer = 0 To 4
                    Dim cookieName As String = "Product_ftrId" & CStr(i)
                    If Not String.IsNullOrEmpty(CookieValue(cookieName)) AndAlso featuresCheckBoxList.Items.FindByValue(CookieValue(cookieName)) IsNot Nothing Then
                        featuresCheckBoxList.Items.FindByValue(CookieValue(cookieName)).Selected = True
                    End If
                Next

                statusRadioButtonList.Items(0).Selected = Not String.IsNullOrEmpty(CookieValue("Product_prdIsActive")) AndAlso CBool(CookieValue("Product_prdIsActive"))
                statusRadioButtonList.Items(1).Selected = Not statusRadioButtonList.Items(0).Selected
            End If

            brandDropDown.Enabled = True
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(ProductConstants.InactiveColor)

            updateButton.Visible = False
            createButton.Visible = CanMaintain
            addButton.Visible = False

            tabs.Visible = False
            saleableProductLabel.Visible = False
            saleableProductTable.Visible = False
        Else
            If Not useViewState Then
                shortNameTextBox.Text = _productRow.PrdShortName
                nameTextBox.Text = _productRow.PrdName
                dvassSeqTextBox.Text = _productRow.PrdDvassSeq.ToString()
                Try
                    typeDropDown.SelectedValue = _productRow.PrdTypId
                Catch
                End Try
                Try
                    brandDropDown.SelectedValue = _productRow.PrdBrdCode
                Catch
                End Try

                For i As Integer = 0 To _productDataSet.Feature.Rows.Count - 1
                    featuresCheckBoxList.Items(i).Selected = _productDataSet.Feature(i).GetProductFeatureRows().Length > 0
                Next
                For Each listItem As ListItem In flagsCheckBoxList.Items
                    listItem.Selected = _productRow.Table.Columns.Contains(listItem.Value) _
                     AndAlso Not _productRow.IsNull(listItem.Value) _
                     AndAlso CType(_productRow(listItem.Value), Boolean)
                Next
                statusRadioButtonList.Items(0).Selected = _productRow.IsActive
                statusRadioButtonList.Items(1).Selected = Not statusRadioButtonList.Items(0).Selected
            End If

            Try

                ''rev:mia Aug 8 2011 added for the PFF
                Me.TankSizeTextBox.Text = IIf(_productRow.PrdTankSize = 0, "", _productRow.PrdTankSize.ToString("00.00", CultureInfo.InvariantCulture))
                Me.FuelTypeDropDownList.Items.FindByText(_productRow.PrdFuelType).Selected = True
                
                Me.TankSizeTextBox.Enabled = _productRow.PrdIsVehicle
                Me.FuelTypeDropDownList.Enabled = _productRow.PrdIsVehicle

                ' Added by Nimesh on 2nd July 2015
                Me.ProductCategoryDropDownList.Items.FindByValue(_productRow.PrdCatId).Selected = True
                Me.ProductCategoryDropDownList.Enabled = _productRow.PrdIsVehicle
                ' End Added by Nimesh on 2nd July 2015

            Catch ex As Exception
                Me.TankSizeTextBox.Enabled = _productRow.PrdIsVehicle
                Me.FuelTypeDropDownList.Enabled = _productRow.PrdIsVehicle
                ' Added by Nimesh on 2nd July 2015
                Me.ProductCategoryDropDownList.Enabled = _productRow.PrdIsVehicle
                ' End Added by Nimesh on 2nd July 2015
            End Try



            shortNameTextBox.ReadOnly = True
            nameTextBox.ReadOnly = Not CanMaintain
            typeDropDown.Enabled = CanMaintain
            brandDropDown.Enabled = False
            featuresCheckBoxList.Enabled = CanMaintain
            flagsCheckBoxList.Enabled = CanMaintain
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(_productRow.StatusColor())
            statusRadioButtonList.Enabled = CanMaintain

            updateButton.Visible = CanMaintain
            createButton.Visible = False
            addButton.Visible = CanMaintain

            While saleableProductTable.Rows.Count > 1
                saleableProductTable.Rows.RemoveAt(1)
            End While

            Dim sapStatus As String = Nothing
            Dim currentFutureOnly As Boolean = False
            'If saleableProductRadioButtonList.SelectedValue = "Current" Then
            '    sapStatus = "Active"
            '    currentFutureOnly = True
            'ElseIf saleableProductRadioButtonList.SelectedValue = "Current" Then
            '    sapStatus = "Active"
            'ElseIf saleableProductRadioButtonList.SelectedValue = "Pending" Then
            '    sapStatus = "Pending"
            'ElseIf saleableProductRadioButtonList.SelectedValue = "Inactive" Then
            '    sapStatus = "Inactive"
            'End If

            Select Case saleableProductRadioButtonList.SelectedValue
                Case "Active-Current"
                    sapStatus = "Active"
                    currentFutureOnly = True
                Case "Active-Future"
                    sapStatus = "Active"
                    currentFutureOnly = True
                Case "Active-Past"
                    sapStatus = "Active"
                Case "Pending"
                    sapStatus = "Pending"
                Case "Inactive"
                    sapStatus = "Inactive"
            End Select


            DataRepository.SearchSaleableProduct( _
                _productDataSet, _
                Me.CompanyCode, _
                _prdId, _
                sapStatus, _
                currentFutureOnly)

            Dim index As Integer = 0
            For Each saleableProductRow As SaleableProductRow In _productDataSet.SaleableProduct
                'If (saleableProductRadioButtonList.SelectedValue = "Current" AndAlso Not (saleableProductRow.HasCurrent(currentDate) OrElse saleableProductRow.HasFuture(currentDate))) _
                ' OrElse (saleableProductRadioButtonList.SelectedValue = "Active" AndAlso Not saleableProductRow.IsActive()) _
                ' OrElse (saleableProductRadioButtonList.SelectedValue = "Pending" AndAlso Not saleableProductRow.IsPending()) _
                ' OrElse (saleableProductRadioButtonList.SelectedValue = "Inactive" AndAlso Not saleableProductRow.IsInactive()) Then
                '    Continue For
                'End If

                Select Case saleableProductRadioButtonList.SelectedValue
                    Case "Active-Current"
                        If Not saleableProductRow.HasCurrent(currentDate) Or Not saleableProductRow.IsActive() Then
                            Continue For
                        End If
                    Case "Active-Future"
                        If Not saleableProductRow.HasFuture(currentDate) Or Not saleableProductRow.IsActive() Then
                            Continue For
                        End If
                    Case "Active-Past"
                        If saleableProductRow.HasFuture(currentDate) Or saleableProductRow.HasCurrent(currentDate) Or Not saleableProductRow.IsActive() Then
                            Continue For
                        End If
                    Case "Pending"
                        If Not saleableProductRow.IsPending() Then
                            Continue For
                        End If
                    Case "Inactive"
                        If Not saleableProductRow.IsInactive() Then
                            Continue For
                        End If
                End Select

                Dim tableRow As New TableRow()
                tableRow.BackColor = saleableProductRow.StatusColor(currentDate)
                saleableProductTable.Rows.Add(tableRow)

                Dim nameCell As New TableCell()
                Dim nameHyperLink As New HyperLink()
                nameHyperLink.Text = Server.HtmlEncode(_productRow.PrdShortName & " - " & saleableProductRow.SapSuffix.ToString())
                nameHyperLink.NavigateUrl = ProductUserControl.MakeSaleableProductUrl(Me, prdId:=saleableProductRow.SapPrdId, sapId:=saleableProductRow.SapId)
                nameCell.Controls.Add(nameHyperLink)
                tableRow.Controls.Add(nameCell)

                Dim countryCell As New TableCell()
                If saleableProductRow.CountryRow IsNot Nothing Then
                    countryCell.Text = Server.HtmlEncode(saleableProductRow.CountryRow.CtyCode)
                Else
                    countryCell.Text = ""
                End If
                tableRow.Controls.Add(countryCell)

                Dim currencyCell As New TableCell()
                currencyCell.Text = saleableProductRow.SapCodCurrCode
                tableRow.Controls.Add(currencyCell)

                Dim attributeCell As New TableCell()
                attributeCell.Text = saleableProductRow.GetProductAttributeSummaryHtml()
                tableRow.Controls.Add(attributeCell)

                Dim rateCell As New TableCell()
                rateCell.Text = ""
                If saleableProductRow.GetTravelBandSetRows().Length = 1 _
                 AndAlso saleableProductRow.GetTravelBandSetRows()(0).GetProductRateTravelBandRows().Length = 1 Then
                    Dim productRateTravelBandRow As ProductRateTravelBandRow = saleableProductRow.GetTravelBandSetRows()(0).GetProductRateTravelBandRows()(0)
                    rateCell.Text = productRateTravelBandRow.BuildRateSummeryHtml()
                Else
                    rateCell.Text = ""
                End If
                tableRow.Controls.Add(rateCell)

                Dim flagsCell As New TableCell()
                Dim flagsList As New List(Of String)
                If Not saleableProductRow.IsSapIsBaseNull AndAlso saleableProductRow.SapIsBase Then flagsList.Add("Base")
                If Not saleableProductRow.IsSapIsFlexNull AndAlso saleableProductRow.SapIsFlex Then flagsList.Add("Flex")
                flagsCell.Text = String.Join(" /<br>", flagsList.ToArray())
                tableRow.Controls.Add(flagsCell)

                Dim statusCell As New TableCell()
                statusCell.Text = Server.HtmlEncode(saleableProductRow.StatusText(currentDate))
                tableRow.Controls.Add(statusCell)

                index += 1

            Next

            tabs.Visible = True
            If index <= 0 Then
                saleableProductLabel.Text = "No Saleable products found"
                saleableProductTable.Visible = False
            ElseIf index = 1 Then
                saleableProductLabel.Text = index & " Saleable product found"
                saleableProductTable.Visible = True
            Else
                saleableProductLabel.Text = index & " Saleable products found"
                saleableProductTable.Visible = True
            End If
        End If
    End Sub

    Protected Sub Product_Product_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try

            Dim result As New DataSet
            Dim sp As String = "PPF_Getfuel" ''"PPF_FuelPriceCalculation"
            Aurora.Common.Data.ExecuteDataSetSP(sp, result)


            Me.FuelTypeDropDownList.AppendDataBoundItems = True
            Me.FuelTypeDropDownList.Items.Add("")
            Me.FuelTypeDropDownList.DataSource = result.Tables(0).DefaultView
            Me.FuelTypeDropDownList.DataTextField = "CodCode"
            Me.FuelTypeDropDownList.DataValueField = "CodId"
            Me.FuelTypeDropDownList.DataBind()

            ' Added by Nimesh on 2nd July 2015
            Dim catresult As New DataSet
            Dim catsp As String = "Product_GetProductCategory" ''"PPF_FuelPriceCalculation"
            Aurora.Common.Data.ExecuteDataSetSP(catsp, catresult)


            Me.ProductCategoryDropDownList.AppendDataBoundItems = True
            Me.ProductCategoryDropDownList.Items.Add("")
            Me.ProductCategoryDropDownList.DataSource = catresult.Tables(0).DefaultView
            Me.ProductCategoryDropDownList.DataTextField = "PCDescription"
            Me.ProductCategoryDropDownList.DataValueField = "PCID"
            Me.ProductCategoryDropDownList.DataBind()
            ' End Added by Nimesh on 2nd July 2015
        Catch ex As Exception

        End Try
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim BPDelegate As New BuildProductDelegate(AddressOf BuildProduct)
        Me.ProductSetupControl.InvokeDelegate = BPDelegate

        If Page.IsPostBack OrElse _initException IsNot Nothing Then Return

        Try
            If String.IsNullOrEmpty(Me.CookieValue("Product_sapIsActive")) Then
                saleableProductRadioButtonList.SelectedValue = "Active-Current"
            Else
                saleableProductRadioButtonList.SelectedValue = "" & Me.CookieValue("Product_sapIsActive")
            End If
        Catch
        End Try

        BuildProduct(False)

        If Not String.IsNullOrEmpty(Request.QueryString("Create")) Then
            Me.AddInformationMessage("Product created")
        End If

        Me.ProductSetupControl.PrdId = _prdId

    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("Product_sapIsActive") = saleableProductRadioButtonList.SelectedValue
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("Search.aspx")
    End Sub

    Private Sub ValidateProduct(ByVal create As Boolean)
        If create Then
            If shortNameTextBox.Text.Trim() = "" Then
                Throw New Aurora.Common.ValidationException("Short name cannot be blank")
            End If

            Dim newProductDataSet As New ProductDataSet
            DataRepository.GetProductByShortName(newProductDataSet, Me.CompanyCode, shortNameTextBox.Text.Trim())
            For Each productRow0 As ProductRow In newProductDataSet.Package
                If productRow0.PrdShortName.ToLower().Trim() = shortNameTextBox.Text.ToLower().Trim() Then
                    Throw New Aurora.Common.ValidationException("Short name must be unique")
                End If
            Next
        End If

        If nameTextBox.Text.Trim() = "" Then
            Throw New Aurora.Common.ValidationException("Name cannot be blank")
        End If
    End Sub

    Private Sub UpdateFeatures()
        For Each featureRow As FeatureRow In _productDataSet.Feature
            Dim featureListItem As ListItem = featuresCheckBoxList.Items.FindByValue(featureRow.FtrId)
            If featureListItem Is Nothing Then Return

            Dim productFeatureRow As ProductFeatureRow = _productDataSet.ProductFeature.FindByPftPrdIdPftFtrId(_productRow.PrdId, featureRow.FtrId)

            If productFeatureRow Is Nothing AndAlso featureListItem.Selected Then
                productFeatureRow = _productDataSet.ProductFeature.NewProductFeatureRow()
                productFeatureRow.PftPrdId = _productRow.PrdId
                productFeatureRow.PftFtrId = featureRow.FtrId
                _productDataSet.ProductFeature.AddProductFeatureRow(productFeatureRow)

                Aurora.Common.Data.ExecuteDataRow(productFeatureRow, UsrId:=Me.UserId, PrgmName:=Me.PrgmName)
            ElseIf productFeatureRow IsNot Nothing AndAlso Not featureListItem.Selected Then
                productFeatureRow.Delete()
                Aurora.Common.Data.ExecuteDataRow(productFeatureRow, UsrId:=Me.UserId, PrgmName:=Me.PrgmName)
                _productDataSet.ProductFeature.RemoveProductFeatureRow(productFeatureRow)
            End If
        Next
    End Sub

    Private Sub UpdateProduct()
        _productRow.PrdName = nameTextBox.Text.Trim()
        _productRow.PrdTypId = typeDropDown.SelectedValue
        For Each listItem As ListItem In flagsCheckBoxList.Items
            If _productRow.Table.Columns.Contains(listItem.Value) Then _productRow(listItem.Value) = listItem.Selected
        Next
        _productRow.IsActive = statusRadioButtonList.Items(0).Selected

        ''rev:mia Aug 8 2011 added for the PFF
        _productRow.PrdTankSize = IIf(String.IsNullOrEmpty(Me.TankSizeTextBox.Text), 0, Me.TankSizeTextBox.Text)
        _productRow.PrdFuelType = IIf(String.IsNullOrEmpty(Me.FuelTypeDropDownList.SelectedItem.Text), "", Me.FuelTypeDropDownList.SelectedItem.Text)

        ''added by Nimesh on 2nd July 2015
        _productRow.PrdCatId = IIf(String.IsNullOrEmpty(Me.ProductCategoryDropDownList.SelectedItem.Value), vbNullString, Me.ProductCategoryDropDownList.SelectedItem.Value)
        ''End added by Nimesh on 2nd July 2015

        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(_productRow, UsrId:=Me.UserId, PrgmName:=Me.PrgmName)
    End Sub

    Private Sub DeactivateSaleableProducts()
        For Each saleableProductRow As SaleableProductRow In _productRow.GetSaleableProductRows
            If saleableProductRow.SapStatus = ProductConstants.SaleableProductStatus_Active Then
                saleableProductRow.SapStatus = ProductConstants.SaleableProductStatus_Inactive
                Aurora.Common.Data.ExecuteDataRow(saleableProductRow, UsrId:=Me.UserId, PrgmName:=Me.PrgmName)
            End If
        Next
    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateProduct(True)
        Catch ex As Aurora.Common.ValidationException
            BuildProduct(True)
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        Try
            _productRow = _productDataSet.Product.NewProductRow()
            _productRow.PrdId = DataRepository.GetNewId()
            _productRow.PrdShortName = shortNameTextBox.Text.Trim()
            _productRow.PrdName = nameTextBox.Text.Trim()
            _productRow.PrdTypId = typeDropDown.SelectedValue
            _productRow.PrdBrdCode = brandDropDown.SelectedValue

            UpdateIsVehicle()

            For Each listItem As ListItem In flagsCheckBoxList.Items
                If _productRow.Table.Columns.Contains(listItem.Value) Then _productRow(listItem.Value) = listItem.Selected
            Next
            _productRow.IsActive = statusRadioButtonList.Items(0).Selected
            _productDataSet.Product.AddProductRow(_productRow)

            Aurora.Common.Data.ExecuteDataRow(_productRow, UsrId:=Me.UserId, PrgmName:=Me.PrgmName)

            UpdateFeatures()
        Catch ex As Exception
            _productDataSet.Product.RemoveProductRow(_productRow)
            _productRow = Nothing
            BuildProduct(True)
            Me.AddErrorMessage("Error creating product")
            Aurora.Common.Logging.LogException("PRODUCT_CREATE", ex)
            Return
        End Try

        Response.Redirect(ProductUserControl.MakeProductUrl(Me.Page, prdId:=_productRow.PrdId) & "&create=true")
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return
        If _productRow Is Nothing Then Return
        UpdateIsVehicle()
        Try
            ValidateProduct(False)
        Catch ex As Aurora.Common.ValidationException
            BuildProduct(True)
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If _productRow.PrdIsActive And Not statusRadioButtonList.Items(0).Selected _
         AndAlso _productRow.GetSaleableProductRows().Length > 0 Then
            BuildProduct(True)
            confimationBoxControl.Show()
            Return
        End If

        Try
            UpdateProduct()
            UpdateFeatures()
        Catch ex As Exception
            BuildProduct(False)
            Me.AddErrorMessage("Error updating product")
            Aurora.Common.Logging.LogException("PRODUCT_UPDATE", ex)
            Return
        End Try

        statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(_productRow.StatusColor())

        Me.AddInformationMessage("Product updated")
        BuildProduct(False)
    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        If Not CanMaintain Then Return

        If leftButton Then
            Try
                UpdateProduct()
                UpdateFeatures()
                DeactivateSaleableProducts()
            Catch
                Me.AddErrorMessage("Error updating product")
            End Try

            Me.AddInformationMessage("Product updated")
            BuildProduct(False)
        Else
            BuildProduct(True)
            statusRadioButtonList.SelectedIndex = 0
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(_productRow.StatusColor())
        End If
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.Page, prdId:=_productRow.PrdId))
    End Sub

    Protected Sub saleableProductRadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles saleableProductRadioButtonList.SelectedIndexChanged
        UpdateCookieValues()

        BuildProduct(True)
    End Sub

    Protected Sub typeDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles typeDropDown.SelectedIndexChanged
        UpdateIsVehicle()
    End Sub


    Sub UpdateIsVehicle()
        ' check the box!
        'typeDropDown.Items(0).Selected = True 
        flagsCheckBoxList.Items(3).Selected = Aurora.Product.Data.IsTypeVehicle(typeDropDown.SelectedItem.Text.Split("-"c)(0).Trim())
    End Sub

   



End Class
