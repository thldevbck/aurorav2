Imports System.Collections.Generic


Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductEnquiry)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.SaleableProduct)> _
<AuroraPageTitle("Saleable Product Search")> _
Partial Class Product_SaleableProductSearch
    Inherits AuroraPage

    Private _productDataSet As New ProductDataSet()

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _productDataSet.EnforceConstraints = False

        DataRepository.GetProductLookups(_productDataSet, Me.CompanyCode)
        DataRepository.GetSaleableProductLookups(_productDataSet, Me.CompanyCode)

        brandDropDown.Items.Clear()
        brandDropDown.Items.Add(New ListItem("(All)", ""))
        For Each brandRow As BrandRow In _productDataSet.Brand
            brandDropDown.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
        Next

        typeDropDown.Items.Clear()
        typeDropDown.Items.Add(New ListItem("(All)", ""))
        For Each typeRow As TypeRow In _productDataSet.Type
            typeDropDown.Items.Add(New ListItem(typeRow.TypCode & " - " & typeRow.TypDesc, typeRow.TypId))
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem("(All)", ""))
        For Each countryRow As CountryRow In _productDataSet.Country
            countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        Next
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        shortNameTextBox.Text = "" & CookieValue("SA_prdShortName")
        nameTextBox.Text = "" & CookieValue("SA_prdName")

        Try
            If Not String.IsNullOrEmpty(CookieValue("SA_prdTypId")) Then
                typeDropDown.SelectedValue = CookieValue("SA_prdTypId")
            End If
        Catch
            typeDropDown.SelectedIndex = 0
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("SA_prdBrdCode")) Then
                brandDropDown.SelectedValue = CookieValue("SA_prdBrdCode")
            End If
        Catch
            brandDropDown.SelectedIndex = 0
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("SA_ctyCode")) Then
                countryDropDown.SelectedValue = CookieValue("SA_ctyCode")
            End If
        Catch
            countryDropDown.SelectedIndex = 0
        End Try

        packagePicker.DataId = CookieValue("SA_pkgId")
        packagePicker.Text = CookieValue("SA_pkgText")

        If Not String.IsNullOrEmpty(CookieValue("SA_sapStatus")) Then
            Try
                statusRadioButtonList.SelectedValue = CookieValue("SA_sapStatus")
            Catch
                statusRadioButtonList.SelectedIndex = 0
            End Try
        End If

        For Each name As String In New String() {"sapIsBase", "sapIsFlex"}
            flagsCheckBoxList.Items.FindByValue(name).Selected = Not String.IsNullOrEmpty(CookieValue("SA_" & name))
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Me.SearchParam) Then
                shortNameTextBox.Text = Me.SearchParam
            Else
                BuildForm(False)
            End If

            If HasSearchParameters() Then searchButton_Click(Me, Nothing)
        End If
    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("SA_prdShortName") = shortNameTextBox.Text.Trim()
        CookieValue("SA_prdName") = nameTextBox.Text.Trim()
        CookieValue("SA_prdTypId") = typeDropDown.SelectedValue
        CookieValue("SA_prdBrdCode") = brandDropDown.SelectedValue
        CookieValue("SA_ctyCode") = countryDropDown.SelectedValue
        CookieValue("SA_pkgId") = packagePicker.DataId
        CookieValue("SA_pkgText") = packagePicker.Text
        CookieValue("SA_sapStatus") = statusRadioButtonList.SelectedValue
        For Each name As String In New String() {"sapIsBase", "sapIsFlex"}
            CookieValue("SA_" & name) = IIf(flagsCheckBoxList.Items.FindByValue(name).Selected, "True", Nothing)
        Next
    End Sub

    Public Function HasSearchParameters() As Boolean
        Return shortNameTextBox.Text.Trim().Length > 0 _
         OrElse nameTextBox.Text.Trim().Length > 0 _
         OrElse typeDropDown.SelectedIndex > 0 _
         OrElse brandDropDown.SelectedIndex > 0 _
         OrElse countryDropDown.SelectedIndex > 0 _
         OrElse Not String.IsNullOrEmpty(packagePicker.DataId)
    End Function

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        If Not HasSearchParameters() Then
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "Enter valid search parameters")
            searchResultLabel.Text = ""
            searchResultTable.Visible = False
            Return
        End If

        UpdateCookieValues()

        Dim prdShortName As String = shortNameTextBox.Text.Trim()
        Dim sapSuffix As New Nullable(Of Integer)
        Dim prdShortNameArray As String() = prdShortName.Split("-")
        If prdShortNameArray.Length > 1 Then
            Dim prdShortNameNew As String = prdShortNameArray(0).Trim()
            For i As Integer = 1 To prdShortNameArray.Length - 2
                prdShortNameNew &= "-" & prdShortNameArray(i).Trim()
            Next
            Dim sapSuffixNew As Integer = -1
            Integer.TryParse(prdShortNameArray(prdShortNameArray.Length - 1), sapSuffixNew)
            If sapSuffixNew > 0 Then
                prdShortName = prdShortNameNew
                sapSuffix = New Nullable(Of Integer)(sapSuffixNew)
            End If
        End If

        Dim sapIsBase As New Nullable(Of Boolean)
        If flagsCheckBoxList.Items.FindByValue("sapIsBase").Selected Then sapIsBase = New Nullable(Of Boolean)(True)

        Dim sapIsFlex As New Nullable(Of Boolean)
        If flagsCheckBoxList.Items.FindByValue("sapIsFlex").Selected Then sapIsFlex = New Nullable(Of Boolean)(True)

        Dim pkgId As String = Nothing
        If Not String.IsNullOrEmpty(packagePicker.DataId) AndAlso Not String.IsNullOrEmpty(packagePicker.Text) Then
            pkgId = packagePicker.DataId
        End If

        Dim sapStatus As String = Nothing
        Dim currentFutureOnly As Boolean = False
        'If statusRadioButtonList.SelectedValue = "Current" Then
        '    sapStatus = "Active"
        '    currentFutureOnly = True
        'ElseIf statusRadioButtonList.SelectedValue = "Current" Then
        '    sapStatus = "Active"
        'ElseIf statusRadioButtonList.SelectedValue = "Pending" Then
        '    sapStatus = "Pending"
        'ElseIf statusRadioButtonList.SelectedValue = "Inactive" Then
        '    sapStatus = "Inactive"
        'End If

        Select Case statusRadioButtonList.SelectedValue
            Case "Active-Current"
                sapStatus = "Active"
                currentFutureOnly = True
            Case "Active-Future"
                sapStatus = "Active"
                currentFutureOnly = True
            Case "Active-Past"
                sapStatus = "Active"
            Case "Pending"
                sapStatus = "Pending"
            Case "Inactive"
                sapStatus = "Inactive"
            Case "All"
                sapStatus = ""
        End Select

        DataRepository.SearchSaleableProduct(_productDataSet, _
            Me.CompanyCode, _
            Nothing, _
            typeDropDown.SelectedValue, _
            brandDropDown.SelectedValue, _
            prdShortName, _
            nameTextBox.Text.Trim(), _
            countryDropDown.SelectedValue, _
            sapSuffix, _
            sapStatus, _
            sapIsBase, _
            sapIsFlex, _
            pkgId, _
            currentFutureOnly, _
            True, _
            True)

        Dim currentDate As Date = Date.Now

        Dim index As Integer = 0
        For Each saleableProductRow As SaleableProductRow In _productDataSet.SaleableProduct
            'If (statusRadioButtonList.SelectedValue = "Current" AndAlso Not (saleableProductRow.HasCurrent(currentDate) OrElse saleableProductRow.HasFuture(currentDate))) _
            ' OrElse (statusRadioButtonList.SelectedValue = "Active" AndAlso Not saleableProductRow.IsActive()) _
            ' OrElse (statusRadioButtonList.SelectedValue = "Pending" AndAlso Not saleableProductRow.IsPending()) _
            ' OrElse (statusRadioButtonList.SelectedValue = "Inactive" AndAlso Not saleableProductRow.IsInactive()) Then
            '    Continue For
            'End If

            Select Case statusRadioButtonList.SelectedValue
                Case "Active-Current"
                    If Not saleableProductRow.HasCurrent(currentDate) Or Not saleableProductRow.IsActive() Then
                        Continue For
                    End If
                Case "Active-Future"
                    If Not saleableProductRow.HasFuture(currentDate) Or Not saleableProductRow.IsActive() Then
                        Continue For
                    End If
                Case "Active-Past"
                    If saleableProductRow.HasFuture(currentDate) Or saleableProductRow.HasCurrent(currentDate) Or Not saleableProductRow.IsActive() Then
                        Continue For
                    End If
                Case "Pending"
                    If Not saleableProductRow.IsPending() Then
                        Continue For
                    End If
                Case "Inactive"
                    If Not saleableProductRow.IsInactive() Then
                        Continue For
                    End If
            End Select

            Dim tableRow As New TableRow()
            tableRow.BackColor = saleableProductRow.StatusColor(currentDate)
            searchResultTable.Rows.Add(tableRow)

            Dim nameCell As New TableCell()
            Dim nameHyperLink As New HyperLink()
            nameHyperLink.Text = Server.HtmlEncode(saleableProductRow.ProductRow.PrdShortName & " - " & saleableProductRow.SapSuffix.ToString())
            nameHyperLink.NavigateUrl = ProductUserControl.MakeSaleableProductUrl(Me, prdId:=saleableProductRow.SapPrdId, sapId:=saleableProductRow.SapId) & "&BackUrl=" & Server.UrlEncode(Me.Request.RawUrl)
            nameCell.Controls.Add(nameHyperLink)
            nameCell.Controls.Add(New LiteralControl("<br/>" & Server.HtmlEncode(saleableProductRow.ProductRow.PrdName)))
            tableRow.Controls.Add(nameCell)

            Dim brandCell As New TableCell()
            If saleableProductRow.ProductRow.BrandRow IsNot Nothing Then
                brandCell.Text = Server.HtmlEncode(saleableProductRow.ProductRow.BrandRow.BrdCode)
            Else
                brandCell.Text = ""
            End If
            tableRow.Controls.Add(brandCell)

            Dim countryCell As New TableCell()
            If saleableProductRow.CountryRow IsNot Nothing Then
                countryCell.Text = Server.HtmlEncode(saleableProductRow.CountryRow.CtyCode)
            Else
                countryCell.Text = ""
            End If
            tableRow.Controls.Add(countryCell)

            Dim currencyCell As New TableCell()
            currencyCell.Text = saleableProductRow.SapCodCurrCode
            tableRow.Controls.Add(currencyCell)

            Dim attributeCell As New TableCell()
            attributeCell.Text = saleableProductRow.GetProductAttributeSummaryHtml()
            tableRow.Controls.Add(attributeCell)

            Dim rateCell As New TableCell()
            rateCell.Text = ""
            If saleableProductRow.GetTravelBandSetRows().Length = 1 _
             AndAlso saleableProductRow.GetTravelBandSetRows()(0).GetProductRateTravelBandRows().Length = 1 Then
                Dim productRateTravelBandRow As ProductRateTravelBandRow = saleableProductRow.GetTravelBandSetRows()(0).GetProductRateTravelBandRows()(0)
                rateCell.Text = productRateTravelBandRow.BuildRateSummeryHtml()
            Else
                rateCell.Text = ""
            End If
            tableRow.Controls.Add(rateCell)

            Dim flagsCell As New TableCell()
            Dim flagsList As New List(Of String)
            If Not saleableProductRow.IsSapIsBaseNull AndAlso saleableProductRow.SapIsBase Then flagsList.Add("Base")
            If Not saleableProductRow.IsSapIsFlexNull AndAlso saleableProductRow.SapIsFlex Then flagsList.Add("Flex")
            flagsCell.Text = String.Join(" /<br>", flagsList.ToArray())
            tableRow.Controls.Add(flagsCell)

            Dim statusCell As New TableCell()
            statusCell.Text = Server.HtmlEncode(saleableProductRow.StatusText(currentDate))
            tableRow.Controls.Add(statusCell)

            index += 1
        Next

        If index <= 0 Then
            searchResultLabel.Text = "No Saleable products found"
            searchResultLabel.Visible = True
            searchResultTable.Visible = False
        ElseIf index = 1 Then
            searchResultLabel.Text = index & " Saleable product found"
            searchResultLabel.Visible = True
            searchResultTable.Visible = True
        Else
            searchResultLabel.Text = index & " Saleable products found"
            searchResultLabel.Visible = True
            searchResultTable.Visible = True
        End If

    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        shortNameTextBox.Text = ""
        nameTextBox.Text = ""
        typeDropDown.SelectedIndex = 0
        brandDropDown.SelectedIndex = 0
        countryDropDown.SelectedIndex = 0
        packagePicker.DataId = Nothing
        packagePicker.Text = ""
        statusRadioButtonList.SelectedIndex = 0
        For Each listItem As ListItem In flagsCheckBoxList.Items
            listItem.Selected = False
        Next
        searchResultTable.Visible = False
        searchResultLabel.Text = ""

        UpdateCookieValues()
    End Sub
End Class
