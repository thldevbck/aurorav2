<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Product.aspx.vb" Inherits="Product_Product"
    MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\ConfirmationBox\ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/NonAvailabilityControl.ascx" TagName="NonAvailabilityControl"
    TagPrefix="NonAvailabilityUserControl" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="CollapsiblePanelUserControl" %>
<%@ Register Src="Controls/ProductSetupControl.ascx" TagName="ProductSetupControl"
    TagPrefix="ProductSetupControl" %>
<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .saleableProductTable td { vertical-align: top; }
        .saleableProductTable ul { margin: 0 0 0 16px; padding: 0; }
        .saleableProductTable li { margin: 0px; padding: 0; list-style-type: square; }
    </style>

    <script language="javascript" type="text/javascript">
     function onPageSearch()
       {
            WindowNavigate('Product/Search.aspx','prdId=' + '<%= request.querystring("prdId") %>');
       }

       //rev:mia Aug 8 2011 added for the PFF
       function IsValidSized(sender) {
           if (isNaN(sender.value) && (sender.value.length != 0)) {
               sender.value = ""
           }
       }

       function CurrencyFormatted(sender) {
           amount = sender.value
           var i = parseFloat(amount);
           if (isNaN(i)) { i = 0.00; }
           var minus = '';
           if (i < 0) { minus = '-'; }
           i = Math.abs(i);
           i = parseInt((i + .005) * 100);
           i = i / 100;
           s = new String(i);
           if (s.indexOf('.') < 0) { s += '.00'; }
           if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
           s = minus + s;
           //return s;
           sender.value = s
       }

    </script>

</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:Panel ID="productEditPanel" runat="Server" Width="780px" CssClass="productEdit">
        <asp:UpdatePanel ID="updMain" runat="server">
            <contenttemplate>
                <table style="width:100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width: 150px">Short Name:</td>
                        <td style="width: 250px">
                            <asp:TextBox ID="shortNameTextBox" runat="server" style="width:100px" MaxLength="8" />
                        </td>
                        <td style="width: 150px">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>                
                    <tr>
                        <td>Name:</td>
                        <td colspan="3"><asp:TextBox ID="nameTextBox" runat="server" style="width:100%" MaxLength="64" /></td>
                    </tr>
                    <tr>
                        <td>Brand:</td>
                        <td>
                            <asp:DropDownList ID="brandDropDown" runat="server" style="width:150px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                        <td>Type:</td>
                        <td>
                            <asp:DropDownList ID="typeDropDown" runat="server" style="width:250px" AutoPostBack="true">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>DVASS Seq:</td>
                        <td colspan="3"><asp:TextBox ID="dvassSeqTextBox" runat="server" style="width:150px" ReadOnly="true" /></td>
                    </tr>
                    <!--rev:mia Aug 8 2011 added for the PFF-->
                    <tr>
                        <td> Tank Size: </td>
                        <td><asp:TextBox ID="TankSizeTextBox" runat="server" style="width:150px" ReadOnly="false" MaxLength="6" /></td>
                        <td> Fuel Type: </td>
                        <td>
                            <asp:DropDownList ID="FuelTypeDropDownList" runat="server" style="width:250px" AutoPostBack="true">
                              <%--  <asp:ListItem  Text="" Value=""/>
                                <asp:ListItem  Text="Diesel" Value="Diesel"/>
                                <asp:ListItem  Text="Petrol" Value="Petrol"/>
                                <asp:ListItem  Text="Both" Value="Both"/>--%>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td> Product Category: </td>
                        <td>
                            <asp:DropDownList ID="ProductCategoryDropDownList" runat="server" style="width:250px" AutoPostBack="true"></asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">Features:</td>
                        <td colspan="1" style="border-top-width: 1px; border-top-style: dotted; padding: 0;">Flags:</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="border-top-width: 1px; border-top-style: dotted; margin: 0; padding: 0; vertical-align: top">
                            <asp:CheckBoxList ID="featuresCheckBoxList" runat="server" RepeatDirection="Vertical" RepeatLayout="Table" RepeatColumns="2" Width="100%">
                            </asp:CheckBoxList>
                        </td>
                        <td colspan="1" style="border-top-width: 1px; border-top-style: dotted; margin: 0; padding: 0; vertical-align: top;">
                            <div class="tablePadding">
                                <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Vertical" 
                                                  RepeatLayout="Table" RepeatColumns="1" Width="100%">
                                    <asp:ListItem Value="PrdIsSerialised">Serialised</asp:ListItem>
                                    <asp:ListItem Value="PrdPostToFinan" Selected="true">Posted to Financials</asp:ListItem>
                                    <asp:ListItem Value="PrdIsPrimary">Primary</asp:ListItem>
                                    <asp:ListItem Value="PrdIsVehicle" Enabled="false" >Vehicle</asp:ListItem>
                                    <asp:ListItem Value="PrdIsToPostToSS">Posted to Smart Stream</asp:ListItem>
                                    <asp:ListItem Value="PrdIsFltExp">Fleet Expense</asp:ListItem>
                                    <asp:ListItem Value="PrdToAppearOnFS">Appear on Fleet Status</asp:ListItem>
                                    <asp:ListItem Value="PrdIsInclusive">Inclusive</asp:ListItem>
                                    <asp:ListItem Value="PrdIsCommissionable">Commissionable</asp:ListItem>
                                </asp:CheckBoxList>
                            </div>
                        </td>
                    </tr>
                </table>
            </contenttemplate>
        </asp:UpdatePanel>
        <table style="width: 100%; border-width: 1px 0px;" cellpadding="2" cellspacing="0"
            id="statusTable" runat="server">
            <tr>
                <td style="width: 150px;">
                    Status:</td>
                <td colspan="3">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal"
                        RepeatColumns="2" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Inactive" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <table style="width: 100%;" cellpadding="2" cellspacing="0">
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        Visible="False" />
                    <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        Visible="False" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
        <br />
        <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="0" Width="100%">
            <ajaxToolkit:TabPanel runat="server" ID="saleableProductTabPanel" HeaderText="Saleable Products">
                <ContentTemplate>
                    <table style="width: 100%;" cellpadding="2" cellspacing="0">
                        <tr>
                            <td style="width: 150px; border-width: 1px 0px;">
                                <b>Saleable Products:</b></td>
                            <td style="border-width: 1px 0px">
                                <asp:RadioButtonList ID="saleableProductRadioButtonList" runat="server" RepeatDirection="Horizontal"
                                    RepeatColumns="5" RepeatLayout="Table" AutoPostBack="True" CssClass="repeatLabelTable">
                                    <%-- <asp:ListItem Text="Current+Future" Value="Current" />
                                    <asp:ListItem Text="Active" Value="Active" />
                                    <asp:ListItem Text="Pending" Value="Pending" />
                                    <asp:ListItem Text="Inactive" Value="Inactive" />
                                    <asp:ListItem Text="All" Value="All" />--%>
                                    <asp:ListItem Text="Active-Current" Selected="True" />
                                    <asp:ListItem Text="Active-Future" />
                                    <asp:ListItem Text="Active-Past" />
                                    <asp:ListItem Text="Pending" />
                                    <asp:ListItem Text="Inactive" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 100%;" cellpadding="2" cellspacing="0">
                        <tr>
                            <td colspan="2">
                                <i>
                                    <asp:Label ID="saleableProductLabel" runat="server" /></i>&nbsp;</td>
                            <td align="right" width="150px">
                                &nbsp;
                                <asp:Button ID="addButton" runat="server" Text="New" CssClass="Button_Standard Button_New"
                                    Visible="False" />
                            </td>
                        </tr>
                    </table>
                    <asp:Table ID="saleableProductTable" runat="server" CellPadding="2" CellSpacing="0"
                        Width="100%" Visible="False" CssClass="dataTableColor saleableProductTable">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell Text="Name" />
                            <asp:TableHeaderCell Text="Cty" Width="40px" />
                            <asp:TableHeaderCell Text="Cur" Width="40px" />
                            <asp:TableHeaderCell Text="Attributes" Width="220px" />
                            <asp:TableHeaderCell Text="Current Rate Summary" Width="220px" />
                            <asp:TableHeaderCell Text="Flags" Width="60px" />
                            <asp:TableHeaderCell Text="Status" Width="60px" />
                        </asp:TableHeaderRow>
                    </asp:Table>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" ID="nonAvailabilityTabPanel" HeaderText="Non-Availability">
                <ContentTemplate>
                    <NonAvailabilityUserControl:NonAvailabilityControl ID="nonAvailabilityControl" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" ID="ProductTabPanel" HeaderText="Web Product">
                <ContentTemplate>
                    <ProductSetupControl:ProductSetupControl ID="ProductSetupControl" runat="server" />
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
        </ajaxToolkit:TabContainer>
    </asp:Panel>
    <uc1:ConfimationBoxControl ID="confimationBoxControl" runat="server" LeftButton_Text="Continue"
        RightButton_Text="Cancel" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="true"
        Title="Update Product" Text="Setting a product inactive will inactivate all the saleable products too.  Are you sure you want to do this?" />
</asp:Content>
