Imports System.Collections.Generic


Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductEnquiry)> _
<AuroraPageTitle("Product Search")> _
Partial Class Product_Search
    Inherits AuroraPage

    Private _productDataSet As New ProductDataSet()

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.ProductMaintenance)
        End Get
    End Property


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _productDataSet.EnforceConstraints = False

        DataRepository.GetProductLookups(_productDataSet, Me.CompanyCode)

        brandDropDown.Items.Clear()
        brandDropDown.Items.Add(New ListItem("(All)", ""))
        For Each brandRow As BrandRow In _productDataSet.Brand
            brandDropDown.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
        Next

        typeDropDown.Items.Clear()
        typeDropDown.Items.Add(New ListItem("(All)", ""))
        For Each typeRow As TypeRow In _productDataSet.Type
            typeDropDown.Items.Add(New ListItem(typeRow.TypCode & " - " & typeRow.TypDesc, typeRow.TypId))
        Next

        featuresCheckBoxList.Items.Clear()
        For Each featureRow As FeatureRow In _productDataSet.Feature
            featuresCheckBoxList.Items.Add(New ListItem(featureRow.FtrDesc, featureRow.FtrId))
        Next
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        Try
            If Not String.IsNullOrEmpty(CookieValue("Product_prdTypId")) Then
                typeDropDown.SelectedValue = CookieValue("Product_prdTypId")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("Product_prdBrdCode")) Then
                brandDropDown.SelectedValue = CookieValue("Product_prdBrdCode")
            End If
        Catch
        End Try

        shortNameTextBox.Text = "" & CookieValue("Product_prdShortName")
        nameTextBox.Text = "" & CookieValue("Product_prdName")

        If Not String.IsNullOrEmpty(CookieValue("Product_prdIsActive")) Then
            If CBool(CookieValue("Product_prdIsActive")) Then
                statusRadioButtonList.Items(1).Selected = True
            Else
                statusRadioButtonList.Items(2).Selected = True
            End If
        End If

        For i As Integer = 0 To 4
            Dim cookieName As String = "Product_ftrId" & CStr(i)
            If Not String.IsNullOrEmpty(CookieValue(cookieName)) AndAlso featuresCheckBoxList.Items.FindByValue(CookieValue(cookieName)) IsNot Nothing Then
                featuresCheckBoxList.Items.FindByValue(CookieValue(cookieName)).Selected = True
            End If
        Next

        ' SHOEL - Phoenix Change
        For j As Integer = 0 To cblWebEnabledProductType.Items.Count - 1
            cblWebEnabledProductType.Items(j).Selected = CBool(CookieValue("Product_WEPT_" & j.ToString()))
        Next
        ' SHOEL - Phoenix Change

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        createButton.Visible = CanMaintain

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Me.SearchParam) Then
                shortNameTextBox.Text = Me.SearchParam
            Else
                BuildForm(False)
            End If

            If HasSearchParameters() Then searchButton_Click(Me, Nothing)
        End If
    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("Product_prdTypId") = typeDropDown.SelectedValue
        CookieValue("Product_prdBrdCode") = brandDropDown.SelectedValue
        CookieValue("Product_prdShortName") = shortNameTextBox.Text.Trim()
        CookieValue("Product_prdName") = nameTextBox.Text.Trim()
        If statusRadioButtonList.Items(1).Selected Then
            CookieValue("Product_prdIsActive") = "True"
        ElseIf statusRadioButtonList.Items(2).Selected Then
            CookieValue("Product_prdIsActive") = "False"
        Else
            CookieValue("Product_prdIsActive") = ""
        End If

        For i As Integer = 0 To 4
            Dim cookieName As String = "Product_ftrId" & CStr(i)
            CookieValue(cookieName) = ""
        Next
        Dim index As Integer = 0
        For Each featureListItem As ListItem In featuresCheckBoxList.Items
            If index < 5 AndAlso featureListItem.Selected Then
                Dim cookieName As String = "Product_ftrId" & CStr(index)
                CookieValue(cookieName) = featureListItem.Value
                index += 1
            End If
        Next

        ' SHOEL - Phoenix Change
        For j As Integer = 0 To cblWebEnabledProductType.Items.Count - 1
            CookieValue("Product_WEPT_" & j.ToString()) = cblWebEnabledProductType.Items(j).Selected
        Next
        ' SHOEL - Phoenix Change

    End Sub

    Public Function HasSearchParameters() As Boolean
        If typeDropDown.SelectedIndex > 0 _
            OrElse brandDropDown.SelectedIndex > 0 _
            OrElse shortNameTextBox.Text.Trim().Length > 0 _
            OrElse nameTextBox.Text.Trim().Length > 0 _
            OrElse Not statusRadioButtonList.Items(0).Selected _
            OrElse cblWebEnabledProductType.Items(0).Selected _
            OrElse cblWebEnabledProductType.Items(1).Selected _
            OrElse False Then Return True

        For Each listItem As ListItem In featuresCheckBoxList.Items
            If listItem.Selected Then Return True
        Next

        Return False
    End Function

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        If Not HasSearchParameters() Then
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "Enter valid search parameters")
            searchResultLabel.Text = ""
            searchResultTable.Visible = False
            Return
        End If

        UpdateCookieValues()

        Dim prdIsActive As New Nullable(Of Boolean)
        If statusRadioButtonList.Items(1).Selected Then
            prdIsActive = New Nullable(Of Boolean)(True)
        ElseIf statusRadioButtonList.Items(2).Selected Then
            prdIsActive = New Nullable(Of Boolean)(False)
        End If

        Dim ftrId As String() = New String() {Nothing, Nothing, Nothing, Nothing, Nothing}
        Dim index As Integer = 0
        For Each featureListItem As ListItem In featuresCheckBoxList.Items
            If index < ftrId.Length AndAlso featureListItem.Selected Then
                ftrId(index) = featureListItem.Value
                index += 1
            End If
        Next

        ' SHOEL - Phoenix Change
        Dim iWebEnabledSelection As Integer = 0 ' Default All Products
        If cblWebEnabledProductType.Items(0).Selected Then
            iWebEnabledSelection = 1 ' Web Enabled Vehicles
            If cblWebEnabledProductType.Items(1).Selected Then
                iWebEnabledSelection = 3 ' Web Enabled Vehicles & Non-Vehicles
            End If
        ElseIf cblWebEnabledProductType.Items(1).Selected Then
            iWebEnabledSelection = 2 ' Web Enabled Non-Vehicles
        End If

        ' SHOEL - Phoenix Change

        DataRepository.SearchProduct(_productDataSet, _
            Me.CompanyCode, _
            Nothing, _
            typeDropDown.SelectedValue, _
            brandDropDown.SelectedValue, _
            shortNameTextBox.Text.Trim(), _
            nameTextBox.Text.Trim(), _
            prdIsActive, _
            ftrId(0), _
            ftrId(1), _
            ftrId(2), _
            ftrId(3), _
            ftrId(4), _
            iWebEnabledSelection)

        While searchResultTable.Rows.Count > 1
            searchResultTable.Rows.RemoveAt(1)
        End While

        If _productDataSet.Product.Rows.Count = 0 Then
            searchResultLabel.Text = "No products found"
            searchResultTable.Visible = False
        Else
            If _productDataSet.Product.Rows.Count = 1 Then
                searchResultLabel.Text = _productDataSet.Product.Rows.Count.ToString() & " product found"
            Else
                searchResultLabel.Text = _productDataSet.Product.Rows.Count.ToString() & " products found"
            End If
            searchResultTable.Visible = True

            index = 0
            For Each productRow As ProductRow In _productDataSet.Product
                Dim tableRow As New TableRow()
                tableRow.BackColor = productRow.StatusColor()
                searchResultTable.Rows.Add(tableRow)

                Dim nameCell As New TableCell()
                Dim nameHyperLink As New HyperLink()
                nameHyperLink.Text = Server.HtmlEncode(productRow.PrdShortName & " - " & productRow.PrdName)
                nameHyperLink.NavigateUrl = ProductUserControl.MakeProductUrl(Me.Page, prdId:=productRow.PrdId)
                nameCell.Controls.Add(nameHyperLink)
                tableRow.Controls.Add(nameCell)

                Dim brandCell As New TableCell()
                If productRow.BrandRow IsNot Nothing Then
                    brandCell.Text = Server.HtmlEncode(productRow.BrandRow.BrdCode & " - " & productRow.BrandRow.BrdName)
                Else
                    brandCell.Text = ""
                End If
                tableRow.Controls.Add(brandCell)

                Dim typeCell As New TableCell()
                typeCell.Text = ""
                If productRow.TypeRow IsNot Nothing Then
                    typeCell.Text = Server.HtmlEncode(productRow.TypeRow.TypCode & " - " & productRow.TypeRow.TypDesc)
                End If
                tableRow.Controls.Add(typeCell)

                Dim featuresCell As New TableCell()
                featuresCell.Text = ""
                For Each productFeatureRow As ProductFeatureRow In productRow.GetProductFeatureRows()
                    If productFeatureRow.FeatureRow IsNot Nothing Then
                        If Not String.IsNullOrEmpty(featuresCell.Text) Then featuresCell.Text += ",<br/>"
                        featuresCell.Text += Server.HtmlEncode(productFeatureRow.FeatureRow.FtrDesc)
                    End If
                Next
                tableRow.Controls.Add(featuresCell)

                Dim statusCell As New TableCell()
                If Not productRow.IsPrdIsActiveNull Then
                    statusCell.Text = IIf(productRow.PrdIsActive, "Active", "Inactive")
                Else
                    statusCell.Text = ""
                End If
                tableRow.Controls.Add(statusCell)

                index += 1
            Next
        End If

    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If Not CanMaintain Then Return

        UpdateCookieValues()

        Response.Redirect(ProductUserControl.MakeProductUrl(Me.Page))
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        typeDropDown.SelectedIndex = 0
        brandDropDown.SelectedIndex = 0
        shortNameTextBox.Text = ""
        nameTextBox.Text = ""
        statusRadioButtonList.Items(0).Selected = True
        For Each listItem As ListItem In featuresCheckBoxList.Items
            listItem.Selected = False
        Next
        searchResultTable.Visible = False
        searchResultLabel.Text = ""
        cblWebEnabledProductType.Items(0).Selected = False
        cblWebEnabledProductType.Items(1).Selected = False

        UpdateCookieValues()
    End Sub
End Class
