<%@ Page Language="VB" MasterPageFile="~/Include/PopupHeader.master" AutoEventWireup="false" CodeFile="RateSetReport.aspx.vb" Inherits="Product_RateSetReport" Title="Untitled Page" ValidateRequest="False" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <asp:Panel ID="saleableProductEditPanel" runat="Server">
        <asp:UpdatePanel runat="server" ID="saleableProductUpdatePanel">
            <ContentTemplate>
                <table style="width:100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width:150px;">Product:</td>
                        <td><asp:TextBox ID="productNameTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="400px" /></td>
                    </tr>
                    <tr>
                        <td>Saleable Product:</td>
                        <td><asp:TextBox ID="saleableProductNameTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="400px" /></td>
                    </tr>
                    <tr>
                        <td>Rate Set:</td>
                        <td><asp:TextBox ID="rateSetTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="400px" /></td>
                    </tr>
                </table>

                <hr />

                <asp:Table ID="productRateTravelBandTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" CssClass="dataTable">
                    <asp:TableHeaderRow>
                        <asp:TableHeaderCell Style="width: 200px">Type</asp:TableHeaderCell>
                        <asp:TableHeaderCell Style="width: 40px; padding-right: 10px">Qty</asp:TableHeaderCell>
                        <asp:TableHeaderCell Style="width: 100px">Uom</asp:TableHeaderCell>
                        <asp:TableHeaderCell Style="width: 50px; text-align: right">Amount&nbsp;</asp:TableHeaderCell>
                        <asp:TableHeaderCell Style="width: 20px">&nbsp;</asp:TableHeaderCell>
                        <asp:TableHeaderCell>Description</asp:TableHeaderCell>
                    </asp:TableHeaderRow>
                </asp:Table>

            </ContentTemplate>
        </asp:UpdatePanel>
       
        <br />

    </asp:Panel>
    
</asp:Content>
