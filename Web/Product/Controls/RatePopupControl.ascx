<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RatePopupControl.ascx.vb" Inherits="Product_RatePopupControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<asp:Button runat="server" ID="ratePopupButton" Style="display: none" />
<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="ratePopup" 
    BehaviorID="ratePopupBehavior"
    TargetControlID="ratePopupButton" 
    PopupControlID="ratePopupPanel"
    BackgroundCssClass="modalBackground" 
    DropShadow="True" 
    PopupDragHandleControlID="ratePopupDragPanel" />

<asp:Panel runat="server" CssClass="modalPopup" ID="ratePopupPanel" Style="display: none; padding: 10px;" Width="500px" Height="275px">
    <asp:Panel runat="Server" ID="ratePopupDragPanel" CssClass="modalPopupTitle" >
        <asp:Label ID="ratePopupTitleLabel" runat="Server" Text="Rate" />
    </asp:Panel>

    <asp:HiddenField ID="idHidden" runat="server" />
    <asp:HiddenField ID="parentPtbIdHidden" runat="server" />
    <asp:HiddenField ID="parentBapIdHidden" runat="server" />

    <div style="width: 475px; height:35px; padding: 2px 0 2px 0;">
        <uc1:MessagePanelControl runat="server" ID="rateMessagePanelControl" CssClass="popupMessagePanel" />
    </div>
    
    <div runat="server" id="containerDiv" style="width: 100%; height: 165px">
        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td style="width:100px">Rate Type:</td>
                <td style="width:40px">&nbsp;</td>
                <td colspan="3">
                    <asp:DropDownList ID="typeDropDown" runat="server" Width="300px" AutoPostBack="True">
                        <asp:ListItem Value="ProductRateTravelBandRow" Text="Base" />
                        <asp:ListItem Value="AgentDependentRateRow" Text="Agent Dependent" />
                        <asp:ListItem Value="VehicleDependentRateRow" Text="Vehicle Dependent" />
                        <asp:ListItem Value="ProductRateBookingBandRow" Text="Booked Date Availablity" />
                        <asp:ListItem Value="LocationDependentRateRow" Text="Location Dependent" />
                        <asp:ListItem Value="PersonDependentRateRow" Text="Person Dependent" />
                        <asp:ListItem Value="FixedRateRow" Text="Fixed" />
                        <asp:ListItem Value="PersonRateRow" Text="Person" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="quantityTableRow" runat="server">
                <td>Quantity:</td>
                <td>&nbsp;</td>
                <td colspan="3">
                    <uc1:RegExTextBox 
                        ID="quantityTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="false"
                        RegExPattern="^-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Rate Quantity must be a decimal value" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
            </tr>
            <tr id="fromToTableRow" runat="server">
                <td>Quantity</td>
                <td>From:</td>
                <td>
                    <uc1:RegExTextBox 
                        ID="fromTextBox" 
                        runat="server" 
                        Width="100px" 
                        Nullable="false"
                        RegExPattern="-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Rate Quantity From must be a decimal value" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
                <td>To:</td>
                <td>
                    <uc1:RegExTextBox 
                        ID="toTextBox" 
                        runat="server" 
                        Width="100px" 
                        Nullable="false"
                        RegExPattern="-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Rate Quantity To must be a decimal value" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
            </tr>
            <tr id="uomTableRow" runat="server">
                <td>UoM:</td>
                <td>&nbsp;</td>
                <td colspan="3"><asp:DropDownList ID="uomDropDown" runat="server" Width="150px" /></td>
            </tr>
            <tr>
                <td>Rate:</td>
                <td>&nbsp;&nbsp;$</td>
                <td colspan="3">
                    <uc1:RegExTextBox 
                        ID="rateTextBox" 
                        runat="server" 
                        Width="50px"
                        Nullable="false" 
                        ErrorMessage="Rate value must be a decimal value greater than zero" 
                        MessagePanelControlID="rateMessagePanelControl" MaxLength="8" />
                </td>
            </tr>
            <tr id="chargeTableRow" runat="server">
                <td>&nbsp;</td>
                <td>Min:</td>
                <td>
                    <uc1:RegExTextBox 
                        ID="minChargeTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="true" 
                        ErrorMessage="Rate Min Charge must be a decimal value greater than zero" 
                        MessagePanelControlID="rateMessagePanelControl" MaxLength="8" />
                </td>
                <td>Max:</td>
                <td>
                    <uc1:RegExTextBox 
                        ID="maxChargeTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="true" 
                        ErrorMessage="Rate Max Charge must be a decimal value greater than zero" 
                        MessagePanelControlID="rateMessagePanelControl" MaxLength="8" />
                </td>
            </tr>
            <tr id="agentTableRow" runat="server">
                <td>Agent:</td>
                <td>&nbsp;</td>
                <td colspan="3">
                    <uc1:PickerControl 
                        ID="agentPicker" 
                        runat="server" 
                        PopupType="AGENT" 
                        AppendDescription="true" 
                        Nullable="false"
                        Width="250px" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
            </tr>
            <tr id="vehicleTableRow" runat="server">
                <td>Vehicle:</td>
                <td>&nbsp;</td>
                <td colspan="3">
                    <uc1:PickerControl 
                        ID="vehiclePicker" 
                        runat="server" 
                        PopupType="PRODUCTVEHICLE" 
                        Param2="1" 
                        AppendDescription="true" 
                        Nullable="false"
                        Width="250px" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
            </tr>
            
            <tr id="vehicleDependentNoteTableRow" runat="server">
                <td colspan="2" valign="top">Vehicle Dependent Text:
                </td>
                <td colspan="3">
                    <asp:TextBox ID="vehicleDependentText" style="width:420px;height:100px" runat="server" TextMode="MultiLine" MaxLength="2048" CssClass="TextBox_Standard" Rows="10" onblur="if(this.value.length>2048){this.value=this.value.substring(0,2048);}"></asp:TextBox>
                </td>
            </tr>
            
            <tr id="bookedTableRow" runat="server">
                <td>Booked</td>
                <td>From:</td>
                <td><uc1:DateControl ID="bookedFromTextBox" runat="server" Width="100px" Nullable="false" /></td>
                <td style="width:40px">To:</td>
                <td><uc1:DateControl ID="bookedToTextBox" runat="server" Width="100px" Nullable="false" /></td>
            </tr>
            <tr id="locationFromTableRow" runat="server">
                <td>Location</td>
                <td>From:</td>
                <td colspan="3">
                    <uc1:PickerControl 
                        ID="locationFromPicker" 
                        runat="server" 
                        Width="300px" 
                        Caption="Location From"
                        PopupType="LOCATIONFORCOUNTRY" 
                        AppendDescription="true" 
                        Nullable="true"
                        Param1="NZ" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
            </tr>
            <tr id="locationToTableRow" runat="server">
                <td>&nbsp;</td>
                <td>To:</td>
                <td colspan="3">
                    <uc1:PickerControl 
                        ID="locationToPicker" 
                        runat="server" 
                        Width="300px" 
                        Caption="Location To"
                        PopupType="LOCATIONFORCOUNTRY" 
                        AppendDescription="true" 
                        Nullable="true"
                        Param1="NZ" 
                        MessagePanelControlID="rateMessagePanelControl" />
                </td>
            </tr>
            <tr id="personTypeTableRow" runat="server">
                <td>Person Type:</td>
                <td>&nbsp;</td>
                <td colspan="3"><asp:DropDownList ID="personTypeDropDown" runat="server" Width="100px" /></td>
            </tr>
        </table>
    </div>
    
    <table cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td><i><asp:Label ID="addModLabel" runat="server" /></i></td>
        </tr>
        <tr valign="bottom">
            <td align="right">
                <asp:Button ID="addButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="addNextButton" runat="Server" Text="OK / Next" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                <asp:Button ID="updateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="cancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
