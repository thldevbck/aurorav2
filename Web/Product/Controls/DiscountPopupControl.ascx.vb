Imports System.Collections.Generic

Imports System.Data

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<ToolboxData("<{0}:DiscountPopupControl runat=server></{0}:DiscountPopupControl>")> _
Partial Class Product_DiscountPopupControl
    Inherits ProductUserControl

    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)

    End Sub

    Private Sub InitLookups()
        Dim selectedValue As String

        selectedValue = uomDropDown.SelectedValue
        uomDropDown.Items.Clear()
        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Unit_of_Measure Then
                uomDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            End If
        Next
        Try
            uomDropDown.SelectedValue = selectedValue
        Catch
        End Try

        selectedValue = freeUOMDropDown.SelectedValue
        freeUOMDropDown.Items.Clear()
        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Unit_of_Measure Then
                freeUOMDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            End If
        Next
        Try
            freeUOMDropDown.SelectedValue = selectedValue
        Catch
        End Try
    End Sub

    Protected Sub Product_DiscountPopupControl_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        PopulateList()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.SaleableProductRow Is Nothing Then Return

        Try
            InitLookups()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If IsPostBack Then Return

        ' cancelButton.Attributes.Add("onclick", "$find('discountPopupBehavior').hide(); return false;")
    End Sub

    Private Sub InitDiscountPopTypeVisibility()
        Me.quantityTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"FOC", "LHD", "DBT", "PDR"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.fromToTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"FRR"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.dayFromToTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"DDT"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.uomTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"FRR"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.amountTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"LHD", "DBT", "PDR", "DDT", "FRR"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.freeTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"FOC"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.freeUomTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"FOC"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
        Me.freeProductTableRow.Style.Add("display", IIf(Array.IndexOf(New String() {"FOC"}, Me.typeDropDown.SelectedValue) <> -1, "", "none"))
    End Sub

    Private Function GetProductRateTravelBandFromPopup() As ProductRateTravelBandRow
        Return Me.ProductDataSet.ProductRateTravelBand.FindByPtbId(Me.parentPtbIdHidden.Value)
    End Function

    Private Function GetProductRateDiscountFromPopup() As ProductRateDiscountRow
        Return Me.ProductDataSet.ProductRateDiscount.FindByPdsId(Me.idHidden.Value)
    End Function

    Private Function CreateProductRateDiscountFromPopup() As ProductRateDiscountRow
        Dim productRateDiscountRow As ProductRateDiscountRow = Me.ProductDataSet.ProductRateDiscount.NewProductRateDiscountRow()
        productRateDiscountRow.PdsId = DataRepository.GetNewId()
        productRateDiscountRow.PdsParentId = parentIdHidden.Value
        productRateDiscountRow.PdsPtbId = parentPtbIdHidden.Value
        Me.ProductDataSet.ProductRateDiscount.AddProductRateDiscountRow(productRateDiscountRow)
        Return productRateDiscountRow
    End Function

    Private Function ValidateRateDiscountFromPopup() As Boolean
        Dim type As String = Me.typeDropDown.SelectedValue

        Dim message As String = Nothing


        Select Case typeDropDown.SelectedValue.ToUpper()
            Case "FOC"
                If Not quantityTextBox.IsTextValid() Then
                    message = quantityTextBox.ErrorMessage
                End If
                If Not freeQuantityTextBox.IsTextValid() Then
                    message = freeQuantityTextBox.ErrorMessage
                End If
                If Not freeProductPicker.IsValid() Then
                    message = freeProductPicker.ErrorMessage
                End If
            Case "LHD"
                If Not quantityTextBox.IsTextValid() Then
                    message = quantityTextBox.ErrorMessage
                End If

                If (String.IsNullOrEmpty(amountTextBox.Text) Or Not IsNumeric(amountTextBox.Text)) Then
                    ' no amount!
                    If (String.IsNullOrEmpty(percentTextBox.Text) Or Not IsNumeric(percentTextBox.Text)) Then
                        message = "Discount Amount or Percent must be entered"
                    End If
                End If

            Case "DBT"
                If Not quantityTextBox.IsTextValid() Then
                    message = quantityTextBox.ErrorMessage
                End If
                If (String.IsNullOrEmpty(amountTextBox.Text) Or Not IsNumeric(amountTextBox.Text)) Then
                    ' no amount!
                    If (String.IsNullOrEmpty(percentTextBox.Text) Or Not IsNumeric(percentTextBox.Text)) Then
                        message = "Discount Amount or Percent must be entered"
                    End If
                End If
            Case "PDR"
                If Not quantityTextBox.IsTextValid() Then
                    message = quantityTextBox.ErrorMessage
                End If
                If (String.IsNullOrEmpty(amountTextBox.Text) Or Not IsNumeric(amountTextBox.Text)) Then
                    ' no amount!
                    If (String.IsNullOrEmpty(percentTextBox.Text) Or Not IsNumeric(percentTextBox.Text)) Then
                        message = "Discount Amount or Percent must be entered"
                    End If
                End If
            Case "DDT"
                If (String.IsNullOrEmpty(amountTextBox.Text) Or Not IsNumeric(amountTextBox.Text)) Then
                    ' no amount!
                    If (String.IsNullOrEmpty(percentTextBox.Text) Or Not IsNumeric(percentTextBox.Text)) Then
                        message = "Discount Amount or Percent must be entered"
                    End If
                End If
            Case "FRR"
                If Not fromTextBox.IsTextValid() Then
                    message = fromTextBox.ErrorMessage
                End If
                If Not toTextBox.IsTextValid() Then
                    message = toTextBox.ErrorMessage
                End If
                If (String.IsNullOrEmpty(amountTextBox.Text) Or Not IsNumeric(amountTextBox.Text)) Then
                    ' no amount!
                    If (String.IsNullOrEmpty(percentTextBox.Text) Or Not IsNumeric(percentTextBox.Text)) Then
                        message = "Discount Amount or Percent must be entered"
                    End If
                End If
        End Select


        'If Array.IndexOf(New String() {"FOC", "LHD", "DBT", "PDR"}, type) <> -1 AndAlso Not quantityTextBox.IsTextValid() Then
        '    message = quantityTextBox.ErrorMessage
        'ElseIf Array.IndexOf(New String() {"FRR"}, type) <> -1 AndAlso Not fromTextBox.IsTextValid() Then
        'message = fromTextBox.ErrorMessage
        'ElseIf Array.IndexOf(New String() {"FRR"}, type) <> -1 AndAlso Not toTextBox.IsTextValid() Then
        'message = toTextBox.ErrorMessage
        'ElseIf Array.IndexOf(New String() {"LHD", "DBT", "PDR", "DDT", "FRR"}, type) <> -1 AndAlso (String.IsNullOrEmpty(amountTextBox.Text) Or Not IsNumeric(amountTextBox.Text)) Then 'Not amountTextBox.IsTextValid() Then
        'message = "Discount Amount must be a decimal value" 'toTextBox.ErrorMessage
        'ElseIf Array.IndexOf(New String() {"LHD", "DBT", "PDR", "DDT", "FRR"}, type) <> -1 AndAlso (String.IsNullOrEmpty(percentTextBox.Text) Or Not IsNumeric(percentTextBox.Text)) Then
        'message = "Discount Percent must be a decimal value" 'percentTextBox.ErrorMessage
        'ElseIf Array.IndexOf(New String() {"FOC"}, type) <> -1 AndAlso Not freeQuantityTextBox.IsTextValid() Then
        'message = freeQuantityTextBox.ErrorMessage
        'ElseIf Array.IndexOf(New String() {"FOC"}, type) <> -1 AndAlso Not freeProductPicker.IsValid() Then
        'message = freeProductPicker.ErrorMessage
        'End If

        If message IsNot Nothing Then
            discountMessagePanelControl.SetMessagePanelWarning(message)
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub UpdateProductRateDiscountFromPopup(ByVal productRateDiscountRow As ProductRateDiscountRow)

        productRateDiscountRow.DiscountType = typeDropDown.SelectedValue

        ' give mandatory fields default values
        productRateDiscountRow.Qty = 1

        If Array.IndexOf(New String() {"FOC", "LHD", "DBT", "PDR"}, productRateDiscountRow.DiscountType) <> -1 Then
            If Not String.IsNullOrEmpty(quantityTextBox.Text.Trim()) Then
                productRateDiscountRow.Qty = Decimal.Parse(quantityTextBox.Text)
            Else
                'productRateDiscountRow.Qty = 0
                productRateDiscountRow.Qty = 9999999999
            End If
        End If

        If Array.IndexOf(New String() {"FRR"}, productRateDiscountRow.DiscountType) <> -1 Then
            If Not String.IsNullOrEmpty(fromTextBox.Text.Trim()) Then
                productRateDiscountRow.FromHire = Decimal.Parse(fromTextBox.Text)
            Else
                'productRateDiscountRow.FromHire = 9999999999 '0
                productRateDiscountRow.SetPdsFromHireNull()
            End If
            If Not String.IsNullOrEmpty(toTextBox.Text.Trim()) Then
                productRateDiscountRow.ToHire = Decimal.Parse(toTextBox.Text)
            Else
                'productRateDiscountRow.ToHire = 9999999999 '0
                productRateDiscountRow.SetPdsToHireNull()
            End If

            productRateDiscountRow.UomId = uomDropDown.SelectedValue
        End If

        If Array.IndexOf(New String() {"DDT"}, productRateDiscountRow.DiscountType) <> -1 Then
            productRateDiscountRow.FromDayOfWeek = dayFromDropDown.SelectedValue
            productRateDiscountRow.ToDayOfWeek = dayToDropDown.SelectedValue
        End If

        If Array.IndexOf(New String() {"LHD", "DBT", "PDR", "DDT", "FRR"}, productRateDiscountRow.DiscountType) <> -1 Then
            If Not String.IsNullOrEmpty(amountTextBox.Text.Trim()) Then
                productRateDiscountRow.Amt = Decimal.Parse(amountTextBox.Text)
            Else
                'productRateDiscountRow.Amt = 0
                productRateDiscountRow.SetPdsAmtNull()
            End If

            If Not String.IsNullOrEmpty(percentTextBox.Text.Trim()) Then
                productRateDiscountRow.PdsPercentage = Decimal.Parse(percentTextBox.Text)
            Else
                'productRateDiscountRow.PdsPercentage = 0
                productRateDiscountRow.SetPdsPercentageNull()
            End If

            'If productRateDiscountRow.Amt <> 0 OrElse productRateDiscountRow.Percentage = 0 Then
            '    productRateDiscountRow.SetPdsPercentageNull()
            'Else
            '    productRateDiscountRow.SetPdsAmtNull()
            'End If
        End If

        If Array.IndexOf(New String() {"FOC"}, productRateDiscountRow.DiscountType) <> -1 Then
            If Not String.IsNullOrEmpty(freeQuantityTextBox.Text.Trim()) Then
                productRateDiscountRow.FreeQty = Decimal.Parse(freeQuantityTextBox.Text)
            Else
                'productRateDiscountRow.FreeQty = 9999999999 '0
                productRateDiscountRow.SetPdsFreeQtyNull()
            End If

            productRateDiscountRow.FreeUomId = freeUOMDropDown.SelectedValue
            productRateDiscountRow.UomId = freeUOMDropDown.SelectedValue

            If Not String.IsNullOrEmpty(freeProductPicker.DataId) Then
                productRateDiscountRow.PdsPrdFreeId = freeProductPicker.DataId
            Else
                productRateDiscountRow.SetPdsPrdFreeIdNull()
            End If
        End If
    End Sub

    Public Sub ShowNew(ByVal productRateTravelBandRow As ProductRateTravelBandRow, ByVal productRate As IProductRate)
        idHidden.Value = ""
        parentIdHidden.Value = productRate.Id
        parentPtbIdHidden.Value = productRateTravelBandRow.PtbId

        ' auto percent changes - $ value changes
        ' the base rate, needed for JS changes
        baseRateHidden.Value = productRate.Rate
        amountTextBox.Attributes.Add("onChange", "CheckContents(this,'');if(this.value!='')UpdatePercentField('" & percentTextBox.ClientID & "')")
        percentTextBox.Attributes.Add("onChange", "CheckContents(this,'');if(this.value!='')UpdateAmountField('" & baseRateHidden.ClientID & "','" & amountTextBox.ClientID & "','" & percentTextBox.ClientID & "')")
        ' auto percent changes - $ value changes

        typeDropDown.Enabled = True
        typeDropDown.SelectedIndex = 0

        quantityTextBox.Text = "1"
        fromTextBox.Text = "1"
        toTextBox.Text = "1"
        Try
            dayFromDropDown.SelectedIndex = 0
        Catch
        End Try
        Try
            dayToDropDown.SelectedIndex = 0
        Catch
        End Try
        Try
            uomDropDown.SelectedValue = productRate.UomId
        Catch
        End Try
        percentTextBox.Text = ""
        amountTextBox.Text = ""
        freeQuantityTextBox.Text = "1"
        Try
            freeUOMDropDown.SelectedIndex = 0
        Catch
        End Try
        freeProductPicker.Text = ""
        freeProductPicker.DataId = ""

        addModLabel.Text = ""

        addButton.Visible = True
        addNextButton.Visible = True
        deleteButton.Visible = False
        updateButton.Visible = False

        If typeDropDown.SelectedItem.Text.ToUpper().Contains("DISCOUNT") Then
            discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text
        Else
            discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Discount"
        End If
        'discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Discount"
        InitDiscountPopTypeVisibility()
        discountPopup.Show()
    End Sub

    Public Sub ShowExisting(ByVal productRateTravelBandRow As ProductRateTravelBandRow, _
                            ByVal productRateDiscountRow As ProductRateDiscountRow, _
                            ByVal productRate As IProductRate)
        idHidden.Value = productRateDiscountRow.PdsId
        parentIdHidden.Value = productRateDiscountRow.PdsParentId
        parentPtbIdHidden.Value = productRateTravelBandRow.PtbId

        ' auto percent changes - $ value changes
        ' the base rate, needed for JS changes
        baseRateHidden.Value = productRate.Rate  'productRateTravelBandRow.PtbBaseRate
        amountTextBox.Attributes.Add("onChange", "CheckContents(this,'');if(this.value!='')UpdatePercentField('" & percentTextBox.ClientID & "')")
        percentTextBox.Attributes.Add("onChange", "CheckContents(this,'');if(this.value!='')UpdateAmountField('" & baseRateHidden.ClientID & "','" & amountTextBox.ClientID & "','" & percentTextBox.ClientID & "')")
        ' auto percent changes - $ value changes

        typeDropDown.SelectedValue = productRateDiscountRow.DiscountType()
        typeDropDown.Enabled = False

        If Array.IndexOf(New String() {"FOC", "LHD", "DBT", "PDR"}, productRateDiscountRow.DiscountType) <> -1 Then
            'quantityTextBox.Text = productRateDiscountRow.Qty.ToString("#.####")
            quantityTextBox.Text = productRateDiscountRow.Qty.ToString("f2")
        End If

        If Array.IndexOf(New String() {"FRR"}, productRateDiscountRow.DiscountType) <> -1 Then
            If productRateDiscountRow.IsPdsFromHireNull OrElse productRateDiscountRow.FromHire = -999999999 Then
                fromTextBox.Text = String.Empty
            Else
                fromTextBox.Text = productRateDiscountRow.FromHire.ToString("f2")
            End If

            If productRateDiscountRow.IsPdsToHireNull OrElse productRateDiscountRow.ToHire = -999999999 Then
                toTextBox.Text = String.Empty
            Else
                toTextBox.Text = productRateDiscountRow.ToHire.ToString("f2")
            End If


            Try
                uomDropDown.SelectedValue = productRateDiscountRow.UomCode.CodId
            Catch
            End Try
        End If

        If Array.IndexOf(New String() {"DDT"}, productRateDiscountRow.DiscountType) <> -1 Then
            Try
                dayFromDropDown.SelectedValue = productRateDiscountRow.FromDayOfWeek.ToString()
            Catch
            End Try
            Try
                dayToDropDown.SelectedValue = productRateDiscountRow.ToDayOfWeek.ToString()
            Catch
            End Try
        End If

        If Array.IndexOf(New String() {"LHD", "DBT", "PDR", "DDT", "FRR"}, productRateDiscountRow.DiscountType) <> -1 Then

            If productRateDiscountRow.IsPdsAmtNull OrElse productRateDiscountRow.Amt = -999999999 Then
                amountTextBox.Text = String.Empty
            Else
                amountTextBox.Text = productRateDiscountRow.Amt.ToString("f2")
            End If

            If productRateDiscountRow.IsPdsPercentageNull OrElse productRateDiscountRow.PdsPercentage = -999999999 Then
                percentTextBox.Text = String.Empty
            Else
                percentTextBox.Text = productRateDiscountRow.PdsPercentage.ToString("f2")
            End If
        End If

        If Array.IndexOf(New String() {"FOC"}, productRateDiscountRow.DiscountType) <> -1 Then

            If productRateDiscountRow.IsPdsFreeQtyNull OrElse productRateDiscountRow.FreeQty = -999999999 Then
                freeQuantityTextBox.Text = String.Empty
            Else
                freeQuantityTextBox.Text = productRateDiscountRow.FreeQty.ToString("f2")
            End If

            Try
                freeUOMDropDown.SelectedValue = productRateDiscountRow.FreeUomId
            Catch
            End Try

            If productRateDiscountRow.DiscountProductRowByProductRateDiscount_Free IsNot Nothing Then
                freeProductPicker.Text = productRateDiscountRow.DiscountProductRowByProductRateDiscount_Free.Description
                freeProductPicker.DataId = productRateDiscountRow.DiscountProductRowByProductRateDiscount_Free.PrdId
            Else
                freeProductPicker.Text = ""
                freeProductPicker.DataId = ""
            End If
        End If

        addModLabel.Text = Server.HtmlEncode(Aurora.Common.Data.GetAddModDescription(productRateDiscountRow))

        addButton.Visible = False
        addNextButton.Visible = False
        deleteButton.Visible = True
        updateButton.Visible = True
        'discountPopupTitleLabel.Text = "Edit " & typeDropDown.SelectedItem.Text & " Discount"
        If typeDropDown.SelectedItem.Text.ToUpper().Contains("DISCOUNT") Then
            discountPopupTitleLabel.Text = "Edit " & typeDropDown.SelectedItem.Text
        Else
            discountPopupTitleLabel.Text = "Edit " & typeDropDown.SelectedItem.Text & " Discount"
        End If
        InitDiscountPopTypeVisibility()
        discountPopup.Show()
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        If Not ValidateRateDiscountFromPopup() Then
            discountPopup.Show()
            Return
        End If

        Dim productRateDiscountRow As ProductRateDiscountRow = CreateProductRateDiscountFromPopup()
        If productRateDiscountRow Is Nothing Then Return

        UpdateProductRateDiscountFromPopup(productRateDiscountRow)

        ' add the discount
        ''Aurora.Common.Data.ExecuteDataRow(productRateDiscountRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRateDiscountRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)


        ' update "moddatetime" the base product rate band
        productRateTravelBandRow.ModDateTime = Date.Now
        ''Aurora.Common.Data.ExecuteDataRow(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=productRateTravelBandRow.PtbTbsId, _
            ptbId:=productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub addNextButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addNextButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        If Not ValidateRateDiscountFromPopup() Then
            discountPopup.Show()
            Return
        End If

        Dim productRateDiscountRow As ProductRateDiscountRow = CreateProductRateDiscountFromPopup()
        If productRateDiscountRow Is Nothing Then Return

        UpdateProductRateDiscountFromPopup(productRateDiscountRow)

        ' add the discount
        ''Aurora.Common.Data.ExecuteDataRow(productRateDiscountRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRateDiscountRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        ' update "moddatetime" the base product rate band
        productRateTravelBandRow.ModDateTime = Date.Now
        ''Aurora.Common.Data.ExecuteDataRow(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        'ShowNew(productRateTravelBandRow,
        idHidden.Value = ""

        typeDropDown.Enabled = True
        typeDropDown.SelectedIndex = 0

        quantityTextBox.Text = "1"
        fromTextBox.Text = "1"
        toTextBox.Text = "1"
        Try
            dayFromDropDown.SelectedIndex = 0
        Catch
        End Try
        Try
            dayToDropDown.SelectedIndex = 0
        Catch
        End Try
        percentTextBox.Text = ""
        amountTextBox.Text = ""
        freeQuantityTextBox.Text = "1"
        Try
            freeUOMDropDown.SelectedIndex = 0
        Catch
        End Try
        freeProductPicker.Text = ""
        freeProductPicker.DataId = ""

        addModLabel.Text = ""

        addButton.Visible = True
        addNextButton.Visible = True
        deleteButton.Visible = False
        updateButton.Visible = False
        'discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Discount"
        If typeDropDown.SelectedItem.Text.ToUpper().Contains("DISCOUNT") Then
            discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text
        Else
            discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Discount"
        End If
        InitDiscountPopTypeVisibility()
        discountPopup.Show()

    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles updateButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        Dim productRateDiscountRow As ProductRateDiscountRow = GetProductRateDiscountFromPopup()
        If productRateDiscountRow Is Nothing Then Return

        If Not ValidateRateDiscountFromPopup() Then
            discountPopup.Show()
            Return
        End If

        UpdateProductRateDiscountFromPopup(productRateDiscountRow)

        ' update the discount
        'Aurora.Common.Data.ExecuteDataRow(productRateDiscountRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRateDiscountRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        ' update "moddatetime" the base product rate band
        productRateTravelBandRow.ModDateTime = Date.Now
        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=productRateTravelBandRow.PtbTbsId, _
            ptbId:=productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles deleteButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        Dim productRateDiscountRow As ProductRateDiscountRow = GetProductRateDiscountFromPopup()
        If productRateDiscountRow Is Nothing Then Return

        ' delete the discount
        productRateDiscountRow.Delete()
        Aurora.Common.Data.ExecuteDataRow(productRateDiscountRow)

        ' update "moddatetime" the base product rate band
        productRateTravelBandRow.ModDateTime = Date.Now
        'Aurora.Common.Data.ExecuteDataRow(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        Aurora.Common.Data.ExecuteDataRowUpdateVehicleDependentRate(productRateTravelBandRow, Me.CurrentPage.UserCode)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=productRateTravelBandRow.PtbTbsId, _
            ptbId:=productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        discountPopup.Hide()
    End Sub

    Protected Sub typeDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles typeDropDown.SelectedIndexChanged
        If typeDropDown.SelectedItem.Text.ToUpper().Contains("DISCOUNT") Then
            discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text
        Else
            discountPopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Discount"
        End If

        InitDiscountPopTypeVisibility()
        discountPopup.Show()
    End Sub

#Region "Rev:Mia June 30 2011 "
    Sub PopulateList()
        Dim ds As New DataSet("FreeChargeDiscount")
        Aurora.Common.Data.ExecuteDataSetSP("ProductSaleable_GetFreeChargeDiscount", ds)

        typeDropDown.DataSource = ds.Tables(0)
        typeDropDown.DataTextField = "CodDesc"
        typeDropDown.DataValueField = "CodCode"
        typeDropDown.DataBind()
    End Sub

#End Region
End Class
