Imports System.Collections.Generic

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet


<ToolboxData("<{0}:LocationsControl runat=server></{0}:LocationsControl>")> _
Partial Class Product_LocationsControl
    Inherits ProductUserControl

    Private _loadException As Exception
    Private _checkBoxesByLcaId As New Dictionary(Of String, CheckBox)
    Private _locationsByLinkButtonId As New Dictionary(Of String, LocationAvailabilityRow)

    Public Sub BuildLocations()
        _checkBoxesByLcaId.Clear()
        _locationsByLinkButtonId.Clear()
        While locationsTable.Rows.Count > 1
            locationsTable.Rows.RemoveAt(1)
        End While

        If Me.SaleableProductRow Is Nothing Then Return

        locationPopupTravelFromPicker.Param1 = Me.SaleableProductRow.CountryRow.Code
        locationPopupTravelToPicker.Param1 = Me.SaleableProductRow.CountryRow.Code

        Dim rowIndex As Integer = 0
        For Each locationAvailabilityRow As LocationAvailabilityRow In Me.SaleableProductRow.GetLocationAvailabilityRows()
            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "evenRow", "oddRow") : rowIndex += 1
            locationsTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & locationAvailabilityRow.LcaId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByLcaId.Add(locationAvailabilityRow.LcaId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim typeCell As New TableCell
            tableRow.Controls.Add(typeCell)

            If CanMaintain Then
                Dim linkButton As New LinkButton
                linkButton.ID = "editLocation" & locationAvailabilityRow.LcaId & "LinkButton"
                linkButton.Text = "Location"
                typeCell.Controls.Add(linkButton)
                _locationsByLinkButtonId.Add(linkButton.ID, locationAvailabilityRow)
                AddHandler linkButton.Click, AddressOf editButton_Click
            Else
                typeCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim travelFromTableCell As New TableCell
            If locationAvailabilityRow.LocationRowByLocation_LocationAvailability_From IsNot Nothing Then
                travelFromTableCell.Text = locationAvailabilityRow.LocationRowByLocation_LocationAvailability_From.Description
            End If
            tableRow.Controls.Add(travelFromTableCell)

            Dim travelToTableCell As New TableCell
            If locationAvailabilityRow.LocationRowByLocation_LocationAvailability_To IsNot Nothing Then
                travelToTableCell.Text = locationAvailabilityRow.LocationRowByLocation_LocationAvailability_To.Description
            End If
            tableRow.Controls.Add(travelToTableCell)

            Dim excludeTableCell As New TableCell
            excludeTableCell.Text = IIf(locationAvailabilityRow.IsNotAvailable, "Yes", "&nbsp;")
            tableRow.Controls.Add(excludeTableCell)

            Dim linkCell As New TableCell
            tableRow.Controls.Add(linkCell)
        Next

        addButton.Visible = CanMaintain
        deleteButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        locationPopupCancelButton.OnClientClick = "$find('locationPopupBehavior').hide(); return false;"

        If Me.SaleableProductRow Is Nothing Then Return

        Try
            BuildLocations()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain Then Return

        locationPopupTravelFromPicker.DataId = ""
        locationPopupTravelFromPicker.Text = ""

        locationPopupTravelToPicker.DataId = ""
        locationPopupTravelToPicker.Text = ""

        locationPopupExcludeCheckBox.Checked = False

        locationPopupAddButton.Visible = True
        locationPopupAddNextButton.Visible = True
        locationPopupDeleteButton.Visible = False
        locationPopupUpdateButton.Visible = False
        locationPopupTitleLabel.Text = "Add Location"
        locationPopup.Show()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If Not CanMaintain Then Return

        For Each locationAvailabilityRow As LocationAvailabilityRow In Me.SaleableProductRow.GetLocationAvailabilityRows()
            Dim checkBox As CheckBox = _checkBoxesByLcaId(locationAvailabilityRow.LcaId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            locationAvailabilityRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(locationAvailabilityRow)
            Me.ProductDataSet.LocationAvailability.RemoveLocationAvailabilityRow(locationAvailabilityRow)
        Next

        BuildLocations()
    End Sub

    Protected Sub editButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim locationAvailabilityRow As LocationAvailabilityRow = _locationsByLinkButtonId(CType(sender, LinkButton).ID)
        If locationAvailabilityRow Is Nothing Then Return

        locationPopupIdHidden.Value = locationAvailabilityRow.LcaId

        locationPopupTravelFromPicker.DataId = ""
        locationPopupTravelFromPicker.Text = ""
        If locationAvailabilityRow.LocationRowByLocation_LocationAvailability_From IsNot Nothing Then
            locationPopupTravelFromPicker.DataId = locationAvailabilityRow.LocationRowByLocation_LocationAvailability_From.Code
            locationPopupTravelFromPicker.Text = locationAvailabilityRow.LocationRowByLocation_LocationAvailability_From.Description
        End If

        locationPopupTravelToPicker.DataId = ""
        locationPopupTravelToPicker.Text = ""
        If locationAvailabilityRow.LocationRowByLocation_LocationAvailability_To IsNot Nothing Then
            locationPopupTravelToPicker.DataId = locationAvailabilityRow.LocationRowByLocation_LocationAvailability_To.Code
            locationPopupTravelToPicker.Text = locationAvailabilityRow.LocationRowByLocation_LocationAvailability_To.Description
        End If

        locationPopupExcludeCheckBox.Checked = locationAvailabilityRow.IsNotAvailable

        locationPopupAddButton.Visible = False
        locationPopupAddNextButton.Visible = False
        locationPopupDeleteButton.Visible = True
        locationPopupUpdateButton.Visible = True
        locationPopupTitleLabel.Text = "Update Location"
        locationPopup.Show()
    End Sub

    Private Sub ValidateLocation()
        If Not locationPopupTravelFromPicker.IsValid Then
            Throw New Aurora.Common.ValidationException(locationPopupTravelFromPicker.ErrorMessage)
        End If
        If Not locationPopupTravelToPicker.IsValid Then
            Throw New Aurora.Common.ValidationException(locationPopupTravelFromPicker.ErrorMessage)
        End If
        If String.IsNullOrEmpty(locationPopupTravelFromPicker.DataId) _
         AndAlso String.IsNullOrEmpty(locationPopupTravelToPicker.DataId) Then
            Throw New Aurora.Common.ValidationException("Either or both a from and to location must be specified")
        End If
    End Sub

    Private Function UpdateLocationRowFromUI(ByVal locationAvailabilityRow As LocationAvailabilityRow) As Boolean
        Try
            ValidateLocation()
        Catch ex As Aurora.Common.ValidationException
            messagePanelControl.SetMessagePanelWarning(ex.Message)
            locationPopup.Show()
            Return False
        End Try

        If Not String.IsNullOrEmpty(locationPopupTravelFromPicker.DataId) Then
            locationAvailabilityRow.LcaFromLocCode = locationPopupTravelFromPicker.DataId
        Else
            locationAvailabilityRow.SetLcaFromLocCodeNull()
        End If
        If Not String.IsNullOrEmpty(locationPopupTravelToPicker.DataId) Then
            locationAvailabilityRow.LcaToLocCode = locationPopupTravelToPicker.DataId
        Else
            locationAvailabilityRow.SetLcaToLocCodeNull()
        End If
        locationAvailabilityRow.LcaIsNotAvailable = locationPopupExcludeCheckBox.Checked

        Return True
    End Function

    Protected Sub locationPopupAddButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles locationPopupAddButton.Click, locationPopupAddNextButton.Click
        Dim locationAvailabilityRow As LocationAvailabilityRow = Me.ProductDataSet.LocationAvailability.NewLocationAvailabilityRow()
        If Not UpdateLocationRowFromUI(locationAvailabilityRow) Then Return

        locationAvailabilityRow.LcaId = DataRepository.GetNewId()
        locationAvailabilityRow.LcaSapId = Me.SaleableProductRow.SapId
        Me.ProductDataSet.LocationAvailability.AddLocationAvailabilityRow(locationAvailabilityRow)
        Aurora.Common.Data.ExecuteDataRow(locationAvailabilityRow, UsrId:=Me.CurrentPage.UserId, PrgmName:=Me.CurrentPage.PrgmName)

        BuildLocations()

        If sender Is locationPopupAddNextButton Then
            addButton_Click(sender, e)
        End If
    End Sub

    Protected Sub locationPopupUpdateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles locationPopupUpdateButton.Click
        Dim locationAvailabilityRow As LocationAvailabilityRow = Me.ProductDataSet.LocationAvailability.FindByLcaId(locationPopupIdHidden.Value)
        If locationAvailabilityRow Is Nothing Then Return
        If Not UpdateLocationRowFromUI(locationAvailabilityRow) Then Return

        Aurora.Common.Data.ExecuteDataRow(locationAvailabilityRow)

        BuildLocations()
    End Sub

    Protected Sub locationPopupDeleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationPopupDeleteButton.Click
        Dim locationAvailabilityRow As LocationAvailabilityRow = Me.ProductDataSet.LocationAvailability.FindByLcaId(locationPopupIdHidden.Value)
        If locationAvailabilityRow Is Nothing Then Return

        locationAvailabilityRow.Delete()
        Aurora.Common.Data.ExecuteDataRow(locationAvailabilityRow)
        Me.ProductDataSet.LocationAvailability.RemoveLocationAvailabilityRow(locationAvailabilityRow)

        BuildLocations()
    End Sub

    Protected Sub locationPopupCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles locationPopupCancelButton.Click
        locationPopup.Hide()
    End Sub

End Class
