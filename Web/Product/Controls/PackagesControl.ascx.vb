Imports System.Collections.Generic

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet


<ToolboxData("<{0}:PackagesControl runat=server></{0}:PackagesControl>")> _
Partial Class Product_PackagesControl
    Inherits ProductUserControl

    Private _loadException As Exception
    Private _checkBoxesByPkgId As New Dictionary(Of String, CheckBox)

    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)
    End Sub

    Public Sub InitPackages()
        If Me.SaleableProductRow Is Nothing Then Return

        Dim currentDate As Date = Date.Now

        Dim rowIndex As Integer = 0
        For Each packageProductRow As PackageProductRow In Me.SaleableProductRow.GetPackageProductRows()
            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "row", "rowalt") : rowIndex += 1
            tableRow.BackColor = packageProductRow.PackageRow.StatusColor(currentDate)
            packageTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "remove" & packageProductRow.PplPkgId & "_" & packageProductRow.PplSapId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByPkgId.Add(packageProductRow.PplPkgId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim nameCell As New TableCell
            tableRow.Controls.Add(nameCell)

            If Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.PackageEnquiry) Then
                Dim hyperLink As New HyperLink
                hyperLink.Text = Server.HtmlEncode(packageProductRow.PackageDescription)
                hyperLink.NavigateUrl = PackageUserControl.MakePackageUrl(Me.Page, packageProductRow.PplPkgId)
                nameCell.Controls.Add(hyperLink)
            Else
                nameCell.Text = Server.HtmlEncode(packageProductRow.PackageDescription)
            End If

            Dim typeCell As New TableCell
            If Not packageProductRow.IsPplCopyCreateLinkNull Then typeCell.Text = Server.HtmlEncode(packageProductRow.PplCopyCreateLink)
            tableRow.Controls.Add(typeCell)

            Dim brandCell As New TableCell
            brandCell.Text = Server.HtmlEncode(packageProductRow.PackageRow.BrandRow.Description)
            tableRow.Controls.Add(brandCell)

            Dim bookedCell As New TableCell
            bookedCell.Text = Server.HtmlEncode(packageProductRow.PackageRow.BookedDescription)
            tableRow.Controls.Add(bookedCell)

            Dim travelCell As New TableCell
            travelCell.Text = Server.HtmlEncode(packageProductRow.PackageRow.TravelDescription)
            tableRow.Controls.Add(travelCell)

            Dim statusCell As New TableCell
            statusCell.Text = Server.HtmlEncode(packageProductRow.PackageRow.StatusText(currentDate))
            tableRow.Controls.Add(statusCell)
        Next

        addPackageProductButton.Visible = CanMaintain
        removePackageProductButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.SaleableProductRow Is Nothing Then Return

        Try
            InitPackages()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Me.IsPostBack Then Return

        packagePopupCancelButton.Attributes.Add("onclick", "$find('packagePopupBehavior').hide(); return false;")
    End Sub

    Protected Sub removePackageProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removePackageProductButton.Click
        If Not CanMaintain Then Return

        For Each packageProductRow As PackageProductRow In Me.SaleableProductRow.GetPackageProductRows()
            Dim checkBox As CheckBox = _checkBoxesByPkgId(packageProductRow.PplPkgId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            DataRepository.DeletePackageProduct(packageProductRow.PplPkgId, packageProductRow.PplSapId)
        Next

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Packages"))
    End Sub

    Protected Sub addPackageProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addPackageProductButton.Click
        If Not CanMaintain Then Return

        packagePopupPicker.Param2 = Me.SaleableProductRow.SapId
        packagePopupPicker.DataId = ""
        packagePopupPicker.Text = ""

        packagePopup.Show()
    End Sub

    Protected Sub packagePopupAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles packagePopupAddButton.Click
        If Not String.IsNullOrEmpty(packagePopupPicker.DataId) Then
            DataRepository.CreatePackageProduct(packagePopupPicker.DataId, Me.SaleableProductRow.SapId, saleableProductTypeRadioButtonList.SelectedValue, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        End If

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Packages"))
    End Sub

    Protected Sub packagePopupCancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles packagePopupCancelButton.Click
        packagePopup.Hide()
    End Sub

End Class
