<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AttributesControl.ascx.vb" Inherits="Product_AttributesControl" %>

<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<table style="width:100%" cellpadding="0" cellspacing="0">
    <tr>
        <td style="vertical-align:top; padding-right: 5px">
            <asp:Table ID="promptAttributesTable" runat="server" CellPadding="1" CellSpacing="0" CssClass="dataTable attributesTable" style="width:100%">
                <asp:TableHeaderRow CssClass="trHeader">
                    <asp:TableHeaderCell ColumnSpan="2">Prompt Attributes</asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </td>
        <td style="vertical-align:top">
            <asp:Table ID="valueAttributesTable" runat="server" CellPadding="1" CellSpacing="0" CssClass="dataTable attributesTable" style="width:100%">
                <asp:TableHeaderRow CssClass="trHeader">
                    <asp:TableHeaderCell ColumnSpan="2">Value Attributes</asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <asp:Button ID="attributesUpdateButton" runat="server" Text="Save Attributes" CssClass="Button_Standard Button_Save" />
        </td>
    </tr>
</table>    


