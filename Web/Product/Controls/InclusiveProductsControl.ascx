﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InclusiveProductsControl.ascx.vb" Inherits="Product_InclusiveProductsControl"%>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>

<script type="text/javascript" language="javascript">
    

    function cancelPostbackOnInvalidDate() {
        var value = document.getElementById('<%=newDateControlFrom.ClientID%>').value;
        if (value == "") {
            return false;
        }
        if (isNaN(Date.parse(value.toString()))) {
            return false;
        }

        var splitDate = value.split("/");
        var day = splitDate[0];
        var month = splitDate[1];
        var year = splitDate[2];

        if (isNaN(day) == true) {
            return false;
        }
        if (isNaN(month) == true) {
            return false;
        }
        if (isNaN(year) == true) {
            return false;
        }

        return true;

//        if (parseInt(day) < 1 && parseInt(day) > 31) {
//            return false;
//        }

//        if (parseInt(month) < 1 && parseInt(month) > 12) {
//            return false;
//        }

//        if (parseInt(year) < 1900 && parseInt(year) > 2099) {
//            return false;
//        }

//        if (parseInt(month) == 2) {
//            if ((parseInt(year) % 400 == 0) || (parseInt(year) % 100 != 0 && parseInt(year) % 4 == 0)) {
//                if (parseInt(day) > 29) {
//                    return false;
//                }
//            }
//            else {
//                if (parseInt(day) > 28) {
//                    return false;
//                }
//            }
//        }


    }

    function cancelPostback() {
        var value=document.getElementById('<%=inclusiveProductPopupPicker.ClientID%>').value;
        if (value == "") 
        {
            //alert('No vehicle products in the list.');
            return false;
        }
        return true;
    }

    function cancelPostbackOnAdd() {
        //debugger;
        var value = document.getElementById('<%=ckoFromDropdown.ClientID%>').value;
        var valueTo = document.getElementById('<%=ckoToTextbox.ClientID%>').value;

        if (value == "" || valueTo == "") 
        {
            //alert('Please select from and to dates first.');
            return false;
        }


        return true;
    }


    function cancelPostbackOnSave() {
        //return true;
        //debugger;
        var value = document.getElementById('<%=ckoFromDropdown.ClientID%>').value;
        var valueTo = document.getElementById('<%=ckoToTextbox.ClientID%>').value;

        if (value == "" || valueTo == "") {
            //alert('Please select from and to dates first.');
            return false;
        }
       
       
        //--------------------------------------------------------------------------
        // Added May 2, 2011
        // Please comment if a package can be saved without any inclusive products
        // (for both existing and new packages where only dates are specified
        // but no inclusive products are added to the list
        // IMP: at least one inclusive product should be associated with the saleable product
        // without which the save option will not be allowed
        //--------------------------------------------------------------------------
        var table = document.getElementById('<%=inclusiveProductsTable.ClientID%>')
        var inputs = table.getElementsByTagName("input");
        var counter = 0;
        for (var i = 0; i < inputs.length; i += 1) {
            counter++;
        }

        if (counter==0 || counter == 1 ) {
            return false;
        }
        //--------------------------------------------------------------------------

        return true;
    }

    //Javascript Code Added

    function cancelPostbackOnRem() {
        //return true;
        var value = document.getElementById('<%=ckoFromDropdown.ClientID%>').value;
        var valueTo = document.getElementById('<%=ckoToTextbox.ClientID%>').value;
//        var inclVar = document.getElementById('<%=HiddenField_InclusiveProds.ClientID%>').value;

//        if (inclVar == "" || inclVar == 0) 
//        {
//            //alert('No inclusive products in the list.');
//            return false;
//        }

        if (value == "" || valueTo == "") 
        {
            //alert('Please select from and to dates first.');
            return false;
        }

        var table = document.getElementById('<%=inclusiveProductsTable.ClientID%>')
        var inputs = table.getElementsByTagName("input");    
        var isValid = false;     
        for (var i=0; i < inputs.length; i += 1) 
        {
            if (inputs[i].type == "checkbox") {

                if (inputs[i].checked == true) {
                    isValid = true;
                    break;
                }
            }
        }

        if (isValid == false) 
        {
            //alert('No product selected for removal.');
            return false;
        }
        return true;
    }
    
    function cancelPostbackOnRemoveVehicles() {
        var count = document.getElementById('<%=HiddenField_VehicleRows.ClientID%>');
        
        if (count.value == 0) 
        {
            //alert('No vehicle to remove.');
            return false;
        }
        
        var valid = false;
        var grid = document.getElementById('<%=GridView1.ClientID%>');

        if (grid == null) 
        {
            return false;
        }

        for (var i = 0; i < grid.all.length; i++) 
        {
            var node = grid.all[i];
            if (node != null && node.type == "checkbox" && node.checked) 
            {
                valid = true; 
                break;
            }
        }
        if (!valid) {
            //alert("No  vehicle selected for removal.");
            return false;
        } 
        return true;
    }
</script>

    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <table cellpadding="0" cellspacing="0" width="100%">               
                <tr>
                    <td align="left" style="background-color:#eeeeee;">
                        <asp:HiddenField ID="HiddenField_InclusiveProds" runat="server" />
                        <b>Pick-up From Date:</b>&nbsp;&nbsp;<asp:DropDownList ID="ckoFromDropdown" runat="server" Width="120" AutoPostBack="true">
                        </asp:DropDownList>       
                    </td>
                    <td align="left" style="background-color:#eeeeee;">
                        <b>Drop-off To Date:</b>&nbsp;&nbsp;<asp:TextBox id="ckoToTextbox" runat="server" Width="120" AutoPostBack="false">
                        </asp:TextBox>    
                    </td>
                    <td align="right" style="background-color:#eeeeee;">
                        <asp:Button ID="addPickupDateButton" runat="Server" Text="Add Pickup From Date" Width="265" CssClass="Button_Standard Button_Add" Visible="true" />
                    </td>
                    <td>
                        <ajaxToolkit:ModalPopupExtender 
                        runat="server"
                        ID="addPickupDateButtonExt" 
                        BehaviorID="addPickupDateButtonPopupBehavior"
                        TargetControlID="addPickupDateButton" 
                        PopupControlID="addPickupDatePopupPanel"
                        BackgroundCssClass="modalBackground" 
                        DropShadow="True" 
                        PopupDragHandleControlID="addPickupDatePopupDragPanel" />

                        <asp:Panel runat="server" CssClass="modalPopup" ID="addPickupDatePopupPanel" OnLoad="datePopup_onLoad" Style="display: none; padding: 10px;" Width="500px">
                            <asp:Panel runat="Server" ID="addPickupDatePopupDragPanel" CssClass="modalPopupTitle" >
                                <asp:Label ID="addPickupDatePopupTitleLabel" runat="Server" Text="Select From Date For the New Package" />
                            </asp:Panel>
                            <table cellpadding="1" cellspacing="0" width="100%">             
                                <tr>
                                    <td style="text-align:left">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left">
                                        New Pick-up from date:&nbsp;<uc1:DateControl ID="newDateControlFrom" runat="server" AutoPostBack="false" />
                                    </td>
                                </tr>          
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">
                                        <asp:Button ID="newFromDatePopupOKButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" OnClientClick="return cancelPostbackOnInvalidDate();"  />
                                        <asp:Button ID="newFromDatePopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        
            <br />                    

            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="removeInclusiveProductButton" runat="Server" Text="Remove" CssClass="Button_Standard Button_Remove" OnClientClick="return cancelPostbackOnRem();" Visible="true"   />
                        <asp:Button ID="addInclusiveProductButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="true" OnClientClick="return cancelPostbackOnAdd();" />
                    </td>
                </tr>
            </table>

            <asp:Table ID="inclusiveProductsTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableColor" Width="100%">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Style="width: 20px">
                        <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                    </asp:TableHeaderCell>
                    <asp:TableHeaderCell Style="width: 130px">Inclusive Product</asp:TableHeaderCell>
                    <asp:TableHeaderCell Style="width: 140px">Vehicle(s)</asp:TableHeaderCell>
                  <%--  <asp:TableHeaderCell Style="width: 70px">From</asp:TableHeaderCell>
                    <asp:TableHeaderCell Style="width: 70px">To</asp:TableHeaderCell>--%>
                    <asp:TableHeaderCell Style="width: 20px">Included</asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        
            <br />
        
             <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="resetInclusiveProductsButton" runat="Server" Text="Reset" CssClass="Button_Standard Button_Reset" Visible="false" />
                        <asp:Button ID="updateInclusiveProductsButton" runat="Server" Text="Save" CssClass="Button_Standard Button_Save"  Visible="false" OnClientClick="return cancelPostbackOnSave();" />
                    </td>
                </tr>
            </table>

            <asp:Button runat="server" ID="inclusiveProductPopupButton" Style="display: none" />
            <ajaxToolkit:ModalPopupExtender 
                    runat="server" 
                    ID="inclusiveProductPopup" 
                    BehaviorID="inclusiveProductPopupBehavior"
                    TargetControlID="inclusiveProductPopupButton" 
                    PopupControlID="inclusiveProductPopupPanel"
                    BackgroundCssClass="modalBackground" 
                    DropShadow="True" 
                    PopupDragHandleControlID="inclusiveProductPopupDragPanel" />
                
            <asp:Panel runat="server" CssClass="modalPopup" ID="inclusiveProductPopupPanel" Style="display: none; padding: 10px;" Width="500px">
                <asp:Panel runat="Server" ID="inclusiveProductPopupDragPanel" CssClass="modalPopupTitle" >
                    <asp:Label ID="inclusiveProductPopupTitleLabel" runat="Server" Text="Add Product" />
                </asp:Panel>

                <table cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td><b>Product</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <uc1:PickerControl ID="inclusiveProductPopupPicker" AppendDescription="true" runat="server" Width="300px" PopupType="NonVehicleProduct" /></td>
                        <td style="width:25px">&nbsp;</td>
                        <td style="text-align:left">
                            <%--<uc1:PickerControl ID="inclusiveProductPopupPicker" AppendDescription="true" runat="server" Width="300px" PopupType="NonVehicleProduct" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:right;">
                            <asp:Button ID="removeVehicle" runat="server" Text="Remove" CssClass="Button_Standard Button_Remove" Visible="true" OnClientClick="return cancelPostbackOnRemoveVehicles();" />
                             <asp:HiddenField ID="HiddenField_VehicleRows" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Table ID="tblVehicles" EnableViewState="true" runat="server" CellPadding="2" CellSpacing="0" Width="100%">
                                <asp:TableHeaderRow>
                                    <asp:TableHeaderCell ID="chk" Style="width: 150px;background-color:transparent">
                                    Add Vehicle&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="addVehicleDropDown" runat="server" style="width:100px"  AutoPostBack="True">
                                    </asp:DropDownList>
                                    </asp:TableHeaderCell>                                   
                                    <asp:TableHeaderCell Style="width: 100px;background-color:transparent">
                                    </asp:TableHeaderCell>
                                    <asp:TableHeaderCell Style="width: 40px;background-color:transparent"></asp:TableHeaderCell>
                                    <asp:TableHeaderCell Style="width: 10px; text-align:left;background-color:transparent">
                                    </asp:TableHeaderCell>
                                </asp:TableHeaderRow>
                            </asp:Table>
                        </td>
                    </tr>   
                    <tr>
                        <td align="left" colspan="3">
                            <asp:GridView ID="GridView1" runat="server" CssClass="dataTableGrid" ShowHeader="true" GridLines="Both" AutoGenerateColumns="true">
                                 <RowStyle CssClass="evenRow" />
                                 <AlternatingRowStyle CssClass="oddRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="">  
                                        <ItemTemplate>  
                                            <asp:CheckBox ID="remVehicle" runat="server" Enabled="true" Width="87" />  
                                        </ItemTemplate>  
                                    </asp:TemplateField> 
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Vehicle">  
                                        <ItemTemplate>  
                                            <asp:Label ID="lblVehicle" runat="server" Enabled="true" Width="350"  />  
                                        </ItemTemplate>  
                                    </asp:TemplateField> 
                                </Columns>
                                <Columns>
                                    <asp:TemplateField HeaderText="Incl">  
                                        <ItemTemplate>  
                                            <asp:CheckBox ID="excludeVehicle" runat="server" Enabled="true" Width="50" />  
                                        </ItemTemplate>  
                                    </asp:TemplateField> 
                                </Columns>
                                 <Columns>
                                    <asp:TemplateField HeaderText="vehID" Visible="false">  
                                        <ItemTemplate>  
                                            <asp:Label ID="vehID" runat="server" Enabled="true" Width="50" />  
                                        </ItemTemplate>  
                                    </asp:TemplateField> 
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="3">
                            <asp:Button ID="inclusiveProductPopupOKButton" runat="Server" Text="OK"  OnClientClick="return cancelPostback();"  CssClass="Button_Standard Button_OK" />
                            <asp:Button ID="inclusiveProductPopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>