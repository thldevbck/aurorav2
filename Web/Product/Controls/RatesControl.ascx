<%@ Control Language="VB" AutoEventWireup="false" CodeFile="RatesControl.ascx.vb" Inherits="Product_RatesControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<%@ Register Src="RatePopupControl.ascx" TagName="RatePopupControl" TagPrefix="uc1" %>
<%@ Register Src="DiscountPopupControl.ascx" TagName="DiscountPopupControl" TagPrefix="uc1" %>
<%@ Register Src="CopyRateSetPopupControl.ascx" TagName="CopyRateSetPopupControl" TagPrefix="uc1" %>

<asp:UpdatePanel runat="server" ID="rateUpdatePanel">
    <ContentTemplate>

        <table style="width:100%" cellpadding="2" cellspacing="0" id="rateSetTable" runat="server">
            <tr id="rateSetTableRow" runat="server">
                <td style="width:150px; border-style: dotted; border-width: 1px 0px"><b>Rate Set:</b></td>
                <td style="width:250px; border-style: dotted; border-width: 1px 0px"><asp:DropDownList ID="rateSetDropDown" runat="server" style="width:200px" AutoPostBack="True" /></td>
                <td style="width:150px; border-style: dotted; border-width: 1px 0px">Effective Date/Time:</td>
                <td style="border-style: dotted; border-width: 1px 0px">
                    <uc1:DateControl ID="rateSetEffectiveDateTextBox" runat="server" Width="100px" Nullable="false" />
                    <uc1:TimeControl ID="rateSetEffectiveTimeTextBox" runat="server" Width="80px" Nullable="false" />
                </td>
            </tr>
            <tr>
                <td>Unit Of Measure:</td>
                <td><asp:DropDownList ID="rateSetUomDropDown" runat="server" Width="200px"/></td>
                <td>Quantity:</td>
                <td><uc1:RegExTextBox ID="rateSetQuantityTextBox" runat="server" Width="50px" Nullable="false" RegExPattern="^-{0,1}\d+(\.\d+)?$" ErrorMessage="Rate Set Quantity must be a decimal value greater than zero" /></td>
            </tr>
            <tr>
                <td>Flags:</td>
                <td colspan="3">
                    <asp:CheckBox ID="rateSetIsFlexExpCheckBox" runat="server" Text="Flex Export" Enabled="False" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="rateSetPropogrationPendingCheckBox" runat="server" Text="Propagation Pending" Enabled="False" />
                </td>
            </tr>
            <tr>
                <td style="border-bottom-width: 1px" colspan="2"><i><asp:Label ID="rateSetAddModLabel" runat="server" /></i></td>
                <td style="border-bottom-width: 1px" colspan="2" align="right">
                    <asp:Button ID="rateSetReportButton" runat="server" Text="View All" CssClass="Button_Standard Button_Report" Visible="False" Width="100px" />
                    <asp:Button ID="rateSetCopyButton" runat="server" Text="Copy Set" CssClass="Button_Standard Button_Save" Visible="False" Width="100px" />
                    <asp:Button ID="rateSetUpdateButton" runat="server" Text="Save Set" CssClass="Button_Standard Button_Save" Visible="False" />
                    <asp:Button ID="rateSetCreateButton" runat="server" Text="Save Set" CssClass="Button_Standard Button_Save" Visible="False" />
                </td>
            </tr>
        </table>

        <table style="width:100%; margin-top: 10px" cellpadding="2" cellspacing="0" id="rateBandTable" runat="server">
            <tr id="rateBandTableRow" runat="server">
                <td style="width:150px; border-style: dotted; border-width: 1px 0px"><b>Rate Band:</b></td>
                <td style="width:250px; border-style: dotted; border-width: 1px 0px"><asp:DropDownList ID="rateBandDropDown" runat="server" style="width:200px" AutoPostBack="True" /></td>
                <td style="width:150px; border-style: dotted; border-width: 1px 0px">Travel From Date:</td>
                <td style="border-style: dotted; border-width: 1px 0px">
                    <uc1:DateControl ID="rateBandTravelFromTextBox" runat="server" Width="100px" Nullable="false" />
                </td>
            </tr>
            <tr>
                <td>Flags:</td>
                <td colspan="3">
                    <asp:CheckBox ID="rateBandIsFlexExpCheckBox" runat="server" Text="Flex Export" Enabled="False" />
                </td>
            </tr>
            <tr style="height: 24px">
                <td style="border-bottom-width: 1px" colspan="3"><i><asp:Label ID="rateBandAddModLabel" runat="server" /></i></td>
                <td style="border-bottom-width: 1px" colspan="1" align="right">&nbsp;
                    <asp:Button ID="rateBandCreateButton" runat="server" Text="New Rate Band" CssClass="Button_Standard Button_New" Visible="False" />
                </td>
            </tr>
        </table>
        
        <table style="width:100%; margin-top: 10px" cellpadding="2" cellspacing="0" id="ratesTable" runat="server">
            <tr>
                <td>
                    <b>Rates:</b><br />
                </td>
                <td align="right">
                    <asp:Button ID="deleteRateButton" runat="server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                    <asp:Button ID="addRateButton" runat="server" Text="Add Rate" CssClass="Button_Standard Button_Add" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Table ID="productRateTravelBandTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" CssClass="dataTable">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell Style="width: 20px">
                                <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                            </asp:TableHeaderCell>
                            <asp:TableHeaderCell Style="width: 180px">Type</asp:TableHeaderCell>
                            <asp:TableHeaderCell Style="width: 40px; padding-right: 10px">Qty</asp:TableHeaderCell>
                            <asp:TableHeaderCell Style="width: 100px">Uom</asp:TableHeaderCell>
                            <asp:TableHeaderCell Style="width: 50px; text-align: right">Amount&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Description</asp:TableHeaderCell>
                            <asp:TableHeaderCell Style="width: 140px">&nbsp;</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </td>
            </tr>
        </table>
        
        <uc1:RatePopupControl ID="ratePopupControl" runat="server" />
        <uc1:DiscountPopupControl ID="discountPopupControl" runat="server" />
        <uc1:CopyRateSetPopupControl ID="copyRateSetPopupControl" runat="server" />
    </ContentTemplate>
</asp:UpdatePanel>

