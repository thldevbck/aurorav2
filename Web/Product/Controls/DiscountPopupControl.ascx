<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DiscountPopupControl.ascx.vb" Inherits="Product_DiscountPopupControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<asp:Button runat="server" ID="discountPopupButton" Style="display: none" />
<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="discountPopup" 
    BehaviorID="discountPopupBehavior"
    TargetControlID="discountPopupButton" 
    PopupControlID="discountPopupPanel"
    BackgroundCssClass="modalBackground" 
    DropShadow="True" 
    PopupDragHandleControlID="discountPopupDragPanel" />

<script src="../OpsAndLogs/JS/OpsAndLogsCommon.js" language="javascript" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
function UpdateAmountField(baseRateHiddenID,amountTextBoxID,percentTextBoxID)
{
    if(parseFloat( document.getElementById(percentTextBoxID).value) > parseFloat(100))
    {
        // Invalid percentage!
        document.getElementById(percentTextBoxID).value = 100
    }
    else
    {
        document.getElementById(amountTextBoxID).value =   trimDecimal( (parseFloat( document.getElementById(baseRateHiddenID).value) *  parseFloat( document.getElementById(percentTextBoxID).value)/100 ),2);
    }
}

function UpdatePercentField(percentTextBoxId)
{
    document.getElementById(percentTextBoxId).value = ''
}

</script>

<asp:Panel runat="server" CssClass="modalPopup" ID="discountPopupPanel" Style="display: none; padding: 10px;" Width="500px" Height="275px">
    <asp:Panel runat="Server" ID="discountPopupDragPanel" CssClass="modalPopupTitle" >
        <asp:Label ID="discountPopupTitleLabel" runat="Server" Text="Location" />
    </asp:Panel>

    <asp:HiddenField ID="idHidden" runat="server" />
    <asp:HiddenField ID="parentIdHidden" runat="server" />
    <asp:HiddenField ID="parentPtbIdHidden" runat="server" />
    <asp:HiddenField ID = "baseRateHidden" runat="server" />

    <div style="width: 475px; height:35px; padding: 2px 0 2px 0;">
        <uc1:MessagePanelControl runat="server" ID="discountMessagePanelControl" CssClass="popupMessagePanel" />
    </div>

    <div style="width: 100%; height: 165px">
        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td style="width:100px">Discount Type:</td>
                <td style="width:40px">&nbsp;</td>
                <td colspan="3">
                    <asp:DropDownList ID="typeDropDown" runat="server" Width="300px" AutoPostBack="True">
                        <%--<asp:ListItem Value="FOC" Text="Free of Charge" />
                        <asp:ListItem Value="LHD" Text="Long Hire Discount" />
                        <asp:ListItem Value="DBT" Text="Days Before Travel" />
                        <asp:ListItem Value="PDR" Text="Person Dependent" />
                        <asp:ListItem Value="DDT" Text="Day Dependent Travel" />
                        <asp:ListItem Value="FRR" Text="Fixed Rate Reductions" />
                        <asp:ListItem Value="PersonRate" Text="Person Rate Type" />--%>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="quantityTableRow" runat="server">
                <td>Quantity:</td>
                <td>&nbsp;</td>
                <td colspan="3">
                    <uc1:RegExTextBox 
                        ID="quantityTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="false"
                        RegExPattern="-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Discount Quantity must be a decimal value" 
                        MessagePanelControlID="discountMessagePanelControl" />
                </td>
            </tr>
            <tr id="fromToTableRow" runat="server">
                <td>Quantity</td>
                <td>From:</td>
                <td>
                    <uc1:RegExTextBox 
                        ID="fromTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="false"
                        RegExPattern="-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Discount Quantity From must be a decimal value" 
                        MessagePanelControlID="discountMessagePanelControl" />
                </td>
                <td>To:</td>
                <td>
                    <uc1:RegExTextBox 
                        ID="toTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="false"
                        RegExPattern="-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Discount Quantity To must be a decimal value" 
                        MessagePanelControlID="discountMessagePanelControl" />
                </td>
            </tr>
            <tr id="dayFromToTableRow" runat="server">
                <td>Day</td>
                <td>From:</td>
                <td>
                    <asp:DropDownList ID="dayFromDropDown" runat="server" Width="100px">
                        <asp:ListItem Value="1" Text="Monday" />
                        <asp:ListItem Value="2" Text="Tuesday" />
                        <asp:ListItem Value="3" Text="Wednesday" />
                        <asp:ListItem Value="4" Text="Thursday" />
                        <asp:ListItem Value="5" Text="Friday" />
                        <asp:ListItem Value="6" Text="Saturday" />
                        <asp:ListItem Value="7" Text="Sunday" />
                    </asp:DropDownList>
                </td>
                <td style="width:40px">To:</td>
                <td>
                    <asp:DropDownList ID="dayToDropDown" runat="server" Width="100px">
                        <asp:ListItem Value="1" Text="Monday" />
                        <asp:ListItem Value="2" Text="Tuesday" />
                        <asp:ListItem Value="3" Text="Wednesday" />
                        <asp:ListItem Value="4" Text="Thursday" />
                        <asp:ListItem Value="5" Text="Friday" />
                        <asp:ListItem Value="6" Text="Saturday" />
                        <asp:ListItem Value="7" Text="Sunday" />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="uomTableRow" runat="server">
                <td>UoM:</td>
                <td>&nbsp;</td>
                <td colspan="3"><asp:DropDownList ID="uomDropDown" runat="server" Width="150px" /></td>
            </tr>
            <tr id="amountTableRow" runat="server">
                <td>Discount:</td>
                <td>%</td>
                <td>
                    <%--<uc1:RegExTextBox 
                        ID="percentTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="true"
                        RegExPattern="\d+(\.\d+)?$" 
                        ErrorMessage="Discount Percent To must be a decimal value" 
                        MessagePanelControlID="discountMessagePanelControl" />--%>
                        <asp:TextBox CssClass="Textbox_Standard" style="width:50px" 
                        ID="percentTextBox" runat="server" MaxLength="5" 
                        onkeypress="return keyStrokeDecimal(event);" ></asp:TextBox>
                </td>
                <td>$</td>
                <td>
                    <%--<uc1:RegExTextBox 
                        ID="amountTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="true"
                        RegExPattern="\d+(\.\d+)?$" 
                        ErrorMessage="Discount Amount To must be a decimal value"
                        MessagePanelControlID="discountMessagePanelControl" />--%>
                        <asp:TextBox ID="amountTextBox" runat="server" MaxLength="8" 
                        CssClass="Textbox_Standard" style="width:50px"
                        onkeypress="return keyStrokeDecimal(event);" ></asp:TextBox>
                </td>
            </tr>
            <tr id="freeTableRow" runat="server">
                <td>Free</td>
                <td>Quantity:</td>
                <td colspan="3">
                    <uc1:RegExTextBox 
                        ID="freeQuantityTextBox" 
                        runat="server" 
                        Width="50px" 
                        Nullable="false"
                        RegExPattern="-{0,1}\d+(\.\d+)?$" 
                        ErrorMessage="Discount Free Quantity To must be a decimal value" 
                        MessagePanelControlID="discountMessagePanelControl" />
                </td>
            </tr>
            <tr id="freeUomTableRow" runat="server">
                <td>&nbsp;</td>
                <td>UoM:</td>
                <td colspan="3"><asp:DropDownList ID="freeUOMDropDown" runat="server" Width="150px" /></td>
            </tr>
            <tr id="freeProductTableRow" runat="server">
                <td>&nbsp;</td>
                <td>Product:</td>
                <td colspan="3">
                    <uc1:PickerControl 
                        ID="freeProductPicker" 
                        runat="server" 
                        PopupType="PRODUCT" 
                        Nullable="false"
                        AppendDescription="true" 
                        Width="250px" 
                        MessagePanelControlID="discountMessagePanelControl" />
                </td>
            </tr>
        </table>
    </div>

    <table cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td><i><asp:Label ID="addModLabel" runat="server" /></i></td>
        </tr>
        <tr valign="bottom">
            <td align="right" colspan="5">
                <asp:Button ID="addButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="addNextButton" runat="Server" Text="OK / Next" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                <asp:Button ID="updateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="cancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
