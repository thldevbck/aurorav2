<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NonAvailabilityControl.ascx.vb" Inherits="Product_NonAvailabilityControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="False" />
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
        </table>    

        <asp:Table ID="nonAvailabilityTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTable" Width="100%">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Style="width: 20px;">
                    <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                </asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">Travel From</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">Travel To</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">Booked From</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">Booked To</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">Check-Out From</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">Check-Out To</asp:TableHeaderCell>
                <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>

        <asp:Button runat="server" ID="nonAvailabilityPopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="nonAvailabilityPopup" 
            BehaviorID="nonAvailabilityPopupBehavior"
            TargetControlID="nonAvailabilityPopupButton" 
            PopupControlID="nonAvailabilityPopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="nonAvailabilityPopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup" ID="nonAvailabilityPopupPanel" Style="display: none; width: 500px; height: 200px">
            <asp:Panel runat="Server" ID="nonAvailabilityPopupDragPanel" CssClass="modalPopupTitle">
                <asp:Label ID="nonAvailabilityPopupTitleLabel" runat="Server" Text="Non-Availability" />
            </asp:Panel>

            <asp:HiddenField ID="nonAvailabilityPopupIdHidden" runat="server" />

            <div style="width: 475px; height:35px; padding: 2px 0 2px 0;">
                <uc1:MessagePanelControl runat="server" ID="messagePanelControl" CssClass="popupMessagePanel" />
            </div>
    
            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td style="width:150px">Travel</td>
                    <td style="width:25px" align="right">From:</td>
                    <td><uc1:DateControl ID="nonAvailabilityPopupTravelFromTextBox" runat="server" MessagePanelControlID="messagePanelControl" /></td>
                    <td style="width:25px" align="right">To:</td>
                    <td><uc1:DateControl ID="nonAvailabilityPopupTravelToTextBox" runat="server" MessagePanelControlID="messagePanelControl" /></td>
                </tr>
                <tr>
                    <td>Booked</td>
                    <td align="right">From:</td>
                    <td><uc1:DateControl ID="nonAvailabilityPopupBookedFromTextBox" runat="server" MessagePanelControlID="messagePanelControl" /></td>
                    <td align="right">To:</td>
                    <td><uc1:DateControl ID="nonAvailabilityPopupBookedToTextBox" runat="server" MessagePanelControlID="messagePanelControl" /></td>
                </tr>
                <tr>
                    <td>Check-Out</td>
                    <td align="right">From:</td>
                    <td><uc1:DateControl ID="nonAvailabilityPopupCkoFromTextBox" runat="server" MessagePanelControlID="messagePanelControl" /></td>
                    <td align="right">To:</td>
                    <td><uc1:DateControl ID="nonAvailabilityPopupCkoToTextBox" runat="server" MessagePanelControlID="messagePanelControl" /></td>
                </tr>
                <tr><td colspan="5">&nbsp;</td></tr>
                <tr>
                    <td align="right" colspan="5">
                        <asp:Button ID="nonAvailabilityPopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="nonAvailabilityPopupAddNextButton" runat="Server" Text="OK / Next" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="nonAvailabilityPopupDeleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                        <asp:Button ID="nonAvailabilityPopupUpdateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="nonAvailabilityPopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>