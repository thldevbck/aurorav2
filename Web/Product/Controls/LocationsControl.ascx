<%@ Control Language="VB" AutoEventWireup="false" CodeFile="LocationsControl.ascx.vb" Inherits="Product_LocationsControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="false" />
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="false" />
                </td>
            </tr>
        </table>    

        <asp:Table ID="locationsTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTable" Style="width: 100%">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Style="width: 20px;">
                    <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                </asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px;">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 200px;">Travel From</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 200px;">Travel To</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 80px;">Exclude</asp:TableHeaderCell>
                <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>

        <asp:Button runat="server" ID="locationPopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="locationPopup" 
            BehaviorID="locationPopupBehavior"
            TargetControlID="locationPopupButton" 
            PopupControlID="locationPopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="locationPopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup" ID="locationPopupPanel" Style="display: none; padding: 10px;" Width="500px">
            <asp:Panel runat="Server" ID="locationPopupDragPanel" CssClass="modalPopupTitle">
                <asp:Label ID="locationPopupTitleLabel" runat="Server" Text="Location" />
            </asp:Panel>

            <asp:HiddenField ID="locationPopupIdHidden" runat="server" />

            <div style="width: 475px; height:35px; padding: 2px 0 2px 0;">
                <uc1:MessagePanelControl runat="server" ID="messagePanelControl" CssClass="popupMessagePanel" />
            </div>
    
            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td style="width:150px">Travel From:</td>
                    <td><uc1:PickerControl ID="locationPopupTravelFromPicker" runat="server" Width="300px" AppendDescription="true" PopupType="LOCATIONFORCOUNTRY" Param1="NZ" MessagePanelControlID="messagePanelControl" /></td>
                </tr>
                <tr>
                    <td>Travel To:</td>
                    <td><uc1:PickerControl ID="locationPopupTravelToPicker" runat="server" Width="300px" AppendDescription="true" PopupType="LOCATIONFORCOUNTRY" Param1="NZ" MessagePanelControlID="messagePanelControl" /></td>
                </tr>
                <tr>
                    <td>Exclude:</td>
                    <td><asp:CheckBox ID="locationPopupExcludeCheckBox" runat="server" /></td>
                </tr>
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td align="right" colspan="2">
                        <asp:Button ID="locationPopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="locationPopupAddNextButton" runat="Server" Text="OK / Next" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="locationPopupDeleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                        <asp:Button ID="locationPopupUpdateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="locationPopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
