Imports System.Collections.Generic

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet


<ToolboxData("<{0}:AttributesControl runat=server></{0}:AttributesControl>")> _
Partial Class Product_AttributesControl
    Inherits ProductUserControl

    Private _loadException As Exception
    Private _attributeControls As New Dictionary(Of String, Control)

    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)
    End Sub

    Public Sub InitAttributes(ByVal isValueAttribute As Boolean)
        Dim rowIndex As Integer = 0
        For Each attributeRow As AttributeRow In ProductDataSet.Attribute
            If attributeRow.IsAttDataTypeNull Then Continue For
            If isValueAttribute = (attributeRow.AttDataType = ProductConstants.AttributeDataType_Boolean) Then Continue For

            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "evenRow", "oddRow") : rowIndex += 1
            If isValueAttribute Then
                valueAttributesTable.Rows.Add(tableRow)
            Else
                promptAttributesTable.Rows.Add(tableRow)
            End If

            Dim valueTableCell As New TableCell
            tableRow.Controls.Add(valueTableCell)

            Dim nameTableCell As New TableCell
            tableRow.Controls.Add(nameTableCell)

            Dim nameLabel As New Label
            nameLabel.Text = Server.HtmlEncode(attributeRow.AttDesc)
            nameTableCell.Controls.Add(nameLabel)

            Dim control As Control
            If Not isValueAttribute Then
                Dim checkBox As New CheckBox()
                control = checkBox
            Else
                'Dim textBox As UserControls_RegExTextBox = CType(LoadControl("~\UserControls\RegExTextBox\RegExTextBox.ascx"), UserControls_RegExTextBox)
                'textBox.MaxLength = 10
                'textBox.ErrorMessage = "Attribute value must be a decimal"
                'textBox.Nullable = True
                'textBox.RegExPattern = "^-?\d+(\.\d+)?$"
                'textBox.Width = Unit.Parse("40px")
                'control = textBox
                Dim textBox As TextBox = New TextBox
                textBox.MaxLength = 10
                textBox.Width = Unit.Parse("40px")
                control = textBox
            End If
            control.ID = "attribute" & attributeRow.AttCode
            valueTableCell.Controls.Add(control)
            nameLabel.AssociatedControlID = control.ID
            _attributeControls.Add(attributeRow.AttId, control)
        Next
    End Sub

    Public Sub InitAttributes()
        InitAttributes(True)
        InitAttributes(False)
    End Sub

    Public Sub BuildAttributes(ByVal useViewState As Boolean)
        If useViewState Then Return

        For Each attributeRow As AttributeRow In ProductDataSet.Attribute
            If attributeRow.IsAttDataTypeNull Then Continue For

            Dim productAttributeRow As ProductAttributeRow = Nothing
            If attributeRow.GetProductAttributeRows().Length = 1 Then
                productAttributeRow = attributeRow.GetProductAttributeRows()(0)
            End If

            If attributeRow.AttDataType = ProductConstants.AttributeDataType_Boolean Then
                Dim checkBox As CheckBox = CType(_attributeControls(attributeRow.AttId), CheckBox)
                checkBox.Checked = productAttributeRow IsNot Nothing
                checkBox.Enabled = CanMaintain
            Else
                'Dim textBox As UserControls_RegExTextBox = CType(_attributeControls(attributeRow.AttId), UserControls_RegExTextBox)
                Dim textBox As TextBox = CType(_attributeControls(attributeRow.AttId), TextBox)
                If Not (productAttributeRow Is Nothing OrElse productAttributeRow.IsPatValueNull) Then
                    textBox.Text = productAttributeRow.PatValue
                Else
                    textBox.Text = ""
                End If
                textBox.ReadOnly = Not CanMaintain
            End If
        Next

        attributesUpdateButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.SaleableProductRow Is Nothing Then Return

        Try
            InitAttributes()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Page.IsPostBack Then Return

        BuildAttributes(False)
    End Sub

    Protected Sub attributesUpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles attributesUpdateButton.Click
        If Not CanMaintain Then Return

        'Try
        '    For Each attributeRow As AttributeRow In ProductDataSet.Attribute
        '        If attributeRow.IsAttDataTypeNull Then Continue For

        '        Dim productAttributeRow As ProductAttributeRow = Me.ProductDataSet.ProductAttribute.FindByAttIdSapId(attributeRow.AttId, Me.SaleableProductRow.SapId)

        '        If attributeRow.AttDataType <> ProductConstants.AttributeDataType_Boolean Then
        '            Dim textBox As UserControls_RegExTextBox = CType(_attributeControls(attributeRow.AttId), UserControls_RegExTextBox)
        '            If Not textBox.IsTextValid Then
        '                Throw New Aurora.Common.ValidationException(textBox.ErrorMessage)
        '            End If
        '        End If
        '    Next
        'Catch ex As Aurora.Common.ValidationException
        '    Me.CurrentPage.AddErrorMessage(ex.Message)
        '    Return
        'End Try

        Try
            For Each attributeRow As AttributeRow In ProductDataSet.Attribute
                If attributeRow.IsAttDataTypeNull Then Continue For

                Dim productAttributeRow As ProductAttributeRow = Me.ProductDataSet.ProductAttribute.FindByAttIdSapId(attributeRow.AttId, Me.SaleableProductRow.SapId)

                If attributeRow.AttDataType = ProductConstants.AttributeDataType_Boolean Then
                    Dim checkBox As CheckBox = CType(_attributeControls(attributeRow.AttId), CheckBox)

                    If productAttributeRow Is Nothing AndAlso checkBox.Checked Then
                        productAttributeRow = Me.ProductDataSet.ProductAttribute.NewProductAttributeRow()
                        productAttributeRow.PatId = DataRepository.GetNewId()
                        productAttributeRow.PatAttId = attributeRow.AttId
                        productAttributeRow.PatSapId = Me.SaleableProductRow.SapId
                        Me.ProductDataSet.ProductAttribute.AddProductAttributeRow(productAttributeRow)
                    ElseIf productAttributeRow IsNot Nothing AndAlso Not checkBox.Checked Then
                        productAttributeRow.Delete()
                    End If
                Else
                    'Dim textBox As UserControls_RegExTextBox = CType(_attributeControls(attributeRow.AttId), UserControls_RegExTextBox)
                    Dim textBox As TextBox = CType(_attributeControls(attributeRow.AttId), TextBox)
                    Dim newValue As String = textBox.Text.Trim()
                    If attributeRow.AttDataType = ProductConstants.AttributeDataType_Integer Then
                        'Try
                        '    newValue = Decimal.Parse(newValue).ToString("0.####")
                        'Catch
                        '    newValue = ""
                        'End Try
                    End If

                    If productAttributeRow Is Nothing AndAlso Not String.IsNullOrEmpty(newValue) Then
                        productAttributeRow = Me.ProductDataSet.ProductAttribute.NewProductAttributeRow()
                        productAttributeRow.PatId = DataRepository.GetNewId()
                        productAttributeRow.PatAttId = attributeRow.AttId
                        productAttributeRow.PatSapId = Me.SaleableProductRow.SapId
                        productAttributeRow.PatValue = newValue
                        Me.ProductDataSet.ProductAttribute.AddProductAttributeRow(productAttributeRow)
                    ElseIf productAttributeRow IsNot Nothing AndAlso String.IsNullOrEmpty(newValue) Then
                        productAttributeRow.Delete()
                    ElseIf productAttributeRow IsNot Nothing AndAlso productAttributeRow.Value <> newValue Then
                        productAttributeRow.PatValue = newValue
                    End If
                End If

                If productAttributeRow IsNot Nothing AndAlso productAttributeRow.RowState <> Data.DataRowState.Unchanged Then
                    Aurora.Common.Data.ExecuteDataRow(productAttributeRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
                End If

                If productAttributeRow IsNot Nothing AndAlso productAttributeRow.RowState = Data.DataRowState.Deleted Then
                    Me.ProductDataSet.ProductAttribute.Rows.Remove(productAttributeRow)
                End If
            Next
        Catch
            Me.CurrentPage.AddInformationMessage("Error updating Attributes")
            Return
        End Try

        Me.CurrentPage.AddInformationMessage("Attributes updated")
    End Sub
End Class
