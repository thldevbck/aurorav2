' Change Log!
' 17.7.9    -   Shoel   - Log added!
' 17.7.9    -   Shoel   - Fixed bug with Vehicle dependent note causing base rate to be unchangable!
' 1.9.9     -   Shoel   - Added fix for SQUISH 756

Imports System.Collections.Generic

Imports System.Data

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<ToolboxData("<{0}:RatePopupControl runat=server></{0}:RatePopupControl>")> _
Partial Class Product_RatePopupControl
    Inherits ProductUserControl

    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)
    End Sub

    Private Sub InitLookups()
        Dim selectedValue As String

        selectedValue = uomDropDown.SelectedValue
        uomDropDown.Items.Clear()
        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Unit_of_Measure Then
                uomDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            End If
        Next
        Try
            uomDropDown.SelectedValue = selectedValue
        Catch
        End Try

        selectedValue = personTypeDropDown.SelectedValue
        personTypeDropDown.Items.Clear()
        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Person_Rate Then
                personTypeDropDown.Items.Add(New ListItem(codeRow.CodCode, codeRow.CodId))
            End If
        Next
        Try
            personTypeDropDown.SelectedValue = selectedValue
        Catch
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.SaleableProductRow Is Nothing Then Return

        Try
            InitLookups()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Me.IsPostBack Then Return

        ' 1.12.8 : Shoel : removed to ensure postback and reload :)
        'cancelButton.Attributes.Add("onclick", "$find('ratePopupBehavior').hide(); return false;")
    End Sub

    Private Sub InitRatePopupTypeVisibility()
        Me.quantityTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue <> "FixedRateRow", "", "none"))
        Me.fromToTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "FixedRateRow", "", "none"))
        Me.uomDropDown.Enabled = Me.typeDropDown.SelectedValue = "ProductRateTravelBandRow" OrElse Me.typeDropDown.SelectedValue = "VehicleDependentRateRow"
        Me.chargeTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "VehicleDependentRateRow", "", "none"))
        Me.agentTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "AgentDependentRateRow", "", "none"))
        Me.vehicleTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "VehicleDependentRateRow", "", "none"))
        Me.bookedTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "ProductRateBookingBandRow", "", "none"))
        Me.locationFromTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "LocationDependentRateRow", "", "none"))
        Me.locationToTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "LocationDependentRateRow", "", "none"))
        Me.personTypeTableRow.Style.Add("display", IIf(Me.typeDropDown.SelectedValue = "PersonRateRow", "", "none"))

        If typeDropDown.SelectedValue = "VehicleDependentRateRow" Then
            containerDiv.Style("height") = "300px"
            ratePopupPanel.Height = 400
            vehicleDependentNoteTableRow.Style("display") = "block"
        Else
            containerDiv.Style("height") = "165px"
            ratePopupPanel.Height = 275
            vehicleDependentNoteTableRow.Style("display") = "none"
        End If

    End Sub

    Private Function GetProductRateFromPopup() As IProductRate
        Dim id As String = Me.idHidden.Value
        Dim type As String = Me.typeDropDown.SelectedValue

        Select Case type
            Case "ProductRateTravelBandRow" : Return Me.ProductDataSet.ProductRateTravelBand.FindByPtbId(id)
            Case "AgentDependentRateRow" : Return Me.ProductDataSet.AgentDependentRate.FindByAdrId(id)
            Case "VehicleDependentRateRow" : Return Me.ProductDataSet.VehicleDependentRate.FindByVdrId(id)
            Case "ProductRateBookingBandRow" : Return Me.ProductDataSet.ProductRateBookingBand.FindByBapId(id)
            Case "LocationDependentRateRow" : Return Me.ProductDataSet.LocationDependentRate.FindByLdrId(id)
            Case "PersonDependentRateRow" : Return Me.ProductDataSet.PersonDependentRate.FindByPdrId(id)
            Case "FixedRateRow" : Return Me.ProductDataSet.FixedRate.FindByFfrId(id)
            Case "PersonRateRow" : Return Me.ProductDataSet.PersonRate.FindByPrrId(id)
        End Select

        Return Nothing
    End Function


    Private Function GetProductRateTravelBandRowFromPopup() As ProductRateTravelBandRow
        Return Me.ProductDataSet.ProductRateTravelBand.FindByPtbId(Me.parentPtbIdHidden.Value)
    End Function

    Private Function CreateProductRateFromPopup() As IProductRate
        Dim type As String = Me.typeDropDown.SelectedValue

        Select Case type
            Case "AgentDependentRateRow"
                Dim agentDependentRateRow As AgentDependentRateRow = Me.ProductDataSet.AgentDependentRate.NewAgentDependentRateRow()
                agentDependentRateRow.Id = DataRepository.GetNewId()
                agentDependentRateRow.ParentPtbId = parentPtbIdHidden.Value
                Me.ProductDataSet.AgentDependentRate.AddAgentDependentRateRow(agentDependentRateRow)
                Return agentDependentRateRow
            Case "VehicleDependentRateRow"
                Dim vehicleDependentRateRow As VehicleDependentRateRow = Me.ProductDataSet.VehicleDependentRate.NewVehicleDependentRateRow()
                vehicleDependentRateRow.Id = DataRepository.GetNewId()
                vehicleDependentRateRow.ParentPtbId = parentPtbIdHidden.Value
                Me.ProductDataSet.VehicleDependentRate.AddVehicleDependentRateRow(vehicleDependentRateRow)
                Return vehicleDependentRateRow
            Case "ProductRateBookingBandRow"
                Dim productRateBookingBandRow As ProductRateBookingBandRow = Me.ProductDataSet.ProductRateBookingBand.NewProductRateBookingBandRow()
                productRateBookingBandRow.Id = DataRepository.GetNewId()
                productRateBookingBandRow.ParentPtbId = parentPtbIdHidden.Value
                Me.ProductDataSet.ProductRateBookingBand.AddProductRateBookingBandRow(productRateBookingBandRow)
                Return productRateBookingBandRow
            Case "LocationDependentRateRow"
                Dim locationDependentRateRow As LocationDependentRateRow = Me.ProductDataSet.LocationDependentRate.NewLocationDependentRateRow()
                locationDependentRateRow.Id = DataRepository.GetNewId()
                locationDependentRateRow.LdrBapId = parentBapIdHidden.Value
                Me.ProductDataSet.LocationDependentRate.AddLocationDependentRateRow(locationDependentRateRow)
                Return locationDependentRateRow
            Case "PersonDependentRateRow"
                Dim personDependentRateRow As PersonDependentRateRow = Me.ProductDataSet.PersonDependentRate.NewPersonDependentRateRow()
                personDependentRateRow.Id = DataRepository.GetNewId()
                personDependentRateRow.ParentPtbId = parentPtbIdHidden.Value
                Me.ProductDataSet.PersonDependentRate.AddPersonDependentRateRow(personDependentRateRow)
                Return personDependentRateRow
            Case "FixedRateRow"
                Dim fixedRateRow As FixedRateRow = Me.ProductDataSet.FixedRate.NewFixedRateRow()
                fixedRateRow.Id = DataRepository.GetNewId()
                fixedRateRow.ParentPtbId = parentPtbIdHidden.Value
                Me.ProductDataSet.FixedRate.AddFixedRateRow(fixedRateRow)
                Return fixedRateRow
            Case "PersonRateRow"
                Dim personRateRow As PersonRateRow = Me.ProductDataSet.PersonRate.NewPersonRateRow()
                personRateRow.Id = DataRepository.GetNewId()
                personRateRow.ParentPtbId = parentPtbIdHidden.Value
                Me.ProductDataSet.PersonRate.AddPersonRateRow(personRateRow)
                Return personRateRow
        End Select

        Return Nothing
    End Function

    Private Function ValidateProductRateFromPopup() As Boolean
        Dim type As String = Me.typeDropDown.SelectedValue

        Dim message As String = Nothing
        If Not quantityTextBox.IsTextValid() Then
            message = quantityTextBox.ErrorMessage
        ElseIf Not rateTextBox.IsTextValid() Then
            message = rateTextBox.ErrorMessage
        ElseIf type = "AgentDependentRateRow" AndAlso Not agentPicker.IsValid Then
            message = agentPicker.ErrorMessage
        ElseIf type = "VehicleDependentRateRow" AndAlso Not minChargeTextBox.IsTextValid Then
            message = minChargeTextBox.ErrorMessage
        ElseIf type = "VehicleDependentRateRow" AndAlso Not maxChargeTextBox.IsTextValid Then
            message = maxChargeTextBox.ErrorMessage
        ElseIf type = "VehicleDependentRateRow" AndAlso Not vehiclePicker.IsValid Then
            message = vehiclePicker.ErrorMessage
        ElseIf (type = "ProductRateBookingBandRow") AndAlso Not bookedFromTextBox.IsValid Then
            message = "Enter a valid booked from date"
        ElseIf (type = "ProductRateBookingBandRow") AndAlso Not bookedToTextBox.IsValid Then
            message = "Enter a valid booked to date"
        ElseIf type = "LocationDependentRateRow" AndAlso Not locationFromPicker.IsValid Then
            message = locationFromPicker.ErrorMessage
        ElseIf type = "LocationDependentRateRow" AndAlso Not locationToPicker.IsValid Then
            message = locationToPicker.ErrorMessage
        ElseIf type = "LocationDependentRateRow" AndAlso locationFromPicker.DataId Is Nothing AndAlso locationToPicker.DataId Is Nothing Then
            message = "A From or To Location must be specified"
        ElseIf type = "FixedRateRow" AndAlso Not fromTextBox.IsTextValid Then
            message = fromTextBox.ErrorMessage
        ElseIf type = "FixedRateRow" AndAlso Not toTextBox.IsTextValid Then
            message = toTextBox.ErrorMessage
        End If

        If message IsNot Nothing Then
            rateMessagePanelControl.SetMessagePanelWarning(message)
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub UpdateProductRateFromPopup(ByVal productRate As IProductRate)
        productRate.Qty = Decimal.Parse(quantityTextBox.Text)
        ''rev:mia dec8
        productRate.Rate = IIf(rateTextBox.Text = "", Decimal.Parse(9999999999), rateTextBox.Text)
        productRate.UomId = uomDropDown.SelectedValue

        If TypeOf productRate Is ProductRateTravelBandRow Then
        ElseIf TypeOf productRate Is AgentDependentRateRow Then
            Dim agentDependentRateRow As AgentDependentRateRow = CType(productRate, AgentDependentRateRow)
            If Not String.IsNullOrEmpty(agentPicker.DataId) Then
                agentDependentRateRow.AdrAgnId = agentPicker.DataId
            Else
                agentDependentRateRow.SetAdrAgnIdNull()
            End If
        ElseIf TypeOf productRate Is VehicleDependentRateRow Then
            Dim vehicleDependentRateRow As VehicleDependentRateRow = CType(productRate, VehicleDependentRateRow)

            Try
                vehicleDependentRateRow.VdrMinChg = IIf(minChargeTextBox.Text = "", Decimal.Parse(9999999999), minChargeTextBox.Text) ''Decimal.Parse(minChargeTextBox.Text)
                If vehicleDependentRateRow.VdrMinChg < 0 Then vehicleDependentRateRow.VdrMinChg = 0
            Catch
                vehicleDependentRateRow.VdrMinChg = Decimal.Parse(9999999999)
            End Try
            Try
                vehicleDependentRateRow.VdrMaxChg = IIf(maxChargeTextBox.Text = "", Decimal.Parse(9999999999), maxChargeTextBox.Text) ''Decimal.Parse(maxChargeTextBox.Text)
                If vehicleDependentRateRow.VdrMaxChg < 0 Then vehicleDependentRateRow.VdrMaxChg = 0
            Catch
                vehicleDependentRateRow.VdrMaxChg = Decimal.Parse(9999999999)
            End Try
            If vehicleDependentRateRow.VdrMinChg > vehicleDependentRateRow.VdrMaxChg Then
                Dim chg As Integer = vehicleDependentRateRow.VdrMaxChg
                vehicleDependentRateRow.VdrMaxChg = vehicleDependentRateRow.VdrMinChg
                vehicleDependentRateRow.VdrMinChg = chg
            End If

            If Not String.IsNullOrEmpty(vehiclePicker.DataId) Then
                vehicleDependentRateRow.VdrPrdId = vehiclePicker.DataId
            Else
                vehicleDependentRateRow.SetVdrPrdIdNull()
            End If
        ElseIf TypeOf productRate Is ProductRateBookingBandRow Then
            Dim productRateBookingBandRow As ProductRateBookingBandRow = CType(productRate, ProductRateBookingBandRow)
            productRateBookingBandRow.BapBookedFromDate = bookedFromTextBox.Date
            productRateBookingBandRow.BapBookedToDate = bookedToTextBox.Date
        ElseIf TypeOf productRate Is LocationDependentRateRow Then
            Dim locationDependentRateRow As LocationDependentRateRow = CType(productRate, LocationDependentRateRow)
            If Not String.IsNullOrEmpty(locationFromPicker.DataId) Then
                locationDependentRateRow.LdrLocFromCode = locationFromPicker.DataId
            Else
                locationDependentRateRow.SetLdrLocFromCodeNull()
            End If
            If Not String.IsNullOrEmpty(locationToPicker.DataId) Then
                locationDependentRateRow.LdrLocToCode = locationToPicker.DataId
            Else
                locationDependentRateRow.SetLdrLocToCodeNull()
            End If
        ElseIf TypeOf productRate Is FixedRateRow Then
            Dim fixedRateRow As FixedRateRow = CType(productRate, FixedRateRow)
            fixedRateRow.FfrFrom = Integer.Parse(fromTextBox.Text)
            fixedRateRow.FfrTo = Integer.Parse(toTextBox.Text)
        ElseIf TypeOf productRate Is PersonRateRow Then
            Dim personRateRow As PersonRateRow = CType(productRate, PersonRateRow)
            personRateRow.PrrCodPrtId = personTypeDropDown.SelectedValue
        End If
    End Sub

    Public Sub ShowNew(ByVal productRateTravelBandRow As ProductRateTravelBandRow, ByVal productRateBookingBandRow As ProductRateBookingBandRow)

        idHidden.Value = ""
        parentPtbIdHidden.Value = productRateTravelBandRow.PtbId

        If productRateBookingBandRow Is Nothing Then
            typeDropDown.SelectedIndex = 1
            typeDropDown.Items(0).Enabled = False
            typeDropDown.Items(4).Enabled = False
            typeDropDown.Enabled = True
            parentBapIdHidden.Value = ""
        Else
            typeDropDown.Items(0).Enabled = True
            typeDropDown.Items(4).Enabled = True
            typeDropDown.SelectedIndex = 4
            typeDropDown.Enabled = False
            parentBapIdHidden.Value = productRateBookingBandRow.BapId
        End If
        vehicleDependentText.Text = String.Empty

        quantityTextBox.Text = "1"
        fromTextBox.Text = ""
        toTextBox.Text = ""
        rateTextBox.Text = ""
        uomDropDown.SelectedValue = productRateTravelBandRow.UomId
        agentPicker.DataId = ""
        agentPicker.Text = ""
        vehiclePicker.DataId = ""
        vehiclePicker.Text = ""
        bookedFromTextBox.Text = ""
        bookedToTextBox.Text = ""
        locationFromPicker.Param1 = Me.SaleableProductRow.SapCtyCode
        locationFromPicker.DataId = ""
        locationFromPicker.Text = ""
        locationToPicker.Param1 = Me.SaleableProductRow.SapCtyCode
        locationToPicker.DataId = ""
        locationToPicker.Text = ""
        personTypeDropDown.SelectedIndex = 0

        addModLabel.Text = ""

        addButton.Visible = True
        addNextButton.Visible = True
        deleteButton.Visible = False
        updateButton.Visible = False

        ratePopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Rate"

        InitRatePopupTypeVisibility()
        ratePopup.Show()
    End Sub

    Public Sub ShowExisting(ByVal productRate As IProductRate)
        idHidden.Value = productRate.Id
        parentPtbIdHidden.Value = productRate.ParentPtbId

        typeDropDown.Items(0).Enabled = True
        typeDropDown.Items(4).Enabled = True
        typeDropDown.SelectedValue = CType(productRate, Object).GetType().Name
        typeDropDown.Enabled = False

        quantityTextBox.Text = productRate.Qty.ToString("#.####")
        ''rev:mia dec17
        If productRate.Qty.ToString = "0.0000" Then
            quantityTextBox.Text = 0.0
        End If
        ''rev:mia dec17
        rateTextBox.Text = productRate.Rate.ToString("#.####")
        If productRate.Rate.ToString = "0.0000" Then
            rateTextBox.Text = 0.0
        End If

        uomDropDown.SelectedIndex = 0
        Try
            If productRate.UomCode IsNot Nothing Then uomDropDown.SelectedValue = productRate.UomCode.CodId
        Catch
        End Try
        If TypeOf productRate Is ProductRateTravelBandRow Then
        ElseIf TypeOf productRate Is AgentDependentRateRow Then
            agentPicker.DataId = ""
            agentPicker.Text = ""
            Try
                If CType(productRate, AgentDependentRateRow).AgentRow IsNot Nothing Then
                    agentPicker.DataId = CType(productRate, AgentDependentRateRow).AgentRow.AgnId
                    agentPicker.Text = CType(productRate, AgentDependentRateRow).AgentRow.Description
                End If
            Catch
            End Try
        ElseIf TypeOf productRate Is VehicleDependentRateRow Then
            Dim vehicleDependentRateRow As VehicleDependentRateRow = CType(productRate, VehicleDependentRateRow)
            minChargeTextBox.Text = vehicleDependentRateRow.MinChg.ToString("#.####")
            maxChargeTextBox.Text = vehicleDependentRateRow.MaxChg.ToString("#.####")
            vehiclePicker.DataId = ""
            vehiclePicker.Text = ""
            Try
                If vehicleDependentRateRow.VehicleProductRow IsNot Nothing Then
                    vehiclePicker.DataId = vehicleDependentRateRow.VehicleProductRow.PrdId
                    vehiclePicker.Text = vehicleDependentRateRow.VehicleProductRow.Description
                End If
                If Not vehicleDependentRateRow.IsNteDescNull Then
                    vehicleDependentText.Text = vehicleDependentRateRow.NteDesc
                Else
                    vehicleDependentText.Text = String.Empty
                End If
            Catch
            End Try
        ElseIf TypeOf productRate Is ProductRateBookingBandRow Then
            Dim productRateBookingBandRow As ProductRateBookingBandRow = CType(productRate, ProductRateBookingBandRow)
            bookedFromTextBox.Date = productRateBookingBandRow.BapBookedFromDate
            bookedToTextBox.Date = productRateBookingBandRow.BapBookedToDate
        ElseIf TypeOf productRate Is LocationDependentRateRow Then
            Dim locationDependentRateRow As LocationDependentRateRow = CType(productRate, LocationDependentRateRow)
            locationFromPicker.Param1 = Me.SaleableProductRow.SapCtyCode
            locationFromPicker.DataId = ""
            locationFromPicker.Text = ""
            Try
                If locationDependentRateRow.LocationRowByLocation_LocationDependentRate_From IsNot Nothing Then
                    locationFromPicker.DataId = locationDependentRateRow.LocationRowByLocation_LocationDependentRate_From.LocCode
                    locationFromPicker.Text = locationDependentRateRow.LocationRowByLocation_LocationDependentRate_From.Description
                End If
            Catch
            End Try
            locationToPicker.Param1 = Me.SaleableProductRow.SapCtyCode
            locationToPicker.DataId = ""
            locationToPicker.Text = ""
            Try
                If locationDependentRateRow.LocationRowByLocation_LocationDependentRate_To IsNot Nothing Then
                    locationToPicker.DataId = locationDependentRateRow.LocationRowByLocation_LocationDependentRate_To.LocCode
                    locationToPicker.Text = locationDependentRateRow.LocationRowByLocation_LocationDependentRate_To.Description
                End If
            Catch
            End Try
        ElseIf TypeOf productRate Is FixedRateRow Then
            Dim fixedRateRow As FixedRateRow = CType(productRate, FixedRateRow)
            fromTextBox.Text = CStr(fixedRateRow.FfrFrom)
            toTextBox.Text = CStr(fixedRateRow.FfrTo)
        ElseIf TypeOf productRate Is PersonRateRow Then
            Dim personRateRow As PersonRateRow = CType(productRate, PersonRateRow)
            personTypeDropDown.SelectedIndex = 0
            Try
                If Not personRateRow.IsPrrCodPrtIdNull AndAlso Me.ProductDataSet.Code.FindById(personRateRow.PrrCodPrtId) IsNot Nothing Then _
                    personTypeDropDown.SelectedValue = personRateRow.PrrCodPrtId
            Catch
            End Try
        End If

        addModLabel.Text = Server.HtmlEncode(Aurora.Common.Data.GetAddModDescription(CType(productRate, DataRow)))

        addButton.Visible = False
        addNextButton.Visible = False
        deleteButton.Visible = Not (TypeOf productRate Is ProductRateTravelBandRow)
        updateButton.Visible = True
        ratePopupTitleLabel.Text = "Edit " & typeDropDown.SelectedItem.Text & " Rate"
        InitRatePopupTypeVisibility()
        ratePopup.Show()
    End Sub

    Protected Sub ratePopupAddButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandRowFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        If Not ValidateProductRateFromPopup() Then
            ratePopup.Show()
            Return
        End If

        Dim productRate As IProductRate = CreateProductRateFromPopup()
        If productRate Is Nothing Then Return

        UpdateProductRateFromPopup(productRate)

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                ' add the product rate
                ''Aurora.Common.Data.ExecuteDataRow(productRate, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
                ''rev:mia dec 9
                Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRate, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

                ' Add the Vehicle Dependent Note
                Dim sRetMsg As String
                sRetMsg = Aurora.Product.Data.CreateVehicleDependentNote(System.Guid.NewGuid.ToString(), Left(Me.vehicleDependentText.Text, 2048), productRate.Id, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
                If sRetMsg.ToUpper().Trim().IndexOf("SUCCESS") < 0 Then
                    Throw New Exception("Unable to create Vehicle Dependent Note")
                End If
                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Me.CurrentPage.SetErrorMessage(ex.Message)
                Exit Sub
            End Try
        End Using


        ' update "moddatetime" the base product rate band
        productRateTravelBandRow.ModDateTime = Date.Now
        ''Aurora.Common.Data.ExecuteDataRow(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=productRateTravelBandRow.PtbTbsId, _
            ptbId:=productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub ratePopupAddNextButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addNextButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandRowFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        If Not ValidateProductRateFromPopup() Then
            ratePopup.Show()
            Return
        End If

        Dim productRate As IProductRate = CreateProductRateFromPopup()
        If productRate Is Nothing Then Return

        UpdateProductRateFromPopup(productRate)

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                ' add the product rate
                ''Aurora.Common.Data.ExecuteDataRow(productRate, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
                ''rev:mia dec 9
                Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRate, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

                ' Add the Vehicle Dependent Note
                Dim sRetMsg As String
                sRetMsg = Aurora.Product.Data.CreateVehicleDependentNote(System.Guid.NewGuid.ToString(), Me.vehicleDependentText.Text, productRate.Id, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
                If sRetMsg.ToUpper().Trim().IndexOf("SUCCESS") < 0 Then
                    Throw New Exception("Unable to create Vehicle Dependent Note")
                End If

                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Me.CurrentPage.SetErrorMessage(ex.Message)
                Exit Sub
            End Try
        End Using
        ' update "moddatetime" the base product rate band
        productRateTravelBandRow.ModDateTime = Date.Now
        ''Aurora.Common.Data.ExecuteDataRow(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        ShowNew(GetProductRateTravelBandRowFromPopup(), Nothing)
    End Sub

    Protected Sub ratePopupDeleteButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles deleteButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandRowFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        Dim productRate As IProductRate = GetProductRateFromPopup()
        If productRate Is Nothing Then Return

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                ' delete the product rate
                DataRepository.DeleteRate(productRateTravelBandRow.PtbId, productRate.Id)

                ' delete the Vehicle Dependent Rate if any
                If Not CType(productRate, VehicleDependentRateRow).IsNteIdNull AndAlso _
                   Not String.IsNullOrEmpty(CType(productRate, VehicleDependentRateRow).NteId) Then
                    Dim sRetMsg As String
                    sRetMsg = Aurora.Product.Data.DeleteVehicleDependentNote(CType(productRate, VehicleDependentRateRow).NteId)
                    If sRetMsg.ToUpper().Trim().IndexOf("SUCCESS") < 0 Then
                        Throw New Exception("Unable to delete Vehicle Dependent Note")
                    End If
                End If

                ' update "moddatetime" the base product rate band
                productRateTravelBandRow.ModDateTime = Date.Now
                Aurora.Common.Data.ExecuteDataRowUpdateVehicleDependentRate(productRateTravelBandRow, Me.CurrentPage.UserCode)

                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Me.CurrentPage.SetErrorMessage(ex.Message)
                Exit Sub
            End Try
        End Using


        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=productRateTravelBandRow.PtbTbsId, _
            ptbId:=productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub ratePopupUpdateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles updateButton.Click
        Dim productRateTravelBandRow As ProductRateTravelBandRow = GetProductRateTravelBandRowFromPopup()
        If productRateTravelBandRow Is Nothing Then Return

        Dim productRate As IProductRate = GetProductRateFromPopup()
        If productRate Is Nothing Then Return

        If Not ValidateProductRateFromPopup() Then
            ratePopup.Show()
            Return
        End If

        UpdateProductRateFromPopup(productRate)

        ' update the product rate
        ''Aurora.Common.Data.ExecuteDataRow(productRate, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        ''rev:mia dec 9
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRate, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

                Dim sParentId As String = ""
                ' also update all its children!!
                Select Case typeDropDown.SelectedValue
                    Case "ProductRateTravelBandRow"
                        sParentId = productRateTravelBandRow.PtbId
                    Case "AgentDependentRateRow"
                        sParentId = productRate.Id
                    Case "VehicleDependentRateRow"
                        sParentId = productRate.Id
                    Case "ProductRateBookingBandRow"
                        sParentId = productRate.Id
                    Case "LocationDependentRateRow"
                        sParentId = productRate.Id
                    Case "PersonDependentRateRow"
                        sParentId = productRate.Id
                    Case "FixedRateRow"
                        sParentId = productRate.Id
                    Case "PersonRateRow"
                        sParentId = productRate.Id
                End Select
                If rateTextBox.Text.Trim().Equals(String.Empty) Then
                    Aurora.Product.Data.UpdateChildDicounts(sParentId, 9999999999D)
                Else
                    Aurora.Product.Data.UpdateChildDicounts(sParentId, CDec(rateTextBox.Text))
                End If

                ' also update all its children!!

                ' Update Vehicle Dependent Rate, if it exists
                Dim oVDRRow As VehicleDependentRateRow
                Try
                    oVDRRow = CType(productRate, VehicleDependentRateRow)
                Catch ex As Exception
                    ' do nothing
                End Try

                If oVDRRow IsNot Nothing AndAlso _
                   Not oVDRRow.IsNteIdNull AndAlso _
                   Not String.IsNullOrEmpty(oVDRRow.NteId) Then
                    Dim sRetMsg As String
                    sRetMsg = Aurora.Product.Data.UpdateVehicleDependentNote(oVDRRow.NteId, Me.vehicleDependentText.Text, Me.CurrentPage.UserId)
                    If sRetMsg.ToUpper().Trim().IndexOf("SUCCESS") < 0 Then
                        Throw New Exception("Unable to update Vehicle Dependent Note")
                    End If
                Else
                    ' if it doesnt exist, add it, provided there is some text in the vehicle dependent rate textbox
                    If Not String.IsNullOrEmpty(Me.vehicleDependentText.Text.Trim()) Then
                        Dim sRetMsg As String
                        sRetMsg = Aurora.Product.Data.CreateVehicleDependentNote(System.Guid.NewGuid.ToString(), Me.vehicleDependentText.Text, productRate.Id, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
                        If sRetMsg.ToUpper().Trim().IndexOf("SUCCESS") < 0 Then
                            Throw New Exception("Unable to update Vehicle Dependent Note")
                        End If
                    End If
                End If

                ' update "moddatetime" the base product rate band
                If productRate IsNot productRateTravelBandRow Then
                    productRateTravelBandRow.ModDateTime = Date.Now
                    ''Aurora.Common.Data.ExecuteDataRow(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
                    Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
                End If

                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Me.CurrentPage.SetErrorMessage(ex.Message)
                Exit Sub
            End Try
        End Using

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=productRateTravelBandRow.PtbTbsId, _
            ptbId:=productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub ratePopupCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        ratePopup.Hide()
    End Sub

    Protected Sub typeDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles typeDropDown.SelectedIndexChanged
        ratePopupTitleLabel.Text = "Add " & typeDropDown.SelectedItem.Text & " Rate"
        InitRatePopupTypeVisibility()
        ratePopup.Show()
    End Sub

End Class
