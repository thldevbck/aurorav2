Imports System.Collections.Generic

Imports System.Data

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<ToolboxData("<{0}:RatesControl runat=server></{0}:RatesControl>")> _
Partial Class Product_RatesControl
    Inherits ProductUserControl

    Dim _loadException As Exception

    Private _travelBandSetRow As TravelBandSetRow = Nothing
    Private _productRateTravelBandRow As ProductRateTravelBandRow = Nothing

    Private _ratesByLinkButtonId As New Dictionary(Of String, IProductRate)
    Private _discountByLinkButtonId As New Dictionary(Of String, ProductRateDiscountRow)
    Private _ratesByAddDiscountLinkButtonId As New Dictionary(Of String, IProductRate)
    Private _ratesByAddLDRLinkButtonId As New Dictionary(Of String, ProductRateBookingBandRow)
    Private _checkBoxesByRateId As New Dictionary(Of String, CheckBox)
    Private _checkBoxesByDiscountId As New Dictionary(Of String, CheckBox)

    Private currentDate As Date = Date.Now
    Private _defaultTbsId As String = Nothing
    Private _defaultPtdId As String = Nothing

    Private Sub InitLookups()
        rateSetUomDropDown.Items.Clear()
        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Unit_of_Measure Then
                rateSetUomDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            End If
        Next
    End Sub

    Private Sub InitRateSet()
        Dim listItem As ListItem

        If Me.SaleableProductRow IsNot Nothing Then

            Dim selectedTbsId As String = Me.Request.QueryString(TbsIdParam)
            If Me.IsPostBack Then
                selectedTbsId = rateSetDropDown.SelectedValue
            End If

            Dim travelBandSetRows As TravelBandSetRow() = Me.SaleableProductRow.GetTravelBandSetRows()
            Array.Reverse(travelBandSetRows)
            rateSetDropDown.Items.Clear()
            For Each travelBandSetRow As TravelBandSetRow In travelBandSetRows
                listItem = New ListItem(travelBandSetRow.ShortDescription, travelBandSetRow.TbsId)
                listItem.Attributes.Add("style", "background-color:" + System.Drawing.ColorTranslator.ToHtml(travelBandSetRow.StatusColor(currentDate)))
                rateSetDropDown.Items.Add(listItem)
            Next
            listItem = New ListItem(NewListItemText)
            listItem.Attributes.Add("style", "background-color:" + System.Drawing.ColorTranslator.ToHtml(ProductConstants.InactiveColor))
            rateSetDropDown.Items.Insert(0, listItem)

            Dim defaultTravelBandSet As TravelBandSetRow = Me.SaleableProductRow.CurrentTravelBandSetRow(currentDate)
            If defaultTravelBandSet IsNot Nothing Then
                _defaultTbsId = defaultTravelBandSet.TbsId
            ElseIf travelBandSetRows.Length > 0 Then
                _defaultTbsId = travelBandSetRows(travelBandSetRows.Length - 1).TbsId
            End If

            If selectedTbsId Is Nothing OrElse rateSetDropDown.Items.FindByValue(selectedTbsId) Is Nothing Then
                selectedTbsId = _defaultTbsId
            End If

            If selectedTbsId IsNot Nothing Then
                rateSetDropDown.SelectedValue = selectedTbsId
                _travelBandSetRow = Me.ProductDataSet.TravelBandSet.FindByTbsId(selectedTbsId)
            End If

            rateSetTableRow.Attributes.Add("style", rateSetDropDown.SelectedItem.Attributes("style"))
        Else
            rateSetDropDown.Items.Clear()
        End If

        rateSetCreateButton.Visible = Me.SaleableProductRow IsNot Nothing _
            AndAlso _travelBandSetRow Is Nothing _
            AndAlso CanMaintain
        rateSetUpdateButton.Visible = Me.SaleableProductRow IsNot Nothing _
            AndAlso _travelBandSetRow IsNot Nothing _
            AndAlso CanMaintain
        rateSetCopyButton.Visible = Me.SaleableProductRow IsNot Nothing _
            AndAlso _travelBandSetRow IsNot Nothing _
            AndAlso CanMaintain
        rateSetReportButton.Visible = Me.SaleableProductRow IsNot Nothing _
            AndAlso _travelBandSetRow IsNot Nothing

        If rateSetReportButton.Visible Then
            rateSetReportButton.OnClientClick = "window.open(""RateSetReport.aspx" & _
                "?sapId=" + Server.UrlEncode(Me.SaleableProductRow.SapId) & _
                "&tbsId=" + Server.UrlEncode(_travelBandSetRow.TbsId) & _
                """, ""rateBandSummary"", ""scrollbars=yes,width=700,height=550""); return false;"
        End If
    End Sub


    Private Sub InitRateBand()
        Dim listItem As ListItem

        If _travelBandSetRow IsNot Nothing Then
            DataRepository.GetTravelBandSet(Me.ProductDataSet, Me.CurrentPage.CompanyCode, _travelBandSetRow.TbsId)
            Dim productRateTravelBandRows As ProductRateTravelBandRow() = _travelBandSetRow.GetProductRateTravelBandRows()
            Array.Reverse(productRateTravelBandRows)

            Dim selectedPtbId As String = Me.Request.QueryString(PtbIdParam)
            If Me.IsPostBack Then
                selectedPtbId = rateBandDropDown.SelectedValue
            End If

            rateBandDropDown.Items.Clear()
            For Each productRateTravelBandRow As ProductRateTravelBandRow In productRateTravelBandRows
                listItem = New ListItem(productRateTravelBandRow.ShortDescription, productRateTravelBandRow.PtbId)
                listItem.Attributes.Add("style", "background-color:" + System.Drawing.ColorTranslator.ToHtml(productRateTravelBandRow.StatusColor(currentDate)))
                rateBandDropDown.Items.Add(listItem)
            Next
            If Aurora.Common.Utility.ParseDateTime(rateSetDropDown.SelectedItem.Text.Split("-")(0), Aurora.Common.UserSettings.Current.ComCulture) > Now Then
                listItem = New ListItem(NewListItemText)
                listItem.Attributes.Add("style", "background-color:" + System.Drawing.ColorTranslator.ToHtml(ProductConstants.InactiveColor))
                rateBandDropDown.Items.Insert(0, listItem)
            End If

            Dim defaultProductRateTravelBandRow As ProductRateTravelBandRow = _travelBandSetRow.CurrentProductRateTravelBandRow(currentDate)
            If defaultProductRateTravelBandRow IsNot Nothing Then
                _defaultPtdId = defaultProductRateTravelBandRow.PtbId
            ElseIf productRateTravelBandRows.Length > 0 Then
                _defaultPtdId = productRateTravelBandRows(productRateTravelBandRows.Length - 1).PtbId
            End If

            If selectedPtbId Is Nothing OrElse rateBandDropDown.Items.FindByValue(selectedPtbId) Is Nothing Then
                selectedPtbId = _defaultPtdId
            End If

            If selectedPtbId IsNot Nothing Then
                rateBandDropDown.SelectedValue = selectedPtbId
                _productRateTravelBandRow = Me.ProductDataSet.ProductRateTravelBand.FindByPtbId(selectedPtbId)
            End If
            If rateBandDropDown.Items.Count > 0 Then
                rateBandTableRow.Attributes.Add("style", rateBandDropDown.SelectedItem.Attributes("style"))
            End If
        Else
            rateBandDropDown.Items.Clear()
        End If

        rateBandCreateButton.Visible = Me._travelBandSetRow IsNot Nothing _
            AndAlso Me._productRateTravelBandRow Is Nothing _
            AndAlso CanMaintain
        rateBandTable.Style.Add("display", IIf(Me._travelBandSetRow IsNot Nothing, "", "none"))
    End Sub

    Private Function InitTableHeaderRow(ByVal table As Table, ByVal cssClass As String) As TableHeaderRow
        Dim result As New TableHeaderRow
        If Not String.IsNullOrEmpty(cssClass) Then result.CssClass = cssClass
        table.Rows.Add(result)
        Return result
    End Function

    Private Function InitTableHeaderCell(ByVal tableHeaderRow As TableHeaderRow, ByVal cssClass As String, ByVal columnSpan As Integer, ByVal text As String) As TableHeaderCell
        Dim result As New TableHeaderCell()
        If Not String.IsNullOrEmpty(cssClass) Then result.CssClass = cssClass
        If columnSpan <> 1 Then result.ColumnSpan = columnSpan
        If Not String.IsNullOrEmpty(text) Then result.Text = text
        tableHeaderRow.Controls.Add(result)
        Return result
    End Function

    Private Function InitTableRow(ByVal table As Table, ByVal cssClass As String) As TableRow
        Dim result As New TableRow
        If Not String.IsNullOrEmpty(cssClass) Then result.CssClass = cssClass
        table.Rows.Add(result)
        Return result
    End Function

    Private Function InitTableCell(ByVal tableRow As TableRow, ByVal cssStyle As String, ByVal columnSpan As Integer, ByVal text As String) As TableCell
        Dim result As New TableCell
        If Not String.IsNullOrEmpty(cssStyle) Then result.Attributes.Add("style", cssStyle)
        If columnSpan <> 1 Then result.ColumnSpan = columnSpan
        If Not String.IsNullOrEmpty(text) Then result.Text = text
        tableRow.Controls.Add(result)
        Return result
    End Function

 

    Private Sub InitDiscountCheckbox(ByVal tableCell As TableCell, ByVal productRateDiscountRow As ProductRateDiscountRow)
        If CanMaintain Then
            Dim checkBox As New CheckBox
            checkBox.ID = "discount" & productRateDiscountRow.PdsId & "CheckBox"
            tableCell.Controls.Add(checkBox)
            _checkBoxesByDiscountId.Add(productRateDiscountRow.PdsId, checkBox)
        Else
            tableCell.Controls.Add(New LiteralControl("&nbsp;"))
        End If
    End Sub

    'Private Sub InitVehicleDiscountCheckbox(ByVal tableCell As TableCell, ByVal vehicleDependentRateDiscountRow As VehicleDependentRateRow)
    '    If CanMaintain Then
    '        Dim checkBox As New CheckBox
    '        checkBox.ID = "rate" & vehicleDependentRateDiscountRow.VdrId & "CheckBox"
    '        'tableCell.Controls.Add(checkBox)
    '        If Not _checkBoxesByDiscountId.ContainsKey(vehicleDependentRateDiscountRow.VdrId) Then
    '            _checkBoxesByDiscountId.Add(vehicleDependentRateDiscountRow.VdrId, checkBox)
    '        End If
    '    Else
    '        '    tableCell.Controls.Add(New LiteralControl("&nbsp;"))
    '    End If
    'End Sub

 

    Private Sub InitAddLDRLinkButton(ByVal tableCell As TableCell, ByVal productRateBookingBandRow As ProductRateBookingBandRow)
        If CanMaintain Then
            Dim linkButton As New LinkButton
            linkButton.ID = "addLDR" & productRateBookingBandRow.Id & "LinkButton"
            linkButton.Text = "Add LDR"
            tableCell.Controls.Add(linkButton)
            _ratesByAddLDRLinkButtonId.Add(linkButton.ID, productRateBookingBandRow)
            AddHandler linkButton.Click, AddressOf addLDRButton_Click

            tableCell.Controls.Add(New LiteralControl(", "))
        End If
    End Sub

    Private Sub InitProductRateTravelBand_Rate(ByVal productRateTravelBandRow As ProductRateTravelBandRow, ByVal leftPadding As Integer, ByVal name As String, ByRef rowIndex As Integer, ByVal rate As IProductRate)

        Dim tableRow As TableRow = InitTableRow(productRateTravelBandTable, IIf((rowIndex Mod 2) = 0, "evenRow", "oddRow")) : rowIndex += 1
        InitRateCheckbox(InitTableCell(tableRow, "", 1, Nothing), rate)
        InitEditRateLinkButton(InitTableCell(tableRow, IIf(leftPadding <= 0, "", "padding-left: " & CStr(leftPadding) & "px"), 1, Nothing), rate, name)
        InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(rate.QtyDescription))
        InitTableCell(tableRow, "", 1, Server.HtmlEncode(rate.UomDescription))
        InitTableCell(tableRow, "text-align: right", 1, _
        IIf(rate.Rate = -999999999, String.Empty, Server.HtmlEncode(ProductConstants.RateToString(rate.Rate))) & "&nbsp;")
        InitTableCell(tableRow, "", 1, Server.HtmlEncode(rate.Description))

        Dim linkTableCell As TableCell = InitTableCell(tableRow, "text-align: right", 1, Nothing)
        If TypeOf rate Is ProductRateBookingBandRow Then InitAddLDRLinkButton(linkTableCell, rate)
        InitAddDiscountLinkButton(linkTableCell, rate)

        Dim productRateDiscounts As ProductRateDiscountRow() = productRateTravelBandRow.GetProductRateDiscountForParent(rate.Id)
        For Each productRateDiscountRow As ProductRateDiscountRow In productRateDiscounts
            tableRow = InitTableRow(productRateTravelBandTable, IIf((rowIndex Mod 2) = 0, "evenRow", "oddRow")) : rowIndex += 1
            InitDiscountCheckbox(InitTableCell(tableRow, "", 1, Nothing), productRateDiscountRow)
            InitEditDiscountLinkButton(InitTableCell(tableRow, "padding-left: " & CStr(leftPadding + 10) & "px", 1, Nothing), productRateDiscountRow)
            InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(productRateDiscountRow.QtyDescription))
            InitTableCell(tableRow, "", 1, Server.HtmlEncode(productRateDiscountRow.UomDescription))
            InitTableCell(tableRow, "text-align: right", 1, IIf(productRateDiscountRow.RateDescription = "$-999999999.00", String.Empty, Server.HtmlEncode(productRateDiscountRow.RateDescription)) & "&nbsp;")
            InitTableCell(tableRow, "", 1, Server.HtmlEncode(productRateDiscountRow.Description))
            InitTableCell(tableRow, "", 1, "&nbsp;")
        Next

        'Dim oVehicleRateDiscounts As VehicleDependentRateRow() = productRateTravelBandRow.GetVehicleDependentRateRows()
        'For Each oVehDepRateDiscount As VehicleDependentRateRow In oVehicleRateDiscounts
        '    'tableRow = InitTableRow(productRateTravelBandTable, IIf((rowIndex Mod 2) = 0, "evenRow", "oddRow")) : rowIndex += 1
        '    InitVehicleDiscountCheckbox(InitTableCell(tableRow, "", 1, Nothing), oVehDepRateDiscount)
        '    'InitEditDiscountLinkButton(InitTableCell(tableRow, "padding-left: " & CStr(leftPadding + 10) & "px", 1, Nothing), ProductRateDiscountRow)
        '    'InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(ProductRateDiscountRow.QtyDescription))
        '    'InitTableCell(tableRow, "", 1, Server.HtmlEncode(ProductRateDiscountRow.UomDescription))
        '    'InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(ProductRateDiscountRow.RateDescription) & "&nbsp;")
        '    'InitTableCell(tableRow, "", 1, Server.HtmlEncode(ProductRateDiscountRow.Description))
        '    'InitTableCell(tableRow, "", 1, "&nbsp;")
        'Next

    End Sub

    Private Sub InitRate(ByVal productRateTravelBandRow As ProductRateTravelBandRow, ByVal name As String, ByRef rowIndex As Integer, ByVal rates As IProductRate())
        If rates Is Nothing OrElse rates.Length < 1 Then Return

        For Each rate As IProductRate In rates
            InitProductRateTravelBand_Rate(productRateTravelBandRow, 0, name, rowIndex, rate)
        Next
    End Sub

    Private Sub InitRates()
        While productRateTravelBandTable.Rows.Count > 1
            productRateTravelBandTable.Rows.RemoveAt(1)
        End While

        If _productRateTravelBandRow IsNot Nothing Then
            DataRepository.GetRates(Me.ProductDataSet, Me.CurrentPage.CompanyCode, _productRateTravelBandRow.PtbTbsId, _productRateTravelBandRow.PtbId)

            Dim rowIndex As Integer = 0
            InitRate(_productRateTravelBandRow, "Base", rowIndex, New IProductRate() {_productRateTravelBandRow})
            InitRate(_productRateTravelBandRow, "Agent Dependent", rowIndex, _productRateTravelBandRow.GetAgentDependentRateRows())
            InitRate(_productRateTravelBandRow, "Vehicle Dependent", rowIndex, _productRateTravelBandRow.GetVehicleDependentRateRows())

            For Each productRateBookingBandRow As ProductRateBookingBandRow In _productRateTravelBandRow.GetProductRateBookingBandRows()
                InitProductRateTravelBand_Rate(_productRateTravelBandRow, 0, "Booked Date Availablity", rowIndex, productRateBookingBandRow)
                For Each locationDependentRateRow As LocationDependentRateRow In productRateBookingBandRow.GetLocationDependentRateRows()
                    InitProductRateTravelBand_Rate(_productRateTravelBandRow, 10, "Location Dependent", rowIndex, locationDependentRateRow)
                Next
            Next

            InitRate(_productRateTravelBandRow, "Person Dependent", rowIndex, _productRateTravelBandRow.GetPersonDependentRateRows())
            InitRate(_productRateTravelBandRow, "Fixed", rowIndex, _productRateTravelBandRow.GetFixedRateRows())
            InitRate(_productRateTravelBandRow, "Person", rowIndex, _productRateTravelBandRow.GetPersonRateRows())
        End If

        addRateButton.Visible = Me.SaleableProductRow IsNot Nothing _
            AndAlso _productRateTravelBandRow IsNot Nothing _
            AndAlso CanMaintain
        deleteRateButton.Visible = Me.SaleableProductRow IsNot Nothing _
            AndAlso _productRateTravelBandRow IsNot Nothing _
            AndAlso CanMaintain
        ratesTable.Style.Add("display", IIf(Me._travelBandSetRow IsNot Nothing AndAlso Me._productRateTravelBandRow IsNot Nothing, "", "none"))
    End Sub


    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)

        ratePopupControl.InitProductUserControl(productDataSet, saleableProductRow)
        discountPopupControl.InitProductUserControl(productDataSet, saleableProductRow)
        copyRateSetPopupControl.InitProductUserControl(productDataSet, saleableProductRow)
    End Sub


    Private Sub BuildRateSet(ByVal useViewState As Boolean)
        If _travelBandSetRow IsNot Nothing Then
            If Not useViewState Then
                rateSetEffectiveDateTextBox.Date = _travelBandSetRow.TbsEffDateTime
                rateSetEffectiveTimeTextBox.Time = _travelBandSetRow.TbsEffDateTime.TimeOfDay
                Try
                    rateSetUomDropDown.SelectedValue = _travelBandSetRow.TbsCodUomId
                Catch
                End Try
                rateSetQuantityTextBox.Text = _travelBandSetRow.TbsQty.ToString("#.####")
                rateSetPropogrationPendingCheckBox.Checked = Not _travelBandSetRow.IsTbsPropagationPendingNull AndAlso _travelBandSetRow.TbsPropagationPending
                rateSetIsFlexExpCheckBox.Checked = Not _travelBandSetRow.IsTbsIsFlexExpNull AndAlso _travelBandSetRow.TbsIsFlexExp
            End If
            rateSetAddModLabel.Text = Server.HtmlEncode(Aurora.Common.Data.GetAddModDescription(_travelBandSetRow))
            rateSetEffectiveDateTextBox.ReadOnly = True
            rateSetEffectiveTimeTextBox.ReadOnly = True
            rateSetUomDropDown.Enabled = CanMaintain
            rateSetQuantityTextBox.ReadOnly = Not CanMaintain
        Else
            If Not useViewState Then
                Dim n As Date = Date.Now.AddMinutes(60)
                rateSetEffectiveDateTextBox.Date = n
                rateSetEffectiveTimeTextBox.Time = n.TimeOfDay
                rateSetQuantityTextBox.Text = "1"
                rateSetIsFlexExpCheckBox.Checked = False
                rateSetPropogrationPendingCheckBox.Checked = False
                rateSetIsFlexExpCheckBox.Checked = False
            End If
            rateSetAddModLabel.Text = ""
            rateSetEffectiveDateTextBox.ReadOnly = Not CanMaintain
            rateSetEffectiveTimeTextBox.ReadOnly = Not CanMaintain
            rateSetUomDropDown.Enabled = CanMaintain
            rateSetQuantityTextBox.ReadOnly = Not CanMaintain
        End If
    End Sub

    Private Sub BuildRateBand(ByVal useViewState As Boolean)
        If _travelBandSetRow Is Nothing Then
            Return
        End If

        If _productRateTravelBandRow IsNot Nothing Then
            If Not useViewState Then
                rateBandTravelFromTextBox.Text = _productRateTravelBandRow.PtbTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                rateBandIsFlexExpCheckBox.Checked = Not _productRateTravelBandRow.IsPtbIsFlexExpNull AndAlso _productRateTravelBandRow.PtbIsFlexExp
            End If
            rateBandAddModLabel.Text = Server.HtmlEncode(Aurora.Common.Data.GetAddModDescription(_productRateTravelBandRow))

            ''rev:mia jan 15 2009
            rateBandTravelFromTextBox.ReadOnly = DateTime.Compare(rateBandTravelFromTextBox.Date, Now.Date) = -1
            ChangeAddBandRateText()

        Else
            If Not useViewState Then
                rateBandTravelFromTextBox.Text = Date.Now.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
                rateBandIsFlexExpCheckBox.Checked = False
            End If
            rateBandAddModLabel.Text = ""
            rateBandTravelFromTextBox.ReadOnly = Not CanMaintain
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.SaleableProductRow Is Nothing Then Return

        Try
            If Not IsPostBack Then
                InitLookups()
            End If

            InitRateSet()
            InitRateBand()
            InitRates()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Page.IsPostBack Then
            ChangeAddBandRateText()
            Return
        End If


        BuildRateSet(False)
        BuildRateBand(False)
    End Sub

    Protected Sub rateSetDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateSetDropDown.SelectedIndexChanged
        If _loadException IsNot Nothing Then Return

        BuildRateSet(False)
        BuildRateBand(False)

        ''rev:mia jan 15 2009
        ChangeAddBandRateText()
    End Sub

    Private Sub ValidateRateSet(ByVal create As Boolean)
        If create Then
            If rateSetEffectiveDateTextBox.Date = Date.MinValue OrElse rateSetEffectiveTimeTextBox.Time = TimeSpan.MinValue Then
                Throw New Aurora.Common.ValidationException("Rate Set Effective date/time must be valid")
            End If
        End If

        'If Not rateSetQuantityTextBox.IsTextValid Then
        '    Throw New Aurora.Common.ValidationException(rateSetQuantityTextBox.ErrorMessage)
        'End If

        If String.IsNullOrEmpty(rateSetQuantityTextBox.text) OrElse Not IsNumeric(rateSetQuantityTextBox.text) Then
            Throw New Aurora.Common.ValidationException("Rate Set Quantity must be a decimal value")
        End If

    End Sub

    Protected Sub rateSetCreateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateSetCreateButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateRateSet(True)
        Catch ex As Aurora.Common.ValidationException
            Me.CurrentPage.AddErrorMessage(ex.Message)
            BuildRateSet(True)
            BuildRateBand(True)
            Return
        End Try

        _travelBandSetRow = Me.ProductDataSet.TravelBandSet.NewTravelBandSetRow()
        _travelBandSetRow.TbsId = Aurora.Product.Data.DataRepository.GetNewId()
        _travelBandSetRow.TbsSapId = Me.SaleableProductRow.SapId
        _travelBandSetRow.TbsCodUomId = rateSetUomDropDown.SelectedValue
        _travelBandSetRow.TbsQty = Decimal.Parse(rateSetQuantityTextBox.Text)
        _travelBandSetRow.TbsEffDateTime = rateSetEffectiveDateTextBox.Date + rateSetEffectiveTimeTextBox.Time
        _travelBandSetRow.TbsPropagationPending = False
        _travelBandSetRow.SetTbsTbsCopiedFromIdNull()
        _travelBandSetRow.TbsIsFlexExp = False
        Me.ProductDataSet.TravelBandSet.AddTravelBandSetRow(_travelBandSetRow)

        Aurora.Common.Data.ExecuteDataRow(_travelBandSetRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        InitRateSet()
        BuildRateSet(False)
        BuildRateBand(False)
        Me.CurrentPage.AddInformationMessage("Rate Set created")
    End Sub

    Protected Sub rateSetUpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateSetUpdateButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateRateSet(False)
        Catch ex As Aurora.Common.ValidationException
            Me.CurrentPage.AddErrorMessage(ex.Message)
            BuildRateSet(True)
            BuildRateBand(True)
            Return
        End Try

        _travelBandSetRow.TbsCodUomId = rateSetUomDropDown.SelectedValue
        _travelBandSetRow.TbsQty = Decimal.Parse(rateSetQuantityTextBox.Text)

        Aurora.Common.Data.ExecuteDataRow(_travelBandSetRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)


        BuildRateSet(False)
        BuildRateBand(False)
        Me.CurrentPage.AddInformationMessage("Rate Set updated")
    End Sub

    Protected Sub rateSetCopyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateSetCopyButton.Click
        If Not CanMaintain OrElse _travelBandSetRow Is Nothing Then Return

        copyRateSetPopupControl.Show(_travelBandSetRow)
    End Sub

    Protected Sub rateBandDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateBandDropDown.SelectedIndexChanged
        If _loadException IsNot Nothing Then Return

        BuildRateSet(False)
        BuildRateBand(False)

        ''rev:mia jan 15 2009
        ChangeAddBandRateText
    End Sub

    Protected Sub rateBandCreateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rateBandCreateButton.Click
        If Not CanMaintain Then Return
        If Not IsvalidDate() Then Return
        ''rev:mia jan 15 2009
        If Me.rateBandCreateButton.Text.IndexOf("Save Band") > -1 Then
            rateBandEditSave()
            Exit Sub
        End If

        _productRateTravelBandRow = Me.ProductDataSet.ProductRateTravelBand.NewProductRateTravelBandRow()
        _productRateTravelBandRow.PtbId = Aurora.Product.Data.DataRepository.GetNewId()
        _productRateTravelBandRow.PtbTbsId = _travelBandSetRow.TbsId
        _productRateTravelBandRow.PtbTravelFromDate = rateBandTravelFromTextBox.Date
        _productRateTravelBandRow.ptbCodUomId = _travelBandSetRow.TbsCodUomId
        _productRateTravelBandRow.PtbBaseRate = 0
        _productRateTravelBandRow.PtbQty = 1
        _productRateTravelBandRow.SetPtbFlxIdNull()
        _productRateTravelBandRow.PtbIsFlexExp = False
        Me.ProductDataSet.ProductRateTravelBand.AddProductRateTravelBandRow(_productRateTravelBandRow)

        Aurora.Common.Data.ExecuteDataRow(_productRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=_productRateTravelBandRow.PtbTbsId, _
            ptbId:=_productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub addRateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addRateButton.Click
        If Not CanMaintain Then Return

        ratePopupControl.ShowNew(_productRateTravelBandRow, Nothing)
    End Sub

    Protected Sub addLDRButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not CanMaintain Then Return

        If Not (TypeOf sender Is LinkButton) Then Return
        Dim productRateBookingBandRow As ProductRateBookingBandRow = _ratesByAddLDRLinkButtonId(CType(sender, LinkButton).ID)
        If productRateBookingBandRow Is Nothing Then Return

        ratePopupControl.ShowNew(_productRateTravelBandRow, productRateBookingBandRow)
    End Sub

    Protected Sub deleteRateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteRateButton.Click
        If Not CanMaintain Then Return

        If _productRateTravelBandRow Is Nothing Then Return


        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                For Each rate As IProductRate In _productRateTravelBandRow.GetProductRates()
                    If _checkBoxesByRateId.ContainsKey(rate.Id) _
                     AndAlso _checkBoxesByRateId(rate.Id).Checked Then
                        Aurora.Product.Data.DataRepository.DeleteRate(_productRateTravelBandRow.PtbId, rate.Id)
                    Else
                        Dim productRateDiscounts As ProductRateDiscountRow() = _productRateTravelBandRow.GetProductRateDiscountForParent(rate.Id)

                        For Each productRateDiscountRow As ProductRateDiscountRow In Me.ProductDataSet.ProductRateDiscount

                            ' this bit checks for a deleted row 
                            Dim sRowId As String
                            Dim bSkipThisRow As Boolean = False
                            Try
                                sRowId = productRateDiscountRow.PdsId
                            Catch ex As System.Data.DeletedRowInaccessibleException
                                'If ex.Message = "Deleted row information cannot be accessed through the row." Then
                                '    bSkipThisRow = True
                                'Else
                                '    Throw ex
                                'End If
                                bSkipThisRow = True
                            End Try

                            If bSkipThisRow Then Continue For

                            If Not _checkBoxesByDiscountId.ContainsKey(productRateDiscountRow.PdsId) _
                             OrElse Not _checkBoxesByDiscountId(productRateDiscountRow.PdsId).Checked _
                              Then Continue For

                            productRateDiscountRow.Delete()

                            Aurora.Common.Data.ExecuteDataRow(productRateDiscountRow)

                            _checkBoxesByDiscountId(sRowId).Checked = False

                        Next
                    End If



                Next

                ' vehicle dependent
                For Each vehicleRateDiscountRow As VehicleDependentRateRow In ProductDataSet.VehicleDependentRate

                    Dim sRowId As String
                    Dim bSkipThisRow As Boolean = False
                    Try
                        sRowId = vehicleRateDiscountRow.VdrId
                    Catch ex As System.Data.DeletedRowInaccessibleException
                        'If ex.Message = "Deleted row information cannot be accessed through the row." Then
                        '    bSkipThisRow = True
                        'Else
                        '    Throw ex
                        'End If
                        bSkipThisRow = True
                    End Try

                    If bSkipThisRow Then Continue For

                    If Not _checkBoxesByRateId.ContainsKey(vehicleRateDiscountRow.VdrId) _
                    OrElse Not _checkBoxesByRateId(vehicleRateDiscountRow.VdrId).Checked _
                    Then Continue For
                    'Dim sRowId As String = vehicleRateDiscountRow.VdrId

                    ' check if its a VDR, if so delete any VDR note
                    If vehicleRateDiscountRow.GetType.Equals(GetType(VehicleDependentRateRow)) _
                    AndAlso Not CType(vehicleRateDiscountRow, VehicleDependentRateRow).IsNteIdNull Then
                        Dim sRetMsg As String
                        sRetMsg = Aurora.Product.Data.DeleteVehicleDependentNote(CType(vehicleRateDiscountRow, VehicleDependentRateRow).NteId)
                        If sRetMsg.ToUpper().Trim().IndexOf("SUCCESS") < 0 Then
                            Throw New Exception("Unable to delete Vehicle Dependent Note")
                        End If
                    End If


                    vehicleRateDiscountRow.Delete()

                    Aurora.Common.Data.ExecuteDataRow(vehicleRateDiscountRow)
                    _checkBoxesByRateId(sRowId).Checked = False

                Next

                ' update "moddatetime" the base product rate band
                _productRateTravelBandRow.ModDateTime = Date.Now
                Aurora.Common.Data.ExecuteDataRowUpdateVehicleDependentRate(_productRateTravelBandRow, Me.CurrentPage.UserCode)

                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Me.CurrentPage.SetErrorMessage(ex.Message)
                Exit Sub
            End Try

        End Using




        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=_productRateTravelBandRow.PtbTbsId, _
            ptbId:=_productRateTravelBandRow.PtbId))
    End Sub

    Protected Sub editRateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not (TypeOf sender Is LinkButton) Then Return
        Dim productRate As IProductRate = _ratesByLinkButtonId(CType(sender, LinkButton).ID)
        If productRate Is Nothing Then Return

        ratePopupControl.ShowExisting(productRate)
    End Sub

    Protected Sub addDiscountButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not CanMaintain Then Return

        If Not (TypeOf sender Is LinkButton) Then Return
        Dim productRate As IProductRate = _ratesByAddDiscountLinkButtonId(CType(sender, LinkButton).ID)
        If productRate Is Nothing Then Return

        discountPopupControl.ShowNew(_productRateTravelBandRow, productRate)
    End Sub

    Protected Sub editDiscountButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not (TypeOf sender Is LinkButton) Then Return
        Dim productRateDiscount As ProductRateDiscountRow = _discountByLinkButtonId(CType(sender, LinkButton).ID)
        If productRateDiscount Is Nothing Then Return

        Dim productRate As IProductRate = _ratesByAddDiscountLinkButtonId("addDiscount" & productRateDiscount.PdsParentId & "LinkButton")

        discountPopupControl.ShowExisting(_productRateTravelBandRow, productRateDiscount, productRate)
    End Sub

#Region "rev:mia december 23 - fixes for duplicate control names"
    Private Sub InitRateCheckbox(ByVal tableCell As TableCell, ByVal productRate As IProductRate)
        If productRate IsNot _productRateTravelBandRow Then
            Dim checkBox As New CheckBox

            ''rev:mia dec23 trapped duplicate controls
            If Not _checkBoxesByRateId.ContainsKey(productRate.Id) Then
                checkBox.ID = "rate" & productRate.Id & "CheckBox"
                _checkBoxesByRateId.Add(productRate.Id, checkBox)
                tableCell.Controls.Add(checkBox)
            Else
                Dim tempKey As String = "rate" & productRate.Id & RandomSuffixes() & "CheckBox"
                checkBox.ID = tempKey
                _checkBoxesByRateId.Add(tempKey, checkBox)
                tableCell.Controls.Add(checkBox)
                ''System.Diagnostics.Debug.WriteLine("InitRateCheckbox: " & productRate.Id & " --> Found? " & _checkBoxesByRateId.ContainsKey(productRate.Id) & " -- > newkey :" & tempKey)
            End If

        Else
            tableCell.Controls.Add(New LiteralControl("&nbsp;"))
        End If
    End Sub

    Private Sub InitEditRateLinkButton(ByVal tableCell As TableCell, ByVal productRate As IProductRate, ByVal name As String)
        If CanMaintain Then
            Dim linkButton As New LinkButton
            linkButton.Text = Server.HtmlEncode(name).Replace(" ", "&nbsp;")
            linkButton.ID = "editRate" & productRate.Id & "LinkButton"

            ''rev:mia dec23 trapped duplicate controls
            If Not _ratesByLinkButtonId.ContainsKey(linkButton.ID) Then
                _ratesByLinkButtonId.Add(linkButton.ID, productRate)
                tableCell.Controls.Add(linkButton)
                AddHandler linkButton.Click, AddressOf editRateButton_Click
            Else
                Dim tempKey As String = "editRate" & productRate.Id & RandomSuffixes() & "LinkButton"
                linkButton.ID = tempKey
                tableCell.Controls.Add(linkButton)
                _ratesByLinkButtonId.Add(tempKey, productRate)
                '' System.Diagnostics.Debug.WriteLine("InitEditRateLinkButton: " & productRate.Id & " --> Found? " & _ratesByLinkButtonId.ContainsKey(linkButton.ID) & " -- > newkey :" & tempKey)
                AddHandler linkButton.Click, AddressOf editRateButton_Click
            End If


        Else
            tableCell.Controls.Add(New LiteralControl(Server.HtmlEncode(Server.HtmlEncode(name).Replace(" ", "&nbsp;"))))
        End If
    End Sub

    Private Sub InitEditDiscountLinkButton(ByVal tableCell As TableCell, ByVal productRateDiscountRow As ProductRateDiscountRow)
        If CanMaintain Then
            Dim linkButton As New LinkButton
            linkButton.Text = Server.HtmlEncode(productRateDiscountRow.TypeDescription)
            linkButton.ID = "editDiscount" & productRateDiscountRow.PdsId & "LinkButton"


            ''rev:mia dec23 trapped duplicate controls
            If Not _discountByLinkButtonId.ContainsKey(linkButton.ID) Then
                _discountByLinkButtonId.Add(linkButton.ID, productRateDiscountRow)
                tableCell.Controls.Add(linkButton)
                AddHandler linkButton.Click, AddressOf editDiscountButton_Click
            Else
                Dim tempKey As String = "editDiscount" & productRateDiscountRow.PdsId & RandomSuffixes() & "LinkButton"
                linkButton.ID = tempKey
                _discountByLinkButtonId.Add(linkButton.ID, productRateDiscountRow)
                tableCell.Controls.Add(linkButton)
                AddHandler linkButton.Click, AddressOf editDiscountButton_Click
                ''System.Diagnostics.Debug.WriteLine("InitEditDiscountLinkButton: " & productRateDiscountRow.PdsId & " --> Found? " & _ratesByLinkButtonId.ContainsKey(linkButton.ID) & " -- > newkey :" & tempKey)
            End If

        Else
            tableCell.Controls.Add(New LiteralControl(Server.HtmlEncode(productRateDiscountRow.TypeDescription)))
        End If
    End Sub

    Private Sub InitAddDiscountLinkButton(ByVal tableCell As TableCell, ByVal productRate As IProductRate)
        If CanMaintain Then
            Dim linkButton As New LinkButton
            linkButton.Text = "Add Discount"
            linkButton.ID = "addDiscount" & productRate.Id & "LinkButton"

            ''rev:mia dec23 trapped duplicate controls
            If Not _ratesByAddDiscountLinkButtonId.ContainsKey(linkButton.ID) Then
                _ratesByAddDiscountLinkButtonId.Add(linkButton.ID, productRate)
                tableCell.Controls.Add(linkButton)
                AddHandler linkButton.Click, AddressOf addDiscountButton_Click
            Else
                Dim tempKey As String = "addDiscount" & productRate.Id & RandomSuffixes() & "LinkButton"
                linkButton.ID = tempKey
                _ratesByAddDiscountLinkButtonId.Add(linkButton.ID, productRate)
                tableCell.Controls.Add(linkButton)
                AddHandler linkButton.Click, AddressOf addDiscountButton_Click
                ''System.Diagnostics.Debug.WriteLine("InitAddDiscountLinkButton: " & productRate.Id & " --> Found? " & _ratesByAddDiscountLinkButtonId.ContainsKey(linkButton.ID) & " -- > newkey :" & tempKey)
            End If

        Else
            tableCell.Controls.Add(New LiteralControl("&nbsp;"))
        End If
    End Sub

    Function RandomSuffixes() As String
        Dim objRandom As New Random
        Dim myrandomNumber As String = objRandom.Next()
        Return "_" & myrandomNumber
    End Function


#End Region

#Region "rev:mia jan 15 2009 adding update functionalities in creating band rate"
    Private Sub rateBandEditSave()
        Dim TempproductRateTravelBandRow As ProductRateTravelBandRow = Me.ProductDataSet.ProductRateTravelBand.FindByPtbId(Me.rateBandDropDown.SelectedValue)
        TempproductRateTravelBandRow.PtbTravelFromDate = rateBandTravelFromTextBox.Date
        If TempproductRateTravelBandRow Is Nothing Then Return
        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(TempproductRateTravelBandRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=TempproductRateTravelBandRow.PtbTbsId, _
            ptbId:=TempproductRateTravelBandRow.PtbId))
    End Sub

    Sub ChangeAddBandRateText()
        If rateBandDropDown.SelectedValue = "" Then
            Me.rateBandCreateButton.Visible = True
            Exit Sub
        End If
        If Me.rateBandDropDown.Items.Count - 1 = 0 Then
            If Me.rateBandDropDown.Text.IndexOf("new") > -1 Then
                Me.rateBandCreateButton.Text = "Add Band"
                Me.rateBandCreateButton.CssClass = "Button_Standard Button_New"
                Exit Sub
            End If
        End If
        Me.rateBandCreateButton.Visible = Not rateBandTravelFromTextBox.ReadOnly

        If rateBandDropDown.SelectedValue.IndexOf("new") = -1 Then
            Me.rateBandCreateButton.Text = "Save Band"
            Me.rateBandCreateButton.CssClass = "Button_Standard Button_OK"
        Else
            Me.rateBandCreateButton.Text = "Add Band"
            Me.rateBandCreateButton.CssClass = "Button_Standard Button_New"
        End If
    End Sub

    Function IsvalidDate() As Boolean
        Try
            If String.IsNullOrEmpty(rateBandTravelFromTextBox.Text) Then
                MyBase.CurrentPage.SetWarningShortMessage("Enter Rate Band Date")
                Return False
            End If
            ''Dim tempDate As String = Aurora.Common.Utility.DateUIToString(rateBandTravelFromTextBox.Date)
            Dim temp As String = Convert.ToDateTime(rateBandTravelFromTextBox.Text)
        Catch ex As Exception
            MyBase.CurrentPage.SetWarningShortMessage("Rate Band Date is invalid")
            Return False
        End Try
        Return True
    End Function
#End Region

End Class
