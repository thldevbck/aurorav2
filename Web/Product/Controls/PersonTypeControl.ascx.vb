Imports System.Collections.Generic

Imports Aurora.Common

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet


<ToolboxData("<{0}:PersonTypeControl runat=server></{0}:PersonTypeControl>")> _
Partial Class Product_PersonTypeControl
    Inherits ProductUserControl

    Private _loadException As Exception
    Private _personTypeIdHiddenFields As New Dictionary(Of String, HiddenField)
    Private _personTypeAmtTextboxes As New Dictionary(Of String, UserControls_RegExTextBox)
    Private _personTypePercentTextboxes As New Dictionary(Of String, UserControls_RegExTextBox)

    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)
    End Sub

    Public Sub InitPersonTypes()
        If Me.SaleableProductRow Is Nothing Then Return

        Dim rowIndex As Integer = 0
        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum <> codeRow.CodeType_Person_Rate Then Continue For

            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "evenRow", "oddRow") : rowIndex += 1
            personTypeTable.Rows.Add(tableRow)

            Dim typeTableCell As New TableCell
            typeTableCell.CssClass = "tdType"
            tableRow.Controls.Add(typeTableCell)

            typeTableCell.Controls.Add(New LiteralControl(Server.HtmlEncode(codeRow.CodDesc)))

            Dim idHiddenField As New HiddenField()
            idHiddenField.ID = codeRow.CodCode + "IdHiddenField"
            typeTableCell.Controls.Add(idHiddenField)
            _personTypeIdHiddenFields.Add(codeRow.CodId, idHiddenField)

            Dim amountTableCell As New TableCell
            amountTableCell.CssClass = "tdAmount"
            tableRow.Controls.Add(amountTableCell)

            Dim amountTextBox As UserControls_RegExTextBox = CType(LoadControl("~\UserControls\RegExTextBox\RegExTextBox.ascx"), UserControls_RegExTextBox)
            amountTextBox.ID = codeRow.CodCode + "AmountTextBox"
            amountTableCell.Controls.Add(amountTextBox)
            _personTypeAmtTextboxes.Add(codeRow.CodId, amountTextBox)
            amountTextBox.MaxLength = 10
            amountTextBox.ErrorMessage = "Person Type Discount Amount must be a decimal greater than zero"
            amountTextBox.Nullable = True
            amountTextBox.RegExPattern = "(?!^0*$)(?!^0*\.0*$)^\d+(\.\d+)?$"
            amountTextBox.Width = Unit.Parse("80px")

            Dim percentTableCell As New TableCell
            percentTableCell.CssClass = "tdAmount"
            tableRow.Controls.Add(percentTableCell)

            Dim percentTextBox As UserControls_RegExTextBox = CType(LoadControl("~\UserControls\RegExTextBox\RegExTextBox.ascx"), UserControls_RegExTextBox)
            percentTextBox.ID = codeRow.CodCode + "PercentTextBox"
            percentTableCell.Controls.Add(percentTextBox)
            _personTypePercentTextboxes.Add(codeRow.CodId, percentTextBox)
            percentTextBox.MaxLength = 10
            percentTextBox.ErrorMessage = "Person Type Discount Percent must be a decimal greater than zero"
            percentTextBox.Nullable = True
            percentTextBox.RegExPattern = "(?!^0*$)(?!^0*\.0*$)^\d+(\.\d+)?$"
            percentTextBox.Width = Unit.Parse("80px")

            Dim linkTableCell As New TableCell
            linkTableCell.CssClass = "tdLink"
            tableRow.Controls.Add(linkTableCell)
        Next
    End Sub

    Public Sub BuildPersonTypes(ByVal useViewState As Boolean)
        If Me.SaleableProductRow Is Nothing Then Return
        If useViewState Then Return

        For Each codeRow As CodeRow In Me.ProductDataSet.Code
            If codeRow.CodCdtNum <> codeRow.CodeType_Person_Rate Then Continue For

            Dim idHiddenField As HiddenField = _personTypeIdHiddenFields(codeRow.CodId)
            idHiddenField.Value = ""

            Dim amountTextBox As UserControls_RegExTextBox = _personTypeAmtTextboxes(codeRow.CodId)
            amountTextBox.Text = ""
            amountTextBox.ReadOnly = Not CanMaintain

            Dim percentTextBox As UserControls_RegExTextBox = _personTypePercentTextboxes(codeRow.CodId)
            percentTextBox.Text = ""
            percentTextBox.ReadOnly = Not CanMaintain

            For Each personTypeDiscountRow As PersonTypeDiscountRow In Me.SaleableProductRow.GetPersonTypeDiscountRows()
                If personTypeDiscountRow.PtdCodPtyId <> codeRow.CodId Then Continue For

                idHiddenField.Value = personTypeDiscountRow.PtdId
                If Not personTypeDiscountRow.IsPtdAmtNull Then amountTextBox.Text = personTypeDiscountRow.PtdAmt.ToString("#.##")
                If Not personTypeDiscountRow.IsPtdPercentageNull Then percentTextBox.Text = personTypeDiscountRow.PtdPercentage.ToString("#.##")
            Next
        Next

        updateButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.SaleableProductRow Is Nothing Then Return

        Try
            InitPersonTypes()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Page.IsPostBack Then Return

        BuildPersonTypes(False)
    End Sub

    Private Sub GetAmountPercent(ByVal codId As String, ByRef amount As Decimal, ByRef percent As Decimal)
        Dim amountTextBox As UserControls_RegExTextBox = _personTypeAmtTextboxes(codId)
        Dim percentTextBox As UserControls_RegExTextBox = _personTypePercentTextboxes(codId)

        amount = 0
        Try
            If Not String.IsNullOrEmpty(amountTextBox.Text.Trim()) Then amount = Decimal.Parse(amountTextBox.Text)
            If amount < 0 Then Throw New Exception()
        Catch
            amount = 0
        End Try

        percent = 0
        Try
            If Not String.IsNullOrEmpty(percentTextBox.Text.Trim()) Then percent = Decimal.Parse(percentTextBox.Text)
            If percent < 0 Then Throw New Exception()
        Catch
            percent = 0
        End Try
    End Sub

    Private Sub SetAmountPercent(ByVal personTypeDiscountRow As PersonTypeDiscountRow, ByRef amount As Decimal, ByRef percent As Decimal)
        If amount <> 0 Then
            personTypeDiscountRow.PtdAmt = amount
        Else
            personTypeDiscountRow.SetPtdAmtNull()
        End If

        If percent <> 0 Then
            personTypeDiscountRow.PtdPercentage = percent
        Else
            personTypeDiscountRow.SetPtdPercentageNull()
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        Try
            ' Validate the individual rows
            For Each codeRow As CodeRow In Me.ProductDataSet.Code
                If codeRow.CodCdtNum <> codeRow.CodeType_Person_Rate Then Continue For

                Dim amountTextBox As UserControls_RegExTextBox = _personTypeAmtTextboxes(codeRow.CodId)
                If Not amountTextBox.IsTextValid Then Throw New ValidationException(amountTextBox.ErrorMessage)

                Dim percentTextBox As UserControls_RegExTextBox = _personTypePercentTextboxes(codeRow.CodId)
                If Not percentTextBox.IsTextValid Then Throw New ValidationException(percentTextBox.ErrorMessage)
            Next
        Catch ex As ValidationException
            Me.CurrentPage.AddErrorMessage(ex.Message)
            Return
        End Try

        Try
            ' Delete if not found or update any PersonTypeDiscountRows
            For Each personTypeDiscountRow As PersonTypeDiscountRow In Me.SaleableProductRow.GetPersonTypeDiscountRows()
                Dim found As Boolean = False
                For Each codeRow As CodeRow In Me.ProductDataSet.Code
                    If codeRow.CodCdtNum <> codeRow.CodeType_Person_Rate Then Continue For

                    Dim idHiddenField As HiddenField = _personTypeIdHiddenFields(codeRow.CodId)
                    Dim amount As Decimal = 0
                    Dim percent As Decimal = 0
                    GetAmountPercent(codeRow.CodId, amount, percent)

                    If idHiddenField.Value = personTypeDiscountRow.PtdId And Not (amount = 0 AndAlso percent = 0) Then
                        SetAmountPercent(personTypeDiscountRow, amount, percent)
                        found = True
                        Exit For
                    End If
                Next

                If Not found Then
                    personTypeDiscountRow.Delete()
                End If
                Aurora.Common.Data.ExecuteDataRow(personTypeDiscountRow)
            Next

            ' create new PersonTypeDiscountRows
            For Each codeRow As CodeRow In Me.ProductDataSet.Code
                If codeRow.CodCdtNum <> codeRow.CodeType_Person_Rate Then Continue For

                Dim idHiddenField As HiddenField = _personTypeIdHiddenFields(codeRow.CodId)
                Dim amount As Decimal = 0
                Dim percent As Decimal = 0
                GetAmountPercent(codeRow.CodId, amount, percent)

                If Not String.IsNullOrEmpty(idHiddenField.Value) OrElse (amount = 0 AndAlso percent = 0) Then Continue For

                Dim personTypeDiscountRow As PersonTypeDiscountRow = Me.ProductDataSet.PersonTypeDiscount.NewPersonTypeDiscountRow()
                personTypeDiscountRow.PtdId = DataRepository.GetNewId()
                personTypeDiscountRow.PtdSapId = Me.SaleableProductRow.SapId
                personTypeDiscountRow.PtdCodPtyId = codeRow.CodId
                SetAmountPercent(personTypeDiscountRow, amount, percent)
                Me.ProductDataSet.PersonTypeDiscount.AddPersonTypeDiscountRow(personTypeDiscountRow)

                Aurora.Common.Data.ExecuteDataRow(personTypeDiscountRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)

                idHiddenField.Value = personTypeDiscountRow.PtdId
            Next

        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error updating Person Type Discounts")
            Return
        End Try

        Me.CurrentPage.AddInformationMessage("Person Type Discounts updated")
    End Sub
End Class
