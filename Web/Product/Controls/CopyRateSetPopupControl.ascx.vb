Imports System.Collections.Generic

Imports System.Data

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<ToolboxData("<{0}:RatePopupControl runat=server></{0}:RatePopupControl>")> _
Partial Class Product_CopyRateSetPopupControl
    Inherits ProductUserControl

    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)
    End Sub

    Private Sub InitLookups()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.SaleableProductRow Is Nothing Then Return

        Try
            InitLookups()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Me.IsPostBack Then Return

        cancelButton.Attributes.Add("onclick", "$find('copyRateSetPopupBehavior').hide(); return false;")
    End Sub

    Public Sub Show(ByVal travelBandSetRow As TravelBandSetRow)
        idHidden.Value = travelBandSetRow.TbsId

        Dim n As Date = Date.Now.AddMinutes(60)
        rateSetEffectiveDateTextBox.Text = n.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
        rateSetEffectiveTimeTextBox.Text = n.ToString("HH:mm")

        copyRateSetPopup.Show()
    End Sub

    Protected Sub copyButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles copyButton.Click

        If rateSetEffectiveDateTextBox.Date = Date.MinValue OrElse rateSetEffectiveTimeTextBox.Time = TimeSpan.MinValue Then
            copyRateSetMessagePanelControl.SetMessagePanelWarning("Effective date/time must be valid")
            copyRateSetPopup.Show()
            Return
        End If


        Dim newTbsId As String = Nothing
        Try
            newTbsId = DataRepository.CopyTravelBandSet( _
                Me.CurrentPage.CompanyCode, _
                idHidden.Value, _
                rateSetEffectiveDateTextBox.Date + rateSetEffectiveTimeTextBox.Time, _
                Me.CurrentPage.UserCode, _
                Me.CurrentPage.PrgmName)
            If String.IsNullOrEmpty(newTbsId) Then Throw New Exception()
        Catch ex As Exception
            copyRateSetMessagePanelControl.SetMessagePanelWarning("Error copying the Rate Set")
            copyRateSetPopup.Show()
            Return
        End Try

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
            prdId:=Me.SaleableProductRow.SapPrdId, _
            sapId:=Me.SaleableProductRow.SapId, _
            tabName:="Rates", _
            tbsId:=newTbsId))
    End Sub


    Protected Sub ratePopupCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        copyRateSetPopup.Hide()
    End Sub

End Class
