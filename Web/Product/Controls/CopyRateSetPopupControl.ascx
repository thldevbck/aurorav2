<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CopyRateSetPopupControl.ascx.vb" Inherits="Product_CopyRateSetPopupControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>

<asp:Button runat="server" ID="copyRateSetPopupButton" Style="display: none" />
<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="copyRateSetPopup" 
    BehaviorID="copyRateSetPopupBehavior"
    TargetControlID="copyRateSetPopupButton" 
    PopupControlID="copyRateSetPopupPanel"
    BackgroundCssClass="modalBackground" 
    DropShadow="True" 
    PopupDragHandleControlID="copyRateSetPopupDragPanel" />

<asp:Panel runat="server" CssClass="modalPopup" ID="copyRateSetPopupPanel" Style="display: none; padding: 10px;" Width="500px" Height="150px">
    <asp:Panel runat="Server" ID="copyRateSetPopupDragPanel" CssClass="modalPopupTitle" >
        <asp:Label ID="copyRateSetPopupTitleLabel" runat="Server" Text="Copy Rate Set" />
    </asp:Panel>

    <asp:HiddenField ID="idHidden" runat="server" />

    <div style="width: 475px; height:35px; padding: 2px 0 2px 0;">
        <uc1:MessagePanelControl runat="server" ID="copyRateSetMessagePanelControl" CssClass="popupMessagePanel" />
    </div>
    
    <div style="width: 100%; height: 50px">
        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td style="width: 150px">New Effective Date/Time:</td>
                <td>
                    <uc1:DateControl ID="rateSetEffectiveDateTextBox" runat="server" Width="100px" Nullable="false" />
                    <uc1:TimeControl ID="rateSetEffectiveTimeTextBox" runat="server" Width="80px" Nullable="false" />
                </td>
            </tr>
        </table>
    </div>
    
    <table cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="copyButton" runat="Server" Text="Copy" CssClass="Button_Standard Button_OK" />
                <asp:Button ID="cancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
            </td>
        </tr>
    </table>
</asp:Panel>
