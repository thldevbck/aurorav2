﻿Imports System.Collections.Generic
Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet
Imports System.Data.SqlClient
Imports System.Data
Imports System.Xml
Imports System.Transactions
Imports Aurora.Common

<ToolboxData("<{0}:InclusiveProductsControl runat=server></{0}:InclusiveProductsControl>")> _
Partial Class Product_InclusiveProductsControl
    Inherits ProductUserControl

#Region "Local Variable declaration"

    Private _loadException As Exception
    Private _checkBoxesByProduct As New Dictionary(Of String, CheckBox)
    Private _checkBoxesVehicles As New Dictionary(Of String, CheckBox)
    Private _vehiclesArraylist As New ArrayList
    Private dsSaleableProducts As New DataSet
    Private dsInclProducts As DataSet
    Private dsNewAddtions As DataSet
    Private sbLogText As StringBuilder

#End Region

#Region "Event Handlers"

    ''' <summary>
    ''' Form load event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then Return
        If Me.SaleableProductRow Is Nothing Then Return
        Try
            
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Me.IsPostBack Then Return
    End Sub

    ''' <summary>
    ''' Event handler to handle the button click event that triggers the Inclusive Products popup screen to pop up
    ''' that allows the user to select inclusive products and associated vehicle products
    ''' by calling the 'showInclusiveProductsPopupScreen()' method
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub addInclusiveProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addInclusiveProductButton.Click
        If Not CanMaintain Then Return
        Try
            showInclusiveProductsPopupScreen()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Error Show Inclusive Product: " & ex.Message)
        End Try

    End Sub

    ''' <summary>
    ''' Remove inclusive products from an existing package
    ''' The changes are only reflected in the UI and are not saved to database until 'Save' button is clicked
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub removeInclusiveProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeInclusiveProductButton.Click
        If Not CanMaintain Then Return
        Try
            removeInclusiveProductfromPackageListDB()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Remove Inclusive Product: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler to Add inclusive product to the existing package after the user clicks
    ''' the 'Ok' button on the Inclusive products popup screen following product/vehicle selection
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub inclusiveProductPopupOKButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles inclusiveProductPopupOKButton.Click
        If Not CanMaintain Then Return
        Try
            addInclusiveProductToPackageList()
            addInclusiveProductToPackageListDb()
            'redirect()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Add Inclusive Product to Package: " & ex.Message)
        End Try
    End Sub

    Protected Sub addInclusiveProductToPackageList()
        If (inclusiveProductPopupPicker.DataId.ToString().Trim() = "") Then
            inclusiveProductPopup.Show()
            Return
        End If

        dsInclProducts = ViewState("dsInclProducts")

        'If (ViewState("datasetIsDirtyFlag") Is Nothing) Then
        '    ViewState("datasetIsDirtyFlag") = "1"
        'End If

        'If (ViewState("isNewItem") Is Nothing Or ViewState("isNewItem") = False) Then
        '    addPickupDateButton.Enabled = False
        'End If
        'If (ViewState("isNewItem") = True) Then
        '    addPickupDateButton.Enabled = True
        'End If


        'If (ViewState("dsInclProducts") Is Nothing) Then
        '    dsInclProducts = New DataSet
        '    Try
        '        'for a new product check if the ckoFrmdate is nothing - start here
        '        dsInclProducts = Aurora.SalesAndMarketing.Data.DataRepository.getInclusiveProducts(dsInclProducts, Convert.ToInt64(ckoFromDropdown.SelectedValue))
        '    Catch ex As Exception
        '        sbLogText = New StringBuilder()
        '        sbLogText.Length = 0
        '        sbLogText.AppendLine("Method : getInclusiveProducts")
        '        sbLogText.AppendLine("Message :" & ex.Message)
        '        sbLogText.AppendLine("Trace :" & ex.StackTrace)
        '        Logging.LogError("Exception", sbLogText.ToString())
        '    End Try

        '    'conv dataset here
        '    If (dsInclProducts.Tables.Count > 0) Then
        '        Dim temp As DataSet = unFlattenDataset(dsInclProducts)
        '        dsInclProducts = temp
        '        ViewState("dsInclProducts") = temp
        '    End If
        '    If (dsInclProducts.Tables.Count = 0) Then
        '        If (ViewState("dsInclProducts") Is Nothing) Then
        '            createInclusiveProductSchema()
        '        End If
        '    End If
        'Else
        '    dsInclProducts = ViewState("dsInclProducts")
        'End If


        Dim checkedExc As Boolean = False
        Dim vehicles As String = ""
        Dim vehIDs As String = ""

        'Case- Gridview has no rows implying no vehicle was selected for the inclusive products
        'which further implies it is associated with ALL the vehicles

        If (GridView1.Rows.Count = 0) Then
            vehicles = "ALL"
            Dim blninExisting As Boolean = checkDuplicatesExisting(inclusiveProductPopupPicker.DataId, vehicles)
            Dim blninNew As Boolean = checkDuplicatesNew(inclusiveProductPopupPicker.DataId, vehicles)

            If (blninExisting = False And blninNew = False) Then

                If (ViewState("dsNewAdditions") Is Nothing) Then
                    dsNewAddtions = New DataSet

                    'error May 13
                    If (dsInclProducts Is Nothing) Then
                        createInclusiveProductSchema()
                    End If

                    dsNewAddtions = dsInclProducts.Clone()
                Else
                    dsNewAddtions = ViewState("dsNewAdditions")
                End If


                Dim foundRows As DataRow()
                Dim strExpr As String = "prdid = '" & inclusiveProductPopupPicker.DataId & "'"
                foundRows = dsNewAddtions.Tables(0).Select(strExpr)

                If (foundRows.Length > 0) Then
                    Dim drEdit As DataRow = foundRows(0)
                    drEdit.BeginEdit()
                    drEdit(1) = "ALL"


                    '
                    'drEdit(4) = "Yes"
                    '

                    drEdit(7) = inclusiveProductPopupPicker.DataId
                    drEdit(8) = "ALL"
                    drEdit.EndEdit()

                Else
                    If (ViewState("incr") Is Nothing) Then
                        ViewState("incr") = -1
                    Else
                        ViewState("incr") = ViewState("incr") - 1
                    End If

                    Dim newIncr As Integer = Convert.ToInt32(ViewState("incr"))

                    Dim dr As DataRow
                    dr = dsNewAddtions.Tables(0).NewRow()
                    dr(0) = inclusiveProductPopupPicker.Text
                    dr(1) = vehicles
                    dr(2) = Convert.ToDateTime(Convert.ToDateTime(ckoFromDropdown.SelectedItem.ToString()))
                    dr(3) = Convert.ToDateTime(Convert.ToDateTime(ckoToTextbox.Text))
                    dr(4) = "Yes"
                    dr(5) = newIncr
                    dr(6) = True
                    dr(7) = inclusiveProductPopupPicker.DataId
                    dr(8) = "ALL"
                    dsNewAddtions.Tables(0).Rows.Add(dr)

                End If

                dsNewAddtions.AcceptChanges()
                ViewState("dsNewAdditions") = dsNewAddtions
            End If
        End If

        'Case- Gridview has rows implying vehicle/s were selected for the inclusive products
        'which further implies the product is associated with the selected vehicles

        If (GridView1.Rows.Count > 0) Then
            Dim lb As Label = CType(GridView1.Rows(0).FindControl("lblVehicle"), Label)
            Dim lbVehID As Label = CType(GridView1.Rows(0).FindControl("vehID"), Label)
            Dim cb1 As CheckBox = CType(GridView1.Rows(0).FindControl("excludeVehicle"), CheckBox)
            Dim inc As Integer = 0

            Dim blninExisting As Boolean = checkDuplicatesExisting(inclusiveProductPopupPicker.DataId, lbVehID.Text)
            Dim blninNew As Boolean = checkDuplicatesNew(inclusiveProductPopupPicker.DataId, lbVehID.Text)


            If (blninExisting = False And blninNew = False) Then
                vehicles = lb.Text
                vehIDs = lbVehID.Text
            End If


            If (cb1.Checked = True) Then
                checkedExc = True
            End If

            If (GridView1.Rows.Count > 1) Then
                For inc = 1 To GridView1.Rows.Count - 1
                    lb = CType(GridView1.Rows(inc).FindControl("lblVehicle"), Label)
                    lbVehID = CType(GridView1.Rows(inc).FindControl("vehID"), Label)
                    blninExisting = checkDuplicatesExisting(inclusiveProductPopupPicker.Text, lbVehID.Text)
                    blninNew = checkDuplicatesNew(inclusiveProductPopupPicker.Text, lbVehID.Text)

                    If (blninExisting = False And blninNew = False) Then
                        vehicles = vehicles + ", " + lb.Text
                        vehIDs = vehIDs + ", " + lbVehID.Text
                        If (checkedExc = False) Then
                            cb1 = CType(GridView1.Rows(0).FindControl("excludeVehicle"), CheckBox)
                            checkedExc = cb1.Checked
                        End If
                    End If
                Next
                If (vehicles.StartsWith(", ") = True) Then
                    vehicles = vehicles.Substring(1).Trim()
                    vehIDs = vehIDs.Substring(1).Trim()
                End If
            End If

            If (vehicles <> "") Then

                If (ViewState("dsNewAdditions") Is Nothing) Then
                    dsNewAddtions = New DataSet
                    dsNewAddtions = dsInclProducts.Clone()
                Else
                    dsNewAddtions = ViewState("dsNewAdditions")
                End If

                Dim foundRows As DataRow()
                Dim strExpr As String = "prdid = '" & inclusiveProductPopupPicker.DataId & "'"
                foundRows = dsNewAddtions.Tables(0).Select(strExpr)

                If (foundRows.Length > 0) Then
                    Dim drEdit As DataRow = foundRows(0)
                    If (drEdit(1) = "ALL") Then
                        drEdit.BeginEdit()
                        drEdit(1) = vehicles
                        drEdit(7) = inclusiveProductPopupPicker.DataId
                        drEdit(8) = vehicles
                        drEdit.EndEdit()
                    Else
                        drEdit.BeginEdit()
                        drEdit(1) = drEdit(1) & ", " & vehicles
                        drEdit(7) = inclusiveProductPopupPicker.DataId

                        drEdit(8) = drEdit(8) & ", " & vehIDs
                        drEdit.EndEdit()
                    End If
                Else
                    If (ViewState("incr") Is Nothing) Then
                        ViewState("incr") = -1
                    Else
                        ViewState("incr") = ViewState("incr") - 1
                    End If
                    Dim newIncr As Integer = Convert.ToInt32(ViewState("incr"))

                    Dim dr As DataRow
                    dr = dsNewAddtions.Tables(0).NewRow()
                    dr(0) = inclusiveProductPopupPicker.Text
                    dr(1) = vehicles
                    dr(2) = Convert.ToDateTime(Convert.ToDateTime(ckoFromDropdown.SelectedItem.ToString()))
                    dr(3) = Convert.ToDateTime(Convert.ToDateTime(ckoToTextbox.Text))
                    If (checkedExc = True) Then

                        dr(4) = "Yes"
                    Else
                        dr(4) = "No"
                    End If
                    dr(5) = newIncr
                    dr(6) = True
                    dr(7) = inclusiveProductPopupPicker.DataId
                    dr(8) = vehIDs
                    dsNewAddtions.Tables(0).Rows.Add(dr)

                End If
                dsNewAddtions.AcceptChanges()
                ViewState("dsNewAdditions") = dsNewAddtions
            End If
        End If

        GridView1.DataSource = Nothing
        GridView1.DataBind()
        inclusiveProductPopupPicker.Text = ""
        inclusiveProductPopup.Hide()
        ViewState("dtVehicles") = Nothing
        ViewState("datasetIsDirtyFlag") = 1
    End Sub


    Protected Sub addInclusiveProductToPackageListDb()

        Try
            Using scope As New TransactionScope()
                sbLogText = New StringBuilder

                If (Not dsNewAddtions Is Nothing) Then
                    Dim j As Integer
                    For j = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
                        Dim prod As String = dsNewAddtions.Tables(0).Rows(j)(0)
                        Dim veh As String = dsNewAddtions.Tables(0).Rows(j)(1)

                        If (veh.ToLower() = "all") Then
                            Dim foundRows As DataRow()
                            Dim search As String = "Product ='" & prod & "'"

                            foundRows = dsInclProducts.Tables(0).Select(search)

                            Dim incLink As Int64 = 0
                            Dim blnFound As Boolean = False
                            ' if row is found in inclusive linked product dataset set it to false
                            ' and later incLinkID to point to that vehicle in dsNewAdditions
                            For Each dsRow As DataRow In foundRows
                                Dim drEdit As DataRow = dsRow
                                drEdit.BeginEdit()
                                '
                                'drEdit(4) = "Yes"
                                '
                                drEdit(6) = False
                                incLink = Convert.ToInt64(dsRow(5))
                                drEdit.EndEdit()
                                blnFound = True
                            Next

                            If (blnFound = True) Then
                                Dim drEdit0 As DataRow = dsNewAddtions.Tables(0).Rows(j)
                                drEdit0.BeginEdit()
                                drEdit0(5) = incLink
                                drEdit0.EndEdit()
                            End If

                            Dim inc As Int64 = 0
                            Dim strArr() As String
                            Dim prdShortName As String = ""
                            strArr = prod.Split("-")
                            Dim prdID As String = dsNewAddtions.Tables(0).Rows(j)(7)
                            'Dim incPrd As Int64 = Convert.ToInt64(ViewState("IncPrdSetupid"))
                            Dim incPrd As Int64 = dsNewAddtions.Tables(0).Rows(j)(5)
                            inc = Aurora.SalesAndMarketing.Data.DataRepository.addExistingInclusiveProductIncLinkID(prdID, incPrd)

                            If (inc > 0) Then
                                Dim drEdit0 As DataRow = dsNewAddtions.Tables(0).Rows(j)
                                drEdit0.BeginEdit()
                                drEdit0(5) = inc
                                drEdit0.EndEdit()
                            End If
                        End If
                    Next
                    dsInclProducts.AcceptChanges()
                    dsNewAddtions.AcceptChanges()
                End If

                'Dim err As String = Aurora.SalesAndMarketing.Data.DataRepository.removeInclusiveProducts_FromPackage(dsInclProducts, ViewState("selectedVal"))

                Dim i As Integer
                If (Not dsNewAddtions Is Nothing) Then
                    For i = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
                        If (dsNewAddtions.Tables(0).Rows(i)(6) = True) Then
                            Aurora.SalesAndMarketing.Data.DataRepository.addExistingInclusiveProductAndVehicles_ToPackage(dsNewAddtions, ckoFromDropdown.SelectedItem.Text, ckoToTextbox.Text, i, Convert.ToInt64(ViewState("selectedVal")))
                        End If
                    Next
                End If
                scope.Complete()
            End Using
        Catch ex As TransactionAbortedException
            sbLogText.Length = 0
            sbLogText.Append("Exception while adding products to existing saleable product")
            sbLogText.Append("Message :" & ex.Message)
            sbLogText.AppendLine("Trace :" & ex.StackTrace)
            Logging.LogError("Exception", sbLogText.ToString())
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        Catch ex As Exception
            sbLogText.Length = 0
            sbLogText.AppendLine("Exception while adding products to existing saleable product")
            sbLogText.AppendLine("Message :" & ex.Message)
            sbLogText.AppendLine("Trace :" & ex.StackTrace)
            Logging.LogError("Exception", sbLogText.ToString())
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try

        setControlStateAfterSave()
        redirect()

    End Sub


    ''' <summary>
    ''' Event handler to cancel inclusive product selection to the existing package after the user clicks
    ''' the 'Cancel' button on the Inclusive products popup screen
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub inclusiveProductPopupCancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles inclusiveProductPopupCancelButton.Click
        If Not CanMaintain Then Return
        Try
            inclusiveProductSelectionCancelled()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Inclusive Product Selection Cancelled: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler for the Gridview Inclusive products deletion
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub GridView1_RowDeleting(ByVal sender As [Object], ByVal e As GridViewDeleteEventArgs) Handles GridView1.RowDeleting
        e.Cancel = False
    End Sub

    ''' <summary>
    ''' Remove vehicle association from the inclusive product by removing the entry from
    ''' the dtVehicles datatable for that vehicle 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub removeVehicle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeVehicle.Click
        If Not CanMaintain Then Return
        Try
            removeVehicleFromInclusiveProduct()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Remove Vehicle from Inclusive Product: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Add vehicle association to the inclusive product by adding the entry from
    ''' the dtVehicles datatable for that vehicle 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub addVehicleDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles addVehicleDropDown.SelectedIndexChanged
        If _loadException IsNot Nothing Then Return

        Try
            addVehicletoInclusiveProduct()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Add Vehicle to Inclusive Product: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' A different date is selected for a saleable product to retrieve the inclusive products for the range
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ckoFromDropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ckoFromDropdown.SelectedIndexChanged
        Try
            packageFromDateDropdownSelectionChanged()
            redirect()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Package From Date Selection Changed: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' New From Date Selection event handler
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub newFromDatePopupOKButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newFromDatePopupOKButton.Click
        Try
            If (IsDate(newDateControlFrom.Text) = False) Then
                addPickupDateButtonExt.Show()
                Return
            End If
            newFromDateSelected()
            addPackageDatesToDB()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - New From Date Selected: " & ex.Message)
        End Try
    End Sub

    Protected Sub newFromDatePopupCancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newFromDatePopupCancelButton.Click
        newFromDatePopupCancelButton.Attributes.Add("onclick", "$find('addPickupDateButtonPopupBehavior').hide(); return false;")
    End Sub

    ''' <summary>
    ''' Reset Screen state after an operation is cancelled. Enable/Disable required controls
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub resetInclusiveProductsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetInclusiveProductsButton.Click
        Try
            resetState()
            redirect()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Reset State: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Populate the popup date control with a default date
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub datePopup_onLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles addPickupDatePopupPanel.Load
        Try
            'setDefaultNewFromDate()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Set New Default From Date: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Event handler to Update all changes to the database after the user clicks on the 'Save' button
    ''' All deletions and insertions are taken care of in the method ('saveAllChangestoDB')
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub updateInclusiveProductsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateInclusiveProductsButton.Click
        'saveAllChangestoDB()
    End Sub

#End Region

#Region "Helper 'Sub' and 'Function' definitions used referred to in control event handlers"

    ''' <summary>
    ''' Initialize the Inclusive products tab with inclusive products for the selected saleable product
    ''' by calling the 'initInclusiveProducts()' method
    ''' </summary>
    ''' <param name="productDataSet"></param>
    ''' <param name="saleableProductRow"></param>
    ''' <remarks></remarks>
    Public Overrides Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        MyBase.InitProductUserControl(productDataSet, saleableProductRow)
        setControlState()
        Try
            initInclusiveProducts()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage("Error - Init Inclusive Products: " & ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Changes May 12, 2011
    ''' </summary>
    ''' <remarks></remarks>

    Protected Sub addPackageDatesToDB()
        If (ViewState("isNewItem") = True) Then
            Try

                Using scope As New TransactionScope()
                    sbLogText = New StringBuilder
                    ViewState("fromDate") = ckoFromDropdown.Text
                    Dim retStr As String = Aurora.SalesAndMarketing.Data.DataRepository.updateLastInclusivePrdSetupDate(ViewState("fromDate"), SaleableProductRow.SapId)
                    Dim incPrdSetupID As Int64 = Aurora.SalesAndMarketing.Data.DataRepository.addCompletelyNewPackage(SaleableProductRow.SapId, ckoFromDropdown.Text, ckoToTextbox.Text)
                    ViewState("incPrdSetupID") = incPrdSetupID
                    scope.Complete()
                End Using
            Catch ex As TransactionAbortedException
                sbLogText.Length = 0
                sbLogText.AppendLine("Exception while inserting completely new inclusive product setup dates")
                sbLogText.AppendLine("Message :" & ex.Message)
                sbLogText.AppendLine("Trace :" & ex.StackTrace)
                Logging.LogError("Exception", sbLogText.ToString())

                _loadException = ex
                Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))

            Catch ex As Exception
                sbLogText.Length = 0
                sbLogText.AppendLine("Exception while inserting completely new inclusive product setup dates")
                sbLogText.AppendLine("Message :" & ex.Message)
                sbLogText.AppendLine("Trace :" & ex.StackTrace)
                Logging.LogError("Exception", sbLogText.ToString())

                _loadException = ex
                Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            End Try
        End If
        setControlStateAfterSave()
        redirect()
    End Sub


    Protected Sub saveAllChangestoDB()
        'dsInclProducts = ViewState("dsInclProducts")
        'dsNewAddtions = ViewState("dsNewAdditions")

        'If (ViewState("isNewItem") = True) Then
        '    Try
        '        If (dsNewAddtions Is Nothing) Then
        '            Me.CurrentPage.AddErrorMessage("Cannot update without any inclusive products linked to the package.")
        '            Return
        '        End If

        '        If (dsNewAddtions.Tables(0).Rows.Count = 0) Then
        '            Me.CurrentPage.AddErrorMessage("Cannot update without any inclusive products linked to the package.")
        '            Return
        '        End If


        '        Using scope As New TransactionScope()
        '            sbLogText = New StringBuilder
        '            ViewState("fromDate") = ckoFromDropdown.Text
        '            Dim retStr As String = Aurora.SalesAndMarketing.Data.DataRepository.updateLastInclusivePrdSetupDate(ViewState("fromDate"), SaleableProductRow.SapId)
        '            Dim incPrdSetupID As Int64 = Aurora.SalesAndMarketing.Data.DataRepository.addCompletelyNewPackage(SaleableProductRow.SapId, ckoFromDropdown.Text, ckoToTextbox.Text)
        '            Dim i As Integer

        '            If (Not dsNewAddtions Is Nothing) Then
        '                For i = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
        '                    If (dsNewAddtions.Tables(0).Rows(i)(6) = True) Then
        '                        Aurora.SalesAndMarketing.Data.DataRepository.addNewInclusiveProductAndVehicles_ToPackage(dsNewAddtions, i, incPrdSetupID)
        '                    End If
        '                Next
        '            End If

        '            scope.Complete()
        '        End Using
        '    Catch ex As TransactionAbortedException
        '        sbLogText.Length = 0
        '        sbLogText.AppendLine("Exception while adding completely new inclusive product setup")
        '        sbLogText.AppendLine("Message :" & ex.Message)
        '        sbLogText.AppendLine("Trace :" & ex.StackTrace)
        '        Logging.LogError("Exception", sbLogText.ToString())

        '        _loadException = ex
        '        Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))

        '    Catch ex As Exception
        '        sbLogText.Length = 0
        '        sbLogText.AppendLine("Exception while adding completely new inclusive product setup")
        '        sbLogText.AppendLine("Message :" & ex.Message)
        '        sbLogText.AppendLine("Trace :" & ex.StackTrace)
        '        Logging.LogError("Exception", sbLogText.ToString())

        '        _loadException = ex
        '        Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        '    End Try
        '    setControlStateAfterSave()
        '    redirect()
        '    Return
        'End If

        'If (ViewState("isNewItem") Is Nothing Or ViewState("isNewItem") = False) Then
        '    Try
        '        Using scope As New TransactionScope()
        '            sbLogText = New StringBuilder

        '            If (Not dsNewAddtions Is Nothing) Then
        '                Dim j As Integer
        '                For j = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
        '                    Dim prod As String = dsNewAddtions.Tables(0).Rows(j)(0)
        '                    Dim veh As String = dsNewAddtions.Tables(0).Rows(j)(1)

        '                    If (veh.ToLower() = "all") Then
        '                        Dim foundRows As DataRow()
        '                        Dim search As String = "Product ='" & prod & "'"

        '                        foundRows = dsInclProducts.Tables(0).Select(search)

        '                        Dim incLink As Int64 = 0
        '                        Dim blnFound As Boolean = False
        '                        ' if row is found in inclusive linked product dataset set it to false
        '                        ' and later incLinkID to point to that vehicle in dsNewAdditions
        '                        For Each dsRow As DataRow In foundRows
        '                            Dim drEdit As DataRow = dsRow
        '                            drEdit.BeginEdit()
        '                            drEdit(6) = False
        '                            incLink = Convert.ToInt64(dsRow(5))
        '                            drEdit.EndEdit()
        '                            blnFound = True
        '                        Next


        '                        If (blnFound = True) Then
        '                            Dim drEdit0 As DataRow = dsNewAddtions.Tables(0).Rows(j)
        '                            drEdit0.BeginEdit()
        '                            drEdit0(5) = incLink
        '                            drEdit0.EndEdit()
        '                        End If

        '                        Dim inc As Int64 = 0
        '                        Dim strArr() As String
        '                        Dim prdShortName As String = ""
        '                        strArr = prod.Split("-")

        '                        'gets the product short name
        '                        'prdShortName = strArr(0).Trim()
        '                        Dim prdID As String = dsNewAddtions.Tables(0).Rows(j)(7)

        '                        Dim incPrd As Int64 = Convert.ToInt64(ViewState("IncPrdSetupid"))

        '                        inc = Aurora.SalesAndMarketing.Data.DataRepository.addExistingInclusiveProductIncLinkID(prdID, incPrd)

        '                        If (inc > 0) Then
        '                            Dim drEdit0 As DataRow = dsNewAddtions.Tables(0).Rows(j)
        '                            drEdit0.BeginEdit()
        '                            drEdit0(5) = inc
        '                            drEdit0.EndEdit()
        '                        End If
        '                    End If
        '                Next
        '                'accept changes to both the inclusive and newadditions datasets
        '                'before calling the stored procedures
        '                dsInclProducts.AcceptChanges()
        '                dsNewAddtions.AcceptChanges()
        '            End If

        '            Dim err As String = Aurora.SalesAndMarketing.Data.DataRepository.removeInclusiveProducts_FromPackage(dsInclProducts, ViewState("selectedVal"))
        '            Dim i As Integer

        '            If (Not dsNewAddtions Is Nothing) Then
        '                For i = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
        '                    If (dsNewAddtions.Tables(0).Rows(i)(6) = True) Then
        '                        Aurora.SalesAndMarketing.Data.DataRepository.addExistingInclusiveProductAndVehicles_ToPackage(dsNewAddtions, ckoFromDropdown.SelectedItem.Text, ckoToTextbox.Text, i, Convert.ToInt64(ViewState("selectedVal")))
        '                    End If
        '                Next
        '            End If
        '            scope.Complete()
        '        End Using
        '    Catch ex As TransactionAbortedException
        '        sbLogText.Length = 0
        '        sbLogText.Append("Exception while adding products to existing saleable product")
        '        sbLogText.Append("Message :" & ex.Message)
        '        sbLogText.AppendLine("Trace :" & ex.StackTrace)
        '        Logging.LogError("Exception", sbLogText.ToString())
        '        _loadException = ex
        '        Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        '    Catch ex As Exception
        '        sbLogText.Length = 0
        '        sbLogText.AppendLine("Exception while adding products to existing saleable product")
        '        sbLogText.AppendLine("Message :" & ex.Message)
        '        sbLogText.AppendLine("Trace :" & ex.StackTrace)
        '        Logging.LogError("Exception", sbLogText.ToString())
        '        _loadException = ex
        '        Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        '    End Try

        'End If
        'setControlStateAfterSave()
        'redirect()
    End Sub

    Protected Sub setControlStateAfterSave()
        If (ViewState("isNewItem") = True) Then
            'Session("selectedIndx") = ckoFromDropdown.Items.Count - 1
            Session("new") = 1
        End If

        ViewState("isNewItem") = Nothing
        ViewState("dsNewAdditions") = Nothing
        ViewState("dsInclProducts") = Nothing
        ViewState("datasetIsDirtyFlag") = "0"
        ViewState("dsSaleableProducts") = Nothing
        addPickupDateButton.Enabled = True
        ckoFromDropdown.Enabled = True
        ckoToTextbox.Enabled = True
        resetInclusiveProductsButton.Enabled = True
        updateInclusiveProductsButton.Enabled = True
        redirect()
    End Sub

    Protected Sub setControlState()
        If (ViewState("datasetIsDirtyFlag") = "1") Then
            If (ViewState("isNewItem") Is Nothing Or ViewState("isNewItem") = False) Then
                addPickupDateButton.Enabled = False
            End If
            If (ViewState("isNewItem") = True) Then
                addPickupDateButton.Enabled = True
            End If

            ckoFromDropdown.Enabled = False
            ckoToTextbox.Enabled = False
            resetInclusiveProductsButton.Enabled = True
            updateInclusiveProductsButton.Enabled = True
        Else
            addPickupDateButton.Enabled = True
            ckoFromDropdown.Enabled = True
            ckoToTextbox.Enabled = True
            resetInclusiveProductsButton.Enabled = False
            updateInclusiveProductsButton.Enabled = False
        End If
        'Rem
        ViewState("fromDate") = Nothing
    End Sub

    Protected Sub packageFromDateDropdownSelectionChanged()
        Try
            ViewState("selectedVal") = Convert.ToInt64(ckoFromDropdown.SelectedValue)
            Session("selectedIndx") = ckoFromDropdown.SelectedIndex
        Catch ex As Exception
            ViewState("selectedVal") = 0
            Session("selectedIndx") = 0
        End Try

        ViewState("dsInclProducts") = Nothing
    End Sub

    Protected Sub resetState()
        ckoToTextbox.Enabled = True
        ckoFromDropdown.Enabled = True
        addPickupDateButton.Enabled = True
        addVehicleDropDown.Items.Clear()

        If (Not ViewState("newDateIndx") Is Nothing) Then
            ViewState("newDateIndx") = Nothing
            ViewState("isNewItem") = Nothing
            If (ckoFromDropdown.Items.Count > 0) Then
                ckoFromDropdown.Items.RemoveAt(Convert.ToInt32(ViewState("newDateIndx")))
            End If
        End If
        addPickupDateButton.Enabled = True
        ViewState("dsNewAdditions") = Nothing
        ViewState("dsInclProducts") = Nothing
        ViewState("datasetIsDirtyFlag") = "0"
        ViewState("fromDate") = Nothing
        ViewState("isNewItem") = Nothing
        ViewState("newDateIndx") = Nothing

        If (ViewState("datasetIsDirtyFlag") Is Nothing) Then
            Return
        End If

        If (ViewState("dsInclProducts") Is Nothing) Then
            Return
        End If
    End Sub


    Protected Sub setDefaultNewFromDate()
        Dim d As New DataSet
        Dim fromdate As Date

        Try
            d = Aurora.SalesAndMarketing.Data.DataRepository.getMaxDteforSaleableProduct(SaleableProductRow.SapId)

            If (d.Tables(0).Rows.Count > 0) Then
                'if no entry is found  for a saleable product in the InclusiveProductSetup use the current date
                If (d.Tables(0).Rows(0)(0).ToString() = "") Then
                    fromdate = New Date(Now.Year, Now.Month, Now.Day)
                    ViewState("fromDate") = fromdate
                    fromdate = New Date(fromdate.Year, fromdate.Month, fromdate.Day)
                    newDateControlFrom.Date = fromdate
                    'newDateControlFrom.Text = fromdate
                Else
                    ViewState("fromDate") = d.Tables(0).Rows(0)(0)
                    fromdate = Convert.ToDateTime(d.Tables(0).Rows(0)(0))
                    fromdate = fromdate.AddDays(2)
                    'fromdate = fromdate.AddDays(368)
                    fromdate = New Date(fromdate.Year, fromdate.Month, fromdate.Day)
                    newDateControlFrom.Date = fromdate
                    'newDateControlFrom.Text = fromdate
                End If

            Else
                fromdate = New Date(Now.Year, Now.Month, Now.Day)
                newDateControlFrom.Date = fromdate
                'newDateControlFrom.Text = fromdate
            End If
            'newDateControlFrom.IS. = True
            'newDateControlFrom.AutoPostBack = True
            'newDateControlFrom.EnableViewState = True
            'addPickupDateButtonExt.Show()
        Catch ex As Exception
            sbLogText = New StringBuilder()
            sbLogText.Length = 0
            sbLogText.AppendLine("Method :getMaxDteforSaleableProduct")
            sbLogText.AppendLine("Message :" & ex.Message)
            sbLogText.AppendLine("Trace :" & ex.StackTrace)
            Logging.LogError("Exception", sbLogText.ToString())
        End Try
    End Sub

    Protected Sub newFromDateSelected()
        Dim fromDate As Date

        Try
            If (IsDate(newDateControlFrom.Text) = False) Then
                Return
            End If
            fromDate = New Date(newDateControlFrom.Date.Year, newDateControlFrom.Date.Month, newDateControlFrom.Date.Day)
        Catch ex As Exception
            Return
        End Try


        'Dim txttest As TextBox = CType(newDateControlFrom.FindControl("dateTextBox"), TextBox)
        'System.Diagnostics.Debug.WriteLine(txttest.Text)

        'check here if for the saleableproduct using a stored  procedure  if there is for the SAPID there are rows where
        'the new from date is between ckoFrom and ckkoTo if yes display an error message and return
        'if needs to be modified insert as parameter a to date equal to from date + 367 days
        'fromDate = New Date(2011, 4, 30)
        'the above date is just used for testing purposes

        Dim toDt As DateTime = fromDate.AddDays(367)

        'Dim count As Integer = Aurora.SalesAndMarketing.Data.DataRepository.checkDateOverlapforSapID(SaleableProductRow.SapId, fromDate)
        'If (count = 1) Then
        '    Me.CurrentPage.AddErrorMessage("Cannot have overlapping dates for the selected saleable product.")
        '    Return
        'End If


        ckoFromDropdown.Items.Add(fromDate.ToShortDateString())
        ckoFromDropdown.SelectedIndex = ckoFromDropdown.Items.Count - 1
        ckoFromDropdown.Enabled = False
        If (ViewState("isNewItem") Is Nothing Or ViewState("isNewItem") = False) Then
            addPickupDateButton.Enabled = False
        End If
        If (ViewState("isNewItem") = True) Then
            addPickupDateButton.Enabled = False
        End If

        ckoToTextbox.Text = "31/12/2099"
        ckoToTextbox.Enabled = False

        ViewState("newDateIndx") = ckoFromDropdown.Items.Count - 1
        ViewState("datasetIsDirtyFlag") = "1"
        ViewState("isNewItem") = True
        Response.Cookies("newFromDate").Value = fromDate.ToShortDateString()
        Response.Cookies("newFromDate").Expires = DateTime.Now.AddDays(1)
        Response.Cookies("newToDate").Value = "31/12/2099"
        Response.Cookies("newToDate").Expires = DateTime.Now.AddDays(1)
        'ViewState("newFromDate") = fromDate.ToShortDateString()
        'ViewState("newToDate") = "31/12/2099"

        resetInclusiveProductsButton.Enabled = True
        updateInclusiveProductsButton.Enabled = True
        inclusiveProductsTable.Rows.Clear()
        If (Not ViewState("dsInclProducts") Is Nothing) Then
            dsInclProducts = ViewState("dsInclProducts")
            dsInclProducts.Tables(0).Rows.Clear()
            ViewState("dsInclProducts") = dsInclProducts
        End If
    End Sub

    Protected Sub inclusiveProductSelectionCancelled()

        inclusiveProductPopup.Hide()
        addVehicleDropDown.Items.Clear()
        GridView1.DataSource = Nothing
        GridView1.DataBind()
        inclusiveProductPopupPicker.Text = ""
        HiddenField_VehicleRows.Value = 0
        If (ViewState("dtVehicles") Is Nothing) Then Return
        ViewState("dtVehicles") = Nothing

    End Sub


    Protected Sub addVehicletoInclusiveProduct()
        inclusiveProductPopup.Show()
        Dim dtVehicles As New DataTable("Vehicles")
        If (ViewState("dtVehicles") Is Nothing) Then
            'create a data table dynamically to hold the vehicle details
            Dim remVehicle As DataColumn = New DataColumn("remVehicle")
            remVehicle.DataType = System.Type.GetType("System.Int32")
            dtVehicles.Columns.Add(remVehicle)
            Dim vehicle As DataColumn = New DataColumn("vehicle")
            vehicle.DataType = System.Type.GetType("System.String")
            dtVehicles.Columns.Add(vehicle)
            Dim exclude As DataColumn = New DataColumn("exclude")
            exclude.DataType = System.Type.GetType("System.Int32")
            dtVehicles.Columns.Add(exclude)

            Dim vid As DataColumn = New DataColumn("vid")
            vid.DataType = System.Type.GetType("System.String")
            dtVehicles.Columns.Add(vid)

            If (addVehicleDropDown.SelectedIndex > 0) Then
                Dim drNew As DataRow
                drNew = dtVehicles.NewRow()
                drNew(0) = 0
                drNew(1) = addVehicleDropDown.SelectedItem.Text
                drNew(2) = 0
                drNew(3) = addVehicleDropDown.SelectedValue.ToString()
                dtVehicles.Rows.Add(drNew)
                ViewState("dtVehicles") = dtVehicles
            End If
        Else
            dtVehicles = ViewState("dtVehicles")

            Dim foundRow As DataRow()
            Dim search As String = ""
            search = "vid ='" & addVehicleDropDown.SelectedValue.ToString() & "'"
            foundRow = dtVehicles.Select(search)

            If (foundRow.Length = 0) Then
                If (addVehicleDropDown.SelectedIndex > 0) Then
                    Dim drNew As DataRow
                    drNew = dtVehicles.NewRow()
                    drNew(0) = 0
                    drNew(1) = addVehicleDropDown.SelectedItem.Text
                    drNew(2) = 0
                    drNew(3) = addVehicleDropDown.SelectedValue.ToString()
                    dtVehicles.Rows.Add(drNew)
                    ViewState("dtVehicles") = dtVehicles
                End If
            End If
        End If

        Dim inc As Integer = 0

        Dim dt As New DataTable
        Dim dr As DataRow
        For inc = 0 To dtVehicles.Rows.Count - 1
            dr = dt.NewRow()
            dt.Rows.Add(dr)
        Next
        GridView1.DataSource = dt
        GridView1.DataBind()

        For inc = 0 To dtVehicles.Rows.Count - 1
            Dim drTemp As DataRow
            drTemp = dtVehicles.Rows(inc)
            Dim lb As Label = CType(GridView1.Rows(inc).FindControl("lblVehicle"), Label)
            lb.Text = drTemp(1).ToString()
            Dim lbVeh As Label = CType(GridView1.Rows(inc).FindControl("vehID"), Label)
            lbVeh.Text = drTemp(3).ToString()
            Dim cb1 As CheckBox = CType(GridView1.Rows(inc).FindControl("excludeVehicle"), CheckBox)
            If (drTemp(2) <> 0) Then
                cb1.Checked = True
            Else
                cb1.Checked = False
            End If
        Next

        HiddenField_VehicleRows.Value = GridView1.Rows.Count.ToString()
    End Sub

    Protected Sub removeVehicleFromInclusiveProduct()
        Dim dtVehicles As New DataTable("Vehicles")
        Dim inc As Integer = 0
        Dim dt As New DataTable
        Dim dr As DataRow

        dtVehicles = ViewState("dtVehicles")

        If (dtVehicles Is Nothing) Then
            inclusiveProductPopup.Show()
            HiddenField_VehicleRows.Value = 0
            Return
        End If

        If (dtVehicles.Rows.Count = 0) Then
            inclusiveProductPopup.Show()
            HiddenField_VehicleRows.Value = 0
            Return
        End If

        'iterate thru the datatable dtvehicles and store the selected vehicle id
        'to be deleted from the list into an arraylist 
        Dim keystoRem As New ArrayList
        For inc = 0 To dtVehicles.Rows.Count - 1
            Dim cb As CheckBox = CType(GridView1.Rows(inc).FindControl("remVehicle"), CheckBox)
            Dim lb As Label = CType(GridView1.Rows(inc).FindControl("lblVehicle"), Label)
            If (cb.Checked = True) Then
                Dim searchExpr As String = "vid= '" & dtVehicles.Rows(inc)(3).ToString() & "'"
                Dim foundRows As DataRow()
                foundRows = dtVehicles.Select(searchExpr)
                For Each dsRow As DataRow In foundRows
                    'dtVehicles.Rows(inc)(3).ToString() - contains the vehicle product ID value'
                    keystoRem.Add(dtVehicles.Rows(inc)(3).ToString())
                Next
            End If
        Next


        'iterate thru the arraylist with the vehicle ids to be deleted
        'find them in the datatable, delete them and the following the iteration 
        'accept all changes to the datatable
        Dim i As Integer = 0
        For i = 0 To keystoRem.Count - 1
            Dim searchExpr As String = "vid= '" & keystoRem(i) & "'"
            Dim foundRows As DataRow()
            foundRows = dtVehicles.Select(searchExpr)
            For Each dsRow As DataRow In foundRows
                dsRow.Delete()
            Next
        Next

        If (GridView1.Rows.Count = 0) Then
            addVehicleDropDown.SelectedIndex = 0
        End If


        dtVehicles.AcceptChanges()
        ViewState("dtVehicles") = dtVehicles
        GridView1.DataSource = Nothing
        GridView1.DataBind()
        inclusiveProductPopup.Show()

        For inc = 0 To dtVehicles.Rows.Count - 1
            dr = dt.NewRow()
            dt.Rows.Add(dr)
        Next
        GridView1.DataSource = dt
        GridView1.DataBind()

        For inc = 0 To dtVehicles.Rows.Count - 1
            Dim drTemp As DataRow
            drTemp = dtVehicles.Rows(inc)
            Dim lb As Label = CType(GridView1.Rows(inc).FindControl("lblVehicle"), Label)
            lb.Text = drTemp(1).ToString()
            Dim cb1 As CheckBox = CType(GridView1.Rows(inc).FindControl("excludeVehicle"), CheckBox)
            If (drTemp(2) <> 0) Then
                cb1.Checked = True
            Else
                cb1.Checked = False
            End If
        Next

        HiddenField_VehicleRows.Value = GridView1.Rows.Count.ToString()

    End Sub

    ''' <summary>
    '''create schema to store temp data for inclusive products and associated vehicles for a saleable product 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub createInclusiveProductSchema()
        Dim dtIncl As New DataTable("Inclusive")

        Dim Product As DataColumn = New DataColumn("Product")
        Product.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(Product)

        Dim Vehicles As DataColumn = New DataColumn("Vehicles")
        Vehicles.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(Vehicles)

        Dim IncPrdSetupCkoFromdate As DataColumn = New DataColumn("IncPrdSetupCkoFromdate")
        IncPrdSetupCkoFromdate.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(IncPrdSetupCkoFromdate)

        Dim IncPrdSetupCkoToDate As DataColumn = New DataColumn("IncPrdSetupCkoToDate")
        IncPrdSetupCkoToDate.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(IncPrdSetupCkoToDate)

        Dim IncLnkPrdVehIsInclude As DataColumn = New DataColumn("IncLnkPrdVehIsInclude")
        IncLnkPrdVehIsInclude.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(IncLnkPrdVehIsInclude)

        Dim inclinkid As DataColumn = New DataColumn("inclinkid")
        inclinkid.DataType = System.Type.GetType("System.Int64")
        dtIncl.Columns.Add(inclinkid)

        Dim IncLinkIsActive As DataColumn = New DataColumn("IncLinkIsActive")
        IncLinkIsActive.DataType = System.Type.GetType("System.Boolean")
        dtIncl.Columns.Add(IncLinkIsActive)

        Dim prdID As DataColumn = New DataColumn("prdID")
        prdID.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(prdID)

        Dim prdVehID As DataColumn = New DataColumn("prdVehID")
        prdVehID.DataType = System.Type.GetType("System.String")
        dtIncl.Columns.Add(prdVehID)

        dsInclProducts = New DataSet
        dsInclProducts.Tables.Add(dtIncl)
        ViewState("dsInclProducts") = dsInclProducts
    End Sub



    Protected Sub initInclusiveProducts()

        Dim ref As String = ""
        If (Not Request.UrlReferrer Is Nothing) Then
            ref = Request.UrlReferrer.ToString().ToLower()
            If (Session("refferer") Is Nothing) Then
                Session("refferer") = ref
            End If

            'If (Session("refferer").ToString().ToLower = ref) Then
            If (String.Compare(Session("refferer").ToString(), ref, StringComparison.OrdinalIgnoreCase) = 0) Then
                resetState()
                ViewState("SapID") = (SaleableProductRow.SapId)
                ViewState("dsSaleableProducts") = Nothing
                ViewState("dsInclProducts") = Nothing
                Session("selectedIndx") = Nothing
                ViewState("selectedVal") = Nothing
            End If

        End If

        HiddenField_InclusiveProds.Value = 0

        If (ViewState("SapID") Is Nothing) Then
            ViewState("SapID") = (SaleableProductRow.SapId)
        ElseIf (ViewState("SapID") <> SaleableProductRow.SapId) Then
            resetState()
            ViewState("SapID") = (SaleableProductRow.SapId)
            ViewState("dsSaleableProducts") = Nothing
            ViewState("dsInclProducts") = Nothing
            Session("selectedIndx") = Nothing
            ViewState("selectedVal") = Nothing
        End If

        addInclusiveProductButton.Visible = CanMaintain
        removeInclusiveProductButton.Visible = CanMaintain

        'If (ViewState("isNewItem") = True) Then
        '    dsSaleableProducts = ViewState("dsSaleableProducts")
        '    If Not Request.Cookies("newFromDate") Is Nothing Then
        '        Dim fromDt As String = Request.Cookies("newFromDate").Value
        '        ckoFromDropdown.Items.Add(fromDt)
        '    End If
        '    ckoFromDropdown.SelectedIndex = ckoFromDropdown.Items.Count - 1
        '    ckoFromDropdown.Enabled = False
        '    addPickupDateButton.Enabled = False
        '    If Not Request.Cookies("newToDate") Is Nothing Then
        '        Dim toDt As String = Request.Cookies("newToDate").Value
        '        ckoToTextbox.Text = toDt
        '    End If
        '    If (Not ViewState("dsNewAdditions") Is Nothing) Then
        '        dsNewAddtions = ViewState("dsNewAdditions")
        '        displayInclusiveProducts(dsNewAddtions, True)
        '    End If
        '    Session("new") = "1"

        '    HiddenField_InclusiveProds.Value = inclusiveProductsTable.Rows.Count - 1
        '    Return
        'End If
        'add comment entries
        If (ViewState("dsSaleableProducts") Is Nothing) Then
            Try
                dsSaleableProducts = Aurora.SalesAndMarketing.Data.DataRepository.getSaleableProducts(dsSaleableProducts, SaleableProductRow.SapId)
            Catch ex As Exception
                sbLogText = New StringBuilder()
                sbLogText.Length = 0
                sbLogText.AppendLine("Method : getSaleableProducts")
                sbLogText.AppendLine("Message :" & ex.Message)
                sbLogText.AppendLine("Trace :" & ex.StackTrace)
                Logging.LogError("Exception", sbLogText.ToString())
            End Try
            ViewState("dsSaleableProducts") = dsSaleableProducts
        Else
            dsSaleableProducts = ViewState("dsSaleableProducts")
        End If

        If (dsSaleableProducts.Tables(0).Rows.Count > 0) Then
            ViewState("IncPrdSetupid") = dsSaleableProducts.Tables(0).Rows(0)(0)
        End If

        If dsSaleableProducts.Tables(0).Rows.Count = 0 Then
            createInclusiveProductSchema()
            If (Not ViewState("dsNewAdditions") Is Nothing) Then
                dsNewAddtions = ViewState("dsNewAdditions")
                displayInclusiveProducts(dsNewAddtions, True)
            End If
            HiddenField_InclusiveProds.Value = inclusiveProductsTable.Rows.Count - 1
            Return
        End If

        populateComboBoxPackages(dsSaleableProducts)

        Dim dsInclProducts As New DataSet
        Dim indxSelValue As Int64 = 0

        'If (ViewState("selectedVal") Is Nothing) Then
        '    indxSelValue = Convert.ToInt64(ckoFromDropdown.SelectedValue)
        '    ViewState("selectedVal") = indxSelValue
        'Else
        '    indxSelValue = Convert.ToInt64(ViewState("selectedVal"))
        'End If

        Dim indx As Integer

        If (Session("selectedIndx") Is Nothing) Then
            indx = 0
            Session("selectedIndx") = 0
        Else
            indx = Session("selectedIndx")
        End If

        If (Session("new") = "1") Then
            Session("selectedIndx") = ckoFromDropdown.Items.Count - 1
            indx = ckoFromDropdown.Items.Count - 1
            Session("new") = "0"
        Else
            ckoFromDropdown.SelectedIndex = indx
        End If

        'Code added May 12, 2011
        Dim li As ListItem = Nothing
        Try
            li = ckoFromDropdown.Items(indx)
        Catch ex As Exception
            If (ckoFromDropdown.Items.Count > 0) Then
                indx = ckoFromDropdown.Items.Count - 1
                Session("selectedIndx") = indx
                li = ckoFromDropdown.Items(indx)
            Else
                Return
            End If
        End Try



        ckoFromDropdown.ClearSelection()
        ckoFromDropdown.Items.FindByValue(li.Value).Selected = True
        ckoFromDropdown.Items(indx).Selected = True
        ckoToTextbox.Text = dsSaleableProducts.Tables(0).Rows(indx)(2)
        indxSelValue = Convert.ToInt64(ckoFromDropdown.SelectedValue)
        ViewState("selectedVal") = indxSelValue

        If (ViewState("dsInclProducts") Is Nothing) Then
            Try
                dsInclProducts = Aurora.SalesAndMarketing.Data.DataRepository.getInclusiveProducts(dsInclProducts, Convert.ToInt64(ckoFromDropdown.SelectedValue))
            Catch ex As Exception
                sbLogText = New StringBuilder()
                sbLogText.Length = 0
                sbLogText.AppendLine("Method : getInclusiveProducts")
                sbLogText.AppendLine("Message :" & ex.Message)
                sbLogText.AppendLine("Trace :" & ex.StackTrace)
                Logging.LogError("Exception", sbLogText.ToString())
            End Try
            'conv dataset here
            If (dsInclProducts.Tables.Count > 0) Then
                Dim temp As DataSet = unFlattenDataset(dsInclProducts)
                dsInclProducts = temp
                ViewState("dsInclProducts") = temp
            End If
        Else
            dsInclProducts = ViewState("dsInclProducts")
        End If

        displayInclusiveProducts(dsInclProducts, False)

        If (Not ViewState("dsNewAdditions") Is Nothing) Then
            dsNewAddtions = ViewState("dsNewAdditions")
            displayInclusiveProducts(dsNewAddtions, True)
        End If
        HiddenField_InclusiveProds.Value = inclusiveProductsTable.Rows.Count - 1

    End Sub

    ''' <summary>
    ''' Unflatten dataset to meet the desired UI format 
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function unFlattenDataset(ByVal ds As DataSet) As DataSet
        Dim dsTempr As New DataSet
        dsTempr = ds.Copy()

        If (ds.Tables(0).Rows.Count > 1) Then
            Dim j As Integer
            For j = 0 To ds.Tables(0).Rows.Count - 1
                If (ds.Tables(0).Rows(j)(1).ToString() = "") Then

                    Dim prd As String = ds.Tables(0).Rows(j)(0).ToString()
                    Dim strExpr As String = "Product = '" & prd & "'"
                    Dim foundRows As DataRow() = dsTempr.Tables(0).Select(strExpr)
                    If (foundRows.Length > 1) Then
                        ds.Tables(0).Rows(j).Delete()
                    End If
                End If
            Next
        End If

        ds.AcceptChanges()

        Dim dsTemp As New DataSet
        dsTemp = ds.Clone
        Dim i As Integer
        For i = 0 To ds.Tables(0).Rows.Count - 1
            Dim prod As String = ""
            prod = ds.Tables(0).Rows(i)(0)

            If (i = 0) Then
                Dim dr As DataRow
                If (ds.Tables(0).Rows(i)(1).ToString() <> "") Then
                    dr = dsTemp.Tables(0).NewRow()
                    dr(0) = ds.Tables(0).Rows(i)(0)
                    dr(1) = ds.Tables(0).Rows(i)(1)
                    dr(2) = ds.Tables(0).Rows(i)(2)
                    dr(3) = ds.Tables(0).Rows(i)(3)
                    dr(4) = ds.Tables(0).Rows(i)(4)
                    dr(5) = ds.Tables(0).Rows(i)(5)
                    dr(6) = ds.Tables(0).Rows(i)(6)
                    dr(7) = ds.Tables(0).Rows(i)(7)
                    dr(8) = ds.Tables(0).Rows(i)(8)
                    dsTemp.Tables(0).Rows.Add(dr)
                    dsTemp.AcceptChanges()
                Else
                    dr = dsTemp.Tables(0).NewRow()
                    dr(0) = ds.Tables(0).Rows(i)(0)
                    dr(1) = "ALL"
                    dr(2) = ds.Tables(0).Rows(i)(2)
                    dr(3) = ds.Tables(0).Rows(i)(3)
                    dr(4) = ds.Tables(0).Rows(i)(4)
                    dr(5) = ds.Tables(0).Rows(i)(5)
                    dr(6) = ds.Tables(0).Rows(i)(6)
                    dr(7) = ds.Tables(0).Rows(i)(7)
                    dr(8) = "ALL"
                    dsTemp.Tables(0).Rows.Add(dr)
                    dsTemp.AcceptChanges()
                End If
            Else
                Dim foundRows As DataRow()
                Dim strExpr As String = "Product = '" & prod & "'"
                foundRows = dsTemp.Tables(0).Select(strExpr)
                If (foundRows.Length = 0) Then
                    Dim dr As DataRow
                    If (ds.Tables(0).Rows(i)(1).ToString() <> "") Then
                        dr = dsTemp.Tables(0).NewRow()
                        dr(0) = ds.Tables(0).Rows(i)(0)
                        dr(1) = ds.Tables(0).Rows(i)(1)
                        dr(2) = ds.Tables(0).Rows(i)(2)
                        dr(3) = ds.Tables(0).Rows(i)(3)
                        dr(4) = ds.Tables(0).Rows(i)(4)
                        dr(5) = ds.Tables(0).Rows(i)(5)
                        dr(6) = ds.Tables(0).Rows(i)(6)
                        dr(7) = ds.Tables(0).Rows(i)(7)
                        dr(8) = ds.Tables(0).Rows(i)(8)
                        dsTemp.Tables(0).Rows.Add(dr)
                        dsTemp.AcceptChanges()
                    Else
                        dr = dsTemp.Tables(0).NewRow()
                        dr(0) = ds.Tables(0).Rows(i)(0)
                        dr(1) = "ALL"
                        dr(2) = ds.Tables(0).Rows(i)(2)
                        dr(3) = ds.Tables(0).Rows(i)(3)
                        dr(4) = ds.Tables(0).Rows(i)(4)
                        dr(5) = ds.Tables(0).Rows(i)(5)
                        dr(6) = ds.Tables(0).Rows(i)(6)
                        dr(7) = ds.Tables(0).Rows(i)(7)
                        dr(8) = "ALL"
                        dsTemp.Tables(0).Rows.Add(dr)
                        dsTemp.AcceptChanges()

                    End If

                Else
                    Dim drEdit As DataRow = foundRows(0)
                    drEdit.BeginEdit()
                    drEdit(1) = drEdit(1) & ", " & ds.Tables(0).Rows(i)(1)
                    drEdit(8) = drEdit(8) & ", " & ds.Tables(0).Rows(i)(8)
                    drEdit.EndEdit()
                    dsTemp.AcceptChanges()
                End If
            End If

        Next
        Return dsTemp
    End Function

    ''' <summary>
    ''' populate combo-box with dates for a saleable product from the 'InclusiveProductSetup' table
    ''' </summary>
    ''' <param name="dsPackages"></param>
    ''' <remarks></remarks>
    Protected Sub populateComboBoxPackages(ByVal dsPackages As DataSet)
        ckoFromDropdown.Items.Clear()
        ckoFromDropdown.DataSource = dsPackages.Tables(0)
        ckoFromDropdown.DataTextField = "IncPrdSetupCkoFromDate"
        ckoFromDropdown.DataValueField = "IncPrdSetupId"
        ckoFromDropdown.DataBind()
        If (dsPackages.Tables(0).Rows.Count > 0) Then
            ckoToTextbox.Text = dsPackages.Tables(0).Rows(0)(2)
        End If
    End Sub

    ''' <summary>
    ''' Display all the existing inclusive products and any new inclusive products added 
    ''' in this current operation in a gridview control
    ''' </summary>
    ''' <param name="ds"></param>
    ''' <param name="blnAdditions"></param>
    ''' <remarks></remarks>
    ''' 
    Protected Sub displayInclusiveProducts(ByVal ds As DataSet, ByVal blnAdditions As Boolean)
        Dim rowindex As Integer = 0
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim chkID As String = ""

        Dim strVehicles As String = ""
        For i = 0 To ds.Tables(0).Rows.Count - 1

            If (ds.Tables(0).Rows(i)(6) = False) Then
                Continue For
            End If

            Dim strProduct As String = ds.Tables(0).Rows(i)(0).ToString()
            Dim fromDtTemp As Date = ds.Tables(0).Rows(i)(2).ToString()
            Dim toDtTemp As Date = ds.Tables(0).Rows(i)(3).ToString()

            strVehicles = ds.Tables(0).Rows(i)(1).ToString()

            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowindex Mod 2 = 0, "row", "rowalt") : rowindex += 1
            tableRow.BackColor = SaleableProductRow.ProductRow.StatusColor()
            inclusiveProductsTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                chkID = Server.HtmlEncode(ds.Tables(0).Rows(i)(5).ToString())
                checkBox.ID = chkID.ToString()
                selectCell.Controls.Add(checkBox)
                _checkBoxesByProduct.Add(chkID, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim prdName As New TableCell
            prdName.Text = Server.HtmlEncode(ds.Tables(0).Rows(i)(0).ToString())
            tableRow.Controls.Add(prdName)

            Dim vehicles As New TableCell
            If ((strVehicles.Trim().Length = 0) Or (strVehicles.Trim() = ",")) Then
                strVehicles = "ALL"
            End If
            vehicles.Text = Server.HtmlEncode(strVehicles)
            tableRow.Controls.Add(vehicles)

            Dim exclusion As New TableCell
            If (ds.Tables(0).Rows(i)(4).ToString() = "") Then
                exclusion.Text = "Yes"
            Else
                exclusion.Text = Server.HtmlEncode(ds.Tables(0).Rows(i)(4).ToString())
            End If
            tableRow.Controls.Add(exclusion)
            chkID = chkID + 1
        Next i

    End Sub

    ''' <summary>
    ''' Show inclusive products for a saleable product in a popup window.
    ''' Allows the user to select a non-vehicle product and then
    ''' associate vehicle products with that inclusive product.
    ''' The vehicle product combo box is populated by vehicle short and names and 
    ''' and vehicle product id is associated with tht combo box selected value
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub showInclusiveProductsPopupScreen()
        Dim dsVehicles As New DataSet
        Try
            dsVehicles = Aurora.SalesAndMarketing.Data.DataRepository.getvehicles(dsVehicles, SaleableProductRow.SapCtyCode)
        Catch ex As Exception
            sbLogText = New StringBuilder()
            sbLogText.Length = 0
            sbLogText.AppendLine("Method : getvehicles")
            sbLogText.AppendLine("Message :" & ex.Message)
            sbLogText.AppendLine("Trace :" & ex.StackTrace)
            Logging.LogError("Exception", sbLogText.ToString())
        End Try

        Dim inc As Integer = 0
        addVehicleDropDown.Items.Clear()
        addVehicleDropDown.DataSource = dsVehicles.Tables(0)
        addVehicleDropDown.DataValueField = "ID"
        addVehicleDropDown.DataTextField = "CODE"
        addVehicleDropDown.DataBind()
        addVehicleDropDown.Items.Insert(0, "Select")
        addVehicleDropDown.SelectedIndex = 0

        inclusiveProductPopupPicker.Param1 = ""
        inclusiveProductPopupPicker.Param2 = Me.SaleableProductRow.SapCtyCode
        inclusiveProductPopupPicker.Param3 = Nothing
        inclusiveProductPopupPicker.Param4 = Nothing
        inclusiveProductPopupPicker.Param5 = Nothing
        inclusiveProductPopupPicker.DataId = ""
        inclusiveProductPopupPicker.Text = ""
        inclusiveProductPopup.Show()
    End Sub

    Protected Sub removeInclusiveProductfromPackageListDB()
        Dim blnChanges As Boolean = False

        Dim i As Integer = 0
        If (Not ViewState("dsInclProducts") Is Nothing) Then
            dsInclProducts = ViewState("dsInclProducts")
        End If

        If (Not dsInclProducts Is Nothing) Then
            For i = 0 To dsInclProducts.Tables(0).Rows.Count - 1
                If (dsInclProducts.Tables(0).Rows(i)(6) = False) Then
                    Continue For
                End If

                Dim checkBox As CheckBox = _checkBoxesByProduct(dsInclProducts.Tables(0).Rows(i)(5))
                If checkBox.Checked = True Then
                    Dim drEdit As DataRow = dsInclProducts.Tables(0).Rows(i)
                    drEdit.BeginEdit()
                    drEdit(6) = False
                    drEdit.EndEdit()
                    blnChanges = True
                End If
            Next

            If (blnChanges = False) Then
                setControlStateAfterSave()
                Return
            End If

            dsInclProducts.AcceptChanges()
            ViewState("dsInclProducts") = dsInclProducts


            Try
                Using scope As New TransactionScope()
                    sbLogText = New StringBuilder
                    Dim err As String = Aurora.SalesAndMarketing.Data.DataRepository.removeInclusiveProducts_FromPackage(dsInclProducts, ViewState("selectedVal"))
                    scope.Complete()
                End Using
            Catch ex As TransactionAbortedException
                sbLogText.Length = 0
                sbLogText.Append("Exception while removing products products to existing saleable product")
                sbLogText.Append("Message :" & ex.Message)
                sbLogText.AppendLine("Trace :" & ex.StackTrace)
                Logging.LogError("Exception", sbLogText.ToString())
                _loadException = ex
                Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Catch ex As Exception
                sbLogText.Length = 0
                sbLogText.AppendLine("Exception while removing products to existing saleable product")
                sbLogText.AppendLine("Message :" & ex.Message)
                sbLogText.AppendLine("Trace :" & ex.StackTrace)
                Logging.LogError("Exception", sbLogText.ToString())
                _loadException = ex
                Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            End Try
        End If
        setControlStateAfterSave()
        redirect()
    End Sub


    '''<summary>
    '''Remove an inclusive product froma package list Only UI is affected changes 
    '''are not actually saved to the dataset until 'Save' button is clicked
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Protected Sub removeInclusiveProductfromPackageList()
        'ViewState("datasetIsDirtyFlag") = "0"

        'Dim i As Integer = 0
        'dsInclProducts = ViewState("dsInclProducts")
        'If (Not dsInclProducts Is Nothing) Then
        '    For i = 0 To dsInclProducts.Tables(0).Rows.Count - 1
        '        If (dsInclProducts.Tables(0).Rows(i)(6) = False) Then
        '            Continue For
        '        End If

        '        Dim checkBox As CheckBox = _checkBoxesByProduct(dsInclProducts.Tables(0).Rows(i)(5))
        '        If checkBox.Checked = True Then
        '            Dim drEdit As DataRow = dsInclProducts.Tables(0).Rows(i)
        '            drEdit.BeginEdit()
        '            drEdit(6) = False
        '            drEdit.EndEdit()
        '            'ViewState("datasetIsDirtyFlag") = "1"
        '            'addPickupDateButton.Enabled = False
        '        End If
        '    Next
        '    dsInclProducts.AcceptChanges()
        '    ViewState("dsInclProducts") = dsInclProducts
        '    'ViewState("datasetIsDirtyFlag") = "1"
        '    'addPickupDateButton.Enabled = False
        'End If

        'dsNewAddtions = ViewState("dsNewAdditions")
        'Dim keystoRem As New ArrayList
        'Dim indxtoRem As New ArrayList

        'If (Not dsNewAddtions Is Nothing) Then
        '    For i = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
        '        If (dsNewAddtions.Tables(0).Rows(i)(6) = False) Then
        '            Continue For
        '        End If
        '        Dim checkBox As CheckBox = _checkBoxesByProduct(dsNewAddtions.Tables(0).Rows(i)(5))
        '        If checkBox.Checked = True Then
        '            keystoRem.Add(dsNewAddtions.Tables(0).Rows(i)(5))
        '        End If
        '    Next

        '    For i = 0 To keystoRem.Count - 1
        '        _checkBoxesByProduct.Remove(keystoRem(i))
        '        Dim searchExpr As String = "inclinkid= " & Convert.ToInt32(keystoRem(i))
        '        Dim foundRows As DataRow()
        '        foundRows = dsNewAddtions.Tables(0).Select(searchExpr)
        '        For Each dsRow As DataRow In foundRows
        '            dsRow.Delete()
        '            'ViewState("datasetIsDirtyFlag") = "1"
        '            'addPickupDateButton.Enabled = False
        '        Next

        '    Next
        '    dsNewAddtions.AcceptChanges()
        '    ViewState("dsNewAdditions") = dsNewAddtions
        '    'ViewState("datasetIsDirtyFlag") = "1"
        '    'addPickupDateButton.Enabled = False
        'End If

        'addPickupDateButton.Enabled = False
        'ViewState("datasetIsDirtyFlag") = "1"

        ''Added May 3, 2011
        'resetInclusiveProductsButton.Enabled = True
        'updateInclusiveProductsButton.Enabled = True
    End Sub


    'Protected Sub addInclusiveProductToPackageList()
    'If (inclusiveProductPopupPicker.DataId.ToString().Trim() = "") Then
    '    inclusiveProductPopup.Show()
    '    Return
    'End If

    'If (ViewState("datasetIsDirtyFlag") Is Nothing) Then
    '    ViewState("datasetIsDirtyFlag") = "1"
    'End If

    'If (ViewState("isNewItem") Is Nothing Or ViewState("isNewItem") = False) Then
    '    addPickupDateButton.Enabled = False
    'End If
    'If (ViewState("isNewItem") = True) Then
    '    addPickupDateButton.Enabled = True
    'End If


    'If (ViewState("dsInclProducts") Is Nothing) Then
    '    dsInclProducts = New DataSet
    '    Try
    '        'for a new product check if the ckoFrmdate is nothing - start here
    '        dsInclProducts = Aurora.SalesAndMarketing.Data.DataRepository.getInclusiveProducts(dsInclProducts, Convert.ToInt64(ckoFromDropdown.SelectedValue))
    '    Catch ex As Exception
    '        sbLogText = New StringBuilder()
    '        sbLogText.Length = 0
    '        sbLogText.AppendLine("Method : getInclusiveProducts")
    '        sbLogText.AppendLine("Message :" & ex.Message)
    '        sbLogText.AppendLine("Trace :" & ex.StackTrace)
    '        Logging.LogError("Exception", sbLogText.ToString())
    '    End Try

    '    'conv dataset here
    '    If (dsInclProducts.Tables.Count > 0) Then
    '        Dim temp As DataSet = unFlattenDataset(dsInclProducts)
    '        dsInclProducts = temp
    '        ViewState("dsInclProducts") = temp
    '    End If
    '    If (dsInclProducts.Tables.Count = 0) Then
    '        If (ViewState("dsInclProducts") Is Nothing) Then
    '            createInclusiveProductSchema()
    '        End If
    '    End If
    'Else
    '    dsInclProducts = ViewState("dsInclProducts")
    'End If


    'Dim checkedExc As Boolean = False
    'Dim vehicles As String = ""
    'Dim vehIDs As String = ""

    ''Case- Gridview has no rows implying no vehicle was selected for the inclusive products
    ''which further implies it is associated with all the vehicles

    'If (GridView1.Rows.Count = 0) Then
    '    vehicles = "ALL"
    '    Dim blninExisting As Boolean = checkDuplicatesExisting(inclusiveProductPopupPicker.DataId, vehicles)
    '    Dim blninNew As Boolean = checkDuplicatesNew(inclusiveProductPopupPicker.DataId, vehicles)

    '    If (blninExisting = False And blninNew = False) Then

    '        If (ViewState("dsNewAdditions") Is Nothing) Then
    '            dsNewAddtions = New DataSet
    '            dsNewAddtions = dsInclProducts.Clone()
    '        Else
    '            dsNewAddtions = ViewState("dsNewAdditions")
    '        End If


    '        Dim foundRows As DataRow()
    '        Dim strExpr As String = "prdid = '" & inclusiveProductPopupPicker.DataId & "'"
    '        foundRows = dsNewAddtions.Tables(0).Select(strExpr)

    '        If (foundRows.Length > 0) Then
    '            Dim drEdit As DataRow = foundRows(0)
    '            drEdit.BeginEdit()
    '            drEdit(1) = "ALL"
    '            drEdit(7) = inclusiveProductPopupPicker.DataId
    '            drEdit(8) = "ALL"
    '            drEdit.EndEdit()

    '        Else
    '            If (ViewState("incr") Is Nothing) Then
    '                ViewState("incr") = -1
    '            Else
    '                ViewState("incr") = ViewState("incr") - 1
    '            End If

    '            Dim newIncr As Integer = Convert.ToInt32(ViewState("incr"))

    '            Dim dr As DataRow
    '            dr = dsNewAddtions.Tables(0).NewRow()
    '            dr(0) = inclusiveProductPopupPicker.Text
    '            dr(1) = vehicles
    '            dr(2) = Convert.ToDateTime(Convert.ToDateTime(ckoFromDropdown.SelectedItem.ToString()))
    '            dr(3) = Convert.ToDateTime(Convert.ToDateTime(ckoToTextbox.Text))
    '            dr(4) = "Yes"
    '            dr(5) = newIncr
    '            dr(6) = True
    '            dr(7) = inclusiveProductPopupPicker.DataId
    '            dr(8) = "ALL"
    '            dsNewAddtions.Tables(0).Rows.Add(dr)

    '        End If

    '        dsNewAddtions.AcceptChanges()
    '        ViewState("dsNewAdditions") = dsNewAddtions
    '    End If
    'End If

    ''Case- Gridview has rows implying vehicle/s were selected for the inclusive products
    ''which further implies the product is associated with the selected vehicles

    'If (GridView1.Rows.Count > 0) Then
    '    Dim lb As Label = CType(GridView1.Rows(0).FindControl("lblVehicle"), Label)
    '    Dim lbVehID As Label = CType(GridView1.Rows(0).FindControl("vehID"), Label)
    '    Dim cb1 As CheckBox = CType(GridView1.Rows(0).FindControl("excludeVehicle"), CheckBox)
    '    Dim inc As Integer = 0

    '    Dim blninExisting As Boolean = checkDuplicatesExisting(inclusiveProductPopupPicker.DataId, lbVehID.Text)
    '    Dim blninNew As Boolean = checkDuplicatesNew(inclusiveProductPopupPicker.DataId, lbVehID.Text)


    '    If (blninExisting = False And blninNew = False) Then
    '        vehicles = lb.Text
    '        vehIDs = lbVehID.Text
    '    End If


    '    If (cb1.Checked = True) Then
    '        checkedExc = True
    '    End If

    '    If (GridView1.Rows.Count > 1) Then
    '        For inc = 1 To GridView1.Rows.Count - 1
    '            lb = CType(GridView1.Rows(inc).FindControl("lblVehicle"), Label)
    '            lbVehID = CType(GridView1.Rows(inc).FindControl("vehID"), Label)
    '            blninExisting = checkDuplicatesExisting(inclusiveProductPopupPicker.Text, lbVehID.Text)
    '            blninNew = checkDuplicatesNew(inclusiveProductPopupPicker.Text, lbVehID.Text)

    '            If (blninExisting = False And blninNew = False) Then
    '                vehicles = vehicles + ", " + lb.Text
    '                vehIDs = vehIDs + ", " + lbVehID.Text
    '                If (checkedExc = False) Then
    '                    cb1 = CType(GridView1.Rows(0).FindControl("excludeVehicle"), CheckBox)
    '                    checkedExc = cb1.Checked
    '                End If
    '            End If
    '        Next
    '        If (vehicles.StartsWith(", ") = True) Then
    '            vehicles = vehicles.Substring(1).Trim()
    '            vehIDs = vehIDs.Substring(1).Trim()
    '        End If
    '    End If

    '    If (vehicles <> "") Then

    '        If (ViewState("dsNewAdditions") Is Nothing) Then
    '            dsNewAddtions = New DataSet
    '            dsNewAddtions = dsInclProducts.Clone()
    '        Else
    '            dsNewAddtions = ViewState("dsNewAdditions")
    '        End If

    '        Dim foundRows As DataRow()
    '        Dim strExpr As String = "prdid = '" & inclusiveProductPopupPicker.DataId & "'"
    '        foundRows = dsNewAddtions.Tables(0).Select(strExpr)

    '        If (foundRows.Length > 0) Then
    '            Dim drEdit As DataRow = foundRows(0)
    '            If (drEdit(1) = "ALL") Then
    '                drEdit.BeginEdit()
    '                drEdit(1) = vehicles
    '                drEdit(7) = inclusiveProductPopupPicker.DataId
    '                drEdit(8) = vehicles
    '                drEdit.EndEdit()
    '            Else
    '                drEdit.BeginEdit()
    '                drEdit(1) = drEdit(1) & ", " & vehicles
    '                drEdit(7) = inclusiveProductPopupPicker.DataId

    '                drEdit(8) = drEdit(8) & ", " & vehIDs
    '                drEdit.EndEdit()
    '            End If
    '        Else
    '            If (ViewState("incr") Is Nothing) Then
    '                ViewState("incr") = -1
    '            Else
    '                ViewState("incr") = ViewState("incr") - 1
    '            End If
    '            Dim newIncr As Integer = Convert.ToInt32(ViewState("incr"))

    '            Dim dr As DataRow
    '            dr = dsNewAddtions.Tables(0).NewRow()
    '            dr(0) = inclusiveProductPopupPicker.Text
    '            dr(1) = vehicles
    '            dr(2) = Convert.ToDateTime(Convert.ToDateTime(ckoFromDropdown.SelectedItem.ToString()))
    '            dr(3) = Convert.ToDateTime(Convert.ToDateTime(ckoToTextbox.Text))
    '            If (checkedExc = True) Then

    '                dr(4) = "Yes"
    '            Else
    '                dr(4) = "No"
    '            End If
    '            dr(5) = newIncr
    '            dr(6) = True
    '            dr(7) = inclusiveProductPopupPicker.DataId
    '            dr(8) = vehIDs
    '            dsNewAddtions.Tables(0).Rows.Add(dr)

    '        End If
    '        dsNewAddtions.AcceptChanges()
    '        ViewState("dsNewAdditions") = dsNewAddtions
    '    End If
    'End If

    'GridView1.DataSource = Nothing
    'GridView1.DataBind()
    'inclusiveProductPopupPicker.Text = ""
    'inclusiveProductPopup.Hide()
    'ViewState("dtVehicles") = Nothing
    'ViewState("datasetIsDirtyFlag") = 1
    'End Sub

    ''' <summary>
    ''' Check dsInclusiveProducts dataset for any existing vehicle products
    ''' </summary>
    ''' <param name="prodID"></param>
    ''' <param name="veh"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' 
    Protected Function checkDuplicatesExisting(ByVal prodID As String, ByVal veh As String) As Boolean
        Dim blnExists As Boolean = False
        If (ViewState("dsInclProducts") Is Nothing) Then
            Return False
        End If

        Dim searchExpr As String = "prdID='" & prodID & "' And prdvehID= '" & veh & "'"

        dsInclProducts = ViewState("dsInclProducts")
        Dim foundRows As DataRow()
        foundRows = dsInclProducts.Tables(0).Select(searchExpr)
        For Each dsRow As DataRow In foundRows
            Return True
        Next
        Return blnExists
    End Function

    ''' <summary>
    ''' Check dsNewAdditions dataset for any existing vehicle product as an example see below
    ''' Check if id for a vehicle '2BMA' exists in the group of already associated vehicles
    ''' say '2BBS, 4BBS, 6BM' etc.
    ''' </summary>
    ''' <param name="prodID">non-vehicle product id</param>
    ''' <param name="veh">vehicle product id</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Protected Function checkDuplicatesNew(ByVal prodID As String, ByVal veh As String) As Boolean
        dsNewAddtions = ViewState("dsNewAdditions")
        If (dsNewAddtions Is Nothing) Then
            Return False
        End If

        If (dsNewAddtions.Tables(0).Rows.Count = 0) Then
            Return False
        End If

        Dim blnExists As Boolean = False
        Dim i As Integer = 0

        For i = 0 To dsNewAddtions.Tables(0).Rows.Count - 1
            Dim vehs As String = dsNewAddtions.Tables(0).Rows(i)(8)
            Dim strArr As String() = vehs.Split(",")
            Dim incr As Integer = 0

            For incr = 0 To strArr.Length - 1
                Dim vehicle As String = strArr(incr).Trim()
                If ((String.Compare(prodID, dsNewAddtions.Tables(0).Rows(i)(7), True) = 0) And (String.Compare(veh, vehicle, True) = 0)) Then
                    Return True
                End If
            Next
        Next i

        Return blnExists
    End Function

    ''' <summary>
    ''' Redirect to main page with the current tab -'Inclusive Products'
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Protected Sub redirect()
        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me.CurrentPage, _
          prdId:=Me.SaleableProductRow.SapPrdId, _
      sapId:=Me.SaleableProductRow.SapId, _
      tabName:="Inclusive Products"))
    End Sub

    Protected Sub dateButtonClicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles addPickupDateButtonExt.Load
        If (Page.IsPostBack = True) Then
            ViewState("fromDate") = newDateControlFrom.Date
            Return
        End If
        setDefaultNewFromDate()
    End Sub
#End Region

End Class

