' Change Log!
'
' 3.6.9     -   Shoel - Added this log 
' 3.6.9     -   Shoel - 1 - TCRef column data removed
'                       2 - InfoLink is now 256 chars long
'                       3 - Save/Error messages now consistent with rest of Aurora
'                       4 - Stored Procs have also been rewritten to conform to standards
'                       5 - Transaction moved to frontend
'                       6 - UI layout slightly changed
' 24.7.9    -   Shoel - WebEnabledDate replaced by Web EnabledFrom and To dates!
' 27.7.9    -   Shoel - To date now has 23:59:59 added 


Partial Class Product_Controls_ProductSetupControl
    Inherits AuroraUserControl

#Region "events"
    Private BuildProductDelegate As [Delegate]
    Public WriteOnly Property InvokeDelegate() As [Delegate]
        Set(ByVal value As [Delegate])
            BuildProductDelegate = value
        End Set
    End Property
#End Region

#Region "Public Property"
    Public Property PrdId() As String
        Get
            Return ViewState("prdId")
        End Get
        Set(ByVal value As String)
            ViewState("prdId") = value
        End Set
    End Property
#End Region

#Region "Private Property"

    Private Property isVehicle() As Boolean
        Get
            If ViewState("isVehicle") IsNot Nothing Then
                Return CBool(ViewState("isVehicle"))
            Else
                Return False
            End If

        End Get
        Set(ByVal value As Boolean)
            ViewState("isVehicle") = value
        End Set
    End Property

    Private ReadOnly Property PrdInfoUrl() As String
        Get
            Return Left(InformationLinkTextBox.Text, 256)
        End Get
    End Property

    Private ReadOnly Property NoOfAdults() As Integer
        Get
            Return IIf(NoOfAdultsDropdownlist.SelectedItem.Text = "(Select)", 0, NoOfAdultsDropdownlist.SelectedValue)
        End Get
    End Property

    Private ReadOnly Property NoOfChilds() As Integer
        Get
            Return IIf(NoofChildsDropDownList.SelectedItem.Text = "(Select)", 0, NoofChildsDropDownList.SelectedValue)
        End Get
    End Property

    Private ReadOnly Property NoOfInfants() As Integer
        Get
            Return 0 ''IIf(NoOfInfantsDropDownList.SelectedItem.Text = "(Select)", 0, NoOfInfantsDropDownList.SelectedValue)
        End Get
    End Property

#End Region

#Region "Private Procedure"

    Sub EnabledDropdown(ByVal IsEnabled As Boolean)
        Me.NoOfAdultsDropdownlist.Enabled = IsEnabled
        Me.NoofChildsDropDownList.Enabled = IsEnabled
        ''Me.NoOfInfantsDropDownList.Enabled = IsEnabled
    End Sub

    Sub PopulateDropDown()
        Me.NoOfAdultsDropdownlist.Items.Add("(Select)")
        Me.NoofChildsDropDownList.Items.Add("(Select)")
        ''Me.NoOfInfantsDropDownList.Items.Add("(Select)")

        For i As Integer = 1 To Aurora.Product.Data.GetMaxPaxForWebProductTab(1)
            Me.NoOfAdultsDropdownlist.Items.Add(i)
        Next

        For i As Integer = 1 To Aurora.Product.Data.GetMaxPaxForWebProductTab(2)
            Me.NoofChildsDropDownList.Items.Add(i)
        Next
    End Sub

    Sub GetProductForB2Csetup(ByVal PrdId As String)
        If String.IsNullOrEmpty(PrdId) Then Return
        Dim result As System.Data.DataSet

        Try
            result = Aurora.Product.Data.GetProductForB2CSetup(PrdId)
            If result.Tables.Count > 0 Then

                ''check first the type of vehicle by calling the second table w/c store IsVehicle field
                If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdIsVehicle").GetType().Equals(GetType(DBNull)) Then
                    isVehicle = CBool(DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdIsVehicle"))
                End If

                EnabledDropdown(isVehicle)
                trNote.Visible = Not isVehicle
                If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdWebEnabledFromDate").GetType().Equals(GetType(DBNull)) Then
                    dtcWebEnabledFromDate.Date = CDate(DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdWebEnabledFromDate"))
                Else
                    dtcWebEnabledFromDate.Text = ""
                End If
                If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdWebEnabledToDate").GetType().Equals(GetType(DBNull)) Then
                    dtcWebEnabledToDate.Date = CDate(DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdWebEnabledToDate"))
                Else
                    dtcWebEnabledToDate.Text = ""
                End If

                If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdInfoURL").GetType().Equals(GetType(DBNull)) Then
                    InformationLinkTextBox.Text = DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdInfoURL")
                Else
                    InformationLinkTextBox.Text = ""
                End If

                If isVehicle Then

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdNoOfAdult").GetType().Equals(GetType(DBNull)) Then
                        NoOfAdultsDropdownlist.SelectedValue = DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdNoOfAdult")
                    Else
                        NoOfAdultsDropdownlist.SelectedValue = ""
                    End If

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdNoOfAdult").GetType().Equals(GetType(DBNull)) Then
                        NoofChildsDropDownList.SelectedValue = DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdNoOfChild")
                    Else
                        NoofChildsDropDownList.SelectedValue = ""
                    End If

                    'Dim _PrdNoOfInfant As String = DataBinder.Eval(result, "Tables(0).DefaultView(0).PrdNoOfInfant")
                    '' Me.NoOfInfantsDropDownList.SelectedValue = _PrdNoOfInfant
                End If

            End If

        Catch ex As Exception
            CurrentPage.SetErrorMessage("Error loading B2C Product Tab")
        End Try
    End Sub

    Sub UpdateProductForB2CSetup( _
                                           ByVal PrdId As String, _
                                           ByVal PrdWebEnabledFromDate As Date, _
                                           ByVal PrdWebEnabledToDate As Date, _
                                           ByVal PrdInfoURL As String, _
                                           ByVal PrdNoOfAdult As Integer, _
                                           ByVal PrdNoOfChild As Integer, _
                                           ByVal PrdNoOfInfant As Integer)


        Dim retvalue As String = ""
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                retvalue = Aurora.Product.Data.UpdateProductForB2CSetup(PrdId, _
                                                                       PrdWebEnabledFromDate, _
                                                                       PrdWebEnabledToDate, _
                                                                       PrdInfoURL, _
                                                                       PrdNoOfAdult, _
                                                                       PrdNoOfChild, _
                                                                       PrdNoOfInfant, _
                                                                       CurrentPage.UserCode)

                If retvalue.ToUpper().Contains("ERROR") Then Throw New Exception(retvalue.Split("/"c)(1))

                If retvalue.Contains("SUCCESS") Then CurrentPage.SetInformationMessage(retvalue.Split("/"c)(1))
                oTransaction.CommitTransaction()
            Catch ex As Exception
                CurrentPage.SetErrorMessage(ex.Message)
                oTransaction.RollbackTransaction()
            End Try
        End Using
    End Sub

#End Region

#Region "Private Function"
    Function AreFieldsValid() As Boolean

        If String.IsNullOrEmpty(dtcWebEnabledFromDate.Text) Then
            MyBase.CurrentPage.SetWarningShortMessage("Web Enabled From date is mandatory")
            Return False
        End If
        If String.IsNullOrEmpty(dtcWebEnabledToDate.Text) Then
            MyBase.CurrentPage.SetWarningShortMessage("Web Enabled To date is mandatory")
            Return False
        End If

        If dtcWebEnabledFromDate.Date > dtcWebEnabledToDate.Date Then
            CurrentPage.SetWarningShortMessage("Web Enabled From date cannot be after Web Enabled To date")
            Return False
        End If

        Return True

    End Function
#End Region

#Region "SP used"
    ''ProductSetup_UpdateProductForB2CSetup
    ''ProductSetup_GetProductForB2CSetup
#End Region

#Region "Page Event"
    ''
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            PopulateDropDown()
            GetProductForB2Csetup(PrdId)
        End If
    End Sub
#End Region

#Region "Button Event"
    Protected Sub SaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        If Not AreFieldsValid() Then Return
        UpdateProductForB2CSetup(PrdId, _
                                 dtcWebEnabledFromDate.Date, _
                                 dtcWebEnabledToDate.Date.AddHours(23).AddMinutes(59).AddSeconds(59), _
                                 PrdInfoUrl, _
                                 NoOfAdults, _
                                 NoOfChilds, _
                                 NoOfInfants)

        Dim obj(0) As Object
        obj(0) = True
        BuildProductDelegate.DynamicInvoke(obj)
        ''Response.Redirect("Product.aspx?prdId=" & Request.QueryString(0).ToString & "&tabs=2")

    End Sub
#End Region


End Class
