<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductSetupControl.ascx.vb"
    Inherits="Product_Controls_ProductSetupControl" %>
<%--<%@ Register Src="ProductSetupNonVehicleControl.ascx" TagName="ProductSetupNonVehicleControl"
    TagPrefix="uc2" %>
--%>    
<%@ Register Src="../../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>

<table style="width: 500px" cellpadding="2" cellspacing="0">
    <tr runat="server" id="trEnabledDate">
        <td nowrap="nowrap">
            Web Enabled From Date:</td>
        <td>
            <uc1:datecontrol id="dtcWebEnabledFromDate" runat="server" width="200px" />
        </td>
        <td nowrap="nowrap">
            Web Enabled To Date:</td>
        <td>
            <uc1:datecontrol id="dtcWebEnabledToDate" runat="server" width="200px" />
        </td>
    </tr>
    <tr>
        <td nowrap="nowrap">
            Information Link:</td>
        <td colspan="3">
            <asp:TextBox ID="InformationLinkTextBox" runat="server" Width="600px" MaxLength="256" /></td>
    </tr>
    <tr id="trNote" runat="server">
        <td>
            </td>
        <td>
             <i>Available only on <b>Vehicle-Type</b> products</i>
        </td>
    </tr>
    <tr>
        <td>
            No of Adults:</td>
        <td>
            <asp:DropDownList ID="NoOfAdultsDropdownlist" runat="server" />
        </td>
    </tr>
    <tr>
        <td>
            No of Children:
        </td>
        <td>
            <asp:DropDownList ID="NoofChildsDropDownList" runat="server" />
        </td>
    </tr>
    <%--<tr>
        <td>
            No of Infants:
        </td>
        <td>
            <asp:DropDownList ID="NoOfInfantsDropDownList" runat="server" />
        </td>
    </tr>--%>
</table>
<hr />
<table width="100%">
    <tr>
        <td align="Right">
           
                <asp:Button ID="SaveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                Width="100px" />
           
            </td>
    </tr>
    <tr>
        <td>
         <%--   <uc2:ProductSetupNonVehicleControl ID="ProductSetupNonVehicleControl1" runat="server" />--%>
        </td>
    </tr>
</table>
