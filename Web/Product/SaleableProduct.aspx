<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false" CodeFile="SaleableProduct.aspx.vb" Inherits="Product_SaleableProduct" Title="Untitled Page" ValidateRequest="False" %>

<%@ Register Src="Controls/PackagesControl.ascx" TagName="PackagesControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/RatesControl.ascx" TagName="RatesControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/LocationsControl.ascx" TagName="LocationsControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/NonAvailabilityControl.ascx" TagName="NonAvailabilityControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/AttributesControl.ascx" TagName="AttributesControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/PersonTypeControl.ascx" TagName="PersonTypeControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/InclusiveProductsControl.ascx" TagName="InclusiveProductsControl" TagPrefix="uc1" %>
<%--<%@ Register Src="Controls/VehicleNumberAvailabilityControl.ascx" TagName="VehicleNumberAvailableControl" TagPrefix="uc1" %>--%>
<asp:Content ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <asp:Panel ID="saleableProductEditPanel" runat="Server" Width="780px">
        <asp:UpdatePanel runat="server" ID="saleableProductUpdatePanel">
            <ContentTemplate>
                <table style="width:100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width:150px;">Product:</td>
                        <td colspan="3"><asp:TextBox ID="productNameTextBox" runat="server" style="width:550px" ReadOnly="true" CssClass="inputreadonly" /></td>
                    </tr>                
                    <tr>
                        <td>Suffix:</td>
                        <td colspan="3"><asp:TextBox ID="saleableProductSuffixTextBox" runat="server" style="width:100px" ReadOnly="True" CssClass="inputreadonly" /></td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td style="width:250px;"><asp:DropDownList ID="countryDropDown" runat="server" Width="200px" /></td>
                        <td style="width:150px;">Currency:</td>
                        <td><asp:DropDownList ID="currencyDropDown" runat="server" Width="200px" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td style="border-top-style: dotted; border-top-width: 1px">Flags:</td>
                        <td style="border-top-style: dotted; border-top-width: 1px" colspan="3">
                            <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="repeatLabelTable">
                                <asp:ListItem Value="SapIsBase">Base</asp:ListItem>
                                <asp:ListItem Value="SapIsFlex">Flex</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
                
                <table style="width:100%;" cellpadding="2" cellspacing="0" id="statusTable" runat="server">
                    <tr>
                        <td style="width:150px; border-width: 1px 0px">Status:</td>
                        <td style="border-width: 1px 0px">
                            <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                                <asp:ListItem Text="Active" Selected="True" />
                                <asp:ListItem Text="Pending" />
                                <asp:ListItem Text="Inactive" />
                            </asp:RadioButtonList>
                        </td>
                        <td style="border-width: 1px 0px">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="statusInfoLabel" runat="server" Font-Italic="true">(Status cannot be changed if product is inactive)</asp:Label>
                        </td>
                    </tr>
                </table>
                
                <table style="width:100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td colspan="4" align="right" style="border-bottom-width: 1px">
                            <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="False" />
                            <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="False" />
                            <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <br />
            
        <asp:Panel ID="saleableProductTabContainer" runat="server">
            <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="0" Width="100%">
                <ajaxToolkit:TabPanel runat="server" ID="packagesTabPanel" HeaderText="Packages">
                    <ContentTemplate>
                        <uc1:PackagesControl ID="packagesControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="ratesTabPanel" HeaderText="Rates">
                    <ContentTemplate>
                        <uc1:RatesControl ID="ratesControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="nonAvailabilityTabPanel" HeaderText="Non-Availability">
                    <ContentTemplate>
                        <uc1:NonAvailabilityControl ID="nonAvailabilityControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="locationsTabPanel" HeaderText="Locations">
                    <ContentTemplate>
                        <uc1:LocationsControl ID="locationsControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="attributesTabPanel" HeaderText="Attributes">
                    <ContentTemplate>
                        <uc1:AttributesControl ID="attributesControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="personTypesTabPanel" HeaderText="Person Type Discounts">
                    <ContentTemplate>
                        <uc1:PersonTypeControl ID="personTypeControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="inclusiveProductsTabPanel" HeaderText="Inclusive Products">
                    <ContentTemplate>
                        <uc1:InclusiveProductsControl ID="inclusiveProductsControl" runat="server" />
                    </ContentTemplate>
        	    </ajaxToolkit:TabPanel>
                <%--<ajaxToolkit:TabPanel runat="server" ID="vehicleNumberAvailabilityTabPanel" HeaderText="Vehicle # Availability">
                    <ContentTemplate>
                        <uc1:VehicleNumberAvailableControl runat="server" id="vehicleNumberAvailableControl" /> 
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>--%>
            </ajaxToolkit:TabContainer>
        
        </asp:Panel>

    </asp:Panel>
    
    <br />
    
</asp:Content>
