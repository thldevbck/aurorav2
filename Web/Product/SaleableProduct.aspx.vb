Imports System.Collections.Generic
Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<AuroraPageTitleAttribute("Saleable Product")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductEnquiry)> _
<AuroraMenuFunctionCodeAttribute("SALABPROD")> _
Partial Class Product_SaleableProduct
    Inherits AuroraPage

    Private _initException As Exception

    Private _prdId As String = Nothing
    Private _sapId As String = Nothing
    Private _productDataSet As New ProductDataSet()
    Private _productRow As ProductRow
    Private _saleableProductRow As SaleableProductRow

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.ProductMaintenance)
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If String.IsNullOrEmpty(_sapId) Then
                Return "New Saleable Product"
            ElseIf CanMaintain Then
                Return "Edit Saleable Product"
            Else
                Return "View Saleable Product"
            End If
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            _prdId = Request.QueryString("prdId").Trim()
            If Not String.IsNullOrEmpty(Request.QueryString("sapId")) Then
                _sapId = Request.QueryString("sapId").Trim()
            End If

            MyBase.OnInit(e)

            _productDataSet.EnforceConstraints = False

            DataRepository.GetProductLookups(_productDataSet, Me.CompanyCode)
            DataRepository.GetSaleableProductLookups(_productDataSet, Me.CompanyCode)

            If Not String.IsNullOrEmpty(_sapId) Then
                DataRepository.GetSaleableProduct(_productDataSet, Me.CompanyCode, _sapId)
                If _productDataSet.SaleableProduct.Count <= 0 Then
                    Throw New Exception("No saleable product found")
                ElseIf _productDataSet.SaleableProduct.Count > 1 Then
                    Throw New Exception("Unexpected saleable product result")
                Else
                    _saleableProductRow = _productDataSet.SaleableProduct(0)
                    _productRow = _saleableProductRow.ProductRow
                End If

                For Each productUserControl As ProductUserControl In New ProductUserControl() { _
                 ratesControl, _
                 packagesControl, _
                 nonAvailabilityControl, _
                 locationsControl, _
                 attributesControl, _
                 personTypeControl, _
                 inclusiveProductsControl}
                    productUserControl.InitProductUserControl(_productDataSet, _saleableProductRow)
                Next
            Else
                DataRepository.GetProduct(_productDataSet, Me.CompanyCode, _prdId)
                If _productDataSet.Product.Count <= 0 Then
                    Throw New Exception("No product found")
                ElseIf _productDataSet.Product.Count > 1 Then
                    Throw New Exception("Unexpected product result")
                Else
                    _productRow = _productDataSet.Product(0)
                End If
            End If

            countryDropDown.Items.Clear()
            For Each countryRow As CountryRow In _productDataSet.Country
                countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
            Next

            currencyDropDown.Items.Clear()
            For Each codeRow As CodeRow In _productDataSet.Code
                If codeRow.CodCdtNum = codeRow.CodeType_Currency Then _
                    currencyDropDown.Items.Add(New ListItem(codeRow.CodCode & " - " & codeRow.CodDesc, codeRow.CodId))
            Next
            currencyDropDown.Items.Insert(0, New ListItem("", ""))
            currencyDropDown.SelectedIndex = 0
        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Private Sub BuildSaleableProduct(ByVal useViewState As Boolean)
        If _productRow IsNot Nothing Then
            productNameTextBox.Text = _productRow.LongDescription
        End If

        If _saleableProductRow Is Nothing Then
            saleableProductTabContainer.Visible = False
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(ProductConstants.InactiveColor)

            saleableProductSuffixTextBox.Text = CStr(Aurora.Product.Data.DataRepository.GetMaxSapSuffix(Me.CompanyCode, Me._productRow.PrdId) + 1)

            statusRadioButtonList.Items(1).Selected = True ' ProductConstants.SaleableProductStatus_Pending

            updateButton.Visible = False
            createButton.Visible = CanMaintain
        Else
            saleableProductTabContainer.Visible = True
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(_saleableProductRow.StatusColor())

            saleableProductSuffixTextBox.Text = _saleableProductRow.SapSuffix.ToString()

            Try
                countryDropDown.SelectedValue = _saleableProductRow.SapCtyCode
            Catch
            End Try
            countryDropDown.Enabled = False

            Try
                currencyDropDown.SelectedValue = _saleableProductRow.SapCodCurrId
            Catch
            End Try
            currencyDropDown.Enabled = CanMaintain

            For Each listItem As ListItem In flagsCheckBoxList.Items
                listItem.Selected = _saleableProductRow.Table.Columns.Contains(listItem.Value) _
                 AndAlso Not _saleableProductRow.IsNull(listItem.Value) _
                 AndAlso CType(_saleableProductRow(listItem.Value), Boolean)
            Next
            flagsCheckBoxList.Enabled = CanMaintain

            statusRadioButtonList.Items(0).Selected = _saleableProductRow.SapStatus = ProductConstants.SaleableProductStatus_Active
            statusRadioButtonList.Items(1).Selected = _saleableProductRow.SapStatus = ProductConstants.SaleableProductStatus_Pending
            statusRadioButtonList.Items(2).Selected = Not statusRadioButtonList.Items(0).Selected AndAlso Not statusRadioButtonList.Items(1).Selected

            updateButton.Visible = CanMaintain
            createButton.Visible = False
        End If

        statusRadioButtonList.Enabled = CanMaintain AndAlso _productRow IsNot Nothing AndAlso _productRow.IsActive
        statusInfoLabel.Visible = CanMaintain AndAlso Not statusRadioButtonList.Enabled
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack OrElse _initException IsNot Nothing Then Return

        'Modified by: Raj / 14 March, 2011
        'Display 'Inclusive Products' tab only if 'PrdIsInclusive=true' and 'PrdIsInclusive is not null'
        Try
            If (_productRow Is Nothing) Then
                inclusiveProductsTabPanel.Visible = False
            End If

            If (_productRow.IsPrdIsInclusiveNull = True) Then
                inclusiveProductsTabPanel.Visible = False
            Else
                inclusiveProductsTabPanel.Visible = True
            End If
        Catch ex As Exception
            inclusiveProductsTabPanel.Visible = False
        End Try
        'Modified by: Raj / 14 March, 2011

        If Not String.IsNullOrEmpty(Me.Request.QueryString(ProductUserControl.TabParam)) Then
            For i As Integer = 0 To tabs.Tabs.Count - 1
                If tabs.Tabs(i).HeaderText = Me.Request.QueryString(ProductUserControl.TabParam) Then
                    tabs.ActiveTabIndex = i
                End If
            Next
        End If

        BuildSaleableProduct(False)

        If Not String.IsNullOrEmpty(Request.QueryString("Create")) Then
            Me.AddInformationMessage("Saleable Product created")
        End If
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        If Not String.IsNullOrEmpty(Me.Request.QueryString("BackUrl")) Then
            Response.Redirect(Me.Request.QueryString("BackUrl"))
        Else
            Response.Redirect(ProductUserControl.MakeProductUrl(Me, prdId:=_productRow.PrdId))
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        If currencyDropDown.SelectedValue = String.Empty Then
            Me.SetWarningShortMessage("Currency must be selected")
            Exit Sub
        End If

        _saleableProductRow.SapCtyCode = countryDropDown.SelectedValue
        _saleableProductRow.SapCodCurrId = currencyDropDown.SelectedValue
        _saleableProductRow.SapCodCurrCode = _productDataSet.Code.FindById(_saleableProductRow.SapCodCurrId).CodCode
        For Each listItem As ListItem In flagsCheckBoxList.Items
            If _saleableProductRow.Table.Columns.Contains(listItem.Value) Then
                _saleableProductRow(listItem.Value) = listItem.Selected
            End If
        Next
        _saleableProductRow.SapStatus = statusRadioButtonList.SelectedValue

        Aurora.Common.Data.ExecuteDataRow(_saleableProductRow, Me.UserCode, Me.PrgmName)

        statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(_saleableProductRow.StatusColor())

        Me.AddInformationMessage("Saleable product updated")
    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If Not CanMaintain Then Return

        If currencyDropDown.SelectedValue = String.Empty Then
            Me.SetWarningShortMessage("Currency must be selected")
            Exit Sub
        End If

        _saleableProductRow = _productDataSet.SaleableProduct.NewSaleableProductRow()
        _saleableProductRow.SapId = Aurora.Product.Data.DataRepository.GetNewId()
        _saleableProductRow.SapPrdId = _productRow.PrdId
        _saleableProductRow.SapSuffix = Aurora.Product.Data.DataRepository.GetMaxSapSuffix(Me.CompanyCode, Me._productRow.PrdId) + 1
        _saleableProductRow.SapCtyCode = countryDropDown.SelectedValue
        _saleableProductRow.SapCodCurrId = currencyDropDown.SelectedValue
        _saleableProductRow.SapCodCurrCode = _productDataSet.Code.FindById(_saleableProductRow.SapCodCurrId).CodCode
        For Each listItem As ListItem In flagsCheckBoxList.Items
            If _saleableProductRow.Table.Columns.Contains(listItem.Value) Then
                _saleableProductRow(listItem.Value) = listItem.Selected
            End If
        Next
        _saleableProductRow.SapStatus = statusRadioButtonList.SelectedValue
        _productDataSet.SaleableProduct.AddSaleableProductRow(_saleableProductRow)

        Aurora.Common.Data.ExecuteDataRow(_saleableProductRow, Me.UserCode, Me.PrgmName)

        Response.Redirect(ProductUserControl.MakeSaleableProductUrl(Me, _
            prdId:=_productRow.PrdId, _
            sapId:=_saleableProductRow.SapId) & "&create=true")
    End Sub
End Class
