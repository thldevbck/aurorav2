<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SaleableProductSearch.aspx.vb" Inherits="Product_SaleableProductSearch" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .searchResultTable td { vertical-align: top; }
        .searchResultTable ul { margin: 0 0 0 16px; padding: 0; }
        .searchResultTable li { margin: 0px; padding: 0; list-style-type: square; }
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="productSearchPanel" runat="Server" Width="780px" CssClass="productSearch" DefaultButton="searchButton">
    
        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width:150px">Short Name:</td>
                <td style="width:250px"><asp:TextBox ID="shortNameTextBox" runat="server" style="width:100px" MaxLength="12" /></td>
                <td style="width:100px">Name:</td>
                <td><asp:TextBox ID="nameTextBox" runat="server" style="width:250px" MaxLength="64" /></td>
            </tr>
            <tr>
                <td>Brand:</td>
                <td>
                    <asp:DropDownList ID="brandDropDown" runat="server" style="width:200px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>Type:</td>
                <td>
                    <asp:DropDownList ID="typeDropDown" runat="server" style="width:250px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Country:</td>
                <td><asp:DropDownList ID="countryDropDown" runat="server" Width="200px" /></td>
                <td>Flags:</td>
                <td>
                    <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Value="sapIsBase">Base</asp:ListItem>
                        <asp:ListItem Value="sapIsFlex">Flex</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <tr>
                <td>Contains Package:</td>
                <td colspan="3"><uc1:PickerControl ID="packagePicker" runat="server" Width="400px" PopupType="PACKAGE" AppendDescription="true" /></td>
            </tr>
            <tr>
                <td style="border-top-width: 1px; border-top-style: dotted">Status:</td>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="6" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <%--<asp:ListItem Text="Current+Future" Value="Current" Selected="true" />
                        <asp:ListItem Text="Active" Value="Active" />
                        <asp:ListItem Text="Pending" Value="Pending" />
                        <asp:ListItem Text="Inactive" Value="Inactive" />
                        <asp:ListItem Text="All" Value="All" />--%>
                        <asp:ListItem Text="Active-Current"  />
                        <asp:ListItem Text="Active-Future" />
                        <asp:ListItem Text="Active-Past" />
                        <asp:ListItem Text="Pending" />
                        <asp:ListItem Text="Inactive" />
                        <asp:ListItem Text="All" Value="All" Selected="True"/>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>

        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="border-width: 1px 0px"><i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;</td>
                <td align="right" style="border-width: 1px 0px">
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                </td>
            </tr>
        </table>
        
        <br />
        
        <asp:Table ID="searchResultTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" Visible="False" CssClass="dataTableColor searchResultTable">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Text="Name" />
                <asp:TableHeaderCell Text="Brd" Width="25px" />
                <asp:TableHeaderCell Text="Cty" Width="25px" />
                <asp:TableHeaderCell Text="Cur" Width="30px" />
                <asp:TableHeaderCell Text="Attributes" Width="200px" />
                <asp:TableHeaderCell Text="Current Rate Summary" Width="220px" />
                <asp:TableHeaderCell Text="Flags" Width="60px" />
                <asp:TableHeaderCell Text="Status" Width="60px" />
            </asp:TableHeaderRow>
        </asp:Table>

    </asp:Panel>

</asp:Content>

