<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Product_Search" MasterPageFile="~/Include/AuroraHeader.master" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="productSearchPanel" runat="Server" Width="780px" CssClass="productSearch" DefaultButton="searchButton">
    
        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width:150px">Short Name:</td>
                <td style="width:250px"><asp:TextBox ID="shortNameTextBox" runat="server" style="width:150px" MaxLength="8" /></td>
                <td style="width:150px">Name:</td>
                <td style="width:250px"><asp:TextBox ID="nameTextBox" runat="server" style="width:200px" MaxLength="64" /></td>
            </tr>
            <tr>
                <td style="width:100px;">Brand:</td>
                <td>
                    <asp:DropDownList ID="brandDropDown" runat="server" style="width:150px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>Type:</td>
                <td>
                    <asp:DropDownList ID="typeDropDown" runat="server" style="width:250px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">Features:</td>
                <td colspan="1" style="border-top-width: 1px; border-top-style: dotted">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:CheckBoxList ID="featuresCheckBoxList" runat="server" RepeatDirection="Vertical" RepeatLayout="Table" RepeatColumns="2" Width="100%">
                        <asp:ListItem Text="Feature 1" />
                        <asp:ListItem Text="Feature 2" />
                        <asp:ListItem Text="Feature 3" />
                    </asp:CheckBoxList>
                </td>
                <td colspan="1" style="border-top-width: 1px; border-top-style: dotted">&nbsp;</td>
            </tr>
            <%--SHOEL - Phoenix Change--%>
            <tr>
                <td style="border-top-width: 1px; border-top-style: dotted">Display Web Enabled:</td>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:CheckBoxList ID="cblWebEnabledProductType" runat="server" RepeatColumns="2" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Vehicle" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Non-Vehicle" Value="2"></asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
            <%--SHOEL - Phoenix Change--%>
            <tr>
                <td style="border-top-width: 1px; border-top-style: dotted">Status:</td>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="All" Selected="True" />
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Inactive" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>

        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="border-width: 1px 0px"><i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;</td>
                <td align="right" style="border-width: 1px 0px">
                    <asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                </td>
            </tr>
        </table>
        
        <br />
        
        <asp:Table ID="searchResultTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" Visible="False" CssClass="dataTableColor">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Text="Name" />
                <asp:TableHeaderCell Text="Brand" Width="100px" />
                <asp:TableHeaderCell Text="Type" Width="175px" />
                <asp:TableHeaderCell Text="Features" Width="200px" />
                <asp:TableHeaderCell Text="Status" Width="50px" />
            </asp:TableHeaderRow>
        </asp:Table>

    </asp:Panel>

</asp:Content>

