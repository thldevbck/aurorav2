Imports System.Collections.Generic

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<AuroraPageTitleAttribute("View All Rate Bands for Rate Set")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductEnquiry)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.SaleableProduct)> _
Partial Class Product_RateSetReport
    Inherits AuroraPage

    Private _initException As Exception

    Private currentDate As Date = Date.Now

    Private _sapId As String = Nothing
    Private _tbsId As String = Nothing
    Private _productDataSet As New ProductDataSet()
    Private _productRow As ProductRow
    Private _saleableProductRow As SaleableProductRow
    Private _travelBandSetRow As TravelBandSetRow = Nothing

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            _sapId = Request.QueryString("sapId").Trim()
            _tbsId = Request.QueryString("tbsId").Trim()

            MyBase.OnInit(e)

            _productDataSet.EnforceConstraints = False

            DataRepository.GetProductLookups(_productDataSet, Me.CompanyCode)
            DataRepository.GetSaleableProductLookups(_productDataSet, Me.CompanyCode)

            DataRepository.GetSaleableProduct(_productDataSet, Me.CompanyCode, _sapId)
            _saleableProductRow = _productDataSet.SaleableProduct.FindBySapId(_sapId)
            If _saleableProductRow Is Nothing Then
                Throw New Exception("No saleable product found")
            End If
            _productRow = _saleableProductRow.ProductRow

            _travelBandSetRow = _productDataSet.TravelBandSet.FindByTbsId(_tbsId)
            If _travelBandSetRow Is Nothing Then
                Throw New Exception("No rate set found")
            End If

            DataRepository.GetTravelBandSet(_productDataSet, Me.CompanyCode, _tbsId)
            DataRepository.GetRates(_productDataSet, Me.CompanyCode, _tbsId, Nothing)

        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Private Function InitTableHeaderRow(ByVal table As Table, ByVal cssClass As String) As TableHeaderRow
        Dim result As New TableHeaderRow
        If Not String.IsNullOrEmpty(cssClass) Then result.CssClass = cssClass
        table.Rows.Add(result)
        Return result
    End Function

    Private Function InitTableHeaderCell(ByVal tableHeaderRow As TableHeaderRow, ByVal cssClass As String, ByVal columnSpan As Integer, ByVal text As String) As TableHeaderCell
        Dim result As New TableHeaderCell()
        If Not String.IsNullOrEmpty(cssClass) Then result.CssClass = cssClass
        If columnSpan <> 1 Then result.ColumnSpan = columnSpan
        If Not String.IsNullOrEmpty(text) Then result.Text = text
        tableHeaderRow.Controls.Add(result)
        Return result
    End Function

    Private Function InitTableRow(ByVal table As Table, ByVal cssClass As String, ByVal cssStyle As String) As TableRow
        Dim result As New TableRow
        If Not String.IsNullOrEmpty(cssClass) Then result.CssClass = cssClass
        If Not String.IsNullOrEmpty(cssStyle) Then result.Attributes.Add("style", cssStyle)
        table.Rows.Add(result)
        Return result
    End Function

    Private Function InitTableCell(ByVal tableRow As TableRow, ByVal cssStyle As String, ByVal columnSpan As Integer, ByVal text As String) As TableCell
        Dim result As New TableCell
        If Not String.IsNullOrEmpty(cssStyle) Then result.Attributes.Add("style", cssStyle)
        If columnSpan <> 1 Then result.ColumnSpan = columnSpan
        If Not String.IsNullOrEmpty(text) Then result.Text = text
        tableRow.Controls.Add(result)
        Return result
    End Function

    Private Sub InitProductRateTravelBand_Rate(ByVal productRateTravelBandRow As ProductRateTravelBandRow, ByVal leftPadding As Integer, ByVal name As String, ByRef rowIndex As Integer, ByVal rate As IProductRate)

        Dim tableRow As TableRow = InitTableRow(productRateTravelBandTable, IIf((rowIndex Mod 2) = 0, "evenRow", "oddRow"), Nothing) : rowIndex += 1
        InitTableCell(tableRow, IIf(leftPadding <= 0, "", "padding-left: " & CStr(leftPadding) & "px"), 1, Server.HtmlEncode(name))
        InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(rate.QtyDescription))
        InitTableCell(tableRow, Nothing, 1, Server.HtmlEncode(rate.UomDescription))
        InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(ProductConstants.RateToString(rate.Rate)) & "&nbsp;")
        InitTableCell(tableRow, Nothing, 1, "&nbsp;")
        InitTableCell(tableRow, Nothing, 1, Server.HtmlEncode(rate.Description))

        Dim productRateDiscounts As ProductRateDiscountRow() = productRateTravelBandRow.GetProductRateDiscountForParent(rate.Id)
        For Each productRateDiscountRow As ProductRateDiscountRow In productRateDiscounts
            tableRow = InitTableRow(productRateTravelBandTable, IIf((rowIndex Mod 2) = 0, "evenRow", "oddRow"), Nothing) : rowIndex += 1
            InitTableCell(tableRow, "padding-left: " & CStr(leftPadding + 20) & "px", 1, Server.HtmlEncode(productRateDiscountRow.TypeDescription))
            InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(productRateDiscountRow.QtyDescription))
            InitTableCell(tableRow, Nothing, 1, Server.HtmlEncode(productRateDiscountRow.UomDescription))
            InitTableCell(tableRow, "text-align: right", 1, Server.HtmlEncode(productRateDiscountRow.RateDescription) & "&nbsp;")
            InitTableCell(tableRow, Nothing, 1, "&nbsp;")
            InitTableCell(tableRow, Nothing, 1, Server.HtmlEncode(productRateDiscountRow.Description))
        Next
    End Sub

    Private Sub InitRate(ByVal productRateTravelBandRow As ProductRateTravelBandRow, ByVal name As String, ByRef rowIndex As Integer, ByVal rates As IProductRate())
        If rates Is Nothing OrElse rates.Length < 1 Then Return

        For Each rate As IProductRate In rates
            InitProductRateTravelBand_Rate(productRateTravelBandRow, 0, name, rowIndex, rate)
        Next
    End Sub

    Private Sub InitRateBand(ByVal productRateTravelBandRow As ProductRateTravelBandRow)

        Dim tableRow As TableRow = InitTableRow(productRateTravelBandTable, Nothing, "background-color:" & System.Drawing.ColorTranslator.ToHtml(productRateTravelBandRow.StatusColor(currentDate)))
        InitTableCell(tableRow, "border-top-width: 1px", 6, Server.HtmlEncode(productRateTravelBandRow.LongDescription))

        Dim rowIndex As Integer = 0
        InitRate(productRateTravelBandRow, "Base", rowIndex, New IProductRate() {productRateTravelBandRow})
        InitRate(productRateTravelBandRow, "Agent Dependent", rowIndex, productRateTravelBandRow.GetAgentDependentRateRows())
        InitRate(productRateTravelBandRow, "Vehicle Dependent", rowIndex, productRateTravelBandRow.GetVehicleDependentRateRows())

        For Each productRateBookingBandRow As ProductRateBookingBandRow In productRateTravelBandRow.GetProductRateBookingBandRows()
            InitProductRateTravelBand_Rate(productRateTravelBandRow, 0, "Booked Date Availablity", rowIndex, productRateBookingBandRow)
            For Each locationDependentRateRow As LocationDependentRateRow In productRateBookingBandRow.GetLocationDependentRateRows()
                InitProductRateTravelBand_Rate(productRateTravelBandRow, 20, "Location Dependent", rowIndex, locationDependentRateRow)
            Next
        Next

        InitRate(productRateTravelBandRow, "Person Dependent", rowIndex, productRateTravelBandRow.GetPersonDependentRateRows())
        InitRate(productRateTravelBandRow, "Fixed", rowIndex, productRateTravelBandRow.GetFixedRateRows())
        InitRate(productRateTravelBandRow, "Person", rowIndex, productRateTravelBandRow.GetPersonRateRows())
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _initException IsNot Nothing Then Return

        productNameTextBox.Text = _productRow.LongDescription
        productNameTextBox.BackColor = _productRow.StatusColor
        saleableProductNameTextBox.Text = _saleableProductRow.Description
        saleableProductNameTextBox.BackColor = _saleableProductRow.StatusColor(currentDate)
        rateSetTextBox.Text = _travelBandSetRow.Description
        rateSetTextBox.BackColor = _travelBandSetRow.StatusColor(currentDate)

        While productRateTravelBandTable.Rows.Count > 1
            productRateTravelBandTable.Rows.RemoveAt(1)
        End While

        Dim productRateTravelBandRows As ProductRateTravelBandRow() = _travelBandSetRow.GetProductRateTravelBandRows()
        Array.Reverse(productRateTravelBandRows)
        For Each productRateTravelBandRow As ProductRateTravelBandRow In productRateTravelBandRows
            InitRateBand(productRateTravelBandRow)
        Next
    End Sub

End Class
