Imports Aurora.Common

Partial Class Initial
    Inherits System.Web.UI.Page

    Protected Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
        Dim sUserSetting As UserSettings

        Dim sUserCode As String = Request.ServerVariables("LOGON_USER")
        sUserCode = sUserCode.Replace("THL\", "")
        sUserCode = sUserCode.Replace("thl\", "")
        sUserSetting = UserSettings.Load(sUserCode)
        Dim sIniFun As String = sUserSetting.FunFileName
        Response.Redirect(sIniFun)
    End Sub

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        
    End Sub
End Class
