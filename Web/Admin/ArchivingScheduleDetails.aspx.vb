Imports System.Data

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DatabaseArchiving)> _
Partial Class Admin_ArchivingScheduleDetails
    Inherits AuroraPage

    Public Property ScheduleDataTable() As DataTable
        Get
            Return CType(Session("ScheduleDataTable"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            Session("ScheduleDataTable") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim iScheduleId As Long = CLng(Request("ScheduleId"))
            ScheduleDataTable = Aurora.Admin.Services.Archiving.GetScheduleDetails(iScheduleId)
            BindGrid()
        End If
    End Sub

    Sub BindGrid()
        grdArchivingScheduleDetails.DataSource = ScheduleDataTable
        grdArchivingScheduleDetails.DataBind()
    End Sub


    Protected Sub grdArchivingScheduleDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdArchivingScheduleDetails.PageIndexChanging
        grdArchivingScheduleDetails.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
