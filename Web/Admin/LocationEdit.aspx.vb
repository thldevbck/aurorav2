''rev:mia Nov.4 2011 - see marked
''                   - addition of LocAxNum

Imports System.Collections.Generic
Imports System.Data

Imports System.Web.Script.Serialization

Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.LocationDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.LocationMaintenance)> _
Partial Class Admin_LocationEdit
    Inherits AuroraPage

    Public Class TownCityItem
        Public Text As String
        Public Value As String
        Public CtyCode As String
    End Class

    Private _townCityItemsString As String = ""
    Public ReadOnly Property TownCityItemsString() As String
        Get
            Return _townCityItemsString
        End Get
    End Property

    Private _initException As Exception

    Private _seasonControlList As New List(Of Admin_SeasonControl)

    Private _locCode As String = Nothing
    Private _locationDataSet As New LocationDataSet()
    Private _locationRow As LocationRow


    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If String.IsNullOrEmpty(_locCode) Then
                Return "New Location"
            Else
                Return "Edit Location"
            End If
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("locCode")) Then _
                _locCode = Request.QueryString("locCode").Trim()

            If Not String.IsNullOrEmpty(Request("MsgToShow")) Then Me.AddInformationMessage(Request("MsgToShow"))

            Dim nSelectedTab As Integer = 0
            If Not String.IsNullOrEmpty(Request("CurrentTab")) Then nSelectedTab = CInt(Request("CurrentTab"))
            tabs.ActiveTabIndex = nSelectedTab

            MyBase.OnInit(e)

            _locationDataSet.EnforceConstraints = False

            DataRepository.GetLocationLookups(_locationDataSet)

            typeDropDown.Items.Clear()
            For Each codeRow As CodeRow In _locationDataSet.Code
                If codeRow.CodCdtNum <> Aurora.Common.DataConstants.CodeType_Location_Type Then Continue For
                Dim li As ListItem = New ListItem(codeRow.CodDesc, codeRow.CodId)
                typeDropDown.Items.Add(li)
            Next

            countryDropDown.Items.Clear()
            For Each countryRow As CountryRow In _locationDataSet.Country
                Dim li As ListItem = New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode)
                countryDropDown.Items.Add(li)
            Next

            Dim townCityItemList As New List(Of TownCityItem)
            townCityDropDown.Items.Clear()
            townCityDropDown.Items.Add(New ListItem("", ""))
            For Each townCityRow As TownCityRow In _locationDataSet.TownCity
                Dim li As ListItem = New ListItem(townCityRow.TctCode & " - " & townCityRow.TctName, townCityRow.TctCode)
                townCityDropDown.Items.Add(li)

                Dim townCityItem As New TownCityItem()
                townCityItem.Text = li.Text
                townCityItem.Value = li.Value
                townCityItem.CtyCode = townCityRow.TctCtyCode
                townCityItemList.Add(townCityItem)
            Next

            txtThirdPartyLocCode.MaxLength = _locationDataSet.Location.LocThirdPartyCodeColumn.MaxLength

            'Populating "Auto Pre-Checkout Schedule Time" list
            'It should contain time options within the 24 hour interval with difference of 15 minutes.
            'The value is expressed in minutes (therefore it's an integer).
            'The text is expressed in 12 hour time format.
            Dim strMin As String = ""
            For intMinutes As Integer = 0 To 1425 Step 15
                ''ddlPreCheckoutScheduleTime.Items.Add(New ListItem(GetTimeText(intMinutes, True), intMinutes))
                ''rev:mia July 26 2012 - added this line of code but keeping GetTimeText function intact for possible usage.
                ''regional settings for time of Pap-app-002 is in different countries
                ''ddlPreCheckoutScheduleTime.Items.Add(New ListItem(New DateTime(TimeSpan.FromMinutes(intMinutes).Ticks).ToShortTimeString.Replace(".", ""), intMinutes))

                strMin = New DateTime(TimeSpan.FromMinutes(intMinutes).Ticks).ToString("hh:mm tt").Replace(".", "")
                If (strMin.Contains("m") = False) Then
                    strMin = String.Concat(strMin, IIf(intMinutes >= 720, " pm", " am"))
                End If
                ddlPreCheckoutScheduleTime.Items.Add(New ListItem(strMin, intMinutes))

                ''LogDebug(intMinutes & " >  " & New DateTime(TimeSpan.FromMinutes(intMinutes).Ticks).ToString("hh:mm tt"))
            Next

            Dim js As New JavaScriptSerializer()
            _townCityItemsString = js.Serialize(townCityItemList)

            currencyDropDown.Items.Clear()
            For Each codeRow As CodeRow In _locationDataSet.Code
                If codeRow.CodCdtNum <> Aurora.Common.DataConstants.CodeType_Currency Then Continue For
                Dim li As ListItem = New ListItem(codeRow.CodCode & " - " & codeRow.CodDesc, codeRow.CodId)
                currencyDropDown.Items.Add(li)
            Next

            If Not String.IsNullOrEmpty(_locCode) Then
                DataRepository.GetLocation(_locationDataSet, Me.CompanyCode, _locCode)
                _locationRow = _locationDataSet.Location.FindByLocCode(_locCode)
                If _locationRow Is Nothing Then
                    Throw New Exception("No location found")
                End If
            End If

            Dim seasonCount As Integer
            If Me.IsPostBack Then
                seasonCount = Integer.Parse(Request(seasonCountHidden.ClientID.Replace("_", "$")))
            Else
                seasonCount = 0
            End If
            seasonCountHidden.Value = seasonCount.ToString()

            For i As Integer = 0 To seasonCount - 1
                Dim seasonControl As Admin_SeasonControl = Me.LoadControl("Controls/SeasonControl.ascx")
                seasonsPanel.Controls.Add(seasonControl)
                _seasonControlList.Add(seasonControl)
            Next

            ' For PHOENIX
            InitialiseBrandAndVehicleClass()

            ''rev:mia Nov.4 2011
            TextBoxLocation.Attributes.Add("onkeyup", "IsValidSized(this);")
            TextBoxLocation.MaxLength = 4

        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    ''' <summary>
    ''' Gets time string formatted as hours and minutes separated by a colon.
    ''' </summary>
    ''' <param name="minutes"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function GetTimeText(ByVal minutes As Integer, ByVal useMeridiemNotation As Boolean) As String
        If minutes < 0 Then
            Throw New ArgumentException("minutes - value cannot be negative.")
        End If

        If minutes > 1439 Then
            Throw New ArgumentException("minutes - value cannot be greater than 1439, which is the equivalent of 23:59, the last minute before midnight. The latter is represented by 0.")
        End If

        If useMeridiemNotation Then
            Dim meridiem As String
            If minutes >= 720 Then
                meridiem = "pm"
            Else
                meridiem = "am"
            End If
            Return ((minutes \ 60 + 11) Mod 12 + 1).ToString() + ":"c + EnsureTimeDigits((minutes Mod 60).ToString()) + " "c + meridiem
        Else
            Return EnsureTimeDigits((minutes \ 60).ToString()) + ":"c + EnsureTimeDigits((minutes Mod 60).ToString())
        End If
    End Function

    Private Function EnsureTimeDigits(ByVal value As String) As String
        If value.Length < 2 Then
            Return "0"c + value
        End If
        Return value
    End Function

    Private Sub BuildLocation(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = ""
        nameTextBox.Text = ""

        If _locationRow Is Nothing Then
            codeTextBox.ReadOnly = False
            codeTextBox.Text = ""
            nameTextBox.Text = ""
            typeDropDown.SelectedIndex = 0
            countryDropDown.SelectedValue = Me.CountryCode
            townCityDropDown.SelectedValue = UserSettings.Current.TctCode
            hoursBehindServerTextBox.Text = "0"
            currencyDropDown.SelectedIndex = 0
            maximumCheckOutInsTextBox.Text = "0"
            throughputPenaltyCostTextBox.Text = "0"
            statusRadioButtonList.SelectedIndex = 1
            txtThirdPartyLocCode.Text = ""
            ddlPreCheckoutScheduleTime.SelectedValue = ""
            datCompletedDate.Text = ""

            ' manager tab
            managerNameTextBox.Text = ""
            managerPhoneNumberControl.Clear()

            ' opening hours tab
            businessTimeFromAMTextBox.Text = ""
            businessTimeToAMTextBox.Text = ""
            businessTimeFromPMTextBox.Text = ""
            businessTimeToPMTextBox.Text = ""

            ' location address
            physicalAddressDetailsControl.Clear()
            postalAddressDetailsControl.Clear()

            ' third party address
            thirdPartyAddressDetailsControl.Clear()
            thirdPartyPhoneNumberControl.Clear()

            ' airport address
            internationalAirportAddressDetailsControl.Clear()
            internationalAirportPhoneNumberControl.Clear()
            domesticAirportAddressDetailsControl.Clear()
            domesticAirportPhoneNumberControl.Clear()

            updateButton.Visible = False
            createButton.Visible = True
        Else
            codeTextBox.Text = _locationRow.LocCode
            codeTextBox.ReadOnly = True
            nameTextBox.Text = _locationRow.LocName
            Try
                typeDropDown.SelectedValue = _locationRow.LocCodTypId
            Catch
            End Try
            hoursBehindServerTextBox.Text = _locationRow.LocHoursBehindServerLocation.ToString("0.#")
            countryDropDown.SelectedValue = _locationRow.TownCityRow.CountryRow.CtyCode
            townCityDropDown.SelectedValue = _locationRow.TownCityRow.TctCode
            currencyDropDown.SelectedValue = _locationRow.LocCodCurrId
            maximumCheckOutInsTextBox.Text = _locationRow.LocMaxCheckoutsIns.ToString()
            throughputPenaltyCostTextBox.Text = _locationRow.LocThroughputPenalty.ToString("0.#")
            statusRadioButtonList.SelectedIndex = IIf(Not _locationRow.IsLocIsActiveNull AndAlso _locationRow.LocIsActive, 0, 1)

            txtThirdPartyLocCode.Text = _locationRow.LocThirdPartyCode

            If _locationRow.IsLocAutoCkoRunTimeNull() Then
                ddlPreCheckoutScheduleTime.SelectedValue = ""
            Else
                ddlPreCheckoutScheduleTime.SelectedValue = _locationRow.LocAutoCkoRunTime
            End If

            If _locationRow.IsLocAutoCkoProcDateNull() Then
                datCompletedDate.Text = ""
            Else
                datCompletedDate.Date = _locationRow.LocAutoCkoProcDate
            End If


            ' manager tab
            If _locationRow.ContactRow IsNot Nothing Then
                managerNameTextBox.Text = _locationRow.ContactRow.ConName
            Else
                managerNameTextBox.Text = ""
            End If
            managerPhoneNumberControl.Build(_locationRow)

            ' opening hours tab
            businessTimeFromAMTextBox.Time = _locationRow.LocAMStartTime.TimeOfDay
            businessTimeToAMTextBox.Time = _locationRow.LocAmFinishTime.TimeOfDay
            businessTimeFromPMTextBox.Time = _locationRow.LocPMStartTime.TimeOfDay
            businessTimeToPMTextBox.Time = _locationRow.LocPMFinishTime.TimeOfDay

            For Each locationSeasonRow As LocationSeasonRow In _locationRow.GetLocationSeasonRows()
                Dim seasonControl As Admin_SeasonControl = Me.LoadControl("Controls/SeasonControl.ascx")
                seasonsPanel.Controls.Add(seasonControl)
                _seasonControlList.Add(seasonControl)

                seasonControl.Build(locationSeasonRow)

                seasonCountHidden.Value = _seasonControlList.Count.ToString()
            Next

            ' location address
            physicalAddressDetailsControl.Build(_locationRow)
            postalAddressDetailsControl.Build(_locationRow)

            ' third party address
            thirdPartyAddressDetailsControl.Build(_locationRow)
            thirdPartyPhoneNumberControl.Build(_locationRow)

            ' airport address
            internationalAirportAddressDetailsControl.Build(_locationRow)
            internationalAirportPhoneNumberControl.Build(_locationRow)
            domesticAirportAddressDetailsControl.Build(_locationRow)
            domesticAirportPhoneNumberControl.Build(_locationRow)

            updateButton.Visible = True
            createButton.Visible = False

            Try
                ''rev:mia feb 3 added zone location"
                ''Me.OtherLocationControl.ZoneID = _locationRow.LocZoneId
                ''Me.OtherLocationControl.WebEnableDate = _locationRow.LocWebEnabledDate

                EnabledDateControl.Text = ConvertDate(_locationRow.LocWebEnabledDate)
                Me.typeDropDownZone.SelectedValue = _locationRow.LocZoneId
            Catch ex As Exception
            End Try
            
            ' Location Brand
            LoadLocationBrands(_locationRow.LocCode)
            ' location veh class
            LoadLocationVehicleClasses(_locationRow.LocCode)

            Try
                ''rev:mia Nov.4 2011
                If (Not _locationRow.IsNull("LocAxNum")) Then
                    TextBoxLocation.Text = _locationRow.LocAxNum
                End If

            Catch ex As Exception
                ''do nothing
            End Try
            

        End If

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        ''Me.EnabledDateControl.AutoPostBack = True
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' build the form
        If Not Page.IsPostBack AndAlso _initException Is Nothing Then
            ''-------------------------------------------------
            ''rev:mia feb 3 added zone location"
            ''-------------------------------------------------
            InitZoneLocation()
            InitOtherLocationControl()
            ''-------------------------------------------------

            BuildLocation(False)

            If Not String.IsNullOrEmpty(Request.QueryString("Create")) Then
                Me.AddInformationMessage("Location created")
            End If

            Try
                If Not String.IsNullOrEmpty(_locationRow.LocWebEnabledDate) Then
                    Me.EnabledDateControl.Date = IIf(String.IsNullOrEmpty(_locationRow.LocWebEnabledDate), "", ConvertDate(_locationRow.LocWebEnabledDate))
                End If
                If Not String.IsNullOrEmpty(_locationRow.LocZoneId) Then
                    Me.typeDropDownZone.SelectedValue = _locationRow.LocZoneId
                End If
            Catch ex As Exception

            End Try
            


        End If
        ''rev:mia may 4 2009
        MaxCheck.Visible = False
        Me.OtherLocationControl.LocationZoneList = Aurora.Admin.Data.DataRepository.SelectLocationZone
        Me.OtherLocationControl.enabledGrid = IIf(Not String.IsNullOrEmpty(Me.typeDropDownZone.SelectedValue), True, False)
        
        If statusRadioButtonList.SelectedIndex = 0 Then
            statusTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.ActiveColor)
        Else
            statusTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.InactiveColor)
        End If
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("LocationSearch.aspx")
    End Sub

    Private Sub ValidateDefaultOpeningHours()
        If Not businessTimeFromAMTextBox.IsValid _
         OrElse Not businessTimeToAMTextBox.IsValid _
         OrElse businessTimeFromAMTextBox.Time > businessTimeToAMTextBox.Time _
         OrElse Not businessTimeFromPMTextBox.IsValid _
         OrElse Not businessTimeToPMTextBox.IsValid _
         OrElse businessTimeFromPMTextBox.Time > businessTimeToPMTextBox.Time _
         OrElse businessTimeToAMTextBox.Time > businessTimeFromPMTextBox.Time Then
            Throw New Aurora.Common.ValidationException("A valid time range must be specified for the morning and evening business times")
        End If
    End Sub

    Private Sub ValidateLocation(ByVal create As Boolean)
        If create Then
            If codeTextBox.Text.Trim() = "" Then
                Throw New Aurora.Common.ValidationException("Code cannot be blank")
            End If

            Dim codeCheckLocationDataSet As New LocationDataSet()
            codeCheckLocationDataSet.EnforceConstraints = False
            DataRepository.GetLocation(codeCheckLocationDataSet, Nothing, codeTextBox.Text.Trim())
            For Each locationRow0 As LocationRow In codeCheckLocationDataSet.Location
                If locationRow0.LocCode.ToLower().Trim() = codeTextBox.Text.ToLower().Trim() Then
                    Throw New Aurora.Common.ValidationException("Another location with this code already exists")
                End If
            Next
        End If

        If nameTextBox.Text.Trim() = "" Then
            Throw New Aurora.Common.ValidationException("Name cannot be blank")
        End If

        If Not hoursBehindServerTextBox.IsTextValid OrElse Decimal.Parse(hoursBehindServerTextBox.Text) < 0 OrElse Decimal.Parse(hoursBehindServerTextBox.Text) >= 24 Then
            Throw New Aurora.Common.ValidationException(hoursBehindServerTextBox.ErrorMessage)
        End If

        If Not maximumCheckOutInsTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException(maximumCheckOutInsTextBox.ErrorMessage)
        End If

        If Not throughputPenaltyCostTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException(throughputPenaltyCostTextBox.ErrorMessage)
        End If

        If Not String.IsNullOrEmpty(datCompletedDate.Text) Then
            datCompletedDate.Text = datCompletedDate.Text.Trim()
            If datCompletedDate.Text.Length > 0 Then
                Dim parsedDate As DateTime
                If Not DateTime.TryParse(datCompletedDate.Text, parsedDate) Then
                    Throw New Aurora.Common.ValidationException("Invalid value entered for: ""Completed Date""")
                End If
            End If
        End If

        ' manager tab
        Try
            managerPhoneNumberControl.Validate(create)
        Catch ex As ValidationException
            tabs.ActiveTabIndex = 0
            Throw
        End Try

        ' opening hours tab
        Try
            ValidateDefaultOpeningHours()

            For Each seasonControl As Admin_SeasonControl In _seasonControlList
                seasonControl.Validate(create)
            Next

            For Each seasonControl0 As Admin_SeasonControl In _seasonControlList
                For Each seasonControl1 As Admin_SeasonControl In _seasonControlList
                    If seasonControl0 IsNot seasonControl1 _
                     AndAlso Not seasonControl0.IsDeleted _
                     AndAlso Not seasonControl1.IsDeleted _
                     AndAlso Utility.DoesDateRangeIntersect(seasonControl0.FromDate, seasonControl0.ToDate, seasonControl1.FromDate, seasonControl1.ToDate) Then
                        Throw New ValidationException("Season period date ranges may not overlap")
                    End If
                Next
            Next
        Catch ex As ValidationException
            tabs.ActiveTabIndex = 1
            Throw
        End Try

        ' location address
        Try
            physicalAddressDetailsControl.Validate(create)
            postalAddressDetailsControl.Validate(create)
        Catch ex As ValidationException
            tabs.ActiveTabIndex = 2
            Throw
        End Try

        ' third party address
        Try
            thirdPartyAddressDetailsControl.Validate(create)
            thirdPartyPhoneNumberControl.Validate(create)
        Catch ex As ValidationException
            tabs.ActiveTabIndex = 3
            Throw
        End Try

        ' airport address
        Try
            internationalAirportAddressDetailsControl.Validate(create)
            internationalAirportPhoneNumberControl.Validate(create)
            domesticAirportAddressDetailsControl.Validate(create)
            domesticAirportPhoneNumberControl.Validate(create)
        Catch ex As ValidationException
            tabs.ActiveTabIndex = 4
            Throw
        End Try



    End Sub

    Private Function CreateUpdateLocation(ByVal create As Boolean) As Boolean
        Dim updateError As Boolean = False

        Try
            ValidateLocation(create)
        Catch ex As Aurora.Common.ValidationException
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return False
        End Try



        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                ' Location
                Try
                    If create Then
                        _locationRow = _locationDataSet.Location.NewLocationRow()
                        _locationRow.LocComCode = Me.CompanyCode
                        _locationRow.LocCode = codeTextBox.Text.Trim()
                    End If

                    _locationRow.LocName = nameTextBox.Text.Trim()
                    _locationRow.LocCodTypId = typeDropDown.SelectedValue
                    _locationRow.LocHoursBehindServerLocation = Decimal.Parse(hoursBehindServerTextBox.Text)
                    _locationRow.LocTctCode = townCityDropDown.SelectedValue
                    _locationRow.LocDefCurrCode = currencyDropDown.SelectedItem.Text.Substring(0, currencyDropDown.SelectedItem.Text.IndexOf("-") - 1).Trim()
                    _locationRow.LocCodCurrId = currencyDropDown.SelectedValue
                    _locationRow.LocMaxCheckoutsIns = Integer.Parse(maximumCheckOutInsTextBox.Text)
                    _locationRow.LocThroughputPenalty = Decimal.Parse(throughputPenaltyCostTextBox.Text)
                    _locationRow.LocIsActive = statusRadioButtonList.SelectedIndex = 0
                    _locationRow.LocAMStartTime = New Date(1900, 1, 1) + businessTimeFromAMTextBox.Time
                    _locationRow.LocAmFinishTime = New Date(1900, 1, 1) + businessTimeToAMTextBox.Time
                    _locationRow.LocPMStartTime = New Date(1900, 1, 1) + businessTimeFromPMTextBox.Time
                    _locationRow.LocPMFinishTime = New Date(1900, 1, 1) + businessTimeToPMTextBox.Time

                    ''rev:mia feb 3 added zone location
                    _locationRow.LocZoneId = IIf(Not String.IsNullOrEmpty(Me.typeDropDownZone.SelectedValue), Me.typeDropDownZone.SelectedValue, -1)

                    If Not String.IsNullOrEmpty(Me.EnabledDateControl.Text) Then
                        _locationRow.LocWebEnabledDate = Convert.ToDateTime(Me.EnabledDateControl.Date)
                    Else
                        _locationRow.LocWebEnabledDate = Nothing
                    End If

                    ''rev:mia Nov.4 2011
                    If (Not String.IsNullOrEmpty(TextBoxLocation.Text)) Then
                        _locationRow.LocAxNum = TextBoxLocation.Text
                    End If

                    If txtThirdPartyLocCode.Text Is Nothing Then
                        _locationRow.SetLocThirdPartyCodeNull()
                        _locationRow.LocThirdPartyCode = Nothing
                    Else
                        _locationRow.LocThirdPartyCode = txtThirdPartyLocCode.Text.Trim()
                    End If

                    If String.IsNullOrEmpty(ddlPreCheckoutScheduleTime.SelectedValue) Then
                        _locationRow.SetLocAutoCkoRuntimeNull()
                    Else
                        _locationRow.LocAutoCkoRuntime = Integer.Parse(ddlPreCheckoutScheduleTime.SelectedValue)
                    End If

                    If String.IsNullOrEmpty(datCompletedDate.Text) Then
                        _locationRow.SetLocAutoCkoProcDateNull()
                    Else
                        _locationRow.LocAutoCkoProcDate = datCompletedDate.Date
                    End If


                    If create Then
                        _locationDataSet.Location.AddLocationRow(_locationRow)
                    End If

                    ''reV:mia Project Phoenix
                    '' Aurora.Common.Data.ExecuteDataRow(_locationRow, Me.UserCode, Me.PrgmName)
                    Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(_locationRow, Me.UserCode, Me.PrgmName)
                Catch ex As Exception
                    Dim message As String = ex.Message
                    If message.Contains("TctCode") Then
                        message = " - Town / City is empty"
                    Else
                        message = ""
                    End If

                    Throw New Exception("Error " & IIf(create, "creating", "updating") & " location" & " - " & message)
                End Try

                ' Manager
                Try
                    Dim isBlank As Boolean = String.IsNullOrEmpty(managerNameTextBox.Text.Trim())
                    Dim cnr As ContactRow = _locationRow.ContactRow

                    If cnr IsNot Nothing And isBlank Then
                        cnr.Delete()
                        Aurora.Common.Data.ExecuteDataRow(cnr, Me.UserCode, Me.PrgmName)
                        _locationDataSet.Contact.RemoveContactRow(cnr)

                    ElseIf cnr Is Nothing And Not isBlank Then
                        cnr = _locationDataSet.Contact.NewContactRow()
                        cnr.ConId = Aurora.Common.Data.GetNewId()
                        cnr.ConPrntId = _locationRow.LocCode
                        cnr.ConPrntTableName = "Location"
                        cnr.ConCodContactId = _locationDataSet.Code.FindByCodCdtNumCodCode(DataConstants.CodeType_Contact_Type, "Manager").CodId

                        cnr.ConName = managerNameTextBox.Text.Trim()
                        Dim firstName As String = ""
                        Dim lastName As String = ""
                        Utility.ExtractFirstLastName(cnr.ConName, firstName, lastName)
                        cnr.ConFirstName = firstName
                        cnr.ConLastName = lastName

                        _locationDataSet.Contact.AddContactRow(cnr)
                        Aurora.Common.Data.ExecuteDataRow(cnr, Me.UserCode, Me.PrgmName)

                    ElseIf cnr IsNot Nothing And Not isBlank Then
                        cnr.ConName = managerNameTextBox.Text.Trim()
                        Dim firstName As String = ""
                        Dim lastName As String = ""
                        Utility.ExtractFirstLastName(cnr.ConName, firstName, lastName)
                        cnr.ConFirstName = firstName
                        cnr.ConLastName = lastName

                        Aurora.Common.Data.ExecuteDataRow(cnr, Me.UserCode, Me.PrgmName)
                    End If

                    managerPhoneNumberControl.Update(_locationRow)
                Catch ex As Exception
                    Throw New Exception("Error updating manager details" & " - " & ex.Message)
                End Try


                ' Opening Hours
                Try
                    For Each seasonControl As Admin_SeasonControl In _seasonControlList
                        seasonControl.Update(_locationRow)
                    Next
                Catch ex As Exception
                    Throw New Exception("Error updating opening hour details" & " - " & ex.Message)
                End Try


                ' Location Address
                Try
                    physicalAddressDetailsControl.Update(_locationRow)
                    postalAddressDetailsControl.Update(_locationRow)
                Catch ex As Exception
                    Throw New Exception("Error updating location address details" & " - " & ex.Message)
                End Try

                ' Third Party
                Try
                    thirdPartyAddressDetailsControl.Update(_locationRow)
                    thirdPartyPhoneNumberControl.Update(_locationRow)
                Catch ex As Exception
                    Throw New Exception("Error updating third party details" & " - " & ex.Message)
                End Try

                ' Airport
                Try
                    internationalAirportAddressDetailsControl.Update(_locationRow)
                    internationalAirportPhoneNumberControl.Update(_locationRow)
                    domesticAirportAddressDetailsControl.Update(_locationRow)
                    domesticAirportPhoneNumberControl.Update(_locationRow)
                Catch ex As Exception
                    Throw New Exception("Error updating airport details" & " - " & ex.Message)
                End Try

                ' Location Brands
                SaveLocationBrands()
                SaveLocationVehicleClasses()

                oTransaction.CommitTransaction()

            Catch ex As Exception
                Me.AddErrorMessage(ex.Message)
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Return False
            End Try

        End Using

        Return Not updateError
    End Function

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If CreateUpdateLocation(True) Then
            Response.Redirect("LocationEdit.aspx?LocCode=" & Server.UrlEncode(_locationRow.LocCode) & "&create=true")
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        ''rev:mia feb 3 added zone location
        ''check if otherlocation should be mandatory
        ''Dim IsOtherLocationMandatory As Boolean = Me.OtherLocationControl.IsOtherLocationMandatory

        If Not String.IsNullOrEmpty(Me.typeDropDownZone.SelectedValue) Then
            ''If Not IsOtherLocationMandatory Then
            If Not Me.OtherLocationControl.IsMandatoryLocationCheckingOK Then
                Dim unmappedLocation As Integer = CInt(Me.OtherLocationControl.TotalNumberOfChildLocationUnMapped())
                Dim totalLocation As Integer = CInt(Me.OtherLocationControl.TotalNumberOfChildLocationUnMappedtoZone) ''CInt(Me.OtherLocationControl.TotalNumberOfChildLocation)

            
                ''(location code) shall non longer be assigned to a zone. 
                ''Saving this will allow this location code to show separately on the booking pages. 

                Me.ConfirmationBox.Text = "There " & IIf(CInt(totalLocation) > 1, "are ", " is ") & " <b>" & totalLocation & "</b> locations found assigned to this city, " & _
                                                          "<b>" & unmappedLocation & "</b>" & " of these locations are not allocated to a zone. " & _
                                                          "<br/><br/> Do you wish to continue with this zone allocation?"
                Me.ConfirmationBox.LeftButton_Text = "Yes"
                ConfirmationBox.Show()
            Else
                If WebEnabledDateComparedMessage(EnabledDateControl.Date) Then
                    Dim textMsg As String = "The date entered means this location shall be displayed on the B2C booking pages today.<br/><br/>"
                    textMsg = textMsg & "Do you wish to continue?"
                    Me.ConfirmationBoxWebEnabledDate.RightButton_AutoPostBack = True
                    Me.ConfirmationBoxWebEnabledDate.Text = textMsg
                    Me.ConfirmationBoxWebEnabledDate.Show()
                Else
                    If CreateUpdateLocation(False) Then
                        Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
                    End If
                End If
                
            End If
        Else
            ''rev:mia put comment on this one to avoid resetting of child records
            ''If Not isResetChildLocationMappingOK() Then Exit Sub
            'If CreateUpdateLocation(False) Then
            '    Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
            'End If

            ''regardless whether there is a zone display date if possible
            If WebEnabledDateComparedMessage(EnabledDateControl.Date) Then
                Dim textMsg As String = "The date entered means this location shall be displayed on the B2C booking pages today.<br/><br/>"
                textMsg = textMsg & "Do you wish to continue?"
                Me.ConfirmationBoxWebEnabledDate.RightButton_AutoPostBack = True
                Me.ConfirmationBoxWebEnabledDate.Text = textMsg
                Me.ConfirmationBoxWebEnabledDate.Show()
            Else
                'If CreateUpdateLocation(False) Then
                '    Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
                'End If
                Dim tempMsgA As String = "Zone Location is empty. Saving this will allow this location code to show separately on the booking pages.<br/><br/>"
                Dim tempMsgB As String = "Do you wish to continue?"
                Me.ConfirmationBoxControlZoneLocation.Text = tempMsgA & tempMsgB
                Me.ConfirmationBoxControlZoneLocation.Show()
            End If
        End If
        
    End Sub

    Protected Sub seasonAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles seasonAddButton.Click
        Try
            ValidateDefaultOpeningHours()
        Catch ex As ValidationException
            Me.AddErrorMessage(ex.Message)
            Return
        End Try

        Dim seasonControl As Admin_SeasonControl = Me.LoadControl("Controls/SeasonControl.ascx")
        seasonsPanel.Controls.AddAt(0, seasonControl)
        _seasonControlList.Add(seasonControl)

        seasonControl.BuildNew(Date.Now.Date, Date.Now.Date, businessTimeFromAMTextBox.Time, businessTimeToPMTextBox.Time)

        seasonCountHidden.Value = _seasonControlList.Count.ToString()
    End Sub

#Region "rev:mia feb 3 added zone location"
    ''Admin_TotalNumberOfChildLocationUnMapped
    ''Admin_TotalNumberOfChildLocation
    ''Admin_SelectOtherLocation
    ''proc Admin_SelectLocationZone
    ''res_InsertLocationZone
    ''Admin_UpdateLocationZoneMapping

    
    
    Private Property IsOtherLocationMandatory() As Boolean
        Get
            Return CBool(ViewState("IsOtherLocationMandatory"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsOtherLocationMandatory") = value
        End Set
    End Property

    Private Sub InitOtherLocationControl()
        If _locationRow Is Nothing Then Return
        Me.OtherLocationControl.LocCode = _locationRow.LocCode
        Me.OtherLocationControl.LocCodTypeID = _locationRow.LocCodTypId
        Me.OtherLocationControl.LocTctCode = _locationRow.LocTctCode
        Me.OtherLocationControl.LocationZoneList = Aurora.Admin.Data.DataRepository.SelectLocationZone
        Me.EnabledDateControl.Enabled = OtherLocationControl.IsB2Cuser
        Me.typeDropDownZone.Enabled = OtherLocationControl.IsB2Cuser
    End Sub

    Protected Sub ConfirmationBox_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBox.PostBack
        If leftButton Then
            tabs.ActiveTabIndex = 5
            If CreateUpdateLocation(False) Then
                Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
            End If
        End If
    End Sub

    Private Function isResetChildLocationMappingOK() As Boolean
        Try
            Aurora.Admin.Data.DataRepository.ResetChildLocationUnMapping(_locationRow.LocCodTypId, _locationRow.LocTctCode, _locationRow.LocCode)
        Catch ex As Exception
            SetErrorShortMessage(ex.Message.Replace("ERROR:", ""))
            Return False
        End Try
        Return True
    End Function

    Private Function ProperCasing(ByVal FullName As String) As String
        If String.IsNullOrEmpty(FullName) Then Return ""
        Dim strFullPackage() As String = FullName.ToLower.Split(" ")
        Dim tempStrFullPackage As String = ""

        For Each tempStrPackage As String In strFullPackage
            If Not String.IsNullOrEmpty(tempStrPackage) Then
                Dim firstLetter As String = tempStrPackage.Substring(0, 1).ToUpper
                tempStrPackage = tempStrPackage.Remove(0, 1)
                tempStrPackage = firstLetter & tempStrPackage & " "
                tempStrFullPackage = tempStrFullPackage & tempStrPackage
            End If
        Next

        Dim tempZone As String() = tempStrFullPackage.Split("-")
        Return tempZone(0).ToUpper & " - " & tempZone(1)
    End Function

    Private Sub InitZoneLocation()
        If Page.IsPostBack Then Exit Sub
        IsOtherLocationMandatory = False

        Dim vwzone As System.Data.DataView = Aurora.Admin.Data.DataRepository.SelectLocationZone.Tables(0).DefaultView
        Dim item As ListItem = New ListItem("", "")
        typeDropDownZone.Items.Add(item)
        For Each row As DataRow In vwzone.Table.Rows
            System.Diagnostics.Debug.WriteLine(row("LocZoneId") & " - " & ProperCasing(row("FullZone")))
            item = New ListItem(ProperCasing(row("FullZone")), row("LocZoneId"))
            typeDropDownZone.Items.Add(item)
        Next

    End Sub

    Protected Sub typeDropDownZone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles typeDropDownZone.SelectedIndexChanged
        IsOtherLocationMandatory = IIf(String.IsNullOrEmpty(typeDropDownZone.SelectedValue), False, True)
        Me.OtherLocationControl.enabledGrid = IsOtherLocationMandatory
    End Sub

    Private WriteOnly Property WebEnableDate() As String
        Set(ByVal value As String)
            Me.EnabledDateControl.Text = ConvertDate(value)
        End Set
    End Property

    Private Function ConvertDate(ByVal objDate As Object) As Object
        If objDate Is Nothing Then Return Nothing
        If objDate = "" Then Return Nothing
        If objDate Is DBNull.Value Then Return Nothing
        Return Convert.ToDateTime(objDate)

    End Function

    Private Function WebEnabledDateComparedMessage(ByVal d1 As Date) As Boolean
        If String.IsNullOrEmpty(Me.EnabledDateControl.Text) Then Return False
        Dim d2 As Date = Date.Now
        Return d1 <= d2
    End Function

    Protected Sub ConfirmationBoxWebEnabledDate_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxWebEnabledDate.PostBack
        If leftButton Then
            If CreateUpdateLocation(False) And Me.typeDropDownZone.SelectedItem.Text <> "" Then
                Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
            Else
                If String.IsNullOrEmpty(Me.typeDropDownZone.SelectedItem.Text) Then
                    ''A zone has not been allocated to this location.  
                    ''Locations with the same city/town have been allocated to zones. 
                    ''Saving without allocating this location to a zone will allow this location to show separately on the B2C booking pages

                    Dim tempMsgA As String = "A zone has not been allocated to this location.<br/>Locations with the same city/town have been allocated to zones.<br/>Saving without allocating this location to a zone will allow this location to show separately on the B2C booking pages.</br></br>"
                    Dim tempMsgB As String = "Do you wish to continue?"
                    Me.ConfirmationBoxControlZoneLocation.Text = tempMsgA & tempMsgB
                    Me.ConfirmationBoxControlZoneLocation.Show()
                End If
            End If
            
        Else

            If (String.IsNullOrEmpty(_locationRow.LocWebEnabledDate)) Then
                Me.EnabledDateControl.Text = ""
            End If

            If CreateUpdateLocation(False) Then
                Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
            End If
        End If
    End Sub

    Protected Sub ConfirmationBoxControlZoneLocation_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControlZoneLocation.PostBack
        If leftButton Then
            If CreateUpdateLocation(False) Then
                Response.Redirect("LocationEdit.aspx?LocCode=" & Request("LocCode") & "&MsgToShow=Location updated&CurrentTab=" & tabs.ActiveTabIndex)
            End If
        End If
    End Sub
#End Region


#Region "Tourism Holdings and the project called PHOENIX"

    Sub InitialiseBrandAndVehicleClass()
        Dim arrDataTables As DataTable()
        arrDataTables = Aurora.Admin.Data.GetBrandsAndVehicleClasses(CompanyCode)

        cblLocationClass.DataSource = arrDataTables(0)
        cblLocationClass.DataTextField = "ClaDesc"
        cblLocationClass.DataValueField = "ClaId"
        cblLocationClass.DataBind()

        cblLocationBrand.DataSource = arrDataTables(1)
        cblLocationBrand.DataTextField = "BrdName"
        cblLocationBrand.DataValueField = "BrdCode"
        cblLocationBrand.DataBind()
    End Sub

    Sub LoadLocationBrands(ByVal LocationCode As String)
        Dim sBrandList As String
        sBrandList = Aurora.Admin.Data.LoadLocationBrands(LocationCode)
        If String.IsNullOrEmpty(sBrandList) Then
            ' do nothing, as there are no records :)
        Else
            For Each oBrandListItem As ListItem In cblLocationBrand.Items
                oBrandListItem.Selected = sBrandList.Contains(oBrandListItem.Value)
            Next
        End If
    End Sub

    Sub SaveLocationBrands()

        Dim sbAddedBrandList, sbRemovedBrandList As StringBuilder
        sbAddedBrandList = New StringBuilder
        sbRemovedBrandList = New StringBuilder

        For Each oBrand As ListItem In cblLocationBrand.Items
            If oBrand.Selected Then
                sbAddedBrandList.Append(oBrand.Value & ",")
            Else
                sbRemovedBrandList.Append(oBrand.Value & ",")
            End If
        Next
        Dim sRetMsg As String

        Dim sAddedBrandList, sRemovedBrandList As String

        If sbAddedBrandList.Length > 1 Then
            sAddedBrandList = sbAddedBrandList.ToString().Substring(0, sbAddedBrandList.ToString().Length - 1)
        Else
            sAddedBrandList = sbAddedBrandList.ToString()
        End If

        If sbRemovedBrandList.Length > 1 Then
            sRemovedBrandList = sbRemovedBrandList.ToString().Substring(0, sbRemovedBrandList.ToString().Length - 1)
        Else
            sRemovedBrandList = sbRemovedBrandList.ToString()
        End If

        sRetMsg = Aurora.Admin.Data.UpdateLocationBrands(codeTextBox.Text.Trim(), sAddedBrandList, sRemovedBrandList _
                    , UserId, PrgmName)
        If sRetMsg.IndexOf("ERROR") > 0 Then
            Throw New Exception(sRetMsg)
        End If
    End Sub

    Sub LoadLocationVehicleClasses(ByVal LocationCode As String)
        Dim sVehClassList As String
        sVehClassList = Aurora.Admin.Data.LoadLocationVehicleClasses(LocationCode)
        If String.IsNullOrEmpty(sVehClassList) Then
            ' do nothing, as there are no records :)
        Else
            For Each oVehicleClassListItem As ListItem In cblLocationClass.Items
                oVehicleClassListItem.Selected = sVehClassList.Contains(oVehicleClassListItem.Value)
            Next
        End If
    End Sub

    Sub SaveLocationVehicleClasses()
        Dim sbAddedVehicleClassList, sbRemovedVehicleClassList As StringBuilder
        sbAddedVehicleClassList = New StringBuilder
        sbRemovedVehicleClassList = New StringBuilder

        For Each oVechicleClass As ListItem In cblLocationClass.Items
            If oVechicleClass.Selected Then
                sbAddedVehicleClassList.Append(oVechicleClass.Value & ",")
            Else
                sbRemovedVehicleClassList.Append(oVechicleClass.Value & ",")
            End If
        Next
        Dim sRetMsg, sAddedVehicleClassList, sRemovedVehicleClassList As String

        If sbAddedVehicleClassList.Length > 1 Then
            sAddedVehicleClassList = sbAddedVehicleClassList.ToString().Substring(0, sbAddedVehicleClassList.ToString().Length - 1)
        Else
            sAddedVehicleClassList = sbAddedVehicleClassList.ToString()
        End If

        If sbRemovedVehicleClassList.Length > 1 Then
            sRemovedVehicleClassList = sbRemovedVehicleClassList.ToString().Substring(0, sbRemovedVehicleClassList.ToString().Length - 1)
        Else
            sRemovedVehicleClassList = sbRemovedVehicleClassList.ToString()
        End If
        sRetMsg = Aurora.Admin.Data.UpdateLocationVehicleClasses(codeTextBox.Text.Trim(), sAddedVehicleClassList, _
                    sRemovedVehicleClassList, UserId, PrgmName)
        If sRetMsg.IndexOf("ERROR") > 0 Then
            Throw New Exception(sRetMsg)
        End If
    End Sub



#End Region

    
   
    
End Class
