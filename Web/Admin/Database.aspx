<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Database.aspx.vb" Inherits="Admin_Database" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
<script type="text/javascript">
</script>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="searchPanel" runat="server">
        <table cellpadding="2" cellspacing="0" width="100%" style="border-bottom-width: 1px; border-bottom-style: dotted">
            <tr>
                <td width="150px">Table:</td>
                <td>
                    <asp:DropDownList 
                        ID="tableNameDropDown" 
                        runat="server" 
                        Width="600px" 
                        AutoPostBack="True" 
                        AppendDataBoundItems="True">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>

                    <uc1:CollapsiblePanel 
                        ID="collumsCollapsiblePanel" 
                        runat="server"
                        Title="Table Information" 
                        TargetControlID="columnsPanel" />
                    <asp:Panel ID="columnsPanel" runat="server" Style="overflow: hidden;">

                        <asp:Table ID="columnsTable" runat="server" CellSpacing="0" CellPadding="2" CssClass="dataTable" EnableViewState="False" Visible="false" Width="100%">
                            <asp:TableHeaderRow>
                                <asp:TableHeaderCell Width="150px">Name</asp:TableHeaderCell>
                                <asp:TableHeaderCell Width="50px">PK</asp:TableHeaderCell>
                                <asp:TableHeaderCell Width="50px">Nullable</asp:TableHeaderCell>
                                <asp:TableHeaderCell Width="50px">Identity</asp:TableHeaderCell>
                                <asp:TableHeaderCell Width="100px">Type</asp:TableHeaderCell>
                                <asp:TableHeaderCell>Size</asp:TableHeaderCell>
                            </asp:TableHeaderRow>
                        </asp:Table>
                    </asp:Panel>

                </td>
            </tr>
            <tr>
                <td valign="top" style="padding-top: 3px">Sql:</td>
                <td>
                    <asp:TextBox 
                        ID="sqlTextBox" 
                        runat="server" 
                        TextMode="MultiLine" 
                        Rows="12" 
                        Width="600px" 
                        Wrap="false" 
                        Font-Names="Courier New" 
                    />
                    <asp:CheckBox ID="sqlChangedCheckBox" runat="server" Style="display: none" />
                </td>
            </tr>
            <tr>
                <td>Output Mode:</td>
                <td>
                    <asp:RadioButtonList ID="outputRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Value="table" Selected="true">Scalar/Table</asp:ListItem>
                        <asp:ListItem Value="csv">CSV</asp:ListItem>
                        <asp:ListItem Value="dataset">DataSet-Xml</asp:ListItem>
                        <asp:ListItem Value="xml">Result-Xml</asp:ListItem>
                        <asp:ListItem Value="insert">INSERT-Sql</asp:ListItem>
                        <asp:ListItem Value="update">UPDATE-Sql</asp:ListItem>
                        <asp:ListItem Value="delete">DELETE-Sql</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <asp:Button ID="executeButton" runat="server" Text="Execute" CssClass="Button_Standard" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="resultPanel" runat="server" DefaultButton="executeButton" style="margin-top: 10px" EnableViewState="False">

        <asp:Repeater ID="resultRepeater" runat="server">
            <ItemTemplate>
                <asp:Label ID="resultLabel" runat="server" /><br />
                <asp:GridView 
                    ID="resultGridView" 
                    runat="Server" 
                    Width="100%" 
                    AutoGenerateColumns="True" 
                    CssClass="dataTableGrid"
                    Style="margin-bottom: 10px">
                    <RowStyle CssClass="evenRow" />
                    <AlternatingRowStyle CssClass="oddRow" />
                </asp:GridView>
                <hr />
            </ItemTemplate>
        </asp:Repeater>
    
        <asp:TextBox 
            ID="resultTextBox" 
            runat="server" 
            TextMode="MultiLine" 
            Rows="24" 
            Width="780px" 
            Wrap="false" 
            ReadOnly="true" 
            Visible="false" 
            Font-Names="Courier New" />
    </asp:Panel>
    
    <asp:Panel ID="helpPanel" runat="server" EnableViewState="false" Visible="true">
    
        <ul>
            <li>The &quot;Database Sql&quot; page lets you run Sql queries against the Aurora database 
            and output the result of that query in different ways (for queries that return result sets).</li>
            <li>Please take <b>extreme caution</b> when running ad-hoc Sql queries, especially those 
            that modify data (UPDATE, DELETE, INSERT, EXEC etc) or return large results.</li>
            <li>
                The different output modes are:
                <ul>
                    <li><i>Scalar/Table:</i> shows the result of the query in a table or grid format.</li>
                    <li><i>CSV:</i> shows the result of the query in CSV format.</li>
                    <li><i>DataSet-Xml:</i> shows the result of the query in an ADO.NET Xml DataSet format.</li>
                    <li><i>Result-Xml:</i> shows the result of a &quot;FOR XML AUTO,ELEMENTS&quot; 
                    query in Xml format.</li>
                    <li><i>INSERT/UPDATE/DELETE-Sql:</i> shows the data of a query in Sql format 
                    using the meta data of the table selected.  These output formats are useful
                    for generating Sql to update data across different databases e.g. for deployment scripts</li>
                </ul>
            </li>
        </ul>
    
    </asp:Panel>
    
    <p>&nbsp;</p>
    
</asp:Content>

