<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LocationSearch.aspx.vb" Inherits="Admin_LocationSearch" MasterPageFile="~/Include/AuroraHeader.master" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
<script type="text/javascript">
var townCityItems = <%= Me.TownCityItemsString %>;

var countryDropDownId = '<%= countryDropDown.ClientId %>';
var countryDropDown;
var townCityDropDownId = '<%= townCityDropDown.ClientId %>';
var townCityDropDown;


function townCityFilter()
{
    var oldTctCode = townCityDropDown.value;

    clearDropDown (townCityDropDown);
    addDropDownOption (townCityDropDown, "(All)", "");
    for (var i = 0; i < townCityItems.length; i++)
    {
        var townCityItem = townCityItems[i];
        if ((countryDropDown.value == '') || (countryDropDown.value == townCityItem.CtyCode))
            addDropDownOption (townCityDropDown, townCityItem.Text, townCityItem.Value);
    }
    
    townCityDropDown.value = oldTctCode;
}

function townCityInit()
{
    countryDropDown = document.getElementById (countryDropDownId);
    townCityDropDown = document.getElementById (townCityDropDownId);
    
    townCityFilter();
}

addEvent (window, "load", townCityInit);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(townCityInit);
</script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:Panel ID="locationSearchPanel" runat="Server" Width="780px" DefaultButton="searchButton">
    
        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width:150px">Code:</td>
                <td style="width:250px"><asp:TextBox ID="codeTextBox" runat="server" style="width:75px" MaxLength="12" /></td>
                <td style="width:150px">&nbsp;</td>
                <td style="width:250px">&nbsp;</td>
            </tr>
            <tr>
                <td>Type:</td>
                <td>
                    <asp:DropDownList ID="typeDropDown" runat="server" style="width:250px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                 <td>Zone Location:</td>
                 <td align="right">
                    <asp:DropDownList ID="ZoneLocationDropDown" runat="server" style="width:200px"/>
                </td>
            </tr>
            <tr>
                <td>Country:</td>
                <td>
                    <asp:DropDownList ID="countryDropDown" runat="server" style="width:250px" onchange="townCityFilter();">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>Town / City:</td>
                <td align="right">
                    <asp:DropDownList ID="townCityDropDown" runat="server" style="width:200px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>   
            <tr>
                <td>Manager:</td>
                <td colspan="3">
                    <asp:TextBox ID="managerTextBox" runat="server" style="width:240px" />
                </td>
            </tr>
            <tr>
                <td style="border-top-width: 1px; border-top-style: dotted">Status:</td>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="All" Selected="True" />
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Inactive" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>

        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="border-width: 1px 0px"><i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;</td>
                <td align="right" style="border-width: 1px 0px">
                    <asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                </td>
            </tr>
        </table>
        
        <br />
        
        <asp:Table ID="searchResultTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" Visible="False" CssClass="dataTableColor">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Text="Name" />
                <asp:TableHeaderCell Text="Type" Width="125px" />
                <asp:TableHeaderCell Text="Country" Width="125px" />
                <asp:TableHeaderCell Text="Town/City" Width="125px" />
                <asp:TableHeaderCell Text="Manager" Width="100px" />
                <asp:TableHeaderCell Text="Status" Width="50px" />
            </asp:TableHeaderRow>
        </asp:Table>

    </asp:Panel>

</asp:Content>

