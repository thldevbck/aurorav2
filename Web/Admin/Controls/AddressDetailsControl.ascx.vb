Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.LocationDataSet

Partial Class Admin_AddressDetailsControl
    Inherits AuroraUserControl

    Public _label As String
    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Public _code As String
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property

    Public ReadOnly Property IsBlank() As Boolean
        Get
            Return address1TextBox.Text.Trim() = "" _
             AndAlso address2TextBox.Text.Trim() = "" _
             AndAlso address3TextBox.Text.Trim() = "" _
             AndAlso stateTextBox.Text.Trim() = "" _
             AndAlso postCodeTextBox.Text.Trim() = ""
        End Get
    End Property

    Public Sub Clear()
        address1TextBox.Text = ""
        address2TextBox.Text = ""
        address3TextBox.Text = ""
        stateTextBox.Text = ""
        postCodeTextBox.Text = ""
    End Sub

    Public Sub Build(ByVal locationRow As LocationRow)
        Me.Clear()
        If locationRow.AddressDetailsRow(Code) Is Nothing Then Return

        Dim adr As AddressDetailsRow = locationRow.AddressDetailsRow(code)
        If Not adr.IsAddAddress1Null Then address1TextBox.Text = adr.AddAddress1
        If Not adr.IsAddAddress2Null Then address2TextBox.Text = adr.AddAddress2
        If Not adr.IsAddAddress3Null Then address3TextBox.Text = adr.AddAddress3
        If Not adr.IsAddStateNull Then stateTextBox.Text = adr.AddState
        If Not adr.IsAddPostcodeNull Then postCodeTextBox.Text = adr.AddPostcode
    End Sub

    Public Sub Validate(ByVal create As Boolean)

    End Sub

    Public Sub Update(ByVal locationRow As LocationRow)
        Dim adr As AddressDetailsRow = locationRow.AddressDetailsRow(Code)

        If adr IsNot Nothing And Me.IsBlank Then
            adr.Delete()
            Aurora.Common.Data.ExecuteDataRow(adr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
            locationRow.LocationDataSet.AddressDetails.RemoveAddressDetailsRow(adr)

        ElseIf adr Is Nothing And Not Me.IsBlank Then
            adr = locationRow.LocationDataSet.AddressDetails.NewAddressDetailsRow()
            adr.AddId = Aurora.Common.Data.GetNewId()
            adr.AddPrntID = locationRow.LocCode
            adr.AddPrntTableName = "Location"
            adr.AddCodAddressId = locationRow.LocationDataSet.Code.FindByCodCdtNumCodCode(DataConstants.CodeType_Address_Type, Code).CodId

            adr.AddAddress1 = address1TextBox.Text
            adr.AddAddress2 = address2TextBox.Text
            adr.AddAddress3 = address3TextBox.Text
            adr.AddState = stateTextBox.Text
            adr.AddPostcode = postCodeTextBox.Text
            adr.AddCtyCode = locationRow.TownCityRow.CountryRow.CtyCode

            locationRow.LocationDataSet.AddressDetails.AddAddressDetailsRow(adr)
            Aurora.Common.Data.ExecuteDataRow(adr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        ElseIf adr IsNot Nothing And Not Me.IsBlank Then
            adr.AddAddress1 = address1TextBox.Text
            adr.AddAddress2 = address2TextBox.Text
            adr.AddAddress3 = address3TextBox.Text
            adr.AddState = stateTextBox.Text
            adr.AddPostcode = postCodeTextBox.Text
            adr.AddCtyCode = locationRow.TownCityRow.CountryRow.CtyCode

            Aurora.Common.Data.ExecuteDataRow(adr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        End If
    End Sub


End Class
