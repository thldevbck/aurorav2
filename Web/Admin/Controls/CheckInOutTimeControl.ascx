<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CheckInOutTimeControl.ascx.vb" Inherits="Admin_CheckInOutTimeControl" %>

<%@ Register Src="..\..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>

<tr id="tableRow" runat="Server">
    <td><asp:Label ID="dayOfWeekLabel" runat="server" />:</td>
    <td align="center"><asp:CheckBox ID="closedCheckBox" runat="server" /></td>
    <td style="border-left-width: 1px"><uc1:TimeControl ID="ckoFrTimeControl" runat="server" /></td>
    <td><uc1:TimeControl ID="ckoToTimeControl" runat="server" /></td>
    <td style="border-left-width: 1px"><uc1:TimeControl ID="ckiFrTimeControl" runat="server" /></td>
    <td><uc1:TimeControl ID="ckiToTimeControl" runat="server" /></td>
</tr>
