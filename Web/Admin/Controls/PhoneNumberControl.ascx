<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PhoneNumberControl.ascx.vb" Inherits="Admin_PhoneNumberControl" %>

<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<table style="width:375px" cellpadding="2" cellspacing="0">
    <tr>
        <td style="width:150px">Phone:</td>
        <td><asp:TextBox ID="phoneTextBox" runat="server" Width="150px" /></td>
    </tr>
    <tr>
        <td>Fax:</td>
        <td><asp:TextBox ID="faxTextBox" runat="server" Width="150px" /></td>
    </tr>
    <tr>
        <td>Email:</td>
        <td>
            <uc1:RegExTextBox 
                ID="emailTextBox" 
                runat="server" 
                Nullable="true"
                ErrorMessage="Enter a valid email"
                RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" 
                Width="200px" />                
        </td>
    </tr>
</table>