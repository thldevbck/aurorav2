
Partial Class Admin_Controls_OtherLocationControl
    Inherits AuroraUserControl

#Region "Properties"

    Public ReadOnly Property IsB2Cuser() As Boolean
        Get
            Return isB2CApprover()
        End Get
    End Property

    Public Property LocCode() As String
        Get
            Return ViewState("LocCode")
        End Get
        Set(ByVal value As String)
            ViewState("LocCode") = value
        End Set
    End Property

    Public ReadOnly Property TotalNumberOfChildLocationUnMapped() As String
        Get
            Return Aurora.Admin.Data.DataRepository.TotalNumberOfChildLocationUnMapped(LocCodTypeID, LocTctCode, Me.LocCode)
        End Get

    End Property

    Public ReadOnly Property TotalNumberOfChildLocation() As String
        Get
            Return Aurora.Admin.Data.DataRepository.TotalNumberOfChildLocation(LocCodTypeID, LocTctCode, Me.LocCode)
        End Get
    End Property

    Public ReadOnly Property TotalNumberOfChildLocationUnMappedtoZone() As String
        Get
            Return Aurora.Admin.Data.DataRepository.TotalNumberOfChildLocationUnMappedtoZone(LocCodTypeID, LocTctCode, Me.LocCode)
        End Get
    End Property

    Public Property enabledGrid() As Boolean
        Get
            Return CBool(ViewState("enabledGrid"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("enabledGrid") = value
        End Set
    End Property

    Public Property LocCodTypeID() As String
        Get
            Return ViewState("LocCodTypeID")
        End Get
        Set(ByVal value As String)
            ViewState("LocCodTypeID") = value
        End Set
    End Property

    Public Property LocTctCode() As String
        Get
            Return ViewState("LocTctCode")
        End Get
        Set(ByVal value As String)
            ViewState("LocTctCode") = value
        End Set
    End Property

    Private dsLocationZone As System.Data.DataSet
    Public Property LocationZoneList() As System.Data.DataSet
        Get
            Return dsLocationZone
        End Get
        Set(ByVal value As System.Data.DataSet)
            dsLocationZone = value
        End Set
    End Property

    Private Property GroupBrand() As String
        Get
            Return ViewState("groupBrand")
        End Get
        Set(ByVal value As String)
            ViewState("groupBrand") = value
        End Set
    End Property

    Private Property GroupLocatioRowIndex() As String
        Get
            Return ViewState("GroupLocatioRowIndex")
        End Get
        Set(ByVal value As String)
            ViewState("GroupLocatioRowIndex") = value
        End Set
    End Property
#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then

            Dim dsTemp As Data.DataSet = Aurora.Admin.Data.DataRepository.SelectOtherLocation(Me.LocCodTypeID, Me.LocTctCode, Me.LocCode)
            If dsTemp.Tables.Count = 1 Then
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    BindGrid(dsTemp)
                    Me.noValuePanel.Visible = False
                Else
                    Me.noValuePanel.Visible = True
                    GridView1.Visible = False
                End If
            End If
            Me.GroupBrand = ""
        Else
            GridView1.Caption = GridCaptionMessage()
        End If

        Me.GridView1.Enabled = Me.enabledGrid

        ''check if grid is enabled
        If Me.enabledGrid Then
            Me.GridView1.Enabled = IsB2Cuser
        End If


    End Sub

    Private groupRow As GridViewRow

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim ddlZoneLocation As DropDownList = CType(e.Row.FindControl("ddlZoneLocation"), DropDownList)
            Dim hidLocZoneId As HiddenField = CType(e.Row.FindControl("hidLocZoneId"), HiddenField)
            If ddlZoneLocation IsNot Nothing AndAlso hidLocZoneId IsNot Nothing Then
                ddlZoneLocation.Items.Clear()
                Dim vwzone As System.Data.DataView = LocationZoneList.Tables(0).DefaultView
                Dim item As ListItem = New ListItem("", "")
                ddlZoneLocation.Items.Add(item)
                For Each row As Data.DataRow In vwzone.Table.Rows
                    item = New ListItem(ProperCasing(row("FullZone")), row("LocZoneId"))
                    ddlZoneLocation.Items.Add(item)
                Next
                '' If enabledGrid Then ddlZoneLocation.SelectedValue = IIf(String.IsNullOrEmpty(hidLocZoneId.Value), -1, hidLocZoneId.Value)
                ddlZoneLocation.SelectedValue = IIf(String.IsNullOrEmpty(hidLocZoneId.Value), -1, hidLocZoneId.Value)
            End If


            Dim lblBrand As Label
            If IsSameLocation(e.Row) Then

                If groupRow Is Nothing Then
                    If e.Row.RowIndex > 0 Then
                        groupRow = GridView1.Rows(e.Row.RowIndex - 1)
                    End If
                End If

                Dim litBrandGrouping As Literal = CType(groupRow.FindControl("litBrandGrouping"), Literal)
                If Not litBrandGrouping Is Nothing Then
                    lblBrand = CType(groupRow.FindControl("lblBrand"), Label)
                    If lblBrand IsNot Nothing Then
                        If String.IsNullOrEmpty(litBrandGrouping.Text) Then
                            lblBrand.Visible = False
                            litBrandGrouping.Text = lblBrand.Text & "<br>"
                            lblBrand = CType(e.Row.FindControl("lblBrand"), Label)

                            If Not litBrandGrouping.Text.Contains(lblBrand.Text) Then
                                litBrandGrouping.Text = litBrandGrouping.Text & lblBrand.Text & " <br/>"
                            End If
                        Else
                            lblBrand = CType(e.Row.FindControl("lblBrand"), Label)
                            If Not litBrandGrouping.Text.Contains(lblBrand.Text) Then
                                litBrandGrouping.Text = litBrandGrouping.Text & lblBrand.Text & " <br/>"
                            End If
                        End If
                    End If
                    Me.GroupBrand = litBrandGrouping.Text
                    If GroupLocatioRowIndex > 0 Then
                        Dim prevpage As GridViewRow = GridView1.Rows(GroupLocatioRowIndex)
                        prevpage.Cells(1).Text = litBrandGrouping.Text
                    End If
                End If

                ddlZoneLocation.Enabled = False
                Dim txtLocation As String = e.Row.Cells(2).Text
                txtLocation = txtLocation & " <sup><font color='red'>**</font></sup>"
                e.Row.Cells(2).Text = txtLocation
                e.Row.Visible = False

               
            Else

                If e.Row.RowIndex > 0 Then
                    GroupLocatioRowIndex = e.Row.RowIndex
                Else
                    GroupLocatioRowIndex = -1
                End If
            End If
            
        End If
    End Sub

    Dim sbLocations As StringBuilder
    Dim sbMap As StringBuilder
    Dim sbParameters As StringBuilder
    Private sbSingleLocation As StringBuilder
    Private Property SingleLocation() As String
        Get
            Return CStr(ViewState("SingleLocation"))
        End Get
        Set(ByVal value As String)
            ViewState("SingleLocation") = value
        End Set
    End Property
    Protected Sub SaveMappingButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim _LocCode As String = ""
        Dim _LocZoneId As String = ""
        Dim _modifiedBy As String = MyBase.CurrentPage.UserCode
        Dim retValue As String = ""
        Dim displayMessage As Boolean = False
        Dim ctrMappedtoOneLoction As Integer = 0

        sbLocations = New StringBuilder
        sbMap = New StringBuilder
        sbParameters = New StringBuilder
        sbSingleLocation = New StringBuilder

        Dim sbBlankValue As StringBuilder = New StringBuilder
        Dim blnDisplayPop As Boolean = False
        Dim ddlValue As String = ""
        Try
            For Each griditem As GridViewRow In GridView1.Rows
                Dim ddlZoneLocation As DropDownList = CType(griditem.FindControl("ddlZoneLocation"), DropDownList)
                If ddlZoneLocation IsNot Nothing And ddlZoneLocation.Enabled Then
                    _LocZoneId = IIf(String.IsNullOrEmpty(ddlZoneLocation.SelectedItem.Text), -1, ddlZoneLocation.SelectedValue)
                    _LocCode = griditem.Cells(2).Text.Split("-")(0).TrimEnd

                    ''When more than one location is linked to the same Town / City then a warning message shall be displayed to a user advising two or more locations are assigned 
                    ''ctrMappedtoOneLoction = Aurora.Admin.Data.DataRepository.SelectOtherLocationMappedtoOneLocation(Me.LocCodTypeID, Me.LocTctCode, _LocCode)
                    ''If ctrMappedtoOneLoction > 1 OrElse String.IsNullOrEmpty(ddlZoneLocation.SelectedItem.Text) Then
                    If String.IsNullOrEmpty(ddlZoneLocation.SelectedItem.Text) Then
                        ddlValue = ddlZoneLocation.SelectedItem.Text
                        If String.IsNullOrEmpty(ddlValue) Then ddlValue = "&lt;none&gt;"

                        sbParameters.Append(String.Concat(Me.LocCodTypeID, "|", Me.LocTctCode, "|", _LocCode, "|", _LocZoneId, "|", _modifiedBy, "*"))
                        sbLocations.Append(String.Concat(_LocCode, ", "))
                        sbMap.Append(String.Concat("<br/>" & ddlValue, ", "))

                        blnDisplayPop = True
                    Else
                        sbSingleLocation.Append(String.Concat(Me.LocCodTypeID, "|", Me.LocTctCode, "|", _LocCode, "|", _LocZoneId, "|", _modifiedBy, "*"))
                        SingleLocation = sbSingleLocation.ToString
                        displayMessage = True
                    End If

                    If String.IsNullOrEmpty(ddlZoneLocation.SelectedItem.Text) Then
                        sbBlankValue.Append(griditem.Cells(2).Text.Split("-")(0) & ",")
                    End If
                End If
            Next

            If blnDisplayPop Then

                Dim paramLocations As String = sbLocations.ToString
                Dim paramMapping As String = sbMap.ToString

                If paramLocations.EndsWith(" ") Then paramLocations = paramLocations.Remove(paramLocations.Length - 2)
                If paramMapping.EndsWith(" ") Then paramMapping = paramMapping.Remove(paramMapping.Length - 2)

                Dim unmappedLocation As Integer = CInt(Me.TotalNumberOfChildLocationUnMapped())
                Dim totalLocation As Integer = CInt(Me.TotalNumberOfChildLocation)

                ''Dim tempMsgA As String = "<b>'" & paramLocations & "'</b> location(s) is currently mapped to more than one location.<br/><br/> "
                ''(location code) shall non longer be assigned to a zone. 
                ''Saving this will allow this location code to show separately on the booking pages. 

                Dim tempMsgA As String = ""
                If Not String.IsNullOrEmpty(sbBlankValue.ToString) Then
                    tempMsgA = "('<b>" & sbBlankValue.ToString.Remove(sbBlankValue.ToString.Length - 1, 1) & "</b>') are still assigned to a zone.<br/>"
                End If
                tempMsgA = String.Concat(tempMsgA, "Saving this will allow this location code to show separately on the booking pages.", "<br/><br/>")
                ''tempMsgA = tempMsgA & "'<b>" & unmappedLocation & "</b>' of these locations are not allocated to a zone.<br/><br/>"

                Dim paramLocationAry() As String = paramLocations.Split(",")
                Dim paramMappingAry() As String = paramMapping.Split(",")
                Dim mergeMapping As String = ""
                Dim i As Integer = 0
                If paramLocationAry.Length = paramMappingAry.Length Then
                    For Each param As String In paramLocationAry
                        mergeMapping = "<br/>" & mergeMapping & param & "</b> to <b>" & paramMappingAry(i).Replace("<br/>", "") & "<br/>"
                        i = i + 1
                    Next
                End If


                Dim tempMsgB As String = "" ''"Saving this will force these locations to be map to.. <b>" & IIf(String.IsNullOrEmpty(mergeMapping), paramMapping, mergeMapping) & "</b><br/><br/>"
                Dim tempMsgC As String = "Do you wish to continue?"

                ConfirmationBoxMultipleRecords.Text = String.Concat(tempMsgA, tempMsgB, tempMsgC)

                Me.ConfirmationBoxMultipleRecords.Param = sbParameters.ToString & SingleLocation
                Me.ConfirmationBoxMultipleRecords.Show()
            Else
                If displayMessage Then
                    LocationMappingUpdate(sbSingleLocation.ToString, displayMessage)
                End If
            End If
            If Not displayMessage Then MyBase.CurrentPage.SetWarningShortMessage("No Location and Mapping found.")
   
        Catch ex As Exception
            MyBase.CurrentPage.SetErrorShortMessage(ex.Message.Replace("ERROR:", ""))
        End Try
    End Sub

    Protected Sub ConfirmationBoxMultipleRecords_PostBack(ByVal sender As Object, _
                                                          ByVal leftButton As Boolean, _
                                                          ByVal rightButton As Boolean, _
                                                          ByVal param As String) Handles ConfirmationBoxMultipleRecords.PostBack
        Try
            If leftButton Then
                LocationMappingUpdate(param)
            End If
       
        Catch ex As Exception
            MyBase.CurrentPage.SetErrorShortMessage(ex.Message.Replace("ERROR:", ""))
        End Try
    End Sub


    Sub LocationMappingUpdate(ByVal param As String, Optional ByVal displayMessage As Boolean = False, Optional ByRef ReturnValue As String = "")
        Dim paramListofParameters() As String = param.Split("*")
        Dim retvalue As String = ""
        For Each paramListOfParameterValue As String In paramListofParameters
            Dim paramAry() As String = paramListOfParameterValue.Split("|")
            If paramAry.Length = 5 Then
                retvalue = Aurora.Admin.Data.DataRepository.UpdateLocationZoneMapping(paramAry(0), paramAry(1), paramAry(2), paramAry(3), paramAry(4), ZoneWebEnabledDate)
                If retvalue.Contains("ERROR:") Then Throw New Exception(retvalue)
            End If
        Next

        MyBase.CurrentPage.SetInformationShortMessage(retvalue.Replace("OK:", ""))
        enabledGrid = True
        Dim dsTemp As Data.DataSet = Aurora.Admin.Data.DataRepository.SelectOtherLocation(Me.LocCodTypeID, Me.LocTctCode, Me.LocCode)
        BindGrid(dsTemp)
    End Sub

#Region "Private Function"

    Public Function IsMandatoryLocationCheckingOK() As Boolean
        If Not String.IsNullOrEmpty(TotalNumberOfChildLocationUnMapped) Then
            If CInt(TotalNumberOfChildLocationUnMapped) = 0 Then
                Return True
            Else
                Return False
            End If
        End If
    End Function

    Public Function ConvertDate(ByVal objDate As Object) As String
        If objDate Is Nothing Then Return ""
        If objDate Is DBNull.Value Then Return ""
        Return Convert.ToDateTime(objDate)
    End Function

    Private Function ProperCasing(ByVal FullName As String) As String
        If String.IsNullOrEmpty(FullName) Then Return ""
        Dim strFullPackage() As String = FullName.ToLower.Split(" ")
        Dim tempStrFullPackage As String = ""

        For Each tempStrPackage As String In strFullPackage
            If Not String.IsNullOrEmpty(tempStrPackage) Then
                Dim firstLetter As String = tempStrPackage.Substring(0, 1).ToUpper
                tempStrPackage = tempStrPackage.Remove(0, 1)
                tempStrPackage = firstLetter & tempStrPackage & " "
                tempStrFullPackage = tempStrFullPackage & tempStrPackage
            End If
        Next
        Dim tempZone As String() = tempStrFullPackage.Split("-")
        Return tempZone(0).ToUpper & " - " & tempZone(1)
        Return tempStrFullPackage
    End Function

    Private Function IsSameLocation(ByRef currentRow As GridViewRow) As Boolean
        Dim blnIsSameLocation As Boolean = False
        Dim i As Integer = currentRow.RowIndex
        If i = 0 Then Return False
        Dim previousRow As GridViewRow = Me.GridView1.Rows(i - 1)
        If previousRow.Cells(2).Text.Contains(currentRow.Cells(2).Text) Then
            blnIsSameLocation = True
        End If
        Return blnIsSameLocation
    End Function

    Private Function isB2CApprover() As Boolean
        Dim xmldoc As New System.Xml.XmlDocument
        xmldoc.LoadXml(Aurora.Common.Data.ExecuteScalarSP("sp_B2CApprover", MyBase.CurrentPage.UserCode))
        If xmldoc.SelectSingleNode("isUser") IsNot Nothing Then
            If xmldoc.SelectSingleNode("isUser").InnerText = "True" Then Return True
        End If
        Return False
    End Function

    Public Function GridCaptionMessage() As String
        Me.GridView1.Enabled = enabledGrid
        Me.GridView1.Caption = ""
        If Not enabledGrid Then Return "<b><font-color='red'>Please select Zone in the dropdown to enable this grid</font></b>"
    End Function

    Private ReadOnly Property ZoneWebEnabledDate() As String
        Get
            Dim strDate As String = ""
            Dim cp As ContentPlaceHolder = CType(MyBase.CurrentPage.Master.FindControl("ContentPlaceholder"), ContentPlaceHolder)
            If Not cp Is Nothing Then
                Dim zoneDate As UserControls_DateControl = CType(cp.findcontrol("EnabledDateControl"), UserControls_DateControl)
                If Not zoneDate Is Nothing Then
                    strDate = zoneDate.Date
                End If
            End If
            
            Return strDate
        End Get
    End Property

    Public Property LocationWebEnabledDate() As String
        Get
            Return ViewState("LocatinWebEnabledDate")
        End Get
        Set(ByVal value As String)
            ViewState("LocatinWebEnabledDate") = value
        End Set
    End Property

#End Region

#Region "Private Procedure"
    Sub BindGrid(ByVal ds As Data.DataSet)
        Me.GridView1.DataSource = Nothing
        Me.GridView1.DataBind()

        Me.GridView1.DataSource = ds.Tables(0).DefaultView
        Me.GridView1.DataBind()
    End Sub
#End Region


End Class
