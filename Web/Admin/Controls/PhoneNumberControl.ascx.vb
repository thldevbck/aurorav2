Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.LocationDataSet

Partial Class Admin_PhoneNumberControl
    Inherits AuroraUserControl

    Public _label As String
    Public Property Label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            _label = value
        End Set
    End Property

    Public _tableName As String
    Public Property TableName() As String
        Get
            Return _tableName
        End Get
        Set(ByVal value As String)
            _tableName = value
        End Set
    End Property

    Public Sub Clear()
        Me.phoneTextBox.Text = ""
        Me.faxTextBox.Text = ""
        Me.emailTextBox.Text = ""
    End Sub

    Public Sub Build(ByVal locationRow As LocationRow)
        Me.Clear()

        Dim pnr As PhoneNumberRow

        pnr = locationRow.PhoneNumberRow(TableName, "Phone")
        If pnr IsNot Nothing AndAlso Not pnr.IsPhnNumberNull Then Me.phoneTextBox.Text = pnr.PhnNumber

        pnr = locationRow.PhoneNumberRow(TableName, "Fax")
        If pnr IsNot Nothing AndAlso Not pnr.IsPhnNumberNull Then Me.faxTextBox.Text = pnr.PhnNumber

        pnr = locationRow.PhoneNumberRow(TableName, "Email")
        If pnr IsNot Nothing AndAlso Not pnr.IsPhnEmailAddressNull Then Me.emailTextBox.Text = pnr.PhnEmailAddress
    End Sub

    Public Sub Validate(ByVal create As Boolean)
        If Not emailTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException("Specify a valid " & Label & " email address")
        End If
    End Sub

    Private Sub Update(ByVal locationRow As LocationRow, ByVal code As String, ByVal fieldName As String, ByVal text As String)

        Dim pnr As PhoneNumberRow = locationRow.PhoneNumberRow(TableName, code)
        Dim isBlank As Boolean = String.IsNullOrEmpty(text.Trim())

        If pnr IsNot Nothing And isBlank Then
            pnr.Delete()
            Aurora.Common.Data.ExecuteDataRow(pnr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
            locationRow.LocationDataSet.PhoneNumber.RemovePhoneNumberRow(pnr)

        ElseIf pnr Is Nothing And Not isBlank Then
            pnr = locationRow.LocationDataSet.PhoneNumber.NewPhoneNumberRow()
            pnr.PhnID = Aurora.Common.Data.GetNewId()
            pnr.PhnPrntTableName = TableName
            pnr.PhnPrntId = locationRow.LocCode
            pnr.PhnCodPhoneId = locationRow.LocationDataSet.Code.FindByCodCdtNumCodCode(DataConstants.CodeType_Phone_Number_Type, code).CodId

            pnr(fieldName) = text

            locationRow.LocationDataSet.PhoneNumber.AddPhoneNumberRow(pnr)
            Aurora.Common.Data.ExecuteDataRow(pnr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        ElseIf pnr IsNot Nothing And Not isBlank Then
            pnr(fieldName) = text

            Aurora.Common.Data.ExecuteDataRow(pnr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        End If
    End Sub

    Public Sub Update(ByVal locationRow As LocationRow)
        Update(locationRow, "Phone", "PhnNumber", phoneTextBox.Text)
        Update(locationRow, "Fax", "PhnNumber", faxTextBox.Text)
        Update(locationRow, "Email", "PhnEmailAddress", emailTextBox.Text)
    End Sub

End Class
