Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.LocationDataSet

Partial Class Admin_CheckInOutTimeControl
    Inherits AuroraUserControl

    Public _dayOfWeek As LocationConstants.DayOfWeek = LocationConstants.DayOfWeek.Min
    Public Property DayOfWeek() As LocationConstants.DayOfWeek
        Get
            Return _dayOfWeek
        End Get
        Set(ByVal value As LocationConstants.DayOfWeek)
            _dayOfWeek = value
        End Set
    End Property

    Private ReadOnly Property IsClosed() As Boolean
        Get
            Return closedCheckBox.Checked
        End Get
    End Property

    Private ReadOnly Property IsCkoBlank() As Boolean
        Get
            Return String.IsNullOrEmpty(ckoFrTimeControl.Text.Trim) _
             AndAlso String.IsNullOrEmpty(ckoToTimeControl.Text.Trim)
        End Get
    End Property

    Private ReadOnly Property IsCkiBlank() As Boolean
        Get
            Return String.IsNullOrEmpty(ckiFrTimeControl.Text.Trim) _
             AndAlso String.IsNullOrEmpty(ckiToTimeControl.Text.Trim)
        End Get
    End Property

    Public ReadOnly Property IsBlank() As Boolean
        Get
            Return IsCkoBlank() AndAlso IsCkiBlank()
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tableRow.Attributes.Add("class", IIf(CType(_dayOfWeek, Integer) Mod 2 = 0, "evenRow", "oddRow"))
        dayOfWeekLabel.Text = Server.HtmlEncode(DayOfWeek.GetName(DayOfWeek.GetType(), _dayOfWeek))
    End Sub

    Public Sub Build(ByVal locationSeasonRow As LocationSeasonRow)
        ckoFrTimeControl.Text = ""
        ckoToTimeControl.Text = ""
        ckiFrTimeControl.Text = ""
        ckiToTimeControl.Text = ""

        Dim lio As LocationCheckInOutTimeRow = locationSeasonRow.LocationCheckInOutTimeRow(DayOfWeek)
        If lio IsNot Nothing Then
            closedCheckBox.Checked = False
            If Not lio.IslioCkoFromTimeNull Then ckoFrTimeControl.Time = lio.lioCkoFromTime.TimeOfDay
            If Not lio.IslioCkoToTimeNull Then ckoToTimeControl.Time = lio.lioCkoToTime.TimeOfDay
            If Not lio.IslioCkiFromTimeNull Then ckiFrTimeControl.Time = lio.lioCkiFromTime.TimeOfDay
            If Not lio.IslioCkiToTimeNull Then ckiToTimeControl.Time = lio.lioCkiToTime.TimeOfDay
        Else
            closedCheckBox.Checked = True
        End If
    End Sub

    Public Sub BuildNew(ByVal startTime As TimeSpan, ByVal endTime As TimeSpan)
        closedCheckBox.Checked = False
        ckoFrTimeControl.Time = startTime
        ckoToTimeControl.Time = endTime
        ckiFrTimeControl.Time = startTime
        ckiToTimeControl.Time = endTime
    End Sub


    Public Sub Validate(ByVal create As Boolean)
        If (Me.IsBlank OrElse Me.IsClosed) Then
            ckoFrTimeControl.Text = ""
            ckoToTimeControl.Text = ""
            ckiFrTimeControl.Text = ""
            ckiToTimeControl.Text = ""
            Return
        Else
            If Not IsCkoBlank _
             AndAlso (ckoFrTimeControl.Time = TimeSpan.MinValue _
              OrElse ckoToTimeControl.Time = TimeSpan.MinValue _
              OrElse Not ckoFrTimeControl.IsValid _
              OrElse Not ckoToTimeControl.IsValid _
              OrElse ckoFrTimeControl.Time > ckoToTimeControl.Time) Then
                Throw New Aurora.Common.ValidationException("A valid check-out time range must be specified for " + LocationConstants.DayOfWeekNames(DayOfWeek))
            End If

            If Not IsCkiBlank _
             AndAlso (ckiFrTimeControl.Time = TimeSpan.MinValue _
              OrElse ckiToTimeControl.Time = TimeSpan.MinValue _
              OrElse Not ckiFrTimeControl.IsValid _
              OrElse Not ckiToTimeControl.IsValid _
              OrElse ckiFrTimeControl.Time > ckiToTimeControl.Time) Then
                Throw New Aurora.Common.ValidationException("A valid check-in time range must be specified for " + LocationConstants.DayOfWeekNames(DayOfWeek))
            End If
        End If
    End Sub

    Public Sub Update(ByVal locationSeasonRow As LocationSeasonRow)
        Dim lio As LocationCheckInOutTimeRow = locationSeasonRow.LocationCheckInOutTimeRow(DayOfWeek)

        If lio IsNot Nothing AndAlso (Me.IsBlank OrElse Me.IsClosed) Then
            lio.Delete()
            Aurora.Common.Data.ExecuteDataRow(lio, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
            locationSeasonRow.LocationDataSet.LocationCheckInOutTime.RemoveLocationCheckInOutTimeRow(lio)

        ElseIf Not (Me.IsBlank OrElse Me.IsClosed) Then
            If lio Is Nothing Then
                lio = locationSeasonRow.LocationDataSet.LocationCheckInOutTime.NewLocationCheckInOutTimeRow()
                lio.lioLosId = locationSeasonRow.LosId
                lio.lioDayOfWeek = CType(DayOfWeek, Integer)
                lio.SetlioCkoFromTimeNull()
                lio.SetlioCkoToTimeNull()
                lio.SetlioCkiFromTimeNull()
                lio.SetlioCkiToTimeNull()
                locationSeasonRow.LocationDataSet.LocationCheckInOutTime.AddLocationCheckInOutTimeRow(lio)
            End If

            If Not IsCkoBlank Then
                lio.lioCkoFromTime = New Date(1900, 1, 1) + ckoFrTimeControl.Time
                lio.lioCkoToTime = New Date(1900, 1, 1) + ckoToTimeControl.Time
            Else
                lio.SetlioCkoFromTimeNull()
                lio.SetlioCkoToTimeNull()
            End If

            If Not IsCkiBlank Then
                lio.lioCkiFromTime = New Date(1900, 1, 1) + ckiFrTimeControl.Time
                lio.lioCkiToTime = New Date(1900, 1, 1) + ckiToTimeControl.Time
            Else
                lio.SetlioCkiFromTimeNull()
                lio.SetlioCkiToTimeNull()
            End If

            Aurora.Common.Data.ExecuteDataRow(lio, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        End If

    End Sub

End Class
