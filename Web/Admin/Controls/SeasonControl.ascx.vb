Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.LocationDataSet

Partial Class Admin_SeasonControl
    Inherits AuroraUserControl

    Private _checkInOutTimeControlList As New List(Of Admin_CheckInOutTimeControl)

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        _checkInOutTimeControlList.Add(checkInOutTimeControl0)
        _checkInOutTimeControlList.Add(checkInOutTimeControl1)
        _checkInOutTimeControlList.Add(checkInOutTimeControl2)
        _checkInOutTimeControlList.Add(checkInOutTimeControl3)
        _checkInOutTimeControlList.Add(checkInOutTimeControl4)
        _checkInOutTimeControlList.Add(checkInOutTimeControl5)
        _checkInOutTimeControlList.Add(checkInOutTimeControl6)
    End Sub

    Private Sub RefreshDateColor()
        Dim now As Date = Date.Now.Date
        If FromDate = Date.MinValue OrElse ToDate = Date.MinValue Then
            periodTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.ErrorColor)
        ElseIf now > FromDate AndAlso now > ToDate Then
            periodTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.PastColor)
        ElseIf now < FromDate AndAlso now < ToDate Then
            periodTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.FutureColor)
        Else
            periodTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.CurrentColor)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsDeleted Then
            seasonPanel.Style.Add("display", "none")
        Else
            seasonPanel.Style.Remove("display")
        End If

        RefreshDateColor()
    End Sub

    Public Property LosId() As String
        Get
            Return idHidden.Value
        End Get
        Set(ByVal value As String)
            idHidden.Value = value
        End Set
    End Property

    Public Property IsDeleted() As Boolean
        Get
            Return Boolean.Parse(deletedHidden.Value)
        End Get
        Set(ByVal value As Boolean)
            deletedHidden.Value = value.ToString()
        End Set
    End Property

    Public ReadOnly Property FromDate() As DateTime
        Get
            Return fromDateControl.Date
        End Get
    End Property

    Public ReadOnly Property ToDate() As DateTime
        Get
            Return toDateControl.Date
            'Return toDateControl.Date.Add(New TimeSpan(23, 59, 0))
        End Get
    End Property

    Public Sub Build(ByVal locationSeasonRow As LocationSeasonRow)
        If locationSeasonRow Is Nothing Then Throw New ArgumentNullException()

        Me.LosId = locationSeasonRow.LosId
        Me.IsDeleted = False
        nameTextBox.Text = locationSeasonRow.LosName
        fromDateControl.Date = locationSeasonRow.LosDateFrom
        toDateControl.Date = locationSeasonRow.LosDateTo

        For Each checkInOutTimeControl As Admin_CheckInOutTimeControl In _checkInOutTimeControlList
            checkInOutTimeControl.Build(locationSeasonRow)
        Next

        RefreshDateColor()
    End Sub

    Public Sub BuildNew(ByVal fromDate As Date, ByVal toDate As Date, ByVal startTime As TimeSpan, ByVal endTime As TimeSpan)
        Me.LosId = ""
        Me.IsDeleted = False
        nameTextBox.Text = ""
        fromDateControl.Date = fromDate
        toDateControl.Date = toDate

        For Each checkInOutTimeControl As Admin_CheckInOutTimeControl In _checkInOutTimeControlList
            checkInOutTimeControl.BuildNew(startTime, endTime)
        Next

        RefreshDateColor()
    End Sub


    Public Sub Validate(ByVal create As Boolean)
        If Me.IsDeleted Then Return

        If fromDateControl.Date = Date.MinValue Then
            Throw New ValidationException("A valid season period from date must be specified")
        End If
        If toDateControl.Date = Date.MinValue Then
            Throw New ValidationException("A valid season period to date must be specified")
        End If
        If toDateControl.Date < fromDateControl.Date Then
            Dim d As Date = toDateControl.Date
            toDateControl.Date = fromDateControl.Date
            fromDateControl.Date = d
        End If

        For Each checkInOutTimeControl As Admin_CheckInOutTimeControl In _checkInOutTimeControlList
            checkInOutTimeControl.Validate(create)
        Next
    End Sub

    Public Sub Update(ByVal locationRow As LocationRow)
        Dim los As LocationSeasonRow = locationRow.LocationSeasonRow(Me.LosId)

        If los IsNot Nothing AndAlso Me.IsDeleted Then
            los.Delete()
            Aurora.Common.Data.ExecuteDataRow(los, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
            locationRow.LocationDataSet.LocationSeason.RemoveLocationSeasonRow(los)

        ElseIf Not Me.IsDeleted Then
            If los Is Nothing Then
                los = locationRow.LocationDataSet.LocationSeason.NewLocationSeasonRow()
                los.LosId = Aurora.Common.Data.GetNewId()
                los.LosLocCode = locationRow.LocCode
                los.LosName = ""
                los.LosDateFrom = Date.Now.Date
                los.LosDateTo = Date.Now.Date
                locationRow.LocationDataSet.LocationSeason.AddLocationSeasonRow(los)
            End If

            los.LosName = nameTextBox.Text
            los.LosDateFrom = fromDateControl.Date

            'commented by: Raj / 28 Feb 2011
            'los.LosDateTo = toDateControl.Date

            'modified by: Raj / 28 Feb 2011

            'toDate field appended to include the time(hours minutes and seconds specifically - '23:59:00')
            Dim toDate As New Date(toDateControl.Date.Year, toDateControl.Date.Month, toDateControl.Date.Day, 23, 59, 0)

            los.LosDateTo = toDate
            Aurora.Common.Data.ExecuteDataRow(los, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

            For Each checkInOutTimeControl As Admin_CheckInOutTimeControl In _checkInOutTimeControlList
                checkInOutTimeControl.Update(los)
            Next
        End If

    End Sub


    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click
        deletedHidden.Value = True.ToString()
        seasonPanel.Style.Add("display", "none")
    End Sub

End Class
