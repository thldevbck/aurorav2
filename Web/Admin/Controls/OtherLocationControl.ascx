<%@ Control Language="VB" AutoEventWireup="false" CodeFile="OtherLocationControl.ascx.vb"
    Inherits="Admin_Controls_OtherLocationControl" %>
<%@ Register Src="../../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx"
    TagName="ConfirmationBoxControl" TagPrefix="uc2" %>
<%@ Register Src="../../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<br />
<table style="width: 200px" cellpadding="2" cellspacing="0">
    <tr runat="server" id="trEnabledDate">
        <%--<td>
            Enabled Date:</td>
        <td>
            <uc1:DateControl ID="EnabledDateControl" runat="server" Width="200px" />
        </td>--%>
        <%--<td>
            Zone:
        </td>
        <td>
            <asp:DropDownList ID="typeDropDownZone" runat="server" Style="width: 200px" AutoPostBack="true">
                <asp:ListItem />
            </asp:DropDownList>
        </td>--%>
    </tr>
</table>
<hr />
<asp:Panel runat="server" ID="noValuePanel">
    <table style="width: 750px" class="dataTableGrid">
        <thead>
            <tr>
                <td>
                    Company</td>
                <td>
                    Brand
                </td>
                <td>
                    Locations
                </td>
                <td>
                    Zone
                </td>
            </tr>
        </thead>
        <tr class="oddRow">
            <td style="width: 200">
            </td>
            <td style="width: 200">
            </td>
            <td style="width: 200">
            </td>
            <td style="width: 300">
                <asp:DropDownList ID="ddlZoneLocationNoValue" runat="server" Width="100%" Enabled="false" />
            </td>
        </tr>
        <tr class="evenRow">
            <td colspan="4" align="right">
                <asp:Button ID="SaveMappingButtonNoValue" Text="Save Mapping" runat="server" CssClass="Button_Standard Button_Save"
                    Enabled="False" />
            </td>
        </tr>
    </table>
</asp:Panel>
<%--<asp:UpdatePanel ID="updatepanelChildLocation" runat ="server">
    <ContentTemplate>--%>
        <asp:GridView ID="GridView1" runat="server" DataKeyNames="LocCode" CssClass="dataTableGrid"
            AutoGenerateColumns="False" Width="100%" ShowFooter="true" Caption="<%# GridCaptionMessage %>"
            Enabled="<%# IsB2Cuser %>">
            <RowStyle CssClass="evenRow" />
            <AlternatingRowStyle CssClass="oddRow" />
            <FooterStyle CssClass="oddRow" HorizontalAlign="Right" />
            <Columns>
                <asp:BoundField HeaderText="Company" DataField="CompanyName" ItemStyle-Width="200"
                    ItemStyle-VerticalAlign="Top" />
                <%--<asp:BoundField HeaderText="Brand" ItemStyle-Width="150" DataField="BrdName"/>--%>
                <asp:TemplateField HeaderText="Brand" ItemStyle-Width="150" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:Label ID="lblBrand" runat="server" Text='<%# eval("BrdName") %>' />
                        <asp:Literal ID="litBrandGrouping" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Locations" DataField="LocationName" ItemStyle-Width="250"
                    ItemStyle-VerticalAlign="Top" />
                <asp:TemplateField HeaderText="Zone" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlZoneLocation" runat="server" Width="100%" EnableViewState="true" />
                        <asp:HiddenField ID="hidLocZoneId" runat="server" Value='<%# eval("LocZoneId") %>' />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="SaveMappingButton" Text="Save Mapping" runat="server" CssClass="Button_Standard Button_Save"
                            OnClick="SaveMappingButton_Click" />
                    </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
        <uc2:ConfirmationBoxControl ID="ConfirmationBoxMultipleRecords" runat="server" />
<%--    </ContentTemplate>
</asp:UpdatePanel>
--%>