<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SeasonControl.ascx.vb" Inherits="Admin_SeasonControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>

<%@ Register Src="CheckInOutTimeControl.ascx" TagName="CheckInOutTimeControl" TagPrefix="uc1" %>

<asp:Panel ID="seasonPanel" runat="server">

<hr />

<asp:HiddenField ID="idHidden" runat="server" />
<asp:HiddenField ID="deletedHidden" runat="server" Value="false" />

<table cellpadding="2" cellspacing="0" class="dataTable">
    <tr>
        <th colspan="4" valign="middle">Season</th>
        <th colspan="2" style="text-align: right">
            <asp:Button ID="removeButton" runat="server" Text="Remove" CssClass="Button_Standard Button_Remove" />
        </th>
    </tr>
    <tr>
        <td colspan="1">Name:</td>
        <td colspan="5">
            <asp:TextBox ID="nameTextBox" runat="server" style="width:250px" />
        </td>
    </tr>
    <tr id="periodTableRow" runat="server">
        <td colspan="1" style="border-top-width: 1px">Period:</td>
        <td colspan="2" style="border-top-width: 1px">From: <uc1:DateControl ID="fromDateControl" runat="server" Nullable="false" />&nbsp;*</td>
        <td colspan="3" style="border-top-width: 1px">To: <uc1:DateControl ID="toDateControl" runat="server" Nullable="false" />&nbsp;*</td>
    </tr>
    <tr>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th colspan="2" style="border-left-width: 1px">Check-out</th>
        <th colspan="2" style="border-left-width: 1px">Check-in</th>
    </tr>
    <tr>
        <th style="width:150px">&nbsp;</th>
        <th style="width:50px; text-align: center">Closed</th>
        <th style="width:100px; border-left-width: 1px">From</th>
        <th style="width:100px">To</th>
        <th style="width:100px; border-left-width: 1px">From</th>
        <th style="width:100px">To</th>
    </tr>
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl0" runat="server" DayOfWeek="Monday" />
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl1" runat="server" DayOfWeek="Tuesday" />
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl2" runat="server" DayOfWeek="Wednesday" />
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl3" runat="server" DayOfWeek="Thursday" />
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl4" runat="server" DayOfWeek="Friday" />
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl5" runat="server" DayOfWeek="Saturday" />
    <uc1:CheckInOutTimeControl ID="checkInOutTimeControl6" runat="server" DayOfWeek="Sunday" />
</table>

</asp:Panel>