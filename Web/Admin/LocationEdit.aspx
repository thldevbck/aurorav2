<%@ Page Language="VB" AutoEventWireup="false" CodeFile="LocationEdit.aspx.vb" Inherits="Admin_LocationEdit"
    MasterPageFile="~/Include/AuroraHeader.master" EnableEventValidation="false" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ConfirmationBoxExtended/ConfirmationBoxControlExtended.ascx"
    TagName="ConfirmationBoxControlExtended" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc2" %>
<%@ Register Src="Controls/AddressDetailsControl.ascx" TagName="AddressDetailsControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/PhoneNumberControl.ascx" TagName="PhoneNumberControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/SeasonControl.ascx" TagName="SeasonControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/CheckInOutTimeControl.ascx" TagName="CheckInOutTimeControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/OtherLocationControl.ascx" TagName="OtherLocationControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc10" %>
<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .ajax__tab_panel
        {
            min-height: 350px;
            height: auto !important;
            height: 350px;
            padding-top: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
    <script type="text/javascript">

//rev:mia Nov.4 2011
 function IsValidSized(sender) {
        if (isNaN(sender.value) && (sender.value.length != 0)) {
            sender.value = ""
        }
    }

var townCityItems = <%= Me.TownCityItemsString %>;

var countryDropDownId = '<%= countryDropDown.ClientId %>';
var countryDropDown;
var townCityDropDownId = '<%= townCityDropDown.ClientId %>';
var townCityDropDown;


function townCityFilter()
{
    var oldTctCode = townCityDropDown.value;

    clearDropDown (townCityDropDown);
    addDropDownOption (townCityDropDown, "", "");
    for (var i = 0; i < townCityItems.length; i++)
    {
        var townCityItem = townCityItems[i];
        if ((countryDropDown.value == '') || (countryDropDown.value == townCityItem.CtyCode))
            addDropDownOption (townCityDropDown, townCityItem.Text, townCityItem.Value);
    }
    
    townCityDropDown.value = oldTctCode;
}

function townCityInit()
{
    countryDropDown = document.getElementById (countryDropDownId);
    townCityDropDown = document.getElementById (townCityDropDownId);
    
    townCityFilter();
}

function FixTableLayout(tableID)
{
    var tableToFix = $get(tableID);
    for(var i = 0 ; i< tableToFix.childNodes(0).childNodes.length; i++)
    {
        var oRow = tableToFix.childNodes(0).childNodes(i);
        for(var j=0;j<oRow.childNodes.length;j++)
        {
            var oCell = oRow.childNodes(j);
            oCell.style.width = "150px";
        }
    }
}
addEvent (window, "load", townCityInit);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(townCityInit);


                        var pbControl = null;
                        var prm = Sys.WebForms.PageRequestManager.getInstance();

                        prm.add_beginRequest(BeginRequestHandler);
                        prm.add_endRequest(EndRequestHandler);

                        function BeginRequestHandler(sender, args) {
                            pbControl = args.get_postBackElement();  //the control causing the postback
                            if(pbControl.id.indexOf('leftButton') > -1 || pbControl.id.indexOf('updateButton') > -1 || pbControl.id.indexOf('SaveMappingButton') > -1)
                            {
                                pbControl.disabled = true;
                            }
                        }

                        function EndRequestHandler(sender, args) {
                            if(pbControl.id.indexOf('leftButton') > -1 || pbControl.id.indexOf('updateButton') > -1 || pbControl.id.indexOf('SaveMappingButton') > -1)
                            {
                                pbControl.disabled = false;
                            }

                            pbControl = null;
                        }    
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder" ID="ContentPlaceholder1">
    <asp:Panel ID="locationPanel" runat="Server" Width="780px">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table style="width: 100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width: 150px">
                            Code:
                        </td>
                        <td style="width: 250px">
                            <asp:TextBox ID="codeTextBox" runat="server" Style="width: 195px" MaxLength="12" />&nbsp;*
                        </td>
                        <td style="width: 150px">
                            Name:
                        </td>
                        <td style="width: 250px">
                            <asp:TextBox ID="nameTextBox" runat="server" Style="width: 255px" />&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Type:
                        </td>
                        <td>
                            <asp:DropDownList ID="typeDropDown" runat="server" Style="width: 200px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                        <%--<td>
                            Zone:</td>
                        <td>
                            <asp:DropDownList ID="typeDropDownZone" runat="server" Style="width: 200px" AutoPostBack="true">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>--%>
                        <td>
                            Web Enabled / Zone:
                        </td>
                        <td>
                            <uc1:DateControl ID="EnabledDateControl" runat="server" Width="100px" />
                            &nbsp;
                            <asp:DropDownList ID="typeDropDownZone" runat="server" Style="width: 150px" AutoPostBack="true" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Country:
                        </td>
                        <td>
                            <asp:DropDownList ID="countryDropDown" runat="server" Style="width: 200px" onchange="townCityFilter();">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                        <td>
                            Town / City:
                        </td>
                        <td>
                            <asp:DropDownList ID="townCityDropDown" runat="server" Style="width: 260px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Hours Behind Server:
                        </td>
                        <td>
                            <uc1:RegExTextBox ID="hoursBehindServerTextBox" runat="server" Nullable="false" ErrorMessage="Enter a valid hours behind server value (between 0.00 to 23.99 hours)"
                                RegExPattern="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$" Width="100px" />
                            &nbsp;*
                        </td>
                        <td>
                            Default Currency Code:
                        </td>
                        <td>
                            <asp:DropDownList ID="currencyDropDown" runat="server" Style="width: 260px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr runat="server" id="MaxCheck">
                        <td>
                            Max CheckOuts/Ins:
                        </td>
                        <td>
                            <uc1:RegExTextBox ID="maximumCheckOutInsTextBox" runat="server" Nullable="true" ErrorMessage="Enter a valid maximum checkout/ins amount"
                                RegExPattern="^\d*[0-9]$" Width="100px" ReadOnly="true" />
                        </td>
                        <td>
                            Throughput Penalty:
                        </td>
                        <td>
                            <uc1:RegExTextBox ID="throughputPenaltyCostTextBox" runat="server" Nullable="true"
                                ErrorMessage="Enter a valid throughput penalty cost" RegExPattern="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$"
                                Width="100px" ReadOnly="true" />
                        </td>
                    </tr>

                    <%--''rev:mia Nov.4 2011--%>
                    <tr>
                        <td>
                            Location Number:
                        </td>
                        <td>
                            <asp:TextBox ID="TextBoxLocation" runat="server" Style="width: 100px"   />
                        </td>
                        <td>Third Party Loc Code:</td>
                        <td>
                            <asp:TextBox ID="txtThirdPartyLocCode" runat="server" Style="width: 150px"   />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="border-top-width: 1px;">
                            Auto Pre-Checkout
                        </td>
                        <td style="border-top-width: 1px;" colspan="3">
                            <span>
                                Schedule Time:&nbsp;
                                <asp:DropDownList ID="ddlPreCheckoutScheduleTime" runat="server" EnableViewState="false" Style="width: 200px">
                                    <asp:ListItem />
                                </asp:DropDownList>
                            </span>
                            <span style="margin-left:30px;">
                                Completed Date:&nbsp;
                                <uc1:DateControl ID="datCompletedDate" runat="server" Width="100px" />
                            </span>
                        </td>
                    </tr>
                    <tr runat="Server" id="statusTableRow">
                        <td style="border-width: 1px 0px;">
                            Status:
                        </td>
                        <td colspan="3" style="border-width: 1px 0px;">
                            <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal"
                                RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                                <asp:ListItem Text="Active" />
                                <asp:ListItem Text="Inactive" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <%--Shoel:6.3.9:Phoenix Changes--%>
                    <tr id="trLocationBrand" runat="server">
                        <td style="border-width: 1px 0px;">
                            Web Brands:
                        </td>
                        <td colspan="3" style="border-width: 1px 0px;">
                            <asp:CheckBoxList ID="cblLocationBrand" runat="server" Width="100%" RepeatDirection="horizontal"
                                RepeatColumns="4" RepeatLayout="table">
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                    <tr id="trLocationClass" runat="server">
                        <td style="border-width: 1px 0px;" nowrap="nowrap">
                            Available Vehicle Classes:
                        </td>
                        <td colspan="5" style="border-width: 1px 0px;" width="500">
                            <table width="50%">
                                <tr>
                                    <td>
                                        <asp:CheckBoxList ID="cblLocationClass" runat="server" RepeatColumns="4" Width="100%"
                                            RepeatDirection="horizontal" CssClass="repeatLabelTable" RepeatLayout="table" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <script language="javascript" type="text/javascript">
                    try { FixTableLayout("<%=cblLocationBrand.ClientID%>"); } catch (err) { }
                    try { FixTableLayout("<%=cblLocationClass.ClientID%>"); } catch (err) { }
                </script>
                <%--Shoel:6.3.9:Phoenix Changes--%>
                <asp:Panel ID="tabsPanel" runat="server" Style="margin-top: 10px">
                    <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="0" Width="100%">
                        <ajaxToolkit:TabPanel runat="server" ID="managerTabPanel" HeaderText="Manager">
                            <ContentTemplate>
                                <table cellpadding="2" cellspacing="0">
                                    <tr>
                                        <td style="width: 150px">
                                            Manager Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="managerNameTextBox" runat="server" Width="200px" />
                                        </td>
                                    </tr>
                                </table>
                                <uc1:PhoneNumberControl ID="managerPhoneNumberControl" runat="server" Label="manager"
                                    TableName="Location" />
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel runat="server" ID="timeTabPanel" HeaderText="Operating Hours">
                            <ContentTemplate>
                                <b>Default:</b>
                                <table cellpadding="2" cellspacing="0" class="dataTable">
                                    <tr>
                                        <th style="width: 150px">
                                            &nbsp;
                                        </th>
                                        <th style="width: 100px">
                                            From:
                                        </th>
                                        <th style="width: 100px">
                                            To:
                                        </th>
                                    </tr>
                                    <tr class="evenRow">
                                        <td>
                                            Morning (AM):
                                        </td>
                                        <td>
                                            <uc1:TimeControl ID="businessTimeFromAMTextBox" runat="server" Nullable="false" />
                                            &nbsp;*
                                        </td>
                                        <td>
                                            <uc1:TimeControl ID="businessTimeToAMTextBox" runat="server" Nullable="false" />
                                            &nbsp;*
                                        </td>
                                    </tr>
                                    <tr class="oddRow">
                                        <td>
                                            Evening (PM):
                                        </td>
                                        <td>
                                            <uc1:TimeControl ID="businessTimeFromPMTextBox" runat="server" Nullable="false" />
                                            &nbsp;*
                                        </td>
                                        <td>
                                            <uc1:TimeControl ID="businessTimeToPMTextBox" runat="server" Nullable="false" />
                                            &nbsp;*
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <div style="height: 25px;">
                                    <asp:Button ID="seasonAddButton" runat="server" Text="Add Season" CssClass="Button_Standard Button_Add"
                                        Style="float: right" />
                                    <asp:Image runat="server" ImageUrl="~\Images\season.png" ImageAlign="AbsMiddle" />
                                    <b>Seasonal Periods:</b>
                                </div>
                                <div style="display: none">
                                    <!%-- horrible hack to force date control scripts to be loaded in dynamically added
                                    stuff %>
                                    <uc1:DateControl ID="dummyDateControl" runat="server" />
                                </div>
                                <asp:HiddenField ID="seasonCountHidden" runat="server" />
                                <asp:Panel ID="seasonsPanel" runat="server" />
                                <p>
                                    <i>Note: If no seasonal periods are specified, or no seasonal period applies, the default
                                        operating hours are used.</i></p>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel runat="server" ID="locationAddressTabPanel" HeaderText="Location Address">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td style="width: 50%; border-right-style: dotted; border-right-width: 1px">
                                            <b>Physical Address:</b>
                                            <uc1:AddressDetailsControl ID="physicalAddressDetailsControl" runat="server" Label="physical address"
                                                Code="Physical" />
                                        </td>
                                        <td style="width: 50%">
                                            <b>Postal Address:</b>
                                            <uc1:AddressDetailsControl ID="postalAddressDetailsControl" runat="server" Label="postal address"
                                                Code="Postal" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel runat="server" ID="thirdPartyAddressTabPanel" HeaderText="Third Party Address">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td>
                                            <b>Third Party Address:</b>
                                            <uc1:AddressDetailsControl ID="thirdPartyAddressDetailsControl" runat="server" Label="third party"
                                                Code="ThirdParty" />
                                            <uc1:PhoneNumberControl ID="thirdPartyPhoneNumberControl" runat="server" Label="third party"
                                                TableName="LocationThird" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <ajaxToolkit:TabPanel runat="server" ID="airportAddressTabPanel" HeaderText="Airport Address">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td style="width: 50%; border-right-style: dotted; border-right-width: 1px">
                                            <b>International Airport Address:</b>
                                            <uc1:AddressDetailsControl ID="internationalAirportAddressDetailsControl" runat="server"
                                                Label="international airport" Code="Airport" />
                                            <uc1:PhoneNumberControl ID="internationalAirportPhoneNumberControl" runat="server"
                                                Label="international airport" TableName="LocationAir" />
                                        </td>
                                        <td style="width: 50%">
                                            <b>Domestic Airport Address:</b>
                                            <uc1:AddressDetailsControl ID="domesticAirportAddressDetailsControl" runat="server"
                                                Label="domestic airport" Code="DomAirport" />
                                            <uc1:PhoneNumberControl ID="domesticAirportPhoneNumberControl" runat="server" Label="domestic airport"
                                                TableName="LocationDomAir" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                        <%--rev:mia feb 3 added zone location--%>
                        <ajaxToolkit:TabPanel runat="server" ID="OtherLocationTabPanel" HeaderText="WebZone Allocation">
                            <ContentTemplate>
                                <table cellspacing="0" cellpadding="2">
                                    <tr>
                                        <td style="width: 50%">
                                            <b>Zones and Locations Mapping</b>
                                            <br />
                                            <uc1:OtherLocationControl ID="OtherLocationControl" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </ajaxToolkit:TabPanel>
                    </ajaxToolkit:TabContainer>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <uc4:ConfirmationBoxControlExtended ID="ConfirmationBoxControlExtended1" runat="server"
            Title="Zone Location Mapping" Text="There are some locations that needs to be map? Do you want to continue without mapping these to its Zone?" />
        <uc2:ConfirmationBoxControl ID="ConfirmationBox" runat="server" Title="Zone Location Mapping" />
        <uc2:ConfirmationBoxControl ID="ConfirmationBoxWebEnabledDate" runat="server" Title="WebEnabled Date" />
        <uc10:ConfirmationBoxControl ID="ConfirmationBoxControlZoneLocation" runat="server"
            Title="Zone Location Mapping" />
        <table style="width: 100%; margin-top: 5px" cellpadding="2" cellspacing="0">
            <tr>
                <td align="right">
                    <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        Visible="False" />
                    <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        Visible="False" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
