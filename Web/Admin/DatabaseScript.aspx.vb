Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Xml
Imports System.IO

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

<AuroraFunctionCodeAttribute("DBSCRIPT")> _
Partial Class Admin_DatabaseScript
    Inherits AuroraPage

    Public Class DataBaseType
        Public Type As String
        Public Name As String
        Public ListItems As New List(Of ListItem)
        Public DatabaseCollapsiblePanel As UserControls_CollapsiblePanel

        Public Sub New(ByVal type As String, ByVal name As String)
            Me.Type = type
            Me.Name = name
        End Sub
    End Class

    Private _dataBaseTypes As New List(Of DataBaseType)
    Private _systemObjectsTable As New DataTable("SystemObjects")

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        _dataBaseTypes.Add(New DataBaseType("V", "Views"))
        _dataBaseTypes.Add(New DataBaseType("P", "Stored Procedures"))
        _dataBaseTypes.Add(New DataBaseType("FN", "Functions"))

        Aurora.Common.Data.ExecuteTableSql("SELECT type, name, id FROM sysobjects WHERE Type IN ('FN', 'P', 'V') ORDER BY Type, name", _systemObjectsTable)

        databaseRepeater.DataSource = _dataBaseTypes
        databaseRepeater.DataBind()
    End Sub

    Protected Sub databaseRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles databaseRepeater.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item AndAlso e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim dataBaseType As DataBaseType = e.Item.DataItem

        Dim databaseCollapsiblePanel As UserControls_CollapsiblePanel = e.Item.FindControl("databaseCollapsiblePanel")
        Dim databaseCheckBoxList As CheckBoxList = e.Item.FindControl("databaseCheckBoxList")
        Dim selectAllHyperLink As HyperLink = e.Item.FindControl("selectAllHyperLink")
        Dim selectNoneHyperLink As HyperLink = e.Item.FindControl("selectNoneHyperLink")

        dataBaseType.DatabaseCollapsiblePanel = databaseCollapsiblePanel

        databaseCollapsiblePanel.Title = dataBaseType.Name
        selectAllHyperLink.Attributes.Add("onclick", "database_selectAllNone('" & databaseCheckBoxList.ClientID & "', true); return false;")
        selectNoneHyperLink.Attributes.Add("onclick", "database_selectAllNone('" & databaseCheckBoxList.ClientID & "', false); return false;")

        For Each systemObjectDataRow As DataRow In _systemObjectsTable.Rows
            Dim type As String = systemObjectDataRow("Type")
            Dim name As String = systemObjectDataRow("Name")
            Dim id As String = systemObjectDataRow("Id")

            If type.Trim() <> dataBaseType.Type Then Continue For

            Dim li As New ListItem(name, id)
            databaseCheckBoxList.Items.Add(li)
            dataBaseType.ListItems.Add(li)
        Next
    End Sub


    Protected Sub generateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles generateButton.Click

        Dim selected As Boolean = False

        Using writer As New StringWriter()
            For Each dataBaseType As DataBaseType In _dataBaseTypes
                Dim dataBaseTypeSelected As Boolean = False
                For Each listItem As ListItem In dataBaseType.ListItems
                    If Not listItem.Selected Then Continue For

                    dataBaseTypeSelected = True
                    selected = True
                Next
                If Not dataBaseTypeSelected Then Continue For

                writer.WriteLine("-- ************************************************************************")
                writer.WriteLine(Aurora.Common.EscapeSqlSingleLineComment(dataBaseType.Name))
                writer.WriteLine("-- ************************************************************************")
                writer.WriteLine()
                writer.WriteLine()

                For Each listItem As ListItem In dataBaseType.ListItems
                    If Not listItem.Selected Then Continue For

                    writer.WriteLine("-- ------------------------------------------------------------------------")
                    writer.WriteLine(Aurora.Common.EscapeSqlSingleLineComment(listItem.Text))
                    writer.WriteLine("-- ------------------------------------------------------------------------")
                    writer.WriteLine()

                    Dim objectDefintionDataTable As New DataTable("ObjectDefinition")
                    Try
                        Aurora.Common.Data.ExecuteTableSql("EXEC SP_HELPTEXT " & Aurora.Common.Utility.EscapeSqlString(listItem.Text), objectDefintionDataTable)

                        Dim first As Boolean = True
                        For Each objectDefintionDataRow As DataRow In objectDefintionDataTable.Rows
                            Dim text As String = objectDefintionDataRow(0)

                            If first AndAlso text.Trim().Length = 0 Then Continue For

                            writer.Write(text)
                            first = False
                        Next
                    Catch ex As Exception
                        writer.Write(Aurora.Common.Utility.EscapeSqlSingleLineComment(ex.Message))
                    End Try

                    writer.WriteLine()
                    writer.WriteLine("GO")
                    writer.WriteLine()
                    writer.WriteLine()
                Next
            Next

            If Not selected Then Return

            For Each dataBaseType As DataBaseType In _dataBaseTypes
                dataBaseType.DatabaseCollapsiblePanel.Collapsed = True
            Next

            generatePanel.Visible = True
            generateLiteral.Text = Server.HtmlEncode(writer.ToString())
        End Using

    End Sub
End Class

