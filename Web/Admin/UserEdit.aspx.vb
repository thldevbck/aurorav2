'   Change Log
'   23.4.10 -   Shoel   -   Fixed weird problems in update, caused by trailing spaces in ID fields

Imports System.Collections.Generic
Imports System.Data
Imports System.Web.Script.Serialization

Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.UserDataSet

Partial Class Admin_UserEdit
    Inherits AuroraPage

    Private _javascriptSerializer As New System.Web.Script.Serialization.JavaScriptSerializer()
    Private _initException As Exception

    Private _usrId As String = Nothing
    Private _userDataSet As New UserDataSet()
    Private _userInfoRow As UserInfoRow
    Private _flexExportTypeCheckBoxListByCompanyCode As New Dictionary(Of String, CheckBoxList)

    Public Class LocationItem
        Public Text As String
        Public Value As String
        Public ComCode As String
        Public CtyCode As String
    End Class

    Private _locationItemsString As String = ""
    Public ReadOnly Property LocationItemsString() As String
        Get
            Return _locationItemsString
        End Get
    End Property

    Public Class RoleItem
        Public Text As String
        Public Value As String
    End Class

    Private _roleItemsString As String = ""
    Public ReadOnly Property RoleItemsString() As String
        Get
            Return _roleItemsString
        End Get
    End Property

    Private _flexExportTypeTableIdsString As String = ""
    Public ReadOnly Property FlexExportTypeTableIdsString() As String
        Get
            Return _flexExportTypeTableIdsString
        End Get
    End Property

    Public Overrides ReadOnly Property FunctionCode() As String
        Get
            If Request("funCode") = AuroraFunctionCodeAttribute.B2BUserMaintenance Then
                Return AuroraFunctionCodeAttribute.B2BUserMaintenance
            Else
                Return AuroraFunctionCodeAttribute.UserMaintenance
            End If
        End Get
    End Property

    Public ReadOnly Property IsB2B() As Boolean
        Get
            Return FunctionCode = AuroraFunctionCodeAttribute.B2BUserMaintenance
        End Get
    End Property

    Public ReadOnly Property DisplayName() As String
        Get
            If IsB2B() Then
                Return "B2B Agent User"
            Else
                Return "User"
            End If
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If IsB2B Then
                If String.IsNullOrEmpty(_usrId) Then
                    Return "New B2B Agent User"
                Else
                    Return "Edit B2B Agent User"
                End If
            Else
                If String.IsNullOrEmpty(_usrId) Then
                    Return "New User"
                Else
                    Return "Edit User"
                End If
            End If
        End Get
    End Property

    ' B2B Change
    Protected Overloads ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.B2BUserSettingsMaintenance)
            'Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentMaintenance)
        End Get
    End Property

    Public Const GROSS_LIST_ITEM_INDEX As Integer = 0
    Public Const NETT_LIST_ITEM_INDEX As Integer = 1

    Public Property CurrencyDataSet() As DataSet
        Get
            If ViewState("CachedCurrencyDataSet") IsNot Nothing Then
                Return CType(ViewState("CachedCurrencyDataSet"), DataSet)
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As DataSet)
            ViewState("CachedCurrencyDataSet") = value
        End Set
    End Property


    ' B2B Change


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("usrId")) Then _
                _usrId = Request.QueryString("usrId").Trim()

            MyBase.OnInit(e)

            _userDataSet.EnforceConstraints = False

            DataRepository.GetUserLookups(_userDataSet)

            If Not String.IsNullOrEmpty(_usrId) Then
                Dim usrIsAgentUser As Nullable(Of Boolean)
                If IsB2B Then
                    usrIsAgentUser = New Nullable(Of Boolean)(True)
                Else
                    usrIsAgentUser = New Nullable(Of Boolean)
                End If

                DataRepository.GetUser(_userDataSet, _usrId, usrIsAgentUser)
                _userInfoRow = _userDataSet.UserInfo.FindByUsrId(_usrId)
                If _userInfoRow Is Nothing Then
                    Throw New Exception("No user found")
                End If
            End If

            companyDropDown.Items.Clear()
            companyDropDown.Items.Add("")
            For Each companyRow As CompanyRow In _userDataSet.Company
                companyDropDown.Items.Add(New ListItem(companyRow.ComCode & " - " & companyRow.ComName, companyRow.ComCode))
            Next
            companyDropDown.SelectedValue = Me.CompanyCode

            countryDropDown.Items.Clear()
            countryDropDown.Items.Add("")
            For Each countryRow As CountryRow In _userDataSet.Country
                Dim li As New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode.ToUpper())
                countryDropDown.Items.Add(li)
            Next
            countryDropDown.SelectedValue = Me.CountryCode

            Dim locationItemList As New List(Of LocationItem)
            locationDropDown.Items.Clear()
            'alt loc
            altLocationDropDown.Items.Clear()

            locationDropDown.Items.Add("")
            altLocationDropDown.Items.Add("")

            For Each locationRow As LocationRow In _userDataSet.Location
                Dim li As New ListItem(locationRow.LocCode & " - " & locationRow.LocName, locationRow.LocCode.ToUpper())
                li.Attributes.Add("ctyCode", locationRow.CtyCode)
                li.Attributes.Add("comCode", locationRow.CompanyRow.ComCode)

                Dim altli As New ListItem(locationRow.LocCode & " - " & locationRow.LocName, locationRow.LocCode.ToUpper())
                altli.Attributes.Add("ctyCode", locationRow.CtyCode)
                altli.Attributes.Add("comCode", locationRow.CompanyRow.ComCode)

                locationDropDown.Items.Add(li)
                'alt loc
                altLocationDropDown.Items.Add(altli)

                Dim locationItem As New LocationItem()
                locationItem.Text = locationRow.LocCode & " - " & locationRow.LocName
                locationItem.Value = locationRow.LocCode
                locationItem.CtyCode = locationRow.CtyCode
                locationItem.ComCode = locationRow.CompanyRow.ComCode
                locationItemList.Add(locationItem)
            Next
            locationDropDown.SelectedValue = UserSettings.Current.LocCode
            ' alt loc
            altLocationDropDown.SelectedValue = ""

            _locationItemsString = _javascriptSerializer.Serialize(locationItemList)

            menuDropDown.Items.Clear()
            menuDropDown.Items.Add(New ListItem("(None)", ""))
            For Each menuRow As MenuRow In _userDataSet.Menu
                Dim li As ListItem = New ListItem(menuRow.Text, menuRow.MenId)
                menuDropDown.Items.Add(li)
            Next

            roleCheckBoxList.Items.Clear()
            For Each roleRow As RoleRow In _userDataSet.Role
                roleCheckBoxList.Items.Add(New ListItem(roleRow.RolCode, roleRow.RolId))
            Next

            Dim flexExportTypeTableIds As New List(Of String)
            For Each companyRow As CompanyRow In _userDataSet.Company
                Dim checkBoxList As New CheckBoxList()
                checkBoxList.ID = companyRow.ComCode
                checkBoxList.Width = New Unit("100%")
                checkBoxList.RepeatLayout = RepeatLayout.Table
                checkBoxList.RepeatDirection = RepeatDirection.Vertical
                checkBoxList.RepeatColumns = 2
                For Each flexExportTypeRow As FlexExportTypeRow In companyRow.GetFlexExportTypeRows()
                    checkBoxList.Items.Add(New ListItem(flexExportTypeRow.FlxCode & " - " & flexExportTypeRow.FlxName, flexExportTypeRow.FlxId.ToString()))
                Next
                flexExportTypePanel.Controls.Add(checkBoxList)
                _flexExportTypeCheckBoxListByCompanyCode.Add(companyRow.ComCode, checkBoxList)
                flexExportTypeTableIds.Add(checkBoxList.ClientID)
            Next
            _flexExportTypeTableIdsString = _javascriptSerializer.Serialize(flexExportTypeTableIds)

            ' B2B Change
            'grdUserInfo.DataSource = _userDataSet.B2BUserConfiguration
            'grdUserInfo.DataBind()
            'grdUserInfo.Enabled = CanMaintain
            If Not IsPostBack Then
                PopulateB2BUserAgentCurrencyData("")
            End If

            If _userDataSet.B2BUserConfiguration IsNot Nothing AndAlso _userDataSet.B2BUserConfiguration.Rows.Count > 0 Then

                With _userDataSet.B2BUserConfiguration.Rows(0)
                    chkB2BCurrencyOptionEnabled.Checked = CBool(.Item("B2BHasCurrOpt"))

                    If Not (chkB2BCurrencyOptionEnabled.Checked) Then
                        tblCurrencySettings.Style("display") = "none"
                    End If

                    'chkB2BCustomerConfirmationEnabled.Checked = CBool(.Item("B2BHasCusConfirmationOpt"))
                    chkB2BGrossNettEnabled.Checked = CBool(.Item("B2BHasGrossNetOpt"))
                    Dim sGrossNettApplicability As String = ""
                    sGrossNettApplicability = CStr(.Item("B2BGrossNetWhatOpt"))
                    cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Selected = sGrossNettApplicability.Contains("G")
                    cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Selected = sGrossNettApplicability.Contains("N")

                    If CStr(.Item("B2BGrossNetDefOpt")).Contains("G") Then
                        rblB2BGrossNetDefOpt.Items(GROSS_LIST_ITEM_INDEX).Selected = True
                        rblB2BGrossNetDefOpt.Items(NETT_LIST_ITEM_INDEX).Selected = False
                    Else
                        rblB2BGrossNetDefOpt.Items(GROSS_LIST_ITEM_INDEX).Selected = False
                        rblB2BGrossNetDefOpt.Items(NETT_LIST_ITEM_INDEX).Selected = True
                    End If

                    cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Attributes.Add("onclick", "UpdateDefaults('" & cblB2BGrossNetWhatOpt.ClientID & "','" & rblB2BGrossNetDefOpt.ClientID & "')")
                    cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Attributes.Add("onclick", "UpdateDefaults('" & cblB2BGrossNetWhatOpt.ClientID & "','" & rblB2BGrossNetDefOpt.ClientID & "')")

                    If cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Selected _
                       AndAlso _
                       cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Selected _
                    Then
                        rblB2BGrossNetDefOpt.Style("display") = "block"
                        tdGNDLabel.Style("display") = "block"
                    Else
                        rblB2BGrossNetDefOpt.Style("display") = "none"
                        tdGNDLabel.Style("display") = "none"
                    End If

                    chkB2BCurrencyOptionEnabled.Enabled = CanMaintain
                    'chkB2BCustomerConfirmationEnabled.Enabled = CanMaintain
                    chkB2BGrossNettEnabled.Enabled = CanMaintain
                    If chkB2BGrossNettEnabled.Enabled Then
                        chkB2BGrossNettEnabled.Attributes.Add("onclick", "ToggleB2BElementsVisibility();")
                    End If

                    cblB2BGrossNetWhatOpt.Enabled = CanMaintain
                    rblB2BGrossNetDefOpt.Enabled = CanMaintain

                End With
            End If
            ' B2B Change

            ' sub users
            If _userDataSet.SubUsers.Rows.Count > 0 AndAlso CType(_userDataSet.UserInfo.Rows(0), UserInfoRow).UsrIsAgentSuperUser Then

                Dim sbSubUsersHTML As New StringBuilder
                ' inactive addition
                Dim sbInactiveSubUsersHTML As New StringBuilder

                sbSubUsersHTML.Append("<table width=""100%"">")
                sbInactiveSubUsersHTML.Append("<table width=""100%"">")

                Dim iTDCounter As Integer = 0
                Dim iTDInactiveCounter As Integer = 0

                For Each rwSubUser As SubUsersRow In _userDataSet.SubUsers.Rows
                    If rwSubUser.SubUsrIsActive Then
                        If iTDCounter Mod 5 = 0 Then
                            sbSubUsersHTML.Append(" <tr>")
                        End If

                        sbSubUsersHTML.Append("    <td width=""150px"">")

                        Dim sUsefulPartOfURL As String = Request.RawUrl.Split("/"c)(Request.RawUrl.Split("/"c).Length - 1)

                        sbSubUsersHTML.Append("    <a href=""" & sUsefulPartOfURL.Substring(0, sUsefulPartOfURL.LastIndexOf("&UsrId=") + Len("&UsrId=")) & rwSubUser.SubUsrId & """>" & rwSubUser.SubUsrName & "</a>")
                        sbSubUsersHTML.Append("    </td>")

                        If iTDCounter + 1 Mod 5 = 0 AndAlso iTDCounter <> 0 Then
                            sbSubUsersHTML.Append(" </tr>")
                        End If

                        iTDCounter += 1
                    Else
                        If iTDInactiveCounter Mod 5 = 0 Then
                            sbInactiveSubUsersHTML.Append(" <tr>")
                        End If

                        sbInactiveSubUsersHTML.Append("    <td width=""150px"">")

                        Dim sUsefulPartOfURL As String = Request.RawUrl.Split("/"c)(Request.RawUrl.Split("/"c).Length - 1)

                        sbInactiveSubUsersHTML.Append("    <a href=""" & sUsefulPartOfURL.Substring(0, sUsefulPartOfURL.LastIndexOf("&UsrId=") + Len("&UsrId=")) & rwSubUser.SubUsrId & """>" & rwSubUser.SubUsrName & "</a>")
                        sbInactiveSubUsersHTML.Append("    </td>")

                        If iTDInactiveCounter + 1 Mod 5 = 0 AndAlso iTDInactiveCounter <> 0 Then
                            sbInactiveSubUsersHTML.Append(" </tr>")
                        End If

                        iTDInactiveCounter += 1
                    End If
                Next

                sbSubUsersHTML.Append("</table>")
                sbInactiveSubUsersHTML.Append("</table>")

                ltrSubUsers.Text = sbSubUsersHTML.ToString()
                ltrInactiveSubUsers.Text = sbInactiveSubUsersHTML.ToString()
            Else
                tblSubUser.Visible = False
            End If
            ' sub users

            ' super users
            If _userDataSet.SuperUser.Rows.Count > 0 AndAlso Not CType(_userDataSet.UserInfo.Rows(0), UserInfoRow).UsrIsAgentSuperUser Then
                Dim sbSuperUsersHTML As New StringBuilder
                sbSuperUsersHTML.Append("<table width=""100%"">")

                Dim iTDCounter As Int16 = 0
                For Each rwSuperUser As SuperUserRow In _userDataSet.SuperUser.Rows
                    If iTDCounter Mod 5 = 0 Then
                        sbSuperUsersHTML.Append(" <tr>")
                    End If

                    sbSuperUsersHTML.Append("    <td>")

                    Dim sUsefulPartOfURL As String = Request.RawUrl.Split("/"c)(Request.RawUrl.Split("/"c).Length - 1)

                    sbSuperUsersHTML.Append("    <a href=""" & sUsefulPartOfURL.Substring(0, sUsefulPartOfURL.LastIndexOf("&UsrId=") + Len("&UsrId=")) & rwSuperUser.SuperUsrId & """>" & rwSuperUser.SuperUsrName & "</a>")
                    sbSuperUsersHTML.Append("    </td>")

                    If iTDCounter Mod 4 = 0 AndAlso iTDCounter <> 0 Then
                        sbSuperUsersHTML.Append(" </tr>")
                    End If

                    iTDCounter += 1
                Next

                sbSuperUsersHTML.Append("</table>")

                ltrSuperUser.Text = sbSuperUsersHTML.ToString()
            Else
                tblSuperUser.Visible = False
            End If
            ' super users

        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Private Sub BuildUser(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = ""
        nameTextBox.Text = ""
        companyDropDown.SelectedIndex = 0
        countryDropDown.SelectedIndex = 0
        locationDropDown.SelectedIndex = 0
        'alt loc
        altLocationDropDown.SelectedIndex = 0
        emailTextBox.Text = ""
        passwordTextBox.Text = ""
        refundLimitTextBox.Text = "0"
        vehicleRequestCountTextBox.Text = "0"
        statusRadioButtonList.SelectedIndex = 1

        menuDropDown.SelectedIndex = 0
        For Each listItem As ListItem In roleCheckBoxList.Items
            listItem.Selected = False
        Next
        userPicker.DataId = ""
        userPicker.Text = ""

        defaultAgentPicker.DataId = ""
        defaultAgentPicker.Text = ""
        agentUserRadioButtonList.SelectedIndex = 0
        addAgentPicker.DataId = ""
        addAgentPicker.Text = ""

        For Each companyRow As CompanyRow In _userDataSet.Company
            Dim checkBoxList As CheckBoxList = _flexExportTypeCheckBoxListByCompanyCode(companyRow.ComCode)
            For Each listItem As ListItem In checkBoxList.Items
                listItem.Selected = False
            Next
        Next

        If _userInfoRow Is Nothing Then
            codeTextBox.ReadOnly = False

            If IsB2B Then
                agentUserRadioButtonList.SelectedIndex = 2
            Else
                agentUserRadioButtonList.SelectedIndex = 0
            End If

            statusRadioButtonList.SelectedIndex = 0
            statusTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.ActiveColor)

            updateButton.Visible = False
            createButton.Visible = True

            tblB2BConfig.Style("display") = "none"
            defaultAgentPicker.AutoPostBack = True
        Else
            codeTextBox.Text = _userInfoRow.UsrCode
            codeTextBox.ReadOnly = True
            nameTextBox.Text = _userInfoRow.UsrName
            If _userInfoRow.CompanyRow IsNot Nothing Then companyDropDown.SelectedValue = _userInfoRow.CompanyRow.ComCode
            If Not _userInfoRow.IsUsrCtyCodeNull Then countryDropDown.SelectedValue = _userInfoRow.UsrCtyCode.ToUpper()
            If Not _userInfoRow.IsUsrLocCodeNull Then locationDropDown.SelectedValue = _userInfoRow.UsrLocCode.ToUpper()
            ' alt loc
            If Not _userInfoRow.IsUsrAltLocCodeNull Then altLocationDropDown.SelectedValue = _userInfoRow.UsrAltLocCode.ToUpper()


            emailTextBox.Text = _userInfoRow.UsrEmail
            passwordTextBox.Attributes.Add("value", _userInfoRow.UsrPsswd)
            If Not _userInfoRow.IsUsrRefundLimitNull Then refundLimitTextBox.Text = _userInfoRow.UsrRefundLimit.ToString("0.#")
            If Not _userInfoRow.IsUsrVehicleRequestCountNull Then vehicleRequestCountTextBox.Text = _userInfoRow.UsrVehicleRequestCount.ToString()

            If Not _userInfoRow.IsUsrIsActiveNull AndAlso _userInfoRow.UsrIsActive Then statusRadioButtonList.SelectedIndex = 0
            statusTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(_userInfoRow.StatusColor)

            ' Aurora ================================================
            If Not _userInfoRow.IsUsrDefaultMenIdNull Then menuDropDown.SelectedValue = _userInfoRow.UsrDefaultMenId

            For Each listItem As ListItem In roleCheckBoxList.Items
                listItem.Selected = _userDataSet.UserRole.FindByRolIdUsrId(listItem.Value, _userInfoRow.UsrId) IsNot Nothing
            Next

            ' Agents ================================================
            If _userInfoRow.AgentRow IsNot Nothing Then
                defaultAgentPicker.DataId = _userInfoRow.AgentRow.AgnId
                defaultAgentPicker.Text = _userInfoRow.AgentRow.AgnCode & " - " & _userInfoRow.AgentRow.AgnName
                tblB2BConfig.Style("display") = "block"
            Else
                tblB2BConfig.Style("display") = "none"
                defaultAgentPicker.AutoPostBack = True
            End If
            If Not _userInfoRow.IsUsrIsAgentSuperUserNull AndAlso _userInfoRow.UsrIsAgentSuperUser Then
                agentUserRadioButtonList.SelectedIndex = 2
            ElseIf Not _userInfoRow.IsUsrIsAgentUserNull AndAlso _userInfoRow.UsrIsAgentUser Then
                agentUserRadioButtonList.SelectedIndex = 1
            Else
                agentUserRadioButtonList.SelectedIndex = 0
            End If
            For Each agentUserInfoRow As AgentUserInfoRow In _userInfoRow.GetAgentUserInfoRows()
                If agentUserInfoRow.AgentRow IsNot Nothing Then
                    Dim li As New ListItem(agentUserInfoRow.AgentRow.AgnCode & " - " & agentUserInfoRow.AgentRow.AgnName, agentUserInfoRow.AgentRow.AgnId)
                    agentCheckBoxList.Items.Add(li)
                End If
            Next

            ' add links to agent maint
            AddLinksToAgentCheckBoxList()

            ' Flex Export Types =====================================
            For Each companyRow As CompanyRow In _userDataSet.Company
                Dim checkBoxList As CheckBoxList = _flexExportTypeCheckBoxListByCompanyCode(companyRow.ComCode)
                For Each listItem As ListItem In checkBoxList.Items
                    For Each flexExportContactRow As FlexExportContactRow In _userInfoRow.GetFlexExportContactRows()
                        If flexExportContactRow.FxcFlxId = listItem.Value Then
                            listItem.Selected = True
                            Exit For
                        End If
                    Next
                Next
            Next

            updateButton.Visible = True
            createButton.Visible = False
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack Then
            If statusRadioButtonList.SelectedIndex = 0 Then
                statusTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.ActiveColor)
            Else
                statusTableRow.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.InactiveColor)
            End If
            passwordTextBox.Attributes.Add("value", passwordTextBox.Text) ' hack to work around password textboxes losing their value
        Else
            ' fresh load
            If Not String.IsNullOrEmpty(Request("MsgToShow")) Then
                Me.AddInformationMessage(Request("MsgToShow"))
            End If
        End If

        ' set the attributes on the role items.  These are lost on postbacks if set in the init
        For Each roleRow As RoleRow In _userDataSet.Role
            Dim li As ListItem = roleCheckBoxList.Items.FindByValue(roleRow.RolId)
            If li IsNot Nothing Then
                li.Attributes.Add("RolDesc", Server.HtmlEncode(roleRow.RolDesc))
                li.Attributes.Add("onmouseover", "role_onmouseover(this);")
                li.Attributes.Add("onmouseout", "role_onmouseout();")
            End If
        Next

        ' security and default tab
        If IsB2B() Then
            tabs.ActiveTabIndex = 1

            agentUserRadioButtonList.Items(0).Enabled = False
            auroraTabPanel.Enabled = False
            flexExportTypeTabPanel.Enabled = False
            trRefundAndVehicle.Visible = False
            trLocation.Visible = False
            trCompanyAndCountry.Visible = False
            agentUserRadioButtonList.Enabled = False
            trAltLocation.Visible = False
        Else
            agentUserRadioButtonList.Items(0).Enabled = True
            auroraTabPanel.Enabled = True
            flexExportTypeTabPanel.Enabled = True

            If Not Me.IsPostBack AndAlso _userInfoRow IsNot Nothing Then
                If _userInfoRow.GetUserRoleRows().Length > 0 OrElse _userInfoRow.IsUsrIsAgentUserNull OrElse Not _userInfoRow.UsrIsAgentUser Then
                    tabs.ActiveTabIndex = 0
                Else
                    tabs.ActiveTabIndex = 1
                End If
            End If
        End If

        auroraTabPanel.Enabled = FunctionCode <> AuroraFunctionCodeAttribute.B2BUserMaintenance
        flexExportTypeTabPanel.Enabled = FunctionCode <> AuroraFunctionCodeAttribute.B2BUserMaintenance

        ' build the form
        If Not Page.IsPostBack AndAlso _initException Is Nothing Then BuildUser(False)
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("UserSearch.aspx?FunCode=" & Server.UrlEncode(Me.FunctionCode))
    End Sub

    Protected Sub addAgentButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addAgentButton.Click
        If Not String.IsNullOrEmpty(addAgentPicker.DataId) AndAlso Not String.IsNullOrEmpty(addAgentPicker.Text) Then

            For Each listItem As ListItem In agentCheckBoxList.Items
                If listItem.Value = addAgentPicker.DataId Then Return
            Next

            agentCheckBoxList.Items.Add(New ListItem(addAgentPicker.Text, addAgentPicker.DataId))
            AddLinksToAgentCheckBoxList()

            If String.IsNullOrEmpty(defaultAgentPicker.DataId) Then
                defaultAgentPicker.DataId = addAgentPicker.DataId
                defaultAgentPicker.Text = addAgentPicker.Text
            End If
        End If

        addAgentPicker.DataId = ""
        addAgentPicker.Text = ""
    End Sub

    Protected Sub removeAgentButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeAgentButton.Click
        Dim i As Integer = 0
        While i < agentCheckBoxList.Items.Count
            If agentCheckBoxList.Items(i).Selected Then
                If defaultAgentPicker.DataId = agentCheckBoxList.Items(i).Value Then
                    defaultAgentPicker.DataId = ""
                    defaultAgentPicker.Text = ""
                End If
                agentCheckBoxList.Items.RemoveAt(i)
            Else
                i += 1
            End If
        End While

        addAgentPicker.DataId = ""
        addAgentPicker.Text = ""
    End Sub

    Protected Sub userCopyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles userCopyButton.Click
        If IsB2B OrElse String.IsNullOrEmpty(userPicker.DataId) OrElse String.IsNullOrEmpty(userPicker.Text) Then Return

        Dim copyUserInfo As UserInfoRow = Nothing
        Try
            DataRepository.GetUser(_userDataSet, userPicker.DataId, New Nullable(Of Boolean))
            copyUserInfo = _userDataSet.UserInfo.FindByUsrId(userPicker.DataId)
            If copyUserInfo Is Nothing Then
                Throw New Exception("No user found")
            End If
        Catch ex As Exception
            Me.AddErrorMessage("Error loading user")
        End Try

        For Each listItem As ListItem In roleCheckBoxList.Items
            listItem.Selected = False
            For Each userRoleRow As UserRoleRow In copyUserInfo.GetUserRoleRows()
                If userRoleRow.RolId = listItem.Value Then
                    listItem.Selected = True
                    Exit For
                End If
            Next
        Next

        userPicker.DataId = ""
        userPicker.Text = ""
    End Sub

    Private Sub ValidateUser(ByVal create As Boolean)
        If create Then
            If codeTextBox.Text.Trim() = "" Then
                Throw New Aurora.Common.ValidationException("Code cannot be blank")
            End If
        End If

        If nameTextBox.Text.Trim() = "" Then
            Throw New Aurora.Common.ValidationException("Name cannot be blank")
        End If

        If Not IsB2B And companyDropDown.SelectedIndex = 0 Then
            Throw New Aurora.Common.ValidationException("Company must be specified")
        End If

        If Not IsB2B And countryDropDown.SelectedIndex = 0 Then
            Throw New Aurora.Common.ValidationException("Country must be specified")
        End If

        If Not IsB2B And locationDropDown.SelectedIndex = 0 Then
            Throw New Aurora.Common.ValidationException("Location must be specified")
        End If

        'if toggle loc role assigned, then alt code is a must"TOGGLELOCN"
        If altLocationDropDown.SelectedValue.Equals(String.Empty) Then
            For Each altLocationItem As ListItem In roleCheckBoxList.Items
                If altLocationItem.Selected Then
                    If altLocationItem.Text.ToUpper().Trim().Equals("TOGGLELOCN") Then
                        Throw New Aurora.Common.ValidationException("Alternate Location must be specified if the TOGGLELOCN role is assigned to this user")
                    End If
                End If
            Next
        End If

        If Not emailTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException("A valid email address must be specified")
        End If

        If Not IsB2B And Not refundLimitTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException("A valid refund limit must be specified")
        End If

        If IsB2B OrElse agentUserRadioButtonList.SelectedIndex > 0 Then
            If passwordTextBox.Text.Trim().Length < 5 OrElse passwordTextBox.Text.Trim().Length > 12 Then
                Throw New Aurora.Common.ValidationException("Password for a B2B Agent User must be between 5 and 12 characters")
            End If

            If agentCheckBoxList.Items.Count = 0 Then
                Throw New Aurora.Common.ValidationException("No agents associated with the B2B Agent User")
            End If

            If String.IsNullOrEmpty(defaultAgentPicker.DataId) Then
                Throw New Aurora.Common.ValidationException("No default agent specified for the B2B Agent User")
            End If

            If agentCheckBoxList.Items.FindByValue(defaultAgentPicker.DataId) Is Nothing Then
                agentCheckBoxList.Items.Add(New ListItem(defaultAgentPicker.Text, defaultAgentPicker.DataId))
            End If
            AddLinksToAgentCheckBoxList()

        End If

        ' unique code check
        If create Then
            Dim codeCheckUserDataSet As New UserDataSet()
            codeCheckUserDataSet.EnforceConstraints = False
            DataRepository.GetUserByCode(codeCheckUserDataSet, codeTextBox.Text.Trim())
            If codeCheckUserDataSet.UserInfo.Count > 0 Then
                Throw New Aurora.Common.ValidationException("Another user with this code already exists")
            End If
        End If

        ' unique email address check - only for B2B
        If IsB2B Then
            Dim emailCheckDataSet As New UserDataSet()
            emailCheckDataSet.EnforceConstraints = False
            DataRepository.GetUserByEmail(emailCheckDataSet, emailTextBox.Text.Trim())
            If emailCheckDataSet.UserInfo.Count > 0 AndAlso _
            (emailCheckDataSet.UserInfo(0).UsrCode.ToUpper().Trim() <> codeTextBox.Text.ToUpper().Trim() And IIf(IsDBNull(emailCheckDataSet.UserInfo(0).UsrIsAgentUser), False, emailCheckDataSet.UserInfo(0).UsrIsAgentUser)) Then
                Throw New Aurora.Common.ValidationException("Another B2B user with this email address already exists")
            End If
        End If

    End Sub

    ''' <summary>
    ''' Generic method to update the UserInfo's many-to-many records from a checkbox list
    ''' </summary>
    Private Sub UpdateChildRecords( _
     ByVal checkBoxList As CheckBoxList, _
     ByVal tableName As String, _
     ByVal usrIdFieldName As String, _
     ByVal lookupIdFieldName As String, _
     ByVal onlySelected As Boolean)

        ' handle creates or updates (ignored)
        For Each listItem As ListItem In checkBoxList.Items
            If onlySelected AndAlso Not listItem.Selected Then Continue For

            Dim dataRow As DataRow = Nothing

            For Each dataRow0 As DataRow In _userDataSet.Tables(tableName).Rows
                If UCase(Trim(dataRow0(usrIdFieldName))) = UCase(Trim(_userInfoRow.UsrId)) _
                   AndAlso _
                   UCase(Trim(dataRow0(lookupIdFieldName))) = UCase(Trim(listItem.Value)) _
                Then
                    dataRow = dataRow0
                    Exit For
                End If
            Next
            If dataRow Is Nothing Then
                dataRow = _userDataSet.Tables(tableName).NewRow()
                dataRow(usrIdFieldName) = UCase(Trim(_userInfoRow.UsrId))
                dataRow(lookupIdFieldName) = UCase(Trim(listItem.Value))
                If dataRow.Table.Columns.Contains("AddPrgmName") Then dataRow("AddPrgmName") = ""
                If dataRow.Table.Columns.Contains("AddUsrId") Then dataRow("AddUsrId") = ""
                If dataRow.Table.Columns.Contains("AddDateTime") Then dataRow("AddDateTime") = Date.Now
                If dataRow.Table.Columns.Contains("IntegrityNo") Then dataRow("IntegrityNo") = 1
                _userDataSet.Tables(tableName).Rows.Add(dataRow)

                Aurora.Common.Data.ExecuteDataRow(dataRow)
            End If
        Next

        ' handle deletes
        For Each dataRow As DataRow In _userDataSet.Tables(tableName).Rows
            Dim listItem As ListItem = Nothing
            For Each listItem0 As ListItem In checkBoxList.Items
                If onlySelected AndAlso Not listItem0.Selected Then Continue For

                If UCase(Trim(dataRow(usrIdFieldName))) = UCase(Trim(_userInfoRow.UsrId)) _
                   AndAlso _
                   UCase(Trim(dataRow(lookupIdFieldName))) = UCase(Trim(listItem0.Value)) _
                Then
                    listItem = listItem0
                    Exit For
                End If
            Next
            If listItem IsNot Nothing Then Continue For

            dataRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(dataRow)
        Next

        ' remove deleted rows so nothing else accesses it causing an exception
        Dim i As Integer
        While i < _userDataSet.Tables(tableName).Rows.Count
            Dim dataRow As DataRow = _userDataSet.Tables(tableName).Rows(i)
            If dataRow.RowState = DataRowState.Deleted Then
                _userDataSet.Tables(tableName).Rows.Remove(dataRow)
            Else
                i += 1
            End If
        End While
    End Sub


    Private Function CreateUpdateUser(ByVal create As Boolean) As Boolean
        Dim updateError As Boolean = False

        Try
            ValidateUser(create)
        Catch ex As Aurora.Common.ValidationException
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return False
        End Try

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction

            ' UserInfo
            Try
                If create Then
                    _userInfoRow = _userDataSet.UserInfo.NewUserInfoRow()
                    _userInfoRow.UsrId = Aurora.Common.Data.GetNewId()
                    _userInfoRow.UsrCode = codeTextBox.Text.Trim()
                End If

                _userInfoRow.UsrName = nameTextBox.Text.Trim()
                Dim firstName As String = ""
                Dim lastName As String = ""
                Utility.ExtractFirstLastName(_userInfoRow.UsrName, firstName, lastName)
                _userInfoRow.UsrFirstName = firstName
                _userInfoRow.UsrLastName = lastName
                If IsB2B Then
                    _userInfoRow.UsrLocCode = System.Configuration.ConfigurationManager.AppSettings("B2BLocation").ToString()
                    _userInfoRow.UsrCtyCode = System.Configuration.ConfigurationManager.AppSettings("B2BCountry").ToString()
                Else
                    _userInfoRow.UsrCtyCode = countryDropDown.SelectedValue
                    _userInfoRow.UsrLocCode = locationDropDown.SelectedValue
                    If altLocationDropDown.SelectedValue.Trim().Equals(String.Empty) Then
                        _userInfoRow.SetUsrAltLocCodeNull()
                    Else
                        _userInfoRow.UsrAltLocCode = altLocationDropDown.SelectedValue
                    End If

                End If

                _userInfoRow.UsrEmail = emailTextBox.Text
                _userInfoRow.UsrPsswd = passwordTextBox.Text
                _userInfoRow.UsrRefundLimit = Decimal.Parse(refundLimitTextBox.Text)
                _userInfoRow.UsrVehicleRequestCount = 0
                _userInfoRow.UsrIsActive = statusRadioButtonList.SelectedIndex = 0

                If Not IsB2B() AndAlso menuDropDown.SelectedIndex > 0 Then
                    _userInfoRow.UsrDefaultMenId = menuDropDown.SelectedValue
                ElseIf create Then
                    _userInfoRow.SetUsrDefaultMenIdNull()
                End If

                If Not String.IsNullOrEmpty(defaultAgentPicker.DataId) Then
                    _userInfoRow.UsrAgnDefaultId = defaultAgentPicker.DataId
                Else
                    _userInfoRow.SetUsrAgnDefaultIdNull()
                End If
                _userInfoRow.UsrIsAgentUser = agentUserRadioButtonList.SelectedIndex >= 1
                _userInfoRow.UsrIsAgentSuperUser = agentUserRadioButtonList.SelectedIndex >= 2

                If create Then
                    _userDataSet.UserInfo.AddUserInfoRow(_userInfoRow)
                End If

                Aurora.Common.Data.ExecuteDataRow(_userInfoRow, Me.UserCode, Me.PrgmName)
            Catch ex As Exception
                _userInfoRow = Nothing
                Me.AddErrorMessage("Error " & IIf(create, "creating", "updating") & " " & DisplayName)
                oTransaction.RollbackTransaction()
                Return False
            End Try

            ' UserCompany 
            Try
                If _userInfoRow.CompanyRow Is Nothing OrElse _userInfoRow.CompanyRow.ComCode <> companyDropDown.SelectedValue Then
                    For Each userCompanyRow0 As UserCompanyRow In _userInfoRow.GetUserCompanyRows()
                        userCompanyRow0.Delete()
                        Aurora.Common.Data.ExecuteDataRow(userCompanyRow0)
                    Next
                    _userDataSet.UserCompany.Clear()

                    Dim userCompanyRow As UserCompanyRow = _userDataSet.UserCompany.NewUserCompanyRow()
                    userCompanyRow.UsrId = _userInfoRow.UsrId
                    If IsB2B Then
                        userCompanyRow.ComCode = Me.CompanyCode
                    Else
                        userCompanyRow.ComCode = companyDropDown.SelectedValue
                    End If

                    _userDataSet.UserCompany.AddUserCompanyRow(userCompanyRow)
                    Aurora.Common.Data.ExecuteDataRow(userCompanyRow, Me.UserCode, Me.PrgmName)
                End If
            Catch ex As Exception
                Me.AddErrorMessage("Error updating company")
                oTransaction.RollbackTransaction()
                Return False ' end if it fails
            End Try

            ' UserRole
            If Not IsB2B() Then
                Try
                    UpdateChildRecords(roleCheckBoxList, "UserRole", "UsrId", "RolId", True)
                Catch ex As Exception
                    Me.AddErrorMessage("Error updating roles")
                    oTransaction.RollbackTransaction()
                    Return False ' end if it fails
                End Try
            End If

            ' UserAgentInfo
            Try
                UpdateChildRecords(agentCheckBoxList, "AgentUserInfo", "AuiUsrId", "AuiAgnId", False)
            Catch ex As Exception
                Me.AddErrorMessage("Error updating agents")
                oTransaction.RollbackTransaction()
                Return False ' end if it fails
            End Try

            ' FlexExportType
            If Not IsB2B() Then
                Try
                    UpdateChildRecords(_flexExportTypeCheckBoxListByCompanyCode(_userInfoRow.CompanyRow.ComCode), "FlexExportContact", "FxcUsrId", "FxcFlxId", True)
                Catch ex As Exception
                    Me.AddErrorMessage("Error updating flex export types")
                    oTransaction.RollbackTransaction()
                    Return False ' end if it fails
                End Try
            End If

            ' B2B Change
            If CanMaintain Then
                Try
                    Dim oTable As New Aurora.Agent.Data.AgentDataSet.B2BUserInfoDataTable
                    Dim sGrossNettOptions, sGrossNettDefault, sErrorMessage As String

                    If cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Selected Then
                        If cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Selected Then
                            sGrossNettOptions = "G,N"
                        Else
                            sGrossNettOptions = "G"
                        End If
                    Else
                        If cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Selected Then
                            sGrossNettOptions = "N"
                        Else
                            sGrossNettOptions = ""
                        End If
                    End If

                    If rblB2BGrossNetDefOpt.Items(GROSS_LIST_ITEM_INDEX).Selected Then
                        sGrossNettDefault = "G"
                    Else
                        sGrossNettDefault = "N"
                    End If

                    oTable.Rows.Add(_userInfoRow.UsrId, _userInfoRow.UsrCode, "", "", False, chkB2BCurrencyOptionEnabled.Checked, _
                                    chkB2BGrossNettEnabled.Checked, chkB2BGrossNettEnabled.Checked, sGrossNettOptions, sGrossNettDefault, "", "")

                    If Not Aurora.Agent.Data.DataRepository.InsertUpdateB2BConfig(oTable.Rows(0), defaultAgentPicker.DataId, UserCode, AppRelativeVirtualPath, sErrorMessage) Then
                        Throw New Exception(sErrorMessage)
                    End If

                    Dim ProgramName As String = Me.AppRelativeVirtualPath + " - " + "B2B Currency Config"
                    Aurora.Admin.Data.DataRepository.SaveB2BUserAgentCurrencyData(_usrId, cmbAgentForCurrency.SelectedValue, objDefaultAndSelectionUserControl.GetCSVSelectedList(), _
                                                                                                  objDefaultAndSelectionUserControl.GetDefault(), String.Empty, _
                                                                                                  UserCode, ProgramName)

                Catch ex As Exception
                    Me.AddErrorMessage("Error updating B2B Config Info - " & ex.Message)
                    oTransaction.RollbackTransaction()
                    Return False ' end if it fails
                End Try
            End If
            ' B2B Change

            oTransaction.CommitTransaction()

        End Using

        'Return Not updateError
        Return True
    End Function

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If CreateUpdateUser(True) Then
            Response.Redirect("UserEdit.aspx?FunCode=" & Server.UrlEncode(Me.FunctionCode) & "&UsrId=" & Server.UrlEncode(_userInfoRow.UsrId) & "&MsgToShow=" & DisplayName & " created successfully")
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If CreateUpdateUser(False) Then
            Response.Redirect("UserEdit.aspx?FunCode=" & Server.UrlEncode(Me.FunctionCode) & "&UsrId=" & Server.UrlEncode(_userInfoRow.UsrId) & "&MsgToShow=" & DisplayName & " updated successfully")
        End If
    End Sub

    Sub AddLinksToAgentCheckBoxList()
        For Each oItem As ListItem In agentCheckBoxList.Items
            Dim sUserId As String = ""
            If _userInfoRow IsNot Nothing AndAlso Not String.IsNullOrEmpty(_userInfoRow.UsrId) Then
                sUserId = _userInfoRow.UsrId
            End If
            oItem.Text = "<a href='../Agent/Maintenance.aspx?agnId=" + oItem.Value.Trim() + "&tab=B2B&BackUrl=" + Server.UrlEncode(Request.Url.LocalPath & "?FunCode=B2BAgent&UsrId=" + sUserId) + "'>" & oItem.Text & "</a>"
        Next
    End Sub

#Region "B2B User Currency stuff"

    Sub PopulateB2BUserAgentCurrencyData(ByVal AgentId As String)

        btnSaveCurrencyData.Visible = (cmbAgentForCurrency.Items.Count > 0)

        If Not String.IsNullOrEmpty(_usrId) Then
            Dim dstB2BUserAgentCurrencyData As New DataSet
            Dim bIsFreshLoad As Boolean = False

            If IsPostBack AndAlso CurrencyDataSet IsNot Nothing Then
                ' read cached copy
                dstB2BUserAgentCurrencyData = CurrencyDataSet
            Else
                ' read from db
                bIsFreshLoad = True
                dstB2BUserAgentCurrencyData = DataRepository.GetB2BUserAgentCurrencyData(_usrId)
                CurrencyDataSet = dstB2BUserAgentCurrencyData
            End If

            If dstB2BUserAgentCurrencyData Is Nothing OrElse dstB2BUserAgentCurrencyData.Tables.Count < 2 Then
                ' invalid
                'tblCurrencySettings.Visible = False
                'tblCurrencySettings.Style("display") = "none"
                cmbAgentForCurrency.Enabled = False
                Exit Sub
            Else
                If dstB2BUserAgentCurrencyData.Tables(0) Is Nothing OrElse dstB2BUserAgentCurrencyData.Tables(0).Rows.Count = 0 Then
                    ' no data
                    'tblCurrencySettings.Visible = False
                    '    tblCurrencySettings.Style("display") = "none"
                    cmbAgentForCurrency.Enabled = False
                    Exit Sub
                End If
            End If

            If _userDataSet Is Nothing OrElse _userDataSet.AgentUserInfo Is Nothing OrElse _userDataSet.AgentUserInfo.Rows.Count = 0 Then
                ' no user - agent linking yet, so cant do any thing with this
                'tblCurrencySettings.Visible = False
                tblCurrencySettings.Style("display") = "none"
                Exit Sub
            End If


            'If dstB2BUserAgentCurrencyData.Tables(0) IsNot Nothing Then
            '    ' dont show agents with no currency data
            '    Dim dvFilter As New DataView(dstB2BUserAgentCurrencyData.Tables(0))
            '    dvFilter.RowFilter = "AuiB2BCurrencyCodeList <> '' AND AuiB2BDefaultCurrencyCode <> ''"
            '    dvFilter.Sort = "AgnCodeName"

            '    cmbAgentForCurrency.DataSource = dvFilter.ToTable()
            'Else
            '    cmbAgentForCurrency.DataSource = dstB2BUserAgentCurrencyData.Tables(0)
            'End If

            'cmbAgentForCurrency.DataTextField = "AgnCodeName"
            'cmbAgentForCurrency.DataValueField = "AgnId"
            'cmbAgentForCurrency.DataBind()

            cmbAgentForCurrency.DataSource = dstB2BUserAgentCurrencyData.Tables(0)
            cmbAgentForCurrency.DataTextField = "AgnCodeName"
            cmbAgentForCurrency.DataValueField = "AgnId"
            cmbAgentForCurrency.DataBind()
            btnSaveCurrencyData.Visible = (cmbAgentForCurrency.Items.Count > 0)

            If String.IsNullOrEmpty(AgentId) Then
                cmbAgentForCurrency.Items(0).Selected = True
            Else
                cmbAgentForCurrency.SelectedValue = AgentId
            End If

            PopulateCurrencyDefaultAndSelectionControl(dstB2BUserAgentCurrencyData)

        Else
            'tblCurrencySettings.Visible = False
            tblCurrencySettings.Style("display") = "none"
            Exit Sub
        End If
    End Sub

    Sub PopulateCurrencyDefaultAndSelectionControl(ByVal AgentB2BCurrencyData As DataSet)
        For Each rwAgentB2BCurrData As DataRow In AgentB2BCurrencyData.Tables(0).Rows
            If rwAgentB2BCurrData("AgnId").ToString() = cmbAgentForCurrency.SelectedValue Then

                ' this is the agent
                ' using the new control
                Dim dtSelectionData As New DataTable
                dtSelectionData.Columns.Add("ItemCode")
                dtSelectionData.Columns.Add("ItemName")
                dtSelectionData.Columns.Add("ItemDesc")
                dtSelectionData.Columns.Add("Default")
                dtSelectionData.Columns.Add("Selected")

                ' populate dtSelectionData with contents of available currencies
                UpdateCurrencyTable(rwAgentB2BCurrData("CurrenciesAvailableForSelection").ToString(), AgentB2BCurrencyData.Tables(1), False, False, dtSelectionData)


                Dim sCurrentCurrencies, sDefaultCurrenct As String
                sDefaultCurrenct = rwAgentB2BCurrData("AuiB2BDefaultCurrencyCode").ToString()
                sCurrentCurrencies = rwAgentB2BCurrData("AuiB2BCurrencyCodeList").ToString()
                sCurrentCurrencies = sCurrentCurrencies.Replace(sDefaultCurrenct & ",", "")

                If sCurrentCurrencies.Length > 0 AndAlso Trim(sDefaultCurrenct) <> String.Empty Then
                    sCurrentCurrencies = sCurrentCurrencies.Replace(sDefaultCurrenct, "")
                End If

                ' populate dtSelectionData with contents of currently selected currencies
                UpdateCurrencyTable(sCurrentCurrencies, AgentB2BCurrencyData.Tables(1), True, False, dtSelectionData)

                ' populate dtSelectionData with contents of default currency
                UpdateCurrencyTable(sDefaultCurrenct, AgentB2BCurrencyData.Tables(1), True, True, dtSelectionData)

                If dtSelectionData IsNot Nothing AndAlso dtSelectionData.Rows.Count > 0 Then
                    Dim dvSort As New DataView(dtSelectionData)
                    dvSort.Sort = "ItemDesc"
                    objDefaultAndSelectionUserControl.DataSource = dvSort.ToTable()
                Else
                    objDefaultAndSelectionUserControl.DataSource = dtSelectionData
                End If

                objDefaultAndSelectionUserControl.DataBind()
                ' using the new control

            End If
        Next
    End Sub

    Sub PopulateCurrencyDropDown(ByRef TargetDropDown As ListControl, ByVal CSVList As String, ByVal LookupTable As DataTable, ByVal AddDummyOptionOnTop As Boolean)
        If String.IsNullOrEmpty(CSVList) OrElse String.IsNullOrEmpty(CSVList.Trim()) Then
            If AddDummyOptionOnTop Then
                TargetDropDown.Items.Insert(0, "--Select--")
            End If
            Exit Sub
        End If

        Dim saOptions As String() = CSVList.Split(","c)
        For i As Int16 = 0 To saOptions.Length - 1
            saOptions(i) = "'" & saOptions(i) & "'"
        Next
        CSVList = Join(saOptions, ",")

        Dim dvFilter As New DataView(LookupTable, "CurrencyCode IN (" & CSVList & ")", "", DataViewRowState.CurrentRows)

        With TargetDropDown
            .DataSource = dvFilter.ToTable()
            .DataTextField = "CurrencyCodeName"
            .DataValueField = "CurrencyCode"
            .DataBind()
            If AddDummyOptionOnTop Then
                .Items.Insert(0, "--Select--")
            End If
        End With
    End Sub

    Sub UpdateCurrencyTable(ByVal CSVList As String, ByVal LookupTable As DataTable, ByVal MarkRowsAsSelected As Boolean, ByVal MarkRowAsDefault As Boolean, ByRef CurrencyTable As DataTable)
        If String.IsNullOrEmpty(CSVList) OrElse String.IsNullOrEmpty(CSVList.Trim()) Then
            Exit Sub
        End If

        Dim saOptions As String() = CSVList.Split(","c)
        For i As Int16 = 0 To saOptions.Length - 1
            saOptions(i) = "'" & saOptions(i) & "'"
        Next
        CSVList = Join(saOptions, ",")

        Dim dvFilter As New DataView(LookupTable, "CurrencyCode IN (" & CSVList & ")", "", DataViewRowState.CurrentRows)

        For Each oRow As DataRow In dvFilter.ToTable().Rows
            CurrencyTable.Rows.Add(oRow("CurrencyCode"), oRow("CurrencyName"), oRow("CurrencyCodeName"), MarkRowAsDefault, MarkRowsAsSelected)
        Next



    End Sub

    Protected Sub cmbAgentForCurrency_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateB2BUserAgentCurrencyData(cmbAgentForCurrency.SelectedValue)
    End Sub

    Sub defaultAgentPicker_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles defaultAgentPicker.TextChanged
        If defaultAgentPicker.DataId <> "" Then
            If defaultAgentPicker.IsValid Then
                tblB2BConfig.Style("display") = "block"
            Else
                tblB2BConfig.Style("display") = "none"
            End If
        Else
            tblB2BConfig.Style("display") = "none"
        End If
    End Sub

    Protected Sub btnSaveCurrencyData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCurrencyData.Click
        ClearMessages()
        ClearShortMessage()

        If Not objDefaultAndSelectionUserControl.HasChanged Then
            SetWarningShortMessage("No B2B currency config changes made")
            Exit Sub
        End If

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                Dim ProgramName As String = Me.AppRelativeVirtualPath + " - " + "B2B Currency Config"

                Aurora.Admin.Data.DataRepository.SaveB2BUserAgentCurrencyData(_usrId, cmbAgentForCurrency.SelectedValue, objDefaultAndSelectionUserControl.GetCSVSelectedList(), _
                                                                              objDefaultAndSelectionUserControl.GetDefault(), String.Empty, _
                                                                              UserCode, ProgramName)

                oTransaction.CommitTransaction()
                SetInformationMessage("B2B currency config updated")

                PopulateCurrencyDefaultAndSelectionControl(DataRepository.GetB2BUserAgentCurrencyData(_usrId))

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                SetErrorMessage("Unable to update B2B currency config")
            End Try
        End Using
    End Sub

#End Region




End Class
