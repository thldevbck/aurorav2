'' Change Log 
'' 29.3.10  -   Shoel   - Initial version ready
'' 6.4.10   -   Shoel   - Now also reads disk status and server name data

Imports System.Data
Imports System.Text.RegularExpressions


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DatabaseArchiving)> _
Partial Class Admin_Archiving
    Inherits AuroraPage

    Property StatusTable() As DataTable
        Get
            If ViewState("Arch_StatusTable") Is Nothing Then
                Return Nothing
            Else
                Return CType(ViewState("Arch_StatusTable"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("Arch_StatusTable") = value
        End Set
    End Property

    Property Frequency() As DataTable
        Get
            If ViewState("Arch_Frequency") Is Nothing Then
                Return Nothing
            Else
                Return CType(ViewState("Arch_Frequency"), DataTable)
            End If
        End Get
        Set(ByVal value As DataTable)
            ViewState("Arch_Frequency") = value
        End Set
    End Property

    Public Property ServerName() As String
        Get
            Return CStr(ViewState("Arch_ServerName"))
        End Get
        Set(ByVal value As String)
            ViewState("Arch_ServerName") = value
        End Set
    End Property

    Public ReadOnly Property MinimumDaysAhead() As Integer
        Get
            Return CInt(Aurora.Common.Data.ExecuteScalarSQL("select dbo.getUviValue('BOOARCHMINDAYSAHEAD')"))
        End Get
    End Property


    Public Const FROM_EMAIL_ADDRESS As String = "ArchivingScheduler@thlonline.com"
    Public Const EMAIL_SUBJECT As String = "Booking archiving scheduled for {0} has been {2} by {1}"
    Public Const EMAIL_MESSAGE_BODY As String = "{1} has {2} the booking archiving process, which is scheduled to run on {0}."


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            InitPageData()
        End If
    End Sub


    Sub InitPageData()
        Dim dstData As New DataSet
        dstData = Aurora.Admin.Services.Archiving.GetScheduleList()

        Frequency = dstData.Tables(1)

        StatusTable = dstData.Tables(2)

        grdSchedules.DataSource = dstData.Tables(0)
        grdSchedules.DataBind()

        cmbFrequency.DataSource = dstData.Tables(1)
        cmbFrequency.DataTextField = "ARunFrqDesc"
        cmbFrequency.DataValueField = "ARunFrqId"
        cmbFrequency.DataBind()
        cmbFrequency.Items.Insert(0, "")

        cmbFinancialYear.DataSource = dstData.Tables(3)
        cmbFinancialYear.DataTextField = "TextField"
        cmbFinancialYear.DataValueField = "FinYrEnd"
        cmbFinancialYear.DataBind()


        grdDBStatus.DataSource = dstData.Tables(4)
        grdDBStatus.DataBind()

        ' stop running schedule button enable/disable
        'Try
        '    btnStop.Enabled = False
        '    btnUpdate.Enabled = False
        '    For Each oRow As GridViewRow In grdSchedules.Rows
        '        If Not btnUpdate.Enabled Then
        '            btnUpdate.Enabled = True ' enable it if theres atleast 1 row
        '        End If
        '        Dim cmbStatus As DropDownList = CType(oRow.FindControl("cmbStatus"), DropDownList)
        '        btnStop.Enabled = cmbStatus.SelectedItem.Text.Contains("In Progress")
        '        Exit For
        '    Next
        'Catch ex As Exception
        '    btnStop.Enabled = True
        '    btnUpdate.Enabled = True
        'End Try
        ServerName = dstData.Tables(5).Rows(0)("ServerName").ToString()
    End Sub

    Protected Sub grdSchedules_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdSchedules.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim hdnRunAt, hdnStatus, hdnRunForHrs As HiddenField
            Dim cmbRunEvery, cmbStatus, cmbRunForHrs As DropDownList


            Dim txtNotificationList As TextBox
            Dim dtcJobStartDate As UserControls_DateControl
            Dim tmcJobStartTime As UserControls_TimeControl
            Dim cmbRunFrequency As DropDownList

            txtNotificationList = CType(e.Row.FindControl("txtNotificationList"), TextBox)
            dtcJobStartDate = CType(e.Row.FindControl("dtcJobStartDate"), UserControls_DateControl)
            tmcJobStartTime = CType(e.Row.FindControl("tmcJobStartTime"), UserControls_TimeControl)
            cmbRunFrequency = CType(e.Row.FindControl("cmbRunEvery"), DropDownList)

            hdnRunAt = CType(e.Row.FindControl("hdnRunAt"), HiddenField)
            hdnStatus = CType(e.Row.FindControl("hdnStatus"), HiddenField)

            hdnRunForHrs = CType(e.Row.FindControl("hdnRunForHrs"), HiddenField)

            cmbRunEvery = CType(e.Row.FindControl("cmbRunEvery"), DropDownList)
            cmbStatus = CType(e.Row.FindControl("cmbStatus"), DropDownList)

            cmbRunForHrs = CType(e.Row.FindControl("cmbRunForHrs"), DropDownList)

            cmbRunEvery.DataSource = Frequency
            cmbRunEvery.DataTextField = "ARunFrqDesc"
            cmbRunEvery.DataValueField = "ARunFrqId"
            cmbRunEvery.DataBind()
            'cmbRunEvery.Items.Insert(0, "")
            cmbRunEvery.SelectedValue = hdnRunAt.Value

            cmbStatus.DataSource = StatusTable
            cmbStatus.DataTextField = "ASchStatusDescription"
            cmbStatus.DataValueField = "ASchStatusID"
            cmbStatus.DataBind()
            cmbStatus.SelectedValue = hdnStatus.Value
            cmbRunEvery.Enabled = False

            For Each oItem As ListItem In cmbRunTime.Items
                If Not oItem.Text = "" Then
                    cmbRunForHrs.Items.Add(New ListItem(oItem.Text, oItem.Value))
                End If
            Next
            cmbRunForHrs.SelectedValue = hdnRunForHrs.Value


            If cmbStatus.SelectedItem.Text = "Pending" Then
                txtNotificationList.ReadOnly = False
                dtcJobStartDate.ReadOnly = False
                tmcJobStartTime.ReadOnly = False
                cmbRunFrequency.Enabled = True
                cmbRunForHrs.Enabled = True
            Else
                txtNotificationList.ReadOnly = True
                dtcJobStartDate.ReadOnly = True
                tmcJobStartTime.ReadOnly = True
                cmbRunFrequency.Enabled = False
                cmbRunForHrs.Enabled = False
            End If



            If cmbStatus.SelectedItem.Text = "Completed" OrElse cmbStatus.SelectedItem.Text = "Cancelled" OrElse cmbStatus.SelectedItem.Text.Contains("In Progress") Then
                ' make readonly
                cmbStatus.Enabled = False

            Else
                ' take off the unavailable options
                ' list of valid paths
                ' initially - Pending (automatic)

                ' Pending -> Approved (via page)
                '         -> Cancelled (via page)

                ' Approved -> In Progress (the sql job does this)
                '          -> Cancelled (via page)

                ' In Progress -> Paused  (via page using the "Stop Running Schedule" button)

                ' Paused -> Approved (via page)

                For i As Integer = cmbStatus.Items.Count - 1 To 0 Step -1
                    If cmbStatus.SelectedItem.Text = "Pending" Then
                        If "In Progress,Completed,Paused,In Progress - Paused".Contains(cmbStatus.Items(i).Text) Then
                            cmbStatus.Items.RemoveAt(i)
                        End If
                    ElseIf cmbStatus.SelectedItem.Text = "Approved" Then
                        If "Pending,In Progress,Completed,Paused,In Progress - Paused".Contains(cmbStatus.Items(i).Text) Then
                            cmbStatus.Items.RemoveAt(i)
                        End If
                    ElseIf cmbStatus.SelectedItem.Text = "Paused" Then
                        If "Pending,In Progress,Completed,Cancelled,In Progress - Paused".Contains(cmbStatus.Items(i).Text) AndAlso cmbStatus.Items(i).Text <> "Paused" Then
                            cmbStatus.Items.RemoveAt(i)
                        End If
                    End If
                Next
            End If

            ' run time calc
            Dim lblRuntime As Label = CType(e.Row.FindControl("lblRunTime"), Label)
            Dim nTimeInSeconds As Decimal
            Try
                nTimeInSeconds = CDec(lblRuntime.Text)
            Catch ex As Exception
            End Try

            Dim tsTime As New TimeSpan(0, 0, nTimeInSeconds)
            'If tsTime.Days > 0 Then
            '    lblRuntime.Text = tsTime.Days.ToString() & " Day(s), " & tsTime.Hours & " Hr(s) and " & tsTime.Minutes & " Min(s)"
            'Else
            '    If tsTime.Hours > 0 Then
            '        lblRuntime.Text = tsTime.Hours & " Hr(s) and " & tsTime.Minutes & " Min(s)"
            '    Else
            '        lblRuntime.Text = tsTime.Minutes & " Min(s) and " & tsTime.Seconds & " Sec(s)"
            '    End If
            'End If

            lblRuntime.Text = IIf(tsTime.Days > 1, _
                                    tsTime.Days.ToString() & " Days " & tsTime.Hours.ToString().PadLeft(2, "0"c) & ":" & tsTime.Minutes.ToString().PadLeft(2, "0"c) & ":" & tsTime.Seconds.ToString().PadLeft(2, "0"c), _
                                    IIf(tsTime.Days > 0, _
                                        tsTime.Days.ToString() & " Day " & tsTime.Hours.ToString().PadLeft(2, "0"c) & ":" & tsTime.Minutes.ToString().PadLeft(2, "0"c) & ":" & tsTime.Seconds.ToString().PadLeft(2, "0"c), _
                                        tsTime.Hours.ToString().PadLeft(2, "0"c) & ":" & tsTime.Minutes.ToString().PadLeft(2, "0"c) & ":" & tsTime.Seconds.ToString().PadLeft(2, "0"c)))

            'tsTime.Hours.ToString() & ":" & tsTime.Minutes.ToString() & ":" & tsTime.Seconds.ToString())

        End If
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        ' Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
        Try
            Dim bChangesMade As Boolean = False

            For Each rwSchedule As GridViewRow In grdSchedules.Rows
                If CType(rwSchedule.FindControl("cmbStatus"), DropDownList).Enabled Then
                    ' try to save 
                    Dim hdnScheduleId, hdnStatus As HiddenField
                    Dim cmbStatus As DropDownList

                    Dim hdnStartDateTime, hdnNotificationList, hdnRunForHrs, hdnRunFreq As HiddenField
                    Dim txtNotificationList As TextBox
                    Dim dtcJobStartDate As UserControls_DateControl
                    Dim tmcJobStartTime As UserControls_TimeControl
                    Dim cmbRunForHrs, cmbRunFrequency As DropDownList

                    hdnScheduleId = CType(rwSchedule.FindControl("hdnScheduleId"), HiddenField)
                    cmbStatus = CType(rwSchedule.FindControl("cmbStatus"), DropDownList)
                    hdnStatus = CType(rwSchedule.FindControl("hdnStatus"), HiddenField)

                    hdnStartDateTime = CType(rwSchedule.FindControl("hdnStartDateTime"), HiddenField)
                    hdnNotificationList = CType(rwSchedule.FindControl("hdnNotificationList"), HiddenField)
                    hdnRunForHrs = CType(rwSchedule.FindControl("hdnRunForHrs"), HiddenField)
                    hdnRunFreq = CType(rwSchedule.FindControl("hdnRunAt"), HiddenField)

                    txtNotificationList = CType(rwSchedule.FindControl("txtNotificationList"), TextBox)
                    dtcJobStartDate = CType(rwSchedule.FindControl("dtcJobStartDate"), UserControls_DateControl)
                    tmcJobStartTime = CType(rwSchedule.FindControl("tmcJobStartTime"), UserControls_TimeControl)
                    cmbRunForHrs = CType(rwSchedule.FindControl("cmbRunForHrs"), DropDownList)
                    cmbRunFrequency = CType(rwSchedule.FindControl("cmbRunEvery"), DropDownList)

                    '' date time validation
                    If Not dtcJobStartDate.IsValid OrElse dtcJobStartDate.Text = "" Then
                        Throw New Exception("The job start date must be specified")
                    End If

                    If Not tmcJobStartTime.IsValid OrElse tmcJobStartTime.Text = "" Then
                        Throw New Exception("The job start time must be specified")
                    End If

                    If CDate(hdnStartDateTime.Value) <> dtcJobStartDate.Date.Add(tmcJobStartTime.Time) Then
                        If dtcJobStartDate.Date.Add(tmcJobStartTime.Time) < Now.Date.AddDays(MinimumDaysAhead) Then
                            ' must be a week away from now atleast
                            Throw New Exception("The job start date must be atleast " & MinimumDaysAhead.ToString() & " days ahead of today")
                        End If
                    End If
                    '' date time validation

                    Dim bSendEmail As Boolean = False

                    If hdnStatus.Value <> cmbStatus.SelectedValue _
                       OrElse _
                       hdnNotificationList.Value <> txtNotificationList.Text.Trim() _
                       OrElse _
                       CDate(hdnStartDateTime.Value) <> dtcJobStartDate.Date.Add(tmcJobStartTime.Time) _
                       OrElse _
                       CInt(cmbRunForHrs.SelectedValue) <> CInt(hdnRunForHrs.Value) _
                       OrElse _
                       CInt(hdnRunFreq.Value) <> CInt(cmbRunFrequency.SelectedValue) _
                    Then
                        ' there is a change
                        Dim iScheduleId As Int64 = CType(hdnScheduleId.Value, Int64)
                        Dim iStatusId As Integer = CType(cmbStatus.SelectedValue, Integer)

                        If cmbStatus.SelectedItem.Text = "Approved" Then
                            ' run validation - this can be moved/copied to other events as well
                            Dim dtErrors As New DataTable
                            If Not Aurora.Admin.Services.Archiving.ValidateTableStructures(dtErrors) Then

                                Throw New Exception(GetTableValidationErrors(dtErrors))
                            End If

                            If txtNotificationList.Text.Trim() = "" Then
                                Throw New Exception("Notification list may not be empty")
                            Else
                                ' validate the email id(s)
                                For Each sEmailId As String In txtNotificationList.Text.Trim().Split(";"c)
                                    If Not EmailAddressCheck(sEmailId) AndAlso Not String.IsNullOrEmpty(sEmailId) Then
                                        Throw New Exception(sEmailId & " is not a valid email id")
                                    End If
                                Next
                            End If

                            If hdnStatus.Value = "1" Then
                                ' from pending
                                ' send email
                                'Dim sErrMessage As String = ""
                                'If Not Aurora.Admin.Services.Archiving.SendEmail(FROM_EMAIL_ADDRESS, txtNotificationList.Text.Trim(), EMAIL_SUBJECT.Replace("{0}", dtcJobStartDate.Text & " " & tmcJobStartTime.Text).Replace("{1}", UserName).Replace("{2}", "approved"), EMAIL_MESSAGE_BODY.Replace("{0}", dtcJobStartDate.Text & " " & tmcJobStartTime.Text).Replace("{1}", UserName).Replace("{2}", "approved"), sErrMessage) Then
                                'Throw New Exception(sErrMessage)
                                'End If
                                bSendEmail = True
                            End If

                        End If

                        Dim sErrorMessage As String = ""

                        Aurora.Admin.Services.Archiving.UpdateSchedule(iScheduleId, _
                                                                       dtcJobStartDate.Date.Add(tmcJobStartTime.Time), _
                                                                       CInt(cmbRunFrequency.SelectedValue), _
                                                                       CInt(cmbRunForHrs.SelectedValue), _
                                                                       txtNotificationList.Text.Trim(), _
                                                                       iStatusId, _
                                                                       "", _
                                                                       DateTime.MinValue, _
                                                                       UserCode, _
                                                                       Page.AppRelativeVirtualPath, _
                                                                       sErrorMessage)
                        If Not String.IsNullOrEmpty(sErrorMessage) Then
                            Throw New Exception(sErrorMessage)
                        End If

                        If bSendEmail Then
                            Dim sErrMessage As String = ""
                            If Not Aurora.Admin.Services.Archiving.SendEmail(FROM_EMAIL_ADDRESS, txtNotificationList.Text.Trim(), EMAIL_SUBJECT.Replace("{0}", dtcJobStartDate.Text & " " & tmcJobStartTime.Text).Replace("{1}", UserName).Replace("{2}", "approved"), EMAIL_MESSAGE_BODY.Replace("{0}", dtcJobStartDate.Text & " " & tmcJobStartTime.Text).Replace("{1}", UserName).Replace("{2}", "approved"), sErrMessage) Then
                                Throw New Exception(sErrMessage)
                            End If
                        End If

                        bChangesMade = True
                        'Else
                        ' there is no change
                        'SetWarningShortMessage("There is no change to update")
                        'Exit Sub
                    End If


                End If
            Next

            If Not bChangesMade Then
                SetWarningShortMessage("There is no change to update")
                Exit Sub
            End If

            '  oTransaction.CommitTransaction()
            ResetForm()
            SetInformationMessage("Updated successfully")
        Catch ex As Exception
            ' oTransaction.RollbackTransaction()
            SetErrorMessage(ex.Message)
        End Try
        'End Using


    End Sub

    Protected Sub btnCreateSchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateSchedule.Click
        ' validations
        If Not dtcJobStartDate.IsValid _
           OrElse _
           dtcJobStartDate.Text.Trim() = "" _
           OrElse _
           Not tmcJobStartTime.IsValid _
           OrElse tmcJobStartTime.Text.Trim() = "" _
        Then
            SetWarningShortMessage("Please enter a valid start date and start time")
            Exit Sub
        End If

        If txtEmailIdsToNotify.Text.Trim() = "" Then
            SetWarningShortMessage("Please enter a list of semicolon delimited email ids to be notified")
            Exit Sub
        Else
            ' validate the email id(s)
            For Each sEmailId As String In txtEmailIdsToNotify.Text.Trim().Split(";"c)
                If Not EmailAddressCheck(sEmailId) AndAlso Not String.IsNullOrEmpty(sEmailId) Then
                    SetWarningMessage(sEmailId & " is not a valid email id")
                    Exit Sub
                End If
            Next
        End If

        If dtcJobStartDate.Date.Add(tmcJobStartTime.Time) < Now.Date.AddDays(MinimumDaysAhead) Then
            ' must be a week away from now atleast
            SetWarningMessage("The job start date must be atleast " & MinimumDaysAhead.ToString() & " days ahead of today")
            Exit Sub
        End If

        ' Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
        Try
            Dim iRuntimeID, iFrequency As Integer
            If cmbRunTime.SelectedValue = "" Then
                iRuntimeID = -1
            Else
                iRuntimeID = CInt(cmbRunTime.SelectedValue)
            End If
            If cmbFrequency.SelectedValue = "" Then
                iFrequency = -1
            Else
                iFrequency = CInt(cmbFrequency.SelectedValue)
            End If
            Dim sErrorMessage As String = ""
            Aurora.Admin.Services.Archiving.UpdateSchedule(-1, _
                                                           dtcJobStartDate.Date.Add(tmcJobStartTime.Time), _
                                                           iFrequency, _
                                                           iRuntimeID, _
                                                           txtEmailIdsToNotify.Text, _
                                                           1, _
                                                           cmbFinancialYear.SelectedItem.Text, _
                                                           CDate(cmbFinancialYear.SelectedValue).AddDays(1), _
                                                           UserCode, _
                                                           Page.AppRelativeVirtualPath, _
                                                           sErrorMessage)

            If Not String.IsNullOrEmpty(sErrorMessage) Then
                Throw New Exception(sErrorMessage)
            End If

            ' oTransaction.CommitTransaction()
            ' send email
            Dim sErrMessage As String = ""
            If Not Aurora.Admin.Services.Archiving.SendEmail(FROM_EMAIL_ADDRESS, txtEmailIdsToNotify.Text.Trim(), EMAIL_SUBJECT.Replace("{0}", dtcJobStartDate.Text & " " & tmcJobStartTime.Text).Replace("{1}", UserName).Replace("{2}", "created"), EMAIL_MESSAGE_BODY.Replace("{0}", dtcJobStartDate.Text & " " & tmcJobStartTime.Text).Replace("{1}", UserName).Replace("{2}", "created"), sErrMessage) Then
                SetWarningMessage("Created successfully, but failed to send email")
            Else
                SetInformationMessage("Created successfully")
            End If
            ResetForm()



        Catch ex As Exception
            ' oTransaction.RollbackTransaction()
            SetErrorMessage(ex.Message)
        End Try
        ' End Using
    End Sub

    Sub ResetForm()
        cmbFinancialYear.ClearSelection()
        cmbFrequency.ClearSelection()
        cmbRunTime.ClearSelection()
        txtEmailIdsToNotify.Text = ""
        dtcJobStartDate.Text = ""
        tmcJobStartTime.Text = ""

        grdSchedules.DataSource = Aurora.Admin.Services.Archiving.GetScheduleList().Tables(0)
        grdSchedules.DataBind()
    End Sub

    Protected Sub btnStop_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnStop.Click
        Dim sErrMsg As String = ""
        If Aurora.Admin.Services.Archiving.StopRunningSchedule(UserCode, sErrMsg) Then
            InitPageData()
            SetInformationMessage("Running schedule has been stopped")
            ' this only sets the ForceStop flag for any schedules in status 3 = "In Progress"
        Else
            SetErrorMessage("Error occured while stopping the running schedule. " & sErrMsg)
        End If
    End Sub

    Function GetTableValidationErrors(ByVal ErrorsTable As DataTable) As String
        Dim sbErrors As New StringBuilder
        For Each oRow As DataRow In ErrorsTable.Rows
            sbErrors.Append(oRow("Messages").ToString().Replace("[BR]", "<br/>"))
            sbErrors.Append("<br/><br/>")
        Next
        Return sbErrors.ToString()
    End Function


    Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
        If emailAddressMatch.Success Then
            EmailAddressCheck = True
        Else
            EmailAddressCheck = False
        End If
    End Function

End Class
