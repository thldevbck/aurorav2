<%@ Page Language="VB" MasterPageFile="~/Include/PopupHeader.master" AutoEventWireup="false"
    CodeFile="ArchivingRateScheduleDetails.aspx.vb" Inherits="Admin_ArchivingRateScheduleDetails"
    Title="Archiving Rate Schedule Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="900px">
        <tr>
            <td>
                <asp:Button runat="server" Text="Close" OnClientClick="window.close();" CssClass="Button_Standard Button_Close" ID="btnClose" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView Width="100%" ID="grdArchivingScheduleDetails" runat="server" CssClass="dataTableGrid"
                    AutoGenerateColumns="false" PageSize="100" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="ProductName" HeaderText="Product Name" />
                        <asp:BoundField DataField="TbsId" HeaderText="Tbs Id" />
                        <asp:BoundField DataField="SapId" HeaderText="Sap Id" />
                        <asp:BoundField DataField="EffectiveDate" HeaderText="Effective Date" />
                        <asp:CheckBoxField DataField="Archived" HeaderText="Archived ?" />
                    </Columns>
                    <AlternatingRowStyle CssClass="oddRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" Text="Close" OnClientClick="window.close();" CssClass="Button_Standard Button_Close" ID="btnClose2" />
            </td>
        </tr>
    </table>
</asp:Content>
