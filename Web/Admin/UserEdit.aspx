<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserEdit.aspx.vb" Inherits="Admin_UserEdit"
    MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="../UserControls/DefaultAndSelectionControl/DefaultAndSelectionUserControl.ascx"
    TagName="DefaultAndSelectionUserControl" TagPrefix="uc2" %>
<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox"
    TagPrefix="uc1" %>
<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
    .ajax__tab_panel
    {
        min-height:350px; 
        height:auto !important; 
        height:350px;
        padding-top: 10px;
    }
</style>
</asp:Content>
<asp:Content runat="Server" ContentPlaceHolderID="ScriptPlaceHolder">

    <script type="text/javascript">
var locationItems = <%= Me.LocationItemsString %>;
var flexExportTypeTableIds = <%= Me.FlexExportTypeTableIdsString %>;

var companyDropDownId = '<%= companyDropDown.ClientId %>';
var companyDropDown;
var countryDropDownId = '<%= countryDropDown.ClientId %>';
var countryDropDown;
var locationDropDownId = '<%= locationDropDown.ClientId %>';
var locationDropDown;
//Alt Locn
var altLocationDropDownId = '<%= altLocationDropDown.ClientId %>';
var altLocationDropDown;
//Alt Locn
var roleCheckBoxListId = '<%= roleCheckBoxList.ClientId %>';
var roleCheckBoxList;
var flexExportTypePanelId = '<%= flexExportTypePanel.ClientId %>';
var flexExportTypePanel;
var roleDescriptionTableCell;

function locationFilter()
{
    var oldLocCode = locationDropDown.value;
    //alt loc
    var altOldLocCode = altLocationDropDown.value;
    
    clearDropDown (locationDropDown);
    addDropDownOption (locationDropDown, '', '');
    for (var i = 0; i < locationItems.length; i++)
    {
        var locationItem = locationItems[i];
        
        if (((companyDropDown.value == '') || (companyDropDown.value == locationItem.ComCode))
         && ((countryDropDown.value == '') || (countryDropDown.value == locationItem.CtyCode)))
            addDropDownOption (locationDropDown, locationItem.Text, locationItem.Value);
    }
    
    locationDropDown.value = oldLocCode;
    
    //alt loc
    clearDropDown (altLocationDropDown);
    addDropDownOption (altLocationDropDown, '', '');
    for (var i = 0; i < locationItems.length; i++)
    {
        var locationItem = locationItems[i];
        
        if (((companyDropDown.value == '') || (companyDropDown.value != locationItem.ComCode))
         && ((countryDropDown.value == '') || (countryDropDown.value == locationItem.CtyCode)))
            addDropDownOption (altLocationDropDown, locationItem.Text, locationItem.Value);
    }
    altLocationDropDown.value = altOldLocCode;
}

function flexExportTypeFilter()
{
    for (var i = 0; i < flexExportTypeTableIds.length; i++)
    {
        var flexExportTypeTable = document.getElementById (flexExportTypeTableIds[i]);
        if (flexExportTypeTable) // no table is rendered if its empty
            flexExportTypeTable.style.display = ((i == (companyDropDown.selectedIndex - 1)) ? "" : "none");
    }
}

function role_onmouseover(sender)
{
    try
    {
        var roleDescription = sender.getAttribute ("RolDesc");
        roleDescriptionTableCell.innerHTML = roleDescription;
    }
    catch(err){}
}

function role_onmouseout()
{
    try
    {
        roleDescriptionTableCell.innerHTML = "&nbsp;";
    }
    catch(err){}        
}

function role_selectAllNone (all)
{
    if (!roleCheckBoxList) return;
    
    var inputs = roleCheckBoxList.getElementsByTagName('input');
    for (var j = 0; j < inputs.length; j++)
    {
        var input = inputs[j];
        input.checked = all;
    }
}

function flexExportType_selectAllNone (all)
{
    if (!flexExportTypePanel) return;
    
    var inputs = flexExportTypePanel.getElementsByTagName('input');
    for (var j = 0; j < inputs.length; j++)
    {
        var input = inputs[j];
        input.checked = all;
    }
}

function userEdit_Init()
{
    companyDropDown = document.getElementById (companyDropDownId);
    countryDropDown = document.getElementById (countryDropDownId);
    locationDropDown = document.getElementById (locationDropDownId);
    //alt loc
    altLocationDropDown = document.getElementById(altLocationDropDownId);
    //alt loc
    roleDescriptionTableCell = document.getElementById("roleDescriptionTableCell");
    roleCheckBoxList = document.getElementById(roleCheckBoxListId);
    flexExportTypePanel = document.getElementById(flexExportTypePanelId);

    try
    {
        locationFilter();
        flexExportTypeFilter();
    }
    catch(err)
    {}
    
    // b2b stuff
    ToggleB2BElementsVisibility();
    // b2b stuff
}

    function ToggleB2BElementsVisibility()
    {
        try
        {
            var chkB2BGrossNettEnabled = document.getElementById("<% =chkB2BGrossNettEnabled.ClientID %>");
            var chkGross = document.getElementById("<% =cblB2BGrossNetWhatOpt.ClientID %>" + "_0" );
            var chkNett = document.getElementById("<% =cblB2BGrossNetWhatOpt.ClientID %>" + "_1" );

            var radGross = document.getElementById("<% =rblB2BGrossNetDefOpt.ClientID %>" + "_0" );
            var radNett = document.getElementById("<% =rblB2BGrossNetDefOpt.ClientID %>" + "_1" );

            var tdGNA1 = document.getElementById("ctl00_ContentPlaceHolder_tabs_agentTabPanel_tdGNA1");
            var tdGNA2 = document.getElementById("ctl00_ContentPlaceHolder_tabs_agentTabPanel_tdGNA2");
            var trGND = document.getElementById("ctl00_ContentPlaceHolder_tabs_agentTabPanel_trGND");

            if (chkB2BGrossNettEnabled)
            {
                if (chkB2BGrossNettEnabled.checked)
                {
                    tdGNA1.style.display = "block";
                    tdGNA2.style.display = "block";
                    trGND.style.display = "block";
                }
                else
                {
                    tdGNA1.style.display = "none";
                    tdGNA2.style.display = "none";
                    trGND.style.display = "none";
                    
                    chkGross.checked = false;
                    chkNett.checked = false;
                    radGross.checked = false;
                    radNett.checked = false;
                }
            }
        }
        catch(err)
        {}
    }

addEvent (window, "load", userEdit_Init);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(userEdit_Init);
    </script>

    <script language="javascript" type="text/javascript">
    
    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();  //the control causing the postback
        if( (pbControl.id.indexOf('updateButton') > -1) || (pbControl.id.indexOf('createButton') > -1)  )
        {
            pbControl.disabled = true;
        }
    }

    function EndRequestHandler(sender, args) {
        if( (pbControl.id.indexOf('updateButton') > -1) || (pbControl.id.indexOf('createButton') > -1)  )
        {
            pbControl.disabled = false;
        }
        pbControl = null;
        ShowHideCurrencySettings(document.getElementById('<% =chkB2BCurrencyOptionEnabled.ClientID %>'));
    }

    </script>

    <%--B2B Change--%>

    <script type="text/javascript" language="javascript" src="../UserControls/DefaultAndSelectionControl/DefaultAndSelection.js"></script>

    <script language="javascript" type="text/javascript">
    function UpdateDefaults(CheckBoxListId, RadioButtonListId)
    {
        var chkGross = document.getElementById(CheckBoxListId + "_0" );
        var chkNett = document.getElementById(CheckBoxListId + "_1" );
        
        var tblRadioButtons = document.getElementById(RadioButtonListId);
        
        var tdGNDLabel = document.getElementById('<% =tdGNDLabel.ClientID %>');
         
        if(!chkGross.checked || !chkNett.checked)
        {
            // both not selected
            tblRadioButtons.style.display = "none";
            tdGNDLabel.style.display = "none";
            document.getElementById(RadioButtonListId + "_0" ).checked = false;
            document.getElementById(RadioButtonListId + "_1" ).checked = false;
        }
        else
        {
            // both selected
            tblRadioButtons.style.display = "block";
            tdGNDLabel.style.display = "block";
            document.getElementById(RadioButtonListId + "_0" ).checked = false;
            document.getElementById(RadioButtonListId + "_1" ).checked = false;
        }
        
    }
    
    function ShowHideCurrencySettings(chkB2BCurrencyOptionEnabled)
    {
        document.getElementById("<% =tblCurrencySettings.ClientID %>").style.display = chkB2BCurrencyOptionEnabled.checked ? "block" : "none";
    }
    </script>

    <%--B2B Change--%>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:Panel ID="userPanel" runat="Server" Width="780px">
        <table style="width: 100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width: 150px">
                    Code:</td>
                <td style="width: 250px">
                    <asp:TextBox ID="codeTextBox" runat="server" Style="width: 150px" MaxLength="24" /></td>
                <td style="width: 150px">
                    Name:</td>
                <td style="width: 250px">
                    <asp:TextBox ID="nameTextBox" runat="server" Style="width: 250px" MaxLength="64" /></td>
            </tr>
            <tr id="trCompanyAndCountry" runat="server">
                <td>
                    Company:</td>
                <td>
                    <asp:DropDownList ID="companyDropDown" runat="server" Style="width: 200px" onchange="locationFilter(); flexExportTypeFilter();">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>
                    Country:</td>
                <td>
                    <asp:DropDownList ID="countryDropDown" runat="server" Style="width: 200px" onchange="locationFilter()">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr id="trLocation" runat="server">
                <td>
                    Location:</td>
                <td colspan="3">
                    <asp:DropDownList ID="locationDropDown" runat="server" Style="width: 600px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <%--Alt Location--%>
            <tr id="trAltLocation" runat="server">
                <td>
                    Alternate Location:</td>
                <td colspan="3">
                    <asp:DropDownList ID="altLocationDropDown" runat="server" Style="width: 600px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <%--Alt Location--%>
            <tr>
                <td>
                    Email:</td>
                <td colspan="3">
                    <uc1:RegExTextBox ID="emailTextBox" runat="server" Nullable="false" ErrorMessage="Enter a valid email"
                        RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,7}$" Width="300px" />
                </td>
            </tr>
            <tr>
                <td>
                    Password:</td>
                <td colspan="3">
                    <asp:TextBox ID="passwordTextBox" runat="server" TextMode="Password" Width="200px" />
                </td>
            </tr>
            <tr id="trRefundAndVehicle" runat="server">
                <td>
                    Refund Limit:</td>
                <td>
                    <uc1:RegExTextBox ID="refundLimitTextBox" runat="server" Nullable="false" ErrorMessage="Enter a valid refund limit"
                        RegExPattern="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$" Width="100px" />
                </td>
                <td>
                    Vehicle Requests:</td>
                <td>
                    <asp:TextBox ID="vehicleRequestCountTextBox" runat="server" ReadOnly="true" Width="100px" />
                </td>
            </tr>
            <tr runat="Server" id="statusTableRow">
                <td style="border-width: 1px 0px;">
                    Status:</td>
                <td colspan="3" style="border-width: 1px 0px;">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal"
                        RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Inactive" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        <asp:Panel ID="tabsPanel" runat="server" Style="margin-top: 10px">
            <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="1" Width="100%">
                <ajaxToolkit:TabPanel runat="server" ID="auroraTabPanel" HeaderText="Aurora">
                    <ContentTemplate>
                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                            <tr>
                                <td style="border-bottom-width: 1px; width: 100px">
                                    Initial Function:</td>
                                <td style="border-bottom-width: 1px">
                                    <asp:DropDownList ID="menuDropDown" runat="server" Style="width: 600px">
                                        <asp:ListItem />
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <b>Roles:</b>
                        <asp:CheckBoxList ID="roleCheckBoxList" runat="server" RepeatLayout="Table" RepeatColumns="5"
                            Width="100%" RepeatDirection="Vertical">
                        </asp:CheckBoxList>
                        <div style="text-align: right">
                            <a href="#" onclick="role_selectAllNone(true); return false;">Select All</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                            <a href="#" onclick="role_selectAllNone(false); return false;">Select None</a>
                        </div>
                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                            <tr>
                                <td style="width: 100px">
                                    Description:</td>
                                <td id="roleDescriptionTableCell">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border-top-width: 1px; border-top-style: dotted">
                                    Copy From User:</td>
                                <td style="border-top-width: 1px; border-top-style: dotted">
                                    <uc1:PickerControl ID="userPicker" runat="server" AppendDescription="true" PopupType="ADMIN_GETUSERINFO"
                                        Width="200px" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="userCopyButton" runat="Server" Text="Copy" CssClass="Button_Small" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="agentTabPanel" HeaderText="Agents &amp; B2B">
                    <ContentTemplate>
                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                            <tr>
                                <td style="width: 100px">
                                    Default Agent:</td>
                                <td>
                                    <uc1:PickerControl ID="defaultAgentPicker" runat="server" AppendDescription="true"
                                        PopupType="AGENT" Width="325px" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td style="border-top-width: 1px" colspan="2">
                                    <b>B2B</b></td>
                            </tr>
                            <tr>
                                <td>
                                    Type:</td>
                                <td>
                                    <asp:RadioButtonList ID="agentUserRadioButtonList" runat="server" RepeatColumns="3"
                                        CssClass="repeatLabelTable">
                                        <asp:ListItem Text="None" />
                                        <asp:ListItem Text="User" />
                                        <asp:ListItem Text="Administrator" />
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%" cellpadding="2" cellspacing="0">
                            <tr>
                                <td style="border-bottom-width: 1px; border-bottom-style: dotted; width: 100px">
                                    Add Agent:</td>
                                <td style="border-bottom-width: 1px; border-bottom-style: dotted;">
                                    <uc1:PickerControl ID="addAgentPicker" runat="server" AppendDescription="true" PopupType="AGENT"
                                        Width="325px" />
                                </td>
                                <td style="border-bottom-width: 1px; border-bottom-style: dotted;" align="right">
                                    &nbsp;
                                    <asp:Button ID="addAgentButton" runat="server" Text="Add" CssClass="Button_Standard Button_Add" />
                                    <asp:Button ID="removeAgentButton" runat="server" Text="Remove" CssClass="Button_Standard Button_Remove" />
                                </td>
                            </tr>
                        </table>
                        <asp:CheckBoxList ID="agentCheckBoxList" runat="server" RepeatColumns="2" Width="100%">
                        </asp:CheckBoxList>
                        <%--Gross-Nett stuff--%>
                        <br />
                        <table id="tblB2BConfig" runat="server" width="100%">
                            <tr>
                                <td style="border-top-width: 1px; border-top-style: dotted;" colspan="4">
                                    <label>
                                        <b>B2B General User Config:</b></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%">
                                    Currency Option Enabled:
                                </td>
                                <td style="width: 25%">
                                    <asp:CheckBox ID="chkB2BCurrencyOptionEnabled" runat="server" onclick="ShowHideCurrencySettings(this)" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 25%">
                                    Gross Nett Enabled:
                                </td>
                                <td style="width: 25%">
                                    <asp:CheckBox ID="chkB2BGrossNettEnabled" runat="server" />
                                </td>
                                <td style="width: 25%" id="tdGNA1">
                                    Gross Nett Applicability:
                                </td>
                                <td style="width: 25%" id="tdGNA2">
                                    <asp:CheckBoxList CssClass="checkboxlist" ID="cblB2BGrossNetWhatOpt" runat="server"
                                        RepeatDirection="horizontal">
                                        <asp:ListItem Text="Gross" Value="Gross"></asp:ListItem>
                                        <asp:ListItem Text="Nett" Value="Nett"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr id="trGND">
                                <td style="width: 25%" id="tdGNDLabel" runat="server">
                                    Gross Nett Default:
                                </td>
                                <td style="width: 25%">
                                    <asp:RadioButtonList CssClass="checkboxlist" ID="rblB2BGrossNetDefOpt" runat="server"
                                        RepeatDirection="horizontal">
                                        <asp:ListItem Text="Gross" Value="Gross"></asp:ListItem>
                                        <asp:ListItem Text="Nett" Value="Nett"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <%--Gross-Nett stuff--%>
                        <%--Agent-Currency Settings--%>
                        <asp:UpdatePanel ID="updCurr" runat="server">
                            <ContentTemplate>
                                <table id="tblCurrencySettings" runat="server" width="100%">
                                    <tr>
                                        <td style="border-top-width: 1px; border-top-style: dotted;" colspan="4">
                                            <label>
                                                <b>B2B Currency Config:</b></label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Agent Context:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="cmbAgentForCurrency" runat="server" AutoPostBack="true" Width="250px"
                                                OnSelectedIndexChanged="cmbAgentForCurrency_SelectedIndexChanged" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div style="width: 50%">
                                                <uc2:DefaultAndSelectionUserControl ID="objDefaultAndSelectionUserControl" CollectionName="Currency"
                                                    ShowHeader="true" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" align="right">
                                            <asp:Button ID="btnSaveCurrencyData" runat="server" Text="Save B2B Currency Config"
                                                Width="185px" CssClass="Button_Standard Button_Save" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                        <%--Agent-Currency Settings--%>
                        <%--Sub Users--%>
                        <table id="tblSubUser" runat="server" width="100%">
                            <tr>
                                <td style="border-top-width: 1px; border-top-style: dotted;">
                                    <label><b>B2B users linked to current super user:</b></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom:dotted 1px black">
                                    <br />
                                    Active users:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal runat="server" ID="ltrSubUsers" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td style="border-bottom:dotted 1px black">
                                    Inactive users:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal runat="server" ID="ltrInactiveSubUsers" />
                                </td>
                            </tr>
                        </table>
                        <%--Sub Users--%>
                        <%--Super Users--%>
                        <table id="tblSuperUser" runat="server" width="100%">
                            <tr>
                                <td style="border-top-width: 1px; border-top-style: dotted;">
                                    <label><b>Super user in charge of current user:</b></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal runat="server" ID="ltrSuperUser" />
                                </td>
                            </tr>
                        </table>
                        <%--Super Users--%>
                    </ContentTemplate>
                    <HeaderTemplate>
                        Agents &amp; B2B
                    </HeaderTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="flexExportTypeTabPanel" HeaderText="Flex Export Types">
                    <ContentTemplate>
                        <asp:Panel ID="flexExportTypePanel" runat="server" />
                        <div style="text-align: right">
                            <a href="#" onclick="flexExportType_selectAllNone(true); return false;">Select All</a>
                            &nbsp;&nbsp;|&nbsp;&nbsp; <a href="#" onclick="flexExportType_selectAllNone(false); return false;">
                                Select None</a>
                        </div>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </asp:Panel>
        <asp:UpdatePanel runat="server" ID="updMain">
            <ContentTemplate>
                <table style="width: 100%; margin-top: 5px" cellpadding="2" cellspacing="0">
                    <tr>
                        <td align="right">
                            <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                                Visible="False" />
                            <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                                Visible="False" />
                            <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
