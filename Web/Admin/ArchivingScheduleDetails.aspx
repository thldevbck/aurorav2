<%@ Page Language="VB" MasterPageFile="~/Include/PopupHeader.master" AutoEventWireup="false"
    CodeFile="ArchivingScheduleDetails.aspx.vb" Inherits="Admin_ArchivingScheduleDetails"
    Title="Archiving Schedule Details" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="250px">
        <tr>
            <td>
                <asp:Button runat="server" Text="Close" OnClientClick="window.close();" CssClass="Button_Standard Button_Close" ID="btnClose" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView Width="100%" ID="grdArchivingScheduleDetails" runat="server" CssClass="dataTableGrid"
                    AutoGenerateColumns="false" PageSize="100" AllowPaging="True">
                    <Columns>
                        <asp:BoundField DataField="BookingNumber" HeaderText="Booking Number" />
                        <asp:CheckBoxField DataField="Archived" HeaderText="Archived ?" />
                    </Columns>
                    <AlternatingRowStyle CssClass="oddRow" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button runat="server" Text="Close" OnClientClick="window.close();" CssClass="Button_Standard Button_Close" ID="btnClose2" />
            </td>
        </tr>
    </table>
</asp:Content>
