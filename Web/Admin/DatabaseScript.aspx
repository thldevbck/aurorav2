<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DatabaseScript.aspx.vb" Inherits="Admin_DatabaseScript" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
<script type="text/javascript">

function database_selectAllNone (id, all)
{
    var checkBoxList = document.getElementById (id);
    if (!checkBoxList) return;
    
    var inputs = checkBoxList.getElementsByTagName('input');
    for (var j = 0; j < inputs.length; j++)
    {
        var input = inputs[j];
        input.checked = all;
    }
}


</script>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Repeater ID="databaseRepeater" runat="Server">
        <ItemTemplate>

            <uc1:CollapsiblePanel 
                ID="databaseCollapsiblePanel" 
                runat="server"
                Title="Tables" 
                TargetControlID="databasePanel" />
            <asp:Panel ID="databasePanel" runat="server">
                <asp:CheckBoxList ID="databaseCheckBoxList" runat="server" RepeatColumns="3" RepeatDirection="Vertical" Width="100%" />

                <div style="text-align: right">
                    <asp:HyperLink id="selectAllHyperLink" runat="server" NavigateUrl="#" Text="Select All" />
                    &nbsp;&nbsp;|&nbsp;&nbsp;
                    <asp:HyperLink id="selectNoneHyperLink" runat="server" NavigateUrl="#" Text="Select None" />
                </div>
                <br />
            </asp:Panel>
        
        </ItemTemplate>    
    </asp:Repeater>
    
    <table cellpadding="0" cellspacing="0" width="100%" style="margin: 5px 0">
        <tr>
            <td align="right">
                <asp:Button ID="generateButton" runat="server" CssClass="Button_Standard Button_Save" Text="Generate" />
            </td>
        </tr>
    </table>
    
    <asp:Panel ID="generatePanel" runat="server" Visible="false" Style="background-color: #FFF; padding: 10px; border-style: dotted; border-width: 1px;">
        <pre><asp:Literal ID="generateLiteral" runat="server" /></pre>
    </asp:Panel>

    <p>&nbsp;</p>
    
</asp:Content>

