Imports System.Collections.Generic
Imports System.Web.Script.Serialization

Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.LocationDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.LocationMaintenance)> _
<AuroraPageTitle("Location Search")> _
Partial Class Admin_LocationSearch
    Inherits AuroraPage

    Public Class TownCityItem
        Public Text As String
        Public Value As String
        Public CtyCode As String
    End Class

    Private _townCityItemsString As String = ""
    Public ReadOnly Property TownCityItemsString()
        Get
            Return _townCityItemsString
        End Get
    End Property

    Private _locationDataSet As New LocationDataSet()

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _locationDataSet.EnforceConstraints = False

        DataRepository.GetLocationLookups(_locationDataSet)

        typeDropDown.Items.Clear()
        typeDropDown.Items.Add(New ListItem("(All)", ""))
        For Each codeRow As CodeRow In _locationDataSet.Code
            If codeRow.CodCdtNum <> Aurora.Common.DataConstants.CodeType_Location_Type Then Continue For
            Dim li As ListItem = New ListItem(codeRow.CodDesc, codeRow.CodId)
            typeDropDown.Items.Add(li)
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem("(All)", ""))
        For Each countryRow As CountryRow In _locationDataSet.Country
            Dim li As ListItem = New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode)
            countryDropDown.Items.Add(li)
        Next

        Dim townCityItemList As New List(Of TownCityItem)
        townCityDropDown.Items.Clear()
        townCityDropDown.Items.Add(New ListItem("(All)", ""))
        For Each townCityRow As TownCityRow In _locationDataSet.TownCity
            Dim li As ListItem = New ListItem(townCityRow.TctCode & " - " & townCityRow.TctName, townCityRow.TctCode)
            townCityDropDown.Items.Add(li)

            Dim townCityItem As New TownCityItem()
            townCityItem.Text = li.Text
            townCityItem.Value = li.Value
            townCityItem.CtyCode = townCityRow.TctCtyCode
            townCityItemList.Add(townCityItem)
        Next

        Dim js As New JavaScriptSerializer()
        _townCityItemsString = js.Serialize(townCityItemList)
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = "" & CookieValue("Location_locCode")

        Try
            If Not String.IsNullOrEmpty(CookieValue("Location_locCodTypId")) Then
                typeDropDown.SelectedValue = CookieValue("Location_locCodTypId")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("Location_ctyCode")) Then
                countryDropDown.SelectedValue = CookieValue("Location_ctyCode")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("Location_tctCode")) Then
                townCityDropDown.SelectedValue = CookieValue("Location_tctCode")
            End If
        Catch
        End Try

        managerTextBox.Text = "" & CookieValue("Location_conName")

        If Not String.IsNullOrEmpty(CookieValue("Location_locIsActive")) Then
            If CBool(CookieValue("Location_locIsActive")) Then
                statusRadioButtonList.SelectedIndex = 1
            Else
                statusRadioButtonList.SelectedIndex = 2
            End If
        End If

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        createButton.Visible = True

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Me.SearchParam) Then
                codeTextBox.Text = Me.SearchParam
            Else
                BuildForm(False)
            End If

            If HasSearchParameters() Then searchButton_Click(Me, Nothing)
            InitZoneLocation()
        End If
    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("Location_locCode") = codeTextBox.Text.Trim()
        CookieValue("Location_locCodTypId") = typeDropDown.SelectedValue
        CookieValue("Location_ctyCode") = countryDropDown.SelectedValue
        CookieValue("Location_tctCode") = townCityDropDown.SelectedValue
        CookieValue("Location_conName") = managerTextBox.Text.Trim()
        If statusRadioButtonList.SelectedIndex = 1 Then
            CookieValue("Location_locIsActive") = "True"
        ElseIf statusRadioButtonList.SelectedIndex = 2 Then
            CookieValue("Location_locIsActive") = "False"
        Else
            CookieValue("Location_locIsActive") = ""
        End If

        ''re:mia Phoenix
        CookieValue("Location_zone") = Me.ZoneLocationDropDown.SelectedValue
    End Sub

    Public Function HasSearchParameters() As Boolean
        Return codeTextBox.Text.Trim().Length > 0 _
            OrElse typeDropDown.SelectedIndex > 0 _
            OrElse countryDropDown.SelectedIndex > 0 _
            OrElse countryDropDown.SelectedIndex > 0 _
            OrElse townCityDropDown.SelectedIndex > 0 _
            OrElse managerTextBox.Text.Trim().Length > 0 _
            OrElse statusRadioButtonList.SelectedIndex > 0 _
            OrElse False
    End Function

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        If Not HasSearchParameters() AndAlso String.IsNullOrEmpty(Me.ZoneLocationDropDown.SelectedValue) Then
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "Enter search parameters")
            searchResultLabel.Text = ""
            searchResultTable.Visible = False
            Return
        End If

        UpdateCookieValues()

        Dim locIsActive As New Nullable(Of Boolean)
        If statusRadioButtonList.SelectedIndex = 1 Then
            locIsActive = New Nullable(Of Boolean)(True)
        ElseIf statusRadioButtonList.SelectedIndex = 2 Then
            locIsActive = New Nullable(Of Boolean)(False)
        End If

        If String.IsNullOrEmpty(Me.ZoneLocationDropDown.SelectedValue) Then
            DataRepository.SearchLocation(_locationDataSet, _
                        Me.CompanyCode, _
                        codeTextBox.Text.Trim(), _
                        typeDropDown.SelectedValue, _
                        countryDropDown.SelectedValue, _
                        townCityDropDown.SelectedValue, _
                        managerTextBox.Text.Trim(), _
                        locIsActive)
        Else
            Dim comCode As String = Me.CompanyCode
            Dim locCode As String = ""
            Dim locCodeSearch As String = codeTextBox.Text.Trim ''country
            Dim locCodTypId As String = typeDropDown.SelectedValue ''type
            Dim ctyCode As String = countryDropDown.SelectedValue ''Town / City
            Dim tctCode As String = townCityDropDown.SelectedValue
            Dim conName As String = managerTextBox.Text.Trim
            Dim Zoneid As String = Me.ZoneLocationDropDown.SelectedValue
            DataRepository.GetLocationFilteredByZone(_locationDataSet, _
                                                     comCode, _
                                                     locCode, _
                                                     locCodeSearch, _
                                                     locCodTypId, _
                                                     ctyCode, _
                                                     tctCode, _
                                                     conName, _
                                                     locIsActive, _
                                                     Me.ZoneLocationDropDown.SelectedValue)
        End If
        
        While searchResultTable.Rows.Count > 1
            searchResultTable.Rows.RemoveAt(1)
        End While

        If _locationDataSet.Location.Rows.Count = 0 Then
            searchResultLabel.Text = "No locations found"
            searchResultTable.Visible = False
        Else
            If _locationDataSet.Location.Rows.Count = 1 Then
                searchResultLabel.Text = _locationDataSet.Location.Rows.Count.ToString() & " location found"
            Else
                searchResultLabel.Text = _locationDataSet.Location.Rows.Count.ToString() & " locations found"
            End If
            searchResultTable.Visible = True

            Dim index As Integer = 0
            For Each locationRow As LocationRow In _locationDataSet.Location
                Dim tableRow As New TableRow()
                tableRow.BackColor = locationRow.StatusColor
                searchResultTable.Rows.Add(tableRow)

                Dim nameCell As New TableCell()
                Dim nameHyperLink As New HyperLink()
                nameHyperLink.Text = Server.HtmlEncode(locationRow.Description)
                nameHyperLink.NavigateUrl = "LocationEdit.aspx?LocCode=" & Server.UrlEncode(locationRow.LocCode)
                nameCell.Controls.Add(nameHyperLink)
                tableRow.Controls.Add(nameCell)

                Dim typeCell As New TableCell()
                If locationRow.CodeRow IsNot Nothing Then
                    typeCell.Text = locationRow.CodeRow.CodDesc
                Else
                    typeCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(typeCell)

                Dim countryCell As New TableCell()
                If locationRow.TownCityRow IsNot Nothing AndAlso locationRow.TownCityRow.CountryRow IsNot Nothing Then
                    countryCell.Text = locationRow.TownCityRow.CountryRow.CtyCode & " - " & locationRow.TownCityRow.CountryRow.CtyName
                Else
                    countryCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(countryCell)

                Dim townCityCell As New TableCell()
                If locationRow.TownCityRow IsNot Nothing Then
                    townCityCell.Text = locationRow.TownCityRow.TctCode & " - " & locationRow.TownCityRow.TctName
                Else
                    townCityCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(townCityCell)

                Dim managerCell As New TableCell()
                If locationRow.ContactRow IsNot Nothing AndAlso Not locationRow.ContactRow.IsConNameNull Then
                    managerCell.Text = locationRow.ContactRow.ConName
                Else
                    managerCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(managerCell)

                Dim statusCell As New TableCell()
                If Not locationRow.IsLocIsActiveNull Then
                    statusCell.Text = IIf(locationRow.LocIsActive, "Active", "Inactive")
                Else
                    statusCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(statusCell)

                index += 1
            Next
        End If

    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        UpdateCookieValues()

        Response.Redirect("LocationEdit.aspx")
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        codeTextBox.Text = ""
        typeDropDown.SelectedIndex = 0
        countryDropDown.SelectedIndex = 0
        townCityDropDown.SelectedIndex = 0
        managerTextBox.Text = ""
        statusRadioButtonList.SelectedIndex = 0
        searchResultTable.Visible = False
        searchResultLabel.Text = ""

        ''reV:mia Phoenix
        Me.ZoneLocationDropDown.SelectedValue = ""
        UpdateCookieValues()
    End Sub

#Region "Rev:MIA Phoenix"
    Sub InitZoneLocation()
        If Page.IsPostBack Then Exit Sub

        Dim vwzone As System.Data.DataView = Aurora.Admin.Data.DataRepository.SelectLocationZone.Tables(0).DefaultView
        Dim item As ListItem = New ListItem("", "")
        ZoneLocationDropDown.Items.Add(item)
        For Each row As Data.DataRow In vwzone.Table.Rows
            ''System.Diagnostics.Debug.WriteLine(row("LocZoneId") & " - " & ProperCasing(row("FullZone")))
            item = New ListItem(ProperCasing(row("FullZone")), row("LocZoneId"))
            ZoneLocationDropDown.Items.Add(item)
        Next

    End Sub
    Function ProperCasing(ByVal FullName As String) As String
        If String.IsNullOrEmpty(FullName) Then Return ""
        Dim strFullPackage() As String = FullName.ToLower.Split(" ")
        Dim tempStrFullPackage As String = ""

        For Each tempStrPackage As String In strFullPackage
            If Not String.IsNullOrEmpty(tempStrPackage) Then
                Dim firstLetter As String = tempStrPackage.Substring(0, 1).ToUpper
                tempStrPackage = tempStrPackage.Remove(0, 1)
                tempStrPackage = firstLetter & tempStrPackage & " "
                tempStrFullPackage = tempStrFullPackage & tempStrPackage
            End If
        Next

        Dim tempZone As String() = tempStrFullPackage.Split("-")
        Return tempZone(0).ToUpper & " - " & tempZone(1)
    End Function
#End Region
End Class
