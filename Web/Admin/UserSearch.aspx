<%@ Page Language="VB" AutoEventWireup="false" CodeFile="UserSearch.aspx.vb" Inherits="Admin_UserSearch" MasterPageFile="~/Include/AuroraHeader.master" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ScriptPlaceHolder">
<script type="text/javascript">
var locationItems = <%= Me.LocationItemsString %>;

var companyDropDownId = '<%= companyDropDown.ClientId %>';
var companyDropDown;
var countryDropDownId = '<%= countryDropDown.ClientId %>';
var countryDropDown;
var locationDropDownId = '<%= locationDropDown.ClientId %>';
var locationDropDown;


function locationFilter()
{
    var oldLocCode = locationDropDown.value;

    clearDropDown (locationDropDown);
    addDropDownOption (locationDropDown, "(All)", "");
    for (var i = 0; i < locationItems.length; i++)
    {
        var locationItem = locationItems[i];
        
        if (((companyDropDown.value == '') || (companyDropDown.value == locationItem.ComCode))
         && ((countryDropDown.value == '') || (countryDropDown.value == locationItem.CtyCode)))
            addDropDownOption (locationDropDown, locationItem.Text, locationItem.Value);
    }
    
    locationDropDown.value = oldLocCode;
}

function locationInit()
{
    companyDropDown = document.getElementById (companyDropDownId);
    countryDropDown = document.getElementById (countryDropDownId);
    locationDropDown = document.getElementById (locationDropDownId);
    
    locationFilter();
}

addEvent (window, "load", locationInit);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(locationInit);
</script>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="userSearchPanel" runat="Server" Width="780px" CssClass="userSearch" DefaultButton="searchButton">
    
        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width:150px">Code:</td>
                <td style="width:250px"><asp:TextBox ID="codeTextBox" runat="server" style="width:150px" MaxLength="24" /></td>
                <td style="width:150px">Name:</td>
                <td style="width:250px"><asp:TextBox ID="nameTextBox" runat="server" style="width:250px" MaxLength="64" /></td>
            </tr>
            <tr>
                <td style="width:100px;">E-mail Address:</td>
                <td colspan="3">
                   <asp:TextBox ID="txtEmail" runat="server" style="width:250px" MaxLength="64" />
                </td>
            </tr>
            <%--B2B Change--%>
            <tr>
                <td>Agent Code:</td>
                <td colspan="3"><asp:TextBox ID="agentCodeTextBox" runat="server" style="width:200px" /></td>
            </tr>
            <%--B2B Change--%>
            <tr style="display:none;">
                <td>Company:</td>
                <td>
                    <asp:DropDownList ID="companyDropDown" runat="server" style="width:200px" onchange="locationFilter()">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>Country:</td>
                <td>
                    <asp:DropDownList ID="countryDropDown" runat="server" style="width:200px" onchange="locationFilter()">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr style="display:none;">
                <td style="width:100px;">Location:</td>
                <td colspan="3">
                    <asp:DropDownList ID="locationDropDown" runat="server" style="width:600px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="border-top-width: 1px; border-top-style: dotted">Status:</td>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="All" Selected="True" />
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Inactive" />
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td style="border-top-width: 1px; border-top-style: dotted">Type:</td>
                <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                    <asp:RadioButtonList ID="typeRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="All" Selected="True" />
                        <asp:ListItem Text="Aurora" />
                        <asp:ListItem Text="B2B" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>

        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="border-width: 1px 0px"><i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;</td>
                <td align="right" style="border-width: 1px 0px">
                    <asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                </td>
            </tr>
        </table>
        
        <br />
        
        <asp:Table ID="searchResultTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" Visible="False" CssClass="dataTableColor">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Text="Name" />
                <asp:TableHeaderCell Text="Company" Width="125px" />
                <asp:TableHeaderCell Text="Country" Width="125px" />
                <asp:TableHeaderCell Text="Location" Width="175px" />
                <asp:TableHeaderCell Text="Aurora" Width="50px" />
                <asp:TableHeaderCell Text="B2B" Width="50px" />
                <asp:TableHeaderCell Text="Status" Width="50px" />
            </asp:TableHeaderRow>
        </asp:Table>

    </asp:Panel>

</asp:Content>

