Imports System.Data

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.RateArchiving)> _
Partial Class Admin_ArchivingRateScheduleDetails
    Inherits AuroraPage

    Public Property ScheduleDataTable() As DataTable
        Get
            Return CType(Session("RateScheduleDataTable"), DataTable)
        End Get
        Set(ByVal value As DataTable)
            Session("RateScheduleDataTable") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Dim iScheduleId As Long = CLng(Request("ScheduleId"))
            ScheduleDataTable = Aurora.Admin.Services.Archiving.GetRateScheduleDetails(iScheduleId)
            BindGrid()
        End If
    End Sub

    Sub BindGrid()
        grdArchivingScheduleDetails.DataSource = ScheduleDataTable
        grdArchivingScheduleDetails.DataBind()
    End Sub


    Protected Sub grdArchivingScheduleDetails_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdArchivingScheduleDetails.PageIndexChanging
        grdArchivingScheduleDetails.PageIndex = e.NewPageIndex
        BindGrid()
    End Sub
End Class
