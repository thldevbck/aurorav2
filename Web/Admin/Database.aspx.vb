Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Xml

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

<AuroraFunctionCodeAttribute("DBSQL")> _
Partial Class Admin_Database
    Inherits AuroraPage

    Public Const SESSION_TABLENAME As String = "Admin_Database_TableName"
    Public Const SESSION_SQL As String = "Admin_Database_Sql"


    Public Class CsvUtil
        Public Const DefaultSeperator As String = ","
        Public Const DefaultQuote As String = """"

        ' The grunt work method to convert a string to csv
        Public Shared Function Format(ByVal value As Object, ByVal seperator As String, ByVal quote As String) As String
            If value Is Nothing Then Return String.Empty

            Dim result As String = value.ToString().Replace(quote.ToString(), quote.ToString() + quote.ToString())
            If result.IndexOf(seperator) <> -1 OrElse result.IndexOf(vbLf) <> -1 Then
                result = quote + result + quote
            End If
            Return result
        End Function

        Public Shared Function Format(ByVal value As Object) As String
            Return Format(value, DefaultSeperator, DefaultQuote)
        End Function

        ' Convert a collection to a CSV string list
        Public Shared Function Format(ByVal collection As ICollection, ByVal seperator As String, ByVal quote As String) As String
            Dim result As StringBuilder = New StringBuilder()
            Dim first As Boolean = True
            For Each value As Object In collection
                If first Then first = False Else result.Append(seperator)
                result.Append(Format(value, seperator, quote))
            Next
            Return result.ToString()
        End Function

        Public Shared Function Format(ByVal collection As ICollection) As String
            Return Format(collection, DefaultSeperator, DefaultQuote)
        End Function
    End Class



    Private _tableNamesTable As DataTable
    Private _columnInformation As ColumnInformationDataTable = Nothing

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        _tableNamesTable = DataRepository.GetTableNames()
        tableNameDropDown.DataSource = _tableNamesTable
        tableNameDropDown.DataValueField = "text"
        tableNameDropDown.DataTextField = "text"
        tableNameDropDown.DataBind()
    End Sub

    Private Sub LoadColumnInformation()
        columnsTable.Visible = False
        If String.IsNullOrEmpty(tableNameDropDown.SelectedValue) Then Return

        Dim tableName As String = tableNameDropDown.SelectedValue
        _columnInformation = DataRepository.GetColumnInformation(tableName)

        While columnsTable.Rows.Count > 1
            columnsTable.Rows.RemoveAt(1)
        End While

        Dim index As Integer = 0
        For Each _columnInformationRow As ColumnInformationRow In _columnInformation
            Dim tableRow As New TableRow()
            tableRow.CssClass = IIf((index Mod 2) = 0, "evenRow", "oddRow")
            columnsTable.Rows.Add(tableRow)

            Dim fieldNameCell As New TableCell()
            fieldNameCell.Text = Server.HtmlEncode(_columnInformationRow.ColumnName)
            tableRow.Cells.Add(fieldNameCell)

            Dim pkCell As New TableCell()
            pkCell.Text = IIf(_columnInformationRow.IsPrimaryKey, "<b>Yes</b>", "&nbsp;")
            tableRow.Cells.Add(pkCell)

            Dim nullableCell As New TableCell()
            nullableCell.Text = IIf(_columnInformationRow.IsNullable, "<b>Yes</b>", "&nbsp;")
            tableRow.Cells.Add(nullableCell)

            Dim identityCell As New TableCell()
            identityCell.Text = IIf(_columnInformationRow.IsIdentity, "<b>Yes</b>", "&nbsp;")
            tableRow.Cells.Add(identityCell)

            Dim dataTypeCell As New TableCell()
            dataTypeCell.Text = Server.HtmlEncode(_columnInformationRow.DataType)
            tableRow.Cells.Add(dataTypeCell)

            Dim dataSizeCell As New TableCell()
            dataSizeCell.Text = Server.HtmlEncode(_columnInformationRow.MaxLength.ToString())
            tableRow.Cells.Add(dataSizeCell)

            index += 1
        Next
        columnsTable.Visible = True
    End Sub

    Public Function DataRowValueToSqlString(ByVal dataRow As DataRow, ByVal columnName As String) As String
        If dataRow.IsNull(columnName) Then
            Return "NULL"
        ElseIf TypeOf (dataRow(columnName)) Is Boolean Then
            Return IIf(CType(dataRow(columnName), Boolean), "1", "0")
        ElseIf TypeOf (dataRow(columnName)) Is Date Then
            Return "'" & CType(dataRow(columnName), Date).ToString("dd/MM/yyyy") & "'"
        Else
            Return Utility.EscapeSqlString(dataRow(columnName).ToString())
        End If
    End Function

    Public Function DataRowValueToCsvString(ByVal dataRow As DataRow, ByVal columnName As String) As String
        If dataRow.IsNull(columnName) Then
            Return "NULL"
        ElseIf TypeOf (dataRow(columnName)) Is Boolean Then
            Return IIf(CType(dataRow(columnName), Boolean), "1", "0")
        ElseIf TypeOf (dataRow(columnName)) Is Date Then
            Return CType(dataRow(columnName), Date).ToString("dd/MM/yyyy")
        Else
            Return dataRow(columnName).ToString()
        End If
    End Function

    Private Function BuildInsertStatement(ByVal dataRow As DataRow) As String
        Dim dataTable As DataTable = dataRow.Table

        Dim line As New StringBuilder
        line.Append("INSERT INTO [" & tableNameDropDown.SelectedValue & "] ")

        Dim first As Boolean = True
        line.Append("(")
        For Each columnInformationRow As ColumnInformationRow In _columnInformation
            If columnInformationRow.IsIdentity() Then Continue For
            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
            If first Then first = False Else line.Append(", ")
            line.Append("[" & columnInformationRow.ColumnName & "]")
        Next
        line.AppendLine(")")

        first = True
        line.Append("SELECT ")
        For Each columnInformationRow As ColumnInformationRow In _columnInformation
            If columnInformationRow.IsIdentity() Then Continue For
            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
            If first Then first = False Else line.Append(", ")
            line.Append(DataRowValueToSqlString(dataRow, columnInformationRow.ColumnName) & " AS [" & columnInformationRow.ColumnName & "]")
        Next
        line.AppendLine("")

        first = True
        line.Append("WHERE NOT EXISTS (SELECT 1 FROM [" & tableNameDropDown.SelectedValue & "] WHERE ")
        For Each columnInformationRow As ColumnInformationRow In _columnInformation
            If Not columnInformationRow.IsPrimaryKey Then Continue For
            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
            If first Then first = False Else line.Append(" AND ")
            line.Append("[" & columnInformationRow.ColumnName & "]=" & DataRowValueToSqlString(dataRow, columnInformationRow.ColumnName))
        Next
        line.AppendLine(")")

        line.AppendLine()

        Return line.ToString()
    End Function

    Private Function BuildUpdateStatement(ByVal dataRow As DataRow) As String
        Dim dataTable As DataTable = dataRow.Table

        Dim line As New StringBuilder
        line.AppendLine("UPDATE [" & tableNameDropDown.SelectedValue & "] ")

        Dim first As Boolean = True
        line.Append("SET ")
        For Each columnInformationRow As ColumnInformationRow In _columnInformation
            If columnInformationRow.IsPrimaryKey OrElse columnInformationRow.IsIdentity() Then Continue For
            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
            If first Then first = False Else line.Append(", ")
            line.Append("[" & columnInformationRow.ColumnName & "]=" & DataRowValueToSqlString(dataRow, columnInformationRow.ColumnName))
        Next
        line.AppendLine()

        first = True
        line.Append("WHERE ")
        For Each columnInformationRow As ColumnInformationRow In _columnInformation
            If Not columnInformationRow.IsPrimaryKey Then Continue For
            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
            If first Then first = False Else line.Append(" AND ")
            line.Append("[" & columnInformationRow.ColumnName & "]=" & DataRowValueToSqlString(dataRow, columnInformationRow.ColumnName))
        Next
        line.AppendLine()

        line.AppendLine()

        Return line.ToString()
    End Function

    Private Function BuildDeleteStatement(ByVal dataRow As DataRow) As String
        Dim dataTable As DataTable = dataRow.Table

        Dim line As New StringBuilder
        line.AppendLine("DELETE FROM [" & tableNameDropDown.SelectedValue & "] ")

        Dim first As Boolean = True
        line.Append("WHERE ")
        For Each columnInformationRow As ColumnInformationRow In _columnInformation
            If Not columnInformationRow.IsPrimaryKey Then Continue For
            If Not dataTable.Columns.Contains(columnInformationRow.ColumnName) Then Continue For
            If first Then first = False Else line.Append(" AND ")
            line.Append("[" & columnInformationRow.ColumnName & "]=" & DataRowValueToSqlString(dataRow, columnInformationRow.ColumnName))
        Next
        line.AppendLine()

        line.AppendLine()

        Return line.ToString()
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack() Then

            If tableNameDropDown.Items.FindByValue("" & Me.Session(SESSION_TABLENAME)) IsNot Nothing Then
                tableNameDropDown.SelectedValue = "" & Me.Session(SESSION_TABLENAME)
            End If
            sqlTextBox.Text = "" & Me.Session(SESSION_SQL)
        End If

        LoadColumnInformation()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        sqlTextBox.Attributes.Add("onchange", "document.getElementById ('" & sqlChangedCheckBox.ClientID & "').checked = true;")
    End Sub

    Protected Sub executeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles executeButton.Click
        resultPanel.Visible = False
        resultTextBox.Visible = False
        resultRepeater.Visible = False

        Session.Add(SESSION_TABLENAME, tableNameDropDown.SelectedValue)
        Session.Add(SESSION_SQL, sqlTextBox.Text)

        LoadColumnInformation()

        If (outputRadioButtonList.SelectedValue = "insert" _
             OrElse outputRadioButtonList.SelectedValue = "update" _
             OrElse outputRadioButtonList.SelectedValue = "delete") _
         AndAlso _columnInformation Is Nothing Then
            Me.AddWarningMessage("Specify a table")
            Return
        End If

        Try
            If outputRadioButtonList.SelectedValue = "table" Then
                Dim result As DataSet = Aurora.Common.Data.ExecuteDataSetSql(sqlTextBox.Text, New DataSet)

                resultRepeater.DataSource = result.Tables
                resultRepeater.DataBind()

                resultRepeater.Visible = result.Tables.Count > 0

            ElseIf outputRadioButtonList.SelectedValue = "csv" Then
                Dim result As DataSet = Aurora.Common.Data.ExecuteDataSetSql(sqlTextBox.Text, New DataSet)

                Using writer As New System.IO.StringWriter()
                    For Each dataTable As DataTable In result.Tables
                        Dim columnNames As New List(Of String)
                        For Each dataColumn As DataColumn In dataTable.Columns
                            columnNames.Add(dataColumn.ColumnName)
                        Next
                        writer.WriteLine(CsvUtil.Format(columnNames))

                        For Each dataRow As DataRow In dataTable.Rows
                            Dim dataValues As New List(Of String)
                            For Each dataColumn As DataColumn In dataTable.Columns
                                dataValues.Add(DataRowValueToCsvString(dataRow, dataColumn.ColumnName))
                            Next
                            writer.WriteLine(CsvUtil.Format(dataValues))
                        Next
                        writer.WriteLine()
                    Next

                    resultTextBox.Text = writer.ToString()
                End Using

                resultTextBox.Visible = result.Tables.Count > 0

            ElseIf outputRadioButtonList.SelectedValue = "dataset" Then
                Dim result As DataSet = Aurora.Common.Data.ExecuteDataSetSql(sqlTextBox.Text, New DataSet)

                Using writer As New System.IO.StringWriter()
                    result.WriteXml(writer)
                    resultTextBox.Text = writer.ToString()
                End Using

                resultTextBox.Visible = result.Tables.Count > 0

            ElseIf outputRadioButtonList.SelectedValue = "xml" Then
                Dim xmlDoc As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlDoc(sqlTextBox.Text)

                Using writer As New System.IO.StringWriter()
                    Using xmlWriter As New System.Xml.XmlTextWriter(writer)
                        xmlWriter.Formatting = Formatting.Indented
                        xmlDoc.WriteTo(xmlWriter)
                    End Using
                    resultTextBox.Text = writer.ToString()
                End Using

                resultTextBox.Visible = True

            ElseIf outputRadioButtonList.SelectedValue = "insert" _
             OrElse outputRadioButtonList.SelectedValue = "update" _
             OrElse outputRadioButtonList.SelectedValue = "delete" Then
                Dim result As DataSet = Aurora.Common.Data.ExecuteDataSetSql(sqlTextBox.Text, New DataSet)

                Using writer As New System.IO.StringWriter()
                    For Each dataTable As DataTable In result.Tables
                        For Each dataRow As DataRow In dataTable.Rows
                            If outputRadioButtonList.SelectedValue = "insert" Then
                                writer.Write(BuildInsertStatement(dataRow))
                            ElseIf outputRadioButtonList.SelectedValue = "update" Then
                                writer.Write(BuildUpdateStatement(dataRow))
                            ElseIf outputRadioButtonList.SelectedValue = "delete" Then
                                writer.Write(BuildDeleteStatement(dataRow))
                            End If

                        Next
                    Next
                    resultTextBox.Text = writer.ToString()
                End Using

                resultTextBox.Visible = result.Tables.Count > 0
            End If

            Me.AddInformationMessage("SQL successfully executed")
            resultPanel.Visible = True
            helpPanel.Visible = False
        Catch ex As Exception
            Me.AddErrorMessage("Error executing SQL:<br/><br/>" & Me.Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Protected Sub resultRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles resultRepeater.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item AndAlso e.Item.ItemType <> ListItemType.AlternatingItem Then Return

        Dim table As DataTable = e.Item.DataItem
        Dim resultLabel As Label = e.Item.FindControl("resultLabel")
        Dim resultGridView As GridView = e.Item.FindControl("resultGridView")

        resultLabel.Text = table.Rows.Count & " row(s) found"

        resultGridView.DataSource = table
        resultGridView.DataBind()
    End Sub

    Protected Sub tableNameDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tableNameDropDown.SelectedIndexChanged
        If Not String.IsNullOrEmpty(tableNameDropDown.SelectedValue) AndAlso Not sqlChangedCheckBox.Checked Then
            sqlTextBox.Text = "SELECT TOP 10 * FROM " & tableNameDropDown.SelectedValue
        End If
    End Sub
End Class
