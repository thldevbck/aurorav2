<%--
Change Log 
23.7.10  -   Shoel   - Initial version ready
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="RateArchiving.aspx.vb" Inherits="Admin_RateArchiving" Title="Untitled Page" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">
    function ShowDetailsPopup(sPath)
    {
        sPath = './ArchivingRateScheduleDetails.aspx?ScheduleId=' + sPath;
        window.open(sPath,"","top=100,left=200,menubar=no, scrollbars=yes ,height=700 ,width=950");
    }

    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();  //the control causing the postback

        if(pbControl.id.indexOf('btnCreateSchedule') > -1 || pbControl.id.indexOf('btnStop') > -1 || pbControl.id.indexOf('btnUpdate') > -1)
        {
            pbControl.disabled = true;
        }
        
    }

    function EndRequestHandler(sender, args) {
        if(pbControl.id.indexOf('btnCreateSchedule') > -1 || pbControl.id.indexOf('btnStop') > -1 || pbControl.id.indexOf('btnUpdate') > -1)
        {
            pbControl.disabled = false;
        }

        pbControl = null;
    }    

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <table class="dataTable" width="1100px" style="left: -150px; position: relative">
                <tr>
                    <th colspan="4">
                        Create schedule
                    </th>
                </tr>
                <tr>
                    <td>
                        Financial year:</td>
                    <td>
                        <asp:DropDownList ID="cmbFinancialYear" runat="server" CssClass="Dropdown_Small">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Start job on:
                    </td>
                    <td>
                        <uc1:DateControl ID="dtcJobStartDate" runat="server" />
                        <uc1:TimeControl ID="tmcJobStartTime" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Run job for:
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbRunTime" runat="server" CssClass="Dropdown_Small">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="1 hour" Value="1"></asp:ListItem>
                            <asp:ListItem Text="2 hours" Value="2"></asp:ListItem>
                            <asp:ListItem Text="3 hours" Value="3"></asp:ListItem>
                            <asp:ListItem Text="4 hours" Value="4"></asp:ListItem>
                            <asp:ListItem Text="5 hours" Value="5"></asp:ListItem>
                            <asp:ListItem Text="6 hours" Value="6"></asp:ListItem>
                            <asp:ListItem Text="7 hours" Value="7"></asp:ListItem>
                            <asp:ListItem Text="8 hours" Value="8"></asp:ListItem>
                            <asp:ListItem Text="9 hours" Value="9"></asp:ListItem>
                            <asp:ListItem Text="10 hours" Value="10"></asp:ListItem>
                            <asp:ListItem Text="11 hours" Value="11"></asp:ListItem>
                            <asp:ListItem Text="12 hours" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        Run job every:
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbFrequency" runat="server" CssClass="Dropdown_Small">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                            <asp:ListItem Text="Day" Value="Day"></asp:ListItem>
                            <asp:ListItem Text="Week" Value="Week"></asp:ListItem>
                            <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                            <asp:ListItem Text="Year" Value="Year"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Send notification email to:
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtEmailIdsToNotify" runat="server" MaxLength="1000" onblur="if(this.value.length>1000){this.value=this.value.substring(0,1000)}"
                            TextMode="MultiLine" Style="width: 97%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="btnCreateSchedule" runat="server" Text="Create Schedule" CssClass="Button_Standard Button_Save"
                            Style="float: right" OnClientClick="return confirm('Are you sure you want to create a new booking archive schedule?');" />
                    </td>
                </tr>
            </table>
            <br />
            <table width="1100px" style="left: -150px; position: relative">
                <tr>
                    <th>
                        Existing Schedules
                    </th>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="grdSchedules" CssClass="dataTableGrid" AutoGenerateColumns="false"
                            runat="server" Width="100%">
                            <%--
                Schedule Number 
                Start Date Time
                Run For
                Run Every
                Notification List
                Status
                Details
                --%>
                            <Columns>
                                <asp:TemplateField>
                                    <ItemStyle Width="20px" />
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlkSchedule" runat="server" Text='<%# Bind("ScheduleId") %>' onclick="ShowDetailsPopup(this.innerHTML)"
                                            Style="cursor: pointer"></asp:HyperLink>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Schedule Number
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="FinancialYear" HeaderText="Financial Year" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <uc1:DateControl ID="dtcJobStartDate" runat="server" Date='<%# Eval("StartDateTime").Date %>' />
                                        <uc1:TimeControl ID="tmcJobStartTime" runat="server" Time='<%# Eval("StartDateTime").TimeOfDay %>' />
                                        <asp:HiddenField runat="server" ID="hdnStartDateTime" Value='<%# Bind("StartDateTime") %>' />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Start Date Time
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="NextRunDateTime" HeaderText="Next Run Date Time" DataFormatString="{0:dd/MM/yyyy HH:mm}" HtmlEncode="false" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cmbRunForHrs" runat="server" CssClass="Dropdown_Small">
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="hdnRunForHrs" Value='<%# Bind("RunFor") %>' />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Run For (Hrs)
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cmbRunEvery" runat="server" CssClass="Dropdown_Small">
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="hdnRunAt" Value='<%# Bind("RunEvery") %>' />
                                        <asp:HiddenField runat="server" ID="hdnScheduleId" Value='<%# Bind("ScheduleId") %>' />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Run Frequency
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label Text='<%# Bind("RunTime") %>' runat="server" ID="lblRunTime"></asp:Label>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Run Time
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnNotificationList" Value='<%# Bind("NotificationList") %>'
                                            runat="server" />
                                        <asp:TextBox ID="txtNotificationList" TextMode="multiLine" Rows="2" Text='<%# Bind("NotificationList") %>'
                                            runat="server" MaxLength="1024" Style="width: 200px"></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Notification List
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="cmbStatus" runat="server" CssClass="Dropdown_Small" Style="width: 150px">
                                        </asp:DropDownList>
                                        <asp:HiddenField runat="server" ID="hdnStatus" Value='<%# Bind("Status") %>' />
                                    </ItemTemplate>
                                    <HeaderTemplate>
                                        Status
                                    </HeaderTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="PercentageCompleted" HeaderText="Percentage Completed"
                                    ItemStyle-Width="50px" ItemStyle-HorizontalAlign="right" />
                                <asp:BoundField DataField="CompletedRateCount" HeaderText="Completed Travel Band Sets"
                                    ItemStyle-HorizontalAlign="right" />
                                <asp:BoundField DataField="TotalRateCount" HeaderText="Total Travel Band Sets" ItemStyle-HorizontalAlign="right" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnStop" Text="Pause Running Schedule" runat="server" CssClass="Button_Standard Button_Pause"
                            Style="width: 175px; float: right" />
                        <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="Button_Standard Button_Save"
                            Style="float: right" />
                    </td>
                </tr>
            </table>
            <br />
            <table class="dataTable" width="1100px" style="left: -150px; position: relative">
                <tr>
                    <th>
                        Database status for "<% =ServerName%>"
                    </th>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="grdDBStatus" CssClass="dataTableGrid" AutoGenerateColumns="false"
                            runat="server" Width="100%">
                            <AlternatingRowStyle CssClass="oddRow" />
                            <Columns>
                                <asp:BoundField DataField="AdbsDriveLetter" HeaderText="Drive" ItemStyle-Width="25px" />
                                <asp:BoundField DataField="AdbsDriveLabel" HeaderText="Label" ItemStyle-Width="150px" />
                                <asp:BoundField DataField="AdbsFreeSpaceMB" HeaderText="Free Space (MB)" ItemStyle-HorizontalAlign="right"
                                    ItemStyle-Width="100px" DataFormatString="{0:N0}" />
                                <asp:BoundField DataField="AdbsUsedSpaceMB" HeaderText="Used Space (MB)" ItemStyle-HorizontalAlign="right"
                                    ItemStyle-Width="100px" DataFormatString="{0:N0}" />
                                <asp:BoundField DataField="AdbsTotalSpaceMB" HeaderText="Total Space (MB)" ItemStyle-HorizontalAlign="right"
                                    ItemStyle-Width="100px" DataFormatString="{0:N0}" />
                                <asp:BoundField DataField="AdbsPercentageFreeSpace" HeaderText="Free Space %" ItemStyle-HorizontalAlign="right"
                                    ItemStyle-Width="75px" DataFormatString="{0:N} %" />
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
