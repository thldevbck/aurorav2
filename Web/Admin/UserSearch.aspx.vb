Imports System.Collections.Generic
Imports System.Web.Script.Serialization

Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.UserDataSet

Partial Class Admin_UserSearch
    Inherits AuroraPage

    Public Class LocationItem
        Public Text As String
        Public Value As String
        Public ComCode As String
        Public CtyCode As String
    End Class

    Public Overrides ReadOnly Property FunctionCode() As String
        Get
            If Request("funCode") = AuroraFunctionCodeAttribute.B2BUserMaintenance Then
                Return AuroraFunctionCodeAttribute.B2BUserMaintenance
            Else
                Return AuroraFunctionCodeAttribute.UserMaintenance
            End If
        End Get
    End Property

    Public ReadOnly Property IsB2B() As Boolean
        Get
            Return FunctionCode = AuroraFunctionCodeAttribute.B2BUserMaintenance
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If IsB2B Then
                Return "B2B Agent User Search"
            Else
                Return "User Search"
            End If
        End Get
    End Property

    Private _locationItemsString As String = ""
    Public ReadOnly Property LocationItemsString() As String
        Get
            Return _locationItemsString
        End Get
    End Property

    Private _userDataSet As New UserDataSet()

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _userDataSet.EnforceConstraints = False

        DataRepository.GetUserLookups(_userDataSet)

        companyDropDown.Items.Clear()
        companyDropDown.Items.Add(New ListItem("(All)", ""))
        For Each companyRow As CompanyRow In _userDataSet.Company
            companyDropDown.Items.Add(New ListItem(companyRow.ComCode & " - " & companyRow.ComName, companyRow.ComCode))
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem("(All)", ""))
        For Each countryRow As CountryRow In _userDataSet.Country
            Dim li As ListItem = New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode)
            countryDropDown.Items.Add(li)
        Next

        Dim locationItemList As New List(Of LocationItem)
        locationDropDown.Items.Clear()
        locationDropDown.Items.Add(New ListItem("(All)", ""))
        For Each locationRow As LocationRow In _userDataSet.Location
            Dim li As ListItem = New ListItem(locationRow.LocCode & " - " & locationRow.LocName, locationRow.LocCode)
            locationDropDown.Items.Add(li)

            Dim locationItem As New LocationItem()
            locationItem.Text = locationRow.LocCode & " - " & locationRow.LocName
            locationItem.Value = locationRow.LocCode
            locationItem.CtyCode = locationRow.CtyCode
            locationItem.ComCode = locationRow.CompanyRow.ComCode
            locationItemList.Add(locationItem)
        Next

        Dim js As New JavaScriptSerializer()
        _locationItemsString = js.Serialize(locationItemList)
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = "" & CookieValue("User_usrCode")
        nameTextBox.Text = "" & CookieValue("User_usrName")

        Try
            If Not String.IsNullOrEmpty(CookieValue("User_usrComCode")) Then
                companyDropDown.SelectedValue = CookieValue("User_usrComCode")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("User_usrCtyCode")) Then
                countryDropDown.SelectedValue = CookieValue("User_usrCtyCode")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("User_usrLocCode")) Then
                locationDropDown.SelectedValue = CookieValue("User_usrLocCode")
            End If
        Catch
        End Try

        If Not String.IsNullOrEmpty(CookieValue("User_usrIsActive")) Then
            If CBool(CookieValue("User_usrIsActive")) Then
                statusRadioButtonList.SelectedIndex = 1
            Else
                statusRadioButtonList.SelectedIndex = 2
            End If
        End If

        If IsB2B Then
            typeRadioButtonList.SelectedIndex = 2
        Else
            typeRadioButtonList.SelectedIndex = 0
            If Not String.IsNullOrEmpty(CookieValue("User_usrType")) Then
                If CBool(CookieValue("User_usrType")) Then
                    typeRadioButtonList.SelectedIndex = 1
                Else
                    typeRadioButtonList.SelectedIndex = 2
                End If
            End If
        End If

        'b2b change
        agentCodeTextBox.Text = "" & CookieValue("User_AgentCode")
        'b2b change

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        createButton.Visible = True

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Me.SearchParam) Then
                codeTextBox.Text = Me.SearchParam
            Else
                BuildForm(False)
            End If

            If HasSearchParameters() Then searchButton_Click(Me, Nothing)
        End If

        typeRadioButtonList.Enabled = Not Me.IsB2B
    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("User_usrCode") = codeTextBox.Text.Trim()
        CookieValue("User_usrName") = nameTextBox.Text.Trim()
        CookieValue("User_usrComCode") = companyDropDown.SelectedValue
        CookieValue("User_usrCtyCode") = countryDropDown.SelectedValue
        CookieValue("User_usrLocCode") = locationDropDown.SelectedValue
        If statusRadioButtonList.SelectedIndex = 1 Then
            CookieValue("User_usrIsActive") = "True"
        ElseIf statusRadioButtonList.SelectedIndex = 2 Then
            CookieValue("User_usrIsActive") = "False"
        Else
            CookieValue("User_usrIsActive") = ""
        End If
        If typeRadioButtonList.SelectedIndex = 1 Then
            CookieValue("User_usrType") = "True"
        ElseIf typeRadioButtonList.SelectedIndex = 2 Then
            CookieValue("User_usrType") = "False"
        Else
            CookieValue("User_usrType") = ""
        End If

        'b2b change
        CookieValue("User_AgentCode") = agentCodeTextBox.Text.Trim()

        'b2b change
    End Sub

    Public Function HasSearchParameters() As Boolean
        Return codeTextBox.Text.Trim().Length > 0 _
            OrElse nameTextBox.Text.Trim().Length > 0 _
            OrElse companyDropDown.SelectedIndex > 0 _
            OrElse countryDropDown.SelectedIndex > 0 _
            OrElse locationDropDown.SelectedIndex > 0 _
            OrElse statusRadioButtonList.SelectedIndex > 0 _
            OrElse ((Me.FunctionCode <> AuroraFunctionCodeAttribute.B2BUserMaintenance) AndAlso (typeRadioButtonList.SelectedIndex > 0)) _
            OrElse txtEmail.Text.Trim().Length > 0 _
            OrElse agentCodeTextBox.Text.Trim().Length > 0 _
            OrElse False
    End Function

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        If Not HasSearchParameters() Then
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "Enter search parameters")
            searchResultLabel.Text = ""
            searchResultTable.Visible = False
            Return
        End If

        UpdateCookieValues()

        Dim usrIsActive As New Nullable(Of Boolean)
        If statusRadioButtonList.SelectedIndex = 1 Then
            usrIsActive = New Nullable(Of Boolean)(True)
        ElseIf statusRadioButtonList.SelectedIndex = 2 Then
            usrIsActive = New Nullable(Of Boolean)(False)
        End If

        Dim usrIsAgentUser As New Nullable(Of Boolean)
        If Me.IsB2B Then
            usrIsAgentUser = True
        Else
            If typeRadioButtonList.SelectedIndex = 1 Then
                usrIsAgentUser = New Nullable(Of Boolean)(False)
            ElseIf typeRadioButtonList.SelectedIndex = 2 Then
                usrIsAgentUser = New Nullable(Of Boolean)(True)
            End If
        End If

        DataRepository.SearchUser(_userDataSet, _
            codeTextBox.Text.Trim(), _
            nameTextBox.Text.Trim(), _
            companyDropDown.SelectedValue, _
            countryDropDown.SelectedValue, _
            locationDropDown.SelectedValue, _
            usrIsActive, _
            usrIsAgentUser, _
            txtEmail.Text.Trim(), _
            agentCodeTextBox.Text.Trim())

        While searchResultTable.Rows.Count > 1
            searchResultTable.Rows.RemoveAt(1)
        End While

        If _userDataSet.UserInfo.Rows.Count = 0 Then
            searchResultLabel.Text = "No users found"
            searchResultTable.Visible = False
        Else
            If _userDataSet.UserInfo.Rows.Count = 1 Then
                searchResultLabel.Text = _userDataSet.UserInfo.Rows.Count.ToString() & " user found"
            Else
                searchResultLabel.Text = _userDataSet.UserInfo.Rows.Count.ToString() & " users found"
            End If
            searchResultTable.Visible = True

            Dim index As Integer = 0
            For Each userInfoRow As UserInfoRow In _userDataSet.UserInfo
                Dim tableRow As New TableRow()
                tableRow.BackColor = userInfoRow.StatusColor()
                searchResultTable.Rows.Add(tableRow)

                Dim nameCell As New TableCell()
                Dim nameHyperLink As New HyperLink()
                nameHyperLink.Text = Server.HtmlEncode(userInfoRow.Description)
                nameHyperLink.NavigateUrl = "UserEdit.aspx?FunCode=" & Server.UrlEncode(Me.FunctionCode) & "&UsrId=" & Server.UrlEncode(userInfoRow.UsrId)
                nameCell.Controls.Add(nameHyperLink)
                tableRow.Controls.Add(nameCell)

                Dim companyCell As New TableCell()
                If userInfoRow.CompanyRow IsNot Nothing Then
                    companyCell.Text = Server.HtmlEncode(userInfoRow.CompanyRow.ComCode & " - " & userInfoRow.CompanyRow.ComName)
                Else
                    companyCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(companyCell)

                Dim countryCell As New TableCell()
                If userInfoRow.CountryRow IsNot Nothing Then
                    countryCell.Text = Server.HtmlEncode(userInfoRow.CountryRow.CtyCode & " - " & userInfoRow.CountryRow.CtyName)
                Else
                    countryCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(countryCell)

                Dim locationCell As New TableCell()
                If userInfoRow.LocationRow IsNot Nothing Then
                    locationCell.Text = Server.HtmlEncode(userInfoRow.LocationRow.LocCode & " - " & userInfoRow.LocationRow.LocName)
                Else
                    locationCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(locationCell)

                Dim auroraCell As New TableCell()
                If userInfoRow.GetUserRoleRows().Length > 0 OrElse userInfoRow.IsUsrIsAgentUserNull OrElse Not userInfoRow.UsrIsAgentUser Then
                    auroraCell.Text = "Yes"
                Else
                    auroraCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(auroraCell)

                Dim b2bCell As New TableCell()
                If Not userInfoRow.IsUsrIsAgentSuperUserNull AndAlso userInfoRow.UsrIsAgentSuperUser Then
                    b2bCell.Text = "Admin"
                ElseIf Not userInfoRow.IsUsrIsAgentUserNull AndAlso userInfoRow.UsrIsAgentUser Then
                    b2bCell.Text = "User"
                Else
                    b2bCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(b2bCell)

                Dim statusCell As New TableCell()
                If Not userInfoRow.IsUsrIsActiveNull Then
                    statusCell.Text = IIf(userInfoRow.UsrIsActive, "Active", "Inactive")
                Else
                    statusCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(statusCell)

                index += 1
            Next
        End If

    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        UpdateCookieValues()

        Response.Redirect("UserEdit.aspx?FunCode=" & Server.UrlEncode(Me.FunctionCode))
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        codeTextBox.Text = ""
        nameTextBox.Text = ""
        companyDropDown.SelectedIndex = 0
        countryDropDown.SelectedIndex = 0
        locationDropDown.SelectedIndex = 0
        statusRadioButtonList.SelectedIndex = 0
        typeRadioButtonList.SelectedIndex = IIf(IsB2B, 2, 0)
        searchResultTable.Visible = False
        searchResultLabel.Text = ""

        UpdateCookieValues()
    End Sub
End Class
