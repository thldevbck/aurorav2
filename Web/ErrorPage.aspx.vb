
<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class ErrorPage
    Inherits AuroraPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        AddErrorMessage(Request.QueryString("errorMessage").ToString())
    End Sub
End Class
