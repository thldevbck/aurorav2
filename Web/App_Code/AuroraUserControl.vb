Imports Microsoft.VisualBasic

Public Class AuroraUserControl
    Inherits System.Web.UI.UserControl

    Public ReadOnly Property CurrentPage() As AuroraPage
        Get
            Return CType(Me.Page, AuroraPage)
        End Get
    End Property

    Public Sub RequiredChecker(ByVal sender As Object, ByVal ControlType As String)
        If Not Page.IsPostBack Then Exit Sub

        Try

            Dim page_name As String = System.IO.Path.GetFileName(Me.Request.PhysicalPath).ToLower()
            Dim completepath As String = Me.Request.PhysicalApplicationPath + "UserControls\Mappers\"
            Dim objectType As TextBox = Nothing

            If System.IO.File.Exists(completepath & page_name & ".xml") Then
                Dim postback As Boolean = Me.IsPostBack
                Dim xmlmapper As New System.Xml.XmlDocument
                xmlmapper.Load(Server.MapPath("~/UserControls/mappers/" & page_name & ".xml"))
                If xmlmapper.SelectSingleNode("Pages/Page/Name").InnerXml.ToLower.Equals(page_name) Then
                    Dim nodes As System.Xml.XmlNodeList = xmlmapper.SelectNodes("Pages/Page/Controls/" & ControlType & "/ControlName")
                    For Each node As System.Xml.XmlNode In nodes
                        If node.InnerText.ToLower.Equals(sender.id.ToString.ToLower) Then

                            ControlType = ControlType.Replace("usercontrols_", "")
                            ControlType = ControlType.Replace("_ascx", "")

                            If ControlType.Contains("_") = True Then
                                ControlType = ControlType.Substring(ControlType.IndexOf("_") + 1)
                            End If


                            If ControlType.Contains("control") = True Then
                                ControlType = ControlType.Replace("control", "")
                                objectType = sender.findcontrol(ControlType & "TextBox")
                            Else
                                objectType = Nothing
                            End If

                            If Not objectType Is Nothing Then
                                If TypeOf objectType Is TextBox Then
                                    If String.IsNullOrEmpty(objectType.Text) Then
                                        ''objectType.BackColor = System.Drawing.Color.Yellow 
                                        objectType.BackColor = Aurora.Common.DataConstants.RequiredColor
                                    Else
                                        objectType.BackColor = Drawing.Color.White
                                    End If
                                    Exit For
                                End If
                            End If
                            
                        End If
                    Next
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub AuroraUserControl_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        RequiredChecker(sender, sender.GetType.Name)
    End Sub
End Class
