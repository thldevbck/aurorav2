﻿Option Strict On
Option Explicit On

Imports Microsoft.VisualBasic

Imports System.Configuration
Imports System.Web.Configuration

Namespace Aurora.Configuration.Package
    Public Class PackageConfiguration
        Inherits ConfigurationSection

        Private Shared mSettings As PackageConfiguration = DirectCast(WebConfigurationManager.GetSection("packageConfiguration"), PackageConfiguration)

        Public Shared ReadOnly Property Settings As PackageConfiguration
            Get
                Return mSettings
            End Get
        End Property


        <ConfigurationProperty("promoTileLink", IsRequired:=True)> _
        Public ReadOnly Property PromoTileLink As PromoLinkElement
            Get
                Return DirectCast(Me("promoTileLink"), PromoLinkElement)
            End Get
        End Property

        Public Sub New()

        End Sub
    End Class

    Public Class PromoLinkElement
        Inherits ConfigurationElement

        Public Sub New()

        End Sub

        <ConfigurationProperty("url", IsRequired:=True)> _
        Public Property Url As String
            Get
                Return DirectCast(Me("url"), String)
            End Get
            Set(ByVal value As String)
                Me("url") = value
            End Set
        End Property

    End Class
End Namespace

