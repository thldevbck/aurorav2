'' Change Log 
'' 22.8.8 - Shoel - Fix for squish # 582, Method moved here
'' 27.8.8 - Shoel - Added Enum for ThriftyHistory Ctrl - Modified for THRIFTY HISTORY

Imports Microsoft.VisualBasic

Public Enum BookingTabIndex
    CheckOutIn = 0
    ChangeOver = 1
    Extension = 2
    Confirmation = 3
    Complaints = 4
    Cancel = 5
    Vehicle = 6
    Notes = 7
    Customer = 8
    Products = 9
    Payments = 10
    Summary = 11
    Infringements = 12
    Incidents = 13
    History = 14
    Files = 15

    ''rev:mia 24Sept2015 - Experience Tab
    BookingExperience = 16

    ' Added for Thrifty History
    ThriftyHistory = 17
    ' Added for Thrifty History

    
End Enum

Public Class BookingUserControl
    Inherits AuroraUserControl

    Private _bookingId As String
    Public Property BookingId() As String
        Get
            Return _bookingId
        End Get
        Set(ByVal value As String)
            _bookingId = value
        End Set
    End Property

    Private _bookingNumber As String
    Public Property BookingNumber() As String
        Get
            Return _bookingNumber
        End Get
        Set(ByVal value As String)
            _bookingNumber = value
        End Set
    End Property


    Private _rentalId As String
    Public Property RentalId() As String
        Get
            Return _rentalId
        End Get
        Set(ByVal value As String)
            _rentalId = value
        End Set
    End Property


    Private _rentalNo As String
    Public Property RentalNo() As String
        Get
            Return _rentalNo
        End Get
        Set(ByVal value As String)
            _rentalNo = value
        End Set
    End Property

    Private _isFromSearch As String
    Public Property IsFromSearch() As String
        Get
            Return _isFromSearch
        End Get
        Set(ByVal value As String)
            _isFromSearch = value
        End Set
    End Property

    Public Sub RefreshRentalGrids()
        Dim args As CommandEventArgs = New CommandEventArgs("Rebind", String.Empty)
        RaiseBubbleEvent(Nothing, args)
    End Sub

End Class
