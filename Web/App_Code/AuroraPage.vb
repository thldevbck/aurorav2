Imports System.Xml
Imports System.Web.Script.Serialization
Imports System.Collections.Generic

Imports Aurora.Common

Public Class AuroraPage
    Inherits System.Web.UI.Page

#Region "User Properties"
    Public ReadOnly Property UserId() As String
        Get
            Return UserSettings.Current.UsrId
        End Get
    End Property

    Public ReadOnly Property UserCode() As String
        Get
            Return UserSettings.Current.UsrCode
        End Get
    End Property

    Public ReadOnly Property UserName() As String
        Get
            Return UserSettings.Current.UsrName
        End Get
    End Property

    Public ReadOnly Property CompanyCode() As String
        Get
            Return UserSettings.Current.ComCode
        End Get
    End Property

    Public ReadOnly Property CompanyName() As String
        Get
            Return UserSettings.Current.ComName
        End Get
    End Property

    Public ReadOnly Property CountryCode() As String
        Get
            Return UserSettings.Current.CtyCode
        End Get
    End Property

    Public ReadOnly Property CountryName() As String
        Get
            Return UserSettings.Current.CtyName
        End Get
    End Property

    Public ReadOnly Property PrgmName() As String
        Get
            Return UserSettings.Current.PrgmName
        End Get
    End Property
#End Region

#Region "Function Properties"

    Public Overridable ReadOnly Property FunctionCode() As String
        Get
            Dim auroraFunctionCodeAttribute As AuroraFunctionCodeAttribute = CType(Attribute.GetCustomAttribute(Me.Page.GetType(), GetType(AuroraFunctionCodeAttribute)), AuroraFunctionCodeAttribute)
            Return auroraFunctionCodeAttribute.Code
        End Get
    End Property

    Private _functionDoc As XmlDocument
    Public ReadOnly Property FunctionDoc() As XmlDocument
        Get
            Return _functionDoc
        End Get
    End Property

    Private _functionId As String
    Public ReadOnly Property FunctionId() As String
        Get
            Return _functionId
        End Get
    End Property

    Private _functionTitle As String
    Public ReadOnly Property FunctionTitle() As String
        Get
            Return _functionTitle
        End Get
    End Property

    Private _functionFileName As String
    Public ReadOnly Property FunctionFileName() As String
        Get
            Return _functionFileName
        End Get
    End Property

#End Region

#Region "Menu Properties"
    Public Overridable ReadOnly Property MenuFunctionCode() As String
        Get
            Dim auroraMenuFunctionCodeAttribute As AuroraMenuFunctionCodeAttribute = CType(Attribute.GetCustomAttribute(Me.Page.GetType(), GetType(AuroraMenuFunctionCodeAttribute)), AuroraMenuFunctionCodeAttribute)
            If auroraMenuFunctionCodeAttribute IsNot Nothing Then
                Return auroraMenuFunctionCodeAttribute.Code
            Else
                Return FunctionCode
            End If
        End Get
    End Property

    Private _menuFunctionId As String
    Public ReadOnly Property MenuFunctionId() As String
        Get
            Return _menuFunctionId
        End Get
    End Property

    Private _menuFunctionTitle As String
    Public ReadOnly Property MenuFunctionTitle() As String
        Get
            Return _menuFunctionTitle
        End Get
    End Property

    Private _menuFunctionFileName As String
    Public ReadOnly Property MenuFunctionFileName() As String
        Get
            Return _menuFunctionFileName
        End Get
    End Property

    Private _menuFunctionDoc As XmlDocument
    Public ReadOnly Property MenuFunctionDoc() As XmlDocument
        Get
            Return _menuFunctionDoc
        End Get
    End Property

    Private _menuDoc As XmlDocument
    Public ReadOnly Property MenuDoc() As XmlDocument
        Get
            Return _menuDoc
        End Get
    End Property

    Private _auroraPageMenu As AuroraPageMenu
    Public ReadOnly Property AuroraPageMenu() As AuroraPageMenu
        Get
            Return _auroraPageMenu
        End Get
    End Property
#End Region

#Region "Page Properties"
    Public ReadOnly Property SearchParam() As String
        Get
            Return ("" & Request("sTxtSearch")).Trim()
        End Get
    End Property

    Public Overridable ReadOnly Property PageTitle() As String
        Get
            Dim auroraPageTitleAttribute As AuroraPageTitleAttribute = CType(Attribute.GetCustomAttribute(Me.Page.GetType(), GetType(AuroraPageTitleAttribute)), AuroraPageTitleAttribute)
            If Not auroraPageTitleAttribute Is Nothing Then
                Return auroraPageTitleAttribute.Title
            Else
                Return _functionTitle
            End If
        End Get
    End Property

    Public ReadOnly Property BackUrl() As String
        Get
            If Not String.IsNullOrEmpty(Me.Request.QueryString("BackUrl")) Then
                Return Me.Request.QueryString("BackUrl")
            ElseIf Not String.IsNullOrEmpty("" & Me.ViewState("BackUrl")) Then
                Return "" & Me.ViewState("BackUrl")
            Else
                Return Me.ResolveUrl("../" & UserSettings.Current.FunFileName)
            End If
        End Get
    End Property

    Public ReadOnly Property PageScriptManager() As ScriptManager
        Get
            Return Me.Master.FindControl("ScriptManager1")
        End Get
    End Property

    Public Sub SetValueLink(ByVal innerText As String, ByVal hRef As String)
        Dim titleLink As System.Web.UI.HtmlControls.HtmlAnchor
        titleLink = Me.Master.FindControl("titleLink")
        titleLink.HRef = hRef
        titleLink.InnerText = innerText
    End Sub

    Public ReadOnly Property HasHelp() As Boolean
        Get
            If System.IO.File.Exists(Me.Server.MapPath("~/Help/" & Me.FunctionCode & ".htm")) Then
                Return True
            ElseIf Me.MenuFunctionCode <> FunctionCode _
             AndAlso System.IO.File.Exists(Me.Server.MapPath("~/Help/" & Me.MenuFunctionCode & ".htm")) Then
            Else
                Return False
            End If
        End Get
    End Property

    Public ReadOnly Property HelpUrl() As String
        Get
            If System.IO.File.Exists(Me.Server.MapPath("~/Help/" & Me.FunctionCode & ".htm")) Then
                Return Me.ResolveUrl("~/Help/") & "?funCode=" & Server.UrlEncode(Me.FunctionCode)
            Else
                Return Me.ResolveUrl("~/Help/") & "?funCode=" & Server.UrlEncode(Me.MenuFunctionCode)
            End If
        End Get
    End Property

#End Region

#Region "Page Events"
    Protected Overrides Sub OnPreInit(ByVal e As System.EventArgs)
        MyBase.OnPreInit(e)

        If Not String.IsNullOrEmpty(Me.MasterPageFile) AndAlso Not String.IsNullOrEmpty(Request.QueryString("popup")) Then
            Me.MasterPageFile = "~/Include/PopupHeader.master"
        End If
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        Aurora.Common.UserSettings.Load(Request.ServerVariables("LOGON_USER").Replace("THL\", "").ToUpper())
        If String.IsNullOrEmpty(UserSettings.Current.UsrId) Then ErrorRedirect("Error retrieving user information.")

        ' Do a Security check if its not a "GENERAL" page, and gett he function information
        If Me.FunctionCode <> AuroraFunctionCodeAttribute.General Then
            ' get function information
            Dim hasPermission As Boolean
            hasPermission = GetFunctionPermission(Me.FunctionCode)
            If Not hasPermission Then
                Dim errorMessage As String = ""
                ErrorRedirect("Permission Denied, you do not have permission to access this function.")
            End If

            _functionDoc = Data.ExecuteSqlXmlSPDoc("sp_getFunctionId", FunctionCode)
            _functionId = _functionDoc.SelectSingleNode("/data/functions/FunId").InnerText
            _functionTitle = _functionDoc.SelectSingleNode("/data/functions/FunTitle").InnerText
            _functionFileName = _functionDoc.SelectSingleNode("/data/functions/FunFileName").InnerText
        End If

        ' Get the menu function information
        If MenuFunctionCode = FunctionCode Then
            _menuFunctionDoc = _functionDoc
            _menuFunctionId = _functionId
            _menuFunctionTitle = _functionTitle
            _menuFunctionFileName = _functionFileName
        Else
            _menuFunctionDoc = Data.ExecuteSqlXmlSPDoc("sp_getFunctionId", MenuFunctionCode)
            _menuFunctionId = _menuFunctionDoc.SelectSingleNode("/data/functions/FunId").InnerText
            _menuFunctionTitle = _menuFunctionDoc.SelectSingleNode("/data/functions/FunTitle").InnerText
            _menuFunctionFileName = _menuFunctionDoc.SelectSingleNode("/data/functions/FunFileName").InnerText
        End If

        ' Get the menu information
        _menuDoc = Data.ExecuteSqlXmlSPDocCorrecting("GETMenu", UserSettings.Current.UsrCode)
        _auroraPageMenu = New AuroraPageMenu(_menuDoc)

        ' Get the messages
        GetMsgToClient(Message)
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        If Not Page.IsPostBack _
         AndAlso (String.IsNullOrEmpty(Me.MasterPageFile) OrElse Me.ResolveUrl(Me.MasterPageFile).ToLower() = Me.ResolveUrl("~/Include/AuroraHeader.master").ToLower()) Then
            If Not String.IsNullOrEmpty(Me.CookieValue("CurrentPageUrl")) _
             AndAlso Me.CookieValue("CurrentPageUrl") <> Me.Request.RawUrl Then
                Me.ViewState("BackUrl") = Me.CookieValue("CurrentPageUrl")
            End If
            Me.CookieValue("CurrentPageUrl") = Me.Request.RawUrl
        End If

        MyBase.OnLoad(e)
    End Sub

#End Region

#Region "Client Messages"
    Public Overridable ReadOnly Property Message() As String
        Get
            Dim auroraMessageAttribute As AuroraMessageAttribute = CType(Attribute.GetCustomAttribute(Me.Page.GetType(), GetType(AuroraMessageAttribute)), AuroraMessageAttribute)
            If Not auroraMessageAttribute Is Nothing Then
                Return auroraMessageAttribute.Message
            Else
                Return ""
            End If
        End Get
    End Property

    Private _messageCollection As Collection
    Public Property MessageCollection() As Collection
        Get
            Return _messageCollection
        End Get
        Set(ByVal value As Collection)
            _messageCollection = value
        End Set
    End Property

    Public Sub GetMsgToClient(ByVal messageCode As String)
        If Not String.IsNullOrEmpty(messageCode) Then
            Dim errorMessageXmlDoc As XmlDocument = New XmlDocument
            MessageCollection = Aurora.Common.Data.GetMsgToClient(messageCode)
        End If
    End Sub

    Public Function GetMessage(ByVal errorCode As String, ByVal ParamArray params() As Object) As String
        ''rev:mia 09Aug issue #549 Customer Summary page - incorrect error message and column titles
        Dim errorMessage As String
        Dim i As Integer = 1

        Try
            errorMessage = MessageCollection.Item(errorCode)
            For Each o As String In params
                errorMessage = errorMessage.Replace("xxx" & Utility.ParseString(i), o)
                i = i + 1
            Next
            For j As Integer = 1 To 6
                errorMessage = errorMessage.Replace("xxx" & Utility.ParseString(j), "")
            Next j

            Return errorCode + " - " + errorMessage

        Catch argEx As IndexOutOfRangeException
            Return Server.HtmlDecode(errorCode.Replace("&apos;s", "'s"))

        Catch ex As Exception
            Return Server.HtmlDecode(errorCode.Replace("&apos;s", "'s"))
        End Try


    End Function

    ' Generate Message in JSON formate
    Public ReadOnly Property MessageJSON() As String
        Get
            Dim col As ListItemCollection = New ListItemCollection

            Dim message As String
            message = Me.Message
            Dim messageArray As String()
            messageArray = message.Split(",")

            Dim result As String
            For Each msg As String In messageArray
                Dim errorMessage As String = MessageCollection.Item(msg)

                Dim item As ListItem
                item = New ListItem(msg, errorMessage)
                col.Add(item)
            Next

            Dim js As New JavaScriptSerializer()
            result = "{'ErrorMessages': " & js.Serialize(col) & "}"

            Return result

        End Get
    End Property
#End Region

#Region "Information/Warning/Error Messages"
    Public Sub ClearMessages()
        CType(Me.Master, AuroraMasterPage).ClearMessages()
    End Sub

    Public Sub AddMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        CType(Me.Master, AuroraMasterPage).AddMessage(messageType, text)
    End Sub

    Public Sub AddInformationMessage(ByVal text As String)
        AddMessage(AuroraHeaderMessageType.Information, text)
    End Sub

    Public Sub AddErrorMessage(ByVal text As String)
        AddMessage(AuroraHeaderMessageType.Error, text)
    End Sub

    Public Sub AddWarningMessage(ByVal text As String)
        AddMessage(AuroraHeaderMessageType.Warning, text)
    End Sub

    Public Sub SetInformationMessage(ByVal text As String)
        ClearMessages() : AddMessage(AuroraHeaderMessageType.Information, text)
    End Sub

    Public Sub SetErrorMessage(ByVal text As String)
        ClearMessages() : AddMessage(AuroraHeaderMessageType.Error, text)
    End Sub

    Public Sub SetWarningMessage(ByVal text As String)
        ClearMessages() : AddMessage(AuroraHeaderMessageType.Warning, text)
    End Sub
#End Region

#Region "Information/Warning/Error Short Messages"
    Public Sub ClearShortMessage()
        CType(Me.Master, AuroraMasterPage).ClearShortMessage()
    End Sub

    Public Sub SetShortMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
        CType(Me.Master, AuroraMasterPage).SetShortMessage(messageType, text)
    End Sub

    Public Sub SetInformationShortMessage(ByVal text As String)
        SetShortMessage(AuroraHeaderMessageType.Information, text)
    End Sub

    Public Sub SetErrorShortMessage(ByVal text As String)
        SetShortMessage(AuroraHeaderMessageType.Error, text)
    End Sub

    Public Sub SetWarningShortMessage(ByVal text As String)
        SetShortMessage(AuroraHeaderMessageType.Warning, text)
    End Sub
#End Region

#Region "Logging"
    Public Sub LogError(ByVal errorInfo As String)
        Logging.LogError(FunctionCode, errorInfo)
    End Sub

    Public Sub LogException(ByVal ex As Exception)
        Logging.LogException(FunctionCode, ex)
    End Sub

    Public Sub LogWarning(ByVal description As String)
        Logging.LogWarning(FunctionCode, description)
    End Sub

    Public Sub LogInformation(ByVal description As String)
        Logging.LogInformation(FunctionCode, description)
    End Sub

    Public Sub LogDebug(ByVal description As String)
        Logging.LogDebug(FunctionCode, description)
    End Sub

#End Region

#Region "Utility"

    Private _functionPermissions As New Dictionary(Of String, Boolean)
    Public Function GetFunctionPermission(ByVal functionCode As String) As Boolean
        If String.IsNullOrEmpty(Me.UserCode) Then Throw New InvalidOperationException()
        If String.IsNullOrEmpty(functionCode) Then Throw New ArgumentNullException("FunctionCode")

        If _functionPermissions.ContainsKey(functionCode) Then
            Return _functionPermissions(functionCode)
        Else
            Dim result As Boolean = Data.GetFunctionPermission(Me.UserCode, functionCode)
            _functionPermissions.Add(functionCode, result)
            Return result
        End If
    End Function

    Public Property CookieValue(ByVal name As String)
        Get
            Dim cookie As HttpCookie = Request.Cookies(name)
            If cookie IsNot Nothing Then
                Return cookie.Value
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value)
            Dim cookie As New HttpCookie(name)
            cookie.Value = value
            Response.SetCookie(cookie)
        End Set
    End Property

    Public Sub ErrorRedirect(ByVal message As String)
        Dim errorPage As String = Me.ResolveUrl("~\ErrorPage.aspx")

        If System.IO.Path.GetFileName(Me.Request.PhysicalPath).ToLower() <> "errorpage.aspx" Then
            Response.Redirect(errorPage & "?errorMessage=" & System.Web.HttpUtility.UrlEncode(message))
        End If
    End Sub

#End Region

End Class
