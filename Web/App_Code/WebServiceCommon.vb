﻿Imports Microsoft.VisualBasic
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Xml.Xsl
Imports System.IO
Imports Aurora.Common.Logging
Imports Aurora.Common
Imports System.Xml

Namespace WS.Utility
    Public Class WebServiceCommon

        Public Shared Function GetBrandLogoImage(ByVal emailImg As String, ByVal sText As String) As String

            Dim newtext As String = ""
            If (Not String.IsNullOrEmpty(emailImg)) Then
                Try

                    emailImg = emailImg.Replace(" ", "").Replace("MOTORHOMESANDCARS.COM", "MAC").ToLower.Trim()
                    Dim getfirstChar As String = emailImg.Substring(0, 1).ToUpper
                    emailImg = emailImg.Remove(0, 1).Insert(0, getfirstChar)
                    If (emailImg.IndexOf("Explore") <> -1) Then
                        emailImg = "ExploreMore"
                    End If
                    Dim text As String = sText
                    newtext = text.Replace(String.Concat(emailImg, IIf(emailImg.IndexOf("Mighty") <> -1, ".gif", ".gif")), ConfigurationManager.AppSettings(emailImg & "LogoURL"))

                Catch ex As Exception
                    Return sText
                End Try

            End If
            Return newtext

        End Function

        Public Shared Function CreateMessage(EmailFrom As String, _
                                             EmailTo As String, _
                                             Subject As String, _
                                             Content As String, _
                                             CC As String, _
                                             Attachments As String) As Boolean

            Dim sb As New StringBuilder

            ' Create a message and set up the recipients.
            Dim message As New MailMessage(EmailFrom, EmailTo, Subject, Content)
            message.IsBodyHtml = True
            If (Not String.IsNullOrEmpty(CC)) Then
                Dim ccEmail As String = ""
                Dim arrayOfCC() As String = New String() {}
                If (CC.Contains(";") = True) Then
                    arrayOfCC = CC.Split(New String() {";"}, StringSplitOptions.RemoveEmptyEntries)
                ElseIf CC.Contains(",") = True Then
                    arrayOfCC = CC.Split(New String() {","}, StringSplitOptions.RemoveEmptyEntries)
                End If
                If (arrayOfCC.Length - 1 <> -1) Then
                    For Each ccEmail In arrayOfCC
                        Dim bcc As MailAddress = New MailAddress(ccEmail)
                        message.Bcc.Add(ccEmail)
                    Next
                Else
                    Dim bcc As MailAddress = New MailAddress(CC)
                    message.Bcc.Add(CC)
                End If
                

            End If
            If (Not String.IsNullOrEmpty(Attachments)) Then
                Dim data As New Attachment(Attachments, MediaTypeNames.Application.Octet)
                Dim disposition As ContentDisposition = data.ContentDisposition
                disposition.CreationDate = IO.File.GetCreationTime(Attachments)
                disposition.ModificationDate = IO.File.GetLastWriteTime(Attachments)
                disposition.ReadDate = IO.File.GetLastAccessTime(Attachments)
                message.Attachments.Add(data)
                message.IsBodyHtml = False
            End If

            message.BodyEncoding = Encoding.UTF8

            Dim client As New SmtpClient(CStr(ConfigurationManager.AppSettings("MailServerURL")))
            sb = New StringBuilder
            Try
                client.Send(message)
                Logging.LogInformation("CreateMessage", "Rental Confirmation  with a subject " & Subject & " sent Successfully to " & EmailTo)
            Catch ex As Exception
                Logging.LogError("CreateMessage", "Unable to send Rental Confirmatione-mail to/from : " & EmailFrom & " / " & EmailTo & " Error: " & ex.Message & vbCrLf & ex.StackTrace)
                Return False
            End Try

            Return True
        End Function

    End Class

End Namespace
