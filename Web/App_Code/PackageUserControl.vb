Imports Microsoft.VisualBasic
Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


Public Class PackageUserControl
    Inherits AuroraUserControl

    Public Const NewListItemText As String = "(new)"
    Public Const PkgIdParam As String = "pkgId"
    Public Const TabParam As String = "tab"

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.PackageMaintenance)
        End Get
    End Property

    Private _packageDataSet As PackageDataSet
    Public ReadOnly Property PackageDataSet() As PackageDataSet
        Get
            Return _packageDataSet
        End Get
    End Property

    Private _packageRow As PackageRow
    Public ReadOnly Property PackageRow() As PackageRow
        Get
            Return _packageRow
        End Get
    End Property

    Public Overridable Sub InitPackageUserControl(ByVal packageDataSet As PackageDataSet, ByVal packageRow As PackageRow)
        Me._packageDataSet = packageDataSet
        Me._packageRow = packageRow
    End Sub


    Private Shared Function AddUrlParam(ByVal page As Page, ByRef first As Boolean, ByVal paramName As String, ByVal paramValue As String) As String
        If String.IsNullOrEmpty(paramValue) Then Return ""
        Dim result As String = ""
        If first Then
            result &= "?"
            first = False
        Else
            result &= "&"
        End If
        result += paramName & "=" & page.Server.UrlEncode(paramValue)
        Return result
    End Function

    Public Shared Function MakePackageUrl( _
     ByVal page As Page, _
     Optional ByVal pkgId As String = Nothing, _
     Optional ByVal tabName As String = Nothing) As String
        Dim first As Boolean = True
        Return page.ResolveUrl("~/Package/Package.aspx") _
         & AddUrlParam(page, first, PkgIdParam, pkgId) _
         & AddUrlParam(page, first, TabParam, tabName)
    End Function


End Class
