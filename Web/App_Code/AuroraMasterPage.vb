Imports System.Web


Public Class AuroraMasterPage
    Inherits System.Web.UI.MasterPage

    Public Overridable Sub ClearMessages()
    End Sub

    Public Overridable Sub AddMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
    End Sub

    Public Overridable Sub ClearShortMessage()
    End Sub

    Public Overridable Sub SetShortMessage(ByVal messageType As AuroraHeaderMessageType, ByVal text As String)
    End Sub

End Class
