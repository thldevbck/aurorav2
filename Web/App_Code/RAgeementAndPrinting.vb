﻿Imports Microsoft.VisualBasic
Imports System.Web.Script.Serialization
Imports System.Net
Imports System.IO
Imports System.Runtime.InteropServices
Imports Aurora.Common.Logging
Imports System.Net.Security
Imports System.Security.Cryptography.X509Certificates
Imports Aurora.RestAPI.Token

Namespace WS.RentalAgreement

    Public Class TokenObject
        Private _access_token As String
        Public Property access_token As String
            Get
                Return _access_token
            End Get
            Set(value As String)
                _access_token = value
            End Set
        End Property

        Private _token_type As String
        Public Property token_type As String
            Get
                Return _token_type
            End Get
            Set(value As String)
                _token_type = value
            End Set
        End Property

        Private _expires_at As DateTime
        Public Property expires_at as datetime
            Get
                Return _expires_at
            End Get
            Set(value As DateTime)
                _expires_at = value
            End Set
        End Property
    End Class

    Public Class AmazonUrl
        Private _url As String
        Public Property url As String
            Get
                Return _url
            End Get
            Set(value As String)
                _url = value
            End Set
        End Property
    End Class

    Public Class RAgreement

        Private Shared Function GetTokenV2() As String
            Dim client_id As String = ConfigurationManager.AppSettings("client_id")
            Dim client_secret As String = ConfigurationManager.AppSettings("client_secret")
            Dim tokenUrl As String = ConfigurationManager.AppSettings("tokenUrl")
            Dim auroraUserName As String = ConfigurationManager.AppSettings("auroraUserName")
            Dim auroraPassword As String = ConfigurationManager.AppSettings("auroraPassword")
            Dim bodytext As String = String.Format("grant_type=password&username={0}&password={1}&client_id={2}&client_secret={3}", auroraUserName, auroraPassword, client_id, client_secret)
            Return UserToken.GetToken(tokenUrl, "POST", bodytext)
        End Function

        Public Shared Function PrintRentalAgreementV2(rentalId As String, raFilename As String) As String

            
            Dim urlAmazon As String = String.Empty

            If (Not String.IsNullOrEmpty(raFilename) And raFilename.IndexOf("ERROR") = -1) Then
                ''get token
                Dim tokenresult As String = GetTokenV2()
                If (tokenresult.IndexOf("ERROR") <> -1) Then
                    Return tokenresult
                End If
                Dim jsSerializer As JavaScriptSerializer = New JavaScriptSerializer
                Dim tokenObject As TokenObject = jsSerializer.Deserialize(Of TokenObject)(tokenresult)
                urlAmazon = GetrentalAgreementUrl(tokenObject.access_token, tokenObject.token_type, raFilename)
                Dim amazonUrl As AmazonUrl = jsSerializer.Deserialize(Of AmazonUrl)(urlAmazon)
                urlAmazon = amazonUrl.url
            Else
                If (raFilename.IndexOf("ERROR") <> -1) Then
                    Return "ERROR:" & raFilename
                End If
            End If

            Return urlAmazon

        End Function

        'Public Shared Function PrintRentalAgreement(rentalId As String, raFilename As String) As String

        '    ''get RA location
        '    ''Dim raFilename As String = GetRAFilename(rentalId)
        '    Dim urlAmazon As String = String.Empty

        '    If (Not String.IsNullOrEmpty(raFilename) And raFilename.IndexOf("ERROR") = -1) Then
        '        ''get token
        '        Dim tokenresult As String = GetToken()
        '        If (tokenresult.IndexOf("ERROR") <> -1) Then
        '            Return tokenresult
        '        End If
        '        Dim jsSerializer As JavaScriptSerializer = New JavaScriptSerializer
        '        Dim tokenObject As TokenObject = jsSerializer.Deserialize(Of TokenObject)(tokenresult)
        '        urlAmazon = GetrentalAgreementUrl(tokenObject.access_token, tokenObject.token_type, raFilename)
        '        Dim amazonUrl As AmazonUrl = jsSerializer.Deserialize(Of AmazonUrl)(urlAmazon)
        '        urlAmazon = amazonUrl.url
        '    Else
        '        If (raFilename.IndexOf("ERROR") <> -1) Then
        '            Return "ERROR:" & raFilename
        '        End If
        '    End If

        '    Return urlAmazon

        'End Function

        Private Shared Function GetrentalAgreementUrl(access_token As String, token_type As String, urlRA As String) As String
            Dim amazonUrl As String = ConfigurationManager.AppSettings("amazonUrl")
            Dim wr As HttpWebRequest = CallRegisterAPIjson(amazonUrl, "url=" & urlRA, "POST", access_token, token_type)
            Return GetResponse(wr)
        End Function

        ''rev:mia 17july2015 - AWA-3 Investigate Authentication module
        Public Shared Function GetToken(auroraUserName As String, auroraPassword As String) As String
            Return GetTokenV2()
        End Function

        Private Shared Function GetToken() As String
            Dim client_id As String = ConfigurationManager.AppSettings("client_id")
            Dim client_secret As String = ConfigurationManager.AppSettings("client_secret")
            Dim tokenUrl As String = ConfigurationManager.AppSettings("tokenUrl")
            Dim auroraUserName As String = ConfigurationManager.AppSettings("auroraUserName")
            Dim auroraPassword As String = ConfigurationManager.AppSettings("auroraPassword")


            Dim bodytext As String = String.Format("grant_type=password&username={0}&password={1}&client_id={2}&client_secret={3}", auroraUserName, auroraPassword, client_id, client_secret)
            Dim wr As HttpWebRequest = CallRegisterAPIjson(tokenUrl, bodytext, "POST")
            Return GetResponse(wr)
        End Function

        Private Shared Function SetProxyAndCredentials() As IWebProxy

            ServicePointManager.ServerCertificateValidationCallback = New System.Net.Security.RemoteCertificateValidationCallback(AddressOf customCertValidation)
            Dim apiProxy As String = ConfigurationManager.AppSettings("apiProxy")
            Dim apiProxyNo As Integer = IIf(String.IsNullOrEmpty(apiProxy), 3128, CInt(apiProxy))
            Dim proxy As IWebProxy = New WebProxy("proxy", apiProxyNo)
            ' port number is of type integer 
            Dim credentials As ICredentials = CredentialCache.DefaultCredentials ''CredentialCache.DefaultNetworkCredentials

            proxy.Credentials = credentials
            Return proxy
        End Function

        Private Shared Function BuildBodyRequest(url As String, data As String, proxy As IWebProxy, method As String, Optional access_token As String = "", Optional token_type As String = "") As HttpWebRequest
            Dim request As HttpWebRequest = Nothing

            Try


                request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                request.Method = method
                request.ContentType = "application/x-www-form-urlencoded"
                request.ContentLength = data.Length
                request.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1"
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
                request.UseDefaultCredentials = True


                If (Not String.IsNullOrEmpty(access_token) And Not String.IsNullOrEmpty(token_type)) Then
                    request.Headers.Add("Authorization", String.Concat(token_type, " ", access_token))
                End If

                Dim writer As New StreamWriter(request.GetRequestStream())
                writer.Write(data)
                writer.Close()

            Catch ex As Exception
                LogInformation("BuildBodyRequest: ", url & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
            End Try
            Return request
        End Function

        Private Shared Function CallRegisterAPIjson(url As String, data As Object, method As String, Optional access_token As String = "", Optional token_type As String = "") As HttpWebRequest
            Dim proxy As IWebProxy = SetProxyAndCredentials()
            Dim request As HttpWebRequest = BuildBodyRequest(url, DirectCast(data, String), proxy, method, access_token, token_type)
            Return request
        End Function

        Private Shared Function GetResponse(request As HttpWebRequest) As String
            Dim responseFromServer As String = ""
            Try
                If (request Is Nothing) Then
                    Throw New Exception("Unable to connect to the remote server")
                End If

                Dim response As WebResponse = request.GetResponse()
                Dim dataStream As Stream = response.GetResponseStream()
                Dim reader As New StreamReader(dataStream)

                responseFromServer = reader.ReadToEnd()
                ' Clean up the streams.
                reader.Close()
                dataStream.Close()

                response.Close()
            Catch ex As Exception
                responseFromServer = "ERROR: " + ex.Message
            End Try

            Return responseFromServer

        End Function

        Private Shared Function customCertValidation(ByVal sender As Object, ByVal cert As X509Certificate, ByVal chain As X509Chain, ByVal errors As SslPolicyErrors) As Boolean
            Return True
        End Function

        
    End Class
End Namespace

Namespace WS.RAPrinter

    Public Class RAPrinterSelection
        ' ----- Define the data type that supplies basic
        '       print job information to the spooler.
        <StructLayout(LayoutKind.Sequential, _
            CharSet:=CharSet.Unicode)> _
        Public Structure DOCINFO
            <MarshalAs(UnmanagedType.LPWStr)> _
            Public pDocName As String
            <MarshalAs(UnmanagedType.LPWStr)> _
            Public pOutputFile As String
            <MarshalAs(UnmanagedType.LPWStr)> _
            Public pDataType As String
        End Structure

        ' ----- Define interfaces to the functions supplied
        '       in the DLL.
        <DllImport("winspool.drv", EntryPoint:="OpenPrinterW", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function OpenPrinter( _
       ByVal printerName As String, ByRef hPrinter As IntPtr, _
       ByVal printerDefaults As Integer) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="ClosePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function ClosePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="StartDocPrinterW", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function StartDocPrinter( _
       ByVal hPrinter As IntPtr, ByVal level As Integer, _
       ByRef documentInfo As DOCINFO) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="EndDocPrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function EndDocPrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="StartPagePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function StartPagePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="EndPagePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function EndPagePrinter( _
       ByVal hPrinter As IntPtr) As Boolean
        End Function

        <DllImport("winspool.drv", EntryPoint:="WritePrinter", _
           SetLastError:=True, CharSet:=CharSet.Unicode, _
           ExactSpelling:=True, _
           CallingConvention:=CallingConvention.StdCall)> _
        Public Shared Function WritePrinter( _
       ByVal hPrinter As IntPtr, ByVal buffer As IntPtr, _
       ByVal bufferLength As Integer, _
       ByRef bytesWritten As Integer) As Boolean
        End Function

        Private Declare Function ShellExecute Lib "shell32.dll" Alias "ShellExecuteA" (ByVal hwnd As Long, _
        ByVal lpOperation As String, ByVal lpFile As String, ByVal lpParameters As String, ByVal lpDirectory As String, _
        ByVal nShowCmd As Long) As Long


        Public Shared Function IsValidPrinter(printerpath As String) As Boolean
            Dim online As Boolean = False
            Try

                Dim printDocument As New Drawing.Printing.PrintDocument()
                printDocument.PrinterSettings.PrinterName = printerpath
                online = printDocument.PrinterSettings.IsValid
            Catch
                online = False
            End Try
            Return online
        End Function

        Public Shared Function SendPDFToPrinter(path As String, ByVal PrinterName As String, rentalIdFilename As String) As Boolean

            Dim tempRentalPath As String = String.Concat(GetPDFTemporaryStorage, rentalIdFilename, ".pdf")
            If (DownloadFile(path, tempRentalPath)) Then

                If (File.Exists(tempRentalPath)) Then
                    Dim result As Boolean = SendFileToPrinter(PrinterName, tempRentalPath)
                    If (result) Then
                        DeleteFile(tempRentalPath)
                    End If
                Else
                    LogInformation("SendPDFToPrinter", tempRentalPath & " is missing")
                    Return False
                End If
            End If

            Return True
        End Function

        Private Shared ReadOnly Property GetPDFTemporaryStorage As String
            Get
                Return ConfigurationManager.AppSettings("tempRentalPath")
            End Get
        End Property

        Private Shared Function DownloadFile(path As String, filename As String) As Boolean
            Try
                Dim download As New WebClient
                download.DownloadFile(New Uri(path), filename)
                download = Nothing
            Catch ex As Exception
                LogInformation("DownloadFile".ToUpper, ex.Message & ", " & ex.StackTrace)
                Return False
            End Try
            Return True
        End Function

        Private Shared Sub DeleteFile(path As String)
            If (File.Exists(path)) Then
                File.Delete(path)
            End If
        End Sub

        Private Shared Function SendBytesToPrinter(ByVal szPrinterName As String, ByVal pBytes As IntPtr, ByVal dwCount As Int32) As Boolean
            Dim hPrinter As IntPtr      ' The printer handle.
            Dim dwError As Int32        ' Last error - in case there was trouble.
            Dim di As DOCINFO          ' Describes your document (name, port, data type).
            Dim dwWritten As Int32      ' The number of bytes written by WritePrinter().
            Dim bSuccess As Boolean     ' Your success code.

            ' Set up the DOCINFO structure.
            With di
                .pDocName = "Rental Agreement"
                .pDataType = "RAW"
            End With
            ' Assume failure unless you specifically succeed.
            bSuccess = False
            If OpenPrinter(szPrinterName, hPrinter, 0) Then
                If StartDocPrinter(hPrinter, 1, di) Then
                    If StartPagePrinter(hPrinter) Then
                        ' Write your printer-specific bytes to the printer.
                        bSuccess = WritePrinter(hPrinter, pBytes, dwCount, dwWritten)
                        EndPagePrinter(hPrinter)
                    End If
                    EndDocPrinter(hPrinter)
                End If
                ClosePrinter(hPrinter)
            End If
            ' If you did not succeed, GetLastError may give more information
            ' about why not.
            If bSuccess = False Then
                dwError = Marshal.GetLastWin32Error()
            End If
            Return bSuccess
        End Function ' SendBytesToPrinter()

        Public Shared Function SendFileToPrinter(ByVal szPrinterName As String, ByVal szFileName As String) As Boolean
            ' Open the file.
            Dim fs As New FileStream(szFileName, FileMode.Open)
            ' Create a BinaryReader on the file.
            Dim br As New BinaryReader(fs)
            ' Dim an array of bytes large enough to hold the file's contents.
            Dim bytes(fs.Length) As Byte
            Dim bSuccess As Boolean
            ' Your unmanaged pointer.
            Dim pUnmanagedBytes As IntPtr

            ' Read the contents of the file into the array.
            bytes = br.ReadBytes(fs.Length)
            ' Allocate some unmanaged memory for those bytes.
            pUnmanagedBytes = Marshal.AllocCoTaskMem(fs.Length)
            ' Copy the managed byte array into the unmanaged array.
            Marshal.Copy(bytes, 0, pUnmanagedBytes, fs.Length)
            ' Send the unmanaged bytes to the printer.
            bSuccess = SendBytesToPrinter(szPrinterName, pUnmanagedBytes, fs.Length)
            ' Free the unmanaged memory that you allocated earlier.
            Marshal.FreeCoTaskMem(pUnmanagedBytes)
            fs.Close()
            Return bSuccess
        End Function ' SendFileToPrinter()

#Region "Manny's Testing"

        Private Shared Function GetWebContent(url As String) As String
            Dim result As String = ""
            Try
                Dim myRequest As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)
                myRequest.Method = "GET"
                Dim myResponse As WebResponse = myRequest.GetResponse()
                Dim sr As New StreamReader(myResponse.GetResponseStream(), System.Text.Encoding.UTF8)
                result = sr.ReadToEnd()
                sr.Close()
                myResponse.Close()

            Catch ex As Exception
                Return "ERROR: " & ex.Message & "," & ex.StackTrace
            End Try

            Return result
        End Function

        Public Shared Function PrintRaw( _
            ByVal printerName As String, _
            ByVal origString As String) As Boolean
            Dim result As Boolean = True
            Dim hPrinter As IntPtr
            Dim spoolData As New DOCINFO
            Dim dataToSend As IntPtr
            Dim dataSize As Integer
            Dim bytesWritten As Integer

            ' ----- The internal format of a .NET String is just
            '       different enough from what the printer expects
            '       that there will be a problem if we send it
            '       directly. Convert it to ANSI format before
            '       sending.
            dataSize = origString.Length()
            dataToSend = Marshal.StringToCoTaskMemAnsi(origString)

            ' ----- Prepare information for the spooler.
            spoolData.pDocName = "Rental Agreement"
            spoolData.pDataType = "RAW"

            Try
                Call OpenPrinter(printerName, hPrinter, 0)

                ' ----- Start a new document and Section 1.1.
                Call StartDocPrinter(hPrinter, 1, spoolData)
                Call StartPagePrinter(hPrinter)

                ' ----- Send the data to the printer.
                Call WritePrinter(hPrinter, dataToSend, _
                   dataSize, bytesWritten)

                ' ----- Close everything that we opened.
                EndPagePrinter(hPrinter)
                EndDocPrinter(hPrinter)
                ClosePrinter(hPrinter)
            Catch ex As Exception
                ''Logging.LogError("WS.Printer PrintRaw", ex.Message & vbCrLf & ex.StackTrace)
                result = False
            Finally
                ' ----- Get rid of the special ANSI version.
                Marshal.FreeCoTaskMem(dataToSend)
            End Try
            Return result
        End Function

        Private Shared Function AppendHtmlStructure(content As String) As String
            Dim sb As New StringBuilder()
            sb.AppendFormat("{0}", "<html>")
            sb.AppendFormat("{0}", "<head>")
            sb.AppendFormat("{0}", "<title>Self-Checkin Rental Agreement</title>")
            sb.AppendFormat("{0}", "</head>")
            sb.AppendFormat("{0}", "<body>")
            sb.AppendFormat("{0}", content.Replace("<br>", ""))
            sb.AppendFormat("{0}", "</body>")
            sb.AppendFormat("{0}", "</html>")

            Dim replacethis As String = "xmlns:fo=""http://www.w3.org/1999/XSL/Format"""
            sb.Replace(replacethis, "") ''.Replace("<!DOCTYPE>", "")

            Return sb.ToString
        End Function

        Private Shared Sub SaveTempFile(content As String, rentalId As String, path As String)

            If (File.Exists(path)) Then
                File.Delete(path)
            End If
            Dim sw As System.IO.StreamWriter = System.IO.File.AppendText(path)
            Try
                sw.WriteLine(content)
            Catch ex As Exception
            Finally
                sw.Close()
            End Try
        End Sub

        Public Shared Function PrintDocument(urlOrContent As String, printerName As String, IsUrl As Boolean, Optional rentalId As String = "") As String
            Try

                If (RAPrinterSelection.IsValidPrinter(printerName)) Then
                    Dim webcontent As String = urlOrContent
                    If (IsUrl) Then
                        webcontent = AppendHtmlStructure(GetWebContent(urlOrContent))
                        ''LogInformation("PrintDocument HTML", urlOrContent & vbCrLf & vbCrLf & webcontent)
                        If (webcontent.IndexOf("ERROR") <> -1) Then
                            ''Helper.LogInformation("PrintDocument ERROR", webcontent)
                            Return "ERROR: Error retrieving URL content from " & urlOrContent
                        End If
                    End If

                    Dim logOutputOfRentalToPrint As Boolean = CBool(ConfigurationManager.AppSettings("LogOutputOfRentalToPrint"))
                    If (logOutputOfRentalToPrint) Then
                        Dim tempRentalPath As String = ConfigurationManager.AppSettings("tempRentalPath")
                        Dim fullpath As String = tempRentalPath & rentalId & ".htm"
                        SaveTempFile(webcontent, rentalId, fullpath)
                        '' SendFileToPrinter(printerName, fullpath)
                        ''DeleteFile(fullpath)
                    End If

                    If (RAPrinterSelection.PrintRaw(CType(printerName, String), webcontent)) Then
                        Return "OK"
                    Else
                        Return "ERROR: Sending Rental Agreement to " & printerName
                    End If

                Else
                    Return "ERROR: Invalid Printer " & printerName
                End If
            Catch ex As Exception
                LogInformation("PrintDocument EXCEPTION", ex.Message & "," & ex.StackTrace)
                Return "ERROR: " & ex.Message
            End Try
            Return "OK"
        End Function

#End Region

#Region "rev:mia 21Aug2015 - Add saved Rental agreement link in Aurora"
        Public Shared Function MapRentalPath(path As String, rentalIdFilename As String) As String
            Return HttpContext.Current.Server.MapPath(String.Concat(GetPDFtoAuroraTemporaryStorage, rentalIdFilename, ".pdf"))
        End Function

        Public Shared Sub DeleteMappedRentalPath(rentalIdFilename As String)
            DeleteFile(HttpContext.Current.Server.MapPath(String.Concat(GetPDFtoAuroraTemporaryStorage, rentalIdFilename, ".pdf")))
        End Sub

        Public Shared Function SendPDFToAurora(path As String, rentalIdFilename As String) As Boolean

            Dim tempRentalPath As String = MapRentalPath(path, rentalIdFilename)
            LogInformation("tempRentalPath".ToUpper, tempRentalPath)
            LogInformation("path".ToUpper, path)
            If (DownloadFile(path, tempRentalPath)) Then
            Else
                Return False
            End If

            Return True
        End Function
        Private Shared ReadOnly Property GetPDFtoAuroraTemporaryStorage As String
            Get
                Return ConfigurationManager.AppSettings("tempAuroraRentalPath")
            End Get
        End Property
#End Region
        
    End Class

End Namespace
