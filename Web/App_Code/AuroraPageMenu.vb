Imports System.Xml
Imports System.Collections.Generic

Imports Aurora.Common

Public Class AuroraPageMenuFunction
    Public MenId As String       ' A
    Public MenLabel As String    ' B
    Public FunFileName As String ' C
    Public MenFunId As String    ' D
    Public FunCode As String     ' E

    Public Sub New()
    End Sub

    Public Sub New(ByVal e As XmlElement)
        MenId = e.GetAttribute("A")
        MenLabel = e.GetAttribute("B")
        FunFileName = e.GetAttribute("C")
        MenFunId = e.GetAttribute("D")
        FunCode = e.GetAttribute("E")
    End Sub
End Class

Public Class AuroraPageMenuModule
    Public MenId As String       ' m1
    Public MenLabel              ' m2

    Private _items As New List(Of AuroraPageMenuFunction)
    Public Property Items() As AuroraPageMenuFunction()
        Get
            Return _items.ToArray()
        End Get
        Set(ByVal value As AuroraPageMenuFunction())
            _items.Clear()
            If value IsNot Nothing Then _items.AddRange(value)
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(ByVal e As XmlElement)
        MenId = e.GetAttribute("m1")
        MenLabel = e.GetAttribute("m2")
        For Each e0 As XmlElement In e.SelectNodes("L2")
            _items.Add(New AuroraPageMenuFunction(e0))
        Next
    End Sub
End Class

Public Class AuroraPageMenu
    Public Sub New()
    End Sub

    Private _items As New List(Of AuroraPageMenuModule)
    Public Property Items() As AuroraPageMenuModule()
        Get
            Return _items.ToArray()
        End Get
        Set(ByVal value As AuroraPageMenuModule())
            _items.Clear()
            If value IsNot Nothing Then _items.AddRange(value)
        End Set
    End Property

    Public Sub New(ByVal doc As XmlDocument)
        For Each e0 As XmlElement In doc.SelectNodes("/data/L1") ' 
            _items.Add(New AuroraPageMenuModule(e0))
        Next
    End Sub

    Public Function GetMenuFunctionByFunctionId(ByVal menuFunctionId As String) As AuroraPageMenuFunction
        For Each menuModule As AuroraPageMenuModule In Me.Items
            For Each menuFunction As AuroraPageMenuFunction In menuModule.Items
                If menuFunction.MenFunId = menuFunctionId Then Return menuFunction
            Next
        Next
        Return Nothing
    End Function

    Public Function GetMenuModuleByFunctionId(ByVal menuFunctionId As String) As AuroraPageMenuModule
        For Each menuModule As AuroraPageMenuModule In Me.Items
            For Each menuFunction As AuroraPageMenuFunction In menuModule.Items
                If menuFunction.MenFunId = menuFunctionId Then Return menuModule
            Next
        Next
        Return Nothing
    End Function


End Class