Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Xml
Imports Aurora.Common
Imports Aurora.Booking.Data
Imports Aurora.Booking.Services
Imports System.Data
Imports Aurora.Common.Data
Imports Aurora.Common.Logging
Imports WS.RentalAgreement
Imports WS.RAPrinter
Imports System.Web.Script.Serialization
Imports Aurora.RestAPI.Token
Imports System.Xml.Xsl
Imports System.IO
Imports WS.Helper
Imports WS.Utility

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class AuroraWebService
    Inherits System.Web.Services.WebService

#Region "GLOBAL CONSTANTS"
    Const PROGRAM_NAME As String = "PHOENIX_WEBSERVICE"
    Const DATE_FORMAT As String = "dd/MM/yyyy"
    Const TIME_FORMAT As String = "HH:mm"
    Const SUCCESS_STRING As String = "SUCCESS"
    Private Const CUSTOMER_ID As String = "7519EC4A-2601-4855-B703-CDD6DD22B108"
    Private Const AGENT_ID As String = "34CE97B1-5807-41C5-B043-5B10D43685C7"
    Private Const EOF_FLAG As String = "~EOF~"
#End Region


#Region "Confirmation Sending"

#Region "Constants"
    Const INSERT_FLAG As Char = "I"c
    Const UPDATE_FLAGE As Char = "U"c
    Const CONFIRMATION_XSL_PATH As String = "xsl/confreport.xsl"
    Const MAUI_LOGO_PATH As String = "../Images/Mauilogosmall.gif"
    Const BRITZ_LOGO_PATH As String = "../Images/Britz.gif"
    Const BACKPACKER_LOGO_PATH As String = "../Images/Backpackerlogo.gif"
    Const EXPLOREMORE_LOGO_PATH As String = "../Images/ExploreMore-logo.gif"
    Const MAC_LOGO_PATH As String = "../Images/MACLOGO.gif"
    Const CONFIRMATION_XSL_PATH_TCX As String = "xsl/confreporttcx.xsl"
#End Region

    <WebMethod()> _
    Public Function CreateConfirmation(ByVal BookingId As String, ByVal RentalId As String, ByVal RentalNumber As String, _
                                       ByVal AudienceType As String, ByVal HeaderText As String, ByVal FooterText As String, _
                                       ByVal UserCode As String, ByVal GetConfirmationXML As Boolean) As XmlDocument
        Dim xmlReturn As XmlDocument = New XmlDocument
        Dim xmlNotes As XmlDocument = New XmlDocument
        Dim xmlReturnMessage As XmlDocument = New XmlDocument
        Dim sXmlNotes As String
        Dim confirmationHtml As String



        Try
            LogInformation("CreateConfirmation: ", "BookingId: " & BookingId & ", RentalId: " & RentalId & ", sRntNum: " & RentalNumber & ", AudienceType: " & AudienceType & ", HeaderText: " _
                           & HeaderText & ", ConfirmationFooterText: " & FooterText & ", UserCode: " & UserCode & ", bGetConfXML: " & GetConfirmationXML)
            ' <<TO CHECK>>
            ' not sure if this is needed!
            'sXmlNotes = "<Data><NoteSpec><NoteId>3DBCAE57-6F44-4EAC-85BF-D9F46E26CD64</NoteId><Action>I</Action></NoteSpec></Data>"
            sXmlNotes = "<Data></Data>"
            xmlNotes.LoadXml(sXmlNotes)
            ' <<TO CHECK>>
            ' not sure if this is needed!

            Dim sConfirmationId As String = System.Guid.NewGuid().ToString()
            Dim xmlConfirmationInfo As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(BookingId, "")


            For Each oNode As XmlNode In xmlConfirmationInfo.DocumentElement.SelectNodes("audienceType/audience")
                If oNode.SelectSingleNode("CodCode").InnerText.Trim().ToUpper() = AudienceType.Trim().ToUpper() Then
                    AudienceType = oNode.SelectSingleNode("CodId").InnerText.Trim().ToUpper()
                    Exit For
                End If
            Next


            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                        sConfirmationId, _
                        BookingId, _
                        RentalId, _
                        RentalNumber, _
                        AudienceType, _
                        HeaderText, _
                        FooterText, _
                        "", "", "", "", _
                        False, _
                        1, _
                        UserCode, _
                        UserCode, _
                        PROGRAM_NAME, _
                        INSERT_FLAG, _
                        xmlNotes, _
                        "", _
                        "")

            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("GEN046") Then
                Throw New Exception("Error occured while saving confirmation data. CnfId = " & sConfirmationId)
            End If

            ' mailing part

            xmlConfirmationInfo = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(BookingId, sConfirmationId)
            Dim confirmationXml As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(sConfirmationId, False)

            confirmationXml.LoadXml(confirmationXml.DocumentElement.InnerXml)
            confirmationXml.DocumentElement.SelectSingleNode("viewbutton").InnerText = "1"


            Dim transform As System.Xml.Xsl.XslTransform = New System.Xml.Xsl.XslTransform()
            transform.Load(Server.MapPath(CONFIRMATION_XSL_PATH))

            Using writer As System.IO.StringWriter = New System.IO.StringWriter
                transform.Transform(confirmationXml, Nothing, writer, Nothing)

                confirmationHtml = Server.HtmlDecode(writer.ToString())
            End Using
            Try
                If (ConfirmationRequestOrigin = "BuyExperience" And AudienceType = CUSTOMER_ID) Then
                    Dim gstCountry As String = confirmationXml.SelectSingleNode("data/doc/Header/GSTCountry").InnerText
                    Dim BrandFooterText As String = Aurora.BookingExperiences.Service.BookingExperiences.GetBrandFullName(confirmationXml.SelectSingleNode("data/doc/Header/Brand").InnerText)
                    Dim vouchers As String = WebServiceHelper.BuildVouchers(BookingId, RentalId, UserCode, gstCountry, BrandFooterText)
                    If (Not String.IsNullOrEmpty(BookingId) And Not String.IsNullOrEmpty(RentalId) And Not String.IsNullOrEmpty(UserCode) And Not String.IsNullOrEmpty(AudienceType)) Then
                        confirmationHtml = String.Concat(confirmationHtml, vouchers)
                    End If
                End If
                ConfirmationRequestOrigin = "DONOTHING"
            Catch ex As Exception
            End Try



            Dim sHeaderText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/headnote").InnerText
            Dim sFooterText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/footnote").InnerText
            Dim bHighlight As Boolean = CBool(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/highlight").InnerText)
            Dim nIntegrityNo As Integer = CInt(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/integrityno").InnerText)

            ''rev:mia April 3, 2013 - addition of BCC as requested by Rajesh.
            ''sp change is cfn_getRentalsFromBooking
            Dim bccEmail As String = String.Empty
            Try
                bccEmail = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/BCCEmail").InnerText
            Catch ex As Exception
                Logging.LogInformation("CreateConfirmation: ", "BCC Email is empty")
            End Try


            Dim sPath As String = confirmationXml.DocumentElement.SelectSingleNode("CnfPath").InnerText & "\"
            Dim sCompany As String = confirmationXml.DocumentElement.SelectSingleNode("Company").InnerText
            Dim sAttention As String = confirmationXml.DocumentElement.SelectSingleNode("Contact").InnerText
            Dim sEFaxPath As String = confirmationXml.DocumentElement.SelectSingleNode("EFaxPath").InnerText
            Dim sReference As String = confirmationXml.DocumentElement.SelectSingleNode("Reference").InnerText
            Dim sEFaxNotify As String = confirmationXml.DocumentElement.SelectSingleNode("EFaxNotify").InnerText
            Dim sFaxBackup As String = confirmationXml.DocumentElement.SelectSingleNode("EfaxBackup").InnerText

            Dim sCustSurname As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/CustomerSurname").InnerText
            Dim sBookingRef As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/OurRef").InnerText
            Dim sEmailSubject As String = sCustSurname & " " & sBookingRef & " " & confirmationXml.DocumentElement.SelectSingleNode("doc/Header/ConfirmationTitle").InnerText














            ''rev:mia May 15 2012 - dynamic images in confirmation
            Dim brandLogo As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText
            Dim slashlastindex As Integer = brandLogo.LastIndexOf("/")
            If (slashlastindex <> -1) Then
                If (brandLogo.Length > slashlastindex + 1) Then
                    brandLogo = brandLogo.Substring(slashlastindex + 1).Trim
                Else
                    brandLogo = brandLogo.Substring(slashlastindex).Trim
                End If

                brandLogo = brandLogo.Substring(0, brandLogo.LastIndexOf(".")).Trim
                brandLogo = IIf(brandLogo.IndexOf("MAC") <> -1, "MOTORHOMESANDCARS.COM", brandLogo)
            End If
            sEmailSubject = String.Concat(brandLogo, " -  ", sEmailSubject)














            Dim sSavedFileName As String = sPath & sConfirmationId & ".htm"

            Dim sEmailId, sFaxNumber, sSentBy As String


            'email
            sFaxNumber = ""
            sSentBy = "Email"
            'Added by Nimesh on 17th June 2015
            Dim requireToSendEmail As Boolean = Not HeaderText.ToUpper().Trim().Equals("PRINT")
            If requireToSendEmail Then

                ''there is a throw statement in old getCustomerEmail
                Try
                    sEmailId = DataRepository.GetCustomerEmail(RentalId)
                Catch ex As Exception
                    sEmailId = ""
                End Try


                ''rev:mia 17-feb-2016 - get and compared email from EmailTrail
                'Dim emailTrailEmail As String = WebServiceHelper.CompareEmailBetweenAuroraAndEmailTrail(RentalId.Split("-")(0), RentalId)
                'If (Not emailTrailEmail.ToLower().Trim.Equals(sEmailId.ToLower().Trim)) Then
                '    sEmailId = emailTrailEmail
                'End If
                sEmailId = WebServiceHelper.CompareEmailAndMakeItAsReceiver(RentalId, sEmailId)
                ' <<TO REMOVE!!!!>>
                'sEmailId = "shoel.palli@thlonline.com"




                ''rev:mia 23-feb-2016 remove it from when goes live
                ''sEmailId = ConfigurationManager.AppSettings("ExperienceOnrequestEmail")
                WebServiceHelper.EMailConfirmation(confirmationHtml, _
                                         sEmailId, _
                                         sEFaxNotify, _
                                         sEmailSubject, _
                                         bccEmail)
                'Aurora.Booking.Services.BookingConfirmation.EMailDataPDF(confirmationHtml, _
                '                         sEmailId, _
                '                         sEFaxNotify, _
                '                         sEmailSubject, _
                '                         bccEmail)
                ' end mailing part
            End If
            'End Added by Nimeshon 17th June 2015
            ' this part adds the history!!!
            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                        sConfirmationId, _
                        BookingId, _
                        IIf(RentalNumber.ToUpper().Trim().Equals("ALL"), RentalNumber, RentalId), _
                        RentalNumber, _
                        AudienceType, _
                        sHeaderText, _
                        sFooterText, _
                        sEmailId, _
                        sFaxNumber, _
                        sSentBy, _
                        sSavedFileName, _
                        bHighlight, _
                        nIntegrityNo, _
                        UserCode, _
                        UserCode, _
                        PROGRAM_NAME, _
                        UPDATE_FLAGE, _
                        Nothing, _
                        "", _
                        "")
            ' this part adds the history!!!

            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("GEN046") Then
                Throw New Exception("Error occured while saving confirmation data. CnfId = " & sConfirmationId)
            End If

            ''rev:mia July 8 2013 - confirmatin saving added
            Try
                LogWarning("1. WEB  CreateConfirmation - InsertConfirmationHTML", confirmationHtml & " ---- " & sConfirmationId)
                InsertConfirmationHTML(sConfirmationId, confirmationHtml, UserCode, "")
            Catch ex As Exception
                LogWarning("1. FAILED: WEB  CreateConfirmation - InsertConfirmationHTML", confirmationHtml & " ---- " & sConfirmationId & " --- " & ex.StackTrace)
            End Try

            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>SUCCESS</Status>" & _
                                        "<ConfId>" & sConfirmationId & "</ConfId>" & _
                                        IIf(GetConfirmationXML, "<ConfXML>" & confirmationXml.OuterXml & "</ConfXML>", String.Empty) & _
                                     "</Data>")


        Catch ex As Exception
            'Aurora.Common.Logging.LogException(PROGRAM_NAME, ex)
            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>ERROR</Status>" & _
                                        "<Message>" & ex.Message & "</Message>" & _
                                     "</Data>")

            Logging.LogError("CreateConfirmation ERROR: ", ex.Message & ", " & ex.StackTrace & ", " & ex.Source)
        End Try

        Return xmlReturnMessage

    End Function
#End Region

#Region "DVASS NOTIFY"

    <WebMethod()> _
    Public Function DVASSRentalAdd(ByVal CountryCode As Integer, ByVal RentalId As String, ByVal ProductId As String, _
                                   ByVal CheckOutLocationCode As String, ByVal CheckOutDate As Date, ByVal CheckOutDayPart As Long, _
                                   ByVal CheckInLocationCode As String, ByVal CheckInDate As Date, ByVal CheckInDayPart As Long, _
                                   ByVal Revenue As Double, ByVal Priority As Double, ByVal Visibility As Integer, _
                                   ByVal VehicleIdList As String, ByVal ForceFlag As Long, ByVal UserCode As String) As XmlDocument

        Dim xmlReturnMessage As XmlDocument = New XmlDocument
        Try
            Dim sDVASSStatus As String
            Aurora.Reservations.Services.AuroraDvass.RentalAdd(CountryCode, RentalId, ProductId, _
                                                               CheckOutLocationCode, CheckOutDate, CheckOutDayPart, _
                                                               CheckInLocationCode, CheckInDate, CheckInDayPart, _
                                                               Revenue, Priority, Visibility, VehicleIdList, _
                                                               ForceFlag, sDVASSStatus, UserCode)
            If sDVASSStatus.ToUpper().Contains("ERROR") Then
                Throw New Exception(sDVASSStatus)
            Else
                xmlReturnMessage.LoadXml("<Data>" & _
                                                "<Status>DVASS CALL COMPLETE</Status>" & _
                                                "<Message>" & sDVASSStatus & "</Message>" & _
                                         "</Data>")
            End If

        Catch ex As Exception
            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>ERROR</Status>" & _
                                        "<Message>" & ex.Message & "</Message>" & _
                                     "</Data>")
        End Try

        Return xmlReturnMessage
    End Function
    'Added by Nimesh on 09th June 2015 
    <WebMethod()> _
    Public Function DVASSRentalModify(ByVal CountryCode As Integer, ByVal RentalId As String, ByVal ProductId As String, _
                                   ByVal CheckOutLocationCode As String, ByVal CheckOutDate As Date, ByVal CheckOutDayPart As Long, _
                                   ByVal CheckInLocationCode As String, ByVal CheckInDate As Date, ByVal CheckInDayPart As Long, _
                                   ByVal Revenue As Double, ByVal Priority As Double, ByVal Visibility As Integer, _
                                   ByVal VehicleIdList As String, ByVal ForceFlag As Long) As XmlDocument

        Dim xmlReturnMessage As XmlDocument = New XmlDocument
        Try
            'Dim sDVASSStatus As String
            'Aurora.Reservations.Services.AuroraDvass.ModifyRental(CountryCode, RentalId, ProductId, _
            '                                                   CheckOutLocationCode, CheckOutDate, CheckOutDayPart, _
            '                                                   CheckInLocationCode, CheckInDate, CheckInDayPart, _
            '                                                   Revenue, Priority, Visibility, VehicleIdList, _
            '                                                   ForceFlag)
            Aurora.Reservations.Services.AuroraDvass.ModifyRental(CountryCode, RentalId, ProductId, CheckOutDate, CheckOutDayPart, CheckInDate, CheckInDayPart, CheckOutLocationCode, CheckInLocationCode, Revenue, Priority, VehicleIdList, ForceFlag)




            'If sDVASSStatus.ToUpper().Contains("ERROR") Then
            '    Throw New Exception(sDVASSStatus)
            'Else
            xmlReturnMessage.LoadXml("<Data>" & _
                                            "<Status>DVASS CALL COMPLETE</Status>" & _
                                            "<Message>Success</Message>" & _
                                     "</Data>")
            'End If

        Catch ex As Exception
            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>ERROR</Status>" & _
                                        "<Message>" & ex.Message & "</Message>" & _
                                     "</Data>")
        End Try

        Return xmlReturnMessage
    End Function
    'End Added by Nimesh on 09th June 2015 
#End Region

#Region "DVASS AVAIL QUERY"
    <WebMethod()> _
    Public Function GetDVASSStatus(ByVal CountryCode As String, ByVal ProductId As String, _
                                   ByVal CheckOutLocationCode As String, ByVal CheckOutDate As DateTime, _
                                   ByVal CheckInLocationCode As String, ByVal CheckInDate As DateTime) As XmlDocument

        Dim sReturnedErrors As String = String.Empty
        Dim xmlReturnMessage As XmlDocument = New XmlDocument
        Dim bResult As Boolean
        Dim iCountry, iCheckOutAMPM, iCheckInAMPM As Integer

        Try

            iCountry = IIf(CountryCode = "AU", 0, 1)
            iCheckOutAMPM = IIf(CheckOutDate.TimeOfDay >= CDate("12:00:00").TimeOfDay, 1, 0)
            iCheckInAMPM = IIf(CheckInDate.TimeOfDay >= CDate("12:00:00").TimeOfDay, 1, 0)

            bResult = Aurora.Reservations.Services.AuroraDvass.QueryDvassAvailability(iCountry, ProductId, CheckOutLocationCode, CheckOutDate, iCheckOutAMPM, CheckInLocationCode, CheckInDate, iCheckInAMPM, sReturnedErrors)

            If String.IsNullOrEmpty(sReturnedErrors) Then
                xmlReturnMessage.LoadXml("<Data>" & _
                                            "<Status>DVASS CALL COMPLETE</Status>" & _
                                            "<Result>" & bResult & "</Result>" & _
                                            "<Message>" & sReturnedErrors & "</Message>" & _
                                         "</Data>")
            Else
                xmlReturnMessage.LoadXml("<Data>" & _
                                            "<Status>DVASS CALL COMPLETE</Status>" & _
                                            "<Result>" & False & "</Result>" & _
                                            "<Message>" & sReturnedErrors & "</Message>" & _
                                         "</Data>")
            End If
        Catch ex As Exception
            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>ERROR</Status>" & _
                                        "<Result>" & False & "</Result>" & _
                                        "<Message>" & ex.Message & "</Message>" & _
                                     "</Data>")
        End Try

        Return xmlReturnMessage
    End Function
#End Region

#Region "Credit Card Secure Note"
    ''' <summary>
    ''' This method takes the text and encrypts it in a format that is decodable in Aurora by users with appropriate clearance
    ''' </summary>
    ''' <param name="NoteData">Data (string)</param>
    ''' <returns>Encrypted Data (string)</returns>
    ''' <remarks></remarks>
    <WebMethod()> _
    Public Function EncryptTextForAurora(ByVal NoteData As String) As XmlDocument

        Dim xmlReturnMessage As New XmlDocument
        Dim sEncryptedData, sKeyfile As String

        sKeyfile = Server.MapPath("~/app_code/" & ConfigurationManager.AppSettings("KEYFILE"))

        Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = ConfigurationManager.AppSettings("PROTECTKEY")
        Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = ConfigurationManager.AppSettings("ALGORITHM")
        Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(sKeyfile)
        Dim encryptedCreditcard As Byte()

        Try
            encryptedCreditcard = Aurora.Booking.Services.BookingPaymentCardEncryption.EncyrptCardNumber(NoteData, sKeyfile)
            sEncryptedData = Convert.ToBase64String(encryptedCreditcard)


            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>SUCCESS</Status>" & _
                                        "<EncryptedData>" & sEncryptedData & "</EncryptedData>" & _
                                        "<Message/>" & _
                                     "</Data>")

        Catch ex As Exception
            xmlReturnMessage.LoadXml("<Data>" & _
                                        "<Status>ERROR</Status>" & _
                                        "<Message>" & ex.Message & "</Message>" & _
                                        "<EncryptedData/>" & _
                                     "</Data>")
        End Try

        Return xmlReturnMessage
    End Function

    ''' <summary>
    ''' This will solely use by amsterdam 
    ''' </summary>
    ''' <param name="cardNumberEncrypted"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>

    <WebMethod()> _
    Public Function DecryptCardForAmsterdam(ByVal cardNumberEncrypted As String) As String
        Dim decryptedString As String = ""
        Try
            Dim mKeyfile As String = Server.MapPath("~/app_code/" & ConfigurationManager.AppSettings("KEYFILE"))
            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)
            decryptedString = Aurora.Booking.Services.BookingPaymentCardEncryption.DecryptCardNumber _
            (Convert.FromBase64String(cardNumberEncrypted), mKeyfile, True)
        Catch ex As Exception
            Return "<CardError>" & ex.Message & "</CardError><Card/>"
        End Try
        Return "<CardError></CardError><Card>" & decryptedString & "</Card>"


    End Function



#End Region

#Region "SELF CHECKOUT"

#Region "AIMS STUFF"

    <WebMethod()> _
    Public Function DoAIMSF6ActivityCreate(ByVal ActivityType As String, ByVal ExternalRef As String, ByVal UnitNumber As String, _
                                           ByVal StartLocationCode As String, ByVal StartDate As DateTime, _
                                           ByVal StartOdometer As Integer, ByVal ExpectedEndLocation As String, _
                                           ByVal ExpectedEndDate As DateTime, ByVal ExpectedEndOdometer As Integer, _
                                           ByVal OtherAssets As String, ByVal UserCode As String, ByVal DVASSCall As Boolean, _
                                           ByVal Forced As Boolean) As XmlDocument

        Dim xmlReturn As New XmlDocument

        Try

            Dim oAimsOjb As New AI.CMaster, _
                sAimsReturnValue As Integer
            If DVASSCall Then
                If Forced Then
                    sAimsReturnValue = oAimsOjb.F6_ActivityCreate(ActivityType, ExternalRef, UnitNumber, _
                                                                  StartLocationCode, StartDate, StartOdometer, _
                                                                  ExpectedEndLocation, ExpectedEndDate, ExpectedEndOdometer, _
                                                                  OtherAssets, UserCode)
                Else
                    sAimsReturnValue = oAimsOjb.F6_ActivityCreate(ActivityType, ExternalRef, UnitNumber, _
                                                                  StartLocationCode, StartDate, StartOdometer, _
                                                                  ExpectedEndLocation, ExpectedEndDate, ExpectedEndOdometer, _
                                                                  OtherAssets)
                End If
            Else
                sAimsReturnValue = oAimsOjb.F6_ActivityCreate(ActivityType, ExternalRef, UnitNumber, _
                                                              StartLocationCode, StartDate, StartOdometer, _
                                                              ExpectedEndLocation, ExpectedEndDate, ExpectedEndOdometer, _
                                                              OtherAssets, "<F9NotReq>")
            End If


            'check status of the return value
            Dim sErrNo As String
            sErrNo = Trim(sAimsReturnValue)

            'Check for error which need to send back to client Conformation
            ' This error is for Supervisor Override
            If sErrNo <> 0 And (sErrNo = "-2147220471" Or _
                                sErrNo = "-2147220470" Or _
                                sErrNo = "-2147220488") Then
                Throw New Exception("POPUP1/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(sErrNo))
            End If

            ' This error is for Continue Cancel ie. Just WarningAurora.Booking.Services.BookingCheckInCheckOut
            If sErrNo <> 0 And (sErrNo = "-2147220489" Or _
                                sErrNo = "-2147220475" Or _
                                sErrNo = "-2147220469") Then
                Throw New Exception("POPUP2/" & Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(sErrNo))
            End If

            If sErrNo <> "0" Then
                Throw New Exception(Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(sErrNo))
            End If


            xmlReturn.LoadXml("<Data><AIMSMessage>SUCCESS</AIMSMessage></Data>")
        Catch ex As Exception
            xmlReturn.LoadXml("<Data><AIMSMessage/><Error>" & ex.Message & "</Error></Data>")
        End Try

        Return xmlReturn

    End Function

#End Region


#Region "DVASS CANCEL RENTAL"

    <WebMethod()> _
    Public Function DVASSRentalCancel(ByVal RentalId As String, ByVal ForceFlag As Int16, ByVal Cost As Double, _
                                      ByVal CountryCode As String, ByVal UserCode As String) As XmlDocument

        Dim nSelectedCountryCode As Integer, _
            sDvassStatus As String, _
            xmlReturn As New XmlDocument

        Try


            If CountryCode = "AU" Then
                nSelectedCountryCode = 0
            Else
                nSelectedCountryCode = 1
            End If
            Aurora.Reservations.Services.AuroraDvass.CancelRental(RentalId, ForceFlag, Cost, sDvassStatus, nSelectedCountryCode, UserCode)

            If sDvassStatus <> "SUCCESS" Then
                If InStr(sDvassStatus, "333") = 0 Then
                    Throw New Exception(sDvassStatus)
                End If
            End If

            xmlReturn.LoadXml("<Data><DVASSStatus>SUCCESS</DVASSStatus></Data>")
        Catch ex As Exception
            xmlReturn.LoadXml("<Data><DVASSStatus/><Error>" & ex.Message & "</Error></Data>")

        End Try

        Return xmlReturn
    End Function


#End Region

#Region "GetDataForCheckInCheckOut"

    <WebMethod()> _
    Public Function GetCheckOutData(ByVal BookingId As String, ByVal RentalId As String, ByVal UserCode As String, ByVal CountryCode As String) As XmlDocument

        Dim xmlReturn As New XmlDocument
        Try
            xmlReturn = Aurora.Booking.Services.BookingCheckInCheckOut.GetDataForCheckInCheckOut(BookingId, RentalId, UserCode, 0, "", CountryCode)
        Catch ex As Exception

            xmlReturn.LoadXml("<Error><Message>" & ex.Message & "</Message></Data>")
        End Try
        Return xmlReturn

    End Function
#End Region

#End Region

#Region "rev:mia March 14, 2013 - added the function for Confirmation Insertion. "
    ''today in history: March 14, 213 - New Pope was elected. A cardinal from Argentina. Long live Pope Francisco

    <WebMethod(Description:="This Web Method will be use by the SELFCHECKIN to send email confirmation")> _
    Public Function CreateAndSendconfirmation(ByVal RentalInfo As String) As String
        If String.IsNullOrEmpty(RentalInfo) Then Return False
        If RentalInfo.Contains("-") = False Then Return False
        If RentalInfo.Split("-").Length = 1 Then Return False

        Dim xmlReturn As New XmlDocument
        Dim xmlReturnMessage As New XmlDocument


        Dim confirmationHtml As String

        Dim BookingId As String = RentalInfo.Split("-")(0)
        Dim RentalId As String = RentalInfo
        Dim RentalNumber As String = RentalInfo.Split("-")(1)

        Dim HeaderText As String = ""
        Dim FooterText As String = ""

        Const AUDIENCE_TYPE As String = "7519EC4A-2601-4855-B703-CDD6DD22B108" ''CUSTOMER {AGENT = 34CE97B1-5807-41C5-B043-5B10D43685C7}
        Const USER_CODE As String = "SelfCKISYS"
        Const PROGRAM_NAME As String = "PHOENIX_WS"
        Const RETURNED_STRING As String = "<Data><Status>{0}</Status><ConfId>{1}</ConfId><ConfXML>{2}</ConfXML></Data>"

        Dim targetAudience As String = AUDIENCE_TYPE
        Dim formatting As String = RETURNED_STRING


        Try
            Dim sEmailId As String = DataRepository.GetCustomerEmail(RentalId)
            If (String.IsNullOrEmpty(sEmailId)) Then
                Logging.LogError("CreateAndSendconfirmation", "This Rental ID has no associated Email Address")
                Return String.Format(formatting, "ERROR", RentalId, "This Rental ID has no associated Email Address")
            End If

            Dim sConfirmationId As String = System.Guid.NewGuid().ToString()
            Dim xmlConfirmationInfo As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(BookingId, "")

            For Each oNode As XmlNode In xmlConfirmationInfo.DocumentElement.SelectNodes("audienceType/audience")
                If oNode.SelectSingleNode("CodCode").InnerText.Trim().ToUpper() = AUDIENCE_TYPE.Trim().ToUpper() Then
                    targetAudience = oNode.SelectSingleNode("CodId").InnerText.Trim().ToUpper()
                    Exit For
                End If
            Next


            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                        sConfirmationId, _
                        BookingId, _
                        RentalId, _
                        RentalNumber, _
                        targetAudience, _
                        HeaderText, _
                        FooterText, _
                        String.Empty, _
                        String.Empty, _
                        String.Empty, _
                        String.Empty, _
                        False, _
                        1, _
                        USER_CODE, _
                        USER_CODE, _
                        PROGRAM_NAME, _
                        INSERT_FLAG, _
                        Nothing, _
                        String.Empty, _
                        String.Empty)

            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("GEN046") Then
                Logging.LogError("CreateAndSendconfirmation", "Error occured while saving confirmation data. CnfId = " & sConfirmationId)
                Return String.Format(formatting, "ERROR", sConfirmationId, xmlConfirmationInfo.OuterXml)
            End If


            xmlConfirmationInfo = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(BookingId, sConfirmationId)
            Dim confirmationXml As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(sConfirmationId, False)

            confirmationXml.LoadXml(confirmationXml.DocumentElement.InnerXml)
            confirmationXml.DocumentElement.SelectSingleNode("viewbutton").InnerText = "1"

            Dim transform As System.Xml.Xsl.XslTransform = New System.Xml.Xsl.XslTransform()
            transform.Load(Server.MapPath(CONFIRMATION_XSL_PATH))

            Using writer As System.IO.StringWriter = New System.IO.StringWriter
                transform.Transform(confirmationXml, Nothing, writer, Nothing)
                confirmationHtml = Server.HtmlDecode(writer.ToString())
            End Using

            ''confirmationHtml = Transformation(confirmationXml.OuterXml, Server.MapPath(CONFIRMATION_XSL_PATH))

            Dim sHeaderText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/headnote").InnerText
            Dim sFooterText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/footnote").InnerText
            Dim bHighlight As Boolean = CBool(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/highlight").InnerText)
            Dim nIntegrityNo As Integer = CInt(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/integrityno").InnerText)

            ''Dim sPath As String = confirmationXml.DocumentElement.SelectSingleNode("CnfPath").InnerText & "\"
            Dim sEFaxNotify As String = confirmationXml.DocumentElement.SelectSingleNode("EFaxNotify").InnerText


            Dim sCustSurname As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/CustomerSurname").InnerText
            Dim sBookingRef As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/OurRef").InnerText
            Dim sEmailSubject As String = sCustSurname & " " & sBookingRef & " " & confirmationXml.DocumentElement.SelectSingleNode("doc/Header/ConfirmationTitle").InnerText


            Dim brandLogo As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText
            Dim slashlastindex As Integer = brandLogo.LastIndexOf("/")
            If (slashlastindex <> -1) Then
                If (brandLogo.Length > slashlastindex + 1) Then
                    brandLogo = brandLogo.Substring(slashlastindex + 1).Trim
                Else
                    brandLogo = brandLogo.Substring(slashlastindex).Trim
                End If

                brandLogo = brandLogo.Substring(0, brandLogo.LastIndexOf(".")).Trim
                brandLogo = IIf(brandLogo.IndexOf("MAC") <> -1, "MOTORHOMESANDCARS.COM", brandLogo)
            End If
            sEmailSubject = String.Concat(brandLogo, " -  ", sEmailSubject)


            Dim sFaxNumber As String = String.Empty
            Dim sSentBy As String = "Email"

            If Not SendConfirmationSuccessfull( _
                 confirmationHtml, _
                 sEmailId, _
                 sEFaxNotify, _
                 sEmailSubject) Then
                Logging.LogError("CreateAndSendconfirmation", "Unable to send confirmation e-mail for CnfId=" & sConfirmationId & " to e-mail id : " & sEmailId)
                Return String.Format(formatting, "ERROR", sConfirmationId, "Unable to send confirmation e-mail for CnfId=" & sConfirmationId & " to e-mail id : " & sEmailId)
            End If

            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                          sConfirmationId, _
                          BookingId, _
                          IIf(RentalNumber.ToUpper().Trim().Equals("ALL"), RentalNumber, RentalId), _
                          RentalNumber, _
                          AUDIENCE_TYPE, _
                          sHeaderText, _
                          sFooterText, _
                          sEmailId, _
                          sFaxNumber, _
                          sSentBy, _
                          "", _
                          bHighlight, _
                          nIntegrityNo, _
                          USER_CODE, _
                          USER_CODE, _
                          PROGRAM_NAME, _
                          UPDATE_FLAGE, _
                          Nothing, _
                          "", _
                          "")


            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("GEN046") Then
                Logging.LogError("CreateAndSendconfirmation", "Error occured while saving confirmation data. CnfId = " & sConfirmationId)
                Return String.Format(formatting, "ERROR", sConfirmationId, xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            End If
            InsertConfirmationHTML(sConfirmationId, confirmationHtml, USER_CODE, "")

            xmlReturnMessage.LoadXml(String.Format(formatting, "SUCCESS", sConfirmationId, confirmationXml.OuterXml))



        Catch ex As Exception

            xmlReturnMessage.LoadXml(String.Format(formatting, "ERROR", "", ex.Message & " - " & ex.StackTrace))

        End Try


        Return xmlReturnMessage.OuterXml

    End Function


    Private Sub InsertConfirmationHTML(ByVal confirmationid As String, _
                               ByVal confirmationtext As String, _
                               ByVal userid As String, _
                               ByVal newconfirmationid As String)
        '' Aurora.Common.ConfirmationExtension.InsertConfirmationHTML(confirmationid, confirmationtext, userid, newconfirmationid)
        Aurora.Confirmation.Services.Confirmation.InsertConfirmationHTML(confirmationid, confirmationtext, userid, newconfirmationid)
    End Sub

    Private Function SendConfirmationSuccessfull(ByVal sCfnText As String, _
                                ByVal sEmailAddress As String, _
                                ByVal EFaxNotify As String, _
                                ByVal emailSubject As String) As Boolean

        Dim sText As String = sCfnText

        Try

            If sText <> "" Then
                sText = Replace(sText, "../Images/", "")

                Dim emailArray As Array
                Dim emailImg As String
                emailArray = Split(emailSubject, " - ", -1, 1)
                emailImg = Trim(emailArray(0))

                sText = Aurora.Booking.Services.BookingConfirmation.GetBrandLogoImage(emailImg, sText)
            End If

            If sText <> "" Then
                Dim strBody As String = ""

                Dim oMailMsg As System.Web.Mail.MailMessage = New System.Web.Mail.MailMessage
                oMailMsg.From = EFaxNotify
                oMailMsg.To = sEmailAddress
                ' fix for weird mapping problem
                oMailMsg.Headers.Add("Reply-To", EFaxNotify)
                ' fix for weird mapping problem
                oMailMsg.BodyEncoding = System.Text.Encoding.ASCII
                oMailMsg.Subject = emailSubject
                oMailMsg.Body = sText
                oMailMsg.BodyFormat = Web.Mail.MailFormat.Html

                Web.Mail.SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("MailServerURL"))
                Web.Mail.SmtpMail.Send(oMailMsg)

            Else
                Logging.LogDebug("SendConfirmation", "Calling SendConfirmation sText variable is empty")
                Return False
            End If

        Catch ex As Exception
            Logging.LogError("SendConfirmation", ex.Message & vbCrLf & ex.StackTrace)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region "Konstantin Stuff here"

    <WebMethod()> _
    Public Function GetCountryByLocation(ByVal locationCode As String) As String
        If locationCode Is Nothing OrElse locationCode.Length > 12 Then
            Return Nothing
        End If

        Dim command As IDbCommand = Nothing

        Try
            command = Aurora.Common.Data.GetConnection().CreateCommand()

            Dim locParam As IDbDataParameter = command.CreateParameter()
            locParam.ParameterName = "locCode"
            locParam.Value = locationCode

            command.Parameters.Add(locParam)
            command.CommandType = CommandType.Text
            command.CommandText = "select dbo.getCtyCode(@locCode)"

            Dim result As Object = Aurora.Common.Data.ExecuteScalar(Aurora.Common.Data.GetDatabase(), command)

            If Object.ReferenceEquals(result, DBNull.Value) Then
                Return Nothing
            End If

            Return DirectCast(result, String)

            Return result
        Catch ex As Exception
            Return Nothing
        Finally
            If command IsNot Nothing Then
                command.Dispose()
            End If
        End Try

    End Function
#End Region

#Region "rev:mia  July 23 2012 Create a new function for GetWSAvailability"

    '<WebMethod(EnableSession:=True, Description:="Function for GetAvailability that call (R)DVASS, (T)Thrifty, (E)EuroCar, F - FreeSale, X - Stop Sale, M - On Request")> _
    'Public Function GetWSAvailability(CountryCode As String, _
    '                                 ProductShortName As String, _
    '                                CheckOutLocationCode As String, _
    '                                CheckOutDate As Date, _
    '                                CheckInLocationCode As String, _
    '                                CheckInDate As Date, _
    '                                AgentCode As String, _
    '                                PackageCode As String, _
    '                                UserId As String, _
    '                                PrgName As String, _
    '                                NoOfVehiclesLookUp As Integer, _
    '                                brdCode As String, _
    '                                IsQuickAvail As Boolean) As String

    '    If String.IsNullOrEmpty(PrgName) Then PrgName = "WEB"

    '    If Not String.IsNullOrEmpty(ProductShortName) Then ProductShortName = ProductShortName.Trim.ToUpper
    '    If Not String.IsNullOrEmpty(CheckOutLocationCode) Then CheckOutLocationCode = CheckOutLocationCode.Trim.ToUpper
    '    If Not String.IsNullOrEmpty(CheckInLocationCode) Then CheckInLocationCode = CheckInLocationCode.Trim.ToUpper
    '    If Not String.IsNullOrEmpty(AgentCode) Then AgentCode = AgentCode.Trim.ToUpper
    '    If Not String.IsNullOrEmpty(PackageCode) Then PackageCode = PackageCode.Trim.ToUpper
    '    If Not String.IsNullOrEmpty(UserId) Then UserId = UserId.Trim.ToUpper

    '    Dim ErrorMessage As String = String.Empty
    '    Dim sAvail As String = String.Empty
    '    Dim result As String = String.Empty
    '    Dim sbResult As String = String.Empty

    '    Dim sb As New StringBuilder

    '    sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
    '    sb.Append("||1.Calling GetWSAvailability " & vbCrLf)
    '    sb.Append("||************************" & vbCrLf)
    '    sb.Append("||INPUT PARAMETERS" & vbCrLf)
    '    sb.Append("||CountryCode          : " & CountryCode & vbCrLf)
    '    sb.Append("||ProductShortName     : " & ProductShortName & vbCrLf)
    '    sb.Append("||CheckOutLocationCode : " & CheckOutLocationCode & vbCrLf)
    '    sb.Append("||CheckOutDate         : " & CheckOutDate.ToString & vbCrLf)
    '    sb.Append("||CheckInLocationCode  : " & CheckInLocationCode & vbCrLf)
    '    sb.Append("||CheckInDate          : " & CheckInDate.ToString & vbCrLf)
    '    sb.Append("||AgentCode            : " & AgentCode & vbCrLf)
    '    sb.Append("||PackageCode          : " & PackageCode & vbCrLf)
    '    sb.Append("||UserId               : " & UserId & vbCrLf)
    '    sb.Append("||PrgName              : " & PrgName & vbCrLf)
    '    sb.Append("||NoOfVehiclesLookUp   : " & NoOfVehiclesLookUp & vbCrLf)
    '    sb.Append("||BrdCode              : " & brdCode & vbCrLf)

    '    Try
    '        Dim params(12) As Aurora.Common.Data.Parameter

    '        Dim sErrors As String = String.Empty
    '        Dim sWarnings As String = String.Empty
    '        Dim DvassSequenceNumber As Int64


    '        ' INPUT parameters
    '        params(0) = New Aurora.Common.Data.Parameter("sPrdSrtNam", DbType.String, ProductShortName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(1) = New Aurora.Common.Data.Parameter("sCkoLoc", DbType.String, CheckOutLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(2) = New Aurora.Common.Data.Parameter("dCkoDate", DbType.DateTime, CheckOutDate.ToString("dd/MM/yyyy HH:mm"), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(3) = New Aurora.Common.Data.Parameter("sCkiLoc", DbType.String, CheckInLocationCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(4) = New Aurora.Common.Data.Parameter("dCkiDate", DbType.DateTime, CheckInDate.ToString("dd/MM/yyyy HH:mm"), Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(5) = New Aurora.Common.Data.Parameter("sAgnId", DbType.String, AgentCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(6) = New Aurora.Common.Data.Parameter("sPkgId", DbType.String, PackageCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(7) = New Aurora.Common.Data.Parameter("sUserId", DbType.String, UserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
    '        params(8) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, PrgName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

    '        ' OUTPUT parameters
    '        params(9) = New Aurora.Common.Data.Parameter("sAvail", DbType.String, 1, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(10) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(11) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
    '        params(12) = New Aurora.Common.Data.Parameter("sDVASSSeqNum", DbType.Int64, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)


    '        ''sb.Append("||Execute Stored Procedure               : WEBP_CheckAvailability".ToUpper & vbCrLf)
    '        Aurora.Common.Data.ExecuteOutputSP("WEBP_CheckAvailability", params)

    '        sAvail = params(9).Value
    '        sErrors = params(10).Value
    '        sWarnings = params(11).Value
    '        DvassSequenceNumber = params(12).Value

    '        sb.Append("||Output Parameters Result".ToUpper & vbCrLf)
    '        sb.Append("||Avail                : " & "********* [   " & sAvail & "  ] *********" & vbCrLf)
    '        sb.Append("||Errors               : " & sErrors & vbCrLf)
    '        sb.Append("||Warnings             : " & sWarnings & vbCrLf)
    '        sb.Append("||DvassSequenceNumber  : " & DvassSequenceNumber & vbCrLf)
    '        sb.Append("||IsQuickAvail         : " & IsQuickAvail & vbCrLf)

    '        Dim defaultavail As String = ConfigurationManager.AppSettings("availCodeDefault").ToString
    '        If (Not String.IsNullOrEmpty(defaultavail) And String.IsNullOrEmpty(sAvail)) Then
    '            sb.Append("||NOTE: Avail returns empty. Program will now use the default Availcode from Web.config" & vbCrLf)
    '            sAvail = defaultavail
    '        End If
    '        ''sAvail = "R"


    '        If (IsQuickAvail = True) Then
    '            sErrors = ""
    '            sWarnings = ""
    '        End If

    '        If (Not String.IsNullOrEmpty(sAvail)) Then

    '            sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
    '            sb.Append("||2.Calling WSExecute " & vbCrLf)
    '            sb.Append("||************************" & vbCrLf)

    '            result = WSexecute(sAvail, _
    '                             CountryCode, _
    '                             DvassSequenceNumber.ToString(), _
    '                             CheckOutLocationCode, _
    '                             CheckOutDate, _
    '                             CheckInLocationCode, _
    '                             CheckInDate, _
    '                             ProductShortName, _
    '                             sbResult, _
    '                             NoOfVehiclesLookUp, _
    '                             AgentCode, _
    '                             brdCode, _
    '                             IsQuickAvail)
    '            If (result.Contains("Error") = True) Then
    '                Throw New Exception(result)
    '            End If
    '        Else

    '            result = "<Data>" & _
    '                                       "<Status>ERROR</Status>" & _
    '                                       "<Result>" & False & "</Result>" & _
    '                                       "<Message>" & sErrors & "-" & sWarnings & "</Message>" & _
    '                                    "</Data>"

    '        End If

    '    Catch ex As Exception

    '        result = "<Data>" & _
    '                       "<Status>ERROR</Status>" & _
    '                       "<Result>" & False & "</Result>" & _
    '                       "<Message>" & ex.Message & "-" & ex.StackTrace & "</Message>" & _
    '                    "</Data>"

    '        sb.Append("||Exception              : " & result & vbCrLf)
    '    End Try
    '    sb.Append("||" & sbResult & vbCrLf)
    '    ''sb.Append("||************************ GetWSAvailability  End" & vbCrLf)

    '    Logging.LogDebug("GetWSAvailability: ".ToUpper, sb.ToString.Insert(0, vbCrLf))
    '    Context.Session("GetWSAvailability") = sb.ToString

    '    Return result
    'End Function

    ' ''' <summary>
    ' ''' 'Base on the result of GetWSAvailability, this will call Webservice for Thrifty and EuropCar or DVASS
    ' ''' </summary>
    ' ''' <param name="AvailCode"></param>
    ' ''' <returns></returns>
    ' ''' <remarks></remarks>
    'Private Function WSexecute(AvailCode As String, _
    '                           ByVal CountryCode As String, _
    '                           ByVal ProductId As String, _
    '                           ByVal CheckOutLocationCode As String, _
    '                           ByVal CheckOutDate As Date, _
    '                           ByVal CheckInLocationCode As String, _
    '                           ByVal CheckInDate As Date, _
    '                           ByVal ProductShortName As String, _
    '                           ByRef sbResult As String, _
    '                           ByVal NoOfVehiclesLookUp As Integer, _
    '                           ByVal AgentId As String, _
    '                           ByVal BrdCode As String, _
    '                           isQuickAvail As Boolean) As String

    '    Dim result As String = String.Empty
    '    Const NOT_VALID As String = "Warning: Not valid Availability Code"""

    '    Dim sb As New StringBuilder

    '    sb.Append("||FUNCTION PARAMETERS" & vbCrLf)
    '    sb.Append("||CountryCode          : " & CountryCode & vbCrLf)
    '    sb.Append("||DvassSequenceNumber  : " & ProductId & vbCrLf)
    '    sb.Append("||CheckOutLocationCode : " & CheckOutLocationCode & vbCrLf)
    '    sb.Append("||CheckOutDate         : " & CheckOutDate.ToString & vbCrLf)
    '    sb.Append("||CheckInLocationCode  : " & CheckInLocationCode & vbCrLf)
    '    sb.Append("||CheckInDate          : " & CheckInDate.ToString & vbCrLf)
    '    sb.Append("||ProductShortName     : " & ProductShortName & vbCrLf)
    '    sb.Append("||NoOfVehiclesLookUp   : " & NoOfVehiclesLookUp & vbCrLf)
    '    sb.Append("||AgentId              : " & AgentId & vbCrLf)
    '    sb.Append("||BrdCode              : " & BrdCode & vbCrLf)

    '    ''just for testing..forcing R
    '    ''AvailCode = "R"

    '    Try
    '        Select Case AvailCode

    '            Case "R" ''call dvass


    '                sb.Append("||Inside select Statement case: 'R' DVASS :".ToUpper & vbCrLf)
    '                Dim dvassInformation As New DvassInformation(NoOfVehiclesLookUp)

    '                Dim xmlResult As New XmlDocument
    '                sb.AppendFormat("{0}{1}", vbCrLf, vbCrLf)
    '                sb.Append("||3.Calling dvassInformation.GetResult " & vbCrLf)
    '                sb.Append("||************************" & vbCrLf)
    '                xmlResult.LoadXml(dvassInformation.GetResult(CountryCode, _
    '                                                              ProductId, _
    '                                                              CheckOutLocationCode, _
    '                                                              CheckOutDate, _
    '                                                              CheckInLocationCode, _
    '                                                              CheckInDate, _
    '                                                              sbResult).OuterXml)

    '                If (isQuickAvail = True) Then
    '                    result = xmlResult.OuterXml
    '                Else
    '                    result = ExtractDvassResult(xmlResult.OuterXml, sbResult)
    '                End If

    '                sb.Append("||" & sbResult & vbCrLf)
    '                ''sb.Append("||RESULT            : " & result & vbCrLf)
    '                ''sb.Append("||:End Call DVASS : " & vbCrLf)

    '            Case "T"  ''call thrifty  

    '                sb.Append("||Inside select Statement case: 'T' Thrifty Webservice :" & vbCrLf)
    '                Dim sbThriftyLogText As New StringBuilder
    '                Dim htwThriftyLogTextWriter As New HtmlTextWriter(New System.IO.StringWriter(sbThriftyLogText))

    '                Dim oLogin As New Aurora.Thrifty.WS.Login()
    '                oLogin.ProceedLogin()
    '                Dim oThriftyAvail As New Aurora.Thrifty.WS.Avail(oLogin)
    '                result = oThriftyAvail.CheckVehicleStatus(CheckOutLocationCode, _
    '                                                        CheckInLocationCode, _
    '                                                        CheckOutDate, _
    '                                                        CheckInDate, _
    '                                                        ProductShortName, _
    '                                                        htwThriftyLogTextWriter)
    '                htwThriftyLogTextWriter.Flush()
    '                oLogin = Nothing
    '                oThriftyAvail = Nothing

    '                sb.Append("||RESULT            : " & result & vbCrLf)
    '                sb.Append("||:End Call Thrifty Webservice :" & vbCrLf)

    '            Case "E"  ''call Eurocar

    '                'sb.Append("||Inside select Statement case: 'E' EuropCar Webservice :" & vbCrLf)

    '                'Dim objXrsService As New XrsStandardService(Server.MapPath("~/App_Data/XrsClientConfiguration.xml"))
    '                'Dim europCarAvailability As XrsStandardRequest = objXrsService.NewRequest

    '                'Dim mappingError As String = ""
    '                ' ''sb.Append("||Mapping: " & vbCrLf)
    '                'Dim mapper As New EuropCarMapping(CheckOutLocationCode, CheckInLocationCode, CheckOutDate, CheckInDate, ProductShortName, BrdCode, AgentId, mappingError)
    '                'sb.Append("||CheckOutLocationCode: " & CheckOutLocationCode & " mapped to " & mapper.EuropCarPickup & vbCrLf)
    '                'sb.Append("||CheckInLocationCode: " & CheckInLocationCode & " mapped to " & mapper.EuropCarDropOff & vbCrLf)
    '                'sb.Append("||ProductId: " & ProductShortName & " mapped to " & mapper.EuropCarVehicle & vbCrLf)
    '                'sb.Append("||Exception: " & mappingError & vbCrLf)

    '                ' ''CheckOutDate = New DateTime(2012, 11, 5, 9, 0, 0)
    '                ' ''CheckInDate = New DateTime(2012, 11, 9, 19, 0, 0)


    '                'Dim objReservation As New XrsReservation(mapper.EuropCarPickup, _
    '                '                                         New DateTime(CheckOutDate.Year, CheckOutDate.Month, CheckOutDate.Day, CheckOutDate.Hour, CheckOutDate.Minute, CheckOutDate.Second), _
    '                '                                         mapper.EuropCarDropOff, _
    '                '                                         New DateTime(CheckInDate.Year, CheckInDate.Month, CheckInDate.Day, CheckInDate.Hour, CheckInDate.Minute, CheckInDate.Second))
    '                'objReservation.CallerName = mapper.EuropCarAgent
    '                'objReservation.CarCategory = mapper.EuropCarVehicle ''"ECMR


    '                'Dim xrsDriver As New XrsDriver()
    '                'Dim objResponse As XrsReservationResponse = europCarAvailability.GetQuote(objReservation, xrsDriver)

    '                'europCarAvailability.ExecuteAndClose()
    '                ' ''objResponse.DeserializeFromXml(result)

    '                'sb.Append("||RESULT            : " & IIf(objResponse.Status = 0, "OK", "Unavailable") & vbCrLf)
    '                'sb.Append("||:End EuropCar Webservice :" & vbCrLf)
    '                'result = IIf(objResponse.Status = 0, "OK", "Unavailable")

    '            Case "F"  ''Free Cell

    '                sb.Append("||Inside select Statement case: 'F':Free Sale " & vbCrLf)
    '                result = "Y"

    '            Case "X"  ''Stop Cell

    '                sb.Append("||Inside select Statement case: 'X':Stop Sale " & vbCrLf)
    '                result = "N"

    '            Case "M"  ''On Request

    '                sb.Append("||Inside select Statement case: 'M':On Request " & vbCrLf)
    '                result = "N"

    '            Case "Y"  ''available

    '                sb.Append("||Inside select Statement case: 'Y':Available " & vbCrLf)
    '                result = "Y"

    '            Case Else  ''other

    '                sb.Append("||:Else Part " & vbCrLf)
    '                result = NOT_VALID

    '        End Select

    '    Catch ex As Exception
    '        result = String.Concat("Error: ", "Error on '" & AvailCode, "' - ", ex.Message, " - ", ex.StackTrace)
    '        sb.Append("||Exception              : " & result & vbCrLf)
    '    End Try


    '    ''sb.Append("||************************  End" & vbCrLf)
    '    Logging.LogDebug("WSexecute: ", sb.ToString)

    '    sbResult = sb.ToString
    '    sb = Nothing
    '    Return result

    'End Function

    'Private Function ExtractDvassResult(xmlstring As String, _
    '                                    ByRef sbResult As String) As String
    '    Dim xmlDoc As New XmlDocument
    '    Dim message As String = String.Empty
    '    Dim status As String = String.Empty
    '    Dim result As String = String.Empty
    '    Dim sb As New StringBuilder
    '    sb.Append(sbResult)
    '    Try
    '        xmlDoc.LoadXml(xmlstring)

    '        ''this block is call for in the webservice
    '        ''we are interested only in Y or N
    '        ''sb.Append("||************************" & vbCrLf)
    '        sb.Append("||7.Calling ExtractDvassResult " & vbCrLf)
    '        sb.Append("||************************" & vbCrLf)
    '        If (Not xmlDoc.DocumentElement.SelectSingleNode("Status") Is Nothing) Then
    '            Logging.LogDebug("ExtractDvassResult: ", "this is a webservice call")
    '            sb.Append("||ExtractDvassResult  Origin : " & "Call came from WebService" & vbCrLf)
    '            With xmlDoc.DocumentElement
    '                message = .SelectSingleNode("Message").InnerText
    '                status = .SelectSingleNode("Status").InnerText
    '                result = .SelectSingleNode("Result").InnerText
    '            End With
    '            sb.Append("||Message : " & message & vbCrLf)
    '            ''sb.Append("||Status  : " & status & vbCrLf)
    '            ''sb.Append("||Result  : " & result & vbCrLf)

    '            If status.ToUpper().Equals("ERROR") Then
    '                If (Not String.IsNullOrEmpty(message)) Then
    '                    result = message
    '                Else
    '                    result = "N"
    '                End If
    '            Else
    '                If (Convert.ToBoolean(result) = True) Then
    '                    result = "Y"
    '                Else
    '                    result = "N"
    '                End If
    '            End If
    '        Else
    '            ''this is for aurora bookingprocess call
    '            Logging.LogDebug("ExtractDvassResult: ", "this is for aurora bookingprocess call")
    '            sb.Append("||ExtractDvassResult  Origin : " & "Call came from Aurora" & vbCrLf)
    '            result = xmlstring
    '        End If


    '    Catch ex As Exception
    '        Logging.LogError("ExtractDvassResult: Error ", String.Concat(ex.Message, " - ", ex.StackTrace))
    '        sb.Append("||Exception        : " & String.Concat(ex.Message, " - ", ex.StackTrace) & vbCrLf)
    '    End Try
    '    sb.Append("||ExtractDvassResult Return : " & result & vbCrLf)
    '    ''sb.Append("||************************" & vbCrLf)
    '    sbResult = sb.ToString
    '    Return result
    'End Function

    'Private Class DvassInformation

    '    Private Shared _NumberOfVehicle As Integer
    '    Public Shared Property NumberOfVehicle As Integer
    '        Get
    '            Return _NumberOfVehicle
    '        End Get
    '        Set(value As Integer)
    '            _NumberOfVehicle = value
    '        End Set
    '    End Property

    '    Sub New()
    '        NumberOfVehicle = 1
    '    End Sub

    '    Sub New(numberOfVehicleRequired As Integer)
    '        NumberOfVehicle = numberOfVehicleRequired
    '        ''get only 20 numbers of vehicle
    '        If (NumberOfVehicle > 20) Then NumberOfVehicle = 20
    '    End Sub

    '    Public Function GetResult(ByVal CountryCode As String, _
    '                              ByVal ProductId As String, _
    '                              ByVal CheckOutLocationCode As String, _
    '                              ByVal CheckOutDate As Date, _
    '                              ByVal CheckInLocationCode As String, _
    '                              ByVal CheckInDate As Date, _
    '                              ByRef sbResult As String) As XmlDocument

    '        Dim sReturnedMessage As String = String.Empty
    '        Dim xmlReturnMessage As XmlDocument = New XmlDocument
    '        Dim bResult As Boolean
    '        Dim iCountry, iCheckOutAMPM, iCheckInAMPM As Integer
    '        Dim NumberOfVehicleReturned As Integer = 0
    '        Dim sb As New StringBuilder
    '        sb.Append(sbResult)
    '        Dim sContent As String = ""
    '        Try

    '            iCountry = IIf(CountryCode = "AU", 0, 1)
    '            iCheckOutAMPM = IIf(CheckOutDate.TimeOfDay >= CDate("12:00:00").TimeOfDay, 1, 0)
    '            iCheckInAMPM = IIf(CheckInDate.TimeOfDay >= CDate("12:00:00").TimeOfDay, 1, 0)

    '            sb.Append("||FUNCTION PARAMETERS " & vbCrLf)
    '            sb.Append("||iCountry             : " & iCountry & vbCrLf)
    '            sb.Append("||ProductId            : " & ProductId & vbCrLf)
    '            sb.Append("||CheckOutLocationCode : " & CheckOutLocationCode & vbCrLf)
    '            sb.Append("||CheckOutDate         : " & CheckOutDate.ToString & vbCrLf)
    '            sb.Append("||CheckInLocationCode  : " & CheckInLocationCode & vbCrLf)
    '            sb.Append("||CheckInDate          : " & CheckInDate.ToString & vbCrLf)
    '            sb.Append("||ProductShortName     : " & ProductId & vbCrLf)
    '            sb.Append("||iCheckOutAMPM        : " & iCheckOutAMPM & vbCrLf)
    '            sb.Append("||iCheckInAMPM         : " & iCheckInAMPM & vbCrLf)
    '            sb.Append("||NumberOfVehicle(REQ) : " & NumberOfVehicle & vbCrLf)




    '            bResult = Aurora.Reservations.Services.AuroraDvass.IsWSDvassAvailable(iCountry, _
    '                                                                                  ProductId, _
    '                                                                                  CheckOutLocationCode, _
    '                                                                                  CheckOutDate, _
    '                                                                                  iCheckOutAMPM, _
    '                                                                                  CheckInLocationCode, _
    '                                                                                  CheckInDate, _
    '                                                                                  iCheckInAMPM, _
    '                                                                                  sReturnedMessage, _
    '                                                                                  NumberOfVehicle, _
    '                                                                                  NumberOfVehicleReturned, _
    '                                                                                  sbResult)

    '            sb.Append("||NumberOfVehicle(RES) : " & NumberOfVehicleReturned & vbCrLf)
    '            sb.Append("||" & sbResult & vbCrLf)
    '            xmlReturnMessage.LoadXml(sReturnedMessage)

    '            'If (sReturnedMessage.Contains(":") = True) Then
    '            '    sReturnedMessage = sReturnedMessage.Replace(":", "")
    '            'End If
    '            ''sb.Append("||ReturnedMessage         : " & sReturnedMessage & vbCrLf)

    '            'If (Not String.IsNullOrEmpty(sReturnedMessage) _
    '            '    AndAlso sReturnedMessage.Contains("Error") = False _
    '            '    AndAlso bResult = True) Then

    '            '    ''request came from booking process or aurora
    '            '    If (NumberOfVehicle > 1) Then
    '            '        xmlReturnMessage.LoadXml(sReturnedMessage)

    '            '    Else
    '            '        xmlReturnMessage.LoadXml("<Data>" & _
    '            '                           "<Status>DVASS CALL COMPLETE</Status>" & _
    '            '                           "<Result>True</Result>" & _
    '            '                           "<Message>" & NumberOfVehicleReturned & "</Message>" & _
    '            '                        "</Data>")

    '            '    End If
    '            'Else
    '            '    xmlReturnMessage.LoadXml("<Data>" & _
    '            '                           "<Status>DVASS CALL RESULT</Status>" & _
    '            '                           "<Result>False</Result>" & _
    '            '                           "<Message>" & sReturnedMessage & "</Message>" & _
    '            '                        "</Data>")
    '            '    sb.Append("||Exception         : " & xmlReturnMessage.OuterXml & vbCrLf)
    '            'End If



    '        Catch ex As Exception
    '            xmlReturnMessage.LoadXml("<Data>" & _
    '                                        "<Status>ERROR</Status>" & _
    '                                        "<Result>" & False & "</Result>" & _
    '                                        "<Message>" & ex.Message & "-" & sContent & "</Message>" & _
    '                                     "</Data>")
    '            sb.Append("||Exception         : " & xmlReturnMessage.OuterXml & vbCrLf)
    '        End Try


    '        sbResult = sb.ToString

    '        Return xmlReturnMessage
    '    End Function
    'End Class

    'Private Class EuropCarMapping
    '    ''rev: mia August 21,2012 - this is the europcar mapping for all NZ cars.
    '    ''                          today in history: flash Report in Philippines that Sec. Robredo's body found inside the cockpit,180 meters below Masbate's sea.
    '    ''                                            Sad tragedy to one brilliant guy in Philippine's politics. Death anniversary of Ninoy
    '    Private _Pickup As String
    '    Public ReadOnly Property EuropCarPickup As String
    '        Get
    '            Return _Pickup
    '        End Get
    '    End Property

    '    Private _dropoff As String
    '    Public ReadOnly Property EuropCarDropOff As String
    '        Get
    '            Return _dropoff
    '        End Get
    '    End Property


    '    Private _agent As String
    '    Public ReadOnly Property EuropCarAgent As String
    '        Get
    '            Return _agent
    '        End Get
    '    End Property

    '    Private _vehicle As String
    '    Public ReadOnly Property EuropCarVehicle As String
    '        Get
    '            Return _vehicle
    '        End Get
    '    End Property

    '    Sub New(sCkoLocCode As String, _
    '            sCkiLocCode As String, _
    '            dCkoWhen As Date, _
    '            dCkiWhen As Date, _
    '            sVehCode As String, _
    '            sBrdCode As String, _
    '            sAgnCode As String, _
    '            ByRef mappingError As String)
    '        Mapper(sCkoLocCode, sCkiLocCode, dCkoWhen, dCkiWhen, sVehCode, sBrdCode, sAgnCode, mappingError)
    '    End Sub


    '    Private Sub Mapper(sCkoLocCode As String, _
    '            sCkiLocCode As String, _
    '            dCkoWhen As Date, _
    '            dCkiWhen As Date, _
    '            sVehCode As String, _
    '            sBrdCode As String, _
    '            sAgnCode As String, _
    '            ByRef mappingError As String)

    '        Dim spName As String = "getThirdPartyInfo"

    '        Try
    '            '<EuropCarMapping>
    '            '    <PickupLocation></Pickup>
    '            '    <DropOffLocation></DropOff>
    '            '    <VehicleCode></ProductId>
    '            '</EuropCarMapping>

    '            Dim xmlDoc As XmlDocument
    '            xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc(spName, sCkoLocCode, sCkiLocCode, dCkoWhen, dCkiWhen, sVehCode, sBrdCode, sAgnCode)

    '            If (Not xmlDoc.DocumentElement.SelectSingleNode("EuropCarMapping/PickupLocation") Is Nothing) Then
    '                _Pickup = xmlDoc.DocumentElement.SelectSingleNode("EuropCarMapping/PickupLocation").InnerText
    '            End If

    '            If (Not xmlDoc.DocumentElement.SelectSingleNode("EuropCarMapping/DropOffLocation") Is Nothing) Then
    '                _dropoff = xmlDoc.DocumentElement.SelectSingleNode("EuropCarMapping/DropOffLocation").InnerText
    '            End If

    '            If (Not xmlDoc.DocumentElement.SelectSingleNode("EuropCarMapping/VehicleCode") Is Nothing) Then
    '                _vehicle = xmlDoc.DocumentElement.SelectSingleNode("EuropCarMapping/VehicleCode").InnerText
    '            End If


    '            mappingError = String.Empty

    '        Catch ex As Exception
    '            _Pickup = sCkoLocCode
    '            _dropoff = sCkiLocCode
    '            _vehicle = sVehCode
    '            mappingError = "EuropCarMapping.Mapper " & ex.Message
    '        End Try
    '    End Sub


    'End Class

    '<WebMethod(Description:="Method that call DVASS availability and return Aurora-Readable XML")> _
    'Public Function GetWSDvassXmlResult(ByVal CountryCode As String, _
    '                                   ByVal ProductId As String, _
    '                                   ByVal CheckOutLocationCode As String, _
    '                                   ByVal CheckOutDate As Date, _
    '                                   ByVal CheckInLocationCode As String, _
    '                                   ByVal CheckInDate As Date, _
    '                                   ByVal NumberOfVehicleTarget As Integer) As XmlDocument

    '    Dim sresult As String = String.Empty
    '    Dim xmlReturnMessage As XmlDocument = New XmlDocument

    '    Dim iCountry As Integer

    '    Logging.LogInformation("GetWSDvassXmlResult:".ToUpper, "start calling Aurora.Reservations.Services.AuroraDvass.GetDVASSXmlResult")
    '    Try

    '        iCountry = IIf(CountryCode = "AU", 0, 1)
    '        xmlReturnMessage.LoadXml(Aurora.Reservations.Services.AuroraDvass.GetDVASSXmlResult(iCountry, _
    '                                                                    ProductId, _
    '                                                                    CheckOutLocationCode, _
    '                                                                    CheckOutDate, _
    '                                                                    CheckInLocationCode, _
    '                                                                    CheckInDate, _
    '                                                                    NumberOfVehicleTarget))
    '        Logging.LogInformation("GetWSDvassXmlResult:".ToUpper, "end calling Aurora.Reservations.Services.AuroraDvass.GetDVASSXmlResult")

    '    Catch ex As Exception
    '        xmlReturnMessage.LoadXml("<Data>" & _
    '                                    "<Status>ERROR</Status>" & _
    '                                    "<Result>" & False & "</Result>" & _
    '                                    "<Message>" & ex.Message & "</Message>" & _
    '                                 "</Data>")
    '    End Try
    '    Logging.LogInformation("GetWSDvassXmlResult:".ToUpper, "RESULT: " & xmlReturnMessage.OuterXml)
    '    Return xmlReturnMessage

    'End Function

#End Region

#Region "rev:mia 04Dec2014 - CTX"

    Private Function GetErrorMessage(xmlDoc As XmlDocument) As String
        GetErrorMessage = String.Empty
        If (Not xmlDoc Is Nothing) Then
            If Not xmlDoc.SelectSingleNode("Error/Message") Is Nothing Then
                GetErrorMessage = xmlDoc.SelectSingleNode("Error/Message").InnerText
            End If
        End If
        Return GetErrorMessage
    End Function

    ''rev:mia 22July2015 - now this function can now be use for  checkin
    Private Function PrepareCheckInData(ByVal BookingId As String, _
                               ByVal RentalId As String, _
                               ByVal UserCode As String, _
                               ByVal RentalCountryCode As String, _
                               ByVal OdoIn As String, _
                               ByVal RegoNumber As String, _
                               ByVal UnitNumber As String, _
                               ByVal CKILocation As String) As XmlDocument

        Dim oXmlScreenData As XmlDocument = New XmlDocument

        Try
            oXmlScreenData = Aurora.Booking.Services.BookingCheckInCheckOut.GetDataForCheckInCheckOut(BookingId, RentalId, UserCode, 0, "", RentalCountryCode)
            If (Not String.IsNullOrEmpty(GetErrorMessage(oXmlScreenData))) Then
                Throw New Exception(String.Concat("", oXmlScreenData.SelectSingleNode("Error/Message").InnerText))
            End If

            Dim elem As XmlElement

            If (Not String.IsNullOrEmpty(CKILocation)) Then
                If (oXmlScreenData.DocumentElement.SelectSingleNode("RntCkiLocCode") Is Nothing) Then
                    elem = oXmlScreenData.CreateElement("RntCkiLocCode")
                    elem.InnerText = CKILocation
                    oXmlScreenData.DocumentElement.AppendChild(elem)
                Else
                    oXmlScreenData.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText = CKILocation
                End If
            End If


            If (oXmlScreenData.DocumentElement.SelectSingleNode("OddometerIn") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("OddometerIn")
                elem.InnerText = OdoIn
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("OddometerIn").InnerText = OdoIn
            End If


            If (oXmlScreenData.DocumentElement.SelectSingleNode("ChkInUnitNo") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("ChkInUnitNo")
                elem.InnerText = UnitNumber
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("ChkInUnitNo").InnerText = UnitNumber
            End If


            If (oXmlScreenData.DocumentElement.SelectSingleNode("CkiRego") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("CkiRego")
                elem.InnerText = RegoNumber
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("CkiRego").InnerText = RegoNumber
            End If


            Dim odoout As String = oXmlScreenData.DocumentElement.SelectSingleNode("OddometerOut").InnerText
            Dim ododiff As String = "0"
            If (Not String.IsNullOrEmpty(odoout)) Then
                ododiff = Convert.ToDouble(OdoIn) - Convert.ToDouble(odoout)
            End If
            If (oXmlScreenData.DocumentElement.SelectSingleNode("OddoDiff") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("OddoDiff")
                elem.InnerText = ododiff
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("OddoDiff").InnerText = ododiff
            End If

            With oXmlScreenData.DocumentElement
                Try

                    Dim locationcki As String = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
                    Dim dateckiHourtime As Integer = Convert.ToDateTime(.SelectSingleNode("RntCkiTime").InnerText.Trim()).Hour
                    Dim dateckiMintime As Integer = Convert.ToDateTime(.SelectSingleNode("RntCkiTime").InnerText.Trim()).Minute
                    Dim datecki As DateTime = Convert.ToDateTime(.SelectSingleNode("RntCkiWhen").InnerText.Trim() & " " & dateckiHourtime.ToString & ":" & dateckiMintime.ToString)
                    Dim adjustedDropoffTime As String = WebServiceHelper.GetBranchAdjustedDropoffTime(locationcki, datecki)
                    .SelectSingleNode("RntCkiTime").InnerText = adjustedDropoffTime

                Catch ex As Exception
                    .SelectSingleNode("RntCkiTime").InnerText = String.Concat(Now.Hour, ":", IIf(Now.Minute < 10, "0" & Now.Minute, Now.Minute))
                    Logging.LogError("PrepareCheckInData: Error ", ex.Message & ", " & ex.StackTrace)
                End Try


                .SelectSingleNode("Vehicle").InnerText = .SelectSingleNode("Vehicle").InnerText.Split("-"c)(0).Trim()
                ''rev:mia 16-november-2015 fixed for Tablet One way fee issue getting dropped - Live - drop off location getting change
                .SelectSingleNode("UserLocCode").InnerText = CKILocation
                .SelectSingleNode("YrLoc").InnerText = CKILocation
                .SelectSingleNode("DispLoc").InnerText = CKILocation
                .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
            End With


            If (Not String.IsNullOrEmpty(RegoNumber)) Then
                If (oXmlScreenData.DocumentElement.SelectSingleNode("Rego") Is Nothing) Then
                    elem = oXmlScreenData.CreateElement("Rego")
                    elem.InnerText = RegoNumber
                    oXmlScreenData.DocumentElement.AppendChild(elem)
                Else
                    oXmlScreenData.DocumentElement.SelectSingleNode("Rego").InnerText = RegoNumber
                End If
            End If


            If (Not String.IsNullOrEmpty(UnitNumber)) Then
                If (oXmlScreenData.DocumentElement.SelectSingleNode("UnitNo") Is Nothing) Then
                    elem = oXmlScreenData.CreateElement("UnitNo")
                    elem.InnerText = UnitNumber
                    oXmlScreenData.DocumentElement.AppendChild(elem)
                Else
                    oXmlScreenData.DocumentElement.SelectSingleNode("UnitNo").InnerText = UnitNumber
                End If
            End If

        Catch ex As Exception
            LogError("PrepareCheckInData: CheckIn Error: ", ex.Message & "," & ex.StackTrace)
            Dim errors As String = "<Error><Message>" & ex.Message & "</Message></Error>"
            oXmlScreenData.LoadXml(errors)
        End Try

        Return oXmlScreenData
    End Function

    ''rev:mia 22may2015 - modified to add all optional parameters
    Private Function PrepareCheckOutData(ByVal BookingId As String, _
                                 ByVal RentalId As String, _
                                 ByVal UserCode As String, _
                                 ByVal RentalCountryCode As String, _
                                 Optional ByVal RegoNumber As String = "", _
                                 Optional ByVal OdoOut As String = "", _
                                 Optional ByVal UnitNumber As String = "", _
                                 Optional ByVal fromCTX As Boolean = False, _
                                 Optional ByVal IsContinue As Boolean = False) As XmlDocument ''rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129


        Dim oXmlScreenData As XmlDocument = New XmlDocument
        Try
            oXmlScreenData = Aurora.Booking.Services.BookingCheckInCheckOut.GetDataForCheckInCheckOut(BookingId, RentalId, UserCode, 0, "", RentalCountryCode)
            If (Not String.IsNullOrEmpty(GetErrorMessage(oXmlScreenData))) Then
                Throw New Exception(String.Concat("", oXmlScreenData.SelectSingleNode("Error/Message").InnerText))
            End If

            Dim elem As XmlElement

            If (oXmlScreenData.DocumentElement.SelectSingleNode("UnitNoOld") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("UnitNoOld")
                elem.InnerText = String.Empty
                oXmlScreenData.DocumentElement.AppendChild(elem)
            End If

            If (oXmlScreenData.DocumentElement.SelectSingleNode("VehicleOld") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("VehicleOld")
                elem.InnerText = String.Empty
                oXmlScreenData.DocumentElement.AppendChild(elem)
            End If

            With oXmlScreenData.DocumentElement

                Try
                    Dim locationcko As String = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                    Dim datecko As DateTime = Convert.ToDateTime(.SelectSingleNode("RntCkoWhen").InnerText.Trim())
                    Dim adjustedPickupTime As String = WebServiceHelper.GetBranchAdjustedPickupTime(locationcko, datecko)
                    .SelectSingleNode("RntCkoTime").InnerText = adjustedPickupTime ''String.Concat(Now.Hour, ":", Now.Minute)

                    Dim locationcki As String = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
                    Dim dateckiHourtime As Integer = Convert.ToDateTime(.SelectSingleNode("RntCkiTime").InnerText.Trim()).Hour
                    Dim dateckiMintime As Integer = Convert.ToDateTime(.SelectSingleNode("RntCkiTime").InnerText.Trim()).Minute
                    Dim datecki As DateTime = Convert.ToDateTime(.SelectSingleNode("RntCkiWhen").InnerText.Trim() & " " & dateckiHourtime.ToString & ":" & dateckiMintime.ToString)
                    Dim adjustedDropoffTime As String = WebServiceHelper.GetBranchAdjustedDropoffTime(locationcki, datecki)
                    .SelectSingleNode("RntCkiTime").InnerText = adjustedDropoffTime

                Catch ex As Exception
                    .SelectSingleNode("RntCkoTime").InnerText = String.Concat(Now.Hour, ":", Now.Minute)
                    Logging.LogError("PrepareCheckOutData: Error ", ex.Message & ", " & ex.StackTrace)
                End Try



                .SelectSingleNode("Vehicle").InnerText = .SelectSingleNode("Vehicle").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("UserLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("RntCkiLocCode").InnerText = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("YrLoc").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("DispLoc").InnerText = .SelectSingleNode("DispLoc").InnerText.Split("-"c)(0).Trim()
            End With

            ''rev:mia 22may2015
            If (Not String.IsNullOrEmpty(RegoNumber)) Then
                If (oXmlScreenData.DocumentElement.SelectSingleNode("Rego") Is Nothing) Then
                    elem = oXmlScreenData.CreateElement("Rego")
                    elem.InnerText = RegoNumber
                    oXmlScreenData.DocumentElement.AppendChild(elem)
                Else
                    oXmlScreenData.DocumentElement.SelectSingleNode("Rego").InnerText = RegoNumber
                End If
            End If


            If (Not String.IsNullOrEmpty(OdoOut)) Then
                If (oXmlScreenData.DocumentElement.SelectSingleNode("OddometerOut") Is Nothing) Then
                    elem = oXmlScreenData.CreateElement("OddometerOut")
                    elem.InnerText = OdoOut
                    oXmlScreenData.DocumentElement.AppendChild(elem)
                Else
                    oXmlScreenData.DocumentElement.SelectSingleNode("OddometerOut").InnerText = OdoOut
                End If
            End If

            If (Not String.IsNullOrEmpty(UnitNumber)) Then
                If (oXmlScreenData.DocumentElement.SelectSingleNode("UnitNo") Is Nothing) Then
                    elem = oXmlScreenData.CreateElement("UnitNo")
                    elem.InnerText = UnitNumber
                    oXmlScreenData.DocumentElement.AppendChild(elem)
                Else
                    oXmlScreenData.DocumentElement.SelectSingleNode("UnitNo").InnerText = UnitNumber
                End If
            End If

            If (oXmlScreenData.DocumentElement.SelectSingleNode("Forced") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("Forced")
                ''rev:mia 24June2015-if its from CTX, then make it zero
                elem.InnerText = IIf(fromCTX, 0, 1)
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("Forced").InnerText = 1
            End If

            ''rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129
            If (oXmlScreenData.DocumentElement.SelectSingleNode("IsContinue") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("IsContinue")
                elem.InnerText = IIf(IsContinue, "1", "0")
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("IsContinue").InnerText = IIf(IsContinue, "1", "0")
            End If

        Catch ex As Exception
            LogError("Precheckout: CheckOut Error: ", ex.Message & "," & ex.StackTrace)
            Dim errors As String = "<Error><Message>" & ex.Message & "</Message></Error>"
            oXmlScreenData.LoadXml(errors)
        End Try

        Return oXmlScreenData
    End Function

    Private Function CheckOut(ByVal oXmlScreenData As XmlDocument, _
                            ByVal UserCode As String) As XmlDocument
        Dim oRetXml As System.Xml.XmlDocument
        Dim sErr As String = ""

        ''''rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129
        Dim isContinue As Boolean = False
        Dim screenValue As String = "0"
        If (Not oXmlScreenData.DocumentElement.SelectSingleNode("IsContinue") Is Nothing) Then
            isContinue = IIf(oXmlScreenData.DocumentElement.SelectSingleNode("IsContinue").InnerText = "1", True, False)
            If (isContinue = True) Then screenValue = "3"
        End If

        oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.ManageCheckOut(oXmlScreenData, _
                                UserCode, _
                                "SCI/PhoenixWS/CheckOut", _
                                screenValue, _
                                sErr, _
                                isContinue, _
                                String.Empty _
                            )

        Return oRetXml
    End Function

    ''rev:mia 22JUly2015 - added for checkin process
    Private Function CheckIn(ByVal oXmlScreenData As XmlDocument, _
                            ByVal UserCode As String, _
                            Optional ScreenValue As String = "0") As XmlDocument
        Dim oRetXml As System.Xml.XmlDocument
        Dim sErr As String = ""

        oRetXml = DataRepository.ManageCheckIn(oXmlScreenData, UserCode, "SCI/PhoenixWS/CheckIn", ScreenValue)

        Return oRetXml
    End Function

    Private Function IsAllowedToCheckout(RentalId As String) As Boolean
        Dim result As String = ""
        Try
            result = Aurora.Common.Data.ExecuteScalarSP("SCI_GetForceVehicle", RentalId)
            LogInformation("IsAllowedToCheckout  : ", "Force Vehicle: " & result)
        Catch ex As Exception
            LogError("ERROR: IsAllowedToCheckout  : ", result)
            Return False
        End Try
        Return IIf(result = "EMPTY", False, True)
    End Function

    <WebMethod(Description:="This method is going to be use by SCI after Agents verified Drivers")> _
    Public Function AutoCheckOut(ByVal BookingId As String, _
                                 ByVal RentalId As String, _
                                 ByVal UserCode As String, _
                                 ByVal RentalCountryCode As String) As String

        Dim SB As New StringBuilder
        SB.Append(vbCrLf & vbCrLf & vbCrLf & "-------------------AUTO CHECKOUT BLOCK START -------------------" & vbCrLf)


        Dim returnedvalue As String = String.Empty
        SB.Append(vbCrLf & vbCrLf & "Step #1: IsAllowedToCheckout: " & RentalId)

        If (Not IsAllowedToCheckout(RentalId)) Then
            SB.Append(vbCrLf & vbCrLf & "Step #1: IsAllowedToCheckout ERROR: " & "ERROR:Force Vehicle is empty")
            Return "ERROR:Force Vehicle is empty"
        End If


        SB.Append(vbCrLf & "Step #1:  PrepareCheckOutData: Parameters : BookingId : " & BookingId & ",RentalId:  " & RentalId & ", UserCode: " & UserCode & ", RentalCountryCode: " & RentalCountryCode & vbCrLf)
        Dim oScreenData As XmlDocument = PrepareCheckOutData(BookingId, RentalId, UserCode, RentalCountryCode)
        SB.Append(vbCrLf & "Step #1:  PrepareCheckOutData result: " & oScreenData.OuterXml & vbCrLf)



        If (Not String.IsNullOrEmpty(GetErrorMessage(oScreenData))) Then
            SB.Append(vbCrLf & "Step #1:  PrepareCheckOutData ERROR: " & oScreenData.SelectSingleNode("data/Error/Message").InnerText & vbCrLf)
            returnedvalue = String.Concat("ERROR: ", oScreenData.SelectSingleNode("data/Error/Message").InnerText)
        Else
            SB.Append(vbCrLf & "Step #2:  CheckOut Parameters: " & oScreenData.OuterXml & ", UserCode: " & UserCode & vbCrLf)
            oScreenData = CheckOut(oScreenData, UserCode)
            SB.Append(vbCrLf & "Step #2:  CheckOut result: " & oScreenData.OuterXml & vbCrLf)

            If (Not oScreenData.SelectSingleNode("data/Error/Message") Is Nothing) Then
                SB.Append(vbCrLf & "Step #2:  CheckOut ERROR: " & oScreenData.SelectSingleNode("data/Error/Message").InnerText & vbCrLf)
                returnedvalue = String.Concat("ERROR: ", oScreenData.SelectSingleNode("data/Error/Message").InnerText)
            Else
                SB.Append(vbCrLf & "Step #2:  CheckOut RESULT: " & "OK: Check-Out was successful!" & vbCrLf)
                returnedvalue = "OK: Check-Out was successful!"
            End If
        End If
        SB.Append(vbCrLf & "-------------------AUTO CHECKOUT BLOCK END -------------------" & vbCrLf)


        LogInformation("AutoCheckOut", SB.ToString)

        Return returnedvalue
    End Function

#End Region

#Region "rev:mia 11March2015- Part of B2B Auto generated Aurora Booking Confirmation"

    Private Function GetB2BAgentEmailAddress(parameter As String) As String
        Return "MANUEL.AGBAYANI@THLONLINE.COM"
    End Function

    <WebMethod(Description:="This Web Method will be use by the B2B where a confirmation is auto generated for a QN, NN or KK booking made online, and sent by email")> _
    Public Function B2BCreateAndSendconfirmation(ByVal RentalInfo As String, EmailAddress As String, isCustomer As Boolean) As String

        Const FXN_NAME As String = "B2BCreateAndSendconfirmation"

        If String.IsNullOrEmpty(RentalInfo) Then Return False
        If RentalInfo.Contains("-") = False Then Return False
        If RentalInfo.Split("-").Length = 1 Then Return False

        Dim reason As String = ""
        If (RentalInfo.Contains("|") = True) Then
            reason = RentalInfo.Split("|")(1)
            RentalInfo = RentalInfo.Split("|")(0)
        End If

        Dim xmlReturn As New XmlDocument
        Dim xmlReturnMessage As New XmlDocument


        Dim confirmationHtml As String

        Dim BookingId As String = RentalInfo.Split("-")(0)
        Dim RentalId As String = RentalInfo
        Dim RentalNumber As String = RentalInfo.Split("-")(1)

        Dim HeaderText As String = ""
        Dim FooterText As String = ""

        Dim AudienceType As String = "34CE97B1-5807-41C5-B043-5B10D43685C7" ''IIf(isCustomer, "7519EC4A-2601-4855-B703-CDD6DD22B108", "34CE97B1-5807-41C5-B043-5B10D43685C7")

        Const USER_CODE As String = "B2BUSER"
        Const PROGRAM_NAME As String = "PHOENIX_WS"
        Const RETURNED_STRING As String = "<Data><Status>{0}</Status><ConfId>{1}</ConfId><ConfXML>{2}</ConfXML></Data>"

        Dim targetAudience As String = AudienceType
        Dim formatting As String = RETURNED_STRING


        Try

            Dim sEmailId As String = EmailAddress ''DataRepository.GetCustomerEmail(RentalId)
            If String.IsNullOrEmpty(sEmailId) Then
                ''todo: get email agent if there is no EmailAddress
                ''sEmailId = GetB2BEmailAddress(agentcode) ''GetB2BAgentEmailAddress("")
                Throw New Exception("Email address is missing.")
            End If

            Dim sConfirmationId As String = System.Guid.NewGuid().ToString()
            Dim xmlConfirmationInfo As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(BookingId, "")

            For Each oNode As XmlNode In xmlConfirmationInfo.DocumentElement.SelectNodes("audienceType/audience")
                If oNode.SelectSingleNode("CodCode").InnerText.Trim().ToUpper() = AudienceType.Trim().ToUpper() Then
                    targetAudience = oNode.SelectSingleNode("CodId").InnerText.Trim().ToUpper()
                    Exit For
                End If
            Next


            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                        sConfirmationId, _
                        BookingId, _
                        RentalId, _
                        RentalNumber, _
                        targetAudience, _
                        HeaderText, _
                        FooterText, _
                        String.Empty, _
                        String.Empty, _
                        String.Empty, _
                        String.Empty, _
                        False, _
                        1, _
                        USER_CODE, _
                        USER_CODE, _
                        PROGRAM_NAME, _
                        INSERT_FLAG, _
                        Nothing, _
                        String.Empty, _
                        String.Empty)

            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("GEN046") Then
                Logging.LogError(FXN_NAME, "Error occured while saving confirmation data. CnfId = " & sConfirmationId)
                Return String.Format(formatting, "ERROR", sConfirmationId, xmlConfirmationInfo.OuterXml)
            End If


            xmlConfirmationInfo = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(BookingId, sConfirmationId)
            Dim confirmationXml As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(sConfirmationId, False)

            confirmationXml.LoadXml(confirmationXml.DocumentElement.InnerXml)
            confirmationXml.DocumentElement.SelectSingleNode("viewbutton").InnerText = "1"

            Dim transform As System.Xml.Xsl.XslTransform = New System.Xml.Xsl.XslTransform()
            transform.Load(Server.MapPath(CONFIRMATION_XSL_PATH))

            Using writer As System.IO.StringWriter = New System.IO.StringWriter
                transform.Transform(confirmationXml, Nothing, writer, Nothing)
                confirmationHtml = Server.HtmlDecode(writer.ToString())
            End Using

            Dim sHeaderText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/headnote").InnerText
            Dim sFooterText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/footnote").InnerText
            Dim bHighlight As Boolean = CBool(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/highlight").InnerText)
            Dim nIntegrityNo As Integer = CInt(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/integrityno").InnerText)

            Dim sEFaxNotify As String = confirmationXml.DocumentElement.SelectSingleNode("EFaxNotify").InnerText


            Dim sCustSurname As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/CustomerSurname").InnerText
            Dim sBookingRef As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/OurRef").InnerText
            Dim sEmailSubject As String = sCustSurname & " " & sBookingRef & " " & confirmationXml.DocumentElement.SelectSingleNode("doc/Header/ConfirmationTitle").InnerText


            Dim brandLogo As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText
            Dim slashlastindex As Integer = brandLogo.LastIndexOf("/")
            If (slashlastindex <> -1) Then
                If (brandLogo.Length > slashlastindex + 1) Then
                    brandLogo = brandLogo.Substring(slashlastindex + 1).Trim
                Else
                    brandLogo = brandLogo.Substring(slashlastindex).Trim
                End If

                brandLogo = brandLogo.Substring(0, brandLogo.LastIndexOf(".")).Trim
                brandLogo = IIf(brandLogo.IndexOf("MAC") <> -1, "MOTORHOMESANDCARS.COM", brandLogo)
            End If
            sEmailSubject = String.Concat(brandLogo, " -  ", sEmailSubject)


            Dim sFaxNumber As String = String.Empty
            Dim sSentBy As String = "Printing"



            If Not SendConfirmationSuccessfull( _
                 confirmationHtml, _
                 sEmailId, _
                 sEFaxNotify, _
                 sEmailSubject) Then
                Logging.LogError(FXN_NAME, "Unable to send confirmation e-mail for CnfId=" & sConfirmationId & " to e-mail id : " & sEmailId)
                Return String.Format(formatting, "ERROR", sConfirmationId, "Unable to send confirmation e-mail for CnfId=" & sConfirmationId & " to e-mail id : " & sEmailId)
            End If

            xmlReturn = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                          sConfirmationId, _
                          BookingId, _
                          IIf(RentalNumber.ToUpper().Trim().Equals("ALL"), RentalNumber, RentalId), _
                          RentalNumber, _
                          AudienceType, _
                          sHeaderText, _
                          sFooterText, _
                          sEmailId, _
                          sFaxNumber, _
                          sSentBy, _
                          "", _
                          bHighlight, _
                          nIntegrityNo, _
                          USER_CODE, _
                          USER_CODE, _
                          PROGRAM_NAME, _
                          UPDATE_FLAGE, _
                          Nothing, _
                          "", _
                          "")


            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") IsNot Nothing AndAlso xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText.ToUpper().Contains("GEN046") Then
                Logging.LogError(FXN_NAME, "Error occured while saving confirmation data. CnfId = " & sConfirmationId)
                Return String.Format(formatting, "ERROR", sConfirmationId, xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            End If

            InsertConfirmationHTML(sConfirmationId, confirmationHtml, USER_CODE, "")

            xmlReturnMessage.LoadXml(String.Format(formatting, "SUCCESS", sConfirmationId, confirmationXml.OuterXml))

            ''rev:mia 10-march-2016 https://thlonline.atlassian.net/browse/B2BWEB-197
            If (Not String.IsNullOrEmpty(reason)) Then
                WebServiceHelper.B2BCancellationAddNotes(RentalInfo, reason, USER_CODE)
            End If

        Catch ex As Exception

            xmlReturnMessage.LoadXml(String.Format(formatting, "ERROR", "", ex.Message & " - " & ex.StackTrace))

        End Try


        Return xmlReturnMessage.OuterXml

    End Function

    <WebMethod(Description:="This Web Method will be use by the B2B to get the Agent Email Address")> _
    Public Function B2BAgentEmailAddress(agentcode As String) As String
        Return GetB2BEmailAddress(agentcode)
    End Function

    <WebMethod(Description:="This Web Method will be use by the B2B (but will not store History in Aurora)  where a confirmation is auto generated for a QN, NN or KK booking made online, and sent by email")> _
    Public Function B2BSendConfirmation(emailAddress As String, subject As String, content As String, b2bagent As String, CopiedTo As String) As String
        Dim b2bsender As String = ""

        Try

            b2bsender = b2bagent
            Logging.LogInformation("B2BSendconfirmation", "Sender : " & b2bsender & " To " & emailAddress & " b2bagent " & b2bagent)
            Dim attachments As String = String.Empty
            ''b2bsender --EmailFrom
            ''emailaddress --EmailTo
            ''copiedto --CC
            If (WebServiceCommon.CreateMessage(b2bsender, emailAddress, IIf(String.IsNullOrEmpty(subject), "Customer Rental Confirmation", subject), content, CopiedTo, attachments) = True) Then
                ''do nothing
            Else
                Logging.LogError("B2BSendconfirmation", "Unable to send confirmation e-mail to/from : " & emailAddress & " / " & b2bsender)
            End If
        Catch ex As Exception
            Logging.LogError("B2BSendconfirmation", "Unable to send confirmation e-mail to/from : " & emailAddress & " / " & b2bsender & " Error: " & ex.Message & vbCrLf & ex.StackTrace)
            Return String.Concat("ERROR: B2B Sending email Confirmation failed because of this error ", ex.Message)
        End Try
        Return String.Concat("OK: Email sent to these address '", emailAddress & IIf(String.IsNullOrEmpty(CopiedTo), "", ";" & CopiedTo), "' and sender was '", b2bsender, "'")
    End Function


    Private Function GetB2BEmailAddress(agentcode As String) As String
        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("B2B_GetAgentEmailAddress", agentcode)
        If (Not String.IsNullOrEmpty(result)) Then
            Dim valid As Boolean = IsValidEmail(result)
            If Not valid Then
                Return "ERROR: Invalid/Multiple Email Address. Please contact Reservations"
            Else
                Return result
            End If
        Else
            Return "ERROR: Invalid/Empty Email Address. Please contact Reservations"
        End If
    End Function

    Private Function IsValidEmail(emailaddress As String) As Boolean
        ''check if its a multiple email
        Dim items() As String = emailaddress.Split("@")
        If (items.Length > 2) Then
            Return False
        End If
        Try
            Dim testit As Object = New System.Net.Mail.MailAddress(emailaddress)
        Catch ex As Exception
            Return False
        End Try

        Return True
    End Function
#End Region

#Region "rev:mia 22May2015 - CompletePickupProcess"
    <WebMethod(Description:="This method is going to be use by SCI for completing the Pickup/CheckOut process.(eg:RentalId = '3000140-1'")> _
    Public Function CompletePickupProcess(ByVal RentalId As String, _
                                 ByVal UserCode As String, _
                                 ByVal RentalCountryCode As String, _
                                 ByVal RegoNumber As String, _
                                 ByVal OdoOut As String, _
                                 ByVal UnitNumber As String, _
                                 ByVal IsContinue As Boolean) As String ''rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129

        Dim SB As New StringBuilder
        SB.Append(vbCrLf & vbCrLf & vbCrLf & "-------------------CompletePickupProcess BLOCK START -------------------" & vbCrLf)

        If (String.IsNullOrEmpty(RentalId)) Then
            Return "<Error><Message>RentalId is missing</Message></Error>"
        End If

        If (RentalId.Contains("-") = False) Then
            Return "<Error><Message>RentalId should be in this format '3000140-1'</Message></Error>"
        End If


        Dim BookingId As String = RentalId.Split("-")(0)
        Dim returnedvalue As String = String.Empty

        SB.Append(vbCrLf & "Step #1:  PrepareCheckOutData: Parameters : BookingId : " & BookingId & ",RentalId:  " & RentalId & ", UserCode: " & UserCode & ", RentalCountryCode: " & RentalCountryCode & ", RegoNumber: " & RegoNumber & ", OdoOut: " & OdoOut & ", UnitNumber: " & UnitNumber & vbCrLf)
        Dim oScreenData As XmlDocument = PrepareCheckOutData(BookingId, RentalId, UserCode, RentalCountryCode, RegoNumber, OdoOut, UnitNumber, True, IsContinue)
        SB.Append(vbCrLf & "Step #1:  PrepareCheckOutData result: " & oScreenData.OuterXml & vbCrLf)

        If (Not String.IsNullOrEmpty(GetErrorMessage(oScreenData))) Then
            SB.Append(vbCrLf & "Step #1:  PrepareCheckOutData ERROR: " & oScreenData.SelectSingleNode("data/Error/Message").InnerText & vbCrLf)
            returnedvalue = String.Concat("ERROR: ", oScreenData.SelectSingleNode("data/Error/Message").InnerText)
        Else
            SB.Append(vbCrLf & "Step #2:  CompletePickupProcess Parameters: " & oScreenData.OuterXml & ", UserCode: " & UserCode & vbCrLf)

            oScreenData = CheckOut(oScreenData, UserCode)
            SB.Append(vbCrLf & "Step #2:  CompletePickupProcess result: " & oScreenData.OuterXml & vbCrLf)

            If (Not oScreenData.SelectSingleNode("data/Error/Message") Is Nothing) Then
                SB.Append(vbCrLf & "Step #2:  CompletePickupProcess ERROR: " & oScreenData.SelectSingleNode("data/Error/Message").InnerText & vbCrLf)
                Dim typeWarning As String = oScreenData.SelectSingleNode("data/Error/Type").InnerText
                Dim ErrorOrWarningmessage As String = oScreenData.SelectSingleNode("data/Error/Message").InnerText
                returnedvalue = "<Error><Type>" + typeWarning + "</Type><Message>" + ErrorOrWarningmessage + "</Message></Error>"

            Else
                AimsUpdateLastOdometer(OdoOut, UnitNumber)
                returnedvalue = oScreenData.OuterXml
                SB.Append(vbCrLf & "Step #2:  CompletePickupProcess RESULT: FINISHED" & vbCrLf & returnedvalue)

            End If

        End If
        SB.Append(vbCrLf & "-------------------CompletePickupProcess BLOCK END -------------------" & vbCrLf)


        LogInformation("CompletePickupProcess", SB.ToString)

        Return returnedvalue
    End Function

    <WebMethod(Description:="This method is going to be use by SCI for completing the DropOff/CheckIn process.(eg:RentalId = '3000140-1'")> _
    Public Function CompleteCheckinProcess(ByVal RentalId As String, _
                                 ByVal UserCode As String, _
                                 ByVal RentalCountryCode As String, _
                                 ByVal CKILocation As String, _
                                 ByVal RegoNumber As String, _
                                 ByVal OdoIn As String, _
                                 ByVal UnitNumber As String, _
                                 ByVal ScreenValue As String) As String

        Dim SB As New StringBuilder
        SB.Append(vbCrLf & vbCrLf & vbCrLf & "-------------------CompleteCheckinProcess BLOCK START -------------------" & vbCrLf)

        If (String.IsNullOrEmpty(RentalId)) Then
            Return "<Error><Message>RentalId is missing</Message></Error>"
        End If

        If (RentalId.Contains("-") = False) Then
            Return "<Error><Message>RentalId should be in this format '3000140-1'</Message></Error>"
        End If

        Dim BookingId As String = RentalId.Split("-")(0)
        Dim returnedvalue As String = String.Empty

        SB.Append(vbCrLf & "Step #1:  PrepareCheckInData: Parameters : BookingId : " & BookingId & ",RentalId:  " & RentalId & ", UserCode: " & UserCode & ", RentalCountryCode: " & RentalCountryCode & ", RegoNumber: " & RegoNumber & ", OdoIN: " & OdoIn & ", UnitNumber: " & UnitNumber & vbCrLf)
        Dim oScreenData As XmlDocument = PrepareCheckInData(BookingId, RentalId, UserCode, RentalCountryCode, OdoIn, RegoNumber, UnitNumber, CKILocation)
        SB.Append(vbCrLf & "Step #1:  PrepareCheckInData result: " & oScreenData.OuterXml & vbCrLf)

        Dim blnOKToSave As Boolean = False
        Dim tempOScreenData As XmlDocument = oScreenData

        If (Not String.IsNullOrEmpty(GetErrorMessage(oScreenData))) Then
            SB.Append(vbCrLf & "Step #1:  PrepareCheckInData ERROR: " & oScreenData.SelectSingleNode("data/Error/Message").InnerText & vbCrLf)
            returnedvalue = String.Concat("ERROR: ", oScreenData.SelectSingleNode("data/Error/Message").InnerText)
        Else
            SB.Append(vbCrLf & "Step #2:  CompleteCheckinProcess Parameters: " & oScreenData.OuterXml & ", UserCode: " & UserCode & vbCrLf)

            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
                Try
                    oScreenData = CheckIn(oScreenData, UserCode, ScreenValue)

                    SB.Append(vbCrLf & "Step #2:  CompleteCheckinProcess result: " & oScreenData.OuterXml & vbCrLf)

                    returnedvalue = WebServiceHelper.GetErrorAndWarning(oScreenData)
                    SB.Append(vbCrLf & "Step #3:  CompleteCheckinProcess GetErrorAndWarning: " & returnedvalue & vbCrLf)
                    blnOKToSave = IIf(returnedvalue.IndexOf("<data>Check-In completed</data>") <> -1, True, False)

                    If (Not blnOKToSave) Then
                        oTransaction.RollbackTransaction()
                    Else
                        Dim bCrossHire As Boolean = tempOScreenData.DocumentElement.SelectSingleNode("CrossHr").InnerText

                        If (Not bCrossHire) Then
                            Dim sActivityType As String = "Rental"
                            Dim sExternalRef As String = tempOScreenData.DocumentElement.SelectSingleNode("BooRntNum").InnerText

                            Dim sEndLocation As String = CKILocation
                            Dim dtEnd As DateTime = CDate(tempOScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText & " " & tempOScreenData.DocumentElement.SelectSingleNode("RntCkiTime").InnerText)
                            Dim nEndOdometer As Integer = CLng(tempOScreenData.DocumentElement.SelectSingleNode("OddometerIn").InnerText)

                            Dim oAIMSObj As New AI.CMaster
                            Dim AIMSRetValue As Integer
                            SB.Append(vbCrLf & "F8_ActivityComplete: Parameters sActivityType: " & sActivityType & ",sExternalRef: " & sExternalRef & ", sEndLocation: " & sEndLocation & ", dtEnd: " & dtEnd & ", nEndOdometer: " & nEndOdometer)

                            AIMSRetValue = oAIMSObj.F8_ActivityComplete(sActivityType, sExternalRef, sEndLocation, dtEnd, nEndOdometer)

                            If AIMSRetValue <> 0 Then
                                returnedvalue = "<Error><Type>AIMS</Type><Role/><Message>" + Aurora.Booking.Services.BookingCheckInCheckOut.GetAimsMessage(AIMSRetValue) + "</Message></Error>"
                                oTransaction.RollbackTransaction()
                                SB.Append(vbCrLf & "Step #4:  CompleteCheckinProcess : " & returnedvalue & vbCrLf)
                            Else
                                DataRepository.UpdateRentalIntegrityNumber(RentalId)
                                oTransaction.CommitTransaction()
                            End If
                        Else

                            DataRepository.UpdateRentalIntegrityNumber(RentalId)
                            oTransaction.CommitTransaction()
                        End If
                        'Added by Nimesh on 20th Oct 2015 - Ref : https://thlonline.atlassian.net/browse/AURORA-391
                        Try



                            Dim dtEnd2 As DateTime
                            dtEnd2 = CDate(tempOScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText & " " & tempOScreenData.DocumentElement.SelectSingleNode("RntCkiTime").InnerText)

                            Dim tempTelematicsArgs As New Aurora.Telematics.Service.TelematicsArgs
                            tempTelematicsArgs.BookingID = BookingId
                            tempTelematicsArgs.UnitNumber = UnitNumber
                            tempTelematicsArgs.IsCheckOut = False
                            tempTelematicsArgs.CheckInOutDateTime = dtEnd2
                            tempTelematicsArgs.RegoNumber = RegoNumber

                            System.Threading.ThreadPool.QueueUserWorkItem(AddressOf WebServiceHelper.IMARDAtelematicsUpdateASYNC, tempTelematicsArgs)
                        Catch ex As Exception
                            SB.Append(vbCrLf & "Step Intermediate:  Calling Telematics Related Function - Did not Update Imarda for unit " + UnitNumber + " during checkin process" & vbCrLf & ex.Message)
                        End Try
                        'End: Added by Nimesh on 20th Oct 2015 - Ref : https://thlonline.atlassian.net/browse/AURORA-391
                    End If

                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    SB.Append(vbCrLf & "EXCEPTION  CompleteCheckinProcess RESULT: " & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
                    returnedvalue = "<Error><Type>ERROR</Type><Role/><Message>" + ex.Message + "</Message></Error>"
                    SB.Append(vbCrLf & "Step #5:  CompleteCheckinProcess : " & returnedvalue & vbCrLf)
                End Try

            End Using
        End If

        SB.Append(vbCrLf & "Step #6:  CompletePickupProcess RESULT: FINISHED" & vbCrLf & returnedvalue)
        SB.Append(vbCrLf & "-------------------CompleteCheckinProcess BLOCK END -------------------" & vbCrLf)


        LogInformation("CompleteCheckinProcess", SB.ToString)

        Return returnedvalue
    End Function

    Private Sub AimsUpdateLastOdometer(OddometerOut As Double, UnitNum As String)


        Logging.LogDebug("AuroraWebService", "AimsUpdateLastOdometer : OddometerOut : " & OddometerOut & " , UnitNum: " & UnitNum)

        Dim params(1) As Aurora.Common.Data.Parameter

        Try
            params(0) = New Aurora.Common.Data.Parameter("mOddometerOut", DbType.Double, OddometerOut, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("sUnitNum", DbType.String, UnitNum, Parameter.ParameterType.AddInParameter)

            Aurora.Common.Data.ExecuteOutputSP("TCX_AimsUpdateLastOdometer", params)


        Catch ex As Exception
            Logging.LogError("AuroraWebService", "AimsUpdateLastOdometer Error: " & ex.Message & " ," & ex.Source & "," & ex.StackTrace)
        End Try

    End Sub
#End Region

#Region "01July2015 - printing stuff"

    Private Function GetRAFilename(rntId As String) As String
        Dim result As String = ""
        Try
            result = Aurora.Common.Data.ExecuteScalarSP("TCX_GetRentalFilename", rntId)
        Catch ex As Exception
            LogInformation("SavingRentalFlags", ex.Message & "," & ex.StackTrace)
            Return "ERROR:" & ex.Message
        End Try
        Return result
    End Function

    Private Function GetPrintersAssignedToCKIBranch(ByVal rentalid As String) As String
        Dim DSprinters As New DataSet
        Dim result As String = ""
        Try
            result = Aurora.Common.Data.ExecuteScalarSP("TXC_GetRentalCheckOutLocation", rentalid)
            If (String.IsNullOrEmpty(result)) Then
                result = "ERROR: There is no printer attached to the check-out location code for this rental " & rentalid
            End If
        Catch ex As Exception
            LogInformation("GetPrintersAssignedToCKIBranch", ex.Message & vbCrLf & ex.StackTrace)
            result = "ERROR: " & ex.Message
        End Try
        Return result
    End Function

    <WebMethod(Description:="This method returns the url of SignedRentalAgreement")> _
    Public Function GetsignedRentalAgreement(rentalId As String, UserCode As String) As String

        Dim result As String = ""
        Try
            LogInformation("GetsignedRentalAgreement ", "-- checking printer from AuroraWebServiceLocation")

            Dim raFilename As String = GetRAFilename(rentalId)
            If (String.IsNullOrEmpty(raFilename)) Then
                Throw New Exception("RentalId is missing")
            End If

            Dim printername As String = WebServiceHelper.PrinterAssignedToUserBranch(UserCode) ''GetPrintersAssignedToCKOBranch(rentalId)

            If (String.IsNullOrEmpty(printername) Or printername.IndexOf("ERROR") <> -1) Then
                Return printername
            End If



            Dim urlAmazon As String = RAgreement.PrintRentalAgreementV2(rentalId, raFilename)


            If (urlAmazon.IndexOf("http") <> -1) Then
                Dim filepdf As String = urlAmazon ''.Split("?")(0)
                If (Not String.IsNullOrEmpty(filepdf)) Then
                    result = RAPrinterSelection.SendPDFToPrinter(filepdf, printername, rentalId)
                    ''https://thlonline.atlassian.net/browse/CSC-938 As a CSR user I need to be able to see in the Aurora history log when a rental agreement is printed.
                    WebServiceHelper.GenerateLogsForRentalAgreementPrinting(rentalId, UserCode, "PHOENIX_WS-SIGNED")
                Else
                    result = "ERROR: This URL is not valid '" & filepdf & "'"
                End If
            Else
                If (urlAmazon.IndexOf("ERROR") <> -1) Then
                    result = urlAmazon
                Else
                    result = "ERROR: This URL is not valid '" & urlAmazon & "'"
                End If

            End If

        Catch ex As Exception
            result = "ERROR:" & ex.Message
            LogInformation("GetsignedRentalAgreement Error in Phoenix ", ex.Source & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return result
    End Function
#End Region

#Region "manny's test"
    <WebMethod(Description:="Testing for Token")> _
    Public Function GetTokenV2() As String
        Dim client_id As String = ConfigurationManager.AppSettings("client_id")
        Dim client_secret As String = ConfigurationManager.AppSettings("client_secret")
        Dim tokenUrl As String = ConfigurationManager.AppSettings("tokenUrl")
        Dim auroraUserName As String = ConfigurationManager.AppSettings("auroraUserName")
        Dim auroraPassword As String = ConfigurationManager.AppSettings("auroraPassword")
        Dim bodytext As String = String.Format("grant_type=password&username={0}&password={1}&client_id={2}&client_secret={3}", auroraUserName, auroraPassword, client_id, client_secret)
        Return UserToken.GetToken(tokenUrl, "POST", bodytext)
    End Function
#End Region

#Region "rev:mia 21Aug2015 - Add saved Rental agreement link in Aurora"
    <WebMethod(Description:="This method is for AURORA only. It returns the url of RentalAgreement")> _
    Public Function RentalAgreementUrlFromAmazon(rentalId As String, UserCode As String, ProgramName As String) As String

        Dim result As String = ""
        Try
            LogInformation("GetsignedRentalAgreement ", "-- checking printer from AuroraWebServiceLocation")
            Dim raFilename As String = GetRAFilename(rentalId)
            If (String.IsNullOrEmpty(raFilename)) Then
                Dim logsresult As String = WebServiceHelper.GenerateLogsForRentalAgreementPrinting(rentalId, UserCode, ProgramName)
                Throw New Exception("RentalId is missing")
            End If

            Dim urlAmazon As String = RAgreement.PrintRentalAgreementV2(rentalId, raFilename)

            Logging.LogInformation("URLAMAZON: ", urlAmazon)
            If (urlAmazon.IndexOf("http") <> -1) Then
                Dim filepdf As String = urlAmazon ''.Split("?")(0)
                If (Not String.IsNullOrEmpty(filepdf)) Then
                    Logging.LogInformation("FILEPDF: ", filepdf)
                    If (RAPrinterSelection.SendPDFToAurora(filepdf, rentalId) = True) Then
                        result = RAPrinterSelection.MapRentalPath(filepdf, rentalId)
                    Else
                        result = "ERROR: This URL is not valid '" & filepdf & "'"
                    End If
                Else
                    result = "ERROR: This URL is not valid '" & filepdf & "'"
                End If
            Else
                If (urlAmazon.IndexOf("ERROR") <> -1) Then
                    result = urlAmazon
                Else
                    result = "ERROR: This URL is not valid '" & urlAmazon & "'"
                End If

            End If

        Catch ex As Exception
            result = "ERROR:" & ex.Message
            LogInformation("GetsignedRentalAgreement Error in Phoenix ", ex.Source & vbCrLf & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        Return result
    End Function
#End Region

#Region "rev:mia 24Aug2015 - get server location for pickup and drop off"

    <WebMethod(Description:="Check if printer is alive")> _
    Public Function CheckPrinterIfValid(printerName As String) As String
        Return RAPrinterSelection.IsValidPrinter(printerName)
    End Function
#End Region

#Region "rev:mia 29Aug2015 - As a CSR staff I should be able to print RA as receipt after CI - API"

    <WebMethod(Description:="This method returns get the Aurora Rental Agreement")> _
    Public Function MasterRentalAgreement(ByVal RentalInfo As String, UserCode As String, ProgramName As String) As String

        Const xslRentalAgreement As String = "xsl/PrintRentalagreement.xsl"
        Dim HTMLoutput As String = "ERROR"

        Try

            Dim xmlRentalAgreementData As New XmlDocument
            xmlRentalAgreementData = Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement(RentalInfo)

            xmlRentalAgreementData.LoadXml("<Data>" & xmlRentalAgreementData.DocumentElement.InnerXml & "</Data>")

            Dim settings As XmlWriterSettings = New XmlWriterSettings()
            settings.OmitXmlDeclaration = True

            Dim oTransform As New XslCompiledTransform
            oTransform.Load(Server.MapPath(xslRentalAgreement))


            HTMLoutput = xmlRentalAgreementData.OuterXml

            Dim writer As New StringWriter()
            oTransform.Transform(xmlRentalAgreementData, Nothing, writer)
            HTMLoutput = WebServiceHelper.AppendHtmlStructure(writer.ToString())

            writer.Close()

            Dim printername As String = WebServiceHelper.PrinterAssignedToUserBranch(UserCode) ''GetPrintersAssignedToCKIBranch(RentalInfo)
            If (String.IsNullOrEmpty(printername) Or printername.IndexOf("ERROR") <> -1) Then
                Return printername
            End If

            Dim logsresult As String = WebServiceHelper.GenerateLogsForRentalAgreementPrinting(RentalInfo, UserCode, ProgramName)
            If (Not logsresult.Contains("ERROR")) Then
                LogInformation("MasterRentalAgreement confirmationHtml", HTMLoutput)
                If (RAPrinterSelection.PrintRaw(CType(printername, String), HTMLoutput)) Then
                    Return "OK"
                Else
                    Return "ERROR: Sending Rental Agreement to " & printername
                End If
            Else
                Return "ERROR: Generating Rental Agreement Printing History"
            End If

        Catch ex As Exception
            Return "ERROR " & ex.Message
        End Try

        Return "OK"

    End Function

#End Region

#Region "rev:mia 16-november-2015 fixed for Tablet One way fee issue getting dropped - Live - drop off location getting change"

    Public Function BuyExperienceConfirmation(ByVal BookingId As String, ByVal RentalId As String, ByVal RentalNumber As String, _
                                       ByVal AudienceType As String, ByVal HeaderText As String, ByVal FooterText As String, _
                                       ByVal UserCode As String, ByVal GetConfirmationXML As Boolean, RequestOrigin As String) As XmlDocument

        ConfirmationRequestOrigin = RequestOrigin
        Return CreateConfirmation(RentalId.Split("-")(0), RentalId, _
                                                                                            RentalId.Split("-")(1), _
                                                                                            "7519EC4A-2601-4855-B703-CDD6DD22B108", _
                                                                                            String.Empty, _
                                                                                            String.Empty, _
                                                                                            UserCode, _
                                                                                            True)

    End Function
    '''changes
    ''' rev:mia 16-feb-2016 https://thlonline.atlassian.net/browse/AURORA-709 - As a Phoenix developer I want to pass email address to BuyExperience method so i can send email to different email adress
    ''' 
    <WebMethod(Description:="This method will create a new Experience Call Tickets")> _
    Public Function BuyExperience(RentalId As String, _
                                  ExpId As String, _
                                  ExpItemId As String, _
                                  ExpCallTktQty As String, _
                                  ChannelReferrer As String, _
                                  CCLastFourDigits As String, _
                                  ExpiryYear As String, _
                                  CBPhone As String, _
                                  CBPreferredDate As String, _
                                  ExPreferredDate As String, _
                                  Message As String, _
                                  UserId As String, _
                                  PaymentAmt As String, _
                                  PaymentSurchargeAmt As String, _
                                  BillingTokenId As String, _
                                  EmailAddress As String) As String


        Dim number As Integer
        Dim result As Boolean
        Dim resultWs As String
        Dim sb As New StringBuilder
        Dim email As String = ""
        Dim voucher As String = ""
        Dim expCallTktId As String = ""
        Dim expCallId As String = ""
        Dim billingtoken As String = ""
        Dim expCallTktTotal As String = ""
        Dim ExpCallTktTotalSurchargeForAddPayment As String = ""
        Dim paymentResult As String = ""
        Dim Status As String = ""
        Dim msg As String = ""
        Dim AuthCode As String = ""
        Dim DpsTxnRef As String = ""
        Dim ConfirmationId As String = ""
        Dim EFaxNotify As String = ""
        Dim objPaymentResult As XmlDocument
        Dim IsfreeSaleWithOutCredit As String = ""
        Dim CodCode As String = ""

        If (ChannelReferrer.ToUpper = "EXPERIENCECATALOG") Then
            ChannelReferrer = "ExperienceCatalog"
        ElseIf (ChannelReferrer.ToUpper = "BRANCH") Then
            ChannelReferrer = "Branch"
        End If

        If (String.IsNullOrEmpty(RentalId)) Then
            Return "<Data><Status>ERROR</Status><Message>RentalId is missing</Message></Data>"
        End If

        If (String.IsNullOrEmpty(ExpId)) Then
            Return "<Data><Status>ERROR</Status><Message>ExpId is missing</Message></Data>"
        Else
            result = Int32.TryParse(ExpId, number)
            If (Not result) Then
                Return "<Data><Status>ERROR</Status><Message>ExpId is expecting a number</Message></Data>"
            End If
        End If

        Dim isFreeSale As String = WebServiceHelper.IsFreeSale(ExpId)
        If (isFreeSale.Contains("ERROR") = True) Then
            Return "<Data><Status>ERROR</Status><Message>Cannot determine if its a free sale or pre-book. Check ExpId: " & ExpId & "</Message></Data>"
        End If

        If (String.IsNullOrEmpty(ExpItemId)) Then
            Return "<Data><Status>ERROR</Status><Message>ExpItemId is missing</Message></Data>"
        Else
            result = WebServiceHelper.IsValidNumberArray(ExpItemId)
            If (Not result) Then
                Return "<Data><Status>ERROR</Status><Message>ExpItemId is expecting a number or series of numbers separated with comma</Message></Data>"
            End If
            ExpItemId = WebServiceHelper.AppendComma(ExpItemId)
        End If

        If (String.IsNullOrEmpty(ExpCallTktQty)) Then
            Return "<Data><Status>ERROR</Status><Message>ExpCallTktQty is missing</Message></Data>"
        Else
            result = WebServiceHelper.IsValidNumberArray(ExpCallTktQty)
            If (Not result) Then
                Return "<Data><Status>ERROR</Status><Message>ExpCallTktQty is expecting a number or series of numbers separated with comma</Message></Data>"
            End If
            ExpCallTktQty = WebServiceHelper.AppendComma(ExpCallTktQty)
        End If

        If (String.IsNullOrEmpty(ChannelReferrer)) Then
            Return "<Data><Status>ERROR</Status><Message>ChannelReferrer is missing</Message></Data>"
        End If

        If (isFreeSale = "YES") Then

            ''CREDIT CARD INFORMATION IS REQUIRED
            If (String.IsNullOrEmpty(CCLastFourDigits)) Then
                ''rev:mia 3-feb-2016 https://thlonline.atlassian.net/browse/PHOENIXWS-183 - Experiences booking not creating request if the credit card has failed 3 times
                ''if 3 failed booking attempts then make it CALLBACK

                ''part of aurora-709, validations for last four digits will be remove
                'Try
                '    Dim returnValue As DateTime = DateTime.Parse(ExPreferredDate)
                '    ExPreferredDate = returnValue.ToString()
                'Catch ex As Exception
                '    Return "<Data><Status>ERROR</Status><Message>ExPreferredDate is invalid</Message></Data>"
                'End Try
            Else
                ''part of aurora-709, validations for last four digits will be remove
                'If (CCLastFourDigits.Length < 4) Then
                '    Return "<Data><Status>ERROR</Status><Message>CCLastFourDigits must have four numbers</Message></Data>"
                'End If

                'result = Int32.TryParse(CCLastFourDigits, number)
                'If (Not result) Then
                '    Return "<Data><Status>ERROR</Status><Message>CCLastFourDigits must be a number</Message></Data>"
                'End If

                If (String.IsNullOrEmpty(ExpiryYear)) Then
                    Return "<Data><Status>ERROR</Status><Message>ExpiryYear is missing</Message></Data>"
                End If

                If (String.IsNullOrEmpty(PaymentSurchargeAmt)) Then
                    Return "<Data><Status>ERROR</Status><Message>PaymentSurchargeAmt is missing</Message></Data>"
                End If
                result = WebServiceHelper.IsValidDecimalArray(PaymentSurchargeAmt)
                If (Not result) Then
                    Return "<Data><Status>ERROR</Status><Message>PaymentSurchargeAmt is expecting a number or series of numbers separated with comma</Message></Data>"
                End If
                PaymentSurchargeAmt = WebServiceHelper.AppendComma(PaymentSurchargeAmt)

                If (String.IsNullOrEmpty(BillingTokenId)) Then
                    Return "<Data><Status>ERROR</Status><Message>BillingTokenId is missing</Message></Data>"
                End If
                result = Int32.TryParse(BillingTokenId, number)
                If (Not result) Then
                    Return "<Data><Status>ERROR</Status><Message>BillingTokenId is expecting a number</Message></Data>"
                Else
                    If (number < 0) Then
                        Return "<Data><Status>ERROR</Status><Message>BillingTokenId is expecting a number greater than zero</Message></Data>"
                    End If
                End If
            End If

        Else
            If (String.IsNullOrEmpty(CBPhone)) Then
                Return "<Data><Status>ERROR</Status><Message>Phone is missing</Message></Data>"
            End If


            If (String.IsNullOrEmpty(CBPreferredDate)) Then
                Return "<Data><Status>ERROR</Status><Message>CBPreferredDate is missing</Message></Data>"
            End If

            Try
                Dim returnValue As DateTime = DateTime.Parse(CBPreferredDate)
                CBPreferredDate = returnValue.ToString()
            Catch ex As Exception
                Return "<Data><Status>ERROR</Status><Message>CBPreferredDate is invalid</Message></Data>"
            End Try

            Try
                Dim returnValue As DateTime = DateTime.Parse(ExPreferredDate)
                ExPreferredDate = returnValue.ToString()
            Catch ex As Exception
                Return "<Data><Status>ERROR</Status><Message>ExPreferredDate is invalid</Message></Data>"
            End Try


        End If

        If (String.IsNullOrEmpty(UserId)) Then
            Return "<Data><Status>ERROR</Status><Message>UserId is missing</Message></Data>"
        End If

        If (ExpItemId.Split(",").Length <> ExpCallTktQty.Split(",").Length) Then
            Return "<Data><Status>ERROR</Status><Message>ExpCallTktQty does not match the number of item in ExpItemId</Message></Data>"
        End If


        'Dim EmailRegex As New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,7}$")
        'If (Not EmailRegex.IsMatch(EmailAddress)) Then
        '    Throw New Exception("<Data><Status>ERROR</Status><Message>EmailAddress is invalid</Message></Data>")
        'End If
        'Dim isEmailExists As String = WebServiceHelper.CheckEmailIfExist(RentalId.Split("-")(0), RentalId, "", "")
        'If (isEmailExists.IndexOf("ERROR") = 0) Then
        '    Throw New Exception("<Data><Status>ERROR</Status><Message>EmailAddress not found</Message></Data>")
        'End If

        If (isFreeSale = "YES") Then
            If (Not String.IsNullOrEmpty(CCLastFourDigits)) Then
                If (ExpItemId.Split(",").Length <> PaymentSurchargeAmt.Split(",").Length) Then
                    Return "<Data><Status>ERROR</Status><Message>PaymentSurchargeAmt does not match the number of item in ExpItemId</Message></Data>"
                End If
            End If
        End If


        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                resultWs = WebServiceHelper.CheckEmailAndUpdateIfBlank(RentalId.Split("-")(0), RentalId, UserId, EmailAddress)
                If (resultWs.Contains("ERROR") = True) Then
                    Throw New Exception(resultWs)
                End If


                resultWs = WebServiceHelper.BuyExperience(RentalId, _
                                       ExpId, _
                                       ExpItemId, _
                                       ExpCallTktQty, _
                                       ChannelReferrer, _
                                       CCLastFourDigits, _
                                       ExpiryYear, _
                                       CBPhone, _
                                       CBPreferredDate, _
                                       ExPreferredDate, _
                                       Message, _
                                       UserId, _
                                       PaymentAmt, _
                                       PaymentSurchargeAmt, _
                                       BillingTokenId)

                If (resultWs.Contains("ERROR") = True) Then
                    Throw New Exception(resultWs)
                Else
                    If (String.IsNullOrEmpty(resultWs)) Then
                        Throw New Exception("Buy Experience failed.Check your log files")
                    End If
                    If (resultWs.Contains("OK") = True) Then
                        If (resultWs.Contains("|") = True) Then
                            email = resultWs.Split("|")(1)
                            voucher = resultWs.Split("|")(0).Replace("OK:", "").Trim
                            If (voucher.StartsWith(",")) Then
                                voucher = voucher.Substring(1)
                            End If
                            expCallTktId = resultWs.Split("|")(2).Trim
                            expCallId = resultWs.Split("|")(3).Trim
                            billingtoken = resultWs.Split("|")(4).Trim
                            expCallTktTotal = resultWs.Split("|")(5).Trim
                            ExpCallTktTotalSurchargeForAddPayment = resultWs.Split("|")(6).Trim
                            PaymentSurchargeAmt = ExpCallTktTotalSurchargeForAddPayment
                            IsfreeSaleWithOutCredit = resultWs.Split("|")(7).Trim

                            Try
                                If (String.IsNullOrEmpty(PaymentSurchargeAmt)) Then
                                    PaymentSurchargeAmt = "0"
                                End If
                                expCallTktTotal = CDec(expCallTktTotal) + CDec(PaymentSurchargeAmt)

                                ''REV:MIA 18-JAN-2016 - as a Concierge Team I want to be notified by email when a OnRequest experience is purchased
                                CodCode = resultWs.Split("|")(8).Trim
                            Catch ex As Exception
                            End Try
                            email = WebServiceHelper.CompareEmailAndMakeItAsReceiver(RentalId, email)
                        Else
                            Throw New Exception(resultWs)
                        End If



                        If (isFreeSale = "YES") Then
                            If (resultWs.Contains("|") = True) Then
                                ''ADD PAYMENT
                                If (Not String.IsNullOrEmpty(billingtoken)) Then
                                    '1. Create Payment
                                    paymentResult = WebServiceHelper.AddPayment(RentalId, UserId, expCallTktTotal, PaymentSurchargeAmt, CCLastFourDigits, ExpiryYear, AuthCode, DpsTxnRef)
                                    objPaymentResult = Nothing
                                    objPaymentResult = New XmlDocument
                                    objPaymentResult.LoadXml(paymentResult)
                                    Status = objPaymentResult.DocumentElement.SelectSingleNode("Status").InnerText

                                    If (Status = "ERROR") Then
                                        msg = objPaymentResult.DocumentElement.SelectSingleNode("Message").InnerText
                                        Throw New Exception(msg)
                                    End If

                                    '2. Create DPS
                                    Dim txntype As String = IIf(CDec(expCallTktTotal) < 0, "Refund", "Purchase")
                                    Dim data As New PaymentExpressWSData
                                    With data
                                        .Amount = WebServiceHelper.FormatWSAmount(expCallTktTotal).Replace("-", "")
                                        .DpsBillingId = billingtoken
                                        .TxnType = txntype
                                        .TxnRef = RentalId
                                        .DpsTxnRef = RentalId

                                    End With
                                    paymentResult = WebServiceHelper.SubmitTransaction(data, RentalId, UserId)
                                    objPaymentResult = New XmlDocument
                                    objPaymentResult.LoadXml(paymentResult)
                                    Status = objPaymentResult.DocumentElement.SelectSingleNode("status").InnerText
                                    msg = objPaymentResult.DocumentElement.SelectSingleNode("msg").InnerText

                                    If (Status = "ERROR") Then
                                        Throw New Exception(msg)
                                    Else
                                        AuthCode = objPaymentResult.DocumentElement.SelectSingleNode("AuthCode").InnerText
                                        DpsTxnRef = objPaymentResult.DocumentElement.SelectSingleNode("DpsTxnRef").InnerText
                                    End If
                                Else
                                    If (IsfreeSaleWithOutCredit = "N") Then
                                        Throw New Exception("BillingToken was not found in this Rental Id " & RentalId)
                                    End If
                                    voucher = ""
                                    email = ""
                                End If

                                sb.Append("<Data>")
                                sb.Append("<Status>OK</Status>")
                                sb.Append("<Message>New ExperienceCall and ExperienceCallTickets was created.</Message>")
                                If (IsfreeSaleWithOutCredit = "N") Then
                                    sb.Append("<FreeSale>YES</FreeSale>")
                                Else
                                    sb.Append("<FreeSale>NO</FreeSale>")
                                End If

                                sb.AppendFormat("<ExperiencesCalls>{0}</ExperiencesCalls>", expCallId)
                                sb.AppendFormat("<ExperiencesCallTicket>{0}</ExperiencesCallTicket>", expCallTktId)
                                sb.AppendFormat("<Voucher>{0}</Voucher>", voucher)
                                sb.AppendFormat("<Email>{0}</Email>", email)

                                sb.Append("<DpsInfo>")
                                If (IsfreeSaleWithOutCredit = "N") Then
                                    sb.Append(objPaymentResult.OuterXml.Replace("root", "dps"))
                                End If
                                sb.Append("</DpsInfo>")
                                sb.Append("</Data>")
                            End If
                        Else
                            sb.Append("<Data>")
                            sb.Append("<Status>OK</Status>")
                            sb.Append("<Message>New ExperienceCall and ExperienceCallTickets was created.</Message>")
                            sb.Append("<FreeSale>NO</FreeSale>")
                            sb.AppendFormat("<ExperiencesCalls>{0}</ExperiencesCalls>", expCallId)
                            sb.AppendFormat("<ExperiencesCallTicket>{0}</ExperiencesCallTicket>", expCallTktId)
                            sb.Append("<Voucher/>")
                            sb.AppendFormat("<Email>{0}</Email>", email)

                            sb.Append("<DpsInfo/>")
                            sb.Append("</Data>")
                        End If
                        resultWs = sb.ToString
                    End If

                    ''GENERATE XML TO GET THE CALL AND EXPERIENCE TICKETS
                    oTransaction.CommitTransaction()

                    If (isFreeSale = "YES" And IsfreeSaleWithOutCredit = "N" And CodCode <> "OnRequest") Then
                        ConfirmationRequestOrigin = ""
                        Dim xmlConfirmation As XmlDocument = BuyExperienceConfirmation(RentalId.Split("-")(0), RentalId, _
                                                                                            RentalId.Split("-")(1), _
                                                                                            "7519EC4A-2601-4855-B703-CDD6DD22B108", _
                                                                                            String.Empty, _
                                                                                            String.Empty, _
                                                                                            UserId, _
                                                                                            True, _
                                                                                            "BuyExperience")
                        Status = xmlConfirmation.DocumentElement.SelectSingleNode("Status").InnerText
                        If (Status = "ERROR") Then
                            msg = xmlConfirmation.DocumentElement.SelectSingleNode("Message").InnerText
                            Throw New Exception(msg)
                        End If







                    Else
                        ''REV:MIA 18-JAN-2016 - as a Concierge Team I want to be notified by email when a OnRequest experience is purchased
                        If (CodCode = "OnRequest") Then

                            WebServiceHelper.SendVouchers(CStr(ConfigurationManager.AppSettings("ExperienceOnrequestSenderEmail")), CStr(ConfigurationManager.AppSettings("ExperienceOnrequestEmail")), RentalId.Split("-")(0), RentalId, UserId, False)
                        End If
                    End If
                End If
            Catch ex As Exception
                resultWs = "<Data><Status>ERROR</Status><Message>" & ex.Message.Replace("ERROR:", "") & "</Message></Data>"
                If (Not oTransaction.IsClosed) Then oTransaction.RollbackTransaction()
            End Try
        End Using

        Logging.LogError("BuyExperience", resultWs.ToString())
        Return resultWs

    End Function

    '' Private BrandFooterText As String
    'Private _brandFooterText As String
    'Private Property BrandFooterText As String
    '    Get
    '        Return _brandFooterText
    '    End Get
    '    Set(value As String)
    '        Select Case value.ToUpper
    '            Case "B"
    '                _brandFooterText = "Britz"
    '            Case "Y"
    '                _brandFooterText = "Mighty"
    '            Case "M"
    '                _brandFooterText = "Maui"
    '            Case "U"
    '                _brandFooterText = "United"
    '            Case "A"
    '                _brandFooterText = "Alpha"
    '            Case "Q"
    '                _brandFooterText = "Kea"
    '            Case Else
    '                _brandFooterText = "THL"
    '        End Select
    '    End Set
    'End Property

#End Region

#Region "rev:mia https://thlonline.atlassian.net/browse/PHOENIXWS-201 - As a CSR app developer I want send Email copy of Receipt rental Agreement"
    <WebMethod(Description:="This will get the Email Confirmation from Amazon and will be send as an attachment")> _
    Public Function EmailRentalAgreementFromS3(rentalid As String, usercode As String, programname As String, EmailFrom As String, EmailTo As String, Subject As String) As String

        ''a little bit of hack here. When CustomerEmail = 'GetRentalFromAurora', it will trigger the
        ''retrieving logic for Rental inside aurora...previously, it was email but a couple changes happened
        Dim FirstName As String = String.Empty
        Dim result As String
        Dim callAurora As Boolean = IIf(EmailTo = "GetRentalFromAurora", True, False)
        EmailTo = WebServiceHelper.GetEmailAndFirstName(rentalid, FirstName)

        If (callAurora) Then
            result = EmailRentalAgreementAurora(rentalid, EmailFrom, EmailTo, Subject)
        Else

            result = RAPrinterSelection.MapRentalPath("", rentalid)
            If (File.Exists(result)) Then
                LogInformation("EmailRentalAgreementFromS3", "PDF Exist in " & result)
            Else
                result = RentalAgreementUrlFromAmazon(rentalid, usercode, programname)
            End If

            If (result.IndexOf("ERROR") <> -1) Then
                Return "<Data><Status>ERROR</Status><Message>" & result & "</Message></Data>"
            End If

            ''rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176
            result = WebServiceHelper.CreateMessageWithAttachment(EmailFrom, EmailTo, result, Subject, FirstName, rentalid)
        End If
        Return result
    End Function


    Public Function EmailRentalAgreementAurora(rentalid As String, _
                                               EmailFrom As String, _
                                               EmailTo As String, _
                                               Subject As String) As String
        Dim result As String = WebServiceHelper.RentalAgreement(rentalid)
        ''rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176
        Return WebServiceHelper.CreateMessage(EmailFrom, EmailTo, Subject, result, rentalid)

    End Function
#End Region

#Region "rev:mia 08-march-2016 https://thlonline.atlassian.net/browse/AURORA-777"
    Private _ConfirmationRequestOrigin As String
    Private Property ConfirmationRequestOrigin As String
        Get
            Return _ConfirmationRequestOrigin
        End Get
        Set(value As String)
            _ConfirmationRequestOrigin = value
        End Set
    End Property
#End Region

#Region "rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129"
    <WebMethod(Description:="Update Loyalty Cardnumber")> _
    Public Function UpdLoyaltyNumber(rentallId As String, loyaltycardnumber As String) As String
        Return WebServiceHelper.UpdLoyaltyNumber(rentallId, loyaltycardnumber)
    End Function
#End Region

End Class
