Imports Microsoft.VisualBasic
Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet


Public Class ProductUserControl
    Inherits AuroraUserControl

    Public Const NewListItemText As String = "(new)"
    Public Const PrdIdParam As String = "prdId"
    Public Const SapIdParam As String = "sapId"
    Public Const TabParam As String = "tab"
    Public Const TbsIdParam As String = "tbsId"
    Public Const PtbIdParam As String = "ptbId"

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.ProductMaintenance)
        End Get
    End Property

    Private _productDataSet As ProductDataSet
    Public ReadOnly Property ProductDataSet() As ProductDataSet
        Get
            Return _productDataSet
        End Get
    End Property

    Private _saleableProductRow As SaleableProductRow
    Public ReadOnly Property SaleableProductRow() As SaleableProductRow
        Get
            Return _saleableProductRow
        End Get
    End Property

    ' Product Non-Availability Enhancement - Added by Shoel on 12.8.8
    Private _productRow As ProductRow
    Public ReadOnly Property ProductRow() As ProductRow
        Get
            Return _productRow
        End Get
    End Property
    ' Product Non-Availability Enhancement - Added by Shoel on 12.8.8

    Public Overridable Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow)
        Me._productDataSet = productDataSet
        Me._saleableProductRow = saleableProductRow
        Me._productRow = saleableProductRow.ProductRow
    End Sub

    ' Product Non-Availability Enhancement - Added by Shoel on 12.8.8
    Public Overridable Sub InitProductUserControl(ByVal productDataSet As ProductDataSet, ByVal saleableProductRow As SaleableProductRow, ByVal productRow As ProductRow)
        Me._productDataSet = productDataSet
        Me._saleableProductRow = saleableProductRow
        Me._productRow = productRow
    End Sub
    ' Product Non-Availability Enhancement - Added by Shoel on 12.8.8


    Private Shared Function AddUrlParam(ByVal page As Page, ByRef first As Boolean, ByVal paramName As String, ByVal paramValue As String) As String
        If String.IsNullOrEmpty(paramValue) Then Return ""
        Dim result As String = ""
        If first Then
            result &= "?"
            first = False
        Else
            result &= "&"
        End If
        result += paramName & "=" & page.Server.UrlEncode(paramValue)
        Return result
    End Function


    Public Shared Function MakeProductUrl(ByVal page As Page, Optional ByVal prdId As String = Nothing) As String
        Dim first As Boolean = True
        Return page.ResolveUrl("~/Product/Product.aspx") _
         & AddUrlParam(page, first, PrdIdParam, prdId)
    End Function


    Public Shared Function MakeSaleableProductUrl( _
     ByVal page As Page, _
     Optional ByVal prdId As String = Nothing, _
     Optional ByVal sapId As String = Nothing, _
     Optional ByVal tabName As String = Nothing, _
     Optional ByVal tbsId As String = Nothing, _
     Optional ByVal ptbId As String = Nothing) As String
        Dim first As Boolean = True
        Return page.ResolveUrl("~/Product/SaleableProduct.aspx") _
         & AddUrlParam(page, first, PrdIdParam, prdId) _
         & AddUrlParam(page, first, SapIdParam, sapId) _
         & AddUrlParam(page, first, TabParam, tabName) _
         & AddUrlParam(page, first, TbsIdParam, tbsId) _
         & AddUrlParam(page, first, PtbIdParam, ptbId)
    End Function


End Class
