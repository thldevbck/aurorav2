﻿Imports Aurora.Common
Imports Aurora.Common.Data
Imports Aurora.Common.Logging
Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Xml
Imports Aurora.BookingExperiences.Service
Imports Aurora.Telematics.Service.Services
Imports Aurora.Telematics.Service
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Xml.Xsl
Imports System.IO
Imports WS.Utility

Namespace WS.Helper
    Public Class WebServiceHelper

#Region "rev:mia 21Aug2015 - stripped data"
        Public Shared Function CheckInXmlData(oscreendata As XmlDocument) As String
            Dim returnedvalue As String = ""
            returnedvalue = oscreendata.OuterXml.Replace("<data>", "").Replace("</data>", "")
            If (returnedvalue.Contains("<data>") = True) Then
                returnedvalue = returnedvalue.Replace("<data>", "")
            End If
            If (returnedvalue.Contains("</data>") = True) Then
                returnedvalue = returnedvalue.Replace("</data>", "")
            End If

            Return "<Error><Type>ERROR</Type><Message>" + returnedvalue + "</Message></Error>"
        End Function

        Public Shared Function CheckInXmlDataAndReturnText(oscreendata As XmlDocument) As String
            Dim returnedvalue As String = ""
            returnedvalue = oscreendata.OuterXml.Replace("<data>", "").Replace("</data>", "")
            If (returnedvalue.Contains("<data>") = True) Then
                returnedvalue = returnedvalue.Replace("<data>", "")
            End If
            If (returnedvalue.Contains("</data>") = True) Then
                returnedvalue = returnedvalue.Replace("</data>", "")
            End If

            Return returnedvalue
        End Function
#End Region


#Region "rev:mia 24Aug2015 - get server location for pickup and drop off"

        Public Shared Function GetBranchAdjustedPickupTime(location As String, currentdate As DateTime) As String
            Dim parameters As String = String.Format("'{0}','{1} {2}:{3}'", location, currentdate.ToShortDateString, Now.Hour, Now.Minute)
            Dim returnedvalue As DateTime = Convert.ToDateTime(Aurora.Common.Data.ExecuteScalarSQL("select dbo.GEN_getAdjustedTimeForLocation(" & parameters & ")"))
            Return returnedvalue.Hour & ":" & IIf(returnedvalue.Minute < 10, "0" & returnedvalue.Minute, returnedvalue.Minute)
        End Function

        Public Shared Function GetBranchAdjustedDropoffTime(location As String, currentdate As DateTime) As String
            Dim parameters As String = String.Format("'{0}','{1} {2}:{3}'", location, currentdate.ToShortDateString, currentdate.Hour, currentdate.Minute)
            Dim returnedvalue As DateTime = Convert.ToDateTime(Aurora.Common.Data.ExecuteScalarSQL("select dbo.GEN_getAdjustedTimeForLocation(" & parameters & ")"))
            Return returnedvalue.Hour & ":" & IIf(returnedvalue.Minute < 10, "0" & returnedvalue.Minute, returnedvalue.Minute)
        End Function

#End Region


#Region "01SEPT2015 - printing stuff FOR DROPOFF"
        Public Shared Function GetPrintersAssignedToCKOBranch(ByVal rentalid As String) As String
            Dim DSprinters As New DataSet
            Dim result As String = ""
            Try
                result = Aurora.Common.Data.ExecuteScalarSP("TCX_PrintersAssignedToCKOBranch", rentalid)
                If (String.IsNullOrEmpty(result)) Then
                    result = "ERROR: There is no printer attached to the check-out location code for this rental " & rentalid
                End If
            Catch ex As Exception
                LogInformation("GetPrintersAssignedToCKOBranch", ex.Message & vbCrLf & ex.StackTrace)
                result = "ERROR: " & ex.Message
            End Try
            Return result
        End Function

#End Region

#Region "rev:mia 03sept2015-As a CSR developer I want to receive Prompt flag as part of check-in process"
        ''Sept 02 2015 - My Lola and Mareng Lady went to eternal rest. Will be missing them
        Public Shared Function GetErrorAndWarning(oScreenData As XmlDocument) As String
            Dim sb As New StringBuilder
            Dim sbLogging As New StringBuilder
            Dim type As String = "ERROR"
            Dim role As String = ""
            Dim message As String = ""
            Dim blnOKToSave As Boolean = False

            Dim returnedvalue As String = "<Error><Type>{0}</Type><Role>{1}</Role><Message>{2}</Message></Error>"
            ''Error/
            ''Msg/
            ''<data>POPUP/Vehicle has travelled more than 3,000 KMs on this hire. Do you still want to continue?</data>
            sbLogging.Append(vbCrLf & "-------------GetErrorAndWarning START-----------------" & vbCrLf)
            sbLogging.AppendFormat("GetErrorAndWarning: oScreenData : {0}{1}", oScreenData.OuterXml, vbCrLf)
            If (oScreenData.OuterXml.ToLower.Equals("<data></data>")) Then
                sbLogging.Append("Condition 1: OK" & vbCrLf)
                sb.Append("<data>Check-In completed</data>")
                blnOKToSave = True
            Else

                If Not oScreenData.InnerText.ToUpper().Equals("SUCCESS") And _
                          Not oScreenData.InnerText.Trim().Equals(String.Empty) And _
                          Not oScreenData.InnerText.ToUpper().Contains("EXTENSION CREATED") And _
                          Not oScreenData.InnerText.ToUpper().Contains("MSG/") _
                       Then
                    If (oScreenData.OuterXml.ToUpper().Contains("ERRORS")) Then
                        type = "ERROR"
                        message = oScreenData.InnerText
                        sbLogging.AppendFormat("Condition 2: looked for ERRORS: {0}{1}", message, vbCrLf)
                        blnOKToSave = False
                    Else
                        message = CheckInXmlDataAndReturnText(oScreenData)
                        sbLogging.AppendFormat("Condition 3: CheckInXmlDataAndReturnText removed <data> and </data>: {0}{1}", message, vbCrLf)
                        type = "ERROR"
                        If (message.IndexOf("Error/") <> -1) Then
                            message = message.Replace("Error/", "")
                            sbLogging.AppendFormat("Condition 4: looked Error/ : {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        ElseIf (message.IndexOf("Msg/") <> -1) Then
                            message = message.Replace("Msg/", "")
                            sbLogging.AppendFormat("Condition 5: looked Msg/ : {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        ElseIf (message.IndexOf("POPUP/") <> -1) Then
                            type = "PROMPT"
                            message = message.Replace("POPUP/", "")
                            sbLogging.AppendFormat("Condition 6: looked POPUP/ : {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        ElseIf (message.IndexOf("POPUP") <> -1) Then
                            Dim slashindex As Integer = message.IndexOf("/")
                            message = message.Substring(slashindex + 1)
                            sbLogging.AppendFormat("Condition 6A: looked POPUP : {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        Else
                            message = message
                            sbLogging.AppendFormat("Condition 6B: looked POPUP : {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        End If

                    End If
                Else
                    type = "ERROR"
                    If (Not oScreenData.SelectSingleNode("data/Error/Message") Is Nothing) Then
                        type = oScreenData.SelectSingleNode("data/Error/Type").InnerText.ToUpper
                        message = oScreenData.SelectSingleNode("data/Error/Message").InnerText
                        sbLogging.AppendFormat("Condition 7: looked data/Error/Message : {0}{1}", message, vbCrLf)
                        blnOKToSave = False
                    ElseIf oScreenData.InnerText.ToUpper().Contains("EXTENSION CREATED") And oScreenData.InnerText.ToUpper().Contains("MSG/") Then
                        message = CheckInXmlDataAndReturnText(oScreenData)
                        sbLogging.AppendFormat("Condition 8: {0}{1}", message, vbCrLf)
                        blnOKToSave = False
                    ElseIf Not oScreenData.InnerText.ToUpper().Contains("EXTENSION CREATED") And Not oScreenData.InnerText.ToUpper().Contains("MSG/") Then
                        message = oScreenData.OuterXml
                        If (String.Equals(message, "<data></data>")) Then
                            sb.Append("<data>Check-In completed</data>")
                            blnOKToSave = True
                            sbLogging.AppendFormat("Condition 9: {0}{1}", message, vbCrLf)
                        Else
                            message = CheckInXmlDataAndReturnText(oScreenData)
                            sbLogging.AppendFormat("Condition 10: {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        End If
                    Else
                        message = oScreenData.OuterXml
                        If (String.Equals(message, "<data></data>")) Then
                            sb.Append("<data>Check-In completed</data>")
                            blnOKToSave = True
                            sbLogging.AppendFormat("Condition 11 {0}{1}", message, vbCrLf)
                        Else
                            message = CheckInXmlDataAndReturnText(oScreenData)
                            sbLogging.AppendFormat("Condition 12 {0}{1}", message, vbCrLf)
                            blnOKToSave = False
                        End If
                    End If
                End If
            End If

            If (Not blnOKToSave) Then
                sb.AppendFormat(returnedvalue, type, role, message)
            End If


            sbLogging.Append("-------------GetErrorAndWarning END-----------------" & vbCrLf)
            Logging.LogInformation("GetErrorAndWarning OUTPUT", sbLogging.ToString)
            Return sb.ToString
        End Function

#End Region

#Region "rev:NYT 20OCT2015 -Ref:https://thlonline.atlassian.net/browse/AURORA-391 - Aurora / I360 - check-in method is not clearing the bookingID as before "

        Private Shared _connectedSessionID As Guid

        Public Shared Property ConnectedSessionID As Guid
            Get
                Return _connectedSessionID
            End Get
            Set(value As Guid)
                _connectedSessionID = value
            End Set
        End Property


        Public Shared Sub IMARDAtelematicsUpdateASYNC(ByVal state As Object)
            Dim sb As New StringBuilder
            Dim isCheckout As Boolean = DirectCast(state, TelematicsArgs).IsCheckOut
            Dim bookingId As String = DirectCast(state, TelematicsArgs).BookingID
            Dim unitNumber As String = DirectCast(state, TelematicsArgs).UnitNumber
            Dim regoNumber As String = DirectCast(state, TelematicsArgs).RegoNumber
            Dim checkInOutDatetime As DateTime = DirectCast(state, TelematicsArgs).CheckInOutDateTime

            sb.AppendFormat("IsCheckOut: {0}{1}", isCheckout, vbCrLf)
            sb.AppendFormat("BookingId: {0}{1}", bookingId, vbCrLf)
            sb.AppendFormat("UnitNumber: {0}{1}", unitNumber, vbCrLf)
            sb.AppendFormat("Rego: {0}{1}", regoNumber, vbCrLf)
            Dim endPoint As String = ""
            ''Dim TelematicsGUID As String = Aurora.Common.Data.ExecuteScalarSP("GetTelematicsGUID", UnitNumber)

            Try
                Dim TelematicsGUID As String = Aurora.Common.Data.ExecuteScalarSQL("SELECT TelematicsGUID FROM aimsprod..FleetAsset WHERE UnitNumber = " + unitNumber).ToString
                sb.AppendFormat("TelematicsGUID: {0}{1}", TelematicsGUID, vbCrLf)

                If Not String.IsNullOrEmpty(TelematicsGUID) Then

                    Dim vehSer As VehicleService = New VehicleService()

                    Dim conSer As ConnectionService = New ConnectionService()
                    Dim userName As String = ConfigurationManager.AppSettings("UserName").ToString()
                    Dim userPassword As String = ConfigurationManager.AppSettings("Password").ToString()
                    Try
                        ConnectedSessionID = conSer.Login(userName, userPassword)
                    Catch ex As Exception

                        Dim exMessage As String = ex.Message
                    End Try
                    If ConnectedSessionID.Equals(Guid.Empty) Then
                        sb.AppendFormat("Your Login to Telematics was not successful: {0}", vbCrLf)
                    Else
                        vehSer.UpdateVehicleByGUID(TelematicsGUID, IIf(isCheckout, bookingId, String.Empty), ConnectedSessionID)
                        Dim jobSer As JobService = New JobService
                        Dim jobDateTime As DateTime

                        If DateTime.TryParse(checkInOutDatetime, jobDateTime) Then
                            If isCheckout Then
                                jobSer.CreateJobForVehicle(TelematicsGUID, bookingId, regoNumber, jobDateTime, ConnectedSessionID)
                            Else
                                jobSer.CreateCheckInJobForVehicle(TelematicsGUID, "CheckIn", jobDateTime, ConnectedSessionID)
                            End If
                        Else
                            'lblStatus.Text = "[" + txtJobDateTime.Text + "] is not a valid date time"
                            sb.AppendFormat("Message: {0} will not be called. TelematicsGuid is empty {1}", endPoint, vbCrLf)
                            Return
                        End If
                    End If
                Else
                    sb.AppendFormat("Message: {0} will not be called. TelematicsGuid is empty {1}", endPoint, vbCrLf)
                End If
            Catch ex As Exception
                sb.AppendFormat("Error: {0}  {1}", ex.Message & "," & ex.StackTrace, vbCrLf)
            End Try
            Logging.LogDebug("IMARDAtelematicsUpdate: ", sb.ToString)
        End Sub

#End Region


#Region "''rev:mia 21Sept2015 - As a Phoenix user I should be able to pass user code to printing function in aurora"
        Public Shared Function GenerateLogsForRentalAgreementPrinting(RentalInfo As String, UserCode As String, ProgramName As String) As String
            Dim params(13) As Aurora.Common.Data.Parameter
            Dim BookingId As String = RentalInfo.Split("-")(0)
            Dim RentalId As String = RentalInfo


            Dim ChangeEvent As String = ""

            ''rev:mia 21Sept2015 - As a Phoenix user I should be able to pass user code to printing function in aurora
            'If (UserCode = "SelfCKISYS" And ProgramName = "PHOENIX_WS") Then
	    If (ProgramName = "PHOENIX_WS") Then
                ChangeEvent = "TABLETPRINT"
            ElseIf (ProgramName = "PHOENIX_WS-SIGNED") Then
                ChangeEvent = "TABLETPRINT-SIGNED"
            Else
                ChangeEvent = "AURORAPRINT"
            End If
            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
                Try
                    params(0) = New Aurora.Common.Data.Parameter("sBooId", DbType.String, BookingId, Parameter.ParameterType.AddInParameter)
                    params(1) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Parameter.ParameterType.AddInParameter)
                    params(2) = New Aurora.Common.Data.Parameter("sBpdId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
                    params(3) = New Aurora.Common.Data.Parameter("sChangeEvent", DbType.String, ChangeEvent, Parameter.ParameterType.AddInParameter)

                    params(4) = New Aurora.Common.Data.Parameter("bCreateRentalHistory", DbType.Boolean, True, Parameter.ParameterType.AddInParameter)
                    params(5) = New Aurora.Common.Data.Parameter("bGetRental", DbType.Boolean, False, Parameter.ParameterType.AddInParameter)
                    params(6) = New Aurora.Common.Data.Parameter("bGetPayments", DbType.Boolean, False, Parameter.ParameterType.AddInParameter)
                    params(7) = New Aurora.Common.Data.Parameter("bGetAmounts", DbType.Boolean, False, Parameter.ParameterType.AddInParameter)
                    params(8) = New Aurora.Common.Data.Parameter("bGetVehicle", DbType.Boolean, False, Parameter.ParameterType.AddInParameter)
                    params(9) = New Aurora.Common.Data.Parameter("sDataType", DbType.String, False, Parameter.ParameterType.AddInParameter)

                    params(10) = New Aurora.Common.Data.Parameter("sVehSapId", DbType.String, DBNull.Value, Parameter.ParameterType.AddInParameter)
                    params(11) = New Aurora.Common.Data.Parameter("UserCode", DbType.String, UserCode, Parameter.ParameterType.AddInParameter)
                    params(12) = New Aurora.Common.Data.Parameter("AddPrgmName", DbType.String, ProgramName, Parameter.ParameterType.AddInParameter)
                    params(13) = New Aurora.Common.Data.Parameter("sRnhId", DbType.String, 64, Parameter.ParameterType.AddOutParameter)

                    Aurora.Common.Data.ExecuteOutputSP("RES_manageRentalHistory", params)
                    Logging.LogDebug("AuroraWebService", "Calling GenerateLogsForRentalAgreementPrinting : " & params(13).Value)

                    oTransaction.CommitTransaction()
                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    Logging.LogError("AuroraWebService", "GenerateLogsForRentalAgreementPrinting Error: " & ex.Message & " ,  " & ex.StackTrace)
                    Return "ERROR:" & ex.Message
                End Try
            End Using
            Return "OK"
        End Function

        Public Shared Function AppendHtmlStructure(content As String) As String
            Dim sb As New StringBuilder()
            sb.AppendFormat("{0}", "<html>")
            sb.AppendFormat("{0}", "<head>")
            sb.AppendFormat("{0}", "<title>Rental Agreement</title>")
            sb.AppendFormat("{0}", "</head>")
            sb.AppendFormat("{0}", "<body>")
            sb.AppendFormat("{0}", content.Replace("<br>", ""))
            sb.AppendFormat("{0}", "</body>")
            sb.AppendFormat("{0}", "</html>")

            Dim replacethis As String = "xmlns:fo=""http://www.w3.org/1999/XSL/Format"""
            sb.Replace(replacethis, "") ''.Replace("<!DOCTYPE>", "")

            Return sb.ToString
        End Function

        Public Shared Function PrinterAssignedToUserBranch(username As String) As String
            Dim retvalue As String = ""
            Try
                Dim params(1) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("UsrCode", DbType.String, username, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("retValue", DbType.String, 100, Parameter.ParameterType.AddOutParameter)

                Aurora.Common.Data.ExecuteOutputSP("TCX_PrinterAssignedToUserBranch", params)
                retvalue = params(1).Value
                Logging.LogDebug("AuroraWebService", "Calling PrinterAssignedToUserBranch : " & retvalue)
            Catch ex As Exception
                Logging.LogError("AuroraWebService", "PrinterAssignedToUserBranch Error: " & ex.Message & " ,  " & ex.StackTrace)
                Return "ERROR:" & ex.Message
            End Try
            Return retvalue
        End Function
#End Region

#Region "rev:mia 11-november-2015 fixed for Tablet One way fee issue getting dropped - Live - drop off location getting change"

        Public Shared Function BuyExperience(RentalId As String, _
                                  ExpId As String, _
                                  ExpItemId As String, _
                                  ExpCallTktQty As String, _
                                  ChannelReferrer As String, _
                                  CCLastFourDigits As String, _
                                  ExpiryYear As String, _
                                  Phone As String, _
                                  PreferredDate As String, _
                                  ExPreferredDate As String, _
                                  Message As String, _
                                  UserId As String, _
                                  PaymentAmt As String, _
                                  PaymentSurchargeAmt As String, _
                                  BillingTokenId As String) As String

            Dim sb As New StringBuilder
            sb.Append("AuroraWebService Calling BuyExperience : rentalId : " & RentalId _
                             & ", ExpId             : " & ExpId _
                             & ", ExpItemId         : " & ExpItemId _
                             & ", ExpCallTktQty     : " & ExpCallTktQty _
                             & ", ChannelReferrer   : " & ChannelReferrer _
                             & ", CCLastFourDigits  : " & CCLastFourDigits _
                             & ", ExpiryYear        : " & ExpiryYear _
                             & ", Phone             : " & Phone _
                             & ", PreferredDate     : " & PreferredDate _
                             & ", ExPreferredDate   : " & ExPreferredDate _
                             & ", Message           : " & Message _
                             & ", UserId            : " & UserId _
                             & ", PaymentAmt        : " & PaymentAmt _
                             & ", PaymentSurchargeAmt  : " & PaymentSurchargeAmt _
                             & ", BillingTokenId    : " & BillingTokenId)

            Dim retvalue As String = "OK"
            Try
                Dim params(15) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("RentalId", DbType.String, RentalId, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("ExpId", DbType.String, ExpId, Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("ExpItemId", DbType.String, ExpItemId, Parameter.ParameterType.AddInParameter)
                params(3) = New Aurora.Common.Data.Parameter("ExpCallTktQty", DbType.String, ExpCallTktQty, Parameter.ParameterType.AddInParameter)
                params(4) = New Aurora.Common.Data.Parameter("ChannelReferrer", DbType.String, ChannelReferrer.ToUpper, Parameter.ParameterType.AddInParameter)
                params(5) = New Aurora.Common.Data.Parameter("CCLastFourDigits", DbType.String, CCLastFourDigits, Parameter.ParameterType.AddInParameter)
                params(6) = New Aurora.Common.Data.Parameter("ExpiryYear", DbType.String, ExpiryYear, Parameter.ParameterType.AddInParameter)
                params(7) = New Aurora.Common.Data.Parameter("Phone", DbType.String, Phone, Parameter.ParameterType.AddInParameter)
                If (String.IsNullOrEmpty(PreferredDate)) Then
                    params(8) = New Aurora.Common.Data.Parameter("PreferredDate", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
                Else
                    params(8) = New Aurora.Common.Data.Parameter("PreferredDate", DbType.DateTime, PreferredDate, Parameter.ParameterType.AddInParameter)
                End If
                params(9) = New Aurora.Common.Data.Parameter("Message", DbType.String, Message, Parameter.ParameterType.AddInParameter)
                params(10) = New Aurora.Common.Data.Parameter("UserId", DbType.String, UserId, Parameter.ParameterType.AddInParameter)

                ''PaymentAmt will now get its value from DB..so this field will always be empty!
                params(11) = New Aurora.Common.Data.Parameter("PaymentAmt", DbType.String, PaymentAmt, Parameter.ParameterType.AddInParameter)
                params(12) = New Aurora.Common.Data.Parameter("PaymentSurchargeAmt", DbType.String, PaymentSurchargeAmt, Parameter.ParameterType.AddInParameter)

                If (String.IsNullOrEmpty(BillingTokenId)) Then
                    BillingTokenId = 1
                End If
                params(13) = New Aurora.Common.Data.Parameter("BillingTokenId", DbType.String, CInt(BillingTokenId), Parameter.ParameterType.AddInParameter)

                If (String.IsNullOrEmpty(PreferredDate)) Then
                    params(14) = New Aurora.Common.Data.Parameter("ExPreferredDate", DbType.DateTime, DBNull.Value, Parameter.ParameterType.AddInParameter)
                Else
                    params(14) = New Aurora.Common.Data.Parameter("ExPreferredDate", DbType.DateTime, ExPreferredDate, Parameter.ParameterType.AddInParameter)
                End If
                params(15) = New Aurora.Common.Data.Parameter("ReturnMessage", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("EXP_WSbuyExperience", params)
                retvalue = params(15).Value
                sb.Append("Return Message: " & retvalue & vbCrLf)

            Catch ex As Exception
                sb.Append("BuyExperience Error: " & ex.Message & " ,  " & ex.StackTrace)
                retvalue = "ERROR:" & ex.Message
            End Try
            LogInformation("BuyExperience", sb.ToString())
            Return retvalue
        End Function

        Public Shared Function IsFreeSale(ExpId As String) As String
            Dim sb As New StringBuilder
            sb.Append("AuroraWebService Calling IsFreeSale : IsFreeSale : " & " ExpId : " & ExpId)


            Dim retvalue As String = "OK"
            Try
                Dim params(1) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("ExpId", DbType.Int32, CInt(ExpId), Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("ReturnMessage", DbType.String, 2000, Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("EXP_IsAFreeSell", params)
                retvalue = params(1).Value
                If (retvalue = "1") Then
                    retvalue = "YES"
                Else
                    retvalue = "NO"
                End If
                sb.Append("Return Message: " & retvalue & vbCrLf)
            Catch ex As Exception
                sb.Append("IsFreeSale Error: " & ex.Message & " ,  " & ex.StackTrace)
                retvalue = "ERROR:" & ex.Message
            End Try

            LogInformation("IsFreeSale", sb.ToString())
            Return retvalue
        End Function

        Private Shared Sub MapPasswordsForBrands(ByRef username As String, _
                             ByRef password As String, _
                             ByVal _comCode As String, _
                             ByVal _currencyType As String, _
                             ByVal _brdCode As String)

            Dim xml As New XmlDocument
            xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))

            _brdCode = _brdCode.ToUpper
            _currencyType = _currencyType.ToUpper
            _comCode = _comCode.ToUpper

            Dim path As String = "DpsBillingToken/WSBrands/"
            Try

                If _comCode.Equals("THL") = True Then
                    If _currencyType.Equals("NZ") = True Then
                        If _brdCode.Equals("B") = True Then
                            username = xml.SelectSingleNode(path + "britz_username").InnerText   ''"/ComCode/britz_username"
                            password = xml.SelectSingleNode(path + "britz_password").InnerText   ''"/ComCode/britz_password"
                        Else ''maui
                            username = xml.SelectSingleNode(path + "maui_username").InnerText
                            password = xml.SelectSingleNode(path + "maui_password").InnerText

                        End If
                    Else ''rentals
                        username = xml.SelectSingleNode(path + "rentals_username").InnerText
                        password = xml.SelectSingleNode(path + "rentals_password").InnerText
                    End If

                Else ''kxz ''------ If _comcode.Equals("THL") = True Then
                    If _comCode.Equals("KXS") = True Then '------ If _comcode.Equals("KX") = True Then
                        username = xml.SelectSingleNode(path + "kxs_username").InnerText
                        password = xml.SelectSingleNode(path + "kxs_password").InnerText
                    Else
                        ' V2
                        ' RKS MOD: 21-Sep-2009 for MAC.COM
                        If _comCode.ToUpper().Equals("MAC") = True Then '------ If _comcode.Equals("MAC") = True Then

                            If _currencyType.Equals("NZ") = True Then '------ If _comcode.Equals("NZ") = True Then
                                username = xml.SelectSingleNode(path + "mac_nz_username").InnerText
                                password = xml.SelectSingleNode(path + "mac_nz_password").InnerText
                            Else
                                If _currencyType.Equals("AU") = True Then
                                    username = xml.SelectSingleNode(path + "mac_au_username").InnerText
                                    password = xml.SelectSingleNode(path + "mac_au_password").InnerText
                                End If
                            End If '------ If _comcode.Equals("NZ") = True Then
                        End If
                    End If '' '------   If _comcode.Equals("KX") = True Then
                End If ''''kxz ''------ If _comcode.Equals("THL") = True Then

                LogDebug("WebServiceHelper", String.Concat("MapPasswordsForBrands(WEBSERVICE USERNAME AND PASSWORD) INFORMATION: POSTUSERNAME: ", username, " , POSTPASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode))
            Catch ex As Exception
                LogError("WebServiceHelper", String.Concat("MapPasswordsForBrands(WEBSERVICE USERNAME AND PASSWORD) ERROR: POSTUSERNAME: ", username, " , POSTPASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode, " EXCEPTION: ", ex.Message + ", TRACE: ", ex.StackTrace))
            End Try
        End Sub

        Public Shared Function SubmitTransaction(ByVal data As Aurora.Booking.Services.PaymentExpressWSData, _
                                    RentalId As String, _
                                    UserCode As String) As String

            Dim xDetails As New PaymentXpress.TransactionDetails


            Dim postUserName As String = ""
            Dim postPassword As String = ""
            Dim sb As New StringBuilder
            Dim objXmlExperienceSetting As New XmlDocument
            Dim CountryCode As String
            Dim DefaultCurrencyDesc As String
            Dim ComCode As String
            Dim CurrencyType As String
            Dim Brdcode As String

            Try
                objXmlExperienceSetting.LoadXml(GetCountryAndCurrencySettings(RentalId, UserCode))
                CountryCode = objXmlExperienceSetting.SelectSingleNode("root/Country/CountryCode").InnerText
                DefaultCurrencyDesc = objXmlExperienceSetting.SelectSingleNode("root/Country/DefaultCurrencyCode").InnerText
                Brdcode = objXmlExperienceSetting.SelectSingleNode("root/Country/BrandCode").InnerText
                CurrencyType = objXmlExperienceSetting.SelectSingleNode("root/Country/DefaultCurrencyId").InnerText
                ComCode = objXmlExperienceSetting.SelectSingleNode("root/Country/ComCode").InnerText

            Catch ex As Exception
                Throw New Exception("Cannot get country and currency settings of " & RentalId & " and this user " & UserCode)
            End Try

            Try


                MapPasswordsForBrands(postUserName, postPassword, ComCode, CountryCode, Brdcode)

                With xDetails
                    .amount = data.Amount
                    .dpsBillingId = data.DpsBillingId
                    ''------------------------------------------------------------------------------------------------------
                    ''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:NZ USER that is transacting payment and booking in AU
                    ''------------------------------------------------------------------------------------------------------
                    .inputCurrency = DefaultCurrencyDesc ''Me.CountryCode + "D"
                    ''------------------------------------------------------------------------------------------------------
                    .txnType = data.TxnType
                    .txnRef = data.TxnRef
                    If Not (String.IsNullOrEmpty(data.DpsTxnRef)) Then
                        .dpsTxnRef = data.DpsTxnRef
                    End If
                End With
                Logging.LogInformation("AURORAWEBSERVICE: SubmitTransaction: ", _
                                   "postUserName " & postUserName + ", DpsBillingId " & data.DpsBillingId + ",inputCurrency " & CountryCode + " D" + ",TxnType " & data.TxnType)

                Dim px As New PaymentXpress.PaymentExpressWS
                Dim pxResult As PaymentXpress.TransactionResult = px.SubmitTransaction(postUserName, postPassword, xDetails)


                If (pxResult.authorized = "1") Then
                    sb.Append("<root>")
                    sb.AppendFormat("<status>{0}</status>", "SUCCESS")
                    sb.AppendFormat("<msg>{0}</msg>", pxResult.responseText)
                    sb.AppendFormat("<AuthCode>{0}</AuthCode>", pxResult.authCode)
                    sb.AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", pxResult.dpsTxnRef)
                    sb.Append("</root>")

                    Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
                    If data.TxnType = "Purchase" Then
                        objEFTPOS.AddEFTPOSHistory(RentalId, UserCode, "BILLING TOKEN PAYMENT IN AURORAWEBSERVICE")
                    End If

                Else
                    sb.Append("<root>")
                    sb.AppendFormat("<status>{0}</status>", "ERROR")
                    sb.AppendFormat("<msg>{0}</msg>", pxResult.responseText)
                    sb.AppendFormat("<AuthCode>{0}</AuthCode>", pxResult.authCode)
                    sb.AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", pxResult.dpsTxnRef)
                    sb.Append("</root>")
                End If

                Logging.LogInformation("AURORAWEBSERVICE: RESULT SubmitTransaction: ", sb.ToString)
                Logging.LogInformation("AURORAWEBSERVICE: RESULT SubmitTransaction: pxResult.authorized: ", pxResult.authorized)

            Catch ex As Exception
                Logging.LogError("AURORAWEBSERVICE: ERROR SubmitTransaction: ", ex.Message)
                sb.Append("<root>")
                sb.AppendFormat("<status>{0}</status>", "ERROR")
                sb.AppendFormat("<msg>{0}</msg>", ex.Message)
                sb.AppendFormat("<AuthCode>{0}</AuthCode>", "")
                sb.AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", "")
                sb.Append("</root>")
                Return sb.ToString
            End Try

            Return sb.ToString
        End Function

        Public Shared Function FormatWSAmount(ByVal samt As String) As String
            If samt.Contains(".") = False Then
                samt = samt & ".00"
            ElseIf samt.Contains(".") = True Then
                Dim decnumber As String = samt.Split(".")(1)
                If Not String.IsNullOrEmpty(decnumber) Then
                    If decnumber.Length = 1 Then
                        samt = samt & "0"
                    End If
                End If
            End If
            Return samt
        End Function

        Private Shared Function GetCountryAndCurrencySettings(RentalId As String, UserCode As String) As String

            Dim xmlString As String = ""
            Dim tempXMLobject As New XmlDocument

            Try
                xmlString = Aurora.Common.Data.ExecuteScalarSP("EXP_PaymentSettings", RentalId, UserCode)
                If xmlString <> "" Then
                    xmlString = String.Concat("<root>", xmlString, "</root>")
                End If
            Catch ex As Exception
                tempXMLobject = Nothing
                Return xmlString
            Finally
                tempXMLobject = Nothing
            End Try
            Return xmlString

        End Function

        Private Shared Function GetCreditCardData(RentalId As String, LastFourDigit As String, ExpiryYear As String) As String

            Dim xmlString As String = ""
            Dim tempXMLobject As New XmlDocument

            Try
                xmlString = Aurora.Common.Data.ExecuteScalarSP("EXP_WSgetCreditCardData", RentalId, LastFourDigit, ExpiryYear)
                If xmlString <> "" Then
                    xmlString = String.Concat("<root>", xmlString, "</root>")
                End If
            Catch ex As Exception
                tempXMLobject = Nothing
                Return xmlString
            Finally
                tempXMLobject = Nothing
            End Try
            Return xmlString

        End Function

        Public Shared Function AddPayment(RentalID As String, _
                                          UserCode As String, _
                                          PaymentAmt As String, _
                                          PaymentAmtSurcharge As String, _
                                          LastFourDigit As String, _
                                          ExpiryYear As String, _
                                          AuthCode As String, _
                                          DpsTxnRef As String) As String

            Dim objgetCreditCardData As New XmlDocument
            Dim CardType As String = ""
            Dim CreditCardName As String = ""
            Dim CreditCardNumber As String = ""
            Dim ExpiryDate As String = ""
            Dim CountryCode As String = ""
            Dim BillingToken As String = ""

            Dim sb As New StringBuilder

            Dim objXmlExperienceSetting As New XmlDocument
            Try
                objXmlExperienceSetting.LoadXml(GetCountryAndCurrencySettings(RentalID, UserCode))
                CountryCode = objXmlExperienceSetting.SelectSingleNode("root/Country/CountryCode").InnerText

            Catch ex As Exception
                Throw New Exception("Cannot get country and currency settings of " & RentalID & " and this user " & UserCode)
            End Try

            Try

                objgetCreditCardData.LoadXml(GetCreditCardData(RentalID, LastFourDigit, ExpiryYear))
                CardType = objgetCreditCardData.SelectSingleNode("root/PaymentMethod/CardType").InnerText.Trim
                CreditCardName = objgetCreditCardData.SelectSingleNode("root/PaymentMethod/PaymentBillingToken/CreditCardName").InnerText.Trim
                CreditCardNumber = objgetCreditCardData.SelectSingleNode("root/PaymentMethod/PaymentBillingToken/CreditCardNumber").InnerText.Trim
                ExpiryDate = objgetCreditCardData.SelectSingleNode("root/PaymentMethod/PaymentBillingToken/ExpiryDate").InnerText.Trim
                BillingToken = objgetCreditCardData.SelectSingleNode("root/PaymentMethod/PaymentBillingToken/BillingToken").InnerText.Trim

                If (ExpiryDate.Length = 4) Then
                    ExpiryDate = ExpiryDate.Insert(2, "/20")
                End If

                Dim returnedMessage As String
                Dim returnedErrorMessage As String
                Dim resultAddPAyment As Boolean = AddPaymentData(RentalID, CardType, PaymentAmt, PaymentAmtSurcharge, "X", CreditCardNumber, CreditCardName, ExpiryDate, DpsTxnRef, "AuroraWS AddPayment", returnedMessage, returnedErrorMessage, "", BillingToken)
                If (Not resultAddPAyment) Then
                    Throw New Exception("Failed to add payment." & returnedErrorMessage)
                Else
                    sb.Append("<Data><Status>OK</Status><Message>Added Successfully</Message></Data>")
                End If


            Catch ex As Exception
                sb.Append("<Data><Status>ERROR</Status><Message>" & ex.Message.Replace("ERROR:", "") & "</Message></Data>")
            End Try

            Return sb.ToString
        End Function

        Private Shared Function AddPaymentData(ByVal RentalId As String, _
                                    ByVal CardType As String, _
                                    ByVal PaymentAmount As Decimal, _
                                    ByVal PaymentSurchargeAmount As Decimal, _
                                    ByVal PaymentType As Char, _
                                    ByVal CreditCardNumber As String, _
                                    ByVal CreditCardName As String, _
                                    ByVal ExpiryDate As String, _
                                    ByVal DPSTxnRef As String, _
                                    ByVal ProgramName As String, _
                                    ByRef ReturnedWarningMessage As String, _
                                    ByRef ReturnedErrorMessage As String, _
                                    ByVal cvc2 As String, _
                                    ByVal BillingToken As String) As Boolean


            Dim params(13) As Aurora.Common.Data.Parameter
            Dim result As Boolean = True
            Try
                ' INPUT parameters
                params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("sCardType", DbType.String, CardType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("mPmtAmt", DbType.Decimal, PaymentAmount, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(3) = New Aurora.Common.Data.Parameter("mPaymentSurAmt", DbType.Decimal, PaymentSurchargeAmount, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(4) = New Aurora.Common.Data.Parameter("sPmtType", DbType.String, PaymentType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

                params(5) = New Aurora.Common.Data.Parameter("sCrCardNum", DbType.String, CreditCardNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(6) = New Aurora.Common.Data.Parameter("sName", DbType.String, CreditCardName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(7) = New Aurora.Common.Data.Parameter("sExpiryDt", DbType.String, ExpiryDate, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(8) = New Aurora.Common.Data.Parameter("sDpsTxnRef", DbType.String, DPSTxnRef, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(9) = New Aurora.Common.Data.Parameter("sPrgName", DbType.String, ProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)

                ' OUTPUT parameters
                params(10) = New Aurora.Common.Data.Parameter("sErrors", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                params(11) = New Aurora.Common.Data.Parameter("sWarnings", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
                params(12) = New Aurora.Common.Data.Parameter("CVC2", DbType.String, cvc2, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                params(13) = New Aurora.Common.Data.Parameter("PmtBillToToken", DbType.String, BillingToken, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
                Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_AddPayment", params)

                If String.IsNullOrEmpty(params(10).Value) AndAlso String.IsNullOrEmpty(params(11).Value) Then
                    result = True
                Else
                    ReturnedWarningMessage = params(11).Value
                    ReturnedErrorMessage = params(10).Value

                    Aurora.Common.Logging.LogError("AuroraWS AddPaymentData", params(10).Value & vbNewLine & params(11).Value)
                    result = False
                End If

            Catch ex As Exception
                Aurora.Common.Logging.LogError("AuroraWS AddPaymentData ERROR:", ex.Message & "," & ex.StackTrace)
                result = False
            End Try
            Return result
        End Function

        Public Shared Function GetPaymentDetails(ByVal RentalId As String) As XmlDocument
            Dim oParams(1) As Aurora.Common.Data.Parameter

            Dim xmlResult As New XmlDocument
            Dim dstResult As DataSet = New DataSet


            oParams(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, RentalId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            oParams(1) = New Aurora.Common.Data.Parameter("bContinueCKO", DbType.Int16, 16, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            dstResult = Aurora.Common.Data.ExecuteOutputDatasetSP("WEBA_getPaymentDetails", oParams)

            xmlResult.LoadXml(ReadXMLStringFromDataSet(dstResult))

            Return xmlResult

        End Function

        Private Shared Function RemoveSpecialCharacters(ByVal InputString As String) As String
            Return InputString.Replace("&", System.Web.HttpUtility.HtmlAttributeEncode("&"))
        End Function

        Public Shared Function ReadXMLStringFromDataSet(ByVal DBOutputDataSet As DataSet) As String
            Using oResult As New System.IO.StringWriter()
                oResult.Write("<data>")
                For Each dtTemp As DataTable In DBOutputDataSet.Tables
                    For Each rwTemp As DataRow In dtTemp.Rows
                        oResult.Write(RemoveSpecialCharacters(rwTemp(0).ToString()))
                    Next
                Next
                oResult.Write("</data>")
                Return oResult.ToString()
            End Using
        End Function

        Public Shared Function BuildVouchers(bookingId As String, rentalId As String, usercode As String, Optional GstCountry As String = "", Optional BrandFooterText As String = "") As String
            Return Aurora.BookingExperiences.Service.BookingExperiences.BuildVouchers(bookingId, rentalId, usercode, GstCountry, BrandFooterText)
        End Function


        Public Shared Function SendVouchers(sender As String, emailaddress As String, bookingId As String, rentalId As String, usercode As String, Optional SendAsVoucher As Boolean = True, Optional GstCountry As String = "", Optional BrandFooterText As String = "") As String
            Dim vouchers As String = ""
            ''REV:MIA 18-JAN-2016 - as a Concierge Team I want to be notified by email when a OnRequest experience is purchased
            If (SendAsVoucher) Then
                vouchers = BuildVouchers(bookingId, rentalId, usercode, GstCountry, BrandFooterText)
            Else
                vouchers = BuildOnRequestData(bookingId, rentalId, usercode)
                If (String.IsNullOrEmpty(vouchers)) Then
                    Return String.Empty
                End If
            End If


            Dim sb As New StringBuilder
            Try
                ''rev:mia 23-feb-2016 remove it from when goes live
                ''emailaddress = ConfigurationManager.AppSettings("ExperienceOnrequestEmail")

                Logging.LogInformation("SendVouchers", "Sender : " & sender & " To " & emailaddress)

                Dim subject As String = ""
                If (SendAsVoucher) Then
                    subject = rentalId.Replace("-", "/") & " Voucher"
                Else
                    subject = rentalId.Replace("-", "/") & " Experience OnRequest Information"
                End If

                Dim cc As String = String.Empty
                Dim attachments As String = String.Empty
                If (WebServiceCommon.CreateMessage(sender, emailaddress, subject, vouchers, cc, attachments) = True) Then
                    If (SendAsVoucher) Then
                        sb.Append("<Data><Status>OK</Status><Message>Voucher sent Successfully</Message></Data>")
                    Else
                        sb.Append("<Data><Status>OK</Status><Message>OnRequest Data sent Successfully</Message></Data>")
                    End If
                Else
                    Logging.LogError("SendVouchers", "Unable to send voucher e-mail to/from : " & emailaddress & " / " & sender)
                End If

            Catch ex As Exception
                Logging.LogError("SendVouchers", "Unable to send voucher e-mail to/from : " & emailaddress & " / " & sender & " Error: " & ex.Message & vbCrLf & ex.StackTrace)
                sb.Append("<Data><Status>ERROR</Status><Message>" & ex.Message & "</Message></Data>")
            End Try

            Return sb.ToString
        End Function

        Public Shared Function AppendComma(arrayValue As String) As String
            If (arrayValue.EndsWith(",") = True) Then
                Return arrayValue
            Else
                arrayValue = arrayValue & ","
            End If

            Return arrayValue
        End Function

        Public Shared Function IsValidNumberArray(arrayValue As String) As Boolean
            Dim result As Boolean = False
            Dim number As Integer

            If (arrayValue.Contains(",") = False) Then
                result = Int32.TryParse(arrayValue, number)
                Return result
            End If


            Dim arrVal As String() = arrayValue.Split(",")
            For Each s As String In arrVal
                result = Int32.TryParse(s, number)
                If (Not result) Then
                    Return False
                End If
            Next

            Return True
        End Function

        Public Shared Function IsValidDecimalArray(arrayValue As String) As Boolean
            Dim result As Boolean = False
            Dim numberAsDecimal As Decimal

            If (arrayValue.Contains(",") = False) Then
                result = Decimal.TryParse(arrayValue, numberAsDecimal)
                Return result
            End If


            Dim arrVal As String() = arrayValue.Split(",")
            For Each s As String In arrVal
                result = Decimal.TryParse(s, numberAsDecimal)
                If (Not result) Then
                    Return False
                End If
            Next

            Return True

        End Function

#End Region

#Region "REV:MIA 18-JAN-2016 - as a Concierge Team I want to be notified by email when a OnRequest experience is purchased"
        Private Shared Function BuildOnRequestData(bookingId As String, rentalId As String, usercode As String) As String
            Dim dsCus As DataSet = BookingExperiences.GetCustomerInfo(bookingId, rentalId)
            Dim sb As New StringBuilder
            Try
                Dim nameOfClient As String = ""
                Dim emailOfClient As String = ""

                If (Not dsCus.Tables(0).Rows(0)("CName") Is Nothing) Then
                    nameOfClient = dsCus.Tables(0).Rows(0)("CName").ToString
                End If
                If (Not dsCus.Tables(0).Rows(0)("EmailAddress") Is Nothing) Then
                    emailOfClient = dsCus.Tables(0).Rows(0)("EmailAddress").ToString
                End If

                Dim ds As DataSet = BookingExperiences.CallTicketsSummary(rentalId)

                Dim tablecount As Integer = ds.Tables.Count - 1

                If (tablecount > 0) Then
                    sb.Append("<table>")
                    sb.AppendFormat("<tr><td>Rental Id:</td><td>{0}</td></tr>", rentalId)
                    sb.AppendFormat("<tr><td>Name:</td><td>{0}</td></tr>", nameOfClient)
                    sb.AppendFormat("<tr><td>Email:</td><td>{0}</td></tr>", emailOfClient)

                    If (Not ds.Tables(0).Rows(0)("Status") Is Nothing) Then
                        sb.AppendFormat("<tr><td>CallBack Status:</td><td>{0}</td></tr>", ds.Tables(0).Rows(0)("Status").ToString)
                    End If

                    If (Not ds.Tables(0).Rows(0)("CallBackDate") Is Nothing) Then
                        If (CDate(ds.Tables(0).Rows(0)("CallBackDate")).ToShortDateString <> New Date(1900, 1, 1)) Then
                            sb.AppendFormat("<tr><td>CallBack Date:</td><td>{0}</td></tr>", ds.Tables(0).Rows(0)("CallBackDate").ToString)
                        End If
                    End If


                    sb.Append("</table>")
                    sb.Append("<table>")
                    sb.Append("<tr><td/></tr>")
                    sb.Append("<tr>")
                    sb.Append("<td>EXPERIENCE</td>")
                    sb.Append("<td>TICKET</td>")
                    sb.Append("<td>PRICE</td>")
                    sb.Append("<td>QTY</td>")
                    sb.Append("<td>TOTAL</td>")
                    sb.Append("<td>TICKET DATE</td>")
                    sb.Append("<td>SUPPLIER</td>")
                    sb.Append("<td>STATUS</td>")
                    sb.Append("<td>AGENT</td>")
                    sb.Append("</tr>")
                    Dim drowTable0() As DataRow = ds.Tables(0).Select("Status = 'Call Back'")
                    Dim id As String = drowTable0(0)("Id")
                    If (String.IsNullOrEmpty(id)) Then
                        Throw New Exception("Cannot retrieve record with a Call Back Status")
                    End If
                    Dim drowTable1() As DataRow = ds.Tables(1).Select("Status = 'OnRequest' and HeaderId = '" & id & "'")
                    If ((drowTable1.Length - 1) = -1) Then
                        Throw New Exception("Cannot retrieve record with a OnRequest Status and Id of '" & id & "'")
                    End If
                    For Each row As DataRow In drowTable1 ''ds.Tables(1).Rows
                        sb.Append("<tr>")
                        sb.AppendFormat("<td>{0}</td>", row("Experience"))
                        sb.AppendFormat("<td>{0}</td>", row("Ticket"))
                        sb.AppendFormat("<td>{0}</td>", row("Price"))
                        sb.AppendFormat("<td>{0}</td>", row("Qty"))
                        sb.AppendFormat("<td>{0}</td>", row("Total"))
                        If (Not IsDBNull(row("TicketDate"))) Then
                            sb.AppendFormat("<td>{0}</td>", CDate(row("TicketDate")).ToShortDateString)
                        Else
                            sb.AppendFormat("<td>{0}</td>", "")
                        End If
                        sb.AppendFormat("<td>{0}</td>", row("Supplier"))
                        sb.AppendFormat("<td>{0}</td>", row("Status"))
                        sb.AppendFormat("<td>{0}</td>", row("Agent").ToString.ToUpper)
                        sb.Append("</tr>")
                    Next

                    sb.Append("</table>")
                End If

            Catch ex As Exception
                Logging.LogException("BuildOnRequestData", ex)
                Return String.Empty
            End Try
            Return sb.ToString
        End Function
#End Region

#Region "rev:mia https://thlonline.atlassian.net/browse/PHOENIXWS-201 - As a CSR app developer I want send Email copy of Receipt rental Agreement"

        ''rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176
        ''add rentalId to be use by GetCCforRentalAgreement fxn
        Public Shared Function CreateMessageWithAttachment(EmailFrom As String, _
                                                           EmailTo As String, _
                                                           Attachments As String, Subject As String, _
                                                           FirstName As String, _
                                                           Optional rentalId As String = "") As String


            Dim sb As New StringBuilder
            Dim textBody As String = CStr(ConfigurationManager.AppSettings("EmailBodyForS3Bucket"))
            sb.AppendFormat(textBody, FirstName, vbCrLf, vbCrLf, vbCrLf, vbCrLf, vbCrLf, "Booking Hosts")

            Dim cc As String = String.Empty
            ''rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176
            If (Not String.IsNullOrEmpty(rentalId)) Then
                cc = GetCCforRentalAgreement(rentalId)
            End If

            If (WebServiceCommon.CreateMessage(EmailFrom, EmailTo, Subject, sb.ToString, cc, Attachments) = True) Then
                sb.Append("<Data><Status>OK</Status><Message>Rental Confirmation  with a subject " & Subject & " sent Successfully to " & EmailTo & "</Message></Data>")
            Else
                sb.Append("<Data><Status>ERROR</Status><Message>Sending Of Confirmation failed. Please check logs</Message></Data>")
            End If

            Return sb.ToString
        End Function

        ''rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176
        ''add rentalId to be use by GetCCforRentalAgreement fxn
        Public Shared Function CreateMessage(EmailFrom As String, _
                                                           EmailTo As String, _
                                                           Subject As String, _
                                                           Content As String, _
                                                           Optional rentalId As String = "") As String


            Dim sb As New StringBuilder

            Dim cc As String = String.Empty
            ''rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176
            If (Not String.IsNullOrEmpty(rentalId)) Then
                cc = GetCCforRentalAgreement(rentalId)
            End If

            Dim attachments As String = String.Empty
            If (WebServiceCommon.CreateMessage(EmailFrom, EmailTo, Subject, Content, cc, attachments) = True) Then
                sb.Append("<Data><Status>OK</Status><Message>Rental Confirmation  with a subject " & Subject & " sent Successfully to " & EmailTo & "</Message></Data>")
            Else
                sb.Append("<Data><Status>ERROR</Status><Message>Sending Of Confirmation failed. Please check logs</Message></Data>")
            End If

            Return sb.ToString
        End Function

        Public Shared Function GetEmailAndFirstName(rentalId As String, ByRef firstname As String) As String
            Dim dsCus As DataSet = BookingExperiences.GetCustomerInfo(rentalId.Split("-")(0), rentalId)
            Dim emailOfClient As String = ""
            For Each cusrow As DataRow In dsCus.Tables(0).Rows
                If (String.IsNullOrEmpty(emailOfClient)) Then
                    If Not cusrow("EmailAddress") Is Nothing Then
                        emailOfClient = cusrow("EmailAddress").ToString
                    End If
                    If (String.IsNullOrEmpty(firstname)) Then
                        If Not cusrow("FirstName") Is Nothing Then
                            firstname = cusrow("FirstName").ToString
                        End If
                    End If
                End If
            Next
            Return emailOfClient
        End Function

        Public Shared Function GetTestEmailForSender(usercode As String) As String
            Dim params(1) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("usrCode", DbType.String, usercode, Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("email", DbType.String, 1000, Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("SendEmailToUserWhoLogged", params)
            If (String.IsNullOrEmpty(params(1).Value)) Then
                Return "denis.ng@thlonline.com"
            Else
                Return params(1).Value
            End If
        End Function

        Public Shared Function RentalAgreement(ByVal RentalId As String) As String

            Dim HTMLoutput As String = "ERROR"
            Try

                Dim xmlRentalAgreementData As New XmlDocument
                xmlRentalAgreementData = Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement(RentalId)

                xmlRentalAgreementData.LoadXml("<Data>" & xmlRentalAgreementData.DocumentElement.InnerXml & "</Data>")

                Dim settings As XmlWriterSettings = New XmlWriterSettings()
                settings.OmitXmlDeclaration = True

                Dim oTransform As New XslCompiledTransform
                oTransform.Load(HttpContext.Current.Server.MapPath("~/Booking/xsl/RentalAgreement.xsl"))


                HTMLoutput = xmlRentalAgreementData.OuterXml

                Dim writer As New StringWriter()
                oTransform.Transform(xmlRentalAgreementData, Nothing, writer)
                HTMLoutput = writer.ToString()
                writer.Close()


            Catch ex As Exception
                HTMLoutput = "ERROR " & ex.Message
            End Try

            Return HTMLoutput

        End Function
#End Region

#Region "rev:mia 16-feb-2016 https://thlonline.atlassian.net/browse/AURORA-709-As a Phoenix developer I want to pass email address to BuyExperience method so i can send email to different email adress"
        Public Shared Function CheckEmailIfExist(bookingId As String, rentalId As String, usercode As String, newEmailaddress As String) As String
            Dim dsCus As DataSet = BookingExperiences.GetCustomerInfo(bookingId, rentalId)
            Dim emailOfClient As String = ""
            Dim customerId As String = ""
            For Each cusrow As DataRow In dsCus.Tables(0).Rows
                If (String.IsNullOrEmpty(emailOfClient)) Then
                    If Not cusrow("EmailAddress") Is Nothing Then
                        emailOfClient = cusrow("EmailAddress").ToString
                    End If
                    If Not cusrow("CusId") Is Nothing Then
                        customerId = cusrow("CusId").ToString
                    End If
                End If
            Next
            Return IIf(String.IsNullOrEmpty(emailOfClient), "ERROR|" & customerId, "OK")
        End Function

        Public Shared Function CheckEmailAndUpdateIfBlank(bookingId As String, rentalId As String, usercode As String, newEmailaddress As String) As String
            
            Dim result As Boolean = True
            Dim itsEmpty As String = CheckEmailIfExistFromTrail(bookingId, rentalId, usercode, newEmailaddress)

            ''UPDATE EMAIL
            Dim customerid As String = itsEmpty.Split("|")(1)
            If (itsEmpty.IndexOf("ERROR") <> -1) Then
                result = UpdateEmailWhenBlank(newEmailaddress, customerid, usercode, rentalId) '', "OK", "ERROR:Failed to update email")
            End If

            ''go and check if email was saved
            Try
                Dim sEmailId As String = Aurora.Booking.Data.DataRepository.GetCustomerEmail(rentalId)
            Catch ex As Exception
                ''resave it again
                result = UpdateEmailWhenBlank(newEmailaddress, customerid, usercode, rentalId) '', "OK", "ERROR:Failed to update email")
            End Try


            UpdateEmailTrails(rentalId, newEmailaddress, usercode)

            Return IIf(result, "OK", "ERROR:Failed to update email")
        End Function

        Private Shared Function UpdateEmailWhenBlank(newEmail As String, cusId As String, userCode As String, rentalid As String) As Boolean
            Try
                ''this call and update PhoneNumber table
                Dim params(3) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("newEmail", DbType.String, newEmail, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("cusId", DbType.String, cusId, Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("addUsrId", DbType.String, userCode, Parameter.ParameterType.AddInParameter)
                params(3) = New Aurora.Common.Data.Parameter("result", DbType.Int32, 5, Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("Exp_UpdateEmailWhenBlank", params)
            Catch ex As Exception
                Logging.LogError("UpdateEmailWhenBlank", "Unable to update newEmail : " & newEmail & " , cusId " & cusId & " Error: " & ex.Message & vbCrLf & ex.StackTrace)
                Return False
            End Try
            Return True
        End Function

        Private Shared Function UpdateEmailTrails(ExpEmailTrailRentalId As String, ExpEmailTrailUpdatedEmail As String, AddUsrId As String) As Boolean
            Try
                Dim params(3) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("ExpEmailTrailRentalId", DbType.String, ExpEmailTrailRentalId, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("ExpEmailTrailUpdatedEmail", DbType.String, ExpEmailTrailUpdatedEmail, Parameter.ParameterType.AddInParameter)
                params(2) = New Aurora.Common.Data.Parameter("addUsrId", DbType.String, AddUsrId, Parameter.ParameterType.AddInParameter)
                params(3) = New Aurora.Common.Data.Parameter("result", DbType.Int32, 5, Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("Exp_InsEmailTrail", params)
            Catch ex As Exception
                Logging.LogError("UpdateEmailTrails", "Unable to insert email trail ExpEmailTrailRentalId: " & ExpEmailTrailRentalId & " , ExpEmailTrailUpdatedEmail: " & ExpEmailTrailUpdatedEmail & ", AddUsrId: " & AddUsrId & " Error: " & ex.Message & vbCrLf & ex.StackTrace)
                Return False
            End Try
            Return True
        End Function

        Private Shared Function GetEmailTrails(BookingId As String, RentalId As String) As DataSet
            Dim result As New DataSet
            Try
                Aurora.Common.Data.ExecuteDataSetSP("EXP_GetEmailTrailInfo", result, BookingId, RentalId)
            Catch ex As Exception
                Logging.LogError("GetEmailTrails", ex.Message & ", " & ex.StackTrace)
            End Try
            Return result
        End Function

        Public Shared Function CompareEmailBetweenAuroraAndEmailTrail(bookingId As String, rentalId As String) As String
            Dim dsCus As DataSet = GetEmailTrails(bookingId, rentalId)
            Dim emailOfClient As String = ""
            Dim ExpEmailTrailUpdatedEmail As String = ""
            Dim customerId As String = ""
            For Each cusrow As DataRow In dsCus.Tables(0).Rows
                If (String.IsNullOrEmpty(emailOfClient)) Then
                    If Not cusrow("ExpUpdateEmailAddress") Is Nothing Then
                        ExpEmailTrailUpdatedEmail = cusrow("ExpUpdateEmailAddress").ToString
                    End If
                End If
            Next
            Return ExpEmailTrailUpdatedEmail
        End Function

        Public Shared Function CheckEmailIfExistFromTrail(bookingId As String, rentalId As String, usercode As String, newEmailaddress As String) As String
            Dim dsCus As DataSet = GetEmailTrails(bookingId, rentalId)
            Dim ExpEmailTrailUpdatedEmail As String = ""
            Dim customerId As String = ""
            Dim emailAddress As String = ""
            Dim expOrigEmailAddress As String = ""

            For Each cusrow As DataRow In dsCus.Tables(0).Rows

                If Not cusrow("EmailAddress") Is Nothing Then
                    emailAddress = cusrow("EmailAddress").ToString
                End If

                If Not cusrow("ExpOrigEmailAddress") Is Nothing Then
                    expOrigEmailAddress = cusrow("ExpOrigEmailAddress").ToString
                End If

                If Not cusrow("ExpUpdateEmailAddress") Is Nothing Then
                    ExpEmailTrailUpdatedEmail = cusrow("ExpUpdateEmailAddress").ToString
                End If

                If Not cusrow("CusId") Is Nothing Then
                    customerId = cusrow("CusId").ToString
                End If

            Next
            ''if emailAddress is empty, there is no email defined in PhoneAddress table
            Return IIf(String.IsNullOrEmpty(emailAddress), "ERROR|" & customerId, "OK|" & customerId)
        End Function

        Public Shared Function CompareEmailAndMakeItAsReceiver(rentalid As String, emailFromAurora As String) As String
            Dim emailTrailEmail As String = CompareEmailBetweenAuroraAndEmailTrail(rentalid.Split("-")(0), rentalid)
            If (String.IsNullOrEmpty(emailTrailEmail)) Then
                ''use the email from aurora
                Return emailFromAurora.Trim
            End If
            If (Not emailTrailEmail.ToLower().Trim.Equals(emailFromAurora.ToLower().Trim)) Then
                emailFromAurora = emailTrailEmail
            End If
            Return emailFromAurora.Trim
        End Function

#End Region

#Region "rev:mia 15-march-2016 https://thlonline.atlassian.net/browse/AURORA-754 Question marks ? showing on Experience Voucher Email"
        
        Public Shared Sub EMailConfirmation(ByVal sCfnText As String, _
                                     ByVal sEmailAddress As String, _
                                     ByVal EFaxNotify As String, _
                                     ByVal emailSubject As String, _
                                     ByVal BCC As String)

            Dim sText As String = sCfnText

            Try
                Logging.LogInformation("EMailConfirmation", "EmailAddress: " & sEmailAddress & ", BCC:  " & BCC & ", EmailSubject : " & emailSubject)

                If sText <> "" Then
                    sText = Replace(sText, "../Images/", "")

                    Dim emailArray As Array
                    Dim emailImg As String
                    emailArray = Split(emailSubject, " - ", -1, 1)
                    emailImg = Trim(emailArray(0))
                    sText = WebServiceCommon.GetBrandLogoImage(emailImg, sText)
                End If

                If sText <> "" Then
                    Dim strBody As String = ""
                    Dim attachments As String = String.Empty
                    WebServiceCommon.CreateMessage(EFaxNotify, sEmailAddress, emailSubject, sText, (IIf(String.IsNullOrEmpty(BCC), "", BCC)), attachments)
                Else
                    Logging.LogDebug("EMailConfirmation", "Calling EMailConfirmation sText is empty")
                End If

            Catch ex As Exception
                Logging.LogError("EMailConfirmation", "EMailConfirmation Error: " & ex.Message & " , ConfirmationText: " & sCfnText)
            End Try

        End Sub

#End Region

#Region "rev:mia 10-march-2016 https://thlonline.atlassian.net/browse/B2BWEB-197"
        Public Shared Sub B2BCancellationAddNotes(BookingNumber As String, reason As String, usercode As String)
            Dim pNteBooId As String = ""
            Dim pNteBooNo As String = BookingNumber.Split("-"c)(0) ''xmldoc.SelectSingleNode("Root/Data/SelectedVehicles/SelectedVehicle/RentalId").InnerText.Split("/")(0)
            Dim pNteRntNo As Integer = CInt(BookingNumber.Split("-"c)(1)) ''xmldoc.SelectSingleNode("Root/Data/SelectedVehicles/SelectedVehicle/RentalId").InnerText.Split("/")(1)
            Dim pNteCodTypId As String = "471AEF57-626D-434A-ACA8-9E5E9112AC3F"
            Dim pNteDesc As String = reason
            Dim pNteCodAudTypId As String = "FB4207C6-752C-4A04-BCEA-159E3B8E4BB1"
            Dim pNtePriority As Integer = 3
            Dim pNteIsActive As Boolean = True
            Dim pIntegrityNo As Integer = 1
            Dim pUserName As String = usercode
            Dim pAddPrgmName As String = "AuroraWebService.B2BCreateAndSendconfirmation"

            Dim returnMessage As String = Aurora.Booking.Services.BookingNotes.ManageNote(pNteBooId, _
                                                                   pNteBooNo, _
                                                                   CStr(pNteRntNo), _
                                                                   pNteCodTypId, _
                                                                   pNteDesc, _
                                                                   pNteCodAudTypId, _
                                                                   pNtePriority, _
                                                                    pNteIsActive, _
                                                                   pIntegrityNo, _
                                                                   pUserName, _
                                                                   pAddPrgmName)
        End Sub
#End Region

        

#Region "rev:mia 25-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1129"
        Public Shared Function UpdLoyaltyNumber(rentalId As String, loyaltycardnumber As String) As String

            Dim result As String = "OK"
            Try
                Logging.LogInformation("UpdLoyaltyNumber", "RentalId: " & rentalId & ", LoyaltyCardNumber:  " & loyaltycardnumber)
                Aurora.Common.Data.ExecuteScalarSP("SCI_UpdLoyaltyNumber", rentalId, loyaltycardnumber)

            Catch ex As Exception
                Logging.LogError("UpdLoyaltyNumber", "RentalId: " & rentalId & ", LoyaltyCardNumber:  " & loyaltycardnumber & "Exception:" & ex.Message & vbCrLf & ex.StackTrace)
                result = "ERROR:Failed to update Loyalty CardNumber"
            End Try

            Return result
        End Function
#End Region


#Region "rev:mia 21-Dec-2016 https://thlonline.atlassian.net/browse/AURORA-1176"
        Private Shared Function GetCCforRentalAgreement(rentalId As String) As String
            Dim result As String = String.Empty
            Try
                Logging.LogInformation("GetCCforRentalAgreement", "RentalId: " & rentalId)
                Dim params(1) As Aurora.Common.Data.Parameter
                params(0) = New Aurora.Common.Data.Parameter("rntId", DbType.String, rentalId, Parameter.ParameterType.AddInParameter)
                params(1) = New Aurora.Common.Data.Parameter("ccEmailOut", DbType.String, 128, Parameter.ParameterType.AddOutParameter)
                Aurora.Common.Data.ExecuteOutputSP("GetCCforRentalAgreement", params)
                result = params(1).Value
                If (String.IsNullOrEmpty(result)) Then result = String.Empty

            Catch ex As Exception
                Logging.LogError("GetCCforRentalAgreement", "RentalId: " & rentalId & "Exception:" & ex.Message & vbCrLf & ex.StackTrace)
                result = String.Empty
            End Try

            Return result
        End Function
#End Region

    End Class


End Namespace
