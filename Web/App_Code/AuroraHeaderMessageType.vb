Imports Microsoft.VisualBasic

Public Enum AuroraHeaderMessageType
    Information
    Warning
    [Error]
End Enum
