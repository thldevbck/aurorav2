Imports Microsoft.VisualBasic

<AttributeUsage(AttributeTargets.Class)> _
Public Class AuroraFunctionCodeAttribute
    Inherits Attribute

    Public Const General As String = "GENERAL"
    Public Const ScheduleViewer As String = "SCHEDULEVIEW"
    Public Const FlexEdit As String = "FLEXEDIT"
    Public Const FlexExport As String = "FLEXEXPORT"
    Public Const FlexRateMaintenance As String = "FLXRATEMGT"
    Public Const ConfirmationSetup As String = "CONFSETUP"
    Public Const BookingSearch As String = "BOOKSRC"
    Public Const Booking As String = "RS-BOKSUMMGT"
    Public Const BookingNoteSearch As String = "BKNTESEARCH"
    Public Const BookedProductSearch As String = "RES-BOOPRDSR"

    Public Const ProductEnquiry As String = "VIEWPROD"
    Public Const ProductMaintenance As String = "MAINTPROD"
    Public Const PackageEnquiry As String = "VIEWPACKAGE"
    Public Const PackagePublish As String = "PUBLISHPACKAGE"
    Public Const PackageMaintenance As String = "PACKAGE"
    Public Const AgentEnquiry As String = "VIEWAGENT"
    Public Const AgentMaintenance As String = "AGENT"
    Public Const SaleableProduct As String = "SALABPROD"

    Public Const UpdateGeneralData As String = "GEN_GENDATA"
    Public Const UndoCheckin As String = "UNDOCHECKIN"
    Public Const UndoCheckOut As String = "UNDOCKO"
    Public Const OdometerChange As String = "ODOCHG"
    Public Const BookingRequest As String = "RS-VEHREQMGT"
    Public Const DuplicateRentals As String = "RS-RNTDUPLST"
    Public Const DuplicateRentalList As String = "RS-VEHREQRES"
    Public Const SelectedVehiclelList As String = "RS-SELVEHLST"
    Public Const BookedProductDetails As String = "BPDDETAILLIS"
    Public Const PaymentMGT As String = "PAYMGT"

    Public Const IncidentManagement As String = "INCDMGT"

    Public Const FleetRepairManagement As String = "OL-FLTREPMGT"
    Public Const FleetRepairList As String = "OL-FLTREPLST"
    Public Const ProductAssignmentAlternatives As String = "OL-SUBSRPLST"
    Public Const FleetAssignmentAlternatives As String = "OL-SUBSRFLST"
    Public Const MaintainProductAssignmentAlternatives As String = "OL-SUBSRMGT"
    Public Const MaintainBlockingRules As String = "OL-BLKRULLST"

    Public Const ConfirmationManagement As String = "CFNMGT"
    Public Const AgentBulkProductMaintainence As String = "AGNPRDBLK"
    Public Const AgentBulkPackageMaintainence As String = "AGNPKGBLK"

    Public Const DailyCloseOffFloatCount As String = "CS-LOCFLOMGT"
    Public Const DailyCloseOffCashAndBankingSummary As String = "CS-LOCBANMGT"
    Public Const DailyCloseOffReconciliation As String = "CS-LOCRECMGT"
    Public Const StockCheck As String = "OL-LOCINVMGT"
    Public Const DailyActivity As String = "CS-DLYACTMGT"

    Public Const MaintainRelocationRule As String = "OL-RELORUMGT"
    Public Const MaintainFleetModelCosts As String = "OL-FLTMODCOS"

    Public Const AlternativeProductList As String = "OL-ALTPRDLST"
    Public Const AlternativeProductManagement As String = "OL-ALTPRDMGT"

    Public Const VehicleAssign As String = "VEHICLEASSIG"
    Public Const VehicleBulkAssign As String = "VEHBLKASGN"
    Public Const VehicleAllocation As String = "ALLOCMAINT"

    Public Const BatchReport As String = "RPT_BATCH"
    Public Const BondReportOld As String = "BONDREPORT"
    Public Const BondReportNew As String = "BONDREPORT1"

    Public Const BookedProductSelectMgt As String = "RS-SELBPD"

    Public Const UserMaintenance As String = "USERINFO"
    Public Const B2BUserMaintenance As String = "B2BAgent"
    Public Const LocationMaintenance As String = "LOCATION"

    Public Const BookedProductManagement As String = "BPDMGT"

    Public Const WebNonVehicleProductDisplay As String = "WEB_NONVEH"
    Public Const WebbookingRequestDisplay As String = "WEBBOOKREQ"
    Public Const WebbookingRequestQNDisplay As String = "WEBBOOKQNREQ"

    Public Const B2BUserSettingsMaintenance As String = "B2BUSERMAINT"

    Public Const DatabaseArchiving As String = "ARCHIVING"
    Public Const RateArchiving As String = "RATEARCH"

    Public Const WineriesBooking As String = "WINERYBOOK"

    Public Const DiscountProfileSearch As String = "DSCSEARCH"

    Public Const CdBaNumbers As String = "CdBaNumbers"

    ''rev:mia Aug 8 2011 added for the PFF
    Public Const PPF As String = "PPFMAINT"

    ''rev:mia July 2 2012 added for the PreCheckout
    Public Const PreCheckout As String = "PRECHECKOUT"

    ''-------------------------------------------------------------
    ''rev:mia July 25 2012 - this attribute is for testing only
    ''                       it should not be included as part of 
    ''                       any release.
    Public Const TestPage As String = "TestPage"
    ''-------------------------------------------------------------

    ''rev:mia OCT 2 2013 added for the LocationRestriction
    Public Const LocRestriction As String = "LOCRESTRICT"

    ''rev:mia OCT 2 2013 added for the LocationRestriction
    Public Const AIMSFleet As String = "AIMSFLEET"
    Public Const FleetAsset As String = "FleetAsset"
    Public Const FleetSaleDisposalPlan As String = "FleetDispose"
    Public Const FLEETACT As String = "FLEETACT"

    ''rev:mia 30Oct2014 - added SlotMgt
    '' Public Const SlotManagement As String = "SLOTMGT"
    Public Const BookingExperience As String = "BOOKEXP"

    Private _code As String

    Public ReadOnly Property Code() As String
        Get
            Return _code
        End Get
    End Property

    Sub New(ByVal code As String)
        _code = code
    End Sub

End Class


<AttributeUsage(AttributeTargets.Class)> _
Public Class AuroraMenuFunctionCodeAttribute
    Inherits Attribute

    Private _code As String

    Public ReadOnly Property Code() As String
        Get
            Return _code
        End Get
    End Property

    Sub New(ByVal code As String)
        _code = code
    End Sub

End Class


<AttributeUsage(AttributeTargets.Class)> _
Public Class AuroraPageTitleAttribute
    Inherits Attribute

    Private _title As String

    Public ReadOnly Property Title() As String
        Get
            Return _title
        End Get
    End Property

    Sub New(ByVal title As String)
        _title = title
    End Sub

End Class


<AttributeUsage(AttributeTargets.Class)> _
Public Class AuroraMessageAttribute
    Inherits Attribute

    Private _message As String

    Public ReadOnly Property Message() As String
        Get
            Return _message
        End Get
    End Property

    Sub New(ByVal message As String)
        _message = message
    End Sub

End Class