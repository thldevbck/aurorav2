Imports Microsoft.VisualBasic
Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

Public Class AgentUserControl
    Inherits AuroraUserControl

    Public Const NewListItemText As String = "(new)"
    Public Const AgnIdParam As String = "agnId"
    Public Const TabParam As String = "tab"

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentMaintenance)
        End Get
    End Property

    Private _agentDataSet As AgentDataSet
    Public ReadOnly Property AgentDataSet() As AgentDataSet
        Get
            Return _agentDataSet
        End Get
    End Property

    Private _agentRow As AgentRow
    Public ReadOnly Property AgentRow() As AgentRow
        Get
            Return _agentRow
        End Get
    End Property

    Public Overridable Sub InitAgentUserControl(ByVal agentDataSet As AgentDataSet, ByVal agentRow As AgentRow)
        Me._agentDataSet = agentDataSet
        Me._agentRow = agentRow
    End Sub

    Private Shared Function AddUrlParam(ByVal page As Page, ByRef first As Boolean, ByVal paramName As String, ByVal paramValue As String) As String
        If String.IsNullOrEmpty(paramValue) Then Return ""
        Dim result As String = ""
        If first Then
            result &= "?"
            first = False
        Else
            result &= "&"
        End If
        result += paramName & "=" & page.Server.UrlEncode(paramValue)
        Return result
    End Function

    Public Shared Function MakeAgentUrl( _
     ByVal page As Page, _
     Optional ByVal agnId As String = Nothing, _
     Optional ByVal tabName As String = Nothing) As String
        Dim first As Boolean = True
        Dim result As String = page.ResolveUrl("~/Agent/Maintenance.aspx") _
         & AddUrlParam(page, first, AgnIdParam, agnId) _
         & AddUrlParam(page, first, TabParam, tabName)

        Dim backUrl As String = page.Request("BackUrl")
        If Not String.IsNullOrEmpty(backUrl) Then
            result &= AddUrlParam(page, first, "BackUrl", backUrl)
        End If

        Return result
    End Function

End Class
