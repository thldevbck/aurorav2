﻿function ToggleTableColumnCheckboxes(col, evt) {
    if (!evt) evt = window.event;
    if (!evt) return;

    var chkAll = evt.srcElement ? evt.srcElement : evt.target;
    if (!chkAll) return;

    var tbody = chkAll;
    while (tbody && (tbody.tagName != 'TBODY'))
        tbody = tbody.parentNode;
    if (!tbody) return;

    var tableRows = tbody.getElementsByTagName("TR");
    for (var i = 0; i < tableRows.length; i++) {
        var tableCell = tableRows[i].getElementsByTagName("TD")[col];
        if (!tableCell) continue;

        var inputElements = tableCell.getElementsByTagName("INPUT");
        for (var j = 0; j < inputElements.length; j++) {
            var inputElement = inputElements[j];
            if (inputElement.type == 'checkbox')
                inputElement.checked = chkAll.checked;
        }
    }
}