﻿var MonitorTextArea = function (textAreaId) {
    var oTimer, iCharsPerLine = 25, iMaxLines = 4,
        oElement = document.getElementById(textAreaId),
        iNewLineChars, oTempEl;

    oTempEl = document.createElement('textarea');
    oTempEl.value = '\n';
    iNewLineChars = oTempEl.value.length;
    oTempEl = null;

    if (!oElement)
        return;

    var GetCaretPosition = function (element) {
        var oRange = element.createTextRange(),
            oRefRange = element.createTextRange(),
            oDupRange = document.selection.createRange().duplicate(),
            sChar, iOffset = 0,
	        oBookmark = oDupRange.getBookmark();

        oRange.moveToBookmark(oBookmark);
        oRange.moveStart('textedit', -1);

        oRefRange.moveToBookmark(oBookmark);
        if (element.value.length != 0) {
            do {
                if (oRefRange.compareEndPoints('StartToStart', oRange) == 1) {
                    oRefRange.moveStart('character', -1);
                } else break;

                oDupRange.moveStart('character', -1);
                sChar = oDupRange.text;
                oDupRange.moveEnd('character', -1);
                if (sChar == '')
                    iOffset += iNewLineChars;
                else break;
            } while (true);
        }

        return oRange.text.length + iOffset;
    },
        SetCaretPosition = function (element, position) {
            var oRange = element.createTextRange();
            oRange.collapse(true);
            oRange.moveEnd('character', position);
            oRange.moveStart('character', position);
            oRange.select();
        },
        CountNewLines = function (value, endIndex) {
            var iCount = 0, i = 0;
            while (i < endIndex) {
                i = value.indexOf('\r\n', i);
                if (i == -1 || i >= endIndex)
                    break;
                iCount++;
                i += 2;
            };
            return iCount;
        },
        FindWordStart = function (text, index) {
            var iChar, iIndex = index;
            while (
                iIndex > -1
                && (
                    ((iChar = text.charCodeAt(iIndex)) > 47 && iChar < 58)
                    ||
                    (iChar > 64 && iChar < 91)
                    ||
                    (iChar > 96 && iChar < 123)
                   )
                ) {
                iIndex--;
            }
            if (iIndex == -1 || iIndex >= index || iChar == 10 || iChar == 13)
                return index;
            return iIndex + 1;
        },
        FormatTextarea = function () {
            clearTimeout(oTimer);
            var sValue = oElement.value, iCharIndex = 0, iLen = sValue.length,
            iSearchIndex, iCaretPos = -1, bReformatted, sSubstr, iTemp, bUpdate, iCaretOffset = 0, iLines = 0, iLinesOffset = 0;

            while (true) {
                if (iCharIndex < iLen) {
                    iSearchIndex = sValue.indexOf('\n', iCharIndex);
                    if (iSearchIndex > -1 && iSearchIndex < iCharIndex + iCharsPerLine) {
                        iCharIndex = iSearchIndex + 1;

                        if (++iLines >= iMaxLines) {
                            sValue = sValue.substring(0, iCharIndex - iNewLineChars);
                            iLen = sValue.length;
                            if (!bUpdate) {
                                bUpdate = true;
                                iCaretPos = GetCaretPosition(oElement);
                            }
                        }

                        continue;
                    }

                    iCharIndex += iCharsPerLine;
                    if (iCharIndex < iLen) {

                        if ((sValue.charCodeAt(iCharIndex) != 10 && sValue.charCodeAt(iCharIndex) != 13)
                            || iLines >= iMaxLines - 1) {
                            if (!bUpdate) {
                                bUpdate = true;
                                iCaretPos = GetCaretPosition(oElement);
                            }

                            if (++iLines < iMaxLines) {
                                iCharIndex = FindWordStart(sValue, iCharIndex);

                                sSubstr = sValue.substring(iCharIndex);

                                if (!bReformatted) {
                                    iTemp = sSubstr.length;
                                    sSubstr = sSubstr.replace(/(?:(?:\r\n)|\n)/g, '').replace(/\s\s*$/, '');
                                    iLen = iLen - iTemp + sSubstr.length;
                                    bReformatted = true;
                                }

                                sValue = sValue.substring(0, iCharIndex) + '\n' + sSubstr;
                                iLen++;
                                iCharIndex += 1;

                                if (iCharIndex <= iCaretPos) {
                                    iCaretPos++;
                                    iCaretOffset += iNewLineChars - 1;
                                }
                            }
                            else {
                                sValue = sValue.substring(0, iCharIndex);
                                iLen = sValue.length;
                            }

                            continue;
                        }
                        iCharIndex += iNewLineChars;
                        iLines++;
                    } else break;
                } else break;
            }
            if (bUpdate) {
                oElement.value = sValue;
                iCaretPos += iCaretOffset;

                if (iNewLineChars > 1) {
                    iLinesOffset = CountNewLines(oElement.value, iCaretPos);
                    iCaretPos -= iLinesOffset;
                }
                iLen = oElement.value.length - iLinesOffset;
                if (iCaretPos >= iLen + 1)
                    iCaretPos = iLen;
                SetCaretPosition(oElement, iCaretPos);
            }
            oTimer = setTimeout(FormatTextarea, 50);

        };
    FormatTextarea();
}

var InitPromoLink = function (linkElementId, textAreaElementId, dateFormat, bookBeforeElementId, travelFromElementId, travelToElementId, brandElementId) {

    dateFormat = dateFormat.replace(/[\/\\\-\,\.\:\;]/g, '').replace(/[Mm]+/g, 'm').replace(/[Dd]+/g, 'd').replace(/[Yy]+/g, 'y');

    var aMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        iMaximumCharacters = 80,
        oLink = document.getElementById(linkElementId),
        oTextArea,
        oBookBefore,
        oTravelFrom,
        oTravelTo,
        oBrand,
        GetDateString = function (dateElement) {
            //Parsing can be sensitive to browser version and, possibly, platform.
            //It doesn't pick up some formats, such as 'dd/mm/yyyy'.
            //var oDate = new Date(Date.parse(dateElement.value));

            var aParts = dateElement.value.match(/\d+/g);

            //It's uncertain whether the native date methods will return the same result in different browser verions, on different platforms.
            //Constructing the date from separate parts can be more reliable.
            return aParts[dateFormat.indexOf('d')] + ' ' + aMonths[aParts[dateFormat.indexOf('m')] - 1] + ' ' + aParts[dateFormat.indexOf('y')];
        },
        IsAlphaNumeric = function (charCode) {
            return (charCode > 47 && charCode < 58)
                    ||
                   (charCode > 64 && charCode < 91)
                    ||
                   (charCode > 96 && charCode < 123);
        },
        EncodeURIComponent = function (value) {
            if (!value || typeof value !== 'string')
                return null;

            value = value.replace(/%/g, '%25');

            var i = -1, iChar, sChar, sEncodedChar, iLen = value.length;
            while (++i < iLen) {
                iChar = value.charCodeAt(i);

                if (!IsAlphaNumeric(iChar)
                    && iChar != 45
                    && iChar != 95
                    && iChar != 37
                   ) {
                    sChar = value.charAt(i);
                    sEncodedChar = iChar.toString(16).toUpperCase();
                    while (value.indexOf(sChar) > -1)
                        value = value.replace(sChar, '%' + sEncodedChar);
                    iLen = value.length;
                }
            }

            return value;
        },
        UpdateLink = function () {
            var sHref = oLink.href, sQuery, iCharIndex = sHref.indexOf('?', sHref), sText, sHeader, sTileText = '', iCharIndex, sBrand;

            if (!oTextArea)
                oTextArea = document.getElementById(textAreaElementId);
            if (!oBookBefore)
                oBookBefore = document.getElementById(bookBeforeElementId);
            if (!oTravelFrom)
                oTravelFrom = document.getElementById(travelFromElementId);
            if (!oTravelTo)
                oTravelTo = document.getElementById(travelToElementId);
            if (!oBrand)
                oBrand = document.getElementById(brandElementId);

            sBrand = 'B'//oBrand.options[oBrand.selectedIndex].value;

            if (!sBrand)
                return;

            sBrand = sBrand.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

            if (!sBrand)
                return;

            if (iCharIndex > -1)
                sHref = sHref.substring(0, iCharIndex);

            sText = oTextArea.value.replace(/(?:(?:\r\n)|\n)/g, '|');

            if (sText.length > iMaximumCharacters)
                sText = sText.substring(0, iMaximumCharacters);

            iCharIndex = sText.indexOf('|');

            if (iCharIndex > -1) {
                sHeader = sText.substring(0, iCharIndex);
                if (++iCharIndex < sText.length)
                    sTileText = sText.substring(iCharIndex);
            } else
                sHeader = sText;

            sQuery = '?brand=' + sBrand.toLowerCase() +
                     '&promo=' + EncodeURIComponent(
                                    sHeader +
                                    '|Book before ' + GetDateString(oBookBefore) +
                                    '|Travel between ' + GetDateString(oTravelFrom) + ' - ' + GetDateString(oTravelTo) + '|' + sTileText
                                );

            sHref = sHref + sQuery;

            oLink.href = sHref;

            return true;
        };
    oLink.onclick = UpdateLink;
}