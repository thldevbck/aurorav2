﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DiscountProfileSearch.aspx.vb" Inherits="DiscountProfileSearch"  MasterPageFile="~/Include/AuroraHeader.master"%>
<%@ Import Namespace="Aurora.Package.Data" %>
<%@ Import Namespace="Aurora.Common" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
<style type="text/css">
    table#DiscountSearchTable
    {
        width:120%;
        border-collapse: collapse
    }
    
    table#DiscountSearchTable th
    {
        padding: 5px 5px;
    }
    
    table#DiscountSearchTable td
    {
        padding: 5px 5px;
        border-bottom: 1px dotted #000;
    }
    
    table#DiscountSearchTable tr.activeDiscountProfile
    {
        background-color:<%=mActiveColor%>;
    }
    
    table#DiscountSearchTable tr.inactiveDiscountProfile
    {
        background-color: <%=mInactiveColor%>;
    }
    
    .discountProfileSearch select
    {
        width: 150px;
    }
    
    table.dscProfSrch td
    {
        padding-left:2px;
        padding-right:12px;
    }
    
    table.dscProfSrch td label
    {
        padding:0px 20px 0px 0px;
    }
    
    table.dscProfSrch td.rightCol
    {
        padding-left:20px;
    }
    
    table#DiscountSearchTable td.disProNoWrap
    {
        white-space:nowrap;
    }
    
    .discountProductExcluded
    {
        color: Red;
    }
</style>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="packageSearchPanel" runat="Server" Width="780px" CssClass="packageSearch discountProfileSearch" DefaultButton="searchButton">
    
        <table class="dscProfSrch">
            <tr>
                <td>
                    <label>Code: </label>
                </td>
                <td>
                    <asp:DropDownList ID="drpDiscountCode" runat="server"></asp:DropDownList>
                </td>
                <td class="rightCol">
                    <label>Type: </label>
                </td>
                <td>
                    <asp:DropDownList ID="drpDiscountType" runat="server"></asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Booked From: </label>
                </td>
                <td>
                    <uc1:DateControl ID="pkrBookedFrom" runat="server" Nullable="True" />
                </td>
                <td class="rightCol">
                    <label>Booked To: </label>
                </td>
                <td>
                    <uc1:DateControl ID="pkrBookedTo" runat="server" Nullable="True" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Travel From: </label>
                </td>
                <td>
                    <uc1:DateControl ID="pkrTravelFrom" runat="server" Nullable="True" />
                </td>
                <td class="rightCol">
                    <label>Travel To: </label>
                </td>
                <td>
                    <uc1:DateControl ID="pkrTravelTo" runat="server" Nullable="True" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Pickup From: </label>
                </td>
                <td>
                    <uc1:DateControl ID="pkrPickupFrom" runat="server" Nullable="True" />
                </td>
                <td class="rightCol">
                    <label>Pickup To: </label>
                </td>
                <td>
                    <uc1:DateControl ID="pkrPickupTo" runat="server" Nullable="True" />
                </td>
            </tr>
            <tr>
                <td>
                    <label>Package: </label>
                </td>
                <td>
                    <uc1:PickerControl ID="pkrPackage" runat="server" PopupType="PACKAGE" Width="150px" />
                </td>
                <td class="rightCol">
                    <label>Agent: </label>
                </td>
                <td>
                    <uc1:PickerControl ID="pkrAgent" runat="server" PopupType="AGENT" Width="150px" />
                </td>
            </tr>
            <tr style="visibility:hidden">
                <td>ComCode: </td>
                <td>
                    <asp:DropDownList ID="ddlComCode" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Status: </td>
                <td colspan="3">
                    <asp:RadioButtonList ID="rblStatus" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="All"  Selected="True"/>
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Inactive" />
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>

        <table style="width:100%">
            <tr>
                <td style="border-top-width: 1px; border-bottom-width: 1px">
                    <i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;
                </td>
                <td align="right" style="border-top-width: 1px; border-bottom-width: 1px">
                    <%--<asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />--%>
                    <input onclick="location.href='EditPackageDiscount.aspx'" class="Button_Standard Button_New" value="New" type="button" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                </td>
            </tr>
        </table>
        
        <br />

        <asp:Repeater ID="rptDiscounts" runat="server">
            <HeaderTemplate>
                <table id="DiscountSearchTable">
                <thead>
                    <tr>
                        <th>Discount Profile</th><th>Code</th><th>Type</th><th>Booked From</th><th>Booked To</th><th>Travel From</th><th>Travel To</th><%--<th>Status</th>--%><th>Products</th>
                    </tr>
                </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="<%# IIFX(DirectCast(Container.DataItem, DiscountProfile).IsActive, "activeDiscountProfile", "inactiveDiscountProfile") %>">
                    <td>
                        <asp:linkbutton
                            CommandArgument="<%#DirectCast(Container.DataItem, DiscountProfile).DiscountProfileId.ToString() %>"
                            OnCommand="editDiscountProfileClick" runat="server" >
                            <%# DirectCast(Container.DataItem, DiscountProfile).Text%>
                        </asp:linkbutton>
                        <%--<a href="EditPackageDiscount.aspx?DisProID=<%#DirectCast(Container.DataItem, DiscountProfile).DiscountProfileId.ToString() %>"><%# DirectCast(Container.DataItem, DiscountProfile).Text%></a>--%>
                    </td>
                    <td><%# DirectCast(Container.DataItem, DiscountProfile).Code%></td>
                    <td><%# DirectCast(Container.DataItem, DiscountProfile).CodeType%></td>
                    <td class="disProNoWrap"><%# DirectCast(Container.DataItem, DiscountProfile).BookedFrom.ToString("dd/MM/yyyy HH:mm")%></td>
                    <td class="disProNoWrap"><%# DirectCast(Container.DataItem, DiscountProfile).BookedTo.ToString("dd/MM/yyyy HH:mm")%></td>
                    <td><%# DirectCast(Container.DataItem, DiscountProfile).TravelFrom.ToShortDateString()%></td>
                    <td><%# DirectCast(Container.DataItem, DiscountProfile).TravelTo.ToShortDateString()%></td>
                    <td <%# IIFX(DirectCast(Container.DataItem, DiscountProfile).AreProductsExcluded, "class=""discountProductExcluded""", "") %>><%# GetLinkedProducts(DirectCast(Container.DataItem, DiscountProfile))%> <%# IIFX(DirectCast(Container.DataItem, DiscountProfile).AreProductsExcluded, "(exclude)", "") %></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        

    </asp:Panel>

</asp:Content>
