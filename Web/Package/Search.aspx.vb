' Change Log!
' 21.7.9    Shoel   Fixed problem with Search, IsInclusive and Webenabled were swapped around in order
' 28.7.9    Shoel   Fixed problem with Search that happens on returning to page - phoenix params are now considered in HasSearchParams()

Imports System.Collections.Generic

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PackageEnquiry)> _
<AuroraPageTitle("Package Search")> _
Partial Class Package_Search
    Inherits AuroraPage

    Private _packageDataSet As New PackageDataSet()

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.PackageMaintenance)
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _packageDataSet.EnforceConstraints = False

        DataRepository.GetLookups(_packageDataSet, Me.CompanyCode)

        brandDropDown.Items.Clear()
        brandDropDown.Items.Add(New ListItem("(All)", ""))
        For Each brandRow As BrandRow In _packageDataSet.Brand
            brandDropDown.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
        Next

        typeDropDown.Items.Clear()
        typeDropDown.Items.Add(New ListItem("(All)", ""))
        For Each codeRow As CodeRow In _packageDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Package_Type Then _
                typeDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem("(All)", ""))
        For Each countryRow As CountryRow In _packageDataSet.Country
            countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        Next
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = "" & CookieValue("Package_pkgCode")
        nameTextBox.Text = "" & CookieValue("Package_pkgName")
        bookedFromTextBox.Text = "" & CookieValue("Package_pkgBookedFromDate")
        bookedToTextBox.Text = "" & CookieValue("Package_pkgBookedToDate")
        travelFromTextBox.Text = "" & CookieValue("Package_pkgTravelFromDate")
        travelToTextBox.Text = "" & CookieValue("Package_pkgTravelToDate")

        Try
            If Not String.IsNullOrEmpty(CookieValue("Package_pkgBrdCode")) Then
                brandDropDown.SelectedValue = CookieValue("Package_pkgBrdCode")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("Package_pkgCodTypId")) Then
                typeDropDown.SelectedValue = CookieValue("Package_pkgCodTypId")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("Package_pkgCtyCode")) Then
                countryDropDown.SelectedValue = CookieValue("Package_pkgCtyCode")
            End If
        Catch
        End Try

        Try
            If Not String.IsNullOrEmpty(CookieValue("Package_pkgIsActive")) Then
                statusRadioButtonList.SelectedValue = CookieValue("Package_pkgIsActive")
            End If
        Catch
        End Try

        saleableProductPicker.DataId = CookieValue("Package_pkgSapId")
        saleableProductPicker.Text = CookieValue("Package_pkgSapDesc")
        ' Shoel - Phoenix Change
        chkIsInclusivePackage.Checked = CBool(CookieValue("Package_pkgIsInclusive"))
        chkIsWebEnabled.Checked = CBool(CookieValue("Package_pkgIsWebEnabled"))
        ' Shoel - Phoenix Change
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        createButton.Visible = CanMaintain

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Me.SearchParam) Then
                codeTextBox.Text = Me.SearchParam
            Else
                BuildForm(False)
            End If

            If HasSearchParameters() Then searchButton_Click(Me, Nothing)
        End If
    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("Package_pkgCode") = codeTextBox.Text.Trim()
        CookieValue("Package_pkgName") = nameTextBox.Text.Trim()
        CookieValue("Package_pkgBookedFromDate") = bookedFromTextBox.Text.Trim()
        CookieValue("Package_pkgBookedToDate") = bookedToTextBox.Text.Trim()
        CookieValue("Package_pkgTravelFromDate") = travelFromTextBox.Text.Trim()
        CookieValue("Package_pkgTravelToDate") = travelToTextBox.Text.Trim()
        CookieValue("Package_pkgBrdCode") = brandDropDown.SelectedValue
        CookieValue("Package_pkgCodTypId") = typeDropDown.SelectedValue
        CookieValue("Package_pkgCtyCode") = countryDropDown.SelectedValue
        CookieValue("Package_pkgIsActive") = statusRadioButtonList.SelectedValue
        CookieValue("Package_pkgSapId") = saleableProductPicker.DataId
        If Not String.IsNullOrEmpty(saleableProductPicker.DataId) Then
            CookieValue("Package_pkgSapDesc") = saleableProductPicker.Text
        Else
            CookieValue("Package_pkgSapDesc") = ""
        End If
        ' Shoel - Phoenix Change
        CookieValue("Package_pkgIsInclusive") = chkIsInclusivePackage.Checked
        CookieValue("Package_pkgIsWebEnabled") = chkIsWebEnabled.Checked
        ' Shoel - Phoenix Change

    End Sub

    Public Function HasSearchParameters() As Boolean
        If Not saleableProductPicker.IsValid Then Return False

        Return codeTextBox.Text.Trim().Length > 0 _
         OrElse nameTextBox.Text.Trim().Length > 0 _
         OrElse bookedFromTextBox.Date <> Date.MinValue _
         OrElse bookedToTextBox.Date <> Date.MinValue _
         OrElse travelFromTextBox.Date <> Date.MinValue _
         OrElse travelToTextBox.Date <> Date.MinValue _
         OrElse brandDropDown.SelectedIndex > 0 _
         OrElse typeDropDown.SelectedIndex > 0 _
         OrElse countryDropDown.SelectedIndex > 0 _
         OrElse statusRadioButtonList.SelectedValue <> "All" _
         OrElse Not String.IsNullOrEmpty(saleableProductPicker.DataId) _
         OrElse chkIsInclusivePackage.Checked _
         OrElse chkIsWebEnabled.Checked _
         OrElse False
    End Function

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        ''rev:mia jan6 Sales & Marketing - 'Package Search' page - 'All' radio button not working.
        If statusRadioButtonList.SelectedValue <> "All" Then
            If Not HasSearchParameters() Then
                Me.SetShortMessage(AuroraHeaderMessageType.Error, "Enter valid search parameters")
                searchResultLabel.Text = ""
                searchResultTable.Visible = False
                Return
            End If
        End If

        UpdateCookieValues()

        Dim currentDate As Date = Date.Now

        Dim pkgIsActive As String
        Select Case statusRadioButtonList.SelectedIndex
            Case 0, 1, 2
                pkgIsActive = "Active"
            Case 5
                pkgIsActive = Nothing
            Case Else
                pkgIsActive = statusRadioButtonList.SelectedValue
        End Select

        'If statusRadioButtonList.SelectedIndex = 0 Then
        '    pkgIsActive = "Active"
        'ElseIf statusRadioButtonList.SelectedIndex = 4 Then
        '    pkgIsActive = Nothing
        'Else
        '    pkgIsActive = statusRadioButtonList.SelectedValue
        'End If

        DataRepository.SearchPackage(_packageDataSet, _
            Me.CompanyCode, _
            Nothing, _
            codeTextBox.Text.Trim(), _
            nameTextBox.Text.Trim(), _
            bookedFromTextBox.Date, _
            bookedToTextBox.Date, _
            travelFromTextBox.Date, _
            travelToTextBox.Date, _
            brandDropDown.SelectedValue, _
            typeDropDown.SelectedValue, _
            countryDropDown.SelectedValue, _
            pkgIsActive, _
            saleableProductPicker.DataId, _
            chkIsWebEnabled.Checked, _
            chkIsInclusivePackage.Checked)

        While searchResultTable.Rows.Count > 1
            searchResultTable.Rows.RemoveAt(1)
        End While

        ' we did a current+future seach, filter our all the past packages
        If statusRadioButtonList.SelectedIndex = 0 Then
            Dim index As Integer = 0
            While index < _packageDataSet.Package.Rows.Count
                If _packageDataSet.Package(index).IsPast(Date.Now) Then
                    _packageDataSet.Package.Rows.RemoveAt(index)
                Else
                    index += 1
                End If
            End While
        End If

        Select Case statusRadioButtonList.SelectedIndex
            Case 0 ' Active-Current
                Dim index As Integer = 0
                While index < _packageDataSet.Package.Rows.Count
                    If _packageDataSet.Package(index).IsPast(Date.Now) Or _
                       _packageDataSet.Package(index).IsFuture(Date.Now) Then
                        _packageDataSet.Package.Rows.RemoveAt(index)
                    Else
                        index += 1
                    End If
                End While
            Case 1 ' Active-Future
                Dim index As Integer = 0
                While index < _packageDataSet.Package.Rows.Count
                    If _packageDataSet.Package(index).IsPast(Date.Now) Or _
                       _packageDataSet.Package(index).IsCurrent(Date.Now) Then
                        _packageDataSet.Package.Rows.RemoveAt(index)
                    Else
                        index += 1
                    End If
                End While
            Case 2 ' Active-Past
                Dim index As Integer = 0
                While index < _packageDataSet.Package.Rows.Count
                    If _packageDataSet.Package(index).IsFuture(Date.Now) Or _
                       _packageDataSet.Package(index).IsCurrent(Date.Now) Then
                        _packageDataSet.Package.Rows.RemoveAt(index)
                    Else
                        index += 1
                    End If
                End While
                'Case 3 ' Pending
                'Case 4 ' Inactive
                'Case 5 ' All
                'Case Else
        End Select

        If _packageDataSet.Package.Rows.Count = 0 Then
            searchResultLabel.Text = "No packages found"
            searchResultTable.Visible = False
        Else
            If _packageDataSet.Package.Rows.Count = 1 Then
                searchResultLabel.Text = _packageDataSet.Package.Rows.Count.ToString() & " package found"
            Else
                searchResultLabel.Text = _packageDataSet.Package.Rows.Count.ToString() & " packages found"
            End If
            searchResultTable.Visible = True

            Dim index As Integer = 0
            For Each packageRow As PackageRow In _packageDataSet.Package
                Dim tableRow As New TableRow()
                tableRow.BackColor = packageRow.StatusColor(currentDate)
                searchResultTable.Rows.Add(tableRow)

                Dim nameCell As New TableCell()
                Dim nameHyperLink As New HyperLink()
                nameHyperLink.Text = Server.HtmlEncode(packageRow.Description)
                nameHyperLink.NavigateUrl = "Package.aspx?pkgId=" & Server.UrlEncode(packageRow.PkgId)
                nameCell.Controls.Add(nameHyperLink)
                tableRow.Controls.Add(nameCell)

                Dim brandCell As New TableCell()
                brandCell.Text = Server.HtmlEncode(packageRow.BrandDescription)
                tableRow.Controls.Add(brandCell)

                Dim typeCell As New TableCell()
                typeCell.Text = Server.HtmlEncode(packageRow.TypeDescription)
                tableRow.Controls.Add(typeCell)

                Dim countryCell As New TableCell()
                countryCell.Text = Server.HtmlEncode(packageRow.CountryDescription)
                tableRow.Controls.Add(countryCell)

                Dim bookedCell As New TableCell()
                bookedCell.Text = Server.HtmlEncode(packageRow.BookedDescription)
                tableRow.Controls.Add(bookedCell)

                Dim travelCell As New TableCell()
                travelCell.Text = Server.HtmlEncode(packageRow.TravelDescription)
                tableRow.Controls.Add(travelCell)

                Dim statusCell As New TableCell()
                statusCell.Text = packageRow.StatusDescription
                tableRow.Controls.Add(statusCell)

                index += 1
            Next
        End If

    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If Not CanMaintain Then Return

        UpdateCookieValues()

        Response.Redirect(PackageUserControl.MakePackageUrl(Me.Page))
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        codeTextBox.Text = ""
        nameTextBox.Text = ""
        bookedFromTextBox.Text = ""
        bookedToTextBox.Text = ""
        travelFromTextBox.Text = ""
        travelToTextBox.Text = ""
        brandDropDown.SelectedIndex = 0
        typeDropDown.SelectedIndex = 0
        countryDropDown.SelectedIndex = 0
        statusRadioButtonList.SelectedIndex = 0
        saleableProductPicker.DataId = ""
        saleableProductPicker.Text = ""
        searchResultTable.Visible = False
        searchResultLabel.Text = ""
        chkIsWebEnabled.Checked = False
        chkIsInclusivePackage.Checked = False

        UpdateCookieValues()
    End Sub
End Class
