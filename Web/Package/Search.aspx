<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Package_Search" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="packageSearchPanel" runat="Server" Width="780px" CssClass="packageSearch" DefaultButton="searchButton">
    
        <table style="width:100%;" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width:150px">Code:</td>
                <td style="width:250px"><asp:TextBox ID="codeTextBox" runat="server" style="width:150px" MaxLength="10" /></td>
                <td style="width:150px">Name:</td>
                <td style="width:250px"><asp:TextBox ID="nameTextBox" runat="server" style="width:250px" MaxLength="64" /></td>
            </tr>
            <tr>
                <td>Brand:</td>
                <td>
                    <asp:DropDownList ID="brandDropDown" runat="server" style="width:150px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>Type:</td>
                <td>
                    <asp:DropDownList ID="typeDropDown" runat="server" style="width:150px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Country:</td>
                <td>
                    <asp:DropDownList ID="countryDropDown" runat="server" style="width:150px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
                <td>Saleable Product:</td>
                <td><uc1:PickerControl ID="saleableProductPicker" runat="server" PopupType="PACKAGE_SALEABLEPRODUCT" Width="150px" /></td>
            </tr>
            <tr>
                <td>Booked From:</td>
                <td><uc1:DateControl ID="bookedFromTextBox" runat="server" Nullable="True" /></td>
                <td>To:</td>
                <td><uc1:DateControl ID="bookedToTextBox" runat="server" Nullable="True" /></td>
            </tr>
            <tr>
                <td>Travel From:</td>
                <td><uc1:DateControl ID="travelFromTextBox" runat="server" Nullable="True" /></td>
                <td>To:</td>
                <td><uc1:DateControl ID="travelToTextBox" runat="server" Nullable="True" /></td>
            </tr>
            <tr>
                <td>Status:</td>
                <td colspan="3" >
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="6" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <%--<asp:ListItem Text="Current+Future" Selected="True" />
                        <asp:ListItem Text="Active" />
                        <asp:ListItem Text="Pending" />
                        <asp:ListItem Text="Inactive" />
                        <asp:ListItem Text="All" />--%>
                        <asp:ListItem Text="Active-Current"  />
                        <asp:ListItem Text="Active-Future" />
                        <asp:ListItem Text="Active-Past" />
                        <asp:ListItem Text="Pending" />
                        <asp:ListItem Text="Inactive" />
                        <asp:ListItem Text="All"  Selected="True"/>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <%--Shoel - Phoenix Change - Inclusive and WebEnabled Search--%>
            <tr>
                <td>
                    Inclusive:
                </td>
                <td>
                    <asp:CheckBox ID="chkIsInclusivePackage" runat="server" />
                </td>
                <td>
                    Web-enabled:
                </td>
                <td>
                    <asp:CheckBox ID="chkIsWebEnabled" runat="server" />
                </td>
            </tr>
            <%--Shoel - Phoenix Change - Inclusive and WebEnabled Search--%>
        </table>

        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td colspan="2" style="border-top-width: 1px; border-bottom-width: 1px">
                    <i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;
                </td>
                <td colspan="2" align="right" style="border-top-width: 1px; border-bottom-width: 1px">
                    <asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                </td>
            </tr>
        </table>
        
        <br />
        
        <asp:Table ID="searchResultTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" Visible="False" CssClass="dataTableColor">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Text="Name" />
                <asp:TableHeaderCell Text="Brand" Width="100px" />
                <asp:TableHeaderCell Text="Type" Width="50px" />
                <asp:TableHeaderCell Text="Country" Width="120px" />
                <asp:TableHeaderCell Text="Booked" Width="150px" />
                <asp:TableHeaderCell Text="Travel" Width="150px" />
                <asp:TableHeaderCell Text="Status" Width="50px" />
            </asp:TableHeaderRow>
        </asp:Table>

    </asp:Panel>

</asp:Content>

