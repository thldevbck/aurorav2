﻿Option Strict On
Option Explicit On

Imports System
Imports System.Data
Imports System.Data.Common
Imports System.Web
Imports System.Configuration

Imports System.Collections
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Package.Data


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.CdBaNumbers)> _
Partial Class Package_EditCdBaNumbers
    Inherits AuroraPage

    Protected Shared mStateKey As String = Guid.NewGuid().ToString().Replace("-", "")

    'Protected Shared mActiveColor As String = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ActiveColor)
    'Protected Shared mInactiveColor As String = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor)



    Protected mAgents As List(Of AgentTextInfo)
    Protected mSelectedAgents As New List(Of KeyValuePair(Of String, String))

    Protected ReadOnly Property PackageId As String
        Get
            Return Request.QueryString("package")
        End Get
    End Property

    Protected ReadOnly Property CdBaCountryCode As String
        Get
            Return Request.QueryString("country")
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        'Forcing IE into standards-compliant mode
        Response.AddHeader("X-UA-Compatible", "IE=edge")
        MyBase.OnInit(e)
    End Sub

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        If Not Page.IsPostBack Then
            GetData()
        Else
            If Not HandleAsyncPostback() Then
                'This is not a partial request (for agent data, for example)
            End If
        End If
    End Sub

    Protected Function HandleAsyncPostback() As Boolean
        If String.Equals(Request.Form("__EVENTTARGET"), AgentListUpdatePanel.ClientID) Then
            SearchAgents(txtAgentSearch.Value)
            MoveSelectedAgents(False)

            AgentListUpdatePanel.Update()

            Return True
        End If

        Return False
    End Function

    Protected Sub GetData()
        GetPackageData()
        GetMappingData()
    End Sub

    Protected Sub GetPackageData()
        Dim repository As New CdBaRepository()

        Dim packageId As String = Me.PackageId

        If packageId Is Nothing Then
            Return
        End If

        Dim package As CdBaPackage = repository.GetPackageData(packageId)

        If package IsNot Nothing Then
            txtCode.Text = package.Code
            txtName.Text = package.Name
            txtBrand.Text = package.Brand
            txtCountry.Text = package.Country
            txtBookedFrom.Text = package.BookedFrom.ToString("dd/MM/yyyy")
            txtBookedTo.Text = package.BookedTo.ToString("dd/MM/yyyy")
            txtTravelFrom.Text = package.TravelFrom.ToString("dd/MM/yyyy")
            txtTravelTo.Text = package.TravelTo.ToString("dd/MM/yyyy")
        End If
    End Sub

    Protected Sub GetMappingData()
        Dim repository As New CdBaRepository()

        Dim packageId As String = Me.PackageId

        If packageId Is Nothing Then
            Return
        End If

        Dim mappings As List(Of CdBaNumberPackageMapping) = repository.GetEuropcarCdBaMappings(packageId)

        rptMappings.DataSource = mappings
        rptMappings.DataBind()
    End Sub

    Protected Function ResolveNullableBoolean(ByVal value As Nullable(Of Boolean), ByVal resultIfNull As String, ByVal resultIfTrue As String, ByVal resultIfFalse As String) As String
        If Not value.HasValue Then
            Return resultIfNull
        End If

        If value.Value Then
            Return resultIfTrue
        End If

        Return resultIfFalse
    End Function

    Protected Function GetLinkedAgents(ByVal mapping As CdBaNumberPackageMapping) As String
        Dim builder As New StringBuilder()

        'Dim max As Integer = 0
        For Each agent As AgentTextInfo In mapping.Agents
            If builder.Length > 0 Then
                builder.Append(", ")
            End If
            builder.Append(agent.AgentCode)

            'max = max + 1
            'If max = 3 Then
            '    builder.Append(", ...")
            '    Exit For
            'End If
        Next

        Return builder.ToString()
    End Function

    Protected Sub ResetPopupFields()
        ddlCdBaNumbers.SelectedIndex = 0
        ddlPickupPay.SelectedIndex = 0
        ddlAirportPickup.SelectedIndex = 0
        hidSelectedAgents.Value = ""
        txtAgentSearch.Value = ""
        chkExclude.Checked = False
    End Sub

    Public Sub ShowCdBaPopup()
        plhCdBaDialogInner.Visible = True
        PopulatePopup()
        cdBaModalDialog.Show()

        plhCdBaScript.Visible = True
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CdBaInit", RenderControlToString(plhCdBaScript), False)
        plhCdBaScript.Visible = False
    End Sub

    Protected Sub PopulatePopup()
        SearchAgents(txtAgentSearch.Value)

        PopulateSelectedAgents()

        PopulateCdBaNumbers()
    End Sub

    Protected Sub PopulateSelectedAgents()
        MoveSelectedAgents(True)
    End Sub

    Protected Sub MoveSelectedAgents(ByVal populateTargetList As Boolean)
        Dim lstSelectedAgents As List(Of KeyValuePair(Of String, String)) = GetSelectedAgents()

        For Each agentData As KeyValuePair(Of String, String) In lstSelectedAgents

            If mAgents IsNot Nothing Then
                Dim intAgentCount As Integer = mAgents.Count
                Dim intAgentIndex As Integer = 0

                While intAgentIndex < intAgentCount
                    Dim agent As AgentTextInfo = mAgents(intAgentIndex)

                    If agent.AgentId.Trim() = agentData.Key Then
                        mAgents.RemoveAt(intAgentIndex)

                        intAgentCount = mAgents.Count
                    Else
                        intAgentIndex = intAgentIndex + 1
                    End If
                End While
            End If

            If populateTargetList Then
                mSelectedAgents.Add(agentData)
            End If
        Next

    End Sub

    Protected Sub PopulateCdBaNumbers()
        If ddlCdBaNumbers.Items.Count > 0 Then
            Return
        End If

        Dim lstCdBaNumbers As List(Of CdBaNumberSimple) = _
            New CdBaRepository().GetCdBaNumbers(CdBaCountryCode)

        ddlCdBaNumbers.Items.Add(New ListItem("CD - BA number", ""))

        For Each cdBaNumber As CdBaNumberSimple In lstCdBaNumbers
            Dim item As New ListItem(cdBaNumber.CombinedValue, cdBaNumber.Id.ToString())
            ddlCdBaNumbers.Items.Add(item)
        Next
    End Sub

    Protected Function GetSelectedAgents() As List(Of KeyValuePair(Of String, String))
        Dim strSelectedAgents As String = hidSelectedAgents.Value.Trim()

        If strSelectedAgents.Length = 0 Then
            Return New List(Of KeyValuePair(Of String, String))
        End If

        Dim lstValues As List(Of String) = GetValuesFromCsv(strSelectedAgents)
        Dim lstSelectedAgents As New List(Of KeyValuePair(Of String, String))

        Dim intLen As Integer = lstValues.Count

        If intLen Mod 2 = 1 Then
            Throw New Exception("Invalid agent data received from client.")
        End If

        For i As Integer = 0 To intLen - 1 Step 2
            Dim agentData As New KeyValuePair(Of String, String)(lstValues(i), lstValues(i + 1))

            lstSelectedAgents.Add(agentData)
        Next

        Return lstSelectedAgents
    End Function


    Protected Sub SearchAgents(ByVal partialValue As String)
        If String.IsNullOrEmpty(partialValue) Then
            Return
        End If

        partialValue = partialValue.Trim()

        If partialValue.Length = 0 Then
            Return
        End If

        Dim repository As New AgentRepository()

        Dim lstAgents As List(Of AgentTextInfo) = repository.GetAgentTextInfo(partialValue)

        mAgents = lstAgents
    End Sub

    Protected Sub Save()

        If ddlCdBaNumbers.SelectedIndex = 0 Then
            SetWarningShortMessage("Select CD/BA number before saving.")
            Return
        End If

        Dim lstAgents As List(Of KeyValuePair(Of String, String)) = GetSelectedAgents()

        'If lstAgents.Count = 0 Then
        '    SetWarningShortMessage("No agents selected.")
        '    Return
        'End If

        Dim cdBaId As Integer

        If Not Integer.TryParse(ddlCdBaNumbers.SelectedValue, cdBaId) Then
            Return
        End If

        Dim packageId As String = Me.PackageId

        If packageId Is Nothing Then
            Return
        End If

        packageId = packageId.Trim()

        If packageId.Length = 0 Then
            Return
        End If

        Dim isAirport As Nullable(Of Boolean) = GetNullableBolean(ddlAirportPickup.SelectedValue)
        Dim payDirect As Nullable(Of Boolean) = GetNullableBolean(ddlPickupPay.SelectedValue)

        If lstAgents.Count > 0 Then
            Dim excludeAgent As Boolean = chkExclude.Checked

            For Each agentData As KeyValuePair(Of String, String) In lstAgents
                Dim agentId As String = agentData.Key.Trim()

                Aurora.Common.Data.ExecuteScalarSP("CreatePackageCDAgentMapping", cdBaId, packageId, agentId, payDirect, isAirport, excludeAgent, UserCode, PrgmName)
            Next
        Else
            Aurora.Common.Data.ExecuteScalarSP("CreatePackageCDAgentMapping", cdBaId, packageId, Nothing, payDirect, isAirport, False, UserCode, PrgmName)
        End If
        

        ResetPopupFields()
    End Sub

    Protected Sub RemoveMappings()
        Dim lstSelectedMappings As List(Of String) = GetValuesFromCsv(hidSelectedMappings.Value)

        For Each value As String In lstSelectedMappings
            Dim mappingId As Long

            If Long.TryParse(value, mappingId) AndAlso mappingId > 0 Then
                Aurora.Common.Data.ExecuteScalarSP("DeleteEuropcarCdBaMapping", mappingId)
            End If
        Next
    End Sub

    Protected Function GetNullableBolean(ByVal value As String) As Nullable(Of Boolean)
        If value Is Nothing Then
            Return New Nullable(Of Boolean)()
        End If

        value = value.Trim()

        If value.Length = 0 Then
            Return New Nullable(Of Boolean)()
        End If

        Return New Nullable(Of Boolean)(CType(value, Boolean))
    End Function

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRemove.Click
        RemoveMappings()
        GetData()
    End Sub

    Protected Sub btnLink_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnLink.Click
        ShowCdBaPopup()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Save()
        GetData()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        GetData()
    End Sub



    Protected Function GetValuesFromCsv(ByVal value As String) As List(Of String)
        Dim lstValues As New List(Of String)

        If value Is Nothing Then
            Return lstValues
        End If

        Dim strValue As String
        Dim intPrevIndex As Integer = 0
        Dim intIndex As Integer = 0
        Dim intLen As Integer = value.Length

        Dim intBacktrackIndex As Integer

        While intIndex < intLen
            intIndex = value.IndexOf(","c, intIndex)

            If intIndex > -1 Then
                'Count the number of escape slashes
                intBacktrackIndex = intIndex - 1

                While intBacktrackIndex >= 0 AndAlso value(intBacktrackIndex) = "\"c
                    intBacktrackIndex = intBacktrackIndex - 1
                End While

                If (intIndex - intBacktrackIndex - 1) Mod 2 = 1 Then
                    'This is an escaped comma - move on
                Else
                    strValue = value.Substring(intPrevIndex, intIndex - intPrevIndex)
                    'Unescape
                    strValue = strValue.Replace("\\", "\").Replace("\,", ",").Trim()
                    lstValues.Add(strValue)

                    intPrevIndex = intIndex + 1
                End If
            Else
                Exit While
            End If

            intIndex = intIndex + 1
        End While

        If intPrevIndex < intLen Then
            strValue = value.Substring(intPrevIndex, intLen - intPrevIndex)
            'Unescape
            strValue = strValue.Replace("\\", "\").Replace("\,", ",")
            lstValues.Add(strValue)
        ElseIf intPrevIndex = intLen Then
            lstValues.Add("")
        End If


        Return lstValues
    End Function


    Protected Function RenderControlToString(ByVal control As Control) As String
        Dim objStringWriter As System.IO.StringWriter = Nothing
        Dim objHtmlWriter As HtmlTextWriter = Nothing
        Try
            objStringWriter = New System.IO.StringWriter()
            objHtmlWriter = New HtmlTextWriter(objStringWriter)
            control.RenderControl(objHtmlWriter)
            Return objStringWriter.ToString()
        Catch

        Finally
            If objHtmlWriter IsNot Nothing Then
                objHtmlWriter.Close()
            End If

            If objStringWriter IsNot Nothing Then
                objStringWriter.Close()
            End If
        End Try

        Return Nothing
    End Function



    Public Class AgentTextInfo
        Private mAgentId As String
        Private mAgentCode As String
        Private mAgentName As String

        Public Property AgentId As String
            Get
                Return mAgentId
            End Get
            Set(ByVal value As String)
                mAgentId = value
            End Set
        End Property

        Public Property AgentCode As String
            Get
                Return mAgentCode
            End Get
            Set(ByVal value As String)
                mAgentCode = value
            End Set
        End Property

        Public Property AgentName As String
            Get
                Return mAgentName
            End Get
            Set(ByVal value As String)
                mAgentName = value
            End Set
        End Property

        Public ReadOnly Property CodeAndName As String
            Get
                Return mAgentCode & " - " & mAgentName
            End Get
        End Property
    End Class

    Public Class CdBaPackageMappedAgent
        Inherits AgentTextInfo
        Private mMappingId As Long

        Public Property MappingId As Long
            Get
                Return mMappingId
            End Get
            Set(ByVal value As Long)
                mMappingId = value
            End Set
        End Property
    End Class

    Public Class CdBaNumberSimple
        Private mId As Long
        Private mBusinessAccountNumber As String
        Private mContractIdNumber As String
        'Private mCountryCode As String

        Public Property Id As Long
            Get
                Return mId
            End Get
            Set(ByVal value As Long)
                mId = value
            End Set
        End Property

        Public Property BusinessAccountNumber As String
            Get
                Return mBusinessAccountNumber
            End Get
            Set(ByVal value As String)
                mBusinessAccountNumber = value
            End Set
        End Property


        Public Property ContractIdNumber As String
            Get
                Return mContractIdNumber
            End Get
            Set(ByVal value As String)
                mContractIdNumber = value
            End Set
        End Property

        Public ReadOnly Property CombinedValue As String
            Get
                Return mContractIdNumber & " - " & mBusinessAccountNumber
            End Get
        End Property

        'Public Property CountryCode As String
        '    Get
        '        Return mCountryCode
        '    End Get
        '    Set(ByVal value As String)
        '        mCountryCode = value
        '    End Set
        'End Property
    End Class

    Public Class CdBaNumber
        Inherits CdBaNumberSimple

        Private mAccountName As String

        Public Property AccountName As String
            Get
                Return mAccountName
            End Get
            Set(ByVal value As String)
                mAccountName = value
            End Set
        End Property
    End Class

    Public Class CdBaNumberPackageMapping
        Private mCdBaNumber As CdBaNumber
        Private mAgents As List(Of CdBaPackageMappedAgent)

        Private mMappingId As Long
        Private mPayDirect As Nullable(Of Boolean)
        Private mIsAirport As Nullable(Of Boolean)
        Private mIsAgentExcluded As Boolean



        Public Property MappingId As Long
            Get
                Return mMappingId
            End Get
            Set(ByVal value As Long)
                mMappingId = value
            End Set
        End Property

        Public Property Agents As List(Of CdBaPackageMappedAgent)
            Get
                If mAgents Is Nothing Then
                    mAgents = New List(Of CdBaPackageMappedAgent)
                End If

                Return mAgents
            End Get
            Set(ByVal value As List(Of CdBaPackageMappedAgent))
                mAgents = value
            End Set
        End Property


        Public Property CdBaNumber As CdBaNumber
            Get
                Return mCdBaNumber
            End Get
            Set(ByVal value As CdBaNumber)
                mCdBaNumber = value
            End Set
        End Property

        Public Property PayDirect As Nullable(Of Boolean)
            Get
                Return mPayDirect
            End Get
            Set(ByVal value As Nullable(Of Boolean))
                mPayDirect = value
            End Set
        End Property
        Public Property IsAirport As Nullable(Of Boolean)
            Get
                Return mIsAirport
            End Get
            Set(ByVal value As Nullable(Of Boolean))
                mIsAirport = value
            End Set
        End Property

        Public Property IsAgentExcluded As Boolean
            Get
                Return mIsAgentExcluded
            End Get
            Set(ByVal value As Boolean)
                mIsAgentExcluded = value
            End Set
        End Property

    End Class

    Public Class CdBaPackage
        Private mCode As String
        Private mName As String
        Private mBrand As String
        Private mCountry As String
        Private mBookedFrom As DateTime
        Private mBookedTo As DateTime
        Private mTravelFrom As DateTime
        Private mTravelTo As DateTime

        Public Property Code As String
            Get
                Return mCode
            End Get
            Set(ByVal value As String)
                mCode = value
            End Set
        End Property


        Public Property Name As String
            Get
                Return mName
            End Get
            Set(ByVal value As String)
                mName = value
            End Set
        End Property
        Public Property Brand As String
            Get
                Return mBrand
            End Get
            Set(ByVal value As String)
                mBrand = value
            End Set
        End Property
        Public Property Country As String
            Get
                Return mCountry
            End Get
            Set(ByVal value As String)
                mCountry = value
            End Set
        End Property
        Public Property BookedFrom As DateTime
            Get
                Return mBookedFrom
            End Get
            Set(ByVal value As DateTime)
                mBookedFrom = value
            End Set
        End Property
        Public Property BookedTo As DateTime
            Get
                Return mBookedTo
            End Get
            Set(ByVal value As DateTime)
                mBookedTo = value
            End Set
        End Property
        Public Property TravelFrom As DateTime
            Get
                Return mTravelFrom
            End Get
            Set(ByVal value As DateTime)
                mTravelFrom = value
            End Set
        End Property
        Public Property TravelTo As DateTime
            Get
                Return mTravelTo
            End Get
            Set(ByVal value As DateTime)
                mTravelTo = value
            End Set
        End Property
    End Class

    Public Class CdBaRepository

        Public Overridable Function GetPackageData(ByVal packageId As String) As CdBaPackage
            Dim dataset As New CdBaPackageDataSet()

            Aurora.Common.Data.ExecuteListSP("CdBaGetPackage", _
                                                dataset, _
                                                New ParamInfo(packageId, "PackageID") _
                                                )

            If dataset.ResultSet.Count > 0 Then
                Return dataset.ResultSet(0)
            End If

            Return Nothing
        End Function

        Public Overridable Function GetCdBaNumbers(ByVal countryCode As String) As List(Of CdBaNumberSimple)

            If countryCode Is Nothing Then
                Return New List(Of CdBaNumberSimple)()
            End If

            Dim dataSet As New CdBaDataSet()

            Aurora.Common.Data.ExecuteListSP("GetEuropcarCdBaNumbers", _
                                                dataSet, _
                                                New ParamInfo(countryCode, "CountryCode") _
                                                )


            Return dataSet.ResultSet
        End Function

        Public Overridable Function GetEuropcarCdBaMappings(ByVal packageId As String) As List(Of CdBaNumberPackageMapping)

            If packageId Is Nothing Then
                Return New List(Of CdBaNumberPackageMapping)()
            End If

            Dim dataSet As New CdBaNumberPackageMappingDataSet()
            Dim agentDataSet As New CdBaPackageMappedAgentDataSet()


            Aurora.Common.Data.ExecuteListSP("GetEuropcarCdBaMappings", _
                                                dataSet, _
                                                agentDataSet, _
                                                New ParamInfo(packageId, "PackageID") _
                                                )

            MapAgents(dataSet.ResultSet, agentDataSet.ResultSet)

            Return dataSet.ResultSet
        End Function

        Protected Sub MapAgents(ByVal packageContracts As List(Of CdBaNumberPackageMapping), ByVal agents As List(Of CdBaPackageMappedAgent))

            For Each agent As CdBaPackageMappedAgent In agents
                Dim mappingId As Long = agent.MappingId
                For Each packageContract As CdBaNumberPackageMapping In packageContracts
                    If packageContract.MappingId = mappingId Then
                        packageContract.Agents.Add(agent)
                    End If
                Next
            Next

        End Sub

        Protected Class CdBaPackageDataSet
            Inherits ListDataSet(Of CdBaPackage)

            Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As CdBaPackage
                Dim item As New CdBaPackage()

                item.Code = GetDbValue(Of String)(reader, "PkgCode")
                item.Name = GetDbValue(Of String)(reader, "PkgName")
                item.Country = GetDbValue(Of String)(reader, "CtyName")
                item.Brand = GetDbValue(Of String)(reader, "BrdName")
                item.BookedFrom = GetDbValue(Of DateTime)(reader, "PkgBookedFromDate")
                item.BookedTo = GetDbValue(Of DateTime)(reader, "PkgBookedToDate")
                item.TravelFrom = GetDbValue(Of DateTime)(reader, "PkgTravelFromDate")
                item.TravelTo = GetDbValue(Of DateTime)(reader, "PkgTravelToDate")

                Return item
            End Function
        End Class

        ''' <summary>
        ''' Used to populate CdBa entries from data reader.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Class CdBaDataSet
            Inherits ListDataSet(Of CdBaNumberSimple)

            Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As CdBaNumberSimple
                Dim entry As New CdBaNumberSimple()
                PopulateFromReader(entry, reader)

                Return entry
            End Function

            Public Shared Sub PopulateFromReader(ByVal entry As CdBaNumberSimple, ByVal reader As System.Data.Common.DbDataReader)

                entry.Id = GetDbValue(Of Long)(reader, "EuropCarCDId")
                entry.ContractIdNumber = GetDbValue(Of String)(reader, "EuropCarCDRateCDNum")
                entry.BusinessAccountNumber = GetDbValue(Of String)(reader, "EuropCarCDRateBANum")
            End Sub
        End Class

        ''' <summary>
        ''' Used to populate items entries from data reader.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Class CdBaNumberPackageMappingDataSet
            Inherits ListDataSet(Of CdBaNumberPackageMapping)

            Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As CdBaNumberPackageMapping
                Dim entry As New CdBaNumberPackageMapping()
                entry.MappingId = GetDbValue(Of Long)(reader, "PkgCDNumID")
                entry.PayDirect = GetDbValue(Of Nullable(Of Boolean))(reader, "PkgCDNumIsPayDirect")
                entry.IsAirport = GetDbValue(Of Nullable(Of Boolean))(reader, "PkgCDNumIsAirport")
                entry.IsAgentExcluded = GetDbValue(Of Boolean)(reader, "PkgCDNumIsExcludeAgn")

                Dim contractNumber As New CdBaNumber()
                CdBaDataSet.PopulateFromReader(contractNumber, reader)
                contractNumber.AccountName = GetDbValue(Of String)(reader, "EuropCarCDAccountName")

                entry.CdBaNumber = contractNumber

                Return entry
            End Function
        End Class

        ''' <summary>
        ''' Used to populate items entries from data reader.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Class CdBaPackageMappedAgentDataSet
            Inherits ListDataSet(Of CdBaPackageMappedAgent)

            Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As CdBaPackageMappedAgent
                Dim entry As New CdBaPackageMappedAgent()
                entry.MappingId = GetDbValue(Of Long)(reader, "PkgCDNumID")
                entry.AgentId = GetDbValue(Of String)(reader, "AgnId")
                entry.AgentCode = GetDbValue(Of String)(reader, "AgnCode")

                Return entry
            End Function
        End Class

    End Class

    Public Class AgentRepository


        Public Overridable Function GetAgentTextInfo( _
            ByVal partialCode As String) As List(Of AgentTextInfo)

            Dim dataSet As New AgentTextDataSet()

            Aurora.Common.Data.ExecuteListSP("FindAgents", _
                                                dataSet, _
                                                New ParamInfo(partialCode, "partialCode") _
                                                )

            Return dataSet.ResultSet
        End Function

        ''' <summary>
        ''' Used to populate log entries from data reader.
        ''' </summary>
        ''' <remarks></remarks>
        Protected Class AgentTextDataSet
            Inherits ListDataSet(Of AgentTextInfo)

            Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As AgentTextInfo
                Dim entry As New AgentTextInfo()
                entry.AgentId = GetDbValue(Of String)(reader, "AgnId")
                entry.AgentCode = GetDbValue(Of String)(reader, "AgnCode")
                entry.AgentName = GetDbValue(Of String)(reader, "AgnName")

                Return entry
            End Function
        End Class

    End Class

End Class
