Imports System.Collections.Generic
Imports System.Xml

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet
Imports Aurora.Configuration.Package

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PackageEnquiry)> _
<AuroraMessageAttribute("GEN044")> _
Partial Class Package_Package
    Inherits AuroraPage

    Private _initException As Exception

    Private _pkgId As String = Nothing
    Private _packageDataSet As New PackageDataSet()
    Private _packageRow As PackageRow

    Private _promoTileLinkUrl As String

    Protected ReadOnly Property PromoTileLinkUrl As String
        Get
            If _promoTileLinkUrl Is Nothing Then
                Dim objElement As PromoLinkElement = PackageConfiguration.Settings.PromoTileLink

                If objElement Is Nothing Then
                    Throw New Exception("Promotional tile link element is not defined in the configuration section.")
                End If

                _promoTileLinkUrl = objElement.Url

                If _promoTileLinkUrl IsNot Nothing Then
                    _promoTileLinkUrl = _promoTileLinkUrl.Trim()
                    If _promoTileLinkUrl.Length = 0 Then
                        _promoTileLinkUrl = Nothing
                    End If
                End If

                If _promoTileLinkUrl Is Nothing Then
                    Throw New Exception("Promotional tile link URL is not specified in the configuration section.")
                End If
            End If

            Return _promoTileLinkUrl
        End Get
    End Property

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.PackageMaintenance)
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If String.IsNullOrEmpty(_pkgId) Then
                Return "New Package"
            ElseIf CanMaintain Then
                Return "Edit Package"
            Else
                Return "View Package"
            End If
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("pkgId")) Then _
                _pkgId = Request.QueryString("pkgId").Trim()

            MyBase.OnInit(e)

            _packageDataSet.EnforceConstraints = False

            DataRepository.GetLookups(_packageDataSet, Me.CompanyCode)

            If Not String.IsNullOrEmpty(_pkgId) Then
                DataRepository.GetPackage(_packageDataSet, Me.CompanyCode, _pkgId)
                _packageRow = _packageDataSet.Package.FindByPkgId(_pkgId)
                If _packageRow Is Nothing Then
                    Throw New Exception("No package found")
                End If
            End If

            typeDropDown.Items.Clear()
            typeDropDown.Items.Add("")
            For Each codeRow As CodeRow In _packageDataSet.Code
                If codeRow.CodCdtNum = codeRow.CodeType_Package_Type Then _
                    typeDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            Next

            brandDropDown.Items.Clear()
            For Each brandRow As BrandRow In _packageDataSet.Brand
                brandDropDown.Items.Add(New ListItem(brandRow.Description, brandRow.Code))
            Next

            countryDropDown.Items.Clear()
            For Each countryRow As CountryRow In _packageDataSet.Country
                countryDropDown.Items.Add(New ListItem(countryRow.Description, countryRow.Code))
            Next

            currencyDropDown.Items.Clear()
            Dim dvCurrency As Data.DataView = New Data.DataView()
            dvCurrency.Table = _packageDataSet.Code
            dvCurrency.Sort = "CodCode"

            'For Each codeRow As CodeRow In _packageDataSet.Code
            '    If codeRow.CodCdtNum = codeRow.CodeType_Currency Then _
            '        currencyDropDown.Items.Add(New ListItem(codeRow.CodCode & " - " & codeRow.CodDesc, codeRow.CodId))
            'Next

            For Each oRow As Data.DataRow In dvCurrency.ToTable().Rows
                If oRow("CodCdtNum") = Aurora.Package.Data.PackageDataSet.CodeRow.CodeType_Currency Then
                    currencyDropDown.Items.Add(New ListItem(oRow("CodCode") & " - " & oRow("CodDesc"), oRow("CodId")))
                End If
            Next

            For Each packageUserControl As PackageUserControl In New PackageUserControl() {saleableProductsControl, flexLevelControl, agentControl, marketControl, nonAvailabilityControl, locationsControl, weeklyProfileControl, noteControl}
                packageUserControl.InitPackageUserControl(_packageDataSet, _packageRow)
            Next

            PackageSetupControl.PkgId = _pkgId

            If _packageDataSet.SaleableProduct.Count > 0 Then
                ' there are SAPs so enable selection
                statusRadioButtonList.Enabled = True
            Else
                ' no SAPs so disable
                statusRadioButtonList.Enabled = False
                statusRadioButtonList.SelectedIndex = 1
            End If



            lnkPromoTile.NavigateUrl = PromoTileLinkUrl
        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Private Sub BuildPackage(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = ""
        nameTextBox.Text = ""
        descriptionTextBox.Text = ""
        notesTextBox.Text = ""
        typeDropDown.SelectedIndex = 0
        brandDropDown.SelectedIndex = 0
        countryDropDown.SelectedIndex = 0
        currencyDropDown.SelectedIndex = 0
        bookedFromTextBox.Text = ""
        bookedToTextBox.Text = ""
        travelFromTextBox.Text = ""
        travelToTextBox.Text = ""
        For Each listItem As ListItem In flagsCheckBoxList.Items
            If listItem.Value = "PkgDispPrice" Then
                listItem.Selected = True
            Else
                listItem.Selected = False
            End If
        Next
        statusRadioButtonList.Items(0).Selected = False
        statusRadioButtonList.Items(1).Selected = True
        statusRadioButtonList.Items(2).Selected = False

        txtPromoTileText.Text = ""

        If _packageRow Is Nothing Then
            packageTabContainer.Style.Add("display", "none")
            ''rev:mia dec3 - always clear the textbox
            ''codeTextBox.Text = "" & CookieValue("Package_pkgCode")
            nameTextBox.Text = "" & CookieValue("Package_pkgName")
            bookedFromTextBox.Text = "" & CookieValue("Package_pkgBookedFromDate")
            bookedToTextBox.Text = "" & CookieValue("Package_pkgBookedToDate")
            travelFromTextBox.Text = "" & CookieValue("Package_pkgTravelFromDate")
            travelToTextBox.Text = "" & CookieValue("Package_pkgTravelToDate")

            Try
                If Not String.IsNullOrEmpty(CookieValue("Package_pkgBrdCode")) Then
                    brandDropDown.SelectedValue = CookieValue("Package_pkgBrdCode")
                End If
            Catch
            End Try

            Try
                If Not String.IsNullOrEmpty(CookieValue("Package_pkgCodTypId")) Then
                    typeDropDown.SelectedValue = CookieValue("Package_pkgCodTypId")
                End If
            Catch
            End Try

            Try
                If Not String.IsNullOrEmpty(CookieValue("Package_pkgCtyCode")) Then
                    countryDropDown.SelectedValue = CookieValue("Package_pkgCtyCode")
                    lnkCdBaNumbers.NavigateUrl = "EditCdBaNumbers.aspx?country=" + CookieValue("Package_pkgCtyCode")
                End If
            Catch
            End Try

            Try
                If Not String.IsNullOrEmpty(CookieValue("Package_pkgIsActive")) Then
                    statusRadioButtonList.SelectedValue = CookieValue("Package_pkgIsActive")
                End If
            Catch
            End Try

            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(PackageConstants.InactiveColor)

            publishButton.Visible = False
            updateButton.Visible = False
            createButton.Visible = CanMaintain
        Else
            packageTabContainer.Style.Add("display", "")

            codeTextBox.Text = _packageRow.PkgCode
            codeTextBox.ReadOnly = True

            If Not _packageRow.IsPkgNameNull Then nameTextBox.Text = _packageRow.PkgName
            nameTextBox.ReadOnly = Not CanMaintain
            If Not _packageRow.IsPkgDescNull Then descriptionTextBox.Text = _packageRow.PkgDesc
            descriptionTextBox.ReadOnly = Not CanMaintain
            If Not _packageRow.IsPkgCommentsNull Then notesTextBox.Text = _packageRow.PkgComments
            notesTextBox.ReadOnly = Not CanMaintain

            Try
                If Not _packageRow.IsPkgCodTypIdNull Then typeDropDown.SelectedValue = _packageRow.PkgCodTypId
            Catch
            End Try
            ''rev:mia dec3 - always enabled
            typeDropDown.Enabled = True
            flexTabPanel.Enabled = typeDropDown.SelectedItem.Text = "Flex"

            Try
                brandDropDown.SelectedValue = _packageRow.PkgBrdCode
            Catch
            End Try
            ''rev:mia dec3 - always enabled
            brandDropDown.Enabled = True

            Try
                If Not _packageRow.IsPkgCtyCodeNull Then
                    countryDropDown.SelectedValue = _packageRow.PkgCtyCode
                    lnkCdBaNumbers.NavigateUrl = "EditCdBaNumbers.aspx?country=" & _packageRow.PkgCtyCode & "&package=" & _pkgId
                End If
            Catch
            End Try
            ''rev:mia dec3 - always enabled
            countryDropDown.Enabled = True

            Try
                currencyDropDown.SelectedValue = _packageRow.PkgCodCurrId
            Catch
            End Try
            currencyDropDown.Enabled = CanMaintain

            If Not _packageRow.IsPkgBookedFromDateNull Then bookedFromTextBox.Date = _packageRow.PkgBookedFromDate
            bookedFromTextBox.ReadOnly = Not CanMaintain
            If Not _packageRow.IsPkgBookedToDateNull Then bookedToTextBox.Date = _packageRow.PkgBookedToDate
            bookedToTextBox.ReadOnly = Not CanMaintain
            If Not _packageRow.IsPkgTravelFromDateNull Then travelFromTextBox.Date = _packageRow.PkgTravelFromDate
            travelFromTextBox.ReadOnly = Not CanMaintain
            If Not _packageRow.IsPkgTravelToDateNull Then travelToTextBox.Date = _packageRow.PkgTravelToDate
            travelToTextBox.ReadOnly = Not CanMaintain

            For Each listItem As ListItem In flagsCheckBoxList.Items
                listItem.Selected = _packageRow.Table.Columns.Contains(listItem.Value) _
                 AndAlso Not _packageRow.IsNull(listItem.Value) _
                 AndAlso CType(_packageRow(listItem.Value), Boolean)
            Next
            flagsCheckBoxList.Enabled = CanMaintain

            For Each listItem As ListItem In statusRadioButtonList.Items
                listItem.Selected = _packageRow.PkgIsActive = listItem.Value
            Next
            'statusRadioButtonList.Enabled = CanMaintain
            If _packageDataSet.SaleableProduct.Count > 0 And CanMaintain Then
                ' there are SAPs so enable selection
                statusRadioButtonList.Enabled = True
            Else
                ' no SAPs so disable
                statusRadioButtonList.Enabled = False
                statusRadioButtonList.SelectedIndex = 1
            End If

            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(_packageRow.StatusColor(Date.Now))

            publishButton.Visible = False
            updateButton.Visible = CanMaintain
            createButton.Visible = False

            If Not _packageRow.IsPkgEnableTileOnB2CNull() Then
                chkEnablePromoTile.Checked = _packageRow.PkgEnableTileOnB2C
            End If

            If Not _packageRow.IsPkgTileTextNull() Then
                txtPromoTileText.Text = ReplaceBarsWithNewlines(_packageRow.PkgTileText)
            End If
        End If

        chkEnablePromoTile.Enabled = CanMaintain
        'chkEnablePromoTile.Checked = txtPromoTileText.Text.Length > 0
    End Sub

 

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack OrElse _initException IsNot Nothing Then Return

        'Added by Raj, Dec 7, 2011 -------------------------------------------------------------

        Dim retVal As Integer = CheckUserIsInRole(UserId, "BSSpkg")

        If (retVal = 1) Then
            Session("BSSUser") = True
        Else
            Session("BSSUser") = False
        End If
        'Added by Raj, Dec 7, 2011 -------------------------------------------------------------


        If Not String.IsNullOrEmpty(Me.Request.QueryString(PackageUserControl.TabParam)) Then
            For i As Integer = 0 To tabs.Tabs.Count - 1
                If tabs.Tabs(i).HeaderText = Me.Request.QueryString(PackageUserControl.TabParam) Then
                    tabs.ActiveTabIndex = i
                End If
            Next
        End If

        If (Me.Request.QueryString.AllKeys.Length > 0) Then
            If (Me.Request.QueryString.AllKeys(0).ToString().ToLower() = "pkgid") Then
                For i As Integer = 0 To tabs.Tabs.Count - 1
                    If tabs.Tabs(i).HeaderText.ToLower() = "web package" Then
                        tabs.ActiveTabIndex = i
                    End If
                Next
            End If
        End If

        BuildPackage(False)

        If Not String.IsNullOrEmpty(Request.QueryString("Create")) Then
            Me.AddInformationMessage("Package created successfully")
        ElseIf Not String.IsNullOrEmpty(Request.QueryString("Update")) Then
            Me.AddInformationMessage("Package updated successfully")
        End If
    End Sub
    'Added by Raj, Dec 7, 2011 -------------------------------------------------------------
    Public Shared Function CheckUserIsInRole(ByVal usrID As String, ByVal roleCode As String) As Integer
        Dim retCount As Integer
        Try
            Dim params(2) As Aurora.Common.Data.Parameter
            params(0) = New Aurora.Common.Data.Parameter("usrID", System.Data.DbType.String, usrID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            params(1) = New Aurora.Common.Data.Parameter("roleCode", System.Data.DbType.String, roleCode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
            Dim count As Integer = 0
            params(2) = New Aurora.Common.Data.Parameter("count", System.Data.DbType.Int32, count, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)
            Aurora.Common.Data.ExecuteOutputSP("SP_CheckUserForRole", params)
            retCount = params(2).Value

        Catch ex As Exception
            Throw New Exception("ERROR: Getting user status for role.")
        End Try
        Return retCount

    End Function
    'Added by Raj, Dec 7, 2011 -------------------------------------------------------------

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Dim url As String = "Search.aspx"
        ''rev:mia dec8
        If Not String.IsNullOrEmpty(Request.QueryString("vhrid")) AndAlso Me.BackUrl.Contains("bookingprocess.aspx") = False Then
            url = Me.BackUrl & IIf(Me.BackUrl.IndexOf("?") <> -1, "&", "?") & "vhrid=" & Me.Server.UrlEncode(Request.QueryString("vhrid"))
        Else
            If Me.BackUrl.Contains("bookingprocess.aspx") = True Then
                url = "~/../web/booking/bookingprocess.aspx?vhrid=" & Me.Server.UrlEncode(Request.QueryString("vhrid"))
            End If
        End If
        ''rev:mia june 26,2009
        'Me.PackageSetupControl.tableXML = String.Empty
        Response.Redirect(url)
    End Sub

    Private Sub ValidatePackage(ByVal create As Boolean)
        If create Then
            If codeTextBox.Text.Trim() = "" Then
                Throw New Aurora.Common.ValidationException("Code cannot be blank")
            End If

            Dim newPackageDataSet As New PackageDataSet
            DataRepository.GetPackageByCode(newPackageDataSet, Me.CompanyCode, codeTextBox.Text.Trim())
            ' Shoel : 1.12.8 : Not needed
            'For Each packageRow0 As PackageRow In newPackageDataSet.Package
            '    If packageRow0.PkgCode.ToLower().Trim() = codeTextBox.Text.ToLower().Trim() Then
            '        Throw New Aurora.Common.ValidationException("Code must be unique")
            '    End If
            'Next
            ' Shoel : 1.12.8 : Not needed
        End If

        If nameTextBox.Text.Trim() = "" Then
            Throw New Aurora.Common.ValidationException("Name cannot be blank")
        End If

        If Not bookedFromTextBox.IsValid _
         OrElse Not bookedToTextBox.IsValid _
         OrElse Not travelFromTextBox.IsValid _
         OrElse Not travelToTextBox.IsValid _
         OrElse bookedFromTextBox.Date > bookedToTextBox.Date _
         OrElse travelFromTextBox.Date > travelToTextBox.Date Then
            Throw New Aurora.Common.ValidationException("Booked and Travel From/To dates must be valid")
        End If

        If String.IsNullOrEmpty(bookedFromTextBox.Text) _
         OrElse String.IsNullOrEmpty(bookedToTextBox.Text) _
         OrElse String.IsNullOrEmpty(travelFromTextBox.Text) _
         OrElse String.IsNullOrEmpty(travelToTextBox.Text) _
        Then
            Throw New Aurora.Common.ValidationException("Booked and Travel From/To dates must be entered")
        End If

        ' additions by Shoel
        If bookedToTextBox.Date > travelToTextBox.Date Then
            Throw New Aurora.Common.ValidationException(GetMessage("GEN044"))
        End If

    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If Not CanMaintain Then Return

        Try
            ValidatePackage(True)
        Catch ex As Aurora.Common.ValidationException
            BuildPackage(True)
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        Try
            _packageRow = _packageDataSet.Package.NewPackageRow()
            _packageRow.PkgId = DataRepository.GetNewId()

            _packageRow.PkgCode = codeTextBox.Text.Trim()
            _packageRow.PkgName = nameTextBox.Text
            _packageRow.PkgDesc = descriptionTextBox.Text
            If notesTextBox.Text.Length > 500 Then
                _packageRow.PkgComments = notesTextBox.Text.Substring(0, 500)
            Else
                _packageRow.PkgComments = notesTextBox.Text
            End If
            If typeDropDown.SelectedValue <> "" Then
                _packageRow.PkgCodTypId = typeDropDown.SelectedValue
            Else
                _packageRow.SetPkgCodTypIdNull()
            End If

            _packageRow.PkgBrdCode = brandDropDown.SelectedValue

            _packageRow.PkgCtyCode = countryDropDown.SelectedValue
            _packageRow.PkgCodCurrId = currencyDropDown.SelectedValue

            _packageRow.PkgBookedFromDate = bookedFromTextBox.Date
            _packageRow.PkgBookedToDate = bookedToTextBox.Date.AddHours(23).AddMinutes(59).AddSeconds(59)
            _packageRow.PkgTravelFromDate = travelFromTextBox.Date
            _packageRow.PkgTravelToDate = travelToTextBox.Date.AddHours(23).AddMinutes(59).AddSeconds(59)

            For Each listItem As ListItem In flagsCheckBoxList.Items
                If _packageRow.Table.Columns.Contains(listItem.Value) Then
                    _packageRow(listItem.Value) = listItem.Selected
                End If
            Next

            _packageRow.PkgIsActive = statusRadioButtonList.SelectedValue

            Dim strTileText As String = ReplaceNewlinesWithBars(txtPromoTileText.Text).Trim()

            'If strTileText.Length = 0 OrElse Not chkEnablePromoTile.Checked Then
            If strTileText.Length = 0 Then
                _packageRow.SetPkgTileTextNull()
            Else
                _packageRow.PkgTileText = strTileText
            End If

            _packageRow.PkgEnableTileOnB2C = chkEnablePromoTile.Checked

            _packageDataSet.Package.AddPackageRow(_packageRow)

            Aurora.Common.Data.ExecuteDataRow(_packageRow, Me.UserCode, Me.PrgmName)
        Catch ex As Exception
            BuildPackage(True)
            Me.AddErrorMessage("Error creating package")
            Return
        End Try

        Response.Redirect(PackageUserControl.MakePackageUrl(Me.Page, pkgId:=_packageRow.PkgId) & "&Create=true")
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return
        If _packageRow Is Nothing Then Return

        Try
            ValidatePackage(False)
        Catch ex As Aurora.Common.ValidationException
            BuildPackage(True)
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                _packageRow.PkgName = nameTextBox.Text
                _packageRow.PkgDesc = descriptionTextBox.Text
                If notesTextBox.Text.Length > 500 Then
                    _packageRow.PkgComments = notesTextBox.Text.Substring(0, 500)
                Else
                    _packageRow.PkgComments = notesTextBox.Text
                End If
                _packageRow.PkgCodCurrId = currencyDropDown.SelectedValue

                ' Shoel : 10.12.8 : Added because they were missing :)
                _packageRow.PkgBrdCode = brandDropDown.SelectedValue
                _packageRow.PkgCtyCode = countryDropDown.SelectedValue
                If typeDropDown.SelectedValue <> "" Then
                    _packageRow.PkgCodTypId = typeDropDown.SelectedValue
                Else
                    _packageRow.SetPkgCodTypIdNull()
                End If
                ' Shoel : 10.12.8 : Added because they were missing :)

                _packageRow.PkgBookedFromDate = bookedFromTextBox.Date
                _packageRow.PkgBookedToDate = bookedToTextBox.Date.AddHours(23).AddMinutes(59).AddSeconds(59)
                _packageRow.PkgTravelFromDate = travelFromTextBox.Date
                _packageRow.PkgTravelToDate = travelToTextBox.Date.AddHours(23).AddMinutes(59).AddSeconds(59)

                For Each listItem As ListItem In flagsCheckBoxList.Items
                    If _packageRow.Table.Columns.Contains(listItem.Value) Then
                        _packageRow(listItem.Value) = listItem.Selected
                    End If
                Next

                _packageRow.PkgIsActive = statusRadioButtonList.SelectedValue

                Dim strTileText As String = ReplaceNewlinesWithBars(txtPromoTileText.Text).Trim()

                'If strTileText.Length = 0 OrElse Not chkEnablePromoTile.Checked Then
                If strTileText.Length = 0 Then
                    _packageRow.SetPkgTileTextNull()
                Else
                    _packageRow.PkgTileText = strTileText
                End If

                _packageRow.PkgEnableTileOnB2C = chkEnablePromoTile.Checked

                Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(_packageRow, Me.UserCode, Me.PrgmName)

                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                BuildPackage(False)
                Me.AddErrorMessage("Error updating package")
                Aurora.Common.Logging.LogException(Page.AppRelativeVirtualPath, ex)
                Return
            End Try
        End Using
        'BuildPackage(False)
        'Me.AddInformationMessage("Package updated")
        Response.Redirect(PackageUserControl.MakePackageUrl(Me.Page, pkgId:=_packageRow.PkgId) & "&Update=true")
    End Sub

    Protected Function ReplaceBarsWithNewlines(ByVal value As String) As String
        If value Is Nothing Then
            Return value
        End If

        value = value.Replace("|", vbCrLf)

        Return value
    End Function

    Protected Function ReplaceNewlinesWithBars(ByVal value As String) As String
        If value Is Nothing Then
            Return value
        End If

        value = value.Replace("|", "").Replace(vbCrLf, "|").Replace(vbLf, "|")

        Return value
    End Function
End Class
