﻿''rev:mia Sept 16 2013 - add ComCode

Option Strict On
Option Explicit On

Imports System
Imports System.Data
Imports System.Data.Common
Imports System.Web
Imports System.Configuration

Imports System.Collections
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Package.Data


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DiscountProfileSearch)> _
Partial Class DiscountProfileSearch
    Inherits AuroraPage

    Protected Shared mStateKey As String = Guid.NewGuid().ToString().Replace("-", "")

    Protected Shared mActiveColor As String = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ActiveColor)
    Protected Shared mInactiveColor As String = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor)

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)
        'If Not Page.IsPostBack Then
        '    PopulateControls()
        '    RestoreState()
        'End If
    End Sub

    Protected Sub PopulateControls()

        drpDiscountCode.Items.Add("")
        drpDiscountType.Items.Add("")

        Dim repo As New DiscountProfileSearchRepository()

        Dim filters As DiscountProfileFilters = repo.GetSearchCriteriaData(ComCode)

        If filters IsNot Nothing Then
            For Each code As String In filters.Codes
                drpDiscountCode.Items.Add(code)
            Next

            For Each codeType As DiscountProfileCodeType In filters.CodeTypes
                drpDiscountType.Items.Add(New ListItem(codeType.Name, codeType.CodeId))
            Next
        End If
    End Sub


    Protected Sub Search()
        Dim repo As New DiscountProfileSearchRepository()

        Dim isActive As Nullable(Of Boolean)

        If rblStatus.SelectedIndex = 1 Then
            isActive = True
        ElseIf rblStatus.SelectedIndex = 2 Then
            isActive = False
        End If


        Dim discounts As List(Of DiscountProfile) _
            = repo.GetSearchResults( _
                NullIfEmpty(drpDiscountCode.SelectedValue), _
                NullIfEmpty(drpDiscountType.SelectedValue), _
                GetDateFromPicker(pkrTravelFrom), _
                GetDateFromPicker(pkrTravelTo), _
                GetDateFromPicker(pkrBookedFrom), _
                GetDateFromPicker(pkrBookedTo), _
                GetDateFromPicker(pkrPickupFrom), _
                GetDateFromPicker(pkrPickupTo), _
                NullIfEmpty(pkrPackage.DataId), _
                NullIfEmpty(pkrAgent.DataId), _
                isActive, _
                NullIfEmpty(ComCode) _
            )

        rptDiscounts.DataSource = discounts
        rptDiscounts.DataBind()
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Search()
    End Sub

    Protected Sub ResetFields()
        RestoreState(New DiscountProfileSearchState())
    End Sub

    Protected Sub RestoreState()
        Dim state As DiscountProfileSearchState = DirectCast(Session(mStateKey), DiscountProfileSearchState)

        Dim autoSearch As Boolean

        If state Is Nothing Then
            state = New DiscountProfileSearchState()
            autoSearch = False
        Else
            autoSearch = True
        End If

        RestoreState(state)
        If (autoSearch) Then
            Search()
        End If
    End Sub

    Protected Sub RestoreState(ByVal state As DiscountProfileSearchState)
        drpDiscountCode.SelectedIndex = state.CodeIndex
        drpDiscountType.SelectedIndex = state.TypeIndex
        pkrTravelFrom.Text = state.BookedFrom

        pkrTravelFrom.Text = state.TravelFrom
        pkrTravelTo.Text = state.TravelTo

        pkrBookedFrom.Text = state.BookedFrom
        pkrBookedTo.Text = state.BookedTo

        pkrPickupFrom.Text = state.PickupFrom
        pkrPickupTo.Text = state.PickupTo

        pkrPackage.DataId = state.PackageId
        pkrPackage.Text = state.PackageText

        pkrAgent.DataId = state.AgentId
        pkrAgent.Text = state.AgentText

        rblStatus.SelectedIndex = state.ActiveIndex

        ''rev:mia Sept 16 2013 - add ComCode
        'Try
        '    ddlComCode.SelectedIndex = ddlComCode.Items.IndexOf(ddlComCode.Items.FindByText(state.ComCode))
        'Catch ex As Exception
        '    ddlComCode.SelectedIndex = 0
        'End Try


    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        ResetFields()
    End Sub

    Protected Sub SaveState()
        Dim state As New DiscountProfileSearchState()

        state.CodeIndex = drpDiscountCode.SelectedIndex
        state.TypeIndex = drpDiscountType.SelectedIndex
        state.BookedFrom = pkrTravelFrom.Text

        state.TravelFrom = pkrTravelFrom.Text
        state.TravelTo = pkrTravelTo.Text

        state.BookedFrom = pkrBookedFrom.Text
        state.BookedTo = pkrBookedTo.Text

        state.PickupFrom = pkrPickupFrom.Text
        state.PickupTo = pkrPickupTo.Text

        state.PackageId = pkrPackage.DataId
        state.PackageText = pkrPackage.Text

        state.AgentId = pkrAgent.DataId
        state.AgentText = pkrAgent.Text

        state.ActiveIndex = rblStatus.SelectedIndex

        ''rev:mia Sept 16 2013 - add ComCode
        ''state.ComCode = ddlComCode.SelectedItem.Value

        Session(mStateKey) = state
    End Sub

    Protected Sub editDiscountProfileClick(ByVal sender As Object, ByVal e As CommandEventArgs)
        SaveState()
        Response.Redirect("EditPackageDiscount.aspx?DisProID=" + CStr(e.CommandArgument))
    End Sub

    Protected Function GetLinkedProducts(ByVal discount As DiscountProfile) As String
        Dim builder As New StringBuilder()

        For Each product As DiscountProduct In discount.Products
            If builder.Length > 0 Then
                builder.Append(", ")
            End If
            builder.Append(product.ProductName)
        Next

        Return HttpUtility.HtmlEncode(builder.ToString())
    End Function

#Region "Utility_Methods"
    'Utility methods
    Protected Function NullIfEmpty(ByVal value As String) As String
        If String.IsNullOrEmpty(value) Then
            Return Nothing
        End If

        If value.Trim = "" Then
            Return Nothing
        End If

        Return value
    End Function

    Protected Function GetDateFromPicker(ByVal source As UserControls_DateControl) As Nullable(Of DateTime)
        If String.IsNullOrEmpty(source.Text) Then
            Return Nothing
        End If

        Return source.Date
    End Function

   
#End Region

    Public Class DiscountProfileSearchState
        Public CodeIndex As Integer
        Public TypeIndex As Integer
        Public TravelFrom As String = ""
        Public TravelTo As String = ""
        Public BookedFrom As String = ""
        Public BookedTo As String = ""
        Public PickupFrom As String = ""
        Public PickupTo As String = ""
        Public PackageId As String = ""
        Public PackageText As String = ""
        Public AgentId As String = ""
        Public AgentText As String = ""
        Public ComCode As String = "" ''rev:mia Sept 16 2013 - add ComCode

        Public ActiveIndex As Integer
    End Class

#Region "rev:mia Sept 16 2013 - add ComCode"
    Private Property ComCode As String
        Get
            Return CStr(ViewState("ComCode"))
        End Get
        Set(value As String)
            ViewState("ComCode") = value
        End Set
    End Property

    Function GetComCode() As String
        Dim ds As New DataSet
        Aurora.Common.Data.ExecuteDataSetSP("User_GetSettings", ds, UserCode)
        Return ds.Tables(0).Rows(0)("ComCode").ToString
    End Function
    Sub PopulateComCode()

        Dim ds As New DataSet
        Aurora.Common.Data.ExecuteDataSetSql("SELECT ComCode FROM dbo.Company", ds)

        ddlComCode.AppendDataBoundItems = True
        ddlComCode.Items.Add(New ListItem("", ""))
        ddlComCode.DataSource = ds.Tables(0).DefaultView
        ddlComCode.DataTextField = "ComCode"
        ddlComCode.DataValueField = "ComCode"
        ddlComCode.DataBind()
    End Sub
    
#End Region



    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            ComCode = GetComCode()
            PopulateControls()
            RestoreState()
        End If
        ''PopulateComCode() ''rev:mia Sept 16 2013 - add ComCode

    End Sub
End Class