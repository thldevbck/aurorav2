<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SaleableProductPicker.aspx.vb" Inherits="Package_SaleableProductPicker" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .searchResultTable td { vertical-align: top; }
        .searchResultTable ul { margin: 0 0 0 16px; padding: 0; }
        .searchResultTable li { margin: 0px; padding: 0; list-style-type: square; }
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <table style="width:100%" cellpadding="2" cellspacing="0">
        <tr>
            <td style="width:150px">Package:</td>
            <td><asp:TextBox ID="packageTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="500px" /></td>
        </tr>
    </table>

    <table style="width:100%;border-top-width: 1px" cellpadding="2" cellspacing="0">
        <tr>
            <td colspan="4"><b>Saleable Product Search:</b></td>
        </tr>
        <tr>
            <td style="width:150px">Short Name:</td>
            <td style="width:250px"><asp:TextBox ID="shortNameTextBox" runat="server" style="width:100px" MaxLength="12" /></td>
            <td style="width:100px">Name:</td>
            <td><asp:TextBox ID="nameTextBox" runat="server" style="width:250px" MaxLength="64" /></td>
        </tr>
        <tr>
            <td>Brand:</td>
            <td>
                <asp:DropDownList ID="brandDropDown" runat="server" style="width:200px">
                    <asp:ListItem />
                </asp:DropDownList>
            </td>
            <td>Type:</td>
            <td>
                <asp:DropDownList ID="typeDropDown" runat="server" style="width:250px">
                    <asp:ListItem />
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>Country:</td>
            <td>
                <%--<asp:TextBox ID="countryTextBox" runat="server" ReadOnly="true" CssClass="inputreadonly" Width="200px" />--%>
                <asp:DropDownList ID="countryDropDown" runat="server" Width="200px" />
            </td>
            <td>Flags:</td>
            <td>
                <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="repeatLabelTable">
                    <asp:ListItem Value="sapIsBase">Base</asp:ListItem>
                    <asp:ListItem Value="sapIsFlex">Flex</asp:ListItem>
                </asp:CheckBoxList>
            </td>
        </tr>
        <tr>
            <td>Contains Package:</td>
            <td colspan="3"><uc1:PickerControl ID="packagePicker" runat="server" Width="400px" PopupType="PACKAGE" AppendDescription="true" /></td>
        </tr>
        <tr>
            <td style="border-top-width: 1px; border-top-style: dotted">Status:</td>
            <td colspan="3" style="border-top-width: 1px; border-top-style: dotted">
                <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="6" RepeatLayout="Table" CssClass="repeatLabelTable">
                    <%--<asp:ListItem Text="Current+Future" Value="Current" Selected="true" />
                    <asp:ListItem Text="Active" Value="Active" />
                    <asp:ListItem Text="Pending" Value="Pending" />
                    <asp:ListItem Text="Inactive" Value="Inactive" />
                    <asp:ListItem Text="All" Value="All" />--%>
                    <asp:ListItem Text="Active-Current" Selected="True" />
                    <asp:ListItem Text="Active-Future" />
                    <asp:ListItem Text="Active-Past" />
                    <asp:ListItem Text="Pending" />
                    <asp:ListItem Text="Inactive" />
                    <asp:ListItem Text="All"  />
                </asp:RadioButtonList>
            </td>
        </tr>
    </table>
    
    <asp:CheckBox ID="searchCheckBox" runat="server" Style="display: none" Checked="true" />

    <table style="width:100%" cellpadding="2" cellspacing="0">
        <tr>
            <td style="border-width: 1px 0px"><i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;</td>
            <td align="right" style="border-width: 1px 0px">
                <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
    </table>
    
    <asp:Panel runat="server" ID="packageProductPanel" Visible="false">
        <table cellpadding="2" cellspacing="0" width="100%" style="border-top-width: 1px">
            <tr>
                <td width="150">Add Type:</td>
                <td colspan="2">
                    <asp:RadioButtonList ID="copyCreateLinkRadioButtonList" runat="server" RepeatDirection="Horizontal" CssClass="repeatLabelTable">
                        <asp:ListItem Text="Link" Selected="True" />
                        <asp:ListItem Text="Create" />
                        <asp:ListItem Text="Copy" />
                    </asp:RadioButtonList>
                </td> 
                <td align="right">
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" />
                </td>
            </tr>
        </table>

        <asp:Table ID="searchResultTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%" CssClass="dataTableColor searchResultTable">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Width="20px">
                    <input type="checkbox" id="chkSelectAll" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                </asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Name</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 80px">Type</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Brand</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Flags</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Status</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>
    
    </asp:Panel>

    <br />
    
</asp:Content>

