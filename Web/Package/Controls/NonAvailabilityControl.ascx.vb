Imports System.Collections.Generic

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


<ToolboxData("<{0}:NonAvailabilityControl runat=server></{0}:NonAvailabilityControl>")> _
Partial Class Package_NonAvailabilityControl
    Inherits PackageUserControl

    Private _loadException As Exception
    Private _checkBoxesByNapId As New Dictionary(Of String, CheckBox)
    Private _nonAvailabilityByLinkButtonId As New Dictionary(Of String, NonAvailabilityProfileRow)

    Public Sub BuildNonAvailabilities()
        _checkBoxesByNapId.Clear()
        _nonAvailabilityByLinkButtonId.Clear()
        While nonAvailabilityTable.Rows.Count > 1
            nonAvailabilityTable.Rows.RemoveAt(1)
        End While

        If Me.PackageRow Is Nothing Then Return

        Dim rowIndex As Integer = 0
        For Each nonAvailabilityProfileRow As NonAvailabilityProfileRow In Me.PackageRow.GetNonAvailabilityProfileRows()
            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "evenRow", "oddRow") : rowIndex += 1
            nonAvailabilityTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & nonAvailabilityProfileRow.NapId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByNapId.Add(nonAvailabilityProfileRow.NapId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim typeCell As New TableCell
            tableRow.Controls.Add(typeCell)

            If CanMaintain Then
                Dim linkButton As New LinkButton
                linkButton.ID = "editNonAvailability" & nonAvailabilityProfileRow.NapId & "LinkButton"
                linkButton.Text = "Non-Availability"
                typeCell.Controls.Add(linkButton)
                _nonAvailabilityByLinkButtonId.Add(linkButton.ID, nonAvailabilityProfileRow)
                AddHandler linkButton.Click, AddressOf editButton_Click
            Else
                typeCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim travelFromTableCell As New TableCell
            If Not nonAvailabilityProfileRow.IsNapTravelFromDateNull Then travelFromTableCell.Text = nonAvailabilityProfileRow.NapTravelFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            tableRow.Controls.Add(travelFromTableCell)

            Dim travelToTableCell As New TableCell
            If Not nonAvailabilityProfileRow.IsNapTravelToDateNull Then travelToTableCell.Text = nonAvailabilityProfileRow.NapTravelToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            tableRow.Controls.Add(travelToTableCell)

            Dim bookedFromTableCell As New TableCell
            If Not nonAvailabilityProfileRow.IsNapBookedFromDateNull Then bookedFromTableCell.Text = nonAvailabilityProfileRow.NapBookedFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            tableRow.Controls.Add(bookedFromTableCell)

            Dim bookedToTableCell As New TableCell
            If Not nonAvailabilityProfileRow.IsNapBookedToDateNull Then bookedToTableCell.Text = nonAvailabilityProfileRow.NapBookedToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            tableRow.Controls.Add(bookedToTableCell)

            Dim ckoFromTableCell As New TableCell
            If Not nonAvailabilityProfileRow.IsNapCkoFromDateNull Then ckoFromTableCell.Text = nonAvailabilityProfileRow.NapCkoFromDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            tableRow.Controls.Add(ckoFromTableCell)

            Dim ckoToTableCell As New TableCell
            If Not nonAvailabilityProfileRow.IsNapCkoToDateNull Then ckoToTableCell.Text = nonAvailabilityProfileRow.NapCkoToDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            tableRow.Controls.Add(ckoToTableCell)

            Dim linkCell As New TableCell
            tableRow.Controls.Add(linkCell)
        Next
        addButton.Visible = CanMaintain
        deleteButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        nonAvailabilityPopupCancelButton.Attributes.Add("onclick", "$find('nonAvailabilityPopupBehavior').hide(); return false;")

        Try
            BuildNonAvailabilities()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Page.IsPostBack Then Return
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain Then Return

        nonAvailabilityPopupIdHidden.Value = ""

        nonAvailabilityPopupTravelFromTextBox.Text = ""
        nonAvailabilityPopupTravelToTextBox.Text = ""
        nonAvailabilityPopupBookedFromTextBox.Text = ""
        nonAvailabilityPopupBookedToTextBox.Text = ""
        nonAvailabilityPopupCkoFromTextBox.Text = ""
        nonAvailabilityPopupCkoToTextBox.Text = ""

        nonAvailabilityPopupAddButton.Visible = True
        nonAvailabilityPopupAddNextButton.Visible = True
        nonAvailabilityPopupDeleteButton.Visible = False
        nonAvailabilityPopupUpdateButton.Visible = False
        nonAvailabilityPopupTitleLabel.Text = "Add Non-Availability"
        nonAvailabilityPopup.Show()
    End Sub

    Protected Sub editButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim nonAvailabilityProfileRow As NonAvailabilityProfileRow = _nonAvailabilityByLinkButtonId(CType(sender, LinkButton).ID)
        If nonAvailabilityProfileRow Is Nothing Then Return

        nonAvailabilityPopupIdHidden.Value = nonAvailabilityProfileRow.NapId

        nonAvailabilityPopupTravelFromTextBox.Text = ""
        nonAvailabilityPopupTravelToTextBox.Text = ""
        nonAvailabilityPopupBookedFromTextBox.Text = ""
        nonAvailabilityPopupBookedToTextBox.Text = ""
        nonAvailabilityPopupCkoFromTextBox.Text = ""
        nonAvailabilityPopupCkoToTextBox.Text = ""

        If Not nonAvailabilityProfileRow.IsNapTravelFromDateNull Then nonAvailabilityPopupTravelFromTextBox.Date = nonAvailabilityProfileRow.NapTravelFromDate
        If Not nonAvailabilityProfileRow.IsNapTravelToDateNull Then nonAvailabilityPopupTravelToTextBox.Date = nonAvailabilityProfileRow.NapTravelToDate
        If Not nonAvailabilityProfileRow.IsNapBookedFromDateNull Then nonAvailabilityPopupBookedFromTextBox.Date = nonAvailabilityProfileRow.NapBookedFromDate
        If Not nonAvailabilityProfileRow.IsNapBookedToDateNull Then nonAvailabilityPopupBookedToTextBox.Date = nonAvailabilityProfileRow.NapBookedToDate
        If Not nonAvailabilityProfileRow.IsNapCkoFromDateNull Then nonAvailabilityPopupCkoFromTextBox.Date = nonAvailabilityProfileRow.NapCkoFromDate
        If Not nonAvailabilityProfileRow.IsNapCkoToDateNull Then nonAvailabilityPopupCkoToTextBox.Date = nonAvailabilityProfileRow.NapCkoToDate

        nonAvailabilityPopupAddButton.Visible = False
        nonAvailabilityPopupAddNextButton.Visible = False
        nonAvailabilityPopupDeleteButton.Visible = True
        nonAvailabilityPopupUpdateButton.Visible = True
        nonAvailabilityPopupTitleLabel.Text = "Edit Non-Availability"
        nonAvailabilityPopup.Show()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If Not CanMaintain Then Return

        For Each nonAvailabilityProfileRow As NonAvailabilityProfileRow In Me.PackageRow.GetNonAvailabilityProfileRows()
            Dim checkBox As CheckBox = _checkBoxesByNapId(nonAvailabilityProfileRow.NapId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            nonAvailabilityProfileRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(nonAvailabilityProfileRow)
            Me.PackageDataSet.NonAvailabilityProfile.RemoveNonAvailabilityProfileRow(nonAvailabilityProfileRow)
        Next

        BuildNonAvailabilities()
    End Sub

    Private Sub ValidateNonAvailabilityDateRange(ByVal name As String, ByVal fromTextBox As UserControls_DateControl, ByVal toTextBox As UserControls_DateControl)
        If Not fromTextBox.IsValid Then
            Throw New Aurora.Common.ValidationException("Specify a valid From " & name)
        End If
        If Not toTextBox.IsValid Then
            Throw New Aurora.Common.ValidationException("Specify a valid To " & name)
        End If
        If (String.IsNullOrEmpty(fromTextBox.Text.Trim()) AndAlso Not String.IsNullOrEmpty(toTextBox.Text.Trim())) _
         OrElse (Not String.IsNullOrEmpty(fromTextBox.Text.Trim()) AndAlso String.IsNullOrEmpty(toTextBox.Text.Trim())) Then
            Throw New Aurora.Common.ValidationException("Specify both a From and To " & name)
        End If
        If Not String.IsNullOrEmpty(fromTextBox.Text.Trim()) _
         AndAlso Not String.IsNullOrEmpty(toTextBox.Text.Trim()) _
         AndAlso fromTextBox.Date > toTextBox.Date Then
            Throw New Aurora.Common.ValidationException("From " & name & " may not be greater than the To " & name)
        End If

        If Not fromTextBox.text.trim.equals(String.empty) And name.toupper().indexof("CHECK-OUT") < 0 Then
            If fromTextBox.Date < Now.Date Then
                Throw New Aurora.Common.ValidationException("GEN090 - " & name.replace(" Date", "") & " From Date must be Greater than or equal to Current Date")
            End If
        End If
        
    End Sub

    Private Sub ValidateNonAvailability()
        ValidateNonAvailabilityDateRange("Travel Date", nonAvailabilityPopupTravelFromTextBox, nonAvailabilityPopupTravelToTextBox)
        ValidateNonAvailabilityDateRange("Book Date", nonAvailabilityPopupBookedFromTextBox, nonAvailabilityPopupBookedToTextBox)
        ValidateNonAvailabilityDateRange("Check-out Date", nonAvailabilityPopupCkoFromTextBox, nonAvailabilityPopupCkoToTextBox)

        If String.IsNullOrEmpty(nonAvailabilityPopupTravelFromTextBox.Text.Trim()) _
         AndAlso String.IsNullOrEmpty(nonAvailabilityPopupTravelToTextBox.Text.Trim()) _
         AndAlso String.IsNullOrEmpty(nonAvailabilityPopupBookedFromTextBox.Text.Trim()) _
         AndAlso String.IsNullOrEmpty(nonAvailabilityPopupBookedToTextBox.Text.Trim()) _
         AndAlso String.IsNullOrEmpty(nonAvailabilityPopupCkoFromTextBox.Text.Trim()) _
         AndAlso String.IsNullOrEmpty(nonAvailabilityPopupCkoToTextBox.Text.Trim()) Then
            Throw New Aurora.Common.ValidationException("Specify at least one set of From and To Dates")
        End If
    End Sub

    Private Sub UpdateNonAvailabilityDateRangeFromPopup(ByVal nonAvailabilityProfileRow As NonAvailabilityProfileRow, ByVal fromDate As Date, ByVal toDate As Date, ByVal fromField As String, ByVal toField As String)
        nonAvailabilityProfileRow(fromField) = DBNull.Value
        nonAvailabilityProfileRow(toField) = DBNull.Value
        If fromDate <> Date.MinValue Then
            nonAvailabilityProfileRow(fromField) = fromDate
        End If
        If toDate <> Date.MinValue Then
            nonAvailabilityProfileRow(toField) = toDate
        End If
    End Sub

    Private Function UpdateNonAvailabilityFromPopup(ByVal nonAvailabilityProfileRow As NonAvailabilityProfileRow) As Boolean
        Try
            ValidateNonAvailability()
        Catch ex As Aurora.Common.ValidationException
            messagePanelControl.SetMessagePanelWarning(ex.Message)
            nonAvailabilityPopup.Show()
            Return False
        End Try

        UpdateNonAvailabilityDateRangeFromPopup(nonAvailabilityProfileRow, _
            nonAvailabilityPopupTravelFromTextBox.Date, _
            nonAvailabilityPopupTravelToTextBox.Date, _
            "NapTravelFromDate", _
            "NapTravelToDate")

        UpdateNonAvailabilityDateRangeFromPopup(nonAvailabilityProfileRow, _
            nonAvailabilityPopupBookedFromTextBox.Date, _
            nonAvailabilityPopupBookedToTextBox.Date, _
            "NapBookedFromDate", _
            "NapBookedToDate")

        UpdateNonAvailabilityDateRangeFromPopup(nonAvailabilityProfileRow, _
            nonAvailabilityPopupCkoFromTextBox.Date, _
            nonAvailabilityPopupCkoToTextBox.Date, _
            "NapCkoFromDate", _
            "NapCkoToDate")

        Return True
    End Function

    Protected Sub nonAvailabilityPopupAddButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles nonAvailabilityPopupAddButton.Click, nonAvailabilityPopupAddNextButton.Click
        Dim nonAvailabilityProfileRow As NonAvailabilityProfileRow = Me.PackageDataSet.NonAvailabilityProfile.NewNonAvailabilityProfileRow()
        If Not UpdateNonAvailabilityFromPopup(nonAvailabilityProfileRow) Then Return

        nonAvailabilityProfileRow.NapId = DataRepository.GetNewId()
        nonAvailabilityProfileRow.NapPkgId = Me.PackageRow.PkgId

        Me.PackageDataSet.NonAvailabilityProfile.AddNonAvailabilityProfileRow(nonAvailabilityProfileRow)
        Aurora.Common.Data.ExecuteDataRow(nonAvailabilityProfileRow, UsrId:=Me.CurrentPage.UserId, PrgmName:=Me.CurrentPage.PrgmName)

        BuildNonAvailabilities()

        If sender Is nonAvailabilityPopupAddNextButton Then
            addButton_Click(sender, e)
        End If
    End Sub

    Protected Sub nonAvailabilityPopupUpdateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles nonAvailabilityPopupUpdateButton.Click
        Dim nonAvailabilityProfileRow As NonAvailabilityProfileRow = Me.PackageDataSet.NonAvailabilityProfile.FindByNapId(nonAvailabilityPopupIdHidden.Value)
        If nonAvailabilityProfileRow Is Nothing Then Return
        If Not UpdateNonAvailabilityFromPopup(nonAvailabilityProfileRow) Then Return

        Aurora.Common.Data.ExecuteDataRow(nonAvailabilityProfileRow)

        BuildNonAvailabilities()
    End Sub

    Protected Sub nonAvailabilityPopupDeleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles nonAvailabilityPopupDeleteButton.Click
        Dim nonAvailabilityProfileRow As NonAvailabilityProfileRow = Me.PackageDataSet.NonAvailabilityProfile.FindByNapId(nonAvailabilityPopupIdHidden.Value)
        If nonAvailabilityProfileRow Is Nothing Then Return

        nonAvailabilityProfileRow.Delete()
        Aurora.Common.Data.ExecuteDataRow(nonAvailabilityProfileRow)
        Me.PackageDataSet.NonAvailabilityProfile.RemoveNonAvailabilityProfileRow(nonAvailabilityProfileRow)

        BuildNonAvailabilities()
    End Sub

    Protected Sub nonAvailabilityPopupCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles nonAvailabilityPopupCancelButton.Click
        nonAvailabilityPopup.Hide()
    End Sub

End Class
