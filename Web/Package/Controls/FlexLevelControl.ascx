<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FlexLevelControl.ascx.vb" Inherits="Package_FlexLevelControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <table style="width:100%; border-bottom-width: 1px" cellpadding="2" cellspacing="0" id="flexLevelTable" runat="server">
            <tr>
                <td style="width:150px"><b>Flex Rate Set:</b></td>
                <td colspan="3"><asp:DropDownList ID="flexLevelDropDown" runat="server" style="width:200px" AutoPostBack="True" /></td>
            </tr>
            <tr>
                <td style="width:150px">Effective Date/Time:</td>
                <td style="width:250px">
                    <uc1:DateControl ID="flexLevelEffectiveDateTextBox" runat="server" Width="100px" Nullable="false" />
                </td>
                <td style="width:150px">Start Travel Year:</td>
                <td style="width:250px">
                    <asp:DropDownList ID="flexLevelTravelYearDropDown" runat="server" style="width:75px" AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td colspan="4"><i>Note: changing the effective date on an existing flex rate set will create a new copy</i></td>
            </tr>
        </table>

        <asp:Table CssClass="dataTableGrid" ID="levelTable" runat="server" CellPadding="2" CellSpacing="0" Style="width: 760px; margin-top: 10px">
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell>1</asp:TableHeaderCell>
                <asp:TableHeaderCell>2</asp:TableHeaderCell>
                <asp:TableHeaderCell>3</asp:TableHeaderCell>
                <asp:TableHeaderCell>4</asp:TableHeaderCell>
                <asp:TableHeaderCell>5</asp:TableHeaderCell>
                <asp:TableHeaderCell>6</asp:TableHeaderCell>
                <asp:TableHeaderCell>7</asp:TableHeaderCell>
                <asp:TableHeaderCell>8</asp:TableHeaderCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">A</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">B</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">C</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">D</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">E</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">F</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">G</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableHeaderCell Width="20px">H</asp:TableHeaderCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
                <asp:TableCell><uc1:RegExTextBox runat="server" Width="50px" Nullable="false" RegExPattern="^\d*\.?\d*$" /></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        
        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td><i><asp:Label ID="flexLevelAddModLabel" runat="server" /></i></td>
                <td align="right">
                    <asp:Button ID="flexLevelUpdateButton" runat="server" Text="Save Rates" CssClass="Button_Standard Button_Save" Visible="False" />
                    <asp:Button ID="flexLevelCreateButton" runat="server" Text="Save Rates" CssClass="Button_Standard Button_Save" Visible="True" />
                </td>
            </tr>
        </table>

        <p>&nbsp;</p>
        
    </ContentTemplate>
</asp:UpdatePanel>
        