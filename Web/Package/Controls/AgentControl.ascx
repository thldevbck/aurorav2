<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AgentControl.ascx.vb" Inherits="Package_AgentControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td style="width:150px">Agent Type:</td>
                <td colspan="3">
                    <asp:RadioButtonList ID="agentTypeRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="5" CssClass="repeatLabelTable" />
                </td>
            </tr>
            <tr>
                <td>Agent Group:</td>
                <td colspan="3"><uc1:PickerControl ID="agentGroupPicker" runat="server" AppendDescription="true" PopupType="PACKAGE_AGENTGROUP" Width="300px" /></td>
            </tr>
            <tr>
                <td>Agent Discount Override:</td>
                <td colspan="3">
                    <uc1:RegExTextBox 
                        ID="agentDiscountOverride" 
                        runat="server" 
                        Width="50px" 
                        Nullable="true"
                        RegExPattern="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$" 
                        ErrorMessage="Agent Discount must be a decimal value that is zero or more" />%

                    &nbsp;&nbsp;&nbsp;Extra:&nbsp;
                    <uc1:RegExTextBox 
                        ID="agentDiscountExtra" 
                        runat="server" 
                        Width="50px"
                        Nullable="true"
                        RegExPattern="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$" 
                        ErrorMessage="Agent Extra must be a decimal value that is zero or more" />%
                </td>
            </tr>
            <tr>
                <td>Deposit Required:</td>
                <td colspan="3">
                    <uc1:RegExTextBox 
                        ID="depositRequestTextBox" 
                        runat="server" 
                        Width="50px"
                        Nullable="true"
                        RegExPattern="^\d*[0-9](|.\d*[0-9]|,\d*[0-9])?$" 
                        ErrorMessage="Deposit required must be a decimal value that is zero or more" />
                    &nbsp;&nbsp;&nbsp;
                    <asp:RadioButtonList ID="depositRequestRadioButtonList" runat="server" RepeatLayout="Flow" RepeatColumns="2">
                        <asp:ListItem>Amount&nbsp;&nbsp;&nbsp;</asp:ListItem>
                        <asp:ListItem>%</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
            <tr>
                <td valign="top" style="padding-top: 6px">Lump Sum Description:</td>
                <td colspan="3">
                    <asp:TextBox ID="lumpSumDescTextBox" runat="server" style="width:600px" TextMode="multiLine" Rows="3" />
                </td>
            </tr>
            <tr>
                <td>Flags:</td>
                <td colspan="3">
                    <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3" Width="100%" CssClass="repeatLabelTable">
                        <asp:ListItem Value="PkgSortB4Veh">Sort before Vehicle</asp:ListItem>
                    </asp:CheckBoxList>
                </td>
            </tr>
        </table>
        
        <table style="width:100%" cellpadding="2" cellspacing="0">
            <tr>
                <td colspan="2" align="right">&nbsp;
                    <asp:Button ID="updateButton" runat="server" Text="Save Agents" CssClass="Button_Standard Button_Save" Visible="False" />
                </td>
            </tr>
            <tr style="height: 26px;">
                <td width="255" style="border-width: 1px 0px;">
                    <b>Agents:</b>
                </td>
                <td style="border-width: 1px 0px;">
                    <asp:DropDownList ID="agentsDropDown" runat="server" Width="300px" AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td>
                    <i><asp:Label ID="agentsLabel" runat="server" /></i>&nbsp;
                </td>
                <td align="right">&nbsp;
                    <asp:Button ID="removeButton" runat="Server" Text="Remove" CssClass="Button_Standard Button_Remove" Visible="False" />
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
        </table>    

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td>
                    <asp:Table ID="agentTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableColor" Style="width: 100%">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell Style="width: 20px;">&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell Style="width: 100px;">Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </td>
            </tr>
        </table>    

        <asp:Button runat="server" ID="agentPopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="agentPopup" 
            BehaviorID="agentPopupBehavior"
            TargetControlID="agentPopupButton" 
            PopupControlID="agentPopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="agentPopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup" ID="agentPopupPanel" Style="display: none; width: 500px">
            <asp:Panel runat="Server" ID="agentPopupDragPanel" CssClass="modalPopupTitle" >
                <asp:Label ID="agentPopupTitleLabel" runat="Server" Text="Add Agent" />
            </asp:Panel>

            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td style="width:100px">Agent:</td>
                    <td style="width:50px">&nbsp;</td>
                    <td colspan="3"><uc1:PickerControl ID="agentPopupPicker" runat="server" Width="300px" PopupType="AGENT" /></td>
                </tr>
                <tr><td colspan="5">&nbsp;</td></tr>
                <tr>
                    <td align="right" colspan="5">
                        <asp:Button ID="agentPopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="agentPopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel"  />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>