﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PackageDiscountProfileControl.ascx.vb" Inherits="Package_Controls_PackageDiscountProfileControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanelPackageDiscountProfile" runat="server">
    <ContentTemplate>

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="removePackageProfileButton" runat="Server" Text="Remove" CssClass="Button_Standard Button_Remove" Visible="False" />
                    <asp:Button ID="addPackageProfileButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
        </table>

        <asp:Table ID="packageTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableColor" Width="100%">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Style="width: 20px">
                    <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);"  />        
                </asp:TableHeaderCell>
                <asp:TableHeaderCell>Name</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 50px">Brand</asp:TableHeaderCell>
                <%--<asp:TableHeaderCell Style="width: 100px">Brand</asp:TableHeaderCell>--%>
                <asp:TableHeaderCell Style="width: 140px">Booked</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 140px">Travel</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Status</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>

        <asp:Button runat="server" ID="packageProfileButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="packagePopup" 
            BehaviorID="packageProfileBehavior"
            TargetControlID="packageProfileButton" 
            PopupControlID="packageProfilePopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="packagePopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup" ID="packageProfilePopupPanel" Style="display: none; padding: 10px;" Width="500px">
            <asp:Panel runat="Server" ID="packagePopupDragPanel" CssClass="modalPopupTitle" >
                <asp:Label ID="packageProfilepopupTitleLabel" runat="Server" Text="Add Package" />
            </asp:Panel>

            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td style="width:100px">Package:</td>
                    <td style="width:50px">&nbsp;</td>
                    <td colspan="3"><uc1:PickerControl ID="packagePopupPicker" runat="server" Width="300px" PopupType="DISCOUNTONPACKAGEPROFILE" /></td>
                </tr>

                <%--<tr>
                    <td>Type:</td>
                    <td>&nbsp;</td>
                    <td colspan="3">
                        <asp:RadioButtonList ID="packageProfileTypeRadioButtonList" runat="server" RepeatDirection="Horizontal" Enabled="False" Visible="false">
                            <asp:ListItem Text="Link" Selected="True" />
                            <asp:ListItem Text="Create" />
                            <asp:ListItem Text="Copy" />
                        </asp:RadioButtonList>
                    </td> 
                </tr>--%>
                <tr><td colspan="5">&nbsp;</td></tr>
                <tr>
                    <td align="right" colspan="5">
                        <asp:Button ID="packageProfilePopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="packageProfilePopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>