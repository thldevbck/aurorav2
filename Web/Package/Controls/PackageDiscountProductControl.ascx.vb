﻿''rev:mia Feb.28 2013 - Added a complete and working version
''                    - today in history: Last day of Pope Benedict                        

Imports System.Data
Imports Aurora.Package.Data

Imports System.Collections.Generic

Imports ASP.usercontrols_multiselectcontrol_multiselect_ascx

Partial Class Package_Controls_PackageDiscountProductControl
    Inherits AuroraUserControl

    Private Const _maxCodeLength As Integer = 9
    Private Shared _formatting As String = "{0, -" & _maxCodeLength & "} - {1}"

    Private _MessageInfo As String
    Public Property MessageInfo As String
        Get
            Return _MessageInfo
        End Get
        Set(value As String)
            _MessageInfo = value
        End Set
    End Property

    Private _RequeryURL As String
    Public Property RequeryURL As String
        Get
            Return _RequeryURL
        End Get
        Set(value As String)
            _RequeryURL = value
        End Set
    End Property

    Private _SuccessfulMessage As String
    Public Property SuccessfulMessage As String
        Get
            Return _SuccessfulMessage
        End Get
        Set(value As String)
            _SuccessfulMessage = value
        End Set
    End Property

    Private _FailedMessage As String
    Public Property FailedMessage As String
        Get
            Return _FailedMessage
        End Get
        Set(value As String)
            _FailedMessage = value
        End Set
    End Property

    Private _DisproIdControl As String
    Public Property DisproIdControl As String
        Get
            If (_DisproIdControl Is Nothing) Then
                Return Request.QueryString("DisProID").Trim
            End If
            Return _DisproIdControl
        End Get
        Set(value As String)
            _DisproIdControl = value
        End Set
    End Property

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.DiscountProfileSearch)
        End Get
    End Property

    Private _ProductsInfo() As DataTable
    Public Property ProductsInfo() As DataTable()
        Get
            Return _ProductsInfo
        End Get
        Set(value() As DataTable)
            _ProductsInfo = value
        End Set
    End Property

    Protected Sub Package_Controls_PackageDiscountProductControl_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (ProductsInfo Is Nothing) Then Return
        If ProductsInfo.Length - 1 = -1 Then Return
        Try
            If Not Page.IsPostBack Then
                availableProductListbox.Items.Clear()
                selectedProductListBox.Items.Clear()
                InitProductDiscountProfile()

                InitMultiSelectScript()
            Else
                UpdateSelectedItemsFromClientState()
            End If

            Dim buttonclickAdd As String = "document.getElementById('" & addtoButton.ClientID.ToString() & "').click();"
            availableProductListbox.Attributes.Add("OnDblClick", buttonclickAdd)

            Dim buttonclickRemoved As String = "document.getElementById('" & removefromButton.ClientID.ToString() & "').click();"
            selectedProductListBox.Attributes.Add("OnDblClick", buttonclickRemoved)

        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try

        removefromButton.Enabled = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
        addtoButton.Enabled = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
        saveProductProfileButton.Enabled = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
        saveProductProfileButton.Visible = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True

    End Sub

    Protected Sub UpdateSelectedItemsFromClientState()
        If hidSelectedItems.Value = "" Then
            Return
        End If

        Dim values As List(Of String) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(hidSelectedItems.Value)

        Dim index As Integer = 0
        Dim length As Integer = values.Count
        Dim items As ListItemCollection = selectedProductListBox.Items
        Dim item As ListItem

        While index < length
            item = items(index \ 2)

            item.Text = values(index)
            item.Value = values(index + 1)

            index += 2
        End While
    End Sub

    Protected Sub InitMultiSelectScript()
        ScriptManager.RegisterStartupScript(UpdatePanelPackageDiscountProduct, Me.GetType(), "ProductMultiSelectInit", _
                    ASP.usercontrols_multiselectcontrol_multiselect_ascx.RenderControlToString(plhProductMultiSelectInit), False)
    End Sub


    Sub InitProductDiscountProfile()
        If (String.IsNullOrEmpty(_DisproIdControl)) Then Exit Sub
        Dim shortname As String = ""
        Dim longname As String = ""
        Dim productName As String
        Dim id As String = ""
        Dim maxlength As Integer = 9
        'Dim formatting As String = "{0, -" & maxlength & "} - {1}"
        Dim isVehicle As Boolean
        Dim dependencyIndex As Integer = 0
        'Dim discountProfileProductId As Integer
        

        If (ProductsInfo(0).Rows.Count - 1 <> -1) Then
            'availableProductListbox.DataSource = ProductsInfo(0).DefaultView
            'availableProductListbox.DataTextField = "ProductName"
            'availableProductListbox.DataValueField = "PrdId"
            'availableProductListbox.DataBind()


            For Each row As DataRow In ProductsInfo(0).Rows
                shortname = row("ProductName").ToString.Split("-")(0).Trim
                longname = row("ProductName").ToString.Split("-")(1).Trim.ToLower
                id = row("PrdId").ToString

                Dim itemList As New ListItem
                itemList.Text = String.Format(_formatting, shortname.Trim, longname.Trim)
                itemList.Text = itemList.Text.Replace(" ", HttpUtility.HtmlDecode("&nbsp;"))
                itemList.Value = id


                availableProductListbox.Items.Add(itemList)

            Next
        End If

        Dim dependenciesTable As DataTable = ProductsInfo(2)

        If (ProductsInfo(1).Rows.Count - 1 <> -1) Then
            'selectedProductListBox.DataSource = ProductsInfo(1).DefaultView
            'selectedProductListBox.DataTextField = "ProductName"
            'selectedProductListBox.DataValueField = "PrdId"
            'selectedProductListBox.DataBind()

            ''formatting = "{0, -" & maxlength & "} - {1}"

            For Each row As DataRow In ProductsInfo(1).Rows
                productName = row("ProductName")

                shortname = productName.ToString.Split("-")(0).Trim
                longname = productName.ToString.Split("-")(1).Trim.ToLower
                id = row("PrdId").ToString

                'discountProfileProductId = CInt(row("DisProPID"))

                isVehicle = CBool(row("PrdIsVehicle"))

                Dim item As New ListItem

                If isVehicle Then
                    item.Text = String.Format(_formatting, shortname.Trim, longname.Trim)
                    item.Text = item.Text.Replace(" ", HttpUtility.HtmlDecode("&nbsp;"))
                    item.Value = id
                Else
                    Dim dependencyCount As Integer = CInt(row("DependencyCount"))
                    Dim dependecyItems As List(Of MultiSelectItem) = GetDependencyItems(dependenciesTable.Rows, dependencyIndex, dependencyCount)
                    dependencyIndex += dependencyCount

                    Dim dependenciesText As String = UserControls_SelectControl_MultiSelect.GetSeparatedValueString(dependecyItems, False, True, False)

                    Dim dependenciesValueParts(1) As String

                    dependenciesValueParts(0) = id
                    dependenciesValueParts(1) = UserControls_SelectControl_MultiSelect.GetSeparatedValueString(dependecyItems, True, False, True)


                    item.Text = shortname + " (" + dependenciesText + ")"

                    item.Attributes.Add("title", productName)

                    item.Value = UserControls_SelectControl_MultiSelect.GetCsv(dependenciesValueParts)
                End If


                Dim isExcluded As Object = row("DisProPExclude")
                If Not Object.ReferenceEquals(isExcluded, DBNull.Value) Then
                    productcheckboxexclude.Checked = CBool(isExcluded)
                End If

                selectedProductListBox.Items.Add(item)

            Next
        End If

    End Sub

    Protected Function GetDependencyItems(ByVal rows As DataRowCollection, ByVal index As Integer, ByVal count As Integer) As List(Of MultiSelectItem)
        Dim endIndex As Integer = index + count - 1

        Dim list As New List(Of MultiSelectItem)

        While index <= endIndex
            Dim row As DataRow = rows(index)
            Dim item As New MultiSelectItem(row("PrdId"), row("PrdShortName"))
            list.Add(item)

            index += 1
        End While

        Return list
    End Function

    Protected Sub UpdateItems()
        Dim list As New List(Of String)
        Dim offset As Integer

        For Each item As ListItem In selectedProductListBox.Items
            Dim values As List(Of String) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(item.Value)

            list.Add(values(0))
        Next

        offset = list.Count

        For Each item As ListItem In availableProductListbox.Items
            If item.Selected Then
                list.Add(item.Value)
            End If
        Next

        Dim selectedItemsCsv As String = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetCsv(list)

        Dim packageProfile As EditPackageProfile = New EditPackageProfile(DisproIdControl, selectedItemsCsv, True, offset)

        Dim tables As DataTable() = { _
            packageProfile.DiscountProfilesProductNotLinkDataTable, _
            packageProfile.DiscountProfilesProductLinkDataTable, _
            packageProfile.ProductDependenciesTable}

        ProductsInfo = tables

        availableProductListbox.Items.Clear()
        InitProductDiscountProfile()

        InitMultiSelectScript()
    End Sub

    Protected Sub addtoButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addtoButton.Click
        UpdateItems()
    End Sub

    Protected Sub removefromButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removefromButton.Click
        Dim index As Integer = selectedProductListBox.Items.Count - 1

        While index >= 0
            Dim item As ListItem = selectedProductListBox.Items(index)
            If item.Selected Then
                selectedProductListBox.Items.RemoveAt(index)
            End If

            index -= 1
        End While
       

        UpdateItems()
    End Sub

    'Protected Sub addtoButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addtoButton.Click
    '    For Each item As ListItem In availableProductListbox.Items
    '        If item.Selected Then
    '            Dim itemcandidate As ListItem = selectedProductListBox.Items.FindByValue(item.Value)
    '            If (itemcandidate Is Nothing) Then
    '                selectedProductListBox.Items.Add(item)

    '            End If
    '        End If
    '    Next

    '    For Each item As ListItem In selectedProductListBox.Items
    '        Dim itemcandidate As ListItem = availableProductListbox.Items.FindByValue(item.Value)
    '        If (Not itemcandidate Is Nothing) Then
    '            availableProductListbox.Items.Remove(item)
    '        End If

    '    Next

    '    InitMultiSelectScript()
    'End Sub

    'Protected Sub removefromButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removefromButton.Click
    '    For Each item As ListItem In selectedProductListBox.Items
    '        If item.Selected Then
    '            Dim itemcandidate As ListItem = availableProductListbox.Items.FindByValue(item.Value)
    '            If (itemcandidate Is Nothing) Then
    '                availableProductListbox.Items.Add(item)
    '            End If
    '        End If
    '    Next

    '    For Each item As ListItem In availableProductListbox.Items
    '        Dim itemcandidate As ListItem = selectedProductListBox.Items.FindByValue(item.Value)
    '        If (Not itemcandidate Is Nothing) Then
    '            selectedProductListBox.Items.Remove(item)
    '        End If

    '    Next

    '    InitMultiSelectScript()
    'End Sub

    Protected Sub saveProductProfileButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveProductProfileButton.Click
        If Not CanMaintain Then Return
        Dim packageProfile As New EditPackageProfile()
        Dim result As Integer = 0
        Try
            Dim sb As New StringBuilder
            For Each item As ListItem In selectedProductListBox.Items
                Dim values As List(Of String) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(item.Value)

                Dim discountProductId As String = values(0)
                sb.AppendFormat("{0},", discountProductId)

                Dim dependencyProducts As String = Nothing

                If values.Count > 1 Then
                    dependencyProducts = values(1)
                End If

                ''rev:mia Oct. 29 2013 - remove the prdShortName from the list to avoid duplicates saving in the DiscountProfileProductDependencies 
                If (Not String.IsNullOrEmpty(dependencyProducts)) Then

                    Dim arDependencies() As String = dependencyProducts.Split(",")
                    Dim tempDependencyProduct As StringBuilder = New StringBuilder()
                    For Each itemDepdency As String In arDependencies
                        ''check the PrdId lenght, its should be equal to 36
                        If (Len(Trim(itemDepdency)) >= 36) Then
                            tempDependencyProduct.Append(itemDepdency & ",")
                        End If
                    Next
                    If (tempDependencyProduct.ToString().EndsWith(",") = True) Then
                        tempDependencyProduct = tempDependencyProduct.Remove(tempDependencyProduct.Length - 1, 1)
                    End If
                    dependencyProducts = tempDependencyProduct.ToString()
                    tempDependencyProduct = Nothing
                End If
                
                
                result = packageProfile.PackageInsertExcludeProductProfile(_DisproIdControl, discountProductId, productcheckboxexclude.Checked, CurrentPage.UserCode, dependencyProducts)

            Next
            Dim resultDeletion As Integer
            If (sb.ToString.EndsWith(",") = True) Then
                resultDeletion = packageProfile.PackageDeleteProductProfile(_DisproIdControl, sb.ToString.Substring(0, sb.ToString.LastIndexOf(",")))
            Else
                result = packageProfile.PackageDeleteProductProfile(_DisproIdControl, "")
            End If
        Catch ex As Exception
            result = -1
        End Try

        If (result > 0) Then
            MessageInfo = SuccessfulMessage
            Response.Redirect(RequeryURL & "&saved=ok&activeTab=1")
        End If
        If (result < 0) Then
            Me.CurrentPage.AddWarningMessage(FailedMessage)
        End If
    End Sub
End Class
