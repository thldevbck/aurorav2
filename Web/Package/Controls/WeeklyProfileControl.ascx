<%@ Control Language="VB" AutoEventWireup="false" CodeFile="WeeklyProfileControl.ascx.vb" Inherits="Package_WeeklyProfileControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<script type="text/javascript">
var weeklyProfileControlId = '<%= Me.ClientId %>';
var maxWeeklyProfileRows = <%= Me.MaxWeeklyProfileRows %>;

function weeklyProfile_modeChange (sender)
{
    var daysModeRadioButton = document.getElementById (weeklyProfileControlId + "_daysModeRadioButton");
    var mixedModeRadioButton = document.getElementById (weeklyProfileControlId + "_mixedModeRadioButton");
    
    for (var index = 0; index < maxWeeklyProfileRows; index++)
    {
        var fromTimeOfDayTimeControl = document.getElementById (weeklyProfileControlId + "_fromTimeOfDayTimeControl" + index + "_timeTextBox");
        var toTimeOfDayTimeControl = document.getElementById (weeklyProfileControlId + "_toTimeOfDayTimeControl" + index + "_timeTextBox");
        
        if (daysModeRadioButton.checked)
        {
            fromTimeOfDayTimeControl.style.display = 'none';
            toTimeOfDayTimeControl.style.display = 'none';
        }
        else
        {
            fromTimeOfDayTimeControl.style.display = '';
            toTimeOfDayTimeControl.style.display = '';
        }
    }
}

function weeklyProfile_rowChange (sender, index)
{
    var idHiddenField = document.getElementById (weeklyProfileControlId + "_idHiddenField" + index);
    var fromDayOfWeekDropDown = document.getElementById (weeklyProfileControlId + "_fromDayOfWeekDropDown" + index);
    var fromTimeOfDayTimeControl = document.getElementById (weeklyProfileControlId + "_fromTimeOfDayTimeControl" + index + "_timeTextBox");
    var toDayOfWeekDropDown = document.getElementById (weeklyProfileControlId + "_toDayOfWeekDropDown" + index);
    var toTimeOfDayTimeControl = document.getElementById (weeklyProfileControlId + "_toTimeOfDayTimeControl" + index + "_timeTextBox");
    
    if (((sender == fromDayOfWeekDropDown) && (fromDayOfWeekDropDown.value == '0'))
     || ((sender == toDayOfWeekDropDown) && (toDayOfWeekDropDown.value == '0')))
    {
        fromDayOfWeekDropDown.value = '0';
        fromTimeOfDayTimeControl.value = '0:00';
        toDayOfWeekDropDown.value = '0';
        toTimeOfDayTimeControl.value = '23:59';
    }
    else if ((sender == fromDayOfWeekDropDown) && (fromDayOfWeekDropDown.value != '0') && (toDayOfWeekDropDown.value == '0'))
        toDayOfWeekDropDown.value = fromDayOfWeekDropDown.value;
    else if ((sender == toDayOfWeekDropDown) && (toDayOfWeekDropDown.value != '0') && (fromDayOfWeekDropDown.value == '0'))
        fromDayOfWeekDropDown.value = toDayOfWeekDropDown.value;
}

addEvent (window, "load", weeklyProfile_modeChange);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(weeklyProfile_modeChange);

</script>

<table style="width:100%" cellpadding="2" cellspacing="0">
    <tr>
        <td style="width:150px">Mode:</td>
        <td>
            <asp:RadioButton ID="daysModeRadioButton" runat="server" Text="Days" GroupName="weeklyProfileMode" />
            &nbsp;&nbsp;&nbsp;
            <asp:RadioButton ID="mixedModeRadioButton" runat="server" Text="Mixed" GroupName="weeklyProfileMode" />
        </td>
    </tr>
</table>    

<style type="text/css">
#weeklyProfileTable tr
{
    height: 30px;
}
</style>

<table id="weeklyProfileTable" cellpadding="2" cellspacing="0" class="dataTable" style="width:100%">
    <tr>
        <th>Travel From</th>
        <th>Travel To</th>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:HiddenField ID="idHiddenField0" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown0" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl0" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown0" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl0" runat="server" />
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:HiddenField ID="idHiddenField1" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown1" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl1" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown1" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl1" runat="server" />
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:HiddenField ID="idHiddenField2" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown2" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl2" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown2" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl2" runat="server" />
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:HiddenField ID="idHiddenField3" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown3" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl3" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown3" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl3" runat="server" />
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:HiddenField ID="idHiddenField4" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown4" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl4" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown4" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl4" runat="server" />
        </td>
    </tr>
    <tr class="oddRow">
        <td>
            <asp:HiddenField ID="idHiddenField5" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown5" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl5" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown5" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl5" runat="server" />
        </td>
    </tr>
    <tr class="evenRow">
        <td>
            <asp:HiddenField ID="idHiddenField6" runat="server" />
            <asp:DropDownList id="fromDayOfWeekDropDown6" runat="server" />
            <uc1:TimeControl id="fromTimeOfDayTimeControl6" runat="server" />
        </td>
        <td>
            <asp:DropDownList id="toDayOfWeekDropDown6" runat="server"  />
            <uc1:TimeControl id="toTimeOfDayTimeControl6" runat="server" />
        </td>
    </tr>
</table>

<table style="width:100%" cellpadding="2" cellspacing="0">
    <tr>
        <td align="right" colspan="2">
            <asp:Button ID="updateButton" runat="server" Text="Save Profile" CssClass="Button_Standard Button_Save" Visible="False" />
        </td>
    </tr>
</table>    


