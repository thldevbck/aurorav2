﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DiscountProfilesControl.ascx.vb" Inherits="Package_Controls_DiscountProfilesControl" %>

<%@ Import Namespace="Aurora.Package.Data" %>
<%@ Import Namespace="Aurora.Common" %>

<asp:PlaceHolder ID="plhStyle" runat="server">

<style type="text/css">
    table#DiscountSearchTable
    {
        width:100%;
        border-collapse: collapse
    }
    
    table#DiscountSearchTable td
    {
        padding: 5px 2px;
        border-bottom: 1px dotted #000;
    }
    
    table#DiscountSearchTable tr.activeDiscountProfile
    {
        background-color:<%=mActiveColor%>;
    }
    
    table#DiscountSearchTable tr.inactiveDiscountProfile
    {
        background-color: <%=mInactiveColor%>;
    }
</style>

</asp:PlaceHolder>

<asp:Repeater ID="rptDiscounts" EnableViewState="false" runat="server">
    <HeaderTemplate>
        <table id="DiscountSearchTable">
        <thead>
            <tr>
                <th>Discount Profile</th><th>Code</th><th>Type</th><th>Booked From</th><th>Booked To</th><th>Travel From</th><th>Travel To</th><th>Status</th>
            </tr>
        </thead>
            <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="<%# IIFX(DirectCast(Container.DataItem, DiscountProfile).IsActive, "activeDiscountProfile", "inactiveDiscountProfile") %>">
            <td>
                <a href="EditPackageDiscount.aspx?DisProID=<%#DirectCast(Container.DataItem, DiscountProfile).DiscountProfileId.ToString() %>">
                    <%# DirectCast(Container.DataItem, DiscountProfile).Text%>
                </a>
            </td>
            <td><%# DirectCast(Container.DataItem, DiscountProfile).Code%></td>
            <td><%# DirectCast(Container.DataItem, DiscountProfile).CodeType%></td>
            <td><%# DirectCast(Container.DataItem, DiscountProfile).BookedFrom.ToShortDateString()%></td>
            <td><%# DirectCast(Container.DataItem, DiscountProfile).BookedTo.ToShortDateString()%></td>
            <td><%# DirectCast(Container.DataItem, DiscountProfile).TravelFrom.ToShortDateString()%></td>
            <td><%# DirectCast(Container.DataItem, DiscountProfile).TravelTo.ToShortDateString()%></td>
            <td><%# IIFX(DirectCast(Container.DataItem, DiscountProfile).IsActive, "Active", "Inactive")%></td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
            </tbody>
        </table>
    </FooterTemplate>
</asp:Repeater>
