' Change Log!
'
' 3.6.9     -   Shoel - Added this log 
' 3.6.9     -   Shoel - Get/Save rewritten + Integrity Increment added + UI modded 
' 9.6.9     -   Shoel - A valid + Current (w.r.t. Web enabled date) T&C Url is needed to proceed, Enhanced the Web Enabled Date validation :)
' 29.6.9    -   Shoel - Fixes for SQUISH #732, #736
' 21.7.9    -   Shoel - Fix for the refresh problems with child packages (inclusive packages linked to current package)
'                       Also a blank row appears by default when no T&C url exists
' 23.7.9    -   Shoel - The whole page logic has been rewritten! Useless and weird code has been eliminated!
' 24.7.9    -   Shoel - The T&C validation logic has been fixed! And Fixed again! And again :'( And Again...
' 27.7.9    -   Shoel - To date now has 23:59:59 added + weird problems with "Remove" fixed :)
' 28.7.9    -   Shoel - Problem with Remove button hide logic fixed - incorrect object was being checked
'                       Problem wherein obj refs popped up when the parent package list was empty is now fixed!
' March 29 2011 -rev:mia - changes on PackageSetup_GetB2CInformations Terms and Condition section to include all terms and conditions that 
'                          is equal or greater than todays but has webenabled date empty
''March 31 2011 -rev:mia March 31 2011 allow this message 
Imports System.Data
Imports System.Xml
Imports System.IO

Partial Class Package_Controls_PackageSetupControl
    Inherits AuroraUserControl

#Region "Private Property"

    Private ReadOnly Property PgkINFOurl() As String
        Get
            Return Left(InformationLinkTextBox.Text, 256)
        End Get
    End Property

    Private ReadOnly Property PkgParentID() As String
        Get
            If cmbParentPackageList.Items.Count = 0 Then
                ' no items in the list!
                Return ""
            Else
                Return IIf(cmbParentPackageList.SelectedItem.Text = "(Select)", "", cmbParentPackageList.SelectedValue)
            End If
        End Get
    End Property

    Private Property PkgSetupId() As String
        Get
            Return ViewState("PkgSetupId")
        End Get
        Set(ByVal value As String)
            ViewState("PkgSetupId") = value
        End Set
    End Property


    Property VehicleClassList() As SortedList
        Get
            If ViewState("PSC_VehicleClassList") Is Nothing Then
                Return New SortedList
            Else
                Return CType(ViewState("PSC_VehicleClassList"), SortedList)
            End If
        End Get
        Set(ByVal value As SortedList)
            ViewState("PSC_VehicleClassList") = value
        End Set
    End Property

    Property OriginalTandCXML() As XmlDocument
        Get
            Dim xmlReturn As XmlDocument = New XmlDocument
            If ViewState("PSC_OriginalTandCXML") IsNot Nothing OrElse CStr(ViewState("PSC_OriginalTandCXML")) = "" Then
                xmlReturn.LoadXml(CStr(ViewState("PSC_OriginalTandCXML")))
            End If
            Return xmlReturn
        End Get
        Set(ByVal value As XmlDocument)
            ViewState("PSC_OriginalTandCXML") = value.OuterXml
        End Set
    End Property

    Property HasChildPackages() As Boolean
        Get
            Return CBool(ViewState("PSC_HasChildPackages"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("PSC_HasChildPackages") = value
        End Set
    End Property

    Property CurrentParentPackageId() As String
        Get
            Return CStr(ViewState("PSC_CurrentParentPackageId"))
        End Get
        Set(ByVal value As String)
            ViewState("PSC_CurrentParentPackageId") = value
        End Set
    End Property

    Property ChildPackageList() As SortedList
        Get
            If ViewState("PSC_ChildPackageList") Is Nothing Then
                Return New SortedList
            Else
                Return CType(ViewState("PSC_ChildPackageList"), SortedList)
            End If
        End Get
        Set(ByVal value As SortedList)
            ViewState("PSC_ChildPackageList") = value
        End Set
    End Property

    Property TandCContainsValidCurrentURL() As Boolean
        Get
            If ViewState("PSC_TandCContainsValidCurrentURL") Is Nothing Then
                Return False
            Else
                Return CBool(ViewState("PSC_TandCContainsValidCurrentURL"))
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("PSC_TandCContainsValidCurrentURL") = value
        End Set
    End Property

    Property PackageBookedFromDate() As DateTime
        Get
            If ViewState("PSC_PkgBooFromDt") Is Nothing Then
                Return New Date
            Else
                Return CDate(ViewState("PSC_PkgBooFromDt"))
            End If
        End Get
        Set(ByVal value As DateTime)
            ViewState("PSC_PkgBooFromDt") = value
        End Set
    End Property

    Property PackageBookedToDate() As DateTime
        Get
            If ViewState("PSC_PkgBooToDt") Is Nothing Then
                Return New Date
            Else
                Return CDate(ViewState("PSC_PkgBooToDt"))
            End If
        End Get
        Set(ByVal value As DateTime)
            ViewState("PSC_PkgBooToDt") = value
        End Set
    End Property

    Property PkgCtyCode() As String
        Get
            Return CStr(ViewState("PSC_PkgCtyCode"))
        End Get
        Set(ByVal value As String)
            ViewState("PSC_PkgCtyCode") = value
        End Set
    End Property

#End Region

#Region "Public Property"

    Public Property PkgId() As String
        Get
            Return ViewState("PkgId")
        End Get
        Set(ByVal value As String)
            ViewState("PkgId") = value
        End Set
    End Property

    Public Property DomesticCountriesList() As XmlDocument
        Get
            Dim xmlDomesticCountriesList As New XmlDocument

            If ViewState("PSC_DomesticCountriesList") Is Nothing OrElse CStr(ViewState("PSC_DomesticCountriesList")) = "" Then
                Return xmlDomesticCountriesList
            Else
                xmlDomesticCountriesList.LoadXml(CStr(ViewState("PSC_DomesticCountriesList")))
                Return xmlDomesticCountriesList
            End If
        End Get
        Set(ByVal value As XmlDocument)
            ViewState("PSC_DomesticCountriesList") = value.OuterXml
        End Set
    End Property

#End Region

#Region "Grid Events"

    Protected Sub gwTermsAndConditions_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cmbVehicleClass As DropDownList = CType(e.Row.FindControl("cmbVehicleClass"), DropDownList)
            With cmbVehicleClass
                .DataSource = VehicleClassList
                .DataValueField = "Key"
                .DataTextField = "Value"
                .DataBind()
                .SelectedValue = CType(e.Row.FindControl("hdnVehicleClass"), HiddenField).Value
            End With
        End If
    End Sub

#End Region

#Region "Dropdown Event"

    Sub cmbCountriesList_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        UpdateDomesticCountriesList(Nothing, cmbCountriesList.SelectedItem.Value & "#" & cmbCountriesList.SelectedItem.Text, False, False)
        CreateHyperLinksForChildPackages()
    End Sub

#End Region


#Region "Buttons Event"
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Not AreFieldsValid() Then Exit Sub
        SaveB2CPackageInformation(PkgId, dtcWebEnabledFrom.Date, dtcWebEnabledTo.Date.AddHours(23).AddMinutes(59).AddSeconds(59), _
                                  PgkINFOurl, PkgParentID, MyBase.CurrentPage.UserCode, _
                                  MyBase.CurrentPage.UserCode)

    End Sub

    Protected Sub btnReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReset.Click
        InitialisePage()
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click

        Dim sbCountriesSelectedForDeletion As New StringBuilder
        sbCountriesSelectedForDeletion.Append("Do you want to delete the country of residence mappings for ")

        For Each oItem As ListItem In cblDomesticCountriesList.Items
            If oItem.Selected Then
                sbCountriesSelectedForDeletion.Append("<BR/><b>" & oItem.Text & "</b>")
            End If
        Next

        ConfirmationDelete.Title = "Delete country of residence mapping(s)"
        ConfirmationDelete.Text = sbCountriesSelectedForDeletion.ToString()
        ConfirmationDelete.Show()

    End Sub

    Protected Sub btnAddNewTandCRow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewTandCRow.Click

        Dim dtTandC As DataTable = New DataTable
        dtTandC.Columns.Add("PtcId")
        dtTandC.Columns.Add("PtcFromDate")
        dtTandC.Columns.Add("PtcToDate")
        dtTandC.Columns.Add("PtcURL")
        dtTandC.Columns.Add("PtcClaId")

        For Each oRow As GridViewRow In gwTermsAndConditions.Rows
            If oRow.RowType = DataControlRowType.DataRow Then
                Dim drTandC As DataRow = dtTandC.NewRow
                drTandC("PtcId") = CType(oRow.FindControl("hdnPtcId"), HiddenField).Value
                drTandC("PtcFromDate") = CType(oRow.FindControl("dtcFrom"), UserControls_DateControl).Date
                drTandC("PtcToDate") = CType(oRow.FindControl("dtcTo"), UserControls_DateControl).Date
                drTandC("PtcURL") = CType(oRow.FindControl("txtTCURL"), TextBox).Text
                drTandC("PtcClaId") = CType(oRow.FindControl("cmbVehicleClass"), DropDownList).SelectedValue
                dtTandC.Rows.Add(drTandC)
            End If
        Next

        Dim drTandC_New As DataRow = dtTandC.NewRow
        drTandC_New("PtcFromDate") = DateTime.MinValue
        drTandC_New("PtcToDate") = DateTime.MinValue

        dtTandC.Rows.Add(drTandC_New)

        gwTermsAndConditions.DataSource = dtTandC
        gwTermsAndConditions.DataBind()

        CreateHyperLinksForChildPackages()

    End Sub

    Protected Sub btnDeleteTandCRow_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sbDeleteXML As StringBuilder = New StringBuilder
        Dim bIsThereSomethingToDelete As Boolean = False
        sbDeleteXML.Append("<TandCs>")

        For Each oRow As GridViewRow In gwTermsAndConditions.Rows
            If oRow.RowType = DataControlRowType.DataRow Then

                Dim hdnIdField As HiddenField = CType(oRow.FindControl("hdnPtcId"), HiddenField)

                If CType(oRow.FindControl("chkSelectRow"), CheckBox).Checked _
                AndAlso Not hdnIdField.Value.Trim().Equals(String.Empty) Then
                    ' this row was marked for deletion
                    sbDeleteXML.Append("<TandC PtcId=""" & hdnIdField.Value.Trim() & """ />")
                    bIsThereSomethingToDelete = True
                End If
            End If
        Next

        sbDeleteXML.Append("</TandCs>")
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                If bIsThereSomethingToDelete Then
                    Dim sRetMsg As String
                    sRetMsg = Aurora.Package.Data.DeletePackageTermsAndConditions(sbDeleteXML.ToString())

                    If sRetMsg.ToUpper().Contains("GEN047") Then

                        ' deleted successfully, so reload 
                        GetB2CPackageInformations(PkgId)
                        ' set success message
                        CurrentPage.SetInformationMessage(sRetMsg)

                        ' check if the T & C's have gone invalid!
                        ValidateTermsAndConditions()

                    Else
                        CurrentPage.SetErrorMessage(sRetMsg)
                    End If
                End If
                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
            End Try
        End Using

        CreateHyperLinksForChildPackages()

    End Sub

#End Region

#Region "Private Procedure"

    Sub SaveB2CPackageInformation(ByVal PkgId As String, _
                              ByVal PkgWebEnabledFromDate As Date, _
                              ByVal PkgWebEnabledToDate As Date, _
                              ByVal PgkINFOurl As String, _
                              ByVal PkgParentID As String, _
                              ByVal AddUsrId As String, _
                              ByVal ModUsrId As String)

        Dim retvalue As String = ""

        Using otransaction As New Aurora.Common.Data.DatabaseTransaction
            Try


                ' Save the first 2,3 fields
                retvalue = Aurora.Package.Data.SaveB2CPackageInformation(PkgId, _
                                                                         PkgWebEnabledFromDate, _
                                                                         PkgWebEnabledToDate, _
                                                                         PgkINFOurl, _
                                                                         PkgParentID, _
                                                                         ModUsrId)
                If retvalue.Contains("ERROR") Then Throw New Exception(retvalue)


                ' Save the wonderful T&C Table :)
                SaveTermsAndConditionsData()

                ' Save the country mapping data
                For Each oListItem As ListItem In cblDomesticCountriesList.Items
                    If oListItem.Value.Trim() = "" Then
                        ' New Item!!!
                        retvalue = Aurora.Package.Data.InsertDomesticCountries(oListItem.Value, PkgId, oListItem.Text.Split("-"c)(0).Trim(), AddUsrId)
                        If retvalue.ToUpper().Contains("ERROR") Then Throw New Exception(retvalue)
                    End If
                Next

                ' If everything went well, commit the Transaction
                otransaction.CommitTransaction()

                ' reload
                GetB2CPackageInformations(PkgId)
                CreateHyperLinksForChildPackages()

                ' show success message
                MyBase.CurrentPage.SetInformationMessage("Web package tab updated sucessfully")

            Catch ex As Exception
                otransaction.RollbackTransaction()
                MyBase.CurrentPage.SetErrorMessage(ex.Message)
            End Try

        End Using

    End Sub

    Sub GetB2CPackageInformations(ByVal PkgId As String)

        Dim result As System.Data.DataSet
        Try
            result = Aurora.Package.Data.GetB2CInformations(PkgId)

            If result.Tables.Count > 0 Then
                Try

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgWebEnabledFromDate").GetType().Equals(GetType(DBNull)) Then
                        dtcWebEnabledFrom.Date = CDate(DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgWebEnabledFromDate"))
                    End If

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgWebEnabledToDate").GetType().Equals(GetType(DBNull)) Then
                        dtcWebEnabledTo.Date = CDate(DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgWebEnabledToDate"))
                    End If

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgBookedFromDate").GetType().Equals(GetType(DBNull)) Then
                        PackageBookedFromDate = CDate(DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgBookedFromDate"))
                    End If

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgBookedToDate").GetType().Equals(GetType(DBNull)) Then
                        PackageBookedToDate = CDate(DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgBookedToDate"))
                    End If

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgCtyCode").GetType().Equals(GetType(DBNull)) Then
                        PkgCtyCode = CStr(DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgCtyCode"))
                    End If

                    If dtcWebEnabledFrom.Text.Trim() = "" Then
                        ' add an auto postback event to this
                        dtcWebEnabledFrom.AutoPostBack = True
                    Else
                        dtcWebEnabledFrom.AutoPostBack = False
                    End If

                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PgkINFOurl").GetType().Equals(GetType(DBNull)) Then
                        InformationLinkTextBox.Text = DataBinder.Eval(result, "Tables(0).DefaultView(0).PgkINFOurl")
                    End If

                    If result.Tables(4) IsNot Nothing AndAlso result.Tables(4).Rows.Count > 0 Then
                        ' There is some child info!
                        tblParentPackage.Style("display") = "none"
                        tblChildPackages.Style("display") = "block"
                        HasChildPackages = True

                        ' Add Child Package Links!

                        Dim slChildPackageList As SortedList = ChildPackageList
                        For Each rwChildPackage As DataRow In result.Tables(4).Rows
                            slChildPackageList.Add(rwChildPackage("PkgCode"), "../Package.aspx?pkgId=" & rwChildPackage("PkgId") & "&BackUrl=" & "Package.aspx?pkgId=" & PkgId)
                        Next
                        ChildPackageList = slChildPackageList
                        CreateHyperLinksForChildPackages()
                        ' Add Child Package Links!
                    Else
                        ' This is a parent package
                        tblParentPackage.Style("display") = "block"
                        tblChildPackages.Style("display") = "none"
                        HasChildPackages = False
                    End If

                Catch ex As Exception
                End Try

                ' domestic Country info
                If result.Tables(1).Rows.Count > 0 Then
                    UpdateDomesticCountriesList(result.Tables(1), Nothing, False, True)
                End If

                ' Vehicle Class Data
                If result.Tables(2).Rows.Count > 0 Then
                    Dim htVehicleClass As SortedList = New SortedList
                    For Each rwVehicleClass As DataRow In result.Tables(2).Rows
                        htVehicleClass.Add(rwVehicleClass("ClaID"), rwVehicleClass("Code"))
                    Next
                    VehicleClassList = htVehicleClass
                End If

                ' T & C URL Data
                If result.Tables(3).Rows.Count > 0 Then
                    gwTermsAndConditions.DataSource = result.Tables(3)
                    gwTermsAndConditions.DataBind()
                Else
                    gwTermsAndConditions.DataSource = New DataTable
                    gwTermsAndConditions.DataBind()
                    ' add an empty row
                    btnAddNewTandCRow_Click(Nothing, Nothing)
                End If

                ' Parent Package List data
                If result.Tables(5) IsNot Nothing AndAlso result.Tables(5).Rows.Count > 0 Then
                    InitialiseParentPackageList(result.Tables(5))
                End If

                ' Country Data
                If result.Tables(6) IsNot Nothing AndAlso result.Tables(6).Rows.Count > 0 Then
                    InitCountriesList(result.Tables(6))
                End If

                Try
                    If Not DataBinder.Eval(result, "Tables(0).DefaultView(0).PkgParentID").GetType().Equals(GetType(DBNull)) Then
                        cmbParentPackageList.SelectedValue = result.Tables(0).Rows(0)("PkgParentID")
                    End If
                Catch
                End Try

                ' Save a copy of T & C grid xml, for later comparison to determine if anything has changed
                OriginalTandCXML = GetTermsAndConditionsXMLFromGrid()

            End If

        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("Cannot load B2C Package Informations")
        End Try
    End Sub

    Sub InitialiseParentPackageList(ByVal ParentPackageListData As DataTable)

        cmbParentPackageList.Items.Clear()
        cmbParentPackageList.Items.Add("(Select)")

        Dim colIDColumn(1) As Data.DataColumn
        ''make a primary key
        colIDColumn(0) = ParentPackageListData.Columns("PkgId")
        ParentPackageListData.PrimaryKey = colIDColumn

        Dim listitem As ListItem

        For Each ParentIDtableRow As System.Data.DataRow In ParentPackageListData.Rows
            listitem = New ListItem

            listitem.Text = ProperCasing(ParentIDtableRow.Item("FullPackage").ToString, True).PadLeft(20)
            listitem.Value = ParentIDtableRow.Item("PkgId")

            If ParentIDtableRow.Item("PkgIsActive") = "Active" Then
                listitem.Attributes.Add("style", "background-color:" & System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ActiveColor))
            End If

            If ParentIDtableRow.Item("PkgIsActive") = "Inactive" Then
                listitem.Attributes.Add("style", "background-color:" & System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor))
            End If

            If ParentIDtableRow.Item("PkgIsActive") = "Pending" Then
                listitem.Attributes.Add("style", "background-color:" & System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.FutureColor))
            End If
            cmbParentPackageList.Items.Add(listitem)
        Next

    End Sub

    Sub CreateHyperLinksForChildPackages()
        pnlLinkedChildPackages.Controls.Clear()
        For Each sChildPackageCode As String In ChildPackageList.Keys
            Dim lnkChildPackage As HyperLink = New HyperLink
            lnkChildPackage.Text = sChildPackageCode
            lnkChildPackage.NavigateUrl = ChildPackageList(sChildPackageCode)
            pnlLinkedChildPackages.Controls.Add(lnkChildPackage)
            pnlLinkedChildPackages.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
        Next
    End Sub

#Region "T&C Saving"
    Sub SaveTermsAndConditionsData()
        ' Hit the DB !
        Dim sRetMsg As String
        Dim xmlTandCToSave As XmlDocument = GetTermsAndConditionsChanges()

        If xmlTandCToSave.DocumentElement.ChildNodes.Count > 0 Then
            sRetMsg = Aurora.Package.Data.UpdatePackageTermsAndConditions(xmlTandCToSave.OuterXml, PkgId, CurrentPage.UserId)
            If sRetMsg.ToUpper().IndexOf("SUCCESS") < 0 Then
                Throw New Exception(sRetMsg)
            End If
        End If

    End Sub

    Function GetTermsAndConditionsChanges() As XmlDocument
        Dim xmlLatestDataFromGrid As XmlDocument
        xmlLatestDataFromGrid = GetTermsAndConditionsXMLFromGrid()

        ' something has changed
        For i As Integer = xmlLatestDataFromGrid.DocumentElement.ChildNodes.Count - 1 To 0 Step -1
            For Each oNode As XmlNode In OriginalTandCXML.DocumentElement.ChildNodes
                If oNode.OuterXml = xmlLatestDataFromGrid.DocumentElement.ChildNodes(i).OuterXml Then
                    ' match found ! remove it from xml to save !
                    xmlLatestDataFromGrid.DocumentElement.RemoveChild(xmlLatestDataFromGrid.DocumentElement.ChildNodes(i))
                    Exit For
                End If
            Next
        Next

        Return xmlLatestDataFromGrid

    End Function

    Function GetTermsAndConditionsXMLFromGrid() As XmlDocument
        Dim sbXMLToSave As StringBuilder = New StringBuilder

        For Each oRow As GridViewRow In gwTermsAndConditions.Rows
            If oRow.RowType = DataControlRowType.DataRow Then
                If CType(oRow.FindControl("dtcFrom"), UserControls_DateControl).Date <> DateTime.MinValue _
                   AndAlso _
                   CType(oRow.FindControl("dtcTo"), UserControls_DateControl).Date <> DateTime.MinValue _
                   AndAlso _
                   CType(oRow.FindControl("cmbVehicleClass"), DropDownList).SelectedValue <> "" _
                   AndAlso _
                   CType(oRow.FindControl("txtTCURL"), TextBox).Text.Trim() <> "" _
                Then

                    sbXMLToSave.Append("<TandC ")
                    If Not CType(oRow.FindControl("hdnPtcId"), HiddenField).Value.Trim().Equals(String.Empty) Then
                        ' old record, Append PtcId attribute!
                        sbXMLToSave.Append("PtcId=""" & CType(oRow.FindControl("hdnPtcId"), HiddenField).Value.Trim() & """")
                    End If

                    sbXMLToSave.Append(" PtcFromDate=""" & Aurora.Common.Utility.DateDBToString(CType(oRow.FindControl("dtcFrom"), UserControls_DateControl).Date) & _
                                       """ PtcToDate=""" & Aurora.Common.Utility.DateDBToString(CType(oRow.FindControl("dtcTo"), UserControls_DateControl).Date) & _
                                       """ PtcClaId=""" & CType(oRow.FindControl("cmbVehicleClass"), DropDownList).SelectedValue & _
                                       """ PtcURL=""" & XmlEncode(CType(oRow.FindControl("txtTCURL"), TextBox).Text.Trim()) & """ />")
                End If

            End If
        Next

        If Not sbXMLToSave.ToString().Trim().Equals(String.Empty) Then
            ' something to be saved!
            sbXMLToSave.Insert(0, "<TandCs>")
            sbXMLToSave.Append("</TandCs>")
        End If

        If sbXMLToSave.ToString().Trim().Equals(String.Empty) Then
            sbXMLToSave.Append("<TandCs/>")
        End If

        Dim xmlReturn As XmlDocument = New XmlDocument
        xmlReturn.LoadXml(sbXMLToSave.ToString())

        Return xmlReturn

    End Function
#End Region

#End Region

#Region "Private Function"

    Function AreFieldsValid() As Boolean

        'If String.IsNullOrEmpty(dtcWebEnabledFrom.Text) OrElse (Not dtcWebEnabledFrom.IsValid) Then
        '    MyBase.CurrentPage.SetWarningShortMessage("Web enabled from date is mandatory")
        '    Return False
        'End If

        'If String.IsNullOrEmpty(dtcWebEnabledTo.Text) OrElse (Not dtcWebEnabledTo.IsValid) Then
        '    MyBase.CurrentPage.SetWarningShortMessage("Web enabled to date is mandatory")
        '    Return False
        'End If

        ' Shoel : 21.5.9 : Sticking in the stuff to validate the T&C
        If Not ValidateTermsAndConditions() Then
            Return False
        End If

        Return True

    End Function

    Function ProperCasing(ByVal FullName As String, Optional ByVal skipFirstWord As Boolean = False) As String

        If String.IsNullOrEmpty(FullName) Then Return ""
        Dim strFullPackage() As String = FullName.ToLower.Split(" ")
        Dim tempStrFullPackage As String = ""
        Dim i As Integer = 1

        For Each tempStrPackage As String In strFullPackage
            If Not String.IsNullOrEmpty(tempStrPackage) Then
                If skipFirstWord = True AndAlso i = 1 Then
                    tempStrPackage = tempStrPackage.ToUpper.PadRight(25, " ")
                End If
                Dim firstLetter As String = tempStrPackage.Substring(0, 1).ToUpper
                tempStrPackage = tempStrPackage.Remove(0, 1)
                tempStrPackage = firstLetter & tempStrPackage & " "
                tempStrFullPackage = tempStrFullPackage & tempStrPackage
                i = i + 1
            End If

        Next
        Return tempStrFullPackage
    End Function

    Function ValidateTermsAndConditions() As Boolean

        ' Declare and Init table that holds temp data
        Dim dtTandC As DataTable = New DataTable("TermsAndConditions")
        dtTandC.Columns.Add("PtcFromDate")
        dtTandC.Columns.Add("PtcToDate")
        dtTandC.Columns.Add("PtcClaId")

        ' Shoel : 9.6.9 : A valid + Current (w.r.t. Web enabled date) T&C Url is needed to proceed
        If gwTermsAndConditions.Rows.Count = 0 Then
            CurrentPage.SetWarningShortMessage("Please enter Terms and Conditions for this package")
            Return False
        End If

        TandCContainsValidCurrentURL = False

        For i As Int16 = 0 To gwTermsAndConditions.Rows.Count - 1
            With gwTermsAndConditions.Rows(i)
                If .RowType = DataControlRowType.DataRow Then
                    Dim datFrom, datTo As Date
                    Dim sClaId, sURL, sPtcId As String

                    ' Read the row contents
                    sPtcId = CType(.FindControl("hdnPtcId"), HiddenField).Value.Trim()
                    datFrom = CType(.FindControl("dtcFrom"), UserControls_DateControl).Date
                    datTo = CType(.FindControl("dtcTo"), UserControls_DateControl).Date
                    sClaId = CType(.FindControl("cmbVehicleClass"), DropDownList).SelectedValue
                    sURL = CType(.FindControl("txtTCURL"), TextBox).Text.Trim()

                    If sPtcId <> "" OrElse datFrom <> DateTime.MinValue OrElse datTo <> DateTime.MinValue OrElse sClaId <> "" OrElse sURL <> "" Then
                        ' Something has been entered in this row


                        ' drop down check

                        If sClaId.Equals(String.Empty) Then
                            CurrentPage.SetWarningShortMessage("Please select a Vehicle Class for Terms and Conditions row " & (i + 1).ToString())
                            Return False
                        End If

                        ' Simple Date Check

                        If datFrom.Equals(DateTime.MinValue) OrElse datTo.Equals(DateTime.MinValue) OrElse datTo < datFrom Then
                            CurrentPage.SetWarningShortMessage("Invalid date range entered for Terms and Conditions row " & (i + 1).ToString())
                            Return False
                        End If

                        ' Simple URL check

                        If Trim(sURL).Equals(String.Empty) Then
                            ' No url!
                            CurrentPage.SetWarningShortMessage("No URL entered for Terms and Conditions row " & (i + 1).ToString())
                            Return False
                            ''Else
                            ''    If Not IsValidURL(sURL) Then
                            ''        CurrentPage.SetWarningShortMessage("Invalid URL entered for Terms and Conditions row " & (i + 1).ToString())
                            ''        Return False
                            ''    End If
                        End If

                        ' Hellishly Complicated Checks follow :(

                        If dtTandC.Rows.Count > 0 Then
                            ' This isnt the first row
                            For Each rwTableRow As DataRow In dtTandC.Rows
                                Dim previous_datFrom, previous_datTo As Date
                                Dim previous_sClaId As String

                                previous_datFrom = rwTableRow("PtcFromDate")
                                previous_datTo = rwTableRow("PtcToDate")
                                previous_sClaId = rwTableRow("PtcClaId")

                                If datFrom >= previous_datFrom AndAlso datFrom <= previous_datTo AndAlso sClaId = previous_sClaId Then
                                    ' From date overlaps another range!
                                    CurrentPage.SetWarningShortMessage("Date range entered for Terms and Conditions row " & (i + 1).ToString() & " overlaps a previous date range")
                                    Return False
                                End If

                                If datTo >= previous_datFrom AndAlso datTo <= previous_datTo AndAlso sClaId = previous_sClaId Then
                                    ' From date overlaps another range!
                                    CurrentPage.SetWarningShortMessage("Date range entered for Terms and Conditions row " & (i + 1).ToString() & " overlaps a previous date range")
                                    Return False
                                End If

                            Next
                        End If

                        Dim drNewRow As DataRow = dtTandC.NewRow
                        drNewRow("PtcFromDate") = datFrom
                        drNewRow("PtcToDate") = datTo
                        drNewRow("PtcClaId") = sClaId

                        dtTandC.Rows.Add(drNewRow)

                    End If
                End If
            End With
        Next

        ' sort the datatable by class and from date
        Dim dvValidationView As New DataView
        dvValidationView.Table = dtTandC

        ' first class check 
        For Each sVehicleClassId As String In VehicleClassList.Keys
            dvValidationView.RowFilter = "PtcClaId='" & sVehicleClassId & "'"
            dvValidationView.Sort = "PtcFromDate"
            Dim dtTemp As New DataTable
            dtTemp = dvValidationView.ToTable()

            Dim datRollingFromDate, datRollingToDate, datMinFromDate, datMaxToDate As Date

            If dtTemp.Rows.Count > 0 Then

                For i As Int16 = 0 To dtTemp.Rows.Count - 1

                    If datMinFromDate = Date.MinValue Then
                        ' the very first from date is stored here!
                        datMinFromDate = dtTemp.Rows(i)("PtcFromDate")
                    End If

                    datRollingToDate = dtTemp.Rows(i)("PtcToDate")

                    If i = dtTemp.Rows.Count - 1 Then
                        ' the very last to date is stored here!
                        datMaxToDate = dtTemp.Rows(i)("PtcToDate")
                    Else
                        ' this row is NOT the last row!
                        datRollingFromDate = dtTemp.Rows(i + 1)("PtcFromDate")
                        If datRollingToDate.AddDays(1) <> datRollingFromDate Then
                            ' Error there is a gap!
                            CurrentPage.SetWarningShortMessage("There are some travel dates for which no Terms and Conditions links are applicable")
                            Return True ''rev:mia March 31 2011 allow this message 
                        End If
                    End If
                Next

                'If datMinFromDate <> Date.MinValue AndAlso _
                '   datMinFromDate <> Date.MinValue AndAlso _
                '   ( _
                '    datMinFromDate > dtcWebEnabledFrom.Date OrElse _
                '    datMaxToDate < dtcWebEnabledTo.Date _
                '   ) _
                'Then
                '    CurrentPage.SetWarningShortMessage("There are some travel dates for which no Terms and Conditions links are applicable")
                '    Return True
                'Else
                '    TandCContainsValidCurrentURL = True
                'End If
            End If
        Next

        'If Not TandCContainsValidCurrentURL Then
        '    CurrentPage.SetWarningShortMessage("There are some travel dates for which no Terms and Conditions links are applicable")
        '    Return True
        'End If
        'Return TandCContainsValidCurrentURL
        Return True
        ' Shoel : 9.6.9 : A valid + Current (w.r.t. Web enabled date) T&C Url is needed to proceed

    End Function

    Function IsValidURL(ByVal InputString As String) As Boolean
        Dim sPattern As String
        sPattern = "^(https?://)" & _
                   "?(([0-9a-z_!~*'().&=+$%-]+: )?[0-9a-z_!~*'().&=+$%-]+@)?" & _
                   "(([0-9]{1,3}\.){3}[0-9]{1,3}" & _
                   "|" & _
                   "([0-9a-z_!~*'()-]+\.)*" & _
                   "([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\." & _
                   "[a-z]{2,6})" & _
                   "(:[0-9]{1,4})?" & _
                   "((/?)|" & _
                   "(/[0-9a-z_!~*'().;?:@&=+$,%#-]+)+/?)$"
        Dim rgxRegex As Regex = New Regex(sPattern)
        Return rgxRegex.IsMatch(InputString)
    End Function

    Public Function XmlEncode(ByVal strText As String) As String
        Dim aryChars As Integer() = {38, 60, 62, 34, 61, 39}
        Dim i As Integer
        For i = 0 To UBound(aryChars)
            strText = Replace(strText, Chr(aryChars(i)), "&#" & aryChars(i) & ";")
        Next
        XmlEncode = strText
    End Function

#End Region

#Region "private Subs"

    Sub InitialisePage()
        GetB2CPackageInformations(PkgId)
        cmbParentPackageList.AutoPostBack = False
    End Sub

    Sub UpdateDomesticCountriesList(ByVal DomesticCountriesTable As DataTable, ByVal NewlyAddedCountry As String, ByVal CalledPostDelete As Boolean, ByVal FlushContents As Boolean)
        Dim sbCountriesList As New StringBuilder
        Dim xmlSelectedCountries As New XmlDocument

        If FlushContents Then DomesticCountriesList = New XmlDocument

        sbCountriesList.Append("<Root>")

        If DomesticCountriesTable IsNot Nothing AndAlso DomesticCountriesTable.Rows.Count > 0 Then

            For Each rowCountry As DataRow In DomesticCountriesTable.Rows
                sbCountriesList.Append("<Country>")
                sbCountriesList.Append("    <PkgSetupId>")
                sbCountriesList.Append(Trim(rowCountry("PkgSetupId")))
                sbCountriesList.Append("    </PkgSetupId>")
                sbCountriesList.Append("    <CtyCode>")
                sbCountriesList.Append(Trim(rowCountry("CtyCode")))
                sbCountriesList.Append("    </CtyCode>")
                sbCountriesList.Append("    <CtyName>")
                sbCountriesList.Append(Trim(rowCountry("CtyName")))
                sbCountriesList.Append("    </CtyName>")
                sbCountriesList.Append("</Country>")
            Next

        Else ' Otherwise its a newly added country
            If NewlyAddedCountry.Trim() <> "" Then

                Dim sCountryCode, sCountryText As String
                sCountryCode = NewlyAddedCountry.Split("#"c)(0).Trim()
                sCountryText = NewlyAddedCountry.Split("#"c)(1).Trim()

                If DomesticCountriesList.OuterXml.Contains("<CtyCode>" & sCountryCode) Then
                    CurrentPage.SetWarningShortMessage("The selected country of residence already exists in the list")
                    Exit Sub
                End If

                sbCountriesList.Append("<Country>")
                sbCountriesList.Append("<PkgSetupId>")
                sbCountriesList.Append("")
                sbCountriesList.Append("</PkgSetupId>")
                sbCountriesList.Append("<CtyCode>")
                sbCountriesList.Append(sCountryCode)
                sbCountriesList.Append("</CtyCode>")
                sbCountriesList.Append("<CtyName>")
                sbCountriesList.Append(sCountryText)
                sbCountriesList.Append("</CtyName>")
                sbCountriesList.Append("</Country>")
            End If
        End If

        sbCountriesList.Append("</Root>")

        xmlSelectedCountries.LoadXml(sbCountriesList.ToString())

        If DomesticCountriesList.InnerXml <> "" Then
            ' pre-existing data
            xmlSelectedCountries.DocumentElement.InnerXml = DomesticCountriesList.DocumentElement.InnerXml & xmlSelectedCountries.DocumentElement.InnerXml
        End If

        If CalledPostDelete Then
            Dim xmlRemainingCountriesPostDelete As New XmlDocument
            xmlRemainingCountriesPostDelete.LoadXml("<Root/>")

            ' remove the countries that dont exist
            For Each oCountryNode As XmlNode In xmlSelectedCountries.DocumentElement.ChildNodes
                For Each oListItem As ListItem In cblDomesticCountriesList.Items
                    If oCountryNode.SelectSingleNode("CtyName").InnerText.Trim() = oListItem.Text.Trim() Then
                        xmlRemainingCountriesPostDelete.DocumentElement.InnerXml = xmlRemainingCountriesPostDelete.DocumentElement.InnerXml & oCountryNode.OuterXml
                    End If
                Next
            Next

            xmlSelectedCountries = xmlRemainingCountriesPostDelete
        End If

        ' save to viewstate
        DomesticCountriesList = xmlSelectedCountries

        Dim dstSelectedCountries As New DataSet
        dstSelectedCountries.ReadXml(New XmlNodeReader(xmlSelectedCountries))

        Dim dtSelectedCountries As New DataTable
        If dstSelectedCountries.Tables.Count > 0 Then
            dtSelectedCountries = dstSelectedCountries.Tables(0)
        End If

        Try
            For i As Int16 = 0 To dtSelectedCountries.Rows.Count - 1
                If dtSelectedCountries.Rows(i)(0) = "" And dtSelectedCountries.Rows(i)(1) = "" And dtSelectedCountries.Rows(i)(2) = "" Then
                    dtSelectedCountries.Rows.RemoveAt(i)
                    Exit Try
                End If
            Next
        Catch
        End Try

        cblDomesticCountriesList.DataSource = dtSelectedCountries
        cblDomesticCountriesList.DataTextField = "CtyName"
        cblDomesticCountriesList.DataValueField = "PkgSetupId"
        cblDomesticCountriesList.DataBind()

        If cblDomesticCountriesList.Items.Count = 0 Then
            btnRemove.Enabled = False
        Else
            btnRemove.Enabled = True
        End If

    End Sub

    Sub InitCountriesList(ByVal CountryDataTable As DataTable)
        cmbCountriesList.Items.Clear()
        cmbCountriesList.Items.Add("(Select)")
        cmbCountriesList.AppendDataBoundItems = True
        cmbCountriesList.DataSource = CountryDataTable
        cmbCountriesList.DataTextField = "CountryName"
        cmbCountriesList.DataValueField = "CtyCode"
        cmbCountriesList.DataBind()
    End Sub

#End Region

#Region "Page Event"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not Page.IsPostBack Then
            InitialisePage()
        End If

        'Added by Raj, Dec 7, 2011 -------------------------------------------------------------
        'if user not part of BSS group
        DisableControls(Convert.ToBoolean(Session("BSSUser")))
        'Added by Raj, Dec 7, 2011 -------------------------------------------------------------
    End Sub

    'Added by Raj, Dec 7, 2011 -------------------------------------------------------------
    Protected Sub DisableControls(ByVal blnDisabled As Boolean)
        dtcWebEnabledFrom.Enabled = blnDisabled
        dtcWebEnabledTo.Enabled = blnDisabled
        InformationLinkTextBox.Enabled = blnDisabled
        btnDeleteTandCRow.Enabled = blnDisabled
        btnAddNewTandCRow.Enabled = blnDisabled
        gwTermsAndConditions.Enabled = blnDisabled
        cmbParentPackageList.Enabled = blnDisabled
        pnlLinkedChildPackages.Enabled = blnDisabled
        cblDomesticCountriesList.Enabled = blnDisabled
        chkSelectAllDomesticCountries.Enabled = blnDisabled
        cmbCountriesList.Enabled = blnDisabled
        btnRemove.Enabled = blnDisabled
        btnSave.Enabled = blnDisabled
        btnReset.Enabled = blnDisabled
    End Sub
    'Added by Raj, Dec 7, 2011 -------------------------------------------------------------

    Protected Sub dtcWebEnabledFrom_AutoPostback(ByVal sender As Object, ByVal e As EventArgs) Handles dtcWebEnabledFrom.DateChanged
        If dtcWebEnabledFrom.IsValid AndAlso _
           dtcWebEnabledFrom.Date <= PackageBookedToDate _
        Then
            dtcWebEnabledTo.Date = PackageBookedToDate
            ScriptManager.GetCurrent(Page).SetFocus(dtcWebEnabledTo.FindControl("dateTextBox"))
        End If
        CreateHyperLinksForChildPackages()
    End Sub

#End Region

#Region "Confirmation Event"
    Protected Sub ConfirmationBox_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationDelete.PostBack
        If leftButton Then
            Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
                Try
                    If chkSelectAllDomesticCountries.Checked Then
                        Aurora.Package.Data.DeletePackageSetupDomesticCountries(PkgId, 0, PkgCtyCode, True)
                    Else
                        For Each oItem As ListItem In cblDomesticCountriesList.Items
                            If oItem.Selected Then

                                If oItem.Value.Trim() <> "" Then
                                    Aurora.Package.Data.DeletePackageSetupDomesticCountries(PkgId, CInt(oItem.Value.Trim()), PkgCtyCode, False)
                                End If

                                ' remove from memory list
                                Dim xmlTemp As XmlDocument = DomesticCountriesList
                                Dim oNode As XmlNode = xmlTemp.DocumentElement.SelectSingleNode("Country[CtyName='" & oItem.Text & "']")
                                xmlTemp.DocumentElement.RemoveChild(oNode)
                                DomesticCountriesList = xmlTemp
                            End If
                        Next
                    End If

                    oTransaction.CommitTransaction()
                    CurrentPage.SetInformationMessage("Selected country of residence mapping(s) deleted successfully")

                    UpdateDomesticCountriesList(Nothing, "", True, False)
                    CreateHyperLinksForChildPackages()
                Catch ex As Exception
                    oTransaction.RollbackTransaction()
                    CurrentPage.SetErrorMessage("Error occured while deleting selected country of residence mapping(s)")
                End Try
            End Using
        End If
    End Sub
#End Region

#Region "SP Used"


    ' this list needs to be updated!


#End Region





End Class
