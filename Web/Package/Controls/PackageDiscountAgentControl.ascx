﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PackageDiscountAgentControl.ascx.vb" Inherits="Package_Controls_PackageDiscountAgentControl" %>

<asp:UpdatePanel ID="UpdatePanelPackageDiscountAgent" runat="server">
    <ContentTemplate>
      

         <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td width="40%">
                    Available Agents
                </td>
                <td width="20%">
                </td>

                <td width="50%">
                    Selected Agents <asp:CheckBox runat="server" ID="agentcheckboxexclude" Text="Exclude" />
                </td>
            </tr>

            <tr>
                <td height="100%">
                    <asp:ListBox runat="server" ID="availableAgentListbox" runat="server" SelectionMode="Multiple" Width="350" Height="200" CssClass="listboxclass"   >
                    </asp:ListBox>
                </td>

                <td rowspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="addtoButton" text=" > " />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="removefromButton" text=" < " />
                            </td>
                        </tr>
                    </table>
                </td>

                <td height="100%">
                    <asp:ListBox runat="server" ID="selectedAgentListBox" runat="server" SelectionMode="Multiple" Width="350" Height="200" CssClass="listboxclass">
                    </asp:ListBox>
                </td>
            </tr>
        </table>
          <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="saveAgentProfileButton" runat="Server" Text="Save Agent(s)" CssClass="Button_Standard Button_Save"
                        Visible="False" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
