﻿''rev:mia Sept 16 2013 - add ComCode
Option Strict On
Option Explicit On

Imports System.Collections
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Package.Data



Partial Class Package_Controls_DiscountProfilesControl
    Inherits System.Web.UI.UserControl

    Protected Shared mActiveColor As String = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ActiveColor)
    Protected Shared mInactiveColor As String = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor)

    Public ReadOnly Property PackageId() As String
        Get
            Return CStr(Me.Request.QueryString("PkgId"))
        End Get
    End Property

    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        BindDiscounts()
    End Sub

    Protected Sub BindDiscounts()
        Dim repo As New DiscountProfileSearchRepository()

        Dim discounts As List(Of DiscountProfile) _
            = repo.GetSearchResults(Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, PackageId(), Nothing, Nothing, Nothing) ''rev:mia Sept 16 2013 - add ComCode

        rptDiscounts.DataSource = discounts
        rptDiscounts.DataBind()
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        Dim page As Page = Me.Page

        plhStyle.Parent.Controls.Remove(plhStyle)

        If page IsNot Nothing Then
            Dim header As HtmlHead = page.Header

            If header IsNot Nothing Then
                header.Controls.Add(plhStyle)
            End If
        End If

        MyBase.OnPreRender(e)
    End Sub

End Class
