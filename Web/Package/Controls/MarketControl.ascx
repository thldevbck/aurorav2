<%@ Control Language="VB" AutoEventWireup="false" CodeFile="MarketControl.ascx.vb" Inherits="Package_MarketControl" %>

<asp:CheckBoxList 
    ID="marketCodesCheckBoxList" 
    runat="server" 
    RepeatDirection="Vertical" 
    RepeatLayout="Table" 
    RepeatColumns="5" 
    Width="100%" />

<div style="text-align: right">
    <asp:HyperLink runat="server" ID="selectAllHyperLink" NavigateUrl="#">Select All</asp:HyperLink>
    &nbsp;&nbsp;|&nbsp;&nbsp;
    <asp:HyperLink runat="server" ID="selectNoneHyperLink" NavigateUrl="#">Select None</asp:HyperLink>
</div>

<br />

<div style="text-align: right">
    <asp:Button ID="updateButton" runat="server" Text="Save Codes" CssClass="Button_Standard Button_Save" Visible="False" />
</div>

