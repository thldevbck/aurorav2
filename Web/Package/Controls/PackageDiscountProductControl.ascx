﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PackageDiscountProductControl.ascx.vb"
    Inherits="Package_Controls_PackageDiscountProductControl" %>

<%@ Register Src="~/UserControls/MultiSelectControl/MultiSelect.ascx" TagName="MultiSelect" TagPrefix="uc1" %>

<div style="display:none;">
    <uc1:MultiSelect SelectType="VehicleProducts" ID="productsMultiSelect" RetainValuesOnPostback="false" runat="server" />
</div>

<asp:PlaceHolder ID="plhProductMultiSelectInit" Visible="false" runat="server">
    <script type="text/javascript">
        (
            function () {
                var oSelectControl = <%=productsMultiSelect.ClientScriptReference %>,
                oList = document.getElementById('<%=selectedProductListBox.ClientID %>'),                
                oSelectedItemsState = document.getElementById('<%= hidSelectedItems.ClientID %>'),                
                aOptions = oList.options,
                iLen = aOptions.length,                
                aState=[],
                buildState = function(){
                    var i=-1, oOption;

                    while(++i < iLen){
                        (oOption = aOptions[i]).selected = false;
                        aState.push(oOption.text);
                        aState.push(oOption.value);                        
                    }
                    
                    oSelectedItemsState.value = oSelectControl.toCsv(aState);
                };

                buildState();

                oList.ondblclick = function(){
                    var
                    i=-1,
                    oOption = aOptions[oList.selectedIndex],
                    bSelected,
                    aValues,
                    saveState = (function(option, index){
                        return function(displayValues, stateValues){
                            var sText = option.text,
                            i = option.text.indexOf('(');

                            sText = sText.substr(0, i+1) + displayValues + ')';

                            aValues = oSelectControl.getValuesFromCsv(option.value);
                            aValues[1] = stateValues;

                            option.text = sText;                            

                            aState[index*2] = sText;
                            aState[index*2 + 1] = oSelectControl.toCsv(aValues);

                            oSelectedItemsState.value = oSelectControl.toCsv(aState);
                        }
                    })(oOption, oList.selectedIndex),
                    getState = (function(index){
                        return function(){
                            aValues = oSelectControl.getValuesFromCsv(aState[index*2 + 1]);

                            return aValues[1];
                        }
                    })(oList.selectedIndex);

                    aValues = oSelectControl.getValuesFromCsv(oOption.value);

                    if(aValues.length < 2)
                        return;

                    while(++i < iLen){
                        if(bSelected && aOptions[i].selected)
                            return;

                        bSelected = aOptions[i].selected;
                    }

                    oSelectControl.open(null, saveState, getState);                                
                };
            }
        )();
    </script>
</asp:PlaceHolder>

<asp:UpdatePanel ID="UpdatePanelPackageDiscountProduct" runat="server">
    <ContentTemplate>
       
       <asp:HiddenField ID="hidSelectedItems" runat="server" />

         <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td width="40%">
                    Available Products
                </td>
                <td width="20%">
                </td>

                <td width="50%">
                    Selected Products <asp:CheckBox runat="server" ID="productcheckboxexclude" Text="Exclude" />
                </td>
            </tr>

            <tr>
                <td height="100%">
                    <asp:ListBox runat="server" ID="availableProductListbox" runat="server" SelectionMode="Multiple" Width="350" Height="200"  CssClass="listboxclass">
                    </asp:ListBox>
                </td>
                
                <td rowspan="2" align="center">
                    <table>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="addtoButton" text=" > " />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="removefromButton" text=" < " />
                            </td>
                        </tr>
                    </table>
                </td>

                <td height="100%">
                    <asp:ListBox runat="server" ID="selectedProductListBox" runat="server" SelectionMode="Multiple" Width="350" Height="200" CssClass="listboxclass">
                    </asp:ListBox>
                </td>
            </tr>
        </table>
         <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="saveProductProfileButton" runat="Server" Text="Save Product(s)" CssClass="Button_Standard Button_Add"
                        Visible="False" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
