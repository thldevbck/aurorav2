<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NoteControl.ascx.vb" Inherits="Package_NoteControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:UpdatePanel runat="server">
    <ContentTemplate>

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">&nbsp;
                    <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="False" />
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
        </table>    

        <asp:Table ID="notesTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableColor" Style="width:100%">
            <asp:TableHeaderRow CssClass="trHeader">
                <asp:TableHeaderCell Style="width:20px">
                    <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                </asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width:75px">Type</asp:TableHeaderCell>
                <asp:TableHeaderCell>Notes</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width:150px">Details</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width:150px">User</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>

        <asp:Button runat="server" ID="notePopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="notePopup" 
            BehaviorID="notePopupBehavior"
            TargetControlID="notePopupButton" 
            PopupControlID="notePopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="notePopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup" ID="notePopupPanel" Style="display: none; padding: 10px;" Width="500px">
            <asp:Panel runat="Server" ID="notePopupDragPanel" CssClass="modalPopupTitle">
                <asp:Label ID="notePopupTitleLabel" runat="Server" Text="Note" />
            </asp:Panel>
            
            <asp:HiddenField ID="notePopupIdHidden" runat="server" />

            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td>Audience:</td>
                    <td><asp:DropDownList ID="notePopupAudienceDropDown" runat="server" /></td>
                    <td>Priority:</td>
                    <td>
                        <asp:DropDownList ID="notePopupPriorityDropDown" runat="server">
                            <asp:ListItem Text="1" />
                            <asp:ListItem Text="2" />
                            <asp:ListItem Text="3" />
                        </asp:DropDownList>
                    </td>
                    <td>Active:</td>
                    <td><asp:CheckBox ID="notePopupActiveCheckBox" runat="server" /></td>
                </tr>
                <tr>
                    <td colspan="6"><asp:TextBox ID="notePopupNoteTextBox" runat="server" TextMode="MultiLine" Rows="6" Width="99%"></asp:TextBox></td>
                </tr>                    
                <tr><td colspan="2">&nbsp;</td></tr>
                <tr>
                    <td align="right" colspan="6">
                        <asp:Button ID="notePopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="notePopupDeleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                        <asp:Button ID="notePopupUpdateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="notePopupCancelButton" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
