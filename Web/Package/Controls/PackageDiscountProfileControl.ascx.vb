﻿''rev:mia Feb.28 2013 - Added a complete and working version
''                    - today in history: Last day of Pope Benedict                        

Imports System.Data
Imports Aurora.Package.Data

Partial Class Package_Controls_PackageDiscountProfileControl
    Inherits AuroraUserControl

    Private Const Active As String = "Active"
    Private Const Pending As String = "Pending"
    Private Const Inactive As String = "Inactive"

    Private _MessageInfo As String
    Public Property MessageInfo As String
        Get
            Return _MessageInfo
        End Get
        Set(value As String)
            _MessageInfo = value
        End Set
    End Property

    Private _RequeryURL As String
    Public Property RequeryURL As String
        Get
            Return _RequeryURL
        End Get
        Set(value As String)
            _RequeryURL = value
        End Set
    End Property

    Private _SuccessfulMessage As String
    Public Property SuccessfulMessage As String
        Get
            Return _SuccessfulMessage
        End Get
        Set(value As String)
            _SuccessfulMessage = value
        End Set
    End Property

    Private _FailedMessage As String
    Public Property FailedMessage As String
        Get
            Return _FailedMessage
        End Get
        Set(value As String)
            _FailedMessage = value
        End Set
    End Property

    Private _DisproIdControl As String
    Public Property DisproIdControl As String
        Get
            Return _DisproIdControl
        End Get
        Set(value As String)
            _DisproIdControl = value
        End Set
    End Property

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.DiscountProfileSearch)
        End Get
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (LoadPackageDiscountProfilesDataTable Is Nothing) Then
            addPackageProfileButton.Visible = CanMaintain
            Return
        End If

        Try
            InitPackageDiscountProfile()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try

        packageProfilePopupCancelButton.Attributes.Add("onclick", "$find('packageProfileBehavior').hide(); return false;")
    End Sub

    Private _PackageDiscountProfilesDataTable As DataTable
    Public Property LoadPackageDiscountProfilesDataTable() As DataTable
        Get
            Return _PackageDiscountProfilesDataTable
        End Get
        Set(value As DataTable)
            _PackageDiscountProfilesDataTable = value
        End Set
    End Property

    Sub InitPackageDiscountProfile()

        Dim rowIndex As Integer = 0

        For Each row As DataRow In LoadPackageDiscountProfilesDataTable.Rows

            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "row", "rowalt") : rowIndex += 1
            If (row.Item("PkgIsActive") = Active) Then
                tableRow.BackColor = Aurora.Common.DataConstants.ActiveColor
            ElseIf (row.Item("PkgIsActive") = Pending) Then
                tableRow.BackColor = Aurora.Common.DataConstants.FutureColor
            ElseIf (row.Item("PkgIsActive") = Inactive) Then
                tableRow.BackColor = Aurora.Common.DataConstants.InactiveColor
            End If

            packageTable.Rows.Add(tableRow)


            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "remove" & row.Item("PkgId") & "CheckBox"
                selectCell.Controls.Add(checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim nameCell As New TableCell
            tableRow.Controls.Add(nameCell)

            If Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.DiscountProfileSearch) Then
                Dim hyperLink As New HyperLink
                hyperLink.Text = Server.HtmlEncode(row.Item("PkgCode") & " - " & row.Item("PkgName"))
                hyperLink.NavigateUrl = PackageUserControl.MakePackageUrl(Me.Page, row.Item("PkgId"))
                nameCell.Controls.Add(hyperLink)
            Else
                nameCell.Text = Server.HtmlEncode(row.Item("PkgCode") & " - " & row.Item("PkgName"))
            End If

            Dim brandCell As New TableCell
            brandCell.Text = Server.HtmlEncode(row.Item("BrdName"))
            tableRow.Controls.Add(brandCell)

            Dim bookedCell As New TableCell
            bookedCell.Text = Server.HtmlEncode(Convert.ToDateTime(row.Item("PkgBookedFromDate")).ToShortDateString & " - " & Convert.ToDateTime(row.Item("PkgBookedToDate")).ToShortDateString)
            tableRow.Controls.Add(bookedCell)

            Dim travelCell As New TableCell
            travelCell.Text = Server.HtmlEncode(Convert.ToDateTime(row.Item("PkgTravelFromDate")).ToShortDateString & " - " & Convert.ToDateTime(row.Item("PkgTravelToDate")).ToShortDateString)
            tableRow.Controls.Add(travelCell)

            Dim statusCell As New TableCell
            statusCell.Text = Server.HtmlEncode(row.Item("PkgIsActive"))
            tableRow.Controls.Add(statusCell)

        Next

        addPackageProfileButton.Visible = CanMaintain
        removePackageProfileButton.Visible = CanMaintain
    End Sub

    Protected Sub addPackageProfileButton_Click(sender As Object, e As System.EventArgs) Handles addPackageProfileButton.Click
        If Not CanMaintain Then Return

        packagePopupPicker.Param2 = ""
        packagePopupPicker.DataId = ""
        packagePopupPicker.Text = ""

        packagePopup.Show()
    End Sub

    Protected Sub removePackageProfileButton_Click(sender As Object, e As System.EventArgs) Handles removePackageProfileButton.Click
        If Not CanMaintain Then Return
        Dim packageProfile As New EditPackageProfile()
        Dim _checkBox As CheckBox = Nothing
        Dim result As Integer = 0
        For Each row As TableRow In packageTable.Rows

            Try
                If (row.Controls(0).Controls.Count <> 0) Then
                    If TypeOf row.Controls(0).Controls(0) Is CheckBox Then
                        _checkBox = row.Controls(0).Controls(0)
                        If (_checkBox.Checked = True) Then
                            result = packageProfile.PackageDeletePackageDiscountProfile(DisproIdControl, _checkBox.ID.ToString.Replace("remove", "").Replace("CheckBox", "").Trim)
                        End If
                    End If
                End If
                

            Catch ex As Exception
                'do nothing
            End Try
            If (result < 0) Then Exit For
        Next

        If (result > 0) Then
            MessageInfo = SuccessfulMessage
            Response.Redirect(RequeryURL & "&saved=ok")
        End If
        If (result < 0) Then
            Me.CurrentPage.AddWarningMessage(FailedMessage)
        End If

    End Sub

    Protected Sub packageProfilePopupAddButton_Click(sender As Object, e As System.EventArgs) Handles packageProfilePopupAddButton.Click
        If Not CanMaintain Then Return
        Dim packageProfile As New EditPackageProfile()
        Dim result As Integer = 0
        Try
            result = packageProfile.PackageInsertSinglePackageProfile(String.Empty, DisproIdControl, packagePopupPicker.DataId, True, CurrentPage.UserCode)
        Catch ex As Exception
            result = -1
        End Try

        If (result > 0) Then
            MessageInfo = SuccessfulMessage
            Response.Redirect(RequeryURL & "&saved=ok&activeTab=0")
        End If
        If (result <= 0) Then
            Me.CurrentPage.AddWarningMessage(FailedMessage)
        End If
        
    End Sub
End Class
