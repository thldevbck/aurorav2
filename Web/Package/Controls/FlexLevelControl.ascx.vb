Imports System.Collections.Generic

Imports Aurora.Flex.Data
Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


<ToolboxData("<{0}:FlexLevelControl runat=server></{0}:FlexLevelControl>")> _
Partial Class Package_FlexLevelControl
    Inherits PackageUserControl

    Private _now As Date = Date.Now
    Private _initException As Exception
    Private _flexLevelRow As FlexLevelRow

    Public ReadOnly Property CanMaintainFlexRates() As Boolean
        Get
            Return Me.PackageRow IsNot Nothing _
             AndAlso CanMaintain AndAlso (Me.PackageRow.IsCurrent(_now) OrElse Me.PackageRow.IsFuture(_now)) _
             AndAlso Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.FlexRateMaintenance)
        End Get
    End Property

    Public Sub InitFlexLevels()
        For flexInt As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
            Dim col As Integer = flexInt Mod 8 + 1
            Dim row As Integer = Int(flexInt / 8) + 1

            Dim levelTableCell As WebControls.TableCell = levelTable.Rows(row).Cells(col)
            levelTableCell.ID = "level" + CStr(flexInt) + "Cell"
            levelTableCell.BackColor = FlexConstants.FlexIntToColor(flexInt)

            Dim regExTextBox As UserControls_RegExTextBox = CType(levelTableCell.Controls(0), UserControls_RegExTextBox)
            regExTextBox.ErrorMessage = "Flex Level Rate for " & FlexConstants.FlexIntToRate(flexInt) & " must be a positive decimal value"
        Next
    End Sub

    Public Overrides Sub InitPackageUserControl(ByVal packageDataSet As Aurora.Package.Data.PackageDataSet, ByVal packageRow As Aurora.Package.Data.PackageDataSet.PackageRow)
        MyBase.InitPackageUserControl(packageDataSet, packageRow)

        Try
            flexLevelTravelYearDropDown.Items.Clear()
            For Each flexTravelYearRow As FlexTravelYearRow In Me.PackageDataSet.FlexTravelYear
                flexLevelTravelYearDropDown.Items.Add(New ListItem(flexTravelYearRow.FwnTrvYearStart.ToString("yyyy"), flexTravelYearRow.FwnTrvYearStart.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)))
            Next

            InitFlexLevels()
        Catch ex As Exception
            _initException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Private Sub BuildRateSet(ByVal useViewState As Boolean)
        Dim listItem As ListItem
        Dim selectedFlxId As String = flexLevelDropDown.SelectedValue
        Dim defaultFlxId As String = Nothing
        Dim uniqueFlexLevelSets As FlexLevelRow() = Me.PackageRow.UniqueFlexLevelSets

        flexLevelDropDown.Items.Clear()
        For Each flexLevelRow As FlexLevelRow In Me.PackageRow.UniqueFlexLevelSets
            Dim color As System.Drawing.Color = PackageConstants.FutureColor
            If flexLevelRow.FlxEffDate <= DateTime.Now Then
                If defaultFlxId Is Nothing Then
                    defaultFlxId = flexLevelRow.FlxId
                    color = PackageConstants.CurrentColor
                Else
                    color = PackageConstants.PastColor
                End If
            End If
            listItem = New ListItem(flexLevelRow.Description, flexLevelRow.FlxId)
            listItem.Attributes.Add("style", "background-color:" + System.Drawing.ColorTranslator.ToHtml(color))
            flexLevelDropDown.Items.Add(listItem)
        Next
        listItem = New ListItem(NewListItemText)
        listItem.Attributes.Add("style", "background-color:" + System.Drawing.ColorTranslator.ToHtml(PackageConstants.InactiveColor))
        flexLevelDropDown.Items.Insert(0, listItem)

        If defaultFlxId Is Nothing AndAlso uniqueFlexLevelSets.Length > 0 Then
            defaultFlxId = uniqueFlexLevelSets(uniqueFlexLevelSets.Length - 1).FlxId
        End If

        If selectedFlxId Is Nothing OrElse flexLevelDropDown.Items.FindByValue(selectedFlxId) Is Nothing Then
            selectedFlxId = defaultFlxId
        End If

        If selectedFlxId IsNot Nothing Then
            flexLevelDropDown.SelectedValue = selectedFlxId
            _flexLevelRow = Me.PackageDataSet.FlexLevel.FindByFlxId(selectedFlxId)
        End If

        flexLevelUpdateButton.Visible = _flexLevelRow IsNot Nothing AndAlso CanMaintainFlexRates
        flexLevelCreateButton.Visible = _flexLevelRow Is Nothing AndAlso CanMaintainFlexRates
    End Sub

    Private Sub BuildRates(ByVal useViewState As Boolean)
        If useViewState Then Return

        If _flexLevelRow IsNot Nothing Then
            flexLevelAddModLabel.Text = Server.HtmlEncode(Aurora.Common.Data.GetAddModDescription(_flexLevelRow))
            flexLevelEffectiveDateTextBox.Date = _flexLevelRow.FlxEffDate
            Try
                flexLevelTravelYearDropDown.SelectedValue = _flexLevelRow.FlxTravelYrStart.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            Catch ex As Exception
                flexLevelTravelYearDropDown.SelectedIndex = 0
            End Try
        Else
            flexLevelAddModLabel.Text = ""
            flexLevelEffectiveDateTextBox.Date = Date.Now
            flexLevelTravelYearDropDown.SelectedIndex = 0
        End If

        flexLevelEffectiveDateTextBox.Enabled = CanMaintainFlexRates
        flexLevelTravelYearDropDown.Enabled = CanMaintainFlexRates

        For flexInt As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
            Dim col As Integer = flexInt Mod 8 + 1
            Dim row As Integer = Int(flexInt / 8) + 1

            Dim levelTableCell As WebControls.TableCell = levelTable.Rows(row).Cells(col)
            Dim regExTextBox As UserControls_RegExTextBox = CType(levelTableCell.Controls(0), UserControls_RegExTextBox)

            regExTextBox.Text = "0"
            regExTextBox.Enabled = CanMaintainFlexRates
            If _flexLevelRow IsNot Nothing Then
                For Each flexLevelRow0 As FlexLevelRow In Me.PackageRow.GetFlexLevelRows()
                    If flexLevelRow0.FlxEffDate = _flexLevelRow.FlxEffDate _
                     AndAlso flexLevelRow0.FlxNum = FlexConstants.FlexIntToFlexNum(flexInt) Then
                        regExTextBox.Text = flexLevelRow0.FlxRate.ToString("0.#")
                        Exit For
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If Me.PackageRow Is Nothing Then Return

        BuildRateSet(useViewState)
        BuildRates(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack OrElse _initException IsNot Nothing Then Return

        BuildForm(False)
    End Sub

    Private Sub ValidateFlexRateSet()
        If Not flexLevelEffectiveDateTextBox.IsValid Then
            Throw New Aurora.Common.ValidationException("Enter a valid effective Date")
        End If

        For flexInt As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
            Dim col As Integer = flexInt Mod 8 + 1
            Dim row As Integer = Int(flexInt / 8) + 1

            Dim levelTableCell As WebControls.TableCell = levelTable.Rows(row).Cells(col)
            Dim regExTextBox As UserControls_RegExTextBox = CType(levelTableCell.Controls(0), UserControls_RegExTextBox)

            Dim d As Decimal
            If Not regExTextBox.IsTextValid Or Not Decimal.TryParse(regExTextBox.Text, d) Then
                Throw New Aurora.Common.ValidationException(regExTextBox.ErrorMessage)
            End If
        Next

        If Me.PackageRow.GetPackageProductRows().Length = 0 Then
            Throw New Aurora.Common.ValidationException("No saleable products are linked to on this package")
        End If
    End Sub

    Protected Sub flexLevelUpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles flexLevelUpdateButton.Click, flexLevelCreateButton.Click
        If Not CanMaintainFlexRates Then
            BuildForm(False)
            Return
        End If

        BuildForm(True)

        Try
            ValidateFlexRateSet()
        Catch ex As Aurora.Common.ValidationException
            Me.CurrentPage.AddWarningMessage(ex.Message)
            Return
        End Try

        Dim effDate As Date = flexLevelEffectiveDateTextBox.Date
        Dim travelYear As Date = Date.ParseExact(flexLevelTravelYearDropDown.SelectedValue, Aurora.Common.UserSettings.Current.ComDateFormat, System.Globalization.CultureInfo.CurrentCulture)
        Dim flexLevelRow As FlexLevelRow = Nothing

        ' create or update FlexLevel rows for each flex number and saleable product (for this package)
        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                For flexInt As Integer = FlexConstants.FlexIntMin To FlexConstants.FlexIntMax
                    Dim col As Integer = flexInt Mod 8 + 1
                    Dim row As Integer = Int(flexInt / 8) + 1

                    Dim levelTableCell As WebControls.TableCell = levelTable.Rows(row).Cells(col)
                    Dim regExTextBox As UserControls_RegExTextBox = CType(levelTableCell.Controls(0), UserControls_RegExTextBox)

                    Dim rate As Decimal = Decimal.Parse(regExTextBox.Text)

                    For Each packageProductRow As PackageProductRow In Me.PackageRow.GetPackageProductRows

                        ' dont bother updating non-vehicles
                        If Not packageProductRow.SaleableProductRow.ProductRow.PrdIsVehicle Then Continue For

                        Dim sapId As String = packageProductRow.PplSapId

                        flexLevelRow = Nothing
                        For Each flexLevelRow0 As FlexLevelRow In Me.PackageRow.GetFlexLevelRows()
                            If flexLevelRow0.FlxPkgId = Me.PackageRow.PkgId _
                             AndAlso flexLevelRow0.FlxSapId = sapId _
                             AndAlso flexLevelRow0.FlxNum = FlexConstants.FlexIntToFlexNum(flexInt) _
                             AndAlso flexLevelRow0.FlxEffDate = effDate Then
                                flexLevelRow = flexLevelRow0
                                Exit For
                            End If
                        Next
                        If flexLevelRow Is Nothing Then
                            flexLevelRow = Me.PackageDataSet.FlexLevel.NewFlexLevelRow()
                            flexLevelRow.FlxId = Aurora.Common.Data.GetNewId()
                            flexLevelRow.FlxPkgId = Me.PackageRow.PkgId
                            flexLevelRow.FlxSapId = sapId
                            flexLevelRow.FlxNum = FlexConstants.FlexIntToFlexNum(flexInt)
                            flexLevelRow.FlxEffDate = effDate
                            Me.PackageDataSet.FlexLevel.AddFlexLevelRow(flexLevelRow)
                        End If

                        flexLevelRow.FlxComplete = False
                        flexLevelRow.FlxTravelYrStart = travelYear
                        flexLevelRow.FlxRate = rate

                        Dim sAction As String
                        Dim xmlResult As New System.Xml.XmlDocument

                        With flexLevelRow
                            If flexLevelRow.RowState = Data.DataRowState.Added Then
                                sAction = "I"
                                .IntegrityNo = 1
                                .AddUsrId = CurrentPage.UserCode
                                .AddPrgmName = Left(Me.AppRelativeVirtualPath, 64)
                                .AddDateTime = Now
                            ElseIf flexLevelRow.RowState = Data.DataRowState.Modified Then
                                sAction = "U"

                            End If

                            'xmlResult = Aurora.Package.Data.DataRepository.SaveFlexData(.FlxId, .FlxNum, .FlxPkgId, .FlxSapId, _
                            '                                                            .FlxEffDate, .FlxRate, IIf(.FlxComplete, "1"c, "0"c), _
                            '                                                            .FlxTravelYrStart, .IntegrityNo, _
                            '                                                             .AddUsrId, CurrentPage.UserCode, _
                            '                                                             .AddPrgmName, sAction)

                            xmlResult = SaveFlexData(.FlxId, .FlxNum, .FlxPkgId, .FlxSapId, _
                                                     .FlxEffDate, .FlxRate, IIf(.FlxComplete, "1"c, "0"c), _
                                                     .FlxTravelYrStart, .IntegrityNo, _
                                                     .AddUsrId, CurrentPage.UserCode, _
                                                     .AddPrgmName, sAction)


                        End With

                        If xmlResult.OuterXml.ToUpper().Contains("ERROR") Then
                            Throw New Exception(xmlResult.DocumentElement.SelectSingleNode("Error/ErrDesc").InnerText)
                        End If

                    Next
                Next

                ' All ok
                oTransaction.CommitTransaction()
                oTransaction.Dispose()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                Me.CurrentPage.AddErrorMessage("Error updating Flex Rate Set")
                Aurora.Common.Logging.LogError(Me.CurrentPage.FunctionCode, "Error updating Flex Rate Set", ex)
                Return
            End Try

        End Using

        ' get the "unique" flex level row that represents this effective date
        For Each flexLevelRow0 As FlexLevelRow In Me.PackageRow.UniqueFlexLevelSets
            If flexLevelRow0.FlxEffDate = effDate Then
                flexLevelRow = flexLevelRow0
                Exit For
            End If
        Next

        ' create in temporarily in the combo box if it doesnt yet exist, and make it selected
        If flexLevelDropDown.Items.FindByValue(flexLevelRow.FlxId) Is Nothing Then
            flexLevelDropDown.Items.Add(New ListItem(flexLevelRow.Description, flexLevelRow.FlxId))
        End If
        flexLevelDropDown.SelectedValue = flexLevelRow.FlxId

        BuildForm(False)
        Me.CurrentPage.AddInformationMessage("Flex Rate Set updated")
    End Sub

    Protected Sub flexLevelDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles flexLevelDropDown.SelectedIndexChanged
        BuildForm(False)
    End Sub

    Public Function SaveFlexData(ByVal FlexId As String, ByVal FlexNumber As Int16, ByVal FlexPackageId As String, _
                                 ByVal FlexSapId As String, ByVal FlexEffectiveDate As DateTime, ByVal FlexRate As Double, _
                                 ByVal Complete As Char, ByVal FlexYearStart As String, ByVal IntegrityNo As Int16, _
                                 ByVal AddUserId As String, ByVal ModUserId As String, ByVal AddProgramName As String, _
                                 ByVal Action As String) As System.Xml.XmlDocument

        Dim xmlResult As New System.Xml.XmlDocument
        xmlResult.LoadXml(Aurora.Common.Data.ExecuteSqlXmlSP("flex_update_FlexRates", FlexId, FlexNumber, FlexPackageId, FlexSapId, FlexEffectiveDate, FlexRate, Complete, FlexYearStart, _
        IntegrityNo, AddUserId, ModUserId, AddProgramName, Action))

        '@sFlxId		        varchar(64),    
        '@iFlxNum			int,   
        '@sFlxPkgId			varchar(64),
        '@sFlxSapId			varchar(64),
        '@sFlxEffDate		datetime,
        '@sFlxRate			money,	
        '@bComplete	        varchar(1),
        '@sFlxYearStart		varchar(64),
        '@IntegrityNo        int,	
        '@sAdduserId			varchar(64),   	 
        '@sAddModuserId      varchar(64),    
        '@sAddProgramName  	varchar(64) ,
        '@sAction            varchar(3)   

        Return xmlResult
    End Function

End Class
