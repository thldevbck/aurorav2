<%@ Control Language="VB" AutoEventWireup="false" CodeFile="SaleableProductsControl.ascx.vb" Inherits="Package_SaleableProductsControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
    
        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">&nbsp;
                    <asp:Button ID="removeSaleableProductButton" runat="Server" Text="Remove" CssClass="Button_Standard Button_Remove" Visible="False" />
                    <asp:Button ID="addSaleableProductButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
        </table>    

        <asp:Table ID="saleableProductTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableColor" style="width:100%">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Style="width: 20px">
                    <input type="checkbox" id="chkSelect" onclick="return toggleTableColumnCheckboxes(0, event);" />        
                </asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Name</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="">&nbsp;</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 80px">Type</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Brand</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Flags</asp:TableHeaderCell>
                <asp:TableHeaderCell Style="width: 100px">Status</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>

    </ContentTemplate>
</asp:UpdatePanel>