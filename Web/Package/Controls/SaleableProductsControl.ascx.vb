Imports System.Collections.Generic

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet

<ToolboxData("<{0}:SaleableProductsControl runat=server></{0}:SaleableProductsControl>")> _
Partial Class Package_SaleableProductsControl
    Inherits PackageUserControl

    Private _loadException As Exception
    Private _checkBoxesBySapId As New Dictionary(Of String, CheckBox)

    Public Sub InitSaleableProducts()
        If Me.PackageRow Is Nothing Then Return

        Dim currentDate As Date = Date.Now

        Dim rowIndex As Integer = 0
        For Each packageProductRow As PackageProductRow In Me.PackageRow.GetPackageProductRows()
            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "row", "rowalt") : rowIndex += 1
            tableRow.BackColor = packageProductRow.SaleableProductRow.StatusColor(currentDate)
            saleableProductTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "remove" & packageProductRow.PplPkgId & "_" & packageProductRow.PplSapId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesBySapId.Add(packageProductRow.PplSapId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim nameCell As New TableCell
            If Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.ProductEnquiry) Then
                Dim hyperLink As New HyperLink
                hyperLink.Text = Server.HtmlEncode(packageProductRow.SaleableProductDescription)
                hyperLink.NavigateUrl = ProductUserControl.MakeSaleableProductUrl(Me.Page, prdId:=packageProductRow.SaleableProductRow.SapPrdId, sapId:=packageProductRow.SaleableProductRow.SapId)
                nameCell.Controls.Add(hyperLink)
            Else
                nameCell.Text = Server.HtmlEncode(packageProductRow.SaleableProductDescription)
            End If
            tableRow.Controls.Add(nameCell)

            nameCell = New TableCell
            nameCell.Text = Server.HtmlEncode(packageProductRow.SaleableProductRow.ProductRow.PrdName)
            tableRow.Controls.Add(nameCell)

            Dim typeCell As New TableCell
            If Not packageProductRow.IsPplCopyCreateLinkNull Then typeCell.Text = Server.HtmlEncode(packageProductRow.PplCopyCreateLink)
            tableRow.Controls.Add(typeCell)

            Dim brandCell As New TableCell
            brandCell.Text = Server.HtmlEncode(packageProductRow.SaleableProductRow.ProductRow.BrandRow.Description)
            tableRow.Controls.Add(brandCell)

            Dim flagsCell As New TableCell()
            Dim flagsList As New List(Of String)
            If Not packageProductRow.SaleableProductRow.IsSapIsBaseNull AndAlso packageProductRow.SaleableProductRow.SapIsBase Then flagsList.Add("Base")
            If Not packageProductRow.SaleableProductRow.IsSapIsFlexNull AndAlso packageProductRow.SaleableProductRow.SapIsFlex Then flagsList.Add("Flex")
            flagsCell.Text = String.Join(" , ", flagsList.ToArray())
            tableRow.Controls.Add(flagsCell)

            Dim statusCell As New TableCell
            statusCell.Text = Server.HtmlEncode(packageProductRow.SaleableProductRow.StatusText(currentDate))
            tableRow.Controls.Add(statusCell)
        Next

        addSaleableProductButton.Visible = CanMaintain
        removeSaleableProductButton.Visible = CanMaintain And Not (Me.PackageRow.IsActive)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            InitSaleableProducts()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try
    End Sub

    Protected Sub addSaleableProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addSaleableProductButton.Click
        If Not CanMaintain Then Return

        Me.Response.Redirect("~\Package\SaleableProductPicker.aspx" & _
            "?pkgId=" & Server.UrlEncode(Me.PackageRow.PkgId))
    End Sub

    Protected Sub removeSaleableProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeSaleableProductButton.Click
        If Not CanMaintain Then Return

        For Each packageProductRow As PackageProductRow In Me.PackageRow.GetPackageProductRows()
            Dim checkBox As CheckBox = _checkBoxesBySapId(packageProductRow.PplSapId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            DataRepository.DeletePackageProduct(packageProductRow.PplPkgId, packageProductRow.PplSapId)
        Next

        Response.Redirect(PackageUserControl.MakePackageUrl(Me.Page, pkgId:=Me.PackageRow.PkgId, tabName:="Saleable Products"))
    End Sub

End Class
