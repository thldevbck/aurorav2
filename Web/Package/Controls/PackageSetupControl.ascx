<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PackageSetupControl.ascx.vb"
    Inherits="Package_Controls_PackageSetupControl" %>
<%@ Register Src="../../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx"
    TagName="ConfirmationBoxControl" TagPrefix="uc2" %>

<script language="javascript" type="text/javascript">
    function SelectAllDomesticCountries(chkAll)
    {
        var spanCountries = $get("ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_cblDomesticCountriesList");
        for (var i = 0; i < spanCountries.childNodes.length; i++)
        {
            if (spanCountries.childNodes[i].type=="checkbox")
            {
                spanCountries.childNodes[i].checked = chkAll.checked;
            }
        }
    }
</script>

<asp:UpdatePanel runat="server" ID="UpdatePanelB2CSetup">
    <ContentTemplate>
        <table style="width: 500px;" cellpadding="2" cellspacing="0">
            <%--    <tr>
            <td style="width: 200px">
            Is Web Enabled?:</td>
            <td>
            <asp:CheckBox ID="IsWebEnabledCheckBox" runat="server" Width="200px" /></td>
            </tr>
            --%>
            <tr runat="server" id="trEnabledDate">
                <td nowrap="nowrap">
                    Web Enabled From Date:</td>
                <td style="width: 150px;">
                    <uc1:DateControl ID="dtcWebEnabledFrom" runat="server" Width="200px" AutoPostBack="true" />
                </td>
                <td nowrap="nowrap" style="width: 150px;">
                    Web Enabled To Date:</td>
                <td style="width: 450px;">
                    <uc1:DateControl ID="dtcWebEnabledTo" runat="server" Width="200px" />
                </td>
            </tr>
            <tr>
                <td nowrap="nowrap" style="">
                    Information Link:</td>
                <td colspan="3" style="">
                    <asp:TextBox ID="InformationLinkTextBox" runat="server" Width="600px" MaxLength="256" /></td>
            </tr>
        </table>
        <hr />
        <table style="width: 100%">
            <tr>
                <td>
                    <b>Terms and Condition Link: </b>
                </td>
                <td align="right" valign="top">
                    <asp:Button ID="btnDeleteTandCRow" runat="Server" Text="Delete" CssClass="Button_Small Button_Delete"
                        OnClick="btnDeleteTandCRow_Click" />
                    <asp:Button ID="btnAddNewTandCRow" Text="Add" runat="server" CssClass="Button_Small Button_Add" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <%--<asp:TextBox ID="TermsAndConditionLinkTextBox" runat="server" Width="300px" MaxLength="64" />--%>
                    <asp:GridView ID="gwTermsAndConditions" runat="server" Width="100%" CssClass="dataTableGrid"
                        AutoGenerateColumns="False" OnRowDataBound="gwTermsAndConditions_RowDataBound">
                        <RowStyle CssClass="evenRow" />
                        <AlternatingRowStyle CssClass="oddRow" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" onclick="return toggleTableColumnCheckboxes(0, event);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelectRow" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    From
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnPtcId" runat="server" Value='<%# bind("PtcId") %>' />
                                    <uc1:DateControl ID="dtcFrom" runat="server" Style="width: 165px" Date='<%# bind("PtcFromDate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    To
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <uc1:DateControl ID="dtcTo" runat="server" Style="width: 165px" Date='<%# bind("PtcToDate") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Vehicle Class
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnVehicleClass" runat="server" Value='<%# bind("PtcClaId") %>' />
                                    <asp:DropDownList ID="cmbVehicleClass" runat="server" CssClass="Dropdown_Small" Style="width: 120px">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    URL
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtTCURL" runat="server" Width="400px" Text='<%# bind("PtcURL") %>'
                                        MaxLength="256"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <hr />
        <table style="width: 700px" cellpadding="2" cellspacing="0" id="tblParentPackage"
            runat="server">
            <tr>
                <td nowrap="nowrap" colspan="2">
                    <b>Link Package: </b>
                </td>
            </tr>
            <tr>
                <td style="width: 158px" nowrap="nowrap">
                    Package Parent:
                </td>
                <td>
                    <asp:DropDownList ID="cmbParentPackageList" runat="server" AppendDataBoundItems="true"
                        AutoPostBack="true" Width="500" />
                </td>
            </tr>
        </table>
        <table style="width: 500px" cellpadding="2" cellspacing="0" id="tblChildPackages"
            runat="server">
            <tr>
                <td nowrap="nowrap" colspan="2">
                    <b>Link Package: </b>
                </td>
            </tr>
            <tr>
                <td style="width: 158px" nowrap="nowrap">
                    Currently Linked Packages:
                </td>
                <td>
                    <asp:Panel ID="pnlLinkedChildPackages" runat="server" EnableViewState="true">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <hr />
        <table width="100%">
            <tr>
                <td align="left">
                    <b>Country of Residence Definition: </b>
                </td>
                <td align="right">
                    <asp:Button ID="btnRemove" runat="server" Text="Remove" CssClass="Button_Standard"
                        Width="100px" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" class="dataTableGrid">
                        <thead>
                            <tr>
                                <th>
                                    Mapped To
                                </th>
                                <th>
                                    Check All
                                </th>
                                <th>
                                    Countries
                                </th>
                            </tr>
                        </thead>
                        <tr>
                            <td style="width:420px">
                                <%--A Checkbox list with the countries inside--%>
                                <asp:CheckBoxList ID="cblDomesticCountriesList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow"
                                    RepeatColumns="3" CssClass="repeatLabelTable">
                                </asp:CheckBoxList>
                            </td>
                            <td>
                                <asp:CheckBox ID="chkSelectAllDomesticCountries" runat="server" onClick="SelectAllDomesticCountries(this);" />
                            </td>
                            <td>
                                <asp:DropDownList ID="cmbCountriesList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cmbCountriesList_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset"
                        Width="100px" />
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        Width="100px" />
                </td>
            </tr>
        </table>
        <uc2:ConfirmationBoxControl runat="server" ID="ConfirmationDelete" />
    </ContentTemplate>
</asp:UpdatePanel>
