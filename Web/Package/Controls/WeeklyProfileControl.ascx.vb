Imports System.Collections.Generic

Imports Aurora.Common

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


<ToolboxData("<{0}:WeeklyProfileControl runat=server></{0}:WeeklyProfileControl>")> _
Partial Class Package_WeeklyProfileControl
    Inherits PackageUserControl

    Public Shared ReadOnly DayOfWeekNames() As String = {"", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}
    Public Const MaxWeeklyProfileRows As Integer = 7

    Public Class WeeklyProfileTableRow
        Public IdHiddenField As HiddenField
        Public FromDayOfWeekDropDown As DropDownList
        Public FromTimeOfDayTimeControl As UserControls_TimeControl
        Public ToDayOfWeekDropDown As DropDownList
        Public ToTimeOfDayTimeControl As UserControls_TimeControl
        Public WeeklyProfileRow As WeeklyProfileRow = Nothing

        Public Sub New(ByVal index As Integer, _
         ByVal idHiddenField As HiddenField, _
         ByVal fromDayOfWeekDropDown As DropDownList, _
         ByVal fromTimeOfDayTimeControl As UserControls_TimeControl, _
         ByVal toDayOfWeekDropDown As DropDownList, _
         ByVal toTimeOfDayTimeControl As UserControls_TimeControl)
            Me.IdHiddenField = idHiddenField
            Me.FromDayOfWeekDropDown = fromDayOfWeekDropDown
            Me.FromTimeOfDayTimeControl = fromTimeOfDayTimeControl
            Me.ToDayOfWeekDropDown = toDayOfWeekDropDown
            Me.ToTimeOfDayTimeControl = toTimeOfDayTimeControl

            For Each dropDown As DropDownList In New DropDownList() {Me.FromDayOfWeekDropDown, Me.ToDayOfWeekDropDown}
                dropDown.Items.Clear()
                For i As Integer = 0 To DayOfWeekNames.Length - 1
                    dropDown.Items.Add(New ListItem(DayOfWeekNames(i), i.ToString()))
                Next
                dropDown.Attributes.Add("onchange", "weeklyProfile_rowChange (this, " & index.ToString() & ")")
            Next
        End Sub

        Public ReadOnly Property IsBlank() As Boolean
            Get
                Return Me.FromDayOfWeekDropDown.SelectedIndex = 0 AndAlso Me.FromDayOfWeekDropDown.SelectedIndex = 0
            End Get
        End Property

        Public Function IsValid(ByVal daysMode As Boolean, ByRef message As String) As Boolean
            If IsBlank() Then
                message = Nothing
                Return True
            ElseIf Me.FromDayOfWeekDropDown.SelectedIndex = 0 OrElse Me.FromDayOfWeekDropDown.SelectedIndex = 0 Then
                message = "A from and to day must be specified"
                Return False
            ElseIf Not daysMode AndAlso (FromTimeOfDayTimeControl.Time = TimeSpan.MinValue OrElse ToTimeOfDayTimeControl.Time = TimeSpan.MinValue) Then
                message = "An invalid time has been entered"
                Return False
            Else
                message = Nothing
                Return True
            End If
        End Function

        Public Function IsValid(ByVal daysMode As Boolean) As Boolean
            Dim message As String = Nothing
            Return IsValid(daysMode, message)
        End Function

        Public Function GetFromTimeSpan(ByVal daysMode As Boolean) As TimeSpan
            If IsValid(daysMode) Then
                Dim result As New TimeSpan(Me.FromDayOfWeekDropDown.SelectedIndex - 1, 0, 0, 0)
                If Not daysMode Then result += Me.FromTimeOfDayTimeControl.Time
                Return result
            Else
                Return TimeSpan.MinValue
            End If
        End Function

        Public Function GetToTimeSpan(ByVal daysMode As Boolean) As TimeSpan
            If IsValid(daysMode) Then
                Dim result As New TimeSpan(Me.ToDayOfWeekDropDown.SelectedIndex - 1, 0, 0, 0)
                If Not daysMode Then
                    result += Me.ToTimeOfDayTimeControl.Time
                End If
                Return result
            Else
                Return TimeSpan.MinValue
            End If
        End Function

        Public Sub ReadFromWeeklyProfileRow(ByVal weeklyProfileRow As WeeklyProfileRow, ByVal canMaintain As Boolean)
            If weeklyProfileRow IsNot Nothing Then
                Me.IdHiddenField.Value = weeklyProfileRow.WkpId
                Me.FromDayOfWeekDropDown.SelectedValue = weeklyProfileRow.WkpFromDayOfWeek
                If Not weeklyProfileRow.IsWkpFromTimeOfDayNull Then
                    Me.FromTimeOfDayTimeControl.Time = weeklyProfileRow.WkpFromTimeOfDay.TimeOfDay
                Else
                    Me.FromTimeOfDayTimeControl.Text = "0:00"
                End If
                Me.ToDayOfWeekDropDown.SelectedIndex = weeklyProfileRow.WkpToDayOfWeek
                If Not weeklyProfileRow.IsWkpFromTimeOfDayNull Then
                    Me.ToTimeOfDayTimeControl.Time = weeklyProfileRow.WkpToTimeOfDay.TimeOfDay
                Else
                    Me.ToTimeOfDayTimeControl.Text = "23:59"
                End If
            Else
                Me.IdHiddenField.Value = ""
                Me.FromDayOfWeekDropDown.SelectedIndex = 0
                Me.FromTimeOfDayTimeControl.Text = "0:00"
                Me.ToDayOfWeekDropDown.SelectedIndex = 0
                Me.ToTimeOfDayTimeControl.Text = "23:59"
            End If

            Me.FromDayOfWeekDropDown.Enabled = canMaintain
            Me.FromTimeOfDayTimeControl.ReadOnly = Not canMaintain
            Me.ToDayOfWeekDropDown.Enabled = canMaintain
            Me.ToTimeOfDayTimeControl.ReadOnly = Not canMaintain
        End Sub

        Public Sub WriteToWeeklyProfileRow(ByVal daysMode As Boolean, ByVal weeklyProfileRow As WeeklyProfileRow)
            weeklyProfileRow.WkpProfileType = IIf(daysMode, "Days", "Mixed")

            weeklyProfileRow.WkpFromDayOfWeek = Me.FromDayOfWeekDropDown.SelectedIndex
            If daysMode Then
                weeklyProfileRow.SetWkpFromTimeOfDayNull()
            Else
                weeklyProfileRow.WkpFromTimeOfDay = New Date(1900, 1, 1) + Me.FromTimeOfDayTimeControl.Time
            End If

            weeklyProfileRow.WkpToDayOfWeek = Me.ToDayOfWeekDropDown.SelectedIndex
            If daysMode Then
                weeklyProfileRow.SetWkpToTimeOfDayNull()
            Else
                weeklyProfileRow.WkpToTimeOfDay = New Date(1900, 1, 1) + Me.ToTimeOfDayTimeControl.Time
            End If
        End Sub
    End Class

    Private _initException As Exception
    Private _weeklyProfileTableRows(MaxWeeklyProfileRows) As WeeklyProfileTableRow

    Public Sub InitWeeklyProfile()
        daysModeRadioButton.Attributes.Add("onclick", "weeklyProfile_modeChange (this)")
        mixedModeRadioButton.Attributes.Add("onclick", "weeklyProfile_modeChange (this)")

        _weeklyProfileTableRows = New WeeklyProfileTableRow() { _
             New WeeklyProfileTableRow(0, idHiddenField0, fromDayOfWeekDropDown0, fromTimeOfDayTimeControl0, toDayOfWeekDropDown0, toTimeOfDayTimeControl0), _
             New WeeklyProfileTableRow(1, idHiddenField1, fromDayOfWeekDropDown1, fromTimeOfDayTimeControl1, toDayOfWeekDropDown1, toTimeOfDayTimeControl1), _
             New WeeklyProfileTableRow(2, idHiddenField2, fromDayOfWeekDropDown2, fromTimeOfDayTimeControl2, toDayOfWeekDropDown2, toTimeOfDayTimeControl2), _
             New WeeklyProfileTableRow(3, idHiddenField3, fromDayOfWeekDropDown3, fromTimeOfDayTimeControl3, toDayOfWeekDropDown3, toTimeOfDayTimeControl3), _
             New WeeklyProfileTableRow(4, idHiddenField4, fromDayOfWeekDropDown4, fromTimeOfDayTimeControl4, toDayOfWeekDropDown4, toTimeOfDayTimeControl4), _
             New WeeklyProfileTableRow(5, idHiddenField5, fromDayOfWeekDropDown5, fromTimeOfDayTimeControl5, toDayOfWeekDropDown5, toTimeOfDayTimeControl5), _
             New WeeklyProfileTableRow(6, idHiddenField6, fromDayOfWeekDropDown6, fromTimeOfDayTimeControl6, toDayOfWeekDropDown6, toTimeOfDayTimeControl6)}
    End Sub

    Public Overrides Sub InitPackageUserControl(ByVal packageDataSet As Aurora.Package.Data.PackageDataSet, ByVal packageRow As Aurora.Package.Data.PackageDataSet.PackageRow)
        MyBase.InitPackageUserControl(packageDataSet, packageRow)

        Try
            InitWeeklyProfile()
        Catch ex As Exception
            _initException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Page.IsPostBack _
         OrElse _initException IsNot Nothing _
         OrElse Me.PackageRow Is Nothing Then Return

        Dim weeklyProfileRows As WeeklyProfileRow() = Me.PackageRow.GetWeeklyProfileRows()

        daysModeRadioButton.Checked = True
        For Each weeklyProfileRow As WeeklyProfileRow In weeklyProfileRows
            If Not weeklyProfileRow.IsWkpProfileTypeNull AndAlso weeklyProfileRow.WkpProfileType = "Mixed" Then mixedModeRadioButton.Checked = True
            Exit For
        Next
        daysModeRadioButton.Enabled = CanMaintain
        mixedModeRadioButton.Enabled = CanMaintain

        Dim index As Integer = 0
        For Each weeklyProfileTableRow As WeeklyProfileTableRow In _weeklyProfileTableRows
            If index < weeklyProfileRows.Length Then
                weeklyProfileTableRow.ReadFromWeeklyProfileRow(weeklyProfileRows(index), CanMaintain)
            Else
                weeklyProfileTableRow.ReadFromWeeklyProfileRow(Nothing, CanMaintain)
            End If
            index += 1
        Next

        updateButton.Visible = CanMaintain
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        Dim daysMode As Boolean = daysModeRadioButton.Checked
        Try
            ' Validate the individual rows
            For Each weeklyProfileTableRow As WeeklyProfileTableRow In _weeklyProfileTableRows
                Dim message As String = Nothing
                If Not weeklyProfileTableRow.IsValid(daysMode, message) Then
                    Throw New ValidationException(message, weeklyProfileTableRow)
                End If
            Next

            ' Validate for time intersections
            For Each weeklyProfileTableRow0 As WeeklyProfileTableRow In _weeklyProfileTableRows
                For Each weeklyProfileTableRow1 As WeeklyProfileTableRow In _weeklyProfileTableRows
                    If weeklyProfileTableRow0 IsNot weeklyProfileTableRow1 _
                     AndAlso Not weeklyProfileTableRow0.IsBlank _
                     AndAlso weeklyProfileTableRow0.IsValid(daysMode) _
                     AndAlso Not weeklyProfileTableRow1.IsBlank _
                     AndAlso weeklyProfileTableRow1.IsValid(daysMode) Then _
                        If Aurora.Common.Utility.DoesModulaRangeIntersect( _
                         weeklyProfileTableRow0.GetFromTimeSpan(daysMode).Ticks, _
                         weeklyProfileTableRow0.GetToTimeSpan(daysMode).Ticks, _
                         weeklyProfileTableRow1.GetFromTimeSpan(daysMode).Ticks, _
                         weeklyProfileTableRow1.GetToTimeSpan(daysMode).Ticks, _
                            TimeSpan.TicksPerDay * 7) Then Throw New ValidationException("Weekly profile rows time periods may not overlap")
                Next
            Next
        Catch ex As ValidationException
            Me.CurrentPage.AddErrorMessage(ex.Message)
            Return
        End Try

        Try
            ' Delete if not found or update any WeeklyProfileRows
            For Each weeklyProfileRow As WeeklyProfileRow In Me.PackageRow.GetWeeklyProfileRows()
                Dim found As Boolean = False
                For Each weeklyProfileTableRow As WeeklyProfileTableRow In _weeklyProfileTableRows
                    If weeklyProfileTableRow.IdHiddenField.Value = weeklyProfileRow.WkpId And Not weeklyProfileTableRow.IsBlank Then
                        weeklyProfileTableRow.WriteToWeeklyProfileRow(daysMode, weeklyProfileRow)

                        Aurora.Common.Data.ExecuteDataRow(weeklyProfileRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)

                        found = True
                        Exit For
                    End If
                Next
                If Not found Then
                    weeklyProfileRow.Delete()
                    Aurora.Common.Data.ExecuteDataRow(weeklyProfileRow)
                End If
            Next

            ' create new WeeklyProfileRows
            For Each weeklyProfileTableRow As WeeklyProfileTableRow In _weeklyProfileTableRows
                If Not String.IsNullOrEmpty(weeklyProfileTableRow.IdHiddenField.Value) _
                 OrElse weeklyProfileTableRow.IsBlank Then Continue For

                Dim weeklyProfileRow As WeeklyProfileRow = Me.PackageDataSet.WeeklyProfile.NewWeeklyProfileRow()
                weeklyProfileRow.WkpId = DataRepository.GetNewId()
                weeklyProfileRow.WkpPkgId = Me.PackageRow.PkgId
                weeklyProfileTableRow.WriteToWeeklyProfileRow(daysMode, weeklyProfileRow)
                Me.PackageDataSet.WeeklyProfile.AddWeeklyProfileRow(weeklyProfileRow)

                Aurora.Common.Data.ExecuteDataRow(weeklyProfileRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)

                weeklyProfileTableRow.IdHiddenField.Value = weeklyProfileRow.WkpId
            Next

        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(ex.Message)
        End Try
    End Sub

End Class
