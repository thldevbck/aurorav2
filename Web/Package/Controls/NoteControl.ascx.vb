Imports System.Collections.Generic

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


<ToolboxData("<{0}:NoteControl runat=server></{0}:NoteControl>")> _
Partial Class Package_NoteControl
    Inherits PackageUserControl

    Private _loadException As Exception
    Private _checkBoxesByNteId As New Dictionary(Of String, CheckBox)
    Private _notesByLinkButtonId As New Dictionary(Of String, NoteRow)

    Public Overrides Sub InitPackageUserControl(ByVal packageDataSet As Aurora.Package.Data.PackageDataSet, ByVal packageRow As Aurora.Package.Data.PackageDataSet.PackageRow)
        MyBase.InitPackageUserControl(packageDataSet, packageRow)

        Try
            notePopupAudienceDropDown.Items.Clear()
            For Each codeRow As CodeRow In Me.PackageDataSet.Code
                If codeRow.CodCdtNum = codeRow.CodeType_Audience_Type Then _
                    notePopupAudienceDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            Next
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Public Sub BuildNotes()
        _checkBoxesByNteId.Clear()
        _notesByLinkButtonId.Clear()
        While notesTable.Rows.Count > 1
            notesTable.Rows.RemoveAt(1)
        End While

        If Me.PackageRow Is Nothing Then Return

        Dim rowIndex As Integer = 0
        For Each noteRow As NoteRow In Me.PackageRow.GetNoteRows()
            Dim tableRow As New TableRow
            tableRow.CssClass = IIf(rowIndex Mod 2 = 0, "row", "rowalt") : rowIndex += 1
            tableRow.BackColor = noteRow.StatusColor()
            notesTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            selectCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & noteRow.NteId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByNteId.Add(noteRow.NteId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim typeCell As New TableCell
            typeCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(typeCell)

            If CanMaintain Then
                Dim linkButton As New LinkButton
                linkButton.ID = "edit" & noteRow.NteId & "Button"
                linkButton.Text = noteRow.Type
                _notesByLinkButtonId.Add(linkButton.ID, noteRow)
                AddHandler linkButton.Click, AddressOf editButton_Click
                typeCell.Controls.Add(linkButton)
            Else
                typeCell.Controls.Add(New LiteralControl(Server.HtmlEncode(noteRow.Type)))
            End If

            Dim noteCell As New TableCell
            noteCell.Text = Server.HtmlEncode(noteRow.Description).Replace(vbCr, "").Replace(vbLf, "<br/>")
            noteCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(noteCell)

            Dim detailsCell As New TableCell
            detailsCell.Text = "" _
                & "<i>Audience:</i> " & Server.HtmlEncode(noteRow.Audience) & "<br/>" _
                & "<i>Priority:</i> " & CStr(noteRow.Priority) & ", <i>Active:</i> " & IIf(noteRow.IsActive, "Yes", "No")
            detailsCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(detailsCell)

            Dim userCell As New TableCell
            userCell.Text = "<i>User:</i> " & Server.HtmlEncode(noteRow.UserDescription)
            If noteRow.ModifiedDate <> Date.MinValue Then userCell.Text += "<br/><i>Modified:</i> " & noteRow.ModifiedDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
            userCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(userCell)
        Next

        addButton.Visible = CanMaintain
        deleteButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BuildNotes()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        notePopupCancelButton.Attributes.Add("onclick", "$find('notePopupBehavior').hide(); return false;")
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain Then Return

        notePopupIdHidden.Value = ""
        notePopupAudienceDropDown.SelectedIndex = 0
        notePopupPriorityDropDown.SelectedIndex = 1
        notePopupActiveCheckBox.Checked = True
        notePopupNoteTextBox.Text = ""

        notePopupAddButton.Visible = True
        notePopupDeleteButton.Visible = False
        notePopupUpdateButton.Visible = False
        notePopupTitleLabel.Text = "Add Note"
        notePopup.Show()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If Not CanMaintain Then Return

        For Each noteRow As NoteRow In Me.PackageRow.GetNoteRows()
            Dim checkBox As CheckBox = _checkBoxesByNteId(noteRow.NteId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            noteRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(noteRow)
            Me.PackageDataSet.Note.RemoveNoteRow(noteRow)
        Next

        BuildNotes()
    End Sub

    Protected Sub editButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim noteRow As NoteRow = _notesByLinkButtonId(CType(sender, LinkButton).ID)
        If noteRow Is Nothing Then Return

        notePopupIdHidden.Value = noteRow.NteId
        Try
            notePopupAudienceDropDown.SelectedValue = noteRow.CodAudTypId
        Catch
        End Try
        Try
            notePopupPriorityDropDown.SelectedValue = noteRow.Priority.ToString()
        Catch
        End Try
        notePopupActiveCheckBox.Checked = noteRow.IsActive
        notePopupNoteTextBox.Text = noteRow.Description

        notePopupAddButton.Visible = False
        notePopupDeleteButton.Visible = True
        notePopupUpdateButton.Visible = True
        notePopupTitleLabel.Text = "Edit Note"
        notePopup.Show()
    End Sub

    Protected Sub notePopupAddButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notePopupAddButton.Click
        Dim noteRow As NoteRow = Me.PackageDataSet.Note.NewNoteRow()
        noteRow.NteId = DataRepository.GetNewId()
        noteRow.NtePkgId = Me.PackageRow.PkgId
        noteRow.NteCodTypId = Me.PackageDataSet.Code.FindByCodCdtNumCodCode(CodeRow.CodeType_Note_Type, "Package").CodId
        noteRow.CodAudTypId = notePopupAudienceDropDown.SelectedValue
        noteRow.Priority = notePopupPriorityDropDown.SelectedValue
        noteRow.IsActive = notePopupActiveCheckBox.Checked
        If notePopupNoteTextBox.Text.Length > 1024 Then
            noteRow.Description = notePopupNoteTextBox.Text.Substring(0, 1024)
        Else
            noteRow.Description = notePopupNoteTextBox.Text
        End If
        Me.PackageDataSet.Note.AddNoteRow(noteRow)

        Aurora.Common.Data.ExecuteDataRow(noteRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)

        BuildNotes()
    End Sub

    Protected Sub notePopupUpdateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notePopupUpdateButton.Click
        Dim noteRow As NoteRow = Me.PackageDataSet.Note.FindByNteId(notePopupIdHidden.Value)
        If noteRow Is Nothing Then Return

        noteRow.CodAudTypId = notePopupAudienceDropDown.SelectedValue
        noteRow.Priority = notePopupPriorityDropDown.SelectedValue
        noteRow.IsActive = notePopupActiveCheckBox.Checked
        If notePopupNoteTextBox.Text.Length > 1024 Then
            noteRow.Description = notePopupNoteTextBox.Text.Substring(0, 1024)
        Else
            noteRow.Description = notePopupNoteTextBox.Text
        End If

        Aurora.Common.Data.ExecuteDataRow(noteRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)

        BuildNotes()
    End Sub

    Protected Sub notePopupCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notePopupCancelButton.Click
        notePopup.Hide()
    End Sub

    Protected Sub notePopupDeleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles notePopupDeleteButton.Click
        Dim noteRow As NoteRow = Me.PackageDataSet.Note.FindByNteId(notePopupIdHidden.Value)
        If noteRow Is Nothing Then Return

        noteRow.Delete()
        Aurora.Common.Data.ExecuteDataRow(noteRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
        Me.PackageDataSet.Note.RemoveNoteRow(noteRow)

        BuildNotes()
    End Sub
End Class
