Imports System.Collections.Generic

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


<ToolboxData("<{0}:MarketCodesControl runat=server></{0}:MarketCodesControl>")> _
Partial Class Package_MarketControl
    Inherits PackageUserControl

    Private _initException As Exception

    Public Sub InitCodes()
        Dim rowIndex As Integer = 0
        marketCodesCheckBoxList.Items.Clear()
        For Each codeRow As CodeRow In Me.PackageDataSet.Code
            If codeRow.CodCdtNum <> codeRow.CodeType_Marketing_Region Then Continue For
            If codeRow.IsCodDescNull Then Continue For

            marketCodesCheckBoxList.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
        Next
    End Sub

    Public Overrides Sub InitPackageUserControl(ByVal packageDataSet As Aurora.Package.Data.PackageDataSet, ByVal packageRow As Aurora.Package.Data.PackageDataSet.PackageRow)
        MyBase.InitPackageUserControl(packageDataSet, packageRow)

        Try
            InitCodes()
        Catch ex As Exception
            _initException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Public Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return
        If Me.PackageRow Is Nothing Then Return

        For Each codeRow As CodeRow In Me.PackageDataSet.Code
            If codeRow.CodCdtNum <> codeRow.CodeType_Marketing_Region Then Continue For
            If codeRow.IsCodDescNull Then Continue For

            Dim packageMarketingRegionRow As PackageMarketingRegionRow = Me.PackageDataSet.PackageMarketingRegion.FindByPmrPkgIdPmrCodMktRgnId(Me.PackageRow.PkgId, codeRow.CodId)

            marketCodesCheckBoxList.Items.FindByValue(codeRow.CodId).Selected = packageMarketingRegionRow IsNot Nothing
        Next
        marketCodesCheckBoxList.Enabled = CanMaintain

        updateButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        selectAllHyperLink.Attributes.Add("onclick", "toggleCheckboxesList ('" & marketCodesCheckBoxList.ClientID & "', true); return false;")
        selectNoneHyperLink.Attributes.Add("onclick", "toggleCheckboxesList ('" & marketCodesCheckBoxList.ClientID & "', false); return false;")

        If Page.IsPostBack OrElse _initException IsNot Nothing Then Return

        BuildForm(False)
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        For Each codeRow As CodeRow In Me.PackageDataSet.Code
            If codeRow.CodCdtNum <> codeRow.CodeType_Marketing_Region Then Continue For
            If codeRow.IsCodDescNull Then Continue For

            Dim packageMarketingRegionRow As PackageMarketingRegionRow = Me.PackageDataSet.PackageMarketingRegion.FindByPmrPkgIdPmrCodMktRgnId(Me.PackageRow.PkgId, codeRow.CodId)

            Dim checked As Boolean = marketCodesCheckBoxList.Items.FindByValue(codeRow.CodId).Selected

            If packageMarketingRegionRow Is Nothing AndAlso checked Then
                packageMarketingRegionRow = Me.PackageDataSet.PackageMarketingRegion.NewPackageMarketingRegionRow()
                packageMarketingRegionRow.PmrPkgId = Me.PackageRow.PkgId
                packageMarketingRegionRow.PmrCodMktRgnId = codeRow.CodId
                Me.PackageDataSet.PackageMarketingRegion.AddPackageMarketingRegionRow(packageMarketingRegionRow)
            ElseIf packageMarketingRegionRow IsNot Nothing AndAlso Not checked Then
                packageMarketingRegionRow.Delete()
            End If

            If packageMarketingRegionRow IsNot Nothing AndAlso packageMarketingRegionRow.RowState <> Data.DataRowState.Unchanged Then
                Aurora.Common.Data.ExecuteDataRow(packageMarketingRegionRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
            End If

            If packageMarketingRegionRow IsNot Nothing AndAlso packageMarketingRegionRow.RowState = Data.DataRowState.Deleted Then
                Me.PackageDataSet.PackageMarketingRegion.Rows.Remove(packageMarketingRegionRow)
            End If
        Next
    End Sub
End Class
