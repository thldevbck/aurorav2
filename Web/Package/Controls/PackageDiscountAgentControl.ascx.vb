﻿''rev:mia Feb.28 2013 - Added a complete and working version
''                    - today in history: Last day of Pope Benedict                        

Imports System.Data
Imports Aurora.Package.Data

Partial Class Package_Controls_PackageDiscountAgentControl
    Inherits AuroraUserControl
    Private _MessageInfo As String
    Public Property MessageInfo As String
        Get
            Return _MessageInfo
        End Get
        Set(value As String)
            _MessageInfo = value
        End Set
    End Property

    Private _RequeryURL As String
    Public Property RequeryURL As String
        Get
            Return _RequeryURL
        End Get
        Set(value As String)
            _RequeryURL = value
        End Set
    End Property

    Private _SuccessfulMessage As String
    Public Property SuccessfulMessage As String
        Get
            Return _SuccessfulMessage
        End Get
        Set(value As String)
            _SuccessfulMessage = value
        End Set
    End Property

    Private _FailedMessage As String
    Public Property FailedMessage As String
        Get
            Return _FailedMessage
        End Get
        Set(value As String)
            _FailedMessage = value
        End Set
    End Property

    Private _DisproIdControl As String
    Public Property DisproIdControl As String
        Get
            Return _DisproIdControl
        End Get
        Set(value As String)
            _DisproIdControl = value
        End Set
    End Property

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.DiscountProfileSearch)
        End Get
    End Property

    Private _AgentsInfo() As DataTable
    Public Property AgentsInfo() As DataTable()
        Get
            Return _AgentsInfo
        End Get
        Set(value() As DataTable)
            _AgentsInfo = value
        End Set
    End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (AgentsInfo Is Nothing) Then Return
        If AgentsInfo.Length - 1 = -1 Then Return
        Try
            If Not Page.IsPostBack Then InitAgentDiscountProfile()

            Dim buttonclickAdd As String = "document.getElementById('" & addtoButton.ClientID.ToString() & "').click();"
            availableAgentListbox.Attributes.Add("OnDblClick", buttonclickAdd)

            Dim buttonclickRemoved As String = "document.getElementById('" & removefromButton.ClientID.ToString() & "').click();"
            selectedAgentListBox.Attributes.Add("OnDblClick", buttonclickRemoved)

        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try

        removefromButton.Enabled = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
        addtoButton.Enabled = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
        saveAgentProfileButton.Enabled = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
        saveAgentProfileButton.Visible = Not String.IsNullOrEmpty(_DisproIdControl) And CanMaintain = True
    End Sub

    Sub InitAgentDiscountProfile()
        If (String.IsNullOrEmpty(_DisproIdControl)) Then Exit Sub

        Dim shortname As String = ""
        Dim longname As String = ""
        Dim id As String = ""
        Dim maxlength As Integer = 9
        Dim formatting As String = "{0, -" & maxlength & "} - {1}"

        availableAgentListbox.Items.Clear()
        selectedAgentListBox.Items.Clear()

        If (AgentsInfo(0).Rows.Count - 1 <> -1) Then
            'availableAgentListbox.DataSource = AgentsInfo(0).DefaultView
            'availableAgentListbox.DataTextField = "AgentName"
            'availableAgentListbox.DataValueField = "AgnId"
            'availableAgentListbox.DataBind()

            For Each row As DataRow In AgentsInfo(0).Rows
                shortname = row("AgentName").ToString.Split("-")(0).Trim
                longname = row("AgentName").ToString.Split("-")(1).Trim.ToLower
                id = row("AgnId").ToString

                Dim itemList As New ListItem
                itemList.Text = String.Format(formatting, shortname.Trim, longname.Trim)
                itemList.Text = itemList.Text.Replace(" ", HttpUtility.HtmlDecode("&nbsp;"))
                itemList.Value = id


                availableAgentListbox.Items.Add(itemList)

            Next

        End If

        If (AgentsInfo(1).Rows.Count - 1 <> -1) Then
            'selectedAgentListBox.DataSource = AgentsInfo(1).DefaultView
            'selectedAgentListBox.DataTextField = "AgentName"
            'selectedAgentListBox.DataValueField = "AgnId"
            'selectedAgentListBox.DataBind()


            For Each row As DataRow In AgentsInfo(1).Rows
                shortname = row("AgentName").ToString.Split("-")(0).Trim
                longname = row("AgentName").ToString.Split("-")(1).Trim.ToLower
                id = row("AgnId").ToString

                Dim itemList As New ListItem
                itemList.Text = String.Format(formatting, shortname.Trim, longname.Trim)
                itemList.Text = itemList.Text.Replace(" ", HttpUtility.HtmlDecode("&nbsp;"))
                itemList.Value = id

                agentcheckboxexclude.Checked = Convert.ToBoolean(row("DisProAExclude"))
                selectedAgentListBox.Items.Add(itemList)

            Next
        End If
    End Sub


    Protected Sub addtoButton_Click(sender As Object, e As System.EventArgs) Handles addtoButton.Click
        For Each item As ListItem In availableAgentListbox.Items
            If (item.Selected = True) Then
                Dim itemcandidate As ListItem = selectedAgentListBox.Items.FindByValue(item.Value)
                If (itemcandidate Is Nothing) Then
                    selectedAgentListBox.Items.Add(item)

                End If
            End If
        Next

        For Each item As ListItem In selectedAgentListBox.Items
            Dim itemcandidate As ListItem = availableAgentListbox.Items.FindByValue(item.Value)
            If (Not itemcandidate Is Nothing) Then
                availableAgentListbox.Items.Remove(item)
            End If

        Next
    End Sub

    Protected Sub removefromButton_Click(sender As Object, e As System.EventArgs) Handles removefromButton.Click
        For Each item As ListItem In selectedAgentListBox.Items
            If (item.Selected = True) Then
                Dim itemcandidate As ListItem = availableAgentListbox.Items.FindByValue(item.Value)
                If (itemcandidate Is Nothing) Then
                    availableAgentListbox.Items.Add(item)
                End If
            End If
        Next

        For Each item As ListItem In availableAgentListbox.Items
            Dim itemcandidate As ListItem = selectedAgentListBox.Items.FindByValue(item.Value)
            If (Not itemcandidate Is Nothing) Then
                selectedAgentListBox.Items.Remove(item)
            End If

        Next
    End Sub

    Protected Sub saveagentProfileButton_Click(sender As Object, e As System.EventArgs) Handles saveAgentProfileButton.Click
        If Not CanMaintain Then Return
        Dim packageProfile As New EditPackageProfile()
        Dim result As Integer = 0
        Try
            Dim sb As New StringBuilder
            For Each item As ListItem In selectedAgentListBox.Items
                sb.AppendFormat("{0},", item.Value)
                result = packageProfile.PackageInsertExcludeAgentProfile(_DisproIdControl, String.Empty, item.Value, agentcheckboxexclude.Checked, CurrentPage.UserCode)
            Next

            Dim resultDeletion As Integer
            If (sb.ToString.EndsWith(",") = True) Then
                resultDeletion = packageProfile.PackageDeleteAgentProfile(_DisproIdControl, sb.ToString.Substring(0, sb.ToString.LastIndexOf(",")))
            Else
                result = packageProfile.PackageDeleteAgentProfile(_DisproIdControl, "")
            End If

        Catch ex As Exception
            result = -1
        End Try

        If (result > 0) Then
            MessageInfo = SuccessfulMessage
            Response.Redirect(RequeryURL & "&saved=ok&activeTab=2")
        End If
        If (result < 0) Then
            Me.CurrentPage.AddWarningMessage(FailedMessage)
        End If
    End Sub

   
End Class
