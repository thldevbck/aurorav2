Imports System.Collections.Generic

Imports Aurora.Package.Data
Imports Aurora.Package.Data.PackageDataSet


<ToolboxData("<{0}:AgentControl runat=server></{0}:AgentControl>")> _
Partial Class Package_AgentControl
    Inherits PackageUserControl

    Public Const MaxAgentPerPage As Integer = 100

    Private _loadException As Exception
    Private _checkBoxesByApkId As New Dictionary(Of String, CheckBox)

    Public Sub InitAgent()
        If Me.PackageRow Is Nothing Then Return

        Dim selectedValue As String = agentTypeRadioButtonList.SelectedValue
        agentTypeRadioButtonList.Items.Clear()
        agentTypeRadioButtonList.Items.Add(New ListItem("None", ""))
        For Each codeRow As CodeRow In Me.PackageDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Agent_Type Then _
                agentTypeRadioButtonList.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
        Next
        Try
            agentTypeRadioButtonList.SelectedValue = selectedValue
        Catch
        End Try

        Dim agentPackageRows As AgentPackageRow() = Me.PackageRow.GetAgentPackageRows()

        If agentPackageRows.Length <= 0 Then
            agentsLabel.Text = "No agents found"
        ElseIf agentPackageRows.Length = 1 Then
            agentsLabel.Text = agentPackageRows.Length & " agent found"
        Else
            agentsLabel.Text = agentPackageRows.Length & " agents found"
        End If

        If agentPackageRows.Length < MaxAgentPerPage Then
            agentsDropDown.Style.Add(HtmlTextWriterStyle.Display, "none")
        Else
            agentsDropDown.Style.Add(HtmlTextWriterStyle.Display, "")
        End If

        Dim selectedIndex As Integer = agentsDropDown.SelectedIndex
        Dim index0 As Integer
        Dim index1 As Integer

        agentsDropDown.Items.Clear()
        If agentPackageRows.Length > 0 Then
            For i As Integer = 0 To agentPackageRows.Length \ MaxAgentPerPage
                index0 = Math.Min(i * MaxAgentPerPage, agentPackageRows.Length - 1)

                Dim agentPackageRow0 As AgentPackageRow = agentPackageRows(index0)
                index1 = Math.Min(i * MaxAgentPerPage + MaxAgentPerPage - 1, agentPackageRows.Length - 1)
                'If index1 >= agentPackageRows.Length Then index1 = agentPackageRows.Length - 1
                Dim agentPackageRow1 As AgentPackageRow = agentPackageRows(index1)
                If index0 = index1 And index0 < i * MaxAgentPerPage And index1 < i * MaxAgentPerPage Then
                    ' skip
                Else
                    agentsDropDown.Items.Add(New ListItem(agentPackageRow0.AgentRow.Code + " - " + agentPackageRow1.AgentRow.Code, i.ToString()))
                End If
            Next
        End If
        Try
            If selectedIndex < 0 Then
                selectedIndex = 0
            ElseIf selectedIndex >= agentsDropDown.Items.Count Then
                selectedIndex = agentsDropDown.Items.Count - 1
            End If
            agentsDropDown.SelectedIndex = selectedIndex
        Catch
        End Try

        While agentTable.Rows.Count > 1
            agentTable.Rows.RemoveAt(1)
        End While

        index0 = selectedIndex * MaxAgentPerPage
        index1 = selectedIndex * MaxAgentPerPage + MaxAgentPerPage - 1
        If index1 >= agentPackageRows.Length Then index1 = agentPackageRows.Length - 1

        For rowIndex As Integer = index0 To index1
            Dim agentPackageRow As AgentPackageRow = agentPackageRows(rowIndex)

            Dim tableRow As New TableRow
            tableRow.BackColor = agentPackageRow.AgentRow.StatusColor()
            agentTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)

            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "remove" & agentPackageRow.ApkId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByApkId.Add(agentPackageRow.ApkId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim codeCell As New TableCell

            If Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentEnquiry) Then
                Dim hyperLink As New HyperLink
                hyperLink.Text = Server.HtmlEncode(agentPackageRow.AgentRow.Code)
                hyperLink.NavigateUrl = AgentUserControl.MakeAgentUrl(Me.Page, agnId:=agentPackageRow.ApkAgnId)
                codeCell.Controls.Add(hyperLink)
                tableRow.Controls.Add(codeCell)
            Else
                selectCell.Controls.Add(New LiteralControl(Server.HtmlEncode(agentPackageRow.AgentRow.Code)))
            End If

            Dim nameCell As New TableCell
            nameCell = New TableCell
            nameCell.Text = Server.HtmlEncode(agentPackageRow.AgentRow.Name)
            tableRow.Controls.Add(nameCell)
        Next

        addButton.Visible = CanMaintain
        removeButton.Visible = CanMaintain
        updateButton.Visible = CanMaintain
    End Sub

    Public Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return
        If Me.PackageRow Is Nothing Then Return

        agentTypeRadioButtonList.SelectedIndex = 0
        Try
            If Not Me.PackageRow.IsPkgCodTypIdNull Then
                agentTypeRadioButtonList.SelectedValue = Me.PackageRow.PkgCodTypId
            End If
        Catch ex As Exception
        End Try
        agentTypeRadioButtonList.Enabled = CanMaintain

        If Me.PackageRow.AgentGroupRow IsNot Nothing Then
            agentGroupPicker.DataId = Me.PackageRow.AgentGroupRow.AgpId
            agentGroupPicker.Text = Me.PackageRow.AgentGroupRow.Description
        Else
            agentGroupPicker.DataId = ""
            agentGroupPicker.Text = ""
        End If
        agentGroupPicker.ReadOnly = Not CanMaintain

        ''rev:mia jan 28
        '' agentDiscountOverride.Text = Me.PackageRow.OvrAgnDiscount.ToString("f2")
        If Not String.IsNullOrEmpty(Me.PackageRow.OvrAgnDiscount) AndAlso Not Me.PackageRow.OvrAgnDiscount.ToString.Equals("9999999999".ToString) Then
            agentDiscountOverride.Text = Me.PackageRow.OvrAgnDiscount.ToString("f2")
        ElseIf Me.PackageRow.OvrAgnDiscount.ToString.Equals("9999999999".ToString) Then
            agentDiscountOverride.Text = ""
        End If

        agentDiscountOverride.ReadOnly = Not CanMaintain
        ''rev:mia jan 28
        If Not String.IsNullOrEmpty(Me.PackageRow.AddAgnDiscount) AndAlso Not Me.PackageRow.AddAgnDiscount.ToString.Equals("9999999999".ToString) Then
            agentDiscountExtra.Text = Me.PackageRow.AddAgnDiscount.ToString("f2")
        ElseIf Me.PackageRow.AddAgnDiscount.ToString.Equals("9999999999".ToString) Then
            agentDiscountExtra.Text = ""
        End If


        agentDiscountExtra.ReadOnly = Not CanMaintain

        If Me.PackageRow.DepositAmount <> 0 OrElse Me.PackageRow.DepositPercentage = 0 Then
            depositRequestTextBox.Text = Me.PackageRow.DepositAmount.ToString("f2")
            depositRequestRadioButtonList.SelectedIndex = 0
        Else
            depositRequestTextBox.Text = Me.PackageRow.DepositPercentage.ToString("f2")
            depositRequestRadioButtonList.SelectedIndex = 1
        End If
        depositRequestTextBox.ReadOnly = Not CanMaintain
        depositRequestRadioButtonList.Enabled = CanMaintain

        lumpSumDescTextBox.Text = Me.PackageRow.LumpSumDesc()
        lumpSumDescTextBox.ReadOnly = Not CanMaintain

        For Each listItem As ListItem In flagsCheckBoxList.Items
            listItem.Selected = Me.PackageRow.Table.Columns.Contains(listItem.Value) _
             AndAlso Not Me.PackageRow.IsNull(listItem.Value) _
             AndAlso CType(Me.PackageRow(listItem.Value), Boolean)
        Next
        flagsCheckBoxList.Enabled = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            InitAgent()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If Page.IsPostBack Then Return

        agentPopupCancelButton.Attributes.Add("onclick", "$find('agentPopupBehavior').hide(); return false;")
        BuildForm(False)
    End Sub

    Private Sub ValidateAgent()
        If Not agentDiscountOverride.IsTextValid Then
            Throw New Aurora.Common.ValidationException(agentDiscountOverride.ErrorMessage)
        End If

        If Not agentDiscountExtra.IsTextValid Then
            Throw New Aurora.Common.ValidationException(agentDiscountExtra.ErrorMessage)
        End If

        If Not depositRequestTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException(depositRequestTextBox.ErrorMessage)
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateAgent()
        Catch ex As Aurora.Common.ValidationException
            Me.CurrentPage.AddErrorMessage(ex.Message)
            Return
        End Try

        If agentTypeRadioButtonList.SelectedIndex <> 0 Then
            ' Me.PackageRow.PkgCodTypId = agentTypeRadioButtonList.SelectedValue
            Me.PackageRow.PkgCodAgnTypId = agentTypeRadioButtonList.SelectedValue
        Else
            'Me.PackageRow.SetPkgCodTypIdNull()
            Me.PackageRow.SetPkgCodAgnTypIdNull()
        End If

        'If agentGroupPicker.DataId IsNot Nothing Then
        If Not String.IsNullOrEmpty(agentGroupPicker.DataId) Then
            Me.PackageRow.PkgAgpId = agentGroupPicker.DataId
        Else
            Me.PackageRow.SetPkgAgpIdNull()
        End If

        'If Aurora.Common.Utility.ParseDecimal(agentDiscountOverride.Text, 0) > 0 Then
        '    Me.PackageRow.PkgOvrAgnDiscount = Aurora.Common.Utility.ParseDecimal(agentDiscountOverride.Text, 0)
        'Else
        '    Me.PackageRow.SetPkgOvrAgnDiscountNull()
        'End If

        If Not String.IsNullOrEmpty(agentDiscountOverride.Text) Then
            Me.PackageRow.PkgOvrAgnDiscount = Aurora.Common.Utility.ParseDecimal(agentDiscountOverride.Text, 0)
        Else
            Me.PackageRow.SetPkgOvrAgnDiscountNull()
        End If

        'If Aurora.Common.Utility.ParseDecimal(agentDiscountExtra.Text, 0) > 0 Then
        '    Me.PackageRow.PkgAddAgnDiscount = Aurora.Common.Utility.ParseDecimal(agentDiscountExtra.Text, 0)
        'Else
        '    Me.PackageRow.SetPkgAddAgnDiscountNull()
        'End If

        If Not String.IsNullOrEmpty(agentDiscountExtra.Text) Then
            Me.PackageRow.PkgAddAgnDiscount = Aurora.Common.Utility.ParseDecimal(agentDiscountExtra.Text, 0)
        Else
            Me.PackageRow.SetPkgAddAgnDiscountNull()
        End If


        If depositRequestRadioButtonList.SelectedIndex = 0 Then
            If Aurora.Common.Utility.ParseDecimal(depositRequestTextBox.Text, 0) <> 0 Then
                Me.PackageRow.PkgDepositAmt = Aurora.Common.Utility.ParseDecimal(depositRequestTextBox.Text, 0)
            Else
                ''rev:mia jan-07-09
                ''Me.PackageRow.SetPkgDepositAmtNull()
                Me.PackageRow.PkgDepositAmt = 0
                depositRequestTextBox.Text = "0.00".ToString
            End If
            ''Me.PackageRow.SetPkgDepositPercentageNull()
            Me.PackageRow.PkgDepositPercentage = 0
        Else
            If Aurora.Common.Utility.ParseDecimal(depositRequestTextBox.Text, 0) <> 0 Then
                Me.PackageRow.PkgDepositPercentage = Aurora.Common.Utility.ParseDecimal(depositRequestTextBox.Text, 0)
            Else
                ''Me.PackageRow.SetPkgDepositPercentageNull()
                Me.PackageRow.PkgDepositPercentage = 0
                depositRequestTextBox.Text = "0.00".ToString
            End If
            ''Me.PackageRow.SetPkgDepositAmtNull()
            Me.PackageRow.PkgDepositAmt = 0
        End If

        If Not String.IsNullOrEmpty(lumpSumDescTextBox.Text.Trim()) Then
            Me.PackageRow.LumpSumDesc() = lumpSumDescTextBox.Text
        Else
            Me.PackageRow.SetPkgLumpSumDescNull()
        End If

        For Each listItem As ListItem In flagsCheckBoxList.Items
            If Me.PackageRow.Table.Columns.Contains(listItem.Value) Then
                Me.PackageRow(listItem.Value) = listItem.Selected
            End If
        Next
        '' RKS: 19-DEC-2008 to fix NULL
        '' Calling new funtion created by Manny to handle this
        ''Aurora.Common.Data.ExecuteDataRow(Me.PackageRow)
        Aurora.Common.Data.ExecuteDataRowUpdateVehicleDependentRate(Me.PackageRow)

        Me.CurrentPage.AddInformationMessage("Agent information updated")
    End Sub

    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click
        If Not CanMaintain Then Return

        For Each agentPackageRow As AgentPackageRow In Me.PackageRow.GetAgentPackageRows()
            Dim checkBox As CheckBox = Nothing
            If Not _checkBoxesByApkId.ContainsKey(agentPackageRow.ApkId) OrElse Not _checkBoxesByApkId(agentPackageRow.ApkId).Checked Then Continue For

            agentPackageRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(agentPackageRow)
        Next

        Response.Redirect(PackageUserControl.MakePackageUrl(Me.Page, pkgId:=Me.PackageRow.PkgId, tabName:="Agents"))
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain Then Return

        agentPopupPicker.DataId = ""
        agentPopupPicker.Text = ""
        agentPopup.Show()
    End Sub

    Protected Sub agentPopupAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentPopupAddButton.Click
        If Not String.IsNullOrEmpty(agentPopupPicker.DataId) Then
            Dim agentPackageRow As AgentPackageRow = Me.PackageDataSet.AgentPackage.NewAgentPackageRow()
            agentPackageRow.ApkId = DataRepository.GetNewId()
            agentPackageRow.ApkAgnId = agentPopupPicker.DataId
            agentPackageRow.ApkPkgId = Me.PackageRow.PkgId
            ' fix by Shoel : 8.1.9 and 11.2.9
            ' its important that from date be the start of today i.e. Today at 00:00:00, and end date be 31/12/2099 00:00:00
            agentPackageRow.ApkBookFrom = Now.Date
            agentPackageRow.ApkBookTo = New Date(2099, 12, 31) 'PackageRow.PkgBookedToDate
            ' fix by Shoel : 8.1.9 and 11.2.9
            Me.PackageDataSet.AgentPackage.AddAgentPackageRow(agentPackageRow)
            Try
                Aurora.Common.Data.ExecuteDataRow(agentPackageRow, Me.CurrentPage.UserId, Me.CurrentPage.PrgmName)
            Catch ex As System.Data.SqlClient.SqlException
            End Try
        End If

        Response.Redirect(PackageUserControl.MakePackageUrl(Me.Page, pkgId:=Me.PackageRow.PkgId, tabName:="Agents"))
    End Sub

    Protected Sub agentPopupCancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentPopupCancelButton.Click
        agentPopup.Hide()
    End Sub
End Class
