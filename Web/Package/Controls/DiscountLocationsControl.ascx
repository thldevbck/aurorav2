﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DiscountLocationsControl.ascx.vb" Inherits="Package_Controls_DiscountLocationsControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>


<asp:PlaceHolder ID="plhStyle" runat="server">
    <style type="text/css">
        .dislocRightText
        {
            text-align:right;
        }
        
        .dislocPopupPanel
        {
            width: 500px;
            height:180px;
        }
        
        .dislocMessage
        {
            height:30px;
        }
        
        #locationsTable
        {
            width:100%;
            margin:20px 0px 0px 0px;
        }
    </style>
</asp:PlaceHolder>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>

        <div class="dislocRightText">
            <asp:Button ID="btnDelete" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="False" />
            <asp:Button ID="btnAdd" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
        </div>

        <asp:Repeater ID="rptLocations" runat="server">        
            <HeaderTemplate>
                <table id="locationsTable">
                <tr>
                    <th><input id="locationsCheckAllBox" type="checkbox" /></th>
                    <th>Edit</th>
                    <th>Travel From</th>
                    <th>Travel To</th>
                    <th>Exclude</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:CheckBox ID="chkSelected" runat="server"/></td>
                    <td><asp:LinkButton ID="lnkEdit" CausesValidation="false" Text="edit" CommandArgument="<%# DirectCast(Container.DataItem, DiscountProfileLocation).Id%>"  Enabled="<%#mCanMaintain %>" OnClick="EditLocation" runat="server" /></td>
                    <td><%# DirectCast(Container.DataItem, DiscountProfileLocation).FromLocationCode%></td>
                    <td><%# DirectCast(Container.DataItem, DiscountProfileLocation).ToLocationCode%></td>
                    <td><%# Aurora.Common.IIFX(DirectCast(Container.DataItem, DiscountProfileLocation).Exclude, "Yes", "")%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>            
        </asp:Repeater>

        <asp:PlaceHolder ID="plhInitScript" runat="server" Visible="false">
            <script  type="text/javascript">
                (function () {
                    var mCheckAllBox = document.getElementById('locationsCheckAllBox');

                    if (!mCheckAllBox)
                        return;

                    var 
                    mCheckboxes = $('#locationsTable input[type=checkbox]').not(mCheckAllBox),
                    checkAll = function (check) {
                        $(mCheckboxes).each(function (i, item) { item.checked = check; });
                    };

                    mCheckAllBox.onclick = mCheckAllBox.onkeypress = function () { checkAll(this.checked); };
                })();
            </script>
        </asp:PlaceHolder>

        <asp:Button runat="server" ID="locationPopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="locationPopup" 
            BehaviorID="locationPopupBehavior"
            TargetControlID="locationPopupButton" 
            PopupControlID="locationPopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="locationPopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup dislocPopupPanel" ID="locationPopupPanel">
            <asp:PlaceHolder ID="plhLocationsPopupContent" runat="server" Visible="false">
                <asp:Panel runat="Server" ID="locationPopupDragPanel" CssClass="modalPopupTitle">
                    <asp:Literal ID="litTitleEdit" Mode="PassThrough" runat="Server">Edit location</asp:Literal>
                    <asp:Literal ID="litTitleAdd" Mode="PassThrough" runat="Server" Visible="false">Add location</asp:Literal>
                </asp:Panel>

                <asp:HiddenField ID="hidLocationId" runat="server" />
                <asp:HiddenField ID="hidLocationIntegrity" runat="server" />

                <div class="dislocMessage">
                    <uc1:MessagePanelControl runat="server" ID="messagePanelControl" CssClass="popupMessagePanel" />
                </div>
    
                <table cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td style="width:150px">Travel From:</td>
                        <td><uc1:PickerControl ID="pkrLocationPopupTravelFrom" runat="server" Width="300px" AppendDescription="true" PopupType="LOCATIONFORCOUNTRY"  MessagePanelControlID="messagePanelControl" /></td>
                    </tr>
                    <tr>
                        <td>Travel To:</td>
                        <td><uc1:PickerControl ID="pkrLocationPopupTravelTo" runat="server" Width="300px" AppendDescription="true" PopupType="LOCATIONFORCOUNTRY"  MessagePanelControlID="messagePanelControl" /></td>
                    </tr>
                    <tr>
                        <td>Exclude:</td>
                        <td><asp:CheckBox ID="chkLocationPopupExclude" runat="server" /></td>
                    </tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td colspan="2" >
                            <asp:Button ID="btnLocationPopupSave" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                            <asp:Button ID="btnLocationPopupSaveNext" runat="Server" Text="OK / Next" CssClass="Button_Standard Button_OK" />
                            <asp:Button ID="btnLocationPopupDelete" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                            <asp:Button ID="btnLocationPopupCancel" runat="Server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                        </td>
                    </tr>
                </table>
            </asp:PlaceHolder>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
