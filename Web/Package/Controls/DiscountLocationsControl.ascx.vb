﻿Option Strict On
Option Explicit On

Imports System
Imports System.Data
Imports System.Data.Common
Imports System.Web
Imports System.Configuration

Imports System.Collections
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.Package.Data

Partial Class Package_Controls_DiscountLocationsControl
    Inherits PackageUserControl

    Private mDiscountProfileID As String
    Protected mCanMaintain As Boolean

    Protected mPopupShown As Boolean

    Protected mDiscountLocations As List(Of DiscountProfileLocation)

    Public Property DiscountProfileID As String
        Get
            Return mDiscountProfileID
        End Get
        Set(ByVal value As String)
            mDiscountProfileID = value
        End Set
    End Property

    Protected Property IsNew As Boolean
        Get
            Return CBool(ViewState("dislocIsNew"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("dislocIsNew") = value
        End Set
    End Property

    Protected Property IsEditMode As Boolean
        Get
            Return CBool(ViewState("dislocIsEdit"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("dislocIsEdit") = value
        End Set
    End Property


    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        MyBase.OnLoad(e)

        ''rev:mia: May 16 2013 - remove the restriction. Request by Rajesh
        mCanMaintain = True ''CanMaintain

        btnAdd.Visible = mCanMaintain
        btnDelete.Visible = mCanMaintain

        If Not IsEditMode Then
            GetData()
        End If

        plhInitScript.Visible = True
        ScriptManager.RegisterStartupScript(Me.Page, Me.Page.GetType(), "DiscountLocationsInit", RenderControlToString(plhInitScript), False)
        plhInitScript.Visible = False
    End Sub

    Protected Sub GetData()
        Dim repository As New DiscountLocationRepository()

        Dim discountId As Integer
        If Not Integer.TryParse(Me.DiscountProfileID, discountId) Then
            Return
        End If

        mDiscountLocations = repository.GetDiscountLocations(discountId)

        rptLocations.DataSource = mDiscountLocations
        AddHandler rptLocations.ItemDataBound, AddressOf ItemDataBound
        rptLocations.DataBind()
    End Sub

    Private Sub ItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        Dim item As RepeaterItem = e.Item

        'This will ensure that each control within the repeater item is uniquely identified by the bound data object's database identity value.
        'It will prevent postback data being incorrectly associated with the wrong data object.
        If item.ItemType = ListItemType.Item Then
            e.Item.ID = item.ID + mDiscountLocations(item.ItemIndex).Id.ToString()
        End If

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If Not CanMaintain Then Return

        Dim discountId As Integer

        If Not Integer.TryParse(Me.DiscountProfileID, discountId) Then
            messagePanelControl.SetMessagePanelError("Invalid discount ID supplied.")
            Return
        End If

        Dim page As AuroraPage = DirectCast(Me.Page, AuroraPage)

        Dim repository As New DiscountLocationRepository()

        Dim index As Integer = 0

        For Each item As RepeaterItem In rptLocations.Items

            Dim location As DiscountProfileLocation = mDiscountLocations(index)

            Dim checkbox As CheckBox = DirectCast(item.FindControl("chkSelected"), CheckBox)

            If checkbox IsNot Nothing AndAlso checkbox.Checked Then
                Dim errorMessage As String = repository.DeleteDiscountLocation(location.Id, discountId)

                If errorMessage IsNot Nothing Then
                    page.SetWarningShortMessage(errorMessage)
                    Exit For
                Else
                    mDiscountLocations.RemoveAt(index)
                    Continue For
                End If
            End If
            index += 1
        Next

        If rptLocations.Items.Count <> mDiscountLocations.Count Then
            rptLocations.DataBind()
        End If
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If Not CanMaintain Then Return

        ShowPopup()
        ClearFields()
        IsNew = True
    End Sub

    Protected Sub btnLocationPopupUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLocationPopupSave.Click, btnLocationPopupSaveNext.Click
        If Not ValidateInput() Then
            ShowPopup()
            Return
        End If

        If SaveLocation() Then
            messagePanelControl.SetMessagePanelInformation("Location saved.")

            If Object.ReferenceEquals(sender, btnLocationPopupSaveNext) Then
                ClearFields()
                ShowPopup()
            End If
        Else
            'messagePanelControl.SetMessagePanelError("Location could not be saved.")
            ShowPopup()
        End If
    End Sub

    Protected Sub EditLocation(ByVal sender As Object, ByVal e As EventArgs)
        Dim button As LinkButton = DirectCast(sender, LinkButton)

        If Not CanMaintain Then Return

        ShowPopup()

        Dim locationId As Integer
        Dim discountId As Integer

        If Not Integer.TryParse(button.CommandArgument, locationId) Then
            messagePanelControl.SetMessagePanelError("Invalid location ID supplied.")
            Return
        End If
        If Not Integer.TryParse(Me.DiscountProfileID, discountId) Then
            messagePanelControl.SetMessagePanelError("Invalid discount ID supplied.")
            Return
        End If


        Dim repository As New DiscountLocationRepository()
        Dim location As DiscountProfileLocation = repository.GetDiscountLocation(discountId, locationId)

        If location Is Nothing Then
            messagePanelControl.SetMessagePanelError("Discount location could not be found.")
            Return
        End If

        SetFields(location)
        IsNew = False
    End Sub

    Protected Sub btnLocationPopupDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLocationPopupDelete.Click
        If Not CanMaintain Then Return

        Dim locationId As Integer
        Dim discountId As Integer

        If Not Integer.TryParse(hidLocationId.Value, locationId) Then
            messagePanelControl.SetMessagePanelError("Invalid location ID supplied.")
            Return
        End If
        If Not Integer.TryParse(Me.DiscountProfileID, discountId) Then
            messagePanelControl.SetMessagePanelError("Invalid discount ID supplied.")
            Return
        End If

        Dim repository As New DiscountLocationRepository()
        Dim errorMessage As String = repository.DeleteDiscountLocation(locationId, discountId)
        If errorMessage IsNot Nothing Then
            messagePanelControl.SetMessagePanelWarning(errorMessage)
        End If
    End Sub

    Protected Function SaveLocation() As Boolean


        If NullIfEmpty(Me.DiscountProfileID) Is Nothing Then
            'Throw New InvalidOperationException("Discount profile is not specified.")
            messagePanelControl.SetMessagePanelWarning("Discount profile must be created first before adding locations.")
            Return False
        End If

        Dim discountId As Integer
        Dim locationId As Integer
        Dim integrity As Integer
        Integer.TryParse(hidLocationIntegrity.Value, integrity)
        Dim locationFromId As String = NullIfEmpty(pkrLocationPopupTravelFrom.DataId)
        Dim locationToId As String = NullIfEmpty(pkrLocationPopupTravelTo.DataId)
        Dim page As AuroraPage = DirectCast(Me.Page, AuroraPage)

        If Not Integer.TryParse(Me.DiscountProfileID, discountId) Then
            messagePanelControl.SetMessagePanelError("Invalid discount ID supplied.")
            Return False
        End If

        Dim repository As New DiscountLocationRepository()

        Dim errorMessage As String = Nothing

        If IsNew Then
            errorMessage = repository.CreateDiscountLocation(discountId, locationFromId, locationToId, chkLocationPopupExclude.Checked, page.UserCode, page.PrgmName, integrity)
        Else
            If Not Integer.TryParse(hidLocationId.Value, locationId) Then
                messagePanelControl.SetMessagePanelError("Invalid location ID supplied.")
                Return False
            End If

            errorMessage = repository.UpdateDiscountLocation(locationId, discountId, locationFromId, locationToId, chkLocationPopupExclude.Checked, page.UserCode, page.PrgmName, integrity)
        End If

        If errorMessage IsNot Nothing Then
            messagePanelControl.SetMessagePanelWarning(errorMessage)
            Return False
        End If


        Return True
    End Function

    Protected Function NullIfEmpty(ByVal value As String) As String
        If String.IsNullOrEmpty(value) Then
            Return Nothing
        End If

        value = value.Trim()

        If value.Length = 0 Then
            Return Nothing
        End If

        Return value
    End Function


    Protected Function ValidateInput() As Boolean
        If Not pkrLocationPopupTravelFrom.IsValid Then
            messagePanelControl.SetMessagePanelWarning(pkrLocationPopupTravelFrom.ErrorMessage)
            Return False
        End If

        If Not pkrLocationPopupTravelTo.IsValid Then
            messagePanelControl.SetMessagePanelWarning(pkrLocationPopupTravelTo.ErrorMessage)
            Return False
        End If

        If String.IsNullOrEmpty(pkrLocationPopupTravelFrom.DataId) AndAlso String.IsNullOrEmpty(pkrLocationPopupTravelTo.DataId) Then
            messagePanelControl.SetMessagePanelWarning("A value must be entered into at least one of the location fields.")
            Return False
        End If

        Return True
    End Function

    Protected Sub ShowPopup()
        plhLocationsPopupContent.Visible = True
        locationPopup.Show()
        mPopupShown = True
    End Sub
    Protected Sub ClearFields()
        SetFields(New DiscountProfileLocation())
    End Sub

    Protected Sub SetFields(ByVal location As DiscountProfileLocation)
        hidLocationId.Value = IIFX(location.Id > 0, location.Id.ToString(), "")

        hidLocationIntegrity.Value = IIFX(location.Integrity > 0, location.Id.ToString(), "")

        pkrLocationPopupTravelFrom.DataId = location.FromLocationCode
        pkrLocationPopupTravelFrom.Text = location.FromLocationCode

        pkrLocationPopupTravelTo.DataId = location.ToLocationCode
        pkrLocationPopupTravelTo.Text = location.ToLocationCode

        chkLocationPopupExclude.Checked = location.Exclude
    End Sub

    Protected Sub SetControlState()

        Dim isNewRecord As Boolean = IsNew

        btnLocationPopupSaveNext.Visible = isNewRecord
        btnLocationPopupDelete.Visible = Not isNewRecord

        litTitleAdd.Visible = isNewRecord
        litTitleEdit.Visible = Not isNewRecord
    End Sub

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        SetControlState()

        If IsEditMode Then
            GetData()
        End If
        IsEditMode = mPopupShown

        PrepareStyle()

        MyBase.OnPreRender(e)
    End Sub

    Protected Sub PrepareStyle()
        Dim page As Page = Me.Page

        plhStyle.Parent.Controls.Remove(plhStyle)

        If page IsNot Nothing Then
            Dim header As HtmlHead = page.Header

            If header IsNot Nothing Then
                header.Controls.Add(plhStyle)
            End If
        End If
    End Sub


    Protected Function RenderControlToString(ByVal control As Control) As String
        Dim objStringWriter As System.IO.StringWriter = Nothing
        Dim objHtmlWriter As HtmlTextWriter = Nothing
        Try
            objStringWriter = New System.IO.StringWriter()
            objHtmlWriter = New HtmlTextWriter(objStringWriter)
            control.RenderControl(objHtmlWriter)
            Return objStringWriter.ToString()
        Catch

        Finally
            If objHtmlWriter IsNot Nothing Then
                objHtmlWriter.Close()
            End If

            If objStringWriter IsNot Nothing Then
                objStringWriter.Close()
            End If
        End Try

        Return Nothing
    End Function


    Protected Class DiscountProfileLocation
        Protected mId As Integer
        Protected mFromLocationCode As String
        Protected mToLocationCode As String
        Protected mExclude As Boolean
        Protected mIntegrity As Integer

        Public Property Id As Integer
            Get
                Return mId
            End Get
            Set(ByVal value As Integer)
                mId = value
            End Set
        End Property
        Public Property FromLocationCode As String
            Get
                Return mFromLocationCode
            End Get
            Set(ByVal value As String)
                mFromLocationCode = value
            End Set
        End Property
        Public Property ToLocationCode As String
            Get
                Return mToLocationCode
            End Get
            Set(ByVal value As String)
                mToLocationCode = value
            End Set
        End Property
        Public Property Exclude As Boolean
            Get
                Return mExclude
            End Get
            Set(ByVal value As Boolean)
                mExclude = value
            End Set
        End Property
        Public Property Integrity As Integer
            Get
                Return mIntegrity
            End Get
            Set(ByVal value As Integer)
                mIntegrity = value
            End Set
        End Property
    End Class

    Protected Class DiscountLocationRepository
        Public Overridable Function CreateDiscountLocation( _
            ByVal discountId As Integer, ByVal locationFromId As String, ByVal locationToId As String, _
            ByVal exclude As Boolean, ByVal userCode As String, ByVal programName As String, ByVal integrity As Integer) As String

            Try
                Aurora.Common.Data.ExecuteScalarSP("CreateOrUpdateDiscountProfileLocation", _
                                                   Nothing, discountId, locationFromId, locationToId, exclude, userCode, programName, integrity)
            Catch ex As Exception
                Return "Location could not be saved."
            End Try

            Return Nothing
        End Function

        Public Overridable Function UpdateDiscountLocation( _
            ByVal locationId As Integer, ByVal discountId As Integer, ByVal locationFromId As String, ByVal locationToId As String, _
            ByVal exclude As Boolean, ByVal userCode As String, ByVal programName As String, ByVal integrity As Integer) As String

            Try
                Aurora.Common.Data.ExecuteScalarSP("CreateOrUpdateDiscountProfileLocation", _
                                                   locationId, discountId, locationFromId, locationToId, exclude, userCode, programName, integrity)
            Catch ex As Exception
                Return "Location could not be saved."
            End Try

            Return Nothing
        End Function

        Public Overridable Function DeleteDiscountLocation(ByVal locationId As Integer, ByVal discountId As Integer) As String

            Try
                Aurora.Common.Data.ExecuteScalarSP("DeleteDiscountLocation", discountId, locationId)
            Catch ex As Exception
                Return "Location could not be deleted."
            End Try

            Return Nothing
        End Function

        Public Overridable Function GetDiscountLocations(ByVal discountId As Integer) As List(Of DiscountProfileLocation)
            Dim dataset As New DiscountLocationDataset()
            Data.ExecuteListSP("GetDiscountLocations", dataset, New ParamInfo(discountId, "DiscountID"))

            Return dataset.ResultSet
        End Function

        Public Overridable Function GetDiscountLocation(ByVal discountId As Integer, ByVal locationId As Integer) As DiscountProfileLocation
            Dim dataset As New DiscountLocationDataset()
            Data.ExecuteListSP("GetDiscountLocations", dataset, New ParamInfo(discountId, "DiscountID"), New ParamInfo(locationId, "LocationID"))

            If dataset.ResultSet.Count = 1 Then
                Return dataset.ResultSet(0)
            End If

            Return Nothing
        End Function

        Public Class DiscountLocationDataset
            Inherits Aurora.Common.ListDataSet(Of DiscountProfileLocation)

            Public Overrides Function CreateItemFromReader(ByVal reader As System.Data.Common.DbDataReader) As DiscountProfileLocation
                Dim location As New DiscountProfileLocation()

                location.Id = GetDbValue(Of Integer)(reader, "DispProLId")
                location.FromLocationCode = GetDbValue(Of String)(reader, "DispProLFromLocCode")
                location.ToLocationCode = GetDbValue(Of String)(reader, "DispProLToLocCode")
                location.Exclude = GetDbValue(Of Boolean)(reader, "DispProLExclude")
                location.Integrity = GetDbValue(Of Integer)(reader, "IntegrityNo")

                Return location
            End Function
        End Class
    End Class
End Class
