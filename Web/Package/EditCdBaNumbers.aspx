﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="EditCdBaNumbers.aspx.vb" Inherits="Package_EditCdBaNumbers" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Import Namespace="Aurora.Package.Data" %>
<%@ Import Namespace="Aurora.Common" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>



<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="StylePlaceHolder">
<style type="text/css">
    .cdBaBackButton
    {
        float:right;
    }
    
    .cdBaClear 
    {
        clear:both;
    }
    
    table#CdBaTable
    {
        width:100%;
        border-collapse: collapse
    }
    
    table#CdBaTable td, table#CdBaTable th
    {
        padding: 5px 4px;
        border-bottom: 1px dotted #000;
    }
    
    table.CdBaPackageInfoTable select
    {
        width: 150px;
    }
    
    table.CdBaPackageInfoTable td
    {
        padding-left:2px;
        padding-right:2px;
        white-space: nowrap;
    }
    
    table.CdBaPackageInfoTable td label
    {
        padding:0px 0px 0px 10px;
    }
    
    table.CdBaPackageInfoTable td.CdBaLeftCol label
    {
        padding-left:0px;
    }
    
    table.CdBaPackageInfoTable td input.CdBaPkgTextBox
    {
        width:110px;
    }
    
    .cdBaLinkPanel
    {
        width:740px;
        height:500px;
    }
    
    .CdBaControls
    {
        margin: 20px 0px 0px 0px;
        text-align:center;
    }
    
    #cdBaLinkTable
    {        
    }
    
    .cdBaLinkAgents
    {
        margin: 10px 0px 0px 0px;
        height: 300px;
        width: 350px;
    }
    .cdBaLinkAgentsSelected
    {
        height: 330px;
        width: 350px;
    }
    
    #CdBaAgentAdd, #CdBaAgentRemove
    {
        padding-left: 8px;
        padding-right: 8px;
    }
    
    #cdBaExcludeOption
    {
        float: right;
    }
    
    table.cdBaOptionsTable
    {
        margin:10px 0px;
    }
    
    table.cdBaOptionsTable td
    {
        padding-left:2px;
        padding-right:12px;
    }
    
    table.cdBaOptionsTable td label
    {
        padding:0px 0px 0px 0px;
    }
    
    table.cdBaOptionsTable td.rightCol
    {
        padding-left:20px;
    }
    
    .cdBaErrorControl
    {
        background:#FF9999;
    }
    
</style>
</asp:Content>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <table class="CdBaPackageInfoTable">
            <tr>
                <td class="CdBaLeftCol">
                    <label>Code: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtCode" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>Name: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtName" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>Brand: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtBrand" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>Country: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtCountry" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="CdBaLeftCol">
                    <label>Booked From: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtBookedFrom" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>Booked To: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtBookedTo" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>Travel From: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtTravelFrom" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td>
                    <label>Travel To: </label>
                </td>
                <td>
                    <asp:TextBox ID="txtTravelTo" CssClass="CdBaPkgTextBox" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>

        <div class="CdBaControls">
            <input type="button" class="cdBaBackButton Button_Back Button_Medium" onclick="location.href='Package.aspx?pkgId=<%=PackageId%>'" value="Back" />
            <asp:Button ID="btnRemove" Text="Remove" CssClass="Button_Medium" runat="server" />
            <asp:Button ID="btnLink" Text="Link" CssClass="Button_Medium" runat="server" />
        </div>

        <br class="cdBaClear" />
        <br />

        <asp:Repeater ID="rptMappings" EnableViewState="false" runat="server">
            <HeaderTemplate>
                <table id="CdBaTable">
                <thead>
                    <tr>
                        <th><input id="cdBaCheckAll" type="checkbox" /></th><th>CD number</th><th>BA number</th><th>Account Name</th><th>Pay at Branch</th><th>Pickup at Airport</th><th>Linked Agents</th><th>Is Agent Excluded</th>
                    </tr>
                </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><input id="cdBaCheck<%# DirectCast(Container.DataItem, CdBaNumberPackageMapping).MappingId%>" type="checkbox" /></td>
                    <td><%# DirectCast(Container.DataItem, CdBaNumberPackageMapping).CdBaNumber.ContractIdNumber%></td>
                    <td><%# DirectCast(Container.DataItem, CdBaNumberPackageMapping).CdBaNumber.BusinessAccountNumber%></td>
                    <td><%# DirectCast(Container.DataItem, CdBaNumberPackageMapping).CdBaNumber.AccountName%></td>
                    <td><%# ResolveNullableBoolean(DirectCast(Container.DataItem, CdBaNumberPackageMapping).PayDirect, "All", "Yes", "No")%></td>
                    <td><%# ResolveNullableBoolean(DirectCast(Container.DataItem, CdBaNumberPackageMapping).IsAirport, "All", "Yes", "No")%></td>
                    <td><%# GetLinkedAgents(DirectCast(Container.DataItem, CdBaNumberPackageMapping))%></td>
                    <td><%# IIFX( _
                                DirectCast(Container.DataItem, CdBaNumberPackageMapping).IsAgentExcluded, "Yes", _
                                IIFX(DirectCast(Container.DataItem, CdBaNumberPackageMapping).Agents.Count > 0, "No", "") _
                                )%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>

        <asp:HiddenField ID="hidSelectedMappings" runat="server" />
        
        <script type="text/javascript">
            (function (removeMappingButtonId, selectedItemsInputId) {
                var mCheckAllBox = document.getElementById('cdBaCheckAll');

                if (!mCheckAllBox)
                    return;

                var 
                mRemoveButton = document.getElementById(removeMappingButtonId),
                mSelectedItemsInput = document.getElementById(selectedItemsInputId),
                mCheckboxes = $('#CdBaTable input[type=checkbox]').not(mCheckAllBox),
                checkAll = function (check) {
                    $(mCheckboxes).each(function (i, item) { item.checked = check; });
                },
                onRemoveMappings = function () {
                    var i = -1,
                    sId,
                    sResult = '';

                    while (oCheckbox = mCheckboxes[++i]) {
                        if (!oCheckbox.checked)
                            continue;

                        sId = (oCheckbox.id.match(/\d+$/) || [0])[0] * 1;

                        if (sId) {
                            if (sResult)
                                sResult += ',';

                            sResult += sId;
                        }
                    }

                    mSelectedItemsInput.value = sResult;
                };

                if (mCheckAllBox) {
                    mCheckAllBox.onclick = mCheckAllBox.onkeypress = function () { checkAll(this.checked); };
                    mRemoveButton.onclick = onRemoveMappings;
                }
            })(
                '<%=btnRemove.ClientID %>',
                '<%=hidSelectedMappings.ClientID %>'
            );
        </script>

        <%--Dummy control, required by the modal popup dialog--%>
        <asp:Button 
            runat="server" 
            ID="dummyCdBaTargetButton" 
            Style="display: none" />

        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="cdBaModalDialog" 
            BehaviorID="cdBaModalDialogBehavior"
            TargetControlID="dummyCdBaTargetButton" 
            PopupControlID="cdBaModalDialogContent"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="cdBaModalDialogDragHandle" />


        <asp:HiddenField ID="hidSelectedAgents" runat="server" />


        <asp:Panel runat="server" CssClass="modalPopup cdBaLinkPanel" ID="cdBaModalDialogContent">
            <asp:PlaceHolder ID="plhCdBaDialogInner" runat="server" Visible="false">
                <asp:Panel runat="Server" ID="cdBaModalDialogDragHandle" CssClass="modalPopupTitle">
                    <asp:Label ID="titleLabel" runat="server">Link CD/BA Numbers</asp:Label>
                </asp:Panel>

               
                        <%--<asp:HiddenField ID="hidSelectedAgents" runat="server" />--%>

                        <table class="cdBaOptionsTable">
                            <tr>
                                <td>
                                    <label>CD/BA Number: </label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCdBaNumbers" EnableViewState="true" runat="server">                                        
                                    </asp:DropDownList>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Pay at Pickup: </label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlPickupPay" EnableViewState="false" runat="server">
                                        <asp:ListItem Value="">All</asp:ListItem>
                                        <asp:ListItem Value="true">Yes</asp:ListItem>
                                        <asp:ListItem Value="false">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td class="rightCol">
                                    <label>Pickup at Airport: </label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAirportPickup" EnableViewState="false" runat="server">
                                        <asp:ListItem Value="">All</asp:ListItem>
                                        <asp:ListItem Value="true">Yes</asp:ListItem>
                                        <asp:ListItem Value="false">No</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>

                        <table id="cdBaLinkTable">
                            <tr>
                                <td>
                                    <b>Available Agents</b> <br /> <br />
                                    Search: <input id="txtAgentSearch" type="text" EnableViewState="false" runat="server" />
                                    <input id="cdBaSearchButton" type="button" value="Search" onclick="__doPostBack('<%= AgentListUpdatePanel.ClientID %>', '');return false;" />
                                    <asp:UpdatePanel ID="AgentListUpdatePanel" UpdateMode="Conditional" ChildrenAsTriggers="false" RenderMode="Block" EnableViewState="false" runat="server">
                                        <ContentTemplate>
                                            <select id="lbxAgents" multiple="multiple" class="cdBaLinkAgents" >
                                                <%
                                                    If mAgents IsNot Nothing Then
                                                        Dim lastIndex As Integer = mAgents.Count -1
                                                        For i As Integer = 0 To lastIndex
                                                        %>
                                                  
                                                    <option value="<%= mAgents(i).AgentId %>"><%= mAgents(i).CodeAndName%></option>
                                                <%
                                                Next
                                            End If
                                                %>
                                            </select>
                                        </ContentTemplate>
                                        <Triggers>
                                            <%--<asp:AsyncPostBackTrigger ControlID="btnApplyFilter" EventName="Click" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </td>
                                <td>
                                    <input value="&gt;" id="CdBaAgentAdd" type="button" /><br /><br />
                                    <input  value="&lt;" id="CdBaAgentRemove" type="button" />
                                </td>
                                <td>
                                    <div id="cdBaExcludeOption">
                                        <asp:CheckBox ID="chkExclude" runat="server" />&nbsp;Exclude
                                    </div>
                                    <b>Selected Agents</b> <br /> <br />
                                    <select id="lbxSelectedAgents" multiple="multiple" class="cdBaLinkAgentsSelected" >
                                        <%
                                            If mSelectedAgents IsNot Nothing Then
                                                Dim lastIndex As Integer = mSelectedAgents.Count -1
                                                For i As Integer = 0 To lastIndex
                                                %>
                                                  
                                            <option value="<%= mSelectedAgents(i).Key %>"><%= mSelectedAgents(i).Value%></option>
                                        <%
                                        Next
                                    End If
                                        %>
                                    </select>
                                </td>
                            </tr>
                        </table>

                        <table width="100%">
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Save Button_Medium" />
                                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Back Button_Medium" />
                                </td>
                            </tr>
                        </table>

                    

                <%--<asp:Button ID="btnApplyFilter" Text="Apply" OnClick="btnApplyFilter_Click" OnClientClick="" runat="server" />--%>
                

                <asp:PlaceHolder ID="plhCdBaScript" runat="server" Visible="false">
                    <script type="text/javascript">
                        (function (sourceListId, targetListId, addButtonId, removeButtonId, saveButtonId, backButtonId, selectedItemsInputId, searchButtonId) {
                            var 
                            mAddButton = document.getElementById(addButtonId),
                            mRemoveButton = document.getElementById(removeButtonId),
                            mSaveButton = document.getElementById(saveButtonId),
                            mBackButton = document.getElementById(backButtonId),
                            mSelectedItemsInput = document.getElementById(selectedItemsInputId),
                            mSearchButton = document.getElementById(searchButtonId),
                            mTemp,
                            moveItems = function (sourceList, targetList) {
                                while (sourceList.selectedIndex > -1) {
                                    var oOption = sourceList.options[sourceList.selectedIndex];
                                    sourceList.remove(sourceList.selectedIndex);
                                    targetList.add(oOption, 99999);
                                }
                            },
                            addItems = function () {
                                moveItems(
                                    document.getElementById(sourceListId),
                                    document.getElementById(targetListId)
                                );
                            },
                            removeItems = function () {
                                moveItems(
                                    document.getElementById(targetListId),
                                    document.getElementById(sourceListId)
                                );
                            },
                            onSave = function () {
                                var oList = document.getElementById(targetListId),
                                i = -1,
                                oOptions = oList.options,
                                oOption,
                                sResult = '';

                                while (oOption = oOptions[++i]) {
                                    if (sResult)
                                        sResult += ',';

                                    //TEST
                                    //oOption.value = oOption.value + '\\,as,d\\abc';

                                    sResult += oOption.value.replace(/\\/gi, '\\\\').replace(/,/gi, '\\,');
                                    sResult += ',' + oOption.text.replace(/\\/gi, '\\\\').replace(/,/gi, '\\,');
                                }

                                mSelectedItemsInput.value = sResult;
                            };


                            mAddButton.onclick = addItems;
                            mRemoveButton.onclick = removeItems;
                            mSaveButton.onclick = onSave;
                            mBackButton.onclick = onSave;

                            mTemp = mSearchButton.onclick;
                            mSearchButton.onclick = function () { onSave(); mTemp(); };
                        })(
                            'lbxAgents',
                            'lbxSelectedAgents',
                            'CdBaAgentAdd',
                            'CdBaAgentRemove',
                            '<%=btnSave.ClientID %>',
                            '<%=btnBack.ClientID %>',
                            '<%=hidSelectedAgents.ClientID %>',
                            'cdBaSearchButton'
                        );

                        //
//                        (function () {
//                            var mCdBaDropdown = document.getElementById('<%=ddlCdBaNumbers.ClientID %>'),
//                            validateControls = function(){
//                                if (mCdBaDropdown.selectedIndex == 0){
//                                    setError(mCdBaDropdown);
//                                    return false;
//                                }
//                                return true;
//                            },
//                            setError = function(control){
//                                $(control).addClass('cdBaErrorControl');
//                            },
//                            clearError = function(){
//                                $(this).removeClass('cdBaErrorControl');
//                            };

//                            mCdBaDropdown.onchange = clearError;

//                            document.getElementById('<%=btnSave.ClientID %>').onclick =
//                                function(){
//                                    return validateControls();
//                                };

//                            document.getElementById('<%=btnBack.ClientID %>').onclick =
//                                function(){
//                                    return validateControls();
//                                };
//                        })();
                    </script>
                </asp:PlaceHolder>
                
            </asp:PlaceHolder>
        </asp:Panel>

</asp:Content>