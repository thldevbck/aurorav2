﻿''rev:mia Feb.28 2013 - Added a complete and working version
''                    - today in history: Last day of Pope Benedict                        
''rev:mia Oct 16 2014 - addition of PromoCode
''                    - DiscountProfiles                                    - addition of DisProIsPromoCode
''                    - Package_InsertUpdateSingleProfile                   - addition of PromoCode
''                    - Aurora.Package.Data\DataRepository.vb
''                    - Aurora.Package.Data\EditPackageProfile.vb  

Imports Aurora.Package.Data
Imports System.Data
Imports Aurora.Common
Imports Aurora.Configuration.Package
Imports System.Collections.Generic


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DiscountProfileSearch)> _
Partial Class Package_EditPackageDiscount
    Inherits AuroraPage

    Private Const NEW_DISCOUNT_TEXT As String = "New Package Discount Profile"
    Private Const EDIT_DISCOUNT_TEXT As String = "Edit Package Discount Profile"
    Private Const VIEW_DISCOUNT_TEXT As String = "View Package Discount Profile"

    Private Const EXCEPTION_DISCOUNTCODE_EMPTY As String = "Discount Code cannot be blank"
    Private Const EXCEPTION_DISCOUNTTEXT_EMPTY As String = "Discount Text cannot be blank"
    Private Const EXCEPTION_TYPE_EMPTY As String = "Type cannot be blank"
    Private Const EXCEPTION_TYPE_EMPTY_DEFAULT As String = "Type is not valid"

    Private Const EXCEPTION_DISCOUNT_EMPTY As String = "Discount cannot be blank"
    Private Const EXCEPTION_DISCOUNT_ZERO As String = "Discount cannot be ZERO or less than ZERO"
    Private Const EXCEPTION_DISCOUNT_OPTION As String = "Discount Radio cannot be left uncheck"

    Private Const EXCEPTION_BOOKEDFROM_EMPTY As String = "Booked From cannot be blank"
    Private Const EXCEPTION_BOOKEDTO_EMPTY As String = "Booked To cannot be blank"

    Private Const EXCEPTION_BOOKEDFROM_TIME_EMPTY As String = "Booked From time cannot be blank"
    Private Const EXCEPTION_BOOKEDTO_TIME_EMPTY As String = "Booked To time cannot be blank"

    Private Const EXCEPTION_TRAVELFROM_EMPTY As String = "Travel From cannot be blank"
    Private Const EXCEPTION_TRAVELTO_EMPTY As String = "Travel To cannot be blank"

    Private Const EXCEPTION_BOOKEDFROM_RANGE As String = "Booked From cannot be greater or equal to Booked To"
    Private Const EXCEPTION_BOOKEDTO_RANGE As String = "Booked To cannot be less or equal to Booked From"
    Private Const EXCEPTION_TRAVELFROM_RANGE As String = "Travel From cannot be greater or equal to Travel To"
    Private Const EXCEPTION_TRAVELTO_RANGE As String = "Travel To cannot be less or equal to Travel From"

    Private Const EXCEPTION_PIKCUPFROM_RANGE As String = "Pickup From cannot be greater  or equal to Pickup To"
    Private Const EXCEPTION_PIKCUPTO_RANGE As String = "Pickup To cannot be less or equal to Pickup From"

    Private Const EXCEPTION_UPDATE_NOROLE As String = "Only user that has a valid role can able to Update Package Profile"
    Private Const EXCEPTION_NEW_NOROLE As String = "Only user that has a valid role can able to add new Package Profile"

    Private Const SUCCESS_SAVING As String = "Updated successfully"
    Private Const EXCECPTION_SAVING As String = "Update not successful"

    Private Const SEARCH_PAGE_BACK As String = "DiscountProfileSearch.aspx"
    Private packageProfile As EditPackageProfile

#Region "Properties"

    Private _promoTileLinkUrl As String

    Protected ReadOnly Property PromoTileLinkUrl As String
        Get
            If _promoTileLinkUrl Is Nothing Then
                Dim objElement As PromoLinkElement = PackageConfiguration.Settings.PromoTileLink

                If objElement Is Nothing Then
                    Throw New Exception("Promotional tile link element is not defined in the configuration section.")
                End If

                _promoTileLinkUrl = objElement.Url

                If _promoTileLinkUrl IsNot Nothing Then
                    _promoTileLinkUrl = _promoTileLinkUrl.Trim()
                    If _promoTileLinkUrl.Length = 0 Then
                        _promoTileLinkUrl = Nothing
                    End If
                End If

                If _promoTileLinkUrl Is Nothing Then
                    Throw New Exception("Promotional tile link URL is not specified in the configuration section.")
                End If
            End If

            Return _promoTileLinkUrl
        End Get
    End Property

    Private Property MessageInfo As String
        Get
            Return Session("MessageInfo_EditPackageProfile")
        End Get
        Set(ByVal value As String)
            Session("MessageInfo_EditPackageProfile") = value
        End Set
    End Property

    Private ReadOnly Property DisProID As String
        Get
            Return IIf(String.IsNullOrEmpty(Request.QueryString("DisProID")), String.Empty, Request.QueryString("DisProID"))
        End Get
    End Property

    Private ReadOnly Property TabIndexActive As Integer
        Get
            Return IIf(String.IsNullOrEmpty(Request.QueryString("activeTab")), 0, Request.QueryString("activeTab"))
        End Get
    End Property
    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.DiscountProfileSearch)
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If String.IsNullOrEmpty(DisProID) Then
                Return NEW_DISCOUNT_TEXT
            ElseIf CanMaintain Then
                Return EDIT_DISCOUNT_TEXT
            Else
                Return VIEW_DISCOUNT_TEXT
            End If
        End Get
    End Property

    Private Property PackageProfileInCache As EditPackageProfile
        Get
            Return Session("PackageProfileInCache")
        End Get
        Set(ByVal value As EditPackageProfile)
            Session("PackageProfileInCache") = value
        End Set
    End Property

    ReadOnly Property RequeryURL(ByVal newDisProID As String, Optional ByVal mode As String = "") As String
        Get
            Dim tempQueryURL As String = ""
            If (Request.QueryString("DisProID") Is Nothing And Request.QueryString("saved") Is Nothing) Then
                Return Request.Url.AbsolutePath & "?DisProID=" & newDisProID
            Else

                tempQueryURL = Request.Url.PathAndQuery

                If (Not Request.QueryString("activeTab") Is Nothing) Then
                    tempQueryURL = tempQueryURL.Replace("&activeTab=" & Request.QueryString("activeTab"), "")
                End If

                If (Not Request.QueryString("saved") Is Nothing) Then
                    tempQueryURL = tempQueryURL.Replace("&saved=ok", "")
                    '    Return tempQueryURL ''Request.Url.PathAndQuery.Replace("&saved=ok", "")
                    'Else
                    '    Return Request.Url.PathAndQuery
                End If

                Return tempQueryURL
            End If
        End Get

    End Property

#End Region

    Protected Sub Package_EditPackageDiscount_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        lnkPromoTile.NavigateUrl = PromoTileLinkUrl
    End Sub

    Protected Sub Package_EditPackageDiscount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        dicountLocationsControl.DiscountProfileID = DisProID

        If Not Page.IsPostBack Then
            packageProfile = New EditPackageProfile(DisProID)
            PackageProfileInCache = packageProfile
            LoadCodeDataTable()

            If (Not String.IsNullOrEmpty(DisProID)) Then
                If Not String.IsNullOrEmpty(MessageInfo) Then
                    Me.AddInformationMessage(MessageInfo)
                    MessageInfo = String.Empty
                Else
                    If (Not Request.QueryString("saved") Is Nothing) Then
                        Me.AddInformationMessage(SUCCESS_SAVING)
                    End If
                End If

                LoadDiscountProfilesDataTable()

            Else
                ''add new profile
                ClearPackageProfile()
            End If
        Else
            packageProfile = PackageProfileInCache
        End If

        tabs.ActiveTabIndex = TabIndexActive
        LoadPackageDiscountProfilesDataTable()
        LoadAllProductsTable()
        LoadAllAgentsTable()


        ButtonEnabling()
    End Sub

    Sub LoadAllDataTables()
        LoadDiscountProfilesDataTable()
    End Sub


    Sub LoadAllProductsTable()
        PackageDiscountProductControl.DisproIdControl = DisProID
        PackageDiscountProductControl.SuccessfulMessage = SUCCESS_SAVING
        PackageDiscountProductControl.FailedMessage = EXCECPTION_SAVING
        PackageDiscountProductControl.MessageInfo = MessageInfo
        PackageDiscountProductControl.RequeryURL = RequeryURL(DisProID)


        Dim datatableNotLink As DataTable = packageProfile.DiscountProfilesProductNotLinkDataTable
        Dim datatableLink As DataTable = packageProfile.DiscountProfilesProductLinkDataTable
        Dim dependenciesTable As DataTable = packageProfile.ProductDependenciesTable


        Dim arrayOfTable As New List(Of DataTable)
        arrayOfTable.Add(datatableNotLink)
        arrayOfTable.Add(datatableLink)
        arrayOfTable.Add(dependenciesTable)
        PackageDiscountProductControl.ProductsInfo = arrayOfTable.ToArray


    End Sub


    Sub LoadAllAgentsTable()

        PackageDiscountAgentControl.DisproIdControl = DisProID
        PackageDiscountAgentControl.SuccessfulMessage = SUCCESS_SAVING
        PackageDiscountAgentControl.FailedMessage = EXCECPTION_SAVING
        PackageDiscountAgentControl.MessageInfo = MessageInfo
        PackageDiscountAgentControl.RequeryURL = RequeryURL(DisProID)

        Dim datatableNotLink As DataTable = packageProfile.DiscountProfilesAgentNotLinkDataTable
        Dim datatableLink As DataTable = packageProfile.DiscountProfilesAgentLinkDataTable


        Dim arrayOfTable As New List(Of DataTable)
        arrayOfTable.Add(datatableNotLink)
        arrayOfTable.Add(datatableLink)


        PackageDiscountAgentControl.AgentsInfo = arrayOfTable.ToArray


    End Sub

#Region "Loading of Profiles for agent,package etc"

    Private Function DiscountProfilesDataTableCandidate() As DataTable
        If packageProfile.DiscountProfilesDataTable Is Nothing Then Return Nothing

        Dim datatable As DataTable = packageProfile.DiscountProfilesDataTable

        If (String.IsNullOrEmpty(DisProID)) Then
            Dim newrow As DataRow = datatable.NewRow
            newrow.Item("DisProID") = -1
            newrow.Item("DisProCode") = EncodeText(discountcodeTextBox.Text)
            newrow.Item("DisProText") = EncodeText(discounttextTextBox.Text)
            newrow.Item("DisProEnableTile") = chkEnablePromoTile.Checked
            newrow.Item("DisProDescription") = EncodeText(discountDescriptionTextBox.Text)
            newrow.Item("DisProTile") = EncodeText(txtPromoTileText.Text)
            newrow.Item("DisProDiscountCodType") = typeDropDown.SelectedValue
            newrow.Item("DisProTravelFrom") = travelFromTextBox.Date
            newrow.Item("DisProTravelTo") = travelToTextBox.Date
            newrow.Item("DisProBookedFromDate") = bookedFromTextBox.Date + bookedFromTime.Time
            newrow.Item("DisProBookedToDate") = bookedToTextBox.Date + bookedToTime.Time

            ''rev:mia Dec.09,2013 - trapped invalid dates
            If (pickupFromTextBox.Date.Equals(Date.MinValue)) Then
                newrow.Item("DisProCkoFromDate") = DBNull.Value
            Else
                newrow.Item("DisProCkoFromDate") = pickupFromTextBox.Date
            End If

            If (pickupToTextBox.Date.Equals(Date.MinValue)) Then
                newrow.Item("DisProCkoToDate") = DBNull.Value
            Else
                newrow.Item("DisProCkoToDate") = pickupToTextBox.Date
            End If

            newrow.Item("DisProMustTravelBetween") = DisProMustTravelBetweenCheckbox.Checked
            newrow.Item("DisProOverridable") = DisProOverridableCheckBox.Checked
            newrow.Item("DisProIsActive") = DisProIsActiveCheckBox.Checked
            newrow.Item("DisProApplyToOnlyVehicle") = DisProApplyToOnlyVehicleCheckBox.Checked

            If radioDiscount.SelectedValue = 1 Then
                newrow.Item("DisProDiscountPer") = discountTextbox.Text
                newrow.Item("DisProDiscountAmt") = DBNull.Value
            Else
                newrow.Item("DisProDiscountPer") = DBNull.Value
                newrow.Item("DisProDiscountAmt") = discountTextbox.Text
            End If

            newrow.Item("AddUsrId") = UserCode

            ''rev:mia Oct 16 2014 - addition of PromoCode
            newrow.Item("DisProIsPromoCode") = PromoCodeCheckBox.Checked

            datatable.Rows.Add(newrow)

        Else
            If datatable.Rows.Count - 1 = -1 Then Return Nothing
            datatable.Rows(0).Item("DisProID") = DisProID
            datatable.Rows(0).Item("DisProCode") = EncodeText(discountcodeTextBox.Text)
            datatable.Rows(0).Item("DisProText") = EncodeText(discounttextTextBox.Text)
            datatable.Rows(0).Item("DisProEnableTile") = chkEnablePromoTile.Checked
            datatable.Rows(0).Item("DisProDescription") = EncodeText(discountDescriptionTextBox.Text)
            datatable.Rows(0).Item("DisProTile") = EncodeText(txtPromoTileText.Text)
            datatable.Rows(0).Item("DisProDiscountCodType") = typeDropDown.SelectedValue
            datatable.Rows(0).Item("DisProTravelFrom") = travelFromTextBox.Date
            datatable.Rows(0).Item("DisProTravelTo") = travelToTextBox.Date
            datatable.Rows(0).Item("DisProBookedFromDate") = bookedFromTextBox.Date + bookedFromTime.Time
            datatable.Rows(0).Item("DisProBookedToDate") = bookedToTextBox.Date + bookedToTime.Time

            ''rev:mia Dec.09,2013 - trapped invalid dates
            If (pickupFromTextBox.Date.Equals(Date.MinValue)) Then
                datatable.Rows(0).Item("DisProCkoFromDate") = DBNull.Value
            Else
                datatable.Rows(0).Item("DisProCkoFromDate") = pickupFromTextBox.Date
            End If

            If (pickupToTextBox.Date.Equals(Date.MinValue)) Then
                datatable.Rows(0).Item("DisProCkoToDate") = DBNull.Value
            Else
                datatable.Rows(0).Item("DisProCkoToDate") = pickupToTextBox.Date
            End If

            datatable.Rows(0).Item("DisProMustTravelBetween") = DisProMustTravelBetweenCheckbox.Checked
            datatable.Rows(0).Item("DisProOverridable") = DisProOverridableCheckBox.Checked
            datatable.Rows(0).Item("DisProIsActive") = DisProIsActiveCheckBox.Checked
            datatable.Rows(0).Item("DisProApplyToOnlyVehicle") = DisProApplyToOnlyVehicleCheckBox.Checked

            If radioDiscount.SelectedValue = 1 Then
                datatable.Rows(0).Item("DisProDiscountPer") = discountTextbox.Text
                datatable.Rows(0).Item("DisProDiscountAmt") = DBNull.Value
            Else
                datatable.Rows(0).Item("DisProDiscountPer") = DBNull.Value
                datatable.Rows(0).Item("DisProDiscountAmt") = discountTextbox.Text
            End If

            datatable.Rows(0).Item("AddUsrId") = UserCode

            ''rev:mia Oct 16 2014 - addition of PromoCode
            datatable.Rows(0).Item("DisProIsPromoCode") = PromoCodeCheckBox.Checked
        End If

        LogDebug("Tracing PickupDate in DiscountProfilesDataTableCandidate:" & pickupFromTextBox.Date & " " & datatable.Rows(0).Item("DisProCkoFromDate"))

        Return datatable
    End Function

    Sub LoadDiscountProfilesDataTable()

        Dim datatable As DataTable = packageProfile.DiscountProfilesDataTable
        If datatable.Rows.Count - 1 = -1 Then Return

        discountcodeTextBox.Text = DecodeText(datatable.Rows(0).Item("DisProCode").ToString.Trim)
        discounttextTextBox.Text = DecodeText(datatable.Rows(0).Item("DisProText").ToString.Trim)

        If Not IsDBNull(datatable.Rows(0).Item("DisProEnableTile")) Then
            chkEnablePromoTile.Checked = Convert.ToBoolean(datatable.Rows(0).Item("DisProEnableTile").ToString)
        Else
            chkEnablePromoTile.Checked = False
        End If

        discountDescriptionTextBox.Text = DecodeText(datatable.Rows(0).Item("DisProDescription").ToString.Trim & "")

        txtPromoTileText.Text = DecodeText(datatable.Rows(0).Item("DisProTile").ToString.Trim & "")
        typeDropDown.SelectedValue = datatable.Rows(0).Item("DisProDiscountCodType").ToString.Trim

        If Not IsDBNull(datatable.Rows(0).Item("DisProTravelFrom")) Then
            travelFromTextBox.Date = ParseDateTime(datatable.Rows(0).Item("DisProTravelFrom").ToString.Trim, SystemCulture)
        Else
            travelFromTextBox.Text = String.Empty
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProTravelTo")) Then
            travelToTextBox.Date = ParseDateTime(datatable.Rows(0).Item("DisProTravelTo").ToString.Trim, SystemCulture)
        Else
            travelToTextBox.Text = String.Empty
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProBookedFromDate")) Then
            bookedFromTextBox.Date = ParseDateTime(datatable.Rows(0).Item("DisProBookedFromDate").ToString.Trim, SystemCulture)
            bookedFromTime.Text = DirectCast(datatable.Rows(0).Item("DisProBookedFromDate"), DateTime).ToString("HH:mm")
        Else
            bookedFromTextBox.Text = String.Empty
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProBookedToDate")) Then
            bookedToTextBox.Date = ParseDateTime(datatable.Rows(0).Item("DisProBookedToDate").ToString.Trim, SystemCulture)
            bookedToTime.Text = DirectCast(datatable.Rows(0).Item("DisProBookedToDate"), DateTime).ToString("HH:mm")
        Else
            bookedToTextBox.Text = String.Empty
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProCkoFromDate")) Then
            pickupFromTextBox.Date = ParseDateTime(datatable.Rows(0).Item("DisProCkoFromDate").ToString.Trim, SystemCulture)
        Else
            pickupFromTextBox.Text = String.Empty
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProCkoToDate")) Then
            pickupToTextBox.Date = ParseDateTime(datatable.Rows(0).Item("DisProCkoToDate").ToString.Trim, SystemCulture)
        Else
            pickupToTextBox.Text = String.Empty
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProMustTravelBetween")) Then
            DisProMustTravelBetweenCheckbox.Checked = Convert.ToBoolean(datatable.Rows(0).Item("DisProMustTravelBetween").ToString)
        Else
            DisProMustTravelBetweenCheckbox.Checked = False
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProOverridable")) Then
            DisProOverridableCheckBox.Checked = Convert.ToBoolean(datatable.Rows(0).Item("DisProOverridable").ToString)
        Else
            DisProOverridableCheckBox.Checked = False
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProIsActive")) Then
            DisProIsActiveCheckBox.Checked = Convert.ToBoolean(datatable.Rows(0).Item("DisProIsActive").ToString)
        Else
            DisProIsActiveCheckBox.Checked = False
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProApplyToOnlyVehicle")) Then
            DisProApplyToOnlyVehicleCheckBox.Checked = Convert.ToBoolean(datatable.Rows(0).Item("DisProApplyToOnlyVehicle").ToString)
        Else
            DisProApplyToOnlyVehicleCheckBox.Checked = False
        End If


        Dim DisProDiscountPer As String = String.Empty
        Dim DisProDiscountAmt As String = String.Empty

        If Not IsDBNull(datatable.Rows(0).Item("DisProDiscountPer")) Then
            DisProDiscountPer = datatable.Rows(0).Item("DisProDiscountPer")
        End If

        If Not IsDBNull(datatable.Rows(0).Item("DisProDiscountAmt")) Then
            DisProDiscountAmt = datatable.Rows(0).Item("DisProDiscountAmt")
        End If

        If Not String.IsNullOrEmpty(DisProDiscountPer) Then
            If (Convert.ToDecimal(DisProDiscountPer) > 0) Then
                discountTextbox.Text = Math.Round(Convert.ToDecimal(DisProDiscountPer), 2)
                radioDiscount.SelectedValue = 1
            End If
        Else
            If (Convert.ToDecimal(DisProDiscountAmt) > 0) Then
                discountTextbox.Text = Math.Round(Convert.ToDecimal(DisProDiscountAmt), 2)
                radioDiscount.SelectedValue = 0
            End If
        End If

        ''rev:mia Oct 16 2014 - addition of PromoCode
        If Not IsDBNull(datatable.Rows(0).Item("DisProIsPromoCode")) Then
            PromoCodeCheckBox.Checked = Convert.ToBoolean(datatable.Rows(0).Item("DisProIsPromoCode").ToString)
        Else
            PromoCodeCheckBox.Checked = False
        End If

    End Sub

    Sub LoadPackageDiscountProfilesDataTable()
        PackageDiscountProfileControl.DisproIdControl = DisProID
        PackageDiscountProfileControl.SuccessfulMessage = SUCCESS_SAVING
        PackageDiscountProfileControl.FailedMessage = EXCECPTION_SAVING
        PackageDiscountProfileControl.MessageInfo = MessageInfo
        PackageDiscountProfileControl.RequeryURL = RequeryURL(DisProID)

        Dim datatable As DataTable = packageProfile.PackageDiscountProfilesDataTable
        If datatable.Rows.Count - 1 = -1 Then Return

        PackageDiscountProfileControl.LoadPackageDiscountProfilesDataTable = datatable
    End Sub

    Sub LoadCodeDataTable()
        Dim datatable As DataTable = packageProfile.CodeDataTable
        If datatable.Rows.Count - 1 = -1 Then Return

        typeDropDown.AppendDataBoundItems = True
        typeDropDown.Items.Add(New ListItem("Please Select Type", -1))
        typeDropDown.DataSource = datatable
        typeDropDown.DataTextField = "CodDesc"
        typeDropDown.DataValueField = "CodId"
        typeDropDown.DataBind()
    End Sub

#End Region

#Region "Button activation"

    Sub ButtonEnabling()
        publishButton.Enabled = False
        publishButton.Visible = False

        updateButton.Enabled = IIf(String.IsNullOrEmpty(DisProID), False, True)
        updateButton.Visible = IIf(String.IsNullOrEmpty(DisProID), False, True)

        createButton.Enabled = IIf(String.IsNullOrEmpty(DisProID), True, False)
        createButton.Visible = IIf(String.IsNullOrEmpty(DisProID), True, False)
        backButton.Enabled = True

        copyButton.Enabled = updateButton.Enabled
        copyButton.Visible = updateButton.Enabled

    End Sub

#End Region

#Region "Validation when Saving"

    Function IsNotValidPackageProfile(ByRef message As String) As Boolean
        IsNotValidPackageProfile = False

        If (String.IsNullOrEmpty(discountcodeTextBox.Text.Trim)) Then
            message = EXCEPTION_DISCOUNTCODE_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(discounttextTextBox.Text.Trim)) Then
            message = EXCEPTION_DISCOUNTTEXT_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(typeDropDown.Text.Trim)) Then
            message = EXCEPTION_TYPE_EMPTY
            Return True
        ElseIf typeDropDown.SelectedItem.Text.Trim.Contains("Please Select Type") = True Then
            message = EXCEPTION_TYPE_EMPTY_DEFAULT
            Return True
        ElseIf typeDropDown.SelectedIndex = 0 Then
            message = EXCEPTION_TYPE_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(bookedFromTextBox.Text.Trim)) Then
            message = EXCEPTION_BOOKEDFROM_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(bookedToTextBox.Text.Trim)) Then
            message = EXCEPTION_BOOKEDTO_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(bookedFromTime.Text.Trim())) Then
            message = EXCEPTION_BOOKEDFROM_TIME_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(bookedToTime.Text.Trim())) Then
            message = EXCEPTION_BOOKEDTO_TIME_EMPTY
            Return True
        End If

        Dim bookedFromDate As DateTime = bookedFromTextBox.Date + bookedFromTime.Time
        Dim bookedToDate As DateTime = bookedToTextBox.Date + bookedToTime.Time

        If (Date.Compare(bookedFromDate, bookedToTextBox.Date) >= 0) Then
            message = EXCEPTION_BOOKEDFROM_RANGE
            Return True
        End If

        If (Date.Compare(bookedToDate, bookedFromTextBox.Date) <= 0) Then
            message = EXCEPTION_BOOKEDTO_RANGE
            Return True
        End If

        ''--

        If (String.IsNullOrEmpty(travelFromTextBox.Text.Trim)) Then
            message = EXCEPTION_TRAVELFROM_EMPTY
            Return True
        End If

        If (String.IsNullOrEmpty(travelToTextBox.Text.Trim)) Then
            message = EXCEPTION_TRAVELTO_EMPTY
            Return True
        End If

        If (Date.Compare(travelFromTextBox.Date, travelToTextBox.Date) >= 0) Then
            message = EXCEPTION_TRAVELFROM_RANGE
            Return True
        End If

        If (Date.Compare(travelToTextBox.Date, travelFromTextBox.Date) <= 0) Then
            message = EXCEPTION_TRAVELTO_RANGE
            Return True
        End If

        ''--
        If (Not String.IsNullOrEmpty(pickupFromTextBox.Text.Trim) And Not String.IsNullOrEmpty(pickupToTextBox.Text.Trim)) Then
            If (Date.Compare(pickupFromTextBox.Date, pickupToTextBox.Date) >= 0) Then
                message = EXCEPTION_PIKCUPFROM_RANGE
                Return True
            End If

            If (Date.Compare(pickupToTextBox.Date, pickupFromTextBox.Date) <= 0) Then
                message = EXCEPTION_PIKCUPTO_RANGE
                Return True
            End If
        End If


        If (String.IsNullOrEmpty(discountTextbox.Text)) Then
            message = EXCEPTION_DISCOUNT_EMPTY
            Return True
        End If

        If (Not String.IsNullOrEmpty(discountTextbox.Text)) Then
            If (CInt(discountTextbox.Text) <= 0) Then
                message = EXCEPTION_DISCOUNT_ZERO
                Return True
            End If
        End If

        If (radioDiscount.SelectedIndex = -1) Then
            message = EXCEPTION_DISCOUNT_OPTION
            Return True
        End If
    End Function

#End Region

#Region "Clear All"

    Sub ClearPackageProfile()

        discountcodeTextBox.Text = String.Empty
        discounttextTextBox.Text = String.Empty
        discountDescriptionTextBox.Text = String.Empty

        typeDropDown.SelectedValue = -1

        bookedFromTextBox.Text = String.Empty
        bookedToTextBox.Text = String.Empty

        bookedFromTime.Text = String.Empty
        bookedToTime.Text = String.Empty

        travelFromTextBox.Text = String.Empty
        travelToTextBox.Text = String.Empty

        pickupFromTextBox.Text = String.Empty
        pickupToTextBox.Text = String.Empty

        discountTextbox.Text = String.Empty

        DisProMustTravelBetweenCheckbox.Checked = True

        DisProOverridableCheckBox.Checked = False

        DisProIsActiveCheckBox.Checked = False
        DisProApplyToOnlyVehicleCheckBox.Checked = True
        radioDiscount.SelectedIndex = 1

    End Sub

#End Region

#Region "New and Üpdates"

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click, createButton.Click
        Dim fromNew As Boolean = String.IsNullOrEmpty(DisProID)
        If Not CanMaintain Then
            Me.AddErrorMessage(Server.HtmlEncode(IIf(fromNew = True, EXCEPTION_NEW_NOROLE, EXCEPTION_UPDATE_NOROLE)))
            Return
        End If
        ''rev:mia https://thlonline.atlassian.net/browse/AURORA-602 - Discount Profile Tile Text restriction.
        If (chkEnablePromoTile.Checked) Then
            If (Not String.IsNullOrEmpty(txtPromoTileText.Text)) Then
                Dim items As String() = txtPromoTileText.Text.Split(Chr(10))
                If (items.Length - 1 = 0) Then
                    Me.AddErrorMessage("The Tile text is requiring two lines of information")
                    Return
                End If
                Dim ctr As Integer = 0
                For Each item As String In items
                    If (String.IsNullOrEmpty(item)) Then
                        If (ctr <= 1) Then
                            Me.AddErrorMessage("The Tile text is requiring two lines of information")
                            Return
                        End If
                    Else
                        ctr += 1
                    End If
                Next
            Else
                Me.AddErrorMessage("The Tile text is requiring two lines of information")
                Return
            End If
        End If

        Dim message As String = String.Empty
        Dim IsNotValid As Boolean = IsNotValidPackageProfile(message)

        Try
            If (IsNotValid = True) Then
                Throw New Aurora.Common.ValidationException(message)
            End If

            Dim result As Integer = packageProfile.PackageInsertUpdateSingleProfile(DiscountProfilesDataTableCandidate())
            If (result >= 1) Then

                If (fromNew) Then
                    MessageInfo = SUCCESS_SAVING
                    Response.Redirect(RequeryURL(result))
                Else
                    MessageInfo = String.Empty
                    Me.AddInformationMessage(SUCCESS_SAVING)
                    LoadAllDataTables()
                End If

            Else
                Me.AddErrorMessage(EXCECPTION_SAVING)
            End If

        Catch ex As Aurora.Common.ValidationException
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
        Catch sys As Exception
            Me.AddErrorMessage(Server.HtmlEncode(sys.Message))
        End Try
    End Sub

#End Region

#Region " Back Button"

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(String.Concat(SEARCH_PAGE_BACK, IIf(Request.QueryString("DisProID") Is Nothing, "", "?DisProID=" & Request.QueryString("DisProID"))))
    End Sub

#End Region

    Protected Function buildQueryString(ByVal replaceKey As String, ByVal replaceValue As String) As String
        Dim builder As New StringBuilder()

        For Each key As String In Request.QueryString.AllKeys
            Dim value As String = Request.QueryString(key)
            If builder.Length = 0 Then
                builder.Append("?")
            Else
                builder.Append("&")
            End If

            builder.Append(key)
            builder.Append("=")

            If replaceKey = key Then
                builder.Append(replaceValue)
            Else
                builder.Append(value)
            End If
        Next

        Return builder.ToString()
    End Function

    Protected Sub copyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles copyButton.Click
        Dim profileId As Long

        If Not Long.TryParse(DisProID, profileId) Then
            Return
        End If

        Dim newProfileId As Long = packageProfile.CopyDiscountProfile(profileId, UserCode)

        Dim query As String = buildQueryString("DisProID", newProfileId.ToString())

        If Not String.IsNullOrEmpty(query) Then
            Response.Redirect("EditPackageDiscount.aspx" + query)
        Else
            Response.Redirect("EditPackageDiscount.aspx")
        End If
    End Sub

End Class
