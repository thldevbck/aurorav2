﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="EditPackageDiscount.aspx.vb" Inherits="Package_EditPackageDiscount" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/PackageDiscountProfileControl.ascx" TagName="PackageDiscountProfileControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/PackageDiscountProductControl.ascx" TagName="PackageDiscountProductControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/PackageDiscountAgentControl.ascx" TagName="PackageDiscountAgentControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/DiscountLocationsControl.ascx" TagName="DiscountLocationsControl"
    TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        .listboxclass
        {
            font-family: 'Courier New'; /* well known monospaced font */
            width: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="../Booking/Jquery/jquery.validate.js" type="text/javascript"></script>
    <script src="JS/PromoTileJS.js" type="text/javascript"></script>
    <script src="JS/EditPackageDiscount.js" type="text/javascript"></script>
     <script  type="text/javascript">
         var pbControl = null;
         var prm = Sys.WebForms.PageRequestManager.getInstance();

         prm.add_beginRequest(BeginRequestHandler);
         prm.add_endRequest(EndRequestHandler);


         Sys.Application.add_load(loadhandler);
         function loadhandler() { }

         function BeginRequestHandler(sender, args) {
             pbControl = args.get_postBackElement();
         }

         function EndRequestHandler(sender, args) {
             if (pbControl.id.indexOf('Button') > -1) {
                 (function () {
                     var oText = document.getElementById('<%=txtPromoTileText.ClientID %>'),
                                    oLink = document.getElementById('<%=lnkPromoTile.ClientID %>'),
                                    oCheck = document.getElementById('<%=chkEnablePromoTile.ClientID %>');
                     oLink.style.display = 'none';

                     oText.onchange = function () {
                         this.value = this.value.replace(/(?:\||(?:<[^>]+>))/g, '');
                     }

                     oCheck.onchange = oCheck.onkeydown = oCheck.onclick =
                                        function () {
                                            oText.disabled = !this.checked || this.disabled;
                                            oLink.style.display = this.checked ? '' : 'none';
                                        };

                     oCheck.onchange();
                 })();
             }

         }

         </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="packageEditDiscountPanel" runat="Server" Width="780px" CssClass="packageEdit">
        <asp:UpdatePanel runat="server" ID="packageDiscountUpdatePanel">
            <ContentTemplate>
                <table style="width: 100%;" cellpadding="2" cellspacing="0">
                    <%--Discount Code--%>
                    <tr>
                        <td style="width: 150px">
                            Discount Code:
                        </td>
                        <td style="width: 250px">
                            <asp:TextBox ID="discountcodeTextBox" name="discountcodeTextBox" runat="server" Style="width: 150px"
                                MaxLength="15" />&nbsp;*
                        </td>
                        <td style="width: 150px">
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <%--Discount Text--%>
                    <tr>
                        <td>
                            Discount Text:
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="discounttextTextBox" runat="server" Style="width: 600px" MaxLength="64" />&nbsp;*
                        </td>
                    </tr>

                    <%--Discount Description Text--%>
                    <tr>
                        <td>
                            Description:
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="discountDescriptionTextBox" runat="server" Style="width: 600px" MaxLength="64" TextMode="MultiLine" Rows="4"/>
                        </td>
                    </tr>

                    <%--Promo Tile--%>
                    <tr>
                        <td>
                            Tile text
                        </td>
                        <td colspan="3">
                            <asp:CheckBox ID="chkEnablePromoTile" EnableViewState="false" Text="Enable Tile on B2C"
                                runat="server" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:HyperLink ID="lnkPromoTile" Target="_blank" EnableViewState="false" runat="server">Show Tile</asp:HyperLink>
                            <br />
                            <asp:TextBox ID="txtPromoTileText" Enabled="false" Style="width: 600px" TextMode="MultiLine"
                                EnableViewState="false" Rows="4" runat="server" MaxLength="256"></asp:TextBox>
                            <script type="text/javascript">
                                (function () {
                                    var oText = document.getElementById('<%=txtPromoTileText.ClientID %>'),
                                    oLink = document.getElementById('<%=lnkPromoTile.ClientID %>'),
                                    oCheck = document.getElementById('<%=chkEnablePromoTile.ClientID %>');
                                    oLink.style.display = 'none';

                                    oText.onchange = function () {
                                        this.value = this.value.replace(/(?:\||(?:<[^>]+>))/g, '');
                                    }

                                    oCheck.onchange = oCheck.onkeydown = oCheck.onclick =
                                        function () {
                                            oText.disabled = !this.checked || this.disabled;
                                            oLink.style.display = this.checked ? '' : 'none';
                                        };

                                    oCheck.onchange();
                                })();

                                MonitorTextArea('<%=txtPromoTileText.ClientID %>');
                                InitPromoLink('<%=lnkPromoTile.ClientID %>', '<%=txtPromoTileText.ClientID %>', '<%=bookedToTextBox.DateFormat %>',
                                            '<%=bookedToTextBox.ClientID %>', '<%=travelFromTextBox.ClientID %>', '<%=travelToTextBox.ClientID %>', '');
                            </script>
                        </td>
                    </tr>
                    <%--Type--%>
                    <tr>
                        <td>
                            Type:
                        </td>
                        <td>
                            <asp:DropDownList ID="typeDropDown" runat="server" Style="width: 150px">
                                <asp:ListItem />
                            </asp:DropDownList>
                            &nbsp;*
                        </td>
                    </tr>
                </table>
                <br />
                <table style="width: 100%;" cellpadding="2" cellspacing="0" id="statusTable" runat="server"
                    bgcolor='#ddffdd'>
                    <tr>
                        <td style="width: 150px">
                            Booked From:
                        </td>
                        <td style="width: 250px">
                            <uc1:DateControl ID="bookedFromTextBox" runat="server" Nullable="True" />
                            <uc1:TimeControl ID="bookedFromTime" Text="00:00" runat="server" Nullable="true" />&nbsp;*
                        </td>
                        <td style="width: 150px">
                            To:
                        </td>
                        <td>
                            <uc1:DateControl ID="bookedToTextBox" runat="server" Nullable="True" />
                            <uc1:TimeControl ID="bookedToTime" Text="23:59" runat="server" Nullable="true" />&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Travel From:
                        </td>
                        <td>
                            <uc1:DateControl ID="travelFromTextBox" runat="server" Nullable="True" />&nbsp;*
                        </td>
                        <td>
                            To:
                        </td>
                        <td>
                            <uc1:DateControl ID="travelToTextBox" runat="server" Nullable="True" />&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Pickup From:
                        </td>
                        <td>
                            <uc1:DateControl ID="pickupFromTextBox" runat="server" Nullable="True" />
                        </td>
                        <td>
                            To:
                        </td>
                        <td>
                            <uc1:DateControl ID="pickupToTextBox" runat="server" Nullable="True" />
                        </td>
                    </tr>
                </table>
                <br />
                <table style="width: 100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width: 150px">
                            Flags:
                        </td>
                        <td>
                            <table>
                                <tr>
                                   <td style="width: 250px">
                                        <asp:CheckBox runat="server" ID="DisProMustTravelBetweenCheckbox" Checked="true"
                                            Text="Must Travel Between these dates"  Enabled="false"/>
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="DisProOverridableCheckBox" Checked="false" Text="Override All Discounts"  Enabled="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:CheckBox runat="server" ID="DisProIsActiveCheckBox" Checked="false" Text="Active" />
                                    </td>
                                    <td>
                                        <asp:CheckBox runat="server" ID="DisProApplyToOnlyVehicleCheckBox" Checked="true"
                                            Text="Apply Discount Only to vehicles" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <br />
                <table style="width: 100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width: 150px">
                            Discount:
                        </td>
                        <td>
                            <asp:TextBox runat="server" Style="width: 50px" MaxLength="6" ID="discountTextbox" />
                            &nbsp;*
                            <ajaxToolkit:FilteredTextBoxExtender ID="discountTextboxajax" runat="server" FilterType="Numbers,Custom"
                             TargetControlID="discountTextbox" ValidChars="." />
                           
                        </td>
                        <td>
                          <asp:RadioButtonList ID="radioDiscount" RepeatDirection="Horizontal" RepeatColumns="4"
                                RepeatLayout="Table" CssClass="repeatLabelTable" runat="server">
                                <asp:ListItem Value="0" Selected="False" Enabled="true">Amount</asp:ListItem>
                                <asp:ListItem Value="1" Enabled ="true" Selected="true">%</asp:ListItem>
                            </asp:RadioButtonList>                           
                        </td>
                         <%--rev:mia Oct 16 2014 - addition of PromoCode--%>
                        <td>
                             <asp:CheckBox runat="server" ID="PromoCodeCheckBox" Checked="false"
                                            Text="Promo Code" />
                        </td>
                    </tr>
                </table>
                <br />
                <table style="width: 100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td align="right" style="border-width: 1px 0px;">
                            <asp:Button ID="copyButton" runat="server" Text="Copy" CssClass="Button_Standard"
                                Visible="False" />
                            <asp:Button ID="publishButton" runat="server" Text="Publish" CssClass="Button_Standard" />
                            <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                                Visible="False" />
                            <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                                Visible="False" />
                            <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <asp:Panel ID="packageTabContainer" runat="server">
            <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="0" Width="100%">
                <ajaxToolkit:TabPanel runat="server" ID="PackageTabPanel" HeaderText="Packages">
                    <ContentTemplate>
                        <uc1:PackageDiscountProfileControl ID="PackageDiscountProfileControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="ProductTabPanel1" HeaderText="Product">
                    <ContentTemplate>
                        <uc1:PackageDiscountProductControl ID="PackageDiscountProductControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="AgentTabPanel1" HeaderText="Agent">
                    <ContentTemplate>
                        <uc1:PackageDiscountAgentControl ID="PackageDiscountAgentControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="TabPanel1" HeaderText="Locations">
                    <ContentTemplate>
                        <uc1:DiscountLocationsControl ID="dicountLocationsControl" runat="server"/>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        </asp:Panel>
    </asp:Panel>
</asp:Content>
