Imports System.Collections.Generic

Imports Aurora.Product.Data
Imports Aurora.Product.Data.ProductDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PackageMaintenance)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.PackageEnquiry)> _
<AuroraPageTitle("Edit Package - Add Saleable Product")> _
Partial Class Package_SaleableProductPicker
    Inherits AuroraPage

    Private _pkgId As String
    Private _packageDataSet As New Aurora.Package.Data.PackageDataSet()
    Private _packageRow As Aurora.Package.Data.PackageDataSet.PackageRow
    Private _productDataSet As New ProductDataSet()
    Private _checkBoxesBySapId As New Dictionary(Of String, CheckBox)

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _packageDataSet.EnforceConstraints = False

        Aurora.Package.Data.DataRepository.GetLookups(_packageDataSet, Me.CompanyCode)

        _pkgId = ("" & Request.QueryString("pkgId")).Trim()
        If String.IsNullOrEmpty(_pkgId) Then
            Throw New Exception("Package information not supplied")
        End If
        Aurora.Package.Data.DataRepository.GetPackageByPkgId(_packageDataSet, Me.CompanyCode, _pkgId)
        _packageRow = _packageDataSet.Package.FindByPkgId(_pkgId)
        If _packageRow Is Nothing Then
            Throw New Exception("No package found")
        End If

        packageTextBox.Text = _packageRow.Description
        packageTextBox.BackColor = _packageRow.StatusColor(Date.Now)

        _productDataSet.EnforceConstraints = False

        DataRepository.GetProductLookups(_productDataSet, Me.CompanyCode)
        DataRepository.GetSaleableProductLookups(_productDataSet, Me.CompanyCode)

        brandDropDown.Items.Clear()
        For Each brandRow As BrandRow In _productDataSet.Brand
            If brandRow.BrdCode = _packageRow.PkgBrdCode OrElse brandRow.BrdIsGeneric Then
                brandDropDown.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
            End If
        Next
        brandDropDown.SelectedValue = _packageRow.PkgBrdCode

        typeDropDown.Items.Clear()
        typeDropDown.Items.Add(New ListItem("(All)", ""))
        For Each typeRow As TypeRow In _productDataSet.Type
            typeDropDown.Items.Add(New ListItem(typeRow.TypCode & " - " & typeRow.TypDesc, typeRow.TypId))
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem("(All)", ""))
        For Each countryRow As CountryRow In _productDataSet.Country
            countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        Next
        'countryTextBox.Text = _packageRow.CountryRow.Description
        countryDropDown.SelectedValue = _packageRow.CountryRow.Description.Split("-")(0).Trim().ToUpper()
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        shortNameTextBox.Text = ""
        nameTextBox.Text = ""
        typeDropDown.SelectedIndex = 0
        'brandDropDown.SelectedIndex = 0
        packagePicker.DataId = Nothing
        packagePicker.Text = ""
        statusRadioButtonList.SelectedIndex = 0
        For Each name As String In New String() {"sapIsBase", "sapIsFlex"}
            flagsCheckBoxList.Items.FindByValue(name).Selected = False
        Next
    End Sub

    Private Sub BuildSearch()
        Dim currentDate As Date = Date.Now

        _checkBoxesBySapId.Clear()
        While searchResultTable.Rows.Count > 1
            searchResultTable.Rows.RemoveAt(1)
        End While

        Dim prdShortName As String = shortNameTextBox.Text.Trim()
        Dim sapSuffix As New Nullable(Of Integer)
        Dim prdShortNameArray As String() = prdShortName.Split("-")
        If prdShortNameArray.Length > 1 Then
            Dim prdShortNameNew As String = prdShortNameArray(0).Trim()
            For i As Integer = 1 To prdShortNameArray.Length - 2
                prdShortNameNew &= "-" & prdShortNameArray(i).Trim()
            Next
            Dim sapSuffixNew As Integer = -1
            Integer.TryParse(prdShortNameArray(prdShortNameArray.Length - 1), sapSuffixNew)
            If sapSuffixNew > 0 Then
                prdShortName = prdShortNameNew
                sapSuffix = New Nullable(Of Integer)(sapSuffixNew)
            End If
        End If

        Dim sapIsBase As New Nullable(Of Boolean)
        If flagsCheckBoxList.Items.FindByValue("sapIsBase").Selected Then sapIsBase = New Nullable(Of Boolean)(True)

        Dim sapIsFlex As New Nullable(Of Boolean)
        If flagsCheckBoxList.Items.FindByValue("sapIsFlex").Selected Then sapIsFlex = New Nullable(Of Boolean)(True)

        Dim pkgId As String = Nothing
        If Not String.IsNullOrEmpty(packagePicker.DataId) AndAlso Not String.IsNullOrEmpty(packagePicker.Text) Then
            pkgId = packagePicker.DataId
        End If

        Dim sapStatus As String = Nothing
        Dim currentFutureOnly As Boolean = False
        'If statusRadioButtonList.SelectedValue = "Current" Then
        '    sapStatus = "Active"
        '    currentFutureOnly = True
        'ElseIf statusRadioButtonList.SelectedValue = "Current" Then
        '    sapStatus = "Active"
        'ElseIf statusRadioButtonList.SelectedValue = "Pending" Then
        '    sapStatus = "Pending"
        'ElseIf statusRadioButtonList.SelectedValue = "Inactive" Then
        '    sapStatus = "Inactive"
        'End If

        Select Case statusRadioButtonList.SelectedValue
            Case "Active-Current"
                sapStatus = "Active"
                currentFutureOnly = True
            Case "Active-Future"
                sapStatus = "Active"
                currentFutureOnly = True
            Case "Active-Past"
                sapStatus = "Active"
            Case "Pending"
                sapStatus = "Pending"
            Case "Inactive"
                sapStatus = "Inactive"
            Case "All"
                sapStatus = ""
        End Select

        _productDataSet.Product.Clear()
        _productDataSet.SaleableProduct.Clear()
        _productDataSet.ProductAttribute.Clear()
        _productDataSet.Package.Clear()
        _productDataSet.PackageProduct.Clear()


        'DataRepository.SearchSaleableProduct(_productDataSet, _
        '    Me.CompanyCode, _
        '    Nothing, _
        '    typeDropDown.SelectedValue, _
        '    brandDropDown.SelectedValue, _
        '    prdShortName, _
        '    nameTextBox.Text.Trim(), _
        '    _packageRow.PkgCtyCode, _
        '    sapSuffix, _
        '    sapStatus, _
        '    sapIsBase, _
        '    sapIsFlex, _
        '    pkgId, _
        '    currentFutureOnly, _
        '    True, _
        '    False)

        DataRepository.SearchSaleableProduct(_productDataSet, _
            Me.CompanyCode, _
            Nothing, _
            typeDropDown.SelectedValue, _
            brandDropDown.SelectedValue, _
            prdShortName, _
            nameTextBox.Text.Trim(), _
            countryDropDown.SelectedValue, _
            sapSuffix, _
            sapStatus, _
            sapIsBase, _
            sapIsFlex, _
            pkgId, _
            currentFutureOnly, _
            True, _
            False)

        Dim index As Integer = 0
        For Each saleableProductRow As SaleableProductRow In _productDataSet.SaleableProduct
            'If (statusRadioButtonList.SelectedValue = "Current" AndAlso Not (saleableProductRow.HasCurrent(currentDate) OrElse saleableProductRow.HasFuture(currentDate))) _
            ' OrElse (statusRadioButtonList.SelectedValue = "Active" AndAlso Not saleableProductRow.IsActive()) _
            ' OrElse (statusRadioButtonList.SelectedValue = "Pending" AndAlso Not saleableProductRow.IsPending()) _
            ' OrElse (statusRadioButtonList.SelectedValue = "Inactive" AndAlso Not saleableProductRow.IsInactive()) Then
            '    Continue For
            'End If

            Select Case statusRadioButtonList.SelectedValue
                Case "Active-Current"
                    If Not saleableProductRow.HasCurrent(currentDate) Or Not saleableProductRow.IsActive() Then
                        Continue For
                    End If
                Case "Active-Future"
                    If Not saleableProductRow.HasFuture(currentDate) Or Not saleableProductRow.IsActive() Then
                        Continue For
                    End If
                Case "Active-Past"
                    If saleableProductRow.HasFuture(currentDate) Or saleableProductRow.HasCurrent(currentDate) Or Not saleableProductRow.IsActive() Then
                        Continue For
                    End If
                Case "Pending"
                    If Not saleableProductRow.IsPending() Then
                        Continue For
                    End If
                Case "Inactive"
                    If Not saleableProductRow.IsInactive() Then
                        Continue For
                    End If
            End Select

            Dim packageProductRow As PackageProductRow = saleableProductRow.PackageProductRowByPkgId(_pkgId)

            Dim tableRow As New TableRow()
            tableRow.BackColor = saleableProductRow.StatusColor(currentDate)
            searchResultTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)
            Dim checkBox As New CheckBox
            checkBox.ID = "select" & saleableProductRow.SapId & "CheckBox"
            If packageProductRow IsNot Nothing Then
                checkBox.Enabled = False
                checkBox.Checked = True
            End If
            selectCell.Controls.Add(checkBox)
            If packageProductRow Is Nothing Then
                _checkBoxesBySapId.Add(saleableProductRow.SapId, checkBox)
            End If

            Dim nameCell As New TableCell()
            Dim nameHyperLink As New HyperLink()
            nameHyperLink.Text = Server.HtmlEncode(saleableProductRow.ProductRow.PrdShortName & " - " & saleableProductRow.SapSuffix.ToString())
            nameHyperLink.NavigateUrl = ProductUserControl.MakeSaleableProductUrl(Me, prdId:=saleableProductRow.SapPrdId, sapId:=saleableProductRow.SapId) & "&BackUrl=" & Server.UrlEncode(Me.Request.RawUrl)
            nameCell.Controls.Add(nameHyperLink)
            tableRow.Controls.Add(nameCell)

            nameCell = New TableCell
            nameCell.Text = Server.HtmlEncode(saleableProductRow.ProductRow.PrdName)
            tableRow.Controls.Add(nameCell)

            Dim typeCell As New TableCell
            If packageProductRow IsNot Nothing Then
                If Not packageProductRow.IsPplCopyCreateLinkNull Then typeCell.Text = Server.HtmlEncode(packageProductRow.PplCopyCreateLink)
            Else
                typeCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If
            tableRow.Controls.Add(typeCell)

            Dim brandCell As New TableCell
            brandCell.Text = Server.HtmlEncode(saleableProductRow.ProductRow.BrandRow.Description)
            tableRow.Controls.Add(brandCell)

            Dim flagsCell As New TableCell()
            Dim flagsList As New List(Of String)
            If Not saleableProductRow.IsSapIsBaseNull AndAlso saleableProductRow.SapIsBase Then flagsList.Add("Base")
            If Not saleableProductRow.IsSapIsFlexNull AndAlso saleableProductRow.SapIsFlex Then flagsList.Add("Flex")
            flagsCell.Text = String.Join(" , ", flagsList.ToArray())
            tableRow.Controls.Add(flagsCell)

            Dim statusCell As New TableCell
            statusCell.Text = Server.HtmlEncode(saleableProductRow.StatusText(currentDate))
            tableRow.Controls.Add(statusCell)

            index += 1
        Next

        If index <= 0 Then
            searchResultLabel.Text = "No Saleable products found"
            searchResultLabel.Visible = True
            packageProductPanel.Visible = False
        ElseIf index = 1 Then
            searchResultLabel.Text = index & " Saleable product found"
            searchResultLabel.Visible = True
            packageProductPanel.Visible = True
        Else
            searchResultLabel.Text = index & " Saleable products found"
            searchResultLabel.Visible = True
            packageProductPanel.Visible = True
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then BuildForm(False)

        If searchCheckBox.Checked Then BuildSearch()
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        If Not searchCheckBox.Checked Then
            searchCheckBox.Checked = True
            BuildSearch()
        End If
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        shortNameTextBox.Text = ""
        nameTextBox.Text = ""
        typeDropDown.SelectedIndex = 0
        brandDropDown.SelectedIndex = 0
        packagePicker.DataId = Nothing
        packagePicker.Text = ""
        statusRadioButtonList.SelectedIndex = 0
        For Each listItem As ListItem In flagsCheckBoxList.Items
            listItem.Selected = False
        Next
        searchResultLabel.Text = ""
        packageProductPanel.Visible = False
        searchCheckBox.Checked = False
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(PackageUserControl.MakePackageUrl(Me, _pkgId))
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        Dim created As Boolean = False
        For Each sapId As String In _checkBoxesBySapId.Keys
            Dim checkBox As CheckBox = _checkBoxesBySapId(sapId)
            If Not checkBox.Enabled OrElse Not checkBox.Checked Then Continue For

            Aurora.Package.Data.DataRepository.CreatePackageProduct( _
                _packageRow.PkgId, _
                sapId, _
                copyCreateLinkRadioButtonList.SelectedValue, _
                Me.UserCode, _
                Me.PrgmName)

            created = True
        Next
        If created Then
            BuildSearch()
            Me.SetInformationShortMessage("Updated Successfully")
        End If

    End Sub
End Class
