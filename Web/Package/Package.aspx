<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false" CodeFile="Package.aspx.vb" Inherits="Package_Package" Title="Untitled Page" ValidateRequest="False" %>

<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<%@ Register Src="Controls/SaleableProductsControl.ascx" TagName="SaleableProductsControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/FlexLevelControl.ascx" TagName="FlexLevelControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/AgentControl.ascx" TagName="AgentControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/MarketControl.ascx" TagName="MarketControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/LocationsControl.ascx" TagName="LocationsControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/NonAvailabilityControl.ascx" TagName="NonAvailabilityControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/WeeklyProfileControl.ascx" TagName="WeeklyProfileControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/NoteControl.ascx" TagName="NoteControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/PackageSetupControl.ascx" TagName="PackageSetupControl" TagPrefix="uc1" %>

<%@ Register Src="Controls/DiscountProfilesControl.ascx" TagName="DiscountProfiles" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="StylePlaceHolder" runat="Server">
<style type="text/css">
.ajax__tab_panel
{
    min-height:350px; 
    height:auto !important; 
    height:350px;
}
</style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
<script language ="javascript" type="text/javascript">

    function onPageSearch()
    {
            WindowNavigate('Package/Search.aspx','pkgId=' + '<%= request.querystring("pkgId") %>');
    }
    
    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();  //the control causing the postback
        
        if(pbControl.id.indexOf('RemoveButton') > -1)
        {   
            pbControl.disabled = true;
        }
        if(pbControl.id.indexOf('SaveButton') > -1)
        {   
            pbControl.disabled = true;
        }
        
        if(pbControl.id.indexOf('PackageSetupControl_PackageParentIDDropdown') > -1)
        {   
            $get('ctl00_ContentPlaceHolder_updateButton').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnSave').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnReset').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnRemove').disabled = true;
        }
        
        if(pbControl.id.indexOf('PackageSetupControl_PackageParentIDDropdown') > -1 || pbControl.id.indexOf('MappedToCheckboxList') > -1 || pbControl.id.indexOf('CountriesDropdown') > -1)
        {   
            $get('ctl00_ContentPlaceHolder_updateButton').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnSave').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnReset').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnRemove').disabled = true;
        }

        // for the flex level control
        if (pbControl.id.indexOf('flexLevelUpdateButton') > -1 || pbControl.id.indexOf('flexLevelCreateButton') > -1) {
            pbControl.disabled = true;
        }
        // for the flex level control
        
    }

    function EndRequestHandler(sender, args) {
        if(pbControl.id.indexOf('RemoveButton') > -1)
        {   
            pbControl.disabled = false;
        }
        if(pbControl.id.indexOf('SaveButton') > -1)
        {   
            pbControl.disabled = false;
        }
        if(pbControl.id.indexOf('PackageSetupControl_PackageParentIDDropdown') > -1)
        {   
            $get('ctl00_ContentPlaceHolder_updateButton').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnSave').disabled = false;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnReset').disabled = false;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnRemove').disabled = false;
        }
        
        if(pbControl.id.indexOf('PackageSetupControl_PackageParentIDDropdown') > -1 || pbControl.id.indexOf('MappedToCheckboxList') > -1 || pbControl.id.indexOf('CountriesDropdown') > -1)
        {   
            $get('ctl00_ContentPlaceHolder_updateButton').disabled = true;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnSave').disabled = false;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnReset').disabled = false;
            $get('ctl00_ContentPlaceHolder_tabs_PackageSetupPanel_PackageSetupControl_btnRemove').disabled = false;
        }

        // for the flex level control
        if (pbControl.id.indexOf('flexLevelUpdateButton') > -1 || pbControl.id.indexOf('flexLevelCreateButton') > -1) {
            pbControl.disabled = false;
        }
        // for the flex level control

        pbControl = null;
    }

    var MonitorTextArea = function (textAreaId) {
        var oTimer, iCharsPerLine = 40, iMaxLines = 4,
        oElement = document.getElementById(textAreaId),
        iNewLineChars, oTempEl;

        oTempEl = document.createElement('textarea');
        oTempEl.value = '\n';
        iNewLineChars = oTempEl.value.length;
        oTempEl = null;

        if (!oElement)
            return;

        var GetCaretPosition = function (element) {
            var oRange = element.createTextRange(),
            oRefRange = element.createTextRange(),
            oDupRange = document.selection.createRange().duplicate(),
            sChar, iOffset = 0,
	        oBookmark = oDupRange.getBookmark();

            oRange.moveToBookmark(oBookmark);
            oRange.moveStart('textedit', -1);

            oRefRange.moveToBookmark(oBookmark);
            if (element.value.length != 0) {
                do {
                    if (oRefRange.compareEndPoints('StartToStart', oRange) == 1) {
                        oRefRange.moveStart('character', -1);
                    } else break;

                    oDupRange.moveStart('character', -1);
                    sChar = oDupRange.text;
                    oDupRange.moveEnd('character', -1);
                    if (sChar == '')
                        iOffset += iNewLineChars;
                    else break;
                } while (true);
            }

            return oRange.text.length + iOffset;
        },
        SetCaretPosition = function (element, position) {
            var oRange = element.createTextRange();
            oRange.collapse(true);
            oRange.moveEnd('character', position);
            oRange.moveStart('character', position);
            oRange.select();
        },
        CountNewLines = function (value, endIndex) {
            var iCount = 0, i = 0;
            while (i < endIndex){
                i = value.indexOf('\r\n', i);
                if (i == -1 || i >= endIndex)
                    break;
                iCount++;
                i += 2;
            };
            return iCount;
        },
        FindWordStart = function (text, index) {
            var iChar, iIndex = index;
            while (
                iIndex > -1
                && (
                    ((iChar = text.charCodeAt(iIndex)) > 47 && iChar < 58)
                    ||
                    (iChar > 64 && iChar < 91)
                    ||
                    (iChar > 96 && iChar < 123)
                   )
                ) {
                iIndex--;
            }
            if (iIndex == -1 || iIndex >= index || iChar == 10 || iChar == 13)
                return index;
            return iIndex + 1;
        },
        FormatTextarea = function () {
            clearTimeout(oTimer);
            var sValue = oElement.value, iCharIndex = 0, iLen = sValue.length,
            iSearchIndex, iCaretPos = -1, bReformatted, sSubstr, iTemp, bUpdate, iCaretOffset = 0, iLines = 0, iLinesOffset = 0;

            while (true) {
                if (iCharIndex < iLen) {
                    iSearchIndex = sValue.indexOf('\n', iCharIndex);
                    if (iSearchIndex > -1 && iSearchIndex < iCharIndex + iCharsPerLine) {
                        iCharIndex = iSearchIndex + 1;

                        if (++iLines >= iMaxLines) {
                            sValue = sValue.substring(0, iCharIndex - iNewLineChars);
                            iLen = sValue.length;
                            if (!bUpdate) {
                                bUpdate = true;
                                iCaretPos = GetCaretPosition(oElement);
                            }
                        }

                        continue;
                    }

                    iCharIndex += iCharsPerLine;
                    if (iCharIndex < iLen) {

                        if ((sValue.charCodeAt(iCharIndex) != 10 && sValue.charCodeAt(iCharIndex) != 13)
                            || iLines >= iMaxLines - 1) {
                            if (!bUpdate) {
                                bUpdate = true;
                                iCaretPos = GetCaretPosition(oElement);
                            }

                            if (++iLines < iMaxLines) {
                                iCharIndex = FindWordStart(sValue, iCharIndex);

                                sSubstr = sValue.substring(iCharIndex);

                                if (!bReformatted) {
                                    iTemp = sSubstr.length;
                                    sSubstr = sSubstr.replace(/(?:(?:\r\n)|\n)/g, '').replace(/\s\s*$/, '');
                                    iLen = iLen - iTemp + sSubstr.length;
                                    bReformatted = true;
                                }

                                sValue = sValue.substring(0, iCharIndex) + '\n' + sSubstr;
                                iLen++;
                                iCharIndex += 1;

                                if (iCharIndex <= iCaretPos) {
                                    iCaretPos++;
                                    iCaretOffset += iNewLineChars - 1;
                                }
                            }
                            else {
                                sValue = sValue.substring(0, iCharIndex);
                                iLen = sValue.length;
                            }

                            continue;
                        }
                        iCharIndex += iNewLineChars;
                        iLines++;
                    } else break;
                } else break;
            }
            if (bUpdate) {
                oElement.value = sValue;
                iCaretPos += iCaretOffset;

                if (iNewLineChars > 1) {
                    iLinesOffset = CountNewLines(oElement.value, iCaretPos);
                    iCaretPos -= iLinesOffset;
                }
                iLen = oElement.value.length - iLinesOffset;
                if (iCaretPos >= iLen + 1)
                    iCaretPos = iLen;
                SetCaretPosition(oElement, iCaretPos);
            }
            oTimer = setTimeout(FormatTextarea, 50);

        };
        FormatTextarea();
    }

    var InitPromoLink = function (linkElementId, textAreaElementId, dateFormat, bookBeforeElementId, travelFromElementId, travelToElementId, brandElementId) {

        dateFormat = dateFormat.replace(/[\/\\\-\,\.\:\;]/g, '').replace(/[Mm]+/g, 'm').replace(/[Dd]+/g, 'd').replace(/[Yy]+/g, 'y');

        var aMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        iMaximumCharacters = 80,
        oLink = document.getElementById(linkElementId),
        oTextArea,
        oBookBefore,
        oTravelFrom,
        oTravelTo,
        oBrand,
        GetDateString = function (dateElement) {
            //Parsing can be sensitive to browser version and, possibly, platform.
            //It doesn't pick up some formats, such as 'dd/mm/yyyy'.
            //var oDate = new Date(Date.parse(dateElement.value));

            var aParts = dateElement.value.match(/\d+/g);

            //It's uncertain whether the native date methods will return the same result in different browser verions, on different platforms.
            //Constructing the date from separate parts can be more reliable.
            return aParts[dateFormat.indexOf('d')] + ' ' + aMonths[aParts[dateFormat.indexOf('m')] - 1] + ' ' + aParts[dateFormat.indexOf('y')];
        },
        IsAlphaNumeric = function (charCode) {
            return (charCode > 47 && charCode < 58)
                    ||
                   (charCode > 64 && charCode < 91)
                    ||
                   (charCode > 96 && charCode < 123);
        },
        EncodeURIComponent = function (value) {
            if (!value || typeof value !== 'string')
                return null;

            value = value.replace(/%/g, '%25');

            var i = -1, iChar, sChar, sEncodedChar, iLen = value.length;
            while (++i < iLen) {
                iChar = value.charCodeAt(i);

                if (!IsAlphaNumeric(iChar)
                    && iChar != 45
                    && iChar != 95
                    && iChar != 37
                   ) {
                    sChar = value.charAt(i);
                    sEncodedChar = iChar.toString(16).toUpperCase();
                    while (value.indexOf(sChar) > -1)
                        value = value.replace(sChar, '%' + sEncodedChar);
                    iLen = value.length;
                }
            }

            return value;
        },
        UpdateLink = function () {
            var sHref = oLink.href, sQuery, iCharIndex = sHref.indexOf('?', sHref), sText, sHeader, sTileText = '', iCharIndex, sBrand;

            if (!oTextArea)
                oTextArea = document.getElementById(textAreaElementId);
            if (!oBookBefore)
                oBookBefore = document.getElementById(bookBeforeElementId);
            if (!oTravelFrom)
                oTravelFrom = document.getElementById(travelFromElementId);
            if (!oTravelTo)
                oTravelTo = document.getElementById(travelToElementId);
            if (!oBrand)
                oBrand = document.getElementById(brandElementId);

            sBrand = oBrand.options[oBrand.selectedIndex].value;

            if (!sBrand)
                return;

            sBrand = sBrand.replace(/^\s\s*/, '').replace(/\s\s*$/, '');

            if (!sBrand)
                return;

            if (iCharIndex > -1)
                sHref = sHref.substring(0, iCharIndex);

            sText = oTextArea.value.replace(/(?:(?:\r\n)|\n)/g, '|');

            if (sText.length > iMaximumCharacters)
                sText = sText.substring(0, iMaximumCharacters);

            iCharIndex = sText.indexOf('|');

            if (iCharIndex > -1) {
                sHeader = sText.substring(0, iCharIndex);
                if (++iCharIndex < sText.length)
                    sTileText = sText.substring(iCharIndex);
            } else
                sHeader = sText;

            sQuery = '?brand=' + sBrand.toLowerCase() +
                     '&promo=' + EncodeURIComponent(
                                    sHeader + '|Book before ' + GetDateString(oBookBefore) + '|' + sTileText
//                                    '|Book before ' + GetDateString(oBookBefore) +
//                                    '|Travel between ' + GetDateString(oTravelFrom) + ' - ' + GetDateString(oTravelTo) + '|'+ sTileText
                                );

            sHref = sHref + sQuery;

            oLink.href = sHref;

            return true;
        };
        oLink.onclick = UpdateLink;
    }
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <asp:Panel ID="packageEditPanel" runat="Server" Width="780px" CssClass="packageEdit" >
        <asp:UpdatePanel runat="server" ID="packageUpdatePanel">
            <ContentTemplate>
                <table style="width:100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width:150px">Code:</td>
                        <td style="width:250px"><asp:TextBox ID="codeTextBox" runat="server" style="width:150px" MaxLength="10" /></td>
                        <td style="width:150px">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Name:</td>
                        <td colspan="3"><asp:TextBox ID="nameTextBox" runat="server" style="width:600px" MaxLength="64" /></td>
                    </tr>
                    <tr>
                        <td>Description:</td>
                        <td colspan="3"><asp:TextBox ID="descriptionTextBox" runat="server" style="width:600px" MaxLength="32" /></td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding-top: 6px">Comments:</td>
                        <td colspan="3"><asp:TextBox ID="notesTextBox" runat="server" style="width:600px" TextMode="multiLine" Rows="3" /></td>
                    </tr>
                    <tr>
                        <td>Tile text</td>
                        <td colspan="3">
                            <asp:CheckBox ID="chkEnablePromoTile" EnableViewState="false" Text="Enable Tile on B2C" runat="server" />
                            &nbsp;&nbsp;&nbsp;
                            <asp:HyperLink ID="lnkPromoTile" Target="_blank" EnableViewState="false" runat="server">Show Tile</asp:HyperLink>
                            <br />
                            <asp:TextBox ID="txtPromoTileText" Enabled="false" style="width:600px" TextMode="MultiLine" EnableViewState="false" Rows="4" runat="server"></asp:TextBox>

                            <script type="text/javascript">
                                (function () {
                                    var oText = document.getElementById('<%=txtPromoTileText.ClientID %>'),
                                    oLink = document.getElementById('<%=lnkPromoTile.ClientID %>'),
                                    oCheck = document.getElementById('<%=chkEnablePromoTile.ClientID %>');
                                    oLink.style.display = 'none';

                                    oText.onblur = function () {
                                        //this.value = this.value.replace(/(?:\||(?:<[^>]+>))/g,'');
                                        this.value = this.value.replace(/(?:\||(?:<[^>]+>))/g,'').replace(/[<>"']/g,'');
                                    }

                                    oCheck.onchange = oCheck.onkeydown = oCheck.onclick =
                                        function () {
                                            oText.disabled = !this.checked || this.disabled;
                                            oLink.style.display = this.checked ? '' : 'none';
                                        };

                                    oCheck.onchange();
                                })();

                                MonitorTextArea('<%=txtPromoTileText.ClientID %>');
                                InitPromoLink('<%=lnkPromoTile.ClientID %>', '<%=txtPromoTileText.ClientID %>', '<%=bookedToTextBox.DateFormat %>',
                                            '<%=bookedToTextBox.ClientID %>', '<%=travelFromTextBox.ClientID %>', '<%=travelToTextBox.ClientID %>', '<%=brandDropDown.ClientID %>');
                            </script>
                        </td>                        
                    </tr>
                    <tr>
                        <td>Brand:</td>
                        <td>
                            <asp:DropDownList ID="brandDropDown" runat="server" style="width:150px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                        <td>Type:</td>
                        <td>
                            <asp:DropDownList ID="typeDropDown" runat="server" style="width:150px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>Country:</td>
                        <td>
                            <asp:DropDownList ID="countryDropDown" runat="server" style="width:150px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                        <td>Currency:</td>
                        <td>
                            <asp:DropDownList ID="currencyDropDown" runat="server" style="width:150px">
                                <asp:ListItem />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="border-width: 1px 0px; border-top-style: dotted; padding-top: 6px">Flags:</td>
                        <td style="border-width: 1px 0px; border-top-style: dotted" colspan="4">
                            <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="3" Width="100%" CssClass="repeatLabelTable">
                                <asp:ListItem Value="PkgIsRelocatable">Relocatable</asp:ListItem>
                                <asp:ListItem Value="PkgDispPrice" Selected="True">Display Price</asp:ListItem>
                                <asp:ListItem Value="PkgIsExtBaseRate">Extension at Base Rates</asp:ListItem>
                                <asp:ListItem Value="PkgIsChargeAtCoRate">Vehicle charged at Check-out Rate</asp:ListItem>
                                <%--<asp:ListItem Value="PkgEnableTileOnB2C">Enable tile on B2C</asp:ListItem>--%>
                                <asp:ListItem Value="PkgRequiresLoyaltyCard">Package requires Loyalty Card Number</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
                 <br />
                <table style="width:100%;" cellpadding="2" cellspacing="0" id="statusTable" runat="server">
                    <tr>
                        <td style="width:150px">Booked From:</td>
                        <td style="width:250px"><uc1:DateControl ID="bookedFromTextBox" runat="server" Nullable="True" />&nbsp;*</td>
                        <td style="width:150px">To:</td>
                        <td><uc1:DateControl ID="bookedToTextBox" runat="server" Nullable="True" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td>Travel From:</td>
                        <td><uc1:DateControl ID="travelFromTextBox" runat="server" Nullable="True" />&nbsp;*</td>
                        <td>To:</td>
                        <td><uc1:DateControl ID="travelToTextBox" runat="server" Nullable="True" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td>Status:</td>
                        <td colspan="3">
                            <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="4" RepeatLayout="Table" CssClass="repeatLabelTable">
                                <asp:ListItem Text="Active" />
                                <asp:ListItem Text="Pending" />
                                <asp:ListItem Text="Inactive" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
                
                <table style="width:100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td align="right" style="border-width: 1px 0px;">
                            <asp:Button ID="publishButton" runat="server" Text="Publish" CssClass="Button_Standard"/>
                            <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="False" />
                            <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="False" />
                            <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        
        <br />
            
        <asp:Panel ID="packageTabContainer" runat="server">
            <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="0" Width="100%">
                <ajaxToolkit:TabPanel runat="server" ID="saleableProductTabPanel" HeaderText="Saleable Products">
                    <ContentTemplate>
                        <uc1:SaleableProductsControl ID="saleableProductsControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="flexTabPanel" HeaderText="Flex Rates">
                    <ContentTemplate>
                        <uc1:FlexLevelControl ID="flexLevelControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="agentTabPanel" HeaderText="Agents">
                    <ContentTemplate>
                        <uc1:AgentControl ID="agentControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="marketTabPanel" HeaderText="Market Codes">
                    <ContentTemplate>
                        <uc1:MarketControl ID="marketControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="nonAvailabilityTabPanel" HeaderText="Non-Availability">
                    <ContentTemplate>
                        <uc1:NonAvailabilityControl ID="nonAvailabilityControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="locationsTabPanel" HeaderText="Locations">
                    <ContentTemplate>
                        <uc1:LocationsControl ID="locationsControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="weeklyProfileTabPanel" HeaderText="Weekly Profile">
                    <ContentTemplate>
                        <uc1:WeeklyProfileControl ID="weeklyProfileControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="notesTabPanel" HeaderText="Notes">
                    <ContentTemplate>
                        <uc1:NoteControl ID="noteControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
               <ajaxToolkit:TabPanel runat="server" ID="PackageSetupPanel" HeaderText="Web Package">
                    <ContentTemplate>
                        <uc1:PackageSetupControl runat="server" ID="PackageSetupControl" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>

                <ajaxToolkit:TabPanel runat="server" ID="tabDiscountProfiles" HeaderText="Discounts">
                    <ContentTemplate>
                        <uc1:DiscountProfiles runat="server" ID="ctlDiscountProfiles" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
        
            <div style="text-align:right">
                <asp:HyperLink ID="lnkCdBaNumbers"  runat="server">Display/ Link CD Number</asp:HyperLink>
            </div>
            <br />
        </asp:Panel>
        

    </asp:Panel>
    
    <br />
    
</asp:Content>
