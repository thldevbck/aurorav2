<%@ Application Language="VB" %>

<%@ Import Namespace="Aurora.Common" %>
<%@ Import Namespace="Aurora.WebConfig.Service" %>
<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
	 Try
            Dim result As Boolean = SecurityConfig.EncryptConnectionString()
            ''Dim result As Boolean = SecurityConfig.DecryptConnectionString()
        Catch ex As Exception
			Logging.LogError("Application_Start ", ex.Message & vbCrLf & ex.StackTrace)
        End Try
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
       
    Sub Application_EndRequest(ByVal sender As Object, ByVal e As EventArgs)
    End Sub
</script>