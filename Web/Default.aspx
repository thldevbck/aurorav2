<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="_Default" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/LoadingIndicatorControl/LoadingIndicatorControl.ascx" TagName="LoadingIndicatorControl" TagPrefix="uc1" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
    <head runat="server">
        <title>Aurora</title>
        <link rel="shortcut icon" href="~/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="Include/AuroraHeader.css" />
        <script type="text/javascript" src="Include/AuroraHeader.js"></script>
        <script type="text/javascript" src="Include/Date.js"></script>
        <script type="text/javascript" src="Include/Validation.js"></script>

        <style type="text/css">
body
{
    margin: 0;
    padding: 0;
    background: #37465D url(images/header_bg.png) repeat;
}    
        </style>
        
    </head>
    <body>
        <form id="aspnetForm" runat="server">
            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            
            <script type="text/javascript">
var initialFunctionUrl = <%= Me.InitialFunctionUrl %>;
var openNewWindow = <%= Me.OpenNewWindow %>;

function startAurora()
{
    if (initialFunctionUrl == '') return false;

    if (true || openNewWindow)
    {
        window.open(initialFunctionUrl, "", "toolbar=no, menubar=yes, resizable=yes, scrollbars=yes, location=no, directories=no");	
	    window.blur();
    }
    else
        window.location = initialFunctionUrl;

	return false;
}

addEvent (window, "load", onloadHacks);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(onloadHacks);
            </script>

            <asp:UpdatePanel Id="updatePanel" runat="server">
                <Triggers>
                    <asp:PostBackTrigger ControlID="refreshButton" />
                    <asp:AsyncPostBackTrigger ControlID="companyDropDown" />
                    <asp:AsyncPostBackTrigger ControlID="countryDropDown" />
                    <asp:AsyncPostBackTrigger ControlID="locationDropDown" />
                    <asp:AsyncPostBackTrigger ControlID="menuDropDown" />
                </Triggers>
                <ContentTemplate>
                
                    <div style="width: 780px; margin: 0px auto">

                        <img src="images/logo_aurora_lg.gif" alt="" style="margin-left: 135px; margin-top: -70px"/>
                        
                        <div style="margin: 0px auto 10px auto; width: 500px; background-color: #EEE; border-width: 1px; border-style: solid; border-color: #DEF">
                            <table width="100%" cellpadding="2" cellspacing="0">
                                <tr>
                                    <td style="vertical-align:top;" align="left" rowspan="6">
                                        <asp:Image Id="image" runat="Server" ImageUrl="images/keys.gif" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150">Name:</td>
                                    <td>
                                        <asp:TextBox ID="nameTextBox" runat="Server" Width="270px" ReadOnly="True" />
                                        <asp:Button ID="refreshButton" runat="Server" Width="20px" CssClass="Button_Standard Button_Refresh" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Company:</td>
                                    <td>
                                        <asp:DropDownList ID="companyDropDown" runat="Server" Width="300px" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Country:</td>
                                    <td>
                                        <asp:DropDownList ID="countryDropDown" runat="Server" Width="300px" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Location:</td>
                                    <td>
                                        <asp:DropDownList ID="locationDropDown" runat="Server" Width="300px" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Initial Function:</td>
                                    <td>
                                        <asp:DropDownList ID="menuDropDown" runat="Server" Width="300px" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" align="right" style="border-style: dotted; border-top-width: 1px">
                                        <asp:Button ID="startButton" runat="server" Text="Start" CssClass="Button_Standard Button_Next" OnClientClick="return startAurora();" />
                                    </td>
                                </tr>
                            </table>
                        </div>        
                        
                        <div style="text-align: center; padding: 5px; border-width: 1px 0 1px 0; border-style: solid; border-color: #DEF">
                            <asp:Repeater ID="companyRepeater" runat="server">
                                <ItemTemplate>
                                    <span style="margin: 5px; padding: 0">
                                        <asp:Image ID="image" runat="server" ImageUrl='<%# "images/company/" & Container.DataItem("ComCode") & ".png" %>' />
                                    </span>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <span style="margin: 5px; padding: 0">
                                        <asp:Image ID="image" runat="server" ImageUrl='<%# "images/company/" & Container.DataItem("ComCode") & ".png" %>' />
                                    </span>
                                </AlternatingItemTemplate>
                            </asp:Repeater>
                        </div>
                        
                    </div>

                </ContentTemplate>            
            </asp:UpdatePanel>

            <uc1:LoadingIndicatorControl ID="lodLoading" runat="server" />
        </form>
    </body>
</html>
