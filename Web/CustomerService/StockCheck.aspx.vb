Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.CustomerService.Data
Imports Aurora.CustomerService.Data.StockCheckDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.StockCheck)> _
Partial Class CustomerService_StockCheck
    Inherits AuroraPage

    Public Const Selected_CookieName As String = "OL-LOCINVMGT_Selected"

    Private _stockCheckDataSet As New StockCheckDataSet()
    Private _selectedLocation As LocationRow = Nothing
    Private _selectedStockCheck As LocationInventoryLookupRow = Nothing
    Private _locationInventory As LocationInventoryRow = Nothing

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
    End Sub

    Private Sub BuildLocationLookup()
        _stockCheckDataSet.Location.Clear()
        _stockCheckDataSet.LocationInventoryLookup.Clear()
        _stockCheckDataSet.Universalinfo.Clear()
        DataRepository.GetStockCheckLookups(_stockCheckDataSet, Me.CompanyCode, Me.CountryCode)

        Dim selectedLocation As String = IIf(Me.IsPostBack, locationDropDownList.SelectedValue, UserSettings.Current.LocCode)
        locationDropDownList.Items.Clear()
        For Each locationRow As LocationRow In _stockCheckDataSet.Location
            Dim li As New ListItem(locationRow.LocCode & " - " & locationRow.LocName, locationRow.LocCode)
            li.Attributes.Add("style", "background-color: " & ColorTranslator.ToHtml(locationRow.StatusColor))
            locationDropDownList.Items.Add(li)
        Next
        If locationDropDownList.Items.FindByValue(selectedLocation) IsNot Nothing Then
            locationDropDownList.SelectedValue = selectedLocation
            _selectedLocation = _stockCheckDataSet.Location.FindByLocCode(selectedLocation)
        ElseIf _stockCheckDataSet.Location.Count > 0 Then
            _selectedLocation = _stockCheckDataSet.Location(0)
            selectedLocation = _selectedLocation.LocCode
            locationDropDownList.SelectedValue = selectedLocation
        Else
            selectedLocation = ""
            _selectedLocation = Nothing
        End If

        createButton.Visible = _selectedLocation IsNot Nothing AndAlso Not _selectedLocation.HasInProgress
    End Sub

    Private Sub BuildStockCheckLookup()
        Dim selectedStockCheck As String = stockCheckDropDownList.SelectedValue
        stockCheckDropDownList.Items.Clear()
        For Each locationInventoryLookupRow As LocationInventoryLookupRow In _stockCheckDataSet.LocationInventoryLookup
            If locationInventoryLookupRow.LocationRow.LocCode = locationDropDownList.SelectedValue Then
                Dim li As New ListItem(locationInventoryLookupRow.LivStkChkDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) + " - " + locationInventoryLookupRow.LivStatus, locationInventoryLookupRow.LivId)
                li.Attributes.Add("style", "background-color: " & ColorTranslator.ToHtml(locationInventoryLookupRow.StatusColor))
                stockCheckDropDownList.Items.Add(li)
            End If
        Next
        If stockCheckDropDownList.Items.FindByValue(selectedStockCheck) IsNot Nothing Then
            stockCheckDropDownList.SelectedValue = selectedStockCheck
            _selectedStockCheck = _stockCheckDataSet.LocationInventoryLookup.FindByLivId(selectedStockCheck)
        ElseIf _selectedLocation IsNot Nothing AndAlso _selectedLocation.GetLocationInventoryLookupRows().Length > 0 Then
            _selectedStockCheck = _selectedLocation.GetLocationInventoryLookupRows()(0)
            selectedStockCheck = _selectedStockCheck.LivId
            stockCheckDropDownList.SelectedValue = selectedStockCheck
        Else
            Dim li As New ListItem("(None)", "")
            li.Attributes.Add("style", "background-color: " & ColorTranslator.ToHtml(StockCheckConstants.InactiveColor))
            stockCheckDropDownList.Items.Add(li)

            selectedStockCheck = ""
            _selectedStockCheck = Nothing
        End If
    End Sub

    Private Sub BuildLookup()
        BuildLocationLookup()
        BuildStockCheckLookup()
    End Sub

    Private Sub BuildStockCheck(ByVal useViewState As Boolean)
        _stockCheckDataSet.LocationInventory.Clear()
        _stockCheckDataSet.UserInfo.Clear()
        _stockCheckDataSet.CheckedProductItem.Clear()
        _stockCheckDataSet.FleetAsset.Clear()
        _stockCheckDataSet.RMReport.Clear()
        If _selectedStockCheck IsNot Nothing Then
            DataRepository.GetStockCheck(_stockCheckDataSet, Me.CompanyCode, _selectedStockCheck.LivId)
            If _stockCheckDataSet.LocationInventory.Count > 0 Then
                _locationInventory = _stockCheckDataSet.LocationInventory(0)
            End If
        End If

        entryPanel.Visible = False
        itemsPanel.Visible = False
        rmreportPanel.Visible = False

        If _selectedStockCheck Is Nothing OrElse _locationInventory Is Nothing Then Return

        entryPanel.Visible = _selectedStockCheck.CanComplete
        itemsPanel.Visible = True
        rmreportPanel.Visible = _selectedStockCheck.CanComplete AndAlso _stockCheckDataSet.RMReport.Count > 0

        reportButton.OnClientClick = "window.open('../Reports/Default.aspx?funCode=RPT-STKTAKDI&sLocCode=" & _locationInventory.LivLocCode & "&sLivId=" & _locationInventory.LivId & "&popup=True','', 'height=550,width=810,resizable=yes,scrollbars=yes,toolbar=no,status=yes,menubar=no');return false;"
        completeButton.Visible = _selectedStockCheck.CanComplete
        undoButton.Visible = _selectedStockCheck.CanUndo

        itemsListPanel.Height = IIf(_selectedStockCheck.CanComplete, Unit.Parse("250px"), Unit.Empty)

        itemsBulletedList.Items.Clear()
        itemsBulletedList.DisplayMode = BulletedListDisplayMode.HyperLink
        For Each checkedProductItemRow As CheckedProductItemRow In _locationInventory.GetCheckedProductItemRows()
            Dim li As New ListItem()
            li.Text = checkedProductItemRow.Description
            If checkedProductItemRow.PriIsVehicle Then
                li.Value = "~/OpsAndLogs/FleetRepairList.aspx?sTxtSearch=" & Server.UrlEncode(checkedProductItemRow.PriUnitNum)
            Else
                li.Value = "#"
            End If
            li.Attributes.Add("class", IIf(Not checkedProductItemRow.IsPriIsVehicleNull AndAlso checkedProductItemRow.PriIsVehicle, "vehicle", "product"))
            itemsBulletedList.Items.Add(li)
        Next

        rmreportDataGrid.DataSource = _stockCheckDataSet.RMReport
        rmreportDataGrid.DataBind()
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        BuildLookup()
        BuildStockCheck(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Added: Raj (29 Mar, 2012)- To capture keypress for vehicle unit number and replace . with ,
        vehicleTextBox.Attributes.Add("OnKeyUp", "KeyChange()")
        vehicleTextBox.Attributes.Add("OnKeyPress", "KeyChange()")
        'Added: Raj (29 Mar, 2012)- To capture keypress for vehicle unit number and replace . with ,

        If Me.IsPostBack Then Return

        BuildForm(False)
    End Sub

    Protected Sub locationDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationDropDownList.SelectedIndexChanged
        BuildForm(True)
    End Sub

    Protected Sub stockCheckDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles stockCheckDropDownList.SelectedIndexChanged
        BuildForm(True)
    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        BuildLookup()
        Try
            If _selectedLocation Is Nothing OrElse _selectedLocation.HasInProgress Then
                Throw New Exception()
            End If

            Dim result As LocationInventoryDataTable = DataRepository.CreateLocationInventory(Me.CompanyCode, Me.CountryCode, _selectedLocation.LocCode, Me.UserId, Me.PrgmName)
            If result Is Nothing OrElse result.Count = 0 Then Throw New Exception()

            ' create a dummy list item in the stock check combo for the new stock check (will be reset in BuildForm)
            Dim li As New ListItem("", result(0).LivId)
            stockCheckDropDownList.Items.Add(li)
            stockCheckDropDownList.SelectedValue = result(0).LivId

            Me.AddInformationMessage("Stock Check created")
        Catch
            Me.AddErrorMessage("Error creating Stock Check")
        End Try

        BuildForm(True)
    End Sub

    Private Sub AddItem(ByVal isVehicle As Boolean)
        BuildLookup()

        Dim textBox As TextBox = IIf(isVehicle, vehicleTextBox, productTextBox)
        Dim title As String = IIf(isVehicle, "Vehicle", "Other Product")

        Try
            If _selectedStockCheck Is Nothing OrElse Not _selectedStockCheck.CanComplete Then Throw New Exception()

            Dim text As String = textBox.Text.Trim().Replace(vbCr, " ").Replace(vbLf, " ").Replace(",", " ")
            textBox.Text = ""

            If Not String.IsNullOrEmpty(text) Then
                Dim unitNumberList As New List(Of String)
                unitNumberList.AddRange(text.Split(" "))

                Dim successList As New List(Of String)
                Dim warningList As New List(Of String)

                For Each unitNumber As String In unitNumberList
                    unitNumber = ("" & unitNumber).Trim()
                    If String.IsNullOrEmpty(unitNumber) Then Continue For

                    Dim result As CheckedProductItemDataTable = DataRepository.CreateCheckedProductItem( _
                        Me.CompanyCode, _
                        _selectedStockCheck.LivId, _
                        unitNumber, _
                        isVehicle, _
                        Me.UserId, _
                        Me.PrgmName)
                    If result Is Nothing OrElse result.Count = 0 Then
                        warningList.Add(unitNumber)
                    Else
                        successList.Add(unitNumber)
                    End If
                Next

                If successList.Count > 0 Then
                    Me.AddInformationMessage(Server.HtmlEncode("Successfully added " & title & IIf(successList.Count > 1, "s", "") & " : " & String.Join(", ", successList.ToArray())))
                End If
                If warningList.Count > 0 Then
                    Me.AddWarningMessage(Server.HtmlEncode("Could not add " & title & IIf(warningList.Count > 1, "s", "") & " : " & String.Join(", ", warningList.ToArray()) & ".  Not a valid Unit / Rego number."))
                    textBox.Text = String.Join(", ", warningList.ToArray())
                End If
            End If
        Catch
            Me.AddErrorMessage("Error adding " & title & "s")
        End Try

        BuildForm(True)
    End Sub

    Protected Sub vehicleAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles vehicleAddButton.Click
        AddItem(True)
    End Sub

    Protected Sub productAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles productAddButton.Click
        AddItem(False)
    End Sub

    Private Sub RemoveItem(ByVal isVehicle As Boolean)
        BuildLookup()

        Dim textBox As TextBox = IIf(isVehicle, vehicleTextBox, productTextBox)
        Dim title As String = IIf(isVehicle, "Vehicle", "Other Product")

        Try
            If _selectedStockCheck Is Nothing OrElse Not _selectedStockCheck.CanComplete Then Throw New Exception()

            Dim text As String = textBox.Text.Trim().Replace(vbCr, " ").Replace(vbLf, " ").Replace(",", " ")
            textBox.Text = ""

            If Not String.IsNullOrEmpty(text) Then
                Dim unitNumberList As New List(Of String)
                unitNumberList.AddRange(text.Split(" "))

                Dim successList As New List(Of String)
                Dim warningList As New List(Of String)

                For Each unitNumber As String In unitNumberList
                    unitNumber = ("" & unitNumber).Trim()
                    If String.IsNullOrEmpty(unitNumber) Then Continue For

                    Dim result As Integer = DataRepository.DeleteCheckedProductItem( _
                        Me.CompanyCode, _
                        _selectedStockCheck.LivId, _
                        unitNumber, _
                        isVehicle)
                    If result <= 0 Then
                        warningList.Add(unitNumber)
                    Else
                        successList.Add(unitNumber)
                    End If
                Next

                If successList.Count > 0 Then
                    Me.AddInformationMessage(Server.HtmlEncode("Successfully removed " & title & IIf(successList.Count > 1, "s", "") & ": " & String.Join(", ", successList.ToArray())))
                End If
                If warningList.Count > 0 Then
                    Me.AddWarningMessage(Server.HtmlEncode("Could not remove " & title & IIf(warningList.Count > 1, "s", "") & ": " & String.Join(", ", warningList.ToArray()) & ".  Not in the items entered."))
                    textBox.Text = String.Join(", ", warningList.ToArray())
                End If
            End If
        Catch ex As Exception
            Me.AddErrorMessage("Error removing " & title & "s")
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try

        BuildForm(True)
    End Sub

    Protected Sub vehicleRemoveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles vehicleRemoveButton.Click
        RemoveItem(True)
    End Sub

    Protected Sub productRemoveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles productRemoveButton.Click
        RemoveItem(False)
    End Sub

    Protected Sub completeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles completeButton.Click
        BuildLookup()
        Try
            If _selectedStockCheck IsNot Nothing AndAlso _selectedStockCheck.CanComplete Then
                Dim result As LocationInventoryDataTable = DataRepository.UpdateLocationInventory(_selectedStockCheck.LivId, StockCheckConstants.StockCheckStatus_Completed, _selectedStockCheck.IntegrityNo, Me.UserId, Me.PrgmName)
                If result Is Nothing OrElse result.Count = 0 Then Throw New Exception()
            Else
                Throw New Exception()
            End If

            Me.AddInformationMessage("Product Items Stock Check completed")
        Catch
            Me.AddErrorMessage("Error completing Product Items Stock Check")
        End Try

        BuildForm(True)
    End Sub

    Protected Sub undoButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles undoButton.Click
        BuildLookup()
        Try
            If _selectedStockCheck IsNot Nothing AndAlso _selectedStockCheck.CanUndo Then
                Dim result As LocationInventoryDataTable = DataRepository.UpdateLocationInventory(_selectedStockCheck.LivId, StockCheckConstants.StockCheckStatus_InProgress, _selectedStockCheck.IntegrityNo, Me.UserId, Me.PrgmName)
                If result Is Nothing OrElse result.Count = 0 Then Throw New Exception()
            Else
                Throw New Exception()
            End If

            Me.AddInformationMessage("Product Items Stock Check undone")
        Catch
            Me.AddErrorMessage("Error undoing Product Items Stock Check")
        End Try

        BuildForm(True)
    End Sub
End Class
