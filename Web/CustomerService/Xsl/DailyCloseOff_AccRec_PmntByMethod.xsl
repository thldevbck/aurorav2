<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" >
	<xsl:template match="/">
		<html>
			<head>
				<title>
					PAYMENT TRANSACTION REPORT - By Method&#160;(<xsl:value-of select="//Data/Loc"/>&#160;<xsl:value-of select="//Data/CoseOffDate"/>)
				</title>
				<STYLE>P{page-break-after: always;}</STYLE>
			</head>
			<body class="B1">
				<STRONG>
					<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">PAYMENT TRANSACTION REPORT - BY METHOD</FONT>
				</STRONG>
				<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
					<TR>
						<TD>
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								Country&#160;:&#160;<b>
									<xsl:value-of select="//Data/Cty"/>
								</b>
							</FONT>
						</TD>
						<TD>
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								Branch&#160;:&#160;<b>
									<xsl:value-of select="//Data/Loc"/>
								</b>
							</FONT>
						</TD>
						<TD>
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								Date&#160;:&#160;<b>
									<xsl:value-of select="//Data/CoseOffDate"/>
								</b>
							</FONT>
						</TD>
						<TD align="right">
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								User&#160;:&#160;<b>
									<xsl:value-of select="//Data/user"/>
								</b>
							</FONT>
						</TD>
					</TR>
				</TABLE>
				<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0" bordercolor="#000000">
					<TR>
						<TD>&#160;</TD>
					</TR>
					<TR style="COLOR: white; BACKGROUND-COLOR: black" valign="bottom">
						<TD align="left" width="20.69%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">&#160;Method</FONT>
							</B>
						</TD>
						<TD align="center" width="13.79%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Date</FONT>
							</B>
						</TD>
						<TD align="center" width="13.79%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									Total<BR/>Payment
								</FONT>
							</B>
						</TD>
						<TD align="center" width="8.9%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Curr</FONT>
							</B>
						</TD>
						<TD align="center" width="2.9%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Type</FONT>
							</B>
						</TD>
						<TD align="center" width="14.34%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Booking</FONT>
							</B>
						</TD>
						<TD align="center" width="10.34%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Rental</FONT>
							</B>
						</TD>
						<TD align="right" width="15.24%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									$[<xsl:value-of select="Data/CurrCode"/>]&#160;&#160;
								</FONT>
							</B>
						</TD>
					</TR>
					<xsl:for-each select="//Data/Payments/row">
						<TR>
							<TD align="left" nowrap="">
								<xsl:if test="@Method != preceding-sibling::*[position()=1]/@Method or position()=1">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										&#160;<xsl:value-of select="@Method"/>
									</FONT>
								</xsl:if>
							</TD>
							<TD align="center" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Date"/>
								</FONT>
							</TD>
							<TD align="right" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@PaymentAmount"/>&#160;&#160;&#160;&#160;
								</FONT>
							</TD>
							<TD align="center" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Curr"/>
								</FONT>
							</TD>
							<TD align="center" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="substring(@PaymentType,1,1)"/>
								</FONT>
							</TD>
							<TD align="right" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Booking"/>&#160;&#160;
								</FONT>
							</TD>
							<TD align="center" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Rental"/>
								</FONT>
							</TD>
							<TD align="right" nowrap="">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@CurrAmt"/>&#160;&#160;&#160;
								</FONT>
							</TD>
						</TR>
						<xsl:if test="@Method!= following-sibling::*[position()=1]/@Method or position()=last()">
							<TR style="FONT-WEIGHT: bold;">
								<TD align="right" colspan="7" style="BORDER-TOP: black thin solid; BORDER-BOTTOM: black thin solid; ">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										Total <xsl:value-of select="@Method"/>
									</FONT>
								</TD>
								<xsl:variable name="Method" select="@Method"/>
								<TD align="right" style="BORDER-TOP: black thin solid; BORDER-BOTTOM: black thin solid">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row' and ./@Method=$Method]/@CurrAmt),'#,###,##0.00')"/>&#160;&#160;
									</FONT>
								</TD>
							</TR>
						</xsl:if>
						<xsl:if test="@Division!= following-sibling::*[position()=1]/@Division or position()=last()">
							<TR style="FONT-WEIGHT: bold; COLOR: white; BACKGROUND-COLOR: black">
								<TD align="right" colspan="5">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										Total <xsl:value-of select="@Division"/>
									</FONT>
								</TD>
								<TD align="right" colspan="2">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">(Exclude&#160;Imprint)</FONT>
								</TD>
								<xsl:variable name="Division" select="@Division"/>
								<TD align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="format-number(sum(//Data/ImprintNonImprint/*[name()='row'  and ./@Division= $Division ]/@NonImprint),'#,###,##0.00')"/>&#160;&#160;
									</FONT>
								</TD>
							</TR>
							<TR style="FONT-WEIGHT: bold; COLOR: white; BACKGROUND-COLOR: black">
								<TD align="right" colspan="7">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">(Imprint)</FONT>
								</TD>
								<xsl:variable name="Division" select="@Division"/>
								<TD align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="format-number(sum(//Data/ImprintNonImprint/*[name()='row'  and ./@Division= $Division ]/@Imprint),'#,###,##0.00')"/>&#160;&#160;
									</FONT>
								</TD>
							</TR>
							<tr>
								<td>
									&#160;<br/>
								</td>
							</tr>
						</xsl:if>
					</xsl:for-each>
					<tr>
						<td>
							&#160;<br/>
						</td>
					</tr>
					<TR style="FONT-WEIGHT: bold;FONT-SIZE: 14pt">
						<TD align="right" colspan="5" style="BORDER-TOP: black thin solid;">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">Total&#160;Payment&#160;Received</FONT>
						</TD>
						<TD align="right" colspan="2" style="BORDER-TOP: black thin solid;">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">(Exclude&#160;Imprint)</FONT>
						</TD>
						<TD align="right" style="BORDER-TOP: black thin solid;">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">
								<xsl:value-of select="format-number(sum(//Data/TotBankingNoImprint),'#,###,##0.00')"/>&#160;&#160;
							</FONT>
						</TD>
					</TR>
					<TR style="FONT-WEIGHT: bold;FONT-SIZE: 14pt">
						<TD align="right" colspan="7" style="BORDER-BOTTOM: black thin solid;">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">(Imprint)</FONT>
						</TD>
						<TD align="right" style="BORDER-BOTTOM: black thin solid">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">
								<xsl:value-of select="format-number(sum(//Data/TotPaymentsRecivedImprints),'#,###,##0.00')"/>&#160;&#160;
							</FONT>
						</TD>
					</TR>

				</TABLE>
				<TABLE cellSpacing="1" cellPadding="1" width="90%" border="0">
					<xsl:for-each select="//Data/Electronic/row">
						<xsl:variable name="Method" select="@Method"/>
						<TR>
							<TD width="27.59%"/>
							<TD width="31.03%">
								<xsl:if test="position()=1">
									<b>
										<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Electronic Check</FONT>
									</b>
								</xsl:if>
							</TD>
							<TD width="24.14%">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="$Method"/>
								</FONT>
							</TD>
							<TD align="right" width="17.24%">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row'  and ./@Method=$Method]/@CurrAmt),'#,###,##0.00')"/>
								</FONT>
							</TD>
						</TR>
						<xsl:if test="position()=last()">
							<TR style="font-weight:bold">
								<TD colspan="2"/>
								<td>
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Total</FONT>
								</td>
								<TD align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row'  and (./@Division='Credit Card' )]/@CurrAmt),'#,###,##0.00')"/>
									</FONT>
								</TD>
							</TR>
						</xsl:if>
					</xsl:for-each>
					<tr>
						<td>
							&#160;<br/>
						</td>
					</tr>
					<tr>
						<td>
							&#160;<br/>
						</td>
					</tr>
				</TABLE>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>
