<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<HTML>
			<head>
				<title>
					Payment Transaction Report - By Type&#160;(<xsl:value-of select="//Data/Loc"/>&#160;<xsl:value-of select="//Data/CoseOffDate"/>)
				</title>
			</head>
			<body>
				<STRONG>
					<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">PAYMENT TRANSACTION REPORT - BY TYPE</FONT>
				</STRONG>&#160;
				<TABLE cellSpacing="0" cellPadding="0" width="90%" border="0">
					<TR>
						<TD>
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								Country&#160;:&#160;<b>
									<xsl:value-of select="//Data/Cty"/>
								</b>
							</FONT>
						</TD>
						<TD>
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								Branch&#160;:&#160;<b>
									<xsl:value-of select="//Data/Loc"/>
								</b>
							</FONT>
						</TD>
						<TD>
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								Date&#160;:&#160;<b>
									<xsl:value-of select="//Data/CoseOffDate"/>
								</b>
							</FONT>
						</TD>
						<TD align="right">
							<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
								User&#160;:&#160;<b>
									<xsl:value-of select="//Data/user"/>
								</b>
							</FONT>
						</TD>
					</TR>
				</TABLE>

				<TABLE id="Table2" cellSpacing="0" cellPadding="0" width="90%" border="0">
					<tr>
						<td colspan="8">&#160;</td>
					</tr>
					<tr style="COLOR: white; BACKGROUND-COLOR: black" valign="bottom">
						<td width="12.66%" align="left">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									&#160;Payment<BR/>&#160;Type
								</FONT>
							</B>
						</td>
						<td align="center" width="11.39%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Booking</FONT>
							</B>
						</td>
						<td align="center" width="9.49%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Rental</FONT>
							</B>
						</td>
						<td align="center" width="12.66%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Date</FONT>
							</B>
						</td>
						<td align="center" width="12.66%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									Payment<BR/>Amount
								</FONT>
							</B>
						</td>
						<td align="center" width="9.49%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">Curr</FONT>
							</B>
						</td>
						<td align="center" width="12.66%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									$[<xsl:value-of select="Data/CurrCode"/>]
								</FONT>
							</B>
						</td>
						<td align="left" width="18.99%">
							<B>
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">&#160;Method</FONT>
							</B>
						</td>
					</tr>
					<xsl:for-each select="//Data/Payments/*[name()='row']">
						<tr>
							<td align="left">
								<xsl:if test="@PaymentType!= preceding-sibling::*[position()=1]/@PaymentType or position()=1">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="@PaymentType"/>
									</FONT>
								</xsl:if>
							</td>
							<td align="center">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Booking"/>
								</FONT>
							</td>
							<td align="right">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Rental"/>&#160;&#160;&#160;
								</FONT>
							</td>
							<td align="center">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Date"/>
								</FONT>
							</td>
							<td align="right">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@PaymentAmount"/>&#160;&#160;&#160;&#160;
								</FONT>
							</td>
							<td align="center">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@Curr"/>
								</FONT>
							</td>
							<td align="right">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									<xsl:value-of select="@CurrAmt"/>
								</FONT>
							</td>
							<td nowrap="" align="left">
								<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
									&#160;&#160;&#160;&#160;&#160;<xsl:value-of select="@Method"/>
								</FONT>
							</td>
						</tr>
						<xsl:variable name="PaymentType" select="@PaymentType"/>
						<xsl:if test="@PaymentType!= following-sibling::*[position()=1]/@PaymentType or position()=last()">
							<!-- <tr><td colspan="8"><br/></td></tr> -->
							<!-- Non Imprints Total Display -->
							<tr style="FONT-WEIGHT: bold">
								<td style="BORDER-TOP: black thin solid" colSpan="4" align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="@PaymentType"/>&#160;Total&#160;
									</FONT>
								</td>
								<td style="BORDER-TOP: black thin solid" align="right" colSpan="2" >
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">(Exclude&#160;Imprint)</FONT>
								</td>
								<td style="BORDER-TOP: black thin solid" align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row' and @PaymentType=$PaymentType and @IsImprint='0']/@CurrAmt),'#,###,##0.00')"/>
									</FONT>
								</td>
								<td style="BORDER-TOP: black thin solid">&#160;</td>
							</tr>
							<!-- Imprints Total Display -->
							<tr style="FONT-WEIGHT: bold">
								<td style="BORDER-BOTTOM: black thin solid" colSpan="6" align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">(Imprint)</FONT>
								</td>
								<td style="BORDER-BOTTOM: black thin solid" align="right">
									<FONT FACE="Arial" STYLE="FONT-SIZE:9pt">
										<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row' and @PaymentType=$PaymentType and @IsImprint='1']/@CurrAmt),'#,###,##0.00')"/>
									</FONT>
								</td>
								<td style="BORDER-BOTTOM: black thin solid">&#160;</td>
							</tr>
						</xsl:if>
					</xsl:for-each>
					<tr>
						<td colspan="8">
							<br/>
						</td>
					</tr>

					<!-- Non Imprints Total Display -->
					<tr style="FONT-WEIGHT: bold; FONT-SIZE: 14pt ;BORDER-TOP: black thin solid; BORDER-BOTTOM: black thin solid">
						<td style="BORDER-TOP: black thin solid" colSpan="4" align="right">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">Grand&#160;Total</FONT>
						</td>
						<td style="BORDER-TOP: black thin solid" align="right" colSpan="2" >
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">(Exclude&#160;Imprint)</FONT>
						</td>
						<td style="BORDER-TOP: black thin solid" align="right">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">
								<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row' and @IsImprint = '0']/@CurrAmt),'#,###,##0.00')"/>
							</FONT>
						</td>
						<td style="BORDER-TOP: black thin solid">&#160;</td>
					</tr>
					<!-- Imprints Total Display -->
					<tr style="FONT-WEIGHT: bold; FONT-SIZE: 14pt ;BORDER-TOP: black thin solid; BORDER-BOTTOM: black thin solid">
						<td style="BORDER-BOTTOM: black thin solid" colSpan="6" align="right">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">(Imprint)</FONT>
						</td>
						<td style="BORDER-BOTTOM: black thin solid" align="right">
							<FONT FACE="Arial" STYLE="FONT-SIZE:12pt">
								<xsl:value-of select="format-number(sum(//Data/Payments/*[name()='row' and @IsImprint = '1']/@CurrAmt),'#,###,##0.00')"/>
							</FONT>
						</td>
						<td style="BORDER-BOTTOM: black thin solid">&#160;</td>
					</tr>

					<tr>
						<td colspan="8">
							<br/>
							<br/>
							<br/>
							<br/>
						</td>
					</tr>
				</TABLE>
			</body>
		</HTML>
	</xsl:template>
</xsl:stylesheet>
