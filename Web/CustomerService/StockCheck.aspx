<%@ Page Language="VB" AutoEventWireup="false" CodeFile="StockCheck.aspx.vb" Inherits="CustomerService_StockCheck" MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
.itemsBulletedList    
{
	padding-left: 0px;
	margin: 0;
	padding: 0;
}

.itemsBulletedList li
{
    padding: 2px 20px;
	list-style: none;
    text-align: left;
    float: left;
    width: 108px;
}

.itemsBulletedList li a
{
	font-size: 13px;
	font-family: Lucida Console, Courier New, Courier;
}

.itemsBulletedList li.vehicle
{
	background-image: url(../images/campervan.gif);
    background-repeat: no-repeat;
}

.itemsBulletedList li.product
{
	background-image: url(../images/product.gif);
    background-repeat: no-repeat;
}

</style>

<script language="javascript" type="text/javascript">
    //Added: Raj (29 Mar, 2012)- To capture keypress for vehicle unit number and replace . with ,
    
   function KeyChange(e) {
        var value = document.getElementById('<%=vehicleTextBox.ClientID%>').value;
        value = value.replace(".", ",");
        document.getElementById('<%=vehicleTextBox.ClientID%>').value = value;
    }  
    
//    function KeyCheck(e) {
//        var value = document.getElementById('<%=vehicleTextBox.ClientID%>').value;
//        value = value.replace(".", ",");
//        document.getElementById('<%=vehicleTextBox.ClientID%>').value = value;
//    }

    //Added: Raj (29 Mar, 2012)- To capture keypress for vehicle unit number and replace . with ,
</script>

</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="searchPanel" runat="Server" Width="100%">

        <table cellspacing="0" cellpadding="2" border="0" style="border-bottom-width: 1px; width: 100%;">
            <tr>
                <td style="width:150px">Location:</td>
                <td style="width:300px"><asp:DropDownList ID="locationDropDownList" runat="Server" Width="250px" onchange="showLoadingIndicator(); setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder$locationDropDownList\',\'\')', 0);" /></td>
                <td style="width:350px" align="Right" rowspan="2" valign="bottom"><asp:Button ID="createButton" runat="server" CssClass="Button_Standard Button_New" Text="New" /></td>
            </tr>
            <tr>                
                <td style="width:150px">Date / Status:</td>
                <td style="width:300px"><asp:DropDownList ID="stockCheckDropDownList" runat="Server" Width="250px" onchange="showLoadingIndicator(); setTimeout('__doPostBack(\'ctl00$ContentPlaceHolder$stockCheckDropDownList\',\'\')', 0);" /></td>
            </tr>
        </table>
        
    </asp:Panel>
    
    <asp:Panel ID="entryPanel" runat="Server" Width="100%">
        <table cellspacing="0" cellpadding="2" border="0" style="width: 100%; margin-top: 5px;">
            <tr>
                <td colspan="2"><asp:Image runat="Server" ImageUrl="~\Images\campervan.gif" Width="16" Height="16" ImageAlign="AbsMiddle" />&nbsp;Vehicles:</td>
            </tr>
            <tr>
                <td style="width:100%">
                    <asp:TextBox ID="vehicleTextBox" runat="server" Width="99%" Rows="3" TextMode="MultiLine" Font-Names="Lucida Console, Courier New" Font-Size="13px" />
                </td>
                <td align="right" valign="bottom" style="padding-left: 5px">
                    <asp:Button ID="vehicleAddButton" runat="server" Text="Add" CssClass="Button_Medium Button_Add" />
                    <asp:Button ID="vehicleRemoveButton" runat="server" Text="Remove" CssClass="Button_Medium Button_Remove" />
                </td>
            </tr>
            <tr>
                <td colspan="2"><asp:Image runat="Server" ImageUrl="~\Images\product.gif" Width="16" Height="16" ImageAlign="AbsMiddle" />&nbsp;Other Products:</td>
            </tr>
            <tr>
                <td style="width:100%">
                    <asp:TextBox ID="productTextBox" runat="server" Width="99%" Rows="3" TextMode="MultiLine" Font-Names="Lucida Console, Courier New" Font-Size="13px" />
                </td>
                <td align="right" valign="bottom" style="padding-left: 5px">
                    <asp:Button ID="productAddButton" runat="server" Text="Add" CssClass="Button_Medium Button_Add" />
                    <asp:Button ID="productRemoveButton" runat="server" Text="Remove" CssClass="Button_Medium Button_Remove" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="itemsPanel" runat="Server" Width="100%">

        
        <table cellspacing="0" cellpadding="2" border="0" style="border-bottom-width: 1px; width: 100%">
            <tr>
                <td colspan="2">Items Entered:</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel Id="itemsListPanel" runat="server" CssClass="inputreadonly" Width="760px" Height="250px" Style="padding: 5px; overflow: auto; min-height:250px"><asp:BulletedList ID="itemsBulletedList" runat="Server" CssClass="itemsBulletedList" /></asp:Panel>
                    
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="reportButton" runat="server" Text="Discrepancy Report" CssClass="Button_Standard Button_Report" OnClientClick="return false;" Width="175px" />
                    <asp:Button ID="completeButton" runat="server" Text="Complete" CssClass="Button_Standard Button_OK" />
                    <asp:Button ID="undoButton" runat="server" Text="Undo" CssClass="Button_Standard Button_Cancel" />
                </td>
            </tr>
        </table>
    </asp:Panel>
        
    <asp:Panel ID="rmreportPanel" runat="Server" Width="100%">
        <p><b>Current Repairs and Maintenance:</b></p>
        
        <asp:GridView ID="rmreportDataGrid" runat="server" 
         CssClass="dataTable" 
         Width="100%" 
         AutoGenerateColumns="false" GridLines="None">
            <AlternatingRowStyle CssClass="oddRow" />
            <RowStyle CssClass="evenRow" />
            <Columns>
                <asp:TemplateField HeaderText="Unit Number" HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" Style="font-family: Lucida Console, Courier New; font-size: 13px" 
                            Text='<%# Server.HtmlEncode (Eval("UnitNumber") & "/" & Eval("RegistrationNumber")) %>' 
                            NavigateUrl='<%# "~/OpsAndLogs/FleetRepairList.aspx?sTxtSearch=" & Server.UrlEncode (Eval("UnitNumber")) %>' /> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Period" HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <asp:HyperLink runat="server"  
                            Text='<%# CType (Eval("StartDateTime"), Date).ToString (Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") & "- <br />" & CType (Eval("EndDateTime"), Date).ToString (Aurora.Common.UserSettings.Current.ComDateFormat & " HH:mm") %>' 
                            NavigateUrl='<%# "~/OpsAndLogs/FleetRepairManagement.aspx?UnitNo=" & Server.UrlEncode (Eval("UnitNumber")) %>' /> 
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reason">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Server.HtmlEncode (Eval ("ReferenceDescription")) %>' /> 
                        <asp:Label runat="server" Text='<%# Iif (string.IsNullOrEmpty (Eval ("ReferenceDescription")), "", "<br/>") %>' /> 
                        <asp:Label runat="server" Text='<%# Server.HtmlEncode (Eval ("ReasonDescription")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="OdometerDue" HeaderText="Serv" HeaderStyle-Width="50px" />
                <asp:BoundField DataField="StartOdometer" HeaderText="Odo Out" HeaderStyle-Width="50px" />

                <asp:TemplateField HeaderText="Provider / Ref" HeaderStyle-Width="120px">
                    <ItemTemplate>
                        <asp:Label runat="server" Text='<%# Server.HtmlEncode (Eval ("RepairerName")) %>' /> 
                        <asp:Label runat="server" Text='<%# Iif (string.IsNullOrEmpty (Eval ("RepairerName")), "", "<br/>") %>' /> 
                        <asp:Label runat="server" Text='<%# Server.HtmlEncode (Eval ("OtherProvider")) %>' />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
                
    </asp:Panel>

</asp:Content>

