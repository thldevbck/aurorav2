﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Include/PopupHeader.master" AutoEventWireup="false"
    CodeFile="PreCheckOutRA.aspx.vb" Inherits="CustomerService_PreCheckOutRA" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
   <style type="text/css">

            @media print
            {
                .noPrint
                {
                    display: none;
                }
            }

            .rentalAgreement
            {
                background-color: white;
            }
            
            .rentalAgreement td
            {
                font-size: 8pt;
                font-family: Arial;
            }
            
            .rentalAgreement table.tableBlackBorder
            {
                 width:100%;
                border:solid 2px black;
            }

            .pagebreak
            {
            page-break-after: always;
            }

        </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%" class="noPrint">
        <tr>
            <td align="right">
                <asp:Button runat="server" ID="ButtonPrintRA" OnClientClick="window.print(); return false;"
                    Text="Print RA" CssClass="Button_Standard" />
            </td>
        </tr>
    </table>
    <asp:Panel ID="pnlMain" runat="server">
        <asp:Literal ID="RentalAgreementLiteral" runat="server" />
    </asp:Panel>
    <table width="100%" class="noPrint">
        <tr>
            <td align="right">
                <asp:Button runat="server" ID="btnClose" OnClientClick="window.close(); return false;"
                    Text="Close" CssClass="Button_Standard Button_Close" />
            </td>
        </tr>
    </table>
</asp:Content>
