<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DailyCloseOffReportViewer.aspx.vb" Inherits="CustomerService_DailyCloseOffReportViewer"  MasterPageFile="~/Include/PopupHeader.master" validateRequest="false" %>

<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="ucMessagePanel" %>

<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="StylePlaceHolder">

<style type="text/css">
@media print
{
    .pnlParams
    {
        display: none;
    }
}

.pnlMain
{
    background-color: white;
    padding: 5px;
}
.pnlMain H2
{
	page-break-after: always;
}
.pnlMain TD
{
	font-size: 8pt;
	font-family: Courier New		
}
</style>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:UpdatePanel ID="updParams" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlParams" runat="server" CssClass="pnlParams">
        
                <table border="0" width="100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="150px" rowspan="3" valign="top" style="padding-top: 6px">Send By:</td>
                        <td width="150px">
                            <asp:RadioButton 
                                ID="radEmail" 
                                GroupName="rgSendMethod" 
                                runat="server" 
                                Checked="true" 
                                Text="Email Address:" />
                        </td>
                        <td style="padding-top: 4px">
                            <uc1:RegExTextBox 
                                ID="txtEmailAddress" 
                                runat="server" 
                                Width="300px"
                                RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"
                                ErrorMessage="Enter a valid Email Address"
                                Nullable="false" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton 
                                ID="radPrint" 
                                GroupName="rgSendMethod" 
                                runat="server" 
                                Text="Printer" />
                        </td>
                        <td colspan="2" align="right">
                            <asp:Button 
                                ID="btnSend" 
                                runat="server" 
                                Text="Send" 
                                CssClass="Button_Standard Button_OK" />
                        </td>
                    </tr>
                </table>
            
                <hr />
    
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel ID="pnlMain" runat="server" CssClass="pnlMain">
        <asp:Literal ID="dailyCloseOffXmlLiteral" runat="server" />
    </asp:Panel>
    
    <br />

    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button 
                    runat="server" 
                    ID="btnClose" 
                    OnClientClick="window.close(); return false;" 
                    Text="Close" 
                    CssClass="Button_Standard Button_Close" />
            </td>
        </tr>
    </table>
    
</asp:Content>


