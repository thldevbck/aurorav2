﻿Imports System.Xml
Imports Aurora.Booking.Services
Imports Aurora.Common.Logging
Imports System.IO
Imports System.Xml.Xsl

Partial Class CustomerService_PreCheckoutListener_Precheckoutlistener
    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim qs As String = Request.QueryString("Mode")
        Dim returnedvalue As String = ""
        Dim encoded As String = ""

        Try
            If Not String.IsNullOrEmpty(qs) Then

                If qs.Equals("CHECKOUT") Then
                    encoded = Request("CHECKOUT_PARAMETER")
                    LogDebug("Precheckoutlistener: CHECKOUT_PARAMETER : ", encoded)

                    Dim BookingId As String = encoded.Split("|")(0).TrimEnd
                    Dim RentalId As String = encoded.Split("|")(1).TrimEnd
                    Dim UserCode As String = encoded.Split("|")(2).TrimEnd
                    Dim RentalCountryCode As String = encoded.Split("|")(3).TrimEnd

                    LogDebug("Precheckoutlistener: Parameters : BookingId : ", BookingId & ",RentalId:  " & RentalId & ", UserCode: " & UserCode & ", RentalCountryCode: " & RentalCountryCode)
                    Dim oScreenData As XmlDocument = PrepareCheckOutData(BookingId, RentalId, UserCode, RentalCountryCode)
                    LogDebug("Precheckoutlistener: PrepareCheckOutData : ", oScreenData.OuterXml)
                    oScreenData = CheckOut(oScreenData, UserCode)
                    LogDebug("Precheckoutlistener: CheckOut : ", oScreenData.OuterXml)

                    If Not oScreenData.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
                        returnedvalue = String.Concat("ERROR: ", oScreenData.DocumentElement.SelectSingleNode("Error/Message").InnerText)
                    Else
                        returnedvalue = "OK: Check-Out was successful!"
                    End If

                ElseIf (qs.Equals("RESET")) Then
                    encoded = Request("RESET_PARAMETER")
                    LogDebug("Precheckoutlistener: Parameters for ScheduledAgainForReset(): checkout location : ", encoded)
                    returnedvalue = ScheduledAgainForReset(encoded.Split("-")(0).Trim)
                    If (String.IsNullOrEmpty(returnedvalue)) Then

                        Dim buttonStyle As String = ""
                        'Dim buttonisactive As Boolean = PreCheckoutResetButtonActivation(encoded.Split("-")(0).Trim)
                        'If (Not buttonisactive) Then
                        '    buttonStyle = "disabled"
                        'End If

                        returnedvalue = "OK: ReRun date  was successful!" & buttonStyle
                    End If
                    LogDebug("Precheckoutlistener: ScheduledAgainForReset() result : ", returnedvalue)

                ElseIf (qs.Equals("NOTES")) Then
                    encoded = Request("NOTES_PARAMETER")
                    LogDebug("Precheckoutlistener: Parameters for GetPrecheckOutNotes(): Rental : ", encoded)
                    returnedvalue = GetPrecheckOutNotes(encoded.Trim)
                    LogDebug("Precheckoutlistener:GetPrecheckOutNotes() result : ", returnedvalue)
                ElseIf (qs.Equals("RENTALAGREEMENTs")) Then
                    encoded = Request("RENTALAGREEMENT_PARAMETER")
                    LogDebug("Precheckoutlistener: Parameters for DisplayRentalAgreement(): Rental : ", encoded)
                    returnedvalue = DisplayRentalAgreement(encoded.Trim)
                    LogDebug("Precheckoutlistener: DisplayRentalAgreement(): result : ", returnedvalue)
                End If

            End If
        Catch ex As Exception
            returnedvalue = String.Concat("ERROR: ", ex.Message)
        End Try

        Response.Write(returnedvalue)
        Response.End()
    End Sub

    Private Function PrepareCheckOutData(ByVal BookingId As String, _
                                  ByVal RentalId As String, _
                                  ByVal UserCode As String, _
                                  ByVal RentalCountryCode As String) As XmlDocument

        Dim oXmlScreenData As XmlDocument = Aurora.Booking.Services.BookingCheckInCheckOut.GetDataForCheckInCheckOut(BookingId, RentalId, UserCode, 0, "", RentalCountryCode)
        Try
            Dim elem As XmlElement
            If (oXmlScreenData.DocumentElement.SelectSingleNode("Forced") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("Forced")
                elem.InnerText = 1
                oXmlScreenData.DocumentElement.AppendChild(elem)
            Else
                oXmlScreenData.DocumentElement.SelectSingleNode("Forced").InnerText = 1
            End If

            If (oXmlScreenData.DocumentElement.SelectSingleNode("UnitNoOld") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("UnitNoOld")
                elem.InnerText = String.Empty
                oXmlScreenData.DocumentElement.AppendChild(elem)
            End If

            If (oXmlScreenData.DocumentElement.SelectSingleNode("VehicleOld") Is Nothing) Then
                elem = oXmlScreenData.CreateElement("VehicleOld")
                elem.InnerText = String.Empty
                oXmlScreenData.DocumentElement.AppendChild(elem)
            End If

            With oXmlScreenData.DocumentElement
                .SelectSingleNode("RntCkoTime").InnerText = String.Concat(Now.Hour, ":", Now.Minute)
                .SelectSingleNode("Vehicle").InnerText = .SelectSingleNode("Vehicle").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("UserLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("RntCkiLocCode").InnerText = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("YrLoc").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
                .SelectSingleNode("DispLoc").InnerText = .SelectSingleNode("DispLoc").InnerText.Split("-"c)(0).Trim()
            End With

        Catch ex As Exception
            LogDebug("Precheckoutlistener: CheckOut Error: ", ex.Message & "," & ex.StackTrace)
        End Try

        Return oXmlScreenData
    End Function

    Private Function CheckOut(ByVal oXmlScreenData As XmlDocument, _
                              ByVal UserCode As String) As XmlDocument
        Dim oRetXml As System.Xml.XmlDocument
        Dim sErr As String = ""

        LogDebug("Precheckoutlistener: CheckOut Parameters for ManageCheckOut : oXmlScreenData : ", oXmlScreenData.OuterXml & ", UserCode: " & UserCode)

        oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.ManageCheckOut(oXmlScreenData, _
                                UserCode, _
                                "Booking/CheckInCheckOut", _
                                "0", _
                                sErr, _
                                False, _
                                 String.Empty, _
                            )

        Return oRetXml
    End Function

    ''rev:mia Aug.27 2012 - addition of button to reset the scheduled time for precheckout running
    ''                      today in philippines history: smart-gilas 2 won the jones cup by defeating US team
    ''                      World history: neil armstrong died two days back

    Private Function ScheduledAgainForReset(ckoBranch As String) As String
        Return Aurora.CustomerService.Services.PreCheckout.PreCheckoutResetSchedule(ckoBranch)
    End Function

    Private Function GetPrecheckOutNotes(rentalid As String) As String
        Return Aurora.CustomerService.Services.PreCheckout.GetPreCheckOutNotes(rentalid)
    End Function

    Function PreCheckoutResetButtonActivation(ByVal ckoBranch As String) As Boolean
        Return Convert.ToBoolean(Aurora.CustomerService.Services.PreCheckout.PreCheckoutResetButtonActivation(ckoBranch))
    End Function


    Function BuildRentalAgreement(sListOfRentalID As String) As String
        Return Aurora.CustomerService.Services.PreCheckout.BuildRentalAgreement(sListOfRentalID)
    End Function

    Function DisplayRentalAgreement(sListOfRentalID As String) As String
        Dim xmlDoc As New XmlDocument
        Dim result As String = ""
        Try

            Dim stringXml As String = BuildRentalAgreement(sListOfRentalID)
            xmlDoc.LoadXml(stringXml)

            Dim xslt As New XslCompiledTransform
            xslt.Load(Server.MapPath(Me.ResolveUrl("~/Booking/xsl/CKO_RentalAgreement_NonPrem.xsl")))
            Using writer As StringWriter = New StringWriter
                Using xmlReader As XmlReader = New XmlNodeReader(xmlDoc)
                    Using xmlWriter As XmlWriter = New XmlTextWriter(writer)
                        xslt.Transform(xmlReader, xmlWriter)
                    End Using
                End Using

                result = writer.ToString()
            End Using


        Catch ex As Exception
            Return "ERROR:" & ex.Message
        End Try

        Return result

    End Function

End Class
