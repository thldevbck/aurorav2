<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="DailyCloseOff.aspx.vb" Inherits="CustomerService_DailyCloseOff" Title="Daily Close Off" %>

<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/WizardControl/WizardControl.ascx" TagName="WizardControl"
    TagPrefix="uc1" %>
    
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>    

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
  <object id="dpsEftX" classid="CLSID:92EDD80C-B50B-4B85-9AE9-9B3E6C63DED9" width="0"
        height="0">
    </object>
</asp:Content>
<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
   <%-- <script src="../Booking/Jquery/jquery-1.3.2.js" type="text/javascript"></script> --%>
    <script src="Js/DailyCloseOff.aspx.js" type="text/javascript"></script> 
    
    <script for="dpsEftX" event="SettlementDoneEvent()">
        SettlementAndCutOverDone()
    </script>
   
   <script for="dpsEftX" event="LogonDoneEvent()">
        LogonDoneEvent ()
    </script>
   
    <script for="dpsEftX" event="OnlineEvent()">
		  StatusChecking()
    </script>

    <script for="dpsEftX" event="StatusChangedEvent()">
		  StatusChecking();
    </script>
   
    
    <script language="javascript" type="text/javascript">
     var overallstatus
     var isclick
     var prm = Sys.WebForms.PageRequestManager.getInstance();

     prm.add_endRequest(EndRequestHandler);
     Sys.Application.add_load(loadhandler);

     function loadhandler() {
         dpsEftX.PrinterName = $('[id$=_HiddenPrinterName]').val()
         $('#btnEnquiry').attr("disabled", true)
         $('#btnSettle').attr("disabled", true)
     }

     function EndRequestHandler(sender, args) {
//         if (!dpsEftX.Ready)
//             $("#btnMerchantLogon").trigger("click")

         dpsEftX.PrinterName = $('[id$=_HiddenPrinterName]').val()
     }

     function StatusChecking() { overallstatus = dpsEftX.Ready; }

    //|eftpos functions|//
     function Enquiry()
     {
        dpsEftX.EnablePrintReceipt = 0
        var enqdate = $('#ctl00_ContentPlaceHolder_DateControlForEnquiry_dateTextBox').val()
        
        if(enqdate.length == 10)
        {
              var enqdateArr = enqdate.split("/")
              enqdate = enqdateArr[2] + enqdateArr[1] + enqdateArr[0]    
        }
        else return;
        
        dpsEftX.DateSettlement = enqdate
        //dpsEftX.Parameter1("Enquiry")
        dpsEftX.Parameter1($('#EnquirySelect').val())
        dpsEftX.DoSettlement()
     } 
     
     function Settlement()
     {
        dpsEftX.EnablePrintReceipt = 0
        dpsEftX.Parameter1("cutover")
        dpsEftX.DoSettlement()
     }
     
     function SettlementAndCutOverDone()
     {
      PrintReceipt(dpsEftX.Receipt,'<%= me.usercode %>')
     }

     function MerchantLogin() {
         
         var HiddenMemberOfCSREF = $('[id$=_HiddenMemberOfCSREF]').val()
         if (HiddenMemberOfCSREF == "false") {
             setInformationShortMessage("You are not authorized to use this button.")
            return
         }
        if (!dpsEftX.Ready) {
            setInformationShortMessage("DPS connectivity is not ready.")
            return
        }

        if (!dpsEftX.ReadyPinPad) {
            setInformationShortMessage("PinPad is not ready.")
            return
        }

        if (!dpsEftX.ReadyLink) {
            setInformationShortMessage("DPS link is not ready.")
            return
        }

         if (isclick == true) return;
         $("#btnMerchantLogon").attr("disabled", true)
         dpsEftX.Account =  "1"//$("#AccountSelect").val()
         dpsEftX.EnableInvisible = 0
         dpsEftX.DoLogon()
         isclick = true
     }

     function LogonDoneEvent() {
         //alert(dpsEftX.ResponseText)

         if (dpsEftX.ResponseText == 'ACCEPTED') {
             setInformationShortMessage(dpsEftX.ResponseText)
             $('#btnEnquiry').attr("disabled", false).removeClass().addClass("Button_Standard")
             $('#btnSettle').attr("disabled", false).removeClass().addClass("Button_Standard")
             $('#ctl00_ContentPlaceHolder_DateControlForEnquiry_dateTextBox').attr("disabled", false).removeClass()
             isclick = false;
             $("#btnMerchantLogon").attr("disabled", false)
         }
         else {
             setErrorShortMessage(dpsEftX.ResponseText)
             $('#btnEnquiry').attr("disabled", true)
             $('#btnSettle').attr("disabled", true)
             $('#ctl00_ContentPlaceHolder_DateControlForEnquiry_dateTextBox').attr("disabled", true)
             isclick = false;
             $("#btnMerchantLogon").attr("disabled", false)
         }
         //alert(dpsEftX.ResponseText)
         PrintReceipt(dpsEftX.ResponseText, '<%= me.usercode %>')
     }

    //|eftpos functions|//
    
    function LoadReport(url)
    {
        var win = window.open(url , "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=" + screen.availHeight + " ,width=" + screen.availWidth)
        win.resizeTo(win.screen.availWidth,win.screen.availHeight);
        win.moveTo(0,0);
    }

    function ChangeTitle(NewTitle,LabelId)
    {
        document.getElementById(LabelId).innerText = NewTitle;
        document.title = NewTitle;
    }

    function CalculateTotal()
    {
        var txtCount = event.srcElement;
        if(isNaN(parseInt(txtCount.value)))
            txtCount.value = 0;
        var Denomination = txtCount.parentNode.parentNode.childNodes[1].childNodes[1].innerText;
        var txtTotal = txtCount.parentNode.nextSibling.childNodes[0];
        var txtFooterTotal = txtCount.parentNode.parentNode.parentNode.nextSibling.childNodes[0].childNodes[txtCount.parentElement.cellIndex+1].childNodes[0];
        txtTotal.value = (txtCount.value * Denomination).toFixed(2);
        var dblFooterTotal = 0.0;
        for(var i =0 ; i<txtCount.parentNode.parentNode.parentNode.childNodes.length;i++)
            dblFooterTotal = parseFloat(dblFooterTotal) + parseFloat(txtCount.parentNode.parentNode.parentNode.childNodes[i].childNodes[txtCount.parentElement.cellIndex+1].childNodes[0].value);
        txtFooterTotal.value = dblFooterTotal.toFixed(2);
        
    }

    function CheckKey()
    {
        var nVal = window.event.keyCode
        if ((nVal < 48 || nVal > 58)&&(nVal!=45))
	        if(document.selection.type!="Text")
	            window.event.keyCode = 8
    }

    function RecalculateFloatTransactions(RevenueID, ExpenseID, TotalID)
    {
        var txtTotal =  document.getElementById(TotalID);
        var txtRevenue = document.getElementById(RevenueID);
        var txtExpense = document.getElementById(ExpenseID);
        
        if(isNaN(parseFloat(txtRevenue.value)))
            txtRevenue.value = 0.0;
        if(isNaN(parseFloat(txtExpense.value)))
            txtExpense.value = 0.0;
        txtTotal.value = (parseFloat(txtRevenue.value) - parseFloat(txtExpense.value)).toFixed(2);
    }

    function UpdateRowTotal(txtLCYTotalID,FixedBranchCCTotalId, FixedBranchLCYTotalId, FixedBranchFCYTotalId, FixedBranchFinalTotalId)
    {
         var txtBranchAmount = event.srcElement;
         if(isNaN(parseInt(txtBranchAmount.value)))
            txtBranchAmount.value = 0;
         txtBranchAmount.value = trimDecimal(txtBranchAmount.value,2);
         
         var txtAuroraTotal = txtBranchAmount.parentNode.nextSibling.childNodes[0];
         var lblDifference = txtAuroraTotal.parentNode.nextSibling.childNodes[0];
         
         lblDifference.innerText  = (parseFloat(txtBranchAmount.value) - parseFloat(txtAuroraTotal.value)).toFixed(2);
         
         var txtLCYTotal = document.getElementById(txtLCYTotalID);
         var tBody = txtBranchAmount.parentElement.parentElement.parentElement;
         var total = 0.00;
         for(var i = 0;i<tBody.children.length;i++)
            total = parseFloat(total) + parseFloat(tBody.children(i).children(1).children(0).value);
         
         txtLCYTotal.value = total.toFixed(2);
         
         var CCTotal = document.getElementById(FixedBranchCCTotalId);
         var LCYTotal = document.getElementById(FixedBranchLCYTotalId);
         var FCYTotal = document.getElementById(FixedBranchFCYTotalId);
         var FinalTotal = document.getElementById(FixedBranchFinalTotalId);
         
         FinalTotal.value = (parseFloat(CCTotal.value) + parseFloat(LCYTotal.value) + parseFloat(FCYTotal.value)).toFixed(2) ;

    }

    function UpdateCrdFwd(CCAmexId, CCOtherId, LCAmountId, FCAmountId, FloatRevExpId, CashBankedId,FloatBankedId, NetChangeId, TotalBankingId, TotCashChqSunId, ActualBankId, VarianceId ,StrReconciledId )
    {   
        if(isNaN(parseFloat(event.srcElement.value)))
            event.srcElement.value = "0.00";
        
        event.srcElement.value = trimDecimal(parseFloat(event.srcElement.value),2);
        document.getElementById(CashBankedId).value = trimDecimal(parseFloat(document.getElementById(FloatBankedId).value),2);

	    var totalBanking = 
		    parseFloat(document.getElementById(CCAmexId).value) + 
		    parseFloat(document.getElementById(CCOtherId).value) + 
		    parseFloat(document.getElementById(LCAmountId).value) + 
		    parseFloat(document.getElementById(FCAmountId).value) + 
		    parseFloat(document.getElementById(FloatRevExpId).value) + 
		    parseFloat(document.getElementById(CashBankedId).value) - 
		    parseFloat(document.getElementById(NetChangeId).value);
    		
	    document.getElementById(TotalBankingId).value = trimDecimal(totalBanking, 2);
    	
	    var totCash_Chk_Sun = parseFloat(totalBanking) - (parseFloat(document.getElementById(CCAmexId).value) + parseFloat(document.getElementById(CCOtherId).value));
    	
	    document.getElementById(TotCashChqSunId).value = trimDecimal(totCash_Chk_Sun, 2)
    	
	    var variance =	parseFloat(document.getElementById(LCAmountId).value)  +
					    parseFloat(document.getElementById(FCAmountId).value) +
					    parseFloat(document.getElementById(FloatRevExpId).value) +
					    parseFloat(document.getElementById(CashBankedId).value) -						
					    parseFloat(document.getElementById(NetChangeId).value) -
					    parseFloat(document.getElementById(ActualBankId).value);
    					
	    document.getElementById(VarianceId).value = trimDecimal(variance, 2);
    	
	    if (parseFloat(variance) == 0)
	    {
		    document.getElementById(StrReconciledId).innerText = 'RECONCILED';
		    document.getElementById(StrReconciledId).style.color = 'black';
	    }
	    else
	    {
		    document.getElementById(StrReconciledId).innerText = 'NOT RECONCILED';
		    document.getElementById(StrReconciledId).style.color = 'red';
	    }
    }


function CheckContents(field,defaultValue)
{
    if(isNaN(field.value))
    {
        if(defaultValue == null)
        {
            field.value="";
        }
        else
        {
            field.value=defaultValue;
        }                
    }
}
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <uc1:WizardControl ID="wizWizard" SelectedIndex="0" runat="server">
                <Items>
                    <asp:ListItem>Start</asp:ListItem>
                    <asp:ListItem>Float Count</asp:ListItem>
                    <asp:ListItem>Account Reconciliation</asp:ListItem>
                    <asp:ListItem>Payment Reconciliation</asp:ListItem>
                    <asp:ListItem>End</asp:ListItem>
                </Items>
            </uc1:WizardControl>
            <hr />
            <uc1:ConfirmationBoxControl ID="cnfbxConfirmation" runat="server" LeftButton_AutoPostBack="true"
                LeftButton_Text="Yes" RightButton_AutoPostBack="false" RightButton_Text="No"
                Title="Confirmation Required" Text="Are you sure you want to Initiate the Daily Close Off?"
                MessageType="Warning" />
            <%----------------------------------------------------------------------------------------------------------------------%>
            

            <div id="divInitial" runat="server">
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tbody>
                        <tr>
                            <td width="150px">
                                Date:
                            </td>
                            <td width="250px">
                                <asp:DropDownList ID="cmbCloseOffDate" runat="Server" CssClass="Dropdown_Standard">
                                </asp:DropDownList>
                            </td>
                            <td nowrap width="150px">
                                Branch:
                            </td>
                            <td>
                                <asp:TextBox ID="lblLocation" runat="server" class="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                            </td>
                            <td>
                                <asp:TextBox ID="lblMessage" runat="server" class="Textbox_Large" Style="width: 250px"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <%----------------------------------------------------------------------------------------------------------------------%>
                        <%--Print out for Enquiry and Settlement--%>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td width="100px" style="display:none">
                                Account Number:
                            </td>
                            <td>
                                <select id="AccountSelect" style="display:none" >
                                    <option value="1" selected="selected">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                </select> 
                            </td>
                            <td align="right" colspan="4">
                                    <input id="btnMerchantLogon" type="button" value="Merchant Logon"    class="Button_Standard"  onclick="return MerchantLogin()"/>
                            </td>
                         
                        </tr>
                         <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                        
                            <td width = "200px">
                                    Enquiry Date and Options:
                             </td>
                            <td>
                                <uc1:DateControl ID="DateControlForEnquiry" runat="server" AutoPostBack="false" />
                        <%--    </td>
                            <td>
                        --%>        <select id="EnquirySelect">
                                    <option value="Enquiry" selected="selected">Enquiry</option>
                                    <option value="SubTotalEnquiry">SubTotal And Enquiry</option>
                                    <option value="SubTotal">SubTotal</option>
                                </select> 
                            </td>
                            
                            <td align="right" colspan="4">
                                
                                <input id="btnEnquiry"       type="button" value="EFTPOS Enquiry"    class="Button_Standard" onclick="return Enquiry()"/>
                                <input id="btnSettle"        type="button" value="EFTPOS Settlement" class="Button_Standard" onclick="return Settlement()"/>
                            </td>
                        </tr>
                        <%----------------------------------------------------------------------------------------------------------------------%>
                        
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button ID="btnDailyCloseOff" runat="server" Text="Start" CssClass="Button_Standard Button_Next" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <%----------------------------------------------------------------------------------------------------------------------%>
            <div id="divSecond" runat="server">
                <table>
                    <tr>
                        <td width="100%">
                            <asp:Repeater ID="rptFloatCount" runat="server" EnableViewState="true">
                                <HeaderTemplate>
                                    <table width="100%" cellspacing="1" cellpadding="1" border="0">
                                        <thead>
                                            <tr>
                                                <td>
                                                </td>
                                                <td>
                                                </td>
                                                <td align="center" colspan="2">
                                                    <b>Aurora Cash Drawer</b></td>
                                                <td>
                                                </td>
                                                <td align="center" colspan="2">
                                                    <b>Float Count</b></td>
                                                <td style="display: none">
                                                </td>
                                                <td align="center" style="display: none">
                                                    <b>Total</b></td>
                                            </tr>
                                            <tr>
                                                <td width="4%">
                                                </td>
                                                <td width="7%">
                                                </td>
                                                <td width="11%" align="right">
                                                    Count</td>
                                                <td width="16%" align="right">
                                                    Dollars</td>
                                                <td width="9%">
                                                </td>
                                                <td width="11%" align="right">
                                                    Count</td>
                                                <td width="16%" align="right">
                                                    Dollars</td>
                                                <td width="26%">
                                                </td>
                                                <td width="*" align="right" style="display: none">
                                                    Dollars</td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td>
                                        </td>
                                        <td align="right" id="dVal">
                                            $<asp:Label ID="lblCurrencyDenominations" runat="server" Text='<%#Container.DataItem("denomination")%>'></asp:Label>
                                        </td>
                                        <td align="right">
                                            <asp:TextBox CssClass="Textbox_Tiny_Right_Aligned" ID="txtCashCount" runat="server"
                                                Text='<%#Container.DataItem("cashcount")%>' MaxLength="5" OnChange="CheckContents(this,'0');return CalculateTotal();"
                                                OnKeyPress="return CheckKey();" TabIndex="1"></asp:TextBox>
                                            <!--INPUT id="idcashcount" dataFld="cashcount" onkeypress="keyStrokeInt()" maxlength="5" size="5" onBlur="calculate(this)" style="TEXT-ALIGN: right" /-->
                                        </td>
                                        <td align="right">
                                            <asp:TextBox CssClass="Textbox_Readonly_Standard_RightAligned" ID="txtCashTotal"
                                                runat="server" Text='<%#Container.DataItem("cashtotal")%>' ReadOnly="true" TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td align="right">
                                            <asp:TextBox CssClass="Textbox_Tiny_Right_Aligned" ID="txtFloatCount" runat="server" 
                                                OnChange="CheckContents(this,'0');return CalculateTotal();" OnKeyPress="return CheckKey();" Text='<%#Container.DataItem("floatcount")%>'
                                                MaxLength="5" TabIndex="2"></asp:TextBox>
                                        </td>
                                        <td align="right">
                                            <asp:TextBox CssClass="Textbox_Readonly_Standard_RightAligned" ID="txtFloatTotal"
                                                runat="server" Text='<%#Container.DataItem("floattotal")%>' ReadOnly="true"  TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td align="right">
                                        </td>
                                        <td style="display: none">
                                            <asp:TextBox ID="txtMoneyTotal" runat="server" Text='<%#Container.DataItem("total")%>'
                                                ReadOnly="true"></asp:TextBox>
                                        </td>
                                        <td style="display: none;">
                                            <asp:Label ID="lblID" runat="server" Text='<%#Container.DataItem("lfid")%>'>
                                            </asp:Label>
                                            <asp:Label ID="lblLocation" runat="server" Text='<%#Container.DataItem("LocCode")%>'>
                                            </asp:Label>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td width="4%">
                                            </td>
                                            <td width="7%">
                                            </td>
                                            <td width="11%" align="right">
                                                Total</td>
                                            <td width="16%" align="right">
                                                <asp:TextBox ID="txtFooterCDTotal" runat="server" CssClass="Textbox_Readonly_Standard_RightAligned"
                                                    ReadOnly="true" TabIndex="-1"></asp:TextBox>
                                            </td>
                                            <td width="9%">
                                            </td>
                                            <td width="11%" align="right">
                                                Total</td>
                                            <td width="16%" align="right">
                                                <asp:TextBox ID="txtFooterFloatTotal" runat="server" CssClass="Textbox_Readonly_Standard_RightAligned"
                                                    ReadOnly="true" TabIndex="-1"></asp:TextBox></td>
                                            <td width="26%">
                                            </td>
                                            <td width="*" align="right" style="display: none">
                                            </td>
                                        </tr>
                                    </tfoot>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </table>
                <table width="100%">
                    <tr>
                        <td>
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSave1" runat="server" Text="Save" CssClass="Button_Standard Button_Save" TabIndex="3" />
                            <asp:Button ID="btnCancel1" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset"  TabIndex="4"/>
                            <asp:Button ID="btnSaveNext1" runat="server" Text="Save/Next" CssClass="Button_Standard Button_Next" TabIndex="5"/>
                        </td>
                    </tr>
                </table>
            </div>
            <%----------------------------------------------------------------------------------------------------------------------%>
            <!--Account Reconcilliation Stuff-->
            <div id="divThird" runat="server">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <thead>
                        <tr>
                            <td width="150px">
                            </td>
                            <td width="150px">
                            </td>
                            <td width="150px">
                                <b>Branch Total</b></td>
                            <td width="150px">
                                <b>Aurora Total</b></td>
                            <td width="100px">
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td width="150px" valign="top">
                                <b>Credit Cards/EFT POS</b>
                            </td>
                            <td colspan="4" width="550px">
                                <!-- new table with CC info -->
                                <asp:Repeater ID="rptReconCreditCards" runat="server">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="150px">
                                                <asp:Label ID="lblCCName" runat="server" Text='<%#Container.DataItem("Label")%>'></asp:Label>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtCCBranchAmount" runat="server" CssClass="Textbox_Small_Right_Aligned"
                                                    Text='<%#Container.DataItem("BTAmt")%>' MaxLength="10" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"></asp:TextBox>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtCCAuroraAmount" runat="server" CssClass="Textbox_Readonly_Small_RightAligned"
                                                    Text='<%#Container.DataItem("TtlAmt")%>' ReadOnly="true" TabIndex="-1"></asp:TextBox>
                                            <td width="100px">
                                                <asp:Label ID="lblCCDiff" runat="server" Text='<%#Container.DataItem("diff")%>' CssClass="Label_Right_Aligned"></asp:Label>
                                            </td>
                                            <td style="display: none;">
                                                <!-- this holds the ids etc-->
                                                <asp:TextBox ID="txtCCPID" runat="server" Text='<%#Container.DataItem("pid")%>'></asp:TextBox>
                                                <asp:TextBox ID="txtCCCodCode" runat="server" Text='<%#Container.DataItem("CodCode")%>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <!-- new table with CC info -->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150px">
                                <b>Credit Card Imprints</b></td>
                            <td colspan="4" width="550px">
                                <asp:Repeater ID="rptCreditCardImprints" runat="server" EnableViewState="true">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="150px">
                                                <asp:Label ID="lblCCIName" runat="server" Text='<%#Container.DataItem("Label")%>'></asp:Label>
                                            </td>
                                            <td width="150px">
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox runat="server" ID="txtCCITotalAmount" Text='<%#Container.DataItem("TtlAmt")%>'
                                                    ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned" TabIndex="-1"></asp:TextBox>
                                            </td>
                                            <td width="100px">
                                            </td>
                                            <td width="*" style="display: none;">
                                                <asp:TextBox ID="txtCCIReadOnly" runat="server" Text='<%#Container.DataItem("ReadOnly")%>'></asp:TextBox>
                                                <asp:TextBox ID="txtCCIPID" runat="server" Text='<%#Container.DataItem("pid")%>'></asp:TextBox>
                                                <asp:TextBox ID="txtCCIBTAmt" runat="server" Text='<%#Container.DataItem("BTAmt")%>'></asp:TextBox>
                                                <asp:TextBox ID="txtCCiDiff" runat="server" Text='<%#Container.DataItem("diff")%>'></asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150px">
                                <b>Local Currency</b>
                            </td>
                            <td colspan="4" width="550px">
                                <asp:Repeater ID="rptLocalCurrency" runat="server" EnableViewState="true">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="150px">
                                                <asp:Label ID="lblLCYName" runat="server" Text='<%#Container.DataItem("Label")%>'></asp:Label>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtBranchLCYTotal" runat="server" Text='<%#Container.DataItem("BTAmt")%>'
                                                    MaxLength="10" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"></asp:TextBox>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtAuroraLCYTotal" runat="server" Text='<%#Container.DataItem("TtlAmt")%>'
                                                    CssClass="Textbox_Readonly_Small_RightAligned" ReadOnly="true" TabIndex="-1"></asp:TextBox>
                                            </td>
                                            <td width="100px">
                                                <asp:Label ID="lblLCYDifference" runat="server" Text='<%#Container.DataItem("diff")%>'
                                                    CssClass="Label_Right_Aligned"></asp:Label>
                                            </td>
                                            <td style="display: none">
                                                <!--hidden things-->
                                                <asp:TextBox ID="txtLCYReadOnly" runat="server" Text='<%#Container.DataItem("ReadOnly")%>'>
                                                </asp:TextBox>
                                                <asp:TextBox ID="txtLCYPID" runat="server" Text='<%#Container.DataItem("pid")%>'>
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150px">
                                <b>Foreign Currency<br />
                                    (Local Equivalent)</b>
                            </td>
                            <td colspan="4" width="550px">
                                <!--Foreign Currency-->
                                <asp:Repeater ID="rptForeignCurrency" runat="server" EnableViewState="true">
                                    <HeaderTemplate>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td width="150px">
                                                <asp:Label ID="lblFCYName" runat="server" Text='<%#Container.DataItem("Label")%>'></asp:Label>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtBranchFCYTotal" runat="server" Text='<%#Container.DataItem("BTAmt")%>'
                                                    CssClass="Textbox_Small_Right_Aligned" MaxLength="10" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"></asp:TextBox>
                                            </td>
                                            <td width="150px">
                                                <asp:TextBox ID="txtAuroraFCYTotal" runat="server" Text='<%#Container.DataItem("TtlAmt")%>'
                                                    CssClass="Textbox_Readonly_Small_RightAligned" ReadOnly="true" TabIndex="-1"></asp:TextBox>
                                            </td>
                                            <td width="100px">
                                                <asp:Label ID="lblFCYDifference" runat="server" Text='<%#Container.DataItem("diff")%>'
                                                    CssClass="Label_Right_Aligned"></asp:Label>
                                            </td>
                                            <td style="display: none">
                                                <!--hidden things-->
                                                <asp:TextBox ID="txtFCYPID" runat="server" Text='<%#Container.DataItem("pid")%>'>
                                                </asp:TextBox>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                                <!--Foreign Currency-->
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" width="150px">
                                <b>Total Receipts</b></td>
                            <td colspan="4" width="550px">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="150px">
                                            Credit Cards/EFT POS<br />
                                            (Excludes Imprints)</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtBranchCCTotal" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td style="display: none">
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtAuroraCCTotal" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="100px" style="display: none">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Local Currency</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtBranchLCYTotalAmount" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td style="display: none">
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtAuroraLCYTotalAmount" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="100px">
                                            <asp:Label ID="lblAuroraLCYDiff" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Foreign currency (Local Equivalent)</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtBranchFCYTotalAmount" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td style="display: none">
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtAuroraFCYTotalAmount" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="100px">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Total Received</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtBranchFinalTotal" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td style="display: none">
                                        </td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtAuroraFinalTotal" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="100px">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td width="150px" valign="top">
                                <b>Float</b>
                            </td>
                            <td width="550px"" " colspan="5">
                                <table width="100%" border="0" cellspacing="0">
                                    <tr>
                                        <td width="150px">
                                            Closing Float</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtClosingFloat" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="150px">
                                        </td>
                                        <td width="100px">
                                        </td>
                                        <td width="*" style="display: none">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Less Opening float</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtOpeningFloat" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="150px">
                                        </td>
                                        <td width="100px">
                                        </td>
                                        <td width="*" style="display: none">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Net Change</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtNetChangeInFloat" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="150px">
                                        </td>
                                        <td width="100px">
                                        </td>
                                        <td width="*" style="display: none">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td width="150px" rowspan="1" valign="top">
                                <b>Float Transactions</b>
                            </td>
                            <td width="550px" colspan="5">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="150px">
                                            Revenue</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtFloatTransactionsRevenue" runat="server" CssClass="Textbox_Small_Right_Aligned"
                                                MaxLength="10" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"></asp:TextBox>
                                        </td>
                                        <td width="150px">
                                        </td>
                                        <td width="100px">
                                        </td>
                                        <td width="*" style="display: none">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Less Expenditure</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtFloatTransactionsExpense" runat="server" CssClass="Textbox_Small_Right_Aligned"
                                                MaxLength="10" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"></asp:TextBox>
                                        </td>
                                        <td width="150px">
                                        </td>
                                        <td width="100px">
                                        </td>
                                        <td style="display: none">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150px">
                                            Total</td>
                                        <td width="150px">
                                            <asp:TextBox ID="txtFloatTransactionsTotal" runat="server" ReadOnly="true" CssClass="Textbox_Readonly_Small_RightAligned"
                                                TabIndex="-1"></asp:TextBox>
                                        </td>
                                        <td width="150px">
                                        </td>
                                        <td width="100px">
                                        </td>
                                        <td width="*" style="display: none">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5" align="right">
                                <asp:Button ID="btnSave2" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                                <asp:Button ID="btnRunReport" runat="server" Text="Report" CssClass="Button_Standard Button_Print" />
                                <asp:Button ID="btnCancel2" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                                <asp:Button ID="btnBack2" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                                <asp:Button ID="btnSaveNext2" runat="server" Text="Save/Next" CssClass="Button_Standard Button_Next" />
                            </td>
                        </tr>
                </table>
                <br />
                <br />
            </div>
            <%----------------------------------------------------------------------------------------------------------------------%>
            <!--Account Reconcilliation Stuff-->
            <!--     Payment Reconciliation  -->
            <div id="divFourth" runat="server">
                <asp:Panel ID="pnlLast" runat="server">
                    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                        <tr>
                            <td width="100px">
                            </td>
                            <td width="250px">
                            </td>
                            <td width="100px">
                                <b>Aurora Total </b>
                            </td>
                            <td width="100px">
                            </td>
                            <td width="125px">
                            </td>
                            <td width="125px">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="5">
                                <b>Payment Type</b></td>
                            <td>
                                Receipts</td>
                            <td>
                                <asp:TextBox TabIndex="-1" ReadOnly="true" runat="server" ID="txtPRReceipts" CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Deposits</td>
                            <td>
                                <asp:TextBox TabIndex="-1" ReadOnly="true" runat="server" ID="txtPRDeposits" CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Bonds/Securities (Excludes Imprints)</td>
                            <td>
                                <asp:TextBox TabIndex="-1" ReadOnly="true" runat="server" ID="txtPRSecurities" CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Overpayments</td>
                            <td>
                                <asp:TextBox TabIndex="-1" ReadOnly="true" runat="server" ID="txtPROverPayments"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Expenses</td>
                            <td>
                                <asp:TextBox TabIndex="-1" ReadOnly="true" runat="server" ID="txtPRExpenses" CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <b>Revenue</b>
                            </td>
                            <td>
                                Total Receipts (Excludes Imprints)</td>
                            <td>
                                <asp:TextBox TabIndex="-1" ReadOnly="true" runat="server" ID="txtPRTotalReceipts"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                            <td>
                                <asp:TextBox ID="txtPRBranchCheckTotal" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                            <td>
                                <b>&nbsp;Branch (Check total)</b></td>
                            <td>
                                <b><font style="color: red">
                                    <asp:Label ID="lblPRDifferenceInTotal" TabIndex="-1" runat="server"></asp:Label>
                                </font></b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" rowspan="4">
                                <b>Float Section</b></td>
                            <td>
                                Closing Float</td>
                            <td>
                                <asp:TextBox ID="txtPRClosingFloat" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                        </tr>
                        <tr>
                            <td>
                                Plus Cheque Drawn</td>
                            <td>
                                <asp:TextBox ID="txtPRChequeDrawn" runat="server" CssClass="Textbox_Small_Right_Aligned" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                    MaxLength="10"></asp:TextBox>
                            </td>
                            <td>
                                Cheque Number
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="txtPRChequeNumber" runat="server" CssClass="Textbox_Large" MaxLength="32"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Less Float Banked</td>
                            <td>
                                <asp:TextBox ID="txtPRCashBanked" runat="server" CssClass="Textbox_Small_Right_Aligned" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                    MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Float Carried Forward To Next Day</td>
                            <td>
                                <asp:TextBox ID="txtPRFloatCarriedForward" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="7" valign="top">
                                <b>Total Banking Summary</b></td>
                            <td nowrap="nowrap">
                                Credit Card/EFTPOS - Amex & Diners<br />
                                (Excludes Imprints)</td>
                            <td>
                                <asp:TextBox ID="txtPRCreditCardsAmexDiners" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td nowrap="nowrap">
                                Credit Card/EFTPOS Others<br />
                                (Excludes Imprints)</td>
                            <td>
                                <asp:TextBox ID="txtPRCreditCardsOther" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Local Currency</td>
                            <td>
                                <asp:TextBox ID="txtPRLocalCurrency" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Foreign Currency</td>
                            <td>
                                <asp:TextBox ID="txtPRForeignCurrency" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Float Revenue/Expenditure</td>
                            <td>
                                <asp:TextBox ID="txtPRFloatRevenueExpense" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Cash Banked from Float</td>
                            <td>
                                <asp:TextBox ID="txtPRCashBankedFromFloat" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Less Net Change in Float</td>
                            <td>
                                <asp:TextBox ID="txtPRNetChangeInFloat" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                            </td>
                            <td>
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="4">
                            </td>
                            <td>
                                <b>Total Banking</b></td>
                            <td>
                                <asp:TextBox ID="txtPRTotalBanking" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total Cash, Cheques and Sundry Totals</td>
                            <td>
                                <asp:TextBox ID="txtPRTotalCashChequeSundry" TabIndex="-1" ReadOnly="true" runat="server"
                                    CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Actual Banking</td>
                            <td>
                                <asp:TextBox ID="txtPRActualBanking" runat="server" CssClass="Textbox_Small_Right_Aligned" OnBlur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                    MaxLength="10"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Variance</td>
                            <td>
                                <asp:TextBox ID="txtPRVariance" TabIndex="-1" ReadOnly="true" runat="server" CssClass="Textbox_Readonly_Small_RightAligned"></asp:TextBox>
                            </td>
                            <td>
                                <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="lblReconciliationStatus" runat="server"></asp:Label>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <hr />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td align="right" colspan="6">
                            <asp:Button ID="btnSave3" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                            <asp:Button ID="btnPrint" runat="server" Text="Print" CssClass="Button_Standard Button_Print" />
                            <asp:Button ID="btnCancel3" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                            <asp:Button ID="btnBack3" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                            <asp:Button ID="btnReconcile" runat="server" Text="Reconcile" CssClass="Button_Standard Button_Next" />
                        </td>
                    </tr>
                </table>
            </div>
            <!--     Payment Reconciliation  -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="HiddenPrinterName" runat="server" value=""/>   
    <asp:HiddenField ID="HiddenMemberOfCSREF" runat="server" value="false"/>   
</asp:Content>
