﻿Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO

<AuroraPageTitleAttribute("Rental Agreement")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PreCheckout)> _
Partial Class CustomerService_PreCheckOutRA
    Inherits AuroraPage

    Protected Sub CustomerService_PreCheckOutRA_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            If (Not Request.QueryString("RA") Is Nothing) Then
                Dim stringRA As String = DisplayRentalAgreement(Request.QueryString("RA"))
                If Not String.IsNullOrEmpty(stringRA) Then
                    RentalAgreementLiteral.Text = DisplayRentalAgreement(Request.QueryString("RA"))
                End If
            End If
        End If
    End Sub

    Function BuildRentalAgreement(sListOfRentalID As String) As String
        Return Aurora.CustomerService.Services.PreCheckout.BuildRentalAgreement(sListOfRentalID)
    End Function

    Function DisplayRentalAgreement(sListOfRentalID As String) As String
        Dim xmlDoc As New XmlDocument
        Dim result As String = ""
        Try

            Dim stringXml As String = BuildRentalAgreement(sListOfRentalID)
            If (stringXml.IndexOf("Error") <> -1) Then
                SetErrorMessage(stringXml)
                Return Nothing
            End If

            xmlDoc.LoadXml(stringXml)

            Dim xslt As New XslCompiledTransform
            xslt.Load(Server.MapPath(Me.ResolveUrl("~/Booking/xsl/CKO_RentalAgreement_NonPrem.xsl")))
            Using writer As StringWriter = New StringWriter
                Using xmlReader As XmlReader = New XmlNodeReader(xmlDoc)
                    Using xmlWriter As XmlWriter = New XmlTextWriter(writer)
                        xslt.Transform(xmlReader, xmlWriter)
                    End Using
                End Using

                result = writer.ToString()
            End Using


        Catch ex As Exception
            Return "ERROR:" & ex.Message
        End Try

        LogDebug("DisplayRentalAgreement With XSL: " & result)
        Return result

    End Function
End Class
