'' Change Log!
'' 14.7.8 / Shoel / Now the .htm is saved with branchname and type . eg. "AKL_28-06-2008_BYTYPE.htm" and "AKL_28-06-2008_BYMETHOD.htm"
'' 29.9.8 / Shoel / Now the mail is sent using the current user's email id (if available) or by DailyCloseOff_<Branchcode>@thlonline.com

Imports system.Xml
Imports System.IO
Imports System.Xml.Xsl
Imports System.Xml.XPath
Imports Aurora


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DailyCloseOffFloatCount)> _
Partial Class CustomerService_DailyCloseOffReportViewer
    Inherits AuroraPage

    Private reportUserCode As String
    Private reportDate As String
    Private reportType As String
    Private branchCode As String

    Private fileName As String
    Private filePath As String


    Public Overrides ReadOnly Property PageTitle() As String
        Get
            Select Case Request("reportType")
                Case "BYMETHOD"
                    Return "Daily Close Off Account Reconciliation by Method"
                Case "BYTYPE"
                    Return "Daily Close Off Account Reconciliation by Type"
            End Select
        End Get
    End Property

    Private Sub BuildForm()
        reportUserCode = Request("UserCode")
        reportDate = Request("ReportDate")
        reportType = Request("ReportType")
        branchCode = Request("Branch")

        Dim sReportPath As String = ""
        Dim xmlDoc As XmlDocument

        Select Case reportType
            Case "BYMETHOD"
                sReportPath = Server.MapPath("") & "\Xsl\DailyCloseOff_AccRec_PmntByMethod.xsl"
                xmlDoc = Aurora.CustomerService.Services.DailyCloseOff.GetPaymentTransactionReportByMethod(UserCode, reportDate)
            Case "BYTYPE"
                sReportPath = Server.MapPath("") & "\Xsl\DailyCloseOff_AccRec_PmntByType.xsl"
                xmlDoc = Aurora.CustomerService.Services.DailyCloseOff.GetPaymentTransactionReportByType(UserCode, reportDate)
            Case Else
                Throw New Exception("Invalid Report Type")
        End Select

        xmlDoc.InnerXml = xmlDoc.InnerXml.Replace("data>", "Data>")

        Dim transform As New XslTransform()
        transform.Load(sReportPath)

        Using writer As New StringWriter
            transform.Transform(xmlDoc, Nothing, writer, Nothing)

            dailyCloseOffXmlLiteral.Text = writer.ToString

            ' save the contents - in case needed later'DCOREPORTPATH
            filePath = Aurora.CustomerService.Services.DailyCloseOff.GetDCOReportsPath() & "\"
            fileName = branchCode & "_" & reportDate.Replace("\"c, "-"c).Replace("/"c, "-"c) & "_" & reportType '& "_" & Guid.NewGuid().ToString()

            'If Not File.Exists(filePath & fileName & ".htm") Then
            'Aurora.Booking.Services.BookingConfirmation.SaveFile(filePath, fileName, writer.ToString, "htm")
            Try
                System.IO.File.WriteAllText(filePath & fileName & "." & "htm", writer.ToString())
            Catch ex As Exception
            End Try
            'End If
        End Using
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        btnSend.OnClientClick = "if ($get('" & Me.radPrint.ClientID & "').checked) { window.print(); return false; }"

        If IsPostBack Then Return

        Try
            BuildForm()
        Catch ex As Exception
            Me.SetErrorShortMessage("Error loading Daily Close Off Report")
        End Try
    End Sub

    Private Sub ValidateParams()
        If radEmail.Checked AndAlso Not txtEmailAddress.IsTextValid Then
            Throw New Aurora.Common.ValidationException(txtEmailAddress.ErrorMessage)
        End If
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            BuildForm()
        Catch ex As Exception
            Me.SetErrorShortMessage("Error loading Daily Close Off Report")
            pnlParams.Enabled = False
            Return
        End Try

        Try
            ValidateParams()
        Catch ex As Aurora.Common.ValidationException
            Me.SetWarningShortMessage(ex.Message)
            Return
        End Try

        Try
            If radEmail.Checked Then
                'email
                Dim sUserEmail As String
                sUserEmail = Aurora.CustomerService.Services.DailyCloseOff.GetUsersEMailId(UserCode)
                Aurora.Booking.Services.BookingConfirmation.EMailPdf( _
                    filePath, _
                    fileName, _
                    txtEmailAddress.Text, _
                    IIf(String.IsNullOrEmpty(sUserEmail), "DailyCloseOff_" & branchCode & "@thlonline.com", sUserEmail), _
                    "DAILY CLOSE OFF REPORT - " & reportType & " - " & branchCode)
                Me.SetInformationShortMessage("E-mail sent successfully")
            End If

        Catch ex As Exception
            Me.SetErrorShortMessage(ex.Message)
        End Try

    End Sub


End Class
