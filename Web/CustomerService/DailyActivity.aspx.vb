Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.CustomerService.Data
Imports Aurora.CustomerService.Data.DailyActivityDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DailyActivity)> _
Partial Class CustomerService_DailyActivity
    Inherits AuroraPage

    Protected Shared mStateKey As String = Guid.NewGuid().ToString().Replace("-", "")

    Private Sub BuildDailyActivity()
        Dim dailyActivityDataSet As New DailyActivityDataSet()
        DataRepository.GetDailyActivity(dailyActivityDataSet, _
            Me.UserCode, _
            branchPickerControl.DataId, _
            dailyActivityFromTextBox.Date, _
            includeOverdueCheckBox.Checked, _
            Integer.Parse(sortByDropDown.SelectedValue))

        Dim index As Integer

        checkOutOutstandingTextBox.Text = dailyActivityDataSet.Checkout.TotalOutstanding.ToString()

        While checkOutTable.Rows.Count > 1
            checkOutTable.Rows.RemoveAt(1)
        End While
        index = 0
        For Each checkoutRow As CheckoutRow In dailyActivityDataSet.Checkout
            Dim cssClass As String = ""
            Dim tableRow As New TableRow
            checkOutTable.Rows.Add(tableRow)

            Dim customerCell As New TableCell()
            customerCell.Width = New Unit("125px")
            customerCell.Style.Add(HtmlTextWriterStyle.WhiteSpace, "normal")

            Dim bookingLink As New HyperLink()
            bookingLink.NavigateUrl = "~/Booking/Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & Server.UrlEncode(checkoutRow.BooId) & "&hdRentalId=" & Server.UrlEncode(checkoutRow.RntId) & "&activeTab=7"
            bookingLink.Text = checkoutRow.BooName
            If checkoutRow.RntIsVip Then bookingLink.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            customerCell.Controls.Add(bookingLink)

            If checkoutRow.RntIsVip Then
                customerCell.Controls.Add(New LiteralControl("&nbsp;"))

                Dim vipImage As New System.Web.UI.WebControls.Image()
                vipImage.ImageUrl = "~/Images/vip.gif"
                vipImage.ToolTip = "VIP"
                vipImage.ImageAlign = ImageAlign.AbsBottom
                customerCell.Controls.Add(vipImage)
            End If

            If checkoutRow.RntIsRepeatCustomer Then
                customerCell.Controls.Add(CreateStatusIconTag("R"))
            End If

            If checkoutRow.IsStaffHire Then
                customerCell.Controls.Add(CreateStatusIconTag("S"))
            End If

            tableRow.Controls.Add(customerCell)

            Dim bookingCell As New TableCell()
            bookingCell.Text = Server.HtmlEncode(checkoutRow.BooNum)
            tableRow.Controls.Add(bookingCell)

            Dim outCell As New TableCell()
            outCell.Text = Server.HtmlEncode(checkoutRow.RntCkoWhen.ToString("HH:mm"))
            tableRow.Controls.Add(outCell)

            Dim toCell As New TableCell()
            toCell.Text = Server.HtmlEncode(checkoutRow.Ckidet)
            tableRow.Controls.Add(toCell)

            Dim scheduledCell As New TableCell()
            scheduledCell.Text = Server.HtmlEncode(checkoutRow.FleetModelCode) & "&nbsp;"
            tableRow.Controls.Add(scheduledCell)

            Dim vehicleCell As New TableCell()
            vehicleCell.Text = Server.HtmlEncode(checkoutRow.ProductName)
            tableRow.Controls.Add(vehicleCell)

            Dim statusCell As New TableCell()
            Dim statusImage As New System.Web.UI.WebControls.Image()
            If checkoutRow.IsOverdue Then
                statusImage.ImageUrl = "~/Images/exclamation.gif"
                statusImage.ToolTip = "Overdue"
                cssClass = "Overdue"
            ElseIf checkoutRow.IsDue Then
                statusImage.ImageUrl = "~/Images/time.gif"
                statusImage.ToolTip = "Due"
                cssClass = "Due"
            ElseIf checkoutRow.IsDone Then
                statusImage.ImageUrl = "~/Images/tick_green.gif"
                statusImage.ToolTip = "Done"
                cssClass = "Processed"
               
            Else
                statusImage.ImageUrl = "~/Images/n.gif"
            End If
            statusImage.ImageAlign = ImageAlign.AbsBottom
            statusCell.Controls.Add(statusImage)

            If checkoutRow.RntIsError Then
                Dim errorImage As New System.Web.UI.WebControls.Image()
                errorImage.ImageUrl = "~/Images/rental_error.gif"
                errorImage.ToolTip = "Error"
                errorImage.ImageAlign = ImageAlign.AbsBottom
                statusCell.Controls.Add(errorImage)
            End If

            tableRow.Controls.Add(statusCell)

            'Added by Nimesh on 16th July 2015 to display icon for service overdue
            Dim serviceStatusCell As New TableCell()
            Dim serviceStatusImage As New System.Web.UI.WebControls.Image()
            If checkoutRow.IsServiceDue Then
                serviceStatusImage.ImageUrl = "~/Images/bell.gif"
                serviceStatusImage.ToolTip = "Service Overdue"
                cssClass = "Processed"
                'serviceStatusCell.Text = Server.HtmlEncode("SD")
                serviceStatusCell.Controls.Add(serviceStatusImage)
            End If
            tableRow.Controls.Add(serviceStatusCell)
            'End Added by Nimesh on 16th July 2015

            tableRow.CssClass = IIf((index Mod 2) = 0, "row", "rowalt") & cssClass : index += 1
        Next
        checkOutTable.Visible = dailyActivityDataSet.Checkout.Count > 0

        checkInOutstandingTextBox.Text = dailyActivityDataSet.Checkin.TotalOutstanding.ToString()

        While checkInTable.Rows.Count > 1
            checkInTable.Rows.RemoveAt(1)
        End While
        index = 0
        For Each checkinRow As CheckinRow In dailyActivityDataSet.Checkin
            Dim cssClass As String = ""
            Dim tableRow As New TableRow
            checkInTable.Rows.Add(tableRow)

            Dim customerCell As New TableCell()
            customerCell.Width = New Unit("125px")
            customerCell.Style.Add(HtmlTextWriterStyle.WhiteSpace, "normal")

            Dim bookingLink As New HyperLink()
            bookingLink.NavigateUrl = "~/Booking/Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & Server.UrlEncode(checkinRow.BooId) & "&hdRentalId=" & Server.UrlEncode(checkinRow.RntId) & "&activeTab=7"
            bookingLink.Text = checkinRow.BooName
            If checkinRow.RntIsVip Then bookingLink.Style.Add(HtmlTextWriterStyle.FontWeight, "bold")
            customerCell.Controls.Add(bookingLink)

            If checkinRow.RntIsVip Then
                customerCell.Controls.Add(New LiteralControl("&nbsp;"))

                Dim vipImage As New System.Web.UI.WebControls.Image()
                vipImage.ImageUrl = "~/Images/vip.gif"
                vipImage.ToolTip = "VIP"
                vipImage.ImageAlign = ImageAlign.AbsBottom
                customerCell.Controls.Add(vipImage)
            End If

            If checkinRow.RntIsRepeatCustomer Then
                customerCell.Controls.Add(CreateStatusIconTag("R"))
            End If

            If checkinRow.IsStaffHire Then
                customerCell.Controls.Add(CreateStatusIconTag("S"))
            End If

            If checkinRow.HasIncident Then
                customerCell.Controls.Add(CreateStatusIconTag("I"))
            End If

            tableRow.Controls.Add(customerCell)

            Dim bookingCell As New TableCell()
            bookingCell.Text = Server.HtmlEncode(checkinRow.BooNum)
            tableRow.Controls.Add(bookingCell)

            Dim inCell As New TableCell()
            inCell.Text = Server.HtmlEncode(checkinRow.RntCkiWhen.ToString("HH:mm")) & "&nbsp;&nbsp;"
            tableRow.Controls.Add(inCell)

            Dim vehicleCell As New TableCell()
            vehicleCell.Text = Server.HtmlEncode(checkinRow.BpdUnitNum) & "&nbsp;"
            vehicleCell.Attributes("nowrap") = "nowrap"
            tableRow.Controls.Add(vehicleCell)

            Dim statusCell As New TableCell()
            Dim statusImage As New System.Web.UI.WebControls.Image()
            If checkinRow.IsOverdue Then
                statusImage.ImageUrl = "~/Images/exclamation.gif"
                statusImage.ToolTip = "Overdue"
                cssClass = "Overdue"
            ElseIf checkinRow.IsDue Then
                statusImage.ImageUrl = "~/Images/time.gif"
                statusImage.ToolTip = "Due"
                cssClass = "Due"
            ElseIf checkinRow.IsDone Then
                statusImage.ImageUrl = "~/Images/tick_green.gif"
                statusImage.ToolTip = "Done"
                cssClass = "Processed"
                
            Else
                statusImage.ImageUrl = "~/Images/n.gif"
            End If
            statusImage.ImageAlign = ImageAlign.AbsBottom
            statusCell.Controls.Add(statusImage)
            

            If checkinRow.RntIsError Then
                Dim errorImage As New System.Web.UI.WebControls.Image()
                errorImage.ImageUrl = "~/Images/rental_error.gif"
                errorImage.ToolTip = "Error"
                errorImage.ImageAlign = ImageAlign.AbsBottom
                statusCell.Controls.Add(errorImage)
            End If

            tableRow.Controls.Add(statusCell)
            'Added by Nimesh on 16th July 2015 to display icon for overdue
            Dim serviceStatusCell As New TableCell()
            Dim serviceStatusImage As New System.Web.UI.WebControls.Image()
            If checkinRow.IsServiceDue Then
                serviceStatusImage.ImageUrl = "~/Images/bell.gif"
                serviceStatusImage.ToolTip = "Service Overdue"
                cssClass = "Processed"
                serviceStatusCell.Controls.Add(serviceStatusImage)
                'serviceStatusCell.Text = Server.HtmlEncode("SD")
            End If
            tableRow.Controls.Add(serviceStatusCell)
            'End Added by Nimesh on 16th July 2015

            tableRow.CssClass = IIf((index Mod 2) = 0, "row", "rowalt") & cssClass : index += 1
        Next
        checkInTable.Visible = dailyActivityDataSet.Checkin.Count > 0

        resultsPanel.Visible = True

        ' for links to reports
        'Dim dtResults As System.Data.DataTable
        'dtResults = Aurora.Common.Data.ExecuteTableSql("SELECT FunCode,FunFileName FROM FUNCTIONS WITH (NOLOCK) WHERE FunCode IN ('RPT_ACTBRFCI','RPT_ACTBRFCO','RPT_ACTDETCI','RPT_ACTDETCO')", New System.Data.DataTable)
        'For Each oRow As System.Data.DataRow In dtResults.Rows
        '    Select Case oRow("FunCode")
        '        Case "RPT_ACTBRFCI"
        '            lnkACTBRFCI.NavigateUrl = oRow("FunFileName")
        '        Case "RPT_ACTBRFCO"
        '            lnkACTBRFCO.NavigateUrl = oRow("FunFileName")
        '        Case "RPT_ACTDETCI"
        '            lnkACTDETCI.NavigateUrl = oRow("FunFileName")
        '        Case "RPT_ACTDETCO"
        '            lnkACTDETCO.NavigateUrl = oRow("FunFileName")
        '    End Select
        'Next

        lnkACTBRFCI.Attributes.Add("onclick", "window.open('../Reports/Default.aspx?funCode=RPT_ACTBRFCI');return false;")
        lnkACTBRFCI.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
        lnkACTBRFCI.Attributes.Add("onmouseout", "this.style.cursor=''")

        lnkACTBRFCO.Attributes.Add("onclick", "window.open('../Reports/Default.aspx?funCode=RPT_ACTBRFCO');return false;")
        lnkACTBRFCO.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
        lnkACTBRFCO.Attributes.Add("onmouseout", "this.style.cursor=''")

        lnkACTDETCI.Attributes.Add("onclick", "window.open('../Reports/Default.aspx?funCode=RPT_ACTDETCI');return false;")
        lnkACTDETCI.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
        lnkACTDETCI.Attributes.Add("onmouseout", "this.style.cursor=''")

        lnkACTDETCO.Attributes.Add("onclick", "window.open('../Reports/Default.aspx?funCode=RPT_ACTDETCO');return false;")
        lnkACTDETCO.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
        lnkACTDETCO.Attributes.Add("onmouseout", "this.style.cursor=''")

        lnkBMSClass.Attributes.Add("onclick", "window.open('../Reports/Default.aspx?funCode=RPT_BRVEHCLA');return false;")
        lnkBMSClass.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
        lnkBMSClass.Attributes.Add("onmouseout", "this.style.cursor=''")

        lnkBMSType.Attributes.Add("onclick", "window.open('../Reports/Default.aspx?funCode=RPT_BRVEHTYP');return false;")
        lnkBMSType.Attributes.Add("onmouseover", "this.style.cursor='pointer'")
        lnkBMSType.Attributes.Add("onmouseout", "this.style.cursor=''")
    End Sub

    Protected Function CreateStatusIconTag(ByVal text As String) As HtmlControl
        Dim span As New HtmlGenericControl("span")
        span.InnerText = text
        span.Attributes("class") = "dailyActivityRedIcon"
        Return span
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then Return

        RestoreState()

        resultsPanel.Visible = False

        If String.IsNullOrEmpty(dailyActivityFromTextBox.Text) Then
            dailyActivityFromTextBox.Date = Date.Now.Date
        End If

        If Not String.IsNullOrEmpty(UserSettings.Current.LocCode) AndAlso String.IsNullOrEmpty(branchPickerControl.Text) Then
            branchPickerControl.DataId = UserSettings.Current.LocCode
            branchPickerControl.Text = UserSettings.Current.LocCode & " - " & UserSettings.Current.LocName
        End If

        Try
            ValidateDailyActivity()
        Catch
            Return
        End Try

        BuildDailyActivity()
    End Sub

    Private Sub ValidateDailyActivity()
        If dailyActivityFromTextBox.Date = Date.MinValue Then
            Throw New ValidationException("Must specify a valid Daily Activity For date")
        End If

        If branchPickerControl.DataId Is Nothing Then
            Throw New ValidationException("Must specify a valid Branch")
        End If
    End Sub

    Protected Sub refreshButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles refreshButton.Click
        resultsPanel.Visible = False

        Try
            ValidateDailyActivity()
        Catch ex As ValidationException
            Me.AddWarningMessage(ex.Message)
            Return
        End Try

        BuildDailyActivity()

        SaveState()
    End Sub

    Protected Sub RestoreState()
        Dim state As FilterState = DirectCast(Session(mStateKey), FilterState)

        If state Is Nothing Then
            Return
        End If

        RestoreState(state)
    End Sub

    Protected Sub RestoreState(ByVal state As FilterState)

        dailyActivityFromTextBox.Date = state.FromDate
        branchPickerControl.Text = state.Branch
        includeOverdueCheckBox.Checked = state.IncludeOverdue
        sortByDropDown.SelectedValue = state.SortBy
    End Sub



    Protected Function GetCurrentFilterState() As FilterState

        Dim state As New FilterState()

        state.FromDate = dailyActivityFromTextBox.Date

        If branchPickerControl.IsValid Then
            state.Branch = IIFX(String.IsNullOrEmpty(branchPickerControl.Text), Nothing, branchPickerControl.Text)
        End If

        state.IncludeOverdue = includeOverdueCheckBox.Checked
        state.SortBy = sortByDropDown.SelectedValue

        Return state
    End Function

    Protected Sub SaveState()
        Dim state As FilterState = GetCurrentFilterState()

        Session(mStateKey) = state
    End Sub

    Public Class FilterState
        Public FromDate As String
        Public Branch As String
        Public IncludeOverdue As Boolean
        Public SortBy As String
    End Class

End Class
