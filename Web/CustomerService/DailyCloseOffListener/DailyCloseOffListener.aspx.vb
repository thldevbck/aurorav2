
Partial Class CustomerService_Js_DailyCloseOffListener
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim qs As String = Request.QueryString("Mode")
        Dim returnedvalue As String = ""
        Dim encoded As String = ""

        Dim EFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
        If (qs.Equals("PrintReceipt")) Then
            encoded = Request("PrintReceipt")
            Dim username As String = Request.QueryString("usercode")
            returnedvalue = EFTPOS.PrintIt(username, encoded, CInt(ConfigurationManager.AppSettings("ReceiptWidth")))
        End If
    End Sub
End Class
