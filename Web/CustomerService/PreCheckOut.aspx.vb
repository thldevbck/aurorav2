﻿'rev:mia Sept 20, 2012 - first release
'rev:mia Sept 21, 2012 - start adding enhancement for RA printing

Imports Aurora.CustomerService
Imports System.Data
Imports Aurora.Common

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PreCheckout)> _
Partial Class CustomerService_PreCheckOut
    Inherits AuroraPage

    Private Const IMAGEURL_DUE As String = "~/Images/time.gif"
    Private Const IMAGEURL_DUE_WITH_ERROR As String = "~/Images/exclamation.gif"

    Private Const IMAGEURL_DONE As String = "~/Images/tick_green.gif"
    Private Const IMAGEURL_DONE_WITH_ERROR As String = "~/Images/rental_error.gif"



    Private _ReturnLastRunSchedule As String


    Public ReadOnly Property ReturnLastRunSchedule As String
        Get
            If (String.IsNullOrEmpty(_ReturnLastRunSchedule)) Then
                Return "not set"
            Else
                Return Convert.ToDateTime(_ReturnLastRunSchedule).ToString("dd/MM/yy hh:mm tt")
            End If
        End Get
    End Property

    Private _ReturnNextRunSchedule As String
    Public ReadOnly Property ReturnNextRunSchedule As String
        Get
            If (String.IsNullOrEmpty(_ReturnNextRunSchedule)) Then
                Return "not set"
            Else
                Return Convert.ToDateTime(_ReturnNextRunSchedule).ToString("dd/MM/yy hh:mm tt")
            End If
        End Get
    End Property


#Region "Unused properties for images url"
    Public ReadOnly Property KKnoError As String
        Get
            Return IMAGEURL_DUE.Replace("~", "..")
        End Get
    End Property

    Public ReadOnly Property KKwithError As String
        Get
            Return IMAGEURL_DUE_WITH_ERROR.Replace("~", "..")
        End Get
    End Property

    Public ReadOnly Property CKOwithError As String
        Get
            Return IMAGEURL_DONE_WITH_ERROR.Replace("~", "..")
        End Get
    End Property

    Public ReadOnly Property CKOnoError As String
        Get
            Return IMAGEURL_DONE.Replace("~", "..")
        End Get
    End Property
#End Region

    Sub ToogleRerunButton()
        Dim blnDisabled As Boolean = PreCheckoutResetButtonActivation(branchPickerControl.DataId)
        RunResetbutton.Disabled = Not blnDisabled
    End Sub
    Function PreCheckoutResetButtonActivation(ByVal ckoBranch As String) As Boolean
        Try
            Return Convert.ToBoolean(Aurora.CustomerService.Services.PreCheckout.PreCheckoutResetButtonActivation(ckoBranch))
        Catch ex As Exception
            Return False
        End Try

    End Function

    Function InitializeGrid(ByVal ckoBranch As String) As DataSet
        Return Aurora.CustomerService.Services.PreCheckout.GetPreCheckoutData(ckoBranch)
    End Function

    Protected Sub buttonRefresh_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles buttonRefresh.ServerClick

        Dim ds As DataSet = InitializeGrid(branchPickerControl.DataId)
        PreCheckoutGridView.DataSource = ds
        PreCheckoutGridView.DataBind()
        hiddenRowCount.Value = PreCheckoutGridView.Rows.Count
        If (hiddenRowCount.Value = "0") Then
            SetWarningShortMessage("No record found!")
        End If

       ToogleRerunButton()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(UserSettings.Current.LocCode) Then
                branchPickerControl.DataId = UserSettings.Current.LocCode
                branchPickerControl.Text = UserSettings.Current.LocCode & " - " & UserSettings.Current.LocName
            End If

            PreCheckoutGridView.DataSource = InitializeGrid(branchPickerControl.DataId)
            PreCheckoutGridView.DataBind()
            hiddenRowCount.Value = PreCheckoutGridView.Rows.Count
        End If

        Dim GetLastAndNexRunDS As DataSet = Aurora.CustomerService.Services.PreCheckout.GetPreCheckoutDateScheduledRun(branchPickerControl.DataId)
        If (GetLastAndNexRunDS.Tables.Count - 1 <> -1) Then
            If (GetLastAndNexRunDS.Tables(0).Rows.Count - 1 <> -1) Then
                _ReturnLastRunSchedule = IIf(String.IsNullOrEmpty(GetLastAndNexRunDS.Tables(0).Rows(0)(0).ToString), "", GetLastAndNexRunDS.Tables(0).Rows(0)(0).ToString)
            End If
            'If (GetLastAndNexRunDS.Tables(1).Rows.Count - 1 <> -1) Then
            '    _ReturnNextRunSchedule = IIf(String.IsNullOrEmpty(GetLastAndNexRunDS.Tables(1).Rows(0)(0).ToString), "", GetLastAndNexRunDS.Tables(1).Rows(0)(0).ToString)
            'End If


        End If
        ToogleRerunButton()

    End Sub

    Protected Sub PreCheckoutGridView_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles PreCheckoutGridView.RowCreated
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            If (rowview Is Nothing) Then Exit Sub


            Dim customer As String = rowview("Name")
            Dim LabelRentalID As Label = CType(e.Row.FindControl("LabelRentalID"), Label)
            If (Not LabelRentalID Is Nothing) Then
                LabelRentalID.Text = rowview("RntId").ToString
            End If

            Dim LabelOut As Label = CType(e.Row.FindControl("LabelOut"), Label)
            If (Not LabelOut Is Nothing) Then

                If (String.IsNullOrEmpty(rowview("rntArrivalAtBranchDateTime").ToString)) Then
                    LabelOut.Text = CDate(rowview("RntCkoWhen").ToString.Trim).ToString("HH:mm")
                Else
                    LabelOut.Text = CDate(rowview("rntArrivalAtBranchDateTime").ToString.Trim).ToString("HH:mm")
                End If
            End If

            Dim hadError As String = 0
            Dim notes As String = rowview("RntId").ToString

            ''--------checkout and checkin location -----------            
            Dim LabelCkoAndCKI As Label = CType(e.Row.FindControl("LabelCkoAndCKI"), Label)
            If (Not LabelCkoAndCKI Is Nothing) Then
                LabelCkoAndCKI.Text = rowview("ckidet").ToString.Trim
            End If
            ''-------------------------------------------------


            Dim LabelBookedVeh As Label = CType(e.Row.FindControl("LabelBookedVeh"), Label)
            If (Not LabelBookedVeh Is Nothing) Then
                LabelBookedVeh.Text = rowview("BookedVehicleCode").ToString.Trim
            End If

            Dim LabelVehicle As Label = CType(e.Row.FindControl("LabelVehicle"), Label)
            If (Not LabelVehicle Is Nothing) Then
                LabelVehicle.Text = rowview("Vehicle").ToString.Trim
            End If

            ''--------rental link------------------------------            
            ''http://pap-app-002/Aurora/web/Booking/Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=B3626837&hdRentalId=B3626837-1&activeTab=7
            Dim linkcustomername As HyperLink = CType(e.Row.FindControl("linkcustomername"), HyperLink)
            If (Not linkcustomername Is Nothing) Then
                Dim rentalid As String = rowview("RntId").ToString
                linkcustomername.Text = customer
                linkcustomername.NavigateUrl = String.Concat("~/Booking/Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=", rentalid.Split("-")(0), "&hdRentalId=", rentalid, "&activeTab=7")
            End If
            ''-------------------------------------------------

            ''rev:mia Sept 21, 2012 - start adding enhancement for RA printing
            Dim chkRentId As HtmlInputCheckBox = CType(e.Row.FindControl("chkRentId"), HtmlInputCheckBox)



            Dim img As Image = CType(e.Row.FindControl("imgStatus"), Image)
            Dim rdo As HtmlInputRadioButton = CType(e.Row.FindControl("radioRentId"), HtmlInputRadioButton)

            If (Not img Is Nothing And Not rdo Is Nothing And Not chkRentId Is Nothing) Then


                If Not String.IsNullOrEmpty(rowview("PreCkoStatus").ToString) Then
                    hadError = rowview("PreCkoStatus").ToString
                End If

                If Not String.IsNullOrEmpty(rowview("RntStatus").ToString) Then

                    Dim mode As String = rowview("RntStatus").ToString.ToUpper

                    If (mode = "KK") Then
                        If hadError = "0" Then
                            img.ImageUrl = IMAGEURL_DUE
                        ElseIf hadError = "1" Then
                            img.ImageUrl = IMAGEURL_DONE_WITH_ERROR
                        ElseIf hadError = "2" Then
                            img.Visible = False
                        End If

                    ElseIf (mode = "CO") Then
                        ''REV:MIA april 18 2013 - if its CO then no point displaying icon error
                        If hadError = "0" Or hadError = "1" Then
                            img.ImageUrl = IMAGEURL_DONE
                            ''ElseIf hadError = "1" Then
                            ''    img.ImageUrl = IMAGEURL_DONE_WITH_ERROR
                        ElseIf hadError = "2" Then
                            img.Visible = False
                        End If

                        rdo.Visible = False
                    End If

                    img.Attributes.Add("style", IIf(hadError, "cursor:hand", "cursor:arrow"))
                    img.AlternateText = IIf(hadError, notes, "")

                    ''enabled the chkRentId if there are no error
                    chkRentId.Disabled = IIf(hadError <> "1", False, True)
                    chkRentId.Visible = IIf((hadError <> "1" And hadError <> "2"), True, False)

                End If

            End If

            

        End If
    End Sub

    Protected Sub RunResetbutton_ServerClick(sender As Object, e As System.EventArgs) Handles RunResetbutton.ServerClick

        If (hiddenRowCount.Value = CInt("0")) Then
            SetWarningShortMessage("No record found!")
        Else
            Me.cnfbxConfirmation.Show()
            Dim ds As DataSet = InitializeGrid(branchPickerControl.DataId)

            PreCheckoutGridView.DataSource = ds
            PreCheckoutGridView.DataBind()


        End If

    End Sub


End Class
