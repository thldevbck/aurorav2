<%@ Page Language="VB" AutoEventWireup="false" CodeFile="DailyActivity.aspx.vb" Inherits="CustomerService_DailyActivity"
    MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
        .rowOverdue
        {
            background-color: #FEE;
        }
        .rowaltOverdue
        {
            background-color: #EDD;
        }
        .rowDue
        {
            background-color: #EEF;
        }
        .rowaltDue
        {
            background-color: #DDE;
        }
        .rowDone
        {
            background-color: #EFE;
        }
        .rowaltDone
        {
            background-color: #DED;
        }
        
        .dataTableGrid img
        {
            padding: 0px;
            margin: 0 1px 0 0;
            width: 16px;
            height: 16px;
        }
        
        #fromDateCell
        {
            white-space:nowrap;
        }
        
        .dailyActivityRedIcon
        {
            padding-left:10px;
            color: Red;
        }
    </style>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <asp:Panel ID="searchPanel" runat="Server" CssClass="dailyActivity" DefaultButton="refreshButton">
        <table cellspacing="0" cellpadding="2" border="0" style="border-bottom-width: 1px;
            width: 780px">
            <tr>
                <td>
                    Date:&nbsp;
                </td>
                <td id="fromDateCell">
                    <uc1:DateControl ID="dailyActivityFromTextBox" runat="server" Nullable="False" />&nbsp;<a id="lnkIncrementDateFrom" href="click:increment">+1</a>                    
                </td>
                <td>
                    Branch:&nbsp;
                </td>
                <td>
                    <uc1:PickerControl ID="branchPickerControl" runat="server" PopupType="LOCATION" AppendDescription="true"
                        Width="150px" Nullable="false" Caption="Branch" />                    
                </td>
                <td>
                    <asp:CheckBox ID="includeOverdueCheckBox" runat="server" Text="Include Overdue?"
                        Checked="true" />&nbsp;
                </td>
                <td>
                    Sort By:&nbsp;
                </td>
                <td>
                    <asp:DropDownList ID="sortByDropDown" runat="Server" Width="100px">
                        <asp:ListItem Value="0">Customer</asp:ListItem>
                        <asp:ListItem Value="1">Date</asp:ListItem>
                        <asp:ListItem Value="2">Vehicle</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="Right">
                    <asp:Button ID="refreshButton" runat="server" CssClass="Button_Standard Button_Refresh"
                        Text="Refresh" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="resultsPanel" runat="Server" CssClass="dailyActivity">
        <table cellpadding="1" cellspacing="0" border="0">
            <tr>
                <td>
                    <b>Check Outs:</b>
                </td>
                <td align="right">
                    Total Outstanding:
                    <asp:TextBox ID="checkOutOutstandingTextBox" runat="server" Width="50px" ReadOnly="True"
                        Style="text-align: center" />
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <b>Check Ins:</b>
                </td>
                <td align="right">
                    Total Outstanding:
                    <asp:TextBox ID="checkInOutstandingTextBox" runat="server" Width="50px" ReadOnly="True"
                        Style="text-align: center" />
                </td>
            </tr>
            <tr>
                <td valign="top" colspan="2" style="width: 500px">
                    <asp:Table ID="checkOutTable" runat="server" EnableViewState="false" CssClass="dataTableColor"
                        CellPadding="2" CellSpacing="0" Visible="false" Width="500px">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell>Customer</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Booking</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Out</asp:TableHeaderCell>
                            <asp:TableHeaderCell>To</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Sch&nbsp;&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Vehicle</asp:TableHeaderCell>
                            <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </td>
                <td>
                    &nbsp;
                </td>
                <td valign="top" colspan="2" style="width: 400px">
                    <asp:Table ID="checkInTable" runat="server" EnableViewState="false" CssClass="dataTableColor"
                        CellPadding="2" CellSpacing="0" Visible="false" Width="400px">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell>Customer</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Booking</asp:TableHeaderCell>
                            <asp:TableHeaderCell>In</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Vehicle</asp:TableHeaderCell>
                            <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell>&nbsp;</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </td>
            </tr>
        </table>
        <uc1:CollapsiblePanel ID="cpnlSReportLinks" runat="server" Title="Reports" style="width: 900px;" Width="900px" TargetControlID="pnlReportLinks" />
        <asp:Panel ID="pnlReportLinks" runat="server">
            <table cellspacing="0" cellpadding="2" style="width: 900px; margin-top: 20px">
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkACTBRFCO" runat="server" Text="Activity Brief Check-Out Report"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkACTBRFCI" runat="server" Text="Activity Brief Check-In Report"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkACTDETCO" runat="server" Text="Activity Details Check-Out Report"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkACTDETCI" runat="server" Text="Activity Details Check-In Report"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkBMSClass" runat="server" Text="Branch Movement Summary by Vehicle Class"></asp:HyperLink>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:HyperLink ID="lnkBMSType" runat="server" Text="Branch Movement Summary by Vehicle Type"></asp:HyperLink>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <table cellspacing="0" cellpadding="2" style="width: 900px; margin-top: 20px" class="dataTable">
            <tr>
                <td style="width: 10%">
                    <i>Key:</i>
                </td>
                <td style="width: 15%">
                    <asp:Image ImageUrl="~/Images/vip.gif" ImageAlign="absBottom" runat="Server" />&nbsp;<b>VIP</b>
                </td>
                <td style="width: 15%" class="rowDone">
                    <asp:Image ImageUrl="~/Images/tick_green.gif" ImageAlign="absBottom" runat="Server" />&nbsp;Processed
                </td>
                <td style="width: 15%" class="rowDue">
                    <asp:Image ImageUrl="~/Images/time.gif" ImageAlign="absBottom" runat="Server" />&nbsp;Due
                </td>
                <td style="width: 15%" class="rowOverdue">
                    <asp:Image ImageUrl="~/Images/exclamation.gif" ImageAlign="absBottom" runat="Server" />&nbsp;Overdue
                </td>
                <td style="width: 15%">
                    <asp:Image ImageUrl="~/Images/rental_error.gif" ImageAlign="absBottom" runat="Server" />&nbsp;Rental
                    Error
                </td>
                <td style="width: 15%">
                    <asp:Image ImageUrl="~/Images/bell.gif" ImageAlign="absBottom" runat="Server" />&nbsp;Service Overdue
                </td>
            </tr>
        </table>
    </asp:Panel>

    <script type="text/javascript">
        (function (submitButtonId) {
            var oLink = document.getElementById('lnkIncrementDateFrom'),
            oCell = document.getElementById('fromDateCell'),
            oDateInput = oCell.getElementsByTagName('input')[0],
            oButton = document.getElementById(submitButtonId),
            formatDatePart = function (value) {
                value += '';
                if (value.length == 1)
                    return '0' + value;

                return value;
            };

            oLink.onclick = function () {
                var aDateParts = oDateInput.value.split('/'),
                oDate;

                if (aDateParts.length < 3)
                    return false;

                oDate = new Date(aDateParts[2], aDateParts[1]-1, aDateParts[0]);

                oDate.setDate(oDate.getDate() + 1);
                oDateInput.value = formatDatePart(oDate.getDate()) + '/' + formatDatePart(oDate.getMonth()+1) + '/' + oDate.getFullYear();

                oButton.click();

                return false;
            }
        })('<%=refreshButton.ClientID %>');
    </script>
</asp:Content>
