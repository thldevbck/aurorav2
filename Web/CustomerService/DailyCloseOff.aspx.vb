'' Change Log!
'' 8.7.8 / Shoel / Added Wizard Control
'' 8.7.8 / Shoel / fixed conf message after close off cancellation
'' 9.7.8 / Shoel / some more changes as per UI guidelines
'' 14.7.8 / Shoel / Now the .htm is saved with branchname and type . eg. "AKL_28-06-2008_BYTYPE.htm" and "AKL_28-06-2008_BYMETHOD.htm". 
'' 14.7.8 / Shoel / report htms are autosaved on reaching the third div
'' 7.11.8 / Shoel / Fixed bugs in tab order and saving chq number on last screen...

''added eftpos inquiry and settlement

Imports System.Data
Imports System.Xml
Imports System.Xml.Xsl
Imports Aurora.Booking.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DailyCloseOffFloatCount)> _
Partial Class CustomerService_DailyCloseOff
    Inherits AuroraPage

    Const VIEWSTATE_TOTALS As String = "Viewstate_DCO_Totals"
    Const VIEWSTATE_ROOTDATA As String = "Viewstate_DCO_Root"
    Const VIEWSTATE_LOCATIONID As String = "Viewstate_DCO_LocationID"
    Const VIEWSTATE_FLOATDATA As String = "Viewstate_DCO_FloatData"
    Const VIEWSTATE_ARDATA As String = "Viewstate_DCO_ARData"
    Const VIEWSTATE_PR_INTEGRITYNO As String = "Viewstate_DCO_PR_IntegrityNo"
    Const VIEWSTATE_PR_DIFF As String = "Viewstate_DCO_PR_Difference"
    Const VIEWSTATE_PR_CONFIRMATIONSTATUS As String = "Viewstate_DCO_PR_ConfirmationStatus"
    Const VIEWSTATE_DCO_WHEREAMI As String = "ViewState_DCO_WhereAmI"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            LoadDCOData()
            btnDailyCloseOff.Attributes.Add("OnClick", "showConfirmationBoxViaClient('" & cnfbxConfirmation.ClientID & "Behaviour');return false;")

            txtFloatTransactionsExpense.Attributes.Add("OnChange", "return RecalculateFloatTransactions('" & txtFloatTransactionsRevenue.ClientID & "','" & txtFloatTransactionsExpense.ClientID & "','" & txtFloatTransactionsTotal.ClientID & "');")
            txtFloatTransactionsRevenue.Attributes.Add("OnChange", "return RecalculateFloatTransactions('" & txtFloatTransactionsRevenue.ClientID & "','" & txtFloatTransactionsExpense.ClientID & "','" & txtFloatTransactionsTotal.ClientID & "');")
        End If

        Dim eft As New BookingPaymentEFTPOSMaintenance
        HiddenPrinterName.Value = Server.HtmlEncode(eft.GetPrinterNameAndAssignToJS(Me.UserCode))

        If (EFTButtonForCSR() = True) Then
            HiddenMemberOfCSREF.Value = "true"
        Else
            HiddenMemberOfCSREF.Value = "false"
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        'display part

        If divSecond.Style.Item("display") = "block" Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID & "1", "ChangeTitle('Daily Close Off - Float Count','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)
        ElseIf divThird.Style.Item("display") = "block" Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID & "1", "ChangeTitle('Daily Close Off - Account Reconciliation','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)
        ElseIf divFourth.Style.Item("display") = "block" Then
            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID & "1", "ChangeTitle('Daily Close Off - Payment Reconciliation','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)
        Else
            ' default - div 1
            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID & "1", "ChangeTitle('Daily Close Off','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)
        End If
    End Sub

#Region "Div 1 stuff"

    Sub LoadDCOData()
        Dim xmlDCOData As XmlDocument

        xmlDCOData = Aurora.CustomerService.Services.DailyCloseOff.GetDailyCloseOffFloat(UserCode)
        SplitInitialDCOData(xmlDCOData) ', nStep)
    End Sub

    Protected Sub cnfbxConfirmation_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cnfbxConfirmation.PostBack
        If leftButton Then
            'said yes, so continue!
            If ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = "Reconcile" Then
                ' this is from the reconcile part!
                ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = Nothing
                ReconcilePRData()
                Exit Sub
            ElseIf ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = "Cancel" Then
                ' this is the cancel part
                CancelDailyCloseOff()
                ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = Nothing
                Exit Sub
            Else
                'is the initial part!
                Dim xmlReturn As XmlDocument
                xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.StartDailyCloseOff(UserCode, cmbCloseOffDate.SelectedValue)
                If xmlReturn.DocumentElement.InnerText.Contains("GEN122") Then
                    LoadDCOData()
                    SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
                    'Server.Transfer("DailyCloseOffFloatCount.aspx?Step=2")
                Else
                    SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
                End If
            End If

        End If
    End Sub

#End Region

#Region "Div2 stuff"

    Sub SplitInitialDCOData(ByVal DCOData As XmlDocument) ', ByVal StepNumber As Integer)
        lblLocation.Text = DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/LocDesc").InnerText
        lblMessage.Text = DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/message").InnerText

        'for div3
        ViewState(VIEWSTATE_LOCATIONID) = DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/LcoId").InnerText

        'not to show till we save and move ahead!
        divThird.Style.Item("display") = "none"
        divFourth.Style.Item("display") = "none"


        'loading of combo moved here!

        Dim xmlDatesData As XmlElement
        xmlDatesData = DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/DateList")

        LoadDatesCombo(xmlDatesData, DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/LocCloseOffStatus").InnerText, DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/LocCloseOffDate").InnerText)

        If DCOData.DocumentElement.SelectSingleNode("Root/LocDetails/LocCloseOffStatus").InnerText = "In Process" Then
            'show table
            divInitial.Style.Item("display") = "block"
            divSecond.Style.Item("display") = "block"
            divThird.Style.Item("display") = "none"
            divFourth.Style.Item("display") = "none"
            btnDailyCloseOff.Visible = False

            ViewState(VIEWSTATE_TOTALS) = DCOData.DocumentElement.SelectSingleNode("Root/Totals").OuterXml
            ViewState(VIEWSTATE_ROOTDATA) = DCOData.DocumentElement.SelectSingleNode("Root").OuterXml
            LoadFloatRepeater(DCOData.DocumentElement.SelectNodes("Root/Locationfloat"))
            ' part2 active
            wizWizard.SelectedIndex = 1
            ViewState(VIEWSTATE_DCO_WHEREAMI) = "STEP2"
            ' part2 active
        Else
            'hide table
            divInitial.Style.Item("display") = "block"
            divSecond.Style.Item("display") = "none"
            btnDailyCloseOff.Visible = True
            ' part 1 active
            wizWizard.SelectedIndex = 0
            ViewState(VIEWSTATE_DCO_WHEREAMI) = "STEP1"
            ' part 1 active
        End If


    End Sub

    Sub LoadDatesCombo(ByVal DatesData As XmlElement, ByVal Status As String, ByVal SelectedDate As String)
        For Each oNode As XmlNode In DatesData
            cmbCloseOffDate.Items.Add(oNode.InnerText)
        Next

        cmbCloseOffDate.SelectedValue = SelectedDate

        If Status = "In Process" Then
            cmbCloseOffDate.Enabled = False
        End If
    End Sub

    Sub LoadFloatRepeater(ByVal FloatRepeaterData As XmlNodeList)

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml("<Root/>")


        For Each oNode As XmlNode In FloatRepeaterData
            xmlTemp.DocumentElement.InnerXml = xmlTemp.DocumentElement.InnerXml & oNode.OuterXml
        Next


        Dim dstFloatData As New DataSet
        Dim dtFloatData As New DataTable
        dstFloatData.ReadXml(New XmlNodeReader(xmlTemp))

        If dstFloatData.Tables.Count > 0 Then
            dtFloatData = dstFloatData.Tables(0)
        End If

        rptFloatCount.DataSource = dtFloatData
        rptFloatCount.DataBind()

        'For i As Integer = 0 To rptFloatCount.Items.Count - 1
        '    If rptFloatCount.Items(i).ItemType = ListItemType.Footer Then
        '        Console.Write("here")
        '    End If
        'Next

    End Sub

    Protected Sub rptFloatCount_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptFloatCount.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then


            If CType(e.Item.FindControl("lblCurrencyDenominations"), Label).Text.Trim().Equals(String.Empty) Then
                CType(e.Item.FindControl("lblCurrencyDenominations"), Label).Text = "0.00"
            End If
            CType(e.Item.FindControl("lblCurrencyDenominations"), Label).Text = String.Format("{0:f}", CDbl(CType(e.Item.FindControl("lblCurrencyDenominations"), Label).Text))

            If CType(e.Item.FindControl("txtCashCount"), TextBox).Text.Trim().Equals(String.Empty) Then
                CType(e.Item.FindControl("txtCashCount"), TextBox).Text = "0"
            End If
            CType(e.Item.FindControl("txtCashCount"), TextBox).Text = String.Format("{0:0}", CDbl(CType(e.Item.FindControl("txtCashCount"), TextBox).Text))


            If CType(e.Item.FindControl("txtFloatCount"), TextBox).Text.Trim().Equals(String.Empty) Then
                CType(e.Item.FindControl("txtFloatCount"), TextBox).Text = "0"
            End If
            CType(e.Item.FindControl("txtFloatCount"), TextBox).Text = String.Format("{0:0}", CDbl(CType(e.Item.FindControl("txtFloatCount"), TextBox).Text))


            If CType(e.Item.FindControl("txtCashTotal"), TextBox).Text.Trim().Equals(String.Empty) Then
                CType(e.Item.FindControl("txtCashTotal"), TextBox).Text = "0.00"
            End If
            CType(e.Item.FindControl("txtCashTotal"), TextBox).Text = String.Format("{0:f}", CDbl(CType(e.Item.FindControl("txtCashTotal"), TextBox).Text))

            If CType(e.Item.FindControl("txtFloatTotal"), TextBox).Text.Trim().Equals(String.Empty) Then
                CType(e.Item.FindControl("txtFloatTotal"), TextBox).Text = "0.00"
            End If
            CType(e.Item.FindControl("txtFloatTotal"), TextBox).Text = String.Format("{0:f}", CDbl(CType(e.Item.FindControl("txtFloatTotal"), TextBox).Text))

            If CType(e.Item.FindControl("txtMoneyTotal"), TextBox).Text.Trim().Equals(String.Empty) Then
                CType(e.Item.FindControl("txtMoneyTotal"), TextBox).Text = "0.00"
            End If
            CType(e.Item.FindControl("txtMoneyTotal"), TextBox).Text = String.Format("{0:f}", CDbl(CType(e.Item.FindControl("txtMoneyTotal"), TextBox).Text))

        ElseIf e.Item.ItemType = ListItemType.Footer Then

            Dim dblCDTotal, dblFloatTotal, dblDenom, dblFinalCDTotal, dblFinalFloatTotal As Double
            Dim intCDCount, intFloatCount As Integer

            dblCDTotal = 0.0
            dblFloatTotal = 0.0

            dblFinalCDTotal = 0.0
            dblFinalFloatTotal = 0.0

            intCDCount = 0
            intFloatCount = 0

            For Each oItem As RepeaterItem In rptFloatCount.Items

                dblDenom = CDbl(CType(oItem.FindControl("lblCurrencyDenominations"), Label).Text)

                intCDCount = CInt(CType(oItem.FindControl("txtCashCount"), TextBox).Text)
                'dblCDTotal = CDbl(CType(Item.FindControl("txtCashTotal"), TextBox).Text)

                intFloatCount = CInt(CType(oItem.FindControl("txtFloatCount"), TextBox).Text)
                '  dblFloatTotal = CDbl(CType(oItem.FindControl("txtFloatTotal"), TextBox).Text)

                dblCDTotal = dblDenom * intCDCount
                dblFloatTotal = dblDenom * intFloatCount

                dblFinalCDTotal = dblFinalCDTotal + dblCDTotal
                dblFinalFloatTotal = dblFinalFloatTotal + dblFloatTotal

                CType(oItem.FindControl("txtCashTotal"), TextBox).Text = String.Format("{0:f}", dblCDTotal)
                CType(oItem.FindControl("txtFloatTotal"), TextBox).Text = String.Format("{0:f}", dblFloatTotal)

            Next

            CType(e.Item.FindControl("txtFooterCDTotal"), TextBox).Text = String.Format("{0:f}", dblFinalCDTotal)
            CType(e.Item.FindControl("txtFooterFloatTotal"), TextBox).Text = String.Format("{0:f}", dblFinalFloatTotal)

        End If
    End Sub

    Protected Sub btnSave1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click
        SaveFloat()
    End Sub

    Sub SaveFloat()
        Dim xmlReturn, xmlTemp As XmlDocument
        Dim sXMLData As String = ""
        sXMLData = CreateXMLForFloatSave()
        xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.SaveDailyCloseOffFloat(sXMLData)

        If Not xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' error
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        Else
            If Not xmlReturn.DocumentElement.SelectSingleNode("Message") Is Nothing Then
                SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
                LoadDCOData()
                sXMLData = CreateXMLForFloatSave()
                xmlTemp = New XmlDocument
                xmlTemp.LoadXml(sXMLData)
                LoadFloatRepeater(xmlTemp.DocumentElement.SelectNodes("Locationfloat"))

            End If
        End If
    End Sub

    Function CreateXMLForFloatSave() As String
        Dim sbXMLString As New StringBuilder
        Dim sbXMLTotalsString As New StringBuilder
        Dim xmlViewStateRoot As New XmlDocument
        xmlViewStateRoot.LoadXml(CStr(ViewState(VIEWSTATE_ROOTDATA)))


        sbXMLString.Append("<Root>")
        sbXMLString.Append(xmlViewStateRoot.DocumentElement.SelectSingleNode("LocDetails").OuterXml)

        '<Locationfloat>
        '	<lfid>A6C945A1-6848-4AD3-9BDE-5A4057F70BBA</lfid>
        '	<LocCode>AKL</LocCode>
        '	<denomination>100</denomination>
        '	<cashcount>1</cashcount>
        '	<floatcount>2</floatcount>
        '	<cashtotal>100.00</cashtotal>
        '	<floattotal>200.00</floattotal>
        '	<total>300.00</total>
        '</Locationfloat>
        Dim dblFooterCashTotal As Double = 0.0
        Dim dblFooterFloatTotal As Double = 0.0

        For i As Integer = 0 To rptFloatCount.Items.Count - 1

            Dim intCashCount As Integer = 0
            Dim intFloatCount As Integer = 0
            Dim dblDenomination As Double = 0.0
            Dim dblRowTotal As Double = 0.0
            Dim dblCashTotal As Double = 0.0
            Dim dblFloatTotal As Double = 0.0

            dblDenomination = CDbl(CType(rptFloatCount.Items(i).FindControl("lblCurrencyDenominations"), Label).Text)
            intCashCount = CInt(CType(rptFloatCount.Items(i).FindControl("txtCashCount"), TextBox).Text)
            intFloatCount = CInt(CType(rptFloatCount.Items(i).FindControl("txtFloatCount"), TextBox).Text)
            dblCashTotal = dblDenomination * intCashCount
            dblFloatTotal = dblDenomination * intFloatCount
            dblRowTotal = dblCashTotal + dblFloatTotal

            dblFooterCashTotal = dblFooterCashTotal + dblCashTotal
            dblFooterFloatTotal = dblFooterFloatTotal + dblFloatTotal

            sbXMLString.Append("<Locationfloat>")
            sbXMLString.Append("<lfid>" & CType(rptFloatCount.Items(i).FindControl("lblID"), Label).Text & "</lfid>")
            sbXMLString.Append("<LocCode>" & CType(rptFloatCount.Items(i).FindControl("lblLocation"), Label).Text & "</LocCode>")
            If dblDenomination < 1 Then
                sbXMLString.Append("<denomination>" & String.Format("{0:f}", dblDenomination) & "</denomination>")
            Else
                sbXMLString.Append("<denomination>" & String.Format("{0:0}", dblDenomination) & "</denomination>")
            End If
            sbXMLString.Append("<cashcount>" & intCashCount & "</cashcount>")
            sbXMLString.Append("<floatcount>" & intFloatCount & "</floatcount>")
            sbXMLString.Append("<cashtotal>" & String.Format("{0:f}", dblCashTotal) & "</cashtotal>")
            sbXMLString.Append("<floattotal>" & String.Format("{0:f}", dblFloatTotal) & "</floattotal>")
            sbXMLString.Append("<total>" & String.Format("{0:f}", dblRowTotal) & "</total>")
            sbXMLString.Append("</Locationfloat>")
        Next

        'for reloading grid
        sbXMLTotalsString.Append("<Totals>")
        sbXMLTotalsString.Append("<ttlclosingfloat>" & String.Format("{0:f}", CDbl(dblFooterCashTotal + dblFooterFloatTotal)) & "</ttlclosingfloat>")
        sbXMLTotalsString.Append("<ttlcash>" & String.Format("{0:f}", dblFooterCashTotal) & "</ttlcash>")
        sbXMLTotalsString.Append("<ttlfloat>" & String.Format("{0:f}", dblFooterFloatTotal) & "</ttlfloat>")
        sbXMLTotalsString.Append("</Totals>")

        ViewState(VIEWSTATE_TOTALS) = sbXMLTotalsString.ToString()



        sbXMLString.Append(sbXMLTotalsString.ToString())

        ' Fix : 12.12.8 : Floats not saving
        sbXMLString.Append(xmlViewStateRoot.DocumentElement.SelectSingleNode("isCloseOffLoc").OuterXml)

        sbXMLString.Append("</Root>")

        Return sbXMLString.ToString()
    End Function

    Protected Sub btnSaveNext1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext1.Click
        SaveFloat()
        divSecond.Style.Item("display") = "none"
        divThird.Style.Item("display") = "block"
        PrepareAccountReconcilliationScreen()
        'ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "alert(document.getElementById('" & Me.Page.Master.FindControl("titlelabel").ClientID & "').value='123');alert(document.getElementById('" & Me.Page.Master.FindControl("titlelabel").ClientID & "').value);ChangeTitle('Daily Close Off - Account Reconciliation','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)
        'ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "alert('part2');ChangeTitle('Daily Close Off - Account Reconciliation','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)

        ' third part active
        wizWizard.SelectedIndex = 2
        ViewState(VIEWSTATE_DCO_WHEREAMI) = "STEP3"
        ' third part active

        ' save reports
        SaveReports("BYMETHOD")
        SaveReports("BYTYPE")
    End Sub

    Sub PrepareAccountReconcilliationScreen()
        'GetDailyCloseOffAccountReconciliation
        'get data
        Dim sLocationId As String
        sLocationId = ViewState(VIEWSTATE_LOCATIONID)
        Dim xmlARData As XmlDocument
        xmlARData = Aurora.CustomerService.Services.DailyCloseOff.GetDailyCloseOffAccountReconciliation(sLocationId)

        ' Load Credit Cards
        LoadCreditCardRepeater(xmlARData.DocumentElement.SelectSingleNode("Root/CrCards").ChildNodes)
        ' Load Credit Card Imprints
        LoadCreditCardImprintRepeater(xmlARData.DocumentElement.SelectSingleNode("Root/CrCardsimp").ChildNodes)
        ' Load LCY 
        LoadLocalCurrencyRepeater(xmlARData.DocumentElement.SelectSingleNode("Root/LocalTxns").ChildNodes)
        ' Load FCY
        LoadForeignCurrencyRepeater(xmlARData.DocumentElement.SelectSingleNode("Root/ForeignTxns").ChildNodes)
        ' now this here xml has plenty of parts and needs splitting 

        ViewState(VIEWSTATE_FLOATDATA) = xmlARData.DocumentElement.SelectSingleNode("Root/LocDetails").OuterXml

        RecalculatePageContents() '(Nothing, Nothing)

        ViewState(VIEWSTATE_ARDATA) = xmlARData.DocumentElement.InnerXml
    End Sub

#End Region

#Region "Div 3 stuff"

#Region "CREDIT CARD REPEATER ON DIV 3"

    Sub LoadCreditCardRepeater(ByVal CreditCardRepeaterData As XmlNodeList)

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml("<Root/>")


        For Each oNode As XmlNode In CreditCardRepeaterData
            xmlTemp.DocumentElement.InnerXml = xmlTemp.DocumentElement.InnerXml & oNode.OuterXml
        Next


        Dim dstCCData As New DataSet
        Dim dtCCData As New DataTable
        dstCCData.ReadXml(New XmlNodeReader(xmlTemp))

        If dstCCData.Tables.Count > 0 Then
            dtCCData = dstCCData.Tables(0)
        End If

        rptReconCreditCards.DataSource = dtCCData
        rptReconCreditCards.DataBind()
    End Sub

    Protected Sub rptReconCreditCards_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptReconCreditCards.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dblBrachCCTotal, dblAuroraCCTotal, dblDifference As Double

            ' CType(e.Item.FindControl("txtCCBranchAmount"), TextBox).Attributes.Add("OnBlur", "this.value=trimDecimal(this.value,2);")

            dblBrachCCTotal = CDbl(CType(e.Item.FindControl("txtCCBranchAmount"), TextBox).Text)
            dblAuroraCCTotal = CDbl(CType(e.Item.FindControl("txtCCAuroraAmount"), TextBox).Text)

            dblDifference = dblBrachCCTotal - dblAuroraCCTotal

            CType(e.Item.FindControl("txtCCBranchAmount"), TextBox).Text = String.Format("{0:f}", dblBrachCCTotal)
            CType(e.Item.FindControl("txtCCAuroraAmount"), TextBox).Text = String.Format("{0:f}", dblAuroraCCTotal)
            CType(e.Item.FindControl("lblCCDiff"), Label).Text = String.Format("{0:f}", dblDifference)

            CType(e.Item.FindControl("txtCCBranchAmount"), TextBox).Attributes.Add("OnChange", "return UpdateRowTotal('" & txtBranchCCTotal.ClientID & "','" & txtBranchCCTotal.ClientID & "','" & txtBranchLCYTotalAmount.ClientID & "','" & txtBranchFCYTotalAmount.ClientID & "','" & txtBranchFinalTotal.ClientID & "');")
            'OnChange="return UpdateCreditCardRowTotal();"

        End If
    End Sub

#End Region

#Region "CREDIT CARD IMPRINTS REPEATER ON DIV 3"

    Sub LoadCreditCardImprintRepeater(ByVal CreditCardImprintRepeaterData As XmlNodeList)

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml("<Root/>")


        For Each oNode As XmlNode In CreditCardImprintRepeaterData
            xmlTemp.DocumentElement.InnerXml = xmlTemp.DocumentElement.InnerXml & oNode.OuterXml
        Next


        Dim dstCCIData As New DataSet
        Dim dtCCIData As New DataTable
        dstCCIData.ReadXml(New XmlNodeReader(xmlTemp))

        If dstCCIData.Tables.Count > 0 Then
            dtCCIData = dstCCIData.Tables(0)
        End If

        rptCreditCardImprints.DataSource = dtCCIData
        rptCreditCardImprints.DataBind()
        'rptCreditCardImprints
    End Sub

    Protected Sub rptCreditCardImprints_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCreditCardImprints.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dblTotal As Double = 0.0

            If Trim(CType(e.Item.FindControl("txtCCITotalAmount"), TextBox).Text) <> "" Then
                Try
                    dblTotal = CDbl(CType(e.Item.FindControl("txtCCITotalAmount"), TextBox).Text)
                Catch
                End Try

            End If

            CType(e.Item.FindControl("txtCCITotalAmount"), TextBox).Text = String.Format("{0:f}", dblTotal)
        End If
    End Sub

#End Region

#Region "LCY REPEATER ON DIV 3"

    Sub LoadLocalCurrencyRepeater(ByVal LocalCurrencyRepeaterData As XmlNodeList)

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml("<Root/>")


        For Each oNode As XmlNode In LocalCurrencyRepeaterData
            xmlTemp.DocumentElement.InnerXml = xmlTemp.DocumentElement.InnerXml & oNode.OuterXml
        Next


        Dim dstLCYData As New DataSet
        Dim dtLCYData As New DataTable
        dstLCYData.ReadXml(New XmlNodeReader(xmlTemp))

        If dstLCYData.Tables.Count > 0 Then
            dtLCYData = dstLCYData.Tables(0)
        End If

        rptLocalCurrency.DataSource = dtLCYData
        rptLocalCurrency.DataBind()
        'rptCreditCardImprints
    End Sub

    Protected Sub rptLocalCurrency_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptLocalCurrency.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dblBranchTotal As Double = 0.0
            Dim dblAuroraTotal As Double = 0.0
            Dim dblTotal As Double = 0.0
            dblBranchTotal = CDbl(CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).Text)
            dblAuroraTotal = CDbl(CType(e.Item.FindControl("txtAuroraLCYTotal"), TextBox).Text)

            dblTotal = dblBranchTotal - dblAuroraTotal

            CType(e.Item.FindControl("lblLCYDifference"), Label).Text = String.Format("{0:f}", dblTotal)

            'CssClass="Textbox_Readonly_Small_RightAligned"  
            If CType(e.Item.FindControl("txtLCYReadOnly"), TextBox).Text = "True" Then
                'is readonly row
                CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).ReadOnly = True
                CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).CssClass = "Textbox_Readonly_Small_RightAligned"
            Else
                CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).ReadOnly = False
                CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).CssClass = "Textbox_Small_Right_Aligned"
                CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).Attributes.Add("OnChange", "return UpdateRowTotal('" & txtBranchLCYTotalAmount.ClientID & "','" & txtBranchCCTotal.ClientID & "','" & txtBranchLCYTotalAmount.ClientID & "','" & txtBranchFCYTotalAmount.ClientID & "','" & txtBranchFinalTotal.ClientID & "');")
            End If


            CType(e.Item.FindControl("txtBranchLCYTotal"), TextBox).Text = String.Format("{0:f}", dblBranchTotal)
            CType(e.Item.FindControl("txtAuroraLCYTotal"), TextBox).Text = String.Format("{0:f}", dblAuroraTotal)


        End If
    End Sub

#End Region

#Region "FCY REPEATER ON DIV 3"

    Sub LoadForeignCurrencyRepeater(ByVal ForeignCurrencyRepeaterData As XmlNodeList)

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml("<Root/>")


        For Each oNode As XmlNode In ForeignCurrencyRepeaterData
            xmlTemp.DocumentElement.InnerXml = xmlTemp.DocumentElement.InnerXml & oNode.OuterXml
        Next


        Dim dstFCYData As New DataSet
        Dim dtFCYData As New DataTable
        dstFCYData.ReadXml(New XmlNodeReader(xmlTemp))

        If dstFCYData.Tables.Count > 0 Then
            dtFCYData = dstFCYData.Tables(0)
        End If

        rptForeignCurrency.DataSource = dtFCYData
        rptForeignCurrency.DataBind()
        'rptCreditCardImprints
    End Sub

    Protected Sub rptForeignCurrency_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptForeignCurrency.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim dblBranchTotal As Double = 0.0
            Dim dblAuroraTotal As Double = 0.0
            Dim dblTotal As Double = 0.0
            dblBranchTotal = CDbl(CType(e.Item.FindControl("txtBranchFCYTotal"), TextBox).Text)
            dblAuroraTotal = CDbl(CType(e.Item.FindControl("txtAuroraFCYTotal"), TextBox).Text)

            dblTotal = dblBranchTotal - dblAuroraTotal

            CType(e.Item.FindControl("lblFCYDifference"), Label).Text = String.Format("{0:f}", dblTotal)

            CType(e.Item.FindControl("txtBranchFCYTotal"), TextBox).Text = String.Format("{0:f}", dblBranchTotal)
            CType(e.Item.FindControl("txtAuroraFCYTotal"), TextBox).Text = String.Format("{0:f}", dblAuroraTotal)

            CType(e.Item.FindControl("txtBranchFCYTotal"), TextBox).Attributes.Add("OnChange", "return UpdateRowTotal('" & txtBranchFCYTotalAmount.ClientID & "','" & txtBranchCCTotal.ClientID & "','" & txtBranchLCYTotalAmount.ClientID & "','" & txtBranchFCYTotalAmount.ClientID & "','" & txtBranchFinalTotal.ClientID & "');")

        End If
    End Sub

#End Region

#Region "RECALC PART"
    Protected Sub RecalculatePageContents() '(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles btnHiddenRecalculate.Click
        Dim dblBranchCCTotal, dblBranchLCYTotal, dblBranchFCYTotal, dblBranchFinalTotal As New Double
        Dim dblAuroraCCTotal, dblAuroraLCYTotal, dblAuroraFCYTotal, dblAuroraFinalTotal As New Double

        ' part 1 - CC
        For Each oRepeaterItem As RepeaterItem In rptReconCreditCards.Items
            dblBranchCCTotal = dblBranchCCTotal + CDbl(CType(oRepeaterItem.FindControl("txtCCBranchAmount"), TextBox).Text)
            dblAuroraCCTotal = dblAuroraCCTotal + CDbl(CType(oRepeaterItem.FindControl("txtCCAuroraAmount"), TextBox).Text)
            CType(oRepeaterItem.FindControl("lblCCDiff"), Label).Text = String.Format("{0:f}", CDbl(CType(oRepeaterItem.FindControl("txtCCBranchAmount"), TextBox).Text) - CDbl(CType(oRepeaterItem.FindControl("txtCCAuroraAmount"), TextBox).Text))
        Next

        ' part 2 - LCY
        For Each oRepeaterItem As RepeaterItem In rptLocalCurrency.Items
            dblBranchLCYTotal = dblBranchLCYTotal + CDbl(CType(oRepeaterItem.FindControl("txtBranchLCYTotal"), TextBox).Text)
            dblAuroraLCYTotal = dblAuroraLCYTotal + CDbl(CType(oRepeaterItem.FindControl("txtAuroraLCYTotal"), TextBox).Text)
            CType(oRepeaterItem.FindControl("lblLCYDifference"), Label).Text = String.Format("{0:f}", CDbl(CType(oRepeaterItem.FindControl("txtBranchLCYTotal"), TextBox).Text) - CDbl(CType(oRepeaterItem.FindControl("txtAuroraLCYTotal"), TextBox).Text))
        Next
        ' part 3 - FCY
        For Each oRepeaterItem As RepeaterItem In rptForeignCurrency.Items
            dblBranchFCYTotal = dblBranchFCYTotal + CDbl(CType(oRepeaterItem.FindControl("txtBranchFCYTotal"), TextBox).Text)
            dblAuroraFCYTotal = dblAuroraFCYTotal + CDbl(CType(oRepeaterItem.FindControl("txtAuroraFCYTotal"), TextBox).Text)
            CType(oRepeaterItem.FindControl("lblFCYDifference"), Label).Text = String.Format("{0:f}", CDbl(CType(oRepeaterItem.FindControl("txtBranchFCYTotal"), TextBox).Text) - CDbl(CType(oRepeaterItem.FindControl("txtAuroraFCYTotal"), TextBox).Text))
        Next
        ' part 4 - Final Totals
        txtBranchCCTotal.Text = String.Format("{0:f}", dblBranchCCTotal)
        txtAuroraCCTotal.Text = String.Format("{0:f}", dblAuroraCCTotal)

        txtBranchFCYTotalAmount.Text = String.Format("{0:f}", dblBranchFCYTotal)
        txtAuroraFCYTotalAmount.Text = String.Format("{0:f}", dblAuroraFCYTotal)

        txtBranchLCYTotalAmount.Text = String.Format("{0:f}", dblBranchLCYTotal)
        txtAuroraLCYTotalAmount.Text = String.Format("{0:f}", dblAuroraLCYTotal)

        dblBranchFinalTotal = dblBranchCCTotal + dblBranchFCYTotal + dblBranchLCYTotal
        dblAuroraFinalTotal = dblAuroraCCTotal + dblAuroraFCYTotal + dblAuroraLCYTotal

        txtBranchFinalTotal.Text = String.Format("{0:f}", dblBranchFinalTotal)
        txtAuroraFinalTotal.Text = String.Format("{0:f}", dblAuroraFinalTotal)

        ' part 5 - Float
        Dim xmlFloatData As New XmlDocument
        xmlFloatData.LoadXml(ViewState(VIEWSTATE_FLOATDATA))


        'txtOpeningFloat.Text = String.Format("{0:f}", CDbl(xmlFloatData.DocumentElement.SelectSingleNode("OpeningFloat").InnerText))
        'txtClosingFloat.Text = String.Format("{0:f}", CDbl(xmlFloatData.DocumentElement.SelectSingleNode("LocFloat").InnerText))
        'txtNetChangeInFloat.Text = String.Format("{0:f}", CDbl(xmlFloatData.DocumentElement.SelectSingleNode("LocFloat").InnerText) - CDbl(xmlFloatData.DocumentElement.SelectSingleNode("OpeningFloat").InnerText))

        txtOpeningFloat.Text = String.Format("{0:f}", CDbl(xmlFloatData.DocumentElement.SelectSingleNode("OpeningFloat").InnerText))
        txtClosingFloat.Text = String.Format("{0:f}", CDbl(xmlFloatData.DocumentElement.SelectSingleNode("LocFloat").InnerText))
        txtNetChangeInFloat.Text = String.Format("{0:f}", CDbl(xmlFloatData.DocumentElement.SelectSingleNode("LocFloat").InnerText) - CDbl(xmlFloatData.DocumentElement.SelectSingleNode("OpeningFloat").InnerText))

        If txtFloatTransactionsRevenue.Text.Trim().Equals(String.Empty) Then
            txtFloatTransactionsRevenue.Text = "0.00"
        End If

        If txtFloatTransactionsExpense.Text.Trim().Equals(String.Empty) Then
            txtFloatTransactionsExpense.Text = "0.00"
        End If

        txtFloatTransactionsRevenue.Text = String.Format("{0:f}", CDbl(txtFloatTransactionsRevenue.Text))
        txtFloatTransactionsExpense.Text = String.Format("{0:f}", CDbl(txtFloatTransactionsExpense.Text))
        txtFloatTransactionsTotal.Text = String.Format("{0:f}", CDbl(txtFloatTransactionsRevenue.Text) - CDbl(txtFloatTransactionsExpense.Text))
    End Sub
#End Region

#Region "SAVE DIV3"

    Function GenerateXMLForAccountReconciliation() As String
        Dim xmlARData As New XmlDocument

        Dim sARData As String = ""
        sARData = CStr(ViewState(VIEWSTATE_ARDATA))

        xmlARData.LoadXml(sARData)

        ' part 1 - LocDetails
        xmlARData.DocumentElement.SelectSingleNode("LocDetails/UserName").InnerText = UserCode
        xmlARData.DocumentElement.SelectSingleNode("LocDetails/LocFloat").InnerText = txtClosingFloat.Text
        xmlARData.DocumentElement.SelectSingleNode("LocDetails/OpeningFloat").InnerText = txtOpeningFloat.Text

        ' part 2 - CrCards

        Dim sbCreditCards As New StringBuilder

        sbCreditCards.Append("<CrCards>")

        For Each oItem As RepeaterItem In rptReconCreditCards.Items
            sbCreditCards.Append("<CrCard>")

            sbCreditCards.Append("<pid>")
            sbCreditCards.Append(CType(oItem.FindControl("txtCCPID"), TextBox).Text.Trim())
            sbCreditCards.Append("</pid>")

            sbCreditCards.Append("<Label>")
            sbCreditCards.Append(CType(oItem.FindControl("lblCCName"), Label).Text.Trim())
            sbCreditCards.Append("</Label>")

            sbCreditCards.Append("<BTAmt>")
            sbCreditCards.Append(CType(oItem.FindControl("txtCCBranchAmount"), TextBox).Text.Trim())
            sbCreditCards.Append("</BTAmt>")

            sbCreditCards.Append("<CodCode>")
            sbCreditCards.Append(CType(oItem.FindControl("txtCCCodCode"), TextBox).Text.Trim())
            sbCreditCards.Append("</CodCode>")

            sbCreditCards.Append("<TtlAmt>")
            sbCreditCards.Append(CType(oItem.FindControl("txtCCAuroraAmount"), TextBox).Text.Trim())
            sbCreditCards.Append("</TtlAmt>")

            sbCreditCards.Append("<diff>")
            sbCreditCards.Append(CType(oItem.FindControl("lblCCDiff"), Label).Text.Trim())
            sbCreditCards.Append("</diff>")

            sbCreditCards.Append("</CrCard>")
        Next

        sbCreditCards.Append("</CrCards>")

        ' part 2 - CrCardsimp

        Dim sbCreditCardImprints As New StringBuilder

        sbCreditCardImprints.Append("<CrCardsimp>")

        For Each oItem As RepeaterItem In rptCreditCardImprints.Items
            sbCreditCardImprints.Append("<CrCardimp>")

            sbCreditCardImprints.Append("<ReadOnly>")
            sbCreditCardImprints.Append(CType(oItem.FindControl("txtCCIReadOnly"), TextBox).Text.Trim())
            sbCreditCardImprints.Append("</ReadOnly>")

            sbCreditCardImprints.Append("<pid>")
            sbCreditCardImprints.Append(CType(oItem.FindControl("txtCCIPID"), TextBox).Text.Trim())
            sbCreditCardImprints.Append("</pid>")

            sbCreditCardImprints.Append("<Label>")
            sbCreditCardImprints.Append(CType(oItem.FindControl("lblCCIName"), Label).Text.Trim())
            sbCreditCardImprints.Append("</Label>")

            sbCreditCardImprints.Append("<BTAmt>")
            sbCreditCardImprints.Append(CType(oItem.FindControl("txtCCIBTAmt"), TextBox).Text.Trim())
            sbCreditCardImprints.Append("</BTAmt>")

            sbCreditCardImprints.Append("<TtlAmt>")
            sbCreditCardImprints.Append(CType(oItem.FindControl("txtCCITotalAmount"), TextBox).Text.Trim())
            sbCreditCardImprints.Append("</TtlAmt>")

            sbCreditCardImprints.Append("<diff>")
            sbCreditCardImprints.Append(CType(oItem.FindControl("txtCCiDiff"), TextBox).Text.Trim())
            sbCreditCardImprints.Append("</diff>")

            sbCreditCardImprints.Append("</CrCardimp>")
        Next

        sbCreditCardImprints.Append("</CrCardsimp>")

        ' part 3 - LocalTxns

        Dim sbLCYTxns As New StringBuilder

        sbLCYTxns.Append("<LocalTxns>")

        For Each oItem As RepeaterItem In rptLocalCurrency.Items
            sbLCYTxns.Append("<LocalTxn>")

            sbLCYTxns.Append("<ReadOnly>")
            sbLCYTxns.Append(CType(oItem.FindControl("txtLCYReadOnly"), TextBox).Text.Trim())
            sbLCYTxns.Append("</ReadOnly>")

            sbLCYTxns.Append("<pid>")
            sbLCYTxns.Append(CType(oItem.FindControl("txtLCYPID"), TextBox).Text.Trim())
            sbLCYTxns.Append("</pid>")

            sbLCYTxns.Append("<Label>")
            sbLCYTxns.Append(CType(oItem.FindControl("lblLCYName"), Label).Text.Trim())
            sbLCYTxns.Append("</Label>")

            sbLCYTxns.Append("<BTAmt>")
            sbLCYTxns.Append(CType(oItem.FindControl("txtBranchLCYTotal"), TextBox).Text.Trim())
            sbLCYTxns.Append("</BTAmt>")

            sbLCYTxns.Append("<TtlAmt>")
            sbLCYTxns.Append(CType(oItem.FindControl("txtAuroraLCYTotal"), TextBox).Text.Trim())
            sbLCYTxns.Append("</TtlAmt>")

            sbLCYTxns.Append("<diff>")
            sbLCYTxns.Append(CType(oItem.FindControl("lblLCYDifference"), Label).Text.Trim())
            sbLCYTxns.Append("</diff>")

            sbLCYTxns.Append("</LocalTxn>")
        Next

        sbLCYTxns.Append("</LocalTxns>")

        ' part 4 - ForeignTxns

        Dim sbFCYTxns As New StringBuilder

        sbFCYTxns.Append("<ForeignTxns>")

        For Each oItem As RepeaterItem In rptForeignCurrency.Items
            sbFCYTxns.Append("<ForeignTxn>")

            sbFCYTxns.Append("<pid>")
            sbFCYTxns.Append(CType(oItem.FindControl("txtFCYPID"), TextBox).Text.Trim())
            sbFCYTxns.Append("</pid>")

            sbFCYTxns.Append("<Label>")
            sbFCYTxns.Append(CType(oItem.FindControl("lblFCYName"), Label).Text.Trim())
            sbFCYTxns.Append("</Label>")

            sbFCYTxns.Append("<BTAmt>")
            sbFCYTxns.Append(CType(oItem.FindControl("txtBranchFCYTotal"), TextBox).Text.Trim())
            sbFCYTxns.Append("</BTAmt>")

            sbFCYTxns.Append("<TtlAmt>")
            sbFCYTxns.Append(CType(oItem.FindControl("txtAuroraFCYTotal"), TextBox).Text.Trim())
            sbFCYTxns.Append("</TtlAmt>")

            sbFCYTxns.Append("<diff>")
            sbFCYTxns.Append(CType(oItem.FindControl("lblFCYDifference"), Label).Text.Trim())
            sbFCYTxns.Append("</diff>")

            sbFCYTxns.Append("</ForeignTxn>")
        Next

        sbFCYTxns.Append("</ForeignTxns>")

        ' part 5 - Misc

        Dim sMisc As String

        sMisc = "<Misc>		<Revenue>" & txtFloatTransactionsRevenue.Text & "</Revenue>		<Expense>" & txtFloatTransactionsExpense.Text & "</Expense>	</Misc>"

        ' part 6 - Totals

        Dim sTotals As String

        sTotals = "	<Totals>" & _
                    "		<BTCredit>" & txtBranchCCTotal.Text & "</BTCredit>" & _
                    "		<BTLocal>" & txtBranchLCYTotalAmount.Text & "</BTLocal>" & _
                    "		<BTForeign>" & txtBranchFCYTotalAmount.Text & "</BTForeign>" & _
                    "		<BTTotalRec>" & txtBranchFinalTotal.Text & "</BTTotalRec>" & _
                    "		<TtlCredit>" & txtAuroraCCTotal.Text & "</TtlCredit>" & _
                    "		<TtlLocal>" & txtAuroraLCYTotalAmount.Text & "</TtlLocal>" & _
                    "		<TtlForeign>" & txtAuroraFCYTotalAmount.Text & "</TtlForeign>" & _
                    "		<TtlTotalRec>" & txtAuroraFinalTotal.Text & "</TtlTotalRec>" & _
                    "		<FlClose/>" & _
                    "		<FlOpen/>" & _
                    "		<FlNetChange/>" & _
                    "		<MsTotal>" & txtFloatTransactionsTotal.Text & "</MsTotal>" & _
                   "</Totals>"

        xmlARData.DocumentElement.InnerXml = xmlARData.DocumentElement.SelectSingleNode("LocDetails").OuterXml & _
                                                     sbCreditCards.ToString() & _
                                                     sbCreditCardImprints.ToString() & _
                                                     sbLCYTxns.ToString() & _
                                                     sbFCYTxns.ToString() & _
                                                     sMisc & _
                                                     sTotals

        Return xmlARData.OuterXml

    End Function

    Protected Sub btnSave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave2.Click
        RecalculatePageContents() '(Nothing, Nothing)
        SaveAccountReconData()
    End Sub

    Protected Sub btnSaveNext2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext2.Click
        RecalculatePageContents() '(Nothing, Nothing)
        SaveAccountReconData()
        'next part...
        divFourth.Style.Item("display") = "block"
        divThird.Style.Item("display") = "none"
        LoadPaymentReconcilliationData()
        ' third part active
        wizWizard.SelectedIndex = 3
        ViewState(VIEWSTATE_DCO_WHEREAMI) = "STEP4"
        ' third part active
    End Sub

    Sub SaveAccountReconData()
        Dim xmlReturn As XmlDocument

        xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.SaveDailyCloseOffAccountReconciliationData(GenerateXMLForAccountReconciliation())

        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' refresh!
            PrepareAccountReconcilliationScreen()
        Else
            ' theres an error!
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If
    End Sub

#End Region

#Region "REPORTS"

    Protected Sub btnRunReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
        ' open the 2 new windows
        ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "LoadReport('DailyCloseOffReportViewer.aspx?UserCode=" & UserCode & "&ReportDate=" & CDate(cmbCloseOffDate.SelectedValue).ToShortDateString() & "&Branch=" & lblLocation.Text.Split("-"c)(0).Trim() & "&ReportType=BYMETHOD');" & _
                                                                         "LoadReport('DailyCloseOffReportViewer.aspx?UserCode=" & UserCode & "&ReportDate=" & CDate(cmbCloseOffDate.SelectedValue).ToShortDateString() & "&Branch=" & lblLocation.Text.Split("-"c)(0).Trim() & "&ReportType=BYTYPE');", True)
    End Sub

#End Region

#Region "BACK"

    Protected Sub btnBack2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack2.Click
        LoadDCOData()
    End Sub

#End Region


#End Region

#Region "Div 4 stuff"

#Region "Loading Part"
    Sub LoadPaymentReconcilliationData()

        btnPrint.Attributes.Add("OnClick", "window.print();return false;")

        Dim sLocationId As String

        sLocationId = ViewState(VIEWSTATE_LOCATIONID)
        Dim xmlPRData As XmlDocument
        xmlPRData = Aurora.CustomerService.Services.DailyCloseOff.GetDailyCloseOffPaymentReconcilliation(sLocationId, UserCode)

        With xmlPRData.DocumentElement.SelectSingleNode("Root/LocationCloseOff")
            txtPRReceipts.Text = IIf(.SelectSingleNode("receipts").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("receipts").InnerText.Trim())
            txtPRReceipts.Text = String.Format("{0:f}", CDbl(txtPRReceipts.Text))

            txtPRDeposits.Text = IIf(.SelectSingleNode("deposits").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("deposits").InnerText.Trim())
            txtPRDeposits.Text = String.Format("{0:f}", CDbl(txtPRDeposits.Text))

            txtPRSecurities.Text = IIf(.SelectSingleNode("securities").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("securities").InnerText.Trim())
            txtPRSecurities.Text = String.Format("{0:f}", CDbl(txtPRSecurities.Text))
            '
            txtPROverPayments.Text = IIf(.SelectSingleNode("overpayments").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("overpayments").InnerText.Trim())
            txtPROverPayments.Text = String.Format("{0:f}", CDbl(txtPROverPayments.Text))
            '
            txtPRExpenses.Text = IIf(.SelectSingleNode("expenses").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("expenses").InnerText.Trim())
            txtPRExpenses.Text = String.Format("{0:f}", CDbl(txtPRExpenses.Text))
            '
            txtPRTotalReceipts.Text = IIf(.SelectSingleNode("totreceipts").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("totreceipts").InnerText.Trim())
            txtPRTotalReceipts.Text = String.Format("{0:f}", CDbl(txtPRTotalReceipts.Text))
            '
            txtPRBranchCheckTotal.Text = IIf(.SelectSingleNode("brcheckttl").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("brcheckttl").InnerText.Trim())
            txtPRBranchCheckTotal.Text = String.Format("{0:f}", CDbl(txtPRBranchCheckTotal.Text))
            '
            lblPRDifferenceInTotal.Text = IIf(.SelectSingleNode("diffStr").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("diffStr").InnerText.Trim())
            '            lblPRDifferenceInTotal.Text = String.Format("{0:f}", CDbl(lblPRDifferenceInTotal.Text))
            lblPRDifferenceInTotal.Text = lblPRDifferenceInTotal.Text
            '
            txtPRClosingFloat.Text = IIf(.SelectSingleNode("closingfloat").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("closingfloat").InnerText.Trim())
            txtPRClosingFloat.Text = String.Format("{0:f}", CDbl(txtPRClosingFloat.Text))
            '
            txtPRChequeDrawn.Text = IIf(.SelectSingleNode("chqdrawn").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("chqdrawn").InnerText.Trim())
            txtPRChequeDrawn.Text = String.Format("{0:f}", CDbl(txtPRChequeDrawn.Text))
            '
            txtPRChequeNumber.Text = .SelectSingleNode("BankedChq").InnerText.Trim()
            '
            txtPRCashBanked.Text = IIf(.SelectSingleNode("cashbanked").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("cashbanked").InnerText.Trim())
            txtPRCashBanked.Text = String.Format("{0:f}", CDbl(txtPRCashBanked.Text))
            '
            txtPRFloatCarriedForward.Text = IIf(.SelectSingleNode("FloatCF").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("FloatCF").InnerText.Trim())
            txtPRFloatCarriedForward.Text = String.Format("{0:f}", CDbl(txtPRFloatCarriedForward.Text))
            '
            txtPRCreditCardsAmexDiners.Text = IIf(.SelectSingleNode("CC_Amex").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("CC_Amex").InnerText.Trim())
            txtPRCreditCardsAmexDiners.Text = String.Format("{0:f}", CDbl(txtPRCreditCardsAmexDiners.Text))
            '
            txtPRCreditCardsOther.Text = IIf(.SelectSingleNode("CC_Other").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("CC_Other").InnerText.Trim())
            txtPRCreditCardsOther.Text = String.Format("{0:f}", CDbl(txtPRCreditCardsOther.Text))
            '
            txtPRLocalCurrency.Text = IIf(.SelectSingleNode("LC_Amt").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("LC_Amt").InnerText.Trim())
            txtPRLocalCurrency.Text = String.Format("{0:f}", CDbl(txtPRLocalCurrency.Text))
            '
            txtPRForeignCurrency.Text = IIf(.SelectSingleNode("FC_Amt").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("FC_Amt").InnerText.Trim())
            txtPRForeignCurrency.Text = String.Format("{0:f}", CDbl(txtPRForeignCurrency.Text))
            '
            txtPRFloatRevenueExpense.Text = IIf(.SelectSingleNode("LFloat").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("LFloat").InnerText.Trim())
            txtPRFloatRevenueExpense.Text = String.Format("{0:f}", CDbl(txtPRFloatRevenueExpense.Text))
            '
            txtPRCashBankedFromFloat.Text = IIf(.SelectSingleNode("cashbanked").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("cashbanked").InnerText.Trim())
            txtPRCashBankedFromFloat.Text = String.Format("{0:f}", CDbl(txtPRCashBankedFromFloat.Text))
            '
            txtPRNetChangeInFloat.Text = IIf(.SelectSingleNode("NetChange").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("NetChange").InnerText.Trim())
            txtPRNetChangeInFloat.Text = String.Format("{0:f}", CDbl(txtPRNetChangeInFloat.Text))
            '
            txtPRTotalBanking.Text = IIf(.SelectSingleNode("totBank").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("totBank").InnerText.Trim())
            txtPRTotalBanking.Text = String.Format("{0:f}", CDbl(txtPRTotalBanking.Text))
            '
            txtPRTotalCashChequeSundry.Text = IIf(.SelectSingleNode("totCash_Chk_Sun").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("totCash_Chk_Sun").InnerText.Trim())
            txtPRTotalCashChequeSundry.Text = String.Format("{0:f}", CDbl(txtPRTotalCashChequeSundry.Text))
            '
            txtPRActualBanking.Text = IIf(.SelectSingleNode("ActualBank").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("ActualBank").InnerText.Trim())
            txtPRActualBanking.Text = String.Format("{0:f}", CDbl(txtPRActualBanking.Text))
            '
            txtPRVariance.Text = IIf(.SelectSingleNode("variance").InnerText.Trim().Equals(String.Empty), 0.0, .SelectSingleNode("variance").InnerText.Trim())
            txtPRVariance.Text = String.Format("{0:f}", CDbl(txtPRVariance.Text))

            If .SelectSingleNode("StrReconciled").InnerText.Trim().Equals(String.Empty) Then
                'its not reconciled
                lblReconciliationStatus.Text = .SelectSingleNode("StrNotReconciled").InnerText.Trim()
                lblReconciliationStatus.ForeColor = Drawing.Color.Red
            Else
                lblReconciliationStatus.Text = .SelectSingleNode("StrReconciled").InnerText.Trim()
                lblReconciliationStatus.ForeColor = Drawing.Color.Black
            End If


            ViewState(VIEWSTATE_PR_INTEGRITYNO) = .SelectSingleNode("intno").InnerText.Trim()
            ViewState(VIEWSTATE_PR_DIFF) = .SelectSingleNode("diff").InnerText
            'attaching JS thingies
            'UpdateCrdFwd(CCAmexId, CCOtherId, LCAmountId, FCAmountId, FloatRevExpId, CashBankedId,FloatBankedId, 
            '           NetChangeId, TotalBankingId, TotCashChqSunId, ActualBankId, VarianceId ,StrReconciledId )
            'txtPRChequeDrawn
            txtPRChequeDrawn.Attributes.Add("onChange", "UpdateCrdFwd('" & txtPRCreditCardsAmexDiners.ClientID & _
                                                                      "','" & txtPRCreditCardsOther.ClientID & _
                                                                      "','" & txtPRLocalCurrency.ClientID & _
                                                                      "','" & txtPRForeignCurrency.ClientID & _
                                                                      "','" & txtPRFloatRevenueExpense.ClientID & _
                                                                      "','" & txtPRCashBankedFromFloat.ClientID & _
                                                                      "','" & txtPRCashBanked.ClientID & _
                                                                      "','" & txtPRNetChangeInFloat.ClientID & _
                                                                      "','" & txtPRTotalBanking.ClientID & _
                                                                      "','" & txtPRTotalCashChequeSundry.ClientID & _
                                                                      "','" & txtPRActualBanking.ClientID & _
                                                                      "','" & txtPRVariance.ClientID & _
                                                                      "','" & lblReconciliationStatus.ClientID & "');")
            txtPRCashBanked.Attributes.Add("onChange", "UpdateCrdFwd('" & txtPRCreditCardsAmexDiners.ClientID & _
                                                                      "','" & txtPRCreditCardsOther.ClientID & _
                                                                      "','" & txtPRLocalCurrency.ClientID & _
                                                                      "','" & txtPRForeignCurrency.ClientID & _
                                                                      "','" & txtPRFloatRevenueExpense.ClientID & _
                                                                      "','" & txtPRCashBankedFromFloat.ClientID & _
                                                                      "','" & txtPRCashBanked.ClientID & _
                                                                      "','" & txtPRNetChangeInFloat.ClientID & _
                                                                      "','" & txtPRTotalBanking.ClientID & _
                                                                      "','" & txtPRTotalCashChequeSundry.ClientID & _
                                                                      "','" & txtPRActualBanking.ClientID & _
                                                                      "','" & txtPRVariance.ClientID & _
                                                                      "','" & lblReconciliationStatus.ClientID & "');")
            txtPRActualBanking.Attributes.Add("onChange", "UpdateCrdFwd('" & txtPRCreditCardsAmexDiners.ClientID & _
                                                                      "','" & txtPRCreditCardsOther.ClientID & _
                                                                      "','" & txtPRLocalCurrency.ClientID & _
                                                                      "','" & txtPRForeignCurrency.ClientID & _
                                                                      "','" & txtPRFloatRevenueExpense.ClientID & _
                                                                      "','" & txtPRCashBankedFromFloat.ClientID & _
                                                                      "','" & txtPRCashBanked.ClientID & _
                                                                      "','" & txtPRNetChangeInFloat.ClientID & _
                                                                      "','" & txtPRTotalBanking.ClientID & _
                                                                      "','" & txtPRTotalCashChequeSundry.ClientID & _
                                                                      "','" & txtPRActualBanking.ClientID & _
                                                                      "','" & txtPRVariance.ClientID & _
                                                                      "','" & lblReconciliationStatus.ClientID & "');")


        End With

        '<LocationCloseOff>
        '	<LocDesc>AKL - Auckland</LocDesc>
        '	<LocFloat>350.00</LocFloat>
        '	<lcoid>561831B3-683D-441C-86C1-498369518E06</lcoid>
        '	<LocCloseOffStatus>In Process</LocCloseOffStatus>
        '	<sysdate>Thursday 27 Mar 2008</sysdate>
        '	<totreceipts>0.00</totreceipts>
        '	<refunds>0.00</refunds>
        '	<surplusdificit>0.00</surplusdificit>
        '	<diff>-924.85</diff>
        '	<balance>239.00</balance>



        '	<>-461.00</variance>
        '	<StrReconciled></StrReconciled>
        '	<StrNotReconciled>NOT RECONCILED</StrNotReconciled>
        '	<intno>18</intno>
        '	<loc>AKL</loc>
        '	<username></username>
        '	<lcodate>27/03/2008</lcodate>
        '</LocationCloseOff>

    End Sub

    Sub RecalculatePRData()

        'check the contents of editable textboxes
        If Not IsNumeric(txtPRChequeDrawn.Text) Then
            txtPRChequeDrawn.Text = "0.00"
        End If
        If Not IsNumeric(txtPRCashBanked.Text) Then
            txtPRCashBanked.Text = "0.00"
        End If
        If Not IsNumeric(txtPRActualBanking.Text) Then
            txtPRActualBanking.Text = "0.00"
        End If

        'now format the editable fields
        txtPRChequeDrawn.Text = String.Format("{0:f}", CDbl(txtPRChequeDrawn.Text))
        txtPRCashBanked.Text = String.Format("{0:f}", CDbl(txtPRCashBanked.Text))
        txtPRActualBanking.Text = String.Format("{0:f}", CDbl(txtPRActualBanking.Text))

        'do calculations
        txtPRCashBankedFromFloat.Text = txtPRCashBanked.Text

        Dim dblTotalBanking As Double = 0.0
        dblTotalBanking = CDbl(txtPRCreditCardsAmexDiners.Text) _
                          + CDbl(txtPRCreditCardsOther.Text) _
                          + CDbl(txtPRLocalCurrency.Text) _
                          + CDbl(txtPRForeignCurrency.Text) _
                          + CDbl(txtPRFloatRevenueExpense.Text) _
                          + CDbl(txtPRCashBanked.Text) _
                          - CDbl(txtPRNetChangeInFloat.Text)

        txtPRTotalBanking.Text = String.Format("{0:f}", dblTotalBanking)

        Dim dblTotalCashChequeSundry As Double = 0.0
        dblTotalCashChequeSundry = dblTotalBanking - (CDbl(txtPRCreditCardsAmexDiners.Text) + CDbl(txtPRCreditCardsOther.Text))

        txtPRTotalCashChequeSundry.Text = String.Format("{0:f}", dblTotalCashChequeSundry)

        Dim dblVariance As Double = 0.0
        dblVariance = CDbl(txtPRLocalCurrency.Text) _
                      + CDbl(txtPRForeignCurrency.Text) _
                      + CDbl(txtPRFloatRevenueExpense.Text) _
                      + CDbl(txtPRCashBanked.Text) _
                      - CDbl(txtPRNetChangeInFloat.Text) _
                      - CDbl(txtPRActualBanking.Text)

        txtPRVariance.Text = String.Format("{0:f}", dblVariance)

        'coloring the labels

        If dblVariance = 0.0 Then
            lblReconciliationStatus.Text = "RECONCILED"
            lblReconciliationStatus.ForeColor = Drawing.Color.Black
        Else
            lblReconciliationStatus.Text = "NOT RECONCILED"
            lblReconciliationStatus.ForeColor = Drawing.Color.Red
        End If

    End Sub

#End Region

#Region "Save Part"

    Protected Sub btnSave3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave3.Click
        RecalculatePRData()
        SavePRData()
    End Sub

    Sub SavePRData()
        Dim xmlReturn As New XmlDocument

        'xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.SaveDailyCloseOffPaymentReconciliationData(ViewState(VIEWSTATE_LOCATIONID), _
        '                                                                                                        CDbl(txtPRChequeDrawn.Text), _
        '                                                                                                        CDbl(txtPRCashBanked.Text), _
        '                                                                                                        CDbl(ViewState(VIEWSTATE_PR_DIFF)), _
        '                                                                                                        CInt(ViewState(VIEWSTATE_PR_INTEGRITYNO)), _
        '                                                                                                        txtPRChequeNumber.Text, _
        '                                                                                                        CDbl(txtPRTotalBanking.Text), _
        '                                                                                                        UserCode)

        xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.SaveDailyCloseOffPaymentReconciliationData(ViewState(VIEWSTATE_LOCATIONID), _
                                                                                                              CDbl(txtPRChequeDrawn.Text), _
                                                                                                              CDbl(txtPRCashBanked.Text), _
                                                                                                              CDbl(ViewState(VIEWSTATE_PR_DIFF)), _
                                                                                                              CInt(ViewState(VIEWSTATE_PR_INTEGRITYNO)), _
                                                                                                              txtPRChequeNumber.Text, _
                                                                                                              CDbl(txtPRActualBanking.Text), _
                                                                                                              UserCode)
        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' refresh!
            LoadPaymentReconcilliationData()
        Else
            ' theres an error!
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If


    End Sub
#End Region

#Region "Reconcile Part"

    Sub CheckBeforeReconcilePRData()
        'recalc
        RecalculatePRData()
        ' Shoel : 9.1.9 :  Fix for weird carry forward errors
        SavePRData()
        'check preliminary things
        Dim dblDifference, dblVariance

        dblDifference = 0.0
        dblVariance = 0.0

        dblDifference = CDbl(ViewState(VIEWSTATE_PR_DIFF))
        dblVariance = CDbl(txtPRVariance.Text)

        If (dblDifference < -1.0) Or (dblDifference > 1.0) Then
            ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = "Reconcile"
            cnfbxConfirmation.Text = "Aurora and Branch totals do not match." ' Do you want to continue?"
            If dblVariance <> 0.0 Then
                ' 2 problems...
                cnfbxConfirmation.Text = cnfbxConfirmation.Text & vbNewLine & "The Daily Close Off does not reconcile. Do you want to continue?"
            Else
                'just one problem...
                cnfbxConfirmation.Text = cnfbxConfirmation.Text & " Do you want to continue?"
            End If
            ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = "Reconcile"
            cnfbxConfirmation.Show()
            Exit Sub
        End If

        'else part
        ' this is from the reconcile part!
        ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = Nothing
        ReconcilePRData()
        Exit Sub

    End Sub


    Sub ReconcilePRData()
        'checks passed 
        Dim xmlReturn As New XmlDocument

        xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.ReconcileDailyCloseOff(ViewState(VIEWSTATE_LOCATIONID))
        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' something to be done
            ' set status message
            lblMessage.Text = "Daily Close-off successfully completed."
            ' disabling buttons happens here
            btnSave3.Enabled = False
            btnCancel3.Enabled = False
            btnReconcile.Enabled = False
            btnBack3.Enabled = False
            ' clean up
            ViewState(VIEWSTATE_TOTALS) = Nothing
            ViewState(VIEWSTATE_ROOTDATA) = Nothing
            ViewState(VIEWSTATE_LOCATIONID) = Nothing
            ViewState(VIEWSTATE_FLOATDATA) = Nothing
            ViewState(VIEWSTATE_ARDATA) = Nothing
            ViewState(VIEWSTATE_PR_INTEGRITYNO) = Nothing
            ViewState(VIEWSTATE_PR_DIFF) = Nothing
            ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = Nothing
            ViewState(VIEWSTATE_DCO_WHEREAMI) = Nothing
            wizWizard.SelectedIndex = 4
            wizWizard.Enabled = False
            pnlLast.Enabled = False
        Else
            ' theres an error!
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If
    End Sub

    Protected Sub btnReconcile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReconcile.Click
        CheckBeforeReconcilePRData()
    End Sub

#End Region

#Region "Back Part"

    Protected Sub btnBack3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack3.Click
        divSecond.Style.Item("display") = "none"
        divFourth.Style.Item("display") = "none"
        divInitial.Style.Item("display") = "none"

        divThird.Style.Item("display") = "block"
        PrepareAccountReconcilliationScreen()
        wizWizard.SelectedIndex = 2
    End Sub

#End Region

#End Region


#Region "Cancel handlers"

    Protected Sub Cancel(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel1.Click, btnCancel2.Click, btnCancel3.Click
        cnfbxConfirmation.Text = "Are you sure you want to Cancel this Daily Close Off?"
        ViewState(VIEWSTATE_PR_CONFIRMATIONSTATUS) = "Cancel"
        cnfbxConfirmation.Show()
        If CStr(ViewState(VIEWSTATE_DCO_WHEREAMI)) = "STEP1" Then
            wizWizard.SelectedIndex = 0
        ElseIf CStr(ViewState(VIEWSTATE_DCO_WHEREAMI)) = "STEP2" Then
            wizWizard.SelectedIndex = 1
        ElseIf CStr(ViewState(VIEWSTATE_DCO_WHEREAMI)) = "STEP3" Then
            wizWizard.SelectedIndex = 2
        ElseIf CStr(ViewState(VIEWSTATE_DCO_WHEREAMI)) = "STEP4" Then
            wizWizard.SelectedIndex = 3
        End If
    End Sub

    Sub CancelDailyCloseOff()
        Dim xmlReturn As XmlDocument
        Dim sLocationCode As String = ""
        sLocationCode = Trim(lblLocation.Text.Split("-"c)(0))
        xmlReturn = Aurora.CustomerService.Services.DailyCloseOff.CancelDailyCloseOff(sLocationCode)
        If xmlReturn.DocumentElement.InnerText.Contains("GEN078") Then
            cmbCloseOffDate.Enabled = True
            LoadDCOData()
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            cnfbxConfirmation.Text = "Are you sure you want to Initiate the Daily Close Off?"
        Else
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If
    End Sub

#End Region

    Protected Sub wizWizard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles wizWizard.SelectedIndexChanged
        Console.Write(wizWizard.SelectedIndex)
        Select Case wizWizard.SelectedIndex
            Case 0
                ' cancel btn click
                Cancel(New Object, New System.EventArgs)
            Case 1
                ' float count - div2
                btnBack2_Click(New Object, New System.EventArgs)
            Case 2
                ' acct recon - div3
                btnBack3_Click(New Object, New System.EventArgs)
        End Select
    End Sub


    Public Sub SaveReports(ByVal ReportType As String)
        Dim sReportPath As String = ""
        Dim xmlDoc As XmlDocument = New XmlDocument
        Dim args As XsltArgumentList = New XsltArgumentList
        Dim xslDoc As XslTransform = New XslTransform

        Select Case ReportType
            Case "BYMETHOD"
                sReportPath = Server.MapPath("") & "\Xsl\DailyCloseOff_AccRec_PmntByMethod.xsl"
                xmlDoc = Aurora.CustomerService.Services.DailyCloseOff.GetPaymentTransactionReportByMethod(UserCode, CDate(cmbCloseOffDate.SelectedValue).ToShortDateString())

            Case "BYTYPE"
                sReportPath = Server.MapPath("") & "\Xsl\DailyCloseOff_AccRec_PmntByType.xsl"
                xmlDoc = Aurora.CustomerService.Services.DailyCloseOff.GetPaymentTransactionReportByType(UserCode, CDate(cmbCloseOffDate.SelectedValue).ToShortDateString())
        End Select

        xmlDoc.InnerXml = xmlDoc.InnerXml.Replace("data>", "Data>")
        xslDoc.Load(sReportPath)

        Using streamOut As New System.IO.StringWriter()
            xslDoc.Transform(xmlDoc, Nothing, streamOut, Nothing)
            ' save the contents - in case needed later'DCOREPORTPATH
            Dim sFileName, sFilePath As String
            sFilePath = Aurora.CustomerService.Services.DailyCloseOff.GetDCOReportsPath() & "\"
            sFileName = lblLocation.Text.Split("-"c)(0).Trim() & "_" & _
                        CDate(cmbCloseOffDate.SelectedValue).ToShortDateString().Replace("\"c, "-"c).Replace("/"c, "-"c) & "_" & _
                        ReportType

            'If Not System.IO.File.Exists(sFilePath & sFileName & ".htm") Then
            'Aurora.Booking.Services.BookingConfirmation.SaveFile(sFilePath, sFileName, streamOut.ToString(), "htm")
            Try
                System.IO.File.WriteAllText(sFilePath & sFileName & "." & "htm", streamOut.ToString())
            Catch ex As Exception
            End Try

            'End If
        End Using



    End Sub

#Region "disable buttons based on Role"
    Public Function EFTButtonForCSR() As Boolean

        Dim eft As New BookingPaymentEFTPOSMaintenance
        Dim sreturnmsg As String = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForCSR"))
        If (sreturnmsg.Equals("True")) Then

            Return True
        End If
        Return False
    End Function
#End Region
End Class
