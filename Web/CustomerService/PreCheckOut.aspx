﻿<%--rev:mia Sept 20, 2012 - first release--%>
<%--rev:mia Sept 21, 2012 - start adding enhancement for RA printing--%>

<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="PreCheckOut.aspx.vb" Inherits="CustomerService_PreCheckOut" %>

<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script language="javascript" type="text/javascript">

        var rowcount;
        var warningmessage_INVALID          = "Please check your Rental Id. Length must be greater than or equal to "
        var warningmessage_NOSELECTION      = "Please select Rental Id to be check-out!"
        var warningmessage_PLSTRY           = "Rental Id does not exist. Please try again!"
        var warningmessage_CKOALREADY       = "This Rental Id has been already CHECK-OUT!"
        var informationmessage_FOUND        = "Rental Id was found!"
        var warningmessage_RA_NOSELECTION   = "Please select Rental Id to be printed"
        var warningmessage_RA_EMPTY         = "No Rental Agreement to be display"
        var url                             = "PreCheckoutListener/Precheckoutlistener.aspx"

        var windowWidth     = null;
        var windowHeight    = null;
        var popupHeight     = null;
        var popupWidth      = null;



        $(document).ready(function () {

            //----- trap enter key and escape characters------
            $(document).keypress(function (e) {
                if (e.keyCode == 27 || e.keyCode == 13) { }

                //stop displaying confirmationBox
                if (e.keyCode == 8) { }
            });

            $(document).keydown(function (e) {
                if (e.keyCode == 27 || e.keyCode == 13) {
                    UnloadPopup()
                }
                //stop displaying confirmationBox
                if (e.keyCode == 8) { }
            });
            //--------------------------------------------------//


            $('[id$=textSearch]').focus()
            rowcount = $('[id$=_hiddenRowCount]').val()
            $('[id$=textSearch]').attr("disabled", (rowcount == "0" ? "disabled" : false))
            $('[id$=buttonSearch]').attr("disabled", (rowcount == "0" ? "disabled" : false))




            //-------button search function-------//
            $('[id$=buttonSearch]').click(function () {
                setWarningShortMessage("")
                setInformationShortMessage("")

                var rental = $('[id$=textSearch]').val()
                rental = $.trim(rental).toUpperCase()

                var len = 7
                var firstChar = rental.substring(0, 1)
                var letter = firstChar.match(/^[a-zA-Z]$/);

                //user has enter rentals with letters
                if (letter != null) {
                    len = 8
                }

                if (rental.length < len) {

                    setWarningShortMessage(warningmessage_INVALID + (len == 8 ? " 8 " : " 7 ") + "characters!")

                    $('[id$=textSearch]').select()
                    $('tr.evenRow').css("display", "").css('color', 'black').find('td').find('input').attr("disabled", "disabled").attr("checked", false)
                    $('tr.evenRow').removeClass('selected');
                    $('tr.evenRow').css('color', 'black');
                    $('tr.evenRow').css('background-color', '');

                }
                else {
                    $('[id$=buttonSearch]').attr("disabled", "disabled")
                    SearchRental(rental)
                    $('[id$=buttonSearch]').attr("disabled", false)
                }
            });
            //--------------------------------------------------//



            //-------textSearch function-------//
            $('[id$=textSearch]').keypress(function (event) {
                if (event.which == 13) {
                    $('[id$=buttonSearch]').click()
                }
            });
            //--------------------------------------------------//


            //-------reset function-------//
            $('[id=ctl00_ContentPlaceHolder_cnfbxConfirmation_leftButton]').click(function () {
                var parameter = GetResetParameters()
                DisabledButtons('true')

                $.post(url + "?Mode=RESET", { RESET_PARAMETER: parameter }, function (result) {

                    if (result.indexOf('ERROR') != -1) {
                        setWarningShortMessage(result)
                        DisabledButtons('false')
                    }
                    else {
                        if (result.indexOf('OK') != -1) {
                            setInformationShortMessage(result);
                            DisabledButtons('false')
                            __doPostBack('ctl00$ContentPlaceHolder$buttonRefresh', '')
                        }
                    }
                });
            });
            //--------------------------------------------------//

            $('[id$=buttonRAprint]').click(function () {
                var parameter = GetPrintingParameters()

                if (parameter == "") {
                    setWarningShortMessage(warningmessage_RA_NOSELECTION)
                    return;
                }

                if (parameter == "NULL") {
                    setWarningShortMessage(warningmessage_RA_EMPTY)
                    return;
                }


                clearShortMessage()
                var Win = window.open("PreCheckOutRA.aspx?RA=" + parameter, "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=" + screen.availHeight + " ,width=" + screen.availWidth)
                Win.focus();
                Win.resizeTo(Win.screen.availWidth, Win.screen.availHeight);
                Win.moveTo(0, 0);
                return false;

            });
            //--------------------------------------------------//

            //--------------------------------------------------//
            $(window).resize(function () {
                centerPopup()
            });
            //--------------------------------------------------//


            //-------precheckout function-------//
            $('[id$=buttonPreCheckOut]').click(function () {

                //debugger;
                var parameter = GetParameters()

                if (parameter == "") {
                    setWarningShortMessage(warningmessage_NOSELECTION)
                    return;
                }
                if (parameter == "CKO") {
                    setWarningShortMessage(warningmessage_CKOALREADY)
                    return;
                }

                $('[id$=buttonPreCheckOut]').attr("disabled", "disabled")
                $("[id$=_panelPreCheckout] [id$=_linkRentalId]").attr("disabled", 'disabled')
                $("[id$=_panelSearch] table:first *").attr("disabled", 'disabled')
                setInformationShortMessage("Processing..Please wait!")


                $.post(url + "?Mode=CHECKOUT", { CHECKOUT_PARAMETER: parameter }, function (result) {


                    if (result.indexOf('ERROR') != -1) {
                        setWarningShortMessage(result)
                    }
                    else {
                        if (result.indexOf('OK') != -1) {
                            setInformationShortMessage(result);
                            __doPostBack('ctl00$ContentPlaceHolder$buttonRefresh', '')
                        }
                    }
                    $("[id$=_panelPreCheckout] [id$=_linkRentalId]").attr("disabled", false)
                    $('[id$=buttonPreCheckOut]').attr("disabled", false)
                    $("[id$=_panelSearch] table:first *").attr("disabled", false)

                });
            });
            //--------------------------------------------------//


            //-------button refresh function-------//
            $('[id$=buttonRefresh]').click(function () {
                var btn = $('[id$=buttonRefresh]');
                btn.attr("disabled", "disabled")

                $('[id$=buttonSearch]').attr("disabled", "disabled")
                $("[id$=_panelPreCheckout] *").attr("disabled", 'disabled')
                clearShortMessage()
            });
            //--------------------------------------------------//


            //-------image status function-------//
            $("[id$=_imgStatus]").click(function () {
                //alert($(this).attr("alt"))
                var alttext = $(this).attr("alt")
                if (alttext != '') {



                    $.post(url + "?Mode=NOTES", { NOTES_PARAMETER: alttext }, function (result) {

                        if (result.indexOf('ERROR') != -1 && result.indexOf('ERROR') == 1) {
                            setWarningShortMessage(result)
                            centerPopup()

                            $('[id$=divDescriptionMessage]').html("<h3>" + alttext + "</h3>" + "<center><h3 id='Description'>Error retrieving notes!</h3>Press <b>'Esc'</b> or <b>'Enter'</b> to close this window</center>")
                            $("[id$=ctl00_ContentPlaceHolder_panelPopupDescription]").show()
                        }
                        else {
                            if (result.indexOf('table') != -1) {
                                //debugger;
                                centerPopup()
                                $('[id$=divDescriptionMessage]').html(result + "<center></h3>Press <b>'Esc'</b> or <b>'Enter'</b> to close this window</center>")
                                $("[id$=ctl00_ContentPlaceHolder_panelPopupDescription]").show()
                            }
                        }

                    });
                }
            });
            //--------------------------------------------------//

            $(":checkbox").click(function () {
                
                $(':checkbox:checked').each(function () {
                    if (this.title != '') {
                        $("[id$=chkRentIdAll]").attr("checked", false)
                    }
                });

            });

            $("[id$=chkRentIdAll]").click(function () {
                var chkcount = $(':checkbox').length - 1
                //alert(chkcount)
                if (chkcount <= 0) {
                    setWarningShortMessage(warningmessage_RA_EMPTY)
                    return;
                }
                var chk = $("[id$=chkRentIdAll]").attr("checked")
                $(':checkbox').each(function () {
                    //str += this.checked ? this.title + ',' : "";
                    this.checked = chk
                });

            });
            //--------------------------------------------------//

        });              //$(document).ready(function () {

        function centerPopup() {
            //request data for centering   
            windowWidth = document.documentElement.clientWidth;
            windowHeight = document.documentElement.clientHeight;
            popupHeight = $("[id=ctl00_ContentPlaceHolder_panelPopupDescription]").height();
            popupWidth = $("[id=ctl00_ContentPlaceHolder_panelPopupDescription]").width();
            //centering

             $("[id$=ctl00_ContentPlaceHolder_panelPopupDescription]").css(
                        { "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
             });
           
            
        }

        //-----function GetResetParameters() --------
        function GetResetParameters() {
            return $('[id$=_ContentPlaceHolder_branchPickerControl_pickerTextBox]').val() //'<%= ReturnNextRunSchedule %>'
        }
        //--------------------------------------------------//


        //-----function GetParameters() --------
        function GetParameters() {
            //alert('<%= countrycode %>')
            //alert('<%= UserCode %>')
            //debugger
            var rdo = $("tr.selected input:radio")
            var selected = $("tr.evenRow input:radio:checked")

            if (selected.length > 0) {
                var bookingId = $("tr.evenRow input:radio:checked").attr("title").split("-")[0]
                var rentalId = $("tr.evenRow input:radio:checked").attr("title")
                var userCode = '<%= UserCode %>'
                var countryCode = '<%= countrycode %>'

                if (bookingId.length < 0 || bookingId == null || bookingId == "") return ""

                return bookingId + "|" + rentalId + "|" + userCode + "|" + countryCode
            }
            else {

                //user tried to click the button with already checkout conditions
                if (rdo.length == 0) {
                    //return "CKO"  //checkout already
                    return ""
                }
                else
                    return ""
            }
        }
        //--------------------------------------------------//


        //-----function GetPrintingParameters() --------
        function GetPrintingParameters() {
            var str = "";

            var chkcount = $('.RAprintingCheck:checkbox').length 
            //alert(chkcount)
            if (chkcount <= 0) {
                return "NULL"
            }

            $('.RAprintingCheck:checkbox:checked').each(function () {
                str += this.checked ? this.title + ',' : "";
            });

            if (str != "") {
                str = str.substr(0, str.length - 1);    //Remove the trailing comma
            }
            return str
        }
        //--------------------------------------------------//

        //-------------------------------------------------
        function SearchRental(rentalId) {


            var match = $('tr.evenRow:contains("' + rentalId + '")');
            var nomatch = $('tr.evenRow:not(:contains("' + rentalId + '"))');

            if (match.html() == null) {
                setWarningShortMessage(warningmessage_PLSTRY)
                $('[id$=textSearch]').select()
                $('tr.evenRow').removeClass('selected');
                $('tr.evenRow').css('color', 'black');
                $('tr.evenRow').css('background-color', '');
                $('tr.evenRow').css("display", "").css('color', 'black').find('td').find('input').attr("disabled", "disabled").attr("checked", false)
                return;
            }
            setInformationShortMessage(informationmessage_FOUND);
            match.addClass('selected');
            match.css('color', 'white')
            match.css('background-color', '#8888FF')
           
            nomatch.css('color', 'black');
            nomatch.css('background-color', '');

            //match criteria
            var checkbox = match.find('td').find('input:radio')
            checkbox.attr("disabled", false)

            //unmatch
            //alert(nomatch.html())
            //alert(match.html())
            checkbox = nomatch.find('td').find('input:radio')
            checkbox.attr("disabled", "disabled")
            checkbox.attr("checked", false)

        }
        //--------------------------------------------------//


        function UnloadPopup() {
            $("[id$=_panelPopupDescription]").hide();
            $("[id$=_panelRAprinting]").hide();
        }
        //--------------------------------------------------//


        function DisabledButtons(disabled) {
            $('[id$=buttonRefresh]').attr("disabled", (disabled == 'true' ? 'disabled' : false))
            $('[id$=buttonPreCheckOut]').attr("disabled", (disabled == 'true' ? 'disabled' : false))
            $('[id$=RunResetbutton]').attr("disabled", (disabled == 'true' ? 'disabled' : false))
        }
        //--------------------------------------------------//




        
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel runat="server" ID="panelConfirmation">
        <ContentTemplate>
            <uc2:ConfirmationBoxControl ID="cnfbxConfirmation" runat="server" LeftButton_AutoPostBack="false"
                LeftButton_Text="Yes" RightButton_AutoPostBack="false" RightButton_Text="No"
                Title="Confirmation Required" Text="Are you sure you want to include these Rental(s) in the next Auto-CheckOut Schedule?"
                MessageType="Information"  />
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Panel runat="server" ID="panelSearch" CssClass="noPrint">
        <table>
            <tr>
                <td>
                    Location:
                </td>
                <td>
                    <uc1:PickerControl ID="branchPickerControl" runat="server" PopupType="LOCATION" AppendDescription="true"
                        Width="180px" Nullable="false" Caption="Branch" />
                </td>
                <td>
                    <input type="button" value="Refresh" class="Button_Standard" id="buttonRefresh" name="buttonRefresh"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    Rental Number:
                </td>
                <td>
                    <input type="text" size="30" id="textSearch" />
                </td>
                <td>
                    <input type="button" value="Search" class="Button_Standard Button_Search" id="buttonSearch"
                        name="buttonSearch" />
                </td>
            </tr>
        </table>
        <table id="tableRunSchedule">
            <tr align="right">
                <td style="width: 400px;">
                </td>
                <td>
                </td>
                
                <td style="width: 150px;">
                    <b>Last run:</b>
                </td>
                <td style="font-style: italic;width: 200px;" align="center">
                <%= ReturnLastRunSchedule %>
                    
                </td>
                <td>
                    <input type="button" id="RunResetbutton" value="ReRun" class="Button_Standard"
                        runat="server" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    

    <asp:Panel runat="server" ID="panelPreCheckout" CssClass="noPrint">
        <%--1 - Screen column order from left to Right = Customer name – Bkg No. – ETA – Rego No. – Scheduled vehicle code – Bkg vehicle code – ‘Status’ – CO tick box--%>
        <asp:GridView ID="PreCheckoutGridView" runat="server" CssClass="dataTableGrid" Width="100%"
            AutoGenerateColumns="False" ShowFooter="True" DataKeyNames="RntId">
            <RowStyle CssClass="evenRow"></RowStyle>
            <AlternatingRowStyle CssClass="oddRow evenRow" />
            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
            <Columns>

                <%--<asp:BoundField DataField="Name" HeaderText="Customer"  ItemStyle-Width="150"/>--%>
                <asp:TemplateField HeaderText="Customer" ItemStyle-Width="180">
                    <ItemTemplate>
                        <%--<asp:Label ID="LabelCustomer" runat="server" />--%>
                        <asp:hyperlink runat="server" id="linkcustomername"/>
                    </ItemTemplate>
                </asp:TemplateField>


                <%--Booking--%>
                <asp:TemplateField HeaderText="Booking" ItemStyle-Width="70">
                    <ItemTemplate>
                        <%--<asp:hyperlink runat="server" id="linkRentalId"/>--%>
                        <asp:Label ID="LabelRentalID" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <%--Out--%>
                <asp:TemplateField HeaderText="Out" ItemStyle-Width="50">
                    <ItemTemplate>
                        <asp:Label ID="LabelOut" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <%--To--%>
                <asp:TemplateField HeaderText="To" ItemStyle-Width="80">
                    <ItemTemplate>
                        <asp:Label ID="LabelCkoAndCKI" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <%--Booked Veh.--%>
                <asp:TemplateField HeaderText="Booked Veh." ItemStyle-Width="80">
                    <ItemTemplate>
                        <asp:Label ID="LabelBookedVeh" runat="server" Text="" />
                    </ItemTemplate>
                </asp:TemplateField>

                <%--Vehicle--%>
                <asp:TemplateField HeaderText="Vehicle" ItemStyle-Width="180">
                    <ItemTemplate>
                        <asp:Label runat="server" Text="" ID="LabelVehicle"/>
                    </ItemTemplate>
                </asp:TemplateField>

                <%--Status--%>
                <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="40"> 
                    <ItemTemplate>
                        <asp:Image runat="server" ID="imgStatus" Width="20" Height="20" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemStyle Width="60" HorizontalAlign="Center" />
                    <FooterStyle HorizontalAlign="Left" Width="60" />
                    <ItemTemplate>
                        <input type="radio" name="radioRentId" title='<%# eval("RntId") %>' disabled='disabled'
                            runat="server" id="radioRentId" />
                    </ItemTemplate>
                    <FooterTemplate>
                        <input type="button" value="Checkout" class="Button_Standard" id="buttonPreCheckOut"   style="width:58px;" />
                    </FooterTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>

                <asp:TemplateField>
                    <ItemStyle Width="60" HorizontalAlign="Center" />
                    <FooterStyle HorizontalAlign="Left" Width="60" />
                    <ItemTemplate>
                        <input type="checkbox" name="chkRentId" title='<%# eval("RntId") %>' disabled='disabled' 
                            runat="server" id="chkRentId" class="RAprinting RAprintingCheck" />
                    </ItemTemplate>
                    <FooterTemplate>
                        <input type="button" value="Show RA" class="Button_Standard" id="buttonRAprint"   style="width:58px;" />
                    </FooterTemplate>
                    
                    <HeaderTemplate>
                        <center>Select All 
                        <input type="checkbox" name="chkRentIdAll" 
                                               runat="server" 
                                               id="chkRentIdAll" 
                                               class="RAprinting" /></center>
                        
                    </HeaderTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:TemplateField>

            </Columns>
            <EmptyDataRowStyle CssClass="evenRow" />
            <EmptyDataTemplate>
                <table class="dataTableGrid">
                    <thead>
                        <tr>
                            <td style="width: 100px;">
                                Customer
                            </td>
                            <td style="width: 100px;">
                                Booking
                            </td>
                            <td>
                                Out
                            </td>
                            <td>
                                To
                            </td>
                            <td>
                                Booked Veh.
                            </td>
                            <td style="width: 100px;">
                                Vehicle
                            </td>
                            <td>
                                Status
                            </td>
                        </tr>
                    </thead>
                    <tr>
                        <tr>
                            <td>
                                <input type="text" disabled="disabled" style="width: 150px;" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" style="width: 100px;" />
                            </td>
                             <td>
                                <input type="text" disabled="disabled" style="width: 100px;" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" style="width: 100px;" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" style="width: 100px;" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" style="width: 130px;" />
                            </td>
                            <td>
                                <input type="text" disabled="disabled" style="width: 40px;" />
                            </td>
                        </tr>
                </table>
            </EmptyDataTemplate>
        </asp:GridView>
        <table cellspacing="0" cellpadding="2" style="width: 780px; margin-top: 20px" class="dataTable">
            <tr>
                <td style="width: 10%">
                    <i>Key:</i>
                </td>
                <td style="width: 15%" class="rowDone">
                    <asp:Image ID="Image2" ImageUrl="~/Images/tick_green.gif" ImageAlign="absBottom"
                        runat="Server" />&nbsp;Check-Out
                </td>
                <td style="width: 15%" class="rowDue">
                    <asp:Image ID="Image3" ImageUrl="~/Images/time.gif" ImageAlign="absBottom" runat="Server" />&nbsp;Pre-CheckOut
                </td>
               
                <td style="width: 15%">
                    <asp:Image ID="Image5" ImageUrl="~/Images/rental_error.gif" ImageAlign="absBottom"
                        runat="Server" />&nbsp;Rental Error
                </td>
               
            </tr>
        </table>
    </asp:Panel>

    <asp:HiddenField ID="hiddenRowCount" runat="server" />
    <asp:HiddenField ID="HiddenLastRundTime" runat="server" />

    <asp:Panel ID="panelPopupDescription" runat="server" CssClass="modalPopup" Style="display: none;
        width: 600px;">
        <div id="divDescriptionMessage" style="position: relative; border: 0px solid #ccc" />


    </asp:Panel>

</asp:Content>
