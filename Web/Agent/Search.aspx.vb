Imports System.Collections.Generic
Imports System.Drawing

Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet
Imports System.Data

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.AgentEnquiry)> _
<AuroraPageTitle("Agent Search")> _
Partial Class Agent_Search
    Inherits AuroraPage

    Private _agentDataSet As New AgentDataSet()

    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentMaintenance)
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        _agentDataSet.EnforceConstraints = False

        DataRepository.GetLookups(_agentDataSet, Me.CompanyCode)

        Me.agentTypeDropDown.Items.Clear()
        Me.agentTypeDropDown.Items.Add(New ListItem("(All)", ""))
        For Each agentTypeRow As AgentTypeRow In _agentDataSet.AgentType
            Me.agentTypeDropDown.Items.Add(New ListItem(agentTypeRow.AtyDesc, agentTypeRow.AtyId))
        Next

        Me.debtorStatusDropDown.Items.Clear()
        Me.debtorStatusDropDown.Items.Add(New ListItem("(All)", ""))
        For Each debtorStatusCodeRow As CodeRow In _agentDataSet.Code
            If debtorStatusCodeRow.CodCdtNum = CodeRow.CodeType_AGENT_DEBTOR_STATUS Then _
                Me.debtorStatusDropDown.Items.Add(New ListItem(debtorStatusCodeRow.CodDesc, debtorStatusCodeRow.CodId))
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem("(All)", ""))
        For Each countryRow As CountryRow In _agentDataSet.Country
            countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        Next
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        If useViewState Then Return

        codeTextBox.Text = "" & CookieValue("Agent_agnCode")
        nameTextBox.Text = "" & CookieValue("Agent_agnName")

        Try
            agentTypeDropDown.SelectedValue = "" & CookieValue("Agent_agentType")
        Catch
        End Try

        agentGroupPicker.DataId = CookieValue("Agent_agpId")
        agentGroupPicker.Text = CookieValue("Agent_agpDesc")

        Try
            debtorStatusDropDown.SelectedValue = "" & CookieValue("Agent_debtorStatus")
        Catch
        End Try

        phoneCountryCodeTextBox.Text = "" & CookieValue("Agent_phnCountryCode")
        phoneAreaCodeTextBox.Text = "" & CookieValue("Agent_phnAreaCode")
        phoneNumberTextBox.Text = "" & CookieValue("Agent_phnNumber")

        streetTextBox.Text = "" & CookieValue("Agent_addAddress1")
        suburbTextBox.Text = "" & CookieValue("Agent_addAddress2")
        cityTownTextBox.Text = "" & CookieValue("Agent_addAddress3")
        stateTextBox.Text = "" & CookieValue("Agent_addState")
        postcodeTextBox.Text = "" & CookieValue("Agent_addPostcode")
        Try
            countryDropDown.SelectedValue = "" & CookieValue("Agent_addCtyCode")
        Catch
        End Try

        Try
            statusRadioButtonList.SelectedValue = "" & CookieValue("Agent_agnIsActive")
        Catch
        End Try

        Try
            soundExCheckBox.Checked = Boolean.Parse(CookieValue("Agent_agnSoundEx"))
        Catch
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        createButton.Visible = CanMaintain

        If Not Page.IsPostBack Then
            If Not String.IsNullOrEmpty(Me.SearchParam) Then
                nameTextBox.Text = Me.SearchParam
            Else
                BuildForm(False)
            End If

            'Mod: Raj - Feb 17, 2012 (Maintain checkbox state and search parameters on navigation away from page and back)
            'On Non-Postback read session variable to change the checkbox state 'checked' if '1' ,'unchecked' if '0'

            'If (Session("hasB2B") = 1) Then
            If (CookieValue("hasB2B") = 1) Then
                radioB2BAccess.Checked = True
            Else
                radioB2BAccess.Checked = False
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            If HasSearchParameters() Then searchButton_Click(Me, Nothing)
        Else
            'Mod: Raj - Feb 17, 2012 (Maintain checkbox state and search parameters on navigation away from page and back)
            'On Postback set session variable to reflect the checkbox state '1' if 'checked' '0' if 'unchecked'

            If (radioB2BAccess.Checked = True) Then
                'Session("hasB2B") = 1
                CookieValue("hasB2B") = 1
            Else
                'Session("hasB2B") = 0
                CookieValue("hasB2B") = 0
            End If
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        End If

        
    End Sub

    Private Sub UpdateCookieValues()
        CookieValue("Agent_agnCode") = codeTextBox.Text.Trim()
        CookieValue("Agent_agnName") = nameTextBox.Text.Trim()
        CookieValue("Agent_agentType") = agentTypeDropDown.SelectedValue
        If Not String.IsNullOrEmpty(agentGroupPicker.DataId) Then
            CookieValue("Agent_agpId") = agentGroupPicker.DataId
            CookieValue("Agent_agpDesc") = agentGroupPicker.Text
        Else
            CookieValue("Agent_agpId") = ""
            CookieValue("Agent_agpDesc") = ""
        End If
        CookieValue("Agent_debtorStatus") = debtorStatusDropDown.SelectedValue

        CookieValue("Agent_phnCountryCode") = phoneCountryCodeTextBox.Text.Trim()
        CookieValue("Agent_phnAreaCode") = phoneAreaCodeTextBox.Text.Trim()
        CookieValue("Agent_phnNumber") = phoneNumberTextBox.Text.Trim()

        CookieValue("Agent_addAddress1") = streetTextBox.Text.Trim()
        CookieValue("Agent_addAddress2") = suburbTextBox.Text.Trim()
        CookieValue("Agent_addAddress3") = cityTownTextBox.Text.Trim()
        CookieValue("Agent_addState") = stateTextBox.Text.Trim()
        CookieValue("Agent_addPostcode") = postcodeTextBox.Text.Trim()
        CookieValue("Agent_addCtyCode") = countryDropDown.SelectedValue

        CookieValue("Agent_agnIsActive") = statusRadioButtonList.SelectedValue
        CookieValue("Agent_agnSoundEx") = soundExCheckBox.Checked.ToString()
    End Sub

    Public Function HasSearchParameters() As Boolean
        If Not agentGroupPicker.IsValid Then Return False

        Return codeTextBox.Text.Trim().Length > 0 _
         OrElse nameTextBox.Text.Trim().Length > 0 _
         OrElse agentTypeDropDown.SelectedIndex > 0 _
         OrElse Not String.IsNullOrEmpty(agentGroupPicker.DataId) _
         OrElse debtorStatusDropDown.SelectedIndex > 0 _
         OrElse phoneCountryCodeTextBox.Text.Trim().Length > 0 _
         OrElse phoneAreaCodeTextBox.Text.Trim().Length > 0 _
         OrElse phoneNumberTextBox.Text.Trim().Length > 0 _
         OrElse streetTextBox.Text.Trim().Length > 0 _
         OrElse suburbTextBox.Text.Trim().Length > 0 _
         OrElse cityTownTextBox.Text.Trim().Length > 0 _
         OrElse stateTextBox.Text.Trim().Length > 0 _
         OrElse postcodeTextBox.Text.Trim().Length > 0 _
         OrElse Not String.IsNullOrEmpty(countryDropDown.SelectedValue) _
         OrElse Not String.IsNullOrEmpty(statusRadioButtonList.SelectedValue) _
         OrElse False
    End Function

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        If Not HasSearchParameters() Then
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "Enter valid search parameters")
            searchResultLabel.Text = ""
            searchResultTable.Visible = False
            Return
        End If

        UpdateCookieValues()

        Dim agnIsActive As Nullable(Of Boolean)
        If statusRadioButtonList.SelectedValue = "True" Then
            agnIsActive = New Nullable(Of Boolean)(True)
        ElseIf statusRadioButtonList.SelectedValue = "False" Then
            agnIsActive = New Nullable(Of Boolean)(False)
        Else
            agnIsActive = New Nullable(Of Boolean)
        End If

        ''REV:MIA JAN.23 2012 - RELATED TO B2B SEARCH: HELPDESK # 31925
        Dim hasB2B As Integer = IIf(radioB2BAccess.Checked = True, 1, 0)

        DataRepository.SearchAgent(_agentDataSet, _
            Nothing, _
            codeTextBox.Text.Trim(), _
            nameTextBox.Text.Trim(), _
            agentGroupPicker.DataId, _
            agentTypeDropDown.SelectedValue, _
            debtorStatusDropDown.SelectedValue, _
            phoneCountryCodeTextBox.Text.Trim(), _
            phoneAreaCodeTextBox.Text.Trim(), _
            phoneNumberTextBox.Text.Trim(), _
            streetTextBox.Text.Trim(), _
            suburbTextBox.Text.Trim(), _
            cityTownTextBox.Text.Trim(), _
            stateTextBox.Text.Trim(), _
            postcodeTextBox.Text.Trim(), _
            countryDropDown.SelectedValue, _
            agnIsActive, _
            Nothing, _
            soundExCheckBox.Checked, _
            hasB2B) ''REV:MIA JAN.23 2012 - RELATED TO B2B SEARCH: HELPDESK # 31925

        While searchResultTable.Rows.Count > 1
            searchResultTable.Rows.RemoveAt(1)
        End While

        If _agentDataSet.Agent.Rows.Count = 0 Then
            searchResultLabel.Text = "No agents found"
            searchResultTable.Visible = False
        Else
            If _agentDataSet.Agent.Rows.Count = 1 Then
                searchResultLabel.Text = _agentDataSet.Agent.Rows.Count.ToString() & " agent found"
            Else
                searchResultLabel.Text = _agentDataSet.Agent.Rows.Count.ToString() & " agents found"
            End If
            searchResultTable.Visible = True


            For Each agentRow As AgentRow In _agentDataSet.Agent
                Dim tableRow As New TableRow()
                tableRow.BackColor = agentRow.StatusColor
                searchResultTable.Rows.Add(tableRow)

                Dim nameCell As New TableCell()
                Dim nameHyperLink As New HyperLink()
                nameHyperLink.Text = Server.HtmlEncode(agentRow.AgentDescription)
                nameHyperLink.NavigateUrl = "Maintenance.aspx?agnId=" & Server.UrlEncode(agentRow.AgnId) & "&BackUrl=" & Server.UrlEncode(Me.Request.Path)
                nameCell.Controls.Add(nameHyperLink)
                tableRow.Controls.Add(nameCell)

                Dim agentTypeCell As New TableCell()
                agentTypeCell.Text = Server.HtmlEncode(agentRow.AgentTypeDescription)
                tableRow.Controls.Add(agentTypeCell)

                Dim agentGroupCell As New TableCell()
                agentGroupCell.Text = Server.HtmlEncode(agentRow.AgentGroupDescription)
                tableRow.Controls.Add(agentGroupCell)

                Dim debtorStatusCell As New TableCell()
                debtorStatusCell.Text = Server.HtmlEncode(agentRow.DebtorStatusDescription)
                tableRow.Controls.Add(debtorStatusCell)

                Dim billingAddressCell As New TableCell()
                billingAddressCell.Text = Server.HtmlEncode(agentRow.BillingAddressDescription)
                tableRow.Controls.Add(billingAddressCell)

                Dim statusCell As New TableCell()
                statusCell.Text = IIf(Not agentRow.IsAgnIsActiveNull AndAlso agentRow.AgnIsActive, "Active", "Inactive")
                tableRow.Controls.Add(statusCell)
            Next




        End If
    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        UpdateCookieValues()

        Response.Redirect("Maintenance.aspx?BackUrl=" & Server.UrlEncode(Me.Request.Path))
    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        codeTextBox.Text = ""
        nameTextBox.Text = ""
        phoneCountryCodeTextBox.Text = ""
        phoneAreaCodeTextBox.Text = ""
        phoneNumberTextBox.Text = ""
        streetTextBox.Text = ""
        suburbTextBox.Text = ""
        cityTownTextBox.Text = ""
        stateTextBox.Text = ""
        postcodeTextBox.Text = ""

        agentTypeDropDown.SelectedIndex = 0
        debtorStatusDropDown.SelectedIndex = 0

        countryDropDown.SelectedIndex = 0
        agentGroupPicker.DataId = ""
        agentGroupPicker.Text = ""

        searchResultTable.Visible = False
        searchResultLabel.Text = ""

        UpdateCookieValues()
    End Sub
End Class