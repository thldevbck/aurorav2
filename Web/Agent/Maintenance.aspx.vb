'   Change Log
'   23.4.10 -   Shoel   -   Email CodId is now picked up from CodCdtNum = 45 not 11
'                       -   Added a transaction around the update
'                       -   Update click now calls the method that doesnt update the ID field, thus saving time
'   25.6.10 -   Shoel   -   Added validations to email address length (Varchar 64)

'   manny's logs
'   -- oct.6 2011 - Role to enable Debtor Status 
'   -- nov.7 2012 - Addition of Debtor Grouping

Imports System.Collections.Generic

Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.AgentEnquiry)> _
Partial Class Agent_Maintenance
    Inherits AuroraPage

    Private _initException As Exception

    Private _agnId As String = Nothing
    Private _agentDataSet As New AgentDataSet()
    Private _agentRow As AgentRow = Nothing


    Protected ReadOnly Property CanMaintain() As Boolean
        Get
            Return Me.GetFunctionPermission(AuroraFunctionCodeAttribute.AgentMaintenance)
        End Get
    End Property


    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If String.IsNullOrEmpty(_agnId) Then
                Return "New Agent"
            ElseIf CanMaintain Then
                Return "Edit Agent"
            Else
                Return "View Agent"
            End If
        End Get
    End Property


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("agnId")) Then
                _agnId = Request.QueryString("agnId").Trim()
            End If

            MyBase.OnInit(e)

            _agentDataSet.EnforceConstraints = False

            DataRepository.GetLookups(_agentDataSet, Me.CompanyCode)

            If Not String.IsNullOrEmpty(_agnId) Then
                DataRepository.GetAgent(_agentDataSet, Me.CompanyCode, _agnId)
                _agentRow = _agentDataSet.Agent.FindByAgnId(_agnId)
                If _agentRow Is Nothing Then
                    Throw New Exception("No agent found")
                End If
            End If

            Me.agentTypeDropDown.Items.Clear()
            Me.agentTypeDropDown.Items.Add(New ListItem(""))
            For Each agentTypeRow As AgentTypeRow In _agentDataSet.AgentType
                If Not agentTypeRow.IsAtyIsActiveNull AndAlso agentTypeRow.AtyIsActive Then
                    Me.agentTypeDropDown.Items.Add(New ListItem(agentTypeRow.AtyDesc, agentTypeRow.AtyId))
                End If
            Next

            Me.categoryDropDown.Items.Clear()
            For Each categoryCodeRow As CodeRow In _agentDataSet.Code
                If categoryCodeRow.CodCdtNum = CodeRow.CodeType_Agent_Category Then _
                    Me.categoryDropDown.Items.Add(New ListItem(categoryCodeRow.CodDesc, categoryCodeRow.CodId))
            Next

            Me.marketCodeDropDown.Items.Clear()
            Me.marketCodeDropDown.Items.Add(New ListItem("(None)", ""))
            For Each codeRow As CodeRow In _agentDataSet.Code
                If codeRow.CodCdtNum <> codeRow.CodeType_Marketing_Region Then Continue For
                If codeRow.IsCodDescNull Then Continue For

                Me.marketCodeDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            Next

            Me.debtorStatusDropDown.Items.Clear()
            Me.debtorStatusDropDown.Items.Add(New ListItem("", ""))
            For Each debtorStatusCodeRow As CodeRow In _agentDataSet.Code
                If debtorStatusCodeRow.CodCdtNum = CodeRow.CodeType_AGENT_DEBTOR_STATUS Then _
                    Me.debtorStatusDropDown.Items.Add(New ListItem(debtorStatusCodeRow.CodDesc, debtorStatusCodeRow.CodId))
            Next
            ''oct.6 2011 - Role to enable Debtor Status
            Me.debtorStatusDropDown.Enabled = EnableChargeDebtor(UserCode)
            Me.DebtorGrouping1.EnabledDebtorGrouping = Me.debtorStatusDropDown.Enabled

            Me.flexExportTypeCheckBoxList.Items.Clear()
            For Each flexExportTypeRow As FlexExportTypeRow In _agentDataSet.FlexExportType
                If flexExportTypeRow.FlxComCode = Aurora.Common.UserSettings.Current.ComCode Then
                    Me.flexExportTypeCheckBoxList.Items.Add(New ListItem(flexExportTypeRow.FlxCode & " - " & flexExportTypeRow.FlxName, flexExportTypeRow.FlxId))
                End If
            Next

            ' b2b change
            If _agentRow Is Nothing OrElse (_agentDataSet IsNot Nothing AndAlso _agentDataSet.B2BUserInfo.Rows.Count = 0) Then
                For Each agentUserControl As AgentUserControl In New AgentUserControl() { _
              billingAddressDetailsControl, _
              physicalAddressDetailsControl, _
              postalAddressDetailsControl, _
              productControl, _
              packageControl, _
              noteControl, _
              contactsControl}
                    agentUserControl.InitAgentUserControl(_agentDataSet, _agentRow)
                Next
                b2bTabPanel.Visible = False
            Else
                For Each agentUserControl As AgentUserControl In New AgentUserControl() { _
              billingAddressDetailsControl, _
              physicalAddressDetailsControl, _
              postalAddressDetailsControl, _
              productControl, _
              packageControl, _
              noteControl, _
              contactsControl, _
              b2bControl}
                    agentUserControl.InitAgentUserControl(_agentDataSet, _agentRow)
                Next
            End If
            ' b2b change

            'For Each agentUserControl As AgentUserControl In New AgentUserControl() { _
            ' billingAddressDetailsControl, _
            ' physicalAddressDetailsControl, _
            ' postalAddressDetailsControl, _
            ' productControl, _
            ' packageControl, _
            ' noteControl, _
            ' contactsControl, _
            ' b2bControl}
            '    agentUserControl.InitAgentUserControl(_agentDataSet, _agentRow)
            'Next

        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Aurora.Common.Logging.LogError(Me.FunctionCode, ex.Message, ex)
        End Try
    End Sub

    Private Sub BuildAgent(ByVal useViewState As Boolean)

        flexExportTypeEmail0TextBox.Text = ""
        flexExportTypeEmail1TextBox.Text = ""
        flexExportTypeEmail2TextBox.Text = ""
        If _agentRow IsNot Nothing AndAlso _agentRow.AgentCompanyRow(Me.CompanyCode) IsNot Nothing Then
            Dim agentCompanyRow As AgentCompanyRow = _agentRow.AgentCompanyRow(Me.CompanyCode)
            If Not agentCompanyRow.IsAgcEmail0Null() Then flexExportTypeEmail0TextBox.Text = agentCompanyRow.AgcEmail0
            If Not agentCompanyRow.IsAgcEmail1Null() Then flexExportTypeEmail1TextBox.Text = agentCompanyRow.AgcEmail1
            If Not agentCompanyRow.IsAgcEmail2Null() Then flexExportTypeEmail2TextBox.Text = agentCompanyRow.AgcEmail2
        End If

        If useViewState Then Return

        codeTextBox.Text = ""
        nameTextBox.Text = ""
        agentTypeDropDown.SelectedIndex = 0
        categoryDropDown.SelectedIndex = categoryDropDown.Items.Count - 1
        groupSubgroupPicker.DataId = Nothing
        groupSubgroupPicker.Text = ""
        debtorStatusDropDown.SelectedIndex = 2
        marketCodeDropDown.SelectedIndex = 0

        For Each listItem As ListItem In flagsCheckBoxList.Items
            listItem.Selected = False
        Next

        phoneCountryCodeTextBox.Text = ""
        phoneAreaCodeTextBox.Text = ""
        phoneNumberTextBox.Text = ""

        faxCountryCodeTextBox.Text = ""
        faxAreaCodeTextBox.Text = ""
        faxNumberTextBox.Text = ""

        freePhoneNoTextBox.Text = ""
        emailTextBox.Text = ""
        websiteTextBox.Text = ""

        billingAddressDetailsControl.Clear()
        physicalAddressDetailsControl.Clear()
        postalAddressDetailsControl.Clear()

        For Each listItem As ListItem In flexExportTypeCheckBoxList.Items
            listItem.Selected = False
        Next
        flexExportTypeTabPanel.Enabled = flexExportTypeCheckBoxList.Items.Count > 0

        If _agentRow Is Nothing Then
            ' New Agent
            agentDetailsPanel.Enabled = CanMaintain

            codeTextBox.Text = "" & CookieValue("Agent_agnCode")
            codeTextBox.ReadOnly = False

            nameTextBox.Text = "" & CookieValue("Agent_agnName")

            Try
                agentTypeDropDown.SelectedValue = "" & CookieValue("Agent_agentType")
            Catch
            End Try

            phoneCountryCodeTextBox.Text = "" & CookieValue("Agent_phnCountryCode")
            phoneAreaCodeTextBox.Text = "" & CookieValue("Agent_phnAreaCode")
            phoneNumberTextBox.Text = "" & CookieValue("Agent_phnNumber")

            Try
                statusRadioButtonList.SelectedValue = "" & CookieValue("Agent_agnIsActive")
            Catch
            End Try

            statusRadioButtonList.Items(0).Selected = True
            statusRadioButtonList.Items(1).Selected = Not statusRadioButtonList.Items(0).Selected

            contactsPanel.Enabled = CanMaintain
            contactsControl.Visible = False

            flexExportTypeCheckBoxList.Enabled = CanMaintain
            flexExportTypePanel.Visible = CanMaintain

            notesTabPanel.Enabled = False
            productTabPanel.Enabled = False
            packageTabPanel.Enabled = False

            updateButton.Visible = False
            createButton.Visible = CanMaintain

            ' Phoenix Change
            txtMaxDepositAmount.Text = String.Empty
            ' Phoenix Change
        Else
            ' Edit/View Agent

            agentDetailsPanel.Enabled = CanMaintain

            If Not _agentRow.IsAgnCodeNull Then codeTextBox.Text = _agentRow.AgnCode
            codeTextBox.ReadOnly = True
            If Not _agentRow.IsAgnNameNull Then nameTextBox.Text = _agentRow.AgnName
            Try
                agentTypeDropDown.SelectedValue = _agentRow.AgnAtyId
            Catch
            End Try
            Try
                categoryDropDown.SelectedValue = _agentRow.AgnCodBlkCatId
            Catch
            End Try
            If Not _agentRow.IsAgnAsgIdNull Then
                Me.groupSubgroupPicker.DataId = _agentRow.AgnAsgId
                Me.groupSubgroupPicker.Text = _agentRow.AgentGroupDescription
            End If
            Try
                debtorStatusDropDown.SelectedValue = _agentRow.AgnCodDebtStatId
            Catch
            End Try
            Try
                marketCodeDropDown.SelectedValue = _agentRow.AgnCodMktRgnId
            Catch
            End Try

            For Each listItem As ListItem In flagsCheckBoxList.Items
                listItem.Selected = _agentRow.Table.Columns.Contains(listItem.Value) _
                 AndAlso Not _agentRow.IsNull(listItem.Value) _
                 AndAlso CType(_agentRow(listItem.Value), Boolean)
            Next

            statusRadioButtonList.Items(0).Selected = _agentRow.AgnIsActive
            statusRadioButtonList.Items(1).Selected = Not statusRadioButtonList.Items(0).Selected

            contactsPanel.Enabled = CanMaintain
            contactsControl.Visible = True

            phoneCountryCodeTextBox.Text = _agentRow.PhoneNumberField("Day", "PhnCountryCode")
            phoneAreaCodeTextBox.Text = _agentRow.PhoneNumberField("Day", "PhnAreaCode")
            phoneNumberTextBox.Text = _agentRow.PhoneNumberField("Day", "PhnNumber")

            faxCountryCodeTextBox.Text = _agentRow.PhoneNumberField("Fax", "PhnCountryCode")
            faxAreaCodeTextBox.Text = _agentRow.PhoneNumberField("Fax", "PhnAreaCode")
            faxNumberTextBox.Text = _agentRow.PhoneNumberField("Fax", "PhnNumber")

            freePhoneNoTextBox.Text = _agentRow.PhoneNumberField("0800", "PhnNumber")
            emailTextBox.Text = _agentRow.PhoneNumberField("Email", "PhnEmailAddress")
            websiteTextBox.Text = _agentRow.PhoneNumberField("Web", "PhnEmailAddress")

            billingAddressDetailsControl.Build()
            physicalAddressDetailsControl.Build()
            postalAddressDetailsControl.Build()

            For Each listItem As ListItem In flexExportTypeCheckBoxList.Items
                For Each flexExportContactRow As FlexExportContactRow In _agentRow.GetFlexExportContactRows()
                    If flexExportContactRow.FxcFlxId = listItem.Value Then
                        listItem.Selected = True
                        Exit For
                    End If
                Next
            Next
            flexExportTypeCheckBoxList.Enabled = CanMaintain
            flexExportTypePanel.Visible = CanMaintain

            notesTabPanel.Enabled = True
            productTabPanel.Enabled = True
            packageTabPanel.Enabled = True

            updateButton.Visible = CanMaintain
            createButton.Visible = False

            ' Phoenix Change
            If _agentRow.IsAgnMaxDepositAmountNull Then
                txtMaxDepositAmount.Text = String.Empty
            Else
                txtMaxDepositAmount.Text = _agentRow.AgnMaxDepositAmount.ToString("f2")
            End If
            ' Phoenix Change

            ' b2b change
            If _agentRow Is Nothing OrElse (_agentDataSet IsNot Nothing AndAlso _agentDataSet.B2BUserInfo.Rows.Count = 0) Then
                b2bTabPanel.Visible = False
            Else
                b2bTabPanel.Visible = True
            End If
            ' b2b change

            ''rev:mia Nov.7 2012 - part addition of Debtor Grouping
            DebtorGrouping1.AgentID = _agnId
        End If

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If _initException IsNot Nothing Then Return

        If Not Page.IsPostBack Then


            ' Display active tab when TabParam exists, eg notes tab
            If Not String.IsNullOrEmpty(Me.Request.QueryString(AgentUserControl.TabParam)) Then
                For i As Integer = 0 To tabs.Tabs.Count - 1
                    If tabs.Tabs(i).HeaderText = Me.Request.QueryString(AgentUserControl.TabParam) Then
                        tabs.ActiveTabIndex = i
                    End If
                Next
            End If

            updateButton.Enabled = Not (tabs.ActiveTab.ID = b2bTabPanel.ID)
            'b2bTabPanel.Attributes.Add("onclick", "document.getElementById('" & updateButton.ID & "').disabled = true")
            tabs.OnClientActiveTabChanged = "ActiveTabChangedHandler"

            BuildAgent(False)

            If Not String.IsNullOrEmpty(Request.QueryString("Create")) Then
                Me.AddInformationMessage("Agent created")
            End If

            If Not String.IsNullOrEmpty(Request.QueryString("B2BExchangeRateUpdated")) Then
                Me.AddInformationMessage("Agent B2B exchange rates updated")
            End If

        End If

        If statusRadioButtonList.Items(0).Selected Then
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(AgentConstants.ActiveColor)
        Else
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(AgentConstants.InactiveColor)
        End If
    End Sub


    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        ''Response.Redirect(Me.BackUrl & IIf(Me.BackUrl.IndexOf("?") <> -1, "&", "?") & "agentId=" & _agnId & "&agentText=" & codeTextBox.Text & " - " & nameTextBox.Text)


        Dim url As String = ""
        ''rev:mia dec8
        If Me.BackUrl.ToUpper.Contains("BOOKINGPROCESS.ASPX") = False Then
            Response.Redirect(Me.BackUrl & IIf(Me.BackUrl.IndexOf("?") <> -1, "&", "?") & "agentId=" & _agnId & "&agentText=" & codeTextBox.Text & " - " & nameTextBox.Text)
        Else
            If Me.BackUrl.ToUpper.Contains("BOOKINGPROCESS.ASPX") = True Then
                url = "~/../web/booking/bookingprocess.aspx?agentId=" & Me.Server.UrlEncode(Request.QueryString("agnId")) & "&agentText=" & Request.QueryString("agentText")
            End If
        End If

        Response.Redirect(url)
    End Sub


    Private Sub ValidateAgent(ByVal create As Boolean)
        If create Then
            If codeTextBox.Text.Trim() = "" Then
                Throw New Aurora.Common.ValidationException("Code must be specified")
            End If

            Dim newAgentDataSet As New AgentDataSet
            DataRepository.GetAgentByCode(newAgentDataSet, codeTextBox.Text.Trim())
            If newAgentDataSet.Agent.Count > 0 Then
                Throw New Aurora.Common.ValidationException("Code must be unique")
            End If
        End If

        If nameTextBox.Text.Trim() = "" Then
            Throw New Aurora.Common.ValidationException("Name must be specified")
        End If

        If agentTypeDropDown.SelectedIndex = 0 Then
            Throw New Aurora.Common.ValidationException("Agent Type must be specified")
        End If

        If Not Me.groupSubgroupPicker.IsValid Then
            Throw New Aurora.Common.ValidationException(Me.groupSubgroupPicker.ErrorMessage)
        End If

        If marketCodeDropDown.SelectedIndex = 0 Then
            Throw New Aurora.Common.ValidationException("Market Code must be specified")
        End If

        Try
            If Not emailTextBox.IsTextValid Then
                Throw New Aurora.Common.ValidationException("A valid email address must be specified")
            End If
        Catch ex As Aurora.Common.ValidationException
            tabs.ActiveTabIndex = 0
            Throw ex
        End Try

        Try
            billingAddressDetailsControl.Validate(create)
            physicalAddressDetailsControl.Validate(create)
            postalAddressDetailsControl.Validate(create)
        Catch ex As Aurora.Common.ValidationException
            tabs.ActiveTabIndex = 1
            Throw ex
        End Try

        ' Phoenix Change
        If Not String.IsNullOrEmpty(txtMaxDepositAmount.Text.Trim()) Then
            ' somthing entered
            If Not IsNumeric(txtMaxDepositAmount.Text.Trim()) OrElse CDec(txtMaxDepositAmount.Text.Trim()) < 0 Then
                Throw New Aurora.Common.ValidationException("Max Discount Amount must be non-negative or empty")
            End If
        End If
        ' Phoenix Change
    End Sub


    Private Sub CreateUpdatePhoneNumber(ByVal code As String, _
     ByVal fieldName0 As String, ByVal text0 As String, _
     Optional ByVal fieldName1 As String = Nothing, Optional ByVal text1 As String = Nothing, _
     Optional ByVal fieldName2 As String = Nothing, Optional ByVal text2 As String = Nothing)

        Dim phoneNumberRow As PhoneNumberRow = _agentRow.PhoneNumberRow(code)
        If phoneNumberRow Is Nothing Then
            phoneNumberRow = _agentDataSet.PhoneNumber.NewPhoneNumberRow()
            phoneNumberRow.PhnID = DataRepository.GetNewId()
            phoneNumberRow.PhnPrntId = _agentRow.AgnId
            phoneNumberRow.PhnPrntTableName = "Agent"
            'phoneNumberRow.PhnCodPhoneId = _agentDataSet.Code.FindByCodCode(code).CodId
            phoneNumberRow.PhnCodPhoneId = _agentDataSet.Code.FindByCodCode(code, True).CodId

            phoneNumberRow(fieldName0) = text0
            If Not String.IsNullOrEmpty(fieldName1) Then phoneNumberRow(fieldName1) = text1
            If Not String.IsNullOrEmpty(fieldName2) Then phoneNumberRow(fieldName2) = text2

            _agentDataSet.PhoneNumber.AddPhoneNumberRow(phoneNumberRow)
            'Aurora.Common.Data.ExecuteDataRow(phoneNumberRow, Me.UserCode, Me.PrgmName)

        Else
            phoneNumberRow(fieldName0) = text0
            If Not String.IsNullOrEmpty(fieldName1) Then phoneNumberRow(fieldName1) = text1
            If Not String.IsNullOrEmpty(fieldName2) Then phoneNumberRow(fieldName2) = text2

            phoneNumberRow.PhnCodPhoneId = _agentDataSet.Code.FindByCodCode(code, True).CodId
            'Aurora.Common.Data.ExecuteDataRow(phoneNumberRow, Me.UserCode, Me.PrgmName)
        End If
        Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(phoneNumberRow, Me.UserCode, Me.PrgmName)
    End Sub


    Private Sub UpdateFlexExportTypes()

        ' handle creates or updates (ignored)
        For Each listItem As ListItem In flexExportTypeCheckBoxList.Items
            If Not listItem.Selected Then Continue For

            Dim flexExportContactRow As FlexExportContactRow = Nothing
            For Each flexExportContactRow0 As FlexExportContactRow In _agentRow.GetFlexExportContactRows()
                If flexExportContactRow0.FlexExportTypeRow.FlxId = listItem.Value _
                 AndAlso flexExportContactRow0.FlexExportTypeRow.FlxComCode = Me.CompanyCode Then
                    flexExportContactRow = flexExportContactRow0
                    Exit For
                End If
            Next
            If flexExportContactRow Is Nothing Then
                flexExportContactRow = _agentDataSet.FlexExportContact.NewFlexExportContactRow()
                flexExportContactRow.FxcFlxId = listItem.Value
                flexExportContactRow.FxcAgnId = _agentRow.AgnId
                _agentDataSet.FlexExportContact.AddFlexExportContactRow(flexExportContactRow)

                Aurora.Common.Data.ExecuteDataRow(flexExportContactRow, Me.UserCode, Me.PrgmName)
            End If
        Next

        ' handle deletes
        For Each flexExportContactRow As FlexExportContactRow In _agentRow.GetFlexExportContactRows()
            ' ignore flex export contact/types for other companies
            If flexExportContactRow.FlexExportTypeRow.FlxComCode <> Me.CompanyCode Then Continue For

            Dim listItem As ListItem = Nothing
            For Each listItem0 As ListItem In flexExportTypeCheckBoxList.Items
                If listItem0.Selected AndAlso flexExportContactRow.FlexExportTypeRow.FlxId = listItem0.Value Then
                    listItem = listItem0
                    Exit For
                End If
            Next
            If listItem Is Nothing Then
                flexExportContactRow.Delete()
                Aurora.Common.Data.ExecuteDataRow(flexExportContactRow)

                _agentDataSet.FlexExportContact.RemoveFlexExportContactRow(flexExportContactRow)
            End If
        Next
    End Sub

    Private Function CreateUpdateAgent(ByVal create As Boolean) As Boolean

        Try
            ValidateAgent(create)
        Catch ex As Aurora.Common.ValidationException
            Me.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return False
        End Try

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                If create Then
                    _agentRow = _agentDataSet.Agent.NewAgentRow()
                    _agentRow.AgnId = DataRepository.GetNewId()
                    _agentRow.AgnCode = codeTextBox.Text
                End If

                ' 16.2.9 - Shoel - Fix for the missing debtor status
                _agentRow.AgnCodDebtStatId = debtorStatusDropDown.SelectedValue
                ' 16.2.9 - Shoel - Fix for the missing debtor status

                ' Phoenix Change
                If (Not String.IsNullOrEmpty(txtMaxDepositAmount.Text.Trim())) _
                    AndAlso IsNumeric(txtMaxDepositAmount.Text.Trim()) _
                    AndAlso CDec(txtMaxDepositAmount.Text.Trim()) >= 0 _
                Then
                    _agentRow.AgnMaxDepositAmount = CDec(txtMaxDepositAmount.Text.Trim())
                Else
                    _agentRow.SetAgnMaxDepositAmountNull()
                End If
                ' Phoenix Change

                _agentRow.AgnName = nameTextBox.Text.Trim()
                _agentRow.AgnAtyId = agentTypeDropDown.SelectedValue
                _agentRow.AgnCodBlkCatId = categoryDropDown.SelectedValue
                _agentRow.AgnAsgId = groupSubgroupPicker.DataId
                _agentRow.AgnCodMktRgnId = marketCodeDropDown.SelectedValue
                For Each listItem As ListItem In flagsCheckBoxList.Items
                    If _agentRow.Table.Columns.Contains(listItem.Value) Then
                        _agentRow(listItem.Value) = listItem.Selected
                    End If
                Next
                _agentRow.AgnIsActive = statusRadioButtonList.Items(0).Selected

                If create Then
                    _agentDataSet.Agent.AddAgentRow(_agentRow)
                End If
                'Aurora.Common.Data.ExecuteDataRow(_agentRow, Me.UserCode, Me.PrgmName)
                Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(_agentRow, Me.UserCode, Me.PrgmName)

                CreateUpdatePhoneNumber("Day", _
                    "PhnCountryCode", Left(phoneCountryCodeTextBox.Text.Trim(), 6), _
                    "PhnAreaCode", Left(phoneAreaCodeTextBox.Text.Trim(), 6), _
                    "PhnNumber", Left(phoneNumberTextBox.Text.Trim(), 24))

                CreateUpdatePhoneNumber("Fax", _
                    "PhnCountryCode", Left(faxCountryCodeTextBox.Text.Trim(), 6), _
                    "PhnAreaCode", Left(faxAreaCodeTextBox.Text.Trim(), 6), _
                    "PhnNumber", Left(faxNumberTextBox.Text.Trim(), 24))

                CreateUpdatePhoneNumber("0800", "PhnNumber", Left(freePhoneNoTextBox.Text.Trim(), 24))
                CreateUpdatePhoneNumber("Email", "PhnEmailAddress", Left(emailTextBox.Text.Trim(), 64))
                CreateUpdatePhoneNumber("Web", "PhnEmailAddress", Left(websiteTextBox.Text.Trim(), 64))

                billingAddressDetailsControl.Update(_agentRow)
                physicalAddressDetailsControl.Update(_agentRow)
                postalAddressDetailsControl.Update(_agentRow)

                UpdateFlexExportTypes()

                _agentRow.UpdateAgentCompany()

                ''rev:mia Nov.7 2012 - part addition of Debtor Grouping
                Dim sb As String = DebtorGrouping1.GetDebtorsStatus(_agentRow.AgnId)
                Dim result As String = Aurora.Agent.Data.DebtorGroups.UpdateDebtorGroups(sb.ToString)
                If (result.Contains("ERROR") = True) Then
                    Throw New Exception(result)
                End If

                oTransaction.CommitTransaction()
            Catch ex As Exception
                oTransaction.RollbackTransaction()
                Dim description As String = "Error " & IIf(create, "creating", "updating") & " agent"
                Me.AddErrorMessage(description)
                Aurora.Common.Logging.LogError(Me.FunctionCode, description, ex)
                Return False
            End Try

            Return True

        End Using
    End Function


    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        If _agentRow IsNot Nothing OrElse Not CanMaintain Then Return

        If CreateUpdateAgent(True) Then
            Response.Redirect(AgentUserControl.MakeAgentUrl(Me.Page, agnId:=_agentRow.AgnId) & "&create=true")
        Else
            BuildAgent(True)
        End If
    End Sub


    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If _agentRow Is Nothing OrElse Not CanMaintain Then Return

        If CreateUpdateAgent(False) Then
            Me.AddInformationMessage("Agent updated")
        End If

        BuildAgent(True)
    End Sub


#Region "rev:mia Oct.6 2011 - Role to enable Debtor Status "
    Private Function CheckChargeDebtorRole(ByVal id As String) As String
        Dim sReturnMsg As String = "Error"
        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("dialog_checkUserForChargeDebtor", id, "CHARGEDEBTOR")
        Dim oXmlDoc As New System.Xml.XmlDocument
        oXmlDoc.LoadXml(result)
        If Not oXmlDoc.DocumentElement.SelectSingleNode("/Error") Is Nothing Then
            sReturnMsg = oXmlDoc.DocumentElement.SelectSingleNode("/Error").InnerText
        Else
            If Not oXmlDoc.DocumentElement.SelectSingleNode("/isUser") Is Nothing Then
                If CBool(oXmlDoc.DocumentElement.SelectSingleNode("/isUser").InnerText) Then
                    sReturnMsg = "True"
                End If
            End If
        End If
        Return sReturnMsg
    End Function

    Private Function EnableChargeDebtor(ByVal id As String) As Boolean
        Dim result As String = CheckChargeDebtorRole(id)

        If (result.Equals("Error")) Then Return False
        Return IIf(result.Equals("True"), True, False)
    End Function
#End Region
End Class
