﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DebtorGrouping.ascx.vb"
    Inherits="Agent_Controls_DebtorGrouping" %>
<asp:Repeater ID="repDebtorGrouping" runat="server">
    <HeaderTemplate>
        <table id="debtorGroupingable" class="dataTable" cellspacing="0" cellpadding="2"
            border="0" style="width: 620px; border-collapse: collapse;">
            <tr>
                
                <th style="width:400px;">
                    Description
                </th>
                <th style="width:200px;">
                    &nbsp;Status
                </th>
                <th></th>
                <th></th>
                <%--<th>
                    Months
                </th>
                <th>
                    Days
                </th>--%>
                
            </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr>
           
            <td>  <asp:Label runat="server" ID="labelDetGroupId"  Visible="false" />
                <%#Container.DataItem("DetGroupDesc")%>
            </td>
            <td>
                <asp:RadioButtonList runat="server" 
                                     ID="radioDebtorGroup" 
                                     RepeatDirection="Horizontal" 
                                     AutoPostBack="false" ToolTip="">
                    <asp:ListItem Value="None">None</asp:ListItem>
                    <asp:ListItem Value="Prepay">Prepay</asp:ListItem>
                    <asp:ListItem Value="Credit">Credit</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTriggerMonth" Width="40" Visible="false">
                    <asp:ListItem Value="0M">0</asp:ListItem>
                    <asp:ListItem Value="1M">1</asp:ListItem>
                    <asp:ListItem Value="2M">2</asp:ListItem>
                    <asp:ListItem Value="3M">3</asp:ListItem>
                    <asp:ListItem Value="4M">4</asp:ListItem>
                    <asp:ListItem Value="5M">5</asp:ListItem>
                </asp:DropDownList> 
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlTriggerDays" Width="40" Visible="false">
                    <asp:ListItem Value="0D">0</asp:ListItem>
                    <asp:ListItem Value="1D">1</asp:ListItem>
                    <asp:ListItem Value="2D">2</asp:ListItem>
                    <asp:ListItem Value="3D">3</asp:ListItem>
                    <asp:ListItem Value="4D">4</asp:ListItem>
                    <asp:ListItem Value="5D">5</asp:ListItem>
                    <asp:ListItem Value="6D">6</asp:ListItem>
                    <asp:ListItem Value="7D">7</asp:ListItem>
                    <asp:ListItem Value="8D">8</asp:ListItem>
                    <asp:ListItem Value="9D">9</asp:ListItem>
                    <asp:ListItem Value="10D">10</asp:ListItem>
                    <asp:ListItem Value="11D">11</asp:ListItem>
                    <asp:ListItem Value="12D">12</asp:ListItem>
                    <asp:ListItem Value="13D">13</asp:ListItem>
                    <asp:ListItem Value="14D">14</asp:ListItem>
                    <asp:ListItem Value="15D">15</asp:ListItem>
                    <asp:ListItem Value="16D">16</asp:ListItem>
                    <asp:ListItem Value="17D">17</asp:ListItem>
                    <asp:ListItem Value="18D">18</asp:ListItem>
                    <asp:ListItem Value="19D">19</asp:ListItem>
                    <asp:ListItem Value="20D">20</asp:ListItem>
                    <asp:ListItem Value="21D">21</asp:ListItem>
                    <asp:ListItem Value="22D">22</asp:ListItem>
                    <asp:ListItem Value="23D">23</asp:ListItem>
                    <asp:ListItem Value="24D">24</asp:ListItem>
                    <asp:ListItem Value="25D">25</asp:ListItem>
                    <asp:ListItem Value="26D">26</asp:ListItem>
                    <asp:ListItem Value="27D">27</asp:ListItem>
                    <asp:ListItem Value="28D">28</asp:ListItem>
                    <asp:ListItem Value="29D">29</asp:ListItem>
                    <asp:ListItem Value="30D">30</asp:ListItem>
                    <asp:ListItem Value="31D">31</asp:ListItem>
                </asp:DropDownList>

            </td>
        </tr>
    </ItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
