Imports System.Collections.Generic

Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

<ToolboxData("<{0}:PackageControl runat=server></{0}:PackageControl>")> _
    Partial Class Agent_PackageControl
    Inherits AgentUserControl

    Private _checkBoxesByApkId As New Dictionary(Of String, CheckBox)
    Private _loadException As Exception

    Public Overrides Sub InitAgentUserControl(ByVal agentDataSet As AgentDataSet, ByVal agentRow As AgentRow)

        MyBase.InitAgentUserControl(agentDataSet, agentRow)
    End Sub

    Private Sub BuildPackages()
        _checkBoxesByApkId.Clear()
        While historyResultTable.Rows.Count > 1
            historyResultTable.Rows.RemoveAt(1)
        End While

        If Me.AgentRow Is Nothing Then Return

        Dim count As Integer = 0
        Dim _now As Date = Date.Now.Date

        For Each agentPackageRow As AgentPackageRow In Me.AgentRow.GetAgentPackageRows()
            Dim tableRow As TableRow

            Dim activeAndCurrentFuture As Boolean '= agentPackageRow.IsActive _
            'AndAlso (agentPackageRow.IsCurrent(_now) OrElse agentPackageRow.IsFuture(_now))
            If agentPackageRow.ApkBookTo >= Now And agentPackageRow.BookedToDate >= Now Then
                activeAndCurrentFuture = True
            Else
                activeAndCurrentFuture = False
            End If
            ' AS PER OLD (and working) Stord Proc
            'CASE  
            '   WHEN AgentPackage.ApkBookTo >= GETDATE() AND PkgBookedtodate >= getdate() --RSM  
            '   THEN '0'  
            '   ELSE '1'  
            'END AS ISHistory,  

            If Not showHistoryCheckBox.Checked AndAlso Not activeAndCurrentFuture Then Continue For

            tableRow = New TableRow()
            tableRow.BackColor = agentPackageRow.StatusColor(_now)
            historyResultTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)
            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & agentPackageRow.ApkId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByApkId.Add(agentPackageRow.ApkId, checkBox)
            Else
                selectCell.Text = "&nbsp;"
            End If

            Dim productCell As New TableCell()
            If Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.PackageEnquiry) Then
                Dim productHyperLink As New HyperLink()
                productHyperLink.Text = Server.HtmlEncode(agentPackageRow.PackageRow.Description)
                productHyperLink.NavigateUrl = "../../Package/Package.aspx?pkgId=" & Server.UrlEncode(agentPackageRow.PackageRow.PkgId)
                productCell.Controls.Add(productHyperLink)
            Else
                productCell.Text = Server.HtmlEncode(agentPackageRow.PackageRow.Description)
            End If
            tableRow.Controls.Add(productCell)

            Dim brandCell As New TableCell()
            brandCell.Text = Server.HtmlEncode(agentPackageRow.PackageRow.BrandDescription) & "<br/>" _
             & Server.HtmlEncode(agentPackageRow.PackageRow.CountryDescription)
            tableRow.Controls.Add(brandCell)

            Dim periodCell As New TableCell()
            periodCell.Text = "<i>Book: </i>" & Server.HtmlEncode(agentPackageRow.PackageRow.BookedDescription) & "<br/>" _
             & "<i>Travel: </i>" & Server.HtmlEncode(agentPackageRow.PackageRow.TravelDescription)
            tableRow.Controls.Add(periodCell)

            Dim availableCell As New TableCell()
            availableCell.Text = Server.HtmlEncode(agentPackageRow.AvailableBookDescription)
            tableRow.Controls.Add(availableCell)

            Dim statusCell As New TableCell()
            statusCell.Text = Server.HtmlEncode(agentPackageRow.PackageRow.StatusDescription)
            tableRow.Controls.Add(statusCell)

            count += 1
        Next

        If count = 0 Then
            removeButton.Visible = False
            showHistoryResultLabel.Text = "No Agent Packages found"
        Else
            removeButton.Visible = CanMaintain
            showHistoryResultLabel.Text = count.ToString() & " Agent Package" & IIf(count > 1, "s", "") & " found"
        End If

        historyResultTable.Visible = True
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            removeButton.Visible = CanMaintain
            addButton.Visible = CanMaintain

            BuildPackages()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain OrElse Me.AgentRow Is Nothing Then Return

        Response.Redirect("../SalesAndMarketing/AgentBulkLoading.aspx" & _
            "?Option=PKG" & _
            "&AgnId=" & Server.UrlEncode(Me.AgentRow.AgnId) & _
            "&Text=" & Server.UrlEncode(Me.AgentRow.AgnCode & " - " & Me.AgentRow.AgnName) & _
            "&BackUrl=" & Server.UrlEncode(AgentUserControl.MakeAgentUrl(Me.CurrentPage, Me.AgentRow.AgnId, "Packages")))
    End Sub

    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click
        If Not CanMaintain Then Return

        For Each agentPackageRow As AgentPackageRow In Me.AgentRow.GetAgentPackageRows()
            Dim checkBox As CheckBox = Nothing
            If _checkBoxesByApkId.ContainsKey(agentPackageRow.ApkId) Then checkBox = _checkBoxesByApkId(agentPackageRow.ApkId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            ' 1.12.8 : Shoel : Incorrect logic fixed
            'agentPackageRow.Delete()
            agentPackageRow.ApkBookTo = Now
            agentPackageRow.ModDateTime = Now
            agentPackageRow.ModUsrId = CurrentPage.UserId
            ' 1.12.8 : Shoel : Incorrect logic fixed
            Aurora.Common.Data.ExecuteDataRow(agentPackageRow)
            'Me.AgentDataSet.AgentPackage.RemoveAgentPackageRow(agentPackageRow)
        Next
        CurrentPage.SetInformationShortMessage("Updated Successfully")
        BuildPackages()
    End Sub


    Private Sub ValidateUpdate()
        Dim anySelected As Boolean = False
        For Each checkBox As CheckBox In _checkBoxesByApkId.Values
            If checkBox.Checked Then
                anySelected = True
                Exit For
            End If
        Next
        If Not anySelected Then
            Throw New Aurora.Common.ValidationException("No agent packages selected")
        End If

        If Not availFromDateTextBox.IsValid Then
            Throw New Aurora.Common.ValidationException("A valid available for agent from date must be specified")
        End If
        If Not availToDateTextBox.IsValid Then
            Throw New Aurora.Common.ValidationException("A valid available to agent from date must be specified")
        End If

        If String.IsNullOrEmpty(availFromDateTextBox.Text.Trim) _
         AndAlso String.IsNullOrEmpty(availToDateTextBox.Text.Trim) Then
            Throw New Aurora.Common.ValidationException("At least one criteria to change on the agent package must be specified")
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateUpdate()
        Catch ex As Aurora.Common.ValidationException
            messagePanelControl.SetMessagePanelWarning(ex.Message)
            Return
        End Try

        For Each agentPackageRow As AgentPackageRow In Me.AgentRow.GetAgentPackageRows()
            Dim checkBox As CheckBox = Nothing
            If _checkBoxesByApkId.ContainsKey(agentPackageRow.ApkId) Then checkBox = _checkBoxesByApkId(agentPackageRow.ApkId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            If Not String.IsNullOrEmpty(availFromDateTextBox.Text.Trim) Then
                agentPackageRow.ApkBookFrom = availFromDateTextBox.Date
            End If

            If Not String.IsNullOrEmpty(availToDateTextBox.Text.Trim) Then
                agentPackageRow.ApkBookTo = availToDateTextBox.Date
            End If

            Aurora.Common.Data.ExecuteDataRow(agentPackageRow)
        Next

        BuildPackages()

        messagePanelControl.SetMessagePanelInformation("Agent packages updated")
    End Sub

End Class
