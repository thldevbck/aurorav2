﻿
Partial Class Agent_Controls_DebtorGrouping
    Inherits System.Web.UI.UserControl

    Private DS As System.Data.DataSet

    Protected Sub Agent_Controls_DebtorGrouping_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            LoadDebtors()
        End If

    End Sub

    Sub LoadDebtors()
        DS = New Data.DataSet
        DS = Aurora.Agent.Data.DebtorGroups.GetDebtorGroups(AgentID)

        repDebtorGrouping.DataSource = DS.Tables(0).DefaultView
        repDebtorGrouping.DataBind()


    End Sub

    Private _agentId As String
    Public Property AgentID As String
        Get
            Return _agentId
        End Get
        Set(value As String)
            _agentId = value
        End Set
    End Property

    Public Function GetDebtorsStatus(agentCode As String) As String
        Dim sb As New StringBuilder
        sb.AppendFormat("<Debtors agent=""{0}"">", agentCode.Trim)

        For Each item As RepeaterItem In repDebtorGrouping.Items
            Dim labelDetGroupId As Label = CType(item.FindControl("labelDetGroupId"), Label)
            Dim radioDebtorGroup As RadioButtonList = CType(item.FindControl("radioDebtorGroup"), RadioButtonList)

            Dim selectedStatus As String = "NIL"
            If (radioDebtorGroup.SelectedValue.ToUpper.Contains("PREPAY") = True Or radioDebtorGroup.SelectedValue.ToUpper.Contains("CREDIT") = True) Then
                selectedStatus = radioDebtorGroup.SelectedValue
            End If

            Dim ddlTriggerMonth As DropDownList = CType(item.FindControl("ddlTriggerMonth"), DropDownList)
            Dim monthTriggerValue As String = "0M"
            If (Not ddlTriggerMonth Is Nothing) Then
                If (ddlTriggerMonth.Visible = True) Then
                    monthTriggerValue = ddlTriggerMonth.SelectedValue
                End If

            End If

            Dim ddlTriggerDays As DropDownList = CType(item.FindControl("ddlTriggerDays"), DropDownList)
            Dim daysTriggerValue As String = "0D"
            If (Not ddlTriggerDays Is Nothing) Then
                If (ddlTriggerDays.Visible = True) Then
                    daysTriggerValue = ddlTriggerDays.SelectedValue
                End If
            End If


            If (Not labelDetGroupId Is Nothing) Then
                sb.AppendFormat("<Status groupId=""{0}""  oldStatus=""{1}"" triggerMM = ""{2}"" triggerDD=""{3}"">{4}</Status>", _
                                labelDetGroupId.Text, _
                                radioDebtorGroup.ToolTip, _
                                monthTriggerValue, _
                                daysTriggerValue, _
                                selectedStatus)
            End If
        Next
        sb.Append("</Debtors>")

        Return sb.ToString
    End Function

    Protected Sub repDebtorGrouping_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repDebtorGrouping.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then



            Dim labelDetGroupId As Label = CType(e.Item.FindControl("labelDetGroupId"), Label)
            If (Not labelDetGroupId Is Nothing) Then
                labelDetGroupId.Text = DataBinder.Eval(e.Item.DataItem, "DetGroupId")
            End If

            Dim radioDebtorGroup As RadioButtonList = CType(e.Item.FindControl("radioDebtorGroup"), RadioButtonList)
            radioDebtorGroup.Enabled = _EnabledDebtorGrouping

            If (Not radioDebtorGroup Is Nothing) Then
                Dim row As System.Data.DataRow() = DS.Tables(1).Select("AgnGrpDetStDetGroupId = " & CInt(labelDetGroupId.Text))
                If (Not row Is Nothing AndAlso row.Length - 1 <> -1) Then
                    Dim status As String = row(0).Item(1).ToString
                    radioDebtorGroup.SelectedValue = status
                    radioDebtorGroup.ToolTip = status

                    Dim ddlTriggerMonth As DropDownList = CType(e.Item.FindControl("ddlTriggerMonth"), DropDownList)
                    Dim monthTriggerValue As String = row(0).Item(2).ToString
                    If (Not ddlTriggerMonth Is Nothing) Then
                        If (ddlTriggerMonth.Visible = True) Then
                            ddlTriggerMonth.SelectedValue = monthTriggerValue
                        End If
                    End If

                    Dim ddlTriggerDays As DropDownList = CType(e.Item.FindControl("ddlTriggerDays"), DropDownList)
                    Dim daysTriggerValue As String = row(0).Item(3).ToString
                    If (Not ddlTriggerDays Is Nothing) Then
                        If (ddlTriggerDays.Visible = True) Then
                            ddlTriggerDays.SelectedValue = daysTriggerValue
                        End If
                    End If


                Else
                    radioDebtorGroup.ToolTip = "NIL"
                    radioDebtorGroup.SelectedIndex = 0
                End If

                

            End If



        End If
    End Sub

    Private _EnabledDebtorGrouping As Boolean
    Public Property EnabledDebtorGrouping As Boolean
        Get
            Return _EnabledDebtorGrouping
        End Get
        Set(value As Boolean)
            _EnabledDebtorGrouping = value
        End Set
    End Property

End Class
