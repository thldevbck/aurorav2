<%@ Control Language="VB" AutoEventWireup="false" CodeFile="B2BControl.ascx.vb" Inherits="Agent_Controls_B2BControl" %>
<%@ Register Src="~/UserControls/DefaultAndSelectionControl/DefaultAndSelectionUserControl.ascx"
    TagName="DefaultAndSelectionUserControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="ucDate" %>

<script type="text/javascript" language="javascript">

    function keyStrokeDecimal(evt)
    { 
        if (!window.event) return true; // give up, its too hard to test for control keys in FF (IE doesnt pass them)

        evt = window.event;

        return !((evt.keyCode < 48 || evt.keyCode > 58) && (evt.keyCode != 46))
    }


    function CheckContents(field,defaultValue)
    {
        if(isNaN(field.value) || parseFloat(field.value) <= 0.0)
        {
            if(defaultValue == null)
            {
                field.value="";
            }
            else
            {
                field.value=defaultValue;
            }                
        }
        else
        {
            if(isNaN(trimDecimal(parseFloat(field.value),6)) || trimDecimal(parseFloat(field.value),6) <= 0.0 || parseFloat(trimDecimal(field.value,6)) <= 0.0)
            {
                field.value = "";
            }
            else
            {
                field.value = trimDecimal(parseFloat(field.value),6);
            }
        }
    }


    function UpdateDefaults(CheckBoxListId, RadioButtonListId)
    {
        var chkGross = document.getElementById(CheckBoxListId + "_0" );
        var chkNett = document.getElementById(CheckBoxListId + "_1" );
        
        var tblRadioButtons = document.getElementById(RadioButtonListId);
         
        if(!chkGross.checked || !chkNett.checked)
        {
            // both not selected
            tblRadioButtons.style.display = "none";
        }
        else
        {
            // both selected
            tblRadioButtons.style.display = "block";
            document.getElementById(RadioButtonListId + "_0" ).checked = false;
            document.getElementById(RadioButtonListId + "_1" ).checked = false;
        }
        
    }
    
    function UpdatehdnCurrencyData(hdnCurrencyDataID,JavascriptIdList,CurrencyIdDetails)
    {
        var hdnCurrencyData = document.getElementById(hdnCurrencyDataID);
        var arrTextBoxes = JavascriptIdList.split(",");
        var arrCurrDetails = CurrencyIdDetails.split(",");

        // look for all blank rates        
        var bAllRatesAreBlank = true;
        for(var i=0;i<arrTextBoxes.length;i++)
        {
            var arrSubDetails0 = arrCurrDetails[i].split("|");
            if(!(document.getElementById(arrTextBoxes[i]) == null))
            {
                if( document.getElementById(arrTextBoxes[i]).value != "" )
                {
                    // something entered, so invalid
                    bAllRatesAreBlank = false;
                }
            }
        }
        
        if(bAllRatesAreBlank == true)
        {   
            hdnCurrencyData.value = "";
            return true;
        }
        
        for(var i=0;i<arrTextBoxes.length;i++)
        {
            var arrSubDetails = arrCurrDetails[i].split("|");
            if(!(document.getElementById(arrTextBoxes[i]) == null))
            {
                if( document.getElementById(arrTextBoxes[i]).value == "" || isNaN(parseFloat(document.getElementById(arrTextBoxes[i]).value)) || parseFloat(document.getElementById(arrTextBoxes[i]).value) <= 0.0)
                {
                    setWarningShortMessage("Please enter a valid exchange rate for " + arrSubDetails[2] + " to " + arrSubDetails[3]);
                    return false;
                }
                else
                {
                    hdnCurrencyData.value = hdnCurrencyData.value + arrCurrDetails[i] + "|" + document.getElementById(arrTextBoxes[i]).value + "," ;
                }
            }
        }
        return true;
    }

    function ChangeCurrencyRowVisibility()
    {
        var chkB2BCurrencyOptionEnabled = document.getElementById("<%=chkB2BCurrencyOptionEnabled.ClientID %>");
        var trCurrency1 = document.getElementById("<%=trCurrency1.ClientID %>");
        var trCurrency3 = document.getElementById("<%=trCurrency3.ClientID %>");
        var trCurrency4 = document.getElementById("<%=trCurrency4.ClientID %>");
        
        if(chkB2BCurrencyOptionEnabled.checked)
        {
            trCurrency1.style.display = "block";
            trCurrency3.style.display = "block";
            tblCurrencyExchangeData.style.display = "block";
            trCurrency4.style.display = "block";
        }
        else
        {
            trCurrency1.style.display = "none";
            trCurrency3.style.display = "none";
            tblCurrencyExchangeData.style.display = "none";
            trCurrency4.style.display = "none";
        }
    }
    
    function EnableDisableCurrencySelection(chkB2BHasCurrOpt)
    {
        //chkB2BHasCurrOpt.parentElement.parentElement.children[6].disabled = !chkB2BHasCurrOpt.checked;
         EnableDisableAllChildren(chkB2BHasCurrOpt.parentNode.parentNode.children[6],chkB2BHasCurrOpt.checked);
    }
    
    function EnableDisableAllChildren(Element,EnableDisable)
    {
        try
        {
            Element.disabled = !EnableDisable;
            if(Element.children.length>0)
            {
                for(var i = 0;i<Element.children.length;i++)
                {
                    EnableDisableAllChildren(Element.children[i],EnableDisable)
                }
            }
        }
        catch(err)
        {}
    }
    
    function HideShowGrossNett(chkB2BHasGrossNetOpt)
    {
        var sCurrentRowIdentifier = chkB2BHasGrossNetOpt.id.replace("chkB2BHasGrossNetOpt","");
        
        var sGrossNettCheckBoxTableId = sCurrentRowIdentifier + "cblB2BGrossNetWhatOpt";
        var sGrossNettRadioButtonTableId = sCurrentRowIdentifier + "rblB2BGrossNetDefOpt";
        
        var sGrossCheckBoxId = sGrossNettCheckBoxTableId + "_0";
        var sNettCheckBoxId = sGrossNettCheckBoxTableId + "_1";
        
        var sGrossRadioButtonId = sGrossNettRadioButtonTableId + "_0";
        var sNettRadioButtonId = sGrossNettRadioButtonTableId + "_1";
        
        var tblGrossNettCheckBoxTable = document.getElementById(sGrossNettCheckBoxTableId);
        var tblGrossNettRadioButtonTable = document.getElementById(sGrossNettRadioButtonTableId);
        
        var chkGross = document.getElementById(sGrossCheckBoxId);
        var chkNett = document.getElementById(sNettCheckBoxId);
        
        var radGross = document.getElementById(sGrossRadioButtonId);
        var radNett = document.getElementById(sNettRadioButtonId);
    
        if(chkB2BHasGrossNetOpt.checked)
        {
            tblGrossNettCheckBoxTable.style.display = "block";
        }
        else
        {
            tblGrossNettCheckBoxTable.style.display = "none";   
            tblGrossNettRadioButtonTable.style.display = "none";   
        }
        // clear selections
        chkGross.checked = false;
        chkNett.checked = false;
        radGross.checked = false;
        radNett.checked = false;
        
        chkGross.onclick = function(){UpdateDefaults(sGrossNettCheckBoxTableId, sGrossNettRadioButtonTableId)};
        chkNett.onclick = function(){UpdateDefaults(sGrossNettCheckBoxTableId, sGrossNettRadioButtonTableId)};
    }

</script>

<asp:UpdatePanel ID="updMain" runat="server">
    <ContentTemplate>
        <table style="border: none">
            <tr>
                <td>
                    <label>
                        <b>B2B General User Config:</b></label>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="grdUserInfo" runat="server" AutoGenerateColumns="False" CssClass="dataTableGrid"
                        Width="100%" DataKeyNames="UsrId">
                        <AlternatingRowStyle CssClass="oddRow" />
                        <RowStyle CssClass="evenRow" />
                        <Columns>
                            <asp:BoundField Visible="False" DataField="UsrId" HeaderText="UsrId" />
                            <asp:BoundField Visible="False" DataField="UsrCode" HeaderText="UsrCode" />
                            <asp:BoundField DataField="UsrName" HeaderText="User Name" Visible="False" />
                            <asp:HyperLinkField DataTextField="UsrEmail" DataNavigateUrlFields="UsrId" Target="_self"
                                DataNavigateUrlFormatString="~/Admin/UserEdit.aspx?FunCode=B2BAgent&amp;UsrId={0}"
                                HeaderText="Email" />
                            <asp:TemplateField HeaderText="Super User?">
                                <ItemStyle Width="30px" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkUsrIsAgentSuperUser" runat="server" Enabled="false" Checked='<% #(eval("UsrIsAgentSuperUser")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency Option?">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkB2BHasCurrOpt" runat="server" Checked='<% #(eval("B2BHasCurrOpt")) %>'
                                        onclick="EnableDisableCurrencySelection(this)" />
                                </ItemTemplate>
                                <ItemStyle Width="20px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gross Nett?">
                                <ItemStyle Width="30px" />
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkB2BHasGrossNetOpt" onclick="HideShowGrossNett(this);" runat="server" Checked='<% #(eval("B2BHasGrossNetOpt")) %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gross Nett Applicability">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnB2BGrossNetWhatOpt" runat="server" Value='<%# bind("B2BGrossNetWhatOpt") %>' />
                                    <asp:CheckBoxList CssClass="checkboxlist" ID="cblB2BGrossNetWhatOpt" runat="server"
                                        RepeatDirection="horizontal">
                                        <asp:ListItem Text="Gross" Value="Gross"></asp:ListItem>
                                        <asp:ListItem Text="Nett" Value="Nett"></asp:ListItem>
                                    </asp:CheckBoxList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Gross Nett Default">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnB2BGrossNetDefOpt" runat="server" Value='<%# bind("B2BGrossNetDefOpt") %>' />
                                    <asp:RadioButtonList CssClass="checkboxlist" ID="rblB2BGrossNetDefOpt" runat="server"
                                        RepeatDirection="horizontal">
                                        <asp:ListItem Text="Gross" Value="Gross"></asp:ListItem>
                                        <asp:ListItem Text="Nett" Value="Nett"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency Selection" ItemStyle-VerticalAlign="top">
                                <HeaderTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="3">
                                                Currency Selection
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="50px">
                                                Default
                                            </td>
                                            <td align="left" width="50px">
                                                Selected
                                            </td>
                                            <td align="left">
                                                Currency
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnB2BCurrencyCodeList" runat="server" Value='<%# bind("B2BCurrencyCodeList") %>' />
                                    <uc1:DefaultAndSelectionUserControl ID="objDefaultAndSelectionControl" runat="server"
                                        CollectionName="Currency" ShowHeader="false" />
                                    <asp:HiddenField ID="hdnB2BDefaultCurrencyCode" runat="server" Value='<%# bind("B2BDefaultCurrencyCode") %>' />
                                    <%--<asp:Literal ID="ltrScript" runat="server"></asp:Literal>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <label>
                        <b>B2B Currency Config:</b></label>
                    <%--Currency Stuff--%>
                    <table style="width: 100%;">
                        <tr>
                            <td width="150px">
                                Currency Option Enabled:
                            </td>
                            <td colspan="2">
                                <asp:CheckBox ID="chkB2BCurrencyOptionEnabled" runat="server" />
                            </td>
                        </tr>
                        <tr id="trCurrency1" runat="server">
                            <td>
                                Available Currencies:
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="cmbAvailableCurrencies" runat="server" AutoPostBack="true"
                                    Width="150px" />
                            </td>
                            <td>
                                Selected Currencies:
                            </td>
                            <td nowrap="nowrap" style="white-space: nowrap" rowspan="2">
                                <asp:CheckBoxList ID="cblSelectedCurrencies" runat="server" RepeatColumns="1" RepeatDirection="horizontal" />
                            </td>
                            <td>
                                <asp:Button ID="btnRemoveCurrencies" runat="server" Text="Remove" CssClass="Button_Small" />
                            </td>
                        </tr>
                        <%--<tr id="trCurrency2" runat="server">
                    <td style="border: none">
                        Default Currency:
                    </td>
                    <td style="border: none">
                    </td>
                </tr>--%>
                        <tr id="trCurrency3" runat="server">
                            <td style="border: none">
                                <br />
                            </td>
                        </tr>
                    </table>
                    <table class="dataTableGrid" style="width: 50%; border: 1" id="tblCurrencyExchangeData">
                        <tr id="trCurrency4" runat="server">
                            <th colspan="2">
                                Conversion Rate Effective from:
                            </th>
                            <th>
                                <%--<asp:UpdatePanel ID="updMain" runat="server">
                            <ContentTemplate>--%>
                                <asp:DropDownList ID="cmbEffectiveDate" runat="server" AutoPostBack="true" Width="157px" />
                                <%--</ContentTemplate>
                        </asp:UpdatePanel>--%>
                            </th>
                        </tr>
                        <asp:Literal ID="ltrCurrency" runat="server" />
                    </table>
                    <%--Currency Stuff--%>
                    <asp:HiddenField ID="hdnCurrencyData" runat="server" />
                    <asp:Literal ID="ltrScripts" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td align="right" colspan="2">
                    <asp:Button runat="server" ID="btnResetB2BConfigData" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    <asp:Button runat="server" ID="btnSaveB2BConfigData" style="width:150px" Text="Save B2B Config" CssClass="Button_Standard Button_Update" />
                </td>
            </tr>
        </table>
        <script language="javascript" type="text/javascript">
        function DisableUnchecked()
        {
            try
            {
                try
                {
                    var SaveButton = $get("ctl00_ContentPlaceHolder_updateButton");
                    SaveButton.disabled =true;
                    SaveButton.className += " buttondisabled";
                }
                catch(err){}
            
                try
                {
                    ChangeCurrencyRowVisibility();
                }
                catch(err)
                {}
            
                for (var i = 2; i < <% =(grdUserInfo.Rows.Count+2).ToString() %> ;i++)
                {
                    if(i<10)
                    {
                        var sCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_chkB2BHasCurrOpt";
                        var sTableId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_objDefaultAndSelectionControl_grdMain";
                        
                        var sGrossNettEnabledCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_chkB2BHasGrossNetOpt";
                        var sGrossCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_cblB2BGrossNetWhatOpt_0";
                        var sNettCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_cblB2BGrossNetWhatOpt_1";
                        var sGrossRadioButtonId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_rblB2BGrossNetDefOpt_0";
                        var sNettRadioButtonId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_rblB2BGrossNetDefOpt_1";
                        var sGrossNettTableId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_cblB2BGrossNetWhatOpt";
                        var sGRossNettDefaultTableId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl0" + i.toString() + "_rblB2BGrossNetDefOpt";
                    }
                    else
                    {
                        var sCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_chkB2BHasCurrOpt";
                        var sTableId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_objDefaultAndSelectionControl_grdMain";
                        
                        var sGrossNettEnabledCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_chkB2BHasGrossNetOpt";
                        var sGrossCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_cblB2BGrossNetWhatOpt_0";
                        var sNettCheckBoxId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_cblB2BGrossNetWhatOpt_1";
                        var sGrossRadioButtonId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_rblB2BGrossNetDefOpt_0";
                        var sNettRadioButtonId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_rblB2BGrossNetDefOpt_1";
                        var sGrossNettTableId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_cblB2BGrossNetWhatOpt";
                        var sGRossNettDefaultTableId = "ctl00_ContentPlaceHolder_tabs_b2bTabPanel_b2bControl_grdUserInfo_ctl" + i.toString() + "_rblB2BGrossNetDefOpt";
                    }
                    
                    
                    var chkEnabled = document.getElementById(sCheckBoxId);
                    var tblCurrency = document.getElementById(sTableId);
                    
                    var chkGrossNettEnabled = document.getElementById(sGrossNettEnabledCheckBoxId);
                    var chkGross = document.getElementById(sGrossCheckBoxId);
                    var chkNett = document.getElementById(sNettCheckBoxId);
                    var radGross = document.getElementById(sGrossRadioButtonId);
                    var radNett = document.getElementById(sNettRadioButtonId);
                    
                    var tblGrossNett = document.getElementById(sGrossNettTableId);
                    var tblGrossNEttDefault = document.getElementById(sGRossNettDefaultTableId);
                    
                    if(chkEnabled)
                    {
                        if(tblCurrency)
                        {
                            EnableDisableAllChildren(tblCurrency,chkEnabled.checked);
                        }                            
                    }
                    
                    if(chkGrossNettEnabled)
                    {
                        if(chkGrossNettEnabled.checked)
                        {
//                            chkGross.style.display = "block";
//                            chkNett.style.display = "block";
                            tblGrossNett.style.display = "block";
                            if(chkGross.checked && chkNett.checked)
                            {
                                tblGrossNEttDefault.style.display = "block";
                            }
                            else
                            {
                                tblGrossNEttDefault.style.display = "none";
                            }
                        }
                        else
                        {
                            //chkGross.style.display = "none";
                            //chkNett.style.display = "none"; 
                            tblGrossNett.style.display = "none";
                            
                            chkGross.checked = false;                           
                            chkNett.checked = false;
                            radGross.checked = false;
                            radNett.checked = false;
                            
                        }
                    }
                    if(chkGross.onclick)
                    {
                    }
                    else
                    {
                    //chkGross.onclick = function(){UpdateDefaults(sGrossNettTableId, sGRossNettDefaultTableId)};
                    chkGross.onclick = new Function("UpdateDefaults('" + sGrossNettTableId + "','" + sGRossNettDefaultTableId + "')")
                    }
                    if(chkNett.onclick)
                    {}
                    else
                    {
                    //chkNett.onclick = function(){UpdateDefaults(sGrossNettTableId, sGRossNettDefaultTableId)};
                    chkNett.onclick = new Function("UpdateDefaults('" + sGrossNettTableId + "','" + sGRossNettDefaultTableId + "')")
                    }
                    
                }
            }
            catch(err){}
        }
        
        DisableUnchecked();
        </script>
    </ContentTemplate>
</asp:UpdatePanel>

<script type="text/javascript">
    try
    {
        //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(DisableGridParts);
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(DisableUnchecked);
    }
    catch(err)
    {}
</script>

