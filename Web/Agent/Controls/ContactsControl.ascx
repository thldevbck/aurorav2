<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ContactsControl.ascx.vb" Inherits="Agent_ContactsControl" %>

<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<asp:UpdatePanel ID="contactsControlPanel" runat="server">
    <ContentTemplate>
    
        <table cellpadding="2" cellspacing="0" style="width:100%; border-top-width: 1px; margin-top: 5px">
            <tr>
                <td><b>Staff Contacts:</b></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="False" />
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Table ID="contactsTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTable" Width="100%">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell Width="20px">&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="80px">Type</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="175px">Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="225px">Details</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Comments</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </td>
            </tr>
        </table>
        
        <asp:Button runat="server" ID="contactsPopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="contactsPopup" 
            BehaviorID="contactsPopupBehavior"
            TargetControlID="contactsPopupButton" 
            PopupControlID="contactsPopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="contactsPopupDragPanel" />
            
        <asp:Panel runat="server" CssClass="modalPopup" ID="contactsPopupPanel" Style="display: none; width: 600px">
            <asp:Panel runat="Server" ID="contactsPopupDragPanel" CssClass="modalPopupTitle">
                <asp:Label ID="contactsPopupTitleLabel" runat="Server" Text="Contacts" />
            </asp:Panel>
            
            <asp:HiddenField ID="contactsPopupIdHidden" runat="server" />

            <div style="width: 475px; height:35px; padding: 2px 0 2px 0;">
                <uc1:MessagePanelControl runat="server" ID="contactsPopupMessagePanelControl" CssClass="popupMessagePanel" />
            </div>

            <table cellpadding="2" cellspacing="0" width="100%">
                 <tr>
                    <td style="width: 100px">Type:</td>
                    <td style="width: 200px">
                        <asp:DropDownList id="contactTypeDropDown" runat="server" Width="150px">
                            <asp:ListItem></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100px">Title:</td>
                    <td colspan="3"><asp:TextBox ID="titleTextBox" runat="server" Width="50px" /></td>
                </tr>                                   
                 <tr>
                    <td>First Name:</td>
                    <td><asp:TextBox ID="firstNameTextBox" runat="server" Width="150px" /></td>
                    <td>Surname:</td>
                    <td><asp:TextBox ID="lastNameTextBox" runat="server" Width="150px" /></td>
                </tr>  
                <tr>
                    <td>Position:</td>
                    <td colspan="3"><asp:TextBox ID="positionTextBox" runat="server" Width="150px" /></td>
                </tr>
                <tr>
                    <td style="border-top-width: 1px; border-top-style: dotted">Phone Number:</td>
                    <td style="border-top-width: 1px; border-top-style: dotted" colspan="3">
                        <asp:TextBox id="phoneCountryCodeTextBox" runat="server" Width="60"></asp:TextBox>&nbsp;
                        <asp:TextBox id="phoneAreaCodeTextBox" runat="server" Width="60"></asp:TextBox>&nbsp;
                        <asp:TextBox id="phoneNumberTextBox" runat="server" Width="150"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Fax Number:</td>
                    <td colspan="3">
                        <asp:TextBox id="faxCountryCodeTextBox" runat="server" Width="60"></asp:TextBox>&nbsp;
                        <asp:TextBox id="faxAreaCodeTextBox" runat="server" Width="60"></asp:TextBox>&nbsp;
                        <asp:TextBox id="faxNumberTextBox" runat="server" Width="150"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>Email:</td>
                    <td colspan="3">
                        <uc1:RegExTextBox 
                            ID="emailTextBox" 
                            runat="server" 
                            Nullable="true"
                            ErrorMessage="A valid email address must be specified"
                            RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" 
                            Width="300px" 
                            MessagePanelControlID="contactsPopupMessagePanelControl" />                
                    </td>
                </tr>  
                 <tr>
                    <td style="border-top-width: 1px; border-top-style: dotted" valign="top">Comment:</td>
                    <td style="border-top-width: 1px; border-top-style: dotted" colspan="3">
                        <asp:TextBox ID="commentTextBox" runat="server" TextMode="MultiLine" Rows="4" Width="475px"></asp:TextBox>
                    </td>
                </tr>  
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" colspan="4">
                        <asp:Button ID="contactsPopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="contactsPopupDeleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                        <asp:Button ID="contactsPopupUpdateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="contactsPopupCancelButton" runat="Server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
