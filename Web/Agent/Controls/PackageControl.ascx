<%@ Control Language="VB" AutoEventWireup="false" CodeFile="PackageControl.ascx.vb" Inherits="Agent_PackageControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\CollapsiblePanel\CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<table cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td>
            <i><asp:Label ID="showHistoryResultLabel" runat="server" /></i>&nbsp;
        </td>
        <td align="right">
            <asp:CheckBox id="showHistoryCheckBox" runat="server" Text="Show History" AutoPostBack="True" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="removeButton" style="width:150px" runat="Server" Text="End Booked Date " CssClass="Button_Standard Button_Remove" Visible="True" />
            <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="True" />
        </td>
    </tr>
</table>                 

<uc1:CollapsiblePanel 
    ID="updateCollapsiblePanel" 
    runat="server"
    Title="Update Package Selection" 
    TargetControlID="updatePanel" />
<asp:Panel ID="updatePanel" runat="Server">
    <div style="height:20px; padding: 2px 0 2px 0;">
        <uc1:MessagePanelControl runat="server" ID="messagePanelControl" CssClass="popupMessagePanel" />
    </div>

    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td>Available for Agent:</td>
            <td>
                From
                &nbsp;
                <uc1:DateControl 
                    ID="availFromDateTextBox" 
                    runat="server"
                    MessagePanelControlID="messagePanelControl" />
            </td>
            <td>&nbsp;</td>
            <td>
                To
                &nbsp;
                <uc1:DateControl 
                    ID="availToDateTextBox" 
                    runat="server"
                    MessagePanelControlID="messagePanelControl" />
            </td>
        </tr>
        <tr>
            <td colspan="3"><i>Note: updates are saved immediately.</i></td>
            <td align="right">
                <asp:Button ID="updateButton" runat="server" Text="Update" CssClass="Button_Standard Button_Update" />
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Table 
    id="historyResultTable" 
    runat="server" 
    CellPadding="2" 
    CellSpacing="0" 
    Visible="false" 
    width="100%" 
    CssClass="dataTableColor" 
    Style="margin-top: 10px">
    <asp:TableHeaderRow>
        <asp:TableHeaderCell Width="20px">
            <input type="checkbox" id="chkSelectAllPackages" onclick="return toggleTableColumnCheckboxes(0, event);" />        
        </asp:TableHeaderCell>
        <asp:TableHeaderCell>Package</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="125px">Brand / Country</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="200px">Package Period</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="150px">Avail for Agent</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="50px">Status</asp:TableHeaderCell>
    </asp:TableHeaderRow>
</asp:Table> 
