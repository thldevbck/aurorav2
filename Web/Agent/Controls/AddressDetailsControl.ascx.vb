Imports Aurora.Common
Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

<ToolboxData("<{0}:AddressDetailsControl runat=server></{0}:AddressDetailsControl>")> _
Partial Class Agent_AddressDetailsControl
    Inherits AgentUserControl

    Public _code As String
    Public Property Code() As String
        Get
            Return _code
        End Get
        Set(ByVal value As String)
            _code = value
        End Set
    End Property

    Public Overrides Sub InitAgentUserControl(ByVal agentDataSet As AgentDataSet, ByVal agentRow As AgentRow)

        MyBase.InitAgentUserControl(agentDataSet, agentRow)

        Me.countryDropDown.Items.Clear()
        Me.countryDropDown.Items.Add(New ListItem("(None)", ""))
        For Each countryRow As CountryRow In agentDataSet.Country
            Me.countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        Next

        address1Label.Visible = _code = "Billing"
        address3Label.Visible = _code = "Billing"
        countryLabel.Visible = _code = "Billing"
    End Sub


    Public ReadOnly Property IsBlank() As Boolean
        Get
            Return address1TextBox.Text.Trim() = "" _
             AndAlso address2TextBox.Text.Trim() = "" _
             AndAlso address3TextBox.Text.Trim() = "" _
             AndAlso stateTextBox.Text.Trim() = "" _
             AndAlso postCodeTextBox.Text.Trim() = "" _
             AndAlso countryDropDown.SelectedIndex = 0
        End Get
    End Property

    Public Sub Clear()
        address1TextBox.Text = ""
        address2TextBox.Text = ""
        address3TextBox.Text = ""
        stateTextBox.Text = ""
        postCodeTextBox.Text = ""
        countryDropDown.SelectedIndex = 0
    End Sub

    Public Sub Build()
        Me.Clear()
        Dim adr As AddressDetailsRow = Me.AgentRow.AddressDetailsRow(Code)
        If adr Is Nothing Then Return

        If Not adr.IsAddAddress1Null Then address1TextBox.Text = adr.AddAddress1
        If Not adr.IsAddAddress2Null Then address2TextBox.Text = adr.AddAddress2
        If Not adr.IsAddAddress3Null Then address3TextBox.Text = adr.AddAddress3
        If Not adr.IsAddStateNull Then stateTextBox.Text = adr.AddState
        If Not adr.IsAddPostcodeNull Then postCodeTextBox.Text = adr.AddPostcode
        If Not adr.IsAddCtyCodeNull Then countryDropDown.SelectedValue = adr.AddCtyCode
    End Sub

    Public Sub Validate(ByVal create As Boolean)

        If _code = "Billing" Then
            If String.IsNullOrEmpty(address1TextBox.Text.Trim()) _
             OrElse String.IsNullOrEmpty(address3TextBox.Text.Trim()) _
             OrElse String.IsNullOrEmpty(countryDropDown.SelectedValue.Trim()) Then
                Throw New ValidationException("Billing Address must be specified")
            End If
        End If

    End Sub

    Public Sub Update(ByVal agentRow As AgentRow)
        Dim adr As AddressDetailsRow = agentRow.AddressDetailsRow(Code)

        If adr Is Nothing Then
            adr = Me.AgentDataSet.AddressDetails.NewAddressDetailsRow()
            adr.AddId = Aurora.Common.Data.GetNewId()
            adr.AddPrntID = agentRow.AgnId
            adr.AddPrntTableName = "Agent"
            adr.AddCodAddressId = Me.AgentDataSet.Code.FindByCodCdtNumCodCode(DataConstants.CodeType_Address_Type, Code).CodId

            adr.AddAddress1 = address1TextBox.Text.Trim()
            adr.AddAddress2 = address2TextBox.Text.Trim()
            adr.AddAddress3 = address3TextBox.Text.Trim()
            adr.AddState = stateTextBox.Text.Trim()
            adr.AddPostcode = postCodeTextBox.Text.Trim()
            If countryDropDown.SelectedIndex <> 0 Then
                adr.AddCtyCode = countryDropDown.SelectedValue
            Else
                adr.SetAddCtyCodeNull()
            End If

            Me.AgentDataSet.AddressDetails.AddAddressDetailsRow(adr)
            Aurora.Common.Data.ExecuteDataRow(adr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        Else
            adr.AddAddress1 = address1TextBox.Text.Trim()
            adr.AddAddress2 = address2TextBox.Text.Trim()
            adr.AddAddress3 = address3TextBox.Text.Trim()
            adr.AddState = stateTextBox.Text.Trim()
            adr.AddPostcode = postCodeTextBox.Text.Trim()
            If countryDropDown.SelectedIndex <> 0 Then
                adr.AddCtyCode = countryDropDown.SelectedValue
            Else
                adr.SetAddCtyCodeNull()
            End If

            Aurora.Common.Data.ExecuteDataRow(adr, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        End If
    End Sub


End Class
