Imports System.Collections.Generic

Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

<ToolboxData("<{0}:ContactsControl runat=server></{0}:ContactsControl>")> _
Partial Class Agent_ContactsControl
    Inherits AgentUserControl

    Private _checkBoxesByConId As New Dictionary(Of String, CheckBox)
    Private _contactsByLinkButtonId As New Dictionary(Of String, ContactRow)

    Public Overrides Sub InitAgentUserControl(ByVal agentDataSet As Aurora.Agent.Data.AgentDataSet, ByVal agentRow As Aurora.Agent.Data.AgentDataSet.AgentRow)
        MyBase.InitAgentUserControl(agentDataSet, agentRow)

        contactTypeDropDown.Items.Clear()
        For Each codeRow As CodeRow In Me.AgentDataSet.Code
            If codeRow.CodCdtNum = codeRow.CodeType_Contact_Type Then _
                contactTypeDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
        Next
    End Sub


    Public Sub BuildContacts()
        _checkBoxesByConId.Clear()
        _contactsByLinkButtonId.Clear()
        While contactsTable.Rows.Count > 1
            contactsTable.Rows.RemoveAt(1)
        End While

        If Me.AgentRow Is Nothing Then Return

        Dim rowIndex As Integer = 0
        For Each contactRow As ContactRow In Me.AgentRow.GetContactRows()
            Dim tableRow As New TableRow
            tableRow.CssClass = IIf((rowIndex Mod 2) = 0, "evenRow", "oddRow")
            rowIndex += 1
            contactsTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Cells.Add(selectCell)
            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & contactRow.ConId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByConId.Add(contactRow.ConId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim typeCell As New TableCell
            tableRow.Controls.Add(typeCell)
            If CanMaintain Then
                Dim linkButton As New LinkButton
                linkButton.ID = "edit" & contactRow.ConId & "Button"
                linkButton.Text = contactRow.Type
                _contactsByLinkButtonId.Add(linkButton.ID, contactRow)
                AddHandler linkButton.Click, AddressOf editButton_Click
                typeCell.Controls.Add(linkButton)
            Else
                typeCell.Controls.Add(New LiteralControl(Server.HtmlEncode(contactRow.Type)))
            End If

            Dim nameCell As New TableCell()
            If Not String.IsNullOrEmpty(contactRow.FullName.Trim()) Then
                nameCell.Text = Server.HtmlEncode(contactRow.FullName)
            Else
                nameCell.Text = "&nbsp;"
            End If
            tableRow.Controls.Add(nameCell)

            Dim detailsCell As New TableCell
            Dim detailsCellList As New List(Of String)
            If Not String.IsNullOrEmpty(contactRow.PhoneNumber.Trim()) Then detailsCellList.Add("<i>Phone:</i> " & Server.HtmlEncode(contactRow.PhoneNumber))
            If Not String.IsNullOrEmpty(contactRow.FaxNumber.Trim()) Then detailsCellList.Add("<i>Fax:</i> " & Server.HtmlEncode(contactRow.FaxNumber))
            If Not String.IsNullOrEmpty(contactRow.Email.Trim()) Then detailsCellList.Add("<i>Email:</i> " & Server.HtmlEncode(contactRow.Email))
            If detailsCellList.Count > 0 Then
                detailsCell.Text = String.Join("<br/>", detailsCellList.ToArray)
            Else
                detailsCell.Text = "&nbsp;"
            End If
            tableRow.Controls.Add(detailsCell)

            Dim commentsCell As New TableCell
            If Not contactRow.IsConCommentsNull _
             AndAlso Not String.IsNullOrEmpty(contactRow.ConComments) Then
                commentsCell.Text = Server.HtmlEncode(contactRow.ConComments).Replace(vbLf, "<br/>")
            Else
                commentsCell.Text = "&nbsp;"
            End If
            tableRow.Controls.Add(commentsCell)
        Next

        addButton.Visible = CanMaintain
        deleteButton.Visible = CanMaintain
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            BuildContacts()
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        contactsPopupCancelButton.Attributes.Add("onclick", "$find('contactsPopupBehavior').hide(); return false;")
    End Sub


    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain Then Return

        contactsPopupIdHidden.Value = ""
        contactTypeDropDown.SelectedIndex = 0

        titleTextBox.Text = ""
        firstNameTextBox.Text = ""
        lastNameTextBox.Text = ""
        positionTextBox.Text = ""

        phoneCountryCodeTextBox.Text = ""
        phoneAreaCodeTextBox.Text = ""
        phoneNumberTextBox.Text = ""
        faxCountryCodeTextBox.Text = ""
        faxAreaCodeTextBox.Text = ""
        faxNumberTextBox.Text = ""
        emailTextBox.Text = ""

        commentTextBox.Text = ""

        contactsPopupAddButton.Visible = True
        contactsPopupDeleteButton.Visible = False
        contactsPopupUpdateButton.Visible = False
        contactsPopupTitleLabel.Text = "Add Contact"
        contactsPopup.Show()
    End Sub

    Private Sub DeleteContact(ByVal contactRow As ContactRow)
        For Each phoneNumberRow As PhoneNumberRow In contactRow.GetPhoneNumberRows()
            phoneNumberRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(phoneNumberRow)

            AgentDataSet.PhoneNumber.RemovePhoneNumberRow(phoneNumberRow)
        Next

        contactRow.Delete()
        Aurora.Common.Data.ExecuteDataRow(contactRow)

        AgentDataSet.Contact.RemoveContactRow(contactRow)
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If Not CanMaintain Then Return

        For Each contactRow As ContactRow In Me.AgentRow.GetContactRows()
            Dim checkBox As CheckBox = _checkBoxesByConId(contactRow.ConId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            DeleteContact(contactRow)
        Next

        Me.AgentRow.UpdateAgentCompany()

        BuildContacts()
    End Sub


    Protected Sub editButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim contactRow As ContactRow = _contactsByLinkButtonId(CType(sender, LinkButton).ID)
        If contactRow Is Nothing Then Return

        contactsPopupIdHidden.Value = contactRow.ConId
        Try
            contactTypeDropDown.SelectedValue = contactRow.ConCodContactId
        Catch
        End Try

        titleTextBox.Text = contactRow.ConTitle
        firstNameTextBox.Text = contactRow.ConFirstName
        lastNameTextBox.Text = contactRow.ConLastName
        positionTextBox.Text = contactRow.ConPosition

        phoneCountryCodeTextBox.Text = contactRow.PhoneNumberField("Day", "PhnCountryCode")
        phoneAreaCodeTextBox.Text = contactRow.PhoneNumberField("Day", "PhnAreaCode")
        phoneNumberTextBox.Text = contactRow.PhoneNumberField("Day", "PhnNumber")
        faxCountryCodeTextBox.Text = contactRow.PhoneNumberField("Fax", "PhnCountryCode")
        faxAreaCodeTextBox.Text = contactRow.PhoneNumberField("Fax", "PhnAreaCode")
        faxNumberTextBox.Text = contactRow.PhoneNumberField("Fax", "PhnNumber")
        emailTextBox.Text = contactRow.PhoneNumberField("Email", "PhnEmailAddress")
        commentTextBox.Text = contactRow.ConComments

        contactsPopupAddButton.Visible = False
        contactsPopupDeleteButton.Visible = True
        contactsPopupUpdateButton.Visible = True
        contactsPopupTitleLabel.Text = "Update Contact"
        contactsPopup.Show()
    End Sub


    Private ReadOnly Property IsBlank() As Boolean
        Get
            Return String.IsNullOrEmpty(titleTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(firstNameTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(lastNameTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(positionTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(phoneCountryCodeTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(phoneAreaCodeTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(phoneNumberTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(faxCountryCodeTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(faxAreaCodeTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(faxNumberTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(emailTextBox.Text.Trim()) _
             AndAlso String.IsNullOrEmpty(commentTextBox.Text.Trim())
        End Get
    End Property


    Private Sub ValidateContact(ByVal create As Boolean)
        If IsBlank Then
            Throw New Aurora.Common.ValidationException("Contact information must be specified")
        End If

        If Not emailTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException("A valid email address must be specified")
        End If

        If contactTypeDropDown.SelectedValue = AgentDataSet.Code.FindByCodCdtNumCodCode(CodeRow.CodeType_Contact_Type, "Flex").CodId _
         AndAlso String.IsNullOrEmpty(emailTextBox.Text.Trim()) Then
            Throw New Aurora.Common.ValidationException("A valid email address must be specified for a Flex contact type")
        End If
    End Sub


    Private Sub CreateUpdatePhoneNumber(ByVal contactRow As ContactRow, ByVal code As String, _
     ByVal fieldName0 As String, ByVal text0 As String, _
     Optional ByVal fieldName1 As String = Nothing, Optional ByVal text1 As String = Nothing, _
     Optional ByVal fieldName2 As String = Nothing, Optional ByVal text2 As String = Nothing)

        Dim phoneNumberRow As PhoneNumberRow = contactRow.PhoneNumberRow(code)
        If phoneNumberRow Is Nothing Then
            phoneNumberRow = Me.AgentDataSet.PhoneNumber.NewPhoneNumberRow()
            phoneNumberRow.PhnID = DataRepository.GetNewId()
            phoneNumberRow.PhnPrntId = contactRow.ConId
            phoneNumberRow.PhnPrntTableName = "Contact"
            phoneNumberRow.PhnCodPhoneId = Me.AgentDataSet.Code.FindByCodCode(code).CodId

            phoneNumberRow(fieldName0) = text0
            If Not String.IsNullOrEmpty(fieldName1) Then phoneNumberRow(fieldName1) = text1
            If Not String.IsNullOrEmpty(fieldName2) Then phoneNumberRow(fieldName2) = text2

            Me.AgentDataSet.PhoneNumber.AddPhoneNumberRow(phoneNumberRow)
            Aurora.Common.Data.ExecuteDataRow(phoneNumberRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        Else
            phoneNumberRow(fieldName0) = text0
            If Not String.IsNullOrEmpty(fieldName1) Then phoneNumberRow(fieldName1) = text1
            If Not String.IsNullOrEmpty(fieldName2) Then phoneNumberRow(fieldName2) = text2

            Aurora.Common.Data.ExecuteDataRow(phoneNumberRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        End If
    End Sub


    Protected Sub contactsPopupAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contactsPopupAddButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateContact(True)
        Catch ex As Aurora.Common.ValidationException
            Me.contactsPopupMessagePanelControl.SetMessagePanelWarning(Server.HtmlEncode(ex.Message))
            Me.contactsPopup.Show()
            Return
        End Try

        Try
            Dim contactRow As ContactRow = Me.AgentDataSet.Contact.NewContactRow()
            ''rev:mia dec3
            ''-------------------------------------------------------------
            Dim ictr As Integer = 0
            For Each crow As ContactRow In Me.AgentDataSet.Contact
                If crow.Type = "Flex" Then
                    ictr += 1
                End If
                If ictr > 2 Then Exit For
            Next
            If ictr > 2 Then
                MyBase.CurrentPage.SetWarningShortMessage("Only 3 Flex emai Ids can be added")
                Exit Sub
            End If
            ''-------------------------------------------------------------

            contactRow.ConId = DataRepository.GetNewId()
            contactRow.ConPrntTableName = "Agent"
            contactRow.ConPrntId = Me.AgentRow.AgnId
            contactRow.ConCodContactId = contactTypeDropDown.SelectedValue
            contactRow.ConTitle = Me.titleTextBox.Text
            contactRow.ConLastName = Me.lastNameTextBox.Text
            contactRow.ConFirstName = Me.firstNameTextBox.Text
            contactRow.ConPosition = Me.positionTextBox.Text
            contactRow.ConComments = Me.commentTextBox.Text

            Me.AgentDataSet.Contact.AddContactRow(contactRow)
            Aurora.Common.Data.ExecuteDataRow(contactRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

            CreateUpdatePhoneNumber(contactRow, "Day", _
                "PhnCountryCode", phoneCountryCodeTextBox.Text.Trim(), _
                "PhnAreaCode", phoneAreaCodeTextBox.Text.Trim(), _
                "PhnNumber", phoneNumberTextBox.Text.Trim())

            CreateUpdatePhoneNumber(contactRow, "Fax", _
                "PhnCountryCode", faxCountryCodeTextBox.Text.Trim(), _
                "PhnAreaCode", faxAreaCodeTextBox.Text.Trim(), _
                "PhnNumber", faxNumberTextBox.Text.Trim())

            CreateUpdatePhoneNumber(contactRow, "Email", "PhnEmailAddress", emailTextBox.Text.Trim())

            Me.AgentRow.UpdateAgentCompany()

        Catch ex As Exception
            Me.contactsPopupMessagePanelControl.SetMessagePanelError("Error creating contact")
            Me.contactsPopup.Show()
            Return
        End Try

        BuildContacts()
    End Sub


    Protected Sub contactsPopupUpdateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contactsPopupUpdateButton.Click
        If Not CanMaintain Then Return

        Dim contactRow As ContactRow = Me.AgentDataSet.Contact.FindByConId(contactsPopupIdHidden.Value)
        If contactRow Is Nothing Then Return

        Try
            ValidateContact(False)
        Catch ex As Aurora.Common.ValidationException
            Me.contactsPopupMessagePanelControl.SetMessagePanelWarning(Server.HtmlEncode(ex.Message))
            Me.contactsPopup.Show()
            Return
        End Try

        Try
            contactRow.ConCodContactId = contactTypeDropDown.SelectedValue
            contactRow.ConTitle = Me.titleTextBox.Text
            contactRow.ConLastName = Me.lastNameTextBox.Text
            contactRow.ConFirstName = Me.firstNameTextBox.Text
            contactRow.ConPosition = Me.positionTextBox.Text
            contactRow.ConComments = Me.commentTextBox.Text

            Aurora.Common.Data.ExecuteDataRow(contactRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

            CreateUpdatePhoneNumber(contactRow, "Day", _
                "PhnCountryCode", phoneCountryCodeTextBox.Text.Trim(), _
                "PhnAreaCode", phoneAreaCodeTextBox.Text.Trim(), _
                "PhnNumber", phoneNumberTextBox.Text.Trim())

            CreateUpdatePhoneNumber(contactRow, "Fax", _
                "PhnCountryCode", faxCountryCodeTextBox.Text.Trim(), _
                "PhnAreaCode", faxAreaCodeTextBox.Text.Trim(), _
                "PhnNumber", faxNumberTextBox.Text.Trim())

            CreateUpdatePhoneNumber(contactRow, "Email", "PhnEmailAddress", emailTextBox.Text.Trim())

            Me.AgentRow.UpdateAgentCompany()
        Catch ex As Exception
            Me.contactsPopupMessagePanelControl.SetMessagePanelError("Error updating contact")
            Me.contactsPopup.Show()
            Return
        End Try

        BuildContacts()
    End Sub


    Protected Sub contactsPopupDeleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contactsPopupDeleteButton.Click
        If Not CanMaintain Then Return

        Dim contactRow As ContactRow = Me.AgentDataSet.Contact.FindByConId(contactsPopupIdHidden.Value)
        If contactRow Is Nothing Then Return

        DeleteContact(contactRow)

        Me.AgentRow.UpdateAgentCompany()

        BuildContacts()
    End Sub


    Protected Sub contactsPopupCancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles contactsPopupCancelButton.Click
        contactsPopup.Hide()
    End Sub

End Class
