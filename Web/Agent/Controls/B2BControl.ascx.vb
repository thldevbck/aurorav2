Imports Aurora.Common
Imports Aurora.Agent
Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet
Imports System.Data

Partial Class Agent_Controls_B2BControl
    Inherits AgentUserControl

    Public Property B2BUserInfoData() As B2BUserInfoDataTable
        Get
            If ViewState("AGENT_B2BUserInfoData") Is Nothing Then
                Return Nothing
            Else
                Return CType(ViewState("AGENT_B2BUserInfoData"), B2BUserInfoDataTable)
            End If
        End Get
        Set(ByVal value As B2BUserInfoDataTable)
            ViewState("AGENT_B2BUserInfoData") = value
        End Set
    End Property

    Public Property B2BAgentExchangeRateData() As B2BAgentExchangeRateDataTable
        Get
            If ViewState("AGENT_B2BAgentExchangeRateData") Is Nothing Then
                Return Nothing
            Else
                Return CType(ViewState("AGENT_B2BAgentExchangeRateData"), B2BAgentExchangeRateDataTable)
            End If
        End Get
        Set(ByVal value As B2BAgentExchangeRateDataTable)
            ViewState("AGENT_B2BAgentExchangeRateData") = value
        End Set
    End Property

    Public Property B2BCurrencyConfigData() As B2BCurrencyConfigDataTable
        Get
            If ViewState("AGENT_B2BCurrencyConfigData") Is Nothing Then
                Return Nothing
            Else
                Return CType(ViewState("AGENT_B2BCurrencyConfigData"), B2BCurrencyConfigDataTable)
            End If
        End Get
        Set(ByVal value As B2BCurrencyConfigDataTable)
            ViewState("AGENT_B2BCurrencyConfigData") = value
        End Set
    End Property

    Protected Overloads ReadOnly Property CanMaintain() As Boolean
        Get
            Return CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.B2BUserSettingsMaintenance)
        End Get
    End Property

    Public Property AgentId() As String
        Get
            Return CStr(ViewState("Viewstate_B2B_AgentId"))
        End Get
        Set(ByVal value As String)
            ViewState("Viewstate_B2B_AgentId") = value
        End Set
    End Property

    Public Property DefaultCurrency() As String
        Get
            Return CStr(ViewState("AgentB2B_DefaultCurrency"))
        End Get
        Set(ByVal value As String)
            ViewState("AgentB2B_DefaultCurrency") = value
        End Set
    End Property

    Const DATE_FORMAT As String = "dd/MM/yyyy"
    Const TIME_FORMAT As String = "HH:mm"

    Public Overrides Sub InitAgentUserControl(ByVal agentDataSet As AgentDataSet, ByVal agentRow As AgentRow)
        If Not IsPostBack Then
            MyBase.InitAgentUserControl(agentDataSet, agentRow)

            ' save to Viewstate
            B2BUserInfoData = agentDataSet.B2BUserInfo
            B2BAgentExchangeRateData = agentDataSet.B2BAgentExchangeRate
            B2BCurrencyConfigData = agentDataSet.B2BCurrencyConfig

            grdUserInfo.DataSource = agentDataSet.B2BUserInfo
            grdUserInfo.DataBind()

            AgentId = agentDataSet.Agent.Rows(0)("AgnId").ToString()
            If CType(agentDataSet.Agent.Rows(0), AgentRow).IsAgnB2BCurrencyOptionEnabledNull Then
                chkB2BCurrencyOptionEnabled.Checked = False
                ShowHideCurrencyRows(False)
            Else
                chkB2BCurrencyOptionEnabled.Checked = CType(agentDataSet.Agent.Rows(0), AgentRow).AgnB2BCurrencyOptionEnabled
                ShowHideCurrencyRows(chkB2BCurrencyOptionEnabled.Checked)
            End If

            chkB2BCurrencyOptionEnabled.Attributes("onclick") = "ChangeCurrencyRowVisibility();"

            LoadCurrencyData(agentDataSet.B2BAgentExchangeRate, Nothing, False, agentDataSet.Currency, agentDataSet.B2BCurrencyConfig)

        End If
    End Sub

    Sub ShowHideCurrencyRows(ByVal ShowRows As Boolean)
        If ShowRows Then
            trCurrency1.Style("display") = "block"
            'trCurrency2.Style("display") = "none"
            trCurrency3.Style("display") = "block"
            trCurrency4.Style("display") = "block"
        Else
            trCurrency1.Style("display") = "none"
            'trCurrency2.Style("display") = "none"
            trCurrency3.Style("display") = "none"
            trCurrency4.Style("display") = "none"
        End If
    End Sub

    Public Const GROSS_LIST_ITEM_INDEX As Integer = 0
    Public Const NETT_LIST_ITEM_INDEX As Integer = 1

    Protected Sub grdUserInfo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdUserInfo.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim cblB2BGrossNetWhatOpt As CheckBoxList = e.Row.FindControl("cblB2BGrossNetWhatOpt")
            Dim hdnB2BGrossNetWhatOpt As HiddenField = e.Row.FindControl("hdnB2BGrossNetWhatOpt")
            cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Selected = hdnB2BGrossNetWhatOpt.Value.Contains("G")
            cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Selected = hdnB2BGrossNetWhatOpt.Value.Contains("N")

            Dim rblB2BGrossNetDefOpt As RadioButtonList = e.Row.FindControl("rblB2BGrossNetDefOpt")
            Dim hdnB2BGrossNetDefOpt As HiddenField = e.Row.FindControl("hdnB2BGrossNetDefOpt")
            rblB2BGrossNetDefOpt.Items(GROSS_LIST_ITEM_INDEX).Selected = hdnB2BGrossNetDefOpt.Value.Contains("G")
            rblB2BGrossNetDefOpt.Items(NETT_LIST_ITEM_INDEX).Selected = hdnB2BGrossNetDefOpt.Value.Contains("N")

            cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Attributes.Add("onclick", "UpdateDefaults('" & cblB2BGrossNetWhatOpt.ClientID & "','" & rblB2BGrossNetDefOpt.ClientID & "')")
            cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Attributes.Add("onclick", "UpdateDefaults('" & cblB2BGrossNetWhatOpt.ClientID & "','" & rblB2BGrossNetDefOpt.ClientID & "')")

            If cblB2BGrossNetWhatOpt.Items(GROSS_LIST_ITEM_INDEX).Selected _
               AndAlso _
               cblB2BGrossNetWhatOpt.Items(NETT_LIST_ITEM_INDEX).Selected _
            Then
                rblB2BGrossNetDefOpt.Style("display") = "block"
            Else
                rblB2BGrossNetDefOpt.Style("display") = "none"
            End If
        End If
    End Sub

    Protected Sub btnSaveB2BConfigData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveB2BConfigData.Click

        Dim datNow As DateTime = Now.Date.AddHours(Now.Hour).AddMinutes(Now.Minute)
        Dim bExitSub As Boolean = False
        Dim sbWarningMsg As New StringBuilder

        CurrentPage.ClearMessages()
        CurrentPage.ClearShortMessage()

        For j As Integer = 0 To B2BUserInfoData.Rows.Count - 1
            ' gross nett stuff
            Dim cblGrossNettOptions As CheckBoxList = CType(grdUserInfo.Rows(j).FindControl("cblB2BGrossNetWhatOpt"), CheckBoxList)
            Dim rdlGrossNettDefault As RadioButtonList = CType(grdUserInfo.Rows(j).FindControl("rblB2BGrossNetDefOpt"), RadioButtonList)

            cblGrossNettOptions.Items(GROSS_LIST_ITEM_INDEX).Attributes.Add("onclick", "UpdateDefaults('" & cblGrossNettOptions.ClientID & "','" & rdlGrossNettDefault.ClientID & "')")
            cblGrossNettOptions.Items(NETT_LIST_ITEM_INDEX).Attributes.Add("onclick", "UpdateDefaults('" & cblGrossNettOptions.ClientID & "','" & rdlGrossNettDefault.ClientID & "')")

            ' see if both gross and nett are enabled, and if no defaults are specified
            If cblGrossNettOptions.Items(GROSS_LIST_ITEM_INDEX).Selected _
               AndAlso _
               cblGrossNettOptions.Items(NETT_LIST_ITEM_INDEX).Selected _
            Then

                If Not rdlGrossNettDefault.Items(GROSS_LIST_ITEM_INDEX).Selected _
                   AndAlso _
                   Not rdlGrossNettDefault.Items(NETT_LIST_ITEM_INDEX).Selected _
                Then
                    ' select a default
                    'CurrentPage.SetWarningShortMessage("Please select the Nett or Gross default for user - " & CType(grdUserInfo.Rows(j).Cells(3).Controls(0), HyperLink).Text)
                    sbWarningMsg.Append("Please select the Nett or Gross default for user - " & CType(grdUserInfo.Rows(j).Cells(3).Controls(0), HyperLink).Text & "<br/>")
                    rdlGrossNettDefault.Style("display") = "block"
                    bExitSub = True
                End If
            Else
                ' only 1 or no options selected
                rdlGrossNettDefault.Style("display") = "none"
            End If

        Next

        If bExitSub Then
            CurrentPage.SetWarningMessage(sbWarningMsg.ToString())
            Exit Sub
        End If


        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try

                ' compare each row with original and save accordingly
                Dim i As Integer = 0
                Dim bNoChangesMade As Boolean = True
                Dim bExchangeRateChagesMade As Boolean = False

                For Each oRow As Aurora.Agent.Data.AgentDataSet.B2BUserInfoRow In B2BUserInfoData.Rows

                    ' Agent - User currency stuff
                    Dim hdnDefaultCurrency As HiddenField = CType(grdUserInfo.Rows(i).FindControl("hdnB2BDefaultCurrencyCode"), HiddenField)

                    Dim hdnSelectedCurrencies As HiddenField = CType(grdUserInfo.Rows(i).FindControl("hdnB2BCurrencyCodeList"), HiddenField)

                    Dim sDefaultCurrency As String = ""
                    Dim sSelectedCurrencies As String = ""

                    Dim objDefaultAndSelect As DefaultAndSelectionUserControl
                    objDefaultAndSelect = CType(grdUserInfo.Rows(i).FindControl("objDefaultAndSelectionControl"), DefaultAndSelectionUserControl)

                    If objDefaultAndSelect IsNot Nothing AndAlso objDefaultAndSelect.GetCurrentData.Rows.Count <> 0 Then
                        bNoChangesMade = Not objDefaultAndSelect.HasChanged
                        sDefaultCurrency = objDefaultAndSelect.GetDefault
                        sSelectedCurrencies = objDefaultAndSelect.GetCSVSelectedList
                    Else
                        If hdnDefaultCurrency.Value.Trim() <> "" OrElse hdnSelectedCurrencies.Value.Trim() <> "" Then
                            bNoChangesMade = False
                        End If
                        sDefaultCurrency = ""
                        sSelectedCurrencies = ""
                    End If

                    ' another level of checks- this handles weird cases where you remove a currency and the poor grid has no idea what happened
                    Dim sOldCSVList As String = UCase(Trim(hdnSelectedCurrencies.Value))

                    If sOldCSVList <> "" AndAlso sOldCSVList <> sSelectedCurrencies Then
                        For Each sCurrency As String In sOldCSVList.Split(","c)
                            If sSelectedCurrencies.Contains(sCurrency) Then
                                ' okay
                            Else
                                ' not okay
                                bNoChangesMade = False
                                Exit For
                            End If
                        Next

                        For Each sCurrency As String In sSelectedCurrencies.Split(","c)
                            If sOldCSVList.Contains(sCurrency) Then
                                ' okay
                            Else
                                ' not okay
                                bNoChangesMade = False
                                Exit For
                            End If
                        Next
                    End If

                    If UCase(Trim(hdnDefaultCurrency.Value)) <> sDefaultCurrency Then
                        bNoChangesMade = False
                    End If


                    ' Agent - User currency stuff

                    If CType(grdUserInfo.Rows(i).FindControl("chkB2BHasCurrOpt"), CheckBox).Checked = oRow.B2BHasCurrOpt _
                        AndAlso _
                        CType(grdUserInfo.Rows(i).FindControl("chkB2BHasGrossNetOpt"), CheckBox).Checked = oRow.B2BHasGrossNetOpt _
                        AndAlso _
                        CType(grdUserInfo.Rows(i).FindControl("cblB2BGrossNetWhatOpt"), CheckBoxList).Items(GROSS_LIST_ITEM_INDEX).Selected = oRow.B2BGrossNetWhatOpt.Contains("G") _
                        AndAlso _
                        CType(grdUserInfo.Rows(i).FindControl("cblB2BGrossNetWhatOpt"), CheckBoxList).Items(NETT_LIST_ITEM_INDEX).Selected = oRow.B2BGrossNetWhatOpt.Contains("N") _
                        AndAlso _
                        CType(grdUserInfo.Rows(i).FindControl("rblB2BGrossNetDefOpt"), RadioButtonList).Items(GROSS_LIST_ITEM_INDEX).Selected = oRow.B2BGrossNetDefOpt.Contains("G") _
                        AndAlso _
                        bNoChangesMade _
                    Then
                        ' there is no change
                    Else

                        bNoChangesMade = False
                        ' something changed, so update
                        oRow.B2BGrossNetWhatOpt = ""
                        If CType(grdUserInfo.Rows(i).FindControl("cblB2BGrossNetWhatOpt"), CheckBoxList).Items(GROSS_LIST_ITEM_INDEX).Selected Then
                            oRow.B2BGrossNetWhatOpt = "G"
                            If CType(grdUserInfo.Rows(i).FindControl("rblB2BGrossNetDefOpt"), RadioButtonList).Items(GROSS_LIST_ITEM_INDEX).Selected Then
                                oRow.B2BGrossNetDefOpt = "G"
                            End If
                        End If
                        If CType(grdUserInfo.Rows(i).FindControl("cblB2BGrossNetWhatOpt"), CheckBoxList).Items(NETT_LIST_ITEM_INDEX).Selected Then
                            If oRow.B2BGrossNetWhatOpt.Length > 0 Then
                                oRow.B2BGrossNetWhatOpt = oRow.B2BGrossNetWhatOpt & ",N"
                            Else
                                oRow.B2BGrossNetWhatOpt = "N"
                            End If
                            If CType(grdUserInfo.Rows(i).FindControl("rblB2BGrossNetDefOpt"), RadioButtonList).Items(NETT_LIST_ITEM_INDEX).Selected Then
                                oRow.B2BGrossNetDefOpt = "N"
                            End If

                        End If

                        oRow.B2BHasCurrOpt = CType(grdUserInfo.Rows(i).FindControl("chkB2BHasCurrOpt"), CheckBox).Checked



                        oRow.B2BHasGrossNetOpt = CType(grdUserInfo.Rows(i).FindControl("chkB2BHasGrossNetOpt"), CheckBox).Checked
                        oRow.B2BHasCusConfirmationOpt = oRow.B2BHasGrossNetOpt

                        oRow.B2BDefaultCurrencyCode = sDefaultCurrency
                        oRow.B2BCurrencyCodeList = sSelectedCurrencies

                        If oRow.B2BHasCurrOpt Then
                            If String.IsNullOrEmpty(oRow.B2BCurrencyCodeList.Trim()) Then
                                CurrentPage.SetWarningShortMessage("Please tick currencies available for this B2B user - " & CType(grdUserInfo.Rows(i).Cells(3).Controls(0), HyperLink).Text)
                                oTransaction.RollbackTransaction()
                                Return
                            End If
                            If String.IsNullOrEmpty(oRow.B2BDefaultCurrencyCode.Trim()) Then
                                CurrentPage.SetWarningShortMessage("Please determine the default currency for this B2B user - " & CType(grdUserInfo.Rows(i).Cells(3).Controls(0), HyperLink).Text)
                                oTransaction.RollbackTransaction()
                                Return
                            End If
                        End If

                        Dim ErrorMessage As String = ""

                        bNoChangesMade = True

                        If Not UpdateB2BUserInfo(oRow, AgentId, ErrorMessage) Then
                            Throw New Exception(ErrorMessage)
                        End If

                    End If
                    i += 1
                Next



                ' currency data

                ' currency biz

                ' ------------CONFIG------------------

                Dim sbCurrencyIdList As New StringBuilder
                For Each liCurrency As ListItem In cblSelectedCurrencies.Items
                    sbCurrencyIdList.Append(liCurrency.Value & ",")
                Next
                Dim sErrorMessage As String = ""

                If Not SaveB2BConfig(AgentId, chkB2BCurrencyOptionEnabled.Checked, sbCurrencyIdList.ToString(), sErrorMessage) Then
                    Throw New Exception(sErrorMessage)
                Else
                    bNoChangesMade = False
                End If

                ' ------------CONFIG------------------

                ' ------------XCHG------------------

                If cmbEffectiveDate.SelectedItem IsNot Nothing AndAlso cmbEffectiveDate.SelectedItem.Text = "Create New Rate" AndAlso Not String.IsNullOrEmpty(hdnCurrencyData.Value.Trim()) Then

                    bNoChangesMade = False

                    If cmbEffectiveDate.SelectedItem.Text = "Create New Rate" Then
                        bExchangeRateChagesMade = True
                    End If

                    Dim saCurrency As String()
                    saCurrency = hdnCurrencyData.Value.Remove(hdnCurrencyData.Value.Length - 1, 1).Split(","c)
                    For Each sCurrencyDetail As String In saCurrency
                        Dim saDetails As String()
                        saDetails = sCurrencyDetail.Split("|"c)

                        If SaveB2BExchangeRates("", AgentId, saDetails(0), saDetails(1), datNow, CDbl(saDetails(4)), sErrorMessage) Then
                            ' success
                        Else
                            ' failed
                            Throw New Exception(sErrorMessage)
                        End If
                    Next
                End If

                ' ------------XCHG------------------


                ' currency biz


                oTransaction.CommitTransaction()
                oTransaction.Dispose()

                If bNoChangesMade Then
                    CurrentPage.SetInformationShortMessage("No changes to save")
                    Exit Sub
                End If

                If bExchangeRateChagesMade Then
                    ' must reload page
                    Response.Redirect("Maintenance.aspx?agnId=" & AgentId & "&tab=B2B&B2BExchangeRateUpdated=true")
                    Exit Sub
                End If

                BindGrid()
                CurrentPage.SetInformationMessage("B2B details updated successfully")
            Catch ex As Exception

                oTransaction.RollbackTransaction()
                oTransaction.Dispose()

                CurrentPage.SetErrorMessage(ex.Message)
            End Try
        End Using

    End Sub

    Protected Sub btnResetB2BConfigData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetB2BConfigData.Click

        Dim saQueryString As New NameValueCollection
        Dim bTabParamFound As Boolean = False

        ' copy the collection, look for the tab param in the copied collection and store its value as B2B if it exists
        For Each sKey As String In Request.QueryString
            If UCase(Trim(sKey)) = "TAB" Then
                saQueryString.Add(sKey, "B2B")
                bTabParamFound = True
            Else
                saQueryString.Add(sKey, Request(sKey))
            End If
        Next

        If Not bTabParamFound Then
            saQueryString.Add("tab", "B2B")
        End If

        Dim sbNewURL As New StringBuilder
        sbNewURL.Append(Request.Path)
        sbNewURL.Append("?")
        For Each sQSParam As String In saQueryString.AllKeys
            sbNewURL.Append(sQSParam)
            sbNewURL.Append("=")
            sbNewURL.Append(saQueryString(sQSParam))
            sbNewURL.Append("&")
        Next

        Response.Redirect(sbNewURL.ToString())

    End Sub

    Function UpdateB2BUserInfo(ByVal B2BUserInfo As Aurora.Agent.Data.AgentDataSet.B2BUserInfoRow, ByVal AgentId As String, ByRef ErrorMessage As String) As Boolean
        Dim ProgramName As String = CurrentPage.AppRelativeVirtualPath + " - " + NamingContainer.ID

        Return Aurora.Agent.Data.DataRepository.InsertUpdateB2BConfig(B2BUserInfo, AgentId, CurrentPage.UserCode, ProgramName, True, ErrorMessage)

    End Function

    Function SaveB2BExchangeRates(ByVal DefaultCurrencyId As String, ByVal AgentId As String, ByVal FromCurrencyId As String, _
                                  ByVal ToCurrencyId As String, ByVal EffectiveDate As DateTime, _
                                  ByVal ExchangeRate As Double, ByRef ErrorMessage As String) As Boolean
        Dim ProgramName As String = CurrentPage.AppRelativeVirtualPath + " - " + NamingContainer.ID
        Return Aurora.Agent.Data.DataRepository.SaveB2BExchangeRates(DefaultCurrencyId, AgentId, FromCurrencyId, ToCurrencyId, EffectiveDate, ExchangeRate, ProgramName, CurrentPage.UserCode, ErrorMessage)
    End Function

    Function SaveB2BConfig(ByVal AgentId As String, _
                           ByVal CurrencyOptionEnabled As Boolean, _
                           ByVal CurrencyIdList As String, _
                           ByRef ErrorMessage As String) As Boolean
        Dim ProgramName As String = CurrentPage.AppRelativeVirtualPath + " - " + NamingContainer.ID
        Return Aurora.Agent.Data.DataRepository.SaveB2BCurrencyConfig(AgentId, CurrencyOptionEnabled, CurrencyIdList, CurrentPage.UserCode, ProgramName, ErrorMessage)
    End Function

    Sub BindGrid()
        ' refresh grid
        grdUserInfo.DataSource = B2BUserInfoData
        grdUserInfo.DataBind()
        PopulateCurrencyListInGrid()
    End Sub

#Region "Currency stuff"

    Sub LoadCurrencyData(ByVal ExchangeRateData As B2BAgentExchangeRateDataTable, _
                         Optional ByVal EffectiveDate As DateTime = Nothing, _
                         Optional ByVal NewRatesMode As Boolean = False, _
                         Optional ByVal CurrencyData As AgentDataSet.CurrencyDataTable = Nothing, _
                         Optional ByVal CurrencyConfigData As AgentDataSet.B2BCurrencyConfigDataTable = Nothing)

        Dim datEffectiveDate As DateTime
        Dim alFromCurrencies As New ArrayList
        Dim alToCurrencies As New ArrayList
        Dim alEffectiveDates As New ArrayList
        Dim sbHTMLDisplay As New StringBuilder
        Dim sDefaultCurrency As String = ""

        Dim sbJavascriptIds As New StringBuilder
        Dim sbCurrencyIdsForJS As New StringBuilder

        Dim bDummyDataAdded As Boolean = False

        Dim sAUDCodeId As String = "61C1FBAA-06DA-497F-95AA-6958342AF2B8"
        Dim sNZDCodeId As String = "4F96F47F-EAAF-41ED-9F09-7686A6B6D4F4"

        Dim sAUDCurrDesc As String = "Aust Dollar"
        Dim sNZDCurrDesc As String = "New Zealand Dollar"

        Dim datNow As DateTime = Now.Date.AddHours(Now.Hour).AddMinutes(Now.Minute)

        If CurrencyData IsNot Nothing Then
            cmbAvailableCurrencies.DataSource = CurrencyData
            cmbAvailableCurrencies.DataValueField = "CurrencyID"
            cmbAvailableCurrencies.DataTextField = "CurrencyDesc"
            cmbAvailableCurrencies.DataBind()
            cmbAvailableCurrencies.Items.Insert(0, New ListItem("--Select--", ""))
        End If

        If CurrencyConfigData IsNot Nothing Then
            Dim dvSort As New DataView(CurrencyConfigData)
            dvSort.Sort = "B2BAgnCodCurrDesc"
            cblSelectedCurrencies.DataSource = dvSort.ToTable() ' CurrencyConfigData
            cblSelectedCurrencies.DataValueField = "B2BAgnCodCurrId"
            cblSelectedCurrencies.DataTextField = "B2BAgnCodCurrDesc"
            cblSelectedCurrencies.DataBind()

            PopulateCurrencyListInGrid()

        End If

        If ExchangeRateData Is Nothing OrElse ExchangeRateData.Rows.Count = 0 OrElse NewRatesMode Then

            If ExchangeRateData Is Nothing Then
                ExchangeRateData = New B2BAgentExchangeRateDataTable
            End If

            ' commented out as it causes the datatable to get erased 
            'If NewRatesMode Then
            'ExchangeRateData.Rows.Clear()
            'End If
            ' no data exists, so create dummy rows

            For Each oCurrencyConfig As ListItem In cblSelectedCurrencies.Items 'B2BCurrencyConfigRow In CurrencyConfigData.Rows
                ExchangeRateData.Rows.Add(sAUDCodeId, oCurrencyConfig.Value, "AUD", oCurrencyConfig.Text.Split("-"c)(0).Trim(), _
                                          datNow, 0, AgentId, sAUDCurrDesc, oCurrencyConfig.Text.Split("-"c)(1).Trim(), "", "")
                ExchangeRateData.Rows.Add(sNZDCodeId, oCurrencyConfig.Value, "NZD", oCurrencyConfig.Text.Split("-"c)(0).Trim(), _
                                          datNow, 0, AgentId, sNZDCurrDesc, oCurrencyConfig.Text.Split("-"c)(1).Trim(), "", "")
                bDummyDataAdded = True

            Next

            EffectiveDate = datNow

        End If


        ' read data part
        If ExchangeRateData IsNot Nothing Then
            If ExchangeRateData.Rows.Count > 0 Then

                Dim dvFilterEffDate As New DataView(ExchangeRateData, "", "B2BEffectiveDate DESC", DataViewRowState.CurrentRows)
                For Each oEffDateRow As DataRow In dvFilterEffDate.ToTable(True, New String() {"B2BEffectiveDate"}).Rows
                    If Not bDummyDataAdded Then
                        alEffectiveDates.Add(CDate(oEffDateRow("B2BEffectiveDate")).ToString(DATE_FORMAT & " " & TIME_FORMAT))
                    End If
                Next

                If CanMaintain Then
                    If Not NewRatesMode OrElse cmbEffectiveDate.Items.Count = 0 Then
                        alEffectiveDates.Insert(0, "Create New Rate")
                    End If
                End If

                If EffectiveDate = Nothing OrElse EffectiveDate = DateTime.MinValue OrElse cmbEffectiveDate.Items.Count = 0 Then
                    ' nothing passed to filter on
                    cmbEffectiveDate.DataSource = alEffectiveDates
                    cmbEffectiveDate.DataBind()

                    If CanMaintain AndAlso Not bDummyDataAdded Then
                        datEffectiveDate = alEffectiveDates(1)
                        cmbEffectiveDate.SelectedIndex = 1
                    Else
                        If bDummyDataAdded Then
                            datEffectiveDate = datNow
                            NewRatesMode = True
                        Else
                            datEffectiveDate = DateTime.Parse((alEffectiveDates(0)))
                        End If

                        cmbEffectiveDate.SelectedIndex = 0
                    End If

                Else
                    datEffectiveDate = EffectiveDate
                End If


                ' Get list of FROM Currencies (typically AUD, NZD)
                alFromCurrencies.Add(New String() {"AUD", sAUDCodeId, sAUDCurrDesc})
                alFromCurrencies.Add(New String() {"NZD", sNZDCodeId, sNZDCurrDesc})

                ' Get list of TO Currencies (typically GBP, EUR)
                Dim dvFilterToCurr As New DataView(ExchangeRateData, "B2BEffectiveDate='" & datEffectiveDate.ToString("dd-MMM-yyyy" & " " & TIME_FORMAT) & "'", "", DataViewRowState.CurrentRows)
                For Each oToCurrCodeRow As DataRow In dvFilterToCurr.ToTable(True, New String() {"B2BToCurrencyCode", "B2BToCodCurrId", "B2BToCurrencyDesc"}).Rows
                    alToCurrencies.Add(New String() {oToCurrCodeRow("B2BToCurrencyCode").ToString(), oToCurrCodeRow("B2BToCodCurrId").ToString(), oToCurrCodeRow("B2BToCurrencyDesc").ToString()})
                Next

                With sbHTMLDisplay
                    For Each arrCurrencyFrom As String() In alFromCurrencies
                        .Append("    <tr>")
                        .Append("        <th  colspan=""2"">")
                        .Append("        </th>")
                        .Append("        <th >")
                        .Append("            Country of Travel " & arrCurrencyFrom(0))
                        .Append("        </th>")
                        .Append("    </tr>")
                        .Append("    <tr>")
                        .Append("        <td  colspan=""2"">")
                        .Append("            Currency")
                        .Append("        </td>")
                        .Append("        <td >")
                        .Append("            Rate for $1 " & arrCurrencyFrom(0))
                        .Append("        </td>")
                        .Append("    </tr>")

                        Dim iCounter As Int16 = 0

                        For Each arrCurrencyTo As String() In alToCurrencies
                            .Append("    <tr class=""" & IIf(iCounter Mod 2 = 0, "evenRow", "oddRow").ToString() & """>")
                            .Append("        <td valign=""bottom"" align=""center"" >")
                            .Append("            <img src=""../Images/" & arrCurrencyTo(0) & ".png"" alt=""" & arrCurrencyTo(0) & """ style=""width: 40px; height: 30px"" />")
                            .Append("        </td>")
                            .Append("        <td >")
                            .Append(arrCurrencyTo(0) & " - " & arrCurrencyTo(2))
                            .Append("        </td>")
                            .Append("        <td >")
                            .Append("            <input type=""text"" class=""Textbox_Medium"" " _
                                                & IIf(NewRatesMode, "onkeypress=""return keyStrokeDecimal(event);"" onblur=""CheckContents(this,'');""", " READONLY tabindex=""-1"" ") _
                                                & "  id=""txt" & arrCurrencyFrom(0) & "to" & arrCurrencyTo(0) & """ ")
                            For Each oRow As B2BAgentExchangeRateRow In ExchangeRateData.Rows
                                If oRow.B2BFromCurrencyCode = arrCurrencyFrom(0) _
                                   AndAlso _
                                   oRow.B2BToCurrencyCode = arrCurrencyTo(0) _
                                   AndAlso _
                                   oRow.B2BEffectiveDate = datEffectiveDate _
                                Then
                                    ' read the rate
                                    .Append("value=""" & IIf(NewRatesMode, "", oRow.B2BExchRate.ToString("f6")) & """ maxlength=""12"" />")
                                    Exit For
                                End If
                            Next
                            .Append("        </td>")
                            .Append("    </tr>")
                            iCounter += 1

                            sbJavascriptIds.Append("txt" & arrCurrencyFrom(0) & "to" & arrCurrencyTo(0) & ",")
                            sbCurrencyIdsForJS.Append(arrCurrencyFrom(1) & "|" & arrCurrencyTo(1) & "|" & arrCurrencyFrom(0) & "|" & arrCurrencyTo(0) & ",")
                        Next

                    Next
                End With
            End If
        End If

        'If chkB2BCurrencyOptionEnabled.Checked Then
        ltrCurrency.Text = sbHTMLDisplay.ToString()
        'Else
        '    ltrCurrency.Text = String.Empty
        'End If


        If CanMaintain Then
            If sbJavascriptIds.Length > 0 Then
                sbJavascriptIds.Remove(sbJavascriptIds.Length - 1, 1)
                btnSaveB2BConfigData.Attributes.Remove("onclick")
                btnSaveB2BConfigData.Attributes.Add("onclick", "return UpdatehdnCurrencyData('" & hdnCurrencyData.ClientID & "','" & sbJavascriptIds.ToString() & "','" & sbCurrencyIdsForJS.ToString() & "');")
                hdnCurrencyData.Value = "" 'reset this
                btnSaveB2BConfigData.Enabled = True
            End If

            If sbCurrencyIdsForJS.Length > 0 Then
                sbCurrencyIdsForJS.Remove(sbCurrencyIdsForJS.Length - 1, 1)
            End If
        Else
            btnSaveB2BConfigData.Enabled = False
            grdUserInfo.Enabled = False
        End If

        EnableDisableCurrencyControls(CanMaintain)

    End Sub

    Sub EnableDisableCurrencyControls(ByVal Enable As Boolean)
        chkB2BCurrencyOptionEnabled.Enabled = Enable
        cmbAvailableCurrencies.Enabled = Enable
        cblSelectedCurrencies.Enabled = Enable
        btnRemoveCurrencies.Enabled = Enable
        cmbEffectiveDate.Enabled = Enable
    End Sub

    Protected Sub cmbEffectiveDate_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbEffectiveDate.SelectedIndexChanged
        If cmbEffectiveDate.SelectedValue = "Create New Rate" Then
            LoadCurrencyData(B2BAgentExchangeRateData, DateTime.Parse(cmbEffectiveDate.Items(1).Value), True, Nothing, Nothing)
        Else
            LoadCurrencyData(B2BAgentExchangeRateData, DateTime.Parse(cmbEffectiveDate.SelectedValue))
        End If

    End Sub

#End Region

    Sub EnableDisableDropDown(ByRef TargetControl As DropDownList, ByVal SwitchBit As Boolean)
        With TargetControl
            If Not SwitchBit Then
                .Attributes.Add("onclick", "this.blur();return false;")
                .Attributes.Add("onfocus", "this.blur();return false;")
                .Attributes.Add("onchange", "this.selectedIndex = " & .SelectedIndex & ";")
                For i As Int16 = 0 To .Items.Count - 1
                    If i <> .SelectedIndex Then
                        .Items(i).Enabled = False
                    End If
                Next
                .CssClass += " selectdisabled"
            Else
                If (.Attributes("onclick") IsNot Nothing) Then
                    .Attributes.Remove("onclick")
                End If
                If (.Attributes("onfocus") IsNot Nothing) Then
                    .Attributes.Remove("onfocus")
                End If
                If (.Attributes("onchange") IsNot Nothing) Then
                    .Attributes.Remove("onchange")
                End If

                For i As Int16 = 0 To .Items.Count - 1
                    .Items(i).Enabled = True
                Next
                If (.CssClass.Contains("selectdisabled")) Then
                    .CssClass = .CssClass.Replace("selectdisabled", "").Trim()
                End If
            End If
        End With
    End Sub

    Protected Sub cmbAvailableCurrencies_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAvailableCurrencies.SelectedIndexChanged
        ' remove from avail
        ' add to currently selected check box list
        ' add to default options list
        Try
            Dim liItem As ListItem = New ListItem(cmbAvailableCurrencies.SelectedItem.Text, cmbAvailableCurrencies.SelectedItem.Value)
            liItem.Selected = False
            If Not liItem.Text.ToUpper().Contains("SELECT") Then
                cblSelectedCurrencies.Items.Add(liItem)
                UpdateCurrencyListInGrid(liItem, False)
                cmbAvailableCurrencies.Items.RemoveAt(cmbAvailableCurrencies.SelectedIndex)
            End If
            ShowHideCurrencyRows(True)
            If (B2BAgentExchangeRateData Is Nothing _
               OrElse _
               B2BAgentExchangeRateData.Rows.Count = 0) _
               OrElse _
               cmbEffectiveDate.SelectedValue.ToUpper().Contains("CREATE") _
            Then
                LoadCurrencyData(Nothing, Now.Date, True, Nothing, Nothing)
            End If
        Catch ex As Exception
            Aurora.Common.Logging.LogException("B2BAGENT", ex)
            Aurora.Common.Logging.LogInformation("B2BAGENT", ex.StackTrace)
        End Try
    End Sub

    Protected Sub btnRemoveCurrencies_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveCurrencies.Click
        ' remove from check box list
        ' remove from default options list
        ' add to avail list

        For i As Int16 = cblSelectedCurrencies.Items.Count - 1 To 0 Step -1
            Dim liCurrency As ListItem = New ListItem(cblSelectedCurrencies.Items(i).Text, cblSelectedCurrencies.Items(i).Value)
            If cblSelectedCurrencies.Items(i).Selected Then
                cmbAvailableCurrencies.Items.Add(liCurrency)
                cblSelectedCurrencies.Items.RemoveAt(i)
                UpdateCurrencyListInGrid(liCurrency, True)
            End If
        Next
        ShowHideCurrencyRows(True)

        ' exchange rate
        If cmbEffectiveDate.SelectedValue.ToUpper().Contains("CREATE") Then
            LoadCurrencyData(Nothing, Now.Date, True, Nothing, Nothing)
        End If

    End Sub

    Sub UpdateCurrencyListInGrid(ByVal NewListItem As ListItem, ByVal RemoveFlag As Boolean)
        For Each rwUser As GridViewRow In grdUserInfo.Rows

            Dim cblTemp As CheckBoxList = CType(rwUser.FindControl("cblB2BCurrencyCodeList"), CheckBoxList)
            Dim cmbTemp As DropDownList = CType(rwUser.FindControl("cmbB2BCurrencyCodeList"), DropDownList)

            Dim objDefaultAndSelect As DefaultAndSelectionUserControl
            objDefaultAndSelect = CType(rwUser.FindControl("objDefaultAndSelectionControl"), DefaultAndSelectionUserControl)

            Dim dtDataTable As New DataTable
            dtDataTable = objDefaultAndSelect.DataSource

            Dim sCurrCode, sCurrName As String
            sCurrCode = NewListItem.Text.Split("-"c)(0).ToUpper().Trim()
            sCurrName = NewListItem.Text.Split("-"c)(1).ToUpper().Trim()

            If RemoveFlag Then
                For i As Integer = dtDataTable.Rows.Count - 1 To 0 Step -1
                    If dtDataTable.Rows(i)(0).ToString().Trim() = sCurrCode Then
                        dtDataTable.Rows.RemoveAt(i)
                        Exit For
                    End If
                Next
            Else
                dtDataTable.Rows.Add(sCurrCode, sCurrName, NewListItem.Text, False, False)
            End If

            objDefaultAndSelect.DataSource = dtDataTable
            objDefaultAndSelect.DataBind()

        Next
    End Sub

    Sub PopulateCurrencyListInGrid()
        For Each rwUSer As GridViewRow In grdUserInfo.Rows

            Dim hdnCurrencyCodeList As HiddenField = CType(rwUSer.FindControl("hdnB2BCurrencyCodeList"), HiddenField)
            Dim hdnDefault As HiddenField = CType(rwUSer.FindControl("hdnB2BDefaultCurrencyCode"), HiddenField)
            Dim chkB2BHasCurrOpt As CheckBox = CType(rwUSer.FindControl("chkB2BHasCurrOpt"), CheckBox)
            'Dim ltrScript As Literal = CType(rwUSer.FindControl("ltrScript"), Literal)

            'If Not chkB2BHasCurrOpt.Checked Then
            '    ltrScript.Text = "<script>document.getElementById('" & rwUSer.Cells(9).ClientID & "').disabled = true</script>"
            'Else
            '    ltrScript.Text = ""
            'End If

            ' reset everything
            Dim dtSelectionData As New DataTable
            dtSelectionData.Columns.Add("ItemCode")
            dtSelectionData.Columns.Add("ItemName")
            dtSelectionData.Columns.Add("ItemDesc")
            dtSelectionData.Columns.Add("Default")
            dtSelectionData.Columns.Add("Selected")

            For Each liListItem As ListItem In cblSelectedCurrencies.Items
                Dim sItemCode, sItemName As String
                Dim bDefault, bSelected As Boolean

                sItemCode = liListItem.Text.Split("-"c)(0).Trim()
                sItemName = liListItem.Text.Split("-"c)(1).Trim()

                bSelected = hdnCurrencyCodeList.Value.Contains(sItemCode)
                bDefault = sItemCode.Equals(hdnDefault.Value.ToUpper().Trim())

                dtSelectionData.Rows.Add(sItemCode, sItemName, sItemCode & "-" & sItemName, bDefault, bSelected)
            Next

            Dim objDefaultAndSelect As DefaultAndSelectionUserControl
            objDefaultAndSelect = CType(rwUSer.FindControl("objDefaultAndSelectionControl"), DefaultAndSelectionUserControl)

            objDefaultAndSelect.DataSource = dtSelectionData
            objDefaultAndSelect.DataBind()

        Next
        'GenerateScripts()
    End Sub

    

End Class
