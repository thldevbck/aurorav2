Imports System.Collections.Generic

Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

<ToolboxData("<{0}:ProductControl runat=server></{0}:ProductControl>")> _
    Partial Class Agent_ProductControl
    Inherits AgentUserControl

    Private _loadException As Exception
    Private _checkBoxesByAdsId As New Dictionary(Of String, CheckBox)

    Public Overrides Sub InitAgentUserControl(ByVal agentDataSet As AgentDataSet, ByVal agentRow As AgentRow)
        MyBase.InitAgentUserControl(agentDataSet, agentRow)

        brandDropDown.Items.Clear()
        brandDropDown.Items.Add(New ListItem(""))
        brandDropDown.Items.Add(New ListItem("All"))
        For Each brandRow As BrandRow In Me.AgentDataSet.Brand
            If brandRow.IsBrdIsGenericNull OrElse Not brandRow.BrdIsGeneric Then
                brandDropDown.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
            End If
        Next

        countryDropDown.Items.Clear()
        countryDropDown.Items.Add(New ListItem(""))
        countryDropDown.Items.Add(New ListItem("All"))
        For Each countryRow As CountryRow In Me.AgentDataSet.Country
            If Not countryRow.IsCtyHasProductsNull AndAlso countryRow.CtyHasProducts Then
                countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
            End If
        Next

        chargeAgentRadioButtonList.Items(0).Selected = True

        effectiveDateTextBox.Date = Date.Now
    End Sub

    Private Sub BuildProducts()
        _checkBoxesByAdsId.Clear()
        While historyResultTable.Rows.Count > 1
            historyResultTable.Rows.RemoveAt(1)
        End While

        If Me.AgentRow Is Nothing Then Return

        Dim count As Integer = 0
        Dim lastPrdId As String = ""
        Dim lastBrdCode As String = ""

        For Each agentDiscountRow As AgentDiscountRow In Me.AgentRow.GetAgentDiscountRows()
            Dim tableRow As TableRow
            Dim isHistory As Boolean = lastPrdId = agentDiscountRow.AdsPrdId _
             AndAlso lastBrdCode = agentDiscountRow.AdsBrdCode

            If isHistory AndAlso Not showHistoryCheckBox.Checked Then Continue For

            tableRow = New TableRow()
            tableRow.BackColor = IIf(isHistory, Aurora.Common.DataConstants.PastColor, agentDiscountRow.ProductRow.StatusColor)
            historyResultTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            tableRow.Controls.Add(selectCell)
            If CanMaintain AndAlso Not isHistory Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & agentDiscountRow.AdsID & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByAdsId.Add(agentDiscountRow.AdsID, checkBox)
                If agentDiscountRow.RowState <> Data.DataRowState.Unchanged Then
                    checkBox.Checked = True
                End If
            Else
                selectCell.Text = "&nbsp;"
            End If

            If isHistory Then
                Dim productCell As New TableCell()
                productCell.Text = "&nbsp;"
                tableRow.Controls.Add(productCell)

                Dim brandCell As New TableCell()
                brandCell.Text = "&nbsp;"
                tableRow.Controls.Add(brandCell)
            Else
                Dim productCell As New TableCell()
                If Me.CurrentPage.GetFunctionPermission(AuroraFunctionCodeAttribute.ProductEnquiry) Then
                    Dim productHyperLink As New HyperLink()
                    productHyperLink.Text = Server.HtmlEncode(agentDiscountRow.ProductRow.Description)
                    productHyperLink.NavigateUrl = "../../Product/Product.aspx?prdId=" & Server.UrlEncode(agentDiscountRow.ProductRow.PrdId)
                    productCell.Controls.Add(productHyperLink)
                Else
                    productCell.Text = Server.HtmlEncode(agentDiscountRow.ProductRow.Description)
                End If
                tableRow.Controls.Add(productCell)

                Dim brandCell As New TableCell()
                brandCell.Text = Server.HtmlEncode(agentDiscountRow.BrandDescription)
                tableRow.Controls.Add(brandCell)
            End If

            Dim countryOfTravelCell As New TableCell()
            countryOfTravelCell.Text = Server.HtmlEncode(agentDiscountRow.CountryDescription)
            tableRow.Controls.Add(countryOfTravelCell)

            Dim dateTypeCell As New TableCell()
            dateTypeCell.Text = Server.HtmlEncode(agentDiscountRow.BookedDateTypeDescription)
            tableRow.Controls.Add(dateTypeCell)

            ''rev:mia jan.5,2009 this is a fixed on issue Call : 22933 - Unable to Amend Agent Discuont to Zero
            Dim discountCell As New TableCell()
            ''discountCell.Text = Server.HtmlEncode(agentDiscountRow.PercentageDescription)
            discountCell.Text = Server.HtmlEncode(agentDiscountRow.PercentageDescriptionDiscounts)
            tableRow.Controls.Add(discountCell)

            Dim chargeAgentCell As New TableCell()
            chargeAgentCell.Text = Server.HtmlEncode(agentDiscountRow.ChgAgentDescription)
            tableRow.Controls.Add(chargeAgentCell)

            Dim effectiveDateCell As New TableCell()
            effectiveDateCell.Text = Server.HtmlEncode(agentDiscountRow.EffectiveDateDescription)
            tableRow.Controls.Add(effectiveDateCell)

            lastPrdId = agentDiscountRow.AdsPrdId
            lastBrdCode = agentDiscountRow.AdsBrdCode

            count += 1
        Next

        If count = 0 Then
            removeButton.Visible = False
            showHistoryResultLabel.Text = "No Agent Product Discounts found"
        Else
            removeButton.Visible = CanMaintain
            showHistoryResultLabel.Text = count.ToString() & " Agent Product Discount" & IIf(count > 1, "s", "") & " found"
        End If

        historyResultTable.Visible = True
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            removeButton.Visible = CanMaintain
            addButton.Visible = CanMaintain

            BuildProducts()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain OrElse Me.AgentRow Is Nothing Then Return

        Response.Redirect("../SalesAndMarketing/AgentBulkLoading.aspx" & _
            "?Option=PRD" & _
            "&AgnId=" & Server.UrlEncode(Me.AgentRow.AgnId) & _
            "&Text=" & Server.UrlEncode(Me.AgentRow.AgnCode & " - " & Me.AgentRow.AgnName) & _
            "&BackUrl=" & Server.UrlEncode(AgentUserControl.MakeAgentUrl(Me.CurrentPage, Me.AgentRow.AgnId, "Product Discounts")))
    End Sub

    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click
        If Not CanMaintain Then Return

        Dim lastPrdId As String = ""
        Dim lastBrdCode As String = ""
        Dim lastCheckBox As CheckBox = Nothing

        For Each agentDiscountRow As AgentDiscountRow In Me.AgentRow.GetAgentDiscountRows()
            Dim isHistory As Boolean = lastPrdId = agentDiscountRow.AdsPrdId _
             AndAlso lastBrdCode = agentDiscountRow.AdsBrdCode

            Dim checkBox As CheckBox = Nothing
            If Not isHistory Then
                If _checkBoxesByAdsId.ContainsKey(agentDiscountRow.AdsID) Then checkBox = _checkBoxesByAdsId(agentDiscountRow.AdsID)
            Else
                checkBox = lastCheckBox
            End If

            lastPrdId = agentDiscountRow.AdsPrdId
            lastBrdCode = agentDiscountRow.AdsBrdCode
            lastCheckBox = checkBox

            If checkBox IsNot Nothing AndAlso checkBox.Checked Then
                agentDiscountRow.Delete()
                Aurora.Common.Data.ExecuteDataRow(agentDiscountRow)
                Me.AgentDataSet.AgentDiscount.RemoveAgentDiscountRow(agentDiscountRow)
            End If
        Next

        BuildProducts()
    End Sub

    Private Sub ValidateUpdate()
        Dim anySelected As Boolean = False
        For Each checkBox As CheckBox In _checkBoxesByAdsId.Values
            If checkBox.Checked Then
                anySelected = True
                Exit For
            End If
        Next
        If Not anySelected Then
            Throw New Aurora.Common.ValidationException("No agent product discounts selected")
        End If

        If Not String.IsNullOrEmpty(brandDropDown.SelectedValue) Then
            For Each agentDiscountRow As AgentDiscountRow In Me.AgentRow.GetAgentDiscountRows()
                Dim checkBox As CheckBox = Nothing
                If _checkBoxesByAdsId.ContainsKey(agentDiscountRow.AdsID) Then checkBox = _checkBoxesByAdsId(agentDiscountRow.AdsID)
                If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

                If agentDiscountRow.ProductRow IsNot Nothing _
                 AndAlso agentDiscountRow.ProductRow.BrandRow IsNot Nothing _
                 AndAlso Not agentDiscountRow.ProductRow.BrandRow.IsBrdIsGenericNull _
                 AndAlso Not agentDiscountRow.ProductRow.BrandRow.BrdIsGeneric Then
                    Throw New Aurora.Common.ValidationException("Brand may not be changed on Agent discount products for products that have non-generic brands")
                End If
            Next
        End If

        If Not discountTextBox.IsTextValid Then
            Throw New Aurora.Common.ValidationException(discountTextBox.ErrorMessage)
        End If

        If Not effectiveDateTextBox.IsValid Then
            Throw New Aurora.Common.ValidationException("A valid effective date must be specified")
        End If

        If String.IsNullOrEmpty(brandDropDown.SelectedValue) _
         AndAlso String.IsNullOrEmpty(countryDropDown.SelectedValue) _
         AndAlso String.IsNullOrEmpty(dateTypeDropDow.SelectedValue) _
         AndAlso String.IsNullOrEmpty(discountTextBox.Text.Trim) _
         AndAlso String.IsNullOrEmpty(chargeAgentRadioButtonList.SelectedValue) _
         AndAlso (effectiveDateTextBox.IsValid And effectiveDateTextBox.Text.Trim().Equals(String.Empty)) Then
            Throw New Aurora.Common.ValidationException("At least one criteria to change on the agent product discount must be specified")
        End If
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        If Not CanMaintain Then Return

        Try
            ValidateUpdate()
        Catch ex As Aurora.Common.ValidationException
            messagePanelControl.SetMessagePanelWarning(ex.Message)
            Return
        End Try

        For Each agentDiscountRow As AgentDiscountRow In Me.AgentRow.GetAgentDiscountRows()
            Dim checkBox As CheckBox = Nothing
            If _checkBoxesByAdsId.ContainsKey(agentDiscountRow.AdsID) Then checkBox = _checkBoxesByAdsId(agentDiscountRow.AdsID)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            If Not String.IsNullOrEmpty(brandDropDown.SelectedValue) Then
                If brandDropDown.SelectedValue = "All" Then
                    agentDiscountRow.AdsBrdCode = agentDiscountRow.ProductRow.BrandRow.BrdCode
                Else
                    agentDiscountRow.AdsBrdCode = brandDropDown.SelectedValue
                End If
            End If

            If Not String.IsNullOrEmpty(countryDropDown.SelectedValue) Then
                If countryDropDown.SelectedValue = "All" Then
                    agentDiscountRow.SetAdsCtyCodeNull()
                Else
                    agentDiscountRow.AdsCtyCode = countryDropDown.SelectedValue
                End If
            End If

            If Not String.IsNullOrEmpty(dateTypeDropDow.SelectedValue) Then
                agentDiscountRow.AdsIsBookedDateType = dateTypeDropDow.SelectedValue = "Booked"
            End If

            If Not String.IsNullOrEmpty(discountTextBox.Text.Trim) Then
                Dim discount As Decimal = 0
                Decimal.TryParse(discountTextBox.Text, discount)
                agentDiscountRow.AdsPercentage = discount
            Else
                agentDiscountRow.AdsPercentage = CDec(9999999999)
            End If

            If Not String.IsNullOrEmpty(chargeAgentRadioButtonList.SelectedValue) Then
                agentDiscountRow.AdsChgAgent = chargeAgentRadioButtonList.SelectedValue = "Yes"
            End If

            agentDiscountRow.AdsEffDate = effectiveDateTextBox.Date

            ''Aurora.Common.Data.ExecuteDataRow(agentDiscountRow)
            Aurora.Common.Data.ExecuteDataRowAgentMaintenance(agentDiscountRow)
        Next

        BuildProducts()

        messagePanelControl.SetMessagePanelInformation("Agent product discounts updated")
    End Sub
End Class
