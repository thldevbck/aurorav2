<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NoteControl.ascx.vb" Inherits="Agent_NoteControl" %>

<asp:UpdatePanel runat="server" ID="noteControlPanel">
    <ContentTemplate>

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="deleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="False" />
                    <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="False" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Table ID="notesTable" runat="server" CellPadding="2" CellSpacing="0" CssClass="dataTableColor" Width="100%">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell Width="20px">&nbsp;</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="80px">Type</asp:TableHeaderCell>
                            <asp:TableHeaderCell>Notes</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="150px">Details</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </td>
            </tr>
        </table>    

        <asp:Button runat="server" ID="notePopupButton" Style="display: none" />
        <ajaxToolkit:ModalPopupExtender 
            runat="server" 
            ID="notePopup" 
            BehaviorID="notePopupBehavior"
            TargetControlID="notePopupButton" 
            PopupControlID="notePopupPanel"
            BackgroundCssClass="modalBackground" 
            DropShadow="True" 
            PopupDragHandleControlID="notePopupDragPanel" />

        <asp:Panel runat="server" CssClass="modalPopup" ID="notePopupPanel" Style="display: none; width: 600px">
            <asp:Panel runat="Server" ID="notePopupDragPanel" CssClass="modalPopupTitle">
                <asp:Label ID="notePopupTitleLabel" runat="Server" />
            </asp:Panel>
            
            <asp:HiddenField ID="notePopupIdHidden" runat="server" />

            <table cellpadding="2" cellspacing="0" width="100%">
                <tr>
                    <td>Audience:</td>
                    <td><asp:DropDownList ID="notePopupAudienceDropDown" runat="server" /></td>
                    <td>
                        Priority:</td>
                    <td>
                        <asp:DropDownList ID="notePopupPriorityDropDown" runat="server">
                            <asp:ListItem Text="1" />
                            <asp:ListItem Text="2" />
                            <asp:ListItem Text="3" />
                        </asp:DropDownList>
                    </td>
                    <td>Active:</td>
                    <td><asp:CheckBox ID="notePopupActiveCheckBox" runat="server" /></td>
                </tr>
                <tr>
                    <td colspan="6">
                        <asp:TextBox ID="notePopupNoteTextBox" runat="server" TextMode="MultiLine" Rows="6" Width="99%"></asp:TextBox>
                    </td>
                </tr>                    
                <tr><td colspan="6"><asp:Label ID="notePopupUserLabel" runat="server" />&nbsp;</td></tr>
                <tr><td colspan="6">&nbsp;</td></tr>
                <tr>
                    <td align="right" colspan="6">
                        <asp:Button ID="notePopupAddButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="notePopupDeleteButton" runat="Server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                        <asp:Button ID="notePopupUpdateButton" runat="Server" Text="OK" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="notePopupCancelButton" runat="Server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    </td>
                </tr>
            </table>
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>



