<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ProductControl.ascx.vb" Inherits="Agent_ProductControl" %>

<%@ Register Src="..\..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\CollapsiblePanel\CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\..\UserControls\MessagePanelControl\MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<table cellpadding="2" cellspacing="0" width="100%">
    <tr>
        <td>
            <i><asp:Label ID="showHistoryResultLabel" runat="server" /></i>&nbsp;
        </td>
        <td align="right">
            <asp:CheckBox id="showHistoryCheckBox" runat="server" Text="Show History" AutoPostBack="True" />
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="removeButton" runat="Server" Text="Remove" CssClass="Button_Standard Button_Remove" Visible="True" />
            <asp:Button ID="addButton" runat="Server" Text="Add" CssClass="Button_Standard Button_Add" Visible="True" />
        </td>
    </tr>
</table>                 

<uc1:CollapsiblePanel 
    ID="updateCollapsiblePanel" 
    runat="server"
    Title="Update Product Discount Selection" 
    TargetControlID="updatePanel" />
<asp:Panel ID="updatePanel" runat="Server">
    <div style="height:20px; padding: 2px 0 2px 0;">
        <uc1:MessagePanelControl runat="server" ID="messagePanelControl" CssClass="popupMessagePanel" />
    </div>

    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr>
            <td width="150">Brand:</td>
            <td width="250">
                <asp:DropDownList ID="brandDropDown" runat="server" Width="200" />
            </td>
            <td width="150">Country:</td>
            <td>
                <asp:DropDownList ID="countryDropDown" runat="server" Width="200" />
            </td>
        </tr>
        <tr>
            <td>Date Type:</td>
            <td>
                <asp:DropDownList ID="dateTypeDropDow" runat="server">
                    <asp:ListItem></asp:ListItem>
                    <asp:ListItem>Booked</asp:ListItem>
                    <asp:ListItem>Travel</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>Discount %:</td>
            <td>
                <uc1:RegExTextBox 
                    ID="discountTextBox" 
                    runat="server" 
                    Width="50px" 
                    Nullable="true"
                    ErrorMessage="Discount Percent To must be a decimal value greater than zero" 
                    MessagePanelControlID="messagePanelControl" />
            </td>
        </tr>
        <tr>
            <td>Charge Agent:</td>
            <td>
                <asp:RadioButtonList ID="chargeAgentRadioButtonList" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="">Dont Change</asp:ListItem>
                    <asp:ListItem>Yes</asp:ListItem>
                    <asp:ListItem>No</asp:ListItem>
                </asp:RadioButtonList>
            </td>
            <td>Effective Date:</td>
            <td>
                <uc1:DateControl 
                    ID="effectiveDateTextBox" 
                    runat="server"
                    MessagePanelControlID="messagePanelControl" 
                    Nullable="false" />
            </td>
        </tr>
        <tr>
            <td colspan="3"><i>Note: updates are saved immediately.</i></td>
            <td align="right">
                <asp:Button ID="updateButton" runat="server" Text="Update" CssClass="Button_Standard Button_Update" />
            </td>
        </tr>
    </table>
</asp:Panel>

<asp:Table 
    id="historyResultTable" 
    runat="server" 
    CellPadding="2" 
    CellSpacing="0" 
    Visible="false" 
    width="100%" 
    CssClass="dataTableColor" 
    Style="margin-top: 10px">
    <asp:TableHeaderRow>
        <asp:TableHeaderCell Width="20px">
            <input type="checkbox" id="chkSelectAllPackages" onclick="return toggleTableColumnCheckboxes(0, event);" />        
        </asp:TableHeaderCell>
        <asp:TableHeaderCell>Product</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="120px">Brand</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="120px">Country</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="60px">Date Type</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="50px">Disc%</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="60px">Charge</asp:TableHeaderCell>
        <asp:TableHeaderCell Width="60px">Effective</asp:TableHeaderCell>
    </asp:TableHeaderRow>
</asp:Table> 
