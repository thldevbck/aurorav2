Imports System.Collections.Generic

Imports Aurora.Agent.Data
Imports Aurora.Agent.Data.AgentDataSet

<ToolboxData("<{0}:NoteControl runat=server></{0}:NoteControl>")> _
Partial Class Agent_NoteControl
    Inherits AgentUserControl

    Private _loadException As Exception
    Private _checkBoxesByNteId As New Dictionary(Of String, CheckBox)
    Private _notesByLinkButtonId As New Dictionary(Of String, NoteRow)

    Public Sub BuildNotes()
        _checkBoxesByNteId.Clear()
        _notesByLinkButtonId.Clear()
        While notesTable.Rows.Count > 1
            notesTable.Rows.RemoveAt(1)
        End While

        If Me.AgentRow Is Nothing Then Return

        For Each noteRow As NoteRow In Me.AgentRow.GetNoteRows()
            Dim tableRow As New TableRow
            tableRow.BackColor = noteRow.StatusColor()
            notesTable.Rows.Add(tableRow)

            Dim selectCell As New TableCell
            selectCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(selectCell)
            If CanMaintain Then
                Dim checkBox As New CheckBox
                checkBox.ID = "delete" & noteRow.NteId & "CheckBox"
                selectCell.Controls.Add(checkBox)
                _checkBoxesByNteId.Add(noteRow.NteId, checkBox)
            Else
                selectCell.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim typeCell As New TableCell
            typeCell.VerticalAlign = VerticalAlign.Top
            tableRow.Controls.Add(typeCell)
            If CanMaintain Then
                Dim linkButton As New LinkButton
                linkButton.ID = "edit" & noteRow.NteId & "Button"
                linkButton.Text = noteRow.Type
                _notesByLinkButtonId.Add(linkButton.ID, noteRow)
                AddHandler linkButton.Click, AddressOf editButton_Click
                typeCell.Controls.Add(linkButton)
            Else
                typeCell.Controls.Add(New LiteralControl(Server.HtmlEncode(noteRow.Type)))
            End If

            Dim noteCell As New TableCell
            noteCell.VerticalAlign = VerticalAlign.Top
            noteCell.Text = Server.HtmlEncode(noteRow.Description).Replace(vbCr, "").Replace(vbLf, "<br/>")
            tableRow.Controls.Add(noteCell)

            Dim detailsCell As New TableCell
            detailsCell.VerticalAlign = VerticalAlign.Top
            detailsCell.Text = "" _
                & "<i>Audience:</i> " & Server.HtmlEncode(noteRow.Audience) & "<br/>" _
                & "<i>Priority:</i> " & CStr(noteRow.Priority) & ", <i>Active:</i> " & IIf(noteRow.IsActive, "Yes", "No")
            tableRow.Controls.Add(detailsCell)
        Next

        addButton.Visible = CanMaintain
        deleteButton.Visible = CanMaintain
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                notePopupAudienceDropDown.Items.Clear()
                For Each codeRow As CodeRow In Me.AgentDataSet.Code
                    If codeRow.CodCdtNum = codeRow.CodeType_Audience_Type Then _
                        notePopupAudienceDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
                Next
            End If

            BuildNotes()
        Catch ex As Exception
            _loadException = ex
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        notePopupCancelButton.Attributes.Add("onclick", "$find('notePopupBehavior').hide(); return false;")
    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        If Not CanMaintain Then Return

        notePopupIdHidden.Value = ""
        notePopupAudienceDropDown.SelectedIndex = 0
        notePopupPriorityDropDown.SelectedIndex = 1
        notePopupActiveCheckBox.Checked = True
        notePopupNoteTextBox.Text = ""
        notePopupUserLabel.Text = ""

        notePopupAddButton.Visible = True
        notePopupDeleteButton.Visible = False
        notePopupUpdateButton.Visible = False
        notePopupTitleLabel.Text = "Add Note"
        notePopup.Show()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If Not CanMaintain Then Return

        For Each noteRow As NoteRow In Me.AgentRow.GetNoteRows()
            Dim checkBox As CheckBox = _checkBoxesByNteId(noteRow.NteId)
            If checkBox Is Nothing OrElse Not checkBox.Checked Then Continue For

            noteRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(noteRow)
            Me.AgentDataSet.Note.RemoveNoteRow(noteRow)
        Next

        BuildNotes()
    End Sub

    Protected Sub editButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim noteRow As NoteRow = _notesByLinkButtonId(CType(sender, LinkButton).ID)
        If noteRow Is Nothing Then Return

        notePopupIdHidden.Value = noteRow.NteId
        Try
            notePopupAudienceDropDown.SelectedValue = noteRow.CodAudTypId
        Catch
        End Try
        Try
            notePopupPriorityDropDown.SelectedValue = noteRow.Priority.ToString()
        Catch
        End Try
        notePopupActiveCheckBox.Checked = noteRow.IsActive
        notePopupNoteTextBox.Text = noteRow.Description
        notePopupUserLabel.Text = Server.HtmlEncode(Aurora.Common.Data.GetAddModDescription(noteRow))

        notePopupAddButton.Visible = False
        notePopupDeleteButton.Visible = True
        notePopupUpdateButton.Visible = True
        notePopupTitleLabel.Text = "Edit Note"
        notePopup.Show()
    End Sub

    Protected Sub notePopupAddButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notePopupAddButton.Click
        Dim noteRow As NoteRow = Me.AgentDataSet.Note.NewNoteRow()
        noteRow.NteId = DataRepository.GetNewId()
        noteRow.NteAgnId = Me.AgentRow.AgnId
        noteRow.NteCodTypId = Me.AgentDataSet.Code.FindByCodCdtNumCodCode(CodeRow.CodeType_Note_Type, "Agent").CodId
        noteRow.CodAudTypId = notePopupAudienceDropDown.SelectedValue
        noteRow.Priority = notePopupPriorityDropDown.SelectedValue
        noteRow.IsActive = notePopupActiveCheckBox.Checked
        If notePopupNoteTextBox.Text.Length > 1024 Then
            noteRow.Description = notePopupNoteTextBox.Text.Substring(0, 1024)
        Else
            noteRow.Description = notePopupNoteTextBox.Text
        End If
        Me.AgentDataSet.Note.AddNoteRow(noteRow)

        Aurora.Common.Data.ExecuteDataRow(noteRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        BuildNotes()
    End Sub

    Protected Sub notePopupUpdateButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notePopupUpdateButton.Click
        Dim noteRow As NoteRow = Me.AgentDataSet.Note.FindByNteId(notePopupIdHidden.Value)
        If noteRow Is Nothing Then Return

        noteRow.CodAudTypId = notePopupAudienceDropDown.SelectedValue
        noteRow.Priority = notePopupPriorityDropDown.SelectedValue
        noteRow.IsActive = notePopupActiveCheckBox.Checked
        If notePopupNoteTextBox.Text.Length > 1024 Then
            noteRow.Description = notePopupNoteTextBox.Text.Substring(0, 1024)
        Else
            noteRow.Description = notePopupNoteTextBox.Text
        End If

        Aurora.Common.Data.ExecuteDataRow(noteRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)

        BuildNotes()
    End Sub

    Protected Sub notePopupDeleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles notePopupDeleteButton.Click
        Dim noteRow As NoteRow = Me.AgentDataSet.Note.FindByNteId(notePopupIdHidden.Value)
        If noteRow Is Nothing Then Return

        noteRow.Delete()
        Aurora.Common.Data.ExecuteDataRow(noteRow, Me.CurrentPage.UserCode, Me.CurrentPage.PrgmName)
        Me.AgentDataSet.Note.RemoveNoteRow(noteRow)

        BuildNotes()
    End Sub

    Protected Sub notePopupCancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles notePopupCancelButton.Click
        notePopup.Hide()
    End Sub

End Class


