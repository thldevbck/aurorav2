<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddressDetailsControl.ascx.vb" Inherits="Agent_AddressDetailsControl" %>

<table style="width:375px" cellpadding="2" cellspacing="0">
    <tr>
        <td style="width:125px">Street:</td>
        <td><asp:TextBox ID="address1TextBox" runat="server" Width="225px" /><asp:Label ID="address1Label" runat="server">&nbsp;*</asp:Label></td>
    </tr>
    <tr>
        <td>Suburb:</td>
        <td><asp:TextBox ID="address2TextBox" runat="server" Width="225px" /></td>
    </tr>
    <tr>
        <td>Town/City:</td>
        <td><asp:TextBox ID="address3TextBox" runat="server" Width="225px" /><asp:Label ID="address3Label" runat="server">&nbsp;*</asp:Label></td>
    </tr>
    <tr>
        <td>State:</td>
        <td><asp:TextBox ID="stateTextBox" runat="server" Width="150px" /></td>
    </tr>
    <tr>
        <td>Postal Code:</td>
        <td><asp:TextBox ID="postCodeTextBox" runat="server" Width="100px" /></td>
    </tr>
    <tr>
        <td>Country:</td>
        <td><asp:DropDownList ID="countryDropDown" runat="server" Width="225px" /><asp:Label ID="countryLabel" runat="server">&nbsp;*</asp:Label></td>
    </tr>
</table>