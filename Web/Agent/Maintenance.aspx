<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="Maintenance.aspx.vb" Inherits="Agent_Maintenance" Title="Untitled Page" %>
    
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/RegExTextBox/RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<%@ Register Src="Controls/AddressDetailsControl.ascx" TagName="AddressDetailsControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/NoteControl.ascx" TagName="NoteControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/ContactsControl.ascx" TagName="ContactsControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/ProductControl.ascx" TagName="ProductControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/PackageControl.ascx" TagName="PackageControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/DebtorGrouping.ascx" TagName="DebtorGrouping" TagPrefix="uc1" %>
<%@ Register Src="~/Agent/Controls/B2BControl.ascx" TagName="B2BControl" TagPrefix="uc1" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
<style type="text/css">
    .ajax__tab_panel
    {
        min-height:375px; 
        height:auto !important; 
        height:375px;
        padding-top: 10px;
    }

    .CleanGrid 
    {
    	border:none;
    }
    
    .CleanGrid TD
    {
    	border:none;
    }
    
    .checkboxlist table {border:none;}
    
    .checkboxlist td {border:none;}    
    </style>
    
    
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

<script type="text/javascript">
var flexExportTypeCheckBoxListId = '<%= Me.flexExportTypeCheckBoxList.ClientID %>';
var flexExportTypeCheckBoxList;

function flexExportType_selectAllNone (all)
{
    if (!flexExportTypeCheckBoxList) return;
    
    var inputs = flexExportTypeCheckBoxList.getElementsByTagName('input');
    for (var j = 0; j < inputs.length; j++)
    {
        var input = inputs[j];
        input.checked = all;
    }
}

function agentEdit_Init()
{
    flexExportTypeCheckBoxList = document.getElementById(flexExportTypeCheckBoxListId);
}

addEvent (window, "load", agentEdit_Init);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(agentEdit_Init);

function toggleTableColumnCheckboxes(col, evt)
{
    if (!evt) evt = window.event;
    if (!evt) return false;

    var chkAll = evt.srcElement ? evt.srcElement : evt.target;
    if (!chkAll) return false;
    
    var tbody = chkAll;
    while (tbody && (tbody.tagName != 'TBODY'))
        tbody = tbody.parentNode;
    if (!tbody) return false;
    
    var tableRows = tbody.getElementsByTagName("TR");
    for (var i = 0; i < tableRows.length; i++)
    {
        var tableCell = tableRows[i].getElementsByTagName ("TD")[col];
        if (!tableCell) continue;
        
        var inputElements = tableCell.getElementsByTagName ("INPUT");
        for (var j = 0; j < inputElements.length; j++)
        {
            var inputElement = inputElements[j];
            if (inputElement.type == 'checkbox')
                inputElement.checked = chkAll.checked;
        }
    }
    
    return true;
}
    function onPageSearch()
    {
            WindowNavigate('Agent/Search.aspx','agnId=' + '<%= request.querystring("agnId") %>');
    }
    
    function ActiveTabChangedHandler()
    {
        try
        {
            var srcEl = event.srcElement? event.srcElement : event.target; 
            var SaveButton = $get("<%= updateButton.ClientID %>");
            
            if(srcEl.id == "__tab_ctl00_ContentPlaceHolder_tabs_b2bTabPanel")
            {
                SaveButton.disabled =true;
                SaveButton.className += " buttondisabled";
            }
            else
            {
                SaveButton.disabled =false;
                SaveButton.className = "Button_Standard Button_Save";
            }
        }
        catch(err)
        {}
    }
    
</script>

<script language="javascript" src="../OpsAndLogs/JS/OpsAndLogsCommon.js" type="text/javascript"></script>
<script language="javascript" src="../UserControls/DefaultAndSelectionControl/DefaultAndSelection.js" type="text/javascript" ></script>

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
     <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>            
           
           <asp:Panel ID="agentDetailsPanel" runat="server">
           
               <table cellpadding="2" cellspacing="0" width="100%">
                    <tr>
                        <td style="width: 150px">Agent Code:</td>
                        <td colspan="3"><asp:TextBox id="codeTextBox" runat="server" Width="150px" MaxLength="10" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td>Agent Name:</td>
                        <td colspan="3"><asp:TextBox id="nameTextBox" runat="server" Width="550px" MaxLength="256" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td>Agent Type:</td>
                        <td style="width:250px"><asp:DropDownList id="agentTypeDropDown" runat="server" Width="200px"><asp:ListItem></asp:ListItem></asp:DropDownList>&nbsp;*</td>
                        <td style="width:150px">Category:</td>
                        <td><asp:DropDownList id="categoryDropDown" runat="server" Width="200px"></asp:DropDownList>&nbsp;*</td>
                    </tr>
                    <tr>
                        <td>Agent Group/Sub Group:</td>
                        <td colspan="3">
                            <uc1:PickerControl 
                                id="groupSubgroupPicker" 
                                runat="server" 
                                PopupType="SUBGROUP" 
                                AppendDescription="true" 
                                Nullable="false"
                                Caption="Agent Group/Sub Group"
                                Width="400px" />&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td>Debtor Status:</td>
                        <td><asp:DropDownList style="width: 200px" id="debtorStatusDropDown" runat="server" Enabled="false" /></td>
                        <td>Market Code:</td>
                        <td><asp:DropDownList style="width: 200px" id="marketCodeDropDown" runat="server" />&nbsp;*</td>
                        
                    </tr>
                    <%--rev:mia Nov.6 2012 --%>
                    <tr>
                        <td style="vertical-align:top;">Debtor Grouping:</td>
                        <td colspan="5">
                            <uc1:DebtorGrouping ID="DebtorGrouping1" runat="server" />
                        </td>
                       
                        
                    </tr>
                    <%--Phoenix Change--%>
                    <tr>
                        <td>Max Deposit Amount:</td>
                        <td>
                            <asp:TextBox CssClass="TextBox_Standard" id="txtMaxDepositAmount" runat="server" MaxLength="8" 
                                         onkeypress="return keyStrokeDecimal(event);" 
                                         onblur="CheckContents(this,'');CheckPositiveDecimal(this,'');" />
                        </td>
                    </tr>
                    <%--Phoenix Change--%>
                    <tr>
                        <td style="border-top-style: dotted; border-top-width: 1px;">Flags:</td>
                        <td style="border-top-style: dotted; border-top-width: 1px;" colspan="3">
                            <asp:CheckBoxList id="flagsCheckBoxList" runat="server" TextAlign="Right" RepeatDirection="Horizontal" CssClass="repeatLabelTable">
                                <asp:ListItem Value="AgnIsVip">VIP Class</asp:ListItem>
                                <asp:ListItem Value="AgnIsMisc">Misc</asp:ListItem>
                                <asp:ListItem Value="AgnIsSeekingApproval">Seeking Approval</asp:ListItem>
                                <asp:ListItem Value="AgnIsInclusiveProduct">Inclusive Product Only</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>

                <table style="width:100%; border-width: 1px 0px" cellpadding="2" cellspacing="0" id="statusTable" runat="server">
                    <tr>
                        <td style="width:150px;">Status:</td>
                        <td style="width:650px;" colspan="3">
                            <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                                <asp:ListItem Text="Active" />
                                <asp:ListItem Text="Inactive" />
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
        
            </asp:Panel>

            <ajaxToolkit:TabContainer runat="server" ID="tabs" ActiveTabIndex="0" style="margin-top: 5px;">
                <ajaxToolkit:TabPanel runat="server" ID="contactsTabPanel" HeaderText="Contact">
                    <ContentTemplate>
                    
                        <asp:Panel ID="contactsPanel" runat="server">
                            <table cellpadding="2" cellspacing="0" style="width: 100%">
                                <tr>
                                    <td style="width: 125px">Phone Number:</td>
                                    <td>
                                        <asp:TextBox id="phoneCountryCodeTextBox" runat="server" Width="35px" MaxLength="6" />&nbsp;
                                        <asp:TextBox id="phoneAreaCodeTextBox" runat="server" Width="35px" MaxLength="6" />&nbsp;
                                        <asp:TextBox id="phoneNumberTextBox" runat="server" Width="150px" MaxLength="24" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fax Number:</td>
                                    <td>
                                        <asp:TextBox id="faxCountryCodeTextBox" runat="server" Width="35px"  MaxLength="6"/>&nbsp;
                                        <asp:TextBox id="faxAreaCodeTextBox" runat="server" Width="35px"  MaxLength="6"/>&nbsp;
                                        <asp:TextBox id="faxNumberTextBox" runat="server" Width="150px"  MaxLength="24"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Free Phone No:</td>
                                    <td><asp:TextBox id="freePhoneNoTextBox" runat="server" Width="300px"  MaxLength="24"/></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td>
                                        <uc1:RegExTextBox 
                                            ID="emailTextBox" 
                                            runat="server" 
                                            Nullable="true"
                                            MaxLength="64"
                                            Width="300px" />                
                                    </td>
                                </tr>
                                <tr>
                                    <td>Website:</td>
                                    <td><asp:TextBox id="websiteTextBox" runat="server" Width="300px"  MaxLength="24"/></td>
                                </tr>
                            </table>
                        </asp:Panel>                        
                        
                        <uc1:ContactsControl ID="contactsControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="addressTabPanel" HeaderText="Address">
                    <ContentTemplate>
                        <table cellspacing="0" cellpadding="2">
                            <tr>
                                <td style="width: 50%; border-style: dotted; border-right-width: 1px;">
                                    <b>Billing Address:</b>
                                    
                                    <uc1:AddressDetailsControl ID="billingAddressDetailsControl" runat="server" Label="billing address" Code="Billing" />
                                </td>
                                <td style="width: 50%">&nbsp;</td>
                            
                            </tr>
                            <tr>
                                <td style="width: 50%; border-style: dotted; border-right-width: 1px; border-top-width: 1px">
                                    <b>Physical Address:</b>
                                    
                                    <uc1:AddressDetailsControl ID="physicalAddressDetailsControl" runat="server" Label="physical address" Code="Physical" />
                                </td>
                                <td style="width: 50%; border-style: dotted; border-top-width: 1px">
                                    <b>Postal Address:</b>

                                    <uc1:AddressDetailsControl ID="postalAddressDetailsControl" runat="server" Label="postal address" Code="Postal" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="flexExportTypeTabPanel" HeaderText="Flex Export Types">
                    <ContentTemplate>

                        <asp:CheckBoxList 
                            ID="flexExportTypeCheckBoxList" 
                            runat="server" 
                            RepeatColumns="2"
                            RepeatLayout="Table" 
                            RepeatDirection="Vertical"
                            Width="100%" />
                        
                        <asp:Panel ID="flexExportTypePanel" runat="server">
                            <div style="text-align: right">
                                <a href="#" onclick="flexExportType_selectAllNone(true); return false;">Select All</a> 
                                &nbsp;&nbsp;|&nbsp;&nbsp;
                                <a href="#" onclick="flexExportType_selectAllNone(false); return false;">Select None</a>
                            </div>

                            <table cellspacing="0" cellpadding="2" style="width: 100%; margin-top: 5px">
                                <tr>
                                    <td style="border-top-width: 1px; border-style: dotted">
                                        <i><b>Email Addresses Used:</b> 
                                        &nbsp;&nbsp;&nbsp;
                                        <i>(extracted from Staff Contacts of type &quot;Flex&quot;)</i>
                                    </td>
                                    <tr>
                                        <td><asp:TextBox ID="flexExportTypeEmail0TextBox" runat="server" Width="300px" ReadOnly="True" /></td>
                                    </tr>
                                    <tr>
                                        <td><asp:TextBox ID="flexExportTypeEmail1TextBox" runat="server" Width="300px" ReadOnly="True" /></td>
                                    </tr>
                                    <tr>
                                        <td><asp:TextBox ID="flexExportTypeEmail2TextBox" runat="server" Width="300px" ReadOnly="True" /></td>
                                    </tr>
                                </tr>
                            </table>
                        </asp:Panel>
                        
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="productTabPanel" HeaderText="Product Discounts">
                    <ContentTemplate>
                        <uc1:ProductControl ID="productControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="packageTabPanel" HeaderText="Packages">
                    <ContentTemplate>
                        <uc1:PackageControl ID="packageControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="notesTabPanel" HeaderText="Notes">
                    <ContentTemplate>
                        <uc1:NoteControl ID="noteControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
                <ajaxToolkit:TabPanel runat="server" ID="b2bTabPanel" HeaderText="B2B">
                    <ContentTemplate>
                        <uc1:B2BControl ID="b2bControl" runat="server" />
                    </ContentTemplate>
                </ajaxToolkit:TabPanel>
            </ajaxToolkit:TabContainer>
            
            <table style="width:100%; margin-top: 5px" cellpadding="2" cellspacing="0">
                <tr>
                    <td align="right">
                        <asp:Button id="updateButton" runat="server" CssClass="Button_Standard Button_Save" Text="Save" />
                        <asp:Button id="createButton" runat="server" CssClass="Button_Standard Button_Save" Text="Save" />
                        <asp:Button id="backButton" runat="server" CssClass="Button_Standard Button_Back" Text="Back" />
                    </td>
                </tr>
            </table>

        </contenttemplate>
    </asp:UpdatePanel>
        
</asp:Content>
