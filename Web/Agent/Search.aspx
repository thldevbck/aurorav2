<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false" CodeFile="Search.aspx.vb" Inherits="Agent_Search" %>

<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
    
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <asp:Panel ID="agentSearchPanel" runat="server" Width="100%" DefaultButton="searchButton">
    
        <table cellpadding="2" cellspacing="0" style="width:100%">
            <tr style="display: none">
                <td>Agent Code:</td>
                <td colspan="3"><asp:TextBox ID="codeTextBox" runat="server" style="width:200px" /></td>
            </tr>
            <tr>
                <td style="width:150px">Agent Code / Name:</td>
                <td colspan="3">
                    <asp:TextBox ID="nameTextBox" runat="server" style="width:500px" MaxLength="256" />
                    &nbsp;
                    <asp:CheckBox ID="soundExCheckBox" runat="server" Text="Soundex Match" />
                </td>
            </tr>
            <tr>
                <td>Agent Type:</td>
                <td colspan="3">
                    <asp:DropDownList ID="agentTypeDropDown" runat="server" style="width:200px"><asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Agent Group:</td>
                <td colspan="3">
                    <uc1:PickerControl ID="agentGroupPicker" runat="server" Width="200px" PopupType="AGENTGROUP" />
                </td>
            </tr>
            <tr>
                <td>Debtor Status:</td>
                <td colspan="3">
                    <asp:DropDownList ID="debtorStatusDropDown" runat="server" style="width:150px"><asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="border-top-style: dotted; border-top-width: 1px;">Phone Number:</td>
                <td style="border-top-style: dotted; border-top-width: 1px;" colspan="3">
                    <asp:TextBox ID="phoneCountryCodeTextBox" runat="server" style="width:35px" />&nbsp;
                    <asp:TextBox ID="phoneAreaCodeTextBox" runat="server" style="width:35px" />&nbsp;
                    <asp:TextBox ID="phoneNumberTextBox" runat="server" style="width:150px" />
                </td>
            </tr>
            <tr>
                <td style="width:150px">Street:</td>
                <td style="width:250px"><asp:TextBox ID="streetTextBox" runat="server" style="width:150px" /></td>
                <td style="width:150px">Suburb:</td>
                <td><asp:TextBox ID="suburbTextBox" runat="server" style="width:150px" /></td>
            </tr>
            <tr>
                <td>City / Town:</td>
                <td><asp:TextBox ID="cityTownTextBox" runat="server" style="width:150px" /></td>
                <td>State:</td>
                <td><asp:TextBox ID="stateTextBox" runat="server" style="width:150px" /></td>
            </tr>
            <tr>
                <td>Postcode:</td>
                <td><asp:TextBox ID="postcodeTextBox" runat="server" style="width:150px" /></td>
                <td>Country:</td>
                <td>
                    <asp:DropDownList ID="countryDropDown" runat="server" style="width:250px">
                        <asp:ListItem />
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="border-top-style: dotted; border-top-width: 1px;">Status:</td>
                <td colspan="1" style="border-top-style: dotted; border-top-width: 1px;">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3" RepeatLayout="Table" CssClass="repeatLabelTable">
                        <asp:ListItem Text="Active" Value="True" Selected="True" />
                        <asp:ListItem Text="Inactive" Value="False" />
                        <asp:ListItem Text="All" Value="" />
                    </asp:RadioButtonList>
                </td>
                <td style="border-top-style: dotted; border-top-width: 1px;"  colspan="2">
                    <asp:CheckBox runat="server" Text="B2B Access" ID="radioB2BAccess" />
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="border-top-width: 1px; border-bottom-width: 1px">
                    <i><asp:Label ID="searchResultLabel" runat="server" /></i>&nbsp;
                </td>
                <td colspan="3" align="right" style="border-top-width: 1px; border-bottom-width: 1px">
                    <asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
                    <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                </td>
            </tr>
        </table>
     
        <br />
     
        <asp:Table ID="searchResultTable" runat="server" Width="100%" Visible="False" CssClass="dataTableColor">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell Text="Name" />
                <asp:TableHeaderCell Text="Type" Width="100px" />
                <asp:TableHeaderCell Text="Group" Width="150px" />
                <asp:TableHeaderCell Text="Debtor Status" Width="80px" />
                <asp:TableHeaderCell Text="Billing Address" Width="175px" />
                <asp:TableHeaderCell Text="Status" Width="60px" />
            </asp:TableHeaderRow>
        </asp:Table>
       
    </asp:Panel>
</asp:Content>

