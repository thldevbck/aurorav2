Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Text
Imports System.IO

Imports Aurora.Common
Imports Aurora.Reports.Data
Imports Aurora.Reports.Data.ReportDataSet
Imports Aurora.Reports.Services

<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class Help_Default
    Inherits AuroraPage

    Public Const FunCodeParam As String = "funCode"
    Public Shared ReadOnly HelpFileEncoding As System.Text.Encoding = Encoding.GetEncoding(1252)

    Public Class StringWriterUTF8
        Inherits StringWriter

        Public Sub New()
            MyBase.New()
        End Sub

        Public Overrides ReadOnly Property Encoding() As Encoding
            Get
                Return System.Text.Encoding.UTF8
            End Get
        End Property

    End Class

    Private Function ExtractTag(ByVal fileContents As String, ByVal tagName As String, ByVal startIndex As Integer) As String
        Dim string0 As String = "<" & tagName
        Dim index0 As Integer = fileContents.IndexOf(string0, startIndex)
        If index0 = -1 Then Return ""

        Dim string1 As String = ">"
        Dim index1 As Integer = fileContents.IndexOf(string1, index0 + string0.Length)
        If index1 = -1 Then Return ""

        Dim string2 As String = "</" & tagName & ">"
        Dim index2 As Integer = fileContents.IndexOf(string2, index1 + string1.Length)
        If index2 = -1 Then Return ""

        Return fileContents.Substring(index1 + 1, index2 - index1 - 1)
    End Function


    Private _titleString As String = "Help not found"
    Public ReadOnly Property TitleString()
        Get
            Return _titleString
        End Get
    End Property

    Private _styleString As String = ""
    Public ReadOnly Property StyleString()
        Get
            Return _styleString
        End Get
    End Property

    Private _contentString As String = ""
    Public ReadOnly Property ContentString()
        Get
            Return _contentString
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            Return TitleString
        End Get
    End Property


    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)

        Dim fileName As String = Me.Server.MapPath(".\" & Request(FunCodeParam) & ".htm")
        Dim basePath As String = Me.Server.MapPath(".")

        If Not fileName.ToLower().StartsWith(basePath.ToLower()) _
         OrElse Not System.IO.File.Exists(fileName) Then Return ' security check

        Try
            _titleString = ""
            _styleString = ""
            _contentString = ""

            Dim fileContents As String
            Using reader As New StreamReader(fileName, HelpFileEncoding)
                fileContents = reader.ReadToEnd()
            End Using

            _titleString = ExtractTag(fileContents, "title", 0)
            _styleString = ExtractTag(fileContents, "style", 0)
            _contentString = ExtractTag(fileContents, "body", 0)
        Catch
            _titleString = "Error Loading Help"
            _styleString = ""
            _contentString = ""
        End Try

        MyBase.OnInit(e)
    End Sub

End Class
