<%@ Page Language="VB" MasterPageFile="~/Include/PopupHeader.master" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="Help_Default" title="Untitled Page" %>

<%@ Register Src="..\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>
<%@ Register Src="..\UserControls\CollapsiblePanel\CollapsiblePanel.ascx" TagName="CollapsiblePanel" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="StylePlaceHolder" Runat="Server">

<style type="text/css">
<%= Me.StyleString %>
</style>

</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">

<%= Me.ContentString %>

</asp:Content>

