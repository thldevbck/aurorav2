In this folder should go html help files authored in word, and any sub folders for the resources they reference ("*_Files")

- Each help file must be named after a function code e.g. TABLEEDITOR.htm
- It is assumed MS-Word saves the html with a 1252 text encoding (the default)

The help page ("default.aspx") loads the word html file, strips out the title, styles, and 
body, and inserts it into an Aurora template.



