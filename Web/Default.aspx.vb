Imports System.Collections.Generic
Imports System.Data
Imports System.Web.Script.Serialization

Imports Aurora.Common
Imports Aurora.Admin.Data
Imports Aurora.Admin.Data.UserDataSet

<AuroraPageTitleAttribute("Aurora")> _
<AuroraFunctionCodeAttribute("GENERAL")> _
Partial Class _Default
    Inherits AuroraPage

    Private _javascriptSerializer As New System.Web.Script.Serialization.JavaScriptSerializer()
    Private _initException As Exception

    Private _userDataSet As New UserDataSet()
    Private _userInfoRow As UserInfoRow
    Private _flexExportTypeCheckBoxListByCompanyCode As New Dictionary(Of String, CheckBoxList)

    Private _initialFunctionUrl As String = ""
    Public ReadOnly Property InitialFunctionUrl() As String
        Get
            Return _javascriptSerializer.Serialize(UserSettings.Current.FunFileName)
        End Get
    End Property

    Public ReadOnly Property OpenNewWindow() As String
        Get
            Return IIf((UserSettings.Current.SystemIdentifier.ToLower().Contains("test") OrElse UserSettings.Current.SystemIdentifier.ToLower().Contains("dev")) _
             AndAlso Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance), "false", "true")
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        Me.Page.Title = Me.PageTitle & " - " & UserSettings.Current.SystemIdentifier

        Try
            _userDataSet.EnforceConstraints = False

            DataRepository.GetUserLookups(_userDataSet)

            DataRepository.GetUser(_userDataSet, Me.UserId, New Nullable(Of Boolean))
            _userInfoRow = _userDataSet.UserInfo.FindByUsrId(Me.UserId)
            If _userInfoRow Is Nothing Then
                Throw New Exception("No user found")
            End If
        Catch ex As Exception
            Me.ErrorRedirect(ex.Message)
        End Try
    End Sub

    Private Function IsCountryValidForCompany(ByVal comCode As String, ByVal countryRow As CountryRow) As Boolean
        For Each locationRow As LocationRow In _userDataSet.Location
            If locationRow.LocComCode = comCode AndAlso locationRow.CtyCode = countryRow.CtyCode Then
                Return True
            End If
        Next
        Return False
    End Function

    Private Function IsLocationValidForCompanyCountry(ByVal comCode As String, ByVal ctyCode As String, ByVal locationRow As LocationRow) As Boolean
        Return locationRow.LocComCode = comCode AndAlso locationRow.CtyCode = ctyCode
    End Function

    Private Function IsValidMenu(ByVal menuRow As MenuRow) As Boolean
        For Each auroraPageMenuModule As AuroraPageMenuModule In Me.AuroraPageMenu.Items
            For Each auroraPageMenuFunction As AuroraPageMenuFunction In auroraPageMenuModule.Items
                If auroraPageMenuFunction.MenId = menuRow.MenId Then
                    Return True
                End If
            Next
        Next
        Return False
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then Return

        nameTextBox.Text = Me.UserCode & " - " & Me.UserName

        companyDropDown.Items.Clear()
        For Each companyRow As CompanyRow In _userDataSet.Company
            companyDropDown.Items.Add(New ListItem(companyRow.ComCode & " - " & companyRow.ComName, companyRow.ComCode))
        Next
        companyDropDown.SelectedValue = Me.CompanyCode
        companyDropDown.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance)

        companyRepeater.DataSource = _userDataSet.Company
        companyRepeater.DataBind()

        countryDropDown.Items.Clear()
        For Each countryRow As CountryRow In _userDataSet.Country
            If IsCountryValidForCompany(companyDropDown.SelectedValue, countryRow) Then
                countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode.ToUpper()))
            End If
        Next
        countryDropDown.SelectedValue = Me.CountryCode
        countryDropDown.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance)

        locationDropDown.Items.Clear()
        For Each locationRow As LocationRow In _userDataSet.Location
            If IsLocationValidForCompanyCountry(companyDropDown.SelectedValue, countryDropDown.SelectedValue, locationRow) Then
                locationDropDown.Items.Add(New ListItem(locationRow.LocCode & " - " & locationRow.LocName, locationRow.LocCode.ToUpper()))
            End If
        Next
        locationDropDown.SelectedValue = UserSettings.Current.LocCode
        locationDropDown.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance)

        menuDropDown.Items.Clear()
        For Each menuRow As MenuRow In _userDataSet.Menu
            If IsValidMenu(menuRow) Then
                Dim li As ListItem = New ListItem(menuRow.Text, menuRow.MenId)
                menuDropDown.Items.Add(li)
            End If
        Next
        Try
            menuDropDown.SelectedValue = _userInfoRow.UsrDefaultMenId
        Catch
            menuDropDown.SelectedIndex = 0
        End Try
        menuDropDown.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance)
       
    End Sub

    Protected Sub companyDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles companyDropDown.SelectedIndexChanged
        If _userInfoRow Is Nothing OrElse Not Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance) Then Return

        For Each userCompanyRow0 As UserCompanyRow In _userInfoRow.GetUserCompanyRows()
            userCompanyRow0.Delete()
            Aurora.Common.Data.ExecuteDataRow(userCompanyRow0)
        Next
        _userDataSet.UserCompany.Clear()

        Dim userCompanyRow As UserCompanyRow = _userDataSet.UserCompany.NewUserCompanyRow()
        userCompanyRow.UsrId = _userInfoRow.UsrId
        userCompanyRow.ComCode = companyDropDown.SelectedValue
        _userDataSet.UserCompany.AddUserCompanyRow(userCompanyRow)
        Aurora.Common.Data.ExecuteDataRow(userCompanyRow, Me.UserCode, Me.PrgmName)

        For Each countryRow As CountryRow In _userDataSet.Country
            If IsCountryValidForCompany(companyDropDown.SelectedValue, countryRow) Then
                _userInfoRow.UsrCtyCode = countryRow.CtyCode
                Exit For
            End If
        Next
        For Each locationRow As LocationRow In _userDataSet.Location
            If IsLocationValidForCompanyCountry(companyDropDown.SelectedValue, countryDropDown.SelectedValue, locationRow) Then
                _userInfoRow.UsrLocCode = locationRow.LocCode
                Exit For
            End If
        Next
        Aurora.Common.Data.ExecuteDataRow(_userInfoRow, Me.UserCode, Me.PrgmName)

        Response.Redirect("Default.aspx")
    End Sub

    Protected Sub countryDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles countryDropDown.SelectedIndexChanged
        If _userInfoRow Is Nothing OrElse Not Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance) Then Return

        _userInfoRow.UsrCtyCode = countryDropDown.SelectedValue
        For Each locationRow As LocationRow In _userDataSet.Location
            If IsLocationValidForCompanyCountry(companyDropDown.SelectedValue, countryDropDown.SelectedValue, locationRow) Then
                _userInfoRow.UsrLocCode = locationRow.LocCode
                Exit For
            End If
        Next
        Aurora.Common.Data.ExecuteDataRow(_userInfoRow, Me.UserCode, Me.PrgmName)

        Response.Redirect("Default.aspx")
    End Sub

    Protected Sub locationDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationDropDown.SelectedIndexChanged
        If _userInfoRow Is Nothing OrElse Not Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance) Then Return

        _userInfoRow.UsrLocCode = locationDropDown.SelectedValue
        Aurora.Common.Data.ExecuteDataRow(_userInfoRow, Me.UserCode, Me.PrgmName)

        Response.Redirect("Default.aspx")
    End Sub

    Protected Sub menuDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles menuDropDown.SelectedIndexChanged
        If _userInfoRow Is Nothing OrElse Not Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UserMaintenance) Then Return

        _userInfoRow.UsrDefaultMenId = menuDropDown.SelectedValue

        Aurora.Common.Data.ExecuteDataRow(_userInfoRow, Me.UserCode, Me.PrgmName)

        Response.Redirect("Default.aspx")
    End Sub

    Protected Sub refreshButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles refreshButton.Click

        If ("" & Me.CookieValue("refreshUser")) = "" Then
            Session.Abandon()
            System.Web.Security.FormsAuthentication.SignOut()

            Response.Status = "401 Unauthorized"
            Response.AddHeader("WWW-Authenticate", "Digest")
            Me.CookieValue("refreshUser") = "true"
            Response.End()
        Else
            Me.CookieValue("refreshUser") = ""
            Response.Redirect("Default.aspx")
        End If
    End Sub


End Class
