<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="ConfirmationText.aspx.vb" Inherits="Confirmation_ConfirmationText"
    Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
<script language=javascript>

function ClientValidation() 
{
    // typeTextBox
    var typeTextBox;
    typeTextBox = document.getElementById('<%=confirmationTextFormView.FindControl("typeTextBox").ClientID%>');
    if (typeTextBox.value == "")
    {
        setWarningShortMessage("Type is mandatory.")
        return false
    }
    // textTextBox
    var textTextBox;
    textTextBox = document.getElementById('<%=confirmationTextFormView.FindControl("textTextBox").ClientID%>');
    if (textTextBox.value == "")
    {
        setWarningShortMessage("Text is mandatory.")
        return false
    }
}
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <contenttemplate>
                        
        <asp:FormView ID="confirmationTextFormView" runat="server" DefaultMode="edit" width="100%">
            <EditItemTemplate>
                <table width="100%">
                    <tr>
                        <td width="150">
                            Type:
                        </td>
                        <td >
                            <asp:TextBox ID="typeTextBox" runat="server" Text='<%# Bind("ConfType") %>' Width="97%" MaxLength="64"></asp:TextBox>&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td valign=top >
                            Text:
                            <asp:CustomValidator id="CustomValidator1" runat="server" ControlToValidate="typeTextBox" ErrorMessage="" 
                                OnServerValidate="CustomValidator1_OnServerValidate"></asp:CustomValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="textTextBox" runat="server" Text='<%# Bind("ConfText") %>' TextMode="MultiLine"
                                Rows="15" Width="97%" MaxLength="5000"></asp:TextBox>&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Header / Footer:</td>
                        <td>
                            <asp:RadioButtonList ID="headerRadioButtonList" runat="server" SelectedValue='<%# Bind("ConfHeader") %>'
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="Header" Value="1" Selected></asp:ListItem>
                                <asp:ListItem Text="Footer" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Order:</td>
                        <td>
                           
                            <asp:TextBox ID="orderTextBox" runat="server" Text='<%# Bind("ConfOrder") %>'
                                       Width="20%" onkeypress="keyStrokeInt();" style="text-align: right"  MaxLength="4"></asp:TextBox>
                     
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Def Customer:</td>
                        <td>
                            <asp:CheckBox ID="isCusCheckBox" runat="server" Checked='<%# Bind("IsCus") %>'></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Def Agent:</td>
                        <td>
                            <asp:CheckBox ID="isAgnCheckBox" runat="server" Checked='<%# Bind("IsAgn") %>'></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Active:</td>
                        <td>
                            <asp:CheckBox ID="activeCheckBox" runat="server" Checked='<%# Bind("ConfActive") %>'>
                            </asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                         <td>
                            Brand:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBrandCode" runat="server" AutoPostBack="false" width="100"  />
                        </td>
                    </tr>
                </table>
            </EditItemTemplate>
             
            <InsertItemTemplate>
                <table width="100%">
                    <tr>
                        <td width="150">
                            Type:
                        </td>
                        <td >
                            <asp:TextBox ID="typeTextBox" runat="server" Text='<%# Bind("ConfType") %>' Width="97%"></asp:TextBox>&nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td valign=top >
                            Text:
                            <asp:CustomValidator id="CustomValidator1" runat="server" ControlToValidate="typeTextBox" ErrorMessage="" 
                            OnServerValidate="CustomValidator1_OnServerValidate"></asp:CustomValidator>
                            </td>
                        <td>
                            <asp:TextBox ID="textTextBox" runat="server" Text='<%# Bind("ConfText") %>' TextMode="MultiLine"
                                Rows="15" Width="97%" MaxLength="5000"></asp:TextBox>&nbsp;*

                        </td>
                    </tr>
                    <tr>
                        <td>
                            Header / Footer:</td>
                        <td>
                            <asp:RadioButtonList ID="headerRadioButtonList" runat="server" SelectedValue='<%# Bind("ConfHeader") %>'
                                RepeatDirection="Horizontal">
                                <asp:ListItem Text="Header" Value="1" Selected></asp:ListItem>
                                <asp:ListItem Text="Footer" Value="0"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Order:</td>
                        <td>
                           
                                    <asp:TextBox ID="orderTextBox" runat="server" Text='0'
                                       Width="20%"  onkeypress="keyStrokeInt();" style="text-align: right"></asp:TextBox>
                   
                                  
                          
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Def Customer:</td>
                        <td>
                            <asp:CheckBox ID="isCusCheckBox" runat="server" Checked='<%# Bind("IsCus") %>'></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Def Agent:</td>
                        <td>
                            <asp:CheckBox ID="isAgnCheckBox" runat="server" Checked='<%# Bind("IsAgn") %>'></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Active:</td>
                        <td>
                            <asp:CheckBox ID="activeCheckBox" runat="server" Checked='true'>
                            </asp:CheckBox>
                        </td>
                    </tr>
                   <tr>
                        <td>
                            Brand:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBrandCode" runat="server" AutoPostBack="false" width="100" />
                        </td>
                    </tr>
                </table>
            </InsertItemTemplate>
        </asp:FormView>
      </contenttemplate>
    </asp:UpdatePanel>
    <table width="100%">
        <tr>
            <td align="right" >
                <asp:Button ID="saveConfirmationTextButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                    CausesValidation="true" OnClientClick="return ClientValidation();" />          
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                    CausesValidation="false" />
            </td>
        </tr>
    </table>
</asp:Content>
