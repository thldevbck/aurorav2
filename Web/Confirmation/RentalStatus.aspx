<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="RentalStatus.aspx.vb" Inherits="Confirmation_RentalStatus" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%">
        <tr>
            <td width="150">
                Status:</td>
            <td>
               <%-- <asp:Label ID="statusLabel" runat="server"></asp:Label>--%>
                <asp:TextBox ID="statusTextBox" runat="server" ReadOnly=true ></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:FormView ID="rentalStatusFormView" runat="server" DefaultMode="edit" Width="100%">
        <EditItemTemplate>
            <table width="100%">
                <tr valign=top >
                    <td width="150" valign=top >
                        Text:</td>
                    <td>
                        <asp:TextBox ID="crsTextTextBox" runat="server" Text='<%# Bind("CrsText") %>' TextMode="MultiLine"
                            Rows="10" width="620" MaxLength="5000"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </EditItemTemplate>
        <InsertItemTemplate>
            <table width="100%">
                <tr>
                    <td width="150" valign=top >
                        Text:</td>
                    <td>
                        <asp:TextBox ID="crsTextTextBox" runat="server" Text='<%# Bind("CrsText") %>' TextMode="MultiLine"
                            Rows="10" width="620"  MaxLength="5000"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </InsertItemTemplate>
    </asp:FormView>
    <br />
    <table width="100%">
        <tr>
            <td align=right >
                <asp:Button ID="saveRentalStatusButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                    CausesValidation="true" />
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                    CausesValidation="false" />
            </td>
        </tr>
    </table>
</asp:Content>
