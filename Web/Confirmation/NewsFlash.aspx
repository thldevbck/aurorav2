<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="NewsFlash.aspx.vb" Inherits="Confirmation_NewsFlash" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
<script language=javascript>

var errorMessage = <%= MessageJSON %>;

function ClientValidation() 
{
    // audienceDropDownList
    var audienceDropDownList;
    audienceDropDownList = document.getElementById('<%=newsFlashFormView.FindControl( "audienceDropDownList").ClientID%>');
    if (audienceDropDownList.options[audienceDropDownList.selectedIndex].text == "")
    {
        setWarningShortMessage(getErrorMessage("GEN099","Audience"))
        return false
    }
    // newflashTextTextBox
    var newflashTextTextBox;
    newflashTextTextBox = document.getElementById('<%=newsFlashFormView.FindControl( "newflashTextTextBox").ClientID%>');
    if (newflashTextTextBox.value == "")
    {
        setWarningShortMessage(getErrorMessage("GEN099","News Flash Text"))
        return false
    }
}
       
        
</script>
        
        
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:FormView ID="newsFlashFormView" runat="server" DefaultMode="edit" Width="100%">
        <EditItemTemplate>
            <asp:CustomValidator ID="serverCustomValidator" runat="server" ErrorMessage="" OnServerValidate="serverCustomValidator_OnServerValidate"></asp:CustomValidator>
            <table width="100%">
                <tr>
                    <td width="150">
                        Audience:</td>
                    <td width="250">
                        <asp:DropDownList ID="audienceDropDownList" runat="server" DataTextField="Audience"
                            DataValueField="Audience" AppendDataBoundItems="true" SelectedValue='<%# Bind("Audience") %>'>
                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Agent" Value="Agent" Selected="False"></asp:ListItem>
                            <asp:ListItem Text="Customer" Value="Customer" Selected="False"></asp:ListItem>
                        </asp:DropDownList>&nbsp;*
                    </td>
                    <td >
                    </td>
                </tr>
                <tr>
                    <td valign=top >
                        Text:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="newflashTextTextBox" runat="server" Text='<%# Bind("NewflashText") %>'
                            TextMode="MultiLine" Width="97%" Rows="10" MaxLength="5000"></asp:TextBox>&nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                        Effective From:
                    </td>
                    <td>
                        <uc1:DateControl ID="effFrDateControl" runat="server" Nullable="false" Date='<%# Bind("EffFr") %>' />&nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                        Effective To:
                    </td>
                    <td>
                        <uc1:DateControl ID="effToDateControl" runat="server" Nullable="false" Date='<%# Bind("EffTo") %>' />&nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </EditItemTemplate>
        <InsertItemTemplate>
            <asp:CustomValidator ID="serverCustomValidator" runat="server" ErrorMessage="" OnServerValidate="serverCustomValidator_OnServerValidate"></asp:CustomValidator>
            <table width="100%">
                <tr>
                    <td width="150">
                        Audience:</td>
                    <td width="250">
                        <asp:DropDownList ID="audienceDropDownList" runat="server" DataTextField="Audience"
                            DataValueField="Audience" AppendDataBoundItems="true" SelectedValue='<%# Bind("Audience") %>'>
                            <asp:ListItem Text="" Value="" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Agent" Value="Agent" Selected="False"></asp:ListItem>
                            <asp:ListItem Text="Customer" Value="Customer" Selected="False"></asp:ListItem>
                        </asp:DropDownList>&nbsp;*
                    </td>
                    <td >
                    </td>
                </tr>
                <tr>
                    <td valign=top >
                        Text:
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="newflashTextTextBox" runat="server" Text='<%# Bind("NewflashText") %>'
                            TextMode="MultiLine" Width="97%" Rows="10" MaxLength="5000"></asp:TextBox>&nbsp;*
                    </td>
                </tr>
                <tr>
                    <td>
                        Effective From:
                    </td>
                    <td>
                        <uc1:DateControl ID="effFrDateControl" runat="server" Nullable="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Effective To:
                    </td>
                    <td>
                        <uc1:DateControl ID="effToDateControl" runat="server" Nullable="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
        </InsertItemTemplate>
    </asp:FormView>
    <table width="100%">
        <tr>
            <td align=right >
                <asp:Button ID="saveNewFlashButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                    CausesValidation="true" OnClientClick="return ClientValidation();" />
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                    CausesValidation="false" />
           
            </td>
        </tr>
    </table>
    
    
</asp:Content>


