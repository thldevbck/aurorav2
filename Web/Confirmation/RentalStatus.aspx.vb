Imports Aurora.Common
Imports Aurora.Confirmation.Services
Imports System.Data

<AuroraPageTitleAttribute("Rental Status")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ConfirmationSetup)> _
<AuroraMessageAttribute("GEN046")> _
Partial Class Confirmation_RentalStatus
    Inherits AuroraPage

    Private crsId As String

    Public Property formCountryCode() As String
        Get
            Return CStr(ViewState("RentalStatus_CountryCode"))
        End Get
        Set(ByVal value As String)
            ViewState("RentalStatus_CountryCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            crsId = Request.QueryString("CrsId").ToString()
            statusTextBox.Text = Request.QueryString("Status").ToString()
            formCountryCode = Request.QueryString("countryCode").ToString()
            ''rev:mia Issue 593: Reservations > Confirmation SetUp - Can't add a News Flash
            If String.IsNullOrEmpty(formCountryCode) Then
                formCountryCode = Me.CountryCode
            End If


            If Not Page.IsPostBack Then
                If Not String.IsNullOrEmpty(crsId) Then
                    rentalStatusFormView.DataSource = Confirmation.GetRentalStatus(crsId)
                    rentalStatusFormView.DataBind()
                    rentalStatusFormView.ChangeMode(FormViewMode.Edit)
                Else
                    rentalStatusFormView.ChangeMode(FormViewMode.Insert)
                End If
            End If
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(".\ConfirmationSetup.aspx?sTxtSearch=" & formCountryCode)
        'Response.Redirect(BackUrl)
    End Sub

    Protected Sub saveRentalStatusButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveRentalStatusButton.Click
        Try

            Dim crsTextTextBox As System.Web.UI.WebControls.TextBox
            crsTextTextBox = rentalStatusFormView.FindControl("crsTextTextBox")

            If Not Aurora.Confirmation.Services.Confirmation.InsertUpdateConfirmationSetUp(formCountryCode, UserCode, crsId, "SAVE", statusTextBox.Text, crsTextTextBox.Text) Then
                Me.SetShortMessage(AuroraHeaderMessageType.Error, "An error occurred while saving the rental status.")
                Return
            Else
                Me.SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
            End If

            'Me.SetShortMessage(AuroraHeaderMessageType.Information, "Saved successfully")
        Catch ex As Exception
            LogException(ex)
            SetErrorMessage("An error occurred while saving the rental status.")
        End Try

    End Sub

End Class
