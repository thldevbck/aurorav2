<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="ConfirmationSetup.aspx.vb" Inherits="ConfirmationSetup_ConfirmationSetup"
    Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <style type="text/css">
        .content-inner
        {
            width: 98%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%">
        <tr>
            <td width="150">
                Country:
            </td>
            <td>
                <%-- <asp:Label ID="countryLabel" runat="server" Text=""></asp:Label>--%>
                <asp:TextBox ID="countryTextBox" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
    </table>
    <hr />
    <b>Rental Status</b>
    <asp:GridView ID="rentalStatusGridView" runat="server" Width="100%" AutoGenerateColumns="False"
        CssClass="dataTable" CellPadding="3" CellSpacing="0">
        <AlternatingRowStyle CssClass="oddRow" VerticalAlign="top" />
        <RowStyle CssClass="evenRow" VerticalAlign="top" />
        <Columns>
            <asp:TemplateField HeaderText="Status">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("Status") %>' NavigateUrl='<%# GetRentalStatusUrl(Eval("CrsId"),Eval("Status")) %>'></asp:HyperLink>
                </ItemTemplate>
                <ItemStyle Wrap="False" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Id" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="crsIdLabel" runat="server" Text='<%# Bind("CrsId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="statusLabel" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Text">
                <ItemTemplate>
                    <asp:Label ID="rntTextLabel" runat="server" Text='<%# Bind("RntText") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:ButtonField Text="Update" CommandName="Update"></asp:ButtonField>--%>
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <table width="100%">
        <tr>
            <td>
                <b>News Flash (Footer)</b>
            </td>
            <td align="right">
                <asp:Button ID="addNewsFlashLinkButton" runat="server" Text="Add News Flash" Width="140"
                    CssClass="Button_Standard Button_Add" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="newsFlashGridView" runat="server" AutoGenerateColumns="False" Width="100%"
        CssClass="dataTable" CellPadding="3" CellSpacing="0">
        <AlternatingRowStyle CssClass="oddRow" VerticalAlign="top" />
        <RowStyle CssClass="evenRow" VerticalAlign="top" />
        <Columns>
            <asp:TemplateField HeaderText="Audience">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("NewflashText") %>'
                        NavigateUrl='<%# GetNewsFlashUrl(Eval("NwfID")) %>'></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Id" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="nwfIdLabel" runat="server" Text='<%# Bind("NwfID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="NwfID" HeaderText="NwfID" ReadOnly="True" SortExpression="NwfID"
                Visible="False" />
            <asp:BoundField DataField="Audience" HeaderText="Audience" ReadOnly="True" SortExpression="Audience"
                Visible="false" />
            <asp:BoundField DataField="NewflashText" HeaderText="Text" ReadOnly="True" SortExpression="NewflashText" />
            <%--<asp:BoundField DataField="EffFr" HeaderText="EffFr" ReadOnly="True" SortExpression="EffFr" />
            <asp:BoundField DataField="EffTo" HeaderText="EffTo" ReadOnly="True" SortExpression="EffTo" />--%>
            <asp:TemplateField HeaderText="Effective From">
                <ItemTemplate>
                    <asp:Label ID="frLabel" runat="server" Text='<%# GetDateFormat(eval("EffFr")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Effective To">
                <ItemTemplate>
                    <asp:Label ID="toLabel" runat="server" Text='<%# GetDateFormat(eval("EffTo")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:ButtonField CommandName="Update" Text="Update" />--%>
            <asp:ButtonField CommandName="Delete" Text="Delete" />
        </Columns>
    </asp:GridView>
    <table id="emptyNewsFlashTable" runat="server" class="dataTable" width="100%">
        <thead>
            <tr>
                <th>
                    Audience
                </th>
                <th>
                    Text
                </th>
                <th>
                    Effective From
                </th>
                <th>
                    Effective To
                </th>
            </tr>
        </thead>
        <tr>
            <td>
                <br />
            </td>
        </tr>
    </table>
    <br />
    <br />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            <b>Confirmation Text</b>
                        </td>
                        <td align="right">
                            <asp:CheckBox ID="showInactiveCheckBox" runat="server" Text="Show Inactive Text"
                                AutoPostBack="True" OnCheckedChanged="showInactiveCheckBox_CheckedChanged"></asp:CheckBox>&nbsp;
                            &nbsp;
                            <asp:Button ID="addConfirmationTextButton" runat="server" Text="Add Text" Width="140"
                                CssClass="Button_Standard Button_Add" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:GridView ID="confirmationTextGridView" 
                          runat="server" 
                          AutoGenerateColumns="False"
                          Width="100%" 
                          CssClass="dataTable" 
                          CellPadding="3" 
                          CellSpacing="0">

                <AlternatingRowStyle CssClass="oddRow" VerticalAlign="top" />
                <RowStyle CssClass="evenRow" VerticalAlign="top"  />
                
                
                <Columns>
                       
                    <asp:TemplateField HeaderText="Type">
                    <ItemStyle  Width="50"  Wrap="true"  HorizontalAlign="Justify"  />
                        <ItemTemplate >
                            <asp:HyperLink ID="HyperLink1" 
                                           runat="server" 
                                           Text='<%# Bind("ConfType") %>' 
                                           NavigateUrl='<%# GetConfirmationTextUrl(Eval("NtsId")) %>'
                                           Width="200">
                            </asp:HyperLink>
                        
                                   
                                
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField Visible="False" HeaderText="Id">
                        <ItemTemplate>
                            <asp:Label ID="ntsIdLabel" 
                                       runat="server" 
                                       Text='<%# Bind("NtsId") %>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField ReadOnly="True" 
                                    DataField="NtsId" 
                                    Visible="False" 
                                    SortExpression="NtsId"
                                    HeaderText="NtsId">
                     </asp:BoundField>

                    <asp:BoundField ReadOnly="True" 
                                    DataField="ConfType" 
                                    Visible="false" 
                                    SortExpression="ConfType"
                                    HeaderText="Type">
                    </asp:BoundField>
                    
                    <%--rev:mia Oct. 4 2012 - change this to itemtemplate--%>
                    <asp:TemplateField HeaderText="Text">
                        <ItemStyle  Width="200" HorizontalAlign="Justify" />
                        <ItemTemplate>
                            <asp:Label runat="server" 
                                       Width="600" 
                                       Text='<%# Bind("ConfText") %>' 
                                       ID="lblConfText" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:CheckBoxField ReadOnly="True" 
                                       DataField="ConfHeader" 
                                       SortExpression="ConfHeader"
                                       HeaderText="Header">
                    </asp:CheckBoxField>

                    <asp:CheckBoxField ReadOnly="True" 
                                       DataField="ConfFooter" 
                                       SortExpression="ConfFooter"
                                       HeaderText="Footer">
                    </asp:CheckBoxField>
                    
                    <asp:BoundField ReadOnly="True" 
                                    DataField="ConfOrder" 
                                    SortExpression="ConfOrder"
                                    HeaderText="Order">
                     </asp:BoundField>
                    
                     <asp:CheckBoxField ReadOnly="True" 
                                        DataField="IsCus" 
                                        SortExpression="IsCus" 
                                        HeaderText="DefCus">
                    </asp:CheckBoxField>

                    <asp:CheckBoxField ReadOnly="True" 
                                       DataField="IsAgn" 
                                       SortExpression="IsAgn" 
                                       HeaderText="DefAgt">
                    </asp:CheckBoxField>

                    <asp:CheckBoxField ReadOnly="True" 
                                       DataField="ConfActive" 
                                       SortExpression="ConfActive"
                                       HeaderText="Active">
                     </asp:CheckBoxField>

                    
                    <%--rev:mia Oct. 4 2012 - added dropdown control with brandcode--%>
                    <asp:TemplateField HeaderText="Brand">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlBrandCode" runat="server" AutoPostBack="false" Width="100"
                                Enabled="false" />
                            <asp:Label runat="server" Text="" ID="labelBrandCode" />
                                  
                        </ItemTemplate>

                    </asp:TemplateField>

                    
                </Columns>
            </asp:GridView>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="showInactiveCheckBox" EventName="CheckedChanged">
            </asp:AsyncPostBackTrigger>
        </Triggers>
    </asp:UpdatePanel>
    <br />
</asp:Content>
