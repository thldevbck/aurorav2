Imports Aurora.Common
Imports Aurora.Confirmation.Services
Imports System.Data

<AuroraPageTitleAttribute("Confirmation Text")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ConfirmationSetup)> _
<AuroraMessageAttribute("GEN045,GEN046")> _
Partial Class Confirmation_ConfirmationText
    Inherits AuroraPage

    Private ntsId As String

    Public Property formCountryCode() As String
        Get
            Return CStr(ViewState("ConfText_CountryCode"))
        End Get
        Set(ByVal value As String)
            ViewState("ConfText_CountryCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            ntsId = Request.QueryString("NtsId").ToString()
            formCountryCode = Request.QueryString("countryCode").ToString()
            ''rev:mia Issue 593: Reservations > Confirmation SetUp - Can't add a News Flash
            If String.IsNullOrEmpty(formCountryCode) Then
                formCountryCode = Me.CountryCode
            End If

            If Not Page.IsPostBack Then
                ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
                If (_BrandVehicles Is Nothing) Then
                    _BrandVehicles = InitBrandContol()
                End If


                Dim ds As DataSet
                If Not String.IsNullOrEmpty(ntsId) Then
                    ds = Confirmation.GetConfirmationText(formCountryCode, UserCode, False, ntsId)
                    confirmationTextFormView.DataSource = ds.Tables(1)
                    confirmationTextFormView.DataBind()
                    confirmationTextFormView.ChangeMode(FormViewMode.Edit)

                Else
                    confirmationTextFormView.ChangeMode(FormViewMode.Insert)
                End If

                Dim ddlBrandCode As DropDownList = DirectCast(confirmationTextFormView.FindControl("ddlBrandCode"), DropDownList)
                Try
                    ddlBrandCode.Items.Clear()
                    ddlBrandCode.Items.Add("")
                    ddlBrandCode.AppendDataBoundItems = True

                    ddlBrandCode.DataSource = BrandVehicles
                    ddlBrandCode.DataTextField = "BrdName"
                    ddlBrandCode.DataValueField = "BrdCode"
                    ddlBrandCode.DataBind()

                    If (Not ds Is Nothing) Then
                        If (Not ds.Tables(1).Columns("NtsBrdCode") Is DBNull.Value) Then
                            ddlBrandCode.SelectedValue = ds.Tables(1).Rows(0)("ntsbrdcode").ToString()
                        End If
                    End If
                    


                Catch ex As Exception

                End Try
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(".\ConfirmationSetup.aspx?sTxtSearch=" & formCountryCode)
        'Response.Redirect(BackUrl)
    End Sub

    Protected Sub saveConfirmationTextButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveConfirmationTextButton.Click
        Try
            If Page.IsValid Then

                Dim typeTextBox As System.Web.UI.WebControls.TextBox
                typeTextBox = confirmationTextFormView.FindControl("typeTextBox")
                Dim textTextBox As System.Web.UI.WebControls.TextBox
                textTextBox = confirmationTextFormView.FindControl("textTextBox")
                Dim headerRadioButtonList As System.Web.UI.WebControls.RadioButtonList
                headerRadioButtonList = confirmationTextFormView.FindControl("headerRadioButtonList")
                Dim orderTextBox As System.Web.UI.WebControls.TextBox
                orderTextBox = confirmationTextFormView.FindControl("orderTextBox")
                Dim isCusCheckBox As System.Web.UI.WebControls.CheckBox
                isCusCheckBox = confirmationTextFormView.FindControl("isCusCheckBox")
                Dim isAgnCheckBox As System.Web.UI.WebControls.CheckBox
                isAgnCheckBox = confirmationTextFormView.FindControl("isAgnCheckBox")
                Dim activeCheckBox As System.Web.UI.WebControls.CheckBox
                activeCheckBox = confirmationTextFormView.FindControl("activeCheckBox")

                ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
                Dim ddlBrandCode As DropDownList = DirectCast(confirmationTextFormView.FindControl("ddlBrandCode"), DropDownList)

                Dim errorMessage As String

                Dim order As Integer = Utility.ParseInt(orderTextBox.Text, 0)

                If confirmationTextFormView.CurrentMode = FormViewMode.Edit Then
                    'update
                    errorMessage = Aurora.Confirmation.Services.Confirmation.UpdateConfirmationText(formCountryCode, UserCode, ntsId, typeTextBox.Text, headerRadioButtonList.SelectedValue, textTextBox.Text, order, isCusCheckBox.Checked, isAgnCheckBox.Checked, activeCheckBox.Checked, "SAVE", ddlBrandCode.SelectedItem.Value)

                    If Not String.IsNullOrEmpty(errorMessage) Then
                        'SetErrorMessage(errorMessage)
                        Me.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                        Return
                    Else
                        Me.SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
                    End If
                ElseIf confirmationTextFormView.CurrentMode = FormViewMode.Insert Then
                    'insert
                    errorMessage = Aurora.Confirmation.Services.Confirmation.InsertConfirmationText(formCountryCode, UserCode, typeTextBox.Text, headerRadioButtonList.SelectedValue, textTextBox.Text, order, isCusCheckBox.Checked, isAgnCheckBox.Checked, activeCheckBox.Checked, "SAVE", ddlBrandCode.SelectedItem.Value)

                    If Not String.IsNullOrEmpty(errorMessage) Then
                        'SetErrorMessage(errorMessage)
                        Me.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                        Return
                    Else
                        Me.SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN045"))
                    End If
                End If
                'Me.SetShortMessage(AuroraHeaderMessageType.Information, "Saved successfully")
            End If

        Catch ex As Exception
            LogException(ex)
            SetErrorMessage("An error occurred while updating the confirmation text.")
        End Try
    End Sub

    Protected Sub CustomValidator1_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        Try
            'Rental server validation event
            Dim textTextBox As System.Web.UI.WebControls.TextBox
            textTextBox = confirmationTextFormView.FindControl("textTextBox")

            If textTextBox.Text.Length > 5000 Then
                e.IsValid = False
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "The text cannot be longer than 5000 characters.")
            Else
                e.IsValid = True
            End If
        Catch ex As Exception
            LogException(ex)
            SetErrorMessage("An error occurred while validating the page.")
        End Try
    End Sub

#Region "rev:mia Oct.4 2012 - Confirmation Setup addition of brand"
    Private _BrandVehicles As DataTable
    Private ReadOnly Property BrandVehicles As DataTable
        Get
            Return _BrandVehicles
        End Get
    End Property

    Function InitBrandContol() As DataTable
        Dim arrDataTables As DataTable()
        arrDataTables = Aurora.Admin.Data.GetBrandsAndVehicleClasses(CompanyCode)
        Return arrDataTables(1)
    End Function

#End Region
End Class
