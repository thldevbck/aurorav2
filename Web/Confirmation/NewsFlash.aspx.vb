Imports Aurora.Common
Imports Aurora.Confirmation.Services
Imports System.Data
Imports System.Xml

<AuroraPageTitleAttribute("News Flash")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ConfirmationSetup)> _
<AuroraMessageAttribute("GEN121,GEN062,GEN099,GEN045,GEN046")> _
Partial Class Confirmation_NewsFlash
    Inherits AuroraPage

    Private nwfId As String

    Public Property formCountryCode() As String
        Get
            Return CStr(ViewState("NewsFlash_CountryCode"))
        End Get
        Set(ByVal value As String)
            ViewState("NewsFlash_CountryCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            nwfId = Request.QueryString("nwfId").ToString()
            formCountryCode = Request.QueryString("countryCode").ToString()
            ''rev:mia Issue 593: Reservations > Confirmation SetUp - Can't add a News Flash
            If String.IsNullOrEmpty(formCountryCode) Then
                formCountryCode = Me.CountryCode
            End If

            'getMsgToClient("GEN121,GEN062,GEN099")

            If Not Page.IsPostBack Then
                If Not String.IsNullOrEmpty(nwfId) Then
                    newsFlashFormView.DataSource = Confirmation.GetNewsFlash(nwfId)
                    newsFlashFormView.DataBind()
                    newsFlashFormView.ChangeMode(FormViewMode.Edit)
                Else
                    newsFlashFormView.ChangeMode(FormViewMode.Insert)
                    Dim effFrDateControl As UserControls_DateControl
                    effFrDateControl = newsFlashFormView.FindControl("effFrDateControl")
                    effFrDateControl.Date = Today
                End If
            End If
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub serverCustomValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)

        Dim effFrDateControl As UserControls_DateControl
        Dim effToDateControl As UserControls_DateControl
        effFrDateControl = newsFlashFormView.FindControl("effFrDateControl")
        effToDateControl = newsFlashFormView.FindControl("effToDateControl")

        If Not (effFrDateControl.IsValid And effToDateControl.IsValid) Then
            e.IsValid = False
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
            Return
        End If

        If String.IsNullOrEmpty(effFrDateControl.Text) Then
            e.IsValid = False
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN099", "Effective from date"))
            Return
        End If

        If String.IsNullOrEmpty(effToDateControl.Text) Then
            e.IsValid = False
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN099", "Effective to date"))
            'Dim errorMessage As String = errorMessageCollection.Item("GEN099")
            'errorMessage = errorMessage.Replace("xxx1", "Effective to date")
            'SetErrorMessage(errorMessage)
            'SetErrorMessage("GEN099 - Effective to date is mandatory.")
            Return
        End If

        If effFrDateControl.Date > effToDateControl.Date Then
            e.IsValid = False
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN062"))
            'SetErrorMessage("GEN062 - To Date cannot be less than From Date.")
        End If

        If effToDateControl.Date <= Date.Today Then
            e.IsValid = False
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "To Date cannot be equal or less than Today.")
        End If

    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(".\ConfirmationSetup.aspx?sTxtSearch=" & formCountryCode)
        'Response.Redirect(BackUrl)
    End Sub

    Protected Sub saveNewFlashButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveNewFlashButton.Click
        Try
            If Page.IsValid Then

                Dim audienceDropDownList As System.Web.UI.WebControls.DropDownList
                audienceDropDownList = newsFlashFormView.FindControl("audienceDropDownList")
                Dim newflashTextTextBox As System.Web.UI.WebControls.TextBox
                newflashTextTextBox = newsFlashFormView.FindControl("newflashTextTextBox")
                Dim effFrDateControl As UserControls_DateControl
                effFrDateControl = newsFlashFormView.FindControl("effFrDateControl")
                Dim effToDateControl As UserControls_DateControl
                effToDateControl = newsFlashFormView.FindControl("effToDateControl")
                'Dim effFrTextBox As System.Web.UI.WebControls.TextBox
                'effFrTextBox = newsFlashFormView.FindControl("effFrTextBox")
                'Dim effToTextBox As System.Web.UI.WebControls.TextBox
                'effToTextBox = newsFlashFormView.FindControl("effToTextBox")
                Dim effFr As DateTime = effFrDateControl.Date
                Dim effTo As DateTime = effToDateControl.Date

                Dim errorMessage As String

                If newsFlashFormView.CurrentMode = FormViewMode.Edit Then
                    'update
                    errorMessage = Aurora.Confirmation.Services.Confirmation.UpdateNewsFlash(formCountryCode, UserCode, nwfId, audienceDropDownList.SelectedValue, effFr, effTo, newflashTextTextBox.Text)

                    If Not String.IsNullOrEmpty(errorMessage) Then
                        Me.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                        Return
                    Else
                        Me.SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
                    End If
                ElseIf newsFlashFormView.CurrentMode = FormViewMode.Insert Then
                    'insert
                    errorMessage = Aurora.Confirmation.Services.Confirmation.InsertNewsFlash(formCountryCode, UserCode, "SAVE", audienceDropDownList.SelectedValue, effFr, effTo, newflashTextTextBox.Text)

                    If Not String.IsNullOrEmpty(errorMessage) Then
                        Me.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                        Return
                    Else
                        Me.SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN045"))
                    End If
                End If
                'Me.SetShortMessage(AuroraHeaderMessageType.Information, "Saved successfully")
            End If

        Catch ex As Exception
            LogException(ex)
            SetErrorMessage("An error occurred while updating the news flash.")
        End Try
    End Sub

    'Private Sub getMsgToClient(ByVal errorCode As String)

    '    'Write error message to client
    '    Dim errorMessageXmlDoc As XmlDocument = New XmlDocument
    '    Aurora.Common.Data.GetMsgToClient(errorCode, errorMessageXmlDoc, errorMessageCollection)

    'End Sub

    'Private Function getMessage(ByVal errorCode As String, ByVal ParamArray params() As Object) As String

    '    Dim errorMessage As String = errorMessageCollection.Item(errorCode)

    '    Dim i As Integer = 1

    '    For Each o As String In params
    '        errorMessage = errorMessage.Replace("xxx" & Utility.ParseString(i), o)
    '        i = i + 1
    '    Next

    '    For j As Integer = 1 To 6
    '        errorMessage = errorMessage.Replace("xxx" & Utility.ParseString(j), "")
    '    Next j
    '    Return errorMessage

    'End Function


    'Public ReadOnly Property MessageJSON() As String
    '    Get
    '        Dim message As String
    '        message = Me.Message
    '        Dim messageArray As String()
    '        messageArray = message.Split(",")

    '        Dim result As String
    '        result = "{ "
    '        For Each msg As String In messageArray
    '            Dim errorMessage As String = errorMessageCollection.Item(msg)
    '            result = result & "'" + msg & "' : '" + errorMessage + "' ,"
    '        Next

    '        result = result.Substring(0, result.Length - 1)
    '        result = result & "}"
    '        Return result.ToString()

    '    End Get
    'End Property

End Class
