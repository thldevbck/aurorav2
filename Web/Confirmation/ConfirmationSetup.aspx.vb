Imports Aurora.Common
Imports Aurora.Confirmation.Services
Imports System.Data

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ConfirmationSetup)> _
<AuroraMessageAttribute("GEN047")> _
Partial Class ConfirmationSetup_ConfirmationSetup
    Inherits AuroraPage

    Public Property formCountryCode() As String
        Get
            Return CStr(ViewState("ConfSetup_CountryCode"))
        End Get
        Set(ByVal value As String)
            ViewState("ConfSetup_CountryCode") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try


            ClearMessages()

            If Not PageScriptManager.IsInAsyncPostBack Then
                If Not Page.IsPostBack Then

                    If Not String.IsNullOrEmpty(Me.SearchParam) Then
                        formCountryCode = Me.SearchParam.ToUpper
                    Else
                        formCountryCode = CountryCode
                    End If

                    
                End If
            End If

            If Not Page.IsPostBack Then
                ''rev:mia Oct.4 2012 - Confirmation Setup addition of brand
                If (_BrandVehicles Is Nothing) Then
                    _BrandVehicles = InitBrandContol()
                End If

                confirmation_DataBind()
                confirmationTextGridView_DataBind()
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub confirmation_DataBind()
        Try
            Dim ds As DataSet = Confirmation.GetConfirmationSetUp(UserCode, formCountryCode)
            'countryLabel.Text = ds.Tables(0).Rows(0)("Country")
            countryTextBox.Text = ds.Tables(0).Rows(0)("Country")

            ' RentanlStatus 
            rentalStatusGridView.DataSource = ds.Tables(1)
            rentalStatusGridView.DataBind()

            ' NewsFlash
            newsFlashGridView.DataSource = ds.Tables(2) 'newsFlashDataTable
            newsFlashGridView.DataBind()

            If ds.Tables(2).Rows.Count = 0 Then
                emptyNewsFlashTable.Visible = True
            Else
                emptyNewsFlashTable.Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub confirmationTextGridView_DataBind()
        Try
            Dim ds As DataSet
            ds = Confirmation.GetConfirmationText(formCountryCode, UserCode, Not showInactiveCheckBox.Checked)
            Dim dt As DataTable = ds.Tables(1)
            dt.Rows(dt.Rows.Count - 1).Delete()

            For i As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(i)("ConfHeader") = 1 Then
                    dt.Rows(i)("ConfHeader") = True
                Else
                    dt.Rows(i)("ConfHeader") = False
                End If
                If dt.Rows(i)("ConfFooter") = 1 Then
                    dt.Rows(i)("ConfFooter") = True
                Else
                    dt.Rows(i)("ConfFooter") = False
                End If
                If dt.Rows(i)("IsCus") = 1 Then
                    dt.Rows(i)("IsCus") = True
                Else
                    dt.Rows(i)("IsCus") = False
                End If
                If dt.Rows(i)("IsAgn") = 1 Then
                    dt.Rows(i)("IsAgn") = True
                Else
                    dt.Rows(i)("IsAgn") = False
                End If
                If dt.Rows(i)("ConfActive") = 1 Then
                    dt.Rows(i)("ConfActive") = True
                Else
                    dt.Rows(i)("ConfActive") = False
                End If
            Next

            confirmationTextGridView.DataSource = dt
            confirmationTextGridView.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub rentalStatusGridView_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles rentalStatusGridView.RowUpdating
    '    Dim rentalStatusGridViewRow As GridViewRow
    '    rentalStatusGridViewRow = rentalStatusGridView.Rows(e.RowIndex)

    '    Dim crsIdLabel As System.Web.UI.WebControls.Label
    '    crsIdLabel = rentalStatusGridViewRow.FindControl("crsIdLabel")

    '    Dim statusLabel As System.Web.UI.WebControls.Label
    '    statusLabel = rentalStatusGridViewRow.FindControl("statusLabel")

    '    'referenceCurrentPageAsBackUrl()
    '    Response.Redirect(".\RentalStatus.aspx?CrsId=" & crsIdLabel.Text & "&Status=" & statusLabel.Text)
    'End Sub

    Protected Function GetRentalStatusUrl(ByVal crsId As Object, ByVal status As Object) As String
        Dim crsIdString As String
        crsIdString = Convert.ToString(crsId)
        Dim statusString As String
        statusString = Convert.ToString(status)
        Return "~/Confirmation/RentalStatus.aspx?CrsId=" & crsIdString & "&Status=" & statusString & "&countryCode=" & formCountryCode
    End Function


    Protected Sub newsFlashGridView_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles newsFlashGridView.RowUpdating
        Dim newsFlashGridViewRow As GridViewRow
        newsFlashGridViewRow = newsFlashGridView.Rows(e.RowIndex)
        'Dim nwfId As String = newsFlashGridViewRow.Cells(0).Text
        Dim nwfIdLabel As System.Web.UI.WebControls.Label
        nwfIdLabel = newsFlashGridViewRow.FindControl("nwfIdLabel")

        'referenceCurrentPageAsBackUrl()
        Response.Redirect(".\NewsFlash.aspx?nwfId=" & nwfIdLabel.Text & "&countryCode=" & formCountryCode)
    End Sub

    Protected Function GetNewsFlashUrl(ByVal nwfId As Object) As String
        Dim nwfIdString As String
        nwfIdString = Convert.ToString(nwfId)
        Return "~/Confirmation/NewsFlash.aspx?nwfId=" & nwfIdString & "&countryCode=" & formCountryCode
    End Function

    Protected Sub newsFlashGridView_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles newsFlashGridView.RowDeleting
        Try
            Dim newsFlashGridViewRow As GridViewRow
            newsFlashGridViewRow = newsFlashGridView.Rows(e.RowIndex)
            'Dim nwfId As String = newsFlashGridViewRow.Cells(0).Text
            Dim nwfIdLabel As System.Web.UI.WebControls.Label
            nwfIdLabel = newsFlashGridViewRow.FindControl("nwfIdLabel")

            Dim errorMessage As String
            errorMessage = Aurora.Confirmation.Services.Confirmation.DeleteNewsFlash(nwfIdLabel.Text)

            If Not String.IsNullOrEmpty(errorMessage) Then
                'SetErrorMessage(errorMessage)
                Me.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return
            End If

            'Delete row
            confirmation_DataBind()

            'Me.SetShortMessage(AuroraHeaderMessageType.Information, "Deleted successfully")
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN047"))

        Catch ex As Exception
            LogException(ex)
            SetErrorMessage("An error occurred while deleting the news flash.")
        End Try
    End Sub

    Protected Sub addNewsFlashLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addNewsFlashLinkButton.Click
        'referenceCurrentPageAsBackUrl()
        Response.Redirect(".\NewsFlash.aspx?nwfId=" & "&countryCode=" & formCountryCode)
    End Sub

   

    Protected Sub confirmationTextGridView_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles confirmationTextGridView.RowUpdating
        Dim confirmationTextGridViewRow As GridViewRow
        confirmationTextGridViewRow = confirmationTextGridView.Rows(e.RowIndex)

        Dim ntsIdLabel As System.Web.UI.WebControls.Label
        ntsIdLabel = confirmationTextGridViewRow.FindControl("ntsIdLabel")

        'Dim nwfId As String = confirmationTextGridViewRow.Cells(0).Text
        'referenceCurrentPageAsBackUrl()
        Response.Redirect(".\ConfirmationText.aspx?NtsId=" & ntsIdLabel.Text & "&countryCode=" & formCountryCode)
    End Sub

    Protected Function GetConfirmationTextUrl(ByVal ntsId As Object) As String
        Dim ntsIdString As String
        ntsIdString = Convert.ToString(ntsId)
        Return "~/Confirmation/ConfirmationText.aspx?NtsId=" & ntsIdString & "&countryCode=" & formCountryCode
    End Function

    Protected Sub addConfirmationTextButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addConfirmationTextButton.Click
        'referenceCurrentPageAsBackUrl()
        Response.Redirect(".\ConfirmationText.aspx?NtsId=" & "&countryCode=" & formCountryCode)
    End Sub

    Protected Sub showInactiveCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        confirmationTextGridView_DataBind()

    End Sub

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString)
        Else
            Return ""
        End If
    End Function

#Region "rev:mia Oct.4 2012 - Confirmation Setup addition of brand"
    Private _BrandVehicles As DataTable
    Private ReadOnly Property BrandVehicles As DataTable
        Get
            Return _BrandVehicles
        End Get
    End Property

    Function InitBrandContol() As DataTable
        Dim arrDataTables As DataTable()
        arrDataTables = Aurora.Admin.Data.GetBrandsAndVehicleClasses(CompanyCode)
        Return arrDataTables(1)
    End Function
    Protected Sub confirmationTextGridView_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles confirmationTextGridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlBrandCode As DropDownList = DirectCast(e.Row.FindControl("ddlBrandCode"), DropDownList)
                Dim labelBrandCode As Label = DirectCast(e.Row.FindControl("labelBrandCode"), Label)

                ddlBrandCode.Items.Clear()
                ddlBrandCode.Items.Add("")
                ddlBrandCode.AppendDataBoundItems = True

                ddlBrandCode.DataSource = BrandVehicles
                ddlBrandCode.DataTextField = "BrdName"
                ddlBrandCode.DataValueField = "BrdCode"
                ddlBrandCode.DataBind()

                Try
                    If (Not DataBinder.Eval(e.Row.DataItem, "NtsBrdCode") Is DBNull.Value) Then
                        ddlBrandCode.SelectedValue = DataBinder.Eval(e.Row.DataItem, "NtsBrdCode")
                        ddlBrandCode.Visible = False
                        labelBrandCode.Text = ddlBrandCode.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "NtsBrdCode")).Text
                    End If

                Catch ex As Exception

                End Try

            End If
        End If
    End Sub

#End Region
End Class

