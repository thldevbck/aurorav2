Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data

Partial Class TableEditor_Edit
    Inherits AuroraPage

    Private _tableEditorDataSet As TableEditorDataSet
    Private _tableEditorDataSetValidationException As Exception

    Public Overrides ReadOnly Property FunctionCode() As String
        Get
            If _tableEditorDataSet IsNot Nothing Then
                Return _tableEditorDataSet.RootFunction.tefFunCode
            Else
                Return AuroraFunctionCodeAttribute.General
            End If
        End Get
    End Property

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            If _tableEditorDataSet IsNot Nothing AndAlso _tableEditorDataSet.CurrentEntity IsNot Nothing Then
                Return "Edit " & _tableEditorDataSet.CurrentEntity.teeName
            ElseIf _tableEditorDataSet IsNot Nothing AndAlso _tableEditorDataSet.RootEntity IsNot Nothing Then
                Return _tableEditorDataSet.RootEntity.teeName
            Else
                Return "Table Editor"
            End If
        End Get
    End Property

    Private Sub InitForm()

        Dim environmentParams As New Dictionary(Of String, Object)

        environmentParams.Add("@Environment_UserId", UserSettings.Current.UsrId)
        environmentParams.Add("@Environment_UserCode", UserSettings.Current.UsrCode)
        environmentParams.Add("@Environment_UserName", UserSettings.Current.UsrName)
        environmentParams.Add("@Environment_ComCode", UserSettings.Current.ComCode)
        environmentParams.Add("@Environment_ComName", UserSettings.Current.ComName)
        environmentParams.Add("@Environment_CountryCode", UserSettings.Current.CtyCode)
        environmentParams.Add("@Environment_CountryName", UserSettings.Current.CtyName)
        environmentParams.Add("@Environment_LocationCode", UserSettings.Current.LocCode)
        environmentParams.Add("@Environment_LocationName", UserSettings.Current.LocName)
        environmentParams.Add("@Environment_TownCityCode", UserSettings.Current.TctCode)
        environmentParams.Add("@Environment_TownCityName", UserSettings.Current.TctName)
        environmentParams.Add("@Environment_PrgmName", UserSettings.Current.PrgmName)

        _tableEditorDataSet.LoadDataFromParams(Request.QueryString, environmentParams)

        If _tableEditorDataSet.CurrentEntity Is Nothing Then

            locationControl.SetEntity(_tableEditorDataSet.RootEntity)

            Dim listControl As TableEditor_ListControl = Me.LoadControl("ListControl.ascx")
            tableEditorPanel.Controls.Add(listControl)
            listControl.SetEntity(_tableEditorDataSet.RootEntity)

            tableEditorPanel.Controls.Add(New LiteralControl("<br/>"))

            If _tableEditorDataSet.RootEntity.IsInsertable Then

                Dim insertControl As TableEditor_InsertControl = Me.LoadControl("InsertControl.ascx")
                tableEditorPanel.Controls.Add(insertControl)
                insertControl.SetEntity(_tableEditorDataSet.RootEntity)

                tableEditorPanel.Controls.Add(New LiteralControl("<br/>"))
            End If
        Else
            If _tableEditorDataSet.CurrentEntity.DataRow Is Nothing Then
                Me.AddErrorMessage(_tableEditorDataSet.CurrentEntity.teeName & " not found")
                Return
            End If

            locationControl.SetEntity(_tableEditorDataSet.CurrentEntity)

            Dim editControl As TableEditor_EditControl = Me.LoadControl("EditControl.ascx")
            tableEditorPanel.Controls.Add(editControl)
            editControl.SetEntity(_tableEditorDataSet.CurrentEntity)

            tableEditorPanel.Controls.Add(New LiteralControl("<br/>"))

            For Each entity As TableEditorEntityRow In _tableEditorDataSet.CurrentEntity.GetTableEditorEntityRows()
                Dim listControl As TableEditor_ListControl = Me.LoadControl("ListControl.ascx")
                tableEditorPanel.Controls.Add(listControl)
                listControl.SetEntity(entity)

                tableEditorPanel.Controls.Add(New LiteralControl("<br/>"))

                If entity.IsInsertable Then
                    Dim insertControl As TableEditor_InsertControl = Me.LoadControl("InsertControl.ascx")
                    tableEditorPanel.Controls.Add(insertControl)
                    insertControl.SetEntity(entity)

                    tableEditorPanel.Controls.Add(New LiteralControl("<br/>"))
                End If
            Next
        End If
    End Sub

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            _tableEditorDataSet = TableEditorDataSet.LoadFromParams(Request.QueryString)
        Catch ex As Exception
            _tableEditorDataSet = Nothing
            _tableEditorDataSetValidationException = ex
        End Try

        MyBase.OnInit(e)

        If _tableEditorDataSetValidationException Is Nothing AndAlso _tableEditorDataSet IsNot Nothing Then
            Try
                InitForm()
            Catch ex As Exception
                _tableEditorDataSetValidationException = ex
            End Try
        End If

        If _tableEditorDataSetValidationException IsNot Nothing Then
            Me.AddErrorMessage(_tableEditorDataSetValidationException.Message)
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Me.IsPostBack AndAlso Not String.IsNullOrEmpty(Request.QueryString("message")) Then
            Me.AddInformationMessage(Request.QueryString("message"))
        End If
    End Sub
End Class
