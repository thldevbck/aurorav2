<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ImportExport.aspx.vb" Inherits="TableEditor_ImportExport" MasterPageFile="~/Include/AuroraHeader.master" validateRequest="false" %>
<%-- requestValidate is false because data can contain "<" and ">" which trips the validation --%>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    
    <asp:Panel ID="tableEditorPanel" runat="Server" Width="780px" CssClass="tableeditor" >
        <p><b>Import / Export</b></p>

        <table class="tableeditor" style="width:100%">
            <tr>
                <td style="width: 150px">Function:</td>
                <td>
                    <asp:DropDownList ID="functionDropDown" runat="server" width="500px" AutoPostBack="True" />
                </td>
            </tr>
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr>
                <td>Xml:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox ID="xmlTextBox" runat="server" Width="100%" TextMode="MultiLine" Rows="20" Wrap="False" Font-Names="Courier New" spellcheck="false" />
                </td>
            </tr>
            <tr>
                <td colspan="2"  align="right">
                    <asp:Button ID="deleteButton" runat="server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                &nbsp;&nbsp;
                    <asp:Button ID="importButton" runat="server" Text="Import" CssClass="Button_Standard Button_Save" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
</asp:Content>