Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data

<AuroraFunctionCodeAttribute("TABLEEDITOR")> _
<AuroraPageTitleAttribute("Table Editor Maintenance")> _
Partial Class TableEditor_Maintain
    Inherits AuroraPage

    Private _functionCodesTable As DataTable
    Private _functionDataTable As TableEditorFunctionDataTable = Nothing
    Private _selectedFunction As TableEditorDataSet = Nothing
    Private _columnInformation As ColumnInformationDataTable = Nothing
    Private _tableNamesTable As DataTable

    Private Sub BuildFunctionCodes(ByVal useViewState As Boolean)
        Dim selectedValue As String = functionCodeDropDown.SelectedValue

        _functionCodesTable = DataRepository.GetFunctionCodes()
        functionCodeDropDown.DataSource = _functionCodesTable
        functionCodeDropDown.DataValueField = "value"
        functionCodeDropDown.DataTextField = "text"
        functionCodeDropDown.DataBind()

        If useViewState AndAlso functionCodeDropDown.Items.FindByValue(selectedValue) IsNot Nothing Then functionCodeDropDown.SelectedValue = selectedValue
    End Sub


    Private Sub BuildFunctions(ByVal useViewState As Boolean)
        Dim functionDropDown_selectedValue As String = functionDropDown.SelectedValue
        functionDropDown.SelectedValue = Nothing

        _functionDataTable = DataRepository.GetFunctions()

        functionDropDown.DataSource = _functionDataTable
        functionDropDown.DataValueField = "tefId"
        functionDropDown.DataTextField = "tefCode"
        functionDropDown.DataBind()

        If useViewState AndAlso functionDropDown.Items.FindByValue(functionDropDown_selectedValue) IsNot Nothing Then functionDropDown.SelectedValue = functionDropDown_selectedValue

        If functionDropDown.SelectedItem IsNot Nothing AndAlso Not String.IsNullOrEmpty(functionDropDown.SelectedItem.Text) Then
            _selectedFunction = DataRepository.GetTableEditor(functionDropDown.SelectedItem.Text)
        End If

        While functionsTable.Rows.Count > 1
            functionsTable.Rows.RemoveAt(1)
        End While
        Dim index As Integer = 0
        For Each functionRow As TableEditorFunctionRow In _functionDataTable
            Dim tableRow As New TableRow()
            tableRow.CssClass = IIf((index Mod 2) = 0, "evenRow", "oddRow")
            functionsTable.Rows.Add(tableRow)

            Dim codeCell As New TableCell()
            tableRow.Cells.Add(codeCell)
            Dim hyperLink As New HyperLink()
            hyperLink.NavigateUrl = "Edit.aspx" & functionRow.QueryStringParams
            hyperLink.Text = functionRow.tefCode
            codeCell.Controls.Add(hyperLink)

            Dim functionCodeCell As New TableCell()
            functionCodeCell.Text = Server.HtmlEncode(functionRow.tefFunCode)
            tableRow.Cells.Add(functionCodeCell)

            Dim nameCell As New TableCell()
            nameCell.Text = Server.HtmlEncode(functionRow.tefName)
            tableRow.Cells.Add(nameCell)

            index += 1
        Next
    End Sub

    Private Sub BuildParentEntities(ByVal useViewState As Boolean)

        Dim selectedValue As String = parentEntityDropDown.SelectedValue

        parentEntityDropDown.Items.Clear()
        parentEntityDropDown.Items.Add("")
        If _selectedFunction IsNot Nothing Then
            parentEntityDropDown.DataSource = _selectedFunction.TableEditorEntity
            parentEntityDropDown.DataValueField = "teeId"
            parentEntityDropDown.DataTextField = "teeName"
            parentEntityDropDown.AppendDataBoundItems = True
            parentEntityDropDown.DataBind()
        End If

        If useViewState AndAlso parentEntityDropDown.Items.FindByValue(selectedValue) IsNot Nothing Then parentEntityDropDown.SelectedValue = selectedValue
    End Sub

    Private Sub BuildTableNames(ByVal useViewState As Boolean)
        Dim selectedValue As String = tableNameDropDown.SelectedValue

        _tableNamesTable = DataRepository.GetTableNames()
        tableNameDropDown.DataSource = _tableNamesTable
        tableNameDropDown.DataValueField = "value"
        tableNameDropDown.DataTextField = "text"
        tableNameDropDown.DataBind()

        If useViewState AndAlso tableNameDropDown.Items.FindByValue(selectedValue) IsNot Nothing Then tableNameDropDown.SelectedValue = selectedValue

        If Not useViewState And tableNameDropDown.SelectedItem IsNot Nothing Then entityNameTextBox.Text = TableEditorDataSet.NiceName(tableNameDropDown.SelectedItem.Text)
    End Sub

    Private Sub BuildColumnInformation(ByVal useViewState As Boolean)
        While columnsTable.Rows.Count > 1
            columnsTable.Rows.RemoveAt(1)
        End While
        If tableNameDropDown.SelectedItem Is Nothing Then Return

        Dim tableName As String = tableNameDropDown.SelectedItem.Text
        _columnInformation = DataRepository.GetColumnInformation(tableName)

        Dim index As Integer = 0
        For Each _columnInformationRow As ColumnInformationRow In _columnInformation
            Dim tableRow As New TableRow()
            tableRow.CssClass = IIf((index Mod 2) = 0, "evenRow", "oddRow")
            columnsTable.Rows.Add(tableRow)

            Dim nameCell As New TableCell()
            nameCell.Text = Server.HtmlEncode(_columnInformationRow.NiceName)
            tableRow.Cells.Add(nameCell)

            Dim fieldNameCell As New TableCell()
            fieldNameCell.Text = Server.HtmlEncode(_columnInformationRow.ColumnName)
            tableRow.Cells.Add(fieldNameCell)

            Dim pkCell As New TableCell()
            pkCell.Text = IIf(_columnInformationRow.IsPrimaryKey, "<b>Yes</b>", "&nbsp;")
            tableRow.Cells.Add(pkCell)

            Dim nullableCell As New TableCell()
            nullableCell.Text = IIf(_columnInformationRow.IsNullable, "<b>Yes</b>", "&nbsp;")
            tableRow.Cells.Add(nullableCell)

            Dim identityCell As New TableCell()
            identityCell.Text = IIf(_columnInformationRow.IsIdentity, "<b>Yes</b>", "&nbsp;")
            tableRow.Cells.Add(identityCell)

            Dim dataTypeCell As New TableCell()
            dataTypeCell.Text = Server.HtmlEncode(_columnInformationRow.DataType)
            tableRow.Cells.Add(dataTypeCell)

            index += 1
        Next
    End Sub

    Private Sub BuildForm(ByVal useViewState As Boolean)
        BuildFunctionCodes(useViewState)
        BuildFunctions(useViewState)
        BuildParentEntities(useViewState)
        BuildTableNames(useViewState)
        BuildColumnInformation(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Return
        End If

        BuildForm(False)
    End Sub

    Protected Sub createFunctionButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createFunctionButton.Click
        BuildForm(True)

        codeTextBox.Text = codeTextBox.Text.Trim()
        nameTextBox.Text = nameTextBox.Text.Trim()

        If String.IsNullOrEmpty(codeTextBox.Text) _
         OrElse String.IsNullOrEmpty(functionCodeDropDown.SelectedValue) _
         OrElse String.IsNullOrEmpty(nameTextBox.Text) Then
            AddErrorMessage("Specify a Table Editor Function Code, Aurora Function Code and Name")
            Return
        End If

        Dim tefId As Integer = -1
        For Each listItem As ListItem In functionDropDown.Items
            If listItem.Text.Trim.ToUpper() = codeTextBox.Text Then
                tefId = Integer.Parse(listItem.Value)
                Exit For
            End If
        Next

        If tefId <> -1 Then
            AddErrorMessage("Code already exists")
            Return
        End If

        Try
            Dim functionRow As TableEditorFunctionRow = DataRepository.InsertFunction( _
                codeTextBox.Text, _
                functionCodeDropDown.SelectedValue, _
                nameTextBox.Text, _
                Nothing)

            functionDropDown.Items.Add(New ListItem(nameTextBox.Text, functionRow.tefId.ToString()))
            functionDropDown.SelectedValue = functionRow.tefId.ToString()

            BuildForm(True)
            AddInformationMessage("Function created")
        Catch ex As Exception
            AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Protected Sub createEntityButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createEntityButton.Click
        BuildForm(True)

        entityNameTextBox.Text = entityNameTextBox.Text.Trim()

        If String.IsNullOrEmpty(functionDropDown.SelectedValue) _
         OrElse String.IsNullOrEmpty(tableNameDropDown.SelectedValue) _
         OrElse String.IsNullOrEmpty(entityNameTextBox.Text) _
         OrElse _columnInformation Is Nothing _
         OrElse _columnInformation.Rows.Count <= 0 Then
            Return
        End If

        Dim code As String = functionDropDown.SelectedItem.Text
        Dim tefId As Integer = Integer.Parse(functionDropDown.SelectedValue)

        Dim tableEditorDataSet As TableEditorDataSet = DataRepository.GetTableEditor(code)

        Dim parentEntity As TableEditorEntityRow = Nothing
        Dim entityParentId As New Nullable(Of Integer)()
        If Not String.IsNullOrEmpty(parentEntityDropDown.SelectedValue) Then
            entityParentId = New Nullable(Of Integer)(Integer.Parse(parentEntityDropDown.SelectedValue))
        End If

        ' get the meta data for the table selected and build the entity and attributes
        Dim tableName As String = tableNameDropDown.SelectedItem.Text

        Try
            Dim entityRow As TableEditorEntityRow = DataRepository.InsertEntity( _
             tefId, _
             entityParentId, _
             entityNameTextBox.Text, _
             Nothing, _
             tableName, _
             Nothing, _
             False, _
             True, _
             Nothing, _
             Nothing, _
             True, _
             Nothing, _
             Nothing, _
             True, _
             Nothing, _
             Nothing, _
             Nothing, _
             100)
            Dim index As Integer = 0
            For Each columnInformationRow As ColumnInformationRow In _columnInformation

                ''rev:mia March 31 2014 - addition of "modifieddate", "modifiedby"
                Dim isAuroraAttribute As Boolean = Array.IndexOf(New String() {"integrityno", "addusrid", "modusrid", "adddatetime", "moddatetime", "addprgmname", "modifieddate", "modifiedby"}, columnInformationRow.ColumnName.ToLower()) >= 0

                Dim isListable As Boolean = _
                    ( _
                        listAllAttributesCheckBox.Checked _
                        OrElse index = 0 _
                        OrElse columnInformationRow.IsPrimaryKey _
                        OrElse columnInformationRow.DataType = "bit" _
                        OrElse columnInformationRow.NiceName.IndexOf("Code") <> -1 _
                        OrElse columnInformationRow.NiceName.IndexOf("Type") <> -1 _
                        OrElse columnInformationRow.NiceName.IndexOf("Class") <> -1 _
                        OrElse columnInformationRow.NiceName.IndexOf("Name") <> -1 _
                        OrElse columnInformationRow.NiceName.IndexOf("Desc") <> -1 _
                    ) AndAlso Not (columnInformationRow.MaxLength > 512) AndAlso Not isAuroraAttribute

                Dim listWidth As String = Nothing
                Dim isUpdateable As Boolean = Not columnInformationRow.IsIdentity AndAlso Not (auroraReadonlyCheckBox.Checked AndAlso isAuroraAttribute)
                Dim isInsertable As Boolean = Not columnInformationRow.IsIdentity AndAlso Not (auroraReadonlyCheckBox.Checked AndAlso isAuroraAttribute)

                If columnInformationRow.DataType = "bit" Then
                    listWidth = "50px"
                ElseIf columnInformationRow.DataType = "varchar" Or columnInformationRow.DataType = "nvarchar" Then
                    If columnInformationRow.MaxLength <= 32 Then
                        listWidth = "60px"
                    ElseIf columnInformationRow.MaxLength <= 64 Then
                        listWidth = "120px"
                    ElseIf columnInformationRow.MaxLength <= 128 Then
                        listWidth = "180px"
                    End If
                End If

                Dim attributeParentId As New Nullable(Of Integer)()
                For Each a As TableEditorAttributeRow In tableEditorDataSet.TableEditorAttribute
                    If entityParentId.HasValue _
                     AndAlso a.teeId = entityParentId.Value _
                     AndAlso a.teaIsPrimaryKey _
                     AndAlso columnInformationRow.ColumnName.ToLower.EndsWith(a.teaFieldName.ToLower()) Then
                        attributeParentId = New Nullable(Of Integer)(a.teaId)
                        Exit For
                    End If
                Next

                Dim attributeRow As TableEditorAttributeRow = DataRepository.InsertAttribute( _
                    tefId, _
                    entityRow.teeId, _
                    attributeParentId, _
                    columnInformationRow.NiceName, _
                    Nothing, _
                    columnInformationRow.ColumnName, _
                    columnInformationRow.IsPrimaryKey, _
                    False, _
                    isListable, _
                    False, _
                    listWidth, _
                    Nothing, _
                    True, _
                    isUpdateable, _
                    isInsertable, _
                    columnInformationRow.IsNullable, _
                    columnInformationRow.DataType = "bit", _
                    Nothing, _
                    New Nullable(Of Integer)(IIf(columnInformationRow.MaxLength > 256, 5, 1)), _
                    False, _
                    Nothing, _
                    index * 10)
                index += 1
            Next

            AddInformationMessage("Entity created")
            BuildForm(True)
        Catch ex As Exception
            AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try
    End Sub

    Protected Sub tableNameDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tableNameDropDown.SelectedIndexChanged
        BuildForm(True)
        If tableNameDropDown.SelectedItem IsNot Nothing Then
            entityNameTextBox.Text = TableEditorDataSet.NiceName(tableNameDropDown.SelectedItem.Text)
        Else
            entityNameTextBox.Text = ""
        End If

    End Sub

    Protected Sub functionDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles functionDropDown.SelectedIndexChanged
        BuildForm(True)
    End Sub
End Class
