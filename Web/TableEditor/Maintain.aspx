<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Maintain.aspx.vb" Inherits="TableEditor_Maintain" MasterPageFile="~/Include/AuroraHeader.master" validateRequest="false" %>
<%-- requestValidate is false because data can contain "<" and ">" which trips the validation --%>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
    <style type="text/css">
    </style>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    
    <asp:Panel ID="tableEditorPanel" runat="Server" Width="780px">
        <p><b>Functions</b></p>

        <asp:Table ID="functionsTable" runat="server" CellSpacing="0" CellPadding="2" style="width: 100%;" CssClass="dataTable">
            <asp:TableHeaderRow>
                <asp:TableHeaderCell HorizontalAlign="left" Width="175px">Code</asp:TableHeaderCell>
                <asp:TableHeaderCell HorizontalAlign="left" Width="125px">Function Code</asp:TableHeaderCell>
                <asp:TableHeaderCell HorizontalAlign="left">Name</asp:TableHeaderCell>
            </asp:TableHeaderRow>
        </asp:Table>
               
        <hr/>
           
        <p><b>Create Function</b></p>

        <table style="width:780px">
            <tr>
                <td style="width: 150px">Code:</td>
                <td>
                    <asp:TextBox ID="codeTextBox" runat="Server" width="300px" />
                </td>
            </tr>
            <tr>
                <td>Aurora Function Code:</td>
                <td>
                    <asp:DropDownList ID="functionCodeDropDown" runat="server" width="300px" />
                </td>
            </tr>
            <tr>
                <td>Name:</td>
                <td>
                    <asp:TextBox ID="nameTextBox" runat="Server" width="300px" />
                </td>
            </tr>
            
            <tr>
                <td colspan="2" align="right">

                    <asp:Button ID="createFunctionButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                    
                </td>
            </tr>
        </table>        

        <hr/>

        <p><b>Create Entity</b></p>
          

        <table style="width:780px">
            <tr>
                <td style="width: 150px;">Function:</td>
                <td>
                    <asp:DropDownList ID="functionDropDown" runat="server" width="300px" AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td>Parent Entity:</td>
                <td>
                    <asp:DropDownList ID="parentEntityDropDown" runat="server" width="300px" />
                </td>
            </tr>
            <tr>
                <td>Table Name:</td>
                <td>
                    <asp:DropDownList ID="tableNameDropDown" runat="server" width="300px" AutoPostBack="True" />
                </td>
            </tr>
            <tr>
                <td>Attributes:</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Table ID="columnsTable" runat="server" CellSpacing="0" CellPadding="2" CssClass="dataTable">
                        <asp:TableHeaderRow>
                            <asp:TableHeaderCell Width="150px">Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="150px">Column Name</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="50px">PK</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="50px">Nullable</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="50px">Identity</asp:TableHeaderCell>
                            <asp:TableHeaderCell Width="100px">Type</asp:TableHeaderCell>
                        </asp:TableHeaderRow>
                    </asp:Table>

                </td>
            </tr>
            <tr>
                <td style="width: 150px">&nbsp;</td>
                <td>
                    <asp:CheckBox ID="listAllAttributesCheckBox" runat="Server" Text="List All Attributes" />
                    <br />
                    <asp:CheckBox ID="auroraReadonlyCheckBox" runat="Server" Text="Make Aurora Attributes Read-only" />
                </td>
            </tr>
            <tr>
                <td>Name:</td>
                <td>
                    <asp:TextBox ID="entityNameTextBox" runat="Server" width="300px" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="createEntityButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                </td>
            </tr>
        </table>
        
        <p><i>Note: Creating an entity automatically is *not* guaranteed to make a valid Table Editor definition.  
        Verify and edit appropriately before using.</i></p>
  

    </asp:Panel>
    
</asp:Content>