<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ListControl.ascx.vb" Inherits="TableEditor_ListControl" %>

<asp:Panel runat="server" DefaultButton="searchButton">

<table cellpadding="0" cellspacing="0" style="width: 100%;">
    <tr>
        <td><b><asp:Label ID="entityNameLabel" runat="server"></asp:Label></b></td>
    </tr>
    <tr>
        <td>
            <asp:Table ID="filterFieldTable" runat="server" CellSpacing="0" CellPadding="2" style="width: 100%">
            </asp:Table>
        </td>
    </tr>
    <tr id="orderBySpan" runat="server">
        <td>
            <table cellspacing="0" cellpadding="2" style="width: 100%">
                <tr>
                    <td style="width: 150px">Order By: </td>
                    <td>
                        <asp:DropDownList ID="orderByFieldDropDown" runat="server" Width="120px"></asp:DropDownList>
                        &nbsp;
                        <asp:DropDownList ID="orderByDirectionDropDown" runat="server" Width="120px">
                            <asp:ListItem Value="ASC">Ascending</asp:ListItem>
                            <asp:ListItem Value="DESC">Descending</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 2px; border-top-width: 1px">
            <div style="float: right">
                <asp:Button ID="resetButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
            </div>
            <asp:Label ID="listResultLabel" runat="server" />
        </td>
    </tr>
    
    <tr>
        <td style="border-top-width: 1px;">
            <asp:Table ID="listResultsTable" runat="server" CellSpacing="0" CellPadding="2" style="width: 100%" Visible="False">
                <asp:TableHeaderRow>
                    <asp:TableHeaderCell Text="Id"></asp:TableHeaderCell>
                    <asp:TableHeaderCell Text="Name"></asp:TableHeaderCell>
                </asp:TableHeaderRow>
            </asp:Table>
        </td>
    </tr>
</table>

</asp:Panel>