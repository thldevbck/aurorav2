<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EditControl.ascx.vb"
    Inherits="TableEditor_EditControl" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="PopupDisplay" %>

<script type="text/javascript">
var tableEditorEditDeleteButtonId =  '<%= Me.deleteButton.ClientId %>';

function tableEditorEditDeleteConfirm(e)
{
    if (!confirm ("Are you sure you want to delete this data?"))
    {
        cancelEvent(e);
        return false;
    }
    else
        return true;
}

function tableEditorEditInit()
{
    addEvent (document.getElementById (tableEditorEditDeleteButtonId), "click", tableEditorEditDeleteConfirm);
}

addEvent (window, "load", tableEditorEditInit);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(tableEditorEditInit);
</script>

<asp:UpdatePanel runat="server" ID="updatePanelEditControl">
    <ContentTemplate>
        <table cellpadding="0" cellspacing="0" style="width: 100%">
            <tr>
                <td style="padding: 2px">
                    <b>
                        <asp:Label ID="entityNameLabel" runat="server"></asp:Label></b></td>
            </tr>
            <tr>
                <td>
                    <asp:Table ID="editFieldTable" runat="server" CellSpacing="0" CellPadding="2" Style="width: 100%">
                    </asp:Table>
                </td>
            </tr>
            <tr>
                <td align="right" style="padding: 2px;">
                    <asp:Button ID="deleteButton" runat="server" Text="Delete" CssClass="Button_Standard Button_Delete" />
                    <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                    <asp:Button ID="updateBackButton" runat="server" Text="Save / Back" CssClass="Button_Standard Button_Save" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>
<PopupDisplay:ConfirmationBoxControl ID="ConfirmationBoxControl1" 
                                     runat="server"  
                                     LeftButton_Text="Proceed" 
                                     RightButton_Text="Cancel"
                                     MessageType ="Warning" 
                                     Text="All bookings prior to this update will not reflect the surcharge bond change."
                                     Title="Surcharge Bond Change"  />
