<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Edit.aspx.vb" Inherits="TableEditor_Edit" MasterPageFile="~/Include/AuroraHeader.master" validateRequest="false" %>
<%-- requestValidate is false because data can contain "<" and ">" which trips the validation --%>

<%@ Register Src="ListControl.ascx" TagName="ListControl" TagPrefix="te" %>
<%@ Register Src="EditControl.ascx" TagName="EditControl" TagPrefix="te" %>
<%@ Register Src="InsertControl.ascx" TagName="InsertControl" TagPrefix="te" %>
<%@ Register Src="LocationControl.ascx" TagName="LocationControl" TagPrefix="te" %>

<asp:Content runat="server" ContentPlaceHolderID="StylePlaceHolder">
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:Panel ID="tableEditorPanel" runat="Server" Width="780px">
    
        <te:LocationControl ID="locationControl" runat="Server" />
        
        <br />

    </asp:Panel>

</asp:Content>

