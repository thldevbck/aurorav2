Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data


Partial Class TableEditor_ListControl
    Inherits AuroraUserControl

    Private _tableEditorEntityRow As TableEditorEntityRow = Nothing
    Public ReadOnly Property TableEditorEntityRow() As TableEditorEntityRow
        Get
            Return _tableEditorEntityRow
        End Get
    End Property

    Public ReadOnly Property TableEditorDataSet() As TableEditorDataSet
        Get
            Return CType(_tableEditorEntityRow.Table.DataSet, TableEditorDataSet)
        End Get
    End Property

    Private _filterControls As New Dictionary(Of String, Control)

    Private Sub InitFilterControls(ByVal tableCellField As System.Web.UI.WebControls.TableCell, ByVal attribute As TableEditorAttributeRow, ByVal searchParam As String)
        If attribute.LookupDataTable IsNot Nothing Then
            Dim dropDownList As New System.Web.UI.WebControls.DropDownList
            dropDownList.ID = "Filter_" & attribute.ID & "_DropDownList"
            dropDownList.Width = New System.Web.UI.WebControls.Unit("100%")
            tableCellField.Controls.Add(dropDownList)
            _filterControls.Add(dropDownList.ID, dropDownList)

            dropDownList.Items.Add("")
            For Each lookupRow As DataRow In attribute.LookupDataTable.Rows
                dropDownList.Items.Add(New ListItem( _
                 Aurora.Common.Utility.ObjectValueToString(lookupRow("Text")), _
                 Aurora.Common.Utility.ObjectValueToString(lookupRow("Value"))))
            Next

        ElseIf attribute.DataType Is GetType(Boolean) Then
            Dim radioButtonAll As New System.Web.UI.WebControls.RadioButton()
            radioButtonAll.Text = "All"
            radioButtonAll.ID = "Filter_" & attribute.ID & "_AllRadioButton"
            radioButtonAll.GroupName = "Filter_" & attribute.ID
            radioButtonAll.Checked = True
            tableCellField.Controls.Add(radioButtonAll)
            _filterControls.Add(radioButtonAll.ID, radioButtonAll)

            tableCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))

            Dim radioButtonTrue As New System.Web.UI.WebControls.RadioButton()
            radioButtonTrue.Text = "True"
            radioButtonTrue.ID = "Filter_" & attribute.ID & "_TrueRadioButton"
            radioButtonTrue.GroupName = "Filter_" & attribute.ID
            tableCellField.Controls.Add(radioButtonTrue)
            _filterControls.Add(radioButtonTrue.ID, radioButtonTrue)

            tableCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))

            Dim radioButtonFalse As New System.Web.UI.WebControls.RadioButton()
            radioButtonFalse.Text = "False"
            radioButtonFalse.ID = "Filter_" & attribute.ID & "_FalseRadioButton"
            radioButtonFalse.GroupName = "Filter_" & attribute.ID
            tableCellField.Controls.Add(radioButtonFalse)
            _filterControls.Add(radioButtonFalse.ID, radioButtonFalse)

        Else
            Dim dropDownList As New System.Web.UI.WebControls.DropDownList
            dropDownList.ID = "Filter_ExactLike_" & attribute.ID & "_DropDownList"
            dropDownList.Width = New System.Web.UI.WebControls.Unit("15%")
            dropDownList.Items.Add(New ListItem("Exact"))
            dropDownList.Items.Add(New ListItem("Like"))
            tableCellField.Controls.Add(dropDownList)
            _filterControls.Add(dropDownList.ID, dropDownList)

            tableCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))

            Dim textBox As New System.Web.UI.WebControls.TextBox
            textBox.ID = "Filter_" & attribute.ID & "_TextBox"
            textBox.Width = New System.Web.UI.WebControls.Unit("80%")
            tableCellField.Controls.Add(textBox)
            _filterControls.Add(textBox.ID, textBox)

            If Not String.IsNullOrEmpty(searchParam) Then
                textBox.Text = searchParam
                dropDownList.SelectedIndex = 1
            End If
        End If

    End Sub


    Public Sub SetEntity(ByVal tableEditorEntityRow As TableEditorEntityRow)
        If tableEditorEntityRow Is Nothing Then
            Throw New ArgumentNullException()
        End If
        _tableEditorEntityRow = tableEditorEntityRow

        filterFieldTable.Rows.Clear()
        Dim index As Integer = 0
        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.GetTableEditorAttributeRows()
            If Not attribute.teaIsFilterable Then Continue For

            Dim tableRow As New System.Web.UI.WebControls.TableRow()
            filterFieldTable.Rows.Add(tableRow)

            Dim tableCellName As New System.Web.UI.WebControls.TableCell()
            tableRow.Cells.Add(tableCellName)
            tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
            tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode(attribute.teaName & ":")))

            Dim tableCellField As New System.Web.UI.WebControls.TableCell()
            tableRow.Cells.Add(tableCellField)

            Dim searchParam As String = Nothing
            If index = 0 _
             AndAlso Not String.IsNullOrEmpty(Me.CurrentPage.SearchParam) _
             AndAlso attribute.TableEditorEntityRow.TableEditorEntityRowParent Is Nothing Then
                searchParam = Me.CurrentPage.SearchParam
            End If

            InitFilterControls(tableCellField, attribute, searchParam)

            index += 1
        Next

        orderByFieldDropDown.Items.Clear()
        For Each tableEditorAttributeRow As TableEditorAttributeRow In _tableEditorEntityRow.SortableAttributes()
            orderByFieldDropDown.Items.Add(New ListItem(tableEditorAttributeRow.teaName, tableEditorAttributeRow.OrderByName))
        Next
        If orderByFieldDropDown.Items.Count = 0 Then
            orderByFieldDropDown.Items.Add(New ListItem(""))
            orderBySpan.Visible = False
        Else
            orderBySpan.Visible = True
        End If

        If Not _tableEditorEntityRow.IsFilterable _
         AndAlso Not _tableEditorEntityRow.IsSortable _
         AndAlso Not _tableEditorEntityRow.teeMustFilter Then
            entityNameLabel.Text = Server.HtmlEncode("List " & _tableEditorEntityRow.teeName)
            resetButton.Visible = False
            searchButton.Visible = False
        Else
            entityNameLabel.Text = Server.HtmlEncode("Search " & _tableEditorEntityRow.teeName)
            resetButton.Visible = True
            searchButton.Visible = True
        End If

    End Sub


    Private Sub GetFilterParameter(ByVal attribute As TableEditorAttributeRow, ByVal parameters As Dictionary(Of String, Object))
        If attribute.LookupDataTable IsNot Nothing Then
            Dim dropDownList As System.Web.UI.WebControls.DropDownList = _filterControls("Filter_" & attribute.ID & "_DropDownList")
            If (dropDownList.SelectedValue <> "") Then
                parameters.Add(attribute.FilterExactParamName, dropDownList.SelectedValue)
            End If

        ElseIf attribute.DataType Is GetType(Boolean) Then
            Dim radioButtonTrue As System.Web.UI.WebControls.RadioButton = _filterControls("Filter_" & attribute.ID & "_TrueRadioButton")
            Dim radioButtonFalse As System.Web.UI.WebControls.RadioButton = _filterControls("Filter_" & attribute.ID & "_FalseRadioButton")

            If radioButtonTrue.Checked Then
                parameters.Add(attribute.FilterExactParamName, True)
            ElseIf radioButtonFalse.Checked Then
                parameters.Add(attribute.FilterExactParamName, False)
            End If

        Else
            Dim dropDownList As System.Web.UI.WebControls.DropDownList = _filterControls("Filter_ExactLike_" & attribute.ID & "_DropDownList")
            Dim textBox As System.Web.UI.WebControls.TextBox = _filterControls("Filter_" & attribute.ID & "_TextBox")
            If textBox.Text.Trim().Length > 0 Then
                If dropDownList.SelectedValue = "Exact" Then
                    parameters.Add(attribute.FilterExactParamName, textBox.Text.Trim())
                Else
                    parameters.Add(attribute.FilterLikeParamName, textBox.Text.Trim())
                End If
            End If
        End If
    End Sub


    Private Function GetFilterParamaters() As Dictionary(Of String, Object)
        Dim result As New Dictionary(Of String, Object)

        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.GetTableEditorAttributeRows()
            If Not attribute.teaIsFilterable Then Continue For
            GetFilterParameter(attribute, result)
        Next
        Return result
    End Function

    Private Sub BuildListResults()

        Dim params As Dictionary(Of String, Object) = GetFilterParamaters()

        If params.Count = 0 AndAlso _tableEditorEntityRow.teeMustFilter Then
            listResultLabel.Text = "Filter parameters must be specified"
            Return
        End If

        Try
            If Not String.IsNullOrEmpty(orderByFieldDropDown.SelectedValue) Then
                _tableEditorEntityRow.LoadSearchDataFromParams(params, orderByFieldDropDown.SelectedValue, orderByDirectionDropDown.SelectedValue)
            Else
                _tableEditorEntityRow.LoadSearchDataFromParams(params, Nothing, Nothing)
            End If
        Catch ex As Exception
            CType(Me.Page, AuroraPage).AddErrorMessage(ex.Message)
            Return
        End Try


        If _tableEditorEntityRow.SearchDataTable.Rows.Count = 0 Then
            listResultLabel.Text = "<i>No " & Server.HtmlEncode(_tableEditorEntityRow.teeName) & " found</i>"
            listResultsTable.Visible = False
            Return
        End If

        listResultLabel.Text = "<i>" & Server.HtmlEncode(_tableEditorEntityRow.SearchDataTable.Rows.Count.ToString() & " " & _tableEditorEntityRow.teeName & " found") & "</i>"
        listResultsTable.Visible = True

        listResultsTable.Rows.Clear()

        Dim tableHeaderRow As New TableHeaderRow()
        listResultsTable.Rows.Add(tableHeaderRow)

        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.ListableAttributes()
            Dim tableHeaderCell As New TableHeaderCell()
            tableHeaderCell.Text = Server.HtmlEncode(attribute.teaName)
            If Not attribute.IsteaListWidthNull Then tableHeaderCell.Width = New Unit(attribute.teaListWidth)
            If Not attribute.IsteaListAlignNull Then tableHeaderCell.Attributes.Add("align", attribute.teaListAlign)
            tableHeaderRow.Controls.Add(tableHeaderCell)
        Next

        ''REV:MIA AMEX SURCHARGE -- add header
        Dim dview As DataView = Nothing
        If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
            tableHeaderRow.Cells(0).Width = New Unit(30)
            tableHeaderRow.Cells(1).Width = New Unit(100)
            tableHeaderRow.Cells(2).Width = New Unit(100)
            tableHeaderRow.Cells(3).Width = New Unit(30)
            tableHeaderRow.Cells(4).Width = New Unit(30)
            tableHeaderRow.Cells(5).Width = New Unit(30)
            tableHeaderRow.Cells(6).Width = New Unit(30)
            tableHeaderRow.Cells(7).Width = New Unit(30)
            tableHeaderRow.Cells(8).Width = New Unit(30)
            tableHeaderRow.Cells(9).Width = New Unit(30)
            tableHeaderRow.Cells(10).Width = New Unit(30)

            Dim tableHeaderCell As New TableHeaderCell()
            tableHeaderCell.Text = Server.HtmlEncode("Split Surcharge")
            tableHeaderCell.Width = New Unit(20)
            tableHeaderRow.Controls.Add(tableHeaderCell)
            dview = Aurora.TableEditor.Data.DataRepository.GetAllPaymentMethod.Tables(0).DefaultView
            dview.Sort = "PtmName Desc"

            '----------------------
            ''refund
            '----------------------
            tableHeaderCell = New TableHeaderCell()
            tableHeaderCell.Text = Server.HtmlEncode("Refund Surcharge")
            tableHeaderCell.Width = New Unit(20)
            tableHeaderRow.Controls.Add(tableHeaderCell)
        End If

        ''reV:mia March 17,2009 Phoenix
        ''Dim dview As DataView = Nothing
        If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
            tableHeaderRow.Cells(0).Width = New Unit(200)
            tableHeaderRow.Cells(1).Width = New Unit(400)
            Dim tableHeaderCell As New TableHeaderCell()
            tableHeaderCell.Text = Server.HtmlEncode("Is Active")
            tableHeaderCell.Width = New Unit(400)
            tableHeaderRow.Controls.Add(tableHeaderCell)
            dview = Aurora.TableEditor.Data.DataRepository.GetAllZone.Tables(0).DefaultView
            dview.Sort = "LocZoneCode Desc"
        End If
        Dim linkTableHeaderCell As New TableHeaderCell()
        linkTableHeaderCell.Width = New Unit("50px")
        linkTableHeaderCell.HorizontalAlign = HorizontalAlign.Right
        linkTableHeaderCell.Text = "&nbsp;"
        tableHeaderRow.Controls.Add(linkTableHeaderCell)


        Dim index As Integer = 0
        For Each searchDataRow As DataRow In _tableEditorEntityRow.SearchDataTable.Rows
            Dim tableRow As New TableRow()
            listResultsTable.Rows.Add(tableRow)

            For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.ListableAttributes()
                Dim tableCell As New TableCell()
                Dim text As String = ""
                Dim dataValue As Object = Nothing
                If attribute.DataValue(searchDataRow) IsNot Nothing Then
                    dataValue = attribute.DataValue(searchDataRow)
                    If attribute.DataType Is GetType(Boolean) AndAlso TypeOf dataValue Is Boolean Then
                        If CType(attribute.DataValue(searchDataRow), Boolean) Then
                            text = "<i>True</i>"
                        Else
                            text = "&nbsp;"
                        End If
                    ElseIf attribute.LookupDataTable IsNot Nothing Then
                        text = Server.HtmlEncode("" & TableEditorDataSet.LookupDataTable.GetTextByValue(attribute.LookupDataTable, dataValue))
                    Else
                        text = Server.HtmlEncode("" & Aurora.Common.Utility.ObjectValueToString(dataValue))
                    End If
                End If
                If text.Trim().Length > 0 Then
                    tableCell.Text = text
                    If attribute.teaTextRowCount > 1 AndAlso Not attribute.teaTextWrap Then tableCell.Text = "<pre>" & tableCell.Text & "</pre>"
                Else
                    tableCell.Text = "&nbsp;"
                End If
                ' Shoel : 27.11.8 : Fix for wrapping
                If tableCell.Text.Length > 50 AndAlso tableCell.Text.IndexOf(" "c) < 0 Then
                    tableCell.Style("word-wrap") = "break-word"
                    tableCell.Style("display") = "block"
                    tableCell.Style("float") = "left"
                    tableCell.Style("width") = "150px"
                End If
                ' Shoel : 27.11.8 : Fix for wrapping
                tableRow.CssClass = IIf((index Mod 2) = 0, "evenRow", "oddRow")
                tableRow.Controls.Add(tableCell)
            Next

            ''REV:MIA AMEX SURCHARGE -- add header
            If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                dview.RowFilter = "PtmName = '" & tableRow.Cells(1).Text & "'"
                Dim activeCell As New TableCell()
                If Not IsDBNull(dview(0)(1)) Then
                    activeCell.Text = "<i>" & dview(0)(1).ToString & "</i>" ''display IsActive
                Else
                    activeCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(activeCell)

                '----------------------
                ''refund
                '----------------------
                activeCell = New TableCell
                If Not IsDBNull(dview(0)(3)) Then
                    activeCell.Text = "<i>" & dview(0)(3).ToString & "</i>" ''display IsActive
                Else
                    activeCell.Text = "&nbsp;"
                End If
                tableRow.Controls.Add(activeCell)
            End If
            ''reV:mia March 17,2009 Phoenix
            If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                Dim activeCell As New TableCell()
                If tableRow.Cells(0).Text = "&nbsp;" Then
                    '' MyBase.CurrentPage.SetWarningMessage("Enter a valid value for ZoneCode")
                    activeCell.Text = "&nbsp;"
                Else
                    dview.RowFilter = "LocZoneCode = '" & tableRow.Cells(0).Text & "'"
                    If Not IsDBNull(dview(0)(2)) Then
                        activeCell.Text = "<i>" & dview(0)(2).ToString & "</i>" ''display IsActive
                    Else
                        activeCell.Text = "&nbsp;"
                    End If
                End If
                tableRow.Controls.Add(activeCell)
            End If

            Dim linkTableCell As New TableCell()
            tableRow.Controls.Add(linkTableCell)

            Dim hyperLink As New HyperLink()
            hyperLink.NavigateUrl = Me.Request.Path() & _tableEditorEntityRow.QueryStringParams(searchDataRow)
            hyperLink.Controls.Add(New LiteralControl("View"))
            linkTableCell.Controls.Add(hyperLink)

            index += 1
        Next
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _tableEditorEntityRow Is Nothing Then Return
        listResultLabel.Text = ""

        Dim params As Dictionary(Of String, Object) = GetFilterParamaters()
        If Not _tableEditorEntityRow.teeMustFilter OrElse params.Count > 0 Then
            BuildListResults()
        End If

    End Sub

    Protected Sub resetButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles resetButton.Click
        If _tableEditorEntityRow Is Nothing Then Return

        Response.Redirect(Me.Request.Path() & TableEditorDataSet.RootFunction.QueryStringParams)
    End Sub
End Class
