<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InsertControl.ascx.vb"
    Inherits="TableEditor_InsertControl" %>
<%@ Register Src="..\UserControls\CollapsiblePanel\CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<asp:UpdatePanel runat="server" ID="pnlInsertControl">
    <ContentTemplate>
    <uc1:CollapsiblePanel ID="collapsiblePanel" runat="server" TargetControlID="insertPanel"
    Width="780px" />
        <asp:Panel runat="server" ID="insertPanel" Style="width: 100%">
            <table cellpadding="0" cellspacing="0" class="dataTable" style="border-width: 0px;
                width: 100%;" id="insertTable" runat="server">
                <tr>
                    <td>
                        <asp:Table ID="insertFieldTable" runat="server" CellSpacing="0" CellPadding="2" Style="width: 100%">
                        </asp:Table>
                    </td>
                </tr>
                <tr>
                    <td align="right" style="padding: 2px">
                        <asp:Button ID="insertButton" runat="server" Text="Save" Style="margin-left: 10px"
                            CssClass="Button_Standard Button_Save" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
