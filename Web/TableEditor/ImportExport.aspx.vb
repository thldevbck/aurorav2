Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data

<AuroraFunctionCodeAttribute("TABLEEDITOR2")> _
<AuroraPageTitleAttribute("Table Editor Import/Export")> _
Partial Class TableEditor_ImportExport
    Inherits AuroraPage

    Private _functionDataTable As TableEditorFunctionDataTable = Nothing
    Private _selectedFunction As TableEditorDataSet = Nothing

    Private Sub BuildFunctions(ByVal useViewState As Boolean)
        Dim functionDropDown_selectedValue As String = functionDropDown.SelectedValue
        functionDropDown.SelectedValue = Nothing

        _functionDataTable = DataRepository.GetFunctions()

        functionDropDown.DataSource = _functionDataTable
        functionDropDown.DataValueField = "tefId"
        functionDropDown.DataTextField = "tefCode"
        functionDropDown.DataBind()

        If useViewState AndAlso functionDropDown.Items.FindByValue(functionDropDown_selectedValue) IsNot Nothing Then functionDropDown.SelectedValue = functionDropDown_selectedValue

        If functionDropDown.SelectedItem IsNot Nothing AndAlso Not String.IsNullOrEmpty(functionDropDown.SelectedItem.Text) Then
            _selectedFunction = DataRepository.GetTableEditor(functionDropDown.SelectedItem.Text)
        End If
    End Sub


    Private Sub BuildImportExport(ByVal useViewState As Boolean)
        importButton.Enabled = False
        xmlTextBox.ReadOnly = True
        xmlTextBox.Text = ""

        If _selectedFunction IsNot Nothing _
         AndAlso _selectedFunction.RootFunction IsNot Nothing Then

            If _selectedFunction.RootFunction.GetTableEditorEntityRows().Length = 0 Then
                xmlTextBox.ReadOnly = False
                xmlTextBox.CssClass = ""
                importButton.Enabled = True
            Else
                Try
                    _selectedFunction.Validate()

                    Using writer As New System.IO.StringWriter()
                        _selectedFunction.WriteXml(writer)
                        xmlTextBox.Text = writer.ToString()
                    End Using

                Catch ex As Exception
                    xmlTextBox.Text = ex.Message
                End Try
            End If

        End If
    End Sub


    Private Sub BuildForm(ByVal useViewState As Boolean)
        BuildFunctions(useViewState)
        BuildImportExport(useViewState)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Return
        End If
        deleteButton.Attributes.Add("OnClick", "return confirm('This will completely delete the Table Editor Definition selected. \r\n Are you sure?');")
        BuildForm(False)
    End Sub

    Protected Sub functionDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles functionDropDown.SelectedIndexChanged
        BuildForm(True)
    End Sub

    Private Function ConvertNullableString(ByVal dataRow As DataRow, ByVal columnName As String) As String
        If dataRow.IsNull(columnName) Then
            Return Nothing
        Else
            Return dataRow(columnName).ToString()
        End If
    End Function

    Private Function ConvertNullableInteger(ByVal dataRow As DataRow, ByVal columnName As String) As Nullable(Of Integer)
        If dataRow.IsNull(columnName) Then
            Return New Nullable(Of Integer)
        Else
            Return New Nullable(Of Integer)(CType(dataRow(columnName), Integer))
        End If
    End Function

    Private Sub ImportAttribute( _
     ByVal tableEditor As TableEditorDataSet, _
     ByVal attribute As TableEditorAttributeRow, _
     ByVal entityIdMap As Dictionary(Of Integer, Integer), _
     ByVal attributeIdMap As Dictionary(Of Integer, Integer))

        Dim attributeParentId As New Nullable(Of Integer)()
        If Not attribute.IsteaParentIdNull Then
            attributeParentId = New Nullable(Of Integer)(attributeIdMap(attribute.teaParentId))
        End If

        Dim result As TableEditorAttributeRow = DataRepository.InsertAttribute( _
            _selectedFunction.RootFunction.tefId, _
            entityIdMap(attribute.teeId), _
            attributeParentId, _
            attribute.teaName, _
            ConvertNullableString(attribute, "teaDescription"), _
            ConvertNullableString(attribute, "teaFieldName"), _
            attribute.teaIsPrimaryKey, _
            attribute.teaIsFilterable, _
            attribute.teaIsListable, _
            attribute.teaIsSortable, _
            ConvertNullableString(attribute, "teaListWidth"), _
            ConvertNullableString(attribute, "teaListAlign"), _
            attribute.teaIsViewable, _
            attribute.teaIsUpdateable, _
            attribute.teaIsInsertable, _
            attribute.teaIsNullable, _
            attribute.teaIsBoolean, _
            ConvertNullableString(attribute, "teaLookupSql"), _
            ConvertNullableInteger(attribute, "teaTextRowCount"), _
            attribute.teaTextWrap, _
            ConvertNullableString(attribute, "teaDefaultValue"), _
            attribute.teaOrder)
        attributeIdMap.Add(attribute.teaId, result.teaId)
    End Sub

    Private Sub ImportEntity( _
     ByVal tableEditor As TableEditorDataSet, _
     ByVal entity As TableEditorEntityRow, _
     ByVal entityIdMap As Dictionary(Of Integer, Integer), _
     ByVal attributeIdMap As Dictionary(Of Integer, Integer))

        Dim entityParentId As New Nullable(Of Integer)()
        If Not entity.IsteeParentIdNull Then
            entityParentId = New Nullable(Of Integer)(entityIdMap(entity.teeParentId))
        End If

        Dim result As TableEditorEntityRow = DataRepository.InsertEntity( _
            _selectedFunction.RootFunction.tefId, _
            entityParentId, _
            entity.teeName, _
            ConvertNullableString(entity, "teeDescription"), _
            entity.teeTableName, _
            ConvertNullableString(entity, "teeSelectSql"), _
            entity.teeMustFilter, _
            entity.teeUpdateable, _
            ConvertNullableString(entity, "teeUpdateSql"), _
            ConvertNullableString(entity, "teeUpdateableFieldName"), _
            entity.teeInsertable, _
            ConvertNullableString(entity, "teeInsertSql"), _
            ConvertNullableString(entity, "teeInsertableFieldName"), _
            entity.teeDeleteable, _
            ConvertNullableString(entity, "teeDeleteSql"), _
            ConvertNullableString(entity, "teeDeleteableFieldName"), _
            ConvertNullableString(entity, "teeDefaultSql"), _
            entity.teeOrder)
        entityIdMap.Add(entity.teeId, result.teeId)

        For Each attribute As TableEditorAttributeRow In entity.GetTableEditorAttributeRows()
            ImportAttribute(tableEditor, attribute, entityIdMap, attributeIdMap)
        Next

        For Each childEntity As TableEditorEntityRow In entity.GetTableEditorEntityRows()
            ImportEntity(tableEditor, childEntity, entityIdMap, attributeIdMap)
        Next
    End Sub

    Protected Sub importButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles importButton.Click
        If functionDropDown.SelectedItem IsNot Nothing AndAlso Not String.IsNullOrEmpty(functionDropDown.SelectedItem.Text) Then
            _selectedFunction = DataRepository.GetTableEditor(functionDropDown.SelectedItem.Text)
        End If

        If _selectedFunction Is Nothing _
         OrElse _selectedFunction.RootFunction Is Nothing _
         OrElse _selectedFunction.RootFunction.GetTableEditorEntityRows().Length <> 0 Then
            AddErrorMessage("Invalid Function")
            Return
        End If

        Dim tableEditor As New TableEditorDataSet()
        Try
            Using stringReader As New System.IO.StringReader(xmlTextBox.Text)
                tableEditor.ReadXml(stringReader)
            End Using
        Catch
            AddErrorMessage("Invalid Xml")
            Return
        End Try

        Try
            tableEditor.Validate()
        Catch ex As Exception
            AddErrorMessage(ex.Message)
            Return
        End Try

        Dim entityIdMap As New Dictionary(Of Integer, Integer)
        Dim attributeIdMap As New Dictionary(Of Integer, Integer)
        Try
            ImportEntity(tableEditor, tableEditor.RootEntity, entityIdMap, attributeIdMap)
        Catch ex As Exception
            AddErrorMessage("Error importing table editor definition:<br/>" & Server.HtmlEncode(ex.Message))
            Return
        End Try

        BuildForm(True)
        AddInformationMessage("Import Successful")
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        ' This is now going to clear out the definitions
        Dim sTefId, sErrorMessage As String
        sTefId = functionDropDown.SelectedValue

        If Aurora.TableEditor.Data.DeleteDefinition(sTefId, sErrorMessage) Then
            BuildForm(True)
            AddInformationMessage("Definition deleted successfully")
        Else
            AddErrorMessage("Error importing table editor definition:<br/>" & Server.HtmlEncode(sErrorMessage))
        End If
    End Sub
End Class
