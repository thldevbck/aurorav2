Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data


Partial Class TableEditor_LocationControl
    Inherits AuroraUserControl

    Private _tableEditorEntityRow As TableEditorEntityRow = Nothing
    Public ReadOnly Property TableEditorEntityRow() As TableEditorEntityRow
        Get
            Return _tableEditorEntityRow
        End Get
    End Property

    Public ReadOnly Property TableEditorDataSet() As TableEditorDataSet
        Get
            Return CType(_tableEditorEntityRow.Table.DataSet, TableEditorDataSet)
        End Get
    End Property

    Public Sub SetEntity(ByVal tableEditorEntityRow As TableEditorEntityRow)
        If tableEditorEntityRow Is Nothing Then Throw New ArgumentNullException("tableEditorEntityRow")

        _tableEditorEntityRow = tableEditorEntityRow

        If Me.CurrentPage.GetFunctionPermission("TABLEEDITOR") Then
            Me.Controls.Add(New LiteralControl("<b>&gt;</b>&nbsp;&nbsp;"))
            Dim tableEditorLink As HyperLink = New HyperLink()
            tableEditorLink.NavigateUrl = "Maintain.aspx"
            tableEditorLink.Text = Server.HtmlEncode("Table Editor Maintenance")
            Me.Controls.Add(tableEditorLink)
            Me.Controls.Add(New LiteralControl("<br/>"))
        End If

        Me.Controls.Add(New LiteralControl("<b>&gt;</b>&nbsp;&nbsp;"))
        Dim baseLink As HyperLink = New HyperLink()
        baseLink.NavigateUrl = Me.Request.Path() & TableEditorDataSet.RootFunction.QueryStringParams
        baseLink.Text = Server.HtmlEncode(TableEditorDataSet.RootFunction.tefName)
        Me.Controls.Add(baseLink)

        If tableEditorEntityRow IsNot Nothing AndAlso tableEditorEntityRow.DataRow IsNot Nothing Then

            Dim entities As TableEditorEntityRow() = tableEditorEntityRow.AllEntities()

            For Each e As TableEditorEntityRow In entities

                Me.Controls.Add(New LiteralControl("<br/><b>&gt;</b>&nbsp;&nbsp;<b>" & Server.HtmlEncode(e.teeName) & ":</b>&nbsp;"))

                Dim hyperLink As HyperLink = New HyperLink()
                hyperLink.NavigateUrl = Me.Request.Path() & e.QueryStringParams(tableEditorEntityRow.DataRow)
                hyperLink.Text = Server.HtmlEncode(e.ListableText(tableEditorEntityRow.DataRow))
                Me.Controls.Add(hyperLink)
            Next
        End If

        Me.Controls.Add(New LiteralControl("<br/>"))
    End Sub

End Class
