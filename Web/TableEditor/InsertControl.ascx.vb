''rev:mia - PHOENIX CHANGES
'RE-5:	A zone shall be defined by a;  
'        �	Zone code which is made up of a minimum of 3 alpha/numeric characters and a maximum of 3 alpha/numeric characters. 
'        �	Zone Name which contains a minimum of 1 alpha/numeric characters and a maximum of 64 alpha/numeric characters.
'RE-6:	The Zone name shall be stored in alpha lowercase in the database.
'RE-7:	A zone shall have a status of either Active or Inactive.
'       Active means available to be used. 
'       Inactive means not longer in use. 


Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data


Partial Class TableEditor_InsertControl
    Inherits AuroraUserControl

    Private _tableEditorEntityRow As TableEditorEntityRow = Nothing
    Public ReadOnly Property TableEditorEntityRow() As TableEditorEntityRow
        Get
            Return _tableEditorEntityRow
        End Get
    End Property

    Public ReadOnly Property TableEditorDataSet() As TableEditorDataSet
        Get
            Return CType(_tableEditorEntityRow.Table.DataSet, TableEditorDataSet)
        End Get
    End Property

    Private _insertControls As New Dictionary(Of String, Control)

    Private Sub InitInsertControls(ByVal tableCellField As System.Web.UI.WebControls.TableCell, _
                                   ByVal attribute As TableEditorAttributeRow, _
                                   ByVal checkBoxNull As CheckBox, _
                                   Optional ByRef ActiveSurchargeRow As TableRow = Nothing, _
                                   Optional ByRef ActiveRefundSurchargeRow As TableRow = Nothing)

        If _tableEditorEntityRow.IsInsertable() AndAlso attribute.teaIsInsertable() And attribute.IsteaParentIdNull() Then
            If attribute.LookupDataTable IsNot Nothing Then
                Dim dropDownList As New System.Web.UI.WebControls.DropDownList
                dropDownList.ID = "Insert_" & attribute.ID & "_DropDownList"
                dropDownList.Width = New System.Web.UI.WebControls.Unit("600px")

                If attribute.teaIsNullable Then dropDownList.Items.Add(New ListItem(""))
                For Each lookupRow As DataRow In attribute.LookupDataTable.Rows
                    dropDownList.Items.Add(New ListItem( _
                     Aurora.Common.Utility.ObjectValueToString(lookupRow("Text")), _
                     Aurora.Common.Utility.ObjectValueToString(lookupRow("Value"))))
                Next

                If attribute.DefaultValue IsNot Nothing AndAlso dropDownList.Items.FindByValue(attribute.DefaultValueAsString()) IsNot Nothing Then
                    dropDownList.SelectedValue = attribute.DefaultValueAsString()
                    If checkBoxNull IsNot Nothing Then checkBoxNull.Checked = False
                Else
                    If checkBoxNull IsNot Nothing Then checkBoxNull.Checked = True
                End If

                tableCellField.Controls.Add(dropDownList)
                _insertControls.Add(dropDownList.ID, dropDownList)

                If checkBoxNull IsNot Nothing Then
                    dropDownList.Attributes.Add("onchange", "document.getElementById('" & checkBoxNull.ClientID & "').checked = (document.getElementById ('" & dropDownList.ClientID & "').selectedIndex == 0);")
                End If

            ElseIf attribute.DataType Is GetType(Boolean) Then
                Dim radioButtonFalse As New System.Web.UI.WebControls.RadioButton()
                radioButtonFalse.Text = "False"
                radioButtonFalse.ID = "Insert_" & attribute.ID & "_FalseRadioButton"
                radioButtonFalse.GroupName = "Insert_" & attribute.ID

                Dim radioButtonTrue As New System.Web.UI.WebControls.RadioButton()
                radioButtonTrue.Text = "True"
                radioButtonTrue.ID = "Insert_" & attribute.ID & "_TrueRadioButton"
                radioButtonTrue.GroupName = "Insert_" & attribute.ID

                If attribute.DefaultValue IsNot Nothing AndAlso TypeOf attribute.DefaultValue Is Boolean Then
                    radioButtonTrue.Checked = CType(attribute.DefaultValue, Boolean)
                    If checkBoxNull IsNot Nothing Then checkBoxNull.Checked = False
                Else
                    If checkBoxNull IsNot Nothing Then checkBoxNull.Checked = True
                End If
                radioButtonFalse.Checked = Not radioButtonTrue.Checked

                tableCellField.Controls.Add(radioButtonFalse)
                _insertControls.Add(radioButtonFalse.ID, radioButtonFalse)
                tableCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
                tableCellField.Controls.Add(radioButtonTrue)
                _insertControls.Add(radioButtonTrue.ID, radioButtonTrue)

                ''REV:MIA AMEX SURCHARGE -- insert IsSurchargeSplit radio button here
                If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                    If attribute.teaFieldName.Contains("PtmIsActive") Then

                        '----------------------------------------------------------
                        ' for split surcharge
                        '---------------------------------------------------------
                        Dim tableCellName As New System.Web.UI.WebControls.TableCell()
                        tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
                        tableCellName.Style.Add("padding-top", "6px")
                        tableCellName.VerticalAlign = VerticalAlign.Top
                        tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode("Surcharge Split:")))
                        ActiveSurchargeRow.Cells.Add(tableCellName)

                        Dim tableCellNull As New System.Web.UI.WebControls.TableCell()
                        tableCellNull.Style.Add("padding-top", "6px")
                        tableCellNull.VerticalAlign = VerticalAlign.Top
                        ActiveSurchargeRow.Cells.Add(tableCellNull)

                        Dim activeCellField As TableCell = New TableCell

                        Dim rdoPtmNotActive As RadioButton = New RadioButton
                        rdoPtmNotActive.ID = "radioButtonPtmNotActive"
                        rdoPtmNotActive.Text = "False"
                        rdoPtmNotActive.Checked = True
                        rdoPtmNotActive.GroupName = "radioButtonSurchargeGroup"
                        AddHandler rdoPtmNotActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        Dim rdoPtmActive As RadioButton = New RadioButton
                        rdoPtmActive.ID = "radioButtonPtmActive"
                        rdoPtmActive.Text = "True"
                        rdoPtmActive.AutoPostBack = True
                        rdoPtmActive.GroupName = "radioButtonSurchargeGroup"
                        AddHandler rdoPtmActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        activeCellField.Controls.Add(rdoPtmNotActive)
                        ActiveSurchargeRow.Cells.Add(activeCellField)

                        _insertControls.Add(rdoPtmNotActive.ID, rdoPtmNotActive)

                        activeCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
                        ActiveSurchargeRow.Cells.Add(activeCellField)

                        activeCellField.Controls.Add(rdoPtmActive)
                        ActiveSurchargeRow.Cells.Add(activeCellField)
                        _insertControls.Add(rdoPtmActive.ID, rdoPtmActive)

                        '----------------------------------------------------------
                        ' for refund surcharge
                        '---------------------------------------------------------
                        tableCellName = New TableCell
                        tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
                        tableCellName.Style.Add("padding-top", "6px")
                        tableCellName.VerticalAlign = VerticalAlign.Top
                        tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode("Refund Surcharge:")))
                        ActiveRefundSurchargeRow.Cells.Add(tableCellName)

                        tableCellNull = New TableCell
                        tableCellNull.Style.Add("padding-top", "6px")
                        tableCellNull.VerticalAlign = VerticalAlign.Top
                        ActiveRefundSurchargeRow.Cells.Add(tableCellNull)

                        Dim rdoPtmNotRefundSurchargeActive As RadioButton = New RadioButton
                        rdoPtmNotRefundSurchargeActive.ID = "radioButtonPtmNotRefundSurchargeActive"
                        rdoPtmNotRefundSurchargeActive.Text = "False"
                        rdoPtmNotRefundSurchargeActive.Checked = True
                        rdoPtmNotRefundSurchargeActive.GroupName = "radioButtonRefundSurchargeGroup"
                        AddHandler rdoPtmNotRefundSurchargeActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        Dim rdoRefundSurchargePtmActive As RadioButton = New RadioButton
                        rdoRefundSurchargePtmActive.ID = "radioButtonPtmRefundSurchargePtmActive"
                        rdoRefundSurchargePtmActive.Text = "True"
                        rdoRefundSurchargePtmActive.AutoPostBack = True
                        rdoRefundSurchargePtmActive.GroupName = "radioButtonRefundSurchargeGroup"
                        AddHandler rdoRefundSurchargePtmActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        activeCellField = Nothing
                        activeCellField = New TableCell
                        activeCellField.Controls.Add(rdoPtmNotRefundSurchargeActive)
                        ActiveRefundSurchargeRow.Cells.Add(activeCellField)

                        _insertControls.Add(rdoPtmNotRefundSurchargeActive.ID, rdoPtmNotRefundSurchargeActive)

                        activeCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
                        ActiveRefundSurchargeRow.Cells.Add(activeCellField)

                        activeCellField.Controls.Add(rdoRefundSurchargePtmActive)
                        ActiveRefundSurchargeRow.Cells.Add(activeCellField)
                        _insertControls.Add(rdoRefundSurchargePtmActive.ID, rdoRefundSurchargePtmActive)


                    End If
                End If

                If checkBoxNull IsNot Nothing Then
                    radioButtonFalse.Attributes.Add("onclick", "document.getElementById('" & checkBoxNull.ClientID & "').checked = false;")
                    radioButtonTrue.Attributes.Add("onclick", "document.getElementById('" & checkBoxNull.ClientID & "').checked = false;")
                End If

            Else
                Dim textBox As New System.Web.UI.WebControls.TextBox
                textBox.ID = "Insert_" & attribute.ID & "_TextBox"
                textBox.Width = New System.Web.UI.WebControls.Unit("600px")
                If attribute.teaTextRowCount > 1 Then
                    textBox.TextMode = TextBoxMode.MultiLine
                    textBox.Rows = attribute.teaTextRowCount
                    textBox.Wrap = attribute.teaTextWrap
                Else
                    textBox.TextMode = TextBoxMode.SingleLine
                End If

                'reV:mia March 17,2009 Phoenix
                'RE-5:	
                Dim checkIsActive As CheckBox = Nothing
                If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                    If attribute.teaFieldName.Equals("LocZoneCode") Then
                        textBox.MaxLength = 3
                        textBox.Width = New Unit(100)
                        checkIsActive = New CheckBox
                        checkIsActive.ID = "checkIsActive"
                        checkIsActive.AutoPostBack = True
                        AddHandler checkIsActive.CheckedChanged, AddressOf checkIsActive_CheckChanged
                    End If
                    If attribute.teaFieldName.Equals("LocZoneName") Then
                        textBox.MaxLength = 64
                        textBox.Width = New Unit(450)
                    End If
                End If
                If attribute.DefaultValue IsNot Nothing Then
                    textBox.Text = attribute.DefaultValueAsString
                    If checkBoxNull IsNot Nothing Then checkBoxNull.Checked = False
                Else
                    If checkBoxNull IsNot Nothing Then checkBoxNull.Checked = True
                End If

                ''reV:mia March 17,2009 Phoenix
                'RE-7
                If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                    If attribute.teaFieldName.Equals("LocZoneCode") Then
                        tableCellField.Controls.Add(textBox)
                        _insertControls.Add(textBox.ID, textBox)
                        Dim litMsg As Literal = New Literal
                        litMsg.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Is Active?:"
                        tableCellField.Controls.Add(litMsg)
                        tableCellField.Controls.Add(checkIsActive)
                        _insertControls.Add(checkIsActive.ID, checkIsActive)

                    Else
                        tableCellField.Controls.Add(textBox)
                        _insertControls.Add(textBox.ID, textBox)
                    End If
                Else
                    tableCellField.Controls.Add(textBox)
                    _insertControls.Add(textBox.ID, textBox)
                End If

                
                If checkBoxNull IsNot Nothing Then
                    textBox.Attributes.Add("onchange", "document.getElementById('" & checkBoxNull.ClientID & "').checked = document.getElementById('" & textBox.ClientID & "').value == '';")
                End If
            End If
        End If
    End Sub


    Public Sub SetEntity(ByVal tableEditorEntityRow As TableEditorEntityRow)
        If tableEditorEntityRow Is Nothing Then
            Throw New ArgumentNullException()
        End If
        _tableEditorEntityRow = tableEditorEntityRow

        collapsiblePanel.Title = Server.HtmlEncode("New " & _tableEditorEntityRow.teeName)
        insertButton.Visible = _tableEditorEntityRow.IsInsertable

        insertFieldTable.Rows.Clear()
        Dim index As Integer = 0
        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.InsertableAttributes()
            If _tableEditorEntityRow.IsInsertable() AndAlso attribute.teaIsInsertable() And attribute.IsteaParentIdNull() Then
                Dim tableRow As New System.Web.UI.WebControls.TableRow()
                insertFieldTable.Rows.Add(tableRow)

                Dim tableCellName As New System.Web.UI.WebControls.TableCell()
                tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
                tableCellName.Style.Add("padding-top", "6px")
                tableCellName.VerticalAlign = VerticalAlign.Top
                tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode(attribute.teaName & ":")))
                tableRow.Cells.Add(tableCellName)

                Dim tableCellNull As New System.Web.UI.WebControls.TableCell()
                tableCellNull.Style.Add("padding-top", "6px")
                tableCellNull.VerticalAlign = VerticalAlign.Top
                tableRow.Cells.Add(tableCellNull)

                Dim checkBoxNull As System.Web.UI.WebControls.CheckBox = Nothing
                If attribute.teaIsNullable Then
                    checkBoxNull = New System.Web.UI.WebControls.CheckBox()
                    checkBoxNull.ID = "Insert_NULL_" & attribute.ID & "_CheckBox"
                    checkBoxNull.Checked = True
                    tableCellNull.Controls.Add(checkBoxNull)
                    _insertControls.Add(checkBoxNull.ID, checkBoxNull)
                Else
                    tableCellNull.Controls.Add(New LiteralControl("&nbsp;"))
                End If

                Dim tableCellField As New System.Web.UI.WebControls.TableCell()
                tableRow.Cells.Add(tableCellField)

                ''REV:MIA AMEX SURCHARGE -- insert IsSurchargeSplit radio button here
                If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                    If attribute.teaFieldName.Contains("PtmIsActive") Then
                        Dim ActiveRow As TableRow = New TableRow
                        Dim RefundSurchargeRow As TableRow = New TableRow
                        InitInsertControls(tableCellField, attribute, checkBoxNull, ActiveRow, RefundSurchargeRow)
                        insertFieldTable.Rows.Add(ActiveRow)
                        insertFieldTable.Rows.Add(RefundSurchargeRow)
                    Else
                        InitInsertControls(tableCellField, attribute, checkBoxNull)
                    End If
                Else
                    InitInsertControls(tableCellField, attribute, checkBoxNull)
                End If
                ''InitInsertControls(tableCellField, attribute, checkBoxNull)


                If Not attribute.IsteaDescriptionNull AndAlso attribute.teaDescription.Trim().Length > 0 Then
                    Dim tableDescriptionRow As New System.Web.UI.WebControls.TableRow()
                    insertFieldTable.Rows.Add(tableDescriptionRow)

                    Dim tableCellDescription As New System.Web.UI.WebControls.TableCell()
                    tableCellDescription.ColumnSpan = 2
                    tableRow.Cells.Add(tableCellDescription)

                    tableCellName.Controls.Add(New LiteralControl("<pre>" & Server.HtmlEncode(attribute.teaDescription) & "</pre>"))
                End If

                index += 1
            End If
        Next

    End Sub

    Private Sub GetInsertValue(ByVal attribute As TableEditorAttributeRow, ByVal insertParams As Dictionary(Of String, Object))
        ''REV:MIA AMEX SURCHARGE -- insert IsSurchargeSplit radio button here
        Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateSurchargeSplit = False

        If _tableEditorEntityRow.IsInsertable() AndAlso attribute.teaIsInsertable() And attribute.IsteaParentIdNull() Then
            Dim paramValue As Object = Nothing
            Dim checkBoxNull As System.Web.UI.WebControls.CheckBox = Nothing
            If attribute.teaIsNullable Then checkBoxNull = _insertControls("Insert_NULL_" & attribute.ID & "_CheckBox")

            If attribute.teaIsNullable AndAlso checkBoxNull IsNot Nothing AndAlso checkBoxNull.Checked Then
                paramValue = DBNull.Value

               

            ElseIf attribute.LookupDataTable IsNot Nothing Then
                Dim dropDownList As System.Web.UI.WebControls.DropDownList = _insertControls("Insert_" & attribute.ID & "_DropDownList")
                If attribute.teaIsNullable AndAlso dropDownList.SelectedIndex = 0 Then
                    paramValue = DBNull.Value
                Else
                    paramValue = dropDownList.SelectedValue
                End If

            ElseIf attribute.DataType Is GetType(Boolean) Then
                Dim radioButtonFalse As System.Web.UI.WebControls.RadioButton = _insertControls("Insert_" & attribute.ID & "_FalseRadioButton")
                Dim radioButtonTrue As System.Web.UI.WebControls.RadioButton = _insertControls("Insert_" & attribute.ID & "_TrueRadioButton")

                paramValue = radioButtonTrue.Checked
                ''REV:MIA AMEX SURCHARGE 
                If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                    Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateSurchargeSplit = True
                End If

            Else
                Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateActiveZone = False
                Dim textBox As System.Web.UI.WebControls.TextBox = _insertControls("Insert_" & attribute.ID & "_TextBox")
                textBox.ID = "Insert_" & attribute.ID & "_TextBox"
                ''reV:mia March 17,2009 Phoenix
                ''RE-6
                If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                    If attribute.teaFieldName.Equals("LocZoneCode") Then
                        textBox.Text = textBox.Text.ToUpper
                    End If

                    If String.IsNullOrEmpty(textBox.Text) AndAlso attribute.teaFieldName.Equals("LocZoneCode") Then
                        Throw New Exception("Enter a valid value for ZoneCode")
                    ElseIf String.IsNullOrEmpty(textBox.Text) AndAlso attribute.teaFieldName.Equals("LocZoneName") Then
                        Throw New Exception("Enter a valid value for ZoneName")
                    End If

                    If (textBox.Text.Length < 3) AndAlso attribute.teaFieldName.Equals("LocZoneCode") Then
                        Throw New Exception("Invalid length for ZoneCode")
                    End If

                    If attribute.teaFieldName.Equals("LocZoneName") Then
                        Dim templowercase As String = textBox.Text.ToLower
                        Dim tempfirstLetter As String = templowercase.Substring(0, 1)
                        templowercase = templowercase.Remove(0, 1)
                        templowercase = tempfirstLetter.ToUpper & templowercase
                        textBox.Text = templowercase.ToLower
                    End If
                    Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateActiveZone = True

                End If

                paramValue = Aurora.Common.Utility.ObjectValueParse(attribute.DataType, textBox.Text)
            End If

            insertParams.Add(attribute.NewUpdateParamName, paramValue)
        End If
    End Sub

    Private Function GetInsertValues() As Dictionary(Of String, Object)
        Dim result As New Dictionary(Of String, Object)

        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.InsertableAttributes()
            Try
                GetInsertValue(attribute, result)
            Catch
                Throw New ValidationException("Enter a valid value for " & attribute.teaName)
            End Try
        Next
        Return result
    End Function


    Protected Sub insertButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles insertButton.Click
        If _tableEditorEntityRow Is Nothing OrElse Not _tableEditorEntityRow.IsInsertable Then
            Return
        End If

        Dim insertParams As Dictionary(Of String, Object)
        Try
            insertParams = GetInsertValues()
        Catch ex As ValidationException
            Me.CurrentPage.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        ' add parent id's
        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.GetTableEditorAttributeRows()
            If attribute.IsteaParentIdNull OrElse insertParams.ContainsKey(attribute.NewUpdateParamName) Then Continue For
            insertParams.Add(attribute.NewUpdateParamName, attribute.ParentAttribute.DataValue)
        Next

        Try
            _tableEditorEntityRow.InsertDataRow(insertParams)
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        ' redirect to force a page reload
        Dim url As String
        Dim allPKSpecified As Boolean = True
        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.PrimaryKeyAttributes
            allPKSpecified = allPKSpecified AndAlso (insertParams.ContainsKey(attribute.NewUpdateParamName) OrElse attribute.DefaultValue IsNot Nothing)
        Next

        ''rev:mia April 3 2014 - AIMS Fleet Table Maintenance
        allPKSpecified = allPKSpecified And Not IsAIMSTable(_tableEditorEntityRow.InnerSelectTableName)
        If allPKSpecified Then
            url = Me.Request.Path() & _tableEditorEntityRow.TableEditorFunctionRow.QueryStringParams _
             & "&" & EntityParam & "=" & System.Web.HttpUtility.UrlEncode(_tableEditorEntityRow.teeName)
            For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.AllParentPrimaryKeyAttributes()
                url &= "&" & attribute.QueryStringName & "=" & System.Web.HttpUtility.UrlEncode(attribute.DataValue.ToString())
            Next
            For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.PrimaryKeyAttributes
                Dim value As Object
                If insertParams.ContainsKey(attribute.NewUpdateParamName) Then
                    value = insertParams(attribute.NewUpdateParamName)
                Else
                    value = attribute.DefaultValue
                End If
                url &= "&" & attribute.QueryStringName & "=" & System.Web.HttpUtility.UrlEncode(value.ToString())
            Next
        ElseIf Not _tableEditorEntityRow.TableEditorEntityRowParent Is Nothing Then
            url = Me.Request.Path() & _tableEditorEntityRow.TableEditorEntityRowParent.QueryStringParams
        Else
            url = Me.Request.Path() & TableEditorDataSet.RootFunction.QueryStringParams
        End If
        url &= "&message=" & Server.UrlEncode(_tableEditorEntityRow.teeName & " created")

        Response.Redirect(url)
    End Sub

#Region "rev:mia April 3 2014 - AIMS Fleet Table Maintenance"
    Private Function GetAIMSTableName(tablename As String) As String
        Dim xml As New System.Xml.XmlDocument
        Try
            xml.Load(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/AIMSPROD.Tables.xml"))
            Return xml.SelectSingleNode(String.Concat("Tables/Name_", tablename)).InnerText
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Function IsAIMSTable(tablename As String) As Boolean
        tablename = tablename.Replace("[", "").Replace("]", "").ToUpper.Trim
        Return IIf(Not String.IsNullOrEmpty(GetAIMSTableName(tablename)), True, False)
    End Function
#End Region

#Region "REV:MIA AMEX SURCHARGE"

    Sub Surcharge_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)
        If CType(sender, RadioButton).ID = "radioButtonPtmActive" Then
            Aurora.TableEditor.Data.ActiveSplitSurchargeValue = CType(sender, RadioButton).Checked
        End If
        If CType(sender, RadioButton).ID = "radioButtonPtmNotActive" Then
            Aurora.TableEditor.Data.ActiveSplitSurchargeValue = False
        End If

        If CType(sender, RadioButton).ID = "radioButtonPtmRefundSurchargePtmActive" Then
            Aurora.TableEditor.Data.ActiveRefundSurchargeValue = CType(sender, RadioButton).Checked
        End If

        If CType(sender, RadioButton).ID = "radioButtonPtmNotRefundSurchargeActive" Then
            Aurora.TableEditor.Data.ActiveRefundSurchargeValue = False
        End If

    End Sub

    
#End Region

#Region "rev:mia March 17,2009"
    Sub checkIsActive_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)
        Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue = CType(sender, CheckBox).Checked
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue = False
        End If
    End Sub
#End Region
    
End Class
