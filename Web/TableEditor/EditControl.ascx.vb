''rev:mia - PHOENIX CHANGES
'RE-5:	A zone shall be defined by a;  
'        �	Zone code which is made up of a minimum of 3 alpha/numeric characters and a maximum of 3 alpha/numeric characters. 
'        �	Zone Name which contains a minimum of 1 alpha/numeric characters and a maximum of 64 alpha/numeric characters.
'RE-6:	The Zone name shall be stored in alpha lowercase in the database.
'RE-7:	A zone shall have a status of either Active or Inactive.
'       Active means available to be used. 
'       Inactive means not longer in use. 
Imports System.Data
Imports System.Data.Common
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Collections.Generic

Imports Aurora.Common
Imports Aurora.TableEditor.Data
Imports Aurora.TableEditor.Data.TableEditorDataSet

Imports Microsoft.Practices.EnterpriseLibrary.Data


Partial Class TableEditor_EditControl
    Inherits AuroraUserControl

    Private _tableEditorEntityRow As TableEditorEntityRow = Nothing
    Public ReadOnly Property TableEditorEntityRow() As TableEditorEntityRow
        Get
            Return _tableEditorEntityRow
        End Get
    End Property

    Public ReadOnly Property TableEditorDataSet() As TableEditorDataSet
        Get
            Return CType(_tableEditorEntityRow.Table.DataSet, TableEditorDataSet)
        End Get
    End Property

    Private _editControls As New Dictionary(Of String, Control)

    Private Sub InitEditControls(ByVal tableCellField As System.Web.UI.WebControls.TableCell, _
                                 ByVal attribute As TableEditorAttributeRow, _
                                 ByVal checkBoxNull As CheckBox, _
                                 Optional ByVal ActiveSurchargeRow As TableRow = Nothing, _
                                 Optional ByRef ActiveRefundSurchargeRow As TableRow = Nothing)

        If _tableEditorEntityRow.IsUpdateable() AndAlso attribute.teaIsUpdateable() AndAlso attribute.IsteaParentIdNull Then

            If attribute.LookupDataTable IsNot Nothing Then
                Dim dropDownList As New System.Web.UI.WebControls.DropDownList
                dropDownList.ID = "Edit_" & attribute.ID & "_DropDownList"
                dropDownList.Width = New System.Web.UI.WebControls.Unit("600px")

                If attribute.teaIsNullable Then
                    dropDownList.Items.Add(New ListItem("", ""))
                End If

                If attribute.teaIsNullable Then dropDownList.Items.Add(New ListItem(""))
                For Each lookupRow As DataRow In attribute.LookupDataTable.Rows
                    dropDownList.Items.Add(New ListItem( _
                     Aurora.Common.Utility.ObjectValueToString(lookupRow("Text")), _
                     Aurora.Common.Utility.ObjectValueToString(lookupRow("Value"))))
                Next

                If attribute.DataValueAsString() IsNot Nothing AndAlso dropDownList.Items.FindByValue(attribute.DataValueAsString()) IsNot Nothing Then
                    dropDownList.SelectedValue = attribute.DataValueAsString()
                End If

                tableCellField.Controls.Add(dropDownList)
                _editControls.Add(dropDownList.ID, dropDownList)

                If checkBoxNull IsNot Nothing Then
                    dropDownList.Attributes.Add("onchange", "document.getElementById('" & checkBoxNull.ClientID & "').checked = (document.getElementById ('" & dropDownList.ClientID & "').selectedIndex == 0);")
                End If

            ElseIf attribute.DataType Is GetType(Boolean) Then
                Dim dataValue As Boolean = False
                If attribute.DataValue IsNot Nothing AndAlso TypeOf attribute.DataValue Is Boolean Then
                    dataValue = CType(attribute.DataValue, Boolean)
                End If

                Dim radioButtonFalse As New System.Web.UI.WebControls.RadioButton()
                radioButtonFalse.Text = "False"
                radioButtonFalse.ID = "Edit_" & attribute.ID & "_FalseRadioButton"
                radioButtonFalse.GroupName = "Edit_" & attribute.ID
                radioButtonFalse.Checked = Not dataValue
                tableCellField.Controls.Add(radioButtonFalse)
                _editControls.Add(radioButtonFalse.ID, radioButtonFalse)

                tableCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))

                Dim radioButtonTrue As New System.Web.UI.WebControls.RadioButton()
                radioButtonTrue.Text = "True"
                radioButtonTrue.ID = "Edit_" & attribute.ID & "_TrueRadioButton"
                radioButtonTrue.GroupName = "Edit_" & attribute.ID
                radioButtonTrue.Checked = dataValue

                tableCellField.Controls.Add(radioButtonTrue)
                _editControls.Add(radioButtonTrue.ID, radioButtonTrue)

                ''REV:MIA AMEX SURCHARGE -- insert IsSurchargeSplit radio button here
                If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                    If attribute.teaFieldName.Contains("PtmIsActive") Then
                        ''Flag to display popup
                        DisplayPopup = True
                        '----------------------------------------------------------
                        ' for split surcharge
                        '---------------------------------------------------------
                        Aurora.TableEditor.Data.IsNeedToUpdateSurchargeSplit = True
                        Dim tableCellName As New System.Web.UI.WebControls.TableCell()
                        tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
                        tableCellName.Style.Add("padding-top", "6px")
                        tableCellName.VerticalAlign = VerticalAlign.Top
                        tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode("Surcharge Split:")))
                        ActiveSurchargeRow.Cells.Add(tableCellName)

                        Dim tableCellNull As New System.Web.UI.WebControls.TableCell()
                        tableCellNull.Style.Add("padding-top", "6px")
                        tableCellNull.VerticalAlign = VerticalAlign.Top
                        ActiveSurchargeRow.Cells.Add(tableCellNull)

                        Dim activeCellField As TableCell = New TableCell

                        Dim rdoPtmNotActive As RadioButton = New RadioButton
                        rdoPtmNotActive.ID = "radioButtonPtmNotActive"
                        rdoPtmNotActive.Text = "False"
                        rdoPtmNotActive.Checked = True
                        rdoPtmNotActive.GroupName = "radioButtonSurchargeGroup"
                        AddHandler rdoPtmNotActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        Dim rdoPtmActive As RadioButton = New RadioButton
                        rdoPtmActive.ID = "radioButtonPtmActive"
                        rdoPtmActive.Text = "True"
                        rdoPtmActive.AutoPostBack = True
                        rdoPtmActive.GroupName = "radioButtonSurchargeGroup"
                        AddHandler rdoPtmActive.CheckedChanged, AddressOf Surcharge_CheckChanged


                        activeCellField.Controls.Add(rdoPtmNotActive)
                        ActiveSurchargeRow.Cells.Add(activeCellField)

                        _editControls.Add(rdoPtmNotActive.ID, rdoPtmNotActive)

                        activeCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
                        ActiveSurchargeRow.Cells.Add(activeCellField)

                        activeCellField.Controls.Add(rdoPtmActive)
                        ActiveSurchargeRow.Cells.Add(activeCellField)
                        _editControls.Add(rdoPtmActive.ID, rdoPtmActive)



                        '----------------------------------------------------------
                        ' for refund surcharge
                        '---------------------------------------------------------
                        tableCellName = New TableCell
                        tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
                        tableCellName.Style.Add("padding-top", "6px")
                        tableCellName.VerticalAlign = VerticalAlign.Top
                        tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode("Refund Surcharge:")))
                        ActiveRefundSurchargeRow.Cells.Add(tableCellName)

                        tableCellNull = New TableCell
                        tableCellNull.Style.Add("padding-top", "6px")
                        tableCellNull.VerticalAlign = VerticalAlign.Top
                        ActiveRefundSurchargeRow.Cells.Add(tableCellNull)

                        Dim rdoPtmNotRefundSurchargeActive As RadioButton = New RadioButton
                        rdoPtmNotRefundSurchargeActive.ID = "radioButtonPtmNotRefundSurchargeActive"
                        rdoPtmNotRefundSurchargeActive.Text = "False"
                        rdoPtmNotRefundSurchargeActive.Checked = True
                        rdoPtmNotRefundSurchargeActive.GroupName = "radioButtonRefundSurchargeGroup"
                        AddHandler rdoPtmNotRefundSurchargeActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        Dim rdoRefundSurchargePtmActive As RadioButton = New RadioButton
                        rdoRefundSurchargePtmActive.ID = "radioButtonPtmRefundSurchargePtmActive"
                        rdoRefundSurchargePtmActive.Text = "True"
                        rdoRefundSurchargePtmActive.AutoPostBack = True
                        rdoRefundSurchargePtmActive.GroupName = "radioButtonRefundSurchargeGroup"
                        AddHandler rdoRefundSurchargePtmActive.CheckedChanged, AddressOf Surcharge_CheckChanged

                        activeCellField = Nothing
                        activeCellField = New TableCell
                        activeCellField.Controls.Add(rdoPtmNotRefundSurchargeActive)
                        ActiveRefundSurchargeRow.Cells.Add(activeCellField)

                        _editControls.Add(rdoPtmNotRefundSurchargeActive.ID, rdoPtmNotRefundSurchargeActive)

                        activeCellField.Controls.Add(New LiteralControl("&nbsp;&nbsp;"))
                        ActiveRefundSurchargeRow.Cells.Add(activeCellField)

                        activeCellField.Controls.Add(rdoRefundSurchargePtmActive)
                        ActiveRefundSurchargeRow.Cells.Add(activeCellField)
                        _editControls.Add(rdoRefundSurchargePtmActive.ID, rdoRefundSurchargePtmActive)

                        Dim aryQry As NameValueCollection = Request.Params
                        Dim blnActive As Boolean
                        'Dim ie As IEnumerable = aryQry.GetEnumerator
                        For Each s As String In aryQry.AllKeys
                            If s.Contains("Attribute") Then
                                Dim retValueForRefund As Boolean = False
                                blnActive = Aurora.TableEditor.Data.GetPaymentMethodSplitSurchargeValue(aryQry(s), retValueForRefund)
                                If Not blnActive Then
                                    rdoPtmNotActive.Checked = True
                                    rdoPtmActive.Checked = False
                                Else
                                    rdoPtmActive.Checked = True
                                    rdoPtmNotActive.Checked = False
                                End If

                                If Not retValueForRefund Then
                                    rdoPtmNotRefundSurchargeActive.Checked = True
                                    rdoRefundSurchargePtmActive.Checked = False
                                Else
                                    rdoPtmNotRefundSurchargeActive.Checked = False
                                    rdoRefundSurchargePtmActive.Checked = True
                                End If

                                Exit For
                            End If
                        Next

                    End If
                End If



                If checkBoxNull IsNot Nothing Then
                    radioButtonFalse.Attributes.Add("onclick", "document.getElementById('" & checkBoxNull.ClientID & "').checked = false;")
                    radioButtonTrue.Attributes.Add("onclick", "document.getElementById('" & checkBoxNull.ClientID & "').checked = false;")
                End If

            Else
                Dim textBox As New System.Web.UI.WebControls.TextBox
                textBox.ID = "Edit_" & attribute.ID & "_TextBox"
                textBox.Width = New System.Web.UI.WebControls.Unit("600px")
                If attribute.teaTextRowCount > 1 Then
                    textBox.TextMode = TextBoxMode.MultiLine
                    textBox.Rows = attribute.teaTextRowCount
                    textBox.Wrap = attribute.teaTextWrap
                Else
                    textBox.TextMode = TextBoxMode.SingleLine
                End If

                ''reV:mia March 17,2009 Phoenix
                ''RE-5 / RE-7
                Dim checkIsActive As CheckBox = Nothing
                If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                    If attribute.teaFieldName.Equals("LocZoneCode") Then
                        textBox.MaxLength = 3
                        textBox.Width = New Unit(100)
                        checkIsActive = New CheckBox
                        checkIsActive.ID = "checkIsActive"
                        checkIsActive.AutoPostBack = True
                        AddHandler checkIsActive.CheckedChanged, AddressOf checkIsActive_CheckChanged
                    End If
                    If attribute.teaFieldName.Equals("LocZoneName") Then
                        textBox.MaxLength = 64
                        textBox.Width = New Unit(610)
                    End If
                End If

                textBox.Text = "" & attribute.DataValueAsString()

                ''reV:mia March 17,2009 Phoenix
                ''RE-5 / RE-7
                If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                    If attribute.teaFieldName.Equals("LocZoneCode") Then
                        tableCellField.Controls.Add(textBox)
                        _editControls.Add(textBox.ID, textBox)
                        Dim litMsg As Literal = New Literal
                        litMsg.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Is Active?:"
                        tableCellField.Controls.Add(litMsg)
                        tableCellField.Controls.Add(checkIsActive)
                        _editControls.Add(checkIsActive.ID, checkIsActive)
                        checkIsActive.Checked = Aurora.TableEditor.Data.DataRepository.GetLocationZoneStatus(attribute.DataValueAsString)
                    Else
                        tableCellField.Controls.Add(textBox)
                        _editControls.Add(textBox.ID, textBox)
                    End If
                Else
                    tableCellField.Controls.Add(textBox)
                    _editControls.Add(textBox.ID, textBox)
                End If

                ''tableCellField.Controls.Add(textBox)
                ''_editControls.Add(textBox.ID, textBox)

                If checkBoxNull IsNot Nothing Then
                    textBox.Attributes.Add("onchange", "document.getElementById('" & checkBoxNull.ClientID & "').checked = document.getElementById('" & textBox.ClientID & "').value == '';")
                End If
            End If
        Else
            Dim textBox As New System.Web.UI.WebControls.TextBox
            textBox.ID = "Edit_" & attribute.ID & "_TextBox"
            textBox.Width = New System.Web.UI.WebControls.Unit("600px")
            If attribute.teaTextRowCount > 1 Then
                textBox.TextMode = TextBoxMode.MultiLine
                textBox.Rows = attribute.teaTextRowCount
                textBox.Wrap = attribute.teaTextWrap
            Else
                textBox.TextMode = TextBoxMode.SingleLine
            End If
            textBox.ReadOnly = True
            textBox.CssClass = "inputreadonly"

            Dim text As String = ""
            If attribute.DataValue() IsNot Nothing Then
                If attribute.DataType Is GetType(Boolean) Then
                    Dim dataValue As Boolean = False
                    If attribute.DataValueAsString IsNot Nothing Then dataValue = Boolean.TryParse(attribute.DataValueAsString.ToString(), dataValue)
                    text = dataValue.ToString()
                ElseIf attribute.LookupDataTable IsNot Nothing Then
                    text = LookupDataTable.GetTextByValue(attribute.LookupDataTable, attribute.DataValue)
                Else
                    text = attribute.DataValueAsString()
                End If
            End If
            textBox.Text = text

            tableCellField.Controls.Add(textBox)
            _editControls.Add(textBox.ID, textBox)
        End If
    End Sub


    Public Sub SetEntity(ByVal tableEditorEntityRow As TableEditorEntityRow)
        If tableEditorEntityRow Is Nothing Then
            Throw New ArgumentNullException()
        End If
        _tableEditorEntityRow = tableEditorEntityRow

        entityNameLabel.Text = Server.HtmlEncode("Edit " & _tableEditorEntityRow.teeName)

        editFieldTable.Rows.Clear()
        Dim index As Integer = 0
        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.GetTableEditorAttributeRows()
            If Not (attribute.teaIsUpdateable OrElse attribute.teaIsViewable) Then Continue For

            Dim tableRow As New System.Web.UI.WebControls.TableRow()
            editFieldTable.Rows.Add(tableRow)

            Dim tableCellName As New System.Web.UI.WebControls.TableCell()
            tableCellName.Style.Add("padding-top", "6px")
            tableCellName.VerticalAlign = VerticalAlign.Top
            tableCellName.Width = New System.Web.UI.WebControls.Unit("150px")
            tableCellName.Controls.Add(New LiteralControl(Server.HtmlEncode(attribute.teaName & ":")))
            tableRow.Cells.Add(tableCellName)

            Dim tableCellNull As New System.Web.UI.WebControls.TableCell()
            tableCellNull.Style.Add("padding-top", "6px")
            tableCellNull.VerticalAlign = VerticalAlign.Top
            tableRow.Cells.Add(tableCellNull)

            Dim checkBoxNull As System.Web.UI.WebControls.CheckBox = Nothing
            If attribute.teaIsNullable AndAlso _tableEditorEntityRow.IsUpdateable() AndAlso attribute.teaIsUpdateable() AndAlso attribute.IsteaParentIdNull Then
                checkBoxNull = New System.Web.UI.WebControls.CheckBox()
                checkBoxNull.ID = "Edit_NULL_" & attribute.ID & "_CheckBox"
                checkBoxNull.Checked = attribute.DataValue Is Nothing
                tableCellNull.Controls.Add(checkBoxNull)
                _editControls.Add(checkBoxNull.ID, checkBoxNull)
            Else
                tableCellNull.Controls.Add(New LiteralControl("&nbsp;"))
            End If

            Dim tableCellField As New System.Web.UI.WebControls.TableCell()
            tableRow.Cells.Add(tableCellField)

            ''REV:MIA AMEX SURCHARGE -- insert IsSurchargeSplit radio button here
            If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                If attribute.teaFieldName.Contains("PtmIsActive") Then
                    DisplayPopup = True
                    Dim ActiveRow As TableRow = New TableRow
                    Dim activeRefundSurchargeRow As TableRow = New TableRow
                    InitEditControls(tableCellField, attribute, checkBoxNull, ActiveRow, activeRefundSurchargeRow)
                    editFieldTable.Rows.Add(ActiveRow)
                    editFieldTable.Rows.Add(activeRefundSurchargeRow)
                Else
                    InitEditControls(tableCellField, attribute, checkBoxNull)
                End If
            Else
                InitEditControls(tableCellField, attribute, checkBoxNull)
            End If
            ''InitEditControls(tableCellField, attribute, checkBoxNull)

            If Not attribute.IsteaDescriptionNull AndAlso attribute.teaDescription.Trim().Length > 0 Then
                Dim tableDescriptionRow As New System.Web.UI.WebControls.TableRow()
                editFieldTable.Rows.Add(tableDescriptionRow)

                Dim tableCellDescription As New System.Web.UI.WebControls.TableCell()
                tableCellDescription.ColumnSpan = 2
                tableRow.Cells.Add(tableCellDescription)

                tableCellName.Controls.Add(New LiteralControl("<pre>" & Server.HtmlEncode(attribute.teaDescription) & "</pre>"))
            End If

            index += 1
        Next

        updateButton.Visible = _tableEditorEntityRow.IsUpdateable
        updateBackButton.Visible = _tableEditorEntityRow.IsUpdateable
        deleteButton.Visible = _tableEditorEntityRow.IsDeletable
    End Sub


    Private Sub GetEditValues(ByVal attribute As TableEditorAttributeRow, ByVal updateParams As Dictionary(Of String, Object))
        Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateSurchargeSplit = False
        If _tableEditorEntityRow.IsUpdateable() AndAlso attribute.teaIsUpdateable() AndAlso attribute.IsteaParentIdNull Then
            Dim paramValue As Object
            Dim checkBoxNull As System.Web.UI.WebControls.CheckBox = Nothing
            If attribute.teaIsNullable Then checkBoxNull = _editControls("Edit_NULL_" & attribute.ID & "_CheckBox")

            If attribute.teaIsNullable AndAlso checkBoxNull IsNot Nothing AndAlso checkBoxNull.Checked Then
                paramValue = DBNull.Value

            ElseIf attribute.LookupDataTable IsNot Nothing Then
                Dim dropDownList As System.Web.UI.WebControls.DropDownList = _editControls("Edit_" & attribute.ID & "_DropDownList")

                If attribute.teaIsNullable AndAlso dropDownList.SelectedIndex = 0 Then
                    paramValue = DBNull.Value
                Else
                    paramValue = dropDownList.SelectedValue
                End If

            ElseIf attribute.DataType Is GetType(Boolean) Then
                Dim radioButtonTrue As System.Web.UI.WebControls.RadioButton = _editControls("Edit_" & attribute.ID & "_TrueRadioButton")
                paramValue = radioButtonTrue.Checked

                ''REV:MIA AMEX SURCHARGE
                If _tableEditorEntityRow.teeTableName.Contains("PaymentMethod") Then
                    DisplayPopup = True
                    Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateSurchargeSplit = True
                End If

            Else
                Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateActiveZone = False
                Dim textBox As System.Web.UI.WebControls.TextBox = _editControls("Edit_" & attribute.ID & "_TextBox")
                textBox.ID = "Edit_" & attribute.ID & "_TextBox"

                ''reV:mia March 17,2009 Phoenix
                'RE-6:
                If _tableEditorEntityRow.teeTableName.Contains("LocationZone") Then
                    If attribute.teaFieldName.Equals("LocZoneCode") Then
                        textBox.Text = textBox.Text.ToUpper
                    End If

                    If String.IsNullOrEmpty(textBox.Text) AndAlso attribute.teaFieldName.Equals("LocZoneCode") Then
                        Throw New Exception("Enter a valid value for ZoneCode")
                    ElseIf String.IsNullOrEmpty(textBox.Text) AndAlso attribute.teaFieldName.Equals("LocZoneName") Then
                        Throw New Exception("Enter a valid value for ZoneName")
                    End If

                    If (textBox.Text.Length < 3) AndAlso attribute.teaFieldName.Equals("LocZoneCode") Then
                        Throw New Exception("Invalid length for ZoneCode")
                    End If

                    If attribute.teaFieldName.Equals("LocZoneName") Then
                        Dim templowercase As String = textBox.Text.ToLower
                        Dim tempfirstLetter As String = templowercase.Substring(0, 1)
                        templowercase = templowercase.Remove(0, 1)
                        templowercase = tempfirstLetter.ToUpper & templowercase
                        textBox.Text = templowercase.ToLower
                    End If
                    Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue = CType(Me.FindControl("checkIsActive"), CheckBox).Checked
                    Aurora.TableEditor.Data.DataRepository.IsNeedToUpdateActiveZone = True
                    Aurora.TableEditor.Data.DataRepository.ResetChildLocationForInactiveZone(_tableEditorEntityRow.DataRow.Item(0).ToString)
                End If
                paramValue = Aurora.Common.Utility.ObjectValueParse(attribute.DataType, textBox.Text)
            End If

            updateParams.Add(attribute.NewUpdateParamName, paramValue)
        End If
    End Sub


    Private Function GetEditValues() As Dictionary(Of String, Object)
        Dim result As New Dictionary(Of String, Object)

        For Each attribute As TableEditorAttributeRow In _tableEditorEntityRow.GetTableEditorAttributeRows()
            Try
                GetEditValues(attribute, result)
            Catch
                Throw New ValidationException("Enter a valid value for " & attribute.teaName)
            End Try
        Next

        Return result
    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then
            Return
        Else
            Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue = False
        End If
    End Sub


    Private Sub GoBack(Optional ByVal message As String = Nothing)
        Dim url As String
        If Not _tableEditorEntityRow.TableEditorEntityRowParent Is Nothing Then
            url = Me.Request.Path() & _tableEditorEntityRow.TableEditorEntityRowParent.QueryStringParams(_tableEditorEntityRow.DataRow)
        Else
            url = Me.Request.Path() & TableEditorDataSet.RootFunction.QueryStringParams
        End If
        If Not String.IsNullOrEmpty(message) Then
            url &= "&message=" & Me.Server.UrlEncode(message)
        End If
        Response.Redirect(url)
    End Sub


    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        GoBack()
    End Sub


    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click, updateBackButton.Click
        If Not DisplayPopup Then
            updateButtonBlock(sender)
        Else
            PopupSender = CType(sender, Button).ID.ToUpper
            Me.ConfirmationBoxControl1.Show()
        End If
        
    End Sub


    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        If _tableEditorEntityRow Is Nothing OrElse Not _tableEditorEntityRow.IsDeletable Then
            Return
        End If

        ''rev:mia phoenix
        Dim locZonecode As String = _tableEditorEntityRow.DataRow.Item(2).ToString
        Dim locZonecodeDesc As String = _tableEditorEntityRow.DataRow.Item(3).ToString
        If Not String.IsNullOrEmpty(locZonecode) Then
            Dim ctr As Integer = 0
            If Not Aurora.TableEditor.Data.CheckMappedZoneBeforeDeleting(locZonecode, ctr) Then
                If ctr = 1 Then
                    MyBase.CurrentPage.SetWarningMessage("You can't delete '" & locZonecode & " - " & locZonecodeDesc & "'." & ctr & " record found that is using it.")
                Else
                    MyBase.CurrentPage.SetWarningMessage("You can't delete '" & locZonecode & " - " & locZonecodeDesc & "'." & ctr & " records found that are using it.")
                End If

                Return
            End If
        End If
        Try
            _tableEditorEntityRow.DeleteDataRow()
        Catch ex As Exception
            CType(Me.Page, AuroraPage).AddErrorMessage(ex.Message)
            Return
        End Try

        GoBack(TableEditorEntityRow.teeName & " deleted")
    End Sub

#Region "REV:MIA Amex Surcharge"

    Private Property PopupSender() As String
        Get
            Return ViewState("PopupSender")
        End Get
        Set(ByVal value As String)
            ViewState("PopupSender") = value
        End Set
    End Property
    Private Property DisplayPopup() As Boolean
        Get
            Return CBool(ViewState("DisplayPopUp"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("DisplayPopUp") = value
        End Set
    End Property

    Protected Sub Surcharge_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)
        If CType(sender, RadioButton).ID = "radioButtonPtmActive" Then
            Aurora.TableEditor.Data.ActiveSplitSurchargeValue = CType(sender, RadioButton).Checked
        End If
        If CType(sender, RadioButton).ID = "radioButtonPtmNotActive" Then
            Aurora.TableEditor.Data.ActiveSplitSurchargeValue = False
        End If

        If CType(sender, RadioButton).ID = "radioButtonPtmRefundSurchargePtmActive" Then
            Aurora.TableEditor.Data.ActiveRefundSurchargeValue = CType(sender, RadioButton).Checked
        End If

        If CType(sender, RadioButton).ID = "radioButtonPtmNotRefundSurchargeActive" Then
            Aurora.TableEditor.Data.ActiveRefundSurchargeValue = False
        End If

        ''flag to display popup if surcharge or split value changed
        DisplayPopup = True

    End Sub

    Sub updateButtonBlock(ByVal sender As Object, Optional ByVal SenderID As String = "")
        If _tableEditorEntityRow Is Nothing OrElse Not _tableEditorEntityRow.IsUpdateable Then
            Return
        End If

        Dim updateParams As Dictionary(Of String, Object)
        Try
            updateParams = GetEditValues()
        Catch ex As ValidationException
            Me.CurrentPage.AddWarningMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        Try
            _tableEditorEntityRow.UpdateDataRow(updateParams)
        Catch ex As Exception
            Me.CurrentPage.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Return
        End Try

        If sender Is updateBackButton Or SenderID.Contains("UpdateBackButton".ToUpper) Then
            GoBack(_tableEditorEntityRow.teeName & " updated")

        Else
            Response.Redirect(Me.Request.Path() & _tableEditorEntityRow.QueryStringParams() & "&message=" & Me.Server.UrlEncode(_tableEditorEntityRow.teeName & " updated"))
        End If
    End Sub

    Protected Sub ConfirmationBoxControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl1.PostBack
        If leftButton Then
            updateButtonBlock(sender, PopupSender)
        End If
    End Sub

#End Region
#Region "rev:mia March 17,2009"
    Sub checkIsActive_CheckChanged(ByVal sender As Object, ByVal e As EventArgs)
        Aurora.TableEditor.Data.DataRepository.IsActiveZoneValue = CType(sender, CheckBox).Checked
        ''rev:mia phoenix
        If Not CType(sender, CheckBox).Checked Then
            Dim locZonecode As String = _tableEditorEntityRow.DataRow.Item(2).ToString
            Dim locZonecodeDesc As String = _tableEditorEntityRow.DataRow.Item(3).ToString
            If Not String.IsNullOrEmpty(locZonecode) Then
                Dim ctr As Integer = 0
                If Not Aurora.TableEditor.Data.CheckMappedZoneBeforeDeleting(locZonecode, ctr) Then
                    If ctr = 1 Then
                        MyBase.CurrentPage.SetWarningMessage(ctr & " record mapped to '" & locZonecode & " - " & locZonecodeDesc & "'. Clicking Save Button will remove the mapping. ")
                    Else
                        MyBase.CurrentPage.SetWarningMessage(ctr & " records mapped to '" & locZonecode & " - " & locZonecodeDesc & "'. Clicking Save Button will remove the mapping. ")
                    End If
                End If
            End If
        End If
        
    End Sub
    
#End Region
   
End Class
