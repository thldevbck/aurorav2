#Region "Imports"
Imports Aurora.ScheduleViewer.Services
Imports Aurora.Common
Imports System.Xml
Imports System.Data
#End Region

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ScheduleViewer)> _
Partial Class ScheduleViewer_Default
    Inherits AuroraPage

#Region "Event"

    Private _country As String
    Private _city As String
    Private _hasAdminPermission As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Dim sScript As String
            'sScript = vbNewLine & "onloadStep1();" & _
            '            vbNewLine & "onloadStep2();" & _
            '            vbNewLine & "onloadStep3();" & _
            '            vbNewLine & "onloadStep4();"

            'ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, sScript, True)

            ' If Not PageScriptManager.IsInAsyncPostBack Then
            If Not Page.IsPostBack Then
                countryDropDownListDataBind()
                cityDropDownListDataBind()
                searchDropDownListDataBind()
                controlsCollapsiblePanel.Collapsed = False

                forceUnSchedButton.Disabled = True '.Visible = False
                forceUnMaintButton.Disabled = True '.Visible = False
                SubstitutionSummary.Disabled = True '.Visible = False

            End If

            _country = Trim(countryDropDownList.SelectedValue)
            _city = Trim(cityDropDownList.SelectedValue)

            _hasAdminPermission = CommonFunction.HasAdminPermission(UserCode)

            'Response.Write(Utility.BuildScript("HasAdminPermission", Utility.ParseString(_hasAdminPermission)))
            hasAdminPermissionTextbox.Text = _hasAdminPermission.ToString()
            searchTextBox.Attributes.Add("onchange", "searchTextBox_OnTextChanged()")

            'End If
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub countryDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles countryDropDownList.SelectedIndexChanged
        cityDropDownListDataBind()
    End Sub

    Protected Sub RetrieveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles retrieveButton.Click
        Process()
    End Sub
#End Region

#Region "Databind"

    Private Sub SetNumber(ByRef scheduleObject As Schedule)
        Try
            schedTextBox.Text = Utility.ParseInt(scheduleObject.scheduleCount, 0)
            unScheduleTextBox.Text = Utility.ParseInt(scheduleObject.unScheduleCount, 0)
            relocationTextBox.Text = Utility.ParseInt(scheduleObject.relocationCount, 0)
            maintTextBox.Text = Utility.ParseInt(scheduleObject.maintCount, 0)
            unMaintTextBox.Text = Utility.ParseInt(scheduleObject.unMaintCount, 0)
            disposalTextBox.Text = Utility.ParseInt(scheduleObject.disposalCount, 0)
            unDisposalTextBox.Text = Utility.ParseInt(scheduleObject.unDisposalCount, 0)

            If Utility.ParseInt(scheduleObject.unScheduleCount, 0) > 0 Then
                unScheduleTextBox.ForeColor = Drawing.Color.Red
            End If
            If Utility.ParseInt(scheduleObject.unMaintCount, 0) > 0 Then
                unMaintTextBox.ForeColor = Drawing.Color.Red
            End If
            If Utility.ParseInt(scheduleObject.unDisposalCount, 0) > 0 Then
                unDisposalTextBox.ForeColor = Drawing.Color.Red
            End If
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Private Sub cityDropDownListDataBind()
        Try
            _country = Trim(countryDropDownList.SelectedValue)

            'Call GetVehicleCities
            cityDropDownList.DataSource = Nothing
            cityDropDownList.Items.Clear()
            cityDropDownList.Items.Add(New ListItem("ALL", ""))

            Dim vehicleObject As Vehicle
            vehicleObject = New Vehicle(_country, _city, UserCode)

            vehicleObject.GetVehicleCities()
            cityDropDownList.DataSource = vehicleObject.LocationList
            cityDropDownList.DataBind()

        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Private Sub countryDropDownListDataBind()
        Try
            'countryDropDownList
            Dim country() As String = {"AU", "NZ"}
            countryDropDownList.DataSource = country
            countryDropDownList.DataBind()
            countryDropDownList.SelectedValue = CountryCode
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Private Sub searchDropDownListDataBind()
        Try
            'searchDropDownList
            searchDropDownList.DataSource = Nothing
            searchDropDownList.Items.Add(New ListItem("UNIT NO.", "1"))
            searchDropDownList.Items.Add(New ListItem("MODEL", "2"))
            searchDropDownList.Items.Add(New ListItem("REGO", "3"))
            searchDropDownList.Items.Add(New ListItem("RENTAL", "4"))
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

#End Region

#Region "Functions"

    Protected Sub Process()
        Try
            'set onload javascript and vbScript
            Dim sScript As String
            sScript = vbNewLine & "onloadStep1();" & _
                        vbNewLine & "onloadStep2();" & _
                        vbNewLine & "onloadStep3();" & _
                        vbNewLine & "onloadStep4();"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, sScript, True)

            'Call SetupVehicles
            Dim vehicleObject As Vehicle
            vehicleObject = New Vehicle(_country, _city, UserCode)

            Dim vehicleXmlDoc As XmlDocument = Nothing
            vehicleXmlDoc = vehicleObject.SetupVehicles()
            'Response.Write(CommonFunction.BuildXmlScript("vehicleXml", vehicleXmlDoc))
            vehicleXmlTextbox.Text = vehicleXmlDoc.InnerXml

            'GetVehicleStartingPosition
            Dim vechicleStartingPointXmlDoc As XmlDocument = Nothing
            vechicleStartingPointXmlDoc = vehicleObject.GetVehicleStartingPosition()
            'Response.Write(CommonFunction.BuildXmlScript("vechicleStartingPointXml", vechicleStartingPointXmlDoc))
            vechicleStartingPointXmlTextbox.Text = vechicleStartingPointXmlDoc.InnerXml

            cityDropDownListDataBind()

            'SetupSubs
            Dim substitutionObject As Substitution
            substitutionObject = New Substitution(_country, UserCode)
            substitutionObject.SetupSubs()

            'Setup schedule
            'Generate xmlDocs
            Dim scheduleObject As Schedule
            scheduleObject = New Schedule(_country, UserCode, vehicleObject, substitutionObject)
            scheduleObject.SetupScheduled()
            'Scheduled Rentals....
            'Response.Write(CommonFunction.BuildXmlScript("scheduleXml", scheduleObject.ScheduleXmlDoc))
            scheduleXmlTextbox.Text = scheduleObject.ScheduleXmlDoc.InnerXml

            'Unscheduled Rentals....
            unschedDropDownList.DataSource = Nothing
            unschedDropDownList.Items.Clear()

            If scheduleObject.UnScheduleArrayList.Count <> 0 Then
                unschedDropDownList.DataSource = scheduleObject.UnScheduleArrayList
                unschedDropDownList.DataBind()
                If _hasAdminPermission Then
                    forceUnSchedButton.Disabled = False '.Visible = True
                End If
            End If


            'Response.Write(CommonFunction.BuildXmlScript("unScheduleXml", scheduleObject.UnScheduleXmlDoc))
            unScheduleXmlTextbox.Text = scheduleObject.UnScheduleXmlDoc.InnerXml

            'Scheduled Relocations....
            'Response.Write(CommonFunction.BuildXmlScript("relocationXml", scheduleObject.RelocationXmlDoc))
            relocationXmlTextbox.Text = scheduleObject.RelocationXmlDoc.InnerXml

            'Scheduled OffFleets....
            'Response.Write(CommonFunction.BuildXmlScript("maintXml", scheduleObject.MaintXmlDoc))
            maintXmlTextbox.Text = scheduleObject.MaintXmlDoc.InnerXml

            ' Unscheduled OffFleets....
            unMaintDropDownList.DataSource = Nothing
            unMaintDropDownList.Items.Clear()
            If scheduleObject.UnMaintArrayList.Count <> 0 Then
                unMaintDropDownList.DataSource = scheduleObject.UnMaintArrayList
                unMaintDropDownList.DataBind()
                If _hasAdminPermission Then
                    forceUnMaintButton.Disabled = False '.Visible = True
                End If
            Else
                unMaintDropDownList.Enabled = False '.Visible = False
                'unMaintLabel.Visible = False
            End If

            'disposals....
            'Response.Write(CommonFunction.BuildXmlScript("disposalXml", scheduleObject.disposalXmlDoc))
            disposalXmlTextbox.Text = scheduleObject.DisposalXmlDoc.InnerXml

            'Setup current
            'Generate xmlDocs
            Dim currentObject As Current
            currentObject = New Current(_country, UserCode, vehicleObject)
            currentObject.SetupCurrent()

            'Current rental
            'Response.Write(CommonFunction.BuildXmlScript("currentRentalXml", currentObject.CurrentRentalXmlDoc))
            currentRentalXmlTextbox.Text = currentObject.CurrentRentalXmlDoc.InnerXml

            'Current maint
            'Response.Write(CommonFunction.BuildXmlScript("currentMaintXml", currentObject.CurrentMaintXmlDoc))
            currentMaintXmlTextbox.Text = currentObject.CurrentMaintXmlDoc.InnerXml

            SetNumber(scheduleObject)

            If _hasAdminPermission And substitutionObject.SubstitutionListCollection.Count > 0 Then
                SubstitutionSummary.Disabled = False '.Visible = True
                ' Generate a dataview for substituation grid
                ProcessSubstitutionGrid(substitutionObject)
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the data.")
        End Try
    End Sub

    Private Sub ProcessSubstitutionGrid(ByRef substitutionObject As Substitution)
        Try
            ' Create a table 
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            ' Define the columns for the table.
            dt.Columns.Add(New DataColumn("Booked", GetType(String)))
            dt.Columns.Add(New DataColumn("Scheduled", GetType(String)))
            dt.Columns.Add(New DataColumn("Count", GetType(Integer)))
            dt.Columns.Add(New DataColumn("Total", GetType(Integer)))
            dt.Columns.Add(New DataColumn("Percent", GetType(String)))

            ' Populate the  table.
            Dim i As Integer
            For i = 1 To substitutionObject.substitutionListCollection.Count
                dr = dt.NewRow()
                Dim a As ArrayList = New ArrayList()
                a = substitutionObject.substitutionListCollection(i)
                dr(0) = a.Item(0)
                dr(1) = a.Item(1)
                dr(2) = a.Item(2)
                dt.Rows.Add(dr)
            Next

            Dim dv As DataView = New DataView(dt)

            For Each r As DataRow In dt.Rows
                dv.RowFilter = "Booked='" & r.Item(0) & "'"
                Dim count As Integer = 0
                For j As Integer = 0 To dv.Count - 1
                    count = count + dv.Item(j)(2)
                Next
                r.Item(3) = count
            Next

            For Each r As DataRow In dt.Rows
                Dim percent As Double = 0.0
                percent = Convert.ToDouble(r.Item(2)) / Convert.ToDouble(r.Item(3)) * 100
                r.Item(4) = Format(percent, "0.00")

            Next

            dv = Nothing
            dv = New DataView(dt)
            dv.Sort = "Booked, Scheduled"
            Session.Add("ScheduleViewer_dvSubstitution", dv)

        Catch ex As Exception
            LogException(ex)
        End Try

    End Sub

#End Region

End Class
