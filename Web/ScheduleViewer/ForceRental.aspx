<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ForceRental.aspx.vb" Inherits="ScheduleViewer_ForceRental" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Force Rental</title>
    <link rel="stylesheet" type="text/css" href="../Include/Css/Form.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Global.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Messages.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Popups.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Remove.css" />
    <style type="text/css">   
        .titlelarge
        {
            font-weight: bold;
            font-size: 15px;
            color: #FFF;
            font-family: arial;
        }
    </style>
</head>
<body background="../Images/scheduleBg.png">
    <form id="form1" runat="server">
        <div>
        <asp:Label ID="titleLabel" runat="server" Text="Force Rental"  CssClass="titlelarge"></asp:Label>
        <br /><br />
            <asp:Label ID="errorLabel" runat="server" ForeColor="Red" Font-Names="Arial"></asp:Label><br />
            <asp:ValidationSummary ID="validationSummary" DisplayMode="BulletList" runat="server" Font-Names="Arial" />

            <table style="width: 580px">
                <tr>
                    <td style="width: 15%">
                        <asp:Label ID="Label1" runat="server" Text="Rental:"></asp:Label>
                        <asp:RequiredFieldValidator ID="rentalRequiredFieldValidator" runat="server" ControlToValidate="rentalTextBox"
                            ErrorMessage="A rental is required." >*</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="RentalCustomValidator" runat="server" ControlToValidate="rentalTextBox"
                            ErrorMessage="Rental is invalid" OnServerValidate="RentalCustomValidator_OnServerValidate">*</asp:CustomValidator>        
                    </td>
                    <td>
                        <asp:TextBox ID="rentalTextBox" runat="server" Width="50%" MaxLength="20"></asp:TextBox>&nbsp;*</td>
                    <td style="width: 10%">
                    </td>
                </tr>
                <tr>
                    <td style="width: 15%">
                        <asp:Label ID="Label2" runat="server" Text="Vehicle(s):"></asp:Label>
                        <asp:RequiredFieldValidator ID="vehicleRequiredFieldValidator" runat="server" ControlToValidate="vehicleTextBox"
                           ErrorMessage="Vehicle(s) is required." >*</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="VehicleListCustomValidator" runat="server" ControlToValidate="vehicleTextBox"
                            ErrorMessage=" Vehicle(s) is invalid." OnServerValidate="VehicleListCustomValidator_OnServerValidate">*</asp:CustomValidator>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="vehicleTextBox" runat="server" Width="95%" MaxLength="50"></asp:TextBox>&nbsp;*</td>
                </tr>
                <tr>
                    <td style="width: 15%" valign=top >
                        <asp:Label ID="Label3" runat="server" Text="Note:"></asp:Label>
                        <asp:RequiredFieldValidator ID="noteRequiredFieldValidator" runat="server" ControlToValidate="noteTextBox"
                            ErrorMessage="A note is required.">*</asp:RequiredFieldValidator>
                        </td>
                    <td colspan="2">
                        <asp:TextBox ID="noteTextBox" runat="server" Width="95%" Rows="3" TextMode="MultiLine" MaxLength="512"></asp:TextBox>&nbsp;*</td>
                </tr>
                <tr>
                 <td align=right colspan=3>
                        <asp:Button ID="testDvassButton" runat="server" Text="About" CausesValidation="False" CssClass="Button_Standard"  />
                        <asp:Button ID="forceRentalButton" runat="server" Text="Submit/Save" CssClass="Button_Standard" />
                    
                        <input id="closeButton" type="button" value="Close" onclick="window.close()" CausesValidation="False" class="Button_Standard"  /></td>
                </tr>
                <tr>
                    <td style="width: 15%; height: 21px">
                    </td>
                    <td colspan="2" style="height: 21px">
                        <asp:Label ID="notesLabel" runat="server" Width="100%"></asp:Label></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
