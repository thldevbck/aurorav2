
Imports System.Data
Imports Aurora.ScheduleViewer.Services

<AuroraFunctionCodeAttribute("SCHEDULEVIEW")> _
Partial Class ScheduleViewer_SubstitutionSummary
    Inherits AuroraPage

    Dim dv As DataView
    Private _country As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            _country = Request.QueryString("country").ToString()

            dv = New DataView
            If Not Session.Item("ScheduleViewer_dvSubstitution") Is Nothing Then
                dv = Session.Item("ScheduleViewer_dvSubstitution")
            Else
                GetSubstitutionList()
                Session.Add("ScheduleViewer_dvSubstitution", dv)
            End If

            dv.Sort = "Booked, Scheduled"
            GridView1.DataSource = Nothing
            GridView1.DataSource = dv
            GridView1.DataBind()
            If dv.Table.Rows.Count <= 0 Then
                exportButton.Enabled = False
            Else
                exportButton.Enabled = True
            End If
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Protected Sub exportButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles exportButton.Click
        Try
            If Not Session.Item("ScheduleViewer_dvSubstitution") Is Nothing Then
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=Substitutions.txt")

                Response.ContentType = "application/octet-stream"

                Dim output As StringBuilder = New StringBuilder

                output.Append("""Booked"",""Scheduled"",""Count"",""Total"",""Percent""" & Environment.NewLine)

                For i As Integer = 0 To dv.Count - 1
                    Dim row As DataRowView
                    row = dv.Item(i)

                    output.Append(""" " & row.Item("Booked") & """,")
                    output.Append(""" " & row.Item("Scheduled") & """,")
                    output.Append(row.Item("Count") & ",")
                    output.Append(row.Item("Total") & ",")
                    output.Append(row.Item("Percent"))
                    output.Append(Environment.NewLine)
                Next
                Response.Write(output)

                Response.End()
            End If
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Private Sub GetSubstitutionList()
        Try
            'Call SetupVehicles
            Dim vehicleObject As Vehicle
            vehicleObject = New Vehicle(_country, "", UserCode)
            vehicleObject.SetupVehicles()

            'SetupSubs
            Dim substitutionObject As Substitution
            substitutionObject = New Substitution(_country, UserCode)
            substitutionObject.SetupSubs()

            'Setup schedule
            'Generate xmlDocs
            Dim scheduleObject As Schedule
            scheduleObject = New Schedule(_country, UserCode, vehicleObject, substitutionObject)
            scheduleObject.SetupScheduled()

            ' Create a table 
            Dim dt As DataTable = New DataTable()
            Dim dr As DataRow

            ' Define the columns for the table.
            dt.Columns.Add(New DataColumn("Booked", GetType(String)))
            dt.Columns.Add(New DataColumn("Scheduled", GetType(String)))
            dt.Columns.Add(New DataColumn("Count", GetType(Integer)))
            dt.Columns.Add(New DataColumn("Total", GetType(Integer)))
            dt.Columns.Add(New DataColumn("Percent", GetType(String)))

            ' Populate the  table.
            Dim i As Integer
            For i = 1 To substitutionObject.SubstitutionListCollection.Count
                dr = dt.NewRow()
                Dim a As ArrayList = New ArrayList()
                a = substitutionObject.SubstitutionListCollection(i)
                dr(0) = a.Item(0)
                dr(1) = a.Item(1)
                dr(2) = a.Item(2)
                dt.Rows.Add(dr)
            Next

            Dim dv As DataView = New DataView(dt)

            For Each r As DataRow In dt.Rows
                dv.RowFilter = "Booked='" & r.Item(0) & "'"
                Dim count As Integer = 0
                For j As Integer = 0 To dv.Count - 1
                    count = count + dv.Item(j)(2)
                Next
                r.Item(3) = count
            Next

            For Each r As DataRow In dt.Rows
                Dim percent As Double = 0.0
                percent = Convert.ToDouble(r.Item(2)) / Convert.ToDouble(r.Item(3)) * 100
                r.Item(4) = Format(percent, "0.00")

            Next

            dv = Nothing
            dv = New DataView(dt)

        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

End Class
