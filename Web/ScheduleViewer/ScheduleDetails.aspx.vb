#Region "Imports"
Imports Aurora.ScheduleViewer.Services

#End Region

<AuroraFunctionCodeAttribute("SCHEDULEVIEW")> _
Partial Class ScheduleViewer_ScheduleDetails
    Inherits AuroraPage

    Property RentalId() As String
        Get
            Return CStr(ViewState("SCH_VWR_RNT_ID"))
        End Get
        Set(ByVal value As String)
            ViewState("SCH_VWR_RNT_ID") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim output As String
            Dim type As String

            type = Request("type").ToString()

            bookingButton.Visible = False

            Select Case type
                Case "booking"
                    titleLabel.Text = "Booking Details"

                    Dim rentalInfo As String
                    rentalInfo = Request("param1").ToString()
                    rentalInfo = rentalInfo.Replace("Revenue: ", "<br/>Revenue: ")

                    Dim vehicleInfo As String = ""
                    vehicleInfo = Request("param2").ToString()

                    Me.Header.Title = "Unit: " & vehicleInfo

                    output = GetDetails(rentalInfo, vehicleInfo)

                Case "vehicle"
                    titleLabel.Text = "Vehicle Details"

                    Dim vehicleInfo As String
                    vehicleInfo = Request("param1").ToString()
                    output = vehicleInfo

                    Dim unitInfo As String = ""
                    unitInfo = Request("param2").ToString()
                    Me.Header.Title = "Unit: " & unitInfo

                Case Else
                    output = ""
            End Select

            ' fix by shoel
            output = output.Replace("\r\n", "<br/>").Replace("\n\r", "<br/>")

            descriptionLabel.Text = output
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Private Function GetDetails(ByVal rentalInfo As String, ByVal vehicleInfo As String) As String

        Dim output As String = ""

        Try
            Dim i As Integer

            If rentalInfo.Contains("Rental: ") Then
                i = rentalInfo.IndexOf("Rental: ")
                If i > -1 Then
                    bookingButton.Visible = True
                    Me.Header.Title = "Unit: " & vehicleInfo
                    output = Schedule.GetMyRentalDetails(Strings.Trim(Strings.Mid(rentalInfo, i + 8)), UserCode, RentalId) & rentalInfo
                End If
            ElseIf rentalInfo.Contains("Reloc Booking: ") Then
                i = rentalInfo.IndexOf("Reloc Booking: ")
                If i > -1 Then
                    bookingButton.Visible = True
                    Me.Header.Title = "Unit: " & vehicleInfo
                    output = Schedule.GetMyRentalDetails(Strings.Trim(Strings.Mid(rentalInfo, i + 15)), UserCode, RentalId) & rentalInfo
                End If
            ElseIf rentalInfo.Contains("Provisional Booking: ") Then
                i = rentalInfo.IndexOf("Provisional Booking: ")
                If i > -1 Then
                    bookingButton.Visible = True
                    Me.Header.Title = "Unit: " & vehicleInfo
                    output = Schedule.GetMyRentalDetails(Strings.Trim(Strings.Mid(rentalInfo, i + "Provisional Booking: ".Length)), UserCode, RentalId) & rentalInfo
                End If
                'ElseIf (rentalInfo.Contains("Disposal:") Or rentalInfo.Contains("Relocation:")) Then
                ' fixes by Shoel
                '   Me.Header.Title = "Unit: " & vehicleInfo
                ' output = rentalInfo
            Else
                ' fixes by Shoel
                Me.Header.Title = "Unit: " & vehicleInfo
                output = rentalInfo

            End If
            ' fixes by Shoel
            bookingButton.Attributes.Add("onclick", "bookingButton_OnClick('../Booking/Booking.aspx?sTxtSearch=" & RentalId & "');")

            Return output
        Catch ex As Exception
            LogException(ex)
            Return output
        End Try
    End Function

End Class
