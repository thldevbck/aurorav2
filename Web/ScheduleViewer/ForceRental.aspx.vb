
#Region "Imports"
Imports Aurora.ScheduleViewer.Services
Imports Aurora.Common
Imports System.Xml
Imports System.Data
#End Region

<AuroraFunctionCodeAttribute("SCHEDULEVIEW")> _
Partial Class ScheduleViewer_ForceRental
    Inherits AuroraPage

    Private _country As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            _country = Request.QueryString("country").ToString()

            If Not Page.IsPostBack Then
                Dim booking As String = ""
                booking = Request("booking").ToString()

                If booking <> "" Then
                    Dim space As Integer
                    space = booking.IndexOf(" ")
                    rentalTextBox.Text = booking.Substring(0, space)
                End If
            End If

        Catch ex As Exception
            LogException(ex)
            errorLabel.Text = "An error occurred while loading the page."
        End Try
    End Sub

    Protected Sub forceRentalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles forceRentalButton.Click
        Try
            Dim rental As String
            Dim vehicles As String
            Dim note As String
            Dim result As String

            ' Make sure it passes all the validations
            If Page.IsValid Then

                rental = Utility.ParseString(rentalTextBox.Text, "")
                vehicles = Utility.ParseString(vehicleTextBox.Text, "")
                note = Utility.ParseString(noteTextBox.Text, "")

                'Force the rental
                result = Dvass.ForceIt(_country, UserCode, rental, vehicles, note)
                notesLabel.Text = result
            End If
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Protected Sub testDvassButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles testDvassButton.Click
        Try
            Dim text As String

            ' Test the Dvass connection and get the Dvass System State
            text = Dvass.TestDvass(_country, UserCode, True)

            notesLabel.Text = text.Replace(Environment.NewLine, "<br/>")
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Protected Sub RentalCustomValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        Try
            'Rental server validation event
            Dim rental As String
            rental = Utility.ParseString(rentalTextBox.Text, "")

            If ValidateRental(rental) Then
                e.IsValid = True
            Else
                e.IsValid = False
            End If
        Catch ex As Exception
            LogException(ex)
            errorLabel.Text = "An error occurred while validating the page."
        End Try
    End Sub

    Protected Sub VehicleListCustomValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        Try
            'Vehicle server validation event
            Dim vehicles As String
            vehicles = Utility.ParseString(vehicleTextBox.Text, "")

            If ValidateVehicles(vehicles) Then
                e.IsValid = True
            Else
                e.IsValid = False
            End If
        Catch ex As Exception
            LogException(ex)
        End Try
    End Sub

    Private Function ValidateVehicles(ByVal vehicleList As String) As Boolean
        Try

            Dim vehicleObject As Vehicle
            vehicleObject = New Vehicle(_country, "", UserCode)

            If vehicleList.Length > 0 Then
                'Do not start with "VT-"
                If Strings.UCase(Strings.Left(vehicleList, 3)) <> "VT-" Then

                    Dim vehicleArray As String()
                    vehicleArray = vehicleList.Split(New Char() {",", "-"})

                    For Each vehicleId As String In vehicleArray
                        vehicleId = Utility.ParseString(vehicleId, "")
                        If vehicleId <> "" Then
                            'Validate each vehicle
                            If Not vehicleObject.IsValidVehicle(vehicleList) Then
                                Return False
                            End If
                        End If
                    Next
                End If
            End If
            Return True

        Catch ex As Exception
            LogException(ex)
            Return False
        End Try
    End Function

    Private Function ValidateRental(ByVal rental As String) As Boolean
        Try
            If rental.IndexOf("/") > -1 And rental.IndexOf(" ") < 0 Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            LogException(ex)
            Return False
        End Try
    End Function
End Class
