<%@ Page Language="VB" AutoEventWireup="false" CodeFile="SubstitutionSummary.aspx.vb"
    Inherits="ScheduleViewer_SubstitutionSummary" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Substitution Summary</title>
    <link rel="stylesheet" type="text/css" href="../Include/AuroraHeader.css" />
</head>
<body bgcolor="#79b27b">
    <form id="form1" runat="server">
        <table style="width: 98%">
            <tr>
                <td colspan="2">
                    <asp:CustomValidator ID="exportCustomValidator" runat="server" ErrorMessage="The session timed out.  Please retreive/refresh the schedules."></asp:CustomValidator>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="width: 395px; height: 650px; overflow: scroll">
                        <asp:GridView ID="GridView1" runat="server" Width="95%" CssClass="dataTableGrid"
                            CellPadding="3" CellSpacing="0" AutoGenerateColumns="false">
                            <AlternatingRowStyle CssClass="oddRow" />
                            <RowStyle CssClass="evenRow" />
                            <Columns>
                                <asp:BoundField DataField="Booked" HeaderText="Booked" ItemStyle-Width="35%" />
                                <asp:BoundField DataField="Scheduled" HeaderText="Scheduled" ItemStyle-Width="35%" />
                                <asp:BoundField DataField="Count" HeaderText="Count" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" />
                                <asp:BoundField DataField="Percent" HeaderText="Percent" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Right" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td align="right" width="100%">
                    <asp:Button ID="exportButton" runat="server" Text="Export" CssClass="Button_Standard" />
                    <input id="closeButton" type="button" value="Close" onclick="window.close()" causesvalidation="False"
                        class="Button_Standard" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
