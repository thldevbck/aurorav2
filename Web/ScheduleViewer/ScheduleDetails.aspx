<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ScheduleDetails.aspx.vb"
    Inherits="ScheduleViewer_ScheduleDetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link rel="stylesheet" type="text/css" href="../Include/Css/Form.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Global.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Messages.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Popups.css" />
    <link rel="stylesheet" type="text/css" href="../Include/Css/Remove.css" />
    <style type="text/css">   
        .titlelarge
        {
            font-weight: bold;
            font-size: 15px;
            color: #FFF;
            font-family: arial;
        }
    </style>
</head>
<body style="width: 350px; height: 150px;" background="../Images/scheduleBg.png">
    <form id="form1" runat="server" style="left: 0px; width: 350px; height: 150px;">
        <table style="left: 20px; width: 95%;">
            <tr>
                <td width="100%">
                    <br />
                    <asp:Label ID="titleLabel" runat="server" Text="" CssClass="titlelarge"></asp:Label>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td style="height: 21px">
                    <asp:Label ID="descriptionLabel" runat="server" Text="fdfdfd"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">
                    <input id="bookingButton" type="button" value="View Booking" runat="server" class="Button_Standard" />
                    <input id="closeButton" type="button" value="Close" onclick="closeWindow();" class="Button_Standard Button_Cancel" />
                </td>
            </tr>
        </table>
    </form>
</body>

<script id='functionScript' language='javascript'>
        function closeWindow()
        {
            parent.window.returnValue = ""
            window.close();
        }    
        function bookingButton_OnClick(url)
        {
            parent.window.returnValue = url
            window.close();
        }    
</script>

</html>
