<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Main.aspx.vb" Inherits="ScheduleViewer_Default"
    MasterPageFile="~/Include/AuroraHeader.master" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
    
<asp:Content ID="Content2" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        .content-inner
        { 
            width: 98%; 
        } 
    </style>
</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">
    <div style="display: none">
        <asp:TextBox ID="hasAdminPermissionTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="vehicleXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="vechicleStartingPointXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="scheduleXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="unScheduleXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="relocationXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="maintXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="disposalXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="currentRentalXmlTextbox" runat="server" Text=""></asp:TextBox>
        <asp:TextBox ID="currentMaintXmlTextbox" runat="server" Text=""></asp:TextBox>
    </div>
    <uc1:CollapsiblePanel ID="controlsCollapsiblePanel" runat="server" Title="Menu" TargetControlID="controlsPanel"
        Width="100%" />
    <asp:Panel ID="controlsPanel" runat="server" Width="100%">
        <table id="TABLE2" width="780px">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Country:" Width="110px"></asp:Label></td>
                <td width="160px">
                    <asp:DropDownList ID="countryDropDownList" runat="server" Width="100px" AutoPostBack="True">
                    </asp:DropDownList></td>
                <td width="125px">
                </td>
                <td width="50">
                    Sched</td>
                <td style="width: 50px">
                    Not</td>
                <td width="50">
                    Relocs</td>
                <td width="50">
                    Maint</td>
                <td width="50">
                    Not</td>
                <td width="50">
                    Disp</td>
                <td width="50">
                    Not</td>
            </tr>
            <tr>
                <td style="width: 276px">
                    <asp:Label ID="Label2" runat="server" Text="City:"></asp:Label>
                </td>
                <td width="175px">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <contenttemplate>
                        <asp:DropDownList ID="cityDropDownList" runat="server" AppendDataBoundItems="True"
                            Width="100px">
                        </asp:DropDownList>
                    </contenttemplate>
                        <triggers>
                        <asp:AsyncPostBackTrigger ControlID="countryDropDownList" EventName="SelectedIndexChanged" />
                    </triggers>
                    </asp:UpdatePanel>
                </td>
                <td width="125px">
                    <asp:Button ID="retrieveButton" runat="server" Text="Retrieve" CssClass="Button_Medium Button_Search" /></td>
                <td width="50px">
                    <asp:TextBox ID="schedTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="unScheduleTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
                <td>
                    &nbsp;<asp:TextBox ID="relocationTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
                <td>
                    &nbsp;<asp:TextBox ID="maintTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="unMaintTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="disposalTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
                <td>
                    <asp:TextBox ID="unDisposalTextBox" runat="server" Width="40px" ReadOnly="True" style="text-align:right"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 276px">
                    <asp:Label ID="Label3" runat="server" Text="Search:"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="searchDropDownList" runat="server" Width="100px">
                    </asp:DropDownList></td>
                <td width="125px">
                    <input id="searchButton" type="button" onclick="searchButton_OnClick()" value="Find"
                        disabled="disabled" class="Button_Medium" size="" /></td>
                <td colspan="4">
                </td>
                <td colspan="3" rowspan="3" align="right">
                    <table>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <img src="Images/Up.bmp" onclick="upButton_OnClick()" style="cursor: hand;" alt="Up" /></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="Images/Left.bmp" onclick="leftButton_OnClick()" style="cursor: hand" alt="Left" /></td>
                            <td>
                                <img src="Images/Home.bmp" onclick="homeButton_OnClick()" alt="Home" style="cursor: hand" /></td>
                            <td>
                                <img src="Images/Right.bmp" onclick="rightButton_OnClick()" alt="Right" style="cursor: hand" /></td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <img src="Images/Down.bmp" onclick="downButton_OnClick()" alt="Down" style="cursor: hand" /></td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="Images/Minus.bmp" onclick="zoomOut_OnClick()" alt="Zoom Out" style="cursor: hand" /></td>
                            <td>
                            </td>
                            <td>
                                <img src="Images/Plus.bmp" onclick="zoomIn_OnClick()" alt="Zoom In" style="cursor: hand" /></td>
                        </tr>
                    </table>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td style="width: 276px">
                </td>
                <td>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <contenttemplate>
                         <asp:TextBox ID="searchTextBox"  runat="server" MaxLength="20"/> 
                    </contenttemplate>
                    </asp:UpdatePanel></td>
                <td>
                    <input id="findNextButton" type="button" onclick="findNextButton_OnClick()" value="Find Next"
                        disabled="disabled" class="Button_Medium" /></td>
                <td colspan="5">
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 276px">
                    <asp:Label ID="Label4" runat="server" Text="Not Scheduled:"></asp:Label></td>
                <td colspan="3">
                    <asp:DropDownList ID="unschedDropDownList" runat="server" Width="350px">
                    </asp:DropDownList></td>
                <td colspan="2">
                    <input id="forceUnSchedButton" type="button" value="Force It" onclick="forceUnSchedButton_OnClick()"
                        runat="server" class="Button_Medium" size="" /></td>
                <td>
                </td>
            </tr>
            <tr>
                <td style="width: 276px">
                    <asp:Label ID="unMaintLabel" runat="server" Text="UnSched OffFleets:" Width="100%"></asp:Label></td>
                <td colspan="3">
                    <asp:DropDownList ID="unMaintDropDownList" runat="server" Width="350px">
                    </asp:DropDownList></td>
                <td colspan="2">
                    <input id="forceUnMaintButton" type="button" value="Force It" onclick="forceUnMaintButton_OnClick()"
                        runat="server" class="Button_Medium" size="" /></td>
                <td colspan="4" align="right">
                    <input id="SubstitutionSummary" type="button" value="Substitution Summary" onclick="SubstitutionSummary_OnClick()"
                        runat="server" class="Button_Standard" style="width: 160px" /></td>
            </tr>
        </table>
        <!--
        <table onclick="divGantButton_OnClick()" style="width: 100%; background-color: #517951;
            position: static; height: 25px; cursor: hand;">
            <tr>
                <td>
                    <div class="titlelarge">
                        Schedule Viwer
                    </div>
                </td>
            </tr>
        </table>
        -->
    </asp:Panel>
    <br />
    <div id="divGant" style="width: 100%;">
        <object id='object1' classid='clsid:5220cb21-c88d-11cf-b347-00aa00a28331'>
            <param name='LPKPath' value='phGantX.lpk' />
        </object>
        <object classid='clsid:1A9D2E1D-63A4-11D3-9EC5-5C91AD000000' codebase='http://www.plexityhide.com/test/phGantXControl.cab#version=2.1.4.4'
            id='phGantX1' style="width: 100%; height: 78%;">
        </object>
    </div>
    <!-- 
   <div style="width: 100%">
        
        <table onclick="divGridButton_OnClick()" style="width: 100%; background-color: #517951;
            position: static; height: 25px; cursor: hand;">
            <tr>
                <td>
                    <div class="titlelarge">
                        Substitutions Performed
                    </div>
                </td>
            </tr>
        </table>
       
        <div id="divGrid" style="width: 100%; display: inline">
            <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#999999"
                BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="321px">
                <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" Font-Size="X-Small" />
                <AlternatingRowStyle BackColor="Gainsboro" />
            </asp:GridView>
        </div>
    </div>
 -->

    <script id='phGantXScript' type="text/javascript" language='javascript'>

        var hasAdminPermissionTextbox;
        var HasAdminPermission;
        var vehicleXmlTextbox;
        var vehicleXml;
        var vechicleStartingPointXmlTextbox;
        var vechicleStartingPointXml;
        var scheduleXmlTextbox;
        var scheduleXml;
        var unScheduleXmlTextbox;
        var unScheduleXml;
        var relocationXmlTextbox;
        var relocationXml;
        var maintXmlTextbox;
        var maintXml;
        var disposalXmlTextbox;
        var disposalXml;
        var currentRentalXmlTextbox;
        var currentRentalXml;
        var currentMaintXmlTextbox;
        var currentMaintXml; 
        
        //Starting position for search
        //It is used for Find next function
        var searchStartCount 
        
        function onloadStep1()
        {
            hasAdminPermissionTextbox = document.getElementById('<%= hasAdminPermissionTextbox.ClientID %>');   
            HasAdminPermission = hasAdminPermissionTextbox.value;
           
            vehicleXmlTextbox = document.getElementById('<%= vehicleXmlTextbox.ClientID %>');   
            vehicleXml = vehicleXmlTextbox.value;
            
            vechicleStartingPointXmlTextbox = document.getElementById('<%= vechicleStartingPointXmlTextbox.ClientID %>');   
            vechicleStartingPointXml = vechicleStartingPointXmlTextbox.value;
            
            scheduleXmlTextbox = document.getElementById('<%= scheduleXmlTextbox.ClientID %>');   
            scheduleXml = scheduleXmlTextbox.value;
            
            unScheduleXmlTextbox = document.getElementById('<%= unScheduleXmlTextbox.ClientID %>');   
            unScheduleXml = unScheduleXmlTextbox.value;
            
            relocationXmlTextbox = document.getElementById('<%= relocationXmlTextbox.ClientID %>');   
            relocationXml = relocationXmlTextbox.value;
          
            maintXmlTextbox = document.getElementById('<%= maintXmlTextbox.ClientID %>');   
            maintXml = maintXmlTextbox.value;
            
            disposalXmlTextbox = document.getElementById('<%= disposalXmlTextbox.ClientID %>');   
            disposalXml = disposalXmlTextbox.value;
            
            currentRentalXmlTextbox = document.getElementById('<%= currentRentalXmlTextbox.ClientID %>');   
            currentRentalXml = currentRentalXmlTextbox.value;
            
            currentMaintXmlTextbox = document.getElementById('<%= currentMaintXmlTextbox.ClientID %>');   
            currentMaintXml = currentMaintXmlTextbox.value;

            searchStartCount = 0
        }
        
        // Add Tree Item (vehicle)
        function AddTreeEntity(entityName)
        {
            var entity
            entity = aspnetForm.phGantX1.AddRootDataEntityTree()
            var i = entity.OwningDataList.IndexOf(entity)
            entity.CanEdit = false
            entity.Text = entityName  
        }
        
        // Search vehicle by Unit no, model and rego in the vehicleXmlDoc
        // It can be search by UNIT NO. = 1, MODEL = 2, REGO = 3, RENTAL = 4
        function FindVehicle(type, value)
        {
            var id = -1
            var entityValue = ""
        
            var vehicleXmlDoc
            vehicleXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            vehicleXmlDoc.async="false"
            
            if (vehicleXml != "" && (scheduleXml != "" || type != 4))
            {
                if (type == 4)
                {
                    vehicleXmlDoc.loadXML(scheduleXml)
                }
                else
                {
                    vehicleXmlDoc.loadXML(vehicleXml)
                }
          
                var childnodes
                childnodes = vehicleXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=searchStartCount; f<childnodes.length; f++) 
                { 
                    var node = childnodes[f] 

                    switch (type)
                    {
                    case "1":   //UNIT NO. = 1
                      entityValue = node.childNodes[1].text
                      break
                    case "2":   //MODEL = 2
                       entityValue = node.childNodes[2].text
                      break
                    case "3":   //REGO = 3
                       entityValue = node.childNodes[3].text
                      break
                    case "4":   //RENTAL = 4 - can be search by part of the rental number
                       entityValue = node.childNodes[7].text
                       var iLen
                       iLen = value.length
                       entityValue = String(entityValue).substring(0,iLen)
                      break
                    default:
                      entityValue = ""
                    }
                    
                    entityValue = entityValue.toUpperCase();
                    value = value.toUpperCase();
                                  
                    if (entityValue == value)
                    {
                        id = node.childNodes[0].text    //The tree item id
                        f = childnodes.length
                    }                     
                } 
                childnodes = null
            }
            vehicleXmlDoc = null
            
            searchStartCount++  //set the new starting position for find next
            
            return id
        }
        
        // Search schedule rental by rental number
        function FindSchedule(value)
        {  
            var id = -1
        
            var scheduleIdXmlDoc
            scheduleIdXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            scheduleIdXmlDoc.async="false"
            
            if (scheduleIdXML != "")
            {
                scheduleIdXmlDoc.loadXML(scheduleIdXML)
                var childnodes
                childnodes = scheduleIdXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                { 
                    var node = childnodes[f] 
                    var rental
                    rental = node.childNodes[1].text

                    var iLen
                    iLen = value.length
                    rental = String(rental).substring(0,iLen)
         
                    rental = rental.toUpperCase();
                    value = value.toUpperCase();
         
                    if (rental == value)
                    {
                        id = node.childNodes[2].text    //ScheduleId
                        f = childnodes.length   
                    }           
                } 
             }
            return id
        }
        
        // Search unschedule rental by rental number
        function FindUnSchedule(value)
        { 
            var id = -1
            
            var unScheduleXmlDoc
            unScheduleXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            unScheduleXmlDoc.async="false"

            if (unScheduleXml != "")
            {
                unScheduleXmlDoc.loadXML(unScheduleXml)
                var childnodes
                childnodes = unScheduleXmlDoc.documentElement.childNodes; 
              
                for(var f=0;f<childnodes.length;f++) 
                { 
                    var node = childnodes[f] 
                    var rental
                    rental = node.childNodes[7].text

                    var iLen
                    iLen = value.length
                    rental = String(rental).substring(0,iLen)

                    rental = rental.toUpperCase();
                    value = value.toUpperCase();
                    
                    if (rental == value)
                    {
                        id = node.childNodes[7].text    //unScheduleId
                        f = childnodes.length  
                    }           
                } 
             }
            return id
        } 
                 
    </script>

    <script id='phGantXVbScript' type="text/vbscript" language='vbscript'>
    
    ' Create today episode for each vehicle
    Sub showToday()
        Dim phRow
        Dim myTime
        Dim iMaxCount
        Dim iCount
        Dim dToday
        iMaxCount = aspnetForm.phGantX1.RootDataEntitiesTree.Count
        For iCount = 0 To iMaxCount - 1
            Set phRow = aspnetForm.phGantX1.RootDataEntitiesTree.Items(iCount)
            Set myTime = aspnetForm.phGantX1.AddGantTime(phRow, 1)
            myTime.Color = vbYellow
            dToday = CDate(DateValue(Now()))
            myTime.Start = dToday
            myTime.Stop = dToday + 1
            myTime.Style = tsNormal
            myTime.CanEdit = False
            myTime.Visible = True
            myTime.UserVariantReference = ""
        Next
    End Sub

    ' Create episode for activity
    function createepisode(ByVal myRow, ByVal ExCity, ByVal JDateOut, ByVal iAmPmOut, ByVal ToCity, ByVal JDateIn, ByVal iAmPmIn , ByVal sEpisode, ByVal sEpisodeType, ByVal nLayer)' as Integer
        Dim time
        Dim dOut
        Dim dIn 
        Dim sAMOut
        Dim sAMIn 
        Dim sTime 
        Dim eTime 
        Dim phRow 
        Dim iSeed 
        Dim episodeTypeDesc 
        Dim iSwapCities ' To swap to and from cities where time is to small to display them properly
        dim isBlack
        
        dim tsNormal
        dim tsSpan 
        dim tsUser
        dim tsLine 
        dim tsLineWEnd 
        dim tsWave 
        dim tsRomb 
        dim tsImage 
        dim tsPipe 
        
        tsNormal = 0
        tsSpan = 1
        tsUser = 2
        tsLine = 3
        tsLineWEnd = 4
        tsWave = 5
        tsRomb = 6
        tsImage = 7
        tsPipe = 8      
        
        dOut = DateAdd("d", JDateOut, "1-Jan-2000")
        dIn = DateAdd("d", JDateIn, "1-Jan-2000")
        Set phRow = aspnetForm.phGantX1.RootDataEntitiesTree.Items(myRow)
        ' Continue with find data entity tree from user integer reference
        Set time = aspnetForm.phGantX1.AddGantTime(phRow, nLayer)
        iSeed = time.OwningDataList.IndexOf(time)  
        time.Start = dOut
        If iAmPmOut = 1 Then
            sAMOut = "pm"
            time.Start = DateAdd("h", 12, time.Start)
        Else
            sAMOut = "am"
        End If
        time.Stop = dIn
        If iAmPmIn = 1 Then
            sAMIn = "pm"
            time.Stop = DateAdd("h", 24, time.Stop)
        Else
            sAMIn = "am"
            time.Stop = DateAdd("h", 12, time.Stop)
        End If   
        ' If the start and stop are too close we have to change the way we display them
        iSwapCities = 1
        If DateDiff("h", time.Start, time.Stop) < 12 Then
            iSwapCities = -1
        End If
        Select Case sEpisodeType
        Case "A"    ' Forced Scheduled Rental
            time.Color = vbMagenta
            time.Style = tsNormal
            episodeTypeDesc = "Fixed Rental: "
        Case "B"    ' Scheduled Rental
            If DateDiff("d", time.Start, Now()) > 0 Then
                time.Color = vbBlue
            Else
                time.Color = vbCyan
            End If
            time.Style = tsNormal
            episodeTypeDesc = "Rental: "
        Case "b"    ' Current Rental
            If DateDiff("d", time.Stop, Now()) > 0 Then     ' Overdue Return
                time.Color = vbRed
            Else
                time.Color = vbWhite
            End If
            time.Style = tsNormal
            episodeTypeDesc = "Rental: "
        Case "c"    ' FOC or cheap rental
            time.Color = &HA00080   ' A sort of purple
            time.Style = tsNormal
            episodeTypeDesc = "Rental: "
        Case "D"    ' Delayed Rental in same period on same day
            time.Color = vbRed
            time.Style = tsNormal
            episodeTypeDesc = "Delayed Rental: "
        Case "d"    ' Delayed Rental
            isBlack = 1
            time.Color = vbBlack
            time.Style = tsNormal
            episodeTypeDesc = "Delayed Rental: "
        Case "U"    ' Scheduled Rental - Non-zero cost Substitution
            time.Color = vbGreen
            time.Style = tsNormal
            episodeTypeDesc = "Substituted Rental: "
        Case "S"    ' Start on fleet
            time.Color = vbWhite
            time.Style = tsLineWEnd
            episodeTypeDesc = "On Fleet: "
        Case "F"    ' Finish on fleet (Disposal)
            time.Color = vbRed
            time.Style = tsLineWEnd
            episodeTypeDesc = "Disposal: "
        Case "O"    ' Temporary Off fleet for service or whatever
            time.Color = vbRed
            time.Style = tsRomb
            episodeTypeDesc = "Off Fleet: "
        Case "o"    ' Current Off Fleet
            time.Color = vbWhite
            time.Style = tsRomb
            episodeTypeDesc = "Off Fleet: "
        Case "R"    ' Scheduled Relocation
            time.Color = vbYellow
            time.Style = tsSpan
            episodeTypeDesc = "Relocation: "
        Case "r"    ' Relocation Booking
            If DateDiff("d", time.Start, Now()) > 0 Then
                time.Color = vbBlue
            Else
                time.Color = vbCyan
            End If
            time.Style = tsSpan
            episodeTypeDesc = "Reloc Booking: "
        Case "L"    ' Relocation Booking - non-zero cost substitution
            time.Color = vbGreen
            time.Style = tsSpan
            episodeTypeDesc = "Substituted Reloc Booking: "
        Case "a"    ' Relocation Booking - forced
            time.Color = vbMagenta
            time.Style = tsSpan
            episodeTypeDesc = "Fixed Reloc Booking: "
        Case "N"    ' Provisional Booking
            time.Color = &H80FF&     'orange
            time.Style = tsNormal
            episodeTypeDesc = "Provisional Booking: "
        Case Else    ' ?
            time.Color = vbGreen
            time.Style = tsLineWEnd
            episodeTypeDesc = "?: "
        End Select

        time.UserVariantReference = episodeTypeDesc & sEpisode & " \r\nScheduled: " & CStr(dOut) & sAMOut & " -> "
        time.UserVariantReference = time.UserVariantReference & CStr(dIn) & sAMIn & "\r\n"
        time.UserVariantReference = time.UserVariantReference & ExCity & " - " & ToCity
        
        time.UserIntegerReference = myRow
        time.CanEdit = False
        Set sTime = aspnetForm.phGantX1.AddGantTime(phRow, nLayer)
        Set eTime = aspnetForm.phGantX1.AddGantTime(phRow, nLayer)
        sTime.CanEdit = False
        eTime.CanEdit = False
        sTime.Color = time.Color
        eTime.Color = time.Color
        ' Make sure we can read the cities
        If isBlack = 1 then'time.Color = vbBlack Then
            sTime.Color = vbWhite
            eTime.Color = vbWhite
        End If
        If iSwapCities = -1 Then
            sTime.Stop = time.Start
            sTime.Start = DateAdd("h", -8, time.Start)
        Else
            sTime.Start = time.Start
            sTime.Stop = DateAdd("h", 8, time.Start)
        End If
        sTime.TimeItemsTextAdd
        sTime.TimeItemsTextSet 0, ExCity, 1, 1, 1
        If iSwapCities = -1 Then
            eTime.Start = time.Stop
            eTime.Stop = DateAdd("h", 8, time.Stop)
        Else
            eTime.Stop = time.Stop
            eTime.Start = DateAdd("h", -8, time.Stop)
        End If
        
       ' msgbox sTime.Start & " " sTime.Stop & " " eTime.Start & " " eTime.Stop 
        
        eTime.TimeItemsTextAdd
        eTime.TimeItemsTextSet 0, ToCity, 1, 1, 1
        sTime.UserVariantReference = time.UserVariantReference
        eTime.UserVariantReference = time.UserVariantReference
        sTime.UserIntegerReference = time.UserIntegerReference
        eTime.UserIntegerReference = time.UserIntegerReference 
        
        createepisode = iSeed
    End function
    </script>

    <script id='functionVbScript' type="text/vbscript" language='VBScript'>
   
    ' It returns a char to represent a city code
    ' It is used by createepisode()
    function oneChar(byval sCity)
        Dim sReturn 
        sCity = Trim(sCity)
        sReturn = Left(sCity, 1)
        If sCity = "BME" Then
            sReturn = "R"
        End If
        If sCity = "ASP" Then
            sReturn = "G"
        End If
        If sCity = "MKY" Then
            sReturn = "Y"
        End If
        If sCity = "WKA" Then
            sReturn = "K"
        End If
        oneChar = sReturn
    End function
    
    ' Create schedule id xml to keep track of the activity id
    ' This will be used for rental search
    dim scheduleIdXML 
    
    sub onloadStep2()
        scheduleIdXML = ""
    End sub
    
    sub setScheduleIdXML(byVal value)
        scheduleIdXML = scheduleIdXML & value
    end sub
        
    </script>

    <script id='eventJavaScript' type="text/javascript" language='javascript'>
 
        function phGantX1_OnRightClickGantAreaJava(theGant, theDataEntity)
        {
           if (theDataEntity == null)
           {
                // No activity is selected
           }
           else if(theDataEntity.UserVariantReference != "")
           {
//               // An activity is selected
//                var windowX
//                windowX =  window.screenLeft + x - 150
//                var windowY 
//                windowY = window.screenTop + y - 100
                             
                //window.open("./ScheduleDetails.aspx?value=" + theDataEntity.UserVariantReference,"name","left=" + windowX + ",top=" + windowY + ",width=300,height=200")
                
                 var tree
                 tree = theGant.RootDataEntitiesTree.Items(theDataEntity.UserIntegerReference)
                
                 var output = window.showModalDialog("./ScheduleDetails.aspx?type=booking&param1=" + theDataEntity.UserVariantReference + "&param2=" + tree.text,"Schedule","dialogHeight: 180px; dialogWidth: 350px; dialogHide: off; edge: Raised; center: Yes; resizable: No; scroll: off; status: off; unadorned: off");
                 if ((output!=null)&&(output != ""))
                 {
                      // this must be the url!
                      //var win = window.open(output , "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=" + screen.availHeight + " ,width=" + screen.availWidth)
                      var win = window.open(output , "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=700 ,width=900")
                      //win.resizeTo(win.screen.availWidth,win.screen.availHeight);
                      //win.moveTo(0,0);
                      var LeftPosition = (screen.width) ? (screen.width-900)/2 : 0;
                      var TopPosition = (screen.height) ? (screen.height-700)/2 : 0;
                      win.moveTo(LeftPosition,TopPosition);
                 }
           }
         }
                 
         // Open forceRental page when an activity has been double clicked
         function phGantX1_OnDblClickGantAreaJava(theGant, theDataEntity)
         {
            var myTree
            var sCurrentBooking
            var nPos
            
            if ( theDataEntity != null)
            {
    
                myTree = theGant.RootDataEntitiesTree.Items(theDataEntity.UserIntegerReference)
                sCurrentBooking = theDataEntity.UserVariantReference
                
                var ddl
                ddl = $get('<%= countryDropDownList.ClientID %>')
                if (ddl.options[ddl.selectedIndex].text != "")
                {
                    // For rental
                    nPos = sCurrentBooking.indexOf("Rental: ")
                    if(nPos > -1)
                    {
                        sCurrentBooking = sCurrentBooking.substring(nPos + 8)
                        sCurrentBooking = sCurrentBooking.trim()
                        //window.open("./ForceRental.aspx?country=" + ddl.options[ddl.selectedIndex].text + "&booking=" + sCurrentBooking,"name","width=600,height=400")
                        window.open("../OpsAndLogs/VehicleAssign.aspx?popup=true&BookingText=" + sCurrentBooking,"name","width=1000,height=700")
                    }
                    
                    // For Reloc Booking
                    nPos = sCurrentBooking.indexOf("Reloc Booking: ")
                    if(nPos > -1)
                    {
                        sCurrentBooking = sCurrentBooking.substring(nPos + 15)
                        sCurrentBooking = sCurrentBooking.trim()
                        //window.open("./ForceRental.aspx?country=" + ddl.options[ddl.selectedIndex].text + "&booking=" + sCurrentBooking,"name","width=600,height=400")
                        window.open("../OpsAndLogs/VehicleAssign.aspx?popup=true&BookingText=" + sCurrentBooking,"name","width=1000,height=700")
                    }  
                } 
            }
         }
         
         // Open forceRental page for an unsched
         function forceUnSchedButton_OnClick()
         {
            var ddl
            ddl = $get('<%= unschedDropDownList.ClientID %>')
            
            var countryDdl
            countryDdl = $get('<%= countryDropDownList.ClientID %>')

            if (ddl.options[ddl.selectedIndex].text != "" && countryDdl.options[countryDdl.selectedIndex].text != "")
            {
                //window.open("./ForceRental.aspx?country=" + countryDdl.options[countryDdl.selectedIndex].text + "&booking=" + ddl.options[ddl.selectedIndex].text,"name","width=600,height=400")
                window.open("../OpsAndLogs/VehicleAssign.aspx?popup=true&BookingText=" + ddl.options[ddl.selectedIndex].text,"name","width=1000,height=700")
            }
         }
         
         function SubstitutionSummary_OnClick()
         {
            var ddl
            ddl = $get('<%= countryDropDownList.ClientID %>')
            if (ddl.options[ddl.selectedIndex].text != "")
            {
                var win1 = window.open("./SubstitutionSummary.aspx?country=" + ddl.options[ddl.selectedIndex].text,"name","width=400,height=700,resizable=no");
                var LeftPosition = (screen.width) ? (screen.width-400)/2 : 0;
                var TopPosition = (screen.height) ? (screen.height-700)/2 : 0;
                win1.moveTo(LeftPosition,TopPosition);
            }
         }
         
         // Open forceRental page for an unmaint
         function forceUnMaintButton_OnClick()
         {
            var ddl
            ddl = $get('<%= forceUnMaintButton.ClientID %>')
            var countryDdl
            countryDdl = $get('<%= countryDropDownList.ClientID %>')

            if (ddl.options[ddl.selectedIndex].text != "" && countryDdl.options[countryDdl.selectedIndex].text != "")
            {
                //window.open("./ForceRental.aspx?country=" + countryDdl.options[countryDdl.selectedIndex].text + "&booking=" + ddl.options[ddl.selectedIndex].text,"name","width=600,height=400")
                window.open("../OpsAndLogs/VehicleAssign.aspx?popup=true&BookingText=" + ddl.options[ddl.selectedIndex].text,"name","width=1000,height=700")
            }
         }
         
         // Show vehicle details
         function phGantX1_OnDblClickTreeJava(theGant)
         {
            var myTree
            var myRow
            var myKey
            
            myTree = theGant.CurrentDataEntityTree
            myKey = myTree.OwningDataList.IndexOf(myTree)

            //vehicleXmlDoc
            var vehicleXmlDoc
            vehicleXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            vehicleXmlDoc.async="false"

            if (vehicleXml != "")
            {             
                vehicleXmlDoc.loadXML(vehicleXml)
                var childnodes
                childnodes = vehicleXmlDoc.documentElement.childNodes; 

                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                { 
                    var node = childnodes[f] 
                    if (node.childNodes[0].text == myKey)
                    {
                        var message 
                        message = "          Model: " + node.childNodes[2].text + "\\r\\n"
                        message = message + "    Unit Number: " + node.childNodes[1].text + "\\r\\n"
                        message = message + "   Registration: " + node.childNodes[3].text + "\\r\\n"
                        message = message + "       Odometer: " + node.childNodes[4].text + "\\r\\n"
                        message = message + " Next Available: " + node.childNodes[7].text + " " + node.childNodes[5].text + "\\r\\n"
                        message = message + " First On Fleet: " + node.childNodes[6].text
                        
                        var vehicle
                        vehicle = node.childNodes[2].text + " " + node.childNodes[1].text + " " + String(node.childNodes[7].text).substring(0, 1)
                            
                        f = childnodes.length   
                        var output = window.showModalDialog("./ScheduleDetails.aspx?type=vehicle&param1=" + message + "&param2=" + vehicle ,"Vehicle Details","dialogHeight: 180px; dialogWidth: 350px; dialogHide: off; edge: Raised; center: Yes; resizable: No; scroll: off; status: off; unadorned: off");                    
                    }                  
                } 
                childnodes = null
            }
            vehicleXmlDoc = null
         }
         
         // Search event 
         function searchButton_OnClick()
         {
            searchStartCount = 0
            findNextButton_OnClick()
         }
         
         // Find next event
         function findNextButton_OnClick()
         {
            var ddl 
            ddl = $get('<%= searchDropDownList.ClientID %>')
            searchButton_OnClickVb(ddl.value, ddl.options[ddl.selectedIndex].text,$get('<%= searchTextBox.ClientID %>').value)
         }
         
         //searchTextBox_OnTextChanged
         function searchTextBox_OnTextChanged()
         {  
            searchStartCount = 0   
            window.document.getElementById("findNextButton").disabled = "disabled"
         }
         
//         function divGantButton_OnClick(){
//            var divGant
//            divGant = document.getElementById("divGant")
//            var divGrid
//            divGrid = document.getElementById("divGrid")
//            
//            if (divGant.style.display == "inline")
//            {
//                divGant.style.display = "none"
//                divGrid.style.display = "inline"
//            }
//            else
//            {
//                divGant.style.display = "inline"
//                divGrid.style.display = "none"
//            }
//        
//         }
//         
//         function divGridButton_OnClick(){
//            var divGrid
//            divGrid = document.getElementById("divGrid")
//            var divGant
//            divGant = document.getElementById("divGant")
//            
//            if (divGrid.style.display == "inline")
//            {
//                divGrid.style.display = "none"
//                divGant.style.display = "inline"
//            }
//            else
//            {
//                divGrid.style.display = "inline"
//                divGant.style.display = "none"
//            }
//         }
        
    </script>

    <script id='eventVbScript' type="text/vbscript" language='VBScript'>
               
        sub phGantX1_OnRightClickGantArea(ByVal theGant, ByVal theDataEntity)
            phGantX1_OnRightClickGantAreaJava theGant, theDataEntity
        end sub

        sub phGantX1_OnDblClickGantArea(ByVal theGant, ByVal theDataEntity)
            if HasAdminPermission = "True" then
                phGantX1_OnDblClickGantAreaJava theGant, theDataEntity
            end if
        end sub
        
        sub phGantX1_OnDblClickTree(ByVal theGant)
            phGantX1_OnDblClickTreeJava(theGant)
        end sub
        
        Sub phGantX1_OnGantTimeChangeRow(ByVal theGant, ByVal theDataEntity, ByVal theOldRow , ByVal theNewRow)
        End Sub
        sub phGantX1_OnClickGantArea(ByVal theGant, ByVal theDataEntity)
        end sub
        sub phGantX1_OnClickGrid(ByVal theGant, ByVal theDataEntity)
        end sub
        sub phGantX1_OnClickScaler(ByVal theGant)
        end sub
        sub phGantX1_OnMouseMove(ByVal theGant, ByVal theSubComponent, ByVal Shift, ByVal X, ByVal Y  )
        end sub
        
        Sub leftButton_OnClick()
            Dim myStart
            Dim myStop
            Dim myDiff
            myStart = aspnetForm.phGantX1.Start
            myStop = aspnetForm.phGantX1.Stop
            myDiff = DateDiff("d", myStart, myStop)
            myDiff = Round(myDiff / 2, 0)
            myDiff = myDiff * (-1)
            myStart = DateAdd("d", myDiff, myStart)
            myStop = DateAdd("d", myDiff, myStop)
            aspnetForm.phGantX1.Start = myStart
            aspnetForm.phGantX1.Stop = myStop
        End Sub
        
        Sub rightButton_OnClick()
            Dim myStart
            Dim myStop
            Dim myDiff
            myStart = aspnetForm.phGantX1.Start
            myStop = aspnetForm.phGantX1.Stop
            myDiff = DateDiff("d", myStart, myStop)
            myDiff = Round(myDiff / 2, 0)
            myStart = DateAdd("d", myDiff, myStart)
            myStop = DateAdd("d", myDiff, myStop)
            aspnetForm.phGantX1.Start = myStart
            aspnetForm.phGantX1.Stop = myStop
        End Sub
        
        Sub upButton_OnClick()
            Dim myTree' As IphDataEntity_Tree2
            Dim lOldTopRow' As Long
            Dim lNewTopRow' As Long
            Dim lRowJump' As Long
            Dim lJumped' As Long
            Dim lSeeJumped' As Long
            ' Calculate 3/4 of the rows per jump - 726 is the upper section height, 286 is a row
            lRowJump = 25'Round((0.75 * (Me.phGantX1.Height - 723) / 286), 0)
            Set myTree = aspnetForm.phGantX1.TopItemTree
            lOldTopRow = aspnetForm.phGantX1.RootDataEntitiesTree.IndexOf(myTree)
            lJumped = 0
            lSeeJumped = 0
            While (lSeeJumped < lRowJump) And ((lOldTopRow - lJumped) > 0)
                lJumped = lJumped + 1
                Set myTree = aspnetForm.phGantX1.RootDataEntitiesTree.Items(lOldTopRow - lJumped)
                If myTree.HideNode = False Then
                    lSeeJumped = lSeeJumped + 1
                    aspnetForm.phGantX1.SetTopItemTree myTree
                End If
            Wend

        End Sub
        
        Sub downButton_OnClick()
            Dim myTree' As IphDataEntity_Tree2
            Dim lOldTopRow' As Long
            Dim lNewTopRow' As Long
            Dim lCount' As Long
            Dim lRowJump' As Long
            Dim lJumped' As Long
            Dim lSeeJumped' As Long
          
            ' Calculate 3/4 of the rows per jump - 726 is the upper section height, 286 is a row
            lRowJump = 25 ' Round((0.75 * (phGantX1.Height - 723) / 286), 0) 
            Set myTree = aspnetForm.phGantX1.TopItemTree
            lCount = aspnetForm.phGantX1.RootDataEntitiesTree.Count         
            lOldTopRow = aspnetForm.phGantX1.RootDataEntitiesTree.IndexOf(myTree)
            lJumped = 0
            lSeeJumped = 0
            While (lSeeJumped < lRowJump) And ((lOldTopRow + lJumped) < aspnetForm.phGantX1.RootDataEntitiesTree.Count)
                Set myTree = aspnetForm.phGantX1.RootDataEntitiesTree.Items(lOldTopRow + lJumped)
                If myTree.HideNode = False Then
                    lSeeJumped = lSeeJumped + 1
                End If
                lJumped = lJumped + 1
            Wend
            aspnetForm.phGantX1.SetTopItemTree myTree
        End Sub
        
        Sub homeButton_OnClick()
            aspnetForm.phGantX1.Start = Now() - 1 
            aspnetForm.phGantX1.Stop = Now() + 22
        End Sub
        
        sub zoomOut_OnClick()
            With aspnetForm.phGantX1
                .Stop = (.Stop - .Start) * 2 + .Start
            End With
        end sub

        sub zoomIn_OnClick()
            With aspnetForm.phGantX1
                .Stop = Round((.Stop - .Start) / 2, 0) + .Start
            End With
        end sub
       
        ' Search event 
        sub searchButton_OnClickVb(searchType, searchTypeText, value)
            dim vehicleId 
            searchType = Trim(searchType)
            value = Trim(value)
            
                'other search - unit no, rego, model
                If searchType < 4 Then

                    vehicleId = FindVehicle(searchType, value)
                    if vehicleId >= 0 then
                        Set myTree = aspnetForm.phGantX1.RootDataEntitiesTree.Items(vehicleId)
                        myTree.Selected = True
                        aspnetForm.phGantX1.SetTopItemTree myTree
                        setInformationShortMessage("Search - Found")
                        window.document.getElementById("findNextButton").disabled = ""
                    else
                        MsgBox searchTypeText & ": " & value & " Not Found!", vbOKOnly, "Search"
                        setInformationShortMessage("Search - Not Found")
                        window.document.getElementById("findNextButton").disabled = "disabled"
                    end if

                'Rental search
                ElseIf searchType = 4 Then

                        vehicleId = FindVehicle(searchType, value)
                        if vehicleId >= 0 then
                            Set myTree = aspnetForm.phGantX1.RootDataEntitiesTree.Items(vehicleId) 
                            myTree.Selected = True
                            aspnetForm.phGantX1.SetTopItemTree myTree
                            
                            dim scheduleId
                            scheduleId = FindSchedule(value)
                            
                            Set myTime = myTree.GantRow.DataLists.DataList(1).Items(scheduleId)  
                            If Not (myTime Is Nothing) Then
                                myDate = myTime.Start - 3
                                aspnetForm.phGantX1.Start = myDate
                                myDate = myDate + 23
                                aspnetForm.phGantX1.Stop = myDate
                                myTime.Selected = True
                                setInformationShortMessage("Search - Found")
                                'window.document.getElementById("findNextButton").style.display = "inline"
                                window.document.getElementById("findNextButton").disabled = ""
                            Else
                                setInformationShortMessage("Search - Not Found")
                                MsgBox "Booking: " & value & " NotFound!", vbOKOnly, "Search"
                                'window.document.getElementById("findNextButton").style.display = "none"
                                window.document.getElementById("findNextButton").disabled = "disabled"
                            End If

                        else
                           ' Check unscheduled
                            dim rentalId
                            rentalId = FindUnSchedule(value)
                            
                            if rentalId < 0 then
                                setInformationShortMessage("Search - Found Unscheduled Rental")
                                MsgBox "Rental: " & value & " Not Scheduled", vbOKOnly, "Search"
                            else
                                setInformationShortMessage("Search - Not Found")
                                MsgBox "Booking: " & value & " NotFound!", vbOKOnly, "Search"
                            end if    
                            window.document.getElementById("findNextButton").disabled = "disabled"                     
                        end if

                End If

        end sub
    </script>

    <script id='onloadVbScript' type="text/vbscript" language='VBScript'>
    
    sub onloadStep3()
        'Set default settings/properties of the phGantX
        aspnetForm.phGantX1.Start = Now() - 3
        aspnetForm.phGantX1.Stop = Now() + 20
        aspnetForm.phGantX1.RescaleWithCtrl = True
        aspnetForm.phGantX1.DateFormat = "DD/MM/YY"
        aspnetForm.phGantX1.ScalerIndicatorStyle = 2 'isColorfull
        aspnetForm.phGantX1.ZoomFactor = 100
        aspnetForm.phGantX1.GantPyjamasColor = 7827504
        aspnetForm.phGantX1.ShowLinesTree = False
        aspnetForm.phGantX1.AutoExpandTree = True
        
        'divGridButton_OnClick()
    end sub
    
    </script>

    <script id='onloadScript' type="text/javascript" language='javascript'>
    
//        var x
//        var y
//        window.onload = Init;
//        
//        function Init() {
//            if (window.Event) {
//                document.captureEvents(Event.MOUSEMOVE);
//            }
//            document.onmousemove = GetXY;
//        }

//        //Get mouse pointer locations
//        function GetXY(e) {
//            x = (window.Event) ? e.pageX : event.clientX;
//            y = (window.Event) ? e.pageY : event.clientY;
//        }

        var vehicleXmlDoc
        var vechicleStartingPointXmlDoc
        var scheduleXmlDoc
        var relocationXmlDoc
        var maintXmlDoc
        var disposalXmlDoc
        var currentRentalXmlDoc
        var currentMaintXmlDoc

        function onloadStep4()  { 
        try{
           // clearMessages()

        //vehicleXmlDoc
            //var vehicleXmlDoc
            vehicleXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            vehicleXmlDoc.async="false"

            if (vehicleXml != "")
            {
                //setInformationMessage("Loading...")

                vehicleXmlDoc.loadXML(vehicleXml)
                var childnodes
                childnodes = vehicleXmlDoc.documentElement.childNodes; 

                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                { 
                    var node = childnodes[f] 
                    
                    var unitNumber = node.childNodes[1]     //UnitNumber
                    var fleetModelCode = node.childNodes[2] //FleetModelCode
                    var locationCode = node.childNodes[7] //locationCode
                                                  
                    var str 
                    str = "        " + unitNumber.text
                    
                    var iLen = String(str).length;
                    str = String(str).substring(iLen - 8, iLen);

                    var vehicle =  fleetModelCode.text + " " + str + "  " + String(locationCode.text).substring(0, 1)  + " "

                    AddTreeEntity(vehicle)      
                                      
                } 
                childnodes = null
            }
            vehicleXmlDoc = null
            
         //vechicleStartingPointXml
            //var vechicleStartingPointXmlDoc
            vechicleStartingPointXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            vechicleStartingPointXmlDoc.async="false"
            
            if (vechicleStartingPointXml != "")
            {
                vechicleStartingPointXmlDoc.loadXML(vechicleStartingPointXml)
          
                var childnodes
                childnodes = vechicleStartingPointXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                } 
                childnodes = null
            }
            vechicleStartingPointXmlDoc = null
            
            //Create 'Today' time activity  
            showToday()
            
             //scheduleXml
            //var scheduleXmlDoc
            scheduleXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            scheduleXmlDoc.async="false"
            
            if (scheduleXml != "")
            {
                scheduleXmlDoc.loadXML(scheduleXml)
          
                var childnodes
                childnodes = scheduleXmlDoc.documentElement.childNodes; 
              
                setScheduleIdXML("<scheduleIds>")
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    var id 
                    id = 1

                    id = createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                    
                    setScheduleIdXML("<scheduleId>")
                    setScheduleIdXML("<v>" + node.childNodes[0].text + "</v>")
                    setScheduleIdXML("<u>" + node.childNodes[7].text + "</u>")
                    setScheduleIdXML("<s>" + id + "</s>")
                    setScheduleIdXML("</scheduleId>")

                } 
                setScheduleIdXML("</scheduleIds>") 
                childnodes = null
            }
            scheduleXmlDoc = null

             //relocationXml
            //var relocationXmlDoc
            relocationXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            relocationXmlDoc.async="false"
            
            if (relocationXml != "")
            {
                relocationXmlDoc.loadXML(relocationXml)
          
                var childnodes
                childnodes = relocationXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                } 
                childnodes = null
            }
            relocationXmlDoc = null
            
               //maintXml
            //var maintXmlDoc
            maintXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            maintXmlDoc.async="false"
            
            if (maintXml != "")
            {
                maintXmlDoc.loadXML(maintXml)
          
                var childnodes
                childnodes = maintXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                } 
                childnodes = null
            }
            maintXmlDoc = null

            //disposalXml
            //var disposalXmlDoc
            disposalXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            disposalXmlDoc.async="false"
            
            if (disposalXml != "")
            {
                disposalXmlDoc.loadXML(disposalXml)
          
                var childnodes
                childnodes = disposalXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                } 
                childnodes = null
            }
            disposalXmlDoc = null          
            
            //currentRentalXml
            //var currentRentalXmlDoc
            currentRentalXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            currentRentalXmlDoc.async="false"
            
            if (currentRentalXml != "")
            {
                currentRentalXmlDoc.loadXML(currentRentalXml)
          
                var childnodes
                childnodes = currentRentalXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                } 
                childnodes = null
            }
            currentRentalXmlDoc = null          
            
            //currentMaintXml
            //var currentMaintXmlDoc
            currentMaintXmlDoc = new ActiveXObject("Microsoft.XMLDOM")
            currentMaintXmlDoc.async="false"
            
            if (currentMaintXml != "")
            {
                currentMaintXmlDoc.loadXML(currentMaintXml)
          
                var childnodes
                childnodes = currentMaintXmlDoc.documentElement.childNodes; 
              
                // Add Entities
                for(var f=0;f<childnodes.length;f++) 
                {           
                    var node = childnodes[f] 
                    createepisode(node.childNodes[0].text, node.childNodes[1].text,node.childNodes[2].text,node.childNodes[3].text,node.childNodes[4].text,node.childNodes[5].text,node.childNodes[6].text,node.childNodes[7].text,node.childNodes[8].text,node.childNodes[9].text)
                } 
                childnodes = null
            }
            currentMaintXmlDoc = null  
              
            //setInformationMessage("Completed") 
            
            //Enable the search button
            window.document.getElementById("searchButton").disabled = "" 
              
        }
        catch(e){
        } 
        }
        
    </script>

</asp:Content>
