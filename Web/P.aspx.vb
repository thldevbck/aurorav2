''' <summary>
''' Simple page which does a redirect based on the "a" (Aurora) or "o" (Oslo) url parameter to the function code 
''' specified in the "f" querystring parameter based on whether the user has permission to the "OSLO" function code.
''' 
''' We let the functionality in AuroraPage handle all the function code lookups and permission checks, so we only
''' need to do a redirect in Page_Load
''' </summary>
<AuroraPageTitle("Oslo Function Proxy")> _
Partial Class P
    Inherits AuroraPage

    Public Overrides ReadOnly Property FunctionCode() As String
        Get
            Dim result As String = Request("f")
            If Not String.IsNullOrEmpty(result) Then
                Return result
            Else
                Return AuroraFunctionCodeAttribute.General
            End If
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim auroraBaseUrl As String = Me.ResolveUrl("~/../")

        Dim auroraUrl As String = Request.QueryString("a")
        Dim osloUrl As String = Request.QueryString("o")

        If Me.FunctionCode = AuroraFunctionCodeAttribute.General _
         OrElse String.IsNullOrEmpty(auroraUrl) _
         OrElse String.IsNullOrEmpty(osloUrl) Then
            Me.ErrorRedirect("Invalid Page")
            Return
        End If

        Dim auroraFullUrl = auroraBaseUrl & "asp/" & auroraUrl
        Dim osloFullUrl = auroraBaseUrl & "web/" & osloUrl

        Dim js As New System.Web.Script.Serialization.JavaScriptSerializer()
        If Me.GetFunctionPermission("OSLO") Then
            Response.Redirect(osloFullUrl)
        Else
            ' GoToAuroraAspUrl (funId, funCode, funFileName, searchParam, '');
            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "addEvent (window, 'load', function() { GoToAuroraAspUrl(" _
                & js.Serialize(Me.FunctionId) _
                & "," & js.Serialize(Me.FunctionCode) _
                & "," & js.Serialize(auroraUrl) _
                & ",'', ''); });", True)

        End If
    End Sub
End Class
