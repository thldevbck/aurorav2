'' Change log!
''  ID4   - Shoel : date format thingie added !

Imports System.Xml
Imports System.Data
Imports Aurora.OpsAndLogs.Data
Imports Aurora.OpsAndLogs.Services
Imports Aurora.Common
Imports Aurora.Common.Utility

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.FleetRepairList)> _
Partial Class OpsAndLogs_FleetRepairList
    Inherits AuroraPage

    Const SESSION_BACKPATH As String = "Session_RepMgmt_BackPath"
    'Public BackPage As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            LoadPage()
        End If
    End Sub

    'old funcode page name = AI_FleetRepairList.asp
    'new one is = FleetRepairList.aspx

    Public Sub LoadPage(Optional searchParameter As String = "")

        Dim dstVehicleDetails As DataSet
        ''rev:mia 1Aug2014 - provide a parameter that accept either a Unit Number of Registration number
        Try
            Dim hasAreferrer As Boolean = IIf(Request.UrlReferrer.AbsolutePath.ToString.ToLower.Contains("/web/OpsAndLogs/FleetRepairManagement.aspx".ToLower), True, False)

            If (String.IsNullOrEmpty(searchParameter) And Not hasAreferrer) Then
                If (String.IsNullOrEmpty(SearchParam)) Then
                    dstVehicleDetails = DataRepository.GetRepairsList("", "", UserCode) '"2025", "", UserCode)
                Else
                    dstVehicleDetails = DataRepository.GetRepairsList("", SearchParam, UserCode) '"2025", "", UserCode)
                End If

            ElseIf (String.IsNullOrEmpty(searchParameter) And hasAreferrer) Then
                dstVehicleDetails = DataRepository.GetRepairsList(SearchParam, "", UserCode) '"2025", "", UserCode)

            Else
                dstVehicleDetails = DataRepository.GetRepairsList(searchParameter, "", UserCode) ''unit numner or reg number
            End If


            Dim dtVehicleDetails, dtOffFleet As DataTable
            ' BackPage = 

            If dstVehicleDetails.Tables.Count <= 0 Then
                dtVehicleDetails = New DataTable
                dtOffFleet = New DataTable
                btnNew.Attributes.Add("OnClick", "return CreateNewRepairDetails('','" & Page.Master.FindControl("shortMessagePanel").ClientID & "','FleetRepairList.aspx?sTxtSearch=" & GetSearchParamAlternative() & "');")
            Else
                If Not dstVehicleDetails.Tables(1) Is Nothing Then
                    dtVehicleDetails = dstVehicleDetails.Tables(1)

                    ' hack for date format
                    For Each oRow As DataRow In dtVehicleDetails.Rows
                        Dim dtTemp As Date
                        If Not CStr(oRow(4)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(4), SystemCulture)
                            oRow(4) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                        If Not CStr(oRow(5)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(5), SystemCulture)
                            oRow(5) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                        If Not CStr(oRow(6)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(6), SystemCulture)
                            oRow(6) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                        If Not CStr(oRow(11)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(11), SystemCulture)
                            oRow(11) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                    Next
                    ' hack for date format

                    btnNew.Attributes.Add("OnClick", "return CreateNewRepairDetails('" & dtVehicleDetails.Rows(0)("FleetAssetId") & "','" & Page.Master.FindControl("shortMessagePanel").ClientID & "','FleetRepairList.aspx?sTxtSearch=" & GetSearchParamAlternative() & "');")

                    lblDescription.Text = IIf(dtVehicleDetails.Columns("Desc") Is Nothing, "", dtVehicleDetails.Rows(0)("Desc"))
                    lblUnitNo.Text = IIf(dtVehicleDetails.Columns("UnitNo") Is Nothing, "", dtVehicleDetails.Rows(0)("UnitNo"))
                    lblRegNo.Text = IIf(dtVehicleDetails.Columns("RegBo") Is Nothing, "", dtVehicleDetails.Rows(0)("RegBo"))
                    'Session(SESSION_BACKPATH) = Server.HtmlEncode(Page.Request.RawUrl.Substring(Page.Request.RawUrl.LastIndexOf("/") + 1))
                    'Dim sPath As String

                Else
                    dtVehicleDetails = New DataTable
                End If

                If Not dstVehicleDetails.Tables(3) Is Nothing Then
                    dtOffFleet = dstVehicleDetails.Tables(3)
                    ' hack for date format
                    For Each oRow As DataRow In dtOffFleet.Rows
                        Dim dtTemp As Date
                        If Not CStr(oRow(2)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(2), SystemCulture)
                            oRow(2) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                        If Not CStr(oRow(4)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(4), SystemCulture)
                            oRow(4) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                        If Not CStr(oRow(10)).Trim().Equals(String.Empty) Then
                            dtTemp = ParseDateTime(oRow(10), SystemCulture)
                            oRow(10) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        End If
                    Next
                    ' hack for date format
                Else
                    dtOffFleet = New DataTable
                End If

            End If

            rptVehicleDetails.DataSource = dtVehicleDetails
            rptVehicleDetails.DataBind()

            rptOffFleet.DataSource = dtOffFleet
            rptOffFleet.DataBind()
        Catch ex As Exception
            LogException(ex)
        End Try
        
    End Sub


    Protected Sub rptOffFleet_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptOffFleet.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Then
            CType(e.Item.FindControl("lnkEvenDetails"), LinkButton).Attributes.Add("onClick", "return GetRepairDetails('" & CType(e.Item.FindControl("hdnEvenRepairId"), HiddenField).Value & "','" & Server.HtmlEncode("FleetRepairList.aspx?sTxtSearch=" & GetSearchParamAlternative()) & "');")
        End If
        If e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("lnkOddDetails"), LinkButton).Attributes.Add("onClick", "return GetRepairDetails('" & CType(e.Item.FindControl("hdnOddRepairId"), HiddenField).Value & "','" & Server.HtmlEncode("FleetRepairList.aspx?sTxtSearch=" & GetSearchParamAlternative()) & "');")
        End If

    End Sub

    Protected Sub btnSearchUnitNumberOrRegNumber_Click(sender As Object, e As System.EventArgs) Handles btnSearchUnitNumberOrRegNumber.Click
        If (String.IsNullOrEmpty(lblUnitNo.Text) And String.IsNullOrEmpty(lblRegNo.Text)) Then
            SetErrorShortMessage("Please provide Unit Number or Registration Number")
        Else
            If (Not String.IsNullOrEmpty(lblUnitNo.Text) And Not String.IsNullOrEmpty(lblRegNo.Text)) Then
                LoadPage(lblUnitNo.Text)
            ElseIf (String.IsNullOrEmpty(lblUnitNo.Text) And Not String.IsNullOrEmpty(lblRegNo.Text)) Then
                LoadPage(lblRegNo.Text)
            ElseIf (Not String.IsNullOrEmpty(lblUnitNo.Text) And String.IsNullOrEmpty(lblRegNo.Text)) Then
                LoadPage(lblUnitNo.Text)
            End If

        End If
    End Sub

    Function GetSearchParamAlternative() As String

        If (String.IsNullOrEmpty(SearchParam)) Then
            If (Not String.IsNullOrEmpty(lblUnitNo.Text)) Then
                Return lblUnitNo.Text
            End If
            If (Not String.IsNullOrEmpty(lblRegNo.Text)) Then
                Return lblRegNo.Text
            End If
        Else
            If (Not String.IsNullOrEmpty(lblUnitNo.Text)) Then
                If (lblUnitNo.Text <> SearchParam) Then
                    Return lblUnitNo.Text
                End If
            End If
            If (Not String.IsNullOrEmpty(lblRegNo.Text)) Then
                If (lblRegNo.Text <> SearchParam) Then
                    Return lblRegNo.Text
                End If
            End If
        End If

        Return SearchParam
    End Function
End Class
