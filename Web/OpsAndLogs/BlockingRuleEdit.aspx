<%@ Page 
    Language="VB" 
    MasterPageFile="~\Include\AuroraHeader.master" 
    AutoEventWireup="false"
    CodeFile="BlockingRuleEdit.aspx.vb" 
    Inherits="OpsAndLogs_BlockingRuleEdit" 
    Title="Blocking Rule" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        table.dataTableColor ul { margin: 0 0 0 16px; padding: 0; }
        table.dataTableColor li { margin: 0px; padding: 0; list-style-type: square; text-align: left; }
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script type="text/javascript">
var isIncCheckBoxIds = <%= Me.IsIncCheckBoxIds %>;
var tableRowIds = ["productTableRow", "brandTableRow", "packageTableRow", "locationFromTableRow", "locationToTableRow", "agentCountryTableRow", "agentCategoryTableRow", "agentTableRow"];
var includeForeColor = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.IncludeForeColor) %>';
var excludeForeColor = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ExcludeForeColor) %>';

function isIncCheckBoxChange()
{
    for (var i = 0; i < isIncCheckBoxIds.length; i++)
    {
        var isIncCheckBox = document.getElementById (isIncCheckBoxIds[i]);
        var tableRow = document.getElementById (tableRowIds[i]);
        tableRow.style.color = isIncCheckBox.checked ? excludeForeColor : includeForeColor;
    }
}

function isIncCheckBoxChangeInit()
{
    for (var i = 0; i < isIncCheckBoxIds.length; i++)
    {
        var isIncCheckBox = document.getElementById (isIncCheckBoxIds[i]);
        isIncCheckBox.onchange = isIncCheckBoxChange;
        isIncCheckBox.onclick = isIncCheckBoxChange;
    }
    isIncCheckBoxChange();
}

function confirmDelete()
{
    return (confirm ("Are you sure you want to delete this rule?"));
}

function blockingRuleInit()
{
    isIncCheckBoxChangeInit();
}

addEvent (window, "load", blockingRuleInit);
Sys.WebForms.PageRequestManager.getInstance().add_endRequest(blockingRuleInit);
    
    </script>
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <asp:Panel ID="blockingRuleEditPanel" runat="Server" Width="780px">
    
        <asp:UpdatePanel runat="server" ID="blockingRuleUpdatePanel">
            <ContentTemplate>
                <table style="width:100%;" cellpadding="2" cellspacing="0">
                    <tr>
                        <td style="width:150px">Country:</td>
                        <td style="width:250px"><asp:TextBox ID="countryTextBox" runat="server" style="width:200px" Readonly="true" /></td>
                        <td style="width:150px">Rule Type:</td>
                        <td style="width:250px"><asp:TextBox ID="typeTextBox" runat="server" style="width:200px" Readonly="true" /></td>
                    </tr>
                    <tr>
                        <td>Reason / Message:</td>
                        <td colspan="3"><asp:TextBox ID="reasonTextBox" runat="server" style="width:600px" /></td>
                    </tr>
                    <tr>
                        <td>Duration of Booking:</td>
                        <td><uc1:RegExTextBox ID="durationTextBox" runat="server" Width="50px" Nullable="false" RegExPattern="^\d+$" ErrorMessage="Duration of Booking must be a decimal value greater than or equal to zero" />&nbsp;*</td>
                        <td>Book Ahead Period :</td>
                        <td><uc1:RegExTextBox ID="bookAheadTextBox" runat="server" Width="50px" Nullable="false" RegExPattern="^\d+$" ErrorMessage="Book Ahead Period must be a decimal value greater than or equal to zero" />&nbsp;*</td>
                    </tr>
                    <tr>
                        <td>By Date:</td>
                        <td>
                            <asp:RadioButtonList ID="byDateRadioButtonList" runat="server" RepeatDirection="horizontal" RepeatLayout="Table" CssClass="dataTableInternal repeatLabelTable">
                                <asp:ListItem Value="Out">Out</asp:ListItem>
                                <asp:ListItem Value="Use">Use</asp:ListItem>
                                <asp:ListItem Value="In">In</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                        <td>Unit of Measure:</td>
                        <td>
                            <asp:DropDownList ID="uomDropDown" runat="server" style="width:150px" />
                        </td>
                    </tr>
                    <tr>
                        <td style="border-top-width: 1px; border-top-style: dotted">Flags:</td>
                        <td style="border-top-width: 1px; border-top-style: dotted" colspan="4">
                            <asp:CheckBoxList ID="flagsCheckBoxList" runat="server" RepeatDirection="Horizontal" RepeatLayout="Table" RepeatColumns="4" Width="100%" CssClass="dataTableInternal repeatLabelTable">
                                <asp:ListItem Value="blrOverrideable">Overrideable</asp:ListItem>
                                <asp:ListItem Value="blrOnlyFlexBkg">Only Flex Bookings </asp:ListItem>
                                <asp:ListItem Value="blrBypass">Bypass Availability Check </asp:ListItem>
                                <asp:ListItem Value="blrIsTandC">T&amp;C rule</asp:ListItem>
                            </asp:CheckBoxList>
                        </td>
                    </tr>
                </table>
                
                <table cellpadding="2" cellspacing="0" id="statusTable" runat="server" style="width:100%; border-width: 1px 0px;">
                    <tr>
                        <td style="width:150px">Period From:</td>
                        <td style="width:250px"><uc1:DateControl ID="bookedFromTextBox" runat="server" Nullable="False" />&nbsp;*</td>
                        <td style="width:150px">To:</td>
                        <td style="width:250px"><uc1:DateControl ID="bookedToTextBox" runat="server" Nullable="False" />&nbsp;*</td>
                    </tr>
                </table>
                
                <hr />

                <div style="margin: 0 0 15px 0">
                    <asp:Button ID="removeButton" runat="server" CssClass="Button_Standard Button_Remove" Text="Remove" Style="float:right" />
                    <b>Rule Components:</b>
                </div>
                
                <table class="dataTableColor" cellpadding="2" cellspacing="0" style="width:100%">
                    <tr>
                        <th style="width: 125px">Type</th>
                        <th style="width: 405px">Items</th>
                        <th style="width: 25px; border-width: 1px" align="right">Excl</th>
                        <th style="width: 200px" align="right">Add</th>
                    </tr>
                    <tr class="evenRow" id="productTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Products:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="productCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="4" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="productIsInclCheckBox" runat="server" /></td>
                        <td valign="top" align="right" style="padding-top: 4px; text-align: left" colspan="2"><asp:DropDownList ID="productDropDownList" runat="server" Width="195px" AutoPostBack="true" onchange="showLoadingIndicator();" /></td>
                    </tr>
                    <tr class="oddRow" id="brandTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Brands:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="brandCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="3" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="brandIsInclCheckBox" runat="server" /></td>
                        <td valign="top" align="right" style="padding-top: 4px; text-align: left" colspan="2"><asp:DropDownList ID="brandDropDownList" runat="server" Width="195px" AutoPostBack="true" onchange="showLoadingIndicator();" /></td>
                    </tr>
                    <tr class="evenRow" id="packageTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Packages:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="packageCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="3" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="packageIsInclCheckBox" runat="server" /></td>
                        <td valign="top" align="right" style="padding-top: 4px; text-align: left">
                            <uc1:PickerControl ID="packagePickerControl" runat="server" PopupType="PACKAGE" AppendDescription="false" Width="125px" />
                            <asp:Button ID="packageAddButton" runat="server" Style="width:45px" Text="&nbsp;" CssClass="Button_Standard Button_Add" />
                        </td>
                    </tr>
                    <tr class="oddRow" id="locationFromTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Locations From:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="locationFromCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="6" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="locationFromIsInclCheckBox" runat="server" /></td>
                        <td align="right" style="text-align: left" colspan="2"><asp:DropDownList ID="locationFromDropDownList" runat="server" Width="195px" AutoPostBack="true" onchange="showLoadingIndicator();" /></td>
                    </tr>
                    <tr class="evenRow" id="locationToTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Locations To:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="locationToCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="6" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="locationToIsInclCheckBox" runat="server" /></td>
                        <td  valign="top" align="right" style="padding-top: 4px; text-align: left" colspan="2"><asp:DropDownList ID="locationToDropDownList" runat="server" Width="195px" AutoPostBack="true" onchange="showLoadingIndicator();" /></td>
                    </tr>
                    <tr class="oddRow" id="agentCountryTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Agent Countries:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="agentCountryCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="3" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="agentCountryIsInclCheckBox" runat="server" /></td>
                        <td valign="top" align="right" style="padding-top: 4px; text-align: left" colspan="2"><asp:DropDownList ID="agentCountryDropDownList" runat="server" Width="195px" AutoPostBack="true" onchange="showLoadingIndicator();" /></td>
                    </tr>
                    <tr class="evenRow" id="agentCategoryTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Agent Categories:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="agentCategoryCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="4" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="agentCategoryIsInclCheckBox" runat="server" /></td>
                        <td valign="top" align="right" style="padding-top: 4px; text-align: left" colspan="2"><asp:DropDownList ID="agentCategoryDropDownList" runat="server" Width="195px" AutoPostBack="true" onchange="showLoadingIndicator();" /></td>
                    </tr>
                    <tr class="oddRow" id="agentTableRow">
                        <td valign="top" style="padding-top: 6px"><ul><li>Agents:</li></ul></td>
                        <td valign="top" style="padding-top: 2px"><asp:CheckBoxList ID="agentCheckBoxList" runat="server" CssClass="dataTableInternal repeatLabelTable" RepeatLayout="Table" RepeatColumns="3" RepeatDirection="Horizontal"></asp:CheckBoxList></td>
                        <td valign="top" style="padding-top: 4px; border-width: 0px 1px 1px 1px"><asp:CheckBox ID="agentIsInclCheckBox" runat="server" /></td>
                        <td valign="top" align="right" style="padding-top: 4px; text-align: left">
                            <uc1:PickerControl ID="agentPickerControl" runat="server" PopupType="AGENT" AppendDescription="false" Width="125px" />
                            <asp:Button ID="agentAddButton" runat="server" Style="width:45px" Text="&nbsp;" CssClass="Button_Standard Button_Add" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    
        <br />
        
        <table style="width:100%;" cellpadding="2" cellspacing="0">
            <tr>
                <td align="right">
                    <asp:Button ID="deleteButton" runat="server" Text="Delete" CssClass="Button_Standard Button_Delete" Visible="False" OnClientClick="return confirmDelete();" />
                    <asp:Button ID="updateButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="False" />
                    <asp:Button ID="createButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" Visible="False" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>

    </asp:Panel>

</asp:Content>
