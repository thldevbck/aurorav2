<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="FleetStatus.aspx.vb" Inherits="OpsAndLogs_FleetStatus" Title="Fleet Status" %>

<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/FleetStatusExceptionPopupUserControl.ascx" TagName="FSXPopup"
    TagPrefix="uc1" %>
<asp:Content ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        .dataTableGrid
        {
            margin-top: 5px;
        }
        
        .dataTableGrid th
        {
            text-align: center;
            font-weight: normal;
            width: 18px;
            border-width: 1px;
        }

        .dataTableGrid .thdate
        {
            text-align: center;
            font-weight: normal;
            width: 18px;
            border-width: 1px;
        }

        .dataTableGrid .thProduct
        {
            padding-left: 2px;
            text-align: left;
            width: 100px;
        }

        .dataTableGrid td
        {
            border-width: 1px;
            text-align: left;
            padding: 0;
            margin: 0;
        }

        .dataTableGrid .thCurrent { background-color: #FF8; }
        .dataTableGrid .thPast { background-color: #CCC; }
        .dataTableGrid .thFuture { background-color: #FFF; }
        
        .dataTableGrid .tdProduct
        {
            width: 250px;
            padding-left: 2px;
        }

        .dataTableGrid td input
        {
            border: 0 !important;
            width: 18px !important;
            text-align:center !important;
            font-weight: normal !important;
            padding: 0 !important;
            margin: 0 !important;
        }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script type="text/javascript">
    
/*==================================================================================================*/
/* DropDown & IsDirty Checking                                                                      */
/*==================================================================================================*/
var countryDropDownId = '<%= countryDropDown.ClientID %>';
var countryDropDown;
var lookupDropDownId = '<%= lookupDropDown.ClientID %>';
var lookupDropDown;

var countryDropDownIndex = 0;
var lookupDropDownIndex = 0;

function dropDownChange(url)
{
    var dirty = document.getElementById("ctl00_ContentPlaceHolder_hdnDirtyFlag");
    if (dirty.value=="true")
    {
    	if (confirm("You have made changes.  Do you want to lose these?"))
    	{
    	    dirty.value="false";
            showLoadingIndicator();
            window.location = url;
    	}
    	else
    	{
            countryDropDown.selectedIndex = countryDropDownIndex;
            lookupDropDown.selectedIndex = lookupDropDownIndex;
        }
    }
	else
	{
        showLoadingIndicator();
        window.location = url;
    }
}

function countryDropDown_change ()
{
    dropDownChange (auroraBaseUrl + "Web/OpsAndLogs/FleetStatus.aspx" + "?ctyCode=" + countryDropDown.value);
}

function lookupDropDown_change ()
{
    dropDownChange (auroraBaseUrl + "Web/OpsAndLogs/FleetStatus.aspx" + "?ctyCode=" + countryDropDown.value + "&code=" + lookupDropDown.value);
}

addEvent (window, "load", function()
{
    countryDropDown = document.getElementById (countryDropDownId);
    lookupDropDown = document.getElementById (lookupDropDownId);

    countryDropDownIndex = countryDropDown.selectedIndex;
    lookupDropDownIndex = lookupDropDown.selectedIndex;

    addEvent (countryDropDown, "change", countryDropDown_change);
    addEvent (lookupDropDown, "change", lookupDropDown_change);
});

/*==================================================================================================*/
/* Status Textboxes                                                                                 */
/*==================================================================================================*/
var fleetStatusColors = <%= FleetStatusColorsJSON %>;

function fleetStatusTextBox_keypress (sender, e)
{
    var keynum = window.event ? e.keyCode : e.which;
    if (keynum == 0) return true; // cursor keys
    if (keynum == 9) return true; // tab
    if (keynum == 8) return true; // backspace
    
    var keychar = String.fromCharCode(keynum);
    return (/[frxFRX]/.test(keychar));
}

function fleetStatusTextBox_change (sender, e)
{
    var dirty = document.getElementById("ctl00_ContentPlaceHolder_hdnDirtyFlag");
    var s = sender.value.toUpperCase().trim();
    if ((s == "") || !fleetStatusColors[s]) s = "X";
    
    sender.value = s;
    sender.style.backgroundColor = fleetStatusColors[s];
    dirty.value = "true";
}
    
    function showExceptionsPopup(ControlName,ArgumentsControlName,FleetStatusId,WeekNo,FleetStatus,VehicleName,CountryCode,ProductName,BookingWeekStartDate,BrandCode)
    {
        var dirty = document.getElementById("ctl00_ContentPlaceHolder_hdnDirtyFlag");
        if (dirty.value=="true")
        {
    	    if (confirm("You have made changes. Do you want to lose these?"))
    	    {
    	        dirty.value = "false";
    	        var btnHidden = document.getElementById(ControlName);
                var hdnArguments = document.getElementById(ArgumentsControlName);
                hdnArguments.value = FleetStatusId+','+WeekNo+','+FleetStatus+','+VehicleName+','+CountryCode+','+ProductName+','+BookingWeekStartDate+','+BrandCode;
                //alert("calling with args " + FleetStatusId+','+WeekNo+','+FleetStatus+','+VehicleName+','+CountryCode+','+ProductName+','+BookingWeekStartDate+','+BrandCode);
                btnHidden.click();
            }
            else
            {
                return false;
            }
        }
        else
        {
            var btnHidden = document.getElementById(ControlName);
            var hdnArguments = document.getElementById(ArgumentsControlName);
            hdnArguments.value = FleetStatusId+','+WeekNo+','+FleetStatus+','+VehicleName+','+CountryCode+','+ProductName+','+BookingWeekStartDate+','+BrandCode;
            //alert("calling with args " + FleetStatusId+','+WeekNo+','+FleetStatus+','+VehicleName+','+CountryCode+','+ProductName+','+BookingWeekStartDate+','+BrandCode);
            btnHidden.click();
        }
    }
    
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <div style="visibility: hidden; position: absolute; background-color: #dcdcdc; border: solid 1px black;
        padding: 2px; color: red; font-size: 8pt;" id="popupExceptions">
    </div>
    <asp:Panel ID="searchPanel" runat="Server" CssClass="dailyActivity">
        <table cellspacing="0" cellpadding="2">
            <tr>
                <td width="150">
                    Country:&nbsp;</td>
                <td width="250">
                    <asp:DropDownList ID="countryDropDown" runat="Server" Width="200px" EnableViewState="False" /></td>
                <td width="150">
                    Year / Brand:&nbsp;</td>
                <td>
                    <asp:DropDownList ID="lookupDropDown" runat="Server" Width="200px" EnableViewState="False" /></td>
            </tr>
            <tr>
                <td>
                    Last Modified:&nbsp;</td>
                <td colspan="3">
                    <asp:TextBox ID="bookedFromTextBox" runat="server" ReadOnly="true" Width="100" /></td>
            </tr>
        </table>
        <asp:UpdatePanel ID="updMain" runat="server">
            <ContentTemplate>
                <asp:Panel ID="resultPanel" runat="Server">
                    <asp:Panel ID="fleetStatusPanel" runat="Server">
                    </asp:Panel>
                    <hr />
                    <table cellspacing="0" cellpadding="2" width="100%">
                        <tr>
                            <td align="right">
                                <asp:Button ID="saveButton" runat="Server" Text="Save" CssClass="Button_Standard Button_Save" />
                                <asp:Button ID="cancelButton" runat="Server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                            </td>
                        </tr>
                    </table>
                    <table cellspacing="0" cellpadding="2" style="border-width: 1px; width: 780px; margin-top: 5px">
                        <tr>
                            <td style="border-right-width: 1px; background-color: #FFF; width: 20%" rowspan="2">
                                <i>Key:</i></td>
                            <td colspan="2" style="border-bottom-width: 1px; background-color: <%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ActiveColor) %>;
                                width: 40%">
                                Active Vehicle</td>
                            <td colspan="2" style="border-bottom-width: 1px; background-color: <%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor) %>;
                                width: 40%">
                                Inactive Vehicle</td>
                        </tr>
                        <tr>
                            <td style="background-color: <%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.FleetStatusToColor (Aurora.Common.DataConstants.FleetStatus_FreeSale)) %>;
                                width: 20%">
                                F - Free</td>
                            <td style="background-color: <%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.FleetStatusToColor (Aurora.Common.DataConstants.FleetStatus_Request)) %>;
                                width: 20%">
                                R - Request</td>
                            <td style="background-color: <%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.FleetStatusToColor (Aurora.Common.DataConstants.FleetStatus_Unavailable)) %>;
                                width: 20%">
                                X - Unavailable</td>
                        </tr>
                    </table>
                    <asp:Button ID="btnHidden" runat="server" Text="Button" Style="display: none" CausesValidation="false" /></asp:Panel>
                <input type="hidden" runat="server" id="hdnArguments" />
                <div style="display: none">
                    <uc1:PickerControl ID="pkrDummy" runat="server" PopupType="locationforcountry" />
                </div>
                <input type="hidden" runat="server" id="hdnDirtyFlag" value="false" />
                <uc1:FSXPopup ID="popupFSX" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
