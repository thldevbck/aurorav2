<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	MaintainFleetModelCosts.aspx
''	Date Created	-	24.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	24.4.8 / Shoel - Base version
''                      22.5.8 / Shoel - Common JS moved out of this file, Allowed entry of decimals like 0.9
''                      25.8.8 / Shoel - Field validation added
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="MaintainFleetModelCosts.aspx.vb" Inherits="OpsAndLogs_MaintainFleetModelCosts"
    Title="Maintain Fleet Model Costs" %>

<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="ucConfirmation" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="ucDateControl" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="ucPickerControl" %>
<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script src="JS/OpsAndLogsCommon.js" type="text/javascript" language="javascript"></script>

</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td width="150px">
                        Country Of Operation:
                    </td>
                    <td width="630px" align="left">
                        <asp:TextBox ID="txtCountryOfOperation" runat="server" ReadOnly="true" CssClass="Textbox_Standard"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <%--Buttons Row--%>
                        <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <%--<asp:Button ID="btnRemove" Text="Remove" runat="server" CssClass="Button_Standard" />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<br />--%>
                        <%--Table starts--%>
                        <asp:Repeater ID="rptMain" runat="server" EnableViewState="true">
                            <HeaderTemplate>
                                <table class="dataTable">
                                    <tr valign="top">
                                        <th width="46%">
                                            Fleet Model
                                        </th>
                                        <th width="12%">
                                            Variable<br />
                                            Daily Cost
                                        </th>
                                        <th width="12%">
                                            Delay<br />
                                            Penalty Cost
                                        </th>
                                        <th width="10%" style="text-align: center">
                                            Is Reloc<br />
                                            <input type="checkbox" id="Checkbox3" onclick="ToggleTableColumnCheckboxes(3, event);" />
                                        </th>
                                        <th width="10%" style="text-align: center;" nowrap>
                                            One Way Hire<br />
                                            <input type="checkbox" id="chkAllAvl" onclick="ToggleTableColumnCheckboxes(4, event);" />
                                        </th>
                                        <th width="10%" style="text-align: center">
                                            Active<br />
                                            <input type="checkbox" id="Checkbox1" onclick="ToggleTableColumnCheckboxes(5, event);" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="evenRow">
                                    <td nowrap>
                                        <asp:HiddenField ID="hdnVctId" runat="server" Value='<%# Bind("VctId") %>' />
                                        <asp:HiddenField ID="hdnModelId" runat="server" Value='<%# Bind("ModelId") %>' />
                                        <asp:HiddenField ID="hdnIntegrityNo" runat="server" Value='<%# Bind("IntegrityNo") %>' />
                                        <asp:HiddenField ID="hdnFleetModel" runat="server" Value='<%# Bind("FleetMdl") %>' />
                                        <%#Container.DataItem("FleetMdl")%>
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox onkeypress="return keyStrokeDecimal(event);" onblur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                            class="Textbox_Small_Right_Aligned" runat="Server" ID="txtDailyCost" Text='<%#Bind("VarDailyCost")%>' MaxLength="8"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox onkeypress="return keyStrokeDecimal(event);" onblur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                            class="Textbox_Small_Right_Aligned" runat="Server" ID="txtPenaltyCost" Text='<%#Bind("PenaltyCost")%>' MaxLength="8"></asp:TextBox>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkIsReloc" runat="server" Checked='<%# Eval("IsReloc") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkOneWayHire" runat="server" Checked='<%# Eval("OneWayHire") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="oddRow">
                                    <td nowrap>
                                        <asp:HiddenField ID="hdnVctId" runat="server" Value='<%# Bind("VctId") %>' />
                                        <asp:HiddenField ID="hdnModelId" runat="server" Value='<%# Bind("ModelId") %>' />
                                        <asp:HiddenField ID="hdnIntegrityNo" runat="server" Value='<%# Bind("IntegrityNo") %>' />
                                        <asp:HiddenField ID="hdnFleetModel" runat="server" Value='<%# Bind("FleetMdl") %>' />
                                        <%#Container.DataItem("FleetMdl")%>
                                    </td>
                                    <td nowrap>
                                        <asp:TextBox onkeypress="return keyStrokeDecimal(event);" onblur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                            class="Textbox_Small_Right_Aligned" runat="Server" ID="txtDailyCost" Text='<%#Bind("VarDailyCost")%>' MaxLength="8"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox onkeypress="return keyStrokeDecimal(event);" onblur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                            class="Textbox_Small_Right_Aligned" runat="Server" ID="txtPenaltyCost" Text='<%#Bind("PenaltyCost")%>' MaxLength="8"></asp:TextBox>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkIsReloc" runat="server" Checked='<%# Eval("IsReloc") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkOneWayHire" runat="server" Checked='<%# Eval("OneWayHire") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkActive" runat="server" Checked='<%# Eval("Active") %>' />
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <%--Table ends--%>
                        <%--<br />--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="right">
                        <%--Buttons Row--%>
                        <asp:Button ID="btnSave1" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel1" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <%--<asp:Button ID="btnRemove1" Text="Remove" runat="server" CssClass="Button_Standard" />--%>
                    </td>
                </tr>
            </table>
            <ucConfirmation:ConfirmationBoxControl ID="cnfConfirmation" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
