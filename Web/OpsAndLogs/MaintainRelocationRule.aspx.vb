Option Strict On
Option Explicit On



'' Change log!
''  ID4   - Shoel : date format thingie added !
'' 17.9.8 - Shoel : Type is no longer optional - needed to fix Squish#604
'' 19.9.8 - Shoel : 604 is harder than i thought :( - hope its done now!
'' 25.9.8 - Shoel : 622 pickers needed 'LOCATIONFORCOUNTRY' and not 'LOCATION' :)

Imports System.Collections.Generic

Imports System.Xml
Imports System.Data
Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.MaintainRelocationRule)> _
<AuroraMessageAttribute("GEN005,GEN062,GEN070")> _
Partial Class OpsAndLogs_MaintainRelocationRule
    Inherits AuroraPage

    Const VIEWSTATE_OLDXML As String = "ViewState_MRR_OldXml"
    Const VIEWSTATE_OPTION As String = "ViewState_MRR_Option"
    Const VIEWSTATE_COUNTRY As String = "ViewState_MRR_CtyCode"
    Const VIEWSTATE_COMBO As String = "ViewState_MRR_Combo"

    Protected Shared mStateKey As String = Guid.NewGuid().ToString().Replace("-", "")

    Private mCountry As String

    Protected ReadOnly Property Country As String
        Get
            If mCountry Is Nothing Then
                mCountry = DirectCast(ViewState(VIEWSTATE_COUNTRY), String)
            End If

            Return mCountry
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Dim xmlRet
            'fetch combos

            RestoreState()

            CreateInitialCombo()
            LoadMainTable(SearchParam)


            pkrFilterLocationFrom.Param1 = Country
            pkrFilterLocationTo.Param1 = Country
        End If
    End Sub

    Sub CreateInitialCombo()

        Dim dtRelocationTypes As New DataTable
        Dim dstRelocationTypes As DataSet
        dstRelocationTypes = Aurora.Common.Data.GetCodecodetype("33", "")
        If dstRelocationTypes.Tables.Count > 0 Then
            dtRelocationTypes = dstRelocationTypes.Tables(0)
        End If

        ViewState(VIEWSTATE_COMBO) = dtRelocationTypes
        'Dim cmbType As New DropDownList
        ''cmbType = e.Item.FindControl("cmbRelocationType")
        'cmbType.DataSource = dtRelocationTypes
        'cmbType.DataTextField = "DESCRIPTION"
        'cmbType.DataValueField = "ID"


        'cmbType.DataBind()
        'cmbType.Attributes.Add("SelectionId", "")

        'ViewState(SESSION_COMBO) = cmbType
    End Sub

    Sub LoadMainTable(ByVal Country As String)
        Dim xmlReturn As XmlDocument
        If Country = "" Then
            Country = CountryCode
        End If

        Dim state As FilterState = GetCurrentFilterState()

        xmlReturn = Aurora.OpsAndLogs.Services.RelocationRules.GetReloactionRules(Country, UserCode, state.FromLocationCode, state.ToLocationCode, state.FleetModelId)

        ' check for errors
        If Not xmlReturn.DocumentElement.SelectSingleNode("Error/ErrStatus") Is Nothing Then
            If xmlReturn.DocumentElement.SelectSingleNode("Error/ErrStatus").InnerText.ToUpper() = "TRUE" Then
                SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/ErrDesc").InnerText)
                rptMain.DataSource = New DataTable
                rptMain.DataBind()
                ' disable buttons
                btnSave.Enabled = False
                btnSave1.Enabled = False
                btnCancel.Enabled = False
                btnCancel1.Enabled = False
                btnRemove.Enabled = False
                btnRemove1.Enabled = False
                Exit Sub
            End If
        End If


        ' load the upper stuff
        txtCountryOfOperation.Text = xmlReturn.DocumentElement.SelectSingleNode("CountryOfOperation/CtyName").InnerText
        ViewState(VIEWSTATE_COUNTRY) = xmlReturn.DocumentElement.SelectSingleNode("CountryOfOperation/CtyCode").InnerText
        ' save ctycode to viewstate

        ' minor hack for the checkboxes of last row
        xmlReturn.DocumentElement.SelectSingleNode("Rules").LastChild.SelectSingleNode("Avl").InnerText = "0"
        xmlReturn.DocumentElement.SelectSingleNode("Rules").LastChild.SelectSingleNode("Remove").InnerText = "0"

        ' save to viewstate for future use
        ViewState(VIEWSTATE_OLDXML) = xmlReturn.DocumentElement.SelectSingleNode("Rules").OuterXml

        ' load the repeater

        Dim dstRelocationRules As New DataSet
        dstRelocationRules.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Rules")))
        If dstRelocationRules.Tables.Count > 0 Then
            ' hack for date format
            For Each oRow As DataRow In dstRelocationRules.Tables(0).Rows
                Dim dtTemp As Date
                If Not CStr(oRow(3)).Trim().Equals(String.Empty) Then
                    dtTemp = ParseDateTime(CStr(oRow(3)), SystemCulture)
                    oRow(3) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                End If
                If Not CStr(oRow(4)).Trim().Equals(String.Empty) Then
                    dtTemp = ParseDateTime(CStr(oRow(4)), SystemCulture)
                    oRow(4) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                End If
            Next
            ' hack for date format
            rptMain.DataSource = FilterRecords(dstRelocationRules.Tables(0))
            rptMain.DataBind()
        End If

    End Sub

    Protected Function FilterRecords(ByVal table As DataTable) As DataView
        'Dim state As FilterState = GetCurrentFilterState()

        'Dim sb As New StringBuilder()

        'If Not String.IsNullOrEmpty(state.FromLocationCode) Then
        '    sb.Append("LocFrom = '" & state.FromLocationCode)
        '    sb.Append("'")
        'End If

        'If Not String.IsNullOrEmpty(state.ToLocationCode) Then
        '    If sb.Length > 0 Then
        '        sb.Append(" and ")
        '    End If

        '    sb.Append("LocTo = '" & state.ToLocationCode)
        '    sb.Append("'")
        'End If

        'If state.FleetModelId.HasValue Then
        '    If sb.Length > 0 Then
        '        sb.Append(" and ")
        '    End If

        '    sb.Append("FlmId = '" & state.FleetModelId.Value.ToString())
        '    sb.Append("'")
        'End If

        'Dim resultView As New DataView(table)
        'resultView.RowFilter = sb.ToString()

        Dim resultView As New DataView(table)
        resultView.RowFilter = "IsSelected = 1"

        Return resultView
    End Function

    Protected Function GetMultiselectItems(ByVal data As String) As MultiSelectItem()

        Dim doc As New XmlDocument()
        doc.AppendChild(doc.CreateElement("root"))

        doc.FirstChild.InnerXml = data

        Dim items(doc.FirstChild.ChildNodes.Count) As MultiSelectItem

        Dim intIndex As Integer = 0
        For Each modelNode As XmlNode In doc.FirstChild.ChildNodes

            Dim item As New MultiSelectItem(modelNode.SelectSingleNode("FlmId").InnerText, modelNode.SelectSingleNode("FlmCode").InnerText)
            items(intIndex) = item

            intIndex += 1
        Next

        Return items
    End Function

    Protected Sub rptMain_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMain.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            'If CType(e.Item.FindControl("chkAvlSch"), CheckBox).Text = "1" Then
            '    CType(e.Item.FindControl("chkAvlSch"), CheckBox).Checked = True
            'Else
            '    CType(e.Item.FindControl("chkAvlSch"), CheckBox).Checked = False
            'End If

            'If CType(e.Item.FindControl("chkRemove"), CheckBox).Text = "1" Then
            '    CType(e.Item.FindControl("chkRemove"), CheckBox).Checked = True
            'Else
            '    CType(e.Item.FindControl("chkRemove"), CheckBox).Checked = False
            'End If

            Dim cmbType As DropDownList
            cmbType = DirectCast(e.Item.FindControl("cmbRelocationType"), DropDownList)
            cmbType.ClearSelection()
            cmbType.DataSource = ViewState(VIEWSTATE_COMBO)
            cmbType.DataTextField = "DESCRIPTION"
            cmbType.DataValueField = "ID"

            cmbType.DataBind()
            cmbType.Items.Insert(0, "")
            cmbType.SelectedValue = CType(e.Item.FindControl("hdnRelocationType"), HiddenField).Value

            Dim oPickerControl As UserControls_PickerControl
            oPickerControl = DirectCast(e.Item.FindControl("pkrLocationFrom"), UserControls_PickerControl)
            oPickerControl.Param1 = CStr(ViewState(VIEWSTATE_COUNTRY)).Trim()
            oPickerControl = DirectCast(e.Item.FindControl("pkrLocationTo"), UserControls_PickerControl)
            oPickerControl.Param1 = CStr(ViewState(VIEWSTATE_COUNTRY)).Trim()

        End If
    End Sub

    'Protected Sub rptMain_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMain.ItemCreated
    '    'GetCodecodetype

    '    If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
    '        Dim cmbType As DropDownList

    '        cmbType = CType(e.Item.FindControl("cmbRelocationType"), DropDownList)
    '        cmbType.ClearSelection()
    '        cmbType.DataSource = ViewState(VIEWSTATE_COMBO)
    '        cmbType.DataTextField = "DESCRIPTION"
    '        cmbType.DataValueField = "ID"

    '        cmbType.DataBind()


    '    End If

    'End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel1.Click
        LoadMainTable(SearchParam)
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click, btnRemove1.Click
        ' check if the records are selected!
        Dim bAtleastOneRecordSelected As Boolean = False
        For Each oItem As RepeaterItem In rptMain.Items
            If CType(oItem.FindControl("chkRemove"), CheckBox).Checked Then
                bAtleastOneRecordSelected = True
                Exit For
            End If
        Next

        If Not bAtleastOneRecordSelected Then
            ' nothing selected - byebye!
            SetWarningShortMessage("Please select the Relocation Record(s) to Delete")
            Exit Sub
        End If

        ' ask if we should really delete!
        ViewState(VIEWSTATE_OPTION) = "Remove"
        cnfConfirmation.Text = "Removing one or more Relocation Rules may have severe consequences on the DVASS scheduling optimiser." & vbNewLine _
                                & "Are you sure you wish to continue with removing the selected items?"
        cnfConfirmation.Title = "Confirmation required"
        cnfConfirmation.Show()

    End Sub

    Function GetChangedRecords() As XmlDocument
        '<RelocationRule>
        '  <ID>5228FF94-5F53-493A-B865-E1D05DC1A721</ID>
        '  <LocFrom>AKL</LocFrom>
        '  <LocTo>CHC</LocTo>
        '  <StDt>07/01/2003</StDt>
        '  <ToDt>01/01/2020</ToDt>
        '  <Days>3</Days>
        '  <FixCost>1500.00</FixCost>
        '  <Type>99C1EA32-2E38-4EB8-9919-AA64C9366AEB</Type>
        '  <Avl>0</Avl>
        '  <IntegrityNo>3</IntegrityNo>
        '  <Remove>0</Remove>
        '  <ExludeFleetModel>1 or 0</ExludeFleetModel>
        '</RelocationRule>

        ' supposed to return only the modified/new rows (if the new row holds data)

        ' step 1 - get original xml

        Dim xmlOriginal As New XmlDocument
        xmlOriginal.LoadXml(CStr(ViewState(VIEWSTATE_OLDXML)))

        ' step 2 - get xml from screen
        Dim sbNewXML As New StringBuilder
        sbNewXML.Append("<Rules>")

        ' add to xml and simultaneously validate
        'For Each oItem As RepeaterItem In rptMain.Items
        For i As Integer = 0 To rptMain.Items.Count - 1

            sbNewXML.Append("<RelocationRule>")

            sbNewXML.Append("<ID>" & CType(rptMain.Items(i).FindControl("hdnID"), HiddenField).Value & "</ID>")

            ' last node is special - it allows add
            If i = rptMain.Items.Count - 1 Then

                sbNewXML.Append("<LocFrom>" & CType(rptMain.Items(i).FindControl("pkrLocationFrom"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).Text & "</LocFrom>")
                sbNewXML.Append("<LocTo>" & CType(rptMain.Items(i).FindControl("pkrLocationTo"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).Text & "</LocTo>")


                ' date format stuff
                'sbNewXML.Append("<StDt>" & CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text & "</StDt>")
                'sbNewXML.Append("<ToDt>" & CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text & "</ToDt>")
                If Not CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text.Trim().Equals(String.Empty) Then
                    sbNewXML.Append("<StDt>" & DateTimeToString(CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c) & "</StDt>")
                Else
                    ' it has to be empty!
                    sbNewXML.Append("<StDt></StDt>")
                End If
                If Not CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text.Trim().Equals(String.Empty) Then
                    sbNewXML.Append("<ToDt>" & DateTimeToString(CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c) & "</ToDt>")
                Else
                    ' it has to be empty!
                    sbNewXML.Append("<ToDt></ToDt>")
                End If
                ' date format stuff
            Else
                ' modified records
                If CType(rptMain.Items(i).FindControl("pkrLocationFrom"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).IsValid And Trim(CType(rptMain.Items(i).FindControl("pkrLocationFrom"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).Text) <> "" Then
                    sbNewXML.Append("<LocFrom>" & CType(rptMain.Items(i).FindControl("pkrLocationFrom"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).Text & "</LocFrom>")
                Else
                    GoTo FailedValidation
                End If


                If CType(rptMain.Items(i).FindControl("pkrLocationTo"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).IsValid And Trim(CType(rptMain.Items(i).FindControl("pkrLocationTo"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).Text) <> "" Then
                    sbNewXML.Append("<LocTo>" & CType(rptMain.Items(i).FindControl("pkrLocationTo"), ASP.usercontrols_pickercontrol_pickercontrol_ascx).Text & "</LocTo>")
                Else
                    GoTo FailedValidation
                End If

                If CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).IsValid And Trim(CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text) <> "" Then
                    'sbNewXML.Append("<StDt>" & CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text & "</StDt>")
                    sbNewXML.Append("<StDt>" & DateTimeToString(CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c) & "</StDt>")
                Else
                    GoTo FailedValidation
                End If

                If CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).IsValid And Trim(CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text) <> "" Then
                    'sbNewXML.Append("<ToDt>" & CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Text & "</ToDt>")
                    sbNewXML.Append("<ToDt>" & DateTimeToString(CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c) & "</ToDt>")
                Else
                    GoTo FailedValidation
                End If

                ' combo validation
                If CType(rptMain.Items(i).FindControl("cmbRelocationType"), DropDownList).SelectedValue.Trim().Equals(String.Empty) Then
                    GoTo FailedValidation
                End If

            End If

            ' from - to date validation
            ''GEN062
            If CType(rptMain.Items(i).FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx).Date > CType(rptMain.Items(i).FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx).Date Then
                GoTo InvalidDateRange
            End If

            If IsNumeric(CType(rptMain.Items(i).FindControl("txtDays"), TextBox).Text) Or (i = rptMain.Items.Count - 1) Then
                sbNewXML.Append("<Days>" & CType(rptMain.Items(i).FindControl("txtDays"), TextBox).Text & "</Days>")
            Else
                GoTo FailedValidation
            End If

            sbNewXML.Append("<FixCost>" & CType(rptMain.Items(i).FindControl("txtFixedCost"), TextBox).Text & "</FixCost>")
            sbNewXML.Append("<Type>" & CType(rptMain.Items(i).FindControl("cmbRelocationType"), DropDownList).SelectedValue & "</Type>")

            If (CType(rptMain.Items(i).FindControl("chkAvlSch"), CheckBox).Checked) Then
                sbNewXML.Append("<Avl>1</Avl>")
            Else
                sbNewXML.Append("<Avl>0</Avl>")
            End If

            Dim fleetModels As ASP.usercontrols_multiselectcontrol_multiselect_ascx = DirectCast(rptMain.Items(i).FindControl("fleetModels"), ASP.usercontrols_multiselectcontrol_multiselect_ascx)

            'If fleetModels.HasChanged Then
            sbNewXML.Append("<FleetModels>")
            sbNewXML.Append(fleetModels.GetKeysAsCsv())
            sbNewXML.Append("</FleetModels>")
            'End If


            If CType(rptMain.Items(i).FindControl("hdnIntegrityNo"), HiddenField).Value = "" Then
                sbNewXML.Append("<IntegrityNo>1</IntegrityNo>")
            Else
                sbNewXML.Append("<IntegrityNo>" & CType(rptMain.Items(i).FindControl("hdnIntegrityNo"), HiddenField).Value & "</IntegrityNo>")
            End If

            If (CType(rptMain.Items(i).FindControl("chkRemove"), CheckBox).Checked) Then
                sbNewXML.Append("<Remove>1</Remove>")
            Else
                sbNewXML.Append("<Remove>0</Remove>")
            End If

            'Dim modelPicker As ASP.usercontrols_pickercontrol_pickercontrol_ascx = DirectCast(rptMain.Items(i).FindControl("modelPickerControl"), ASP.usercontrols_pickercontrol_pickercontrol_ascx)

            'If Not modelPicker.IsValid Then
            '    SetWarningShortMessage(modelPicker.ErrorMessage)
            '    Return New XmlDocument()
            'End If

            'sbNewXML.Append("<FlmModelID>" & modelPicker.DataId & "</FlmModelID>")

            ''rev:mia             Sept 30, 2013 - addition of Exlude Fleet Model
            Dim chkExcludeFleetMode As CheckBox
            Try
                chkExcludeFleetMode = DirectCast(rptMain.Items(i).FindControl("chkExcludeFleetMode"), CheckBox)
                If (Not String.IsNullOrEmpty(fleetModels.GetKeysAsCsv())) Then
                    sbNewXML.AppendFormat("<ExludeFleetModel>{0}</ExludeFleetModel>", IIf(chkExcludeFleetMode.Checked, 1, 0))
                Else
                    sbNewXML.Append("<ExludeFleetModel>0</ExludeFleetModel>")
                End If

            Catch ex As Exception
                sbNewXML.Append("<ExludeFleetModel>0</ExludeFleetModel>")
            End Try

            sbNewXML.Append("</RelocationRule>")
        Next

        sbNewXML.Append("</Rules>")

        Dim xmlNew As New XmlDocument
        xmlNew.LoadXml(sbNewXML.ToString())
        'xmlChanges.LoadXml("<Rules/>")

        ' step 3 - create the updated version xml

        'For i As Integer = 0 To xmlOriginal.DocumentElement.ChildNodes.Count - 2
        '    If Not xmlOriginal.DocumentElement.ChildNodes(i).InnerXml = xmlNew.DocumentElement.ChildNodes(i).InnerXml Then
        '        ' has been changed, add to the updated 
        '        xmlChanges.DocumentElement.InnerXml = xmlChanges.DocumentElement.InnerXml & xmlNew.DocumentElement.ChildNodes(i).OuterXml
        '    End If
        'Next

        ' step 4 - append the last row of grid, if anything was entered in it - post validation of course!
        With xmlNew.DocumentElement.LastChild

            If .SelectSingleNode("LocFrom").InnerText = "" And _
                           .SelectSingleNode("LocTo").InnerText = "" And _
                           .SelectSingleNode("StDt").InnerText = "" And _
                           .SelectSingleNode("ToDt").InnerText = "" And _
                           .SelectSingleNode("FleetModels").InnerText = "" And _
                           .SelectSingleNode("Days").InnerText = "" Then
                ' not modified, ignore
            Else
                If .SelectSingleNode("LocFrom").InnerText = "" Or _
                   .SelectSingleNode("LocTo").InnerText = "" Or _
                   .SelectSingleNode("StDt").InnerText = "" Or _
                   .SelectSingleNode("ToDt").InnerText = "" Or _
                   .SelectSingleNode("Days").InnerText = "" Or _
                   .SelectSingleNode("Type").InnerText = "" _
                Then
                    ' invalid!!
                    GoTo FailedValidation
                End If

            End If

            'If Not (.SelectSingleNode("LocFrom").InnerText = "" And _
            '   .SelectSingleNode("LocTo").InnerText = "" And _
            '   .SelectSingleNode("StDt").InnerText = "" And _
            '   .SelectSingleNode("ToDt").InnerText = "" And _
            '   .SelectSingleNode("Days").InnerText = "") Then
            '    ' Modified, add it
            '    xmlChanges.DocumentElement.InnerXml = xmlChanges.DocumentElement.InnerXml & .OuterXml
            'End If
        End With

        RemoveUnchangedNodes(xmlOriginal, xmlNew)

        If xmlNew.DocumentElement.ChildNodes.Count = 0 Then
            GoTo NothingChanged
        End If

        If Not ValidateRelocationRules(xmlOriginal, xmlNew, True) OrElse Not ValidateRelocationRules(xmlNew, xmlNew, False) Then
            Return Nothing
        End If

        'If xmlChanges.DocumentElement.ChildNodes.Count = 0 Then
        '    GoTo NothingChanged
        'End If

        ' check for "Overlap"
        'Dim sValidationResult As String
        'sValidationResult = Aurora.OpsAndLogs.Services.RelocationRules.ValidateRelocationRule(xmlNew)
        'If sValidationResult = "GEN070" Then
        '    GoTo Overlap
        'End If

        ' so far so good! this must be valid
        Return xmlNew

        ' validate the 'modified' stuff, no asterix marked fields are allowed to be empty!
FailedValidation:
        SetWarningShortMessage(GetMessage("GEN005"))
        GetChangedRecords = New XmlDocument
        Exit Function
NothingChanged:
        SetWarningShortMessage("No changes have been made")
        GetChangedRecords = New XmlDocument
        Exit Function
InvalidDateRange:
        SetWarningShortMessage(GetMessage("GEN062"))
        GetChangedRecords = New XmlDocument
        Exit Function
Overlap:
        SetWarningShortMessage(GetMessage("GEN070"))
        GetChangedRecords = New XmlDocument
        Exit Function
    End Function

    Protected Sub RemoveUnchangedNodes(ByVal compareDoc As XmlDocument, ByVal resultDoc As XmlDocument)
        Dim compareNodes As XmlNodeList = compareDoc.DocumentElement.ChildNodes
        Dim resultNodes As XmlNodeList = resultDoc.DocumentElement.ChildNodes

        Dim index As Integer = 0
        Dim resultIndex As Integer = 0
        Dim removedSourceCount As Integer = 0
        Dim removedResultCount As Integer = 0
        Dim len As Integer = compareNodes.Count

        While index < len
            If compareNodes(index - removedSourceCount)("IsSelected").InnerText = "1" Then
                Dim resultNode As XmlNode = resultNodes(resultIndex - removedResultCount)
                If AreNodesEqual(compareNodes(index - removedSourceCount), New String() {"FleetModels", "IsSelected"}, resultNode, AddressOf AreModelsEqual) Then
                    'If resultNode.SelectSingleNode("FleetModels") Is Nothing Then
                    resultNode.ParentNode.RemoveChild(resultNode)
                    removedResultCount += 1
                Else
                    Dim sourceNode As XmlNode = compareNodes(index - removedSourceCount)
                    sourceNode.ParentNode.RemoveChild(sourceNode)
                    removedSourceCount += 1
                End If
                resultIndex += 1
            End If
            index += 1
        End While
    End Sub

    Protected Function AreModelsEqual(ByVal nodeA As XmlNode, ByVal nodeB As XmlNode) As Boolean
        If nodeA IsNot Nothing AndAlso nodeB IsNot Nothing Then
            If nodeA.NodeType = XmlNodeType.Element AndAlso nodeA.Name = "FleetModels" Then
                Return AreModelsEqual(nodeA.InnerText, nodeB.InnerText)
            End If
        End If

        Return True
    End Function

    Public Delegate Function NodesComparer(ByVal nodeA As XmlNode, ByVal nodeB As XmlNode) As Boolean
    Protected Function AreNodesEqual(ByVal nodeA As XmlNode, ByVal skipNodeNamesA As String(), ByVal nodeB As XmlNode, ByVal comparer As NodesComparer) As Boolean
        If skipNodeNamesA Is Nothing Then
            ReDim skipNodeNamesA(0)
        End If

        If (nodeA Is Nothing Xor nodeB Is Nothing) _
            OrElse nodeA.NodeType <> nodeB.NodeType Then
            Return False
        End If

        If nodeA.NodeType = XmlNodeType.Text Then
            If nodeA.Value <> nodeB.Value Then
                'nodeA.Value = nodeB.Value

                Return False
            End If

            Return True
        End If

        'If nodeA.ChildNodes.Count <> nodeB.ChildNodes.Count Then
        '    Return False
        'End If


        Dim nodeIndex As Integer = 0
        Dim nodesA As XmlNodeList = nodeA.ChildNodes
        Dim nodesB As XmlNodeList = nodeB.ChildNodes

        Dim nodeLengthA As Integer = nodesA.Count
        Dim nodeLengthB As Integer = nodesB.Count
        Dim nodeSkip As Integer = 0

        While True
            If nodeIndex >= nodeLengthA OrElse nodeIndex - nodeSkip >= nodeLengthB Then
                If nodeLengthB + nodeSkip <> nodeLengthA Then
                    While nodeIndex < nodeLengthA
                        If Array.IndexOf(skipNodeNamesA, nodesA(nodeIndex).Name) = -1 Then
                            Return False
                        End If
                        nodeIndex += 1
                    End While
                End If
                Exit While
            End If

            Dim childA As XmlNode = nodesA(nodeIndex)
            Dim childB As XmlNode = nodesB(nodeIndex - nodeSkip)

            If Array.IndexOf(skipNodeNamesA, childA.Name) = -1 Then
                If Not AreNodesEqual(childA, skipNodeNamesA, childB, comparer) Then
                    Return False
                End If
            Else
                If Array.IndexOf(skipNodeNamesA, childB.Name) = -1 Then
                    nodeSkip += 1
                End If

                If comparer IsNot Nothing AndAlso Not comparer(childA, childB) Then
                    Return False
                End If
            End If

            nodeIndex += 1
        End While

        Return True
    End Function

    ''' <summary>
    ''' Saves the Changes
    ''' </summary>
    ''' <param name="SaveFlag">True - Save/Update, False - Delete</param>
    ''' <remarks></remarks>
    Sub SaveChanges(ByVal SaveFlag As Boolean)


        ' validate data
        Dim xmlChanges As New XmlDocument
        xmlChanges = GetChangedRecords()

        'check if its ok
        If xmlChanges Is Nothing OrElse xmlChanges.OuterXml.Length = 0 Then
            ' nothing inside! exit!
            Exit Sub
        End If

        'If Not ValidateRelocationRules() Then
        '    Return
        'End If

        Dim xmlScreenData As New XmlDocument
        xmlScreenData.LoadXml("<Root><CtyCode>" & CStr(ViewState(VIEWSTATE_COUNTRY)) & "</CtyCode>" & xmlChanges.OuterXml & _
        "<UsrId>" & UserCode & "</UsrId><PrgmName>MaintainRelocationRule.aspx</PrgmName></Root>")

        Dim xmlReturn As XmlDocument
        xmlReturn = Aurora.OpsAndLogs.Services.RelocationRules.UpdateRelocationRule(xmlScreenData, SaveFlag)

        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' refresh the data
            LoadMainTable(CStr(ViewState(VIEWSTATE_COUNTRY)))
        Else
            ' theres an error!
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If

    End Sub

    Protected Function ValidateRelocationRules(ByVal document As XmlDocument, ByVal newDocument As XmlDocument, ByVal oldSource As Boolean) As Boolean

        Dim index1 As Integer = 0
        For Each node As XmlNode In document.FirstChild.ChildNodes

            Dim id As String = node("ID").InnerText

            Dim locFrom As String = node("LocFrom").InnerText
            Dim locTo As String = node("LocTo").InnerText

            Dim dateFrom As DateTime
            DateTime.TryParse(node("StDt").InnerText, dateFrom)

            Dim dateTo As DateTime
            DateTime.TryParse(node("ToDt").InnerText, dateTo)

            Dim relocType As String = node("Type").InnerText
            Dim available As String = node("Avl").InnerText

            Dim models As String = GetNodeText(node, "FleetModels")

            Dim index2 As Integer = 0
            For Each node2 As XmlNode In newDocument.FirstChild.ChildNodes

                Dim id2 As String = node2("ID").InnerText

                If id <> id2 Then

                    Dim locFrom2 As String = node2("LocFrom").InnerText
                    Dim locTo2 As String = node2("LocTo").InnerText

                    Dim dateFrom2 As DateTime
                    DateTime.TryParse(node2("StDt").InnerText, dateFrom2)

                    Dim dateTo2 As DateTime
                    DateTime.TryParse(node2("ToDt").InnerText, dateTo2)

                    Dim relocType2 As String = node2("Type").InnerText
                    Dim available2 As String = node2("Avl").InnerText

                    Dim models2 As String = GetNodeText(node2, "FleetModels")

                    If locFrom = locFrom2 AndAlso locTo = locTo2 AndAlso available = available2 _
                        AndAlso relocType = relocType2 _
                        AndAlso dateFrom <= dateTo2 AndAlso dateFrom2 <= dateTo Then

                        If oldSource Then
                            If AreModelsOverlappingXml(models, models2) Then
                                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN070"))
                                Return False
                            End If
                        Else
                            If AreModelsOverlappingCsv(models, models2) Then
                                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN070"))
                                Return False
                            End If
                        End If
                    End If

                End If

                index2 += 1
            Next

            index1 += 1
        Next

        Return True
    End Function

    Protected Function GetNodeText(ByVal parentNode As XmlNode, ByVal nodeName As String) As String
        Dim node As XmlNode = parentNode(nodeName)

        If node IsNot Nothing Then
            Return node.InnerText
        End If

        Return ""
    End Function

    Protected Function AreModelsOverlappingXml(ByVal modelsXml As String, ByVal modelsCsv As String) As Boolean

        modelsXml = "<root>" + modelsXml + "</root>"

        Dim stringReader As System.IO.StringReader = Nothing
        Dim xmlReader As XmlReader = Nothing

        Dim xmlContainsModels As Boolean = False

        Dim models2 As List(Of String)

        If Not String.IsNullOrEmpty(modelsCsv) Then
            models2 = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(modelsCsv)
        Else
            models2 = New List(Of String)
        End If


        Try
            stringReader = New System.IO.StringReader(modelsXml)
            xmlReader = xmlReader.Create(stringReader)



            While xmlReader.Read()
                If xmlReader.NodeType = XmlNodeType.Element AndAlso xmlReader.Name = "FlmId" Then
                    xmlContainsModels = True

                    Dim model1 As String = xmlReader.ReadInnerXml()
                    For Each model2 As String In models2
                        If model1 = model2 Then
                            Return True
                        End If
                    Next
                End If
            End While
        Catch ex As Exception
            Throw ex
        Finally
            If stringReader IsNot Nothing Then
                stringReader.Close()
            End If

            If xmlReader IsNot Nothing Then
                xmlReader.Close()
            End If
        End Try


        Return Not xmlContainsModels AndAlso models2.Count = 0
    End Function

    Protected Function AreModelsOverlappingCsv(ByVal modelsCsv As String, ByVal modelsCsv2 As String) As Boolean
        If String.IsNullOrEmpty(modelsCsv) AndAlso String.IsNullOrEmpty(modelsCsv2) Then
            Return True
        End If


        Dim models1 As List(Of String) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(modelsCsv)
        Dim models2 As List(Of String) = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(modelsCsv2)

        For Each model1 As String In models1
            For Each model2 As String In models2
                If model1 = model2 Then
                    Return True
                End If
            Next
        Next

        Return False
    End Function

    Protected Function AreModelsEqual(ByVal modelsXml As String, ByVal modelsCsv As String) As Boolean

        modelsXml = "<root>" + modelsXml + "</root>"

        Dim stringReader As System.IO.StringReader = Nothing
        Dim xmlReader As XmlReader = Nothing

        Dim modelIndex As Integer = 0

        Dim xmlModelCount As Integer = 0

        Dim models As List(Of String)

        If Not String.IsNullOrEmpty(modelsCsv) Then
            models = ASP.usercontrols_multiselectcontrol_multiselect_ascx.GetValuesFromCsv(modelsCsv)
        Else
            models = New List(Of String)
        End If

        Try
            stringReader = New System.IO.StringReader(modelsXml)
            xmlReader = xmlReader.Create(stringReader)



            While xmlReader.Read()
                Dim matched As Boolean = False

                If xmlReader.NodeType = XmlNodeType.Element AndAlso xmlReader.Name = "FlmId" Then
                    xmlModelCount += 1

                    If modelIndex >= models.Count Then
                        Return False
                    End If

                    Dim model1 As String = xmlReader.ReadInnerXml().Trim()
                    Dim model2 As String = models(modelIndex).Trim()

                    If model1 <> model2 Then
                        Return False
                    End If

                    modelIndex += 1
                End If
            End While
        Catch ex As Exception
            Throw ex
        Finally
            If stringReader IsNot Nothing Then
                stringReader.Close()
            End If

            If xmlReader IsNot Nothing Then
                xmlReader.Close()
            End If
        End Try


        Return xmlModelCount = models.Count
    End Function

    'Protected Function ValidateRelocationRules1() As Boolean

    '    For Each item As RepeaterItem In rptMain.Items

    '        Dim locFrom As ASP.usercontrols_pickercontrol_pickercontrol_ascx = DirectCast(item.FindControl("pkrLocationFrom"), ASP.usercontrols_pickercontrol_pickercontrol_ascx)
    '        Dim locTo As ASP.usercontrols_pickercontrol_pickercontrol_ascx = DirectCast(item.FindControl("pkrLocationTo"), ASP.usercontrols_pickercontrol_pickercontrol_ascx)

    '        Dim dateFrom As ASP.usercontrols_datecontrol_datecontrol_ascx = DirectCast(item.FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx)
    '        Dim dateTo As ASP.usercontrols_datecontrol_datecontrol_ascx = DirectCast(item.FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx)

    '        Dim relocType As DropDownList = DirectCast(item.FindControl("cmbRelocationType"), DropDownList)
    '        Dim available As CheckBox = DirectCast(item.FindControl("chkAvlSch"), CheckBox)


    '        Dim modelsControl As UserControls_SelectControl_MultiSelect = DirectCast(item.FindControl("fleetModels"), UserControls_SelectControl_MultiSelect)

    '        For Each item2 As RepeaterItem In rptMain.Items

    '            If item2.ItemIndex <> item.ItemIndex Then

    '                Dim locFrom2 As ASP.usercontrols_pickercontrol_pickercontrol_ascx = DirectCast(item2.FindControl("pkrLocationFrom"), ASP.usercontrols_pickercontrol_pickercontrol_ascx)
    '                Dim locTo2 As ASP.usercontrols_pickercontrol_pickercontrol_ascx = DirectCast(item2.FindControl("pkrLocationTo"), ASP.usercontrols_pickercontrol_pickercontrol_ascx)

    '                Dim dateFrom2 As ASP.usercontrols_datecontrol_datecontrol_ascx = DirectCast(item2.FindControl("dtDateFrom"), ASP.usercontrols_datecontrol_datecontrol_ascx)
    '                Dim dateTo2 As ASP.usercontrols_datecontrol_datecontrol_ascx = DirectCast(item2.FindControl("dtDateTo"), ASP.usercontrols_datecontrol_datecontrol_ascx)

    '                Dim relocType2 As DropDownList = DirectCast(item2.FindControl("cmbRelocationType"), DropDownList)
    '                Dim available2 As CheckBox = DirectCast(item2.FindControl("chkAvlSch"), CheckBox)

    '                If locFrom.Text = locFrom2.Text AndAlso locTo.Text = locTo2.Text AndAlso available.Checked = available2.Checked _
    '                    AndAlso relocType.SelectedValue = relocType2.SelectedValue Then

    '                    If dateFrom2.Date <= dateFrom.Date AndAlso dateTo2.Date < dateTo.Date AndAlso dateTo2.Date > dateFrom.Date Or _
    '                            dateFrom2.Date >= dateFrom.Date And dateTo2.Date <= dateTo.Date Or _
    '                            dateFrom2.Date > dateFrom.Date And dateTo2.Date >= dateTo.Date And dateFrom2.Date < dateTo.Date Or _
    '                            dateFrom2.Date <= dateFrom.Date And dateTo2.Date > dateTo.Date Then

    '                        Dim modelsControl2 As UserControls_SelectControl_MultiSelect = DirectCast(item2.FindControl("fleetModels"), UserControls_SelectControl_MultiSelect)

    '                        If DirectCast(modelsControl.Items, IList).Count = 0 AndAlso DirectCast(modelsControl2.Items, IList).Count = 0 Then
    '                            SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN070"))
    '                            Return False
    '                        End If

    '                        For Each modelItem As MultiSelectItem In modelsControl.Items
    '                            For Each modelItem2 As MultiSelectItem In modelsControl2.Items
    '                                If Object.Equals(modelItem.Key, modelItem2.Key) Then
    '                                    SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN070"))
    '                                    Return False
    '                                End If
    '                            Next
    '                        Next
    '                    End If
    '                End If

    '            End If
    '        Next
    '    Next

    '    Return True
    'End Function

    ' ViewState(VIEWSTATE_OPTION) 
    Protected Sub Confirmation_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cnfConfirmation.PostBack

        If leftButton Then
            If CStr(ViewState(VIEWSTATE_OPTION)) = "Remove" Then
                ViewState(VIEWSTATE_OPTION) = Nothing ' resetting this!
                SaveChanges(False)
            End If
        End If
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click, btnSave.Click
        SaveChanges(True)
    End Sub


    Protected Sub resetFields(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel1.Click
        RestoreState(New FilterState())
    End Sub

    Protected Sub btnFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        LoadMainTable(SearchParam)

        SaveState()
    End Sub

    Protected Sub btnResetFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetFilter.Click
        RestoreState(New FilterState())
        LoadMainTable(SearchParam)
        SaveState()
    End Sub

    Protected Sub RestoreState()
        Dim state As FilterState = DirectCast(Session(mStateKey), FilterState)

        If state Is Nothing Then
            Return
        End If

        RestoreState(state)
    End Sub

    Protected Sub RestoreState(ByVal state As FilterState)

        If (state.FleetModelId.HasValue) Then
            pkrFilterFleetModel.DataId = state.FleetModelId.Value.ToString()
        Else
            pkrFilterFleetModel.DataId = Nothing
            pkrFilterFleetModel.Text = Nothing
        End If

        pkrFilterLocationFrom.Text = state.FromLocationCode
        pkrFilterLocationTo.Text = state.ToLocationCode
    End Sub

    Protected Function GetCurrentFilterState() As FilterState

        Dim state As New FilterState()

        Dim fleetModelId As Integer

        If Integer.TryParse(pkrFilterFleetModel.DataId, fleetModelId) Then
            state.FleetModelId = fleetModelId
        Else
            state.FleetModelId = Nothing
        End If

        If pkrFilterLocationFrom.IsValid Then
            state.FromLocationCode = IIFX(String.IsNullOrEmpty(pkrFilterLocationFrom.Text), Nothing, pkrFilterLocationFrom.Text)
        Else
            state.FromLocationCode = Nothing
        End If

        If pkrFilterLocationTo.IsValid Then
            state.ToLocationCode = IIFX(String.IsNullOrEmpty(pkrFilterLocationTo.Text), Nothing, pkrFilterLocationTo.Text)
        Else
            state.ToLocationCode = Nothing
        End If

        Return state
    End Function

    Protected Sub SaveState()
        Dim state As FilterState = GetCurrentFilterState()

        Session(mStateKey) = state
    End Sub

    Protected Function GetValue(ByVal value As Object) As String
        If value Is Nothing Or Object.ReferenceEquals(value, DBNull.Value) Then
            Return ""
        End If

        Return value.ToString().Trim()
    End Function

    Public Class FilterState
        Public ToLocationCode As String
        Public FromLocationCode As String
        Public FleetModelId As Nullable(Of Integer)

    End Class
End Class
