'' Change Log
'' 26/5/8   - Shoel : fixed bug that allowed saving of blank form
'' 27/5/8   - Shoel : form elements are now cleared when a new booking is loaded
''  5/6/8   - Shoel : The Active check box is now selected by default on "Show details"
''  ID4     - Shoel : corrected the error UI
'' 10/7/8   - Shoel : fix for loading of Type combo
'' 23/9/8   - Shoel : Fix for squish issue # 620 also fixed the flashing cos of respose.redirects :)
'' 9/10/8   - Shoel : now called also from sched vwr!!! :D

' SPs reqd
' [OPL_VehicleAssignSaveNoteDetails]
' [OPL_VehicleAssignSaveAssign]
' [OPL_VehicleUndoAssign]

' Use this query to get valid rental nos!
' SELECT RntId FROM Rental 
' WITH(NOLOCK)  INNER JOIN Package (NOLOCK) ON RntPkgId = PkgId  
' WHERE PkgBrdCode IN (SELECT BrdCode FROM dbo.Brand (NOLOCK) WHERE BrdComCode = 'THL') AND RntStatus = 'KK'  
' example is 6322924/1

Imports System.Drawing
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Xml
Imports System.Data
Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.VehicleAssign)> _
<AuroraMessageAttribute("GEN046")> _
Partial Class OpsAndLogs_VehicleAssign
    Inherits AuroraPage

    Public Const BookingReferenceParamName As String = "BookingReference"
    Public Const NoteIdParamName As String = "NoteId"

    Public Const VIEWSTATE_BOOREF As String = "ViewState_Boo_Ref_Veh_Asgn"

    Public Property BookingReference() As String
        Get
            Return CStr(ViewState(VIEWSTATE_BOOREF))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_BOOREF) = value
        End Set
    End Property

    Public Property FromScheduleViewer() As Boolean
        Get
            Return CBool(ViewState("Viewstate_Veh_Asn_FromSchVwr"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("Viewstate_Veh_Asn_FromSchVwr") = value
        End Set
    End Property

    Dim _rowIndex As Integer = 0
    Dim _keepNoteFields As Boolean = False

    Protected Sub vehicleAssignmentRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles vehicleAssignmentRepeater.ItemDataBound
        If Not e.Item.ItemType = ListItemType.Item AndAlso Not e.Item.ItemType = ListItemType.AlternatingItem Then Return

        Dim dataRow As DataRowView = e.Item.DataItem
        If String.IsNullOrEmpty("" & dataRow("Id")) Then Return

        Dim listTableRow As HtmlTableRow = e.Item.FindControl("listTableRow")
        Dim listEditButton As LinkButton = e.Item.FindControl("listEditButton")
        Dim listDescriptionLabel As Label = e.Item.FindControl("listDescriptionLabel")
        Dim listPriorityLabel As Label = e.Item.FindControl("listPriorityLabel")
        Dim listAudienceTypeLabel As Label = e.Item.FindControl("listAudienceTypeLabel")
        Dim listActiveLabel As Label = e.Item.FindControl("listActiveLabel")
        'Dim listHyperLink As HyperLink = e.Item.FindControl("listHyperLink")

        ' <Note><Id/><Desc/><AudTypId>DD3B7A61-EA5D-4314-9923-ECAC0F07533A</AudTypId><AudTyp></AudTyp><Pri/><Act>1</Act><Int/></Note>

        listTableRow.Attributes.Add("class", IIf(_rowIndex Mod 2 = 0, "evenRow", "oddRow")) : _rowIndex += 1
        listEditButton.Visible = True
        listEditButton.CommandArgument = "" & dataRow("Id")
        listDescriptionLabel.Text = Server.HtmlEncode("" & dataRow("Desc"))
        listPriorityLabel.Text = Server.HtmlEncode("" & dataRow("Pri"))
        listAudienceTypeLabel.Text = Server.HtmlEncode("" & dataRow("AudTyp"))
        listActiveLabel.Text = IIf(("" & dataRow("Act")) = "1", "Yes", "No")

        If noteIdHiddenField.Value = ("" & dataRow("Id")) Then
            listTableRow.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(DataConstants.CurrentColor)) : _rowIndex += 1
            noteLabel.Text = "Edit Note"
            integrityNoHiddenField.Value = "" & dataRow("Int")
            noteIdHiddenField.Value = "" & dataRow("Id")

            If Not _keepNoteFields Then
                Try
                    priorityDropDown.SelectedValue = "" & dataRow("Pri")
                Catch
                    priorityDropDown.SelectedIndex = 0
                End Try
                Try
                    audienceDropDown.SelectedValue = "" & dataRow("AudTyp")
                Catch
                    audienceDropDown.SelectedIndex = 0
                End Try
                activeCheckBox.Checked = ("" & dataRow("Act")) = "1"
                descriptionTextBox.Text = "" & dataRow("Desc")
            End If
        End If
    End Sub

    Sub BuildNotes()
        Try
            searchBookingRentalNoTextBox.Text = BookingReference
            selectedBookingPanel.Style.Item("display") = "none"
            If String.IsNullOrEmpty(BookingReference) Then Return

            Dim result As XmlDocument = Aurora.OpsAndLogs.Services.VehicleAssign.GetVehicleAssetsGrid(BookingReference, UserCode)
            If result.DocumentElement.SelectSingleNode("Error/ErrDesc") IsNot Nothing Then
                Me.SetErrorShortMessage(result.DocumentElement.SelectSingleNode("Error/ErrDesc").InnerText)
                Return
            End If

            If result.DocumentElement.SelectSingleNode("Note/Id").InnerText = "" Then
                result.LoadXml("<Note><Id/><Desc/><AudTypId/><AudTyp/><Pri/><Active/><Act>1</Act><Int/></Note>")
            End If

            selectedBookingPanel.Style.Item("display") = "block"

            bookingRentalNoTextBox.Text = BookingReference

            If Not _keepNoteFields Then
                noteLabel.Text = "New Note"
                integrityNoHiddenField.Value = "1"
                priorityDropDown.SelectedIndex = 0
                audienceDropDown.SelectedIndex = 0
                activeCheckBox.Checked = True
                descriptionTextBox.Text = ""
            End If

            ' <Note><Id/><Desc/><AudTypId>DD3B7A61-EA5D-4314-9923-ECAC0F07533A</AudTypId><AudTyp></AudTyp><Pri/><Act>1</Act><Int/></Note>
            Dim vehiclesDateSet As New DataSet
            vehiclesDateSet.ReadXml(New XmlNodeReader(result))
            vehicleAssignmentRepeater.DataSource = vehiclesDateSet.Tables(0)
            vehicleAssignmentRepeater.DataBind()

            If FromScheduleViewer Then
                ' "CLOSE" activated!
                If Not String.IsNullOrEmpty(btnClose.Style.Item("display")) Then
                    btnClose.Style.Remove("display")
                End If
                If String.IsNullOrEmpty(btnClose.Attributes("onclick")) Then
                    btnClose.Attributes.Add("onclick", "window.close();")
                End If
                cancelButton.Visible = False
                undoButton.Visible = False
                If String.IsNullOrEmpty(divInitial.Style.Item("display")) Or divInitial.Style("display") = "block" Then
                    divInitial.Style("display") = "none"
                End If

            End If
        Catch ex As Exception
            Me.AddErrorMessage(ex.Message)
        End Try
    End Sub

    'Sub Page_PreInit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreInit
    '    If Not IsPostBack Then
    '        Dim sBookingText As String
    '        sBookingText = Request("BookingText")
    '        If Not String.IsNullOrEmpty(sBookingText) Then
    '            ' do something!
    '            MasterPageFile = "/Web/Include/PopupHeader.master"
    '            BookingReference = sBookingText.Split(" "c)(0)
    '            FromScheduleViewer = True
    '        End If
    '    End If
    'End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            Dim sBookingText As String
            sBookingText = Request("BookingText")
            If Not String.IsNullOrEmpty(sBookingText) Then
                FromScheduleViewer = True
            End If
            LoadFreshPage()
            If Not String.IsNullOrEmpty(sBookingText) Then
                Me.searchBookingRentalNoTextBox.Text = sBookingText.Split(" "c)(0)
            End If
        End If
    End Sub

    Sub LoadFreshPage()

        Dim audienceDataSet As DataSet = Aurora.Common.Data.GetCodecodetype("26", "")
        audienceDropDown.DataTextField = "CODE"
        audienceDropDown.DataValueField = "CODE"
        audienceDropDown.DataSource = audienceDataSet.Tables(0)
        audienceDropDown.DataBind()

        BuildNotes()
    End Sub

    Protected Sub showDetailsButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles showDetailsButton.Click
        ' initial check
        If Not searchBookingRentalNoTextBox.IsTextValid Then
            SetWarningShortMessage(searchBookingRentalNoTextBox.ErrorMessage)
            Exit Sub
        End If

        BookingReference = searchBookingRentalNoTextBox.Text.Trim
        BuildNotes()
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        vehicleTextBox.Text = ""
        noteIdHiddenField.Value = ""
        ' clear boo ref
        BookingReference = String.Empty
        LoadFreshPage()
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        If String.IsNullOrEmpty(descriptionTextBox.Text.Trim) Then
            SetWarningShortMessage("Description has to be entered")
            Exit Sub
        End If

        If String.IsNullOrEmpty(vehicleTextBox.Text.Trim()) Then
            SetWarningShortMessage("Vehicle code has to be entered")
            Exit Sub
        End If

        If String.IsNullOrEmpty(integrityNoHiddenField.Value.Trim()) Then
            integrityNoHiddenField.Value = "1"
        End If

        Dim result As XmlDocument = Aurora.OpsAndLogs.Services.VehicleAssign.SaveVehicleAssignData( _
            BookingReference.Split("/"c)(0), _
            BookingReference.Split("/"c)(1), _
            vehicleTextBox.Text.Trim(), _
            noteIdHiddenField.Value, _
            descriptionTextBox.Text, _
            CInt(priorityDropDown.SelectedValue), _
            audienceDropDown.SelectedValue, _
            activeCheckBox.Checked, _
            CInt(integrityNoHiddenField.Value), _
            UserCode)
        If result.DocumentElement.SelectSingleNode("Error/Message") IsNot Nothing Then
            AddErrorMessage(result.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("ERROR/", ""))
        Else
            SetInformationMessage(GetMessage("GEN046"))
        End If

        _keepNoteFields = True
        BuildNotes()
    End Sub

    Protected Sub undoButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles undoButton.Click
        Dim result As XmlDocument = Aurora.OpsAndLogs.Services.VehicleAssign.UndoVehicleAssign( _
            BookingReference.Split("/"c)(0), _
            BookingReference.Split("/"c)(1), _
            UserCode)
        If result.DocumentElement.SelectSingleNode("Error/Message") IsNot Nothing Then
            AddErrorMessage(result.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("ERROR/", ""))
        Else
            SetInformationMessage(GetMessage("GEN046"))
        End If

        vehicleTextBox.Text = ""
        noteIdHiddenField.Value = ""
        BuildNotes()
    End Sub

    Protected Sub newNoteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newNoteButton.Click
        noteIdHiddenField.Value = ""
        _keepNoteFields = False
        BuildNotes()
    End Sub

    Protected Sub listEditButton_Command(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.CommandEventArgs)
        noteIdHiddenField.Value = e.CommandArgument
        _keepNoteFields = False
        BuildNotes()
    End Sub
End Class
