﻿Imports System.Data
Imports Aurora.OpsAndLogs.Data
Imports Aurora.Common

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PPF)> _
Partial Class OpsAndLogs_PPFmgt
    Inherits AuroraPage


#Region "Properties"
    Dim FirstLoad As Boolean
    Dim sessionDS As DataSet

    
    Private _EffectiveDate As DateTime
    Private Property EffectiveDate As DateTime
        Get
            Return _EffectiveDate
        End Get
        Set(ByVal value As DateTime)
            _EffectiveDate = value
        End Set
    End Property

    

#End Region


#Region "Pages events"

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        PetrolTextbox.Attributes.Add("onkeyup", "IsValidSized(this);")
        ''PetrolTextbox.Attributes.Add("onmouseup", "IsValidSized(this);")
        PetrolTextbox.Attributes.Add("onblur", "CurrencyFormatted(this);")

        DieselTextBox.Attributes.Add("onkeyup", "IsValidSized(this);")
        ''DieselTextBox.Attributes.Add("onmouseup", "IsValidSized(this);")
        DieselTextBox.Attributes.Add("onblur", "CurrencyFormatted(this);")

        '----Mod: Raj, Sept 6, 2012-----------------------------------------

        Pre_PetrolTextbox.Attributes.Add("onkeyup", "IsValidSized(this);")
        ''PetrolTextbox.Attributes.Add("onmouseup", "IsValidSized(this);")
        Pre_PetrolTextbox.Attributes.Add("onblur", "CurrencyFormatted(this);")

        Pre_DieselTextBox.Attributes.Add("onkeyup", "IsValidSized(this);")
        ''DieselTextBox.Attributes.Add("onmouseup", "IsValidSized(this);")
        Pre_DieselTextBox.Attributes.Add("onblur", "CurrencyFormatted(this);")

        Re_PetrolTextbox.Attributes.Add("onkeyup", "IsValidSized(this);")
        ''PetrolTextbox.Attributes.Add("onmouseup", "IsValidSized(this);")
        Re_PetrolTextbox.Attributes.Add("onblur", "CurrencyFormatted(this);")

        Re_DieselTextBox.Attributes.Add("onkeyup", "IsValidSized(this);")
        ''DieselTextBox.Attributes.Add("onmouseup", "IsValidSized(this);")
        Re_DieselTextBox.Attributes.Add("onblur", "CurrencyFormatted(this);")


        '----Mod: Raj, Sept 6, 2012-----------------------------------------


    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            FirstLoad = True
            InitCountriesBrandAndVehicleTypes()
            ControlsStatus(False)
        End If
    End Sub


    Protected Sub OpsAndLogs_PPFmgt_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Me.newButton.Enabled = ButtonForPPFUsers()
        Me.SaveButton.Enabled = ButtonForPPFUsers()

        Colorcoding()
    End Sub

#End Region

#Region "Procedures"

    Private Sub InitCountriesBrandAndVehicleTypes()
        Try
            Dim country As String = IIf(String.IsNullOrEmpty(countryDropdown.SelectedValue), Me.CountryCode, Me.countryDropdown.SelectedValue)
            Dim ds As DataSet = GetPPF_WEbGetLocationsAndBrands(Me.UserCode, country)
            Me.countryDropdown.Items.Clear()

            Me.countryDropdown.DataSource = ds.Tables(0).DefaultView
            Me.countryDropdown.DataTextField = "Countries"
            Me.countryDropdown.DataValueField = "CtyCode"
            Me.countryDropdown.AutoPostBack = True
            Me.countryDropdown.DataBind()
            Me.countryDropdown.SelectedValue = Me.CountryCode


            Me.FuelDropDown.Items.Clear()
            Me.FuelDropDown.Items.Add("(Select)")
            Me.FuelDropDown.AppendDataBoundItems = True
            Me.FuelDropDown.DataSource = ds.Tables(2).DefaultView
            Me.FuelDropDown.AutoPostBack = True
            Me.FuelDropDown.DataTextField = "LocName"
            Me.FuelDropDown.DataValueField = "LocCode"
            Me.FuelDropDown.DataBind()


        Catch ex As Exception
            SetErrorShortMessage("Error: Loading Countries and Location")
        End Try

    End Sub

    Sub GetLatestCalculationResult(Optional ByVal isFirstLoad As Boolean = False)
        If String.IsNullOrEmpty(Me.dateDropDown.SelectedValue) Then Exit Sub
        Dim rateId As Long = Me.dateDropDown.SelectedValue
        Dim resultDS As DataSet = PPF_GetLatestFuelPriceCalculation(rateId)
        BuildTable(resultDS)
    End Sub

    Sub ControlsStatus(ByVal status As Boolean)
        Me.DieselTextBox.Enabled = status
        Me.PetrolTextbox.Enabled = status
        Me.SaveButton.Enabled = status

        Me.Pre_DieselTextBox.Enabled = False
        Me.Pre_PetrolTextbox.Enabled = False

        Me.Re_DieselTextBox.Enabled = False
        Me.Re_PetrolTextbox.Enabled = False
 
    End Sub

    Sub BuildTable(ByVal result As DataSet)

        Dim brandName As String = ""
        Dim brandNameCurrent As String = ""

        Try

            If (result.Tables(0).Rows.Count - 1 <> -1) Then
                For Each row As DataRow In result.Tables(0).Rows

                    Dim brandRow As New TableRow
                    Dim brandCellHeader As New TableCell
                    brandCellHeader.ColumnSpan = 5
                    brandCellHeader.HorizontalAlign = HorizontalAlign.Center


                    If String.IsNullOrEmpty(brandName) And String.IsNullOrEmpty(brandNameCurrent) Then

                        brandName = Server.HtmlEncode(row("PrdBrdCode"))
                        PPFTable.Rows.Add(brandRow)

                        brandCellHeader.Text = FullBrandsConversion(brandName)
                        brandRow.Cells.Add(brandCellHeader)
                        brandName = Server.HtmlEncode(row("PrdBrdCode"))
                        brandNameCurrent = brandName
                        brandRow.BackColor = BrandColor(brandName)
                    Else
                        brandNameCurrent = Server.HtmlEncode(row("PrdBrdCode"))
                        If (brandName <> brandNameCurrent) Then
                            PPFTable.Rows.Add(brandRow)
                            brandCellHeader.Text = FullBrandsConversion(brandNameCurrent)
                            brandRow.Cells.Add(brandCellHeader)
                            brandName = brandNameCurrent
                            brandRow.BackColor = BrandColor(brandName)
                        End If
                    End If

                    Dim tableRow As New TableRow
                    PPFTable.Rows.Add(tableRow)
                    tableRow.BackColor = Drawing.Color.White

                    Dim codeCell As New TableCell
                    codeCell.Text = Server.HtmlEncode(row("PrdShortName"))
                    tableRow.Cells.Add(codeCell)

                    Dim typeCell As New TableCell
                    typeCell.Text = Server.HtmlEncode(row("PrdName"))
                    tableRow.Cells.Add(typeCell)

                    Dim fuelCell As New TableCell
                    fuelCell.Text = Server.HtmlEncode(row("PrdFuelType").ToString.ToLower)
                    tableRow.Cells.Add(fuelCell)

                    Dim tankCell As New TableCell
                    tankCell.Text = Server.HtmlEncode(row("PrdTankSize"))
                    tableRow.Cells.Add(tankCell)

                    Dim PriceCell As New TableCell
                    ''PriceCell.ToolTip = "Fuel Cost for  '" & row("LocFuRateLocCode").ToString & "' location"
                    Dim priceInDec As Decimal = CDec(row("LocPrdRateTotal").ToString)
                    PriceCell.Text = Server.HtmlEncode(priceInDec.ToString("C2"))
                    tableRow.Cells.Add(PriceCell)

                Next
            End If

            If (result.Tables(1).Rows.Count - 1 <> -1) Then
                For Each fuelrow As DataRow In result.Tables(1).Rows
                    Dim price As Decimal = CDec(fuelrow("LocFuRate"))
                    If fuelrow("CodCode") = "Petrol" Then
                        Me.PetrolTextbox.Text = price.ToString("c2")
                    ElseIf fuelrow("CodCode") = "Diesel" Then
                        Me.DieselTextBox.Text = price.ToString("c2")
                    Else

                    End If
                Next
            End If


            '---- Mod:Raj, Sept 6, 2012 -------------------------------------------------------------------------------'

            ' Update fuel prices on the basis of change type for each country.

            ' Charge type can be a dollar ($) value or a percentage (%) can can be determined from the UniversalInfo 
            ' table by reading the values from the keys NZ_FuelChgType or AU_FuelChgType for the 2 countries.

            

            'Dim foundRows() As System.Data.DataRow
            'Dim petrolDisc As Decimal = 0.0
            'Dim dieselDisc As Decimal = 0.0

            'foundRows = result.Tables(0).Select("PrdFuelType = 'Petrol'")

            'For Each dr As DataRow In foundRows
            '    petrolDisc = CDec(dr("LocPrdRateTotal") / dr("PrdTankSize"))
            '    Exit For
            'Next

            'foundRows = result.Tables(0).Select("PrdFuelType = 'Diesel'")

            'For Each dr As DataRow In foundRows
            '    dieselDisc = CDec(dr("LocPrdRateTotal") / dr("PrdTankSize"))
            '    Exit For
            'Next


            Dim pumpPetrol As Decimal = CDec(Me.PetrolTextbox.Text)
            Dim pumpDiesel As Decimal = CDec(Me.DieselTextBox.Text)
            Dim altPrice As Decimal = 0.0



            Dim NZ_PrepaidFuelChgType As String = ""
            Dim NZ_PrepaidFuelVariance As String = ""

            Dim NZ_RefuelFuelChgType As String = ""
            Dim NZ_RefuelFuelVariance As String = ""


            Dim AU_PrepaidFuelChgType As String = ""
            Dim AU_PrepaidFuelVariance As String = ""

            Dim AU_RefuelFuelChgType As String = ""
            Dim AU_RefuelFuelVariance As String = ""



            If (result.Tables(2).Rows.Count - 1 <> -1) Then
                NZ_PrepaidFuelChgType = result.Tables(2).Rows(0)(0)
            End If

            If (result.Tables(3).Rows.Count - 1 <> -1) Then
                NZ_PrepaidFuelVariance = result.Tables(3).Rows(0)(0)
            End If

            If (result.Tables(4).Rows.Count - 1 <> -1) Then
                NZ_RefuelFuelChgType = result.Tables(4).Rows(0)(0)
            End If

            If (result.Tables(5).Rows.Count - 1 <> -1) Then
                NZ_RefuelFuelVariance = result.Tables(5).Rows(0)(0)
            End If

            If (result.Tables(6).Rows.Count - 1 <> -1) Then
                AU_PrepaidFuelChgType = result.Tables(6).Rows(0)(0)
            End If

            If (result.Tables(7).Rows.Count - 1 <> -1) Then
                AU_PrepaidFuelVariance = result.Tables(7).Rows(0)(0)
            End If

            If (result.Tables(8).Rows.Count - 1 <> -1) Then
                AU_RefuelFuelChgType = result.Tables(8).Rows(0)(0)
            End If

            If (result.Tables(9).Rows.Count - 1 <> -1) Then
                AU_RefuelFuelVariance = result.Tables(9).Rows(0)(0)
            End If

            'Country: NZ
            If (countryDropdown.SelectedValue.ToString().ToUpper = "NZ") Then

                If (NZ_PrepaidFuelChgType = "%") Then
                    'Petrol
                    altPrice = pumpPetrol - (NZ_PrepaidFuelVariance * pumpPetrol)
                    Pre_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel - (NZ_PrepaidFuelVariance * pumpDiesel)
                    Pre_DieselTextBox.Text = altPrice.ToString("c2")
                End If

                If (NZ_PrepaidFuelChgType = "$") Then
                    'Petrol
                    altPrice = pumpPetrol - NZ_PrepaidFuelVariance
                    Pre_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel - NZ_PrepaidFuelVariance
                    Pre_DieselTextBox.Text = altPrice.ToString("c2")
                End If

                If (NZ_RefuelFuelChgType = "%") Then
                    'Petrol
                    altPrice = pumpPetrol + (NZ_RefuelFuelVariance * pumpPetrol)
                    Re_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel + (NZ_RefuelFuelVariance * pumpDiesel)
                    Re_DieselTextBox.Text = altPrice.ToString("c2")
                End If

                If (NZ_RefuelFuelChgType = "$") Then
                    'Petrol
                    altPrice = pumpPetrol + NZ_RefuelFuelVariance
                    Re_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel + NZ_RefuelFuelVariance
                    Re_DieselTextBox.Text = altPrice.ToString("c2")
                End If
            End If

            'Country: AU
            If (countryDropdown.SelectedValue.ToString().ToUpper = "AU") Then

                If (AU_PrepaidFuelChgType = "%") Then
                    'Petrol
                    altPrice = pumpPetrol - (AU_PrepaidFuelVariance * pumpPetrol)
                    Pre_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel - (AU_PrepaidFuelVariance * pumpDiesel)
                    Pre_DieselTextBox.Text = altPrice.ToString("c2")
                End If

                If (AU_PrepaidFuelChgType = "$") Then
                    'Petrol
                    altPrice = pumpPetrol - AU_PrepaidFuelVariance
                    Pre_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel - AU_PrepaidFuelVariance
                    Pre_DieselTextBox.Text = altPrice.ToString("c2")
                End If

                If (AU_RefuelFuelChgType = "%") Then
                    'Petrol
                    altPrice = pumpPetrol + (AU_RefuelFuelVariance * pumpPetrol)
                    Re_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel + (AU_RefuelFuelVariance * pumpDiesel)
                    Re_DieselTextBox.Text = altPrice.ToString("c2")
                End If

                If (AU_RefuelFuelChgType = "$") Then
                    'Petrol
                    altPrice = pumpPetrol + AU_RefuelFuelVariance
                    Re_PetrolTextbox.Text = altPrice.ToString("c2")

                    'Diesel
                    altPrice = pumpDiesel + AU_RefuelFuelVariance
                    Re_DieselTextBox.Text = altPrice.ToString("c2")
                End If
            End If

          

            '---- Mod:Raj, Sept 6, 2012--------------------------------------------------------------------



        Catch ex As Exception
            SetWarningShortMessage("Error: Displaying fuel price calculations")
            Me.DieselTextBox.Text = ""
            Me.PetrolTextbox.Text = ""
        End Try
        

    End Sub

    Sub Colorcoding()
        If Me.dateDropDown.Items.Count - 1 = -1 Then Exit Sub
        For Each item As ListItem In Me.dateDropDown.Items
            If Not String.IsNullOrEmpty(item.Text) Then
                Dim dte As DateTime = CDate(item.Text)
                If Not dte.ToShortDateString.Equals(DateTime.Now.ToShortDateString) Then
                    item.Attributes.Add("style", "background-color: " + Drawing.ColorTranslator.ToHtml(DataConstants.PastColor)) ' past
                Else
                    item.Attributes.Add("style", "background-color: " + Drawing.ColorTranslator.ToHtml(DataConstants.CurrentColor)) ' now
                End If
            End If
        Next
    End Sub

#End Region

#Region "Function"

    Private Function FullBrandsConversion(ByVal brandname As String) As String

        'If (brandname = "M") Then
        '    brandname = "<b>MAUI</b>"
        'ElseIf brandname = "P" Then
        '    brandname = "<b>BACKPACKERS</b>"
        'ElseIf brandname = "X" Then
        '    brandname = "<b>EXPLOREMORE</b>"
        'ElseIf brandname = "B" Then
        '    brandname = "<b>BRITZ</b>"
        'ElseIf brandname = "T" Then
        '    brandname = "<b>THL Vehicles</b>"
        'End If

        Return String.Concat("<b>", PPF_GetTBrandName(brandname), "</b>")
    End Function

    Private Function BrandColor(ByVal brandname As String) As Drawing.Color
        If (brandname = "M") Then
            Return Drawing.Color.LightBlue
        ElseIf brandname = "P" Then
            Return Drawing.Color.LightSkyBlue
        ElseIf brandname = "X" Then
            Return Drawing.Color.LightGreen
        ElseIf brandname = "B" Then
            Return Drawing.Color.Orange
        ElseIf brandname = "T" Then
            Return Drawing.Color.GreenYellow
        ElseIf brandname = "Y" Then
            Return Drawing.Color.LightGray
        End If
    End Function

    Sub ClearControls()
        Me.PetrolTextbox.Text = ""
        Me.DieselTextBox.Text = ""
        Me.Pre_PetrolTextbox.Text = ""
        Me.Pre_DieselTextBox.Text = ""
        Me.Re_PetrolTextbox.Text = ""
        Me.Re_DieselTextBox.Text = ""
    End Sub

    Sub PopulateDates(ByVal location As String, Optional ByVal firstLoad As Boolean = True)
        ClearControls()
        Dim ds As DataSet = PPF_GetRatePerBranch(location)

        Me.dateDropDown.Items.Clear()
        Me.dateDropDown.Items.Add("")
        Me.dateDropDown.AppendDataBoundItems = True
        Me.dateDropDown.AutoPostBack = True

        For Each row As DataRow In ds.Tables(0).Rows
            Dim dte As DateTime = CDate(row("LocFuRateEffectiveDate").ToString)
            Dim li As New ListItem(dte.ToShortDateString, row("LocFuRateId").ToString)
            If Not dte.ToShortDateString.Equals(Now.ToShortDateString) Then
                li.Attributes.Add("style", "background-color: " + Drawing.ColorTranslator.ToHtml(DataConstants.PastColor)) ' past
            Else
                li.Attributes.Add("style", "background-color: " + Drawing.ColorTranslator.ToHtml(DataConstants.CurrentColor)) ' now
            End If
            Me.dateDropDown.Items.Add(li)
        Next

        If Not firstLoad Then
            Dim dr() As DataRow = ds.Tables(0).Select("LocFuRateId=MAX(LocFuRateId)")
            Me.dateDropDown.SelectedValue = dr(0)("LocFuRateId")
        End If
        
    End Sub

    Private Function CheckCSRRole(ByVal id As String, ByVal role As String) As String
        Dim sReturnMsg As String = "Error"
        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("dialog_checkUserForCSR", id, role)
        Dim oXmlDoc As New System.Xml.XmlDocument
        oXmlDoc.LoadXml(result)
        If Not oXmlDoc.DocumentElement.SelectSingleNode("/Error") Is Nothing Then
            sReturnMsg = oXmlDoc.DocumentElement.SelectSingleNode("/Error").InnerText
        Else
            If Not oXmlDoc.DocumentElement.SelectSingleNode("/isUser") Is Nothing Then
                If CBool(oXmlDoc.DocumentElement.SelectSingleNode("/isUser").InnerText) Then
                    sReturnMsg = "True"
                End If
            End If
        End If
        Return sReturnMsg
    End Function

    Function ButtonForPPFUsers() As Boolean

        Dim sreturnmsg As String = CheckCSRRole(Me.UserCode, "PPFMGT")
        If (sreturnmsg.Equals("True")) Then
            Return True
        End If
        Return False
    End Function

#End Region


#Region "Controls Events"

    Protected Sub FuelDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FuelDropDown.SelectedIndexChanged
        PopulateDates(FuelDropDown.SelectedValue)
    End Sub

    Protected Sub dateDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dateDropDown.SelectedIndexChanged
        ClearControls()
        If String.IsNullOrEmpty(Me.dateDropDown.SelectedValue) Then Me.SaveButton.Enabled = True
        If (Me.dateDropDown.SelectedValue.ToString = "-1") Then Me.SaveButton.Enabled = True

        
        ControlsStatus(False)
        GetLatestCalculationResult()

    End Sub

    Protected Sub countryDropdown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles countryDropdown.SelectedIndexChanged

        ClearControls()
        If countryDropdown.SelectedValue Is Nothing Then Return
        Me.dateDropDown.SelectedIndex = -1
        Dim country As String = countryDropdown.SelectedValue
        If String.IsNullOrEmpty(country) Then country = Me.CountryCode
        Dim ds As DataSet = GetPPF_WEbGetLocationsAndBrands(Me.UserCode, country)

        Me.FuelDropDown.Items.Clear()
        Me.FuelDropDown.Items.Add("(Select)")
        Me.FuelDropDown.AppendDataBoundItems = True
        Me.FuelDropDown.DataSource = ds.Tables(2).DefaultView
        Me.FuelDropDown.AutoPostBack = True
        Me.FuelDropDown.DataTextField = "LocName"
        Me.FuelDropDown.DataValueField = "LocCode"
        Me.FuelDropDown.DataBind()

        Me.dateDropDown.Items.Clear()
    End Sub

    Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newButton.Click
        If ButtonForPPFUsers() = False Then
            SetInformationShortMessage("You don't have permission to create new Fuel Rate ")
            ClearControls()
            ControlsStatus(True)
            Exit Sub
        End If


        EffectiveDate = DateTime.Now.ToShortDateString
        Dim item As ListItem = Me.dateDropDown.Items.FindByText(EffectiveDate)
        If item Is Nothing Then
            Me.dateDropDown.Items.Add(New ListItem(EffectiveDate, "-1"))
            Me.dateDropDown.SelectedIndex = Me.dateDropDown.Items.Count - 1
        Else
            Me.dateDropDown.SelectedValue = item.Value
        End If

        ClearControls()
        ControlsStatus(True)

    End Sub

    Protected Sub SaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles SaveButton.Click
        If ButtonForPPFUsers() = False Then
            SetInformationShortMessage("You don't have permission to save new Fuel Rate ")
            Exit Sub
        End If

        If (String.IsNullOrEmpty(Me.DieselTextBox.Text)) Or _
            (String.IsNullOrEmpty(Me.PetrolTextbox.Text)) Or _
            (String.IsNullOrEmpty(Me.FuelDropDown.SelectedValue)) Or _
            (String.IsNullOrEmpty(EffectiveDate)) Then
            SetWarningShortMessage("There are fields that have no values. Please try again.")
            Exit Sub
        End If
        Dim result As String = PPF_FuelPriceCalculation(Me.FuelDropDown.SelectedValue, _
                                                        EffectiveDate, _
                                                        Me.PetrolTextbox.Text, _
                                                        Me.DieselTextBox.Text, _
                                                        UserCode, _
                                                        Me.countryDropdown.SelectedValue)

        If result.Equals("OK") = True Then
            PopulateDates(FuelDropDown.SelectedValue, False)
            GetLatestCalculationResult()
            ControlsStatus(False)
        End If

    End Sub




#End Region




    
  
End Class
