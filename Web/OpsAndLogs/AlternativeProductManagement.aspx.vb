'' Change Log
'' 27/5/8   - Saving without making any changes no longer hits the database

Imports System.Data
Imports System.Xml
Imports Aurora.OpsAndLogs.Services
Imports Aurora.Common

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.AlternativeProductList)> _
Partial Class OpsAndLogs_AlternativeProductManagement
    Inherits AuroraPage

    Public Const VIEWSTATE_COUNTRY As String = "ViewState_APL_CountryCode"
    Public Const VIEWSTATE_PRODUCTID As String = "ViewState_APL_ProductId"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then

            ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "ChangeTitle('Maintain Alternative Products','" & Me.Page.Master.FindControl("titlelabel").ClientID & "');", True)
            Dim sCountryCode, sProductId As String

            sCountryCode = CStr(Request("CountryCode"))
            ViewState(VIEWSTATE_COUNTRY) = sCountryCode

            sProductId = CStr(Request("ProductId"))
            ViewState(VIEWSTATE_PRODUCTID) = sProductId

            btnBack.Attributes.Add("OnClick", "window.location='AlternativeProductList.aspx?CountryCode=" & sCountryCode & "';return false;")
            btnBack1.Attributes.Add("OnClick", "window.location='AlternativeProductList.aspx?CountryCode=" & sCountryCode & "';return false;")

            LoadTables(sCountryCode, sProductId)
        End If
    End Sub


    Sub LoadTables(ByVal Country As String, ByVal ProductId As String)

        Dim xmlReturn As XmlDocument
        xmlReturn = Aurora.OpsAndLogs.Services.AlternativeProduct.GetAlternativeProducts(Country, ProductId)

        ' check for errors
        If xmlReturn.DocumentElement.SelectSingleNode("Root/Country") Is Nothing Then
            SetErrorShortMessage("There is no data for the selected country")
            'rptMain.DataSource = New DataTable
            'rptMain.DataBind()
            Exit Sub
        End If

        If xmlReturn.DocumentElement.SelectSingleNode("Root/ReqPrd") Is Nothing Then
            SetErrorShortMessage("There is no data for the selected product")
            'rptMain.DataSource = New DataTable
            'rptMain.DataBind()
            Exit Sub
        End If

        'populate text boxes on the top
        txtCountryOfOperation.Text = xmlReturn.DocumentElement.SelectSingleNode("Root/Country").Attributes("Name").InnerText
        txtRequestedProduct.Text = xmlReturn.DocumentElement.SelectSingleNode("Root/ReqPrd").Attributes("Desc").InnerText

        ' split the xmls
        'check for empty row
        If xmlReturn.DocumentElement.SelectSingleNode("Root/CuttenrAltProducts").InnerXml = "<AltPrd><PrdId></PrdId><Product></Product><Remove></Remove></AltPrd>" Then
            xmlReturn.DocumentElement.SelectSingleNode("Root/CuttenrAltProducts").InnerXml = "<AltPrd><PrdId></PrdId><Product></Product><Remove>0</Remove></AltPrd>"
        End If
        Dim dstCurrentAlternativeProducts As New DataSet
        dstCurrentAlternativeProducts.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Root/CuttenrAltProducts")))
        If dstCurrentAlternativeProducts.Tables.Count > 0 Then
            rptCurrentAltProds.DataSource = dstCurrentAlternativeProducts.Tables(0)
        Else
            rptCurrentAltProds.DataSource = New DataTable
        End If

        rptCurrentAltProds.DataBind()

        ' next table
        Dim dstAvlblAlternativeProducts As New DataSet
        'check for empty row
        If xmlReturn.DocumentElement.SelectSingleNode("Root/AvlbleAltProducts").InnerXml = "<AvlPrd><PrdId></PrdId><PrdDesc></PrdDesc><Add></Add><Dummy1></Dummy1><Dummy2></Dummy2></AvlPrd>" Then
            xmlReturn.DocumentElement.SelectSingleNode("Root/AvlbleAltProducts").InnerXml = "<AvlPrd><PrdId></PrdId><PrdDesc></PrdDesc><Add>0</Add><Dummy1></Dummy1><Dummy2></Dummy2></AvlPrd>"

        End If

        dstAvlblAlternativeProducts.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Root/AvlbleAltProducts")))
        If dstAvlblAlternativeProducts.Tables.Count > 0 Then
            rptAvailableAltProds.DataSource = dstAvlblAlternativeProducts.Tables(0)
        Else
            rptAvailableAltProds.DataSource = New DataTable
        End If

        rptAvailableAltProds.DataBind()


    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave1.Click
        ' build the xml
        Dim xmlToSave, xmlReturn As XmlDocument
        xmlToSave = BuildXMLToSave()


        ' test if anything has been modified

        Dim bSomethingToAdd, bSomethingToDelete As Boolean
        bSomethingToAdd = False
        bSomethingToDelete = False

        For Each oNode As XmlNode In xmlToSave.DocumentElement.SelectNodes("CuttenrAltProducts/AltPrd")
            If oNode.SelectSingleNode("Remove").InnerText = "1" Then
                bSomethingToDelete = True
                Exit For
            End If
        Next

        If Not bSomethingToDelete Then
            For Each oNode As XmlNode In xmlToSave.DocumentElement.SelectNodes("AvlbleAltProducts/AvlPrd")
                If oNode.SelectSingleNode("Add").InnerText = "1" Then
                    bSomethingToAdd = True
                    Exit For
                End If
            Next
        End If

        If (Not bSomethingToAdd) And (Not bSomethingToDelete) Then
            ' no changes!
            SetInformationShortMessage("No changes have been made")
            Exit Sub
        End If

        ' test if anything has been modified

        xmlReturn = Aurora.OpsAndLogs.Services.AlternativeProduct.UpdateAlternativeProducts(xmlToSave)
        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' refresh the data
            LoadTables(CStr(ViewState(VIEWSTATE_COUNTRY)), CStr(ViewState(VIEWSTATE_PRODUCTID)))
        Else
            ' theres an error!
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If

    End Sub

    Function BuildXMLToSave() As XmlDocument
        Dim sbXMLForSaving As New StringBuilder

        sbXMLForSaving.Append("<Root><Country Code=""" & CStr(ViewState(VIEWSTATE_COUNTRY)) & """ />")
        sbXMLForSaving.Append("<ReqPrd PriPrdId=""" & CStr(ViewState(VIEWSTATE_PRODUCTID)) & """ />")

        ' stuff that might be getting deleted
        sbXMLForSaving.Append("<CuttenrAltProducts>")

        For Each oItem As RepeaterItem In rptCurrentAltProds.Items

            sbXMLForSaving.Append("<AltPrd>")

            sbXMLForSaving.Append("<PrdId>" & CType(oItem.FindControl("hdnID"), HiddenField).Value & "</PrdId>")
            If CType(oItem.FindControl("chkRemove"), CheckBox).Checked Then
                sbXMLForSaving.Append("<Remove>1</Remove>")
            Else
                sbXMLForSaving.Append("<Remove>0</Remove>")
            End If

            sbXMLForSaving.Append("</AltPrd>")
        Next

        sbXMLForSaving.Append("</CuttenrAltProducts>")

        ' stuff that might be getting added
        sbXMLForSaving.Append("<AvlbleAltProducts>")

        For Each oItem As RepeaterItem In rptAvailableAltProds.Items

            sbXMLForSaving.Append("<AvlPrd>")

            sbXMLForSaving.Append("<PrdId>" & CType(oItem.FindControl("hdnID"), HiddenField).Value & "</PrdId>")
            If CType(oItem.FindControl("chkAdd"), CheckBox).Checked Then
                sbXMLForSaving.Append("<Add>1</Add>")
            Else
                sbXMLForSaving.Append("<Add>0</Add>")
            End If

            sbXMLForSaving.Append("</AvlPrd>")
        Next

        sbXMLForSaving.Append("</AvlbleAltProducts>")

        sbXMLForSaving.Append("<UsrId>" & UserCode & "</UsrId><PrgmName>AlternativeProductManagement.aspx</PrgmName></Root>")

        Dim xmlReturn As New XmlDocument
        xmlReturn.LoadXml(sbXMLForSaving.ToString())

        Return xmlReturn
    End Function

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel1.Click
        ' refresh the data
        LoadTables(CStr(ViewState(VIEWSTATE_COUNTRY)), CStr(ViewState(VIEWSTATE_PRODUCTID)))
    End Sub
End Class
