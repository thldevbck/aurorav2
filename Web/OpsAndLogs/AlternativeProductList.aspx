<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	AlternativeProductList.aspx
''	Date Created	-	24.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	24.4.8 / Base version
''
''
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="AlternativeProductList.aspx.vb" Inherits="OpsAndLogs_AlternativeProductList"
    Title="Alternative Product List" %>

<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
<script type="text/javascript" language="javascript">

function ManageAlternativeProduct(CountryCode, ProductId)
{
    window.location  = "AlternativeProductManagement.aspx?CountryCode=" + CountryCode + "&ProductId=" + ProductId;
    
    return false;
}

</script>
</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td width="150px" >
                Country Of Operation:
            </td>
            <td width="250px" >
                <asp:TextBox ID="txtCountryOfOperation" runat="server" ReadOnly="true" CssClass="Textbox_Standard"></asp:TextBox>
            </td>
            <td width="400px">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Repeater ID="rptMain" runat="server" EnableViewState="true">
                    <HeaderTemplate>
                        <table class="dataTable" width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <th width="250px">
                                    <b>Requested Product</b>
                                </th>
                                <th width="*">
                                    <b>Alternative Products</b>
                                </th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr class="evenRow">
                            <td>
                                <%--<a href="#a" onclick="navigateScreen()"><span datafld="PrdDesc"></span></a>--%>
                                <asp:HiddenField ID="hdnID" runat="server" Value='<%#Bind("PrdId")%>' />
                                <asp:LinkButton ID="lnkProduct" runat="server" Text='<%#Bind("PrdDesc")%>'></asp:LinkButton>
                            </td>
                            <td>
                                <%--<span datafld="AltPrdList"></span>--%>
                                <%#Container.DataItem("AltPrdList")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <AlternatingItemTemplate>
                        <tr class="oddRow">
                            <td>
                                <%--<a href="#a" onclick="navigateScreen()"><span datafld="PrdDesc"></span></a>--%>
                                <asp:HiddenField ID="hdnID" runat="server" Value='<%#Bind("PrdId")%>' />
                                <asp:LinkButton ID="lnkProduct" runat="server" Text='<%#Bind("PrdDesc")%>' ></asp:LinkButton>
                            </td>
                            <td>
                                <%--<span datafld="AltPrdList"></span>--%>
                                <%#Container.DataItem("AltPrdList")%>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                    <FooterTemplate>
                        </TABLE>
                    </FooterTemplate>
                </asp:Repeater>
            </td>
        </tr>
    </table>
</asp:Content>
