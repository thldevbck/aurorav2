<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="SubstitutionRuleMgt.aspx.vb" Inherits="OpsAndLogs_SubstitutionRuleMgt"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl"
    TagPrefix="uc3" %>

<%@ Register Src="~/UserControls/MultiSelectControl/MultiSelect.ascx" TagName="MultiSelect" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
<style type="text/css">
    .subRuleFromLoc {padding:0px 0px 0px 20px;}
    .subRuleFromLoc span img{float: right; margin:0px 10px 0px 0px;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript">


     
    function checkUncheckCheckboxes(CheckBox)
    {
    
        var TargetBaseControl = document.getElementById('<%= productGridView.ClientID %>');
        
        //get target base & child control.
        var TargetChildControl = "allCheckBox";

        //get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");  

        for(var n = 0; n < Inputs.length; ++n)
        {
            var id = Inputs[n].id
            if(Inputs[n].type == 'checkbox' && id.indexOf("checkedCheckBox") > -1)
                Inputs[n].checked = CheckBox.checked;
        }
    }
    
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="780">
        <tr>
            <td width="150">
                Country Of Operation:
            </td>
            <td width="250">
<%--                <asp:Label ID="countryLabel" runat="server" Font-Bold="True"></asp:Label>--%>
                <asp:TextBox ID="countryTextBox" runat="server" ReadOnly=true></asp:TextBox>
            </td>
            <td width="150">
                Bookable Product:
            </td>
            <td>
<%--                <asp:Label ID="productLabel" runat="server" Font-Bold="True"></asp:Label>--%>
                <asp:TextBox ID="productTextBox" runat="server" ReadOnly=true Width="220" ></asp:TextBox>
                <uc1:PickerControl ID="productPickerControl" runat="server" PopupType="PRODUCT4SUBSTITUTIONRULE"
                    AppendDescription="true" Width="220" />
            </td>
        </tr>
    </table>
    <table width="780">
        <tr width="100%">
            <td colspan="2" align="right">
                <br />
                <asp:Button ID="removeButton" runat="server" Text="remove" CssClass="Button_Standard Button_Remove" />
            </td>
        </tr>
        </table>
    <table width="130%">
        <tr>
            <td colspan="2">
                <asp:GridView ID="productGridView" runat="server" AutoGenerateColumns="false" CssClass="dataTable">
                    <AlternatingRowStyle CssClass="oddRow" />
                    <RowStyle CssClass="evenRow" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="subIdLabel" runat="server" Text='<%# Bind("SubId") %>'></asp:Label>
                                <asp:Label ID="srpIdLabel" runat="server" Text='<%# Bind("SrpId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="left">
                            <HeaderTemplate>
                                Fleet Model*
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc1:PickerControl ID="modelPickerControl" runat="server" PopupType="OPLOGGETFLEETMODEL"
                                    Width="65" DataId='<%# Bind("SrpFlmId") %>' Text='<%# eval("FlmDesc").Trim() %>' />
                                    <%--<asp:Label ID="modelID" Visible="false" runat="server" Text='<%# Bind("SrpFlmId") %>'></asp:Label>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100" ItemStyle-CssClass="subRuleFromLoc" HeaderStyle-CssClass="subRuleFromLoc">
                            <HeaderTemplate>
                                From Loc
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc1:MultiSelect ID="fromLoc" SelectType="Locations" DisplayKeys="true" Items='<%# GetMultiselectItems(eval("locations")) %>' CountryCode="<%#mCountryCode %>"  runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField >
                            <HeaderTemplate>
                                Excl<br /> from <br /> Loc
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkExcludeLocationMode" Checked='<%# Bind("ExcludeFromLocation") %>'  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                &nbsp;&nbsp;&nbsp;From
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc2:DateControl ID="fromDateControl" runat="server" Nullable="true" width="90px"  />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                To
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc2:DateControl ID="toDateControl" runat="server" Nullable="true" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                Fixed<br /> Penalty<br />*
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="fixedPenaltyTextBox" runat="server" Text='<%# Bind("SrpFxCst") %>'
                                    Width="40" style="text-align: right" MaxLength="8" onkeypress="keyStrokeDecimal();"
                                        onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                Variable<br /> Daily<br /> Penalty*
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="variableDailyPenaltyTextBox" runat="server" Text='<%# Bind("SrpVrCst") %>'
                                    Width="40" style="text-align: right" MaxLength="8" onkeypress="keyStrokeDecimal();"
                                    onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                             &nbsp;&nbsp;&nbsp;   Vis Lvl*
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="visLvlDropDownList" runat="server" Width="100" AppendDataBoundItems="false">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                Type
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="typeDropDownList" runat="server" AppendDataBoundItems="true"
                                    Width="80" DataValueField="SrpAvl">
                                    <asp:ListItem Text="Both" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Schedule" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                Active
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="activeCheckBox" runat="server" Checked='<%# Bind("SrpActive") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100">
                            <HeaderTemplate>
                                &nbsp;&nbsp;&nbsp;Turnaround <br />&nbsp;&nbsp;&nbsp;Costs
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="costLabel" runat="server" Text='<%# Bind("TctDesc") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <%-- <asp:BoundField DataField="FlmDesc" HeaderText="Fleet Model*" ReadOnly="True" />
                        <asp:BoundField DataField="SrpStDt" HeaderText="From" ReadOnly="True" />
                        <asp:BoundField DataField="SrpEnDt" HeaderText="To" ReadOnly="True" />
                        <asp:BoundField DataField="SrpFxCst" HeaderText="Fixed Penalty*" ReadOnly="True" />
                        <asp:BoundField DataField="SrpVrCst" HeaderText="Variable Daily Penalty*" ReadOnly="True" />
                        <asp:BoundField DataField="SrpVsLvl" HeaderText="Vis Lvl*" ReadOnly="True" />
                        <asp:BoundField DataField="SrpAvl" HeaderText="Type" ReadOnly="True" />
                        <asp:BoundField DataField="SrpActive" HeaderText="Active" ReadOnly="True" />
                        <asp:BoundField DataField="TctDesc" HeaderText="Turnaround Costs" ReadOnly="True" />--%>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:CheckBox ID="allCheckBox" runat="server" onclick="javascript:checkUncheckCheckboxes(this);" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="checkedCheckBox" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <br />
            </td>
        </tr>
        </table>
        <table>
        <tr>
            <td>
                <asp:GridView ID="costGridView" runat="server" AutoGenerateColumns="false" CssClass="dataTable">
                    <AlternatingRowStyle CssClass="oddRow" />
                    <RowStyle CssClass="evenRow" />
                    <Columns>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                No of Half Days
                            </HeaderTemplate>
                            <ItemTemplate>
                                Turnaround Cost
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                0
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox0" runat="server" Width="70" Text="0.0" style="text-align: right" MaxLength="8"
                                    onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                1
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server" Width="70" style="text-align: right" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                2
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Width="70" style="text-align: right" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                3
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Width="70" style="text-align: right" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                4
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox4" runat="server" Width="70" style="text-align: right" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                5
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox5" runat="server" Width="70" style="text-align: right" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Wrap="false">
                            <HeaderTemplate>
                                6
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox6" runat="server" Width="70" style="text-align: right" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
            <td align="right">
                <asp:Button ID="applyButton" runat="server" CssClass="Button_Standard" />
            </td>
        </tr>
       
        <tr>
            <td colspan=2 align="right">
                <br />
                <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />          
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
    </table>
    <uc3:ConfimationBoxControl ID="confimationBoxControl" runat="server" LeftButton_Text="Yes"
        RightButton_Text="No" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
        Title="Maintain Product Assignment Alternatives " />
</asp:Content>
