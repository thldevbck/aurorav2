<%@ Page 
    Language="VB" 
    MasterPageFile="~/Include/AuroraHeader.master" 
    AutoEventWireup="false"
    CodeFile="BlockingRuleList.aspx.vb" 
    Inherits="OpsAndLogs_BlockingRuleList" 
    Title="Blocking Rule" %>

<asp:Content ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        table.dataTableGrid ul { margin: 0 0 0 16px; padding: 0; }
        table.dataTableGrid li { margin: 0px; padding: 0; list-style-type: square; text-align: left; }
        table.dataTableGrid td { vertical-align: top; }
    </style>
</asp:Content>
<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    
</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <asp:Panel runat="server" Width="780px">

        <table cellpadding="2" cellspacing="0" width="100%">
            <tr>
                <td style="width: 150px;">Country: </td>
                <td style="width: 250px">
                    <asp:DropDownList ID="countryDropDown" runat="server" Width="200px" AutoPostBack="true" />
                </td>
                <td style="width: 150px">Status: </td>
                <td style="width: 350px">
                    <asp:RadioButtonList ID="statusRadioButtonList" runat="server" CssClass="repeatLabelTable" RepeatLayout="Table" RepeatDirection="Horizontal" AutoPostBack="true">
                        <asp:ListItem Value="TCyes">T & C</asp:ListItem>
                        <asp:ListItem Value="Current" Selected="True">Current + Future</asp:ListItem>
                        <asp:ListItem Value="All">All</asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
        </table>
        
        <asp:Repeater ID="blockingRuleTypeRepeater" runat="server">
            <ItemTemplate>

        <hr />
          
        <table cellpadding="2" cellspacing="0" width="100%" class="dataTableGrid" style="margin-bottom: 10px;">
            <thead>
                <tr>
                    <th style="padding: 2px;" colspan="9">
                        <asp:Label ID="typeLabel" runat="server" Style="float:left"/>
                        <asp:Button ID="createButton" runat="server" Text="New" CssClass="Button_Standard Button_New" style="float:right; margin-right: 2px"/>
                    </th>
                </tr>
                <tr>
                    <th style="width: 155px">Period</th>
                    <th style="width: 30px; text-align: center">Dur</th>
                    <th style="width: 30px; text-align: center">Ahd</th>
                    <th style="width: 40px; text-align: center">UOM</th>
                    <th style="width: 30px; text-align: center">O/U/I</th>
                    <th style="width: 30px; text-align: center">Ovr</th>
                    <th style="width: 30px; text-align: center">T/C</th>
                    <th style="width: 30px; text-align: center">Flex</th>
                    <th>Rules</th>
                </tr>
            </thead>
            
            <tbody>
                    
        <asp:Repeater ID="blockingRuleRepeater" runat="server">
            <ItemTemplate>
                <asp:Literal ID="blockingRuleTableRow" runat="server" />
                    <td colspan="9" style="border-top-width: 2px"><asp:HyperLink ID="messageHyperLink" runat="server" /></td>
                </tr>
                <asp:Literal ID="blockingRuleTableRow2" runat="server" />
                    <td><asp:Label ID="periodLabel" runat="Server" /></td>
                    <td style="text-align: center"><asp:Label ID="durationLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td style="text-align: center"><asp:Label ID="bookAheadLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td style="text-align: center"><asp:Label ID="uomLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td style="text-align: center"><asp:Label ID="dateByLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td style="text-align: center"><asp:Label ID="overrideLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td style="text-align: center"><asp:Label ID="tandcLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td style="text-align: center"><asp:Label ID="flexLabel" runat="Server">&nbsp;</asp:Label></td>
                    <td>
                        <asp:Table ID="blockingRuleDetailTable" runat="Server" CellPadding="0" CellSpacing="0" CssClass="dataTableInternal" Width="100%">
                            <asp:TableRow ID="productsTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>Products:</li></ul></asp:TableCell>
                                <asp:TableCell><asp:Label ID="productsLabel" runat="server" /></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="brandsTableRow" runat="server" >
                                <asp:TableCell Style="width: 70px;"><ul><li>Brands:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="brandsLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="packagesTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>Packages:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="packagesLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="locationsFromTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>From:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="locationsFromLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="locationsToTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>To:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="locationsToLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="agentCountriesTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>Agn Cty:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="agentCountriesLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="agentCategoriesTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>Agn Cgy:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="agentCategoriesLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow ID="agentsTableRow" runat="server">
                                <asp:TableCell Style="width: 70px;"><ul><li>Agn:</li></ul></asp:TableCell>
                                <asp:TableCell ColumnSpan="7"><asp:Label ID="agentsLabel" runat="server">&nbsp;</asp:Label></asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
            
            </tbody>

        </table>
            
            </ItemTemplate>
        </asp:Repeater>

    </asp:Panel>
</asp:Content>
