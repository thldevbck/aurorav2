﻿''REV:MIA OCT. 4 2013                   - Anniversary of TAU GAMMA PHI TRISKELION IN PHILIPPINES

Imports Aurora.Common
Imports Aurora.Common.Data
Imports Aurora.OpsAndLogs.Services
Imports System.Data
Imports System.Xml


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.LocRestriction)> _
Partial Class OpsAndLogs_MaintainRelocRestriction
    Inherits AuroraPage

    Private Const INVALID_COUNTRY As String = "Please select country from the list."
    Private Const NO_CHANGES As String = "There was no update / changes happened in the Location Restriction list."
    Private Const ERROR_MSG As String = "There was an error happened during saving. Please review your changes."
    Private Const SUCCESS_MSG As String = "Location Restriction list was updated."

    Private Property EmptyXmlRow As String
        Get
            Return ViewState("EmptyRow")
        End Get
        Set(value As String)
            ViewState("EmptyRow") = value
        End Set
    End Property

    Private Property CurrentDS As DataSet
        Get
            Return ViewState("CurrentDS")
        End Get
        Set(value As DataSet)
            ViewState("CurrentDS") = value
        End Set
    End Property

    Private Property OldXml As String
        Get
            Return ViewState("OldXml")
        End Get
        Set(value As String)
            ViewState("OldXml") = value
        End Set
    End Property

    Public ReadOnly Property GetCountry As String
        Get
            If (ddlCountry.SelectedItem.Value = "") Then Return UserCode
            Return ddlCountry.SelectedValue
        End Get
    End Property

    Sub BindCountries()
        '' Me.ddlCountry.AppendDataBoundItems = True
        '' Me.ddlCountry.Items.Add(New ListItem("(--please select--)", ""))
        Me.ddlCountry.DataSource = Aurora.OpsAndLogs.Services.LocationRestriction.GetCountriesForLocationRestriction(UserCode)
        Me.ddlCountry.DataValueField = "CtyCode"
        Me.ddlCountry.DataTextField = "Countries"
        Me.ddlCountry.DataBind()
    End Sub

    Sub BindData(Force As Boolean)
        If Force Then
            CurrentDS = Aurora.OpsAndLogs.Services.LocationRestriction.GetLocRestrictions(GetCountry)
        End If
        LocationRestrictRepeater.DataSource = CurrentDS.Tables(0).DefaultView
        LocationRestrictRepeater.DataBind()
    End Sub

    Dim PageRefreshUsingF5 As Boolean

    Private Property CurrentViewState As String
        Get
            Return ViewState("CurrentViewState")
        End Get
        Set(value As String)
            ViewState("CurrentViewState") = value
        End Set
    End Property

    Private Property CurrentSessionState As String
        Get
            Return Session("CurrentSessionState")
        End Get
        Set(value As String)
            Session("CurrentSessionState") = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Not Page.IsPostBack) Then
            PageRefreshUsingF5 = False
            CurrentDS = Aurora.OpsAndLogs.Services.LocationRestriction.GetLocRestrictions(IIf(HasCtyAsParameter, Request.QueryString("cty"), Me.CountryCode))
            BindCountries()

            Me.ddlCountry.SelectedValue = IIf(HasCtyAsParameter, Request.QueryString("cty"), Me.CountryCode)
            BindData(False)

            CurrentViewState = System.Guid.NewGuid.ToString
            CurrentSessionState = CurrentViewState

        Else
            If (CurrentViewState <> CurrentSessionState) Then
                PageRefreshUsingF5 = True
                BindData(True)
            End If

            CurrentViewState = System.Guid.NewGuid.ToString
            CurrentSessionState = CurrentViewState

        End If


    End Sub

    Protected Function GetMultiselectItems(ByVal data As String) As MultiSelectItem()

        Dim doc As New XmlDocument()
        doc.AppendChild(doc.CreateElement("root"))

        doc.FirstChild.InnerXml = "<LrFlmId>" & data & "</LrFlmId>"

        Dim items(doc.FirstChild.ChildNodes.Count) As MultiSelectItem

        Dim intIndex As Integer = 0
        For Each modelNode As XmlNode In doc.FirstChild.ChildNodes

            Dim item As New MultiSelectItem()
            item.Key = modelNode.InnerText.Split("-")(0)
            item.Value = modelNode.InnerText.Split("-")(1)
            items(intIndex) = item

            intIndex += 1
        Next

        Return items
    End Function

    Public Function GeneratingXMLtoSave() As String

        Const MSG_FLEET As String = "MESSAGE Fleet Model is empty."
        Const MSG_LOCATION As String = "MESSAGE Location is empty."
        Const MSG_PENALTY As String = "MESSAGE Penalty is empty."
        Const MSG_PENALTY_ZERO As String = "MESSAGE Penalty should not be equal to ZERO."
        Const MSG_STARTDATE As String = "MESSAGE Starting Date is empty."
        Const MSG_ENDTDATE As String = "MESSAGE Ending Date is empty."
        Const MSG_STARTENDDATE As String = "MESSAGE Starting Date should not be equal or greater than Ending Date."
        Const MSG_ALL_MANDATORY As String = "MESSAGE All fields are mandatory. Please review again your changes."

        Dim sb As New StringBuilder
        Dim totalrow As Integer = LocationRestrictRepeater.Items.Count - 1

        sb.Append("<Root>")

        Dim AllValidationsActive As Boolean = False
        Dim currentRowCounter As Integer = -1
        Dim isThisLastRow As Boolean = False
        Dim isRecordValidToSave As Boolean = False
        Dim emptyRow As String = String.Empty
        For Each row As RepeaterItem In LocationRestrictRepeater.Items

            Dim LrId As String = CType(row.FindControl("hiddenLrId"), HiddenField).Value
            currentRowCounter = currentRowCounter + 1

            If ((totalrow = 0) Or (totalrow <> currentRowCounter)) Then
                AllValidationsActive = True
            Else
                ''reach the last row
                isThisLastRow = (totalrow = currentRowCounter)
                If (isThisLastRow) Then
                    AllValidationsActive = False
                End If
            End If

           

            Dim multiselect As UserControls_SelectControl_MultiSelect = DirectCast(row.FindControl("multiselect"), UserControls_SelectControl_MultiSelect)
            Dim pkrFilterLocationFrom As UserControls_PickerControl = DirectCast(row.FindControl("pkrLocationTo"), UserControls_PickerControl)
            Dim tbox As TextBox = DirectCast(row.FindControl("tbox"), TextBox)
            Dim dtStartDate As UserControls_DateControl = DirectCast(row.FindControl("dtDateFrom"), UserControls_DateControl)
            Dim dtEndDate As UserControls_DateControl = DirectCast(row.FindControl("dtDateTo"), UserControls_DateControl)
            Dim deleteFlrId As CheckBox = DirectCast(row.FindControl("deleteFlrId"), CheckBox)

            Dim deleteMe As String = IIf(deleteFlrId.Checked, "1", "0")


            Dim CtyCode As String = GetCountry
            Dim LrFlmId As String = multiselect.GetKeysAsCsv()
            If (String.IsNullOrEmpty(LrFlmId) And AllValidationsActive And Not deleteFlrId.Checked) Then
                Return MSG_FLEET
            End If

            Dim LrLocCode As String = pkrFilterLocationFrom.DataId
            If (String.IsNullOrEmpty(LrLocCode) And AllValidationsActive And Not deleteFlrId.Checked) Then
                Return MSG_LOCATION
            End If

            Dim LrCost As String = tbox.Text
            If (String.IsNullOrEmpty(LrCost) And AllValidationsActive And Not deleteFlrId.Checked) Then
                Return MSG_PENALTY
            End If

            If (CInt(LrCost) = 0) And AllValidationsActive And Not deleteFlrId.Checked Then
                Return MSG_PENALTY_ZERO
            End If

            Dim LrStartDate As String = dtStartDate.Date.ToShortDateString
            If ((String.IsNullOrEmpty(LrStartDate) Or LrStartDate = "1/01/0001") And AllValidationsActive And Not deleteFlrId.Checked) Then
                Return MSG_STARTDATE
            End If

            Dim LrEndDate As String = dtEndDate.Date.ToShortDateString
            If ((String.IsNullOrEmpty(LrEndDate) Or LrEndDate = "1/01/0001") And AllValidationsActive And Not deleteFlrId.Checked) Then
                Return MSG_ENDTDATE
            End If

            If ((Date.Compare(Convert.ToDateTime(LrStartDate).ToShortDateString, _
                             Convert.ToDateTime(LrEndDate).ToShortDateString) >= 0) And AllValidationsActive And Not deleteFlrId.Checked) Then
                Return MSG_STARTENDDATE
            End If

            If (isThisLastRow) Then
                isRecordValidToSave = (Not String.IsNullOrEmpty(CtyCode) _
                                            And Not String.IsNullOrEmpty(LrFlmId) _
                                            And Not String.IsNullOrEmpty(LrLocCode) _
                                            And (Not String.IsNullOrEmpty(LrCost) And CInt(LrCost) > 0) _
                                            And Not String.IsNullOrEmpty(LrStartDate) _
                                            And Not String.IsNullOrEmpty(LrEndDate) _
                                            )
            Else
                isRecordValidToSave = True
            End If

            If (isRecordValidToSave) Then
                Dim template As String = MapperFlmID(LrId, _
                                                      CtyCode, _
                                                      LrFlmId, _
                                                      LrLocCode, _
                                                      LrCost, _
                                                      LrStartDate, _
                                                      LrEndDate, _
                                                      UserCode, _
                                                      deleteMe)

                sb.Append(template)
            Else

                emptyRow = XmlToSaveTemplate("", GetCountry, LrFlmId, LrLocCode, LrCost, ParseDateTime(DateTime.Now.ToShortDateString), ParseDateTime(DateTime.Now.AddDays(10).ToShortDateString), UserCode, deleteMe)
                System.Diagnostics.Debug.WriteLine(EmptyXmlRow)
                System.Diagnostics.Debug.WriteLine(emptyRow)
                If (emptyRow.Trim <> EmptyXmlRow.Trim And emptyRow.Trim.Length <> EmptyXmlRow.Trim.Length) Then
                    Return MSG_ALL_MANDATORY
                End If
            End If
        Next

        sb.Append("</Root>")

        Return sb.ToString
    End Function

    Function MapperFlmID(LrId As String, _
                               CtyCode As String, _
                               LrFlmId As String, _
                               LrLocCode As String, _
                               LrCost As String, _
                               LrStartDate As String, _
                               LrEndDate As String, _
                               AddModDUsrId As String, _
                               deleteMe As String) As String
        Dim sb As New StringBuilder
        Dim template As String

        For Each item As String In LrFlmId.Split(",")
            template = XmlToSaveTemplate(LrId, _
                                                       CtyCode, _
                                                       item.Trim, _
                                                       LrLocCode, _
                                                       LrCost, _
                                                       LrStartDate, _
                                                       LrEndDate, _
                                                       AddModDUsrId, _
                                                       deleteMe)
            sb.Append(template)
        Next

        Return sb.ToString
    End Function

    Function XmlToSaveTemplate(LrId As String, _
                               CtyCode As String, _
                               LrFlmId As String, _
                               LrLocCode As String, _
                               LrCost As String, _
                               LrStartDate As String, _
                               LrEndDate As String, _
                               AddModDUsrId As String, _
                               deleteMe As String) As String
        Dim sb As New StringBuilder
        sb.Append("<RelocationRestriction>")
        sb.AppendFormat("<LrId>{0}</LrId>", LrId)
        sb.AppendFormat("<CtyCode>{0}</CtyCode>", CtyCode)
        sb.AppendFormat("<LrFlmId>{0}</LrFlmId>", LrFlmId)
        sb.AppendFormat("<LrLocCode>{0}</LrLocCode>", LrLocCode)
        sb.AppendFormat("<LrCost>{0}</LrCost>", LrCost)
        sb.AppendFormat("<LrStartDate>{0}</LrStartDate>", LrStartDate)
        sb.AppendFormat("<LrEndDate>{0}</LrEndDate>", LrEndDate)
        sb.AppendFormat("<AddModDUsrId>{0}</AddModDUsrId>", AddModDUsrId)
        sb.AppendFormat("<Del>{0}</Del>", deleteMe)
        sb.Append("</RelocationRestriction>")

        Return sb.ToString

    End Function

    Protected Sub btnSaveTop_Click(sender As Object, e As System.EventArgs) Handles btnSaveTop.Click, btnSaveBottom.Click
        If PageRefreshUsingF5 Then Return

        If String.IsNullOrEmpty(GetCountry) Then
            SetErrorShortMessage(INVALID_COUNTRY)
            Return
        End If

        Dim template As String = GeneratingXMLtoSave()
        If (template.Contains("MESSAGE") = True) Then
            SetErrorShortMessage(template.Replace("MESSAGE", ""))
            Return
        End If

        If (String.Compare(OldXml, template) = 0) Then
            SetInformationShortMessage(NO_CHANGES)
            Return
        End If

        Dim result As String = Aurora.OpsAndLogs.Services.LocationRestriction.UpdateLocRestriction(GetCountry, template)
        If (result.Contains("ERROR")) Then
            SetErrorShortMessage(ERROR_MSG)
            Return
        Else
            SetInformationShortMessage(SUCCESS_MSG)
            OldXml = template
        End If


        BindData(True)
    End Sub

    Protected Sub LocationRestrictRepeater_ItemCreated(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles LocationRestrictRepeater.ItemCreated
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim multiselect As UserControls_SelectControl_MultiSelect = DirectCast(e.Item.FindControl("multiselect"), UserControls_SelectControl_MultiSelect)
            multiselect.CountryCode = GetCountry

            
        End If

    End Sub

    Protected Sub LocationRestrictRepeater_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles LocationRestrictRepeater.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            Dim multiselect As UserControls_SelectControl_MultiSelect = DirectCast(e.Item.FindControl("multiselect"), UserControls_SelectControl_MultiSelect)
            multiselect.CountryCode = GetCountry
            If (Not IsDBNull(e.Item.DataItem("LrFlmId"))) Then
                If (Not multiselect Is Nothing) Then
                    multiselect.Items = GetMultiselectItems(Server.HtmlEncode(e.Item.DataItem("LrFlmId") & "-" & e.Item.DataItem("LrFlmCode")))
                End If
            End If


            Dim pkrFilterLocationFrom As UserControls_PickerControl = DirectCast(e.Item.FindControl("pkrLocationTo"), UserControls_PickerControl)
            If (Not pkrFilterLocationFrom Is Nothing) Then
                pkrFilterLocationFrom.Param1 = GetCountry
            End If

            Dim tbox As TextBox = DirectCast(e.Item.FindControl("tbox"), TextBox)
            tbox.Attributes.Add("onblur", "CurrencyFormatted(this)")
            If (Not tbox Is Nothing And Not IsDBNull(e.Item.DataItem("LrCost"))) Then
                tbox.Text = Convert.ToDecimal(e.Item.DataItem("LrCost")).ToString("f2")
            Else
                tbox.Text = "0.00"
            End If

            Dim dtStartDate As UserControls_DateControl = DirectCast(e.Item.FindControl("dtDateFrom"), UserControls_DateControl)
            If (Not dtStartDate Is Nothing And Not IsDBNull(e.Item.DataItem("LrStartDate"))) Then
                dtStartDate.Date = ParseDateTime(e.Item.DataItem("LrStartDate"), SystemCulture)
            Else
                dtStartDate.Date = ParseDateTime(DateTime.Now.ToShortDateString)
            End If

            Dim dtEndDate As UserControls_DateControl = DirectCast(e.Item.FindControl("dtDateTo"), UserControls_DateControl)
            If (Not dtEndDate Is Nothing And Not IsDBNull(e.Item.DataItem("LrEndDate"))) Then
                dtEndDate.Date = ParseDateTime(e.Item.DataItem("LrEndDate"), SystemCulture)
            Else
                dtEndDate.Date = ParseDateTime(DateTime.Now.AddDays(10).ToShortDateString)
            End If

            Dim LrId As String = CType(e.Item.FindControl("hiddenLrId"), HiddenField).Value
            Dim deleteFlrId As CheckBox = DirectCast(e.Item.FindControl("deleteFlrId"), CheckBox)
            If (String.IsNullOrEmpty(LrId)) Then
                If (Not deleteFlrId Is Nothing) Then
                    deleteFlrId.Enabled = False
                    deleteFlrId.Checked = False
                End If
                EmptyXmlRow = XmlToSaveTemplate("", GetCountry, "", "", "0.00", ParseDateTime(DateTime.Now.ToShortDateString), ParseDateTime(DateTime.Now.AddDays(10).ToShortDateString), UserCode, "0")
            End If

        End If

    End Sub

    Protected Sub btnResetBottom_Click(sender As Object, e As System.EventArgs) Handles btnResetBottom.Click, btnResetTop.Click
        BindData(True)
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
        '' BindData(True)
        If (Not HasCtyAsParameter()) Then
            Response.Redirect(Request.Url.ToString & "?cty=" & GetCountry)
        Else
            Dim newpath As String = Request.Url.ToString
            If (GetCountry = "AU") Then
                Response.Redirect(newpath.Replace("cty=NZ", "cty=AU"))
            Else
                Response.Redirect(newpath.Replace("cty=AU", "cty=NZ"))
            End If
        End If

    End Sub

    Function HasCtyAsParameter() As Boolean
        Return Request.Url.PathAndQuery.Contains("cty")
    End Function
End Class
