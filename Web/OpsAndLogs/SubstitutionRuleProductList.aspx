<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="SubstitutionRuleProductList.aspx.vb" Inherits="OpsAndLogs_SubstitutionRuleProductList"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>

<%@ Register Src="~/UserControls/MultiSelectControl/MultiSelect.ascx" TagName="MultiSelect" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        .subRuleFromLoc {padding:0px 0px 0px 20px;}
        .subRuleFromLoc span img{float: right; margin:0px 10px 0px 0px;}
                
        #mrrFilterTable td{padding-right:10px;}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%">
        <tr width="100%">
            <td width="150px">
                Country Of Operation:
            </td>
            <td width="250px">
                <asp:TextBox ID="countryTextBox" runat="server" ReadOnly="true" ></asp:TextBox>
            </td>
            <td align="right">
                <asp:Button ID="substitutionRuleButton" runat="server" Text="Fleet Model Assignment Alternatives" 
                    CssClass="Button_Standard" Width="260" />
            </td>
        </tr>
    </table>

    <hr />
    <table id="mrrFilterTable">
        <tr>
            <td>From: </td>
            <td><uc1:DateControl ID="dtFilterDateFrom" runat="server" /></td>
            <td></td>
            <td>To: </td>
            <td><uc1:DateControl ID="dtFilterDateTo" runat="server" /></td>
            <td></td>
            <td>Location From: </td>
            <td><uc1:PickerControl ID="pkrFilterLocationFrom" runat="server" PopupType="LOCATIONFORCOUNTRY" Width="60px"/></td>
        </tr>
        <tr>
            <td colspan="8">
                <br />
                <asp:Button ID="btnFilter" runat="server" Text="Apply Filter" CssClass="Button_Standard Button_Search" />
                <asp:Button ID="btnResetFilter" runat="server" Text="Reset Filter" CssClass="Button_Standard Button_Search" />
            </td>
        </tr>
    </table>
    <hr />

    <table width="130%">
        <tr>
            <td>
                <asp:GridView ID="productGridView" runat="server" Width="80%" AutoGenerateColumns="false"
                    CssClass="dataTable"  HeaderStyle-VerticalAlign="Top">
                    <AlternatingRowStyle CssClass="oddRow" />
                    <RowStyle CssClass="evenRow" />
                    <Columns>
                        <asp:TemplateField Visible="False">
                            <ItemTemplate>
                                <asp:Label ID="subPrdIdLabel" runat="server" Text='<%# Bind("SubPrdId") %>'></asp:Label>
                                <asp:Label ID="srpIdLabel" runat="server" Text='<%# Bind("SrpId") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="240" ItemStyle-Wrap="true">
                            <HeaderTemplate>
                                Bookable Product
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("PrdDesc") %>' NavigateUrl='<%# GetProductUrl(Eval("SubPrdId")) %>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField ItemStyle-Width="100" ItemStyle-Wrap="true">
                            <HeaderTemplate>
                                Fleet Models
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="modelLabel" runat="server" Text='<%# Bind("FlmDesc") %>'></asp:Label>
                                <asp:HiddenField ID="fleetModelId" Value='<%# Bind("SrpFlmId") %>' runat="server" />
                            </ItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100" ItemStyle-CssClass="subRuleFromLoc" HeaderStyle-CssClass="subRuleFromLoc">
                            <HeaderTemplate>
                                From <br />Location
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc1:MultiSelect ID="fromLoc" SelectType="Locations" IsReadOnly="true" DisplayKeys="true" Items='<%# GetMultiselectItems(eval("locations")) %>' CountryCode="<%#mCountryCode %>"  runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField >
                            <HeaderTemplate>
                                Exclude <br /> from <br /> Location
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkExcludeLocationMode" Checked='<%# Bind("ExcludeFromLocation") %>' Enabled="false"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="100">
                            <HeaderTemplate>
                                From
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc1:DateControl ID="fromDateControl" runat="server" date='<%# GetDate(eval("SrpStDt")) %>'
                                    Nullable="false" />
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>                        
                        <asp:TemplateField ItemStyle-Width="100">
                            <HeaderTemplate>
                                To
                            </HeaderTemplate>
                            <ItemTemplate>
                                <uc1:DateControl ID="toDateControl" runat="server" date='<%# GetDate(eval("SrpEnDt")) %>'
                                    Nullable="false" />
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="7">
                            <HeaderTemplate>
                                Fixed <br />Penalty
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="fixedTextBox" runat="server" Text='<%# Bind("SrpFxCst") %>' 
                                     style="text-align: right" Width="40" MaxLength="8"
                                     onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="70">
                            <HeaderTemplate>
                                Variable <br />Daily <br />Penalty
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="variableTextBox" runat="server" Text='<%# Bind("SrpVrCst") %>' 
                                style="text-align: right" Width="40" MaxLength="8"
                                onkeypress="keyStrokeDecimal();" onblur="this.value=trimDecimal(this.value,2)"></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="SrpAvl" HeaderText="Type" ReadOnly="True"  ControlStyle-Width="30" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <%--<tr>
            <td align="right">
                <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
            </td>
        </tr>--%>
    </table>
    <table width="104%">
        <tr align="right">
            <td>
            <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
            <asp:Button ID="newButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
            <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
    </table>
</asp:Content>
