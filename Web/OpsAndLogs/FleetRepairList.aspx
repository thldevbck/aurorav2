<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	FleetRepairList.aspx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	15.4.8 / Base version
''                  -   23.5.8 / Size of popup R&M Report changed
''                  -   11.6.8 / Shoel / URL of R&M Report changed, now uses the SQL Report
''                  -   18.6.8 / Shoel / The R&M report now appears as a non-modal popup, 
''                      this prevents the weird issues with modal popups
''                  -   ID4    / Shoel / Adjusted tables for date format changes
''                  rev:mia 1August2014 
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false" CodeFile="FleetRepairList.aspx.vb" Inherits="OpsAndLogs_FleetRepairList" Title="Fleet Repairs List" %>

<asp:Content ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

<script language="javascript" type="text/javascript">
function GetRepairDetails(RepairId,BackPage)
{
    window.location="FleetRepairManagement.aspx?RepairId=" + RepairId + "&BackPage=" + BackPage + "&NewRecord=False"  ;
    return false;
}

function CreateNewRepairDetails(AssetId,MessagePanelId,BackPage)
{
    if (AssetId!='')
    {
        var msgPanel = document.getElementById(MessagePanelId);
        msgPanel.innerHTML = "";
        window.location="FleetRepairManagement.aspx?AssetId=" + AssetId  + "&BackPage=" + BackPage + "&NewRecord=True" ;
    }
    else
    {
        var msgPanel = document.getElementById(MessagePanelId);
        msgPanel.innerHTML = "<DIV class=Error>Please select the record</DIV>";
    }
    return false;
}

function ShowRMReport()
{
    window.open("../Reports/Default.aspx?funCode=RPT_RMREPORT&popup=True","", "height=650,width=975,resizable=yes,scrollbars=yes,toolbar=no,status=yes,menubar=no");
}
</script>

</asp:Content>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    
     <table width="780px" border="0" cellpadding="2" cellspacing="0">
            <tr>
                <td width="100px">Unit Number:</td>
                <td width="100px">
                    <asp:TextBox  ID="lblUnitNo" runat="server"  Width="100"  BackColor="White"/>
                </td>
                <td align="center" width="100px">
                    <b>- OR -</b>
                </td>
                <td width="100px">Registration Number:</td>
                <td width="100px">
                    <asp:TextBox  ID="lblRegNo" runat="server"  Width="100" BackColor="White"/>
                </td>
                <td width="100px">
                    <asp:Button ID="btnSearchUnitNumberOrRegNumber" Text ="Search" runat="server" CssClass="Button_Search Button_Standard" />
                </td>
            </tr>

            <tr>
                <td width="100px">
                    Description:
                </td>
                <td colspan="4">
                    <asp:TextBox ReadOnly="true" ID="lblDescription" runat="server"  Width="200"/>
                </td>
            </tr>
        </table>
    <div style="width: 890px">

       

        <br />

        <asp:Repeater ID="rptVehicleDetails" runat="server">
            <HeaderTemplate>
                <table width="100%;" border="0" cellpadding="1" cellspacing="0" class="dataTable">
                    <thead>
                        <tr align="left">
                            <th style="width:50px">RUC</th>
                            <th style="width:50px">COF</th>
                            <th style="width:80px">Rego</th>
                            <%--<th>Radio Code</th>--%>
                            <th>Engine Number</th>
                            <th>Vin Number</th>
                            <th>Year</th>
                            <th>1st Reg.Date</th>
                            <th>Self Contain Exp</th>
                            <th>Warranty Exp</th>
                        </tr>
                        
                    </thead>
                    <tbody>
            </HeaderTemplate>
            <ItemTemplate>
                        <tr class="evenRow">
                            <td><%#Container.DataItem("RUCExp")%></td>
                            <td><%#Container.DataItem("COFExp")%></td>
                            <td><%#Container.DataItem("RegoExp")%></td>
                            <%--<td><%#Container.DataItem("RadioCode")%></td>--%>
                            <td><%#Container.DataItem("EngNum")%></td>
                            <td><%#Container.DataItem("VinNum")%></td>
                            <td><%#Container.DataItem("YrOfMan")%></td>
                            <td><%#Container.DataItem("FstRegDt")%></td>
                            <td><%#Container.DataItem("SelfContainExp")%></td>
                            <td><%#Container.DataItem("WarrantyExp")%></td>
                        </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                        <tr class="oddRow">
                            <td><%#Container.DataItem("RUCExp")%></td>
                            <td><%#Container.DataItem("COFExp")%></td>
                            <td><%#Container.DataItem("RegoExp")%></td>
                            <td><%#Container.DataItem("RadioCode")%></td>
                            <td><%#Container.DataItem("EngNum")%></td>
                            <td><%#Container.DataItem("VinNum")%></td>
                            <td><%#Container.DataItem("YrOfMan")%></td>
                            <td><%#Container.DataItem("FstRegDt")%></td>
                        </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                    </tbody>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        
        <br />

        <table width="100%">
            <tr>
                <td width="100%" align="right">
                    <asp:Button 
                        ID="btnNew" 
                        Text="New" 
                        runat="server" 
                        CssClass="Button_Standard Button_New" />
                    <input 
                        type="button" 
                        id="btnTempReport" 
                        value="R&M Report" 
                        onclick="ShowRMReport();"
                        class="Button_Standard Button_Report" />
                </td>
            </tr>
        </table>

        <br />

        <!--OFF FLEET TABLE-->
        <asp:Repeater ID="rptOffFleet" runat="server">
            <HeaderTemplate>
                <table width="100%" border="0" cellpadding="1" cellspacing="0" class="dataTable">
                    <thead>
                        <tr align="center">
                            <th width="6%" style="text-align: left">Branch</th>
                            <th width="9%"  style="text-align: left">From Date</th>
                            <th width="9%"  style="text-align: left">To Date</th>
                            <th width="7%"  style="text-align: left">Odo.Start</th>
                            <th width="13%"  style="text-align: left">Repair Type</th>
                            <th width="19%" style="text-align: left">Service/Repair/Provider</th>
                            <th width="20%"  style="text-align: left">Description</th>
                            <th width="10%"  style="text-align: left">Comp.Date</th>
                            <th width="10%"  style="text-align: left">Complete</th>
                            <th width="7%"  style="text-align: left">Off-Fleet</th>
                            
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class="evenRow">
                    <td align="left">
                      
                        <asp:LinkButton ID="lnkEvenDetails" runat="server" Text='<%#Bind("Branch")%>'></asp:LinkButton>
                        <asp:HiddenField ID="hdnEvenRepairId" runat="server" Value='<%#Bind("RepairId")%>' />
                    </td>
                    <td align="left" nowrap="nowrap"><%#Container.DataItem("FrDt")%></td>
                    <td align="left" nowrap="nowrap"><%#Container.DataItem("Todt")%></td>
                    <td align="left"><%#Container.DataItem("OdoSt")%></td>
                    <td align="left"><%#Container.DataItem("Reason")%></td>
                    <td align="left"><%#Container.DataItem("Provider")%></td>
                    <td align="left"><%#Container.DataItem("ReasonDescription")%></td>
                    <td align="left"><%#Container.DataItem("ComplDt")%></td>
                    <td align="left"><%#Container.DataItem("Completed")%></td>
                    <td align="left"><%#Container.DataItem("AvlSch")%></td>
                    
                </tr>
            </ItemTemplate>
            <AlternatingItemTemplate>
                <tr class="oddRow">
                    <td align="left">
                       
                        <asp:LinkButton ID="lnkOddDetails" runat="server" Text='<%#Bind("Branch")%>'></asp:LinkButton>
                        <asp:HiddenField ID="hdnOddRepairId" runat="server" Value='<%#Bind("RepairId")%>' />
                    </td>
                    <td align="left" nowrap="nowrap"><%#Container.DataItem("FrDt")%></td>
                    
                    <td align="left" nowrap="nowrap"><%#Container.DataItem("Todt")%></td>
                    <td align="left"><%#Container.DataItem("OdoSt")%></td>
                    <td align="left"><%#Container.DataItem("Reason")%></td>
                    <td align="left"><%#Container.DataItem("Provider")%></td>
                    <td align="left"><%#Container.DataItem("ReasonDescription")%></td>
                    <td align="left"><%#Container.DataItem("ComplDt")%></td>
                    <td align="left"><%#Container.DataItem("Completed")%></td>
                    <td align="left"><%#Container.DataItem("AvlSch")%></td>
                    
                </tr>
            </AlternatingItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <!--/OFF FLEET TABLE-->
    
    </div>
</asp:Content>
