<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="SubstitutionRuleFleetModelList.aspx.vb" Inherits="OpsAndLogs_SubstitutionRuleFleetModelList"
    Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table width="100%">
        <tr width="100%">
            <td width="150px">
                Country Of Operation:
            </td>
            <td width="250px">
                <asp:TextBox ID="countryTextBox" runat="server" ReadOnly=true ></asp:TextBox>
            </td>
            <td align="right">
                <asp:Button ID="productButton" runat="server" Text="Bookable Product Assignment Alternatives" 
                    CssClass="Button_Standard" OnClick="backButton_Click" Width="260" />
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
                <asp:GridView ID="productGridView" runat="server" Width="100%" AutoGenerateColumns="false"
                   cellpadding="5" cellspacing="0" CssClass="dataTable" >
                    <AlternatingRowStyle CssClass="oddRow" />
                    <RowStyle CssClass="evenRow" />
                    <Columns>
                        <asp:BoundField DataField="FltModels" HeaderText="Fleet Model" ReadOnly="True" />
                        <asp:BoundField DataField="PrdBookable" HeaderText="Bookable Product" ReadOnly="True" />
                
                    
                        <asp:TemplateField HeaderText="From">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="fromLabel" text='<%# GetDate(eval("FrDt")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="false" ></ItemStyle>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText="To" >
                            <ItemTemplate>
                                <asp:Label runat="server" ID="toLabel" text='<%# GetDate(eval("ToDt")) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Wrap="false" ></ItemStyle>
                        </asp:TemplateField> 

                        <asp:BoundField DataField="FixedCost" HeaderText="Fixed Cost" ReadOnly="True" ItemStyle-HorizontalAlign=right />
                        <asp:BoundField DataField="VarCost" HeaderText="Variable Daily Cost" ReadOnly="True" ItemStyle-HorizontalAlign=right
                            ItemStyle-Width="60" />
                        <asp:BoundField DataField="Type" HeaderText="Type" ReadOnly="True" />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td align=right >
                <asp:Button ID="backButton2" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                    OnClick="backButton_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
