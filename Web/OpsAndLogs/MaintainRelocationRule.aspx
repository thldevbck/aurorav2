<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	MaintainRelocationRule.aspx
''	Date Created	-	24.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	24.4.8 / Shoel - Base version
''                      22.5.8 / Shoel - Common JS moved out of this file, allowed entry of decimal data
''                      25.8.8 / Shoel - Field validation added
''                      17.9.8 / Shoel - Type is no longer optional - needed to fix Squish#604
''                      25.9.8 / Shoel - 622 pickers needed 'LOCATIONFORCOUNTRY' and not 'LOCATION' :)
''  rev:mia             Sept 30, 2013 - addition of Exlude Fleet Model    
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="MaintainRelocationRule.aspx.vb" Inherits="OpsAndLogs_MaintainRelocationRule"
    Title="Maintain Relocation Rule" %>

<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="ucConfirmation" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="ucDateControl" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="ucPickerControl" %>

<%@ Register Src="~/UserControls/MultiSelectControl/MultiSelect.ascx" TagName="MultiSelect" TagPrefix="uc1" %>

<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        .mrrTinyText {width:30px;}
        .mrrSmallText {width:60px;}
        .mrrAlignRight {text-align:right;}
        table.mrrDataTable {width:100%;}
        table.mrrDataTable td, .mrrDataTable th{ text-align:center; width:40px;}
        .mrrAlignRight { text-align:right;}
        #mrrFilterTable td{padding-right:10px;}
        .relocRuleModels span img{float:right; margin:0px 10px 0px 0px;}
    </style>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script type="text/javascript" language="javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            if (args.get_error() != undefined) {
                args.set_errorHandled(true);
            }
        }

    </script>
    <script src="JS/OpsAndLogsCommon.js" type="text/javascript" language="javascript"></script>

</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel runat="server" ID="updMain">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td width="150px">
                        Country Of Operation:
                    </td>
                    <td>
                        <asp:TextBox ID="txtCountryOfOperation" runat="server" ReadOnly="true" CssClass="Textbox_Standard"></asp:TextBox>
                    </td>
                </tr>
            </table>
            <br />
            <%--<h4>Filter</h4>--%>
            <hr />
            <table id="mrrFilterTable">
                <tr>
                    <td>From: </td>
                    <td><ucPickerControl:PickerControl ID="pkrFilterLocationFrom" runat="server" PopupType="LOCATIONFORCOUNTRY" Width="60px"/></td>
                    <td></td>
                    <td>To: </td>
                    <td><ucPickerControl:PickerControl ID="pkrFilterLocationTo" runat="server" PopupType="LOCATIONFORCOUNTRY" Width="60px"/></td>
                    <td></td>
                    <td>Fleet model: </td>
                    <td>
                        <ucPickerControl:PickerControl ID="pkrFilterFleetModel" runat="server" PopupType="OPLOGGETFLEETMODEL" Width="60px"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="8">
                        <br />
                        <asp:Button ID="btnFilter" runat="server" Text="Apply Filter" CssClass="Button_Standard Button_Search" />
                        <asp:Button ID="btnResetFilter" runat="server" Text="Reset Filter" CssClass="Button_Standard Button_Search" />
                    </td>
                </tr>
            </table>
            <hr />
            <table width="100%">
                <tr>
                    <td class="mrrAlignRight">
                        <%--Buttons Row--%>
                        <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <asp:Button ID="btnRemove" Text="Remove" runat="server" CssClass="Button_Standard Button_Remove" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <%--<br />--%>
                        <%--Table starts--%>
                        <asp:Repeater ID="rptMain" runat="server" EnableViewState="true">
                            <HeaderTemplate>
                                <table class="mrrDataTable">
                                    <tr>
                                        <th>
                                            From*
                                        </th>
                                        <th>
                                            To*
                                        </th>
                                        <th>
                                            Fleet Model
                                        </th>
                                        <%--rev:mia             Sept 30, 2013 - addition of Exlude Fleet Model--%>
                                        <th>Exclude <br />Fleet<br /> Model</th>
                                        <th>
                                            From*
                                        </th>                                        
                                        <th>
                                            To*
                                        </th>
                                        <th>
                                            Dur<br />(Days)*
                                        </th>
                                        <th>
                                            Fixed Cost*
                                        </th>
                                        <th>
                                            Type*
                                        </th>
                                        <th nowrap="nowrap">
                                            Avl/Sch<br />
                                            Ticked = Both<br />
                                            <div align="center">
                                                <input type="checkbox" id="chkAllAvl" onclick="ToggleSelectAllAgents(7);" />
                                            </div>
                                        </th>
                                        <th style="text-align: center">
                                            Remove<br />
                                            <input type="checkbox" id="chkAllRemove" onclick="ToggleSelectAllAgents(8);" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="evenRow">
                                    <td nowrap>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("ID") %>' />
                                        <asp:HiddenField ID="hdnIntegrityNo" runat="server" Value='<%# Bind("IntegrityNo") %>' />
                                        <ucPickerControl:PickerControl ID="pkrLocationFrom" runat="server" PopupType="LOCATIONFORCOUNTRY" 
                                            Width="40px" Text='<%# Bind("LocFrom") %>' DataId='<%# Bind("LocFrom") %>' />
                                    </td>
                                    <td nowrap>
                                        <ucPickerControl:PickerControl ID="pkrLocationTo" runat="server" PopupType="LOCATIONFORCOUNTRY" 
                                            Width="40px" Text='<%# Bind("LocTo") %>' DataId='<%# Bind("LocTo") %>' />
                                    </td>
                                    <td class="relocRuleModels">
                                        <uc1:MultiSelect ID="fleetModels" SelectType="FleetModels" DisplayKeys="false" Items='<%# GetMultiselectItems(eval("FleetModels")) %>' CountryCode="<%#Country %>"  runat="server" />
                                    </td>
                                    <%--rev:mia             Sept 30, 2013 - addition of Exlude Fleet Model--%>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkExcludeFleetMode"  Checked='<%# Bind("ExludeFleetModel") %>'/>
                                    </td>
                                    <td>
                                        <ucDateControl:DateControl ID="dtDateFrom" runat="server" Text='<%# Bind("StDt") %>' />
                                    </td>
                                    <td>
                                        <ucDateControl:DateControl ID="dtDateTo" runat="server" Text='<%# Bind("ToDt") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDays" runat="server" CssClass="mrrTinyText" MaxLength="5" onkeypress="return keyStrokeInt(event);"
                                            OnBlur="CheckContents(this,'0');" Text='<%# Bind("Days") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFixedCost" runat="server" CssClass="mrrSmallText mrrAlignRight"
                                            MaxLength="8" onkeypress="return keyStrokeDecimal(event);" onblur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                            Text='<%# Bind("FixCost") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cmbRelocationType" runat="server" Width="100px">
                                        </asp:DropDownList>
                                        <asp:HiddenField ID="hdnRelocationType" runat="server" Value='<%# Bind("Type") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkAvlSch" runat="server" Checked='<%# Eval("Avl") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkRemove" runat="server" Checked='<%# Eval("Remove") %>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="oddRow">
                                    <td nowrap>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("ID") %>' />
                                        <asp:HiddenField ID="hdnIntegrityNo" runat="server" Value='<%# Bind("IntegrityNo") %>' />
                                        <ucPickerControl:PickerControl ID="pkrLocationFrom" runat="server" PopupType="LOCATIONFORCOUNTRY" 
                                            Width="40px" Text='<%# Bind("LocFrom") %>' DataId='<%# Bind("LocFrom") %>' />
                                    </td>
                                    <td nowrap>
                                        <ucPickerControl:PickerControl ID="pkrLocationTo" runat="server" PopupType="LOCATIONFORCOUNTRY" 
                                            Width="40px" Text='<%# Bind("LocTo") %>' DataId='<%# Bind("LocTo") %>' />
                                    </td>
                                    <td class="relocRuleModels">
                                        <uc1:MultiSelect ID="fleetModels" SelectType="FleetModels" DisplayKeys="false" Items='<%# GetMultiselectItems(eval("FleetModels")) %>' CountryCode="<%#Country %>"  runat="server" />
                                    </td>
                                    <%--rev:mia             Sept 30, 2013 - addition of Exlude Fleet Model--%>
                                    <td>
                                        <asp:CheckBox runat="server" ID="chkExcludeFleetMode"  Checked='<%# Bind("ExludeFleetModel") %>'/>
                                    </td>
                                    <td>
                                        <ucDateControl:DateControl ID="dtDateFrom" runat="server" Text='<%# Bind("StDt") %>' />
                                    </td>
                                    <td>
                                        <ucDateControl:DateControl ID="dtDateTo" runat="server" Text='<%# Bind("ToDt") %>' />
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtDays" runat="server" CssClass="mrrTinyText" MaxLength="5" onkeypress="return keyStrokeInt(event);"
                                            OnBlur="CheckContents(this,'0');" Text='<%# Bind("Days") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtFixedCost" runat="server"  CssClass="mrrSmallText mrrAlignRight"
                                            MaxLength="8" onkeypress="return keyStrokeDecimal(event);" onblur="this.value=trimDecimal(this.value,2);CheckContents(this,'0.00');"
                                            Text='<%# Bind("FixCost") %>'></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdnRelocationType" runat="server" Value='<%# Bind("Type") %>' />
                                        <asp:DropDownList ID="cmbRelocationType" runat="server" Width="100px">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkAvlSch" runat="server" Checked='<%# Eval("Avl") %>' />
                                    </td>
                                    <td style="text-align: center">
                                        <asp:CheckBox ID="chkRemove" runat="server" Checked='<%# Eval("Remove") %>' />
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                        <%--Table ends--%>
                        <%--<br />--%>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <%--Buttons Row--%>
                        <asp:Button ID="btnSave1" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel1" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <asp:Button ID="btnRemove1" Text="Remove" runat="server" CssClass="Button_Standard Button_Remove" />
                    </td>
                </tr>
            </table>
            <ucConfirmation:ConfirmationBoxControl ID="cnfConfirmation" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
