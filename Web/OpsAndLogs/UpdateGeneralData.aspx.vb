Imports Aurora.OpsAndLogs.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.UpdateGeneralData)> _
<AuroraPageTitle("Update General Data")> _
Partial Class OpsAndLogs_UpdateGeneralData
    Inherits AuroraPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.odometerButton.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.OdometerChange)
        Me.odometerPanel.Enabled = Me.odometerButton.Enabled
        Me.undoCheckOutButton.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UndoCheckOut)
        Me.undoCheckInButton.Enabled = Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UndoCheckin)
        Me.undoCheckOutInPanel.Enabled = Me.undoCheckOutButton.Enabled OrElse Me.undoCheckInButton.Enabled
    End Sub

    Private Sub ShowResult(ByVal result As String)
        Dim message As String = result.Substring(result.IndexOf("/") + 1)
        If Not result.Contains("SUCCESS") Then
            Me.SetErrorShortMessage(message)
        Else
            Me.SetInformationShortMessage(message)
        End If
    End Sub

    Protected Sub odometerButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles odometerButton.Click
        If Not unitNumberTextBox.IsTextValid Then
            Me.SetWarningShortMessage(unitNumberTextBox.ErrorMessage)
        ElseIf Not odoMeterTextBox.IsTextValid Then
            Me.SetWarningShortMessage(odoMeterTextBox.ErrorMessage)
        ElseIf Me.GetFunctionPermission(AuroraFunctionCodeAttribute.OdometerChange) Then
            ShowResult(UpdateGeneralData.UpdateOdoMeter(unitNumberTextBox.Text.Trim(), odoMeterTextBox.Text.Trim(), Me.UserCode, Me.PrgmName))
        End If
    End Sub

    Protected Sub undoCheckOutButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles undoCheckOutButton.Click, undoCheckInButton.Click
        If Not bookingRefTextBox.IsTextValid() Then
            Me.SetWarningShortMessage(bookingRefTextBox.ErrorMessage)
        ElseIf sender Is undoCheckOutButton AndAlso Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UndoCheckOut) Then
            ShowResult(UpdateGeneralData.UndoCheckOut(bookingRefTextBox.Text, Me.UserCode, Me.PrgmName))
        ElseIf sender Is undoCheckInButton AndAlso Me.GetFunctionPermission(AuroraFunctionCodeAttribute.UndoCheckin) Then
            ShowResult(UpdateGeneralData.UndoCheckIn(bookingRefTextBox.Text.Trim, Me.UserCode))
        End If
    End Sub

End Class
