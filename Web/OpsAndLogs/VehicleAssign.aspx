<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	VehicleAssign.aspx
''	Date Created	-	?.?.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	?.?.8 / Shoel - Base version
''                      25.8.8 / Shoel - Field validation added, Label doesn't go nuts
''                      23.9.8 / Shoel - Changes for SQUISH # 620 
''                      9.10.8 / Shoel - now called also from sched vwr!!! :D
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="VehicleAssign.aspx.vb" Inherits="OpsAndLogs_VehicleAssign" Title="Untitled Page" %>

<%@ Register Src="../UserControls/RegExTextBox/RegExTextBox.ascx" TagName="RegExTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">
       function TrimNoOfChar(field,maxLength)
        {
	        if(field.value.length > maxLength)
	        {
		        setWarningShortMessage("Maximum number of character for details reached");
		        field.value = field.value.substring(0,maxLength);
	        }	
        }
    </script>

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel runat="server" ID="updMain">
        <ContentTemplate>
            <div id="divInitial" runat="server">
                <table cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td width="150">
                            Booking Reference:</td>
                        <td>
                            <uc1:RegExTextBox ID="searchBookingRentalNoTextBox" runat="server" Nullable="false"
                                ErrorMessage="Specify a valid booking reference" RegExPattern="^\s*\w+/\d+\s*$"
                                Width="100px" Text="" MaxLength="14" />
                            &nbsp;*&nbsp;&nbsp;e.g.&nbsp;3000140/1
                        </td>
                        <td align="right">
                            <asp:Button ID="showDetailsButton" runat="server" Text="Show Details" CssClass="Button_Standard Button_Search" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="selectedBookingPanel" runat="server" style="display: none">
                <hr />
                <b>Selected Booking</b>
                <table cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td width="150">
                            Booking Reference:</td>
                        <td width="250">
                            <asp:TextBox ID="bookingRentalNoTextBox" runat="server" ReadOnly="true" Width="100px"
                                />
                        </td>
                        <td width="150">
                            Assign to Vehicle:</td>
                        <td>
                            <asp:TextBox ID="vehicleTextBox" runat="server" Width="100px" MaxLength="20" />
                            &nbsp;*
                        </td>
                    </tr>
                </table>
                <hr style="border-style: dotted" />
                <b>Notes</b>
                <asp:Repeater ID="vehicleAssignmentRepeater" runat="server" EnableViewState="true">
                    <HeaderTemplate>
                        <table class="dataTable" cellspacing="0" cellpadding="2" width="100%" border="0">
                            <tr>
                                <th width="40">
                                    &nbsp;</th>
                                <th width="*">
                                    Description</th>
                                <th width="50">
                                    Priority</th>
                                <th width="75">
                                    Audience</th>
                                <th width="50">
                                    Active</th>
                            </tr>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr id="listTableRow" runat="server" valign="top">
                            <td>
                                <asp:LinkButton ID="listEditButton" Text="Edit" runat="server" CommandName="edit"
                                    Visible="False" OnCommand="listEditButton_Command" />
                            </td>
                            <td>
                                <asp:Label ID="listDescriptionLabel" runat="server" Style="display: block; float: left;
                                    width: 500px; word-wrap: break-word" />
                            </td>
                            <td>
                                <asp:Label ID="listPriorityLabel" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="listAudienceTypeLabel" runat="server" />
                            </td>
                            <td>
                                <asp:Label ID="listActiveLabel" runat="server" />
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <hr style="border-style: dotted" />
                <b>
                    <asp:Label ID="noteLabel" runat="server">New Note</asp:Label></b>
                <asp:HiddenField ID="integrityNoHiddenField" runat="server" Value="1" />
                <asp:HiddenField ID="noteIdHiddenField" runat="server" Value="" />
                <table cellspacing="0" cellpadding="2" width="100%">
                    <tr>
                        <td width="150">
                            Priority:</td>
                        <td width="150">
                            <asp:DropDownList ID="priorityDropDown" runat="server" Width="50px">
                                <asp:ListItem>1</asp:ListItem>
                                <asp:ListItem>2</asp:ListItem>
                                <asp:ListItem>3</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="150">
                            Audience:</td>
                        <td>
                            <asp:DropDownList ID="audienceDropDown" runat="server" Width="100px" />
                        </td>
                        <td>
                            &nbsp;</td>
                        <td>
                            <asp:CheckBox ID="activeCheckBox" runat="server" Text="Active" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" style="padding-top: 5px">
                            Description:</td>
                        <td colspan="5">
                            <asp:TextBox ID="descriptionTextBox" runat="server" Width="100%" TextMode="MultiLine"
                                Rows="3" MaxLength="300" OnBlur="TrimNoOfChar(this,300);" />
                        </td>
                    </tr>
                </table>
                <hr style="border-style: dotted" />
                <div style="float: right">
                    <asp:Button ID="newNoteButton" runat="server" Text="New Note" CssClass="Button_Standard Button_New" />
                    <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                    <asp:Button ID="undoButton" runat="server" Text="Undo" CssClass="Button_Standard" />
                    <asp:Button ID="cancelButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                    <input id="btnClose" style="display: none" type="Button" runat="server" value="Close"
                        class="Button_Standard Button_Cancel" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
