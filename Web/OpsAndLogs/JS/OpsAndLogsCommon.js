//  Change Log!
//  25.8.8 / Shoel - MaxLengths added to some fields
//

function ToggleTableColumnCheckboxes(col, evt)
{
    if (!evt) evt = window.event;
    if (!evt) return;

    var chkAll = evt.srcElement ? evt.srcElement : evt.target;
    if (!chkAll) return;
    
    var tbody = chkAll;
    while (tbody && (tbody.tagName != 'TBODY'))
        tbody = tbody.parentNode;
    if (!tbody) return;
    
    var tableRows = tbody.getElementsByTagName("TR");
    for (var i = 0; i < tableRows.length; i++)
    {
        var tableCell = tableRows[i].getElementsByTagName ("TD")[col];
        if (!tableCell) continue;
        
        var inputElements = tableCell.getElementsByTagName ("INPUT");
        for (var j = 0; j < inputElements.length; j++)
        {
            var inputElement = inputElements[j];
            if (inputElement.type == 'checkbox')
                inputElement.checked = chkAll.checked;
        }
    }
}

function keyStrokeInt(evt)
{ 
    if (!window.event) return true; // give up, its too hard to test for control keys in FF (IE doesnt pass them)
    
    evt = window.event;

    return !(evt.keyCode < 48 || evt.keyCode > 58);
}

function keyStrokeDecimal(evt)
{ 
    if (!window.event) return true; // give up, its too hard to test for control keys in FF (IE doesnt pass them)

    evt = window.event;

    return !((evt.keyCode < 48 || evt.keyCode > 58) && (evt.keyCode != 46))
}

function ChangeTitle(NewTitle,LabelId)
{
    document.getElementById(LabelId).innerText = NewTitle;
}

function CheckContents(field,defaultValue)
{
    if(isNaN(field.value))
    {
        if(defaultValue == null)
        {
            field.value="";
        }
        else
        {
            field.value=defaultValue;
        }                
    }
}

//Phoenix Changes

function CheckPositiveDecimal(textBox,defaultValue)
{
    if(isNaN(parseFloat(textBox.value)))
    {
        textBox.value = defaultValue;
        return;
    }
    if( parseFloat(textBox.value) < 0 )
    {
        textBox.value = defaultValue;
    }
    else
    {
        textBox.value=trimDecimal(parseFloat(textBox.value),2);
    }
}


//Phoenix Changes
