Imports System.Xml
Imports System.Data
Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.AlternativeProductList)> _
Partial Class OpsAndLogs_AlternativeProductList
    Inherits AuroraPage

    Public Const VIEWSTATE_COUNTRY As String = "ViewState_APL_CountryCode"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If CStr(Request("CountryCode")) <> "" Then
                LoadMainTable(CStr(Request("CountryCode")))
            Else
                LoadMainTable(SearchParam)
            End If

        End If
    End Sub

    Sub LoadMainTable(ByVal Country As String)
        Dim xmlReturn As XmlDocument
        If Country = "" Then
            Country = CountryCode
        End If
        xmlReturn = Aurora.OpsAndLogs.Services.AlternativeProduct.GetAlternativeProductList(Country, UserCode)

        ' check for errors
        If xmlReturn.InnerText.Contains("GEN060") Then
            SetErrorShortMessage(xmlReturn.DocumentElement.InnerText)
            rptMain.DataSource = New DataTable
            rptMain.DataBind()
            Exit Sub
        End If

        ' load the upper stuff
        txtCountryOfOperation.Text = xmlReturn.DocumentElement.SelectSingleNode("Root/Country").Attributes("Name").InnerText
        ViewState(VIEWSTATE_COUNTRY) = xmlReturn.DocumentElement.SelectSingleNode("Root/Country").Attributes("Code").InnerText
        ' save ctycode to viewstate

        '' save to viewstate for future use
        'ViewState(VIEWSTATE_OLDXML) = xmlReturn.DocumentElement.SelectSingleNode("Root/Cost").OuterXml

        ' load the repeater

        Dim dstAlternativeProductList As New DataSet
        dstAlternativeProductList.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Root/AltPrdList")))
        If dstAlternativeProductList.Tables.Count > 0 Then
            rptMain.DataSource = dstAlternativeProductList.Tables(0)
            rptMain.DataBind()
        End If

    End Sub



    Protected Sub rptMain_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMain.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("lnkProduct"), LinkButton).Attributes.Add("OnClick", "return ManageAlternativeProduct('" & CStr(ViewState(VIEWSTATE_COUNTRY)) & "','" & CType(e.Item.FindControl("hdnID"), HiddenField).Value & "');")
            'CType(e.Item.FindControl("lnkProduct"), LinkButton).OnClientClick = "ManageAlternativeProduct('" & CStr(ViewState(VIEWSTATE_COUNTRY)) & "','" & CType(e.Item.FindControl("hdnID"), HiddenField).Value & "');"
        End If
    End Sub
End Class

