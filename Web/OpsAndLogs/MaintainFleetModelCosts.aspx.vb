Imports System.Xml
Imports System.Data
Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.MaintainFleetModelCosts)> _
<AuroraMessageAttribute("GEN005")> _
Partial Class OpsAndLogs_MaintainFleetModelCosts
    Inherits AuroraPage

    Const VIEWSTATE_OLDXML As String = "ViewState_MFMC_OldXml"
    Const VIEWSTATE_OPTION As String = "ViewState_MFMC_Option"
    Const VIEWSTATE_COUNTRY As String = "ViewState_MFMC_CtyCode"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            'Dim xmlRet
            'fetch combos
            LoadMainTable(SearchParam)
        End If
    End Sub

  

    Sub LoadMainTable(ByVal Country As String)
        Dim xmlReturn As XmlDocument
        If Country = "" Then
            Country = CountryCode
        End If
        xmlReturn = Aurora.OpsAndLogs.Services.FleetModelCosts.GetFleetModelCosts(Country, UserCode)

        ' check for errors
        If Not xmlReturn.DocumentElement.SelectSingleNode("Error/ErrStatus") Is Nothing Then
            If xmlReturn.DocumentElement.SelectSingleNode("Error/ErrStatus").InnerText.ToUpper() = "TRUE" Then
                SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/ErrDesc").InnerText)
                rptMain.DataSource = New DataTable
                rptMain.DataBind()
                ' disable buttons
                btnSave.Enabled = False
                btnSave1.Enabled = False
                btnCancel.Enabled = False
                btnCancel1.Enabled = False
               
                Exit Sub
            End If
        End If


        ' load the upper stuff
        txtCountryOfOperation.Text = xmlReturn.DocumentElement.SelectSingleNode("Root/Country").Attributes("Name").InnerText
        ViewState(VIEWSTATE_COUNTRY) = xmlReturn.DocumentElement.SelectSingleNode("Root/Country").Attributes("Code").InnerText
        ' save ctycode to viewstate

        ' save to viewstate for future use
        ViewState(VIEWSTATE_OLDXML) = xmlReturn.DocumentElement.SelectSingleNode("Root/Cost").OuterXml

        ' load the repeater

        Dim dstFleetModelCosts As New DataSet
        dstFleetModelCosts.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Root/Cost")))
        If dstFleetModelCosts.Tables.Count > 0 Then
            rptMain.DataSource = dstFleetModelCosts.Tables(0)
            rptMain.DataBind()
        End If

    End Sub

    Protected Sub rptMain_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptMain.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

            CType(e.Item.FindControl("txtDailyCost"), TextBox).Text = String.Format("{0:f}", CDbl(CType(e.Item.FindControl("txtDailyCost"), TextBox).Text))
            CType(e.Item.FindControl("txtPenaltyCost"), TextBox).Text = String.Format("{0:f}", CDbl(CType(e.Item.FindControl("txtPenaltyCost"), TextBox).Text))

        End If
    End Sub

  

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel1.Click
        LoadMainTable(SearchParam)
    End Sub

   

    Function GetChangedRecords(Optional ByVal Confirmed As Boolean = False) As XmlDocument
        ' supposed to return only the modified/new rows (if the new row holds data)

        ' step 1 - get original xml

        Dim xmlOriginal As New XmlDocument
        xmlOriginal.LoadXml(ViewState(VIEWSTATE_OLDXML))

        ' step 2 - get xml from screen
        Dim sbNewXML As New StringBuilder
        sbNewXML.Append("<Cost>")

        ' add to xml and simultaneously validate
        'For Each oItem As RepeaterItem In rptMain.Items
        For i As Integer = 0 To rptMain.Items.Count - 1

            '<VehicleCosts>
            '  <VctId></VctId>
            '  <ModelId>1185</ModelId>
            '  <FleetMdl>2BMA - Toyota Hiace SLWB - Auto</FleetMdl>
            '  <VarDailyCost>0.0000</VarDailyCost>
            '  <PenaltyCost>0.0000</PenaltyCost>
            '  <IsReloc>1</IsReloc>
            '  <OneWayHire>1</OneWayHire>
            '  <Active>1</Active>
            '  <IntegrityNo>0</IntegrityNo>
            '</VehicleCosts>


            sbNewXML.Append("<VehicleCosts>")

            sbNewXML.Append("<VctId>" & CType(rptMain.Items(i).FindControl("hdnVctId"), HiddenField).Value & "</VctId>")
            sbNewXML.Append("<ModelId>" & CType(rptMain.Items(i).FindControl("hdnModelId"), HiddenField).Value & "</ModelId>")
            sbNewXML.Append("<FleetMdl>" & CType(rptMain.Items(i).FindControl("hdnFleetModel"), HiddenField).Value & "</FleetMdl>")

            sbNewXML.Append("<VarDailyCost>" & CType(rptMain.Items(i).FindControl("txtDailyCost"), TextBox).Text & "</VarDailyCost>")
            sbNewXML.Append("<PenaltyCost>" & CType(rptMain.Items(i).FindControl("txtPenaltyCost"), TextBox).Text & "</PenaltyCost>")

            If (CType(rptMain.Items(i).FindControl("chkIsReloc"), CheckBox).Checked) Then
                sbNewXML.Append("<IsReloc>1</IsReloc>")
            Else
                sbNewXML.Append("<IsReloc>0</IsReloc>")
            End If

            If (CType(rptMain.Items(i).FindControl("chkOneWayHire"), CheckBox).Checked) Then
                sbNewXML.Append("<OneWayHire>1</OneWayHire>")
            Else
                sbNewXML.Append("<OneWayHire>0</OneWayHire>")
            End If

            If (CType(rptMain.Items(i).FindControl("chkActive"), CheckBox).Checked) Then
                sbNewXML.Append("<Active>1</Active>")
            Else
                sbNewXML.Append("<Active>0</Active>")
            End If

            sbNewXML.Append("<IntegrityNo>" & CType(rptMain.Items(i).FindControl("hdnIntegrityNo"), HiddenField).Value & "</IntegrityNo>")

            sbNewXML.Append("</VehicleCosts>")
        Next

        sbNewXML.Append("</Cost>")

        Dim xmlNew, xmlChanges As New XmlDocument
        xmlNew.LoadXml(sbNewXML.ToString())
        xmlChanges.LoadXml("<Cost/>")


        '<VehicleCosts>
        '  <VctId></VctId>
        '  <ModelId>1185</ModelId>
        '  <FleetMdl>2BMA - Toyota Hiace SLWB - Auto</FleetMdl>
        '  <VarDailyCost>0.0000</VarDailyCost>
        '  <PenaltyCost>0.0000</PenaltyCost>
        '  <IsReloc>1</IsReloc>
        '  <OneWayHire>1</OneWayHire>
        '  <Active>1</Active>
        '  <IntegrityNo>0</IntegrityNo>
        '</VehicleCosts>

        ' step 3 - create the updated version xml
        For i As Integer = 0 To xmlOriginal.DocumentElement.ChildNodes.Count - 1

            If Not (xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(0).InnerText = xmlNew.DocumentElement.ChildNodes(i).ChildNodes(0).InnerText _
            And xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(1).InnerText = xmlNew.DocumentElement.ChildNodes(i).ChildNodes(1).InnerText _
            And xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(2).InnerText = xmlNew.DocumentElement.ChildNodes(i).ChildNodes(2).InnerText _
            And CDbl(xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(3).InnerText) = CDbl(xmlNew.DocumentElement.ChildNodes(i).ChildNodes(3).InnerText) _
            And CDbl(xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(4).InnerText) = CDbl(xmlNew.DocumentElement.ChildNodes(i).ChildNodes(4).InnerText) _
            And CBool(xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(5).InnerText) = CBool(xmlNew.DocumentElement.ChildNodes(i).ChildNodes(5).InnerText) _
            And CBool(xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(6).InnerText) = CBool(xmlNew.DocumentElement.ChildNodes(i).ChildNodes(6).InnerText) _
            And CBool(xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(7).InnerText) = CBool(xmlNew.DocumentElement.ChildNodes(i).ChildNodes(7).InnerText) _
            And Int(xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(8).InnerText) = CInt(xmlNew.DocumentElement.ChildNodes(i).ChildNodes(8).InnerText)) _
            Then
                ' something has changed
                ' step 4 - check for deactivations, if something was active first and now is inactive
                If Not Confirmed Then ' if confirmed then ignore this
                    If xmlOriginal.DocumentElement.ChildNodes(i).ChildNodes(7).InnerText = "1" And xmlNew.DocumentElement.ChildNodes(i).ChildNodes(7).InnerText = "0" Then
                        cnfConfirmation.Title = "Confirmation Required"
                        cnfConfirmation.Text = "Deactivating a Fleet Model Cost may have severe consequences on DVASS' scheduling optimiser"
                        ViewState(VIEWSTATE_OPTION) = "Deactivate"
                        cnfConfirmation.Show()
                        GetChangedRecords = New XmlDocument
                        Exit Function
                    End If
                End If
                ' append!
                xmlChanges.DocumentElement.InnerXml = xmlChanges.DocumentElement.InnerXml & xmlNew.DocumentElement.ChildNodes(i).OuterXml


            End If

            'If Not xmlOriginal.DocumentElement.ChildNodes(i).InnerXml = xmlNew.DocumentElement.ChildNodes(i).InnerXml Then
            '    ' has been changed, add to the updated 


            '    ' checking for changes

            '    'xmlChanges.DocumentElement.InnerXml = xmlChanges.DocumentElement.InnerXml & xmlNew.DocumentElement.ChildNodes(i).OuterXml
            'End If
        Next

        If xmlChanges.DocumentElement.ChildNodes.Count = 0 Then
            SetErrorShortMessage("No changes have been made")
            GetChangedRecords = New XmlDocument
            Exit Function
        End If

        ' so far so good! this must be valid
        Return xmlChanges

    End Function


    Sub SaveChanges(Optional ByVal Confirmed As Boolean = False)
        ' validate data
        Dim xmlChanges As New XmlDocument
        xmlChanges = GetChangedRecords(Confirmed)

        'check if its ok
        If xmlChanges.OuterXml.Length = 0 Then
            ' nothing inside! exit!
            Exit Sub
        End If

        Dim xmlScreenData As New XmlDocument
        xmlScreenData.LoadXml("<Root><Country Code=""" & CStr(ViewState(VIEWSTATE_COUNTRY)) & """ Name=""" & txtCountryOfOperation.Text.Trim() & """/>" & xmlChanges.OuterXml & _
        "<UsrId>" & UserCode & "</UsrId><PrgmName>MaintainFleetModelCosts.aspx</PrgmName></Root>")

        Dim xmlReturn As XmlDocument
        xmlReturn = Aurora.OpsAndLogs.Services.FleetModelCosts.UpdateFleetModelCosts(xmlScreenData)

        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' refresh the data
            LoadMainTable(CStr(ViewState(VIEWSTATE_COUNTRY)))
        Else
            ' theres an error!
            SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If

    End Sub

    ' ViewState(VIEWSTATE_OPTION) 
    Protected Sub Confirmation_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cnfConfirmation.PostBack

        If leftButton Then
            If CStr(ViewState(VIEWSTATE_OPTION)) = "Deactivate" Then
                ViewState(VIEWSTATE_OPTION) = Nothing ' resetting this!
                SaveChanges(True)
            End If
        End If
    End Sub



    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave1.Click, btnSave.Click
        SaveChanges(False)
    End Sub
End Class
