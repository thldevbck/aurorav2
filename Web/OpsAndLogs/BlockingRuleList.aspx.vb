Imports System
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.ComponentModel
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Common
Imports Aurora.OpsAndLogs.Data
Imports Aurora.OpsAndLogs.Data.BlockingRuleDataSet
Imports Aurora.Booking.Services

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.MaintainBlockingRules)> _
Partial Class OpsAndLogs_BlockingRuleList
    Inherits AuroraPage

    Public Const Country_CookieName As String = "OL-BLKRULLST_Country"
    Public Const Selected_CookieName As String = "OL-BLKRULLST_Selected"

    Private _blockingRuleDataSet As New BlockingRuleDataSet()

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        Dim countryCode As String = Me.Request(countryDropDown.ClientID.Replace("_", "$"))
        If String.IsNullOrEmpty(countryCode) Then
            countryCode = CookieValue(Country_CookieName)
        End If
        If String.IsNullOrEmpty(countryCode) Then
            countryCode = Me.CountryCode
        End If

        DataRepository.GetBlockingRuleLookups(_blockingRuleDataSet, Me.CompanyCode, countryCode)

        ''-------------------------------------------------------------------
        ''rev:mia Dec.7 2009 - dynamic countries based on the userprofile
        'For Each countryRow As CountryRow In _blockingRuleDataSet.Country
        '    If Not countryRow.IsCtyHasProductsNull AndAlso countryRow.CtyHasProducts Then
        '        If Me.CompanyCode.ToUpper <> "MAC" Then
        '            ''rev:mia Dec: 2,2009 display only AU and NZ since THL operates only on these countries
        '            If (countryRow.CtyCode = "NZ" Or countryRow.CtyCode = "AU") Then
        '                countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        '            End If
        '        Else
        '            countryDropDown.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
        '        End If
        '    End If
        'Next
        Dim dt As System.Data.DataTable
        dt = Booking.GetCountryData(UserCode)
        For Each item As System.Data.DataRow In dt.Rows
            countryDropDown.Items.Add(New ListItem(item("CtyCode").ToString & " - " & item("CtyName").ToString, item("CtyCode").ToString))
        Next
        ''-------------------------------------------------------------------

        If countryDropDown.Items.FindByValue(countryCode) IsNot Nothing Then
            countryDropDown.SelectedValue = countryCode
        End If

        If Not String.IsNullOrEmpty(CookieValue(Selected_CookieName)) Then
            Try
                statusRadioButtonList.SelectedValue = CookieValue(Selected_CookieName)
            Catch
            End Try
        End If
    End Sub

    Private Sub SetTableRowRuleRow(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs, _
     ByVal text As String, ByVal isIncl As Boolean, ByVal tableRowId As String, ByVal labelId As String)
        Dim productsTableRow As TableRow = e.Item.FindControl(tableRowId)
        Dim productsTableCell0 As TableCell = productsTableRow.Cells(0)
        Dim productsTableCell1 As TableCell = productsTableRow.Cells(1)
        Dim productsLabel As Label = e.Item.FindControl(labelId)

        If (Not String.IsNullOrEmpty(text)) Then
            productsTableCell0.ForeColor = IIf(isIncl, DataConstants.IncludeForeColor, DataConstants.ExcludeForeColor)
            productsTableCell1.ForeColor = productsTableCell0.ForeColor
            productsLabel.Text = Server.HtmlEncode(text) & IIf(isIncl, "", " &nbsp;&nbsp; (Exclude)")
            productsTableRow.Visible = Not String.IsNullOrEmpty(text)
        Else
            productsTableRow.Visible = False
        End If
    End Sub

    Protected Sub blockingRuleRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles blockingRuleTypeRepeater.ItemDataBound
        If (e.Item.ItemType <> ListItemType.Item AndAlso e.Item.ItemType <> ListItemType.AlternatingItem) _
         OrElse Not TypeOf e.Item.DataItem Is BlockingRuleRow Then
            Return
        End If

        Dim blockingRuleRow As BlockingRuleRow = e.Item.DataItem

        Dim messageHyperLink As HyperLink = e.Item.FindControl("messageHyperLink")
        Dim blockingRuleTableRow As Literal = e.Item.FindControl("blockingRuleTableRow")
        Dim blockingRuleTableRow2 As Literal = e.Item.FindControl("blockingRuleTableRow2")
        Dim periodLabel As Label = e.Item.FindControl("periodLabel")
        Dim durationLabel As Label = e.Item.FindControl("durationLabel")
        Dim bookAheadLabel As Label = e.Item.FindControl("bookAheadLabel")
        Dim uomLabel As Label = e.Item.FindControl("uomLabel")
        Dim dateByLabel As Label = e.Item.FindControl("dateByLabel")
        Dim overrideLabel As Label = e.Item.FindControl("overrideLabel")
        Dim flexLabel As Label = e.Item.FindControl("flexLabel")

        ''REV:MIA OCT302009
        Dim tandcLabel As Label = e.Item.FindControl("tandcLabel")



        If Not String.IsNullOrEmpty(blockingRuleRow.BlrReason) Then
            messageHyperLink.Text = Server.HtmlEncode(blockingRuleRow.BlrReason)
        Else
            messageHyperLink.Text = Server.HtmlEncode("(No Reason)")
        End If
        messageHyperLink.NavigateUrl = "BlockingRuleEdit.aspx" _
         & "?BlrId=" & Server.UrlEncode(blockingRuleRow.BlrId) _
         & "&BlrAllow=" & Server.UrlEncode(blockingRuleRow.BlrAllow) _
         & "&CtyCode=" & Server.UrlEncode(countryDropDown.SelectedValue)

        blockingRuleTableRow.Text = "<tr style=""background-color:"
        If blockingRuleRow.BlrDateTo < Date.Now.Date Then
            blockingRuleTableRow.Text &= System.Drawing.ColorTranslator.ToHtml(DataConstants.PastColor)
        ElseIf blockingRuleRow.BlrDateFrom > Date.Now.Date Then
            blockingRuleTableRow.Text &= System.Drawing.ColorTranslator.ToHtml(DataConstants.FutureColor)
        Else
            blockingRuleTableRow.Text &= System.Drawing.ColorTranslator.ToHtml(DataConstants.CurrentColor)
        End If
        blockingRuleTableRow.Text &= """>"
        blockingRuleTableRow2.Text = blockingRuleTableRow.Text

        periodLabel.Text = Server.HtmlEncode(blockingRuleRow.BlrDateFrom.ToString(Aurora.Common.UserSettings.Current.ComDateFormat) & " - " & blockingRuleRow.BlrDateTo.ToString(Aurora.Common.UserSettings.Current.ComDateFormat))
        durationLabel.Text = blockingRuleRow.BlrDurationOfRental.ToString()
        bookAheadLabel.Text = blockingRuleRow.BlrBookAheadPeriod.ToString()
        If blockingRuleRow.CodeRow IsNot Nothing Then
            uomLabel.Text = blockingRuleRow.CodeRow.CodCode
        End If

        If Not blockingRuleRow.IsBlrApplyToDeptNull Then
            dateByLabel.Text = Server.HtmlEncode(blockingRuleRow.BlrApplyToDept)
        End If
        overrideLabel.Text = IIf(Not blockingRuleRow.IsBlrOverrideableNull AndAlso blockingRuleRow.BlrOverrideable, "Y", "&nbsp;")

        ''REV:MIA OCT302009
        Try
            tandcLabel.Text = IIf(Convert.ToBoolean(blockingRuleRow.BlrIsTandC) = True, "Y", "&nbsp;")
        Catch ex As Exception
            tandcLabel.Text = "&nbsp;"
        End Try
        


        flexLabel.Text = IIf(Not blockingRuleRow.IsBlrOnlyFlexBkgNull AndAlso blockingRuleRow.BlrOnlyFlexBkg, "Y", "&nbsp;")
        ' If Not blockingRuleRow.IsBlrBypassNull AndAlso blockingRuleRow.BlrBypass Then flags.Add("Bypass Availability Check")

        SetTableRowRuleRow(e, blockingRuleRow.ProductDescription, blockingRuleRow.ProductIsIncl, "productsTableRow", "productsLabel")
        SetTableRowRuleRow(e, blockingRuleRow.BrandDescription, blockingRuleRow.BrandIsIncl, "brandsTableRow", "brandsLabel")
        SetTableRowRuleRow(e, blockingRuleRow.PackageDescription, blockingRuleRow.PackageIsIncl, "packagesTableRow", "packagesLabel")
        SetTableRowRuleRow(e, blockingRuleRow.LocationFromDescription, blockingRuleRow.LocationFromIsIncl, "locationsFromTableRow", "locationsFromLabel")
        SetTableRowRuleRow(e, blockingRuleRow.LocationToDescription, blockingRuleRow.LocationToIsIncl, "locationsToTableRow", "locationsToLabel")
        SetTableRowRuleRow(e, blockingRuleRow.AgentCountryDescription, blockingRuleRow.AgentCountryIsIncl, "agentCountriesTableRow", "agentCountriesLabel")
        SetTableRowRuleRow(e, blockingRuleRow.AgentCategoryDescription, blockingRuleRow.AgentCategoryIsIncl, "agentCategoriesTableRow", "agentCategoriesLabel")
        SetTableRowRuleRow(e, blockingRuleRow.AgentDescription, blockingRuleRow.AgentIsIncl, "agentsTableRow", "agentsLabel")
    End Sub

    Protected Sub blockingRuleTypeRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles blockingRuleTypeRepeater.ItemDataBound
        If e.Item.ItemType <> ListItemType.Item AndAlso e.Item.ItemType <> ListItemType.AlternatingItem Then
            Return
        End If

        Dim allow As Boolean = e.Item.DataItem

        Dim typeLabel As Label = e.Item.FindControl("typeLabel")
        Dim createButton As Button = e.Item.FindControl("createButton")
        Dim blockingRuleRepeater As Repeater = e.Item.FindControl("blockingRuleRepeater")

        typeLabel.Text = Server.HtmlEncode(IIf(allow, "Allowing Rules", "Blocking Rules"))
        createButton.Attributes.Add("onclick", "window.location='BlockingRuleEdit.aspx" _
         & "?BlrAllow=" & allow.ToString().ToLower() _
         & "&CtyCode=" & Server.UrlEncode(countryDropDown.SelectedValue) & "'; return false;")

        Dim blockingRules As New List(Of BlockingRuleRow)
        For Each blockingRuleRow As BlockingRuleRow In _blockingRuleDataSet.BlockingRule
            If blockingRuleRow.BlrAllow = allow AndAlso _
             (statusRadioButtonList.SelectedIndex <> 0 OrElse blockingRuleRow.BlrDateTo >= Date.Now.Date) Then
                Try
                    If (statusRadioButtonList.SelectedValue = "TCyes") Then
                        If (blockingRuleRow.BlrIsTandC) Then
                            blockingRules.Add(blockingRuleRow)
                        End If
                    Else
                        ''rev:mia dec 3,2009 filtering does not work on Current and future
                        Select Case statusRadioButtonList.SelectedIndex
                            Case 1 ''Current
                                If blockingRuleRow.BlrDateTo >= Date.Now.Date Then
                                    blockingRules.Add(blockingRuleRow)
                                End If
                            Case 2 ''All
                                blockingRules.Add(blockingRuleRow)
                            Case Else
                                blockingRules.Add(blockingRuleRow)
                        End Select

                    End If

                Catch ex As Exception
                End Try
            End If
        Next

        blockingRuleRepeater.DataSource = blockingRules
        AddHandler blockingRuleRepeater.ItemDataBound, AddressOf blockingRuleRepeater_ItemDataBound
        blockingRuleRepeater.DataBind()
    End Sub

    Private Sub BuildBlockingRule(ByVal useViewState As Boolean)
        DataRepository.GetBlockingRuleList(_blockingRuleDataSet, Me.CompanyCode, countryDropDown.SelectedValue)

        blockingRuleTypeRepeater.DataSource = New String() {False, True}
        blockingRuleTypeRepeater.DataBind()
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then Return

        BuildBlockingRule(False)
    End Sub

    Protected Sub countryDropDown_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles countryDropDown.SelectedIndexChanged
        CookieValue(Country_CookieName) = countryDropDown.SelectedValue
        BuildBlockingRule(True)
    End Sub

    Protected Sub statusRadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles statusRadioButtonList.SelectedIndexChanged
        CookieValue(Selected_CookieName) = statusRadioButtonList.SelectedValue
        BuildBlockingRule(True)
    End Sub

End Class
