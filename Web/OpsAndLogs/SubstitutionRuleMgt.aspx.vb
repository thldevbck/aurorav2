Option Strict On
Option Explicit On


'' Change log!
'' 19.9.8 - Shoel : Save and Remove functionality now actually work! fix for squish 611

Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services
Imports System.Data
Imports System.Xml

Imports ASP.usercontrols_multiselectcontrol_multiselect_ascx

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.MaintainProductAssignmentAlternatives)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductAssignmentAlternatives)> _
<AuroraMessageAttribute("GEN062,GEN121,GEN124")> _
Partial Class OpsAndLogs_SubstitutionRuleMgt
    Inherits AuroraPage

    Private productId As String
    Private Const SubstitutionRuleMgt_XmlDoc_ViewState As String = "SubstitutionRuleMgt_XmlDoc"
    Private refreshRow As Boolean = False

    Protected mLocations()() As MultiSelectItem

    Protected mCountryCode As String

    Private Property SubstitutionRuleMgt_XmlDoc() As XmlDocument
        Get
            Dim xmlDoc As New XmlDocument
            If Not String.IsNullOrEmpty(CStr(ViewState(SubstitutionRuleMgt_XmlDoc_ViewState))) Then
                xmlDoc.LoadXml(CStr(ViewState(SubstitutionRuleMgt_XmlDoc_ViewState)))
            Else
                If String.IsNullOrEmpty(productId) Then
                    'New
                    xmlDoc = SubstitutionRule.GetSubstitutionRuleProductList(CountryCode, "", "", "YES")
                    productPickerControl.Visible = True
                    productTextBox.Visible = False
                    'productLabel.Visible = False
                Else
                    'xmlDoc = SubstitutionRule.GetSubstitutionRuleProductList("AU", "", productId, "")
                    xmlDoc = SubstitutionRule.GetSubstitutionRuleProductList(CountryCode, "", productId, "")
                    productPickerControl.Visible = False
                    productTextBox.Visible = True
                    'productLabel.Visible = True
                End If
                ViewState(SubstitutionRuleMgt_XmlDoc_ViewState) = xmlDoc.InnerXml
            End If

            'Set view state
            Return xmlDoc

        End Get
        Set(ByVal value As XmlDocument)
            ViewState(SubstitutionRuleMgt_XmlDoc_ViewState) = value.InnerXml

        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mCountryCode = CountryCode

        Try

            'productId = "B3A026A7-7CA6-4FB5-89F0-CB0EBDF89D6B"
            'productId = "4CEB522B-78C5-4712-B05C-F7FFF291FE1B"

            If Not Request.QueryString("productId") Is Nothing Then
                productId = Request.QueryString("productId").ToString()
            End If

            applyButton.Text = "Apply To Selected" & vbCrLf & "Fleet Models"

            If Not Page.IsPostBack Then
                formDatabind()
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub formDatabind()

        Dim xmlDoc As New XmlDocument
        xmlDoc = SubstitutionRuleMgt_XmlDoc

        ConstructLocationsFromXml(xmlDoc)

        'If String.IsNullOrEmpty(productId) Then
        '    'New
        '    xmlDoc = SubstitutionRule.GetSubstitutionRuleProductList(CountryCode, "", "", "YES")
        '    productPickerControl.Visible = True
        '    productLabel.Visible = False
        'Else
        '    xmlDoc = SubstitutionRule.GetSubstitutionRuleProductList("AU", "", productId, "")
        '    productPickerControl.Visible = False
        '    productLabel.Visible = True
        'End If

        ''Set view state
        'ViewState(SubstitutionRuleMgt_XmlDoc_ViewState) = xmlDoc.InnerXml

        'Convert from xmlDoc to ds
        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(xmlDoc.DocumentElement))

        If ds.Tables(0).Rows.Count = 1 Then
            countryTextBox.Text = CStr(ds.Tables(0).Rows(0)("name"))
            'countryLabel.Text = ds.Tables(0).Rows(0)("name")
        End If

        If ds.Tables(3).Rows.Count > 0 Then
            productTextBox.Text = CStr(ds.Tables(3).Rows(0)("PrdDesc"))
            'productLabel.Text = ds.Tables(3).Rows(0)("PrdDesc")
        End If


        refreshRow = True

        productGridView.DataSource = ds.Tables(3)
        productGridView.DataBind()
        ' added by nimesh on 1st april to sort on fleet model and then on from date (DSD-836 Jira)
        ' end added by nimesh on 1st april

        'Insert an empty line in costGridView
        Dim dt As New DataTable
        Dim dr As DataRow
        dr = dt.NewRow
        dt.Rows.Add(dr)

        costGridView.DataSource = dt
        costGridView.DataBind()

    End Sub

    Protected Sub ConstructLocationsFromXml(ByVal document As XmlDocument)

        'Referencing nodes directly rather than with XPath for better performance

        'Cycling through all the rule period nodes and constructing location objects for each one

        'document.ChildNodes(0).ChildNodes(2).ChildNodes(0).ChildNodes(18).ChildNodes(0).ChildNodes(0)

        'All the substitution rule records
        Dim nodes As XmlNodeList = document.ChildNodes(0).ChildNodes(2).ChildNodes

        Dim locationDataArray(nodes.Count)() As MultiSelectItem

        Dim intIndex As Integer = 0

        For Each node As XmlNode In nodes

            'Getting the location collection node
            Dim locationListNode As XmlNode = node.ChildNodes(18)

            'Reading all the location nodes
            If locationListNode.NodeType = XmlNodeType.Text Then
                'No locations
                locationListNode.FirstChild.Value = "-1"
            Else
                Dim locationIndex As Integer = 0
                Dim dataItems(locationListNode.ChildNodes.Count) As MultiSelectItem
                For Each locationNode As XmlNode In locationListNode.ChildNodes
                    Dim item As New MultiSelectItem()

                    'Location Code
                    item.Key = locationNode.ChildNodes(0).FirstChild.Value
                    'Location Name
                    item.Value = locationNode.ChildNodes(1).FirstChild.Value

                    dataItems(locationIndex) = item

                    locationIndex += 1
                Next

                locationDataArray(intIndex) = dataItems

                locationListNode.RemoveAll()

                'Referencing the array entry for use in databinding
                locationListNode.AppendChild(document.CreateTextNode(intIndex.ToString()))
            End If

            intIndex += 1
        Next

        mLocations = locationDataArray

    End Sub

    Protected Function GetMultiselectItems(ByVal index As String) As MultiSelectItem()
        Dim intLocationIndex As Integer = Integer.Parse(index)

        If (intLocationIndex > -1) Then
            Return mLocations(intLocationIndex)
        End If

        Return Nothing
    End Function

    Protected Sub productGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles productGridView.RowCreated

        Try
            If e.Row.RowType = DataControlRowType.DataRow And refreshRow Then
                'If Not Page.IsPostBack And e.Row.RowType = DataControlRowType.DataRow Then

                Dim dataRow As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

                'typeDropDownList
                Dim typeDropDownList As DropDownList = DirectCast(e.Row.FindControl("typeDropDownList"), DropDownList)
                Dim srpAvl As String = CStr(dataRow("SrpAvl"))
                If (Not String.IsNullOrEmpty(srpAvl)) AndAlso srpAvl <> "Available" Then
                    typeDropDownList.SelectedValue = srpAvl
                End If

                'fromDateControl
                Dim fromDateControl As UserControls_DateControl = DirectCast(e.Row.FindControl("fromDateControl"), UserControls_DateControl)
                Dim srpStDt As String = CStr(dataRow("SrpStDt"))
                If Not String.IsNullOrEmpty(srpStDt) Then
                    fromDateControl.Date = Utility.ParseDateTime(srpStDt, Utility.SystemCulture)
                End If

                'toDateControl
                Dim toDateControl As UserControls_DateControl = DirectCast(e.Row.FindControl("toDateControl"), UserControls_DateControl)
                Dim srpEnDt As String = CStr(dataRow("SrpEnDt"))
                If Not String.IsNullOrEmpty(srpEnDt) Then
                    toDateControl.Date = Utility.ParseDateTime(srpEnDt, Utility.SystemCulture)
                End If

                'Vis Lvl
                Dim dt As DataTable
                dt = Aurora.Common.Service.GetPopUpData("CODCODE", "36", "", "", "", "", UserCode) 'Aurora.Common.Data.GetCodecodetype(25, "")

                dt.Rows.Add("", "", "")
                Dim visLvlDropDownList As DropDownList = DirectCast(e.Row.FindControl("visLvlDropDownList"), DropDownList)

                visLvlDropDownList.DataSource = dt
                visLvlDropDownList.DataTextField = "DESCRIPTION"
                visLvlDropDownList.DataValueField = "ID"
                visLvlDropDownList.DataBind()

                visLvlDropDownList.SelectedValue = CStr(dataRow("SrpVsLvl"))

            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

#Region "Button Events"

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Try

            If String.IsNullOrEmpty(productId) Then
                'new
                If productPickerControl.DataId.Trim().Equals(String.Empty) Then
                    SetShortMessage(AuroraHeaderMessageType.Warning, "Please Select the product")
                    Return
                End If

                If Aurora.Booking.Services.BookingProcess.GetProductID(productPickerControl.DataId, UserCode) = productPickerControl.DataId Then
                    SetShortMessage(AuroraHeaderMessageType.Warning, productPickerControl.DataId & " is not a valid Product")
                    Return
                End If
            End If

            'Validation for Date 
            If Not dateValidation() Then
                Return
            End If

            'Validation for Date Overlapping
            If Not dateOverLapping() Then
                Return
            End If

            'validateRequiredField
            If validateRequiredField() Then
                If saveSubRule() Then
                    ViewState(SubstitutionRuleMgt_XmlDoc_ViewState) = ""
                    formDatabind()
                End If
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while saving.")
        End Try
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        formDatabind()
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(".\SubstitutionRuleProductList.aspx")

    End Sub

    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click

        Dim isSelected As Boolean = False

        For Each r As GridViewRow In productGridView.Rows
            Dim checkedCheckBox As CheckBox = DirectCast(r.FindControl("checkedCheckBox"), CheckBox)

            If checkedCheckBox.Checked Then
                isSelected = True
            End If
        Next

        If Not isSelected Then
            SetShortMessage(AuroraHeaderMessageType.Warning, "Please Select the Record")
            Return
        End If

        confimationBoxControl.Text = GetMessage("GEN124")
        confimationBoxControl.Param = "Remove"
        confimationBoxControl.Show()

    End Sub

    Protected Sub applyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles applyButton.Click

        'isSelected?
        Dim isSelected As Boolean = False
        For Each r As GridViewRow In productGridView.Rows
            Dim checkedCheckBox As CheckBox = DirectCast(r.FindControl("checkedCheckBox"), CheckBox)

            If checkedCheckBox.Checked Then
                isSelected = True
            End If
        Next
        If Not isSelected Then
            SetShortMessage(AuroraHeaderMessageType.Warning, "You must tick the item(s) before you apply")
            Return
        End If

        'Validation
        Dim TextBox0 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox0"), TextBox)
        Dim TextBox1 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox1"), TextBox)
        Dim TextBox2 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox2"), TextBox)
        Dim TextBox3 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox3"), TextBox)
        Dim TextBox4 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox4"), TextBox)
        Dim TextBox5 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox5"), TextBox)
        Dim TextBox6 As TextBox = DirectCast(costGridView.Rows(0).FindControl("TextBox6"), TextBox)

        Dim costArray() As String = {Utility.ParseString(TextBox0.Text, ""), _
            Utility.ParseString(TextBox1.Text, ""), _
            Utility.ParseString(TextBox2.Text, ""), _
            Utility.ParseString(TextBox3.Text, ""), _
            Utility.ParseString(TextBox4.Text, ""), _
            Utility.ParseString(TextBox5.Text, ""), _
            Utility.ParseString(TextBox6.Text, "")}

        Dim cost0 As Decimal = Utility.ParseDecimal(TextBox0.Text, 0D)
        Dim cost1 As Decimal = Utility.ParseDecimal(TextBox1.Text, 0D)
        Dim cost2 As Decimal = Utility.ParseDecimal(TextBox2.Text, 0D)
        Dim cost3 As Decimal = Utility.ParseDecimal(TextBox3.Text, 0D)
        Dim cost4 As Decimal = Utility.ParseDecimal(TextBox4.Text, 0D)
        Dim cost5 As Decimal = Utility.ParseDecimal(TextBox5.Text, 0D)
        Dim cost6 As Decimal = Utility.ParseDecimal(TextBox6.Text, 0D)

        Dim tempCost As Decimal = 10000000D
        For Each s As String In costArray

            If Not String.IsNullOrEmpty(s) Then
                If Utility.ParseDecimal(s, 0D) > tempCost Then
                    SetWarningMessage("One or more Turnaround Costs has decreased rather than increased compared to other costs of lesser half days")
                    Return
                End If
                tempCost = Utility.ParseDecimal(s, 0D)
            End If
        Next

        Dim cost As String = ""
        If Not String.IsNullOrEmpty(TextBox0.Text) Then cost = cost & "0-$" & cost0 & ","
        If Not String.IsNullOrEmpty(TextBox1.Text) Then cost = cost & "1-$" & cost1 & ","
        If Not String.IsNullOrEmpty(TextBox2.Text) Then cost = cost & "2-$" & cost2 & ","
        If Not String.IsNullOrEmpty(TextBox3.Text) Then cost = cost & "3-$" & cost3 & ","
        If Not String.IsNullOrEmpty(TextBox4.Text) Then cost = cost & "4-$" & cost4 & ","
        If Not String.IsNullOrEmpty(TextBox5.Text) Then cost = cost & "5-$" & cost5 & ","
        If Not String.IsNullOrEmpty(TextBox6.Text) Then cost = cost & "6-$" & cost6 & ","

        If Not String.IsNullOrEmpty(cost) Then
            cost = cost.Substring(0, cost.Length - 1)
        End If

        For Each rr As GridViewRow In productGridView.Rows
            Dim checkedCheckBox As CheckBox = DirectCast(rr.FindControl("checkedCheckBox"), CheckBox)
            Dim costLabel As Label = DirectCast(rr.FindControl("costLabel"), Label)
            If checkedCheckBox.Checked Then
                costLabel.Text = cost
                checkedCheckBox.Checked = False
            End If
        Next

        'Dim allCheckBox As CheckBox = productGridView.FindControl("allCheckBox")
        'allCheckBox.Checked = False
        TextBox0.Text = "0.0"
        TextBox1.Text = ""
        TextBox2.Text = ""
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""

        SetInformationShortMessage("Turnaround cost applied successfully")

    End Sub

#End Region

#Region "Validation"

    Private Function dateValidation() As Boolean
        For Each r As GridViewRow In productGridView.Rows

            'Dim modelPickerControl As UserControls_PickerControl = r.FindControl("modelPickerControl")
            Dim fromDateControl As UserControls_DateControl = DirectCast(r.FindControl("fromDateControl"), UserControls_DateControl)
            Dim toDateControl As UserControls_DateControl = DirectCast(r.FindControl("toDateControl"), UserControls_DateControl)
            'Dim fixedPenaltyTextBox As TextBox = r.FindControl("fixedPenaltyTextBox")
            'Dim variableDailyPenaltyTextBox As TextBox = r.FindControl("variableDailyPenaltyTextBox")
            'Dim visLvlDropDownList As DropDownList = r.FindControl("visLvlDropDownList")

            'If Not (String.IsNullOrEmpty(modelPickerControl.DataId) Or _
            '               fromDateControl.Date = Date.MinValue Or _
            '               toDateControl.Date = Date.MinValue Or _
            '               String.IsNullOrEmpty(fixedPenaltyTextBox.Text) Or _
            '               String.IsNullOrEmpty(variableDailyPenaltyTextBox.Text) Or _
            '               String.IsNullOrEmpty(visLvlDropDownList.SelectedValue)) Then

            If Not (fromDateControl.IsValid And toDateControl.IsValid) Then
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
                Return False
            End If

            'End If

        Next
        Return True

    End Function

    Private Function dateOverLapping() As Boolean
        For Each r As GridViewRow In productGridView.Rows

            Dim modelPickerControl1 As UserControls_PickerControl = DirectCast(r.FindControl("modelPickerControl"), UserControls_PickerControl)
            Dim locationsControl As UserControls_SelectControl_MultiSelect = DirectCast(r.FindControl("fromLoc"), UserControls_SelectControl_MultiSelect)

            If Not String.IsNullOrEmpty(modelPickerControl1.DataId) Then
                Dim fromDateControl1 As UserControls_DateControl = DirectCast(r.FindControl("fromDateControl"), UserControls_DateControl)
                Dim toDateControl1 As UserControls_DateControl = DirectCast(r.FindControl("toDateControl"), UserControls_DateControl)

                If fromDateControl1.Date > toDateControl1.Date Then
                    SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN062"))
                    Return False
                End If

                For Each rr As GridViewRow In productGridView.Rows

                    Dim modelPickerControl2 As UserControls_PickerControl = DirectCast(rr.FindControl("modelPickerControl"), UserControls_PickerControl)
                    If r.RowIndex <> rr.RowIndex AndAlso modelPickerControl1.DataId = modelPickerControl2.DataId _
                        AndAlso (Not String.IsNullOrEmpty(modelPickerControl2.DataId)) Then

                        Dim fromDateControl2 As UserControls_DateControl = DirectCast(rr.FindControl("fromDateControl"), UserControls_DateControl)
                        Dim toDateControl2 As UserControls_DateControl = DirectCast(rr.FindControl("toDateControl"), UserControls_DateControl)

                        If (fromDateControl2.Date >= fromDateControl1.Date And fromDateControl2.Date <= toDateControl1.Date) Or _
                            (toDateControl2.Date >= fromDateControl1.Date And toDateControl2.Date <= toDateControl1.Date) Then

                            Dim locationsControl2 As UserControls_SelectControl_MultiSelect = DirectCast(rr.FindControl("fromLoc"), UserControls_SelectControl_MultiSelect)

                            If DirectCast(locationsControl.Items, IList).Count = 0 AndAlso DirectCast(locationsControl2.Items, IList).Count = 0 Then
                                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN121"))
                                Return False
                            End If

                            For Each locationItem As MultiSelectItem In locationsControl.Items
                                For Each locationItem2 As MultiSelectItem In locationsControl2.Items
                                    If Object.Equals(locationItem.Key, locationItem2.Key) Then
                                        SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN121"))
                                        Return False
                                    End If
                                Next
                            Next
                        End If
                    End If
                Next
            End If
        Next
        Return True

    End Function

    Private Function validateRequiredField() As Boolean

        For Each r As GridViewRow In productGridView.Rows
            Dim modelPickerControl As UserControls_PickerControl = DirectCast(r.FindControl("modelPickerControl"), UserControls_PickerControl)
            Dim fromDateControl As UserControls_DateControl = DirectCast(r.FindControl("fromDateControl"), UserControls_DateControl)
            Dim toDateControl As UserControls_DateControl = DirectCast(r.FindControl("toDateControl"), UserControls_DateControl)
            Dim fixedPenaltyTextBox As TextBox = DirectCast(r.FindControl("fixedPenaltyTextBox"), TextBox)
            Dim variableDailyPenaltyTextBox As TextBox = DirectCast(r.FindControl("variableDailyPenaltyTextBox"), TextBox)
            Dim visLvlDropDownList As DropDownList = DirectCast(r.FindControl("visLvlDropDownList"), DropDownList)

            'New
            If String.IsNullOrEmpty(productId) Then
                If (String.IsNullOrEmpty(modelPickerControl.DataId) Or _
                                fromDateControl.Date = Date.MinValue Or _
                                toDateControl.Date = Date.MinValue Or _
                                String.IsNullOrEmpty(fixedPenaltyTextBox.Text) Or _
                                String.IsNullOrEmpty(variableDailyPenaltyTextBox.Text) Or _
                                String.IsNullOrEmpty(visLvlDropDownList.SelectedValue)) Then
                    SetShortMessage(AuroraHeaderMessageType.Warning, "Values are required for one or more fields marked with *")
                    Return False
                End If
            Else ' update
                If (String.IsNullOrEmpty(modelPickerControl.DataId) Or _
                    fromDateControl.Date = Date.MinValue Or _
                    toDateControl.Date = Date.MinValue Or _
                    String.IsNullOrEmpty(fixedPenaltyTextBox.Text) Or _
                    String.IsNullOrEmpty(variableDailyPenaltyTextBox.Text) Or _
                    String.IsNullOrEmpty(visLvlDropDownList.SelectedValue)) _
                    And _
                    (Not String.IsNullOrEmpty(modelPickerControl.DataId) Or _
                     Not fromDateControl.Date = Date.MinValue Or _
                    Not toDateControl.Date = Date.MinValue Or _
                    Not String.IsNullOrEmpty(fixedPenaltyTextBox.Text) Or _
                    Not String.IsNullOrEmpty(variableDailyPenaltyTextBox.Text) Or _
                    Not String.IsNullOrEmpty(visLvlDropDownList.SelectedValue)) Then

                    SetShortMessage(AuroraHeaderMessageType.Warning, "Values are required for one or more fields marked with *")
                    Return False
                End If

            End If

        Next

        'Dim makeInactive As Boolean = True
        For Each rr As GridViewRow In productGridView.Rows

            Dim modelPickerControl As UserControls_PickerControl = DirectCast(rr.FindControl("modelPickerControl"), UserControls_PickerControl)
            Dim fromDateControl As UserControls_DateControl = DirectCast(rr.FindControl("fromDateControl"), UserControls_DateControl)
            Dim toDateControl As UserControls_DateControl = DirectCast(rr.FindControl("toDateControl"), UserControls_DateControl)
            Dim fixedPenaltyTextBox As TextBox = DirectCast(rr.FindControl("fixedPenaltyTextBox"), TextBox)
            Dim variableDailyPenaltyTextBox As TextBox = DirectCast(rr.FindControl("variableDailyPenaltyTextBox"), TextBox)
            Dim visLvlDropDownList As DropDownList = DirectCast(rr.FindControl("visLvlDropDownList"), DropDownList)
            Dim activeCheckBox As CheckBox = DirectCast(rr.FindControl("activeCheckBox"), CheckBox)

            If (Not activeCheckBox.Checked) And _
                 (Not String.IsNullOrEmpty(modelPickerControl.DataId)) And _
                 (Not fromDateControl.Date = Date.MinValue) And _
                  (Not toDateControl.Date = Date.MinValue) And _
                   (Not String.IsNullOrEmpty(fixedPenaltyTextBox.Text)) And _
                   (Not String.IsNullOrEmpty(variableDailyPenaltyTextBox.Text)) And _
                   (Not String.IsNullOrEmpty(visLvlDropDownList.SelectedValue)) Then

                confimationBoxControl.Text = "Making a Product Assignment Alternative Inactive may have severe " & _
                       "consequences on the DVASS scheduling optimizer. Are you sure " & _
                       "you wish to continue ?"
                confimationBoxControl.Param = "Save"
                confimationBoxControl.Show()
                Return False
            End If
        Next

        Return True

    End Function

#End Region

    Private Function saveSubRule() As Boolean

        If String.IsNullOrEmpty(productId) Then
            'NEW
            productId = Aurora.Booking.Services.BookingProcess.GetProductID(productPickerControl.DataId, UserCode)
        End If

        Dim xmlDoc As XmlDocument = SubstitutionRuleMgt_XmlDoc

        'Build xml
        Dim xmlString As New StringBuilder
        xmlString.Append("<Root>")
        xmlString.Append(xmlDoc.SelectSingleNode("//data/Cop").OuterXml)
        xmlString.Append("<Prd id='" & productId & "'/>")

        xmlString.Append("<SubRuleList>")

        For Each r As GridViewRow In productGridView.Rows

            Dim srpIdLabel As Label = DirectCast(r.FindControl("srpIdLabel"), Label)
            Dim modelPickerControl As UserControls_PickerControl = DirectCast(r.FindControl("modelPickerControl"), UserControls_PickerControl)
            Dim fromDateControl As UserControls_DateControl = DirectCast(r.FindControl("fromDateControl"), UserControls_DateControl)
            Dim toDateControl As UserControls_DateControl = DirectCast(r.FindControl("toDateControl"), UserControls_DateControl)
            Dim fixedPenaltyTextBox As TextBox = DirectCast(r.FindControl("fixedPenaltyTextBox"), TextBox)
            Dim variableDailyPenaltyTextBox As TextBox = DirectCast(r.FindControl("variableDailyPenaltyTextBox"), TextBox)
            Dim visLvlDropDownList As DropDownList = DirectCast(r.FindControl("visLvlDropDownList"), DropDownList)
            Dim typeDropDownList As DropDownList = DirectCast(r.FindControl("typeDropDownList"), DropDownList)
            Dim activeCheckBox As CheckBox = DirectCast(r.FindControl("activeCheckBox"), CheckBox)
            Dim costLabel As Label = DirectCast(r.FindControl("costLabel"), Label)
            Dim locationsFrom As ASP.usercontrols_multiselectcontrol_multiselect_ascx = DirectCast(r.FindControl("fromLoc"), ASP.usercontrols_multiselectcontrol_multiselect_ascx)


            'Dim modelId As Label = r.FindControl("modelID")

            'Check required fields
            If Not (String.IsNullOrEmpty(modelPickerControl.DataId) Or _
                 fromDateControl.Date = Date.MinValue Or _
                 toDateControl.Date = Date.MinValue Or _
                 String.IsNullOrEmpty(fixedPenaltyTextBox.Text) Or _
                 String.IsNullOrEmpty(variableDailyPenaltyTextBox.Text) Or _
                 String.IsNullOrEmpty(visLvlDropDownList.SelectedValue)) Then

                Dim node As XmlNode
                node = xmlDoc.SelectSingleNode("//data/SubRuleList/SubRulePeriod[SrpId = '" & srpIdLabel.Text & "']")

                node.SelectSingleNode("SubPrdId").InnerXml = productId

                node.SelectSingleNode("SrpFlmId").InnerXml = modelPickerControl.DataId
                node.SelectSingleNode("FlmDesc").InnerXml = modelPickerControl.DataId
                'node.SelectSingleNode("FlmDesc").InnerXml = modelId.Text

                'If node.SelectSingleNode("SrpId").InnerText.Trim.Equals(String.Empty) Then
                '    node.SelectSingleNode("SrpId").InnerText = modelPickerControl.DataId
                'End If

                node.SelectSingleNode("SrpStDt").InnerXml = fromDateControl.Date.ToString()
                node.SelectSingleNode("SrpEnDt").InnerXml = toDateControl.Date.ToString()
                node.SelectSingleNode("SrpFxCst").InnerXml = fixedPenaltyTextBox.Text
                node.SelectSingleNode("SrpVrCst").InnerXml = variableDailyPenaltyTextBox.Text
                node.SelectSingleNode("SrpVsLvl").InnerXml = visLvlDropDownList.SelectedValue
                node.SelectSingleNode("SrpAvl").InnerXml = typeDropDownList.SelectedValue
                node.SelectSingleNode("SrpActive").InnerXml = Convert.ToInt32(activeCheckBox.Checked).ToString()
                node.SelectSingleNode("TctDesc").InnerXml = costLabel.Text

                'Updating locations
                Dim locationsCsv As String = locationsFrom.GetKeysAsCsv()
                Dim locationsCsvNode As XmlNode = node.SelectSingleNode("LocationsCsv")

                If String.IsNullOrEmpty(locationsCsv) Then
                    If locationsCsvNode IsNot Nothing Then
                        locationsCsvNode.ParentNode.RemoveChild(locationsCsvNode)
                    End If
                Else
                    If locationsCsvNode Is Nothing Then

                        locationsCsvNode = xmlDoc.CreateElement("LocationsCsv")
                        locationsCsvNode.AppendChild(xmlDoc.CreateTextNode(""))
                        node.AppendChild(locationsCsvNode)
                    End If
                    locationsCsvNode.FirstChild.Value = locationsCsv
                End If

                'node.SelectSingleNode("SrpLocCode").InnerXml = locationFrom.DataId.ToString()

                xmlString.Append("<SubRulePeriod>")

                ''REV:MIA Oct 1 2013 - addition of From Loc in the Product Assignment Alternatives
                Try
                    Dim chkExcludeLocationMode As CheckBox = DirectCast(r.FindControl("chkExcludeLocationMode"), CheckBox)
                    If (Not chkExcludeLocationMode Is Nothing) Then
                        node.SelectSingleNode("ExcludeFromLocation").InnerText = CStr(IIf(chkExcludeLocationMode.Checked = True, "1", "0"))
                    End If
                Catch ex As Exception
                    node.SelectSingleNode("ExcludeFromLocation").InnerText = "0"
                End Try
                xmlString.Append(node.InnerXml)
                xmlString.Append("</SubRulePeriod>")
            End If


        Next
        xmlString.Append("</SubRuleList>")
        xmlString.Append("<UsrId>" & UserCode & "</UsrId>")
        xmlString.Append("<PrgmName>SubstitutionRuleMgt.aspx</PrgmName>")
        xmlString.Append("</Root>")

        xmlDoc = New XmlDocument
        xmlDoc.LoadXml(xmlString.ToString())
        Dim returnMessage As String
        returnMessage = SubstitutionRule.UpdateSubstitutionRuleProductList(xmlDoc)

        If returnMessage.Contains("GEN046") Or returnMessage.Contains("GEN045") Then
            SetShortMessage(AuroraHeaderMessageType.Information, returnMessage)
            Return True ' done!
        Else
            SetShortMessage(AuroraHeaderMessageType.Error, returnMessage)
            Return False ' failed!
        End If



        '        <Root>
        '	<Cop code="AU" name="Australia"/>
        '	<Prd id="2534183E-2EEC-455C-A230-4586036520CC"/>
        '	<SubRuleList>
        '		<SubRulePeriod>
        '			<SubId>557D2D73-7BBE-4417-BB63-31C07E27059E</SubId>
        '			<SubPrdId>2534183E-2EEC-455C-A230-4586036520CC</SubPrdId>
        '			<PrdDesc>PFMR - THL Vehicles Car - 4WD</PrdDesc>
        '			<SubIntNo>1</SubIntNo>
        '			<SrpId>1193E87B-4B7D-42C1-A4F6-A5E345E9240C</SrpId>
        '			<SrpFlmId>1128</SrpFlmId>
        '			<FlmDesc>4BMF2</FlmDesc>
        '			<SrpStDt>18/01/2005</SrpStDt>
        '			<SrpEnDt>31/12/2020</SrpEnDt>
        '			<SrpFxCst>35.00</SrpFxCst>
        '			<SrpVrCst>35.00</SrpVrCst>
        '			<SrpVsLvl>A398F825-FD0D-4A07-8023-FA36F2AC1D8F</SrpVsLvl>
        '			<SrpAvl>0</SrpAvl>
        '			<SrpActive>1</SrpActive>
        '			<SrpIntNo>1</SrpIntNo>
        '			<TctId>E0A77B3C-0CF0-46FF-83D9-8A806CCD340B,6907BA89-4395-4E1A-B6D8-29B94259C4AB,EE48C23E-0DD4-4323-94D0-00F332000E7E</TctId>
        '			<TctDesc>0-$10.00,1-$5.00,2-$0.00</TctDesc>
        '			<TctIntNo>1,1,1</TctIntNo>
        '			<Rem>0</Rem>
        '		</SubRulePeriod>
        '		<SubRulePeriod>
        '			<SubId>557D2D73-7BBE-4417-BB63-31C07E27059E</SubId>
        '			<SubPrdId>2534183E-2EEC-455C-A230-4586036520CC</SubPrdId>
        '			<PrdDesc>PFMR - THL Vehicles Car - 4WD</PrdDesc>
        '			<SubIntNo>1</SubIntNo>
        '			<SrpId>61F8C586-5BDB-4504-AF73-E79348F39FD5</SrpId>
        '			<SrpFlmId>996</SrpFlmId>
        '			<FlmDesc>LWMR        </FlmDesc>
        '			<SrpStDt>24/10/2002</SrpStDt>
        '			<SrpEnDt>31/12/2020</SrpEnDt>
        '			<SrpFxCst>0.00</SrpFxCst>
        '			<SrpVrCst>0.00</SrpVrCst>
        '			<SrpVsLvl>A398F825-FD0D-4A07-8023-FA36F2AC1D8F</SrpVsLvl>
        '			<SrpAvl>1</SrpAvl>
        '			<SrpActive>1</SrpActive>
        '			<SrpIntNo>1</SrpIntNo>
        '			<TctId>66CDD0EE-1551-4FCC-BD8D-5BADD574DF08,A3ACE6BF-12AB-423A-92C4-5C1C883CA3CE,1E546292-475B-40AF-ADED-520FD8C19F43</TctId>
        '			<TctDesc>0-$10.00,1-$5.00,2-$0.00</TctDesc>
        '			<TctIntNo>1,1,1</TctIntNo>
        '			<Rem>0</Rem>
        '		</SubRulePeriod>
        '	</SubRuleList>
        '	<UsrId>jl3</UsrId>
        '	<PrgmName>manageOPLOGSListener.asp</PrgmName>
        '</Root>


    End Function

    Private Function removeSubRule() As Boolean

        If String.IsNullOrEmpty(productId) Then
            'NEW
            productId = Aurora.Booking.Services.BookingProcess.GetProductID(productPickerControl.DataId, UserCode)
        End If

        'Build xml
        Dim xmlString As New StringBuilder
        xmlString.Append("<Root>")
        xmlString.Append(SubstitutionRuleMgt_XmlDoc.SelectSingleNode("//data/Cop").OuterXml)
        xmlString.Append("<Prd id='" & productId & "'/>")
        xmlString.Append("<SubRuleList>")

        For Each r As GridViewRow In productGridView.Rows

            Dim srpIdLabel As Label = DirectCast(r.FindControl("srpIdLabel"), Label)
            Dim checkedCheckBox As CheckBox = DirectCast(r.FindControl("checkedCheckBox"), CheckBox)

            'Check required fields
            If checkedCheckBox.Checked Then

                Dim node As XmlNode
                node = SubstitutionRuleMgt_XmlDoc.SelectSingleNode("//data/SubRuleList/SubRulePeriod[SrpId = '" & srpIdLabel.Text & "']")
                node.SelectSingleNode("Rem").InnerXml = "1"

                xmlString.Append("<SubRulePeriod>")
                xmlString.Append(node.InnerXml)
                xmlString.Append("</SubRulePeriod>")
            End If
        Next

        xmlString.Append("</SubRuleList>")
        xmlString.Append("<UsrId>" & UserCode & "</UsrId>")
        xmlString.Append("<PrgmName>SubstitutionRuleMgt.aspx</PrgmName>")
        xmlString.Append("</Root>")

        Dim xmlDoc As New XmlDocument
        xmlDoc.LoadXml(xmlString.ToString())
        Dim returnMessage As String
        returnMessage = SubstitutionRule.UpdateSubstitutionRuleProductList(xmlDoc)

        If returnMessage.Contains("GEN047") Then
            SetShortMessage(AuroraHeaderMessageType.Information, returnMessage)
            Return True
        Else
            SetShortMessage(AuroraHeaderMessageType.Error, returnMessage)
            Return False
        End If

    End Function

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        If leftButton Then

            Select Case param
                Case "Save"
                    'Save Sub Rule
                    If saveSubRule() Then
                        ViewState(SubstitutionRuleMgt_XmlDoc_ViewState) = ""
                        formDatabind()
                    End If
                Case "Remove"
                    'Remove Sub Rule
                    If removeSubRule() Then
                        ViewState(SubstitutionRuleMgt_XmlDoc_ViewState) = ""
                        formDatabind()
                    End If
            End Select
        End If
    End Sub

End Class

