' Change Log
'
' 9.6.9     Shoel       Log Added
' 9.6.9     Shoel       Fix for NZ save, invalid redirect url problem

Imports System
Imports System.Data
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.ComponentModel
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Common
Imports Aurora.OpsAndLogs.Data
Imports Aurora.OpsAndLogs.Data.FleetStatusDataSet

<AuroraFunctionCodeAttribute("OL-FLEETSTAT")> _
Partial Class OpsAndLogs_FleetStatus
    Inherits AuroraPage

    Private _now As Date = Date.Now
    Private _fleetStatusDataSet As New FleetStatusDataSet()
    Private _selectedFleetStatusLookupRowCountry As FleetStatusLookupRow
    Private _selectedFleetStatusLookupRow As FleetStatusLookupRow
    Private _fleetStatusTextboxByProductWeek As New Dictionary(Of String, TextBox)
    Private _isReadonly As Boolean = True

    Public ReadOnly Property FleetStatusColorsJSON() As String
        Get
            Dim result As New StringBuilder()
            Dim first As Boolean = True
            result.Append("{")
            For Each fleetStatus As String In DataConstants.FleetStatus_All
                If first Then
                    first = False
                Else
                    result.Append(",")
                End If
                result.Append( _
                    "'" + fleetStatus + "' : " + _
                    "'" + ColorTranslator.ToHtml(DataConstants.FleetStatusToColor(fleetStatus)) + "'")
            Next
            result.Append("}")
            Return result.ToString()
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)

        countryDropDown.Items.Clear()
        lookupDropDown.Items.Clear()
        Dim ctyCode As String = Request("ctyCode")
        If ctyCode Is Nothing Then ctyCode = Me.CountryCode
        Dim code As String = Request("code")

        DataRepository.GetFleetStatusLookups(_fleetStatusDataSet, Me.CompanyCode)

        For Each fleetStatusLookupRow As FleetStatusLookupRow In _fleetStatusDataSet.FleetStatusLookup.CountryLookups
            Dim li As New ListItem(fleetStatusLookupRow.CountryDescription, fleetStatusLookupRow.CtyCode)
            countryDropDown.Items.Add(li)

            If fleetStatusLookupRow.CtyCode = ctyCode Then
                _selectedFleetStatusLookupRowCountry = fleetStatusLookupRow
            End If
        Next
        If _selectedFleetStatusLookupRowCountry Is Nothing _
         AndAlso _fleetStatusDataSet.FleetStatusLookup.CountryLookups.Length > 0 Then
            _selectedFleetStatusLookupRowCountry = _fleetStatusDataSet.FleetStatusLookup.CountryLookups(0)
        End If
        If _selectedFleetStatusLookupRowCountry IsNot Nothing Then
            countryDropDown.SelectedValue = _selectedFleetStatusLookupRowCountry.CtyCode
        Else
            Return
        End If

        For Each fleetStatusLookupRow As FleetStatusLookupRow In _fleetStatusDataSet.FleetStatusLookup.LookupsForCountry(_selectedFleetStatusLookupRowCountry.CtyCode)
            Dim li As New ListItem(fleetStatusLookupRow.Description, fleetStatusLookupRow.Code)
            If fleetStatusLookupRow.FwnTrvYearStart.AddYears(1) < _now Then
                li.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.PastColor)) ' past
            ElseIf fleetStatusLookupRow.FwnTrvYearStart > _now Then
                li.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.FutureColor)) ' future
            Else
                li.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.CurrentColor)) ' now
            End If
            lookupDropDown.Items.Add(li)

            If fleetStatusLookupRow.Code = code Then
                _selectedFleetStatusLookupRow = fleetStatusLookupRow
            End If
        Next
        If _selectedFleetStatusLookupRow Is Nothing _
         AndAlso _fleetStatusDataSet.FleetStatusLookup.DefaultLookupForCountry(_selectedFleetStatusLookupRowCountry.CtyCode) IsNot Nothing Then
            _selectedFleetStatusLookupRow = _fleetStatusDataSet.FleetStatusLookup.DefaultLookupForCountry(_selectedFleetStatusLookupRowCountry.CtyCode)
        ElseIf _selectedFleetStatusLookupRow Is Nothing _
         AndAlso _fleetStatusDataSet.FleetStatusLookup.LookupsForCountry(_selectedFleetStatusLookupRowCountry.CtyCode).Length > 0 Then
            _selectedFleetStatusLookupRow = _fleetStatusDataSet.FleetStatusLookup.LookupsForCountry(_selectedFleetStatusLookupRowCountry.CtyCode)(0)
        End If
        If _selectedFleetStatusLookupRow IsNot Nothing Then
            lookupDropDown.SelectedValue = _selectedFleetStatusLookupRow.Code
        Else
            Return
        End If

        DataRepository.GetFleetStatus(_fleetStatusDataSet, _
            _selectedFleetStatusLookupRow.CtyCode, _
            _selectedFleetStatusLookupRow.BrdCode, _
            _selectedFleetStatusLookupRow.FwnTrvYearStart)

        _fleetStatusTextboxByProductWeek.Clear()

        For tableIndex As Integer = 0 To 1
            If tableIndex = 0 Then
                fleetStatusPanel.Controls.Add(New LiteralControl("<hr/><b>April to September:</b>"))
            Else
                fleetStatusPanel.Controls.Add(New LiteralControl("<hr/><b>October to February:</b>"))
            End If

            ' <asp:Table ID="fleetStatusTable" runat="Server" CellPadding="0" CellSpacing="0" CssClass="dataTableGrid" EnableViewState="False">
            Dim fleetStatusTable As New Table()
            fleetStatusTable.CellSpacing = 0
            fleetStatusTable.CellPadding = 0
            fleetStatusTable.CssClass = "dataTableGrid"
            fleetStatusTable.EnableViewState = False
            fleetStatusPanel.Controls.Add(fleetStatusTable)

            Dim yearTableHeaderRow As New TableHeaderRow() : fleetStatusTable.Rows.Add(yearTableHeaderRow)
            Dim monthTableHeaderRow As New TableHeaderRow() : fleetStatusTable.Rows.Add(monthTableHeaderRow)
            Dim dayTableHeaderRow As New TableHeaderRow() : fleetStatusTable.Rows.Add(dayTableHeaderRow)

            Dim yearTableHeaderCell As TableHeaderCell = Nothing
            Dim lastYear As Integer = Integer.MinValue
            Dim monthTableHeaderCell As TableHeaderCell = Nothing
            Dim lastMonth As Integer = Integer.MinValue
            Dim dayTableHeaderCell As TableHeaderCell = Nothing

            yearTableHeaderCell = New TableHeaderCell()
            yearTableHeaderRow.Controls.Add(yearTableHeaderCell)
            yearTableHeaderCell.ColumnSpan = 1
            yearTableHeaderCell.RowSpan = 3
            yearTableHeaderCell.Text = "Date/<br/>Product"
            yearTableHeaderCell.CssClass = "thProduct"

            For Each flexWeekNumberRow As FlexWeekNumberRow In _fleetStatusDataSet.FlexWeekNumber
                If (((flexWeekNumberRow.FwnTrvWkStart.Month >= 3) AndAlso (flexWeekNumberRow.FwnTrvWkStart.Month <= 9) AndAlso (flexWeekNumberRow.FwnTrvYearStart.Year = flexWeekNumberRow.FwnTrvWkStart.Year)) <> (tableIndex = 0)) Then
                    Continue For
                End If

                If lastYear <> flexWeekNumberRow.FwnTrvWkStart.Year Then
                    yearTableHeaderCell = New TableHeaderCell()
                    yearTableHeaderRow.Controls.Add(yearTableHeaderCell)
                    yearTableHeaderCell.ColumnSpan = 1
                    yearTableHeaderCell.Text = flexWeekNumberRow.FwnTrvWkStart.Year.ToString()
                    yearTableHeaderCell.CssClass = "thdate"
                    lastYear = flexWeekNumberRow.FwnTrvWkStart.Year
                Else
                    yearTableHeaderCell.ColumnSpan += 1
                End If

                If lastMonth <> flexWeekNumberRow.FwnTrvWkStart.Month Then
                    monthTableHeaderCell = New TableHeaderCell()
                    monthTableHeaderRow.Controls.Add(monthTableHeaderCell)
                    monthTableHeaderCell.ColumnSpan = 1
                    monthTableHeaderCell.Text = flexWeekNumberRow.FwnTrvWkStart.ToString("MMM")
                    monthTableHeaderCell.CssClass = "thdate"
                    lastMonth = flexWeekNumberRow.FwnTrvWkStart.Month
                Else
                    monthTableHeaderCell.ColumnSpan += 1
                End If

                dayTableHeaderCell = New TableHeaderCell()
                dayTableHeaderRow.Controls.Add(dayTableHeaderCell)
                dayTableHeaderCell.Text = flexWeekNumberRow.FwnTrvWkStart.Day.ToString()
                dayTableHeaderCell.CssClass = "thdate"
                If (DataConstants.NextTravelStart(flexWeekNumberRow.FwnTrvWkStart) < _now) Then
                    dayTableHeaderCell.BackColor = DataConstants.TimeLinePastColor
                ElseIf flexWeekNumberRow.FwnTrvWkStart > _now Then
                    dayTableHeaderCell.BackColor = DataConstants.TimeLineFutureColor
                Else
                    dayTableHeaderCell.BackColor = DataConstants.TimeLineCurrentColor
                End If
            Next

            For Each productRow As ProductRow In _fleetStatusDataSet.Product
                Dim productTableRow As New TableRow() : fleetStatusTable.Rows.Add(productTableRow)

                Dim productNameTableCell As New TableCell
                productTableRow.Controls.Add(productNameTableCell)
                productNameTableCell.CssClass = "tdProduct"
                productNameTableCell.Text = Server.HtmlEncode(productRow.PrdShortName + " - " + productRow.PrdName)
                If productRow.IsActive Then
                    productNameTableCell.BackColor = DataConstants.ActiveColor
                Else
                    productNameTableCell.BackColor = DataConstants.InactiveColor
                End If

                For Each flexWeekNumberRow As FlexWeekNumberRow In _fleetStatusDataSet.FlexWeekNumber
                    If (((flexWeekNumberRow.FwnTrvWkStart.Month >= 3) AndAlso (flexWeekNumberRow.FwnTrvWkStart.Month <= 9) AndAlso (flexWeekNumberRow.FwnTrvYearStart.Year = flexWeekNumberRow.FwnTrvWkStart.Year)) <> (tableIndex = 0)) Then
                        Continue For
                    End If

                    Dim fleetStatusTableCell As New TableCell()
                    productTableRow.Controls.Add(fleetStatusTableCell)

                    Dim fleetStatusTextBox As New TextBox()
                    fleetStatusTableCell.Controls.Add(fleetStatusTextBox)
                    fleetStatusTextBox.EnableViewState = True
                    fleetStatusTextBox.MaxLength = 1
                    fleetStatusTextBox.ID = _selectedFleetStatusLookupRow.Code & "_" & productRow.PrdShortName & "_" & flexWeekNumberRow.FwnWkNo.ToString()

                    If DataConstants.NextTravelStart(flexWeekNumberRow.FwnTrvWkStart) < _now Then
                        fleetStatusTextBox.ReadOnly = True
                    Else
                        _isReadonly = False
                        fleetStatusTextBox.ReadOnly = False
                        fleetStatusTextBox.Attributes.Add("onkeypress", "return fleetStatusTextBox_keypress(this, event);")
                        fleetStatusTextBox.Attributes.Add("onchange", "fleetStatusTextBox_change(this, event);")
                        ' PHOENIX change
                        Try
                            fleetStatusTextBox.Attributes.Add("ondblclick", "showExceptionsPopup('" & btnHidden.ClientID & "','" & _
                                                                                                                              hdnArguments.ClientID & "','" & _
                                                                                                                              productRow.FleetStatusRow.FstId & "','" & _
                                                                                                                              flexWeekNumberRow.FwnWkNo & "',this.value,'" & _
                                                                                                                              productRow.PrdShortName & "','" & _
                                                                                                                              productRow.FleetStatusRow.FstCtyCode & "','" & _
                                                                                                                              productRow.PrdName & "','" & _
                                                                                                                              Utility.DateDBToString(flexWeekNumberRow.FwnTrvWkStart.Date) & "','" & _
                                                                                                                              lookupDropDown.SelectedValue.Split("-"c)(1) & "');")


                            Dim saExceptions, saException As String()
                            saExceptions = productRow.FleetStatusRow.ExceptionList.Split(","c)

                            Dim sbExceptionList As StringBuilder = New StringBuilder
                            Dim bHasAtleast1Exception As Boolean = False


                            For i As Integer = 0 To saExceptions.Length - 1
                                saException = saExceptions(i).Split("-"c)

                                If flexWeekNumberRow.FwnWkNo.ToString() = saException(0) Then
                                    bHasAtleast1Exception = True
                                    sbExceptionList.Append(saException(1) & "-" & saException(2) & vbNewLine)
                                End If
                            Next

                            If bHasAtleast1Exception Then
                                fleetStatusTextBox.Style("background-image") = "url( ../images/frame.png )"
                                fleetStatusTextBox.Style("cursor") = "pointer"
                                fleetStatusTextBox.ToolTip = sbExceptionList.ToString().Trim()
                                fleetStatusTableCell.ToolTip = sbExceptionList.ToString().Trim()
                            End If

                            'End If

                        Catch ex As Exception

                        End Try

                        ' PHOENIX change
                    End If

                    fleetStatusTextBox.BackColor = DataConstants.FleetStatusToColor(fleetStatusTextBox.Text)

                    _fleetStatusTextboxByProductWeek.Add(productRow.PrdShortName & "_" & flexWeekNumberRow.FwnWkNo.ToString(), fleetStatusTextBox)
                Next
            Next

        Next
    End Sub

    Private Sub BuildFleetStatus(ByVal useViewState As Boolean)
        saveButton.Visible = Not _isReadonly
        cancelButton.Visible = Not _isReadonly
        resultPanel.Visible = False

        If _selectedFleetStatusLookupRow Is Nothing Then Return

        lookupDropDown.SelectedValue = _selectedFleetStatusLookupRow.Code
        resultPanel.Visible = True

        Dim maxBookedFromDate As Date = Date.MinValue

        For Each productRow As ProductRow In _fleetStatusDataSet.Product
            Dim fleetStatusRow As FleetStatusRow = productRow.FleetStatusRow

            If fleetStatusRow IsNot Nothing AndAlso fleetStatusRow.FstBookedFrom > maxBookedFromDate Then
                maxBookedFromDate = productRow.FleetStatusRow.FstBookedFrom
            End If

            For Each flexWeekNumberRow As FlexWeekNumberRow In _fleetStatusDataSet.FlexWeekNumber
                Dim fleetStatusTextBox As TextBox = _fleetStatusTextboxByProductWeek(productRow.PrdShortName & "_" & flexWeekNumberRow.FwnWkNo.ToString())

                If Not useViewState Then
                    If fleetStatusRow IsNot Nothing Then
                        fleetStatusTextBox.Text = productRow.FleetStatusRow("FstFleetStatus" & flexWeekNumberRow.FwnWkNo.ToString()).ToString().Trim()
                    Else
                        fleetStatusTextBox.Text = DataConstants.FleetStatus_FreeSale
                    End If
                End If

                If fleetStatusTextBox.Text = "" Then
                    fleetStatusTextBox.Text = DataConstants.FleetStatus_FreeSale
                End If
                fleetStatusTextBox.BackColor = DataConstants.FleetStatusToColor(fleetStatusTextBox.Text)
            Next
        Next

        If maxBookedFromDate <> Date.MinValue Then
            bookedFromTextBox.Text = Utility.DateUIToString(maxBookedFromDate)
        Else
            bookedFromTextBox.Text = ""
        End If

        ' Phoenix change
        If _isReadonly Then
            btnHidden.Attributes.Add("onclick", "return false;")
        Else
            btnHidden.Attributes("onclick") = String.Empty
        End If
        ' Phoenix change
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.IsPostBack Then Return
        BuildFleetStatus(False)
        If Request("msgToShow") IsNot Nothing Then
            SetInformationMessage(Request("msgToShow"))
        End If
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        BuildFleetStatus(False)
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        BuildFleetStatus(True)
        If _isReadonly Then Return

        Try
            For Each productRow As ProductRow In _fleetStatusDataSet.Product
                Dim fleetStatusRow As FleetStatusRow = productRow.FleetStatusRow

                For Each flexWeekNumberRow As FlexWeekNumberRow In _fleetStatusDataSet.FlexWeekNumber
                    If DataConstants.NextTravelStart(flexWeekNumberRow.FwnTrvWkStart) < _now _
                     OrElse Not _fleetStatusTextboxByProductWeek.ContainsKey(productRow.PrdShortName & "_" & flexWeekNumberRow.FwnWkNo.ToString()) Then
                        Continue For
                    End If

                    Dim fleetStatusTextBox As TextBox = _fleetStatusTextboxByProductWeek(productRow.PrdShortName & "_" & flexWeekNumberRow.FwnWkNo.ToString())

                    If fleetStatusRow Is Nothing Then
                        fleetStatusRow = _fleetStatusDataSet.FleetStatus.NewFleetStatusRow()
                        fleetStatusRow.FstCtyCode = _selectedFleetStatusLookupRow.CtyCode
                        fleetStatusRow.FstBrdCode = _selectedFleetStatusLookupRow.BrdCode
                        fleetStatusRow.FstShortName = productRow.PrdShortName
                        fleetStatusRow.FstTravelYear = _selectedFleetStatusLookupRow.FwnTrvYearStart.Year.ToString()
                        fleetStatusRow.FstBookedFrom = _now
                        For Each dataColumn As DataColumn In fleetStatusRow.Table.Columns
                            If dataColumn.ColumnName.StartsWith("FstFleetStatus") Then
                                fleetStatusRow(dataColumn.ColumnName) = DataConstants.FleetStatus_FreeSale
                            End If
                        Next
                        fleetStatusRow.addusrid = Me.UserCode
                        fleetStatusRow.adddatetime = _now
                        fleetStatusRow.addprgmname = Me.PrgmName
                        _fleetStatusDataSet.FleetStatus.AddFleetStatusRow(fleetStatusRow)
                    End If

                    If ("" & productRow.FleetStatusRow("FstFleetStatus" & flexWeekNumberRow.FwnWkNo.ToString())) <> fleetStatusTextBox.Text Then
                        productRow.FleetStatusRow("FstFleetStatus" & flexWeekNumberRow.FwnWkNo.ToString()) = fleetStatusTextBox.Text
                    End If
                Next

                If fleetStatusRow Is Nothing Then
                    Continue For
                End If

                If fleetStatusRow.RowState <> System.Data.DataRowState.Unchanged Then
                    If fleetStatusRow.RowState = DataRowState.Modified Then
                        fleetStatusRow.modusrid = Me.UserCode
                        fleetStatusRow.moddatetime = _now
                    End If
                    fleetStatusRow.FstBookedFrom = _now
                    Aurora.Common.Data.ExecuteDataRowInsertVehicleDependentRate(fleetStatusRow)
                End If
            Next

            hdnDirtyFlag.Value = "false"
            Dim sURL As String
            sURL = Request.Url.AbsoluteUri
            If sURL.ToUpper().Contains(UCase("&msgToShow")) Then
                sURL = Left(sURL, sURL.IndexOf("&msgToShow="))
            End If
            If sURL.ToUpper().Contains(UCase("msgToShow")) Then
                sURL = Left(sURL, sURL.IndexOf("msgToShow="))
            End If
            If Not sURL.Contains("?") Then
                ' Shoel : 9.6.9 : Fix for NZ save, invalid redirect url problem
                sURL = sURL + "?"
            Else
                'sURL = sURL.Split("?"c)(0) & "?"
            End If
            Response.Redirect(sURL & "&msgToShow=Fleet Status saved")

        Catch ex1 As System.Threading.ThreadAbortException
            ' do nothing
        Catch ex As Exception
            BuildFleetStatus(False)

            Dim description As String = "Error saving Fleet Status"
            Me.AddErrorMessage(description)
            Aurora.Common.Logging.LogError(Me.FunctionCode, description, ex)
        End Try
    End Sub

    Protected Sub btnHidden_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHidden.Click
        '  Console.Write(hdnArguments.Value)

        Dim arrValues As String()
        arrValues = hdnArguments.Value.Split(","c)

        popupFSX.ShowPopup(CLng(arrValues(0)), CInt(arrValues(1)), arrValues(2), arrValues(3), arrValues(4), _
                           countryDropDown.SelectedItem.Text.Split("-"c)(1), lookupDropDown.SelectedItem.Text.Trim(), _
                           arrValues(6), arrValues(5), arrValues(7))
    End Sub

    Protected Sub popupFSX_OnPostBack(ByVal sender As Object, ByVal childRecordHasChanged As Boolean, ByVal param As Object) Handles popupFSX.PostBack
        'If childRecordHasChanged Then
        '    BuildFleetStatus(False)
        'End If
        Response.Redirect(Request.Url.PathAndQuery)
    End Sub
End Class
