''REV:MIA: April 22,2008 - Revisions are as follow.
''Note: I disabled all clientside validations controls. This was to fixed the Booking number issue. 
''      Textbox now is accepting up to 12 characters (initial was 10) to give way the extra letters that might be entered.
''      Change AddErrorMessage to SetErrorShortMessage
''      Validations will now be perform in the database SP.
''      Add extra validations in the GEN_UpdateGeneralDataForUndoCheckIn..see below
'                if @BookingNumber = '' 
''	               begin
''			            SELECT 'Please enter Booking Reference Number!' 
''			            GOTO Error
''	               end

''	
''	              if patindex('%/%',@BookingNumber) = 0 
''	                 begin
''		            	SELECT 'ERROR/Booking Ref is not in correct format. i.e. 3000140/1 format.' 
''			            GOTO Error
''	                end

<AuroraFunctionCodeAttribute("UNDOCHECKIN")> _
Partial Class OpsAndLogs_UndoCheckIn
    Inherits AuroraPage

    Public Const ErrorMessage As String = "Booking Ref ? is not in correct format. i.e  3000140/1 format"

    Private Function IsValidBookingRef(ByVal text As String) As Boolean

        If String.IsNullOrEmpty(text) Then Return False

        Dim firstLetter As Char = text.Substring(0, 1)
        Dim isLetter As Boolean = Char.IsLetter(firstLetter)

        Dim index As Integer = IIf(isLetter, 9, 7)
        Dim length As Integer = IIf(isLetter, 10, 9)

        If text.Length <> length Then Return False
        If Not text.Contains("/") Then Return False

        Dim strSlash As String() = text.Split("/")
        If strSlash.Length > 2 Then Return False

        Dim indexChar As Integer = text.IndexOf("/")
        Dim indexAfter As Char = text.Substring(index + 1)

        Return (indexChar = index) AndAlso indexAfter <> "" AndAlso Char.IsDigit(indexAfter)
    End Function

    Protected Sub CustomValidatorBookinRef_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidatorBookinRef.ServerValidate
        args.IsValid = IsValidBookingRef(txtBookingRef.Text.Trim())
        CustomValidatorBookinRef.ErrorMessage = ErrorMessage.Replace("?", txtBookingRef.Text.Trim())
    End Sub

    Protected Sub btnUndoCheckIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUndoCheckIn.Click

        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("GEN_UpdateGeneralDataForUndoCheckIn", Me.txtBookingRef.Text.Trim(), Me.UserCode)

        Dim message As String = result.Substring(result.IndexOf("/") + 1)
        If Not result.Contains("SUCCESS") Then
            ''Me.AddErrorMessage(message)
            Me.SetErrorShortMessage(message)
        Else ' result.Contains("ERROR") 
            Me.AddInformationMessage(message)
        End If
    End Sub
End Class
