Option Strict On
Option Explicit On


Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services
Imports System.Data
Imports System.xml

Imports ASP.usercontrols_multiselectcontrol_multiselect_ascx

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductAssignmentAlternatives)> _
<AuroraMessageAttribute("GEN062,GEN121")> _
Partial Class OpsAndLogs_SubstitutionRuleProductList
    Inherits AuroraPage

    Protected Shared mStateKey As String = Guid.NewGuid().ToString().Replace("-", "")

    Private Const SubstitutionRuleProductList_XmlDoc_ViewState As String = "SubstitutionRuleProductList_XmlDoc"

    Protected mLocations()() As MultiSelectItem

    Protected mCountryCode As String

    Private Property SubstitutionRuleProductList_XmlDoc() As XmlDocument
        Get
            Dim xmlDoc As New XmlDocument
            If Not String.IsNullOrEmpty(CStr(ViewState(SubstitutionRuleProductList_XmlDoc_ViewState))) Then
                xmlDoc.LoadXml(CStr(ViewState(SubstitutionRuleProductList_XmlDoc_ViewState)))
            Else
                Dim state As FilterState = GetCurrentFilterState()

                xmlDoc = SubstitutionRule.GetSubstitutionRuleProductList(CountryCode, UserCode, "", "", state.DateFrom, state.DateTo, state.LocationFrom)
                ViewState(SubstitutionRuleProductList_XmlDoc_ViewState) = xmlDoc.InnerXml
            End If

            'Set view state
            Return xmlDoc

        End Get
        Set(ByVal value As XmlDocument)
            If value Is Nothing Then
                ViewState(SubstitutionRuleProductList_XmlDoc_ViewState) = Nothing
            Else
                ViewState(SubstitutionRuleProductList_XmlDoc_ViewState) = value.InnerXml
            End If
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        mCountryCode = CountryCode

        Try

            If Not Page.IsPostBack Then
                RestoreState()

                'AddHandler productGridView.RowDataBound, AddressOf RowDatabound
                productGridView_Databind()
                pkrFilterLocationFrom.Param1 = mCountryCode
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub productGridView_Databind()

        Dim oXmlDoc As XmlDocument
        oXmlDoc = SubstitutionRuleProductList_XmlDoc 'SubstitutionRule.GetSubstitutionRuleProductList(CountryCode, UserCode, "", "")

        ConstructLocationsFromXml(oXmlDoc)

        Dim ds As DataSet = New DataSet
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))


        If ds.Tables.Count = 1 Then
            Return
        End If

        Dim countryTable As DataTable = ds.Tables(0)

        If countryTable.Columns.IndexOf("name") = -1 Then
            countryTable = ds.Tables(1)
        End If

        If countryTable.Rows.Count = 1 Then
            'countryLabel.Text = ds.Tables(0).Rows(0)("name")
            countryTextBox.Text = CStr(countryTable.Rows(0)("name"))
        End If

        If ds.Tables.Count = 4 Then
            Dim dt As DataTable = ds.Tables(3)
            Dim productName As String = ""
            For Each dr As DataRow In dt.Rows
                If CStr(dr.Item("PrdDesc")) = productName Then
                    dr.Item("PrdDesc") = ""
                Else
                    productName = CStr(dr.Item("PrdDesc"))
                End If
            Next

            productGridView.DataSource = ds.Tables(3)
            productGridView.DataBind()
        Else
            productGridView.DataSource = Nothing
            productGridView.DataBind()
        End If

        

    End Sub

    Protected Sub ConstructLocationsFromXml(ByVal document As XmlDocument)

        'Referencing nodes directly rather than with XPath for better performance

        'Cycling through all the rule period nodes and constructing location objects for each one

        'document.ChildNodes(0).ChildNodes(2).ChildNodes(0).ChildNodes(18).ChildNodes(0).ChildNodes(0)

        'All the substitution rule records
        Dim nodes As XmlNodeList = document.ChildNodes(0).ChildNodes(2).ChildNodes

        Dim locationDataArray(nodes.Count)() As MultiSelectItem

        Dim intIndex As Integer = 0

        For Each node As XmlNode In nodes

            'Getting the location collection node
            Dim locationListNode As XmlNode = node.ChildNodes(18)

            'Reading all the location nodes
            If locationListNode.NodeType = XmlNodeType.Text Then
                'No locations
                locationListNode.FirstChild.Value = "-1"
            Else
                Dim locationIndex As Integer = 0
                Dim dataItems(locationListNode.ChildNodes.Count) As MultiSelectItem
                For Each locationNode As XmlNode In locationListNode.ChildNodes
                    Dim item As New MultiSelectItem()

                    'Location Code
                    item.Key = locationNode.ChildNodes(0).FirstChild.Value
                    'Location Name
                    item.Value = locationNode.ChildNodes(1).FirstChild.Value

                    dataItems(locationIndex) = item

                    locationIndex += 1
                Next

                locationDataArray(intIndex) = dataItems

                locationListNode.RemoveAll()

                'Referencing the array entry for use in databinding
                locationListNode.AppendChild(document.CreateTextNode(intIndex.ToString()))
            End If

            intIndex += 1
        Next

        mLocations = locationDataArray

    End Sub

    Protected Function GetMultiselectItems(ByVal index As String) As MultiSelectItem()
        Dim intLocationIndex As Integer = Integer.Parse(index)

        If (intLocationIndex > -1) Then
            Return mLocations(intLocationIndex)
        End If

        Return Nothing
    End Function

    'Protected Sub RowDatabound(ByVal sender As Object, ByVal args As GridViewRowEventArgs)
    '    Dim row As GridViewRow = args.Row


    'End Sub

    Protected Function GetProductUrl(ByVal productId As Object) As String
        Dim productIdString As String
        productIdString = Convert.ToString(productId)

        Return "~/OpsAndLogs/SubstitutionRuleMgt.aspx?productId=" & productIdString

    End Function

    'Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
    '    If dateOverLapping() Then
    '        save()
    '    End If
    'End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        'refresh the grid
        productGridView_Databind()
    End Sub

    Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newButton.Click
        Response.Redirect("~/OpsAndLogs/SubstitutionRuleMgt.aspx")
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click

        Response.Redirect(Me.BackUrl)
    End Sub

    Protected Sub substitutionRuleButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles substitutionRuleButton.Click
        Response.Redirect("~/OpsAndLogs/SubstitutionRuleFleetModelList.aspx")
    End Sub


    Private Function dateOverLapping() As Boolean

        For Each r As GridViewRow In productGridView.Rows

            Dim subPrdIdLabel1 As Label = DirectCast(r.FindControl("subPrdIdLabel"), Label)
            Dim modelLabel1 As Label = DirectCast(r.FindControl("modelLabel"), Label)
            

                If Not String.IsNullOrEmpty(modelLabel1.Text) Then
                    Dim fromDateControl1 As UserControls_DateControl = DirectCast(r.FindControl("fromDateControl"), UserControls_DateControl)
                    Dim toDateControl1 As UserControls_DateControl = DirectCast(r.FindControl("toDateControl"), UserControls_DateControl)

                    If fromDateControl1.Date > toDateControl1.Date Then
                        SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN062"))
                        Return False
                    End If

                Dim locationsControl As UserControls_SelectControl_MultiSelect = DirectCast(r.FindControl("fromLoc"), UserControls_SelectControl_MultiSelect)



                For Each rr As GridViewRow In productGridView.Rows

                    Dim subPrdIdLabel2 As Label = DirectCast(rr.FindControl("subPrdIdLabel"), Label)
                    Dim modelLabel2 As Label = DirectCast(rr.FindControl("modelLabel"), Label)

                    If r.RowIndex <> rr.RowIndex And modelLabel1.Text = modelLabel2.Text And _
                        (Not String.IsNullOrEmpty(modelLabel2.Text)) And _
                        subPrdIdLabel1.Text = subPrdIdLabel2.Text Then

                        Dim fromDateControl2 As UserControls_DateControl = DirectCast(rr.FindControl("fromDateControl"), UserControls_DateControl)
                        Dim toDateControl2 As UserControls_DateControl = DirectCast(rr.FindControl("toDateControl"), UserControls_DateControl)

                        If (fromDateControl2.Date >= fromDateControl1.Date And fromDateControl2.Date <= toDateControl1.Date) Or _
                            (toDateControl2.Date >= fromDateControl1.Date And toDateControl2.Date <= toDateControl1.Date) Then

                            Dim locationsControl2 As UserControls_SelectControl_MultiSelect = DirectCast(rr.FindControl("fromLoc"), UserControls_SelectControl_MultiSelect)

                            If DirectCast(locationsControl.Items, IList).Count = 0 AndAlso DirectCast(locationsControl2.Items, IList).Count = 0 Then
                                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN121"))
                                Return False
                            End If

                            For Each locationItem As MultiSelectItem In locationsControl.Items
                                For Each locationItem2 As MultiSelectItem In locationsControl2.Items
                                    If Object.Equals(locationItem.Key, locationItem2.Key) Then
                                        SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN121"))
                                        Return False
                                    End If
                                Next
                            Next
                        End If
                    End If

                Next

            End If
        Next
        Return True

    End Function


    Private Sub save()

        Dim xmlDoc As New XmlDocument
        Dim xmlString As String
        xmlString = SubstitutionRuleProductList_XmlDoc.InnerXml

        xmlString = xmlString.Replace("<data>", "<Root>")
        xmlString = xmlString.Replace("</data>", "</Root>")

        xmlDoc.LoadXml(xmlString)

        For Each r As GridViewRow In productGridView.Rows

            Dim srpIdLabel As Label = DirectCast(r.FindControl("srpIdLabel"), Label)
            Dim fromDateControl As UserControls_DateControl = DirectCast(r.FindControl("fromDateControl"), UserControls_DateControl)
            Dim toDateControl As UserControls_DateControl = DirectCast(r.FindControl("toDateControl"), UserControls_DateControl)
            Dim fixedTextBox As TextBox = DirectCast(r.FindControl("fixedTextBox"), TextBox)
            Dim variableTextBox As TextBox = DirectCast(r.FindControl("variableTextBox"), TextBox)
            'Dim locationFrom As ASP.usercontrols_pickercontrol_pickercontrol_ascx = r.FindControl("pkrLocationFrom")
            Dim fleetModelId As HiddenField = DirectCast(r.FindControl("fleetModelId"), HiddenField)

            Dim locationsFrom As ASP.usercontrols_multiselectcontrol_multiselect_ascx = DirectCast(r.FindControl("fromLoc"), ASP.usercontrols_multiselectcontrol_multiselect_ascx)

            If fromDateControl.Date > toDateControl.Date Then
                SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN062"))
                Return
            End If


            Dim rootNode As XmlNode = xmlDoc.SelectSingleNode("//Root")
            Dim userIdNode As XmlNode = xmlDoc.SelectSingleNode("//Root/UsrId")
            Dim programNameNode As XmlNode = xmlDoc.SelectSingleNode("//Root/PrgmName")

            If userIdNode Is Nothing Then
                userIdNode = xmlDoc.CreateElement("UsrId")
                userIdNode.AppendChild(xmlDoc.CreateTextNode(Me.UserCode))
                rootNode.PrependChild(userIdNode)
            End If

            If programNameNode Is Nothing Then
                programNameNode = xmlDoc.CreateElement("PrgmName")
                programNameNode.AppendChild(xmlDoc.CreateTextNode(Me.PrgmName))
                rootNode.PrependChild(programNameNode)
            End If


            Dim node As XmlNode
            node = xmlDoc.SelectSingleNode("//Root/SubRuleList/SubRulePeriod[SrpId = '" & srpIdLabel.Text & "']")

            'node.SelectSingleNode("SrpStDt").InnerXml = fromDateControl.Date
            'node.SelectSingleNode("SrpEnDt").InnerXml = toDateControl.Date
            node.SelectSingleNode("SrpStDt").InnerXml = Utility.DateDBToString(fromDateControl.Date)
            node.SelectSingleNode("SrpEnDt").InnerXml = Utility.DateDBToString(toDateControl.Date)


            node.SelectSingleNode("SrpFxCst").InnerXml = fixedTextBox.Text
            node.SelectSingleNode("SrpVrCst").InnerXml = variableTextBox.Text

            'Updating locations
            Dim locationsCsv As String = locationsFrom.GetKeysAsCsv()
            Dim locationsCsvNode As XmlNode = node.SelectSingleNode("LocationsCsv")

            If String.IsNullOrEmpty(locationsCsv) Then
                If locationsCsvNode IsNot Nothing Then
                    locationsCsvNode.ParentNode.RemoveChild(locationsCsvNode)
                End If
            Else
                If locationsCsvNode Is Nothing Then

                    locationsCsvNode = xmlDoc.CreateElement("LocationsCsv")
                    locationsCsvNode.AppendChild(xmlDoc.CreateTextNode(""))
                    node.AppendChild(locationsCsvNode)
                End If
                locationsCsvNode.FirstChild.Value = locationsCsv
            End If

            node.SelectSingleNode("SrpFlmId").InnerXml = fleetModelId.Value
        Next

        Dim returnMessage As String
        returnMessage = SubstitutionRule.UpdateSubstitutionRuleProductList(xmlDoc)

        If returnMessage.Contains("GEN046") Or returnMessage.Contains("GEN045") Then
            SetShortMessage(AuroraHeaderMessageType.Information, returnMessage)
            'Refresh the grid
            ViewState(SubstitutionRuleProductList_XmlDoc_ViewState) = ""
            productGridView_Databind()
        Else
            SetShortMessage(AuroraHeaderMessageType.Error, returnMessage)
        End If


    End Sub

    Protected Function GetDate(ByVal d As Object) As Date
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Dim dDate As Date
            dDate = Utility.ParseDateTime(dString, Utility.SystemCulture)
            Return dDate
        Else
            Return Date.MinValue
        End If

    End Function


    Protected Sub filterButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFilter.Click
        SaveState()

        SubstitutionRuleProductList_XmlDoc = Nothing

        productGridView_Databind()
    End Sub

    Protected Sub btnResetFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnResetFilter.Click
        RestoreState(New FilterState())
        SubstitutionRuleProductList_XmlDoc = Nothing
        productGridView_Databind()
        SaveState()
    End Sub


    Protected Sub RestoreState()
        Dim state As FilterState = DirectCast(Session(mStateKey), FilterState)

        If state Is Nothing Then
            Return
        End If

        RestoreState(state)
    End Sub

    Protected Sub RestoreState(ByVal state As FilterState)

        If (state.DateFrom.HasValue) Then
            dtFilterDateFrom.Date = state.DateFrom.Value
        Else
            dtFilterDateFrom.Text = Nothing
        End If

        If (state.DateTo.HasValue) Then
            dtFilterDateTo.Date = state.DateTo.Value
        Else
            dtFilterDateTo.Text = Nothing
        End If

        pkrFilterLocationFrom.Text = state.LocationFrom
    End Sub

    Protected Function GetCurrentFilterState() As FilterState

        Dim state As New FilterState()

        If dtFilterDateFrom.IsValid AndAlso Not String.IsNullOrEmpty(dtFilterDateFrom.Text) Then
            state.DateFrom = dtFilterDateFrom.Date
        Else
            state.DateFrom = Nothing
        End If

        If dtFilterDateFrom.IsValid AndAlso Not String.IsNullOrEmpty(dtFilterDateTo.Text) Then
            state.DateTo = dtFilterDateTo.Date
        Else
            state.DateTo = Nothing
        End If

        If pkrFilterLocationFrom.IsValid Then
            state.LocationFrom = IIFX(String.IsNullOrEmpty(pkrFilterLocationFrom.Text), Nothing, pkrFilterLocationFrom.Text)
        Else
            state.LocationFrom = Nothing
        End If

        Return state
    End Function

    Protected Sub SaveState()
        Dim state As FilterState = GetCurrentFilterState()

        Session(mStateKey) = state
    End Sub

    Public Class FilterState
        Public DateTo As Nullable(Of DateTime)
        Public DateFrom As Nullable(Of DateTime)
        Public LocationFrom As String

    End Class
End Class
