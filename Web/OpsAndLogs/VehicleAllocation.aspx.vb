'' Change log!
''  ID4   - Shoel : date format thingie added !
''  ID4   - Shoel : added validations for data entered, no more blanks!
''  22.9.8  - Shoel : Fixed cancel button functionality - Squish 618

Imports System.Xml
Imports System.Data
Imports Aurora.Common
Imports Aurora.Common.UserSettings

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.VehicleAllocation)> _
Partial Class OpsAndLogs_VehicleAllocation
    Inherits AuroraPage

    Const VIEWSTATE_ORIGINALXML As String = "ViewState_VehAlloc_OriginalXML"
    Const VIEWSTATE_OPTION As String = "ViewState_VehAlloc_Option"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If IsPostBack Then Return
        ' initial load
        LoadPageContents()
    End Sub

    Public Sub LoadPageContents()
        ' load the combo box
        LoadCountryCombo()
        ' disable buttons
        DisableButtons()
        ' dummy row
        LoadDummyRow()
        ' dummy
        btnRemove.Attributes.Add("onclick", "return CheckIfAnythingSelected('tblRptMain',6);")

        If cmbCountry.Items.Count > 0 Then
            LoadVehicleAllocationTable()
            EnableButtons()
        End If
    End Sub

    Sub LoadCountryCombo()
        Dim xmlReturn As XmlDocument = Aurora.Common.Data.GetCountriesOfOperation()

        Dim dstCountries As New DataSet
        dstCountries.ReadXml(New XmlNodeReader(xmlReturn))

        If dstCountries.Tables.Count > 0 Then
            cmbCountry.DataSource = dstCountries.Tables(0)
            cmbCountry.DataTextField = "NAME"
            cmbCountry.DataValueField = "CODE"
            cmbCountry.DataBind()
        End If
    End Sub

    Sub EnableButtons()
        btnSave.Enabled = True
        btnCancel.Enabled = True
        btnRemove.Enabled = True
        btnSave1.Enabled = True
        btnCancel1.Enabled = True
        btnRemove1.Enabled = True
    End Sub

    Sub DisableButtons()
        btnSave.Enabled = False
        btnCancel.Enabled = False
        btnRemove.Enabled = False
        btnSave1.Enabled = False
        btnCancel1.Enabled = False
        btnRemove1.Enabled = False
    End Sub

    Sub LoadDummyRow()
        Dim xmlReturn As XmlDocument = New XmlDocument
        xmlReturn.LoadXml("<data><Allocations><Dvs_Allocation><ID></ID><Vehicle></Vehicle><LocFrom></LocFrom><LocTo></LocTo><Allocation></Allocation><AddedBy></AddedBy><ModifiedBy></ModifiedBy><Remove>0</Remove></Dvs_Allocation></Allocations></data>")
        Dim dstAllocations As New DataSet
        dstAllocations.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Allocations")))
        rptMain.DataSource = dstAllocations.Tables(0)
        rptMain.DataBind()

    End Sub


    Sub LoadVehicleAllocationTable()
        ' rptMain
        Dim xmlReturn As XmlDocument
        xmlReturn = Aurora.OpsAndLogs.Services.VehicleAllocation.GetVehicleAllocationGrid(cmbCountry.SelectedValue, UserCode)

        '<Error><ErrStatus>True</ErrStatus><ErrNo>GEN003</ErrNo><ErrType/><ErrDesc>GEN064 - Invalid Country Code. Please re-enter.</ErrDesc></Error>
        If Not xmlReturn.DocumentElement.SelectSingleNode("ErrStatus") Is Nothing Then
            If Not xmlReturn.DocumentElement.SelectSingleNode("ErrDesc") Is Nothing Then
                SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("ErrDesc").InnerText)
                Exit Sub
            End If
        End If

        '' empty row
        ''<data><Allocations><Dvs_Allocation><ID></ID><Vehicle></Vehicle><LocFrom></LocFrom><LocTo></LocTo><Allocation></Allocation><AddedBy></AddedBy><ModifiedBy></ModifiedBy><Remove></Remove></Dvs_Allocation></Allocations></data>
        'If xmlReturn.OuterXml.Trim().Equals("<data><Allocations><Dvs_Allocation><ID></ID><Vehicle></Vehicle><LocFrom></LocFrom><LocTo></LocTo><Allocation></Allocation><AddedBy></AddedBy><ModifiedBy></ModifiedBy><Remove></Remove></Dvs_Allocation></Allocations></data>") Then
        '    xmlReturn.DocumentElement.InnerXml = "<Allocations><Dvs_Allocation><ID></ID><Vehicle></Vehicle><LocFrom></LocFrom><LocTo></LocTo><Allocation></Allocation><AddedBy></AddedBy><ModifiedBy></ModifiedBy><Remove>0</Remove></Dvs_Allocation></Allocations>"
        'End If

        ' hack to take care of remove nodes
        For i As Integer = 0 To xmlReturn.DocumentElement.SelectSingleNode("Allocations").ChildNodes.Count - 1
            If xmlReturn.DocumentElement.SelectSingleNode("Allocations").ChildNodes(i).SelectSingleNode("Remove").InnerText = "" Then
                xmlReturn.DocumentElement.SelectSingleNode("Allocations").ChildNodes(i).SelectSingleNode("Remove").InnerText = "0"
            End If
        Next

        ' save it in the ViewState
        ViewState(VIEWSTATE_ORIGINALXML) = xmlReturn.OuterXml

        Dim dstAllocations As New DataSet
        dstAllocations.ReadXml(New XmlNodeReader(xmlReturn.DocumentElement.SelectSingleNode("Allocations")))
        If dstAllocations.Tables.Count > 0 Then

            ' hack for date format
            For Each oRow As DataRow In dstAllocations.Tables(0).Rows
                Dim dtTemp As Date
                Try

                
                    If Not oRow(2).ToString().Trim().Equals(String.Empty) Then
                        dtTemp = ParseDateTime(oRow(2), SystemCulture)
                        oRow(2) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                    End If
                    If Not oRow(3).ToString().Trim().Equals(String.Empty) Then
                        dtTemp = ParseDateTime(oRow(3), SystemCulture)
                        oRow(3) = dtTemp.ToString(UserSettings.Current.ComDateFormat)
                    End If
                Catch ex As Exception
                    ' suppress the exception, if any
                End Try

                Try

                
                    Dim sDatePart As String
                    If Not oRow(5).ToString().Trim().Equals(String.Empty) Then
                        sDatePart = oRow(5).ToString().Split(" / ")(oRow(5).ToString().Split(" / ").Length - 1)
                        'sDatePart = oRow(5).ToString().Substring(oRow(5).ToString().Length - 10, 10)
                        dtTemp = ParseDateTime(sDatePart, SystemCulture)
                        oRow(5) = oRow(5).ToString().Split(" / ")(0) & " / " & dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        'oRow(5) = oRow(5).ToString().Substring(0, oRow(5).ToString().Length - 10) & dtTemp.ToString(UserSettings.Current.ComDateFormat)
                    End If
                    If Not oRow(6).ToString().Trim().Equals(String.Empty) Then
                        sDatePart = oRow(6).ToString().Split(" / ")(oRow(6).ToString().Split(" / ").Length - 1)
                        'sDatePart = oRow(6).ToString().Substring(oRow(6).ToString().Length - 10, 10)
                        dtTemp = ParseDateTime(sDatePart, SystemCulture)
                        oRow(6) = oRow(6).ToString().Split(" / ")(0) & " / " & dtTemp.ToString(UserSettings.Current.ComDateFormat)
                        'oRow(6) = oRow(6).ToString().Substring(0, oRow(5).ToString().Length - 10) & dtTemp.ToString(UserSettings.Current.ComDateFormat)
                    End If
                Catch ex As Exception
                    ' suppress the exception, if any
                End Try
            Next
            ' hack for date format
            rptMain.DataSource = dstAllocations.Tables(0)
            rptMain.DataBind()
        End If

    End Sub

    Function BuildXMLToSave() As XmlDocument
        Dim xmlNew As XmlDocument = New XmlDocument
        Dim xmlOriginal As XmlDocument = New XmlDocument
        xmlOriginal.LoadXml(CStr(ViewState(VIEWSTATE_ORIGINALXML)))
        Dim sbXMLToSave As New StringBuilder
        '<Root>
        '	<CtyCode>NZ</CtyCode>
        '	<Allocations>
        '		<Dvs_Allocation>
        '			<ID>1067</ID>
        '			<Vehicle>4EAR</Vehicle>
        '			<LocFrom>19/04/2007</LocFrom>
        '			<LocTo>31/03/2008</LocTo>
        '			<Allocation>9</Allocation>
        '			<AddedBy>rl1/ 19/04/2007</AddedBy>
        '			<Remove></Remove>
        '		</Dvs_Allocation>
        '		<Dvs_Allocation>
        '			<ID></ID>
        '			<Vehicle>VLVAR</Vehicle>
        '			<LocFrom>01/04/2008</LocFrom>
        '			<LocTo>31/03/2009</LocTo>
        '			<Allocation>5</Allocation>
        '			<AddedBy></AddedBy>
        '			<ModifiedBy></ModifiedBy>
        '			<Remove></Remove>
        '		</Dvs_Allocation>
        '	</Allocations>
        '	<UsrId>sp7</UsrId>
        '	<PrgmName>manageOPLOGSListener.asp</PrgmName>
        '</Root>
        sbXMLToSave.Append("<Root>")

        sbXMLToSave.Append("<CtyCode>" & cmbCountry.SelectedValue & "</CtyCode>")

        sbXMLToSave.Append("<Allocations>")

        For i As Integer = 0 To xmlOriginal.DocumentElement.SelectSingleNode("Allocations").ChildNodes.Count - 1
            If xmlOriginal.DocumentElement.SelectSingleNode("Allocations").ChildNodes(i).SelectSingleNode("Vehicle").InnerText <> CType(rptMain.Items(i).FindControl("pkrVehicle"), UserControls_PickerControl).Text _
            Or xmlOriginal.DocumentElement.SelectSingleNode("Allocations").ChildNodes(i).SelectSingleNode("LocFrom").InnerText <> IIf(CType(rptMain.Items(i).FindControl("dtcFrom"), UserControls_DateControl).Text.Trim().Equals(String.Empty), "", DateTimeToString(CType(rptMain.Items(i).FindControl("dtcFrom"), UserControls_DateControl).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c)) _
            Or xmlOriginal.DocumentElement.SelectSingleNode("Allocations").ChildNodes(i).SelectSingleNode("LocTo").InnerText <> IIf(CType(rptMain.Items(i).FindControl("dtcTo"), UserControls_DateControl).Text.Trim().Equals(String.Empty), "", DateTimeToString(CType(rptMain.Items(i).FindControl("dtcTo"), UserControls_DateControl).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c)) _
            Or xmlOriginal.DocumentElement.SelectSingleNode("Allocations").ChildNodes(i).SelectSingleNode("Allocation").InnerText <> CType(rptMain.Items(i).FindControl("txtAllocation"), TextBox).Text _
            Or CType(rptMain.Items(i).FindControl("chkRemove"), CheckBox).Checked Then
                ' its not the same -> Add to list  hdnID

                sbXMLToSave.Append("<Dvs_Allocation>")
                sbXMLToSave.Append("<ID>" & CType(rptMain.Items(i).FindControl("hdnID"), HiddenField).Value & "</ID>")
                If CType(rptMain.Items(i).FindControl("pkrVehicle"), UserControls_PickerControl).Text.Trim().Equals(String.Empty) Then
                    ' invalid
                    SetWarningShortMessage("Vehicle code must be entered")
                    Return Nothing
                Else
                    ' valid
                    sbXMLToSave.Append("<Vehicle>" & CType(rptMain.Items(i).FindControl("pkrVehicle"), UserControls_PickerControl).Text.Trim() & "</Vehicle>")
                End If

                'sbXMLToSave.Append("<LocFrom>" & CType(rptMain.Items(i).FindControl("dtcFrom"), UserControls_DateControl).Text.Trim() & "</LocFrom>")
                If CType(rptMain.Items(i).FindControl("dtcFrom"), UserControls_DateControl).Text.Trim().Equals(String.Empty) Then
                    ' invalid date
                    SetWarningShortMessage("From Date must be entered")
                    Return Nothing
                Else
                    ' valid, proceed
                    sbXMLToSave.Append("<LocFrom>" & DateTimeToString(CType(rptMain.Items(i).FindControl("dtcFrom"), UserControls_DateControl).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c) & "</LocFrom>")
                End If

                'sbXMLToSave.Append("<LocTo>" & CType(rptMain.Items(i).FindControl("dtcTo"), UserControls_DateControl).Text.Trim() & "</LocTo>")
                If CType(rptMain.Items(i).FindControl("dtcTo"), UserControls_DateControl).Text.Trim().Equals(String.Empty) Then
                    ' invalid date
                    SetWarningShortMessage("To Date must be entered")
                    Return Nothing
                Else
                    ' valid, proceed
                    sbXMLToSave.Append("<LocTo>" & DateTimeToString(CType(rptMain.Items(i).FindControl("dtcTo"), UserControls_DateControl).Date, SystemCulture).Split(" "c)(0).PadLeft(10, "0"c) & "</LocTo>")
                End If

                If CType(rptMain.Items(i).FindControl("txtAllocation"), TextBox).Text.Trim().Equals(String.Empty) Then
                    ' invalid
                    SetWarningShortMessage("Allocation must be entered")
                    Return Nothing
                Else
                    ' valid
                    sbXMLToSave.Append("<Allocation>" & CType(rptMain.Items(i).FindControl("txtAllocation"), TextBox).Text.Trim() & "</Allocation>")
                End If

                sbXMLToSave.Append("<AddedBy>" & CType(rptMain.Items(i).FindControl("txtAddedBy"), TextBox).Text & "</AddedBy>")
                sbXMLToSave.Append("<Remove>" & IIf(CType(rptMain.Items(i).FindControl("chkRemove"), CheckBox).Checked, "1", "") & "</Remove>")
                sbXMLToSave.Append("</Dvs_Allocation>")

            End If

        Next

        sbXMLToSave.Append("</Allocations>")

        sbXMLToSave.Append("<UsrId>" & UserCode & "</UsrId>")
        sbXMLToSave.Append("<PrgmName>VehicleAllocation.aspx</PrgmName>")

        sbXMLToSave.Append("</Root>")

        xmlNew.LoadXml(sbXMLToSave.ToString())

        Return xmlNew
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave1.Click
        SaveData(True)
    End Sub

    Sub SaveData(ByVal SaveFlag As Boolean)
        ' save functionality
        Dim xmlToSave, xmlReturn As XmlDocument
        xmlToSave = BuildXMLToSave()
        ' check for "no change"
        If xmlToSave Is Nothing Then
            Exit Sub
        End If
        If xmlToSave.DocumentElement.SelectSingleNode("Allocations").ChildNodes.Count = 0 Then
            SetWarningShortMessage("No changes have been made!")
            Exit Sub
        End If
        ' some error checking here?
        xmlReturn = Aurora.OpsAndLogs.Services.VehicleAllocation.SaveVehicleAllocationData(xmlToSave, SaveFlag)
        If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' all fine
            SetInformationMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            ' refresh the data
            LoadVehicleAllocationTable()
        Else
            ' theres an error!
            SetErrorMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
        End If
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click, btnRemove1.Click
        ' remove functionality

        ' check if anything selected
        Dim bSomeThingSelected As Boolean = False
        For i As Integer = 0 To rptMain.Items.Count - 2
            If CType(rptMain.Items(i).FindControl("chkRemove"), CheckBox).Checked Then
                ' theres something
                bSomeThingSelected = True
                Exit For
            End If
        Next

        If bSomeThingSelected Then
            ' ask if okay to remove
            ' ask if we should really delete!
            ViewState(VIEWSTATE_OPTION) = "Remove"
            cnfConfirmation.Text = "Are you sure you wish to continue with removing the selected items?"
            cnfConfirmation.Title = "Confirmation required"
            cnfConfirmation.Show()
        Else
            SetWarningShortMessage("Please select the Relocation Record(s) to Delete")
            Exit Sub
        End If
    End Sub


    ' ViewState(VIEWSTATE_OPTION) 
    Protected Sub Confirmation_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cnfConfirmation.PostBack

        If leftButton Then
            If CStr(ViewState(VIEWSTATE_OPTION)) = "Remove" Then
                ViewState(VIEWSTATE_OPTION) = Nothing ' resetting this!
                SaveData(False)
            End If
        End If
    End Sub


    Protected Sub cmbCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCountry.SelectedIndexChanged
        LoadVehicleAllocationTable()
        EnableButtons()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click, btnCancel1.Click
        LoadPageContents()
    End Sub
End Class
