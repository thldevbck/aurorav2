Imports System
Imports System.Data
Imports System.Drawing
Imports System.Collections.Generic
Imports System.Text
Imports System.IO
Imports System.ComponentModel
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls

Imports Aurora.Common
Imports Aurora.OpsAndLogs.Data
Imports Aurora.OpsAndLogs.Data.BlockingRuleDataSet

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.MaintainBlockingRules)> _
Partial Class OpsAndLogs_BlockingRuleEdit
    Inherits AuroraPage

    Private _initException As Exception

    Private _countryCode As String = Nothing
    Private _countryName As String = Nothing

    Private _blrId As String = Nothing
    Private _blrAllow As Boolean = False
    Private _blockingRuleDataSet As New BlockingRuleDataSet()
    Private _blockingRuleRow As BlockingRuleRow

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            Return "" _
             & IIf(String.IsNullOrEmpty(_blrId), "New", "Edit") & " " _
             & IIf(_blrAllow, "Allowing", "Blocking") & " " _
             & "Rule"
        End Get
    End Property

    Protected ReadOnly Property IsIncCheckBoxIds() As String
        Get
            Dim json As New System.Web.Script.Serialization.JavaScriptSerializer()
            Return json.Serialize(New String() { _
                productIsInclCheckBox.ClientID, _
                brandIsInclCheckBox.ClientID, _
                packageIsInclCheckBox.ClientID, _
                locationFromIsInclCheckBox.ClientID, _
                locationToIsInclCheckBox.ClientID, _
                agentCountryIsInclCheckBox.ClientID, _
                agentCategoryIsInclCheckBox.ClientID, _
                agentIsInclCheckBox.ClientID})
        End Get
    End Property

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        Try
            If Not String.IsNullOrEmpty(Request.QueryString("BlrId")) Then
                _blrId = Request.QueryString("BlrId").Trim()
            End If
            Try
                If Not String.IsNullOrEmpty(Request("BlrAllow")) Then
                    _blrAllow = Boolean.Parse(Request("BlrAllow"))
                End If
            Catch
            End Try

            _countryCode = Request.QueryString("CtyCode")

            MyBase.OnInit(e)

            DataRepository.GetBlockingRuleLookups(_blockingRuleDataSet, Me.CompanyCode, _countryCode)

            For Each countryRow As CountryRow In _blockingRuleDataSet.Country
                If Not countryRow.IsCtyHasProductsNull AndAlso countryRow.CtyHasProducts _
                 AndAlso countryRow.CtyCode = _countryCode Then
                    _countryName = countryRow.CtyName
                End If
            Next
            If _countryName Is Nothing Then
                Throw New Exception("Invalid Country")
            End If

            If Not String.IsNullOrEmpty(_blrId) Then
                DataRepository.GetBlockingRule(_blockingRuleDataSet, Me.CompanyCode, _countryCode, _blrId)
                _blockingRuleRow = _blockingRuleDataSet.BlockingRule.FindByBlrId(_blrId)
                If _blockingRuleRow Is Nothing Then
                    Throw New Exception("Blocking/Allowing Rule not found")
                End If
            Else
                _blockingRuleRow = _blockingRuleDataSet.BlockingRule.NewBlockingRuleRow()
                _blockingRuleRow.BlrId = Data.GetNewId()
                _blockingRuleRow.BlrAllow = _blrAllow
                _blockingRuleRow.BlrOverrideable = _blrAllow
                _blockingRuleRow.BlrComCode = Me.CompanyCode
                _blockingRuleRow.BlrCtyCode = _countryCode
                _blockingRuleRow.AddPrgmName = ""
                _blockingRuleRow.AddUsrId = ""
                _blockingRuleRow.AddDateTime = Date.Now
                _blockingRuleRow.IntegrityNo = 1
                _blockingRuleDataSet.BlockingRule.AddBlockingRuleRow(_blockingRuleRow)
            End If

            uomDropDown.Items.Clear()
            For Each codeRow As CodeRow In _blockingRuleDataSet.Code
                If codeRow.CodCdtNum = DataConstants.CodeType_Unit_of_Measure Then _
                    uomDropDown.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            Next

            productDropDownList.Items.Clear()
            productDropDownList.Items.Add(New ListItem("(Select)", ""))
            For Each productRow As ProductRow In _blockingRuleDataSet.Product
                productDropDownList.Items.Add(New ListItem(productRow.PrdShortName & " - " & productRow.PrdName, productRow.PrdId))
            Next

            brandDropDownList.Items.Clear()
            brandDropDownList.Items.Add(New ListItem("(Select)", ""))
            For Each brandRow As BrandRow In _blockingRuleDataSet.Brand
                brandDropDownList.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
            Next

            locationFromDropDownList.Items.Clear()
            locationToDropDownList.Items.Clear()
            Dim li As New ListItem("(Select)", "")
            locationFromDropDownList.Items.Add(li)
            locationToDropDownList.Items.Add(li)
            For Each locationRow As LocationRow In _blockingRuleDataSet.Location
                li = New ListItem(locationRow.LocCode & " - " & locationRow.LocName, locationRow.LocCode)
                locationFromDropDownList.Items.Add(li)
                locationToDropDownList.Items.Add(li)
            Next

            agentCountryDropDownList.Items.Clear()
            agentCountryDropDownList.Items.Add(New ListItem("(Select)", ""))
            For Each countryRow As CountryRow In _blockingRuleDataSet.Country
                agentCountryDropDownList.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
            Next

            agentCategoryDropDownList.Items.Clear()
            agentCategoryDropDownList.Items.Add(New ListItem("(Select)", ""))
            For Each codeRow As CodeRow In _blockingRuleDataSet.Code
                If codeRow.CodCdtNum = DataConstants.CodeType_Agent_Category Then _
                    agentCategoryDropDownList.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            Next
        Catch ex As Exception
            _initException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
            Aurora.Common.Logging.LogError(Me.FunctionCode, ex.Message, ex)
        End Try
    End Sub

    Private Sub BuildBlockingRule(ByVal useViewState As Boolean)
        countryTextBox.Text = _countryCode & " - " & _countryName
        typeTextBox.Text = IIf(_blockingRuleRow.BlrAllow, "Allowing", "Blocking")

        If useViewState Then Return

        If Not _blockingRuleRow.IsBlrDurationOfRentalNull Then
            reasonTextBox.Text = _blockingRuleRow.BlrReason
        Else
            reasonTextBox.Text = ""
        End If
        If Not _blockingRuleRow.IsBlrDurationOfRentalNull Then
            durationTextBox.Text = _blockingRuleRow.BlrDurationOfRental.ToString()
        Else
            durationTextBox.Text = ""
        End If
        If Not _blockingRuleRow.IsBlrBookAheadPeriodNull Then
            bookAheadTextBox.Text = _blockingRuleRow.BlrBookAheadPeriod.ToString()
        Else
            bookAheadTextBox.Text = ""
        End If
        If Not _blockingRuleRow.IsBlrApplyToDeptNull Then
            byDateRadioButtonList.SelectedValue = _blockingRuleRow.BlrApplyToDept
        Else
            byDateRadioButtonList.SelectedValue = "Out"
        End If
        If Not _blockingRuleRow.IsBlrCodUOMIdNull Then
            uomDropDown.SelectedValue = _blockingRuleRow.BlrCodUOMId
        Else
            uomDropDown.SelectedIndex = 0
        End If
        For Each listItem As ListItem In flagsCheckBoxList.Items
            listItem.Selected = _blockingRuleRow.Table.Columns.Contains(listItem.Value) _
             AndAlso Not _blockingRuleRow.IsNull(listItem.Value) _
             AndAlso CType(_blockingRuleRow(listItem.Value), Boolean)
        Next
        If Not _blockingRuleRow.IsBlrDateFromNull Then
            bookedFromTextBox.Date = _blockingRuleRow.BlrDateFrom
        Else
            bookedFromTextBox.Text = ""
        End If
        If Not _blockingRuleRow.IsBlrDateToNull Then
            bookedToTextBox.Date = _blockingRuleRow.BlrDateTo
        Else
            bookedToTextBox.Text = ""
        End If

        productCheckBoxList.Items.Clear()
        For Each blockingRuleProductRow As BlockingRuleProductRow In _blockingRuleRow.GetBlockingRuleProductRows()
            Dim productRow As ProductRow = blockingRuleProductRow.ProductRow
            If productRow IsNot Nothing Then
                productCheckBoxList.Items.Add(New ListItem(productRow.PrdShortName, productRow.PrdId))
            End If
        Next
        productIsInclCheckBox.Checked = Not _blockingRuleRow.ProductIsIncl

        brandCheckBoxList.Items.Clear()
        For Each blockingRuleBrandRow As BlockingRuleBrandRow In _blockingRuleRow.GetBlockingRuleBrandRows()
            Dim brandRow As BrandRow = BlockingRuleBrandRow.BrandRow
            If brandRow IsNot Nothing Then
                brandCheckBoxList.Items.Add(New ListItem(brandRow.BrdCode & " - " & brandRow.BrdName, brandRow.BrdCode))
            End If
        Next
        brandIsInclCheckBox.Checked = Not _blockingRuleRow.BrandIsIncl

        packageCheckBoxList.Items.Clear()
        For Each blockingRulePackageRow As BlockingRulePackageRow In _blockingRuleRow.GetBlockingRulePackageRows()
            Dim packageRow As PackageRow = BlockingRulePackageRow.PackageRow
            If packageRow IsNot Nothing Then
                packageCheckBoxList.Items.Add(New ListItem(packageRow.PkgCode, packageRow.PkgId))
            End If
        Next
        packageIsInclCheckBox.Checked = Not _blockingRuleRow.PackageIsIncl

        locationFromCheckBoxList.Items.Clear()
        For Each blockingRuleLocationRow As BlockingRuleLocationRow In _blockingRuleRow.GetBlockingRuleLocationRows()
            Dim locationRow As LocationRow = blockingRuleLocationRow.LocationRow
            If blockingRuleLocationRow.LocationRow IsNot Nothing _
             AndAlso blockingRuleLocationRow.BrlDirection = "From" Then
                locationFromCheckBoxList.Items.Add(New ListItem(locationRow.LocCode, locationRow.LocCode))
            End If
        Next
        locationFromIsInclCheckBox.Checked = Not _blockingRuleRow.LocationFromIsIncl

        locationToCheckBoxList.Items.Clear()
        For Each blockingRuleLocationRow As BlockingRuleLocationRow In _blockingRuleRow.GetBlockingRuleLocationRows()
            Dim locationRow As LocationRow = blockingRuleLocationRow.LocationRow
            If blockingRuleLocationRow.LocationRow IsNot Nothing _
             AndAlso blockingRuleLocationRow.BrlDirection = "To" Then
                locationToCheckBoxList.Items.Add(New ListItem(locationRow.LocCode, locationRow.LocCode))
            End If
        Next
        locationToIsInclCheckBox.Checked = Not _blockingRuleRow.LocationToIsIncl

        agentCountryCheckBoxList.Items.Clear()
        For Each blockingRuleAgentCountryRow As BlockingRuleAgentCountryRow In _blockingRuleRow.GetBlockingRuleAgentCountryRows()
            Dim countryRow As CountryRow = blockingRuleAgentCountryRow.CountryRow
            If countryRow IsNot Nothing Then
                agentCountryCheckBoxList.Items.Add(New ListItem(countryRow.CtyCode & " - " & countryRow.CtyName, countryRow.CtyCode))
            End If
        Next
        agentIsInclCheckBox.Checked = Not _blockingRuleRow.AgentIsIncl

        agentCategoryCheckBoxList.Items.Clear()
        For Each blockingRuleAgentCategoryRow As BlockingRuleAgentCategoryRow In _blockingRuleRow.GetBlockingRuleAgentCategoryRows()
            Dim codeRow As CodeRow = blockingRuleAgentCategoryRow.CodeRow
            If codeRow IsNot Nothing Then
                agentCategoryCheckBoxList.Items.Add(New ListItem(codeRow.CodDesc, codeRow.CodId))
            End If
        Next
        agentCategoryIsInclCheckBox.Checked = Not _blockingRuleRow.AgentCategoryIsIncl

        agentCheckBoxList.Items.Clear()
        For Each blockingRuleAgentRow As BlockingRuleAgentRow In _blockingRuleRow.GetBlockingRuleAgentRows()
            Dim agentRow As AgentRow = blockingRuleAgentRow.AgentRow
            If agentRow IsNot Nothing Then
                agentCheckBoxList.Items.Add(New ListItem(agentRow.AgnCode, agentRow.AgnId))
            End If
        Next
        agentIsInclCheckBox.Checked = Not _blockingRuleRow.AgentIsIncl

        flagsCheckBoxList.Items(0).Enabled = Not _blockingRuleRow.BlrAllow

        deleteButton.Visible = Not String.IsNullOrEmpty(_blrId)
        updateButton.Visible = Not String.IsNullOrEmpty(_blrId)
        createButton.Visible = String.IsNullOrEmpty(_blrId)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If _initException IsNot Nothing Then Return

        If Not Page.IsPostBack Then
            BuildBlockingRule(False)
            If Not String.IsNullOrEmpty(Request("Create")) Then
                Me.AddInformationMessage(IIf(_blockingRuleRow.BlrAllow, "Allowing", "Blocking") & " Rule created")
            End If
        End If

        If bookedFromTextBox.Date = Date.MinValue OrElse bookedToTextBox.Date = Date.MinValue Then
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.ErrorColor)
        ElseIf bookedToTextBox.Date < Date.Now.Date Then
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.PastColor)
        ElseIf bookedFromTextBox.Date > Date.Now.Date Then
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.FutureColor)
        Else
            statusTable.BgColor = System.Drawing.ColorTranslator.ToHtml(DataConstants.CurrentColor)
        End If

    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("BlockingRuleList.aspx")
    End Sub

    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click
        For Each checkBoxList As CheckBoxList In New CheckBoxList() { _
         productCheckBoxList, _
         brandCheckBoxList, _
         packageCheckBoxList, _
         locationFromCheckBoxList, _
         locationToCheckBoxList, _
         agentCountryCheckBoxList, _
         agentCategoryCheckBoxList, _
         agentCheckBoxList}
            Dim i As Integer = 0
            While i < checkBoxList.Items.Count
                If checkBoxList.Items(i).Selected Then
                    checkBoxList.Items.RemoveAt(i)
                Else
                    i += 1
                End If
            End While
        Next
    End Sub

    Private Sub addRuleItem(ByVal checkBoxList As CheckBoxList, ByVal dataId As String, ByVal text As String, ByVal stripDescription As Boolean)
        If String.IsNullOrEmpty(dataId) OrElse String.IsNullOrEmpty(text) Then Return
        If stripDescription AndAlso text.IndexOf(" - ") > 0 Then text = text.Substring(0, text.IndexOf(" - "))

        For Each listItem As ListItem In checkBoxList.Items
            If listItem.Value = dataId Then Return
        Next

        checkBoxList.Items.Add(New ListItem(text, dataId))
    End Sub

    Protected Sub productDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles productDropDownList.SelectedIndexChanged
        addRuleItem(productCheckBoxList, productDropDownList.SelectedItem.Value, productDropDownList.SelectedItem.Text, True)
        productDropDownList.SelectedIndex = 0
    End Sub

    Protected Sub brandDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles brandDropDownList.SelectedIndexChanged
        addRuleItem(brandCheckBoxList, brandDropDownList.SelectedItem.Value, brandDropDownList.SelectedItem.Text, False)
        brandDropDownList.SelectedIndex = 0
    End Sub

    Protected Sub packageAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles packageAddButton.Click
        addRuleItem(packageCheckBoxList, packagePickerControl.DataId, packagePickerControl.Text, False)
        packagePickerControl.DataId = Nothing : packagePickerControl.Text = ""
    End Sub

    Protected Sub locationFromDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationFromDropDownList.SelectedIndexChanged
        addRuleItem(locationFromCheckBoxList, locationFromDropDownList.SelectedItem.Value, locationFromDropDownList.SelectedItem.Text, True)
        locationFromDropDownList.SelectedIndex = 0
    End Sub

    Protected Sub locationToDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationToDropDownList.SelectedIndexChanged
        addRuleItem(locationToCheckBoxList, locationToDropDownList.SelectedItem.Value, locationToDropDownList.SelectedItem.Text, True)
        locationToDropDownList.SelectedIndex = 0
    End Sub

    Protected Sub agentCountryDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentCountryDropDownList.SelectedIndexChanged
        addRuleItem(agentCountryCheckBoxList, agentCountryDropDownList.SelectedItem.Value, agentCountryDropDownList.SelectedItem.Text, False)
        agentCountryDropDownList.SelectedIndex = 0
    End Sub

    Protected Sub agentCategoryDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentCategoryDropDownList.SelectedIndexChanged
        addRuleItem(agentCategoryCheckBoxList, agentCategoryDropDownList.SelectedItem.Value, agentCategoryDropDownList.SelectedItem.Text, False)
        agentCategoryDropDownList.SelectedIndex = 0
    End Sub

    Protected Sub agentAddButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentAddButton.Click
        addRuleItem(agentCheckBoxList, agentPickerControl.DataId, agentPickerControl.Text, False)
        agentPickerControl.DataId = Nothing : agentPickerControl.Text = ""
    End Sub

    Private Sub ValidationBlockingRule()
        If Not durationTextBox.IsTextValid Then Throw New ValidationException(durationTextBox.ErrorMessage)
        If Not bookAheadTextBox.IsTextValid Then Throw New ValidationException(bookAheadTextBox.ErrorMessage)
        If Not bookedFromTextBox.IsValid Then Throw New ValidationException("Invalid booked from date")
        If Not bookedToTextBox.IsValid Then Throw New ValidationException("Invalid booked to date")
        If bookedFromTextBox.Date > bookedToTextBox.Date Then Throw New ValidationException("Invalid booked from date, it cannot be greater than then booked to date")
    End Sub

    Private Sub UpdateBlockingRuleItemsFromUI( _
     ByVal checkBoxList As CheckBoxList, _
     ByVal tableName As String, _
     ByVal blrFieldName As String, _
     ByVal linkFieldName As String, _
     ByVal isIncFieldName As String, _
     ByVal isInclValue As Boolean, _
     ByVal extraKeyName As String, _
     ByVal extraKeyValue As String, _
     ByVal extraDataName As String, _
     ByVal extraDataValue As String)

        ' handle updates or creates
        For Each listItem As ListItem In checkBoxList.Items
            Dim dataRow As DataRow = Nothing
            For Each dataRow0 As DataRow In _blockingRuleDataSet.Tables(tableName).Rows
                If dataRow0(blrFieldName) = _blockingRuleRow.BlrId _
                 AndAlso dataRow0(linkFieldName) = listItem.Value _
                 AndAlso (String.IsNullOrEmpty(extraKeyName) OrElse dataRow0(extraKeyName) = extraKeyValue) Then
                    dataRow = dataRow0
                    Exit For
                End If
            Next
            If dataRow Is Nothing Then
                dataRow = _blockingRuleDataSet.Tables(tableName).NewRow()
                dataRow(blrFieldName) = _blockingRuleRow.BlrId
                dataRow(linkFieldName) = listItem.Value
                ' 18.3.9 - Shoel - Fix for missing audit data!!!
                dataRow("AddPrgmName") = PrgmName
                dataRow("AddUsrId") = UserCode
                ' 18.3.9 - Shoel - Fix for missing audit data!!!
                dataRow("AddDateTime") = Date.Now
                dataRow("IntegrityNo") = 1
                dataRow(isIncFieldName) = isInclValue
                If Not String.IsNullOrEmpty(extraKeyName) Then dataRow(extraKeyName) = extraKeyValue
                If Not String.IsNullOrEmpty(extraDataName) Then dataRow(extraDataName) = extraDataValue
                _blockingRuleDataSet.Tables(tableName).Rows.Add(dataRow)
            Else
                ' 18.3.9 - Shoel - Fix for missing audit data!!!
                dataRow("ModUsrId") = UserCode
                dataRow("ModDateTime") = Date.Now
                ' 18.3.9 - Shoel - Fix for missing audit data!!!
                dataRow(isIncFieldName) = isInclValue
                If Not String.IsNullOrEmpty(extraDataName) Then dataRow(extraDataName) = extraDataValue
            End If
            Aurora.Common.Data.ExecuteDataRow(dataRow)
        Next

        ' handle deletes
        For Each dataRow As DataRow In _blockingRuleDataSet.Tables(tableName).Rows
            If Not String.IsNullOrEmpty(extraKeyName) AndAlso dataRow(extraKeyName) <> extraKeyValue Then Continue For

            Dim listItem As ListItem = Nothing
            For Each listItem0 As ListItem In checkBoxList.Items
                If dataRow(blrFieldName) = _blockingRuleRow.BlrId AndAlso dataRow(linkFieldName) = listItem0.Value Then
                    listItem = listItem0
                    Exit For
                End If
            Next
            If listItem IsNot Nothing Then Continue For

            dataRow.Delete()
            Aurora.Common.Data.ExecuteDataRow(dataRow)
        Next

        ' remove deleted rows so nothing else accesses it causing an exception
        Dim i As Integer
        While i < _blockingRuleDataSet.Tables(tableName).Rows.Count
            Dim dataRow As DataRow = _blockingRuleDataSet.Tables(tableName).Rows(i)
            If dataRow.RowState = DataRowState.Deleted Then
                _blockingRuleDataSet.Tables(tableName).Rows.Remove(dataRow)
            Else
                i += 1
            End If
        End While
    End Sub

    Private Sub UpdateBlockingRuleFromUI()
        If _blrId Is Nothing Then
            ' CREATE
            _blockingRuleRow.BlrId = Aurora.Common.Data.GetNewId()
            _blockingRuleRow.AddDateTime = Now
            _blockingRuleRow.AddPrgmName = PrgmName
            _blockingRuleRow.AddUsrId = UserCode
        Else
            ' UPDATE
            _blockingRuleRow.ModDateTime = Now
            _blockingRuleRow.ModUsrId = UserCode
        End If
        _blockingRuleRow.BlrReason = reasonTextBox.Text
        _blockingRuleRow.BlrDurationOfRental = Integer.Parse(durationTextBox.Text)
        _blockingRuleRow.BlrBookAheadPeriod = Integer.Parse(bookAheadTextBox.Text)
        _blockingRuleRow.BlrApplyToDept = byDateRadioButtonList.SelectedValue
        _blockingRuleRow.BlrCodUOMId = uomDropDown.SelectedValue
        For Each listItem As ListItem In flagsCheckBoxList.Items
            If _blockingRuleRow.Table.Columns.Contains(listItem.Value) Then
                _blockingRuleRow(listItem.Value) = listItem.Selected
            End If
        Next
        _blockingRuleRow.BlrDateFrom = bookedFromTextBox.Date
        _blockingRuleRow.BlrDateTo = bookedToTextBox.Date



        Try
            Aurora.Common.Data.ExecuteDataRow(_blockingRuleRow, UserCode, "/Aurora/web/OpsAndLogs/BlockingRuleEdit.aspx")

            UpdateBlockingRuleItemsFromUI(productCheckBoxList, "BlockingRuleProduct", "BlpBlrId", "BlpPrdId", "BlpIsIncl", Not productIsInclCheckBox.Checked, Nothing, Nothing, Nothing, Nothing)
            UpdateBlockingRuleItemsFromUI(brandCheckBoxList, "BlockingRuleBrand", "BlbBlrId", "BlbBrdCode", "BlbIsIncl", Not brandIsInclCheckBox.Checked, Nothing, Nothing, Nothing, Nothing)
            UpdateBlockingRuleItemsFromUI(packageCheckBoxList, "BlockingRulePackage", "BrpBlrId", "BrpPkgId", "BrpIsIncl", Not packageIsInclCheckBox.Checked, Nothing, Nothing, Nothing, Nothing)
            UpdateBlockingRuleItemsFromUI(locationFromCheckBoxList, "BlockingRuleLocation", "BrlBlrId", "BrlLocCode", "BrlIsIncl", Not locationFromIsInclCheckBox.Checked, "BrlDirection", "From", Nothing, Nothing)
            UpdateBlockingRuleItemsFromUI(locationToCheckBoxList, "BlockingRuleLocation", "BrlBlrId", "BrlLocCode", "BrlIsIncl", Not locationToIsInclCheckBox.Checked, "BrlDirection", "To", Nothing, Nothing)
            UpdateBlockingRuleItemsFromUI(agentCategoryCheckBoxList, "BlockingRuleAgentCategory", "BacBlrId", "BacCodAgentCtgyId", "BacIsIncl", Not agentCategoryIsInclCheckBox.Checked, Nothing, Nothing, "BacDesc", "")
            UpdateBlockingRuleItemsFromUI(agentCountryCheckBoxList, "BlockingRuleAgentCountry", "BrcBlrId", "BrcCtyCode", "BrcIsIncl", Not agentCountryIsInclCheckBox.Checked, Nothing, Nothing, Nothing, Nothing)
            UpdateBlockingRuleItemsFromUI(agentCheckBoxList, "BlockingRuleAgent", "BraBlrId", "BraAgnId", "BraIsIncl", Not agentIsInclCheckBox.Checked, Nothing, Nothing, Nothing, Nothing)
        Catch ex As Exception
            Dim description As String = "The was an error saving the " & IIf(_blockingRuleRow.BlrAllow, "Allowing", "Blocking") & " Rule"
            Me.AddErrorMessage(description)
            Aurora.Common.Logging.LogError(Me.FunctionCode, description, ex)
            Return
        End Try

        If _blrId Is Nothing Then
            Response.Redirect("BlockingRuleEdit.aspx" _
             & "?BlrId=" & _blockingRuleRow.BlrId _
             & "&BlrAllow=" & Server.UrlEncode(_blrAllow.ToString()) _
             & "&CtyCode=" & Server.UrlEncode(_countryCode) _
             & "&Create=true")
        Else
            Me.AddInformationMessage(IIf(_blockingRuleRow.BlrAllow, "Allowing", "Blocking") & " Rule updated")
        End If
    End Sub

    Protected Sub createButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles createButton.Click
        Try
            ValidationBlockingRule()
        Catch ex As ValidationException
            Me.AddWarningMessage(ex.Message)
            Return
        End Try

        UpdateBlockingRuleFromUI()
    End Sub

    Protected Sub updateButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateButton.Click
        Try
            ValidationBlockingRule()
        Catch ex As ValidationException
            Me.AddWarningMessage(ex.Message)
            Return
        End Try

        UpdateBlockingRuleFromUI()
    End Sub

    Protected Sub deleteButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles deleteButton.Click
        Try
            DataRepository.DeleteBlockingRule(Me.CompanyCode, _countryCode, _blockingRuleRow.BlrId)
        Catch ex As ValidationException
            Dim description As String = "The was an error deleting the " & IIf(_blockingRuleRow.BlrAllow, "Allowing", "Blocking") & " Rule"
            Me.AddErrorMessage(description)
            Aurora.Common.Logging.LogError(Me.FunctionCode, description, ex)
            Return
        End Try

        Response.Redirect("BlockingRuleList.aspx")
    End Sub

End Class
