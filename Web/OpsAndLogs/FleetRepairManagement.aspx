<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	FleetRepairManagement.aspx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	15.4.8 / Shoel - Base version
''                      22.5.8 / Shoel - Common JS moved out of this file
''                      23.5.8 / Shoel - Fixed issue with date picker moving up and down
''                       9.7.8 / Shoel - Button css ui change
''                      25.8.8 / Shoel - MaxLengths added to some fields
''                      25.9.8 / Shoel - 3 fields made mandatory
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="FleetRepairManagement.aspx.vb" Inherits="OpsAndLogs_FleetRepairManagement"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxUserControl"
    TagPrefix="ucConfirm" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="ucDate" %>
<%@ Register Src="../UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl"
    TagPrefix="ucTime" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="ucPicker" %>
<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
            .hideTD
            {
                visibility: hidden;
            }
    </style>
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JS/OpsAndLogsCommon.js" type="text/javascript" language="javascript"></script>
   
    <script language="javascript" type="text/javascript">

        function ShowConfirm(MessageText, HeaderText) {
            var confirmationBoxBehavior = $find('programmaticConfirmationBoxBehavior');
            document.getElementById(confirmationBoxBehavior._PopupDragHandleControlID).children[0].innerText = HeaderText;
            document.getElementById("tdMessageHolder").children[0].innerText = MessageText;
            //confirmationBoxBehavior.show();
        }

        function GoBack(BackPath) {
            window.location = BackPath;
            return false;
        }

        function ValidateBeforeSave(OdoStartCID, BranchCID, FromDateCID, FromTimeCID, ToDateCID, ToTimeCID, ReportType1CID, ServAgentCID) {
            if (document.getElementById(OdoStartCID).value.trim() == "") {
                setWarningShortMessage("Odometer Start reading must be entered");
                return false;
            }

            if (document.getElementById(BranchCID).value.trim() == "") {
                setWarningShortMessage("Branch must be entered");
                return false;
            }

            if (document.getElementById(FromDateCID).value.trim() == "") {
                setWarningShortMessage("From date must be entered");
                return false;
            }

            if (document.getElementById(FromTimeCID).value.trim() == "") {
                setWarningShortMessage("From time must be entered");
                return false;
            }

            if (document.getElementById(ToDateCID).value.trim() == "") {
                setWarningShortMessage("To date must be entered");
                return false;
            }

            if (document.getElementById(ToTimeCID).value.trim() == "") {
                setWarningShortMessage("To time must be entered");
                return false;
            }

            if (document.getElementById(ReportType1CID).value.trim() == "") {
                setWarningShortMessage("Repair Type 1 must be selected");
                return false;
            }

            if (document.getElementById(ServAgentCID).value.trim() == "") {
                setWarningShortMessage("Service Agent must be entered");
                return false;
            }

            return true;
        }

        //String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ''); };

    </script>
    <script src="../AIMS/JS/GenericAIMSAccess.js" type="text/javascript"></script>
     <script type="text/javascript">
         var isFullAccess = false;
         var isPartialAccess = false;
         var isViewAccess = false;
    </script>
</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <ucConfirm:ConfimationBoxUserControl ID="confimationBoxControl" runat="server" LeftButton_Text="Continue"
                RightButton_Text="Cancel" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
                Title="Booking Check In Check Out" />
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableUnitAndDescription">
                            <tr>
                                <td width="150px">
                                    Unit #:
                                </td>
                                <td width="232px">
                                    <asp:TextBox ID="lblUnitNo" runat="server" ReadOnly="true" CssClass="Textbox_Standard"
                                        TabIndex="-1"></asp:TextBox>
                                </td>
                                <td  width="111px">
                                    Description:
                                </td>
                                <td>
                                    <asp:TextBox ID="lblDescription" runat="server" ReadOnly="true" CssClass="Textbox_Extralarge"
                                        TabIndex="-1" Width="150"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="150px">
                                    Incident #:
                                </td>
                                <td>
                                    <asp:TextBox ID="lblIncidentNo" runat="server" ReadOnly="true" CssClass="Textbox_Standard"
                                        TabIndex="-1"></asp:TextBox>
                                </td>
                                <td>
                                    Registration #:
                                </td>
                                <td>
                                    <asp:TextBox ID="lblRegNo" runat="server" ReadOnly="true" CssClass="Textbox_Standard"
                                        TabIndex="-1"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="150px">
                                    Purchase Order #:
                                </td>
                                <td>
                                    <asp:TextBox ID="lblPurchaseOrderNo" runat="server" ReadOnly="true" CssClass="Textbox_Standard"
                                        TabIndex="-1"></asp:TextBox>
                                </td>
                                <td>
                                    Completed:
                                </td>
                                <td>
                                    <asp:TextBox ID="lblCompleted" runat="server" ReadOnly="true" CssClass="Textbox_Standard"
                                        TabIndex="-1"></asp:TextBox>
                                </td>
                            </tr>
                </table>
            <br />

            <b>Setup Details</b>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableSetup">
                <tr>
                    <td width="150px">
                        Odometer Start:
                    </td>
                    <td width="232px">
                        <asp:TextBox ID="txtOdometerStart" runat="server" CssClass=""
                            onkeypress="return keyStrokeInt(event);" MaxLength="8" OnBlur="CheckContents(this);" Width="100"></asp:TextBox>&nbsp;*
                    </td>
                    <td>
                        Branch:
                    </td>
                    <td>
                        <ucPicker:PickerControl ID="pkrBranch" runat="server" PopupType="LOCATION" AppendDescription="true"
                            Width="125" />
                        &nbsp;*
                    </td>
                </tr>
                <tr>
                    <td width="150px">
                        From:
                    </td>
                    <td>
                        <ucDate:DateControl ID="dtcFromDate" runat="server" />
                        &nbsp;
                        <ucTime:TimeControl ID="tmcFromTime" runat="server" />
                        &nbsp;*
                    </td>
                    
                    <td>
                        To:
                    </td>
                    <td>
                        <ucDate:DateControl ID="dtcToDate" runat="server" />
                        &nbsp;
                        <ucTime:TimeControl ID="tmcToTime" runat="server" />
                        &nbsp;*
                    </td>
                    
                </tr>
                <tr>
                    <td width="150px">
                        Repair Type 1:
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbRepairType1" runat="server" CssClass="Dropdown_Standard" Width="150">
                        </asp:DropDownList>
                        &nbsp;*
                    </td>
                    <td>
                        Repair Type 2:
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbRepairType2" runat="server" CssClass="Dropdown_Standard" Width="150">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td width="150px">
                        Repair Type 3:
                    </td>
                    <td>
                        <asp:DropDownList ID="cmbRepairType3" runat="server" CssClass="Dropdown_Standard" Width="150">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Service Agent:
                    </td>
                    <td>
                        <ucPicker:PickerControl ID="pkrAgent" runat="server" PopupType="oplogfleetrepprovider"
                            AppendDescription="true" Width="145" />
                        &nbsp;*
                    </td>
                </tr>
                <tr>
                    <td width="150px">
                        Reason Description:
                    </td>
                    <td>
                        <asp:TextBox ID="txtReasonDescription" runat="server" MaxLength="128" CssClass="Textbox_Standard" Width="147"></asp:TextBox>
                    </td>
                    <td>
                        Off-Fleet:
                    </td>
                    <td>
                        <asp:CheckBox runat="server" ID="chkAvlSch" />
                    </td>
                </tr>
                <tr>
                    <td width="150px">
                        Service Agt/Loc Ref:
                    </td>
                    <td>
                        <asp:TextBox ID="txtServiceAgentLocationRef" runat="server" CssClass="Textbox_Standard" Width="147"
                            MaxLength="20"></asp:TextBox>
                    </td>
                    <%--<td width="150px">
                                    Agent Ph No:
                                </td>
                                <td width="250px" colspan="2">
                                    <asp:TextBox ID="txtAgentPhoneNo" MaxLength="24" runat="server" CssClass="Textbox_Standard"></asp:TextBox>
                                </td>--%>
                    <td>
                       <%-- Service Point:--%>
                        Odometer Due:
                    </td>
                    <td>
                        <!-- Disabled this field by nimesh on 20th July 2015 - REF https://thlonline.atlassian.net/browse/DSD-901 -->
                        <asp:DropDownList ID="cmbServicePoint" runat="server" Visible="false" CssClass="Dropdown_Standard" Enabled="false" Width="150">
                        </asp:DropDownList>
                         <asp:TextBox ID="txtOdometerDue" runat="server" ReadOnly="true" CssClass="textreadonly" Width="147"
                            MaxLength="20"></asp:TextBox>
                    </td>
                    
                </tr>
                <tr>
                     <!-- Removed this field by nimesh on 20th July 2015 - REF https://thlonline.atlassian.net/browse/DSD-901 -->
                    <td width="150px">
                     <%--  Odometer Due:--%>
                    </td>
                    <td>
                       <%-- <asp:TextBox ID="txtOdometerDue" runat="server" ReadOnly="true" CssClass="Textbox_Standard" Width="147"
                            MaxLength="20"></asp:TextBox>--%>
                    </td>
                </tr>
            </table>

            <br />
            <b>Completion Details</b>
            
             <table width="100%" border="0" cellspacing="0" cellpadding="0" id="tableCompletion">
                            <tr>
                                <td width="150px">
                                    Odometer End:
                                </td>
                                <td width="232px">
                                    <asp:TextBox ID="txtOdometerEnd" runat="server"  Width="150"
                                        onkeypress="return keyStrokeInt(event);"></asp:TextBox>&nbsp;*
                                </td>
                                <td  width="111px" class="hideTD">
                                    Completion:
                                </td>
                                <td class="hideTD">
                                    <ucDate:DateControl ID="dtcCompletionDate" runat="server" />
                                    &nbsp;*
                                    <ucTime:TimeControl ID="tmcCompletionTime" runat="server" />
                                    &nbsp;*
                                </td>
                            </tr>
                        </table>
            
            <br />
            <table width="85%" id="tableButtons" >
                            <tr align="right">
                                <td width="100%">
                                    <asp:Button ID="btnCreateUpdate" runat="server" Text="Create/Update" CssClass="Button_Standard Button_Save"
                                        Width="125px" />
                                    <asp:Button ID="btnComplete" runat="server" Text="Complete" CssClass="Button_Standard"
                                        Width="100px" />
                                    <asp:Button ID="btnNew" runat="server" Text="New" CssClass="Button_Standard Button_New"
                                        Width="100px"  Enabled = "false" Visible="false"/>
                                    <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset"
                                        Width="100px" />
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="Button_Standard Button_Delete"
                                        Width="100px" />
                                    <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                                        Width="100px" />
                                </td>
                            </tr>
                        </table>


                <ucConfirm:ConfimationBoxUserControl ID="confirmBoxOnComplete" 
                        runat="server" 
                        LeftButton_Text="Yes"
                        RightButton_Text="No" 
                        LeftButton_AutoPostBack="true" 
                        RightButton_AutoPostBack="true"
                        Title="Complete Repair Maintenance" />

                 <ucConfirm:ConfimationBoxUserControl ID="confirmBoxOnCreateAndUpdate" 
                            runat="server" 
                            RightButton_Text="Cancel" 
                            LeftButton_AutoPostBack="false" 
                            LeftButton_Visible = "false"
                            RightButton_AutoPostBack="false"
                             RightButton_Visible="false"
                            Title="Create / Update Repair Maintenance" />

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField Value="" runat="server" ID="ManufacturerIDhidden" />
    <asp:HiddenField Value="../AIMS/" runat="server" ID="AIMSListenerPath" />
    <asp:HiddenField Value="FleetRepairManagement" runat="server" ID="hiddenPageIdentity" />
</asp:Content>
