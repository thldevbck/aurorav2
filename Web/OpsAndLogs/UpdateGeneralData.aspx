<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false" CodeFile="UpdateGeneralData.aspx.vb" Inherits="OpsAndLogs_UpdateGeneralData" Title="Untitled Page" %>

<%@ Register Src="../UserControls/RegExTextBox/RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:Content ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <contenttemplate>    
            
            <hr />

            <asp:Panel ID="odometerPanel" runat="server" Enabled="false">
                <b>Update Odometer</b>
                
                <table width="100%">
                    <tr>
                        <td width="150">Unit Number:</td>
                        <td width="250">
                            <uc1:RegExTextBox 
                                ID="unitNumberTextBox" 
                                runat="server" 
                                Nullable="false"
                                ErrorMessage="Specify a valid unit number"
                                RegExPattern="^\d+$"
                                Text=""/>
                            &nbsp;*
                        </td>
                        <td width="150">Actual Odometer:</td>
                        <td>
                            <uc1:RegExTextBox 
                                ID="odoMeterTextBox" 
                                runat="server" 
                                Nullable="false"
                                ErrorMessage="Specify a valid odometer value"
                                RegExPattern="^\d+$"
                                Text=""/>
                            &nbsp;*
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:Button 
                                ID="odometerButton" 
                                runat="server" 
                                Text="Save" 
                                CssClass="Button_Standard Button_Save" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
            <br />
            <hr />

            <asp:Panel ID="undoCheckOutInPanel" runat="server" Enabled="false">
                <b>Undo Check-out/in</b>

                <table width="100%">
                    <tr>
                        <td width="150">Booking Reference:</td>
                        <td colspan="3">
                            <uc1:RegExTextBox 
                                ID="bookingRefTextBox" 
                                runat="server" 
                                Nullable="false"
                                ErrorMessage="Specify a valid booking reference"
                                RegExPattern="^\s*\w+/\d+\s*$"
                                Width="100px"
                                Text=""/>
                            &nbsp;*&nbsp;&nbsp;e.g.&nbsp;3000140/1
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:Button 
                                ID="undoCheckOutButton" 
                                runat="server" 
                                Text="Undo Check-out" 
                                CssClass="Button_Standard" />
                            &nbsp;
                            <asp:Button 
                                ID="undoCheckInButton" 
                                runat="server" 
                                Text="Undo Check-in" 
                                CssClass="Button_Standard" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            
            <br />
            <hr />
        </contenttemplate>
    </asp:UpdatePanel>
</asp:Content>
