
Imports Aurora.Common
Imports Aurora.OpsAndLogs.Services
Imports System.Data
Imports System.xml

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.FleetAssignmentAlternatives)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.ProductAssignmentAlternatives)> _
Partial Class OpsAndLogs_SubstitutionRuleFleetModelList
    Inherits AuroraPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then
                productGridView_Databind()
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try

    End Sub

    Private Sub productGridView_Databind()

        Dim ds As DataSet = New DataSet
        ds = SubstitutionRule.ListFleetAlternatives(CountryCode, UserCode)

        If ds.Tables(1).Rows.Count = 1 Then
            'countryLabel.Text = ds.Tables(1).Rows(0)("name")
            countryTextBox.Text = ds.Tables(1).Rows(0)("name")
        End If

        Dim dt As DataTable = ds.Tables(3)
        Dim modelName As String = ""
        For Each dr As DataRow In dt.Rows
            If dr.Item("FltModels") = modelName Then
                dr.Item("FltModels") = ""
            Else
                modelName = dr.Item("FltModels")
            End If
        Next

        productGridView.DataSource = ds.Tables(3)
        productGridView.DataBind()

    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(".\SubstitutionRuleProductList.aspx")
    End Sub

    Protected Function GetDate(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Dim dDate As Date
            dDate = Utility.ParseDateTime(d, Utility.SystemCulture)

            Return dDate.ToString(Aurora.Common.UserSettings.Current.ComDateFormat)
        Else
            Return ""
        End If


    End Function


End Class

