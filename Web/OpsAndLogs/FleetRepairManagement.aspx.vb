'' Change log!
''  ID4   - Shoel : date format thingie added !
'' 25.9.8 - Shoel : Uses ViewState now, plus many more fixes!
'' 04AUG2014 - see changes with this label rev:mia 4Aug2014
''          today in THL history, first day of Edwin (QA and APPlication developer)      
'' 28AUG2014 - rev:mia addition of AIMS_IsActivityExist         
'' 22SEPT2014 - rev:mia addition of Fleet_UpdateFleetRepairsAIMS
''                                  Fleet_AddFleetRepair
''                                  Fleet_DeleteFleetRepairs
''                      modified:   sp_getRepairsList


Imports System.Xml
Imports System.Data
Imports Aurora.OpsAndLogs.Services
Imports Aurora.Common
Imports Aurora.Common.UserSettings
Imports Aurora.Common.Data.Parameter.ParameterType

<AuroraMessageAttribute("GEN124")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.FleetRepairList)> _
<AuroraPageTitle("Repairs and Maintenance")> _
Partial Class OpsAndLogs_FleetRepairManagement
    Inherits AuroraPage


    Const ViewState_REPAIRID As String = "ViewState_RepMgmt_RepId"
    Const ViewState_REPAIRPAGEDATA As String = "ViewState_RepMgmt_RepPageData"
    Const ViewState_CONFIRMDELETE As String = "ViewState_RepMgmt_ConfirmDelete"
    Const ViewState_REPAIRERID As String = "ViewState_RepMgmt_RepairerID"
    Const ViewState_NEWRECORD As String = "ViewState_RepMgmt_NewRecord"
    Const ViewState_ASSETID As String = "ViewState_RepMgmt_AssetId"
    Const ViewState_BACKPATH As String = "ViewState_RepMgmt_BackPath"
    Const ViewState_UNITNO As String = "ViewState_RepMgmt_UnitNo"
    Const DVASS_MESSAGE_UPDATE_UKNOWN_VEHICLE As String = "HARD-FAIL (4): detail=335, message=DVASS: Unknown Vehicle Maintenance for "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            CleanViewState()

            btnCreateUpdate.Attributes.Add("OnClick", "return ValidateBeforeSave('" & txtOdometerStart.ClientID & "','" & pkrBranch.FindControl("pickerHidden").ClientID & "','" & dtcFromDate.FindControl("dateTextBox").ClientID & "','" & tmcFromTime.FindControl("timeTextBox").ClientID & "','" & dtcToDate.FindControl("dateTextBox").ClientID & "','" & tmcToTime.FindControl("timeTextBox").ClientID & "','" & cmbRepairType1.ClientID & "','" & pkrAgent.FindControl("pickerHidden").ClientID & "');")
            btnComplete.Attributes.Add("OnClick", "return ValidateBeforeSave('" & txtOdometerStart.ClientID & "','" & pkrBranch.FindControl("pickerHidden").ClientID & "','" & dtcFromDate.FindControl("dateTextBox").ClientID & "','" & tmcFromTime.FindControl("timeTextBox").ClientID & "','" & dtcToDate.FindControl("dateTextBox").ClientID & "','" & tmcToTime.FindControl("timeTextBox").ClientID & "','" & cmbRepairType1.ClientID & "','" & pkrAgent.FindControl("pickerHidden").ClientID & "');")

            ' new record setting 
            If Not Request.QueryString("NewRecord") Is Nothing Then
                ViewState(ViewState_NEWRECORD) = CBool(Request.QueryString("NewRecord"))
            Else
                ViewState(ViewState_NEWRECORD) = True ' not specified = true
            End If


            'ViewState_REPAIRID
            ViewState(ViewState_REPAIRID) = CStr(Request.QueryString("RepairId")) '52970
            If ViewState(ViewState_REPAIRID) <> "" Then
                LoadPage()
            ElseIf CStr(Request.QueryString("AssetId")) <> String.Empty Then
                'must be from the other page (List)
                ViewState(ViewState_ASSETID) = CStr(Request.QueryString("AssetId"))
                LoadNewXml(ViewState(ViewState_ASSETID))
            Else
                ViewState(ViewState_UNITNO) = CStr(Request.QueryString("UnitNo"))
                LoadNewXml("", ViewState(ViewState_UNITNO))
                'back page data is in Rental,booking,incident form

            End If
            If CStr(Request.QueryString("BackPage")) <> String.Empty Then
                'Updated by Nimesh on 29th may 2015 as request.urlreferrer was null when working on citrix, so passing from url as querystring - https://thlonline.atlassian.net/browse/DSD-783
                ' btnBack.Attributes.Add("OnClick", "return GoBack('" & Server.HtmlDecode(CStr(Request.QueryString("BackPage"))).Replace("$"c, "&"c) & "');")
                btnBack.Attributes.Add("OnClick", "return GoBack('" & Server.HtmlDecode(CStr(Request.QueryString("BackPage"))).Replace("$"c, "&"c) + "&FromURL=/web/OpsAndLogs/FleetRepairManagement.aspx" & "');")
                'Updated by Nimesh on 29th may 2015 as request.urlreferrer was null when working on citrix, so passing from url as querystring - https://thlonline.atlassian.net/browse/DSD-783
            End If

            ''rev:mia 18Aug2014 - users want to have a visible update message
            ''                    and update details
            If (Not String.IsNullOrEmpty(PostBackMessage)) Then
                SetInformationShortMessage(PostBackMessage)
                PostBackMessage = String.Empty

                txtOdometerEnd.ReadOnly = False
                'dtcCompletionDate.ReadOnly = False
                'tmcCompletionTime.ReadOnly = False
                btnDelete.Enabled = True

                ViewState(ViewState_CONFIRMDELETE) = Nothing
            End If
        End If
       

        ManufacturerIDhidden.Value = UserCode
    End Sub


    Public Sub LoadPage(Optional ByVal oNewXML As XmlDocument = Nothing, Optional ByVal PassedUnitNo As String = "")
        Dim oRepairPageDoc As XmlDocument

        If oNewXML Is Nothing Then
            oRepairPageDoc = Fleet.GetFleetRepair(ViewState(ViewState_REPAIRID), "", PassedUnitNo)
        Else
            'called from NEW click
            ' however disallow the following!!!
            If oNewXML.OuterXml = "<data><Root></Root></data>" Then
                SetErrorShortMessage("No data present")
                Exit Sub
            End If

            oRepairPageDoc = oNewXML
        End If

        'stuff in ViewState :)
        ViewState(ViewState_REPAIRPAGEDATA) = oRepairPageDoc.OuterXml

        BindRepairCombos()

        If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FleetAssetId") Is Nothing) AndAlso (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FleetAssetId").InnerText.Trim().Equals(String.Empty)) Then
            'dont try this unless you have an asset id
            ' that is if u havent come after an "incident" ;)
            BindScheduledServiceCombo(oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FleetAssetId").InnerText)
           
        End If

        'fill in the blanks
        If Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA") Is Nothing Then
            lblUnitNo.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@UnitNo").InnerText
            lblDescription.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ModelDesc").InnerText
            lblIncidentNo.Text = ViewState(ViewState_REPAIRID)
            lblRegNo.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@RegNo").InnerText
            lblPurchaseOrderNo.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@PONo").InnerText
            lblCompleted.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Completed").InnerText

            If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OdoSt") Is Nothing) Then
                txtOdometerStart.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OdoSt").InnerText
            End If

            If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OdoEnd") Is Nothing) Then
                txtOdometerEnd.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OdoEnd").InnerText
            End If


            pkrBranch.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Branch").InnerText

            If Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FrDt").InnerText.Trim().Equals(String.Empty) Then
                dtcFromDate.Date = ParseDateTime(oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FrDt").InnerText, SystemCulture)
            Else
                dtcFromDate.Text = String.Empty
            End If

            tmcFromTime.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FrTm").InnerText
            If tmcFromTime.Text.IndexOf(":"c) < 0 And Not tmcFromTime.Text.Trim().Equals(String.Empty) Then
                tmcFromTime.Text = tmcFromTime.Text.Substring(0, 2) & ":" & tmcFromTime.Text.Substring(2, 2)
            End If

            If Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ToDt").InnerText.Trim().Equals(String.Empty) Then
                dtcToDate.Date = ParseDateTime(oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ToDt").InnerText, SystemCulture)
            Else
                dtcToDate.Text = String.Empty
            End If

            tmcToTime.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ToTm").InnerText
            If tmcToTime.Text.IndexOf(":"c) < 0 And Not tmcToTime.Text.Trim().Equals(String.Empty) Then
                tmcToTime.Text = tmcToTime.Text.Substring(0, 2) & ":" & tmcToTime.Text.Substring(2, 2)
            End If

            cmbRepairType1.SelectedValue = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Reason").InnerText
            cmbRepairType2.SelectedValue = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Reason1").InnerText
            cmbRepairType3.SelectedValue = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Reason2").InnerText

            '' mod: nim 17th aug 2015 to get odometer due from fleet asset table 
            Dim iOdometerDue As String = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OdometerDue").InnerText
            If (iOdometerDue <> "") Then
                txtOdometerDue.Text = iOdometerDue
            Else
                txtOdometerDue.Text = "0"
            End If
            Dim iservicepoint As String = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ServPt").InnerText
            ''Changed by Nimesh on 2nd Sep where in we are not sending first servicepoint item to Sp
            'If (iservicepoint <> "") Then
            '    Dim txtToSearch As String = IIf(ViewState(ViewState_NEWRECORD) = True, iOdometerDue, iservicepoint)

            '    Dim listitem As ListItem = cmbServicePoint.Items.FindByText(txtToSearch)
            '    If listitem IsNot Nothing Then
            '        iservicepoint = listitem.Value

            '        cmbServicePoint.SelectedValue = listitem.Value
            '    End If

            'End If
            cmbServicePoint.SelectedIndex = 1
            ''Changed by Nimesh on 2nd Sep where in we are not sending first servicepoint item to Sp

            '' end mod:nim 17th Aug
            ''rev:mia 22Sept2014 - just found out in this view [AIMSProd].[dbo].[AI_FleetRepair], if reason description is empty then put 
            ''ReferenceDescription from AI_FleetRepairReasons_V
            ''ISNULL(dbo.FleetRepairs.ReasonDescription, dbo.AI_FleetRepairReasons_V.ReferenceDescription)
            txtReasonDescription.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OthReasonDesc").InnerText

            pkrAgent.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Provider").InnerText

            ''rev:mia 4Aug2014 - this function will trigger after CreateUpdateRepairMaintenanceRecord
            Try

                If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Provider") Is Nothing) Then
                    pkrAgent.DataId = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Provider").InnerText.Split("-"c)(0).Trim()
                End If


                If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Ref") Is Nothing) Then
                    txtServiceAgentLocationRef.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Ref").InnerText
                End If

                If (ViewState(ViewState_NEWRECORD) = True) Then
                    chkAvlSch.Checked = False
                    chkAvlSch.Enabled = True
                Else
                    chkAvlSch.Checked = IIf(oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Available").InnerText = "No", False, True)
                    IsDBOffFleet = IIf(oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@Available").InnerText = "No", False, True)
                End If
                '  <!-- Removed this field by nimesh on 20th July 2015 - REF https://thlonline.atlassian.net/browse/DSD-901 -->
                'If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ScheduledService") Is Nothing) Then
                '    txtServiceSchedule.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ScheduledService").InnerText
                'End If

                If (Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ModelCode") Is Nothing) Then
                    FleetModel = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ModelCode").InnerText
                End If


                ''rev:mia 21Aug2014- part of dirty tracking..its bad that we dont implement INotification..much easir than this hack
                Me.Branch = pkrBranch.Text.Split("-")(0)
                Me.CKODate = New DateTime(dtcFromDate.Date.Year, dtcFromDate.Date.Month, dtcFromDate.Date.Day, tmcFromTime.Time.Hours, tmcFromTime.Time.Minutes, 0)
                Me.CKIDate = New DateTime(dtcToDate.Date.Year, dtcToDate.Date.Month, dtcToDate.Date.Day, tmcToTime.Time.Hours, tmcToTime.Time.Minutes, 0)
                Me.FirstReason = cmbRepairType1.SelectedValue
                Me.SecondReason = cmbRepairType2.SelectedValue
                Me.ThirdReason = cmbRepairType3.SelectedValue
                Me.OtherDescription = txtReasonDescription.Text.Trim
                Me.ServicePoint = CInt(IIf(String.IsNullOrEmpty(iservicepoint), 0, iservicepoint)) ' CInt(IIf(String.IsNullOrEmpty(iOdometerDue), 0, iOdometerDue))
                Me.Reference = txtServiceAgentLocationRef.Text.Trim
                Me.Odometer = txtOdometerStart.Text.Trim
                Me.OdometerEnd = txtOdometerEnd.Text.Trim
                ''Me.CompletionDate = New DateTime(dtcCompletionDate.Date.Year, dtcCompletionDate.Date.Month, dtcCompletionDate.Date.Day, tmcCompletionTime.Time.Hours, tmcCompletionTime.Time.Minutes, 0)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.Message & " , " & ex.StackTrace)
            End Try

            txtOdometerEnd.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@OdoEnd").InnerText

            ''rev:mia 28Aug2014 - not needed anymore
            'If Not oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ComletionDT").InnerText.Trim().Equals(String.Empty) Then
            '    dtcCompletionDate.Date = ParseDateTime(oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ComletionDT").InnerText, SystemCulture)
            'Else
            '    dtcCompletionDate.Text = String.Empty
            'End If

            'tmcCompletionTime.Text = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@ComletionTm").InnerText
            'If tmcCompletionTime.Text.IndexOf(":"c) < 0 And Not tmcCompletionTime.Text.Trim().Equals(String.Empty) Then
            '    tmcCompletionTime.Text = tmcCompletionTime.Text.Substring(0, 2) & ":" & tmcCompletionTime.Text.Substring(2, 2)
            'End If



            If CBool(ViewState(ViewState_NEWRECORD)) Or (ViewState(ViewState_NEWRECORD) Is Nothing) Then
                'disabling
                txtOdometerEnd.ReadOnly = True
                'dtcCompletionDate.ReadOnly = True
                'dtcCompletionDate.Text = String.Empty
                'tmcCompletionTime.ReadOnly = True
                'tmcCompletionTime.Text = String.Empty
                btnDelete.Enabled = False
                ViewState(ViewState_NEWRECORD) = False
            Else
                txtOdometerEnd.ReadOnly = False
                'dtcCompletionDate.ReadOnly = False
                'tmcCompletionTime.ReadOnly = False
                btnDelete.Enabled = True
            End If
        End If

        Try
            FleetAssetId = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@FleetAssetId").InnerText
            RepairId = oRepairPageDoc.DocumentElement.SelectSingleNode("//FA/@RepairId").InnerText
        Catch ex As Exception

        End Try

    End Sub

    Sub BindRepairCombos()
        cmbRepairType1.DataSource = Fleet.GetOpsAndLogsReasons()
        cmbRepairType1.DataTextField = "DESCRIPTION"
        cmbRepairType1.DataValueField = "ID"
        cmbRepairType1.DataBind()

        cmbRepairType2.DataSource = Fleet.GetOpsAndLogsReasons()
        cmbRepairType2.DataTextField = "DESCRIPTION"
        cmbRepairType2.DataValueField = "ID"
        cmbRepairType2.DataBind()

        cmbRepairType3.DataSource = Fleet.GetOpsAndLogsReasons()
        cmbRepairType3.DataTextField = "DESCRIPTION"
        cmbRepairType3.DataValueField = "ID"
        cmbRepairType3.DataBind()

        cmbRepairType1.Items.Insert(0, "")
        cmbRepairType2.Items.Insert(0, "")
        cmbRepairType3.Items.Insert(0, "")

    End Sub

    Sub BindScheduledServiceCombo(ByVal AssetId As String)

        'ServicePoint combo is no longer required
        Dim dtScheduledService As DataTable
        dtScheduledService = GetServicePointV2(AssetId)

        cmbServicePoint.DataSource = dtScheduledService
        cmbServicePoint.DataTextField = "DESCRIPTION"
        cmbServicePoint.DataValueField = "ID"

        cmbServicePoint.DataBind()
        cmbServicePoint.Items.Insert(0, "")


    End Sub

    Protected Sub btnCreateUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateUpdate.Click


        Dim iServicePoint As Integer = 0
        '' Changed by Nimesh on 17th Aug 2015 as ServicePointCombo is no longer required
        iServicePoint = txtOdometerDue.Text
        If cmbServicePoint.SelectedValue <> "" Then
            Dim listitem As ListItem = cmbServicePoint.Items.FindByValue(cmbServicePoint.SelectedValue)
            iServicePoint = listitem.Value
        End If

        '' End Changed by Nimesh on 17th Aug 2015 as ServicePointCombo is no longer required
        ''rev:mia 14Aug2014 for test
        Dim toAdd As Boolean = True
        If Not Request.QueryString("NewRecord") Is Nothing Then
            toAdd = CBool(Request.QueryString("NewRecord"))
        End If

        Dim date_raised As DateTime = DateTime.MinValue
        Dim estimated_cost As Decimal = -9999
        Dim actual_cost As Decimal = -9999
        Dim invoice_number As String = Nothing
        Dim repair_code As String = Nothing
        Dim completed As Boolean = False

        Dim fleet_asset_id As Integer = Convert.ToInt32(FleetAssetId)
        Dim LocationCode As String = pkrBranch.Text.Split("-"c)(0).Trim()
        Dim repair_type As String = cmbRepairType1.SelectedValue
        Dim other_type_1 As String = cmbRepairType2.SelectedValue
        Dim other_type_2 As String = cmbRepairType3.SelectedValue

        Dim off_fleet_flag As Boolean = chkAvlSch.Checked
        Dim modified_by As String = Me.UserCode
        Dim reason_description As String = IIf(String.IsNullOrEmpty(txtReasonDescription.Text), "", txtReasonDescription.Text)
        Dim start_date_time As DateTime = New DateTime(dtcFromDate.Date.Year, dtcFromDate.Date.Month, dtcFromDate.Date.Day, tmcFromTime.Time.Hours, tmcFromTime.Time.Minutes, 0)
        Dim end_date_time As DateTime = New DateTime(dtcToDate.Date.Year, dtcToDate.Date.Month, dtcToDate.Date.Day, tmcToTime.Time.Hours, tmcToTime.Time.Minutes, 0)
        Dim start_odometer As Integer = Convert.ToDouble(odometer)
        Dim end_odometer As Integer = Convert.ToDouble(odometerend)
        Dim other_repairer As String = txtServiceAgentLocationRef.Text.Trim ''Service Agt/Loc Re
        Dim service_id As Integer = iServicePoint
        Dim service_provider_id As String = IIf(String.IsNullOrEmpty(pkrAgent.Text), -1, pkrAgent.DataId) 'value here is the repairedId of the FleetRepairer
        Dim po_number As String = lblPurchaseOrderNo.Text
        Dim errorMsg As String = ""

        Dim isactivityExist As Boolean = Fleet.IsActivityExist(lblUnitNo.Text, start_date_time, end_date_time)
        Dim isvalid As Boolean = IsDataValid(start_date_time, end_date_time)
        If (Not isvalid) Then
            Return
        End If

        ''prevent them to add or edit new record
        If (isactivityExist = True) Then
            confirmBoxOnCreateAndUpdate.Text = "You can't " & IIf(toAdd = True, "<b>ADD</b> new ", "<b>UPDATE</b> this ") & "  data. <br/>There is an existing Activity related to this unit number."
            confirmBoxOnCreateAndUpdate.Show()
        Else

            Dim isSuccesfull As Boolean

            If (toAdd) Then
                ''-----CHECKED MAPPING-------------
                '@fleet_asset_id	 =  FleetAssetId
                '@date_raised		 = DateRaised,               NULL
                'NULL				 = FbsPoNumber  ,	         NULL
                '@location_id		 = LocationId,	             pkrBranch.Text.Split("-"c)(0).Trim()
                '@repair_type		 = RepairType,               cmbRepairType1.SelectedValue
                '@other_type_1		 = OtherType1,               cmbRepairType2.SelectedValue
                '@other_type_2		 = OtherType2,	             cmbRepairType3.SelectedValue
                '@repair_code		 = RepairCode,               NULL
                '@off_fleet_flag	 = OffFleetFlag,	         chkAvlSch.Checked   
                'NULL				 = ScheduledService,         NULL   
                'NULL				 = Odometer,                 NULL
                '@estimated_cost	 = EstimatedCost,		     NULL   
                '@actual_cost		 = ActualCost,               NULL
                '@invoice_number	 = InvoiceNr,			     NULL   
                'GetDate()			 = ModifiedDate,             GETDATE()
                '@modified_by		 = ModifiedBy,               USERCODE
                '@reason_description = ReasonDescription,        txtReasonDescription.Text
                '@start_date_time    = StartDateTime,	         dtcFromDate
                '@end_date_time      = EndDatetime,		         dtcEndDate
                '@start_odometer     = StartOdometer,		     odometer
                '@end_odometer       = EndOdometer,              odometerend
                '@completed          = Completed,		         false
                '@other_repairer     = OtherRepairer,  Service Agt/Loc Re		
                '@service_id           = ServiceId,		535
                '@service_provider_id  = ServiceProviderId   -- 566 2009/3 - Hbt External - Misc


                isSuccesfull = Fleet.CreateRepairMaintenanceRecord(fleet_asset_id, date_raised, LocationCode, repair_type, _
                                                                   other_type_1, other_type_2, repair_code, off_fleet_flag, _
                                                                   estimated_cost, actual_cost, invoice_number, modified_by, _
                                                                   reason_description, start_date_time, end_date_time, start_odometer, _
                                                                   end_odometer, completed, other_repairer, service_id, service_provider_id, po_number, errorMsg)
            Else
                isSuccesfull = Fleet.UpdateRepairMaintenanceRecord(fleet_asset_id, date_raised, LocationCode, repair_type, _
                                                                   other_type_1, other_type_2, repair_code, off_fleet_flag, _
                                                                   estimated_cost, actual_cost, invoice_number, modified_by, _
                                                                   reason_description, start_date_time, end_date_time, start_odometer, _
                                                                   end_odometer, completed, other_repairer, service_id, service_provider_id, po_number, errorMsg)
            End If





            If (String.IsNullOrEmpty(RepairId)) Then
                RepairId = -1
            End If

            If (String.IsNullOrEmpty(FleetAssetId)) Then
                FleetAssetId = -1
            End If


            'FleetRepair_InsUpFleetScheduleAndFlag(CInt(RepairId), _
            '                                      FleetAssetId, _
            '                                      txtServiceSchedule.Text, _
            '                                      chkAvlSch.Checked, _
            '                                      UserCode, _
            '                                      txtServiceAgentLocationRef.Text, _
            '                                      iServicePoint, _
            '                                      start_date_time, _
            '                                      end_date_time)

            FleetRepair_InsUpFleetScheduleAndFlag(CInt(RepairId), _
                                                  FleetAssetId, _
                                                  "", _
                                                  chkAvlSch.Checked, _
                                                  UserCode, _
                                                  txtServiceAgentLocationRef.Text, _
                                                  iServicePoint, _
                                                  start_date_time, _
                                                  end_date_time)



            If (isSuccesfull) Then
                PostBackMessage = IIf(toAdd = False, "Updated ", "Added ") & "Successfully"

                If (Not toAdd) Then
                    Response.Redirect(Request.Url.OriginalString)
                Else
                    SetInformationShortMessage(PostBackMessage)
                    PostBackMessage = String.Empty

                    txtOdometerEnd.ReadOnly = False
                    btnDelete.Enabled = True
                    ''Added by Nimesh on 20th July 2015 Ref-DSD-867
                    ' To update PO Number after update has been done
                    lblPurchaseOrderNo.Text = po_number
                    If Not String.IsNullOrEmpty(po_number) Then
                        lblIncidentNo.Text = po_number.ToUpper().Replace("PO-", "")
                    End If

                    ''End Added by Nimesh on 20th July 2015 Ref-DSD-867
                    End If


            Else
                    SetShortMessage(AuroraHeaderMessageType.Error, "Failed to " & IIf(toAdd = False, "update. ", "add. ") & errorMsg.Replace("ERROR:", ""))
            End If

        End If ''If (isactivityExist = True) Then


    End Sub

    ''rev:mia 19Aug2014 - fixing AIMS and DVASS syncHronization by forcing transactions on these
    Protected Sub btnComplete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnComplete.Click
        Dim completiondate As DateTime = New DateTime(dtcToDate.Date.Year, dtcToDate.Date.Month, dtcToDate.Date.Day, tmcToTime.Time.Hours, tmcToTime.Time.Minutes, 0)
        Dim currentDate As DateTime = New DateTime(Now.Year, Now.Month, Now.Day, Now.Hour, Now.Minute, 0)
        Dim valid As Boolean = IIf(Date.Compare(completiondate, currentDate) <> 0, False, True)
        confirmBoxOnComplete.Text = IIf(valid = True, "Do you want to complete it now?", "Would you like to update the <b>Completed Date and Time</b> to <u>today's date</u>?")
        confirmBoxOnComplete.Param = valid
        confirmBoxOnComplete.Show()

    End Sub

    Protected Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        'LoadNewXml()
        LoadNewXml(ViewState(ViewState_ASSETID), ViewState(ViewState_UNITNO))
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        'forget changes and reload
        If String.IsNullOrEmpty(CStr(ViewState(ViewState_REPAIRID))) Then
            ' was a new thing
            LoadNewXml(ViewState(ViewState_ASSETID), ViewState(ViewState_UNITNO))
        Else
            ' was old rec
            LoadPage()
        End If

    End Sub

    Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        ViewState(ViewState_CONFIRMDELETE) = True
        ShowConfirmationDialog("Confirmation Required", GetMessage("GEN124"))
    End Sub

    ''rev:mia 19Aug2014 - fixing AIMS and DVASS syncHronization by forcing transactions on these
    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack

        If leftButton Then
            If CBool(ViewState(ViewState_CONFIRMDELETE)) Then

                Dim fleet_asset_id As Integer = FleetAssetId
                Dim iRepairId As Integer = RepairId
                Dim sUsrCode As String = UserCode
                Dim sAddModPrgmName As String = "OpsAndLogs_FleetRepairManagement"
                Dim errorMsg As String = String.Empty

                Dim isSuccesfull As Boolean = Fleet.DeleteRepairMaintenance(fleet_asset_id, iRepairId, sUsrCode, sAddModPrgmName, errorMsg)

                If (isSuccesfull) Then
                    PostBackMessage = "Deleted successfully."
                    Response.Redirect(Request.Url.OriginalString)
                Else
                    SetErrorShortMessage(errorMsg)
                End If

            End If
        End If

    End Sub

    Public Sub ShowConfirmationDialog(ByVal Message As String, ByVal Title As String)
        confimationBoxControl.Title = Title
        confimationBoxControl.Text = Message
        confimationBoxControl.Show()
    End Sub

    Public Sub LoadNewXml(Optional ByVal AssetId As String = "", Optional ByVal UnitNo As String = "")
        Dim oNewXML As XmlDocument

        If AssetId = "" And UnitNo = "" Then
            Dim oViewStateDoc As XmlDocument = New XmlDocument
            oViewStateDoc.LoadXml(CStr(ViewState(ViewState_REPAIRPAGEDATA)))
            oNewXML = Fleet.GetFleetRepair("", oViewStateDoc.DocumentElement.SelectSingleNode("//FA/@FleetAssetId").InnerText(), "")
        Else
            oNewXML = Fleet.GetFleetRepair("", AssetId, UnitNo)
        End If
        CleanViewState()
        ViewState(ViewState_NEWRECORD) = True
        LoadPage(oNewXML, UnitNo)
    End Sub

    Sub RestrictButtons()
        btnBack.Enabled = True
        btnCancel.Enabled = True
        btnComplete.Enabled = False
        btnCreateUpdate.Enabled = False
        btnDelete.Enabled = False
        ''btnNew.Enabled = True
    End Sub

    Sub CleanViewState()
        ViewState(ViewState_REPAIRID) = Nothing
        ViewState(ViewState_REPAIRPAGEDATA) = Nothing
        ViewState(ViewState_CONFIRMDELETE) = Nothing
        ViewState(ViewState_REPAIRERID) = Nothing
        ViewState(ViewState_NEWRECORD) = Nothing
        ViewState(ViewState_ASSETID) = Nothing
        ViewState(ViewState_BACKPATH) = Nothing
        ViewState(ViewState_UNITNO) = Nothing
    End Sub


#Region "rev:mia 4Aug2014 - this function will trigger after CreateUpdateRepairMaintenanceRecord"

    Property IsDBOffFleet As Boolean
        Get
            Return CBool(ViewState("IsDBOffFleet"))
        End Get
        Set(value As Boolean)
            ViewState("IsDBOffFleet") = value
        End Set
    End Property

    Property PostBackMessage As String
        Get
            Return Session("PostBackMessageFleetRepairMaintenance")
        End Get
        Set(value As String)
            Session("PostBackMessageFleetRepairMaintenance") = value
        End Set
    End Property

    Property RepairId As String
        Get
            Return ViewState(ViewState_REPAIRID)
        End Get
        Set(value As String)
            ViewState(ViewState_REPAIRID) = value
        End Set
    End Property

    Property FleetAssetId As String
        Get
            Return ViewState(ViewState_ASSETID)
        End Get
        Set(value As String)
            ViewState(ViewState_ASSETID) = value
        End Set
    End Property

    Property FleetModel As String
        Get
            Return ViewState("FleetModel")
        End Get
        Set(value As String)
            ViewState("FleetModel") = value
        End Set
    End Property

    Property Branch As String
        Get
            If (String.IsNullOrEmpty(ViewState("Branch"))) Then
                Return pkrBranch.Text.Split("-")(0).Trim
            End If
            Return ViewState("Branch")
        End Get
        Set(value As String)
            ViewState("Branch") = value.Trim
        End Set
    End Property

    Property Odometer As String
        Get
            Return ViewState("Odometer").ToString
        End Get
        Set(value As String)
            ViewState("Odometer") = value
        End Set
    End Property

    Property OdometerEnd As String
        Get
            Return ViewState("OdometerEnd").ToString
        End Get
        Set(value As String)
            ViewState("OdometerEnd") = value
        End Set
    End Property

    Property Reference As String
        Get
            Return ViewState("Reference").ToString
        End Get
        Set(value As String)
            ViewState("Reference") = value
        End Set
    End Property

    Property ServicePoint As Integer
        Get
            Return CInt(ViewState("ServicePoint"))
        End Get
        Set(value As Integer)
            ViewState("ServicePoint") = value
        End Set
    End Property

    Property OtherDescription As String
        Get
            Return ViewState("OtherDescription").ToString
        End Get
        Set(value As String)
            ViewState("OtherDescription") = value
        End Set
    End Property

    Property ThirdReason As String
        Get
            Return ViewState("ThirdReason").ToString
        End Get
        Set(value As String)
            ViewState("ThirdReason") = value
        End Set
    End Property

    Property SecondReason As String
        Get
            Return ViewState("SecondReason").ToString
        End Get
        Set(value As String)
            ViewState("SecondReason") = value
        End Set
    End Property

    Property FirstReason As String
        Get
            Return ViewState("FirstReason").ToString
        End Get
        Set(value As String)
            ViewState("FirstReason") = value
        End Set
    End Property

    Property CKODate As DateTime
        Get
            Return CDate(ViewState("CKODate"))
        End Get
        Set(value As Date)
            ViewState("CKODate") = value
        End Set
    End Property

    Property CKIDate As DateTime
        Get
            Return CDate(ViewState("CKIdate"))
        End Get
        Set(value As Date)
            ViewState("CKIdate") = value
        End Set
    End Property

    Property CompletionDate As DateTime
        Get
            Return CDate(ViewState("Completion"))
        End Get
        Set(value As Date)
            ViewState("Completion") = value
        End Set
    End Property

    Private Function FleetRepair_InsUpFleetScheduleAndFlag(iRepairId As Integer, _
                                                       iFleetAssetId As Integer, _
                                                       sScheduledService As String, _
                                                       bOfffleetFlag As Boolean, _
                                                       sUserCode As String, _
                                                       sOtherRepairer As String, _
                                                       iServiceId As Integer, _
                                                       dfromDate As DateTime, _
                                                       dtodate As DateTime) As String


        Return Fleet.FleetRepair_InsUpFleetScheduleAndFlag(iRepairId, _
                                                           iFleetAssetId, _
                                                           sScheduledService, _
                                                           bOfffleetFlag, _
                                                           sUserCode, _
                                                           sOtherRepairer, _
                                                           iServiceId, _
                                                           dfromDate, _
                                                           dtodate)
    End Function

    Private Function GetServicePointV2(ByVal AssetId As String) As DataTable
        Return Fleet.GetServicePointV2(AssetId)
    End Function

    Private Function DVASSMaintenanceRequest(vehId As String, _
                                                      locId As String, _
                                                      ByVal CkoDate As Date, _
                                                      ByVal CkoDayPart As Long, _
                                                      ByVal CkiDate As Date, _
                                                      ByVal CkiDayPart As Long, _
                                                      forceFlag As Integer, _
                                                      activity As String) As String

        Dim result As String = Fleet.AddModifyDeleteMaintenanceRequest(vehId, _
                                                 locId, _
                                                 CkoDate, _
                                                 CkoDayPart, _
                                                 CkiDate, _
                                                 CkiDayPart, _
                                                 forceFlag, _
                                                 activity)

        Return result
    End Function

    ''DVASS IS A VERY EXPENSIVE CALLS. WE NEED TO EXECUTE ONLY IF THERE ARE CHANGES
    ''IN THE VALUES
    Private Function GetAMPM(dateinput As DateTime) As Integer
        Return IIf(dateinput.Hour < 12, 0, 1)
    End Function

    Private Function Dvass_UPDATECKODATE(newCkoDate As DateTime) As Boolean
        Return IIf(newCkoDate.Equals(CKODate) = True, False, True)
    End Function

    Private Function Dvass_UPDATECKIDATE(newCkiDate As DateTime) As Boolean
        Return IIf(newCkiDate.Equals(CKIDate) = True, False, True)
    End Function

    Private Function Dvass_UPDATEBRANCH(newBranch As String) As Boolean
        Return IIf(newBranch.Equals(Branch) = True, False, True)
    End Function

    ''rev:mia not being use
    Private Function IsDirty(ByVal FromDate As DateTime, _
                                     ByVal ToDate As DateTime, _
                                     ByVal Branch As String, _
                                     ByVal Reason1 As String, _
                                     ByVal Reason2 As String, _
                                     ByVal Reason3 As String, _
                                     ByVal OtherReasonDescription As String, _
                                     ByVal ServicePoint As Integer, _
                                     ByVal Reference As String, _
                                     ByVal odometer As String, _
                                     ByVal offFleet As Boolean, _
                                     ByVal odometerEnd As String, _
                             ByRef dirtyON As String _
                             ) As Boolean


        If (Not CKODate.Equals(FromDate)) Then
            dirtyON = CKODate & " compared to " & FromDate & " -->CKODate"
            Return True
        ElseIf (Not CKIDate.Equals(ToDate)) Then
            dirtyON = CKIDate & " compared to " & ToDate & " -->ToDate"
            Return True
        ElseIf (Not FirstReason.Equals(Reason1)) Then
            dirtyON = FirstReason & " compared to " & Reason1 & " -->Reason1"
            Return True
        ElseIf (Not SecondReason.Equals(Reason2)) Then
            dirtyON = SecondReason & " compared to " & Reason2 & " -->Reason2"
            Return True
        ElseIf (Not ThirdReason.Equals(Reason3)) Then
            dirtyON = ThirdReason & " compared to " & Reason3 & " -->Reason3"
            Return True
        ElseIf (Not Me.OtherDescription.Equals(OtherReasonDescription)) Then
            dirtyON = OtherDescription & " compared to " & OtherReasonDescription & " -->OtherReasonDescription"
            Return True
        ElseIf (Not Me.ServicePoint.Equals(ServicePoint)) Then
            dirtyON = Me.ServicePoint & " compared to " & ServicePoint & " -->ServicePoint"
            Return True
        ElseIf (Not Me.Reference.Equals(Reference)) Then
            dirtyON = Me.Reference & " compared to " & Reference & " -->Reference"
            Return True
        ElseIf (Not Me.Branch.Equals(Branch)) Then
            dirtyON = Me.Branch & " compared to " & Branch & " -->Branch"
            Return True
        ElseIf (Not Me.Odometer.Equals(odometer)) Then
            dirtyON = Me.Odometer & " compared to " & odometer & " -->odometer"
            Return True
        ElseIf Not IsDBOffFleet.Equals(offFleet) Then
            dirtyON = IsDBOffFleet & " compared to " & offFleet & " -->offFleet"
            Return True
            'ElseIf (Not CompletionDate.Equals(CompletionDate)) Then
            '    dirtyON = Me.CompletionDate & " compared to " & CompletionDate & " -->CompletionDate"
            '    Return True
        ElseIf (Not Me.OdometerEnd.Equals(odometerEnd)) Then
            dirtyON = Me.OdometerEnd & " compared to " & odometerEnd & " -->odometerEnd"
            Return True
        End If

        Return False
    End Function

    Protected Sub confirmBoxOnComplete_PostBack(sender As Object, leftButton As Boolean, rightButton As Boolean, param As String) Handles confirmBoxOnComplete.PostBack
        If (leftButton) Then

            ''that means user wants to complete using a past and future date
            ''and not current's date
            If (param = False) Then
                dtcToDate.Date = Now
                tmcToTime.Time = Now.TimeOfDay
            End If


        End If

        Dim oReturnXml As XmlDocument

        Dim ProviderId As String = Trim(pkrAgent.Text.Split("-"c)(0))
        Dim PurchaseOrderNo As String = lblPurchaseOrderNo.Text
        Dim OdometerEnd As Integer = CInt(IIf(txtOdometerEnd.Text.Trim() = "", 0, txtOdometerEnd.Text.Trim()))
        Dim fromDate As DateTime = New DateTime(dtcFromDate.Date.Year, dtcFromDate.Date.Month, dtcFromDate.Date.Day, tmcFromTime.Time.Hours, tmcFromTime.Time.Minutes, 0)
        Dim toDate As DateTime = New DateTime(dtcToDate.Date.Year, dtcToDate.Date.Month, dtcToDate.Date.Day, tmcToTime.Time.Hours, tmcToTime.Time.Minutes, 0)
        Dim DvassMessage As String = ""
        Dim UnitNo As String = lblUnitNo.Text
        Dim Branch As String = Me.Branch
        Dim CkoDayPart As Integer = GetAMPM(fromDate)
        Dim CkiDayPart As Integer = GetAMPM(toDate)
        Dim forceFlag As Integer = 0
        Dim Activity As String = "DONOTHING"
        Dim invokeDvass As Boolean = chkAvlSch.Checked

        '' Added by Nimesh on 18th may 2015

        Dim test As Aurora.Common.Data = New Aurora.Common.Data

        'dbo.GEN_getAdjustedTimeForUser('BBA', '18-may-2015 16:00')
        'Dim modifiedToDate As DateTime = DateTime.Parse(Aurora.Common.Data.ExecuteScalarSQL("select dbo.GEN_getAdjustedTimeForUser('BBA', '" + toDate.ToString("dd-MMM-yyyy HH:mm") + "')"))
        '' Modified by Nimesh on 20th July 2015 REF:- https://thlonline.atlassian.net/browse/DSD-868
        ' to set toDate to current time for the location of the vehicle and not the user code
        'toDate = DateTime.Parse(Aurora.Common.Data.ExecuteScalarSQL("select dbo.GEN_getAdjustedTimeForUser('" + UserCode + "', '" + toDate.ToString("dd-MMM-yyyy HH:mm") + "')"))
        toDate = DateTime.Parse(Aurora.Common.Data.ExecuteScalarSQL("select dbo.GEN_getAdjustedTimeForLocation('" + Me.Branch + "',  '" + toDate.ToString("dd-MMM-yyyy HH:mm") + "')"))
        '' End Added by Nimesh on 18th may 2015
        oReturnXml = Fleet.CompleteRepairMaintenanceRecord(ProviderId, _
                                                           PurchaseOrderNo, _
                                                           OdometerEnd, _
                                                           toDate, _
                                                           DvassMessage, _
                                                           UnitNo, _
                                                           Branch, _
                                                           fromDate, _
                                                           CkoDayPart, _
                                                           CkiDayPart, _
                                                           forceFlag, _
                                                           Activity, _
                                                           invokeDvass)

        If oReturnXml.SelectSingleNode("//Root/Error") Is Nothing Then
            PostBackMessage = oReturnXml.SelectSingleNode("//Root/Message").InnerText
            Response.Redirect(Request.Url.OriginalString)
        Else
            SetShortMessage(AuroraHeaderMessageType.Error, oReturnXml.SelectSingleNode("//Root/Error/Message").InnerText)
        End If

    End Sub

    Protected Sub confirmBoxOnCreateAndUpdate_PostBack(sender As Object, leftButton As Boolean, rightButton As Boolean, param As String) Handles confirmBoxOnCreateAndUpdate.PostBack
        ''do nothing here
    End Sub

    Private Function IsDataValid(start_date_time As DateTime, end_date_time As DateTime) As Boolean

        If (String.IsNullOrEmpty(pkrBranch.Text)) Then
            SetWarningShortMessage("'Branch' is required.")
            Return False
        End If

        If (start_date_time = DateTime.MinValue) Then
            SetWarningShortMessage("'From' date is required.")
            Return False
        End If

        If (end_date_time = DateTime.MinValue) Then
            SetWarningShortMessage("'End' date is required.")
            Return False
        End If

        If (Date.Compare(start_date_time, end_date_time) > 0) Then
            SetWarningShortMessage("Your 'From' date is greater than 'To' date.")
            Return False
        End If

        If (String.IsNullOrEmpty(cmbRepairType1.Text)) Then
            SetWarningShortMessage("'Repair Type 1' is required.")
            Return False
        End If

        If (String.IsNullOrEmpty(pkrAgent.Text)) Then
            SetWarningShortMessage("'Service Agent' is required.")
            Return False
        End If

        Return True
    End Function
#End Region

  
    
End Class
