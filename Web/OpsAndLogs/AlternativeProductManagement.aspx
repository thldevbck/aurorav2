<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	AlternativeProductManagement.aspx
''	Date Created	-	24.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	24.4.8 / Base version
''                      22.5.8 / Common JS moved out of this file
''
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="AlternativeProductManagement.aspx.vb" Inherits="OpsAndLogs_AlternativeProductManagement"
    Title="Untitled Page" %>

<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script src="JS/OpsAndLogsCommon.js" type="text/javascript" language="javascript"></script>

</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel  ID="updMain" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td width="150px" >
                        Country Of Operation:
                    </td>
                    <td width="250px" >
                        <asp:TextBox ID="txtCountryOfOperation" runat="server" ReadOnly="true" CssClass="Textbox_Standard"></asp:TextBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td width="150px" >
                        Requested Product:
                    </td>
                    <td width="250px" >
                        <asp:TextBox ID="txtRequestedProduct" runat="server" ReadOnly="true" CssClass="Textbox_Extralarge"></asp:TextBox>
                    </td>
                    <td >
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />
                        <b>Current Alternative Products</b>
                        <br />
                        <!-- Current Alt Table -->
                        <asp:Repeater ID="rptCurrentAltProds" runat="server" EnableViewState="true">
                            <HeaderTemplate>
                                <table width="100%" class="dataTable" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th width="90%">
                                            <b>Product</b></th>
                                        <th align="left">
                                            <b>Remove</b>
                                            <input type="checkbox" onclick="ToggleTableColumnCheckboxes(1, event);" id="chkSelectAllCurrentAltProds" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="evenRow">
                                    <td>
                                        <%--<span datafld="Product"></span>--%>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Bind("PrdId")%>' />
                                        <%#Container.DataItem("Product")%>
                                    </td>
                                    <td align="right">
                                        <%--<input datafld="Remove" type="checkbox" name="check1" />--%>
                                        <asp:CheckBox ID="chkRemove" runat="server" Checked='<%#Eval("Remove")%>' />&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="oddRow">
                                    <td>
                                        <%--<span datafld="Product"></span>--%>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Bind("PrdId")%>' />
                                        <%#Container.DataItem("Product")%>
                                    </td>
                                    <td align="right">
                                        <%--<input datafld="Remove" type="checkbox" name="check1" />--%>
                                        <asp:CheckBox ID="chkRemove" runat="server" Checked='<%#Eval("Remove")%>' />&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </TABLE>
                            </FooterTemplate>
                        </asp:Repeater>
                        <!-- Current Alt Table -->
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right">
                        <br />
                        <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <asp:Button ID="btnBack" Text="Back" runat="server" CssClass="Button_Standard Button_Back" />
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br />
                        <b>Available Alternative Products </b>
                        <br />
                        <!-- Available Alt Table -->
                        <asp:Repeater ID="rptAvailableAltProds" runat="server" EnableViewState="true">
                            <HeaderTemplate>
                                <table width="100%" class="dataTable" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <th width="90%">
                                            <b>Product</b></th>
                                        <th align="right">
                                            <b>Add&nbsp;&nbsp;</b>
                                            <input type="checkbox" onclick="ToggleSelectAllAgents(1, event);" id="chkSelectAllCurrentAvlProds" />
                                        </th>
                                    </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr class="evenRow">
                                    <td>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Bind("PrdId")%>' />
                                        <%#Container.DataItem("PrdDesc")%>
                                    </td>
                                    <td align="center">
                                        <asp:CheckBox ID="chkAdd" runat="server" Checked='<%#Eval("Add")%>' />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <tr class="oddRow">
                                    <td>
                                        <asp:HiddenField ID="hdnID" runat="server" Value='<%#Bind("PrdId")%>' />
                                        <%#Container.DataItem("PrdDesc")%>
                                    </td>
                                    <td align="center">
                                        <asp:CheckBox ID="chkAdd" runat="server" Checked='<%#Eval("Add")%>' />
                                    </td>
                                </tr>
                            </AlternatingItemTemplate>
                            <FooterTemplate>
                                </TABLE>
                            </FooterTemplate>
                        </asp:Repeater>
                        <!-- Available Alt Table -->
                    </td>
                </tr>
                <tr>
                    <td colspan="3" align="right">
                        <br />
                        <asp:Button ID="btnSave1" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel1" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <asp:Button ID="btnBack1" Text="Back" runat="server" CssClass="Button_Standard Button_Back" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
