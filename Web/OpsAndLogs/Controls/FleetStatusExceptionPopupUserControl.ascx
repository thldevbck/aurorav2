<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FleetStatusExceptionPopupUserControl.ascx.vb"
    Inherits="OpsAndLogs_FleetStatusExceptionPopupUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>

<script type="text/javascript" language="javascript">

    function FSX_Keypress(sender, e)
    {
        var keynum = window.event ? e.keyCode : e.which;
        if (keynum == 0) return true; // cursor keys
        if (keynum == 9) return true; // tab
        if (keynum == 8) return true; // backspace
        
        var keychar = String.fromCharCode(keynum);
        return (/[frxFRX]/.test(keychar));
    }

    function FSX_Change(sender, e)
    {
        var s = sender.value.toUpperCase().trim();
        sender.value = s;
        sender.style.backgroundColor = fleetStatusColors[s];
    }
    
    function returnToFleetStatus(actionFlag)
    {
        if (!actionFlag)
        {
            $find("programmaticFSXPopupBehavior").hide();
            return false;
        }
        else
        {
            return true;
        }    
    }
    
</script>

<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticFSXPopupBehavior"
    TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground" DropShadow="True" PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 350px; height: 450px">
    <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        Maintain Fleet Status Exceptions
    </asp:Panel>
    <br />
    <uc1:MessagePanelControl ID="FSXPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <uc1:MessagePanelControl ID="mpcMessages" runat="server" CssClass="popupMessagePanel" />
            <table width="100%">
                <tr>
                    <td>
                        <%--<asp:Label ID="lblCurrentStatus" runat="server"></asp:Label>--%>
                        <table width="100%">
                            <tr>
                                <td>
                                    Country:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCountry" runat="server" ReadOnly="true" Style="width: 110px"></asp:TextBox>
                                </td>
                                <td nowrap="nowrap">
                                    Booking Week:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBookingWeekStart" runat="server" ReadOnly="true" Style="width: 70px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td nowrap="nowrap"><%-- style="DISPLAY:inline-block;overflow:hidden">--%>
                                    Year / Brand:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtYearBrand" runat="server" ReadOnly="true" Style="width: 110px"></asp:TextBox>
                                </td>
                                <td nowrap="nowrap">
                                    Fleet Status:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFleetStatus" runat="server" ReadOnly="true" Style="width: 70px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Vehicle:
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtVehicle" runat="server" ReadOnly="true" Style="width: 272px"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div style="overflow: auto; height: 260px; width: 100%">
                            <asp:Repeater ID="rptMain" runat="server" EnableViewState="true" OnItemDataBound="rptMain_ItemDataBound">
                                <HeaderTemplate>
                                    <table cellspacing="0" cellpadding="0" class="dataTable" id="tblMain">
                                        <tr>
                                            <th style="display: none;">
                                                LocationId
                                            </th>
                                            <th style="width: 100px;">
                                                Location
                                            </th>
                                            <th>
                                                Fleet Status
                                            </th>
                                        </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr valign="top" class="evenRow">
                                        <td valign="baseline" style="display: none">
                                            <asp:HiddenField ID="hdnFsxId" runat="server" Value='<%# Bind("FsxId") %>' />
                                        </td>
                                        <td valign="baseline">
                                            <uc1:PickerControl ID="pkrLocation" runat="server" PopupType="FsxLOCATION" MessagePanelControlID="mpcMessages"
                                                Width="150px" Text='<%# Bind("FsxLocCode") %>' AppendDescription="true" DataId='<%# Bind("FsxLocCode") %>' />
                                            <asp:HiddenField ID="hdnOriginalFsxLocCode" runat="server" Value='<%# Bind("FsxLocCode") %>' />
                                        </td>
                                        <td valign="baseline">
                                            <asp:TextBox CssClass="Textbox_Tiny" Style="width: 20px" MaxLength="1" runat="server"
                                                ID="txtFleetStatus" Text='<%#Bind("FsxStatus") %>' onchange="FSX_Change(this, event);"
                                                onkeypress="return FSX_Keypress(this, event);"></asp:TextBox>
                                            <asp:HiddenField ID="hdnOriginalFleetStatus" runat="server" Value='<%# Bind("FsxStatus") %>' />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                    <tr valign="top" class="oddRow">
                                        <td valign="baseline" style="display: none">
                                            <asp:HiddenField ID="hdnFsxId" runat="server" Value='<%# Bind("FsxId") %>' />
                                        </td>
                                        <td valign="baseline">
                                            <uc1:PickerControl ID="pkrLocation" runat="server" PopupType="FsxLOCATION" MessagePanelControlID="mpcMessages"
                                                Width="150px" Text='<%# Bind("FsxLocCode") %>' AppendDescription="true" DataId='<%# Bind("FsxLocCode") %>' />
                                            <asp:HiddenField ID="hdnOriginalFsxLocCode" runat="server" Value='<%# Bind("FsxLocCode") %>' />
                                        </td>
                                        <td valign="baseline">
                                            <asp:TextBox CssClass="Textbox_Tiny" Style="width: 20px" MaxLength="1" runat="server"
                                                ID="txtFleetStatus" Text='<%#Bind("FsxStatus") %>' onchange="FSX_Change(this, event);"
                                                onkeypress="return FSX_Keypress(this, event);"></asp:TextBox>
                                            <asp:HiddenField ID="hdnOriginalFleetStatus" runat="server" Value='<%# Bind("FsxStatus") %>' />
                                        </td>
                                    </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                            OnClick="btnSave_Click" />
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="Button_Standard Button_Close" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
</asp:Panel>
