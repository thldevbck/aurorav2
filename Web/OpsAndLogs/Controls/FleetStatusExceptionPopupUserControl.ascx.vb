
Partial Class OpsAndLogs_FleetStatusExceptionPopupUserControl
    Inherits AuroraUserControl


    Public Property CurrentCountryCode() As String
        Get
            Return CStr(ViewState("FSX_CountryCode"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_CountryCode") = value
        End Set
    End Property

    Public Property CurrentCountryName() As String
        Get
            Return CStr(ViewState("FSX_CountryName"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_CountryName") = value
        End Set
    End Property

    Public Property CurrentVechicleCode() As String
        Get
            Return CStr(ViewState("FSX_VechicleCode"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_VechicleCode") = value
        End Set
    End Property

    Public Property CurrentVehicleName() As String
        Get
            Return CStr(ViewState("FSX_VehicleName"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_VehicleName") = value
        End Set
    End Property

    Public Property CurrentYearBrand() As String
        Get
            Return CStr(ViewState("FSX_YearBrand"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_YearBrand") = value
        End Set
    End Property

    Public Property CurrentWeekStartDate() As String
        Get
            Return CStr(ViewState("FSX_CurrentWeekStartDate"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_CurrentWeekStartDate") = value
        End Set
    End Property

    Public Property CurrentFleetStatusId() As Int64
        Get
            If ViewState("FSX_FleetStatusId") Is Nothing Then
                Return -1
            Else
                Return CInt(ViewState("FSX_FleetStatusId"))
            End If

        End Get
        Set(ByVal value As Int64)
            ViewState("FSX_FleetStatusId") = value
        End Set
    End Property

    Public Property WeekNo() As Integer
        Get
            If ViewState("FSX_WeekNo") Is Nothing Then
                Return -1
            Else
                Return CInt(ViewState("FSX_WeekNo"))
            End If

        End Get
        Set(ByVal value As Integer)
            ViewState("FSX_WeekNo") = value
        End Set
    End Property

    Public Property CurrentBrandCode() As String
        Get
            Return CStr(ViewState("FSX_CurrentBrandCode"))
        End Get
        Set(ByVal value As String)
            ViewState("FSX_CurrentBrandCode") = value
        End Set
    End Property

    Public Sub ShowPopup(ByVal FSTId As Int64, ByVal WeekNumber As Integer, ByVal FleetStatus As String, ByVal VehicleCode As String, _
                         ByVal CountryCode As String, ByVal CountryName As String, ByVal YearBrand As String, _
                         ByVal BookingWeekStartDate As String, ByVal VehicleName As String, ByVal BrandCode As String)
        Me.programmaticModalPopup.Show()

        CurrentFleetStatusId = FSTId
        WeekNo = WeekNumber
        CurrentVechicleCode = VehicleCode
        CurrentCountryCode = CountryCode


        CurrentVehicleName = VehicleName
        CurrentYearBrand = YearBrand
        CurrentCountryName = CountryName
        CurrentWeekStartDate = BookingWeekStartDate
        CurrentBrandCode = BrandCode

        txtVehicle.Text = CurrentVechicleCode & " - " & CurrentVehicleName
        txtYearBrand.Text = CurrentYearBrand
        txtCountry.Text = CurrentCountryName
        txtBookingWeekStart.Text = CurrentWeekStartDate

        Select Case FleetStatus
            Case "F"
                txtFleetStatus.Text = "Free Sale"
            Case "R"
                txtFleetStatus.Text = "On Request"
            Case "X"
                txtFleetStatus.Text = "Stop Sale"
        End Select
        LoadData(CurrentFleetStatusId)


    End Sub

    Public Sub HidePopup()
        Me.programmaticModalPopup.Hide()
    End Sub

    Sub LoadData(ByVal FleetStatusId As Int64)
        Dim dtSource As Data.DataTable
        dtSource = Aurora.OpsAndLogs.Data.GetFleetStatusExceptions(FleetStatusId, WeekNo)
        If dtSource.Rows.Count < 10 Then
            For i As Integer = dtSource.Rows.Count + 1 To 10
                dtSource.Rows.Add(dtSource.NewRow())
            Next
        End If
        rptMain.DataSource = dtSource
        rptMain.DataBind()
    End Sub

    Protected Sub rptMain_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            CType(e.Item.FindControl("txtFleetStatus"), TextBox).BackColor = Aurora.Common.DataConstants.FleetStatusToColor(CType(e.Item.FindControl("txtFleetStatus"), TextBox).Text)
            CType(e.Item.FindControl("pkrLocation"), UserControls_PickerControl).Param3 = CurrentVechicleCode
            CType(e.Item.FindControl("pkrLocation"), UserControls_PickerControl).Param4 = CurrentCountryCode
            CType(e.Item.FindControl("pkrLocation"), UserControls_PickerControl).Param2 = CurrentBrandCode
        End If
    End Sub


    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        UpdateData()
        Me.programmaticModalPopup.Show()
    End Sub

    Sub UpdateData()
        'Dim sbSaveList As StringBuilder = New StringBuilder
        Dim sbUpdateInsertList As StringBuilder = New StringBuilder
        Dim sbDeleteList As StringBuilder = New StringBuilder

        Dim sbLocationList As StringBuilder = New StringBuilder

        For Each oItem As RepeaterItem In rptMain.Items
            Dim sOrignalFSXId, sOriginalLocCode, sOriginalFleetStatus, sLocCode, sFleetStatus As String

            sOrignalFSXId = CType(oItem.FindControl("hdnFsxId"), HiddenField).Value.Trim()
            sOriginalLocCode = CType(oItem.FindControl("hdnOriginalFsxLocCode"), HiddenField).Value.Trim()
            sOriginalFleetStatus = CType(oItem.FindControl("hdnOriginalFleetStatus"), HiddenField).Value.Trim()

            sLocCode = CType(oItem.FindControl("pkrLocation"), UserControls_PickerControl).DataId
            sFleetStatus = CType(oItem.FindControl("txtFleetStatus"), TextBox).Text.Trim()

            If Not sLocCode.Trim().Equals(String.Empty) AndAlso sbLocationList.ToString().Contains(sLocCode) Then
                ' this location is already present!
                mpcMessages.SetMessagePanelWarning("Multiple entries for " & sLocCode)
                Exit Sub
            End If
            sbLocationList.Append(sLocCode & ",")

            ' check for missing data
            If (sLocCode.Equals(String.Empty) And Not sFleetStatus.Equals(String.Empty)) Then
                mpcMessages.SetMessagePanelError("Please enter Location Code if Fleet Status has been entered")
                Exit Sub
            End If

            If (Not sLocCode.Equals(String.Empty) And sFleetStatus.Equals(String.Empty)) Then
                mpcMessages.SetMessagePanelWarning("Please enter Fleet Status for " & sLocCode)
                Exit Sub
            End If
            ' check for missing data - completed!

            If sLocCode.Equals(String.Empty) And sFleetStatus.Equals(String.Empty) Then
                If sOrignalFSXId.Equals(String.Empty) Then ' If new record
                    ' nothing has been entered here!
                    Continue For
                Else
                    ' this is to be deleted!
                    sbDeleteList.Append(sOrignalFSXId & ",")
                    Continue For
                End If
            End If

            ' save or update decisions
            ' if u got till here both sLocCode and sFleetStatus are <> ""
            If sLocCode.Equals(sOriginalLocCode) And sFleetStatus.Equals(sOriginalFleetStatus) Then
                ' nothing changed! ignore this row!
                Continue For
            Else
                ' something has changed!
                If sOrignalFSXId.Equals(String.Empty) Then
                    ' new record - create
                    ' imp diff ! DO NOT Add the FSXID node!!! else the SQL proc will go kablooey!
                    sbUpdateInsertList.Append("<FleetStatusException")
                    sbUpdateInsertList.Append(" FSTId=""" & CurrentFleetStatusId.ToString() & """")
                    sbUpdateInsertList.Append(" WeekNo=""" & WeekNo & """")
                    sbUpdateInsertList.Append(" LocCode=""" & sLocCode & """")
                    sbUpdateInsertList.Append(" FleetStatus=""" & sFleetStatus & """ />")
                Else
                    ' old rec - update
                    sbUpdateInsertList.Append("<FleetStatusException")
                    sbUpdateInsertList.Append(" FSXId=""" & sOrignalFSXId & """")
                    sbUpdateInsertList.Append(" FSTId=""" & CurrentFleetStatusId.ToString() & """")
                    sbUpdateInsertList.Append(" LocCode=""" & sLocCode & """")
                    sbUpdateInsertList.Append(" FleetStatus=""" & sFleetStatus & """ />")
                End If
            End If
            ' save or update decisions
        Next

        If sbUpdateInsertList.Length = 0 And sbDeleteList.Length = 0 Then
            mpcMessages.SetMessagePanelWarning("No changes to save")
            Exit Sub
        End If

        Dim sRetMsg As String

        Using oTransaction As New Aurora.Common.Data.DatabaseTransaction
            Try
                If sbUpdateInsertList.Length > 0 Then
                    ' somthing to do!
                    sbUpdateInsertList.Insert(0, "<FleetStatusExceptions>")
                    sbUpdateInsertList.Append("</FleetStatusExceptions>")

                    ' Hit the DB!
                    sRetMsg = Aurora.OpsAndLogs.Data.UpdateFleetStatusExceptions(sbUpdateInsertList.ToString(), CurrentPage.UserId, CurrentPage.PrgmName)
                    If sRetMsg.ToUpper().IndexOf("SUCCESS") < 0 Then
                        Throw New Exception(sRetMsg)
                    End If
                End If


                If sbDeleteList.Length > 0 Then
                    sbDeleteList.Remove(sbDeleteList.Length - 1, 1)
                    sRetMsg = String.Empty
                    sRetMsg = Aurora.OpsAndLogs.Data.DeleteFleetStatusExceptions(sbDeleteList.ToString())
                    If sRetMsg.ToUpper().IndexOf("SUCCESS") < 0 Then
                        Throw New Exception(sRetMsg)
                    End If
                End If

                oTransaction.CommitTransaction()
                oTransaction.Dispose()

                ' refresh!
                LoadData(CurrentFleetStatusId)

                ' show message!
                mpcMessages.SetMessagePanelInformation(sRetMsg)
                ' btnClose.OnClientClick = "returnToFleetStatus(true);"
                ' btnClose.Attributes("onclick") = "return returnToFleetStatus(true);"

            Catch ex As Exception
                oTransaction.RollbackTransaction()
                oTransaction.Dispose()
                mpcMessages.SetMessagePanelError(ex.Message)
                Exit Sub
            End Try

        End Using



    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        HidePopup()
        RaiseEvent PostBack(Me, True, False)
    End Sub

    Public Delegate Sub FleetStatusExceptionPopupUserControlEventHandler(ByVal sender As Object, ByVal childRecordHasChanged As Boolean, ByVal param As Object)
    Public Event PostBack As FleetStatusExceptionPopupUserControlEventHandler
End Class
