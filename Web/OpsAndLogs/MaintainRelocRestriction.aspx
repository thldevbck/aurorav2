﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="MaintainRelocRestriction.aspx.vb" Inherits="OpsAndLogs_MaintainRelocRestriction" %>

<%@ Register Src="~/UserControls/MultiSelectControl/MultiSelect.ascx" TagName="MultiSelect"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="ucPickerControl" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="ucDateControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style>
        .MULTISELECTCLASS span img
        {
            float: right;
            margin: 0px 10px 0px 0px;
        }
        .MULTISELECTCLASS
        {
            width: 300px;
        }
        table.mrrDataTable
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script type="text/javascript">

        function CurrencyFormatted(sender) {
            amount = sender.value
            var i = parseFloat(amount);
            if (isNaN(i)) { i = 0.00; }
            var minus = '';
            if (i < 0) { minus = '-'; }
            i = Math.abs(i);
            i = parseInt((i + .005) * 100);
            i = i / 100;
            s = new String(i);
            if (s.indexOf('.') < 0) { s += '.00'; }
            if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
            s = minus + s;
            sender.value = s
        }

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
  
            <table>
                <tr>
                    <td>
                        Country:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlCountry" Width="150px" AutoPostBack=true>
                        </asp:DropDownList>
                    </td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveTop" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnResetTop" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                    </td>
                </tr>
            </table>
            <i>'NOTE: All fields are required.'</i>
            <asp:Repeater ID="LocationRestrictRepeater" runat="server" EnableViewState="true">
                <HeaderTemplate>
                    <table class="mrrDataTable">
                        <tr>
                            <th style="width: 350">
                                Fleet Model
                            </th>
                            <th style="width: 150">
                                Location Code
                            </th>
                            <th style="width: 150">
                                Penalty Cost
                            </th>
                            <th style="width: 150">
                                Start Date
                            </th>
                            <th style="width: 150">
                                End Date
                            </th>
                            <th style="width: 50">
                                Remove
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="evenRow">
                        <td class="MULTISELECTCLASS">
                            <asp:HiddenField ID="hiddenLrId" runat="server" Value='<%# Bind("LrId") %>' />
                            <uc1:MultiSelect ID="multiselect" SelectType="FleetModels" DisplayKeys="false"
                                runat="server" />
                        </td>
                        <td>
                            <ucPickerControl:PickerControl ID="pkrLocationTo" runat="server" PopupType="LOCATIONFORCOUNTRY"
                                Width="100px" Text='<%# Bind("LrLocCode") %>' DataId='<%# Bind("LrLocCode") %>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="tbox" Width="70px" Text='<%# Bind("LrCost") %>' />
                        </td>
                        <td>
                            <ucDateControl:DateControl ID="dtDateFrom" runat="server" Text='<%# Bind("LrStartDate") %>' />
                        </td>
                        <td>
                            <ucDateControl:DateControl ID="dtDateTo" runat="server" Text='<%# Bind("LrEndDate") %>' />
                        </td>
                        <td>
                            <asp:checkbox runat="server" ID="deleteFlrId" />
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="oddRow">
                        <td class="MULTISELECTCLASS">
                            <asp:HiddenField ID="hiddenLrId" runat="server" Value='<%# Bind("LrId") %>' />
                            <uc1:MultiSelect ID="multiselect" SelectType="FleetModels" DisplayKeys="false" 
                                runat="server" />
                        </td>
                        <td>
                            <ucPickerControl:PickerControl ID="pkrLocationTo" runat="server" PopupType="LOCATIONFORCOUNTRY"
                                Width="100px" Text='<%# Bind("LrLocCode") %>' DataId='<%# Bind("LrLocCode") %>' />
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="tbox" Width="70px" Text='<%# Bind("LrCost") %>' />
                        </td>
                        <td>
                            <ucDateControl:DateControl ID="dtDateFrom" runat="server" Text='<%# Bind("LrStartDate") %>' />
                        </td>
                        <td>
                            <ucDateControl:DateControl ID="dtDateTo" runat="server" Text='<%# Bind("LrEndDate") %>' />
                        </td>
                        <td>
                            <asp:checkbox runat="server" ID="deleteFlrId" />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveBottom" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnResetBottom" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                    </td>
                </tr>
            </table>
</asp:Content>
