<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	VehicleAllocation.aspx
''	Date Created	-	2.5.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	2.5.8  / Shoel - Base version
''                      22.5.8 / Shoel - Common JS moved out of this file
''                      25.8.8 / Shoel - Field validation added
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page 
    Language="VB" 
    MasterPageFile="~/Include/AuroraHeader.master" 
    AutoEventWireup="false"
    CodeFile="VehicleAllocation.aspx.vb" 
    Inherits="OpsAndLogs_VehicleAllocation"
    Title="Untitled Page" %>

<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>

<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script src="JS/OpsAndLogsCommon.js" type="text/javascript"></script>

    <script type="text/javascript">
function CheckIfAnythingSelected(tableID,columnNumber)
{
    var table = document.getElementById(tableID);
    var AtleastOneSelected = false;
    for(var i=0;i<table.children(0).children.length;i++)
    {
        if(table.children(0).children(i).innerHTML.toUpperCase().indexOf("<TH>")==-1) // not header
        {
            if(table.children(0).children(i).children(columnNumber).children(0) != null)              
            {
                if(table.children(0).children(i).children(columnNumber).children(0).checked)
                {
                    AtleastOneSelected = true;
                    break;
                }    
            }
        }
    }
    if(AtleastOneSelected)
        return true;
    else
    {
        setWarningShortMessage("Please select the Relocation Record(s) to Delete");
        return false;
    }
}
    </script>

</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr width="100%">
                    <td width="150px">Country:</td>
                    <td width="250px">
                        <asp:DropDownList ID="cmbCountry" runat="server" CssClass="Dropdown_Standard" AutoPostBack="true" />
                    </td>
                    <td width="150px">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4" style="border-top-width: 1px; border-top-style: solid">
                        <br />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right">
                        <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <asp:Button ID="btnRemove" Text="Remove" runat="server" CssClass="Button_Standard Button_Remove" />
                    </td>
                </tr>
            </table>

            <br />

            <asp:Repeater ID="rptMain" runat="server" EnableViewState="true">
                <HeaderTemplate>
                    <table id="tblRptMain" class="dataTable" width="100%">
                        <tr style="vertical-align: baseline">
                            <th>Vehicle</th>
                            <th>From</th>
                            <th>To</th>
                            <th>Allocation</th>
                            <th>Added by</th>
                            <th>Modified by</th>
                            <th>
                                <div align="center"><input type="checkbox" name="checkAllProd" onclick="ToggleTableColumnCheckboxes(6, event);" /></div>
                            </th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="evenRow">
                        <td nowrap="nowrap">
                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("ID") %>' />
                            <uc1:PickerControl 
                                ID="pkrVehicle" 
                                runat="server" 
                                PopupType="OPLOGGETFLEETMODEL"
                                Text='<%# Bind("Vehicle") %>' 
                                DataId='<%# Bind("Vehicle") %>' />
                        </td>
                        <td>
                            <uc1:DateControl 
                                EnableViewState="true" 
                                ID="dtcFrom" 
                                runat="server" 
                                Text='<%# Bind("LocFrom") %>' />
                        </td>
                        <td>
                            <uc1:DateControl 
                                EnableViewState="true" 
                                ID="dtcTo" 
                                runat="server" Text='<%# Bind("LocTo") %>' />
                        </td>
                        <td>
                            <asp:TextBox 
                                ID="txtAllocation" 
                                runat="server" 
                                CssClass="Textbox_Tiny_Right_Aligned"
                                MaxLength="5" 
                                onkeypress="return keyStrokeInt(event);" OnBlur="CheckContents(this,'0');"
                                Text='<%# Bind("Allocation") %>' />
                        </td>
                        <td>
                            <asp:TextBox 
                                ReadOnly="true" 
                                CssClass="Textbox_Standard" 
                                runat="server" 
                                ID="txtAddedBy"
                                Text='<%# Bind("AddedBy") %>' />
                        </td>
                        <td>
                            <asp:TextBox 
                                ReadOnly="true" 
                                CssClass="Textbox_Standard" 
                                runat="server" 
                                ID="txtModifiedBy"
                                Text='<%# Bind("ModifiedBy") %>' />
                        </td>
                        <td align="center">
                            <asp:CheckBox 
                                ID="chkRemove" 
                                runat="server" 
                                Checked='<%# Eval("Remove") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <AlternatingItemTemplate>
                    <tr class="oddRow">
                        <td nowrap="nowrap">
                            <asp:HiddenField ID="hdnID" runat="server" Value='<%# Bind("ID") %>' />
                            <uc1:PickerControl ID="pkrVehicle" runat="server" PopupType="OPLOGGETFLEETMODEL"
                                Text='<%# Bind("Vehicle") %>' DataId='<%# Bind("Vehicle") %>' />
                        </td>
                        <td>
                            <uc1:DateControl EnableViewState="true" ID="dtcFrom" runat="server" Text='<%# Bind("LocFrom") %>' />
                        </td>
                        <td>
                            <uc1:DateControl EnableViewState="true" ID="dtcTo" runat="server" Text='<%# Bind("LocTo") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="txtAllocation" runat="server" CssClass="Textbox_Tiny_Right_Aligned" OnBlur="CheckContents(this,'0');"
                                MaxLength="5" onkeypress="return keyStrokeInt(event);" Text='<%# Bind("Allocation") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ReadOnly="true" CssClass="Textbox_Standard" runat="server" ID="txtAddedBy"
                                Text='<%# Bind("AddedBy") %>'></asp:TextBox>
                        </td>
                        <td>
                            <asp:TextBox ReadOnly="true" CssClass="Textbox_Standard" runat="server" ID="txtModifiedBy"
                                Text='<%# Bind("ModifiedBy") %>'></asp:TextBox>
                        </td>
                        <td align="center">
                            <asp:CheckBox ID="chkRemove" runat="server" Checked='<%# Eval("Remove") %>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>

            <br />

            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave1" Text="Save" runat="server" CssClass="Button_Standard Button_Save" />
                        <asp:Button ID="btnCancel1" Text="Reset" runat="server" CssClass="Button_Standard Button_Reset" />
                        <asp:Button ID="btnRemove1" Text="Remove" runat="server" CssClass="Button_Standard Button_Remove" />
                    </td>
                </tr>
            </table>

            <uc1:ConfirmationBoxControl 
                ID="cnfConfirmation" 
                runat="server" Text="Are you sure you wish to continue with removing the selected items?"
                Title="Confirmation required" />
                
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
