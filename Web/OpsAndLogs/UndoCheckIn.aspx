<%@ Page Language="VB" 
    MasterPageFile="~/Include/AuroraHeader.master" 
    AutoEventWireup="false" 
    CodeFile="UndoCheckIn.aspx.vb" 
    Inherits="OpsAndLogs_UndoCheckIn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ScriptPlaceHolder" Runat="Server">

<script language="javascript" type="text/javascript">
function IsValidBookingRef (text) 
{
    var Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var Digits = "0123456789";

    text = '' + text;
    
    if (text.length == 0) return false;
    
    var firstLetter = text.substring(0,1);
    var isLetter = Alphabet.indexOf(firstLetter) != -1;
    

    var index = isLetter ? 9 : 7;
    var length = isLetter ? 10 : 9;
    
    if (text.length != length) return false;
    if (text.indexOf("/") == -1) return false;
    
    var strSlash = text.split("/");
    if (strSlash.length > 2) return false;

    var indexChar = text.indexOf("/");
    var indexAfter = text.substring(indexChar +1);

    return (indexChar == index) && (indexAfter != '') && (Digits.indexOf(indexAfter) != -1);
}

function IsValidBookingRefV2 (text) 
{
   var Digits = "0123456789";

   if (text.length == 0) return false;
   if (text.indexOf("/") == -1) return false;
   
     
    var indexChar = text.indexOf("/");
    var indexAfter = text.substring(indexChar +1);
    
    return (indexChar > 0) && (indexAfter != '') && (Digits.indexOf(indexAfter) != -1);
}

function ClientSideValidation(sender, args)
{
    args.IsValid = IsValidBookingRef (args.Value);
}               

function txtBookingRef_onchange(sender)
{
    var value = ('' + sender.value).trim();
    if (value == '')
        MessagePanelControl.setMessagePanelWarning (null, 'Please enter Booking Reference Number!')
    else if (!IsValidBookingRefV2 (sender.value))
       MessagePanelControl.setMessagePanelWarning (null, "Booking Ref is not in correct format. i.e 'Booking num/rental Num' format.");
    else
        MessagePanelControl.clearMessagePanel();
}
 
</script>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder" Runat="Server">
    <asp:UpdatePanel id="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table width="100%">
                <tr width="100%">
                    <td width=150 >Booking Ref:</td>
                    <td >
                        <asp:TextBox runat="server" ID="txtBookingRef" Text="" MaxLength="12"
                            onchange="txtBookingRef_onchange(this);" />&nbsp;*&nbsp;&nbsp;e.g.&nbsp;3000140/1
                            
                        <asp:RequiredFieldValidator 
                            ID="RequiredBookingRef" runat="server" 
                            ErrorMessage="Please enter Booking Reference Number!" 
                            ControlToValidate="txtBookingRef" 
                            Display="Dynamic" 
                            SetFocusOnError="true"
                            Text="*" Enabled="False"/>
                        <asp:CustomValidator 
                            ID="CustomValidatorBookinRef" runat="server" 
                            ErrorMessage="Booking Ref is not in correct format. i.e '3000140/1' format." 
                            ClientValidationFunction="ClientSideValidation" 
                            ControlToValidate="txtBookingRef" 
                            Display="Dynamic" 
                            SetFocusOnError="true" 
                            Text="*" Enabled="False" />
                    </td>
                </tr>
                <tr>
                    <td colspan=2 align=right >
                        <asp:Button ID="btnUndoCheckIn" runat="server" Text="Undo Check-In" 
                            class="Button_Standard" />
                    </td>
                </tr>
                
            </table>

<%--    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender2" runat="server" TargetControlID="CustomValidatorBookinRef" />
    <ajaxToolkit:ValidatorCalloutExtender ID="ValidatorCalloutExtender1" runat="server" TargetControlID="RequiredBookingRef" />
--%>
    
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

