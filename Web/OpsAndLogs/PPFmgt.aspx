﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="PPFmgt.aspx.vb" Inherits="OpsAndLogs_PPFmgt" %>

<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

<script language="javascript" type="text/javascript">
    
    function IsValidSized(sender) {
        if (isNaN(sender.value) && (sender.value.length != 0)) {
            sender.value = ""
        }
    }

    function CurrencyFormatted(sender) {
        amount = sender.value
        var i = parseFloat(amount);
        if (isNaN(i)) { i = 0.00; }
        var minus = '';
        if (i < 0) { minus = '-'; }
        i = Math.abs(i);
        i = parseInt((i + .005) * 100);
        i = i / 100;
        s = new String(i);
        if (s.indexOf('.') < 0) { s += '.00'; }
        if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
        s = minus + s;
        //return s;
        sender.value = s
    }
     
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
<asp:UpdatePanel ID="ppfpanel" runat="server">
<ContentTemplate>
    <table style="width: 100%" cellpadding="2" cellspacing="0">
        <tr>
            <td style="width: 150px">
                Country:
            </td>
            <td style="width: 250px">
                <asp:DropDownList ID="countryDropdown" runat="server" Width="130">
                </asp:DropDownList>
            </td>
            
            <td style="width: 250px">
                Location:
            </td>
            <td style="width: 250px">
                <asp:DropDownList ID="FuelDropDown" runat="server" Width="130">
                </asp:DropDownList>
            </td>
            
        </tr>
        <tr>

            <td style="width: 250px">
                Effectivity:
            </td>
            <td style="width: 250px">
                <asp:DropDownList ID="dateDropDown" runat="server" Width="130">
                </asp:DropDownList>
            </td>
            <td style="width: 250px">
                <asp:Button ID="newButton" runat="server" Text="New" CssClass="Button_Standard Button_New" />
            </td>
            
            
        </tr>

         <tr>
            <td style="border-top-width: 1px; border-top-style: dotted; width:150px;">
                Prepaid Petrol Price:  
            </td>
            <td style="border-top-width: 1px; border-top-style: dotted; padding: -1;">
                <asp:TextBox ID="Pre_PetrolTextbox" runat="server" Width="125px" MaxLength="5"></asp:TextBox>
            </td>
            <td style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                Prepaid Diesel Price:  
            </td>
            <td  style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                <asp:TextBox ID="Pre_DieselTextBox" runat="server" Width="125px" MaxLength="5"></asp:TextBox>
            </td>
        </tr>

        <tr>
            <td style="border-top-width: 1px; border-top-style: dotted; width:150px;">
                Pump Petrol Price:  
            </td>
            <td style="border-top-width: 1px; border-top-style: dotted; padding: -1;">
                <asp:TextBox ID="PetrolTextbox" runat="server" Width="125px" MaxLength="5"></asp:TextBox>
            </td>
            <td style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                Pump Diesel Price:  
            </td>
            <td  style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                <asp:TextBox ID="DieselTextBox" runat="server" Width="125px" MaxLength="5"></asp:TextBox>
            </td>          
        </tr>

         <tr>
            <td style="border-top-width: 1px; border-top-style: dotted; width:150px;">
                Refuel Petrol Price:  
            </td>
            <td style="border-top-width: 1px; border-top-style: dotted; padding: -1;">
                <asp:TextBox ID="Re_PetrolTextbox" runat="server" Width="125px" MaxLength="5"></asp:TextBox>
            </td>
            <td style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                Refuel Diesel Price:  
            </td>
            <td  style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                <asp:TextBox ID="Re_DieselTextBox" runat="server" Width="125px" MaxLength="5"></asp:TextBox>
            </td>
             <td style="border-top-width: 1px; border-top-style: dotted; padding: 0;">
                <asp:Button ID="SaveButton" runat="server" Text="Process" CssClass="Button_Standard Button_Save"  Width="120px"/>
            </td>
        </tr>

        <tr>
            <td colspan="6" style="border-top-width: 1px; border-top-style: dotted" />
        </tr>
    </table>
    <asp:Table ID="PPFTable" runat="server" CellPadding="2" CellSpacing="0" Width="100%"
        Visible="true" CssClass="dataTableColor saleableProductTable">
        <asp:TableHeaderRow>
           <%-- <asp:TableHeaderCell Text="Brand" Width="40px"  HorizontalAlign="Center"/>--%>
           <asp:TableHeaderCell Text="Vehicle Code" Width="20%" />
            <asp:TableHeaderCell Text="Vehicle Type" Width="40%" />
            <asp:TableHeaderCell Text="Fuel" Width="20%" />
            <asp:TableHeaderCell Text="Tank" Width="10%" HorizontalAlign="Center"/>
            <asp:TableHeaderCell Text="Price" Width="10%" />
        </asp:TableHeaderRow>
    </asp:Table>
    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
