<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PaymentMGTConfirmation.aspx.vb" Inherits="Booking_PaymentMGTConfirmation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Card Reversal</title>
    <base  target="_self" />
    <script language="javascript" type="text/javascript">
        function onclose(value){
            var returnvalue = new Object();
                returnvalue.data = value;
                window.returnValue=returnvalue
                window.close('PaymentMGTConfirmation.aspx','_self');
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
        <table>
            <tr>
                <td height="100">
                </td>
            </tr>
            <tr>
                <td colspan ="3">
                    Card Present for reversal?
                </td>
            </tr>
            <tr>
                <td height="20" colspan ="3">
                </td>
            </tr>
            <tr>
            <td>
                    <asp:Button ID="btnYes" runat="server" Text="Yes" Width="50" OnClientClick="onclose('YES')"  CssClass="Button_Standard" />
                </td>
                <td>
                    <asp:Button ID="btnNo" runat="server" Text="No" Width="50" OnClientClick="onclose('NO')" CssClass="Button_Standard" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="50" OnClientClick="onclose('CANCEL')" CssClass="Button_Standard" />
                </td>
                
            </tr>
        </table>
    </div>
    <asp:Literal ID="litjs" runat="server" EnableViewState="true" />
    </form>
</body>
</html>
