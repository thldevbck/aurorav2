'' RKS : 26-Jun-2012
'' Should be always enable for Imprints for AU and NZ
'' CSREFTPMT role is true or False we don't care..
''NEW TABLE AND SP FOR EFTPOS FUNCTIONALITY

''STORED PROCEDURES
''InsertPaymentOverrideReasons
''GetPaymentOverrideList
''InsertPaymentEFTPOSreceipts
''GetCardTypeEftPos
''GetPrintersAssignedToBranchAndUser
''SetPrintersAssignedToBranchAndUser
''GetUserNameAndPrinter
''EFTPOS_GetBookingCreditCards
''EFTPOS_GetCreditCardUsedInBond
''EFTPOS_GetTotalAmountGroupByCardAndType
''dialog_checkUserForCSR

''BillingTokenInsert
''BillingTokenSelectPopulateCardInfo
''BillingTokenSelectForPayment
''BillingTokenSelect

''TABLES
''PaymentEFTPOSreceipts     
''PaymentOverrideReasons    
''PaymentOverrideReasonList 
''PaymentBillingToken
''PaymentDetail
''LocationTable

''MODIFIED:
''SEPT.24 2010 
''  GetPaymentOverrideList
''  EFTPOS_GetTotalAmountGroupByCardAndType

''REV:MIA DEC 2 2010
''ADDED:
''EFTPOS_updatePaymentToStatusToOpen
''paym_managePaymentDetail
''paym_GetPayment

''rev:mia August 6 2012 - addition of messages for these conditions
''                      - Refund/Payment and not imprint for billing token- Add a new message
''                      - DPA/DPZ  and doing Reverse Payment and not a member of CHGTOKEN - Add a new message


''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:NZ USER that is transacting payment and booking in AU


Imports Aurora.Common
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
''Imports WSpayment
Imports Aurora.Payment.DPS
Imports Aurora.Booking.Services
Imports Aurora.Common.Logging

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.PaymentMGT)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingSearch)> _
Partial Class Booking_PaymentMGT
    Inherits AuroraPage


#Region " Enum"
    Private Enum HidePanel
        _CreditCard = 1
        _DebitCard = 2
        _TravellersCheque = 3
        _PersonalCheque = 4
        _Cash = 5
        _RefundCheque = 6
        _All = 0
        _Imprint = 7
        _HoldImprint = 8
    End Enum
#End Region

#Region " Constant"
    Private Const QS_Payment As String = "PaymentMgt.aspx?funCode=PAYMGT"
    Private Const QS_BOOKING As String = "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10"
    Private Const QS_BOOKING_NOQS As String = "Booking.aspx"
    Private Const QS_RS_VEHREQMGT As String = "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=10"
    Private Const CHANGE_CURRENCY As String = "Currency displayed is country of booking as credit card is not present"
    Private Const CHANGE_CURRENCY_BACK As String = "Currency is defaulted to User Location"
    Private Const EFTPOS_MIX_MSG As String = "Credit / Debit payments can only be created individually when using 'EFTPOS TERMINAL'"
    Private Const BILLING_EMPTY_MSG As String = "There is no Credit Card Authority stored in this booking.<br/> Select &nbsp;'OK'&nbsp; and create a Credit card authority from the Payment tab"
    Private Const BILLING_TYPE_OF_MSG As String = "Credit Card Authority can be use only for Credit Card Payment and Imprint."
    Private Const BILLING_TYPE_OF_MSG_NA As String = "Credit Card Authority is not available for "


    Const NODE_ELEMENT As Integer = 1
    Const NODE_ATTRIBUTE As Integer = 2

    Const NEW_PAYMENT As Integer = 0
    Const EXISTING_PAYMENT As Integer = 1

    Const HOLD_NUMBER As String = "Hold Number"

    'rev:mia August 6 2012 - addition of messages for these conditions
    Const BILLING_TOKEN_MSG As String = "Only to be used for Imprints or if the customer is not present"
    Const CHGTOKEN_REFUND_DPZ_OR_DPA_MSG As String = "This payment will NOT be processed online.  A manual refund on the comm machine is required."

#End Region

#Region " Variables"
    Private Shared QS_BOOKINGurl As String
    Protected CheckedRemove As Boolean
    Protected xmlDOMObjRequest As New XmlDocument
    Protected xmlDOMObj As New XmlDocument

    Protected xmlPaymentmgt As New XmlDocument
    Protected xmlPaymentrequest As New XmlDocument
    Protected xmlpaymententrymgt As New XmlDocument
    Protected xmlPaymentDetails As New XmlDocument
    Protected xmlPaymentDetailsOrig As New XmlDocument

    Protected xmlPaymentMethods As New XmlDocument
    Protected xmlDateUser As New XmlDocument
    Protected xmldomObjSetting As New XmlDocument
    Protected xmlActivePaymentData As New XmlDocument
    Protected xmlPayment As New XmlDocument
    Protected xmlAmountOutStanding As New XmlDocument
    Protected xmlTravellerChequeDetails As New XmlDocument
    Protected xmlTempNewXML As New XmlDocument



    ''rev:mia nov10
    Private Property BaseLocalcurrency() As String
        Get
            Return ViewState("BaseLocalcurrency")
        End Get
        Set(ByVal value As String)
            ViewState("BaseLocalcurrency") = value
        End Set
    End Property

    ''Protected Shared paymentType As Integer  ''track the initial paymenttype
    Private Property paymentType() As Integer
        Get
            Return ViewState("paymentType")
        End Get
        Set(ByVal value As Integer)
            ViewState("paymentType") = value
        End Set
    End Property

    Protected bValidCCardNumber As Boolean ''track valid credit card
    ''Protected Shared nCurrentPmMethodSelIndex As Integer ''track the index of the ddlmethod
    Private Property nCurrentPmMethodSelIndex() As Integer
        Get
            Return ViewState("nCurrentPmMethodSelIndex")
        End Get
        Set(ByVal value As Integer)
            ViewState("nCurrentPmMethodSelIndex") = value
        End Set
    End Property


    ''Private LocCurrCode As String
    Private Property LocCurrCode() As String
        Get
            Return ViewState("LocCurrCode")
        End Get
        Set(ByVal value As String)
            ViewState("LocCurrCode") = value
        End Set
    End Property

    ''Private Shared RntId As String
    Private Property RntId() As String
        Get
            Return ViewState("RntId")
        End Get
        Set(ByVal value As String)
            ViewState("RntId") = value
        End Set
    End Property

    ''Private Shared PaymentID As String
    Private Property PaymentID() As String
        Get
            Return ViewState("PaymentID")
        End Get
        Set(ByVal value As String)
            ViewState("PaymentID") = value
        End Set
    End Property


    ''Private Shared BookId As String
    Private Property BookId() As String
        Get
            Return ViewState("BookId")
        End Get
        Set(ByVal value As String)
            ViewState("BookId") = value
        End Set
    End Property


    ''Private Shared BookNo As String
    Private Property BookNo() As String
        Get
            Return ViewState("BookNo")
        End Get
        Set(ByVal value As String)
            ViewState("BookNo") = value
        End Set
    End Property

    ''Private Shared iAmtToPay As String
    Private Property iAmtToPay() As String
        Get
            Return ViewState("iAmtToPay")
        End Get
        Set(ByVal value As String)
            ViewState("iAmtToPay") = value
        End Set
    End Property


    ''Private Shared sUserID As String
    Private Property sUserID() As String
        Get
            Return ViewState("sUserID")
        End Get
        Set(ByVal value As String)
            ViewState("sUserID") = value
        End Set
    End Property


    ''Private Shared PmPaymentDate As String
    Private Property PmPaymentDate() As String
        Get
            Return ViewState("PmPaymentDate")
        End Get
        Set(ByVal value As String)
            ViewState("PmPaymentDate") = value

        End Set
    End Property

    Private Property DefaultCurrency As String
        Get
            Return ViewState("DefaultCurrency")
        End Get
        Set(ByVal value As String)
            ViewState("DefaultCurrency") = value
        End Set
    End Property

    Private Property DefaultCurrencyDes() As String
        Get
            Return ViewState("DefaultCurrencyDes")
        End Get
        Set(ByVal value As String)
            ViewState("DefaultCurrencyDes") = value
        End Set
    End Property


    ''Private Shared UserLocCode As String
    Private Property UserLocCode() As String
        Get
            Return ViewState("UserLocCode")
        End Get
        Set(ByVal value As String)
            ViewState("UserLocCode") = value
        End Set
    End Property


    ''Private Shared sSurList As String
    Private Property sSurList() As String
        Get
            Return ViewState("sSurList")
        End Get
        Set(ByVal value As String)
            ViewState("sSurList") = value
        End Set
    End Property

    ''Private Shared sFinalXml As String
    Private Property sFinalXml() As String
        Get
            Return ViewState("sFinalXml")
        End Get
        Set(ByVal value As String)
            ViewState("sFinalXml") = value
        End Set
    End Property

    ''Private Shared sSettingXml As String
    Private Property sSettingXml() As String
        Get
            Return ViewState("sSettingXml")
        End Get
        Set(ByVal value As String)
            ViewState("sSettingXml") = value
        End Set
    End Property


    ''Private Shared iRunTotalAmount As String
    Private Property iRunTotalAmount() As String
        Get
            Return ViewState("iRunTotalAmount")
        End Get
        Set(ByVal value As String)
            ViewState("iRunTotalAmount") = value
        End Set
    End Property


    ''Private Shared iBalance As String
    Private Property iBalance() As String
        Get
            Return ViewState("iBalance")
        End Get
        Set(ByVal value As String)
            ViewState("iBalance") = value
        End Set
    End Property


    ''Private Shared sPaymentRequest As String
    Private Property sPaymentRequest() As String
        Get
            Return ViewState("sPaymentRequest")
        End Get
        Set(ByVal value As String)
            ViewState("sPaymentRequest") = value
        End Set
    End Property


    ''Private Shared sAmountsOutstandingRoot As String
    Private Property sAmountsOutstandingRoot() As String
        Get
            Return ViewState("sAmountsOutstandingRoot")
        End Get
        Set(ByVal value As String)
            ViewState("sAmountsOutstandingRoot") = value
        End Set
    End Property

    ''Private Shared sMode As String
    Private Property sMode() As String
        Get
            Return ViewState("sMode")
        End Get
        Set(ByVal value As String)
            ViewState("sMode") = value
        End Set
    End Property

    ''Private Shared node As XmlNode
    Private Property node() As XmlNode
        Get
            Return CType(Session("Mode"), XmlNode)
        End Get
        Set(ByVal value As XmlNode)
            Session("Mode") = value
        End Set
    End Property

    ''Private Shared parentNode As XmlNode
    Private Property parentNode() As XmlNode
        Get
            Return CType(Session("parentNode"), XmlNode)
        End Get
        Set(ByVal value As XmlNode)
            Session("parentNode") = value
        End Set
    End Property

    ''    Private Shared bSuperVisor As Object
    Private Property bSuperVisor() As Object
        Get
            Return ViewState("bSuperVisor")
        End Get
        Set(ByVal value As Object)
            ViewState("bSuperVisor") = value
        End Set
    End Property


    ''Private Shared bOridRev As Object
    Private Property bOridRev() As Object
        Get
            Return ViewState("bOridRev")
        End Get
        Set(ByVal value As Object)
            ViewState("bOridRev") = value
        End Set
    End Property

    ''Private Shared bDPSRole As Object
    Private Property bDPSRole() As Object
        Get
            ''Return False -- for testing
            Return ViewState("bDPSRole")
        End Get
        Set(ByVal value As Object)
            ViewState("bDPSRole") = value
        End Set
    End Property


    ''Private Shared UserLocationCode As String
    Private Property UserLocationCode() As Object
        Get
            Return ViewState("UserLocationCode")
        End Get
        Set(ByVal value As Object)
            ViewState("UserLocationCode") = value
        End Set
    End Property

    ''Private Shared sRntCtyCode As String
    Private Property sRntCtyCode() As String
        Get
            If (ViewState("sRntCtyCode") = "NZ" Or ViewState("sRntCtyCode") = "AU") Then
                Me.SupervisorOverrideControl1.ViewReason = True
            Else
                Me.SupervisorOverrideControl1.ViewReason = False
            End If
            Return ViewState("sRntCtyCode")
        End Get
        Set(ByVal value As String)
            ViewState("sRntCtyCode") = value
        End Set
    End Property

    ''Private Shared sBrdCode As String
    Private Property sBrdCode() As String
        Get
            Return ViewState("sBrdCode")
        End Get
        Set(ByVal value As String)
            ViewState("sBrdCode") = value
        End Set
    End Property

    ''Private Shared sComCode As String
    Private Property sComCode() As String
        Get
            Return ViewState("sComCode")
        End Get
        Set(ByVal value As String)
            ViewState("sComCode") = value
        End Set
    End Property


    ''Private Shared PmtPaymentOut As String
    Private Property PmtPaymentOut() As String
        Get
            Return ViewState("PmtPaymentOut")
        End Get
        Set(ByVal value As String)
            ViewState("PmtPaymentOut") = value
        End Set
    End Property


    ''Private Shared DefaultCurrencyDesc As String
    Private Property DefaultCurrencyDesc() As String
        Get
            Return ViewState("DefaultCurrencyDesc")
        End Get
        Set(ByVal value As String)
            ViewState("DefaultCurrencyDesc") = value
        End Set
    End Property


    ''Private Shared sType As String
    Private Property sType() As String
        Get
            Return ViewState("sType")
        End Get
        Set(ByVal value As String)
            ViewState("sType") = value
        End Set
    End Property


    ''Private Shared sXmlData As String
    Private Property sXmlData() As String
        Get
            Return ViewState("sXmlData")
        End Get
        Set(ByVal value As String)
            ViewState("sXmlData") = value
        End Set
    End Property

    ''Private Shared PreserveAmt As Decimal = 0
    Private Property PreserveAmt() As String
        Get
            If ViewState("PreserveAmt") Is Nothing Then
                Return 0
            Else
                Return ViewState("PreserveAmt")
            End If

        End Get
        Set(ByVal value As String)
            ViewState("PreserveAmt") = value
        End Set
    End Property

    ''Private Shared showCard As String
    Private Property showCard() As String
        Get
            Return ViewState("showCard")
        End Get
        Set(ByVal value As String)
            ViewState("showCard") = value
        End Set
    End Property

    ''Private Shared updateCardNum As Integer
    Private Property updateCardNum() As String
        Get
            Return ViewState("updateCardNum")
        End Get
        Set(ByVal value As String)
            ViewState("updateCardNum") = value
        End Set
    End Property

    ''Private Shared pStatus As String
    Private Property pStatus() As String
        Get
            Return ViewState("pStatus")
        End Get
        Set(ByVal value As String)
            ViewState("pStatus") = value
        End Set
    End Property

    ''Private Shared bUserLocation As Boolean
    Private Property bUserLocation() As String
        Get
            Return ViewState("bUserLocation")
        End Get
        Set(ByVal value As String)
            ViewState("bUserLocation") = value
        End Set
    End Property

    ''Private Shared bPmTypeAllowToChange As Boolean
    Private Property bPmTypeAllowToChange() As Boolean
        Get
            Return ViewState("bPmTypeAllowToChange")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bPmTypeAllowToChange") = value
        End Set
    End Property

    ''Private Shared pmtLocCode As String
    Private Property pmtLocCode() As String
        Get
            Return ViewState("pmtLocCode")
        End Get
        Set(ByVal value As String)
            ViewState("pmtLocCode") = value
        End Set
    End Property


    ''Private Shared isPostedToFinanc As Object
    Private Property isPostedToFinanc() As String
        Get
            Return ViewState("isPostedToFinanc")
        End Get
        Set(ByVal value As String)
            ViewState("isPostedToFinanc") = value
        End Set
    End Property


    ''Private Shared sOrigLocCode As String
    Private Property sOrigLocCode() As String
        Get
            Return ViewState("sOrigLocCode")
        End Get
        Set(ByVal value As String)
            ViewState("sOrigLocCode") = value
        End Set
    End Property


    ''Private Shared sAuthCode As String
    Private Property sAuthCode() As String
        Get
            Return ViewState("sAuthCode")
        End Get
        Set(ByVal value As String)
            ViewState("sAuthCode") = value
        End Set
    End Property


    ''Private Shared sDpsTxnRef As String
    Private Property sDpsTxnRef() As String
        Get
            Return ViewState("sDpsTxnRef")
        End Get
        Set(ByVal value As String)
            ViewState("sDpsTxnRef") = value
        End Set
    End Property

    ''Private Shared methodvalue As String
    Private Property methodvalue() As String
        Get
            Return ViewState("methodvalue")
        End Get
        Set(ByVal value As String)
            ViewState("methodvalue") = value
        End Set
    End Property

    ''Private Shared methodType As String
    Private Property methodType() As String
        Get
            Return ViewState("methodType")
        End Get
        Set(ByVal value As String)
            ViewState("methodType") = value
        End Set
    End Property

    ''Private Shared defaultCurrencyForTravCheck As String
    Private Property defaultCurrencyForTravCheck() As String
        Get
            Return ViewState("defaultCurrencyForTravCheck")
        End Get
        Set(ByVal value As String)
            ViewState("defaultCurrencyForTravCheck") = value
        End Set
    End Property

    ''Private Shared defaultCurrencyForCash As String
    Private Property defaultCurrencyForCash() As String
        Get
            Return ViewState("defaultCurrencyForCash")
        End Get
        Set(ByVal value As String)
            ViewState("defaultCurrencyForCash") = value
        End Set
    End Property

    ''Private Shared bCancel As Boolean
    Private Property bCancel() As Boolean
        Get
            Return ViewState("bCancel")
        End Get
        Set(ByVal value As Boolean)
            ViewState("bCancel") = value
        End Set
    End Property

    Dim decAmt As Decimal



#End Region

#Region " Function for Cash"

    Private Function addCashPayment() As XmlDocument

        Dim szMethod, szCurrency, szCurrencyDesc As String
        Dim szAmount, szAmountLocal As String
        Dim valMethod As String
        Dim valSubMethod As String
        Dim selIndex As String
        Dim rootObj As XmlDocument
        Dim tItemNode As XmlNode
        Dim i, j As Integer
        Dim nAmount As Decimal
        Dim valPdtId As String
        Dim valPaymentDate, valStatus, PaymentIntegrity, PdtIntegrity, valPaymentId As String
        Dim szLocalIdentifier As String
        Dim localCurrency As String
        Dim localCurr As String
        Dim foreignCurr As String

        If xmlActivePaymentData.OuterXml = "" AndAlso String.IsNullOrEmpty(litxmlActivePaymentData.Text) = False Then
            xmlActivePaymentData.LoadXml(Server.HtmlDecode(litxmlActivePaymentData.Text))
        End If


        If xmlPaymentmgt.OuterXml = "" Then
            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
        End If

        If xmlPaymentMethods.OuterXml = "" Then
            xmlPaymentMethods.LoadXml(Server.HtmlDecode(litxmlPaymentMethods.Text))
        End If

        Try
            If (xmlActivePaymentData.ChildNodes.Count - 1) = 0 Then
                PaymentIntegrity = xmlActivePaymentData.SelectSingleNode("//PaymentIntegrityNo").InnerText
                valPaymentId = xmlActivePaymentData.SelectSingleNode("//PaymentId").InnerText
                valStatus = xmlActivePaymentData.SelectSingleNode("//Status").InnerText
                valPaymentDate = xmlActivePaymentData.SelectSingleNode("//PaymentDate").InnerText
                valPdtId = xmlActivePaymentData.SelectSingleNode("//PdtId").InnerText
                PdtIntegrity = xmlActivePaymentData.SelectSingleNode("//PdtIntegrityNo").InnerText
                szLocalIdentifier = xmlActivePaymentData.SelectSingleNode("//LocalIdentifier").InnerText
            Else
                PaymentIntegrity = 0
                PdtIntegrity = 0
            End If
            szAmountLocal = lblTotal.Text
            If String.IsNullOrEmpty(szAmountLocal) Or szAmountLocal.Equals("0.00") And ddlMethod.Enabled = True Then
                MyBase.SetInformationMessage("All fields are mandatory")
                Return Nothing
            End If

            selIndex = Me.ddlMethod.SelectedIndex
            valMethod = Me.ddlMethod.SelectedValue
            localCurrency = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
            BaseLocalcurrency = xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText
            rootObj = Me.getPaymentEntryXML

            Dim tmpNode As XmlNode = xmlPaymentMethods.SelectSingleNode("//data")

            For k As Integer = 0 To tmpNode.ChildNodes.Count - 1
                Dim strTmp As String = tmpNode.ChildNodes(k).ChildNodes.Item(1).InnerText.ToLower
                Dim lIndex As Integer = strTmp.IndexOf("local")
                If lIndex > -1 Then
                    localCurr = tmpNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                Else
                    foreignCurr = tmpNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                End If
            Next

            If String.IsNullOrEmpty(localCurr) AndAlso String.IsNullOrEmpty(foreignCurr) Then
                MyBase.SetInformationShortMessage("Local currency and Foreign Curreny not found in AddCashPayment Function")
                Return Nothing
            End If

            Dim otxt As TextBox = Nothing
            Dim ddl As DropDownList = Nothing

            For i = 0 To 2
                If i = 0 Then
                    otxt = DirectCast(Me.txt_Cash_amount1, TextBox)
                    ddl = DirectCast(Me.ddl_Cash_Currency1, DropDownList)
                ElseIf i = 1 Then
                    otxt = DirectCast(Me.txt_Cash_amount2, TextBox)
                    ddl = DirectCast(Me.ddl_Cash_Currency2, DropDownList)
                ElseIf i = 2 Then
                    otxt = DirectCast(Me.txt_Cash_amount3, TextBox)
                    ddl = DirectCast(Me.ddl_Cash_Currency3, DropDownList)
                End If

                If i <= 2 Then

                    If String.IsNullOrEmpty(otxt.Text) = False Then
                        selIndex = ddl.SelectedIndex
                        szCurrency = ddl.SelectedValue
                        szCurrencyDesc = ddl.SelectedItem.Text
                        szAmount = otxt.Text

                        ''rev:mia dec2
                        If String.IsNullOrEmpty(localCurrency) AndAlso LocalString.Contains("Local") Then
                            localCurrency = DefaultCurrency
                        End If
                        If (localCurrency = szCurrency) Then
                            szMethod = "Cash - Local"
                            valSubMethod = localCurr
                        Else
                            szMethod = "Cash - Foreign"
                            valSubMethod = foreignCurr
                        End If

                        Dim bFound As Boolean = False
                        Dim tindex As Integer = rootObj.ChildNodes(0).ChildNodes.Count - 1

                        For j = 0 To tindex
                            If (szCurrency = rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(6).InnerText) Then
                                bFound = True
                                nAmount = szAmount
                                Dim currAmount As String = rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(13).ChildNodes(0).ChildNodes(8).InnerText
                                Dim adjAmount As Decimal = currAmount + nAmount
                                Dim tAmount As Decimal = rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(4).InnerText
                                tAmount = tAmount + nAmount
                                Exit For
                            End If

                            If Not bFound Then

                                Dim bCreate As Boolean = True
                                If (tindex = 0) Then
                                    If (rootObj.ChildNodes(0).ChildNodes(0).SelectSingleNode("//MethodValue").InnerText = "") Then
                                        Dim valAmountLocal As String = getLocalAmountGeneric(BaseLocalcurrency, szCurrencyDesc, otxt.Text)
                                        rootObj.SelectSingleNode("//MethodValue").InnerText = valMethod
                                        rootObj.SelectSingleNode("//Method").InnerText = szMethod
                                        rootObj.SelectSingleNode("//Details").InnerText = "N/A"
                                        rootObj.SelectSingleNode("//Amount").InnerText = szAmount
                                        rootObj.SelectSingleNode("//LocalAmount").InnerText = valAmountLocal
                                        rootObj.SelectSingleNode("//IsRemove").InnerText = 0
                                        rootObj.SelectSingleNode("//SubMethod").InnerText = valSubMethod
                                        rootObj.SelectSingleNode("//Currency").InnerText = szCurrency
                                        rootObj.SelectSingleNode("//CurrencyDesc").InnerText = szCurrencyDesc
                                        rootObj.SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
                                        rootObj.SelectSingleNode("//PmEntryPaymentId").InnerText = valPaymentId
                                        rootObj.SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
                                        rootObj.SelectSingleNode("//Status").InnerText = valStatus
                                        rootObj.SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier

                                        tItemNode = rootObj.SelectSingleNode("//PaymentEntryItems")

                                        tItemNode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = valPaymentId
                                        tItemNode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = ""
                                        tItemNode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = szAmount
                                        tItemNode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
                                        tItemNode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = valAmountLocal
                                        tItemNode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId
                                        bCreate = False
                                    End If
                                End If ''If (tindex = 1) Then

                                If bCreate And rootObj.DocumentElement.ChildNodes.Count < 3 Then
                                    Dim gCloneNode As XmlDocument = getPaymentEntryNode()
                                    Dim valAmountLocal As String = getLocalAmountGeneric(BaseLocalcurrency, szCurrencyDesc, otxt.Text)
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//MethodValue").InnerText = valMethod
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Method").InnerText = szMethod
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Details").InnerText = "N/A"
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Amount").InnerText = szAmount
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//LocalAmount").InnerText = valAmountLocal
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//IsRemove").InnerText = 0
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//SubMethod").InnerText = valSubMethod
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Currency").InnerText = szCurrency
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//CurrencyDesc").InnerText = szCurrencyDesc
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PmEntryPaymentId").InnerText = valPaymentId
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Status").InnerText = valStatus
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = valPaymentId
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = ""
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = szAmount
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = valAmountLocal
                                    gCloneNode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId
                                    Dim addNode As XmlNode = gCloneNode.SelectSingleNode("//PaymentEntry")
                                    ''rootObj.ChildNodes(0).AppendChild(addNode)

                                    Dim xmltempAddnode As String = addNode.OuterXml & "</PaymentEntryRoot>"
                                    Dim xmltempAddroot As String = rootObj.OuterXml
                                    xmltempAddroot = xmltempAddroot.Replace("</PaymentEntryRoot>", xmltempAddnode)
                                    rootObj.LoadXml(xmltempAddroot)

                                End If ''If bCreate Then

                            End If ''If Not bFound Then

                        Next ''For j = 0 To tindex

                    End If ''If String.IsNullOrEmpty(otxt.Text) = False Then

                End If ''If i <= 2 Then

            Next ''For i = 0 To 2


            For i = 0 To rootObj.ChildNodes(0).ChildNodes.Count - 1
                Dim sumAmount As Decimal
                Dim exchAmount As Decimal
                Dim sumNode As XmlNode = rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(13)
                Dim tLength As Integer = sumNode.ChildNodes.Count - 1
                ''rev:mia nov11
                For j = 0 To tLength
                    sumAmount += (sumNode.ChildNodes(j).ChildNodes.Item(8).InnerText)
                Next
                If (localCurrency = rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(6).InnerText) Then
                    exchAmount = sumAmount
                    ''rev:mia nov11
                    sumAmount = 0
                Else
                    Dim tdesc As String = rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(7).InnerText
                    Dim p1 As String = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
                    exchAmount = Me.getLocalAmountGeneric(p1, tdesc, sumAmount)
                    If (exchAmount = Nothing) Then
                        exchAmount = sumAmount
                    ElseIf exchAmount = -1 Then
                        MyBase.SetInformationMessage("No exchange rate exists for " + tdesc + " -> " + xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText)
                        sumAmount = 0
                    Else
                        sumAmount = 0
                        exchAmount += sumAmount
                    End If
                    ''exchAmount = sumAmount
                End If
                rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText = String.Format("{0:N}", exchAmount)
            Next

        Catch ex As Exception
            MyBase.SetErrorShortMessage("AddCashPayment -> " & ex.Message)
            ddlMethod.SelectedIndex = -1
            ddlMethod.Enabled = True
            Return Nothing
        End Try
        Return rootObj
    End Function

#End Region

#Region " Function for Travellers Check"

    Function ReturnTotalTravellersCheck() As Decimal
        Dim sum As Decimal = 0
        If xmlTravellerChequeDetails.OuterXml <> "" Then
            Dim nodes As XmlNodeList = xmlTravellerChequeDetails.SelectNodes("TravellerChqRoot/CheckEntry/PdtLocalAmount")
            For Each mynode As XmlNode In nodes
                If mynode.Name = ("PdtLocalAmount") Then
                    sum = sum + CDec(mynode.InnerText)
                End If
            Next
        End If
        Return sum
    End Function

    ''' <summary>
    ''' addTrvrChequeToTemp2
    ''' </summary>
    ''' <param name="pmtItemObj"></param>
    ''' <param name="szCurrency"></param>
    ''' <param name="szCurrencyDesc"></param>
    ''' <param name="szMethod"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function addTrvrChequeToTempRetreive(ByVal pmtItemObj As XmlNode, _
                                                    ByVal szCurrency As String, _
                                                    ByVal szCurrencyDesc As String, _
                                                    ByVal szMethod As String) As String




        Dim szDetails As String
        Dim szAmount As String

        Dim valMethod As String



        Dim valAmountLocal As String


        Dim valSubMethod As String

        Dim valChequeNo As String
        Dim valBank As String
        Dim valBranch As String
        Dim valAccountNo As String


        Dim localCurrency As String
        Dim localCurrencyDesc As String
        Dim localCheque As String = ""
        Dim foreignCheque As String = ""
        Dim valSequentNo As String = ""




        Dim valPdtId As String
        Dim objDom As XmlDocument
        Dim valPaymentId As String
        Dim valStatus As String
        Dim valPaymentDate As String
        Dim PaymentIntegrity As String
        Dim PdtIntegrity As String

        Dim strTemp As String
        Dim szLocalIdentifier As String
        Try
            If litxmlActivePaymentData.Text <> "" Then
                If Me.xmlActivePaymentData.OuterXml = "" Then
                    Me.xmlActivePaymentData.LoadXml(Server.HtmlDecode(Me.litxmlActivePaymentData.Text))
                End If
            End If

            If (Me.xmlActivePaymentData.ChildNodes.Count - 1) < 0 Then
                MyBase.SetInformationMessage("addTrvrChequeToTempRetreive-> xmlActivePaymentData is empty")
                Return ""
            End If

            If (xmlPaymentmgt.OuterXml = "") Then
                xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
            End If


            PaymentIntegrity = xmlActivePaymentData.SelectSingleNode("//PaymentIntegrityNo").InnerText
            valPaymentId = xmlActivePaymentData.SelectSingleNode("//PaymentId").InnerText
            valStatus = xmlActivePaymentData.SelectSingleNode("//Status").InnerText
            valPaymentDate = xmlActivePaymentData.SelectSingleNode("//PaymentDate").InnerText
            szLocalIdentifier = xmlActivePaymentData.SelectSingleNode("//LocalIdentifier").InnerText


            If xmlPaymentMethods.OuterXml = "" Then
                xmlPaymentMethods.LoadXml(Server.HtmlDecode(litxmlPaymentMethods.Text))
            End If


            Dim tempNode As XmlNode = xmlPaymentMethods.SelectSingleNode("//data")
            For k As Integer = 0 To tempNode.ChildNodes.Count - 1
                strTemp = tempNode.ChildNodes(k).ChildNodes.Item(1).InnerText.ToLower
                Dim indexOfLocal As Integer = strTemp.IndexOf("local")
                If indexOfLocal > -1 Then
                    localCheque = tempNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                Else
                    foreignCheque = tempNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                End If

                If Not String.IsNullOrEmpty(localCheque) AndAlso Not String.IsNullOrEmpty(foreignCheque) Then
                    Exit For
                End If
            Next

            If String.IsNullOrEmpty(localCheque) Or String.IsNullOrEmpty(foreignCheque) Then
                MyBase.SetInformationMessage("addTrvrChequeToTempRetreive-> localCheque or foreignCheque is empty ")
                Return Nothing
            End If

            valMethod = szMethod
            localCurrency = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
            localCurrencyDesc = xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText

            ''rev:mia dec2
            If String.IsNullOrEmpty(localCurrency) AndAlso LocalString.Contains("Local") Then
                localCurrency = DefaultCurrency
            End If
            If localCurrency = szCurrency Then
                szMethod = "Travellers Chq - Local"
                valSubMethod = localCheque
            Else
                szMethod = "Travellers Chq - Foreign"
                valSubMethod = foreignCheque
            End If


            valPdtId = pmtItemObj.ChildNodes(11).InnerText
            szAmount = pmtItemObj.ChildNodes(8).InnerText
            valChequeNo = pmtItemObj.ChildNodes(4).InnerText
            valBank = pmtItemObj.ChildNodes(5).InnerText
            valBranch = pmtItemObj.ChildNodes(6).InnerText
            valAccountNo = pmtItemObj.ChildNodes(7).InnerText
            PdtIntegrity = pmtItemObj.ChildNodes(9).InnerText
            szDetails = String.Concat(valBank, "-", valBranch, "-", valAccountNo, "-", valChequeNo)

            If localCurrency <> szCurrency Then
                valAmountLocal = getLocalAmountGeneric(localCurrencyDesc, szCurrencyDesc, szAmount)
                If String.IsNullOrEmpty(valAmountLocal) = True Then
                    valAmountLocal = szAmount
                ElseIf (valAmountLocal = -1) Then
                    ''litjs.Text = "<script language=""javascript"">alert('No exchange rate exists for '" + szCurrencyDesc + "' -> '" + xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText + "')</script>"
                    MyBase.SetInformationMessage("addTrvrChequeToTempRetreive->  No exchange rate exists for " & szCurrencyDesc & " -> " & xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText)
                    valAmountLocal = ""
                    Return Nothing
                End If
            Else
                valAmountLocal = szAmount
            End If

            objDom = getTravelChequeNode()
            If objDom.OuterXml = "" Then
                MyBase.SetInformationMessage("addTrvrChequeToTempRetreive->  objDom.OuterXml is empty")
                Return ""
            End If

            objDom.SelectSingleNode("//MethodValue").InnerText = valMethod
            objDom.SelectSingleNode("//Method").InnerText = szMethod
            objDom.SelectSingleNode("//Details").InnerText = szDetails
            objDom.SelectSingleNode("//Amount").InnerText = szAmount
            objDom.SelectSingleNode("//IsRemove").InnerText = 0
            objDom.SelectSingleNode("//SubMethod").InnerText = valSubMethod
            objDom.SelectSingleNode("//Currency").InnerText = szCurrency
            objDom.SelectSingleNode("//CurrencyDesc").InnerText = szCurrencyDesc
            objDom.SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
            objDom.SelectSingleNode("//ChequeNo").InnerText = valChequeNo
            objDom.SelectSingleNode("//Bank").InnerText = valBank
            objDom.SelectSingleNode("//Branch").InnerText = valBranch
            objDom.SelectSingleNode("//AccountNo").InnerText = valAccountNo
            objDom.SelectSingleNode("//SequenCheques").InnerText = valSequentNo
            objDom.SelectSingleNode("//PdtLocalAmount").InnerText = valAmountLocal
            objDom.SelectSingleNode("//PdtId").InnerText = valPdtId
            objDom.SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
            objDom.SelectSingleNode("//PaymentId").InnerText = valPaymentId
            objDom.SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
            objDom.SelectSingleNode("//PaymentStatus").InnerText = valStatus
            objDom.SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier

            If Me.litxmlTravellerChequeDetails.Text <> "" Then
                If Me.xmlTravellerChequeDetails.OuterXml = "" Then
                    Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(Me.litxmlTravellerChequeDetails.Text))
                End If
            End If



            If xmlTravellerChequeDetails.ChildNodes.Count = 0 Then
                xmlTravellerChequeDetails.LoadXml(objDom.SelectSingleNode("//TravellerChqRoot").OuterXml)
            Else
                Dim nNode As XmlNode = objDom.SelectSingleNode("//CheckEntry")
                Dim xmlTempString As String = xmlTravellerChequeDetails.OuterXml
                xmlTempString = xmlTempString.Replace("</TravellerChqRoot>", nNode.OuterXml & "</TravellerChqRoot>")
                xmlTravellerChequeDetails.LoadXml(xmlTempString)
            End If



        Catch ex As Exception
            MyBase.SetErrorShortMessage("addTrvrChequeToTempRetreive-> " & ex.Message)
        End Try

        Return xmlTravellerChequeDetails.OuterXml
    End Function

    Private Function addTrvrChequeToTemp() As String
        Dim szMethod As String
        Dim szDetails As String
        Dim szCurrency As String
        Dim szCurrencyDesc As String
        Dim szAmount As String
        Dim szIsRemove As Boolean = False

        Dim valMethod As String
        Dim valAmountLocal As String


        Dim valSubMethod As String

        Dim valChequeNo As String
        Dim valBank As String
        Dim valBranch As String
        Dim valAccountNo As String

        Dim selIndex As Integer = -1
        Dim selCurrencyIndex As Integer = -1

        Dim localCurrency As String
        Dim localCurrencyDesc As String

        Dim localCheque As String
        Dim foreignCheque As String
        Dim valSequentNo As Integer = 0
        Dim i As Integer = -1
        Dim j As Integer = -1

        Dim totalTendered As Decimal = 0
        Dim objDom As New XmlDocument
        Dim strTemp As String
        Try
            If xmlPaymentMethods.OuterXml = "" Then
                xmlPaymentMethods.LoadXml(Server.HtmlDecode(litxmlPaymentMethods.Text))
            End If


            ''Dim tempNode As XmlNode = xmlPaymentMethods.SelectSingleNode("//Data")

            If (xmlPaymentmgt.OuterXml = "") Then
                xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
            End If

            Dim tempNode As XmlNode = xmlPaymentMethods.SelectSingleNode("//data")
            For k As Integer = 0 To tempNode.ChildNodes.Count - 1
                strTemp = tempNode.ChildNodes(k).ChildNodes.Item(1).InnerText.ToLower
                Dim indexOfLocal As Integer = strTemp.IndexOf("local")
                If indexOfLocal > -1 Then
                    localCheque = tempNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                Else
                    foreignCheque = tempNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                End If

                If Not String.IsNullOrEmpty(localCheque) AndAlso Not String.IsNullOrEmpty(foreignCheque) Then
                    Exit For
                End If
            Next

            If String.IsNullOrEmpty(localCheque) Or String.IsNullOrEmpty(foreignCheque) Then
                Return Nothing
            End If

            selIndex = Me.ddlMethod.SelectedIndex
            valMethod = Me.ddlMethod.SelectedValue

            localCurrency = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
            localCurrencyDesc = xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText
            ''rev:mia nov10
            BaseLocalcurrency = localCurrencyDesc
            selCurrencyIndex = Me.ddl_TravellersCheque_Currency.SelectedIndex
            szCurrency = Me.ddl_TravellersCheque_Currency.SelectedValue
            szCurrencyDesc = Me.ddl_TravellersCheque_Currency.SelectedItem.Text
            szAmount = txtAmount.Text


            If localCurrency = szCurrency Then
                szMethod = "Travellers Chq - Local"
                valSubMethod = localCheque
            Else
                szMethod = "Travellers Chq - Foreign"
                valSubMethod = foreignCheque
            End If


            valChequeNo = Me.txt_TravellersCheque_ChequeNo.Text
            valBank = Me.txt_TravellersCheque_Bank.Text
            valBranch = Me.txt_TravellersCheque_Branch.Text
            valAccountNo = Me.txt_TravellersCheque_AccountNo.Text
            szAmount = Me.txt_TravellersCheque_Amount.Text
            szDetails = String.Concat(valBank, "-", valBranch, "-", valAccountNo, "-", valChequeNo, " ")
            valSequentNo = Me.txt_TravellersCheque_SeqChequec.Text

            If localCurrency <> szCurrency Then
                valAmountLocal = getLocalAmountGeneric(localCurrencyDesc, szCurrencyDesc, szAmount)
                If String.IsNullOrEmpty(valAmountLocal) = True Then
                    valAmountLocal = szAmount
                ElseIf (valAmountLocal = -1) Then
                    ''litjs.Text = "<script language=""javascript"">alert('No exchange rate exists for '" + szCurrencyDesc + "' -> '" + xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText + "')</script>"
                    MyBase.SetInformationMessage("addTrvrChequeToTemp->  No exchange rate exists for " & szCurrencyDesc & " -> " & xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText)
                    valAmountLocal = ""
                    Return Nothing
                End If
            Else
                valAmountLocal = szAmount
            End If

            objDom = getTravelChequeNode()
            If objDom.OuterXml = "" Then
                MyBase.SetInformationMessage("addTrvrChequeToTemp->  objDom.OuterXml is empty")
                Return ""
            End If

            objDom.SelectSingleNode("//MethodValue").InnerText = valMethod
            objDom.SelectSingleNode("//Method").InnerText = szMethod
            objDom.SelectSingleNode("//Details").InnerText = szDetails
            objDom.SelectSingleNode("//Amount").InnerText = szAmount
            objDom.SelectSingleNode("//IsRemove").InnerText = 0
            objDom.SelectSingleNode("//SubMethod").InnerText = valSubMethod
            objDom.SelectSingleNode("//Currency").InnerText = szCurrency
            objDom.SelectSingleNode("//CurrencyDesc").InnerText = szCurrencyDesc
            objDom.SelectSingleNode("//IntegrityNo").InnerText = 0
            objDom.SelectSingleNode("//ChequeNo").InnerText = valChequeNo
            objDom.SelectSingleNode("//Bank").InnerText = valBank
            objDom.SelectSingleNode("//Branch").InnerText = valBranch
            objDom.SelectSingleNode("//AccountNo").InnerText = valAccountNo
            objDom.SelectSingleNode("//SequenCheques").InnerText = valSequentNo
            objDom.SelectSingleNode("//PdtLocalAmount").InnerText = valAmountLocal
            objDom.SelectSingleNode("//PdtId").InnerText = ""
            objDom.SelectSingleNode("//PdtIntegrityNo").InnerText = 0
            objDom.SelectSingleNode("//PaymentId").InnerText = ""
            objDom.SelectSingleNode("//PaymentDate").InnerText = ""
            objDom.SelectSingleNode("//PaymentStatus").InnerText = ""

            ''if litxmlTravellerChequeDetails
            If xmlTravellerChequeDetails.ChildNodes.Count = 0 Then
                xmlTravellerChequeDetails.LoadXml(objDom.SelectSingleNode("//TravellerChqRoot").OuterXml)
            Else
                Dim nNode As XmlNode = objDom.SelectSingleNode("//CheckEntry")
                Dim xmlTempString As String = xmlTravellerChequeDetails.OuterXml
                xmlTempString = xmlTempString.Replace("</TravellerChqRoot>", nNode.OuterXml & "</TravellerChqRoot>")
                xmlTravellerChequeDetails.LoadXml(xmlTempString)
            End If

        Catch ex As Exception
            MyBase.SetInformationMessage("addTrvrChequeToTemp -> " & ex.Message)
            objDom = Nothing
        End Try
        Return xmlTravellerChequeDetails.OuterXml

    End Function

    Private Function getTravelChequeNode() As XmlDocument
        Dim objDom As New XmlDocument
        objDom.LoadXml("<TravellerChqRoot><CheckEntry/></TravellerChqRoot>")
        parentNode = objDom.SelectSingleNode("//CheckEntry")

        Try
            node = objDom.CreateElement("MethodValue", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("Method", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("Details", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("Currency", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("Amount", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("IsRemove", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("SubMethod", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("CurrencyDesc", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("IntegrityNo", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("ChequeNo", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("Bank", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("Branch", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("AccountNo", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("SequenCheques", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("PdtLocalAmount", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("PdtId", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("PdtIntegrityNo", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("PaymentId", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("PaymentDate", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("PaymentStatus", "")
            parentNode.AppendChild(node)

            node = objDom.CreateElement("LocalIdentifier", "")
            parentNode.AppendChild(node)

        Catch ex As Exception
            MyBase.SetErrorShortMessage("getTravelChequeNode -> " & ex.Message)
            objDom = Nothing
        End Try
        Return objDom
    End Function

    Private Function increaseChequeNum(ByVal checknumber As String, ByVal intIncrement As Integer) As String
        Dim retvalue As String = ""
        Dim checknumberInt As Integer
        If String.IsNullOrEmpty(checknumber) = True Then
            Return ""
        End If

        If (checknumber.StartsWith("0", StringComparison.OrdinalIgnoreCase) = False) Then
            Try
                checknumberInt = Convert.ToInt16(checknumber)
            Catch ex As Exception
                Return checknumber
            End Try
            Return String.Concat(retvalue, CInt(checknumberInt + intIncrement))
        End If


        Dim zeroStrorage As String = ""
        Dim charArray() As Char = CType(checknumber, Char())
        Dim temp As String
        For i As Integer = 0 To charArray.Length
            If charArray(i) = "0" Then
                zeroStrorage = zeroStrorage & charArray(i)
            Else
                Try
                    temp = charArray(i)
                    checknumberInt = CInt(temp)
                    checknumberInt = checknumberInt + intIncrement
                    Exit For
                Catch ex As Exception
                    Return ""
                End Try
            End If
        Next

        Return String.Concat(zeroStrorage, checknumberInt)

    End Function

    Private Function addTravelChequePayment() As XmlDocument

        Dim rootObj As XmlDocument = Nothing

        Try

            If xmlPaymentmgt.OuterXml = "" Then
                xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
            End If

            If Me.litxmlTravellerChequeDetails.Text <> "" Then
                If Me.xmlTravellerChequeDetails.OuterXml = "" Then
                    Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(Me.litxmlTravellerChequeDetails.Text))
                End If
            End If

            Dim localCurrency = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/Currency").InnerText


            Dim tItemNode As XmlNode

            Dim appendNode As XmlNode

            Dim clonedNode As XmlNode

            Dim selIndex As String = Me.ddlMethod.SelectedIndex
            Dim valMethod As String = Me.ddlMethod.SelectedItem.Value.ToString
            Dim valCurrency As String = Me.lblAmountToBePaidCurrency.Text


            Dim tNode As XmlNodeList = xmlTravellerChequeDetails.SelectSingleNode("//TravellerChqRoot").ChildNodes
            If tNode.Count > 0 Then
                rootObj = Me.getPaymentEntryXML
            Else
                MyBase.SetInformationMessage("addTravelChequePaymen-> tnode.count <=0 ")
                Return Nothing
            End If


            Dim i As Integer
            Dim j As Integer
            Dim PaymentIntegrity As String
            Dim valPdtId As String
            Dim PdtIntegrity As String
            Dim szLocalIdentifier As String
            Dim valPaymentId As String
            Dim valPaymentDate As String
            Dim valStatus As String
            Dim nAmount As Decimal
            Dim locAmount As Decimal
            Dim tAmount As Decimal

            Dim szcurrency As String

            For i = 0 To tNode.Count - 1
                PaymentIntegrity = tNode(i).ChildNodes(8).InnerText
                szcurrency = tNode(i).ChildNodes(3).InnerText
                valPdtId = tNode(i).ChildNodes(15).InnerText
                PdtIntegrity = tNode(i).ChildNodes(16).InnerText
                valPaymentId = tNode(i).ChildNodes(17).InnerText
                valPaymentDate = tNode(i).ChildNodes(18).InnerText
                valStatus = tNode(i).ChildNodes(19).InnerText
                szLocalIdentifier = tNode(i).ChildNodes(20).InnerText

                Dim bfound As Boolean = False
                Dim tindex As Integer = rootObj.ChildNodes(0).ChildNodes.Count - 1
                For j = 0 To tindex
                    If (szcurrency = rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(6).InnerText) Then
                        bfound = True
                        clonedNode = rootObj.ChildNodes(0).ChildNodes(0).CloneNode(True)
                        nAmount = tNode(i).ChildNodes(4).InnerText
                        locAmount = tNode(i).ChildNodes(14).InnerText
                        clonedNode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = ""
                        clonedNode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = ""
                        clonedNode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = ""
                        clonedNode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = ""
                        clonedNode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = tNode(i).ChildNodes(9).InnerText
                        clonedNode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = tNode(i).ChildNodes(10).InnerText
                        clonedNode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = tNode(i).ChildNodes(11).InnerText
                        clonedNode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = tNode(i).ChildNodes(12).InnerText
                        clonedNode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = nAmount
                        clonedNode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
                        clonedNode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = locAmount
                        clonedNode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId

                        appendNode = clonedNode.SelectSingleNode("//PmtItem")

                        rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(13).AppendChild(appendNode)
                        tAmount = rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(4).InnerText
                        tAmount = tAmount + nAmount
                        rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(4).InnerText = tAmount ''String.Format("{0:N}", tAmount)
                        Dim tmpDetails As String = String.Concat(tNode(i).ChildNodes(10).InnerText, "-", tNode(i).ChildNodes(11).InnerText, "-", tNode(i).ChildNodes(12).InnerText, "-", tNode(i).ChildNodes(9).InnerText)
                        rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(3).InnerText = String.Concat(rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(3).InnerText, ", ", tmpDetails)
                        tAmount = (rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(5).InnerText)
                        tAmount = tAmount + locAmount
                        rootObj.ChildNodes(0).ChildNodes(j).ChildNodes(5).InnerText = tAmount '' String.Format("{0:N}", tAmount)

                        Exit For
                    End If
                Next
                If Not bfound Then
                    Dim bcreate As Boolean = True
                    If (tindex = 0) Then
                        If (rootObj.ChildNodes(0).ChildNodes(0).SelectSingleNode("//MethodValue").InnerText = "") Then
                            rootObj.SelectSingleNode("//MethodValue").InnerText = tNode(i).ChildNodes(0).InnerText
                            rootObj.SelectSingleNode("//Method").InnerText = tNode(i).ChildNodes(1).InnerText
                            rootObj.SelectSingleNode("//Details").InnerText = tNode(i).ChildNodes(10).InnerText + "-" + tNode(i).ChildNodes(11).InnerText + "-" + tNode(i).ChildNodes(12).InnerText + "-" + tNode(i).ChildNodes(9).InnerText

                            rootObj.SelectSingleNode("//Amount").InnerText = tNode(i).ChildNodes(4).InnerText
                            rootObj.SelectSingleNode("//LocalAmount").InnerText = tNode(i).ChildNodes(14).InnerText ''--REV:MIA NOV12
                            rootObj.SelectSingleNode("//IsRemove").InnerText = 0
                            rootObj.SelectSingleNode("//SubMethod").InnerText = tNode(i).ChildNodes(6).InnerText
                            rootObj.SelectSingleNode("//Currency").InnerText = tNode(i).ChildNodes(3).InnerText
                            rootObj.SelectSingleNode("//CurrencyDesc").InnerText = tNode(i).ChildNodes(7).InnerText
                            rootObj.SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
                            rootObj.SelectSingleNode("//PmEntryPaymentId").InnerText = valPaymentId
                            rootObj.SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
                            rootObj.SelectSingleNode("//Status").InnerText = valStatus
                            rootObj.SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier
                            tItemNode = rootObj.SelectSingleNode("//PaymentEntryItems")
                            tItemNode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = ""
                            tItemNode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = ""
                            tItemNode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = ""
                            tItemNode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = ""
                            tItemNode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = tNode(i).ChildNodes(9).InnerText
                            tItemNode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = tNode(i).ChildNodes(10).InnerText
                            tItemNode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = tNode(i).ChildNodes(11).InnerText
                            tItemNode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = tNode(i).ChildNodes(12).InnerText
                            tItemNode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = tNode(i).ChildNodes(4).InnerText
                            tItemNode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
                            tItemNode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = tNode(i).ChildNodes(14).InnerText ''--REV:MIA NOV12
                            tItemNode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId
                            bcreate = False
                        End If
                    End If
                    If bcreate = True Then
                        Dim gCloneNode As XmlDocument = getPaymentEntryNode()
                        gCloneNode.ChildNodes(0).SelectSingleNode("//MethodValue").InnerText = tNode(i).ChildNodes(0).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Method").InnerText = tNode(i).ChildNodes(1).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Details").InnerText = String.Concat(tNode(i).ChildNodes(10).InnerText, "-", tNode(i).ChildNodes(11).InnerText, "-", tNode(i).ChildNodes(12).InnerText, "-", tNode(i).ChildNodes(9).InnerText)
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Amount").InnerText = tNode(i).ChildNodes(4).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//LocalAmount").InnerText = tNode(i).ChildNodes(14).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//IsRemove").InnerText = 0
                        gCloneNode.ChildNodes(0).SelectSingleNode("//SubMethod").InnerText = tNode(i).ChildNodes(6).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Currency").InnerText = tNode(i).ChildNodes(3).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//CurrencyDesc").InnerText = tNode(i).ChildNodes(7).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PmEntryPaymentId").InnerText = valPaymentId
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Status").InnerText = valStatus
                        gCloneNode.ChildNodes(0).SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier
                        gCloneNode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = ""
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = ""
                        gCloneNode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = ""
                        gCloneNode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = ""
                        gCloneNode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = tNode(i).ChildNodes(9).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = tNode(i).ChildNodes(10).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = tNode(i).ChildNodes(11).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = tNode(i).ChildNodes(12).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = tNode(i).ChildNodes(4).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = tNode(i).ChildNodes(14).InnerText
                        gCloneNode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId
                        Dim addNode As XmlNode = gCloneNode.SelectSingleNode("//PaymentEntry")
                        ''rootObj.ChildNodes(0).AppendChild(addNode)

                        ''rev:mia nov10
                        Dim xmltempAddnode As String = addNode.OuterXml & "</PaymentEntryRoot>"
                        Dim xmltempAddroot As String = rootObj.OuterXml
                        xmltempAddroot = xmltempAddroot.Replace("</PaymentEntryRoot>", xmltempAddnode)
                        rootObj.LoadXml(xmltempAddroot)

                    End If
                End If
            Next
            For i = 0 To rootObj.ChildNodes(0).ChildNodes.Count - 1
                Dim sumAmount As Decimal
                Dim exchAmount As Decimal
                Dim sumNode As XmlNode = rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(13)
                Dim tLength As Integer = sumNode.ChildNodes.Count - 1
                For j = 0 To tLength
                    sumAmount += (sumNode.ChildNodes(j).ChildNodes.Item(8).InnerText)
                Next
                If (localCurrency = rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(6).InnerText) Then
                    exchAmount = sumAmount
                    sumAmount = 0
                Else
                    Dim tdesc As String = rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(7).InnerText
                    Dim p1 As String = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
                    exchAmount = Me.getLocalAmountGeneric(p1, tdesc, sumAmount)
                    If (exchAmount = Nothing) Then
                        exchAmount = sumAmount
                    ElseIf exchAmount = -1 Then
                        MyBase.SetInformationMessage("No exchange rate exists for " + tdesc + " -> " + xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText)
                        sumAmount = 0
                    Else
                        sumAmount = 0
                        exchAmount += sumAmount
                    End If
                    ''exchAmount = sumAmount
                End If
                rootObj.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText = String.Format("{0:N}", exchAmount)
            Next

        Catch ex As Exception
            MyBase.SetErrorShortMessage("addTravelChequePayment-> " & ex.Message)
        End Try

        Return rootObj


    End Function

    Private Function getPaymentEntryNode() As XmlDocument
        Dim tempXMLobj As New XmlDocument
        Try
            tempXMLobj.LoadXml("<root><PaymentEntry></PaymentEntry></root>")
            Dim parentNode As XmlNode = tempXMLobj.SelectSingleNode("//PaymentEntry")
            Me.appendPaymentEntryNode(tempXMLobj, parentNode)
            If tempXMLobj Is Nothing Then
                Return Nothing
            End If
        Catch ex As Exception
            Me.SetErrorMessage(ex.Message)
        End Try
        Return tempXMLobj
    End Function

    Private Function TravellerChequeRoutineRetrieve(ByVal nodeObj As XmlNode, _
                                                    ByVal szCurrency As String, _
                                                    ByVal szCurrencyDesc As String, _
                                                    ByVal szPaymentIntegrityNo As String, _
                                                    ByVal szPaymentId As String, _
                                                    ByVal szStatus As String, _
                                                    ByVal szPaymentDate As String, _
                                                    ByVal szLocalIdentifier As String, _
                                                    ByVal szMethod As String) As XmlDocument

        Dim tLength As Integer = nodeObj.ChildNodes.Count - 1
        Dim i As Integer
        Dim pmtItem As XmlNode


        Try

            For i = 0 To tLength
                pmtItem = nodeObj.ChildNodes(i)
                If i = 0 Then
                    xmlActivePaymentData = getActivePaymentDataXML()
                    If xmlActivePaymentData.OuterXml = "" Then
                        MyBase.SetInformationMessage("TravellerChequeRoutineRetrieve-> getActivePaymentDataXML  is empty")
                        Return Nothing
                    End If
                    Me.setPaymentDetails(pmtItem, szPaymentIntegrityNo, szPaymentId, szStatus, szPaymentDate, szLocalIdentifier)
                End If
                If (addTrvrChequeToTempRetreive(pmtItem, szCurrency, szCurrencyDesc, szMethod) Is Nothing) Then
                    MyBase.SetInformationShortMessage("TravellerChequeRoutineRetrieve-> addTrvrChequeToTempRetreive is empty")
                End If
            Next
        Catch ex As Exception
            MyBase.SetInformationShortMessage("TravellerChequeRoutineRetrieve->  " & ex.Message)
            Return Nothing
        End Try
        Return xmlActivePaymentData
    End Function

#End Region

#Region " Functions for Creditcard"
    Public ReadOnly Property GridCurrency()
        Get

            Return Me.lblAmountToBePaidCurrency.Text
        End Get
    End Property

    Private Function getCurrencyHashContent(ByVal ddl As DropDownList) As Hashtable
        Dim localhash As Hashtable = New Hashtable
        For Each item As ListItem In ddl.Items
            ''rev:mia nov10
            If localhash.ContainsKey(item.Text) = False Then
                localhash.Add(item.Text, item.Value.ToString)
            End If
        Next
        Return localhash
    End Function

    Function IsCreditCard(ByRef asCardType As String, ByRef anCardNumber As String) As Boolean
        ''rev:mia nov3 -- trapped this one
        If anCardNumber.Contains("XXXX") And (Not String.IsNullOrEmpty(Me.litCardNumber.Text)) Then
            anCardNumber = Me.litCardNumber.Text
        End If
        ' Performs a Mod 10 check To make sure the credit card number
        ' appears valid
        ' Developers may use the following numbers as dummy data:
        ' Visa:                               430-00000-00000
        ' American Express:            372-00000-00000
        ' Mastercard:                  521-00000-00000
        ' Discover:                           620-00000-00000

        Dim lsNumber           ' Credit card number stripped of all spaces, dashes, etc.
        Dim lsChar                     ' an individual character
        Dim lnTotal                    ' Sum of all calculations
        Dim lnDigit                    ' A digit found within a credit card number
        Dim lnPosition         ' identifies a character position In a String
        Dim lnSum                      ' Sum of calculations For a specific Set
        Dim lnMultiplier As Integer
        ' Default result is False
        IsCreditCard = False

        ' ====
        ' Strip all characters that are Not numbers.
        ' ====

        ' Loop through Each character inthe card number submited
        For lnPosition = 1 To Len(anCardNumber)
            ' Grab the current character
            lsChar = Mid(anCardNumber, lnPosition, 1)
            ' if the character is a number, append it To our new number
            If IsNumeric(lsChar) Then lsNumber = lsNumber & lsChar

        Next ' lnPosition

        ' ====
        ' The credit card number must be between 13 and 16 digits.
        ' ====
        ' if the length of the number is less Then 13 digits, then Exit the routine
        If Len(lsNumber) < 13 Then Exit Function

        ' if the length of the number is more Then 16 digits, then Exit the routine
        If Len(lsNumber) > 16 Then Exit Function


        ' ====
        ' The credit card number must start with: 
        '       4 For Visa Cards 
        '       37 For American Express Cards 
        '       5 For MasterCards 
        '       6 For Discover Cards 
        ' ====

        ' Choose action based on Type of card
        Select Case LCase(asCardType)
            ' VISA
            Case "visa", "v"
                ' if first digit Not 4, Exit function
                If Not Left(lsNumber, 1) = "4" Then Exit Function
                ' American Express
            Case "american express", "americanexpress", "american", "ax", "amex"
                ' if first 2 digits Not 37, Exit function
                If Not Left(lsNumber, 2) = "37" Then Exit Function
                ' Mastercard
            Case "mastercard", "master card", "master", "m"
                ' if first digit Not 5, Exit function
                If Not Left(lsNumber, 1) = "5" Then Exit Function
                ' Discover
            Case "discover", "discovercard", "discover card", "d"
                ' if first digit Not 6, Exit function
                If Not Left(lsNumber, 1) = "6" Then Exit Function
            Case "bankcard"
                ' if first digit Not 5, Exit function
                If Not Left(lsNumber, 1) = "5" Then Exit Function
            Case Else
        End Select ' LCase(asCardType)

        ' ====
        ' if the credit card number is less Then 16 digits add zeros
        ' To the beginning to make it 16 digits.
        ' ====
        ' Continue Loop While the length of the number is less Then 16 digits
        While Not Len(lsNumber) = 16

            ' Insert 0 To the beginning of the number
            lsNumber = "0" & lsNumber


        End While ' Not Len(lsNumber) = 16

        ' ====
        ' Multiply Each digit of the credit card number by the corresponding digit of
        ' the mask, and sum the results together.
        ' ====

        ' Loop through Each digit
        For lnPosition = 1 To 16

            ' Parse a digit from a specified position In the number
            lnDigit = Mid(lsNumber, lnPosition, 1)

            ' Determine if we multiply by:
            '       1 (Even)
            '       2 (Odd)
            ' based On the position that we are reading the digit from
            lnMultiplier = 1 + (lnPosition Mod 2)

            ' Calculate the sum by multiplying the digit and the Multiplier
            lnSum = lnDigit * lnMultiplier

            ' (Single digits roll over To remain single. We manually have to Do this.)
            ' if the Sum is 10 or more, subtract 9
            If lnSum > 9 Then lnSum = lnSum - 9

            ' Add the sum To the total of all sums
            lnTotal = lnTotal + lnSum

        Next ' lnPosition

        ' ====
        ' Once all the results are summed divide
        ' by 10, if there is no remainder Then the credit card number is valid.
        ' ====
        IsCreditCard = ((lnTotal Mod 10) = 0)

    End Function ' IsCreditCard




    Private Function VerifyCreditCard(ByVal cardNumber _
           As String) As Boolean


        ' ----- Given a card number, make sure it is valid.
        '       This method uses the Luhn algorithm to verify
        '       the number. This routine assumes that cardNumber
        '       contains only digits.
        Dim counter As Integer
        Dim digitTotal As Integer
        Dim holdValue As Integer
        Dim checkDigit As Integer
        Dim calcDigit As Integer
        Dim useCard As String

        ' ----- Perform some initial checks.
        useCard = Trim(cardNumber)
        'useCard = useCard.Replace("-", "")
        If (IsNumeric(useCard) = False) Then Return False

        ' ----- Separate out the last digit, the check digit.
        '       For cards with an odd number of digits,
        '       prepend with a zero.

        If ((Len(useCard) Mod 2) <> 0) Then _
           useCard = "0" & useCard
        checkDigit = useCard.Substring(Len(useCard) - 1, 1)
        useCard = useCard.Substring(0, Len(useCard) - 1)

        ' ----- Process each digit.
        digitTotal = 0
        For counter = 1 To Len(useCard)
            If ((counter Mod 2) = 1) Then
                ' ----- This is an odd digit position.
                '       Double the number.
                holdValue = CInt(Mid(useCard, counter, 1)) * 2
                If (holdValue > 9) Then
                    ' ----- Process digits (16 becomes 1+6).
                    digitTotal += (holdValue \ 10) + _
                       (holdValue - 10)
                Else
                    digitTotal += holdValue
                End If
            Else
                ' ----- This is an even digit position.
                '       Simply add it.
                digitTotal += CInt(Mid(useCard, counter, 1))
            End If
        Next counter

        ' ----- Calculate the 10's complement of both values.
        calcDigit = 10 - (digitTotal Mod 10)
        If (calcDigit = 10) Then calcDigit = 0
        If (checkDigit = calcDigit) Then Return True Else _
           Return False
    End Function

    Private Function getPaymentEntryXML(Optional ByVal isUsingDPSToken As Boolean = False) As XmlDocument
        Dim tempXMLobj As New XmlDocument
        Try
            tempXMLobj.LoadXml("<PaymentEntryRoot><PaymentEntry/></PaymentEntryRoot>")
            Dim parentNode As XmlNode = tempXMLobj.SelectSingleNode("//PaymentEntry")
            Me.appendPaymentEntryNode(tempXMLobj, parentNode, isUsingDPSToken)
            If tempXMLobj Is Nothing Then
                Return Nothing
            End If
        Catch ex As Exception
            Me.SetErrorMessage(ex.Message)
        End Try
        Return tempXMLobj

    End Function

    ''rev:mia nov11
    ''rev:mia Aug 8 2010
    Private Function addCreditCardPayment(Optional ByVal blnIsReverse As Boolean = False, _
                                          Optional ByVal isUsingDPSToken As Boolean = False) As XmlDocument
        Dim szMethod As String = ""
        Dim szDetails As String = ""
        Dim szCurrency As String = ""
        Dim szCurrencyDesc As String = ""

        Dim szName As String = ""
        Dim szExpDate As String = ""
        Dim szApprovalRef As String = ""

        Dim valMethod As String = ""

        Dim valCurrency As Decimal = 0.0
        Dim valAmount As Decimal = 0.0


        Dim valPaymentDate As String = ""
        Dim valStatus As String = ""
        Dim valSubMethod As String = ""
        Dim valChequeNo As String = ""
        Dim valBank As String = ""
        Dim valBranch As String = ""
        Dim valAccountNo As String = ""
        Dim valPaymentId As String = ""
        Dim valPdtId As String = ""
        Dim PaymentIntegrity As String = ""

        Dim PdtIntegrity As String = ""
        Dim selIndex As Integer
        Dim selIndex2 As Integer
        Dim szMethodType As String = ""

        Dim rootObj As New XmlDocument

        Dim szLocalIdentifier As String = ""
        Dim szOrigCreditCard As String = ""
        Dim szPaymentId As String = ""

        If xmlPaymentmgt.OuterXml = "" Then
            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
        End If
        Try

            szCurrency = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/Currency").InnerText
            szCurrencyDesc = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
            szPaymentId = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/PaymentId").InnerText
            selIndex = Me.ddlMethod.SelectedIndex
            valMethod = Me.ddlMethod.SelectedItem.Value
            methodvalue = valMethod & "," & ddlMethod.Text
            selIndex2 = Me.ddlCardType.SelectedIndex
            szMethodType = Me.ddlCardType.SelectedItem.Text

            If String.IsNullOrEmpty(szMethodType) Then
                MyBase.SetInformationShortMessage("All fields are mandatory")
                Return Nothing
            End If

            ''RECHECK
            ''rev:mia july 27 2010
            If (IsUsingBillingToken()) Then
                valAccountNo = Me.txtCardNumber.Text
                bValidCCardNumber = True
            Else

                If bValidCCardNumber = False Then
                    If nCurrentPmMethodSelIndex = 1 Or Me.ddlMethod.SelectedItem.Text.Contains("Credit Card") Then ''rev:mia oct29
                        bValidCCardNumber = Me.IsCreditCard(Me.ddlCardType.SelectedItem.Text, Me.txtCardNumber.Text) ''Me.VerifyCreditCard(Me.txtCardNumber.Text)
                        If bValidCCardNumber = False Then
                            If Not String.IsNullOrEmpty(Me.litCardNumber.Text) Then
                                Dim cardnumber As String = Me.litCardNumber.Text
                                bValidCCardNumber = Me.IsCreditCard(Me.ddlCardType.SelectedItem.Text, Me.txtCardNumber.Text) ''Me.VerifyCreditCard(cardnumber)
                            End If
                        Else
                            ''rev:mia nov3
                            If Me.txtCardNumber.Text.Contains("XXXX") Then
                                valAccountNo = Me.litCardNumber.Text
                            Else
                                valAccountNo = Me.txtCardNumber.Text
                            End If
                        End If
                    Else
                        ''rev:mia nov3
                        If Me.txtCardNumber.Text.Contains("XXXX") Then
                            valAccountNo = Me.litCardNumber.Text
                        Else
                            valAccountNo = Me.txtCardNumber.Text
                        End If
                    End If
                Else
                    ''rev:mia nov3
                    If Me.txtCardNumber.Text.Contains("XXXX") Then
                        valAccountNo = Me.litCardNumber.Text
                    Else
                        valAccountNo = Me.txtCardNumber.Text
                    End If
                End If

            End If


            If bValidCCardNumber = False AndAlso Me.ddlMethod.SelectedItem.Text.Contains("Credit Card") AndAlso blnIsReverse = False Then ''rev:mia oct29
                MyBase.SetInformationShortMessage("Credit card number may be not valid.")
                bValidCCardNumber = True
                Return Nothing
            End If

            szMethod = Me.ddlMethod.SelectedItem.Text & " - " & szMethodType



            szName = Me.TxtName.Text
            szExpDate = Me.txtExpiryData.Text
            If szExpDate.IndexOf("/") = -1 Then
                szExpDate = szExpDate.Insert(2, "/")
            End If
            szApprovalRef = Me.txtApprovalReference.Text
            valAmount = Me.txtAmount.Text
            valSubMethod = Me.ddlCardType.SelectedItem.Value

            If xmlActivePaymentData.OuterXml = "" AndAlso String.IsNullOrEmpty(litxmlActivePaymentData.Text) = False Then
                xmlActivePaymentData.LoadXml(Server.HtmlDecode(litxmlActivePaymentData.Text))
            End If

            If (xmlActivePaymentData.HasChildNodes = True) Then
                PaymentIntegrity = xmlActivePaymentData.SelectSingleNode("//PaymentIntegrityNo").InnerText
                valPaymentId = xmlActivePaymentData.SelectSingleNode("//PaymentId").InnerText
                valStatus = xmlActivePaymentData.SelectSingleNode("//Status").InnerText
                valPaymentDate = xmlActivePaymentData.SelectSingleNode("//PaymentDate").InnerText
                valPdtId = xmlActivePaymentData.SelectSingleNode("//PdtId").InnerText
                PdtIntegrity = xmlActivePaymentData.SelectSingleNode("//PdtIntegrityNo").InnerText
                szLocalIdentifier = xmlActivePaymentData.SelectSingleNode("//LocalIdentifier").InnerText
                szOrigCreditCard = xmlActivePaymentData.SelectSingleNode("//OrigCard").InnerText
            Else
                PaymentIntegrity = 0
                PdtIntegrity = 0
            End If

            szDetails = szName + ", " + valAccountNo
            If (showCard = 1) Then
                If (updateCardNum <> 1) Then
                    valAccountNo = szOrigCreditCard
                    If valAccountNo = "" Then
                        valAccountNo = Me.txtCardNumber.Text
                    End If
                End If
            End If

            rootObj = Me.getPaymentEntryXML(isUsingDPSToken)
            rootObj.SelectSingleNode("//MethodValue").InnerText = valMethod
            rootObj.SelectSingleNode("//Method").InnerText = szMethod
            rootObj.SelectSingleNode("//Details").InnerText = szDetails
            rootObj.SelectSingleNode("//Amount").InnerText = valAmount + decAmt ''valAmount ''--rev:mia sept16

            rootObj.SelectSingleNode("//OriginalAmt").InnerText = valAmount
            rootObj.SelectSingleNode("//LocalAmount").InnerText = valAmount + decAmt

            rootObj.SelectSingleNode("//IsRemove").InnerText = 0
            rootObj.SelectSingleNode("//SubMethod").InnerText = valSubMethod
            rootObj.SelectSingleNode("//Currency").InnerText = szCurrency
            rootObj.SelectSingleNode("//CurrencyDesc").InnerText = szCurrencyDesc
            rootObj.SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
            rootObj.SelectSingleNode("//PmEntryPaymentId").InnerText = valPaymentId
            rootObj.SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
            rootObj.SelectSingleNode("//Status").InnerText = valStatus
            rootObj.SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier

            


            Dim tnode As XmlNode = rootObj.SelectSingleNode("//PaymentEntryItems")

            tnode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = szName
            tnode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = szPaymentId
            tnode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = szApprovalRef
            tnode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = szExpDate
            tnode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = valChequeNo
            tnode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = valBank
            tnode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = valBranch
            tnode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = valAccountNo
            ''------------------
            ''rev:mia nov7
            ''tnode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = valAmount
            tnode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = (valAmount + decAmt)
            ''------------------


            tnode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
            ''------------------
            ''rev:mia nov7
            tnode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = valAmount
            ''tnode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = valAmount + decAmt
            ''------------------

            tnode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId


            If bDPSRole = True Then
                If (szMethod.IndexOf("Credit") <> -1) Then
                    If Me.rdoCardPresent.Items(0).Selected = False And Me.rdoCardPresent.Items(1).Selected = False Then
                        Me.SetInformationMessage("Please specify the card is present or not")
                        Return Nothing
                    End If

                    If Me.rdoCardPresent.Items(0).Selected = True Then
                        tnode.ChildNodes(0).SelectSingleNode("//IsCrdPre").InnerText = Me.rdoCardPresent.Items(0).Value
                    Else
                        tnode.ChildNodes(0).SelectSingleNode("//IsCrdPre").InnerText = Me.rdoCardPresent.Items(1).Value
                    End If
                End If
            Else
                ''rev:mia Aug 5 2010
                tnode.ChildNodes(0).SelectSingleNode("//IsCrdPre").InnerText = Me.rdoCardPresent.Items(0).Value
            End If

            

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
        Return rootObj
    End Function

#End Region

#Region " Function for Personal Check"

    Private Function addPersonalChequePayment() As XmlDocument

        Dim szMethod, szDetails, szCurrency, szCurrencyDesc As String
        Dim szName, szExpDate, szApprovalRef As String
        Dim valMethod, valAmount As String
        Dim valPaymentDate, valStatus As String
        Dim valSubMethod As String
        Dim valChequeNo, valBank, valBranch, valAccountNo As String
        Dim valPaymentId, valPdtId As String
        Dim PaymentIntegrity, PdtIntegrity As String
        Dim selIndex As String
        Dim szLocalIdentifier As String
        Dim szPaymentId As String

        Dim rootObj As XmlDocument

        Try

            If xmlActivePaymentData.OuterXml = "" AndAlso String.IsNullOrEmpty(litxmlActivePaymentData.Text) = False Then
                xmlActivePaymentData.LoadXml(Server.HtmlDecode(litxmlActivePaymentData.Text))
            End If


            If xmlPaymentmgt.OuterXml = "" Then
                xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
            End If

            If Me.litxmlTravellerChequeDetails.Text <> "" Then
                If Me.xmlTravellerChequeDetails.OuterXml = "" Then
                    Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(Me.litxmlTravellerChequeDetails.Text))
                End If
            End If

            If xmlPaymentMethods.OuterXml = "" Then
                xmlPaymentMethods.LoadXml(Server.HtmlDecode(litxmlPaymentMethods.Text))
            End If


            szCurrency = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
            szCurrencyDesc = xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText
            szPaymentId = xmlPaymentmgt.SelectSingleNode("//PaymentId").InnerText
            selIndex = Me.ddlMethod.SelectedIndex ''frmPaymentMgt.selPaymentType.selectedIndex
            valMethod = Me.ddlMethod.SelectedValue ''frmPaymentMgt.selPaymentType.options[selIndex].value
            valChequeNo = Me.txt_PersonalCheque_ChequeNo.Text ''frmPaymentMgt.txtCheqNo.value
            valBank = Me.txt_PersonalCheque_Bank.Text ''Me.txt_TravellersCheque_Bank.Text ''frmPaymentMgt.txtBank.value
            valBranch = Me.txt_PersonalCheque_Branch.Text ''frmPaymentMgt.txtBranch.value
            valAccountNo = Me.txt_PersonalCheque_AccountNo.Text ''frmPaymentMgt.txtAccount.value
            szName = Me.txt_PersonalCheque_AccountName.Text ''frmPaymentMgt.txtAccountName.value

            ''rev:MIA 07aug - this point to incorrect textbox
            '' valAmount = Me.txtAmount.Text ''frmPaymentMgt.txtAmount.value
            valAmount = Me.txt_PersonalCheque_Amount.Text
            szMethod = Me.ddlMethod.SelectedItem.Text ''frmPaymentMgt.selPaymentType.options[selIndex].innertext
            valSubMethod = xmlPaymentMethods.ChildNodes(0).SelectSingleNode("//ID").InnerText

            If (Me.xmlActivePaymentData.ChildNodes.Count - 1) = 0 Then
                PaymentIntegrity = Me.xmlActivePaymentData.SelectSingleNode("//PaymentIntegrityNo").InnerText
                valPaymentId = Me.xmlActivePaymentData.SelectSingleNode("//PaymentId").InnerText
                valStatus = Me.xmlActivePaymentData.SelectSingleNode("//Status").InnerText
                valPaymentDate = Me.xmlActivePaymentData.SelectSingleNode("//PaymentDate").InnerText
                valPdtId = Me.xmlActivePaymentData.SelectSingleNode("//PdtId").InnerText
                PdtIntegrity = Me.xmlActivePaymentData.SelectSingleNode("//PdtIntegrityNo").InnerText
                szLocalIdentifier = Me.xmlActivePaymentData.SelectSingleNode("//LocalIdentifier").InnerText
            Else
                PaymentIntegrity = 0
                PdtIntegrity = 0
            End If

            szDetails = String.Concat(szName, ", ", valChequeNo, "-", valBank, "-", valBranch, "-", valAccountNo, " ")
            rootObj = getPaymentEntryXML()
            rootObj.SelectSingleNode("//MethodValue").InnerText = valMethod
            rootObj.SelectSingleNode("//Method").InnerText = szMethod
            rootObj.SelectSingleNode("//Details").InnerText = szDetails
            rootObj.SelectSingleNode("//Amount").InnerText = valAmount

            rootObj.SelectSingleNode("//LocalAmount").InnerText = valAmount
            rootObj.SelectSingleNode("//IsRemove").InnerText = 0
            rootObj.SelectSingleNode("//SubMethod").InnerText = valSubMethod
            rootObj.SelectSingleNode("//Currency").InnerText = szCurrency
            rootObj.SelectSingleNode("//CurrencyDesc").InnerText = szCurrencyDesc
            rootObj.SelectSingleNode("//IntegrityNo").InnerText = PaymentIntegrity
            rootObj.SelectSingleNode("//PmEntryPaymentId").InnerText = valPaymentId
            rootObj.SelectSingleNode("//PaymentDate").InnerText = valPaymentDate
            rootObj.SelectSingleNode("//Status").InnerText = valStatus
            rootObj.SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier

            Dim tNode As XmlNode = rootObj.SelectSingleNode("//PaymentEntryItems")

            tNode.ChildNodes(0).SelectSingleNode("//AccountName").InnerText = szName
            tNode.ChildNodes(0).SelectSingleNode("//PmtPtmId").InnerText = szPaymentId
            tNode.ChildNodes(0).SelectSingleNode("//ApprovalRef").InnerText = szApprovalRef
            tNode.ChildNodes(0).SelectSingleNode("//ExpiryDate").InnerText = szExpDate
            tNode.ChildNodes(0).SelectSingleNode("//ChequeNo").InnerText = valChequeNo
            tNode.ChildNodes(0).SelectSingleNode("//Bank").InnerText = valBank
            tNode.ChildNodes(0).SelectSingleNode("//Branch").InnerText = valBranch
            tNode.ChildNodes(0).SelectSingleNode("//AccountNo").InnerText = valAccountNo
            tNode.ChildNodes(0).SelectSingleNode("//PdtAmount").InnerText = valAmount
            tNode.ChildNodes(0).SelectSingleNode("//PdtIntegrityNo").InnerText = PdtIntegrity
            tNode.ChildNodes(0).SelectSingleNode("//PdtLocalAmount").InnerText = valAmount
            tNode.ChildNodes(0).SelectSingleNode("//PdtId").InnerText = valPdtId

        Catch ex As Exception
            MyBase.SetErrorShortMessage("addPersonalChequePayment -> " & ex.Message)
            rootObj = Nothing
        End Try
        Return rootObj
    End Function



#End Region

#Region " Function For Encryption"

    Private Function isAllowedToViewCreditCard() As Boolean

        ''---------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''---------------------------------------------------------
        'If CType(Session("onContinueNoDecryption"), Boolean) = False Then
        '    Dim xmldoc As New System.Xml.XmlDocument
        '    xmldoc.LoadXml(Aurora.Common.Data.ExecuteScalarSP("sp_ViewCreditCard", Me.UserCode))
        '    If xmldoc.SelectSingleNode("isUser") IsNot Nothing Then
        '        If xmldoc.SelectSingleNode("isUser").InnerText = "True" Then Return True
        '    End If
        '    Return False
        'Else
        '    Return False
        'End If

        If CType(Session("onContinueNoDecryption" & "_" & Me.RentalID), Boolean) = False Then
            Dim xmldoc As New System.Xml.XmlDocument
            xmldoc.LoadXml(Aurora.Common.Data.ExecuteScalarSP("sp_ViewCreditCard", Me.UserCode))
            If xmldoc.SelectSingleNode("isUser") IsNot Nothing Then
                If xmldoc.SelectSingleNode("isUser").InnerText = "True" Then Return True
            End If
            Return False
        Else
            Return False
        End If
        ''---------------------------------------------------------

    End Function

    Private Function EncryptCreditCardDataSuccess(ByVal methodType As String, ByVal strxml As String, ByRef returnedXML As String) As Boolean

        If (methodType <> "Credit Card") AndAlso _
           (methodType <> "Debit Card") AndAlso _
           (methodType <> "Imprint") AndAlso _
           (methodType <> "Hold Imprint") AndAlso _
           (methodType <> HOLD_NUMBER) Then ''rev:mia nov3
            Return False
        End If

        Dim xmlTempObject As New XmlDocument
        Dim creditcard As String = ""
        Try


            xmlTempObject.LoadXml(strxml)
            Dim nodes As XmlNodeList = xmlTempObject.SelectNodes("PaymentEntryRoot/PaymentEntry")
            Dim mKeyfile As String = Server.MapPath("~/app_code/" & ConfigurationManager.AppSettings("KEYFILE"))

            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)

            For Each node As XmlNode In nodes
                ''retrieve account number
                creditcard = node.SelectSingleNode("PaymentEntryItems/PmtItem/AccountNo").InnerText
                If creditcard <> "" Then

                    ''If VerifyCreditCard(creditcard) = True Then
                    ''rev:mia nov4
                    If Me.IsCreditCard(Me.ddlCardType.SelectedItem.Text, creditcard) And methodType <> "Debit Card" Then
                        Dim encryptedCreditcard As Byte() = Aurora.Booking.Services.BookingPaymentCardEncryption.EncyrptCardNumber(creditcard, mKeyfile)
                        node.SelectSingleNode("PaymentEntryItems/PmtItem/AccountNo").InnerText = Convert.ToBase64String(encryptedCreditcard)
                    ElseIf methodType = "Debit Card" Then
                        If creditcard.Length > 18 Then
                            Dim encryptedCreditcard As Byte() = Aurora.Booking.Services.BookingPaymentCardEncryption.EncyrptCardNumber(creditcard, mKeyfile)
                            node.SelectSingleNode("PaymentEntryItems/PmtItem/AccountNo").InnerText = Convert.ToBase64String(encryptedCreditcard)
                        End If
                    Else
                        ''rev:mia April 22 2010..this produce unencrypted card when doing reverse
                        '' if its already encyrpted then skip. 
                        ''do nothing

                        ''Dim decryptedCreditcard As String = Aurora.Booking.Services.BookingPaymentCardEncryption.DecryptCardNumber(Convert.FromBase64String(node.SelectSingleNode("PaymentEntryItems/PmtItem/AccountNo").InnerText), mKeyfile, True)
                        ''node.SelectSingleNode("PaymentEntryItems/PmtItem/AccountNo").InnerText = decryptedCreditcard

                    End If
                End If
            Next
            returnedXML = xmlTempObject.OuterXml
            Return True

        Catch ex As Exception
            Logging.LogWarning("", "--------START: EncryptCreditCardDataSuccess MESSAGE-------------------")
            Logging.LogWarning("", vbCrLf & vbCrLf & "ERROR: Failed to Encrypt " & creditcard & vbCrLf & "MESSAGE: " & ex.Message & vbCrLf & "STACKTRACE: " & ex.StackTrace & vbCrLf & vbCrLf)
            Logging.LogWarning("", "--------END: EncryptCreditCardDataSuccess MESSAGE-------------------" & vbCrLf)
            Return False
        Finally
            xmlTempObject = Nothing
        End Try

    End Function

    Private Function DecryptCreditCardDataSuccess(ByVal methodtype As String, ByVal cardNumberEncrypted As String, ByRef cardNumberdecrypted As String, Optional ByVal isAllowed As Boolean = False) As Boolean


        If (methodtype <> "Credit Card") AndAlso _
           (methodtype <> "Debit Card") AndAlso _
           (methodtype <> "Imprint") AndAlso _
           (methodtype <> "Hold Imprint") AndAlso _
           (methodtype <> HOLD_NUMBER) Then ''rev:mia nov3
            Return False
        End If

        Try
            Dim mKeyfile As String = Server.MapPath("~/app_code/" & ConfigurationManager.AppSettings("KEYFILE"))
            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)


            cardNumberdecrypted = Aurora.Booking.Services.BookingPaymentCardEncryption.DecryptCardNumber(Convert.FromBase64String(cardNumberEncrypted), mKeyfile, isAllowed)

            Return True

        Catch ex As Exception
            Logging.LogInformation("PAYMGT", "FAILED: DecryptCreditCardDataSuccess(" & cardNumberEncrypted & ") ---> " & ex.Message)
            Return False
        End Try

    End Function

#End Region

#Region " Function for Online Payment"

    ''rev:mia march 12 2012 - added cvc2
    Private Function OnlinePayment(ByVal vSubMethod As String, _
                                   ByVal vsRntid As String, _
                                   ByVal sCardnumber As String, _
                                   ByVal sCardHolderName As String, _
                                   ByVal sDateExpiry As String, _
                                   ByVal sAmt As String, _
                                   ByVal sCurrency As String, _
                                   ByVal brdcode As String, _
                                   Optional ByVal cardtype As String = "", _
                                   Optional ByVal CVV2 As String = "") As String

        '@sRntId   VARCHAR(64) = NULL,  
        '@sCardType  VARCHAR(64) = NULL,  
        '@sAsFunction BIT   =  0,  
        '@sRetMerchIds VARCHAR(64) OUTPUT  
        Dim xmlTemp As New XmlDocument
        Dim sMerchantRef As String = ""


        Try
            If vSubMethod.Length = 0 Then
                sMerchantRef = vSubMethod
            Else
                ''rev:mia sept16 - change cardtype parameter from cardnumber to cardtype
                xmlTemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("SP_getMerchantReference", vsRntid, cardtype, 0, "")
                If xmlTemp.OuterXml <> "" Then
                    sMerchantRef = xmlTemp.DocumentElement.ChildNodes(0).InnerText
                End If
            End If

            Dim curMoneyType As String
            If UCase(sCurrency) = "NZ" Or sCurrency.IndexOf("nz") <> -1 Then
                GetMACCredentials("nz")
                curMoneyType = "NZD"
            ElseIf UCase(sCurrency) = "AU" Or sCurrency.IndexOf("au") <> -1 Then
                curMoneyType = "AUD"
                GetMACCredentials("au")
            End If

            Dim objXpress As New WSPaymentExpress
            objXpress.ComCode = sComCode
            objXpress.CurrencyType = UCase(sCurrency)
            objXpress.BRDcode = UCase(brdcode)
            objXpress.RetrieveCredential()

            Dim userName As String = ""
            Dim password As String = ""
            If sComCode.ToUpper.Equals("MAC") Then
                userName = Me.MACUsername
                password = Me.MACPassword
            Else
                userName = objXpress.GetUserName
                password = objXpress.GetPassword
            End If
            Dim temp As String = String.Concat("Cardnumber: ", sCardnumber, ", CardHolderName: ", sCardHolderName, ", DateExpiry: ", sDateExpiry, ", Amt: ", sAmt, ", MerchantRef: ", sMerchantRef, ", MoneyType: ", curMoneyType, ",username: ", userName, " ,password: ", password, ", brdcode : ", brdcode, "  ,comcode: ", sComCode)
            Logging.LogWarning(Me.AppendedSaveLog & "PAYMENT SCREEN: OnlinePayment: ", vbCrLf & vbCrLf & "objXpress.Save parameters : ".ToUpper & vbCrLf & temp.ToUpper)
            Dim dpsXref As String = objXpress.Save(sCardnumber, sCardHolderName, sDateExpiry, sAmt, sMerchantRef, curMoneyType, userName, password, CVV2)
            Return dpsXref
        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)
            Logging.LogException("PAYMENT SCREEN: OnlinePayment", ex)
            xmlTemp = Nothing
            Return Nothing
        Finally
            xmlTemp = Nothing
        End Try

    End Function
    Private Function AllowedTomakeOnlinePayment(ByRef shouldReturn As Boolean) As Boolean
        shouldReturn = False

        Dim CHGTOKEN As Boolean = CBool(IsBillingTokenPermittedValue)
        Dim DPSPMT As Boolean = CBool(bDPSRole)
        Dim ISPAYMENT As Boolean = IIf(sMode = 0, True, False)

        ''Oh no...its an imprint
        If (willUseImprint = True) Then
            Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment : ", "willUseImprint is TRUE--> will exit the function and save as a non-dps")
            ''BreakHere()
            shouldReturn = True : Return True
        End If



        Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment : ", _
                                   String.Concat("CHGTOKEN is ", CHGTOKEN, _
                                   " ,DPSPMT is ", DPSPMT, _
                                   " ,ISPAYMENT is ", ISPAYMENT))

        ''------------------ P A Y M E N T----------------------------------------

        ''USER HAS AN OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS ENABLED 
        ''AND ITS A PAYMENT
        ''1.
        If CHGTOKEN And DPSPMT And ISPAYMENT Then ''TESTED

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 1. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''USER SELECTED THE CARDPRESENT = NO, PROCESS WILL BE DPS
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 1. CardPresent is NO")
                ''BreakHere()
                shouldReturn = False : Return False
            End If

        End If

        ''USER HAS AN OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS DISABLED
        ''AND ITS A PAYMENT
        ''2.
        If CHGTOKEN And Not DPSPMT And ISPAYMENT Then ''TESTED

            ''FOR UNKNOWN REASONS, ALL RADIOBUTTON ARE SET TO FALSE,
            ''CHECK IF HASHTABLE HAS VALUE
            If rdoCardPresent.Items(0).Selected = False AndAlso rdoCardPresent.Items(1).Selected = False Then
                ''BreakHere()
                If Not Me.IshashTableHasValue Then
                    Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 2. CardPresent has no default value and Me.IshashTableHasValue has no value")
                    shouldReturn = True : Return True
                Else
                    Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 2. CardPresent has no default value and Me.IshashTableHasValue has a value")
                    shouldReturn = False : Return False
                End If
            End If

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 2. CardPresent is YES ")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''USER SELECTED THE BILLING TOKEN WITH A VALUE STORED IN THE HASH
            If rdoCardPresent.Items(1).Selected = True And Me.IshashTableHasValue Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 2. CardPresent is NO and Me.IshashTableHasValue has a value")
                ''BreakHere()
                shouldReturn = False : Return False
            End If

            '' IF THERE IS A VALUE STORED IN HASH AND USER DECIDED AGAIN TO 
            ''MAKE A PAYMENT USING CARD PRESENT = y
            ''todo


            ''USER SELECTED THE CARDPRESENT = NO, OVERRIDE IT TO PROCESS NON-DPS
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 2. CardPresent is NO")
                ''BreakHere()
                shouldReturn = True : Return True
            End If
        End If

        ''USER HAS AN NO OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS NOT DISABLED
        ''AND ITS A PAYMENT..
        ''3.
        If Not CHGTOKEN And DPSPMT And ISPAYMENT Then

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 3. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''USER SELECTED THE CARDPRESENT = NO, OVERRIDE IT TO PROCESS NON-DPS
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 3. CardPresent is NO")
                ''BreakHere()
                shouldReturn = False : Return False
            End If
        End If

        ''USER HAS AN NO OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS NOT DISABLED
        ''AND ITS A PAYMENT ...
        ''4.
        BreakHere()
        If Not CHGTOKEN And Not DPSPMT And ISPAYMENT Then

            If Me.IshashTableHasValue Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 4. IshashTableHasValue has a value")
                shouldReturn = False : Return False
            End If

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 4. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''USER SELECTED THE CARDPRESENT = NO, OVERRIDE IT TO PROCESS NON-DPS
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "PAYMENT 4. CardPresent is NO")
                ''BreakHere()
                shouldReturn = False : Return False
            End If
        End If


        ''------------------ R E F U N D----------------------------------------

        ''USER HAS AN OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS DISABLED WHEN REFUND
        ''AND ITS A NON PAYMENT
        ''1.
        If CHGTOKEN And DPSPMT And Not ISPAYMENT Then ''TESTED

            ''IF DURING REFUND, USE CHOOSE YES, AND NOT PINPAD THEN
            ''CONSIDER THIS AN OFFLINE PAYMENT
            If willUseEFTPOS = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 1. willUseEFTPOS is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''USER SELECTED BILLING ID, SO RADIO BUTTON 1 WILL BE CHECK
            ''OR DEFAULT PAYMENT IS AN ONLINE THEN RADIO BUTTON 1 IS CHECK
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 1. CardPresent is NO")
                ''BreakHere()
                shouldReturn = False : Return False
            End If

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 1. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

        End If

        ''USER HAS AN OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS DISABLED WHEN REFUND
        ''AND ITS A NON PAYMENT, 
        ''2.
        If CHGTOKEN And Not DPSPMT And Not ISPAYMENT Then ''TESTED

            ''IF DURING REFUND, USE CHOOSE YES, AND NOT PINPAD THEN
            ''CONSIDER THIS AN OFFLINE PAYMENT
            If willUseEFTPOS = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 2. willUseEFTPOS is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''USER SELECTED BILLING ID, SO RADIO BUTTON 1 WILL BE CHECK
            ''OR DEFAULT PAYMENT IS AN ONLINE THEN RADIO BUTTON 1 IS CHECK
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 2. CardPresent is NO")
                ''BreakHere()
                shouldReturn = False : Return False
            End If

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 2. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If


            ''FOR UNKNOWN REASONS, ALL RADIOBUTTON ARE SET TO FALSE,
            If rdoCardPresent.Items(0).Selected = False AndAlso rdoCardPresent.Items(1).Selected = False Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 2. CardPresent has no value")
                ''BreakHere()
                shouldReturn = False : Return False
            End If
        End If

        ''USER HAS AN OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS DISABLED WHEN REFUND
        ''AND ITS A NON PAYMENT,
        ''3.
        If Not CHGTOKEN And DPSPMT And Not ISPAYMENT Then

            ''IF DURING REFUND, USE CHOOSE YES, AND NOT PINPAD THEN
            ''CONSIDER THIS AN OFFLINE PAYMENT
            If willUseEFTPOS = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 3. willUseEFTPOS is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''DEFAULT PAYMENT IS AN ONLINE THEN RADIO BUTTON 1 IS CHECK
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 3. CardPresent is NO")
                ''BreakHere()
                shouldReturn = False : Return False
            End If

            ''USER SELECTED THE CARDPRESENT = Y, PROCESS WILL BE NON DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 3. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

        End If


        ''USER HAS AN OPTION TO USE THE BILLINGTOKEN AND RADIO BUTTON IS DISABLED WHEN REFUND
        ''AND ITS A NON PAYMENT, 
        ''4.
        If Not CHGTOKEN And Not DPSPMT And Not ISPAYMENT Then

            ''IF DURING REFUND, USE CHOOSE YES, AND NOT PINPAD THEN
            ''CONSIDER THIS AN OFFLINE PAYMENT
            If willUseEFTPOS = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 4. willUseEFTPOS is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''OVERRIDE TO USE NON-DPS
            If rdoCardPresent.Items(1).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 4. CardPresent is NO")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

            ''OVERRIDE TO USE NON-DPS
            If rdoCardPresent.Items(0).Selected = True Then
                Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "REFUND 4. CardPresent is YES")
                ''BreakHere()
                shouldReturn = True : Return True
            End If

        End If
        ''BreakHere()
        Logging.LogWarning("PAYMENT SCREEN: AllowedTomakeOnlinePayment :", "Returning FALSE")
        Return False
    End Function

    ''rev:mia nov3 -added optional msg 
    ''rev:mia march 12 2012 - added cvc2
    Private Function makeOnlinePayment(ByVal xmlString As String, _
                                       ByVal blnIsReverse As Boolean, _
                                       Optional ByRef bmsgWarning As String = "", _
                                       Optional ByVal CVV2 As String = "") As Boolean
        BreakHere()
        Dim CHGTOKEN As Boolean = IsBillingTokenSaving()
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()

        If (bDPSRole = False And Not CHGTOKEN) Then
            If (IshashTableHasValue() = True And CSREFTPMT) Then
                Logging.LogWarning("PAYMENT SCREEN: makeOnlinePayment : ", _
                               "IshashTableHasValue() that contains Billing token has a value")
                ''just continue
            Else
                Logging.LogWarning("PAYMENT SCREEN: makeOnlinePayment : ", _
                               String.Concat("bDPSRole is ", bDPSRole, " for User ", Me.UserCode, "NOTE: This will not proceed to execute codes of makeOnlinePayment and will return TRUE", " IsBillingTokenSaving() : FALSE"))
                Return True
            End If

        End If

        Dim lShouldReturn As Boolean
        Dim lResult As Boolean = AllowedTomakeOnlinePayment(lShouldReturn)
        If lShouldReturn Then
            If (Me.IshashTableHasValue) Then
                ''continue..user decided to split payment
            Else
                Return lResult
            End If

        End If


        Dim xmlTemp As New XmlDocument
        Dim xmlTempString As String = ""
        ''rev:mia nov3
        Dim dpsRefundError As String = ""
        Try


            xmlTemp.LoadXml(xmlString)
            Dim NoOfPayment As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes.Count - 1
            Dim TrRefNum As String = ""
            ''rev:mia july 27 2010
            Dim billingtoken As String = ""
           
            For i As Integer = 0 To NoOfPayment
                Dim PmtMethod As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("Method").InnerText
                If Not xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/DPSBillingToken") Is Nothing Then
                    billingtoken = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/DPSBillingToken").InnerText
                End If
                If (PmtMethod.IndexOf("Credit") <> -1) Then
                    ''rev:aug 8 2010
                    ''If (xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/IsCrdPre").InnerText = "0" Or (CHGTOKEN Or (IshashTableHasValue() = True And CSREFTPMT))) Then
                    If (xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/IsCrdPre").InnerText = "0" Or IshashTableHasValue() = True) Then

                        If (sComCode = "THL") Then
                            ''rev:mia dec19
                            If sRntCtyCode = "NZ" Then
                                If GetCurrencyBasedOnTravelDestination Then
                                    If DefaultCurrencyDesc.Contains("NZD") Then
                                        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                                        sRntCtyCode = "NZ"

                                        ''------------------------------------------------------------------------------------------------------
                                        ''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:NZ USER that is transacting payment and booking in AU
                                        ''------------------------------------------------------------------------------------------------------
                                    ElseIf (Not String.IsNullOrEmpty(billingtoken) And DefaultCurrencyDesc.Contains("AUD")) Then
                                        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                                        sRntCtyCode = "AU"
                                    Else
                                        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                                        sRntCtyCode = "AU"
                                    End If
                                Else
                                    xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                                    sRntCtyCode = "NZ"
                                End If
                            Else
                                If GetCurrencyBasedOnTravelDestination Then
                                    If sRntCtyCode = "AU" Then
                                        If DefaultCurrencyDesc.Contains("NZD") Then

                                            ''------------------------------------------------------------------------------------------------------
                                            ''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:AU USER that is transacting payment and booking in NZ
                                            ''------------------------------------------------------------------------------------------------------
                                            If (Not String.IsNullOrEmpty(billingtoken) And DefaultCurrencyDesc.Contains("NZD")) Then
                                                xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                                                sRntCtyCode = "NZ"
                                            Else
                                                xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                                                sRntCtyCode = "NZ"
                                            End If

                                            
                                        Else
                                            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                                            sRntCtyCode = "AU"
                                        End If
                                    End If
                                Else
                                    xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                                    sRntCtyCode = "AU"
                                End If
                            End If

                        ElseIf sComCode = "KXS" Then
                            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPK"
                            ' RKS MOD: 21-Sep-2009 for MAC.COM
                        ElseIf sComCode = "MAC" Then
                            ''rev:mia dec19
                            If sRntCtyCode = "NZ" Then
                                If GetCurrencyBasedOnTravelDestination Then
                                    If DefaultCurrencyDesc.Contains("NZD") Then
                                        sRntCtyCode = "NZ"
                                    Else
                                        sRntCtyCode = "AU"
                                    End If
                                Else
                                    sRntCtyCode = "NZ"
                                End If
                            Else
                                If GetCurrencyBasedOnTravelDestination Then
                                    If sRntCtyCode = "AU" Then
                                        If DefaultCurrencyDesc.Contains("NZD") Then
                                            sRntCtyCode = "NZ"
                                        Else
                                            sRntCtyCode = "AU"
                                        End If
                                    End If
                                Else
                                    sRntCtyCode = "AU"
                                End If
                            End If
                            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPM"
                        End If

                        Logging.LogWarning("PAYMENT SCREEN: MakeOnlinePayment: ", "UserLocCode for user " & Me.UserCode & " will be " & xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText)


                        Dim sCardNum As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/AccountNo").InnerText
                        Dim sName As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/AccountName").InnerText
                        Dim sExpDate As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/ExpiryDate").InnerText.Replace("/", "")
                        Dim sAmt As Decimal

                        If blnIsReverse = True Then
                            sAmt = (xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("LocalAmount").InnerText)
                            If sAmt < 0 Then
                                sAmt = sAmt * -1
                            End If
                        Else
                            sAmt = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("LocalAmount").InnerText
                        End If

                        Dim sCurr As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("CurrencyDesc").InnerText
                        Dim SubMethod As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("SubMethod").InnerText

                       
                        ''rev:mia sept 30 2010 - 
                        Dim dpsTxnRef As String = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/TxnRef").InnerText
                        If Not String.IsNullOrEmpty(billingtoken) Then ''And IsBillingTokenSaving() Then
                            Dim txntype As String = IIf(CDec(sAmt) < 0, "Refund", "Purchase")
                            Dim data As New PaymentExpressWSData
                            With data
                                .Amount = FormatWSAmount(sAmt).Replace("-", "")
                                .DpsBillingId = billingtoken
                                .TxnType = txntype
                                .TxnRef = Me.RentalID
                                .DpsTxnRef = dpsTxnRef

                            End With
                            xmlTempString = Me.SubmitTransaction(data, sComCode, sRntCtyCode, sBrdCode)


                        Else

                            ''rev:mia sept16
                            Dim param1_CardNumber As String = sCardNum
                            Dim param1_CardType As String = ViewState("CardType") ''sCardNum
                            Dim param2_sname As String = sName
                            Dim param3_sexpdate As String = sExpDate
                            Dim param4_sAmt As String = sAmt '' String.Format("{0:N}", sAmt)
                            Dim param5_sCtyCode As String = sRntCtyCode
                            Dim param6_Submethod As String = SubMethod
                            Dim param7_RentalId As String = RntId
                            Dim param8_BrdCode As String = sBrdCode
                            Dim param9_ComCode As String = sComCode

                            ''rev:mia nov27
                            If param4_sAmt.Contains(".") = False Then
                                param4_sAmt = param4_sAmt & ".00"
                            ElseIf param4_sAmt.Contains(".") = True Then
                                Dim decnumber As String = param4_sAmt.Split(".")(1)
                                If Not String.IsNullOrEmpty(decnumber) Then
                                    If decnumber.Length = 1 Then
                                        param4_sAmt = param4_sAmt & "0"
                                    End If
                                End If
                            End If

                            xmlTempString = OnlinePayment(SubMethod, _
                                                                       param7_RentalId, _
                                                                       param1_CardNumber, _
                                                                       param2_sname, _
                                                                       param3_sexpdate, _
                                                                       param4_sAmt, _
                                                                       param5_sCtyCode, _
                                                                       param8_BrdCode, _
                                                                       param1_CardType, _
                                                                       CVV2) ''--''rev:mia sept16

                        End If ''If IsUsingBillingToken() Then

                        Logging.LogWarning(Me.AppendedSaveLog & "PAYMENT SCREEN: makeOnlinePayment: ", vbCrLf & vbCrLf & "OnlinePayment function returned: ".ToUpper & vbCrLf & xmlTempString)
                        Dim xmlTempObject As New XmlDocument
                        xmlTempObject.LoadXml(xmlTempString)

                        Dim Status As String = xmlTempObject.DocumentElement.SelectSingleNode("status").InnerText
                        Dim msg As String = xmlTempObject.DocumentElement.SelectSingleNode("msg").InnerText
                        Dim sAuthCode As String = xmlTempObject.DocumentElement.SelectSingleNode("AuthCode").InnerText
                        Dim sDpsTxnRef As String = xmlTempObject.DocumentElement.SelectSingleNode("DpsTxnRef").InnerText

                        xmlTempNewXML.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/AuthCode").InnerText = sAuthCode
                        xmlTempNewXML.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems/PmtItem/TxnRef").InnerText = sDpsTxnRef

                        If (Status = "ERROR") Then
                            bmsgWarning = msg
                            MyBase.SetErrorShortMessage(msg)
                            If (TrRefNum <> "") Then

                                Dim TrArr As String() = TrRefNum.Split(",")
                                For j As Integer = 0 To TrArr.Length - 1
                                    sAmt = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(j).SelectSingleNode("PaymentEntryItems/PmtItem/PdtAmount").InnerText
                                    sCurr = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(j).SelectSingleNode("CurrencyDesc").InnerText
                                    SubMethod = xmlTemp.SelectSingleNode("//Root/PaymentEntryRoot").ChildNodes(j).SelectSingleNode("SubMethod").InnerText

                                    If (TrRefNum.Contains(",") <> -1) Then
                                        ''rev:mia march 12 2012 -- Added CVC2
                                        If (makeOnlineRefundPayment(TrArr(j), sAmt, sCurr, SubMethod, "", "M", dpsRefundError, CVV2) = False) Then
                                            Throw New Exception(dpsRefundError)
                                        Else
                                            xmlTempNewXML.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(j).SelectSingleNode("PaymentEntryItems/PmtItem/AuthCode").InnerText = sAuthCode
                                            xmlTempNewXML.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(j).SelectSingleNode("PaymentEntryItems/PmtItem/TxnRef").InnerText = ""
                                        End If
                                    Else
                                        ''rev:mia march 12 2012 -- Added CVC2
                                        If (makeOnlineRefundPayment(TrRefNum, sAmt, sCurr, "", "", "M", dpsRefundError, CVV2) = False) Then
                                            Throw New Exception(dpsRefundError)
                                        Else
                                            xmlTempNewXML.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(j).SelectSingleNode("PaymentEntry/PaymentEntryItems/PmtItem/AuthCode").InnerText = sAuthCode
                                            xmlTempNewXML.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(j).SelectSingleNode("PaymentEntry/PaymentEntryItems/PmtItem/TxnRef").InnerText = ""
                                        End If

                                    End If
                                Next

                                Return False
                            Else
                                Return False
                            End If
                        Else
                            If (TrRefNum <> "") Then
                                TrRefNum = TrRefNum + "," + sDpsTxnRef
                            Else
                                TrRefNum = TrRefNum + sDpsTxnRef
                            End If
                        End If

                    Else
                        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = sOrigLocCode
                    End If

                Else
                    xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = sOrigLocCode
                End If ''If (PmtMethod.IndexOf("Credit") <> -1)
            Next

        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)
            Logging.LogException("PAYMENT SCREEN: makeOnlinePayment", ex)
            Return False
        End Try
        Return True
    End Function

#End Region

#Region " Function for Reversal Payment"
    ''rev:mia march 12 2012 -- Added CVC2
    Private Function OnlineRefundPayment(ByVal param1_TRREFNUM As String, _
                                         ByVal param2_AMT As String, _
                                         ByVal param3_CURR As String, _
                                         ByVal param4_SUBMETHOD As String, _
                                         ByVal param5_RENTID As String, _
                                         ByVal param6_MERCHANTREF As String, _
                                         ByVal param8_DPSBRDCODE As String, _
                                         ByVal param9_COMCODE As String, _
                                         Optional ByVal CVV2 As String = "") As String
        Dim xmlTemp As New XmlDocument
        Dim merchantreference As String = ""
        Dim dpsXref As String = ""
        Dim curmoneytype As String = ""
        Dim tempLog As String = ""

        Try
            If param6_MERCHANTREF.Length = 0 Then
                merchantreference = param6_MERCHANTREF
            Else
                xmlTemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("SP_getMerchantReference", param5_RENTID, param4_SUBMETHOD, 0, "")
                If xmlTemp.OuterXml <> "" Then
                    merchantreference = xmlTemp.DocumentElement.ChildNodes(0).InnerText
                End If
            End If


            If UCase(param3_CURR) = "NZ" Or UCase(param3_CURR) = "NZD" Then
                curmoneytype = "NZD"
            ElseIf UCase(param3_CURR) = "AU" Or UCase(param3_CURR) = "AUD" Then
                ' RKS MOD: 21-Sep-2009 for MAC.COM
                curmoneytype = "AUD"
            End If

            Dim objXpress As New WSPaymentExpress
            objXpress.ComCode = sComCode
            objXpress.CurrencyType = IIf(UCase(param3_CURR).IndexOf("NZ") <> -1, "NZ", "AU")
            objXpress.BRDcode = UCase(param8_DPSBRDCODE)
            objXpress.RetrieveCredential()

            GetMACCredentials(IIf(UCase(param3_CURR).IndexOf("NZ") <> -1, "nz", "au"))
            Dim userName As String = ""
            Dim password As String = ""
            If sComCode.ToUpper.Equals("MAC") Then
                userName = Me.MACUsername
                password = Me.MACPassword
            Else
                userName = objXpress.GetUserName
                password = objXpress.GetPassword
            End If
            dpsXref = objXpress.Reverse(param1_TRREFNUM, param2_AMT, merchantreference, curmoneytype, userName, password, CVV2)
            tempLog = String.Concat("RESULT :", dpsXref, ", param1_TRREFNUM: ", param1_TRREFNUM, _
                                                                               ", param2_AMT: ", param2_AMT, _
                                                                               ", param3_CURR:", param3_CURR, _
                                                                               ", param4_SUBMETHOD: ", param4_SUBMETHOD, _
                                                                               ", param5_RENTID: ", param5_RENTID, _
                                                                               ", param6_MERCHANTREF: ", param6_MERCHANTREF, _
                                                                               ", param8_DPSBRDCODE: ", param8_DPSBRDCODE, _
                                                                               ", param9_COMCODE: ", param9_COMCODE, _
                                                                               ", curmoneytype".ToString, curmoneytype, _
                                                                               ", currencytype".ToString, IIf(UCase(param3_CURR).IndexOf("NZ") <> -1, "NZ", "AU"))
        Catch ex As Exception
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN: OnlineRefundPayment :", vbCrLf & vbCrLf & "objXpress.Reverse :".ToUpper & vbCrLf & tempLog.ToUpper)
            MyBase.SetErrorShortMessage(ex.Message)
            xmlTemp = Nothing
            Return Nothing
        Finally
            xmlTemp = Nothing
        End Try

        Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN: OnlineRefundPayment :", vbCrLf & vbCrLf & "objXpress.Reverse :".ToUpper & vbCrLf & tempLog.ToUpper)
        Return dpsXref
    End Function

    ''rev:mia march 12 2012 -- Added CVC2
    Private Function makeOnlineRefundPayment(ByVal TrRefNum As String, _
                                             ByVal sAmt As String, _
                                             ByVal sCurr As String, _
                                             ByVal SubMethod As String, _
                                             ByVal sMerchRef As String, _
                                             ByVal DpsBrdCode As String, _
                                             Optional ByRef dpsWarningMsg As String = "", _
                                             Optional ByVal CVV2 As String = "") As Boolean

        BreakHere()
        Dim CHGTOKEN As Boolean = IsBillingTokenSaving()
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()


        If (bDPSRole = False And Not CHGTOKEN) Then
            If String.IsNullOrEmpty(TrRefNum) Then
                Logging.LogWarning("PAYMENT SCREEN: makeOnlineRefundPayment : ", "Transaction is empty")
                Return True
            End If

            If CHGTOKEN And Not CSREFTPMT Then
                ''just continue
            ElseIf Not CHGTOKEN And CSREFTPMT Then
                ''just continue
            ElseIf CHGTOKEN And CSREFTPMT Then
                ''just continue
            Else
                Logging.LogWarning("PAYMENT SCREEN: makeOnlineRefundPayment : ", String.Concat("bDPSRole is ", bDPSRole, " for User ", Me.UserCode, "NOTE: This will not proceed to execute codes of makeOnlineRefundPayment and will return TRUE"))
                Return True
            End If
        End If

        Dim lShouldReturn As Boolean
        Dim lResult As Boolean = AllowedTomakeOnlinePayment(lShouldReturn)
        If lShouldReturn Then
            If Not String.IsNullOrEmpty(TrRefNum) Then
                ''just continue
            Else
                Return lResult
            End If

        End If

        ''rev:mia nov27
        If sAmt.Contains(".") = False Then
            sAmt = sAmt & ".00"
        ElseIf sAmt.Contains(".") = True Then
            Dim decnumber As String = sAmt.Split(".")(1)
            If Not String.IsNullOrEmpty(decnumber) Then
                If decnumber.Length = 1 Then
                    sAmt = sAmt & "0"
                End If
            End If
        End If

        Dim xmlTemp As New XmlDocument

        Try
            If (sComCode = "THL") Then

                If GetCurrencyBasedOnTravelDestination Then
                    If sRntCtyCode = "NZ" Then
                        If DefaultCurrencyDesc.Contains("NZD") Then
                            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                        Else
                            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                        End If
                    Else
                        If sRntCtyCode = "AU" Then
                            If DefaultCurrencyDesc.Contains("NZD") Then
                                xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                            Else
                                xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                            End If

                        End If
                    End If
                Else

                    If sRntCtyCode = "NZ" Then
                        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                    Else
                        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                    End If

                End If

                ''--------------------------------------------------------------------------------
                ''rev:mia nov26 orig codes
                ''--------------------------------------------------------------------------------
                'If sRntCtyCode = "NZ" Then
                '    xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                'Else
                '    xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                'End If
                ''--------------------------------------------------------------------------------
                ''rev:mia nov26
                'If sRntCtyCode = "NZ" Then
                '    If DefaultCurrencyDesc.Contains("NZD") Then
                '        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                '    Else
                '        xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                '    End If
                'Else
                '    If sRntCtyCode = "AU" Then
                '        If DefaultCurrencyDesc.Contains("NZD") Then
                '            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPZ"
                '        Else
                '            xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPA"
                '        End If

                '    End If
                'End If

            ElseIf sComCode = "KXS" Then
                xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPK"
            ElseIf sComCode = "MAC" Then
                xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText = "DPM"
            End If

            Dim xmltempstring As String
            BreakHere()
            Dim billingtokensaving As Boolean = IsBillingTokenSaving()

            ''If IsUsingBillingToken() And billingtokensaving Or (TrRefNum <> "" And billingtokensaving) Then
            If IsUsingBillingToken() And billingtokensaving Then
                Dim data As New PaymentExpressWSData
                With data
                    .Amount = FormatWSAmount(sAmt)
                    .DpsBillingId = Me.BillingTokenSelect.SelectedValue
                    .TxnType = "Refund"


                    .TxnRef = Me.RentalID
                    .DpsTxnRef = TrRefNum
                End With
                xmltempstring = SubmitTransaction(data, sComCode, sRntCtyCode, DpsBrdCode)
            Else

                Logging.LogWarning("PAYMENT SCREEN: MakeOnlineRefundPayment: ", "UserLocde for user " & Me.UserCode & " will be " & xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText)
                xmltempstring = OnlineRefundPayment(TrRefNum, _
                                                                  sAmt, _
                                                                  sCurr, _
                                                                  SubMethod, _
                                                                  RntId, _
                                                                  sMerchRef, _
                                                                  DpsBrdCode, _
                                                                  sComCode, _
                                                                  CVV2) ''rev:mia march 12 2012 -- Added CVC2

            End If ''If IsUsingBillingToken() Then

            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN: MakeOnlineRefundPayment: ", vbCrLf & vbCrLf & "OnlineRefundPayment function returned :".ToUpper & vbCrLf & xmltempstring & vbCrLf & vbCrLf)
            xmlTemp.LoadXml(xmltempstring)

            Dim Status As String = xmlTemp.DocumentElement.SelectSingleNode("status").InnerText
            Dim msg As String = xmlTemp.DocumentElement.SelectSingleNode("msg").InnerText

            If (Status = "ERROR") Then
                dpsWarningMsg = msg
                MyBase.SetErrorShortMessage(msg)
                Return False
            Else
                sAuthCode = xmlTemp.DocumentElement.SelectSingleNode("AuthCode").InnerText
                sDpsTxnRef = xmlTemp.DocumentElement.SelectSingleNode("DpsTxnRef").InnerText
            End If
        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)
            Return False
        End Try
        Return True
    End Function
#End Region

#Region " Functions"

    Private Function notExpired() As Boolean
        Dim todaysMonth As String = Now.Month
        Dim todaysYear As String = Now.Year
        ''validate expiry date
        ''Credit card will be expired at the time of CheckOut of the last rental selected for Payment.". 
        If String.IsNullOrEmpty(txtExpiryData.Text) Then
            Me.SetInformationShortMessage("Invalid Expiry date")
            Me.txtExpiryData.Focus()
            Return False
            Return False
        End If
        If CInt(Me.txtExpiryData.Text.Split("/")(0).ToString) > 12 Then
            Me.SetInformationShortMessage("Invalid Expiry date")
            Me.txtExpiryData.Focus()
            Return False
        End If

        If CInt(Me.txtExpiryData.Text.Split("/")(1).ToString) < CInt(todaysYear.Substring(2, 2)) Then
            Me.SetInformationShortMessage("Credit card will be expired at the time of CheckOut of the last rental selected for Payment.")
            Me.txtExpiryData.Focus()
            Return False
        End If

        If CInt(todaysYear.Substring(2, 2)) > CInt(Me.txtExpiryData.Text.Split("/")(1).ToString) Then
            Me.SetInformationShortMessage("Credit card will be expired at the time of CheckOut of the last rental selected for Payment.")
            Me.txtExpiryData.Focus()
            Return False
        End If

        If CInt(todaysMonth) > CInt(Me.txtExpiryData.Text.Split("/")(0).ToString) AndAlso (CInt(todaysYear.Substring(2, 2)) >= CInt(Me.txtExpiryData.Text.Split("/")(1).ToString)) Then
            Me.SetInformationShortMessage("Credit card will be expired at the time of CheckOut of the last rental selected for Payment.")
            Me.txtExpiryData.Focus()
            Return False
        End If
        Return True
    End Function
    Private Function IsPageValid(ByVal validationGroup) As Boolean
        Dim validator As BaseValidator
        If Page.IsValid = False Then
            For Each validator In Page.GetValidators(validationGroup)

                If validator.ControlToValidate = "ddlCardType" AndAlso Not IsUsingBillingToken() Then
                    If Me.ddlCardType.Visible = True Then
                        If String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text) = True Then
                            validator.IsValid = False
                            validator.FindControl("ddlCardType").Focus()
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)
                            Return False
                        End If
                    End If
                End If

                If Not validator.IsValid Then
                    validator.FindControl(validator.ControlToValidate).Focus()
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)
                    Return False
                End If
            Next

        Else
            For Each validator In Page.GetValidators(validationGroup)
                If validator.ControlToValidate = "ddlCardType" AndAlso Not IsUsingBillingToken() Then
                    If Me.ddlCardType.Visible = True Then
                        If String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text) = True Then
                            validator.IsValid = False
                            validator.FindControl("ddlCardType").Focus()
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)
                            Return False
                        End If
                    End If
                Else
                    If (IsUsingBillingToken() And validator.ControlToValidate = "txtApprovalReference") Then
                        If String.IsNullOrEmpty(txtApprovalReference.Text) Then
                            validator.IsValid = False
                            validator.FindControl("txtApprovalReference").Focus()
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)
                            Return False
                        End If
                    End If
                End If
            Next

        End If


        If Me.rdoCardPresent.Visible = True Then
            If Me.rdoCardPresent.Items(0).Selected = False AndAlso Me.rdoCardPresent.Items(1).Selected = False Then
                Me.SetInformationShortMessage("Please specify whether credit card is present")
                Me.rdoCardPresent.Focus()
                Return False
            End If

            ''#537 -  Maintain Payment page - No validation of expired dates for CC payments
            If Me.pnlPayment_CreditCardAndDebitCard.Visible = True Then
                ''REV:mia 569/570 Maintain Payment page - user can enter a date prior to the CO date for an EFTPOS payment
                If notExpired() = False Then
                    Return False
                End If
            Else
                ''REV:mia 569 Maintain Payment page - user can enter a date prior to the CO date for an EFTPOS payment
                If notExpired() = False Then
                    Return False
                End If

            End If
        Else

            If Me.pnlPayment_CreditCardAndDebitCard.Visible = True Then
                ''REV:mia 569/570 Maintain Payment page - user can enter a date prior to the CO date for an EFTPOS payment
                If notExpired() = False Then
                    Return False
                End If
            End If

        End If

        Return True
    End Function

    Private Sub WrapClearing()
        Me.ClearPersonalCheck()
        Me.ClearTravellersCheck()
        Me.clearCash()

        ''rev:mia nov7
        If pnlPaymentEntriesHeader.Visible = True And Not IsUsingBillingToken() Then
            Me.ClearCreditCardAndDebit()
        End If


        repTravellersCheque.DataSource = Nothing
        repTravellersCheque.DataBind()

        ''rev:mia nov7
        If pnlPaymentEntriesHeader.Visible = True Then
            RepPaymentEntries.DataSource = Nothing
            RepPaymentEntries.DataBind()
            ''rev:june 30 2010
            EFTPOSallowedAndDisplayMSG(True)
        Else
            ''  Dim btnremove As Button = CType(Me.RepPaymentEntries.Controls(0).FindControl("btnremove"), Button)
        End If

        repTravellersCheque.DataSource = Nothing
        repTravellersCheque.DataBind()
    End Sub

    Private Function ChangeIsValidOperation(ByVal nType As Integer) As Boolean
        If paymentType = -1 Then
            Return True
        End If
        Dim breturn As Boolean = False
        Select Case nType
            Case 0, 2, 3, 4
                Return breturn
            Case 1
                If (paymentType = 0 Or paymentType = 1) Then
                    Return True
                End If
            Case Else
                Return breturn
        End Select
    End Function

    Private Function GetUserDefaultLocation(ByVal userID As String, ByVal sRntID As String) As String

        Dim retString As String = ""
        Dim xmlString As String = ""
        Dim tempXMLobject As New XmlDocument

        Try

            ''@sUserCode		VARCHAR(12)	= '' ,
            ''@sRntId			VARCHAR(64) = ''
            xmlString = Aurora.Common.Data.ExecuteScalarSP("GEN_getDefValuesForUser", userID, sRntID)
            tempXMLobject.LoadXml(xmlString)

            If tempXMLobject.ChildNodes(0).ChildNodes(0).Name = "Error" Then
                If tempXMLobject.SelectSingleNode("//Error/ErrDescription").InnerText = "No Records Found" Then
                    Logging.LogWarning("PAYMENT SCREEN: GetUserDefaultLocation: ", "NO RECORDS FOUND FOR USERID " & userID & " AND RENTALID " & sRntID)
                    Return ""
                End If

            Else
                retString = tempXMLobject.ChildNodes(0).SelectSingleNode("//UsrLoc").InnerText
                sRntCtyCode = tempXMLobject.ChildNodes(0).SelectSingleNode("//CtyCode").InnerText
                sBrdCode = tempXMLobject.ChildNodes(0).SelectSingleNode("//BrdCode").InnerText
                Logging.LogWarning("PAYMENT SCREEN: GetUserDefaultLocation: ", String.Concat(" USERID: ", userID, ", RENTALID: ", sRntID, ", RETSTRING: ", retString, ", sRntCtyCode: ".ToUpper, sRntCtyCode, ", sBrdCode: ".ToUpper, sBrdCode))
            End If

        Catch ex As Exception
            tempXMLobject = Nothing
            Return retString
        Finally
            tempXMLobject = Nothing
        End Try

        Return retString
    End Function

    Private Function GetSettings() As String

        Dim xmlString As String = ""
        Dim tempXMLobject As New XmlDocument

        Try
            xmlString = Aurora.Common.Data.ExecuteScalarSP("paym_getSettings", MyBase.UserCode)
            If xmlString <> "" Then
                xmlString = String.Concat("<root>", xmlString, "</root>")
            End If
        Catch ex As Exception
            tempXMLobject = Nothing
            Return xmlString
        Finally
            tempXMLobject = Nothing
        End Try
        Return xmlString

    End Function

    Private Function isSurChargeApplied(ByVal paymentid As String, ByVal paramRentID As String, _
                                        ByVal paramSelectedMethod As String, _
                                        ByVal paramType As String, _
                                        ByVal paramAmt As Decimal) As Decimal
        Try


            'CREATE Procedure [dbo].[RES_getPaymentMethodSurcharge]
            '	@sRntId				VARCHAR(64),
            '	@sPmtType			VARCHAR(64),
            '	@RptType			VARCHAR(64),
            '	@tPmtLocalCurrAmt	MONEY,
            '	@sUsrCode			VARCHAR(64),
            '	@sPrgmName			VARCHAR(64),
            '	@bUseTransaction	BIT	=	1,
            '	@RetParamValue		BIT =	0,
            '	@RptChargeCurrId	VARCHAR(64) = NULL,
            '	@sRetBpdId			VARCHAR(256)	OUTPUT
            'AS
            ''sQuery = "RES_getPaymentMethodSurcharge '" & param1 & "','" & param2 & "','" & param3 & "'," & param4 & ",'" & userCode & "','PaymentMgt',1,0,'" & param5 & "',''" 

            If (paramAmt < CDec(0) AndAlso ((paymentid = "") AndAlso paramType <> "B")) Then
                Return 0
            End If

            Dim xmlString As String = Aurora.Common.Data.ExecuteScalarSP("RES_getPaymentMethodSurcharge", _
                        paramRentID, paramSelectedMethod, paramType, paramAmt, MyBase.UserCode, "PaymentMgt", 1, 0, "", "")
            Dim tempObjXML As New XmlDocument
            tempObjXML.LoadXml(xmlString)
            If tempObjXML.OuterXml.Contains("Error") = True Then
                tempObjXML = Nothing
                Return 0
            Else
                Dim surcharge As String = (tempObjXML.DocumentElement.ChildNodes(0).InnerText)
                If surcharge <> "" Then
                    If CDec(surcharge) <> 0 Then
                        Me.SetInformationShortMessage("Credit card transaction surcharge amount $" & surcharge)
                    End If
                    Return CDec(surcharge)
                End If

            End If

        Catch ex As Exception
            Return 0

        End Try
    End Function

    Private Function GetLocalIdentifier() As String

        Dim tValue As String = Nothing
        Dim s As String = Nothing
        Dim n As Integer
        Dim randomNum As New Random

        Dim str As String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

        For i As Integer = 0 To 16
            n = randomNum.Next(25)
            s = str.Substring(n, 1)
            If Not String.IsNullOrEmpty(s) Then
                tValue = tValue & s
            End If
        Next
        Return tValue
    End Function

    Private Function getLocalAmountFour(ByVal basecurrency As String, ByVal loccurrency As String, ByVal nAmount As Decimal) As Decimal
        Dim xmlTemp As New XmlDocument
        Try
            ' @iAmount	float		= 0.00,
            '@base_Curr	varchar(12)	= '',
            '@from_Curr	varchar(12)	= '',
            '@to_curr	varchar(12) = ''
            ''sQuery = "ADM_CurrencyConverter " & Cstr(CDbl(nAmount)) & ", '" & localCurrency & "', '" & from_currency & "', '" & to_currency & "'"
            xmlTemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("ADM_CurrencyConverter", nAmount, basecurrency, basecurrency, loccurrency)
            If xmlTemp.ChildNodes(0).ChildNodes.Count = 0 Then
                Return -1
            End If
            Return CDec(xmlTemp.SelectSingleNode("//Root/ConRate").InnerText)
        Catch ex As Exception
            '' MyBase.SetInformationShortMessage(ex.Message)
            Return -1
        Finally
            xmlTemp = Nothing
        End Try
    End Function

    Private Function getLocalAmountGeneric(ByVal Param1_loccurrency As String, ByVal Param2_basecurrency As String, ByVal Param3_nAmount As Decimal, Optional ByVal IsTravellers As Boolean = True) As Decimal
        Dim xmlTemp As New XmlDocument
        Try
            ' @iAmount	float		= 0.00,
            '@base_Curr	varchar(12)	= '',
            '@from_Curr	varchar(12)	= '',
            '@to_curr	varchar(12) = ''
            ''sQuery = "ADM_CurrencyConverter " & Cstr(CDbl(nAmount)) & ", '" & localCurrency & "', '" & from_currency & "', '" & to_currency & "'"


            If IsTravellers Then
                xmlTemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("ADM_CurrencyConverter", Param3_nAmount, Param1_loccurrency, Param2_basecurrency, Param1_loccurrency)
            Else
                xmlTemp = Aurora.Common.Data.ExecuteSqlXmlSPDoc("ADM_CurrencyConverter", Param3_nAmount, Param2_basecurrency, Param1_loccurrency, Param2_basecurrency)
            End If


            If xmlTemp.ChildNodes(0).ChildNodes.Count = 0 Then
                Return -1
            End If
            Return CDec(xmlTemp.SelectSingleNode("//Root/ConRate").InnerText)
        Catch ex As Exception
            ''   MyBase.SetInformationShortMessage(ex.Message)
            Return -1
        Finally
            xmlTemp = Nothing
        End Try
    End Function

    Private Function getActivePaymentDataXML() As XmlDocument
        Dim objDom As New XmlDocument
        Try

            objDom.LoadXml("<ActivePaymentData></ActivePaymentData>")
            parentNode = objDom.SelectSingleNode("//ActivePaymentData")
            Me.appendActivePaymentNode(objDom, parentNode)
            If objDom Is Nothing Then
                Return Nothing
            End If
        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)
        End Try
        Return objDom
    End Function

    Private Function setPaymentDetails(ByVal pmitem As XmlNode, _
                                       ByVal szPaymentIntegrityNo As String, _
                                       ByVal szPaymentId As String, _
                                       ByVal szStatus As String, _
                                       ByVal szPaymentDate As String, _
                                       ByVal szLocalIdentifier As String) As Boolean

        Dim tNode As XmlNode
        Dim szPdtId, szItemIntegrityNo As String

        tNode = Me.xmlActivePaymentData.ChildNodes(0)
        szPdtId = pmitem.ChildNodes.Item(11).InnerText
        szItemIntegrityNo = pmitem.ChildNodes.Item(9).InnerText
        tNode.SelectSingleNode("//PaymentId").InnerText = szPaymentId
        tNode.SelectSingleNode("//Status").InnerText = szStatus
        tNode.SelectSingleNode("//PaymentIntegrityNo").InnerText = szPaymentIntegrityNo
        tNode.SelectSingleNode("//PaymentDate").InnerText = szPaymentDate
        tNode.SelectSingleNode("//PdtId").InnerText = szPdtId
        tNode.SelectSingleNode("//PdtIntegrityNo").InnerText = szItemIntegrityNo
        tNode.SelectSingleNode("//LocalIdentifier").InnerText = szLocalIdentifier
        tNode.SelectSingleNode("//OrigCard ").InnerText = pmitem.ChildNodes.Item(7).InnerText

        litxmlActivePaymentData.Text = Server.HtmlEncode(tNode.OuterXml)

    End Function

    Private Function IsValidExpirydate(ByVal nCurrentPmMethodSelIndex As Integer) As Boolean

        If nCurrentPmMethodSelIndex <> 0 Then
            If nCurrentPmMethodSelIndex > 2 Then
                Return True
            End If
        End If
        Dim paramRentalList As String = ""
        Dim RentalList As String = ""
        Dim paramDate As Date

        Dim xmlTempDoc As XmlDocument
        Try

            If Not String.IsNullOrEmpty(Me.litxmlAmountOutStanding.Text) Then
                If Me.xmlAmountOutStanding.OuterXml = "" Then
                    Me.xmlAmountOutStanding.LoadXml(Server.HtmlDecode(Me.litxmlAmountOutStanding.Text))
                End If
            End If

            Dim ctr As Integer = Me.xmlAmountOutStanding.ChildNodes(0).ChildNodes.Count - 1

            For i As Integer = 0 To ctr
                RentalList = Me.xmlAmountOutStanding.ChildNodes(0).ChildNodes(i).ChildNodes(0).InnerText
                paramRentalList = paramRentalList & RentalList & ","
            Next

            Dim paramExpirationDateArray() As String = Me.txtExpiryData.Text.Split("/")

            If paramExpirationDateArray.Length <> 2 Then
                Me.SetInformationShortMessage("Enter expiry date in mm/yy format.")
                Return False
            End If

            Dim curYear As Integer = CInt("20" & paramExpirationDateArray(1))
            If paramExpirationDateArray.Length <> -1 Then
                paramDate = New Date(curYear, CInt(paramExpirationDateArray(0)), 1)
            End If


            xmlTempDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_ValidateCardExpWithRental", paramRentalList, paramDate)

            If xmlTempDoc IsNot Nothing Then
                If xmlTempDoc.OuterXml = "" Then
                    Me.SetInformationShortMessage("Failed to validate expiry date.")
                    Return False
                End If
            End If

            If xmlTempDoc.ChildNodes(0).ChildNodes(0).SelectSingleNode("//ExpValidation").InnerText <> "TRUE" Then
                Me.SetInformationShortMessage("Credit card will be expired at the time of CheckOut of the last rental selected for Payment.")
                Me.txtExpiryData.Focus()
                Return False
            End If

        Catch ex As Exception
            Me.SetInformationShortMessage("IsValidExpirydate -> " & ex.Message)
            Return False
        Finally
            xmlTempDoc = Nothing
        End Try

        Return True

    End Function

    Private Function CheckCardNumber() As Boolean
        Try
            If Me.ddlCardType.SelectedItem.Text = "EFT-POS" Then
                Return True
            End If

            Dim card As String = Me.ddlCardType.SelectedItem.Text

            Select Case card
                Case "Mastercard"
                    Return False
                Case "Diners"
                    Return False
                Case "Amex"
                    Return False
                Case "Visa"
                    Return False
                Case "Mastercard Imprint"
                    Return False
                Case "Diners Imprint"
                    Return False
                Case "Amex Imprint"
                    Return False
                Case "Visa Imprint"
                    Return False
                Case Else
            End Select

        Catch ex As Exception
            Return False
        End Try

    End Function

#End Region

#Region " Sub for Creditcard"

    Private Sub ClearCreditCardEftPos()

        ''Me.rdoCardPresent.Items(0).Selected = False
        ''Me.rdoCardPresent.Items(1).Selected = False

        Me.ddlCardType.SelectedIndex = -1
        Me.txtCardNumber.Text = ""
        Me.TxtName.Text = ""
        Me.txtExpiryData.Text = ""
        Me.txtApprovalReference.Text = ""
        Me.txtAmount.Text = ""

    End Sub


#End Region

#Region " Sub for Travellers check"

    ''reV:mia nov5
    Private Sub TravellerChequeRoutineChangeCurrency()
        BindToTravellersGrid()
        ClearTravellersCheck()
    End Sub
    Private Sub TravellerChequeRoutine()


        Dim numSequence As Integer = CInt(Me.txt_TravellersCheque_SeqChequec.Text)
        If numSequence <= 0 Then
            MyBase.SetInformationMessage("Sequence Check is invalid. Enter value greater than zero!")
            Me.txt_TravellersCheque_SeqChequec.Text = ""
            Me.txt_TravellersCheque_SeqChequec.Focus()
            Exit Sub
        End If

        Try
            ''''    litxmlTravellerChequeDetails.Text = Server.HtmlEncode(xmlTravellerChequeDetails.OuterXml)
            If litxmlTravellerChequeDetails.Text <> "" Then
                xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(litxmlTravellerChequeDetails.Text))
            End If

            Dim ii As Integer
            For ii = 0 To numSequence - 1
                If String.IsNullOrEmpty(addTrvrChequeToTemp) = True Then
                    MyBase.SetInformationMessage("addTrvrChequeToTemp -- Empty!")
                    Exit Sub
                End If
                Me.txt_TravellersCheque_ChequeNo.Text = increaseChequeNum(Me.txt_TravellersCheque_ChequeNo.Text, 1)
            Next


            BindToTravellersGrid()
            ClearTravellersCheck()

        Catch ex As Exception
            MyBase.SetErrorShortMessage("TravellerChequeRoutine -> " & ex.Message)
        End Try
    End Sub

    Private Sub BindToTravellersGrid()
        Dim attr As XmlAttribute
        Dim nodes As XmlNodeList = xmlTravellerChequeDetails.SelectNodes("TravellerChqRoot/CheckEntry")
        Dim ctr As Integer = 0

        For Each node As XmlNode In nodes
            If node.Name = "CheckEntry" Then
                attr = xmlTravellerChequeDetails.CreateAttribute("id")
                attr.Value = ctr
                node.Attributes.Append(attr)
                ctr += 1
            End If
        Next

        Dim ds As New DataSet
        ds.ReadXml(New XmlTextReader(xmlTravellerChequeDetails.OuterXml, System.Xml.XmlNodeType.Document, Nothing))
        Me.repTravellersCheque.DataSource = ds.Tables("CheckEntry")
        Me.repTravellersCheque.DataBind()

        Dim localhash As Hashtable = getCurrencyHashContent(Me.ddl_TravellersCheque_Currency)
        SplitCurrency(CType(Me.ddl_TravellersCheque_Currency, DropDownList), localhash)

        litxmlTravellerChequeDetails.Text = Server.HtmlEncode(xmlTravellerChequeDetails.OuterXml)
    End Sub

    Private Sub SplitCurrency(ByVal ddl As DropDownList, ByVal contentCurrency As Hashtable)

        Dim dict As StringDictionary = New StringDictionary
        Dim hashIE As IEnumerator = contentCurrency.GetEnumerator
        Dim hasItem As Boolean = False
        Try


            While hashIE.MoveNext
                dict.Add(CType(hashIE.Current, DictionaryEntry).Key, "")
            End While

            Dim decAmount As Decimal
            Dim objhtmlcontrolcurrency As Literal = Nothing
            Dim objhtmlcontrolamount As Literal = Nothing

            Dim itemValue As String
            For Each repitem As RepeaterItem In Me.repTravellersCheque.Items

                If repitem IsNot Nothing Then
                    hasItem = True
                    Dim txtAmount As TextBox = CType(repitem.FindControl("rep_Trav_Amount"), TextBox)
                    Dim ddlCur As DropDownList = CType(repitem.FindControl("rep_TravellersCheque_Currency"), DropDownList)

                    If (txtAmount IsNot Nothing) AndAlso (ddlCur IsNot Nothing) Then
                        If dict.ContainsKey(ddl.SelectedItem.Text) Then
                            If ddl.SelectedItem.Text = ddlCur.SelectedItem.Text Then
                                itemValue = dict.Item(ddl.SelectedItem.Text)
                                If String.IsNullOrEmpty(itemValue) = True Then
                                    decAmount = 0 + CDec(txtAmount.Text)
                                Else
                                    decAmount = CDec(dict.Item(ddl.SelectedItem.Text)) + CDec(txtAmount.Text)
                                End If

                                dict(ddl.SelectedItem.Text) = String.Format("{0:N}", CDec(decAmount)) ''decAmount
                                decAmount = 0
                            Else
                                itemValue = dict(ddlCur.SelectedItem.Text)
                                If String.IsNullOrEmpty(itemValue) = True Then
                                    decAmount = 0 + (txtAmount.Text)
                                Else
                                    decAmount = CDec(dict.Item(ddlCur.SelectedItem.Text)) + CDec(txtAmount.Text)
                                End If
                                dict(ddlCur.SelectedItem.Text) = String.Format("{0:N}", CDec(decAmount))
                                decAmount = 0
                            End If
                        End If
                    End If
                End If
            Next
            If hasItem = False Then Exit Sub
            Dim sbCur As New StringBuilder
            hashIE = dict.GetEnumerator
            With sbCur
                .Append("<table border='1' valign='top'  bordercolor='#C0C0C0' cellspacing='0' cellpading='0' border = '1' width='100%'>")
                While hashIE.MoveNext

                    If CType(hashIE.Current, DictionaryEntry).Value <> "" Then
                        .Append("<tr>")
                        .AppendFormat("<td  align='left'  bordercolor='black'  bgcolor='#dcdcdc' nowrap='nowrap' width='100%'>{0}</td>", CType(hashIE.Current, DictionaryEntry).Key.ToString.ToUpper)
                        .Append("</tr>")
                    End If

                End While

                .Append("</table>")
                .Append("<table border='1' valign='top'  bordercolor='#C0C0C0' cellspacing='0' cellpading='0' border = '1'>")
                .AppendFormat("<tr><td valign='middle' align='center'  bordercolor='black'  bgcolor='#dcdcdc' nowrap='nowrap'><font-color='red'>{0}</font></td></tr>", "Total Tendered (Local)")
                .Append("</table>")
            End With

            Dim sbAmt As New StringBuilder
            hashIE = dict.GetEnumerator
            Dim totalAmt As Decimal
            With sbAmt
                .Append("<table border='1' valign='top'  bordercolor='Black' cellspacing='0' cellpading='0' border = '1' width='100%'>")
                While hashIE.MoveNext

                    If CType(hashIE.Current, DictionaryEntry).Value <> "" Then
                        .Append("<tr>")
                        .AppendFormat("<td valign='middle' align='left'  bordercolor='black'  bgcolor='#dcdcdc' nowrap='nowrap' width='100%'>{0}</td>", CType(hashIE.Current, DictionaryEntry).Value)
                        .Append("</tr>")
                        ''rev:mia nov4
                        ''Dim value As Object = CType(hashIE.Current, DictionaryEntry).Value.ToString
                        totalAmt = totalAmt + CDec(CType(hashIE.Current, DictionaryEntry).Value.ToString)
                    End If

                End While
                .Append("</table>")
                .Append("<table border='1' valign='top'  bordercolor='#C0C0C0' cellspacing='0' cellpading='0' border = '1' width='100%'>")
                ''ReturnTotalTravellersCheck
                ''reV:mia nov5
                ''.AppendFormat("<tr><td valign='middle' align='left'  bordercolor='black'  bgcolor='#dcdcdc' nowrap='nowrap' width='100%'><b>{0} {1}</b></td></tr>", String.Format("{0:N}", totalAmt), Me.lblAmountToBePaidCurrency.Text)
                .AppendFormat("<tr><td valign='middle' align='left'  bordercolor='black'  bgcolor='#dcdcdc' nowrap='nowrap' width='100%'><b>{0} {1}</b></td></tr>", String.Format("{0:N}", ReturnTotalTravellersCheck), Me.lblAmountToBePaidCurrency.Text)
                .Append("</table>")
            End With


            objhtmlcontrolcurrency = CType(Me.repTravellersCheque.Controls(Me.repTravellersCheque.Controls.Count - 1).FindControl("litTravellersFooterCurrency"), Literal)
            objhtmlcontrolamount = CType(Me.repTravellersCheque.Controls(Me.repTravellersCheque.Controls.Count - 1).FindControl("litTravellersFooterAmount"), Literal)
            If (Not objhtmlcontrolcurrency Is Nothing) AndAlso (Not objhtmlcontrolcurrency Is Nothing) Then
                objhtmlcontrolcurrency.Text = sbCur.ToString
                objhtmlcontrolamount.Text = sbAmt.ToString
            End If

        Catch ex As Exception
            MyBase.SetErrorShortMessage("SplitCurrency--> " & ex.Message)
        End Try
    End Sub

    Private Sub ClearTravellersCheck()
        Me.txt_TravellersCheque_SeqChequec.Text = 1
        Me.txt_TravellersCheque_ChequeNo.Text = ""
        Me.txt_TravellersCheque_Branch.Text = ""
        Me.txt_TravellersCheque_Bank.Text = ""
        Me.txt_TravellersCheque_Amount.Text = ""
        Me.txt_TravellersCheque_AccountNo.Text = ""
        Me.txtAmount.Text = ""
    End Sub

    Private Sub ClearCreditCardAndDebit()
        Me.txtAmount.Text = ""
        Me.txtCardNumber.Text = ""
        Me.TxtName.Text = ""
        Me.txtExpiryData.Text = ""
        Me.txtApprovalReference.Text = ""
        Me.txtCVV2.Text = ""
    End Sub

#End Region

#Region " Sub for Cash"

    Private Sub clearCash()
        Me.txtAmount.Text = ""
        Me.txt_Cash_amount1.Text = ""
        Me.txt_Cash_amount2.Text = ""
        Me.txt_Cash_amount3.Text = ""

        Me.lbl_Cash_amount1.Text = "0.00"
        Me.lbl_Cash_amount2.Text = "0.00"
        Me.lbl_Cash_amount3.Text = "0.00"

        Me.lblTotal.Text = "0.00"

        Me.ddl_Cash_Currency1.SelectedValue = DefaultCurrency
        Me.ddl_Cash_Currency2.SelectedValue = DefaultCurrency
        Me.ddl_Cash_Currency3.SelectedValue = DefaultCurrency

    End Sub

    Private Sub CashUpdate(ByVal cashTextBox As TextBox, ByVal ddlCurrency As DropDownList, Optional ByVal lblconverted As Label = Nothing)

        Dim cashTextBoxvalue As Decimal
        Dim valAmountLocal As String

        If Not String.IsNullOrEmpty(cashTextBox.Text) Then
            cashTextBoxvalue = (cashTextBox.Text)

            If String.IsNullOrEmpty(DefaultCurrency) = True Then
                If (xmlPaymentmgt.OuterXml = "") Then
                    xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                End If
                DefaultCurrency = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                DefaultCurrencyDesc = xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText
            End If



            If DefaultCurrency.Equals(ddlCurrency.SelectedValue) = False Then
                valAmountLocal = getLocalAmountGeneric(ddlCurrency.SelectedItem.Text, DefaultCurrencyDesc, cashTextBoxvalue, False)

                If (xmlPaymentmgt.OuterXml = "") Then
                    xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                End If
                If String.IsNullOrEmpty(xmlPaymentmgt.SelectSingleNode("//Currency").InnerText) = False Then
                    '' ddlCurrency.SelectedValue = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                    ''   ddlCurrency.SelectedValue = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                    xmlPaymentmgt.SelectSingleNode("//CurrencyDesc").InnerText = ddlCurrency.SelectedItem.Text
                    xmlPaymentmgt.SelectSingleNode("//Currency").InnerText = ddlCurrency.SelectedValue
                    ddlCurrency.SelectedValue = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                    '' litxmlPaymentmgt.Text = Server.HtmlEncode(xmlPaymentmgt.OuterXml)
                End If


                If String.IsNullOrEmpty(valAmountLocal) Then
                    valAmountLocal = cashTextBoxvalue
                ElseIf valAmountLocal = -1 Then
                    MyBase.SetInformationShortMessage("No exchange rate exists for " & ddlCurrency.SelectedItem.Text & " -> " & DefaultCurrencyDesc)
                    valAmountLocal = 0
                End If
            Else
                valAmountLocal = cashTextBoxvalue
            End If

            If valAmountLocal = 0 Then
                cashTextBox.Text = ""
            Else
                ''REV:MIA NOV.4
                ''cashTextBox.Text = valAmountLocal 
                If lblconverted IsNot Nothing Then
                    lblconverted.Text = valAmountLocal
                End If
            End If

        Else
            cashTextBox.Text = ""
        End If


        Dim total As Decimal

        Dim cash1 As Decimal = 0
        Dim cash2 As Decimal = 0
        Dim cash3 As Decimal = 0

        ''Me.lbl_Cash_amount1
        If lblconverted.ID.Contains("lbl_Cash_amount1") Then
            If String.IsNullOrEmpty(txt_Cash_amount1.Text) = False Then
                cash1 = (valAmountLocal)
                Me.lbl_Cash_amount1.Text = String.Format("{0:N}", cash1)
            End If
        End If

        If lblconverted.ID.Contains("lbl_Cash_amount2") Then
            If String.IsNullOrEmpty(txt_Cash_amount2.Text) = False Then
                cash2 = (valAmountLocal)
                Me.lbl_Cash_amount2.Text = String.Format("{0:N}", cash2)
            End If
        End If

        If lblconverted.ID.Contains("lbl_Cash_amount3") Then
            If String.IsNullOrEmpty(txt_Cash_amount3.Text) = False Then
                cash3 = (valAmountLocal)
                Me.lbl_Cash_amount3.Text = String.Format("{0:N}", cash3)
            End If
        End If

        ''reV:mia nov5
        ''total = cash1 + cash2 + cash3
        total = CDec(Me.lbl_Cash_amount1.Text) + CDec(Me.lbl_Cash_amount2.Text) + CDec(Me.lbl_Cash_amount3.Text)

        lblTotal.Text = String.Format("{0:N}", total)

    End Sub

    Private Sub Displaytotal()
        Dim cash1 As Decimal
        Dim cash2 As Decimal
        Dim cash3 As Decimal

        Try

            If String.IsNullOrEmpty(txt_Cash_amount1.Text) = True Then
                cash1 = 0
            Else
                cash1 = (txt_Cash_amount1.Text)
                Me.lbl_Cash_amount1.Text = cash1 ''String.Format("{0:N}", cash1)
            End If

            If String.IsNullOrEmpty(txt_Cash_amount2.Text) = True Then
                cash2 = 0
            Else
                cash2 = (txt_Cash_amount2.Text)
                Me.lbl_Cash_amount2.Text = cash2 ''String.Format("{0:N}", cash2)
            End If

            If String.IsNullOrEmpty(txt_Cash_amount3.Text) = True Then
                cash3 = 0
            Else
                cash3 = (txt_Cash_amount3.Text)
                Me.lbl_Cash_amount3.Text = cash3 ''String.Format("{0:N}", cash3)
            End If

            Dim totAmount As Decimal = cash1 + cash2 + cash3
            lblTotal.Text = totAmount ''String.Format("{0:N}", totAmount)

        Catch ex As Exception
        End Try

    End Sub

    Private Sub DisplayCashMessage()
        Me.btnRemoveCash.Attributes.Add("onclick", "return RemoveCash();")
    End Sub

#End Region

#Region " Sub for Personal Check"

    Private Sub ClearPersonalCheck()
        Me.txtAmount.Text = ""
        Me.txt_PersonalCheque_Amount.Text = Me.txtAmountToBePaid.Text
        Me.txt_PersonalCheque_Amount.Enabled = True
        Me.txt_PersonalCheque_AccountName.Text = ""
        Me.txt_PersonalCheque_ChequeNo.Text = ""
        Me.txt_PersonalCheque_Bank.Text = ""
        Me.txt_PersonalCheque_Branch.Text = ""
        Me.txt_PersonalCheque_AccountNo.Text = ""
    End Sub

#End Region

#Region " Sub for Bond Transfer"
    Private Sub TransferBondToOtherBooRnt()
        Dim sExternalRef As String = txtBookingNumber.Text
        Dim sTxtType As String = ""
        If Me.xmlPaymentDetails.ChildNodes.Count = 0 Then
            AddPayment(PaymentID, RntId, ddlMethod.SelectedItem.Value, sType, (txtAmount.Text), (Me.txtAmountToBePaid.Text), paymentType, True)
        End If
        ''rev:mia oct.23
        If Not String.IsNullOrEmpty(Me.litxmlAmountOutStanding.Text) Then
            If Me.xmlAmountOutStanding.OuterXml = "" Then
                Me.xmlAmountOutStanding.LoadXml(Server.HtmlDecode(Me.litxmlAmountOutStanding.Text))
            End If
        End If
        Dim strXml As String = String.Concat("<Root>", xmlPayment.OuterXml, xmlPaymentDetails.OuterXml, xmlAmountOutStanding.OuterXml, "</Root>")
        Dim strResult As String = Aurora.Booking.Services.BookingPaymentMaintenance.PaymentTransfer(strXml, sExternalRef, MyBase.UserCode, "PaymentMGT.aspx")
        Dim xmltemp As New XmlDocument

        xmltemp.LoadXml(strResult)
        If xmltemp.OuterXml = "" Then
            MyBase.SetErrorShortMessage("Payment Transfer failed!")
            xmltemp = Nothing
            Exit Sub
        End If

        If xmltemp.DocumentElement.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "True" Then
            MyBase.SetErrorMessage(xmltemp.DocumentElement.SelectSingleNode("//Root/Error/ErrDescription").InnerText)
            xmltemp = Nothing
            Exit Sub
        End If

        MyBase.SetErrorShortMessage(xmltemp.DocumentElement.SelectSingleNode("//Root/Error/ErrDescription").InnerText)

        ''Response.Redirect(QS_BOOKING)
        Dim qsBookingID As String = ""
        Dim qsRentalID As String = ""
        If Not Request.QueryString("hdBookingId") Is Nothing Then
            qsBookingID = "&hdBookingId=" & Request.QueryString("hdBookingId").ToString()
        End If
        If Not Request.QueryString("hdRentalId") Is Nothing Then
            qsRentalID = "&hdRentalId=" & Request.QueryString("hdRentalId").ToString()
        End If
        Response.Redirect(QS_BOOKING & qsBookingID & qsRentalID & "&msgToShow=")
    End Sub
#End Region

#Region " Sub"

    Private Sub UnbindDataToDatasource()

        xmlTravellerChequeDetails = Nothing
        Me.litxmlTravellerChequeDetails.Text = ""
        Me.repTravellersCheque.DataSource = Nothing
        Me.repTravellersCheque.DataBind()

        Me.ddlMethod.SelectedIndex = -1
        Me.ddlMethod.Enabled = True
        Me.WrapClearing()

    End Sub

    Private Sub appendActivePaymentNode(ByRef objdom As XmlDocument, ByRef parentnode As XmlNode)
        Dim node As XmlNode
        node = objdom.CreateElement("PaymentId", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("Status", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PaymentIntegrityNo", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PaymentDate", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtId", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtAccountName", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtApprovalRef", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtExpiryDate", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtChequeNo", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtBank", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtBranch", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtAccountNo", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("PdtIntegrityNo", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("LocalIdentifier", "")
        parentnode.AppendChild(node)

        node = objdom.CreateElement("OrigCard", "")
        parentnode.AppendChild(node)

    End Sub

    Private Sub CancelScreen()

        bCancel = True
        'if cancel is click the set mode to new payment
        If txtAmount.ReadOnly And sMode = 1 Then
            sMode = 1
            txtAmount.ReadOnly = False
        End If

        If String.IsNullOrEmpty(litxmlPaymentDetails.Text) = True Then
            If Me.pnlPayment_CreditCardAndDebitCard.Visible = True Then
                Me.pnlPayment_CreditCardAndDebitCard.Visible = False
                Me.ddlMethod.SelectedIndex = -1
            End If
        End If

        Me.tdTansferBond.Visible = False
        Me.tdTansferBondButton.Visible = False
        Me.tdTansferBondLabel.Visible = False


        If paymentType >= 0 And paymentType < 5 Then
            If paymentType = 2 Then
                Me.xmlTravellerChequeDetails = Nothing
            End If
            paymentType = -1
        Else
            If Me.pnlPayment_CreditCardAndDebitCard.Visible = True Then


                If xmlPaymentDetails Is Nothing Then xmlPaymentDetails = New XmlDocument
                If xmlPaymentDetails.OuterXml = "" Then
                    xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))
                End If

                If Me.xmlPaymentDetails.ChildNodes.Count = 0 Then
                    Return
                End If


                For Each repitem As RepeaterItem In Me.RepPaymentEntries.Items
                    If repitem IsNot Nothing Then
                        Dim litID As Literal = CType(repitem.FindControl("litID"), Literal)
                        Litidtoremove.Text = Litidtoremove.Text & litID.Text & ","
                    End If
                Next

                If Litidtoremove.Text.EndsWith(",") = True Then
                    Litidtoremove.Text = Litidtoremove.Text.Remove(Litidtoremove.Text.Length - 1, 1)
                End If
                Dim aryID() As String = Litidtoremove.Text.Split(",")
                For Each id As String In aryID
                    Dim node As XmlNode = xmlPaymentDetails.SelectSingleNode("PaymentEntryRoot/PaymentEntry[@id='" & id & "']")
                    xmlPaymentDetails.DocumentElement.RemoveChild(node)
                Next
                Litidtoremove.Text = ""

                CalculateBalance()
                BindGrid()

                If sMode = NEW_PAYMENT Then
                    If xmlPayment Is Nothing Then xmlPayment = New XmlDocument
                    If xmlPayment.OuterXml = "" Then
                        xmlPayment.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                        xmlPayment.LoadXml(xmlPayment.SelectSingleNode("//Payment").OuterXml)
                    End If

                    xmlPayment.SelectSingleNode("//PaymentRunningTotal").InnerText = ""
                    xmlPayment.SelectSingleNode("//Balance").InnerText = xmlPayment.SelectSingleNode("//AmountToPay").InnerText
                    litxmlPaymentmgt.Text = Server.HtmlEncode(xmlPayment.OuterXml)
                End If
                ClearCreditCardEftPos()
            Else
                Me.HidePanels(HidePanel._All)
                UnbindDataToDatasource()
            End If
        End If

        Litidtoremove.Text = ""
        Me.litxmlPaymentDetails.Text = ""
        xmlPaymentDetails = New XmlDocument
        CalculateBalance()
        BindGrid()

        ''Feb 12 2010 - DPS eftpos integration - manny
        btnEFTPOS.Enabled = False
        EftposButton()
        Me.rdoCardPresent.SelectedIndex = -1
    End Sub

    Private Sub GoBack()
        PreserveAmt = 0
        Dim qsBookingID As String = ""
        Dim qsRentalID As String = ""
        If Not Request.QueryString("hdBookingId") Is Nothing Then
            qsBookingID = "&hdBookingId=" & Request.QueryString("hdBookingId").ToString()
        End If
        If Not Request.QueryString("hdRentalId") Is Nothing Then
            qsRentalID = "&hdRentalId=" & Request.QueryString("hdRentalId").ToString()
        End If
        Response.Redirect(QS_BOOKING & qsBookingID & qsRentalID)
        ''Response.Redirect(QS_BOOKING)
    End Sub

    Private Sub GetMethodType(ByVal methodTypeID As String, Optional ByVal SelText As String = "", Optional ByVal isvalid As Integer = 0)
        Try
            Dim xmlString As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getMethodType", methodTypeID).OuterXml
            xmlString = xmlString.Replace(" ", "")
            Dim dsLocal As New DataSet
            dsLocal.ReadXml(New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing))
            xmlPaymentMethods.LoadXml(xmlString)
            litxmlPaymentMethods.Text = Server.HtmlEncode(xmlString)

            Dim bPmTypeAllowToChange As Boolean = False
            If SelText <> "" Then

                If (SelText = "Credit Card" Or SelText = "Debit Card" Or SelText = "Imprint" Or SelText = "Hold Imprint" Or SelText = HOLD_NUMBER) Then  ''rev:mia nov3
                    ''Feb 15 2010 - DPS eftpos integration - manny
                    '' CardValue = dsLocal.Tables("PaymentMethod").DataSet.GetXml

                    ''show the Card Present
                    If (SelText = "Debit Card" Or SelText = "Imprint" Or SelText = "Hold Imprint" Or SelText = HOLD_NUMBER) Then ''rev:mia nov3
                        If (Me.trCardPresent.Visible = False) AndAlso (Me.ddlMethod.Enabled = False) AndAlso (bCancel = True) Then
                            Me.trCardPresent.Visible = True
                            Me.rdoCardPresent.Enabled = True
                        End If
                    Else
                        Me.trCardPresent.Visible = True
                        Me.rdoCardPresent.Enabled = True
                    End If
                    ''rev:mia august 10 2010
                    ''BreakHere()
                    EnableCardPresentOption()

                    bPmTypeAllowToChange = True
                    If xmlPaymentMethods.ChildNodes(0).ChildNodes(0).Name = "Error" Then

                        If (xmlPaymentMethods.SelectNodes("data/Error/ErrStatus").Item(0).InnerText = "True") Then
                            If (xmlPaymentMethods.SelectNodes("data/Error/ErrDescription").Item(0).InnerText = "No Records Found") Then
                                MyBase.SetErrorShortMessage(xmlPaymentMethods.SelectNodes("data/Error/ErrDescription").Item(0).InnerText)
                                Exit Sub
                            End If
                        End If

                    End If ''endif for xmlPaymentMethods.ChildNodes(0).ChildNodes(0).Name = "Error" Then

                ElseIf (SelText = "Travellers Chq") Then ''else for If (SelText = "Credit Card" Or SelText = "Debit Card" Or SelText = "Imprint") Then


                    If defaultCurrencyForTravCheck = "" Then
                        If (xmlPaymentmgt.OuterXml = "") Then
                            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                        End If
                        defaultCurrencyForTravCheck = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                    Else
                        If (xmlPaymentmgt.OuterXml = "") Then
                            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                        End If
                        If Not defaultCurrencyForTravCheck.Equals(xmlPaymentmgt.SelectSingleNode("//Currency").InnerText) Then
                            defaultCurrencyForTravCheck = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                        End If
                    End If

                    Me.ddl_TravellersCheque_Currency.SelectedValue = defaultCurrencyForTravCheck
                ElseIf (SelText = "Personal Chq" Or SelText = "Refund Chq") Then ''else for If (SelText = "Credit Card" Or SelText = "Debit Card" Or SelText = "Imprint") Then


                ElseIf (SelText = "Cash") Then ''else for If (SelText = "Credit Card" Or SelText = "Debit Card" Or SelText = "Imprint") Then

                    If defaultCurrencyForCash = "" Then
                        If (xmlPaymentmgt.OuterXml = "") Then
                            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                        End If
                        defaultCurrencyForCash = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                    Else
                        If (xmlPaymentmgt.OuterXml = "") Then
                            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                        End If
                        If Not defaultCurrencyForCash.Equals(xmlPaymentmgt.SelectSingleNode("//Currency").InnerText) Then
                            defaultCurrencyForCash = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText
                        End If
                    End If
                    Me.ddl_Cash_Currency1.SelectedValue = defaultCurrencyForCash
                    Me.ddl_Cash_Currency2.SelectedValue = defaultCurrencyForCash
                    Me.ddl_Cash_Currency3.SelectedValue = defaultCurrencyForCash
                    lblCashCurrency.Text = Me.lblAmountToBePaidCurrency.Text

                End If ''endif for If (SelText = "Credit Card" Or SelText = "Debit Card" Or SelText = "Imprint") Then

                Me.ddlMethod.Enabled = bPmTypeAllowToChange

                ''rev:mia july 27 2010
                If (IsUsingBillingToken()) Then Return

                Me.ddlCardType.DataSource = dsLocal.Tables("PaymentMethod")
                Me.ddlCardType.DataValueField = "ID"
                Me.ddlCardType.DataTextField = "NAME"
                Me.ddlCardType.DataBind()


            End If ''endif for If SelText <> "" Then

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try

    End Sub

    Private Sub HidePanels(ByVal vHidePanel As HidePanel, Optional ByVal active As Boolean = True)
        '' Me.trCardPresent.Visible = True
        Select Case vHidePanel

            Case HidePanel._CreditCard
                pnlPayment_CreditCardAndDebitCard.Visible = True
                pnlTravellersCheque.Visible = False
                pnlPersonalCheque.Visible = False
                pnlCash.Visible = False
                Me.rdoCardPresent.Enabled = True
                Me.rdoCardPresent.Visible = True
                ''rev:mia august 10 2010
                ''BreakHere()
                EnableCardPresentOption()

            Case HidePanel._DebitCard
                pnlPayment_CreditCardAndDebitCard.Visible = True
                pnlTravellersCheque.Visible = False
                pnlPersonalCheque.Visible = False
                pnlCash.Visible = False
                ''pnlPaymentEntries.Visible = False
                Me.rdoCardPresent.Enabled = False
                Me.rdoCardPresent.Visible = True
                Me.trCardPresent.Visible = False

            Case HidePanel._TravellersCheque
                pnlPayment_CreditCardAndDebitCard.Visible = False
                pnlTravellersCheque.Visible = True
                pnlPersonalCheque.Visible = False
                pnlCash.Visible = False
                ''pnlPaymentEntries.Visible = False
                Me.ddlMethod.Enabled = active

            Case HidePanel._PersonalCheque
                pnlPayment_CreditCardAndDebitCard.Visible = False
                pnlTravellersCheque.Visible = False
                pnlPersonalCheque.Visible = True
                pnlCash.Visible = False
                ''pnlPaymentEntries.Visible = False
                Me.ddlMethod.Enabled = active

            Case HidePanel._Cash
                pnlPayment_CreditCardAndDebitCard.Visible = False
                pnlTravellersCheque.Visible = False
                pnlPersonalCheque.Visible = False
                pnlCash.Visible = True
                ''pnlPaymentEntries.Visible = False
                Me.ddlMethod.Enabled = active

            Case HidePanel._RefundCheque
                pnlPayment_CreditCardAndDebitCard.Visible = False
                pnlTravellersCheque.Visible = False
                pnlPersonalCheque.Visible = True
                pnlCash.Visible = False
                ''pnlPaymentEntries.Visible = False
                Me.ddlMethod.Enabled = active

            Case HidePanel._All
                pnlPayment_CreditCardAndDebitCard.Visible = False
                pnlTravellersCheque.Visible = False
                pnlPersonalCheque.Visible = False
                pnlCash.Visible = False
                ''pnlPaymentEntries.Visible = False
                Me.ddlMethod.Enabled = active

        End Select

        If vHidePanel = HidePanel._All Then
            toggleButtons(False)
        Else
            toggleButtons(False, False)
        End If


    End Sub

    Private Sub BlankValueInDropdown(ByVal ddlControl As DropDownList)
        Dim item As ListItem = Nothing
        If ddlControl.ID = "ddlContact" Then
            '    item = New ListItem(" ", 0)
        Else
            item = New ListItem("", 0)
        End If

        ddlControl.Items.Insert(0, item)
        ddlControl.SelectedIndex = -1

        ''rev:mia EFTPOS April 8 2010
        btnEFTPOS.Enabled = False
        EftposButton()
    End Sub

    Private Sub PopulateDropdowns(ByVal sType As String, ByVal ddl As DropDownList, ByVal tablename As String, Optional ByVal method As Boolean = True, Optional ByVal id As Integer = -1)
        '@sCodcdtnum	INT 		= DEFAULT, /*Mandatory*/
        '@sCode		VARCHAR(24)	= '',
        '@sImprint	varchar(24) = '' -- will be excluded

        Dim xmlstring As String
        Dim ds As New DataSet

        Try

            If tablename <> "" Then
                If tablename.Equals("data") AndAlso method = True Then

                    If sType.Equals("B") Then
                        xmlstring = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_codecodetype", 39, String.Empty, String.Empty).OuterXml
                    Else
                        xmlstring = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_codecodetype", 39, String.Empty, "Imprint").OuterXml
                    End If

                    xmlstring = xmlstring.Replace("<Code><", "<Codes><")
                    xmlstring = xmlstring.Replace("></Code>", "></Codes>")
                    ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
                    ddl.DataSource = ds.Tables("Codes")
                    ddl.DataTextField = "CODE"
                    ddl.DataValueField = "ID"
                    ddl.DataBind()

                Else

                    ''populateList("sp_get_codecodetype '23',''", "CurrencyDetails")
                    xmlstring = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_codecodetype", 23, String.Empty, String.Empty).OuterXml
                    xmlstring = xmlstring.Replace("<Code><", "<Codes><")
                    xmlstring = xmlstring.Replace("></Code>", "></Codes>")
                    ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
                    ddl.DataSource = ds.Tables("Codes")
                    ddl.DataTextField = "CODE"
                    ddl.DataValueField = "ID"
                    ddl.DataBind()

                    ddl_Cash_Currency2.DataSource = ds.Tables("Codes")
                    ddl_Cash_Currency2.DataTextField = "CODE"
                    ddl_Cash_Currency2.DataValueField = "ID"
                    ddl_Cash_Currency2.DataBind()

                    ddl_Cash_Currency3.DataSource = ds.Tables("Codes")
                    ddl_Cash_Currency3.DataTextField = "CODE"
                    ddl_Cash_Currency3.DataValueField = "ID"
                    ddl_Cash_Currency3.DataBind()


                    ddl_TravellersCheque_Currency.DataSource = ds.Tables("Codes")
                    ddl_TravellersCheque_Currency.DataTextField = "CODE"
                    ddl_TravellersCheque_Currency.DataValueField = "ID"
                    ddl_TravellersCheque_Currency.DataBind()

                End If
            End If

            If Not ddl Is Nothing AndAlso ddl.ID = "rep_TravellersCheque_Currency" Then
                xmlstring = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_codecodetype", 23, String.Empty, String.Empty).OuterXml
                xmlstring = xmlstring.Replace("<Code><", "<Codes><")
                xmlstring = xmlstring.Replace("></Code>", "></Codes>")
                ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
                ddl.DataSource = ds.Tables("Codes")
                ddl.DataTextField = "CODE"
                ddl.DataValueField = "ID"
                ddl.DataBind()

                If xmlPaymentmgt.OuterXml = "" Then
                    xmlPaymentmgt.LoadXml(Server.HtmlDecode(Me.litxmlPaymentmgt.Text))
                End If
                ''ddl.SelectedValue = xmlPaymentmgt.SelectSingleNode("//Currency").InnerText

                ''reV:mia nov5
                If xmlTravellerChequeDetails.OuterXml <> "" Then
                    ''ddl.SelectedValue = xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry/CurrencyDesc").InnerText
                    ''ddl.SelectedItem.Text = xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id ='" & id & "']/CurrencyDesc").InnerText
                    ddl.SelectedValue = xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id ='" & id & "']/Currency").InnerText
                End If
            End If


        Catch ex As Exception
            ds = Nothing
            Throw
        End Try
    End Sub

    Private Sub toggleButtons(ByVal blnActive As Boolean, Optional ByVal firstLoad As Boolean = True)
        If firstLoad = False Then
            Me.btnAddPayment.Enabled = Not blnActive
        Else
            Me.btnAddPayment.Enabled = blnActive
        End If
    End Sub

    Private Sub BindData(Optional ByVal blnShow As Boolean = True)

        If (xmlPaymentmgt.OuterXml = "") Then
            xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
        End If

        Me.txtInfoBookingNumber.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/BookingNumber").InnerText
        Me.txtAmountToBePaid.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/AmountToPay").InnerText
        If Me.txtAmountToBePaid.Text = "" Then
            Me.txtAmountToBePaid.Text = "0.00"
        ElseIf Me.txtAmountToBePaid.Text.Contains(".") = False AndAlso Me.txtAmountToBePaid.Text <> "" Then
            Me.txtAmountToBePaid.Text = Me.txtAmountToBePaid.Text & ".00"
        End If
        Me.txtRunningPaymentTotal.Text = IIf(xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/PaymentRunningTotal").InnerText = "", "0.00", xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/PaymentRunningTotal").InnerText)
        If Me.txtRunningPaymentTotal.Text.Contains(".") = False AndAlso Me.txtRunningPaymentTotal.Text <> "" Then
            Me.txtRunningPaymentTotal.Text = Me.txtRunningPaymentTotal.Text & ".00"
        ElseIf Me.txtRunningPaymentTotal.Text = "" Then
            Me.txtRunningPaymentTotal.Text = "0.00"
        End If

        Me.txtRunningBalanceChange.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/Balance").InnerText
        If (Me.txtRunningBalanceChange.Text.Contains(".") = False) AndAlso (Me.txtRunningBalanceChange.Text <> "") Then
            Me.txtRunningBalanceChange.Text = Me.txtRunningBalanceChange.Text & ".00"
        ElseIf Me.txtRunningBalanceChange.Text = "" Then
            Me.txtRunningBalanceChange.Text = "0.00"
        End If

        Me.lblAmountToBePaidCurrency.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
        Me.lblRunningPaymentTotalCurrency.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
        Me.lblRunningBalanceChangeCurrency.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
        ''rev:mia 571 Maintain Payment page - (NZD) missing from 2nd 'Amount' column
        Me.lbl_PersonalCheque_Currency.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText

        Me.txtAmount.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/AmountToPay").InnerText

        Me.lblCreditCardcurrency.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
        If (xmlPaymentDetails.ChildNodes.Count - 1) = -1 Then
            lblNoEntries.Visible = True
            lblNoEntries.Text = "(.....No recorded payment)"
            pnlPaymentEntriesHeader.Visible = True
        Else
            lblNoEntries.Visible = True
            If xmlPaymentDetails.DocumentElement.ChildNodes.Count = 0 Then
                lblNoEntries.Text = "(.....No recorded payment)"
                pnlPaymentEntriesHeader.Visible = True
            Else
                lblNoEntries.Text = xmlPaymentDetails.DocumentElement.ChildNodes.Count
                pnlPaymentEntriesHeader.Visible = False
            End If
        End If


    End Sub


    ''rev:mia dec2
    Private Property LocalString() As String
        Get
            Return ViewState("LocalString")
        End Get
        Set(ByVal value As String)
            ViewState("LocalString") = value
        End Set
    End Property

    Private Sub LoadXMLSettings()

        Dim tempSessionNamePaymentMGT As String = "PAYMGT" & MyBase.UserCode & "_" & Me.RentalID
        Dim tempSessionNamePaymentMGTClone As String = "PAYMGTCLONE" & MyBase.UserCode & "_" & Me.RentalID
        If Not GetCurrencyBasedOnTravelDestination Then

            If Session(tempSessionNamePaymentMGT) <> "" Then
                litPaymentRequest.Text = Server.HtmlEncode(CType(Session(tempSessionNamePaymentMGT), String))
            Else
                If String.IsNullOrEmpty(litPaymentRequest.Text) Then
                    Response.Redirect(QS_RS_VEHREQMGT)
                End If
            End If

            ''---------------------------------------------------------

        End If

        If GetCurrencyBasedOnTravelDestination Then

            If Session(tempSessionNamePaymentMGTClone) <> "" Then
                litPaymentRequest.Text = Server.HtmlEncode(CType(Session(tempSessionNamePaymentMGTClone), String))
            Else
                If String.IsNullOrEmpty(litPaymentRequest.Text) Then
                    Response.Redirect(QS_RS_VEHREQMGT)
                End If
            End If
            ''---------------------------------------------------------
        End If


        If litPaymentRequest.Text = "" Then
            Throw New Exception("Payment Request not found")
        End If


        xmlDOMObjRequest.LoadXml(Server.HtmlDecode(litPaymentRequest.Text))

        ''rev:mia dec2
        LocalString = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//Local").InnerText

        RntId = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//RentalId").InnerText
        UserLocationCode = GetUserDefaultLocation(UserCode, RntId)

        BookId = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//BookingId").InnerText
        BookNo = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//BookingNo").InnerText
        iAmtToPay = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountToPay").InnerText
        sMode = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//Mode").InnerText

        ''rev:mia aug 25 fixes for refund
        ''GetCurrencyBasedOnTravelDestination = isRefundBasedOnDestination(tempSessionNamePaymentMGTClone)


        LitMode.Text = sMode
        HiddenMode.Value = sMode

        PaymentID = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//PaymentId").InnerText
        ''BreakHere()
        HiddenPdtPmtID.Value = PaymentID

        PmPaymentDate = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//PmPaymentDate").InnerText
        PmtPaymentOut = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//PmtPaymentOut").InnerText
        sAmountsOutstandingRoot = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot").OuterXml

        ''Feb 15 2010 - DPS eftpos integration - manny
        HiddenPaymentOutstanding.Value = sAmountsOutstandingRoot
        Dim blnHasChild As Boolean = False
        If xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot").HasChildNodes Then
            HiddenCurrency.Value = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency").InnerXml + "|" + xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/CurrCode").InnerXml
            HiddenRentalID.Value = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/RntId").InnerXml
            TypeText.Value = "Payment"
            btnEFTPOS.Enabled = False
            HiddenPurchase.Value = "Payment"
            btnEFTPOS.Text = "EFTPOS Payment"
            rdoCardPresent.Enabled = True
            HiddenType.Value = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency").InnerXml + "|" + xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Type").InnerXml
            IsRefund = False
            blnHasChild = True

        Else
            TypeText.Value = "Refund"
            btnEFTPOS.Enabled = True
            EftposButton()

            HiddenPurchase.Value = "Refund"
            HiddenPaymentID.Value = PaymentID
            btnEFTPOS.Text = "EFTPOS Refund"
            IsRefund = True

        End If

        ''rev:mia April 28 2010. if amount is less than zero.considered it a a refund
        Dim amtToPay As Decimal = CDec(xmlDOMObjRequest.SelectSingleNode("root/AmountToPay").InnerText)
        If amtToPay < 0 And blnHasChild Then
            TypeText.Value = IIf(amtToPay < 0, "Refund", TypeText.Value)
            btnEFTPOS.Enabled = IIf(amtToPay < 0, True, btnEFTPOS.Enabled)
        End If


        HiddenUsercode.Value = Me.UserCode


        xmlDateUser = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getPaymentDateUser", PaymentID)
        txtPaymentEntered.Text = xmlDateUser.SelectSingleNode("data/dateUser").InnerText
        txtPaymentTransferred.Text = xmlDateUser.SelectSingleNode("data/SSData").InnerText
        LocCurrCode = xmlDateUser.DocumentElement.SelectSingleNode("CurrCode").InnerText

        sSettingXml = GetSettings()
        xmldomObjSetting.LoadXml(sSettingXml)
        node = xmldomObjSetting.SelectSingleNode("//UserInfo")

        ''rev:mia nov26 - bypass the currency conversion.Currency will always be based on the country of destination
        ''------------------------------------------------------------------------------------------------------------
        If Not GetCurrencyBasedOnTravelDestination Then
            DefaultCurrency = node.ChildNodes(3).InnerText
            DefaultCurrencyDesc = node.ChildNodes(2).InnerText
        End If

        'If Not sMode = NEW_PAYMENT Then
        '    If xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency") IsNot Nothing Then
        '        If DefaultCurrency = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency").InnerText Then
        '            GetCurrencyBasedOnTravelDestination = False
        '        Else
        '            GetCurrencyBasedOnTravelDestination = True
        '        End If
        '    End If

        'End If

        If GetCurrencyBasedOnTravelDestination Then
            'rev:mia dec19  
            If (xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency")) IsNot Nothing Then
                DefaultCurrency = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency").InnerText
            End If
            If (xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/CurrCode")) IsNot Nothing Then
                DefaultCurrencyDesc = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/CurrCode").InnerText
            End If
        End If
        ''------------------------------------------------------------------------------------------------------------

        UserLocCode = node.ChildNodes(1).InnerText
        bSuperVisor = node.ChildNodes(4).InnerText
        bOridRev = node.ChildNodes(5).InnerText

        bDPSRole = node.ChildNodes(6).InnerText

        sComCode = node.SelectSingleNode("ComCode").InnerText

        If (bSuperVisor = "") Or (bSuperVisor = Nothing) Then
            bSuperVisor = 0
        End If

        If (bOridRev = "") Or (bOridRev = Nothing) Then
            bOridRev = 0
        End If

        xmlDOMObj.LoadXml("<Root><Data><Payment/></Data></Root>")
        parentNode = xmlDOMObj.SelectSingleNode("//Payment")

        node = xmlDOMObj.CreateElement("BookingId", "")
        parentNode.AppendChild(node)


        node = xmlDOMObj.CreateElement("BookingNumber", "")
        parentNode.AppendChild(node)


        node = xmlDOMObj.CreateElement("AmountToPay", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("PaymentRunningTotal", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("Balance", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("Method", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("MethodType", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("Currency", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("RentalId", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("CurrencyDesc", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("PaymentId", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("UserLocCode", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("PmPaymentDate", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("Status", "")
        parentNode.AppendChild(node)

        node = xmlDOMObj.CreateElement("PmtPaymentOut", "")
        parentNode.AppendChild(node)

        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(0).InnerText = BookId
        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(1).InnerText = BookNo


        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(4).InnerText = iAmtToPay
        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(2).InnerText = iAmtToPay

        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(7).InnerText = DefaultCurrency
        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(8).InnerText = RntId

        If PaymentID = "" Then
            xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(9).InnerText = DefaultCurrencyDesc
        Else
            xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(9).InnerText = LocCurrCode
        End If

        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(11).InnerText = UserLocCode
        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(12).InnerText = PmPaymentDate
        xmlDOMObj.SelectSingleNode("//Payment").ChildNodes(10).InnerText = PaymentID
        xmlDOMObj.SelectSingleNode("//Payment/PmtPaymentOut").InnerText = PmtPaymentOut

        xmlPaymentmgt.LoadXml(xmlDOMObj.OuterXml)
        litxmlPaymentmgtOrig.Text = Server.HtmlEncode(xmlPaymentmgt.OuterXml)
        ''Feb 15 2010 - DPS eftpos integration - manny
        hiddenPaymentxml.Value = xmlPaymentmgt.OuterXml
        xmlDOMObj = Nothing

        ''rev:mia Sept.17 2012 - fixing EFTPOS currency errors ,Problem in EFTPOS currency in AU
        ''HiddenFieldEFTPOSCurrency.Value = String.Concat(DefaultCurrency, "|", DefaultCurrencyDesc)

        xmlPaymentrequest.LoadXml(sAmountsOutstandingRoot)

        HiddenRentalID.Value = Me.RentalID
        If sMode = NEW_PAYMENT Then
            If Not (xmlPaymentrequest.SelectSingleNode("AmountsOutstandingRoot/AmountsOutstanding/Type") Is Nothing) Then
                HiddenType.Value = xmlPaymentrequest.SelectSingleNode("AmountsOutstandingRoot/AmountsOutstanding/Type").InnerText
            End If
        Else

            sXmlData = Aurora.Booking.Services.BookingPaymentMaintenance.getPayment(PaymentID)
            Dim objDom As New XmlDocument
            objDom.LoadXml(sXmlData)

            If (objDom.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/Method").InnerText.TrimEnd.Contains("Credit")) Then
                HiddenCardType.Value = "Credit".ToUpper
                Me.willUseImprint = False
            Else
                HiddenCardType.Value = "Debit".ToUpper
                ''BreakHere()
                If (objDom.SelectSingleNode("Root/Data/data/PaymentEntryRoot/PaymentEntry/Method").InnerText.TrimEnd.Contains("Imprint")) Then
                    Me.willUseImprint = True
                End If
            End If



            If objDom.ChildNodes(0).ChildNodes(0).Name = "Error" And objDom.SelectSingleNode("//Error/ErrStatus").InnerText = "True" Then
                If objDom.SelectSingleNode("//Error/ErrDescription").InnerText = "No Records Found" Then
                    sFinalXml = ""
                End If
            Else
                If objDom.ChildNodes(0).ChildNodes(1).ChildNodes(0).ChildNodes(0).Name = "PaymentEntryRoot" Then
                    sFinalXml = objDom.ChildNodes(0).ChildNodes(1).ChildNodes(0).ChildNodes(0).OuterXml
                Else
                    sFinalXml = ""
                End If
            End If



            Me.xmlpaymententrymgt.LoadXml(sFinalXml)
            If Not Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/MerchRef") Is Nothing Then
                ''HiddenRentalID.Value = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/MerchRef").InnerText
                ''HiddenRentalID.Value = Me.RentalID
            End If

            ''Feb 15 2010 - DPS eftpos integration - manny
            HiddenRefundData.Value = sFinalXml
            If Not Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/paymentType") Is Nothing Then
                HiddenType.Value = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/paymentType").InnerText
            End If


            ''rev:mia nov26 - bypass the currency conversion.Currency will always be based on the country of destination
            ''------------------------------------------------------------------------------------------------------------
            'If (String.IsNullOrEmpty(DefaultCurrency)) Then
            '    DefaultCurrency = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Currency").InnerText
            'End If
            'If (String.IsNullOrEmpty(DefaultCurrencyDesc)) Then
            '    DefaultCurrencyDesc = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/CurrencyDesc").InnerText
            'End If


            If Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Currency") IsNot Nothing Then
                If Not DefaultCurrency = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Currency").InnerText Then
                    DefaultCurrency = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Currency").InnerText
                    DefaultCurrencyDesc = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/CurrencyDesc").InnerText
                End If
            End If

            ''rev:mia dec19
            If Not xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/Currency") Is Nothing Then
                xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/Currency").InnerText = DefaultCurrency
            End If
            If Not xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc") Is Nothing Then
                xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText = DefaultCurrencyDesc
            End If

            ''litxmlPaymentmgtOrig.Text = Server.HtmlEncode(xmlPaymentmgt.OuterXml)
            ''------------------------------------------------------------------------------------------------------------

            litpaymententrymgt.Text = Server.HtmlEncode(sFinalXml)

            If (Not blnHasChild) Then
                If Not xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount") Is Nothing Then
                    Dim tempValue As Decimal = CDec(xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount").InnerText)
                    If (tempValue < 0) Then
                        TypeText.Value = "Payment"
                        btnEFTPOS.Enabled = IIf(amtToPay = 0, True, btnEFTPOS.Enabled)
                        HiddenPurchase.Value = "Payment"
                        btnEFTPOS.Text = "EFTPOS Payment"
                    End If
                End If
            End If

        End If

        If CInt(sMode) = CInt(NEW_PAYMENT) Then
            sType = xmlDOMObjRequest.ChildNodes(0).SelectSingleNode("//Type").InnerText
        Else
            sType = "B"
        End If


        ''rev:mia november 22 2010
        If (Not Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch") Is Nothing) Then
            Me.HiddenFieldPdtEFTPOSCardMethodDontMatch.Value = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardMethodDontMatch").InnerText
            Me.HiddenFieldPdtEFTPOSCardTypeSurchargeAdded.Value = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSurchargeAdded").InnerText
            Me.HiddenFieldPdtEFTPOSCardTypeSwiped.Value = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtEFTPOSCardTypeSwiped").InnerText
        End If

        If sMode <> NEW_PAYMENT Then
            If Not Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/BranchLocCode") Is Nothing Then
                PaymentComeFromDPZorDPAorDPM = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/BranchLocCode").InnerText
            End If

            If Not Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/MethodDesc") Is Nothing Then
                PaymentComeFromDPZorDPAorDPMandImprint = Me.xmlpaymententrymgt.SelectSingleNode("PaymentEntryRoot/PaymentEntry/MethodDesc").InnerText
            End If

        End If
        
    End Sub

    Private Sub LoadScreen(Optional ByVal skipDropdown As Boolean = False)

        If Not skipDropdown Then
            PopulateDropdowns(sType, ddlMethod, "data")
            PopulateDropdowns("", Me.ddl_Cash_Currency1, "data", False)
        End If

        SplitData(sMode, EXISTING_PAYMENT)


        If CInt(bDPSRole) > 0 Then
            bDPSRole = True
        Else
            bDPSRole = False
        End If
        bPmTypeAllowToChange = False

        If CInt(bSuperVisor) > 0 Then
            bSuperVisor = True
        Else
            bSuperVisor = False
        End If

        If CInt(bOridRev) > 0 Then
            bOridRev = True
        Else
            bOridRev = False
        End If

        If sMode = NEW_PAYMENT Then
            Me.btnReverse.Enabled = False
            Me.btnSave.Enabled = False
            Me.tdTansferBond.Visible = False
            tdTansferBondButton.Visible = False
            tdTansferBondLabel.Visible = False
            ddlMethod.Enabled = True
        Else
            pmtLocCode = xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//BranchLocCode").InnerText
            isPostedToFinanc = xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//IsPostedtoFinanc").InnerText

            If UserLocationCode = pmtLocCode Then
                bUserLocation = True
            Else
                bUserLocation = False
            End If

            If (bSuperVisor = False) Or (pStatus <> "Open" And pStatus <> "Suspended") Or (bUserLocation = False) Then
                Me.btnSave.Enabled = False
                Me.btnAddPayment.Enabled = False
                If Me.RepPaymentEntries.Visible = True Then
                    If Me.RepPaymentEntries.Items.Count > 0 Then
                        Dim btnremove As Button = CType(Me.repTravellersCheque.Controls(0).FindControl("btnremove"), Button)
                        If btnremove IsNot Nothing Then
                            btnremove.Enabled = False
                        End If

                        Dim chk As CheckBox = CType(Me.repTravellersCheque.Controls(0).FindControl("chkRemove"), CheckBox)
                        If chk IsNot Nothing Then
                            chk.Enabled = False
                        End If
                    End If
                End If

            ElseIf (bSuperVisor = False) Or (pStatus <> "Open" And pStatus <> "Suspended") Or (bUserLocation = True) Then
                Me.btnSave.Enabled = False
                Me.btnAddPayment.Enabled = True
            End If


            Dim isB4Cki As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("RntB4Cki").InnerText
            Dim pmtType As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("paymentType").InnerText
            Dim isCloseOff As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("isCloseOff").InnerText

            Dim localpmtLocCode As String = xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//BranchLocCode").InnerText
            Dim PmtAddUsrLocCode As String = xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//UsrLocCode").InnerText
            Dim pmtAmt As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("PaymentEntry/Amount").InnerText

            If pmtType = "B" And pmtAmt > 0 Then
                Me.tdTansferBond.Visible = True
                tdTansferBondButton.Visible = True
                tdTansferBondLabel.Visible = True
            Else
                Me.tdTansferBond.Visible = False
                tdTansferBondButton.Visible = False
                tdTansferBondLabel.Visible = False
            End If

            If (isB4Cki <> "0") Then
                If (pmtType = "B" Or PmtAddUsrLocCode = UserLocCode Or bOridRev = True) Then
                    Me.btnReverse.Enabled = True
                Else
                    Me.btnReverse.Enabled = False
                End If
            Else
                If (pStatus <> "Reconciled" Or (PmtAddUsrLocCode = UserLocCode And isCloseOff = "1") Or bOridRev = True) Then
                    Me.btnReverse.Enabled = True
                Else
                    Me.btnReverse.Enabled = False
                End If
            End If

            Dim seltext As String = Me.ddlMethod.SelectedItem.Text
            If seltext.Contains("Credit Card") Or _
               seltext.Contains("Debit Card") Or _
               seltext.Contains("Cash") Or _
               seltext.Contains("Personal") Or _
               seltext.Contains("Travellers") Or _
               seltext.Contains("Refund") Or _
               seltext = "Hold Imprint" Or _
               seltext = "Imprint" Or _
               seltext = HOLD_NUMBER Then
                ddlMethod.Enabled = False
            End If
        End If


        If (xmlPaymentDetailsOrig.OuterXml <> "") Then
            If (xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//RevPmtId").InnerText <> "") Then
                IsRevPmtID = xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//RevPmtId").InnerText
                Me.btnReverse.Enabled = False
                tdTansferBond.Visible = False
                tdTansferBondButton.Visible = False
                tdTansferBondLabel.Visible = False
            End If
        End If

        sOrigLocCode = xmlPayment.DocumentElement.SelectSingleNode("UserLocCode").InnerText

        ''rev:mia Aug.4 2010
        'Me.trCardPresent.Visible = CBool(bDPSRole)
        'If (bDPSRole And sMode = NEW_PAYMENT) Then
        '    Me.rdoCardPresent.Visible = True

        'Else
        '    If (Not bDPSRole And sMode = NEW_PAYMENT) Then
        '        Me.trCardPresent.Visible = True
        '        Me.rdoCardPresent.Visible = True
        '        Me.rdoCardPresent.Items(0).Selected = True
        '        Me.btnEFTPOS.Enabled = EFTButtonForCSR()
        '    End If
        'End If
    End Sub

    Private Sub SplitData(ByVal strMode As String, ByVal sExistingPayment As String)
        BindData()
        xmlPayment.LoadXml(xmlPaymentmgt.SelectSingleNode("//Payment").OuterXml)
        xmlAmountOutStanding.LoadXml(xmlPaymentrequest.SelectSingleNode("//AmountsOutstandingRoot").OuterXml)
        litxmlPaymentmgt.Text = Server.HtmlEncode(xmlPaymentmgt.OuterXml)
        litxmlAmountOutStanding.Text = Server.HtmlEncode(xmlAmountOutStanding.OuterXml)

        '' SplitData(sMode, EXISTING_PAYMENT)
        If strMode = sExistingPayment Then
            If xmlpaymententrymgt.HasChildNodes = True Then
                xmlPaymentDetailsOrig.LoadXml(xmlpaymententrymgt.SelectSingleNode("//PaymentEntryRoot/PaymentEntry").OuterXml)
                litxmlPaymentDetailsOrig.Text = Server.HtmlEncode(xmlPaymentDetailsOrig.OuterXml)
                pStatus = xmlPaymentDetailsOrig.ChildNodes(0).SelectSingleNode("//Status").InnerText
                trPaymentEntered.Visible = True
                trPaymentTransferred.Visible = True
                Dim isAllowedToView As Boolean = Me.isAllowedToViewCreditCard
                LoadPaymentToControls(1, -1, isAllowedToView)
            End If
        Else
            'msk_txtCardNumber.MaskType = AjaxControlToolkit.MaskedEditType.Number
            'msk_txtCardNumber.Mask = "99999-99999-99999-9999"
            'msk_txtCardNumber.AutoComplete = False
            'msk_txtCardNumber.InputDirection = AjaxControlToolkit.MaskedEditInputDirection.LeftToRight
            SetMasking()
        End If
    End Sub

    ''rev:mia oct.24
    Sub SetMasking(Optional ByVal blnActiveNonDebit As Boolean = True)
        msk_txtCardNumber.MaskType = AjaxControlToolkit.MaskedEditType.Number
        msk_txtCardNumber.AutoComplete = False
        msk_txtCardNumber.InputDirection = AjaxControlToolkit.MaskedEditInputDirection.LeftToRight

        If blnActiveNonDebit Then
            msk_txtCardNumber.Mask = "9999-9999-9999-9999"
            txtCardNumber.MaxLength = 16
        Else
            msk_txtCardNumber.Mask = "9999-9999-9999-9999-999"
            txtCardNumber.MaxLength = 19
        End If

    End Sub
    Private Sub LoadPaymentToControls(ByVal smode As Integer, ByVal recordNum As Integer, Optional ByVal isAllowedToView As Boolean = False)

        Dim pmtItem As XmlNode
        Dim szMethod As String = ""
        Dim szName As String = ""
        Dim szExpDate As String = ""
        Dim szApprovalRef As String = ""
        Dim szAmount As Decimal = 0
        Dim valSubMethod As String = ""

        Dim szCurrency As String = ""
        Dim szBank As String = ""
        Dim szBranch As String = ""
        Dim szAccountNo As String = ""
        Dim szChequeNo As String = ""
        Dim szCurrencyDesc As String = ""

        Dim szPaymentId As String = ""
        Dim szPdtId As String = ""
        Dim nodeList As XmlNode
        Dim pmtType As String = ""
        Dim tLength As Integer = -1
        Dim i As Integer = -1
        Dim szPaymentIntegrityNo As String = ""
        Dim szItemIntegrityNo As String = ""
        Dim szStatus As String = ""
        Dim szPaymentDate As String = ""
        Dim szLocalIdentifier As String = ""
        Dim pmtTypeDesc As String
        Dim objxml As New XmlDocument

        ''rev:mia nov.11,2010 - issue# 113
        Dim transactionId As String = ""

        ''rev:mia march 12 2012 -- Added CVC2
        Dim strCVV2 As String = ""

        If smode = 1 Then
            objxml.LoadXml(Server.HtmlDecode(litxmlPaymentDetailsOrig.Text))
            pmtTypeDesc = objxml.ChildNodes(0).SelectSingleNode("//MethodDesc").InnerText
            pmtType = objxml.ChildNodes(0).SelectSingleNode("//MethodValue").InnerText
            nodeList = objxml.ChildNodes(0).SelectSingleNode("//PaymentEntryItems")
            valSubMethod = objxml.ChildNodes(0).ChildNodes(1).InnerText
            szCurrency = objxml.ChildNodes(0).SelectSingleNode("//Currency").InnerText
            szCurrencyDesc = objxml.ChildNodes(0).SelectSingleNode("//CurrencyDesc").InnerText
            szPaymentId = objxml.ChildNodes(0).SelectSingleNode("//PmEntryPaymentId").InnerText
            szPaymentIntegrityNo = objxml.ChildNodes(0).SelectSingleNode("//IntegrityNo").InnerText
            szStatus = objxml.ChildNodes(0).SelectSingleNode("//Status").InnerText
            szPaymentDate = objxml.ChildNodes(0).SelectSingleNode("//PaymentDate").InnerText
            showCard = 1

            ''rev:mia march 12 2012 -- Added CVC2
            Try
                strCVV2 = objxml.ChildNodes(0).SelectSingleNode("PaymentEntryItems/PmtItem/Cvv2").InnerText
                CVV2Number = strCVV2
            Catch ex As Exception
            End Try

        Else
            objxml.LoadXml(xmlPaymentDetails.OuterXml)
            pmtTypeDesc = objxml.DocumentElement.ChildNodes(0).SelectSingleNode("//MethodDesc").InnerText
            pmtType = objxml.DocumentElement.ChildNodes(0).ChildNodes(recordNum).ChildNodes(0).InnerText
            szCurrency = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(6).InnerText
            szCurrencyDesc = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(7).InnerText
            nodeList = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(13)
            valSubMethod = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(1).InnerText
            szPaymentId = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(10).InnerText
            szPaymentIntegrityNo = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(9).InnerText
            szStatus = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(12).InnerText
            szPaymentDate = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(11).InnerText
            szLocalIdentifier = objxml.ChildNodes(0).ChildNodes(recordNum).ChildNodes(14).InnerText
            If (showCard <> 1) Then
                showCard = 2
            End If
        End If

        Try

            Dim xmlString As String = Aurora.Common.Data.ExecuteScalarSP("paym_getMethodType", pmtType)
            If xmlString <> "" Then
                xmlString = xmlString.Replace(" ", "")
            End If
            xmlPaymentMethods.LoadXml(String.Concat("<data>" & xmlString & "</data>"))

        Catch ex As Exception
        End Try

        methodType = pmtTypeDesc
        Select Case pmtTypeDesc
            Case "Credit Card", "Debit Card", "Imprint", "Hold Imprint", HOLD_NUMBER
                If pmtTypeDesc.Contains("Debit") = True Or pmtTypeDesc = "Imprint" = True Or pmtTypeDesc = "Hold Imprint" = True Or pmtTypeDesc = HOLD_NUMBER = True Then
                    trCardPresent.Visible = False
                Else
                    trCardPresent.Visible = True
                End If

                bValidCCardNumber = True
                bPmTypeAllowToChange = True
                xmlActivePaymentData = getActivePaymentDataXML()
                If xmlActivePaymentData Is Nothing Then
                    Exit Sub
                End If
                pmtItem = nodeList.ChildNodes(0)
                setPaymentDetails(pmtItem, szPaymentIntegrityNo, szPaymentId, szStatus, szPaymentDate, szLocalIdentifier)
                szName = pmtItem.ChildNodes.Item(0).InnerText
                szExpDate = pmtItem.ChildNodes.Item(3).InnerText
                szApprovalRef = pmtItem.ChildNodes.Item(2).InnerText
                szAmount = pmtItem.ChildNodes.Item(8).InnerText
                ''issue# 113
                Try
                    If Not pmtItem.ChildNodes.Item(14) Is Nothing Then
                        transactionId = pmtItem.ChildNodes.Item(14).InnerText
                        ''rev:mia dec 2 2010
                        Me.DPSTransactionId = transactionId
                    End If
                Catch ex As Exception

                End Try


                Dim decryptedCard As String = ""
                Dim encrypted As String = pmtItem.ChildNodes.Item(7).InnerText
                Dim blnDecrypt As Boolean


                blnDecrypt = DecryptCreditCardDataSuccess(pmtTypeDesc, encrypted, decryptedCard, False)
                ''rev:mia March312010
                hiddenCardNumber.Value = IIf(String.IsNullOrEmpty(decryptedCard), encrypted, decryptedCard)

                litCardNumber.Text = decryptedCard
                If blnDecrypt = True Then
                    If isAllowedToView = False Then
                        If Not String.IsNullOrEmpty(decryptedCard) Then
                            Dim unmaskedChars As String = decryptedCard.Substring(decryptedCard.Length - 4)
                            szAccountNo = "XXXX-XXXX-XXXX-" & unmaskedChars
                            msk_txtCardNumber.Enabled = False
                        Else
                            Dim unmaskedChars As String = pmtItem.ChildNodes.Item(7).InnerText
                            unmaskedChars = unmaskedChars.Substring(unmaskedChars.Length - 4)
                            szAccountNo = "XXXX-XXXX-XXXX-" & unmaskedChars
                            msk_txtCardNumber.Enabled = False
                        End If

                    Else
                        szAccountNo = decryptedCard
                    End If

                Else

                    If isAllowedToView = False Then
                        If Not String.IsNullOrEmpty(decryptedCard) Then
                            Dim unmaskedChars As String = decryptedCard.Substring(decryptedCard.Length - 4)
                            szAccountNo = "XXXX-XXXX-XXXX-" & unmaskedChars
                            msk_txtCardNumber.Enabled = False
                        Else
                            Dim unmaskedChars As String = pmtItem.ChildNodes.Item(7).InnerText
                            unmaskedChars = unmaskedChars.Substring(unmaskedChars.Length - 4)
                            szAccountNo = "XXXX-XXXX-XXXX-" & unmaskedChars
                            msk_txtCardNumber.Enabled = False
                            ''litCardNumber.Text = pmtItem.ChildNodes.Item(7).InnerText
                        End If

                    Else
                        ''rev:mia oct.24
                        SetMasking()
                        szAccountNo = pmtItem.ChildNodes.Item(7).InnerText
                    End If
                End If ''If blnDecrypt = True Then




                Dim getBPDIdforDecryption As String = ""
                If Session("bpdIDforDecryption" & MyBase.UserCode) IsNot Nothing Then
                    getBPDIdforDecryption = Session("bpdIDforDecryption" & MyBase.UserCode)
                End If

                ''rev:mia nov3-bypass decryption history when isAllowedToView = false
                If isAllowedToView Then
                    Dim tempSupervisor As String = Me.UserCode
                    If Not String.IsNullOrEmpty(CType(Session("SupervisorName"), String)) Then
                        tempSupervisor = CType(Session("SupervisorName"), String)
                    End If
                    Aurora.Booking.Services.BookingPaymentMaintenance.AddDecryptHistory(RntId, tempSupervisor, getBPDIdforDecryption, "From PaymentMGT - ")
                End If


                Dim IsRev As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/IsRevPmt").InnerText
                Dim DPSRev As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/DPSRev").InnerText
                Dim IsDPSPmt As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/DPSPmt").InnerText

                ''rev:mia oct31
                If (bDPSRole = True) Then
                    rdoCardPresent.Visible = True
                    If (IsDPSPmt = 1) Then
                        Me.rdoCardPresent.Items(1).Selected = True
                    Else
                        Me.rdoCardPresent.Items(0).Selected = True
                    End If
                Else
                    '' trCardPresent.Visible = False
                    ''rev:mia aug 4 2010
                    RadioButtonMode()
                End If

                paymentType = 1
                If smode = EXISTING_PAYMENT Then
                    ''txtAmount.ReadOnly = True
                    Dim revid As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/RevPmtId").InnerText
                    ''rev: issue #113
                    If isCardNotPresent(transactionId, pmtTypeDesc, revid, szAmount) Then
                        txtAmount.ReadOnly = False
                        HiddenFieldTxtAmount.Value = szAmount
                    Else
                        HiddenFieldTxtAmount.Value = szAmount
                        txtAmount.ReadOnly = True
                    End If
                End If

                HidePanels(HidePanel._CreditCard)
                ''GetMethodType(pmtType, "Imprint", 1)
                GetMethodType(pmtType, pmtTypeDesc, 1)
                Me.txtCardNumber.Text = szAccountNo

                Me.TxtName.Text = szName
                Me.txtExpiryData.Text = szExpDate
                Me.txtApprovalReference.Text = szApprovalRef

                ''rev:mia #issue #113
                If String.IsNullOrEmpty(szAmount) Then
                    Me.txtAmount.Text = 0
                Else
                    Me.txtAmount.Text = szAmount ''String.Format("{0:N}", szAmount)
                End If

                Me.ddlCardType.SelectedValue = valSubMethod
                Me.ddlMethod.SelectedValue = pmtType

                ''Feb 15 2010 - DPS eftpos integration - manny
                If (btnEFTPOS.Text.Equals("EFTPOS Refund")) Then
                    rdoCardPresent.Enabled = False
                Else
                    Me.rdoCardPresent.Enabled = True
                End If

                ''rev:mia march 12 2012 -- Added CVC2
                HideCVVRefund(strCVV2)


            Case "Personal Chq", "Refund Chq"
                xmlActivePaymentData = getActivePaymentDataXML()
                If xmlActivePaymentData Is Nothing Then
                    Exit Sub
                End If

                pmtItem = nodeList.ChildNodes(0)
                setPaymentDetails(pmtItem, szPaymentIntegrityNo, szPaymentId, szStatus, szPaymentDate, szLocalIdentifier)
                szAmount = pmtItem.ChildNodes.Item(8).InnerText
                szName = pmtItem.ChildNodes.Item(0).InnerText
                szChequeNo = pmtItem.ChildNodes.Item(4).InnerText
                szBank = pmtItem.ChildNodes.Item(5).InnerText
                szBranch = pmtItem.ChildNodes.Item(6).InnerText
                szAccountNo = pmtItem.ChildNodes.Item(7).InnerText

                Me.ddlMethod.SelectedValue = pmtType
                Me.txt_PersonalCheque_Amount.Text = szAmount ''String.Format("{0:N}", szAmount)
                Me.txt_PersonalCheque_Amount.Enabled = False
                Me.txt_PersonalCheque_AccountName.Text = szName
                Me.txt_PersonalCheque_ChequeNo.Text = szChequeNo
                Me.txt_PersonalCheque_Bank.Text = szBank
                Me.txt_PersonalCheque_Branch.Text = szBranch
                Me.txt_PersonalCheque_AccountNo.Text = szAccountNo

                If smode = EXISTING_PAYMENT Then
                    txtAmount.ReadOnly = True
                End If
                HidePanels(HidePanel._PersonalCheque)
                GetMethodType(pmtType, "Personal Chq", 1)
                Me.ddlMethod.Enabled = False

            Case "Cash"
                xmlActivePaymentData = getActivePaymentDataXML()
                If xmlActivePaymentData Is Nothing Then
                    Exit Sub
                End If
                pmtItem = nodeList.ChildNodes(0)
                setPaymentDetails(pmtItem, szPaymentIntegrityNo, szPaymentId, szStatus, szPaymentDate, szLocalIdentifier)
                szAmount = pmtItem.ChildNodes.Item(8).InnerText
                Me.ddlMethod.SelectedValue = pmtType


                Me.txt_Cash_amount1.Text = szAmount '' String.Format("{0:N}", szAmount)
                Me.ddl_Cash_Currency1.SelectedValue = szCurrency
                If smode = EXISTING_PAYMENT Then
                    txtAmount.ReadOnly = True

                    Dim otxt As TextBox = Nothing
                    Dim ddl As DropDownList = Nothing
                    Dim chk As CheckBox = Nothing

                    Me.btnRemoveCash.Enabled = False
                    chkCash.Enabled = False

                    For i = 0 To 2
                        If i = 0 Then
                            otxt = DirectCast(Me.txt_Cash_amount1, TextBox)
                            ddl = DirectCast(Me.ddl_Cash_Currency1, DropDownList)
                            chk = DirectCast(Me.chk_Cash_Check1, CheckBox)
                            CashUpdate(otxt, ddl, Me.lbl_Cash_amount1)
                        ElseIf i = 1 Then
                            otxt = DirectCast(Me.txt_Cash_amount2, TextBox)
                            ddl = DirectCast(Me.ddl_Cash_Currency2, DropDownList)
                            chk = DirectCast(Me.chk_Cash_Check2, CheckBox)
                            CashUpdate(otxt, ddl, Me.lbl_Cash_amount2)
                        ElseIf i = 2 Then
                            otxt = DirectCast(Me.txt_Cash_amount3, TextBox)
                            ddl = DirectCast(Me.ddl_Cash_Currency3, DropDownList)
                            chk = DirectCast(Me.chk_Cash_Check3, CheckBox)
                            CashUpdate(otxt, ddl, Me.lbl_Cash_amount3)
                        End If
                        otxt.Enabled = False
                        ddl.Enabled = False
                        ddl.SelectedValue = DefaultCurrency
                        chk.Enabled = False

                    Next
                    ''CashUpdate(otxt, ddl)
                    Me.HidePanels(HidePanel._Cash, False)
                    GetMethodType(pmtType, "Cash", 1)
                End If

            Case "Travellers Chq"
                If (Me.TravellerChequeRoutineRetrieve(nodeList, szCurrency, szCurrencyDesc, szPaymentIntegrityNo, szPaymentId, szStatus, szPaymentDate, szLocalIdentifier, pmtType) Is Nothing) Then
                    MyBase.SetInformationShortMessage("LoadPaymentToControls()-PaymentMgt.aspx. Error: Failed to load travel cheque data.")
                End If

                Me.ddlMethod.SelectedValue = pmtType
                If smode = EXISTING_PAYMENT Then
                    ''txtAmount.Enabled = False
                    Me.txt_TravellersCheque_Amount.Enabled = False
                    Me.ddl_TravellersCheque_Currency.Enabled = False
                    Me.btnAddCheck.Enabled = False

                    Me.HidePanels(HidePanel._TravellersCheque)
                    BindToTravellersGrid()

                    For Each repCheckitem As RepeaterItem In Me.repTravellersCheque.Items
                        If repCheckitem IsNot Nothing Then

                            Dim chkRemove As CheckBox = CType(repCheckitem.FindControl("chkRemoveRemoveTravellersCheque"), CheckBox)
                            Dim btn As Button = CType(repCheckitem.FindControl("btnRemoveTravellersCheque"), Button)
                            Dim chkTravellersCheckRowRemove As CheckBox = CType(repCheckitem.FindControl("chkTravellersCheckRowRemove"), CheckBox)

                            Dim rep_Trav_Amount As TextBox = CType(repCheckitem.FindControl("rep_Trav_Amount"), TextBox)
                            Dim rep_TravellersCheque_Currency As DropDownList = CType(repCheckitem.FindControl("rep_TravellersCheque_Currency"), DropDownList)

                            If chkRemove IsNot Nothing Then
                                chkRemove.Enabled = False
                            End If

                            If chkTravellersCheckRowRemove IsNot Nothing Then
                                chkTravellersCheckRowRemove.Enabled = False
                            End If

                            If btn IsNot Nothing Then
                                btn.Enabled = False
                            End If

                            If rep_Trav_Amount IsNot Nothing Then
                                rep_Trav_Amount.Enabled = False
                            End If

                            If rep_TravellersCheque_Currency IsNot Nothing Then
                                rep_TravellersCheque_Currency.Enabled = False
                            End If


                        End If
                    Next
                End If

            Case Else


        End Select
    End Sub

    ''rev: mia aug 8 2010
    Private Sub AddPayment(ByVal StrPaymentID As String, _
                           ByVal strRentalID As String, _
                           ByVal strSelectedMethod As String, _
                           ByVal strType As String, _
                           ByVal decAmount As Decimal, _
                           ByVal decAmountToPay As Decimal, _
                           ByVal intPaymentType As Integer, _
                           Optional ByVal blnIsReversal As Boolean = False, _
                           Optional ByVal isUsingBillingToken As Boolean = False)

        Dim paymentEntryNode As New XmlDocument
        Dim NodeIsExist As Boolean

        Dim decTotal As Decimal

        If (blnIsReversal = False) Then

            If (PreserveAmt <> 0) Then
                Me.txtAmount.Text = decAmount ''decAmountToPay ''--rev:mia nov7
            Else

                If Me.pnlPersonalCheque.Visible = True Then
                    If String.IsNullOrEmpty(Me.txt_PersonalCheque_Amount.Text) Then
                        PreserveAmt = 0.0
                    Else
                        PreserveAmt = Me.txt_PersonalCheque_Amount.Text
                    End If
                Else
                    If String.IsNullOrEmpty(Me.txtAmount.Text) Then
                        PreserveAmt = 0.0
                    Else
                        PreserveAmt = Me.txtAmount.Text
                    End If
                End If


            End If
            decAmt = Me.isSurChargeApplied(StrPaymentID, strRentalID, strSelectedMethod, strType, decAmount)
            If decAmt <> 0 Then
                decTotal = (txtAmount.Text) + decAmt
                ''decTotal = String.Format("{0:N}", decTotal)
            End If

        End If


        If blnIsReversal = True Then
            intPaymentType = 0
        End If

        Dim methodType As String = Me.ddlMethod.SelectedItem.Text
        If methodType = "Credit Card" Or _
            methodType = "Imprint" Or _
            methodType = "Hold Imprint" Or _
            methodType = HOLD_NUMBER Or _
            methodType = "Debit Card" Then
            intPaymentType = 1
        ElseIf methodType.Contains("Traveller") Then
            intPaymentType = 2
        ElseIf methodType.Contains("Personal") Then
            intPaymentType = 3
        ElseIf methodType.Contains("Cash") Then
            intPaymentType = 4
        ElseIf methodType.Contains("Refund") Then
            intPaymentType = 5
        End If

        Try

            Select Case intPaymentType
                Case 0, 1
                    ''rev: aug 9 2010
                    paymentEntryNode = Me.addCreditCardPayment(blnIsReversal, isUsingBillingToken)
                Case 2
                    paymentEntryNode = Me.addTravelChequePayment
                Case 3
                    paymentEntryNode = Me.addPersonalChequePayment()
                Case 4
                    paymentEntryNode = Me.addCashPayment()
                Case 5
                    paymentEntryNode = Me.addPersonalChequePayment()
                Case Else
                    Exit Sub
            End Select

            If paymentEntryNode Is Nothing Then
                Exit Sub
            End If

            If (PreserveAmt <> 0) Then
                ''paymentEntryNode.DocumentElement.SelectSingleNode("//OriginalAmt").InnerText = decAmountToPay ''PreserveAmt ''rev:mia sept17
                paymentEntryNode.DocumentElement.SelectSingleNode("//OriginalAmt").InnerText = decAmount ''decAmountToPay ''--rev:mia nov7
            Else
                If blnIsReversal = True Then
                    If (xmlpaymententrymgt.DocumentElement.SelectSingleNode("//SurchargeAmtPmt").InnerText <> "") Then
                        paymentEntryNode.DocumentElement.SelectSingleNode("//Amount").InnerText = paymentEntryNode.DocumentElement.SelectSingleNode("//Amount").InnerText - xmlpaymententrymgt.DocumentElement.SelectSingleNode("//SurchargeAmtPmt").InnerText
                        xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText = paymentEntryNode.DocumentElement.SelectSingleNode("//Amount").InnerText
                        paymentEntryNode.DocumentElement.SelectSingleNode("//LocalAmount").InnerText = paymentEntryNode.DocumentElement.SelectSingleNode("//LocalAmount").InnerText - xmlpaymententrymgt.DocumentElement.SelectSingleNode("//SurchargeAmtPmt").InnerText


                        paymentEntryNode.DocumentElement.SelectSingleNode("//PaymentEntryItems/PmtItem/PdtAmount").InnerText = paymentEntryNode.DocumentElement.SelectSingleNode("//Amount").InnerText
                        paymentEntryNode.DocumentElement.SelectSingleNode("//PaymentEntryItems/PmtItem/PdtLocalAmount").InnerText = paymentEntryNode.DocumentElement.SelectSingleNode("//LocalAmount").InnerText
                        Me.txtAmount.Text = paymentEntryNode.DocumentElement.SelectSingleNode("//Amount").InnerText
                        ''rev:mia dec1
                        paymentEntryNode.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Currency").InnerText = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Currency").InnerText
                        paymentEntryNode.SelectSingleNode("PaymentEntryRoot/PaymentEntry/CurrencyDesc").InnerText = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/CurrencyDesc").InnerText

                    End If
                    paymentEntryNode.DocumentElement.SelectSingleNode("//OriginalAmt").InnerText = -1 * (xmlpaymententrymgt.DocumentElement.SelectSingleNode("//OriginalPayment").InnerText)
                End If
            End If

            If litxmlPaymentDetails.Text <> "" Then
                xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))

            End If


            ''If (xmlPaymentDetails.ChildNodes.Count - 1) = 1 Then
            If (xmlPaymentDetails.ChildNodes.Count = 0) Then
                For i As Integer = 0 To paymentEntryNode.ChildNodes(0).ChildNodes.Count - 1
                    paymentEntryNode.ChildNodes(0).ChildNodes(i).ChildNodes(14).InnerText = GetLocalIdentifier()
                Next
                xmlPaymentDetails.LoadXml(paymentEntryNode.OuterXml)

            Else

                Dim nCurrentLength As Integer
                If (xmlPaymentDetails.ChildNodes.Count - 1) = -1 Then
                    nCurrentLength = 0
                Else
                    nCurrentLength = xmlPaymentDetails.ChildNodes(0).ChildNodes.Count - 1
                End If

                Dim tLength As Integer = paymentEntryNode.ChildNodes(0).ChildNodes.Count - 1

                Dim xmlArr As New ArrayList

                For i As Integer = 0 To tLength
                    xmlArr.Add(paymentEntryNode.ChildNodes(0).ChildNodes(i))
                Next

                For i As Integer = 0 To tLength
                    NodeIsExist = False
                    Dim ttNode As XmlNode = xmlArr(i)
                    For j As Integer = 0 To nCurrentLength
                        If nCurrentLength = 0 Then Exit For
                        If (ttNode.ChildNodes(14).InnerText <> "") Then
                            If (xmlPaymentDetails.ChildNodes(0).ChildNodes(j).ChildNodes(14).InnerText = ttNode.ChildNodes(14).InnerText) Then
                                NodeIsExist = True
                                xmlPaymentDetails.DocumentElement.ReplaceChild(ttNode, xmlPaymentDetails.ChildNodes(0).ChildNodes(j))
                            End If
                        End If
                    Next
                    If Not NodeIsExist Then
                        ttNode.ChildNodes(14).InnerText = GetLocalIdentifier()
                        If xmlPaymentDetails.OuterXml = "" Then
                            xmlPaymentDetails.LoadXml("<PaymentEntryRoot>" & ttNode.OuterXml & "</PaymentEntryRoot>")
                        Else
                            Dim strTempXML As String = xmlPaymentDetails.OuterXml
                            Dim TEMPXML As New XmlDocument
                            TEMPXML.LoadXml(ttNode.OuterXml)
                            ''Dim NODES As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry[@id='" & xmlPaymentDetails.DocumentElement.ChildNodes.Count - 1 & "']")
                            Dim NODES As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry")
                            Dim isfound As Boolean = False
                            For Each MYNODE As XmlNode In NODES
                                If MYNODE("MethodValue").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/MethodValue").InnerText AndAlso _
                                        MYNODE("SubMethod").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/SubMethod").InnerText AndAlso _
                                        MYNODE("Method").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/Method").InnerText AndAlso _
                                        MYNODE("Details").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/Details").InnerText AndAlso _
                                        MYNODE("Amount").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/Amount").InnerText AndAlso _
                                        MYNODE("LocalAmount").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/LocalAmount").InnerText AndAlso _
                                        MYNODE("Currency").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/Currency").InnerText AndAlso _
                                        MYNODE("CurrencyDesc").InnerText = TEMPXML.SelectSingleNode("PaymentEntry/CurrencyDesc").InnerText Then
                                    isfound = True
                                End If
                            Next
                            If isfound = False Then
                                strTempXML = strTempXML.Replace("</PaymentEntryRoot>", ttNode.OuterXml & "</PaymentEntryRoot>")
                            Else
                                strTempXML = strTempXML.Replace("</PaymentEntryRoot>", "</PaymentEntryRoot>")
                            End If

                            xmlPaymentDetails.LoadXml(strTempXML)
                        End If

                    End If

                Next
            End If

            If sMode = Nothing Then
                sMode = LitMode.Text
            End If

            If sMode = NEW_PAYMENT Then

                CalculateBalance()
                ddlMethod.Enabled = True
                ddlMethod.SelectedIndex = -1

            Else
                If (bSuperVisor = False Or (pStatus <> "Open" And pStatus <> "Suspended") Or bUserLocation = False) Then
                    Me.btnSave.Enabled = False
                Else
                    Me.btnSave.Enabled = True
                End If
            End If


            paymentType = -1
            Me.btnAddPayment.Enabled = False
            litxmlPaymentDetails.Text = Server.HtmlEncode(xmlPaymentDetails.OuterXml)

            BindGrid()

        Catch ex As Exception
            Me.SetErrorMessage(ex.Message)
        End Try

    End Sub

    Public Sub ShowConfirmationDialog(ByVal Message As String, ByVal Title As String, ByVal confirmControl As String)

        Select Case confirmControl
            Case "ConfirmationBoxControlExtended1"
                ConfirmationBoxControlExtended1.Show()

            Case "ConfirmationBoxControl_DoReverseWithoutRole"
                ConfirmationBoxControl_DoReverseWithoutRole.Show()

            Case "ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue"
                ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue.Text = Message
                ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue.Show()

                'Feb 15 2010 - DPS eftpos integration - manny
            Case "ConfirmationBoxControl_NoDPSTransaction"
                ConfirmationBoxControl_NoDPSTransaction.Show()

        End Select

    End Sub

    Protected Sub ConfirmationBoxControl_DoReverseWithoutRole_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl_DoReverseWithoutRole.PostBack
        If leftButton Then
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "LeftButton (reverse)".ToUpper)
            ReversePaymentPopaction("Reverse")
        Else
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "Cancel (unreverse)".ToUpper)
            ReversePaymentPopaction("UnReverse")
        End If

    End Sub

    Protected Sub ConfirmationBoxControl_DoReverse_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl_DoReverse.PostBack
        If leftButton Then
            Me.rdoCardPresent.Items(0).Selected = True

            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "LeftButton (1)".ToUpper)
            ReversePaymentPopaction("1")
        Else
            Me.rdoCardPresent.Items(1).Selected = True

            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "Cancel (0)".ToUpper)
            ReversePaymentPopaction("0")
        End If

    End Sub

    Protected Sub ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue.PostBack
        If leftButton Then
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "LeftButton (reverse)".ToUpper)
            Dim CSREFTPMT As Boolean = EFTButtonForCSR()

            Dim appliedOverride As Boolean = appliedOverrideBox()
            ''another surprises
            If (CSREFTPMT And appliedOverride) Then
                SupervisorOverrideControl1.Show()
            Else
                ReversePaymentPopaction("Reverse")
            End If


        Else
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "Cancel (unreverse)".ToUpper)
            HiddenIsOverride.Value = "False"
            ReversePaymentPopaction("UnReverse")

            If IsMethodContainsThisParameter("Imprint") And sMode = 1 Then
                Me.btnEFTPOS.Enabled = False
                Me.ddlMethod.Enabled = False
            End If
        End If

    End Sub

    Sub ReversePaymentPopaction(ByVal myaction As String, Optional ByVal reverse As Boolean = True)
        Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN: ReversePaymentPopaction: ", vbCrLf & vbCrLf & "myaction: ".ToUpper & myaction & vbCrLf & "reverse :".ToUpper & reverse)
        Dim sPmtMethod As String = Me.ddlMethod.SelectedItem.Text
        Dim bCardPresent As Boolean = False
        Dim TrRefNum As String = ""
        Dim bASKUser As String = ""
        Dim dialogRtn As String = ""
        Dim IsRev As String
        litpaymententrymgt.Text = Server.HtmlEncode(sFinalXml)

        Try
            If xmlpaymententrymgt.OuterXml = "" Then
                xmlpaymententrymgt.LoadXml(Server.HtmlDecode(litpaymententrymgt.Text))
            End If


            If sPmtMethod.Contains("Credit") = True Then
                IsRev = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/IsRevPmt").InnerText
                TrRefNum = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("PaymentEntryItems/PmtItem/TxnRef").InnerText
            End If


            If myaction = "1" Then
                bCardPresent = True
            ElseIf myaction = "0" Then
                bCardPresent = False
            ElseIf myaction = "UnReverse" Then
                Exit Sub
            End If


            Dim actionMode As String = ""
            Dim Action As String = ""
            Dim sTxtType As String = ""
            Dim strXml As String = ""

            Dim LocCurrAmr As String = ""
            Dim sLocAmt As String = ""
            Dim ischanged As Boolean = False
            If (Not isAmountNotChanged(txtAmount.Text) And ddlMethod.SelectedItem.Text.Contains("Credit Card")) Then
                Logging.LogWarning("PAYMENT SCREEN: ReversePaymentPopaction: ", vbCrLf & vbCrLf & "IsAmountNotChanged is False" & vbCrLf & "originalvalue : " & Me.HiddenFieldTxtAmount.Value & vbCrLf & " new value : " & Me.txtAmount.Text)
                xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = CDec(txtAmount.Text)
                xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText = CDec(txtAmount.Text)
                xmlpaymententrymgt.DocumentElement.SelectSingleNode("//SurchargeAmtPmt").InnerText = "0.00"
                ischanged = True
            End If

            LocCurrAmr = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText
            sLocAmt = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText


            Dim IsRevPmt As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/IsRevPmt").InnerText


            Dim localTxtAmount As Decimal
            If String.IsNullOrEmpty(txtAmount.Text) Then
                localTxtAmount = 0.0
            Else
                localTxtAmount = txtAmount.Text ''CDec(txtAmount.Text)
            End If

            Dim localtxtAmountToBePaid As Decimal
            If String.IsNullOrEmpty(txtAmountToBePaid.Text) Then
                localtxtAmountToBePaid = 0.0
            Else
                localtxtAmountToBePaid = (txtAmountToBePaid.Text)
            End If

            If Me.xmlPaymentDetails.ChildNodes.Count = 0 Then
                AddPayment(PaymentID, _
                           RntId, _
                           ddlMethod.SelectedItem.Value, _
                           sType, _
                           localTxtAmount, _
                           localtxtAmountToBePaid, _
                           paymentType, True, IsUsingBillingToken())
            End If



            If litxmlPaymentDetails.Text = "" Then
                ''MyBase.SetInformationMessage("Payment to reverse not found!")
                Exit Sub
            End If

            If Me.xmlPaymentDetails.OuterXml = "" Then
                xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))
            End If

            Dim sMerchRef As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("PaymentEntryItems/PmtItem/MerchRef").InnerText
            Dim DpsBrdCode As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("PaymentEntryItems/PmtItem/DpsBrdCode").InnerText
            Dim sAmt As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText


            ''REV:MIA NOV12
            ''xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText
            ''xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = (LocCurrAmr) - (xmlpaymententrymgt.DocumentElement.SelectSingleNode("//SurchargeAmtPmt").InnerText)
            xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryItems/PmtItem/PdtAmount").InnerText = xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText
            xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText = CDec(LocCurrAmr) - CDec(xmlpaymententrymgt.DocumentElement.SelectSingleNode("//SurchargeAmtPmt").InnerText)
            xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = CDec(xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/Amount").InnerText)
            xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/CurrencyDesc").InnerText = xmlPayment.SelectSingleNode("Payment/CurrencyDesc").InnerText

            xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText = CDec(xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText)
            xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryItems/PmtItem/PdtLocalAmount").InnerText = CDec(xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/LocalAmount").InnerText)

            xmlPayment.SelectSingleNode("Payment/Currency").InnerText = xmlPaymentDetails.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Currency").InnerText
            xmlPayment.SelectSingleNode("Payment/CurrencyDesc").InnerText = xmlPaymentDetails.SelectSingleNode("PaymentEntryRoot/PaymentEntry/CurrencyDesc").InnerText

            ''just to stop processing
            ''Exit Sub

            xmlTempNewXML.LoadXml(xmlPaymentDetails.OuterXml)
            Dim sCurr As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("CurrencyDesc").InnerText
            Dim sSubMethod As String = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("SubMethod").InnerText


            ''rev:mia march 12 2012 - added cvc2
            ''dont allow cvv2 changed by the user
            If (Not String.IsNullOrEmpty(Me.CVV2Number)) Then
                If (Me.CVV2Number.Equals(txtCVV2.Text.Trim) = False) Then
                    txtCVV2.Text = Me.CVV2Number
                End If
            End If
            
            ''rev: mia Aug 8 2010
            BreakHere()
            '' If ((TrRefNum <> "" And bCardPresent = False) Or (TrRefNum <> "" And IsBillingTokenSaving()) Or TrRefNum <> "" And Not Me.IsCardPresentWhenRefunding) Then
            If ((TrRefNum <> "" And bCardPresent = False) Or (TrRefNum <> "" And Not Me.IsCardPresentWhenRefunding)) Then

                If (sAmt < 0) Then
                    sAmt = sAmt * -1
                    sAmt = sAmt
                Else
                    If sAmt.Contains(".") = False Then
                        sAmt = sAmt & ".00"
                    End If
                    Dim aryAmt As String() = sAmt.Split(".")
                    If aryAmt(1).Length = 1 Then
                        sAmt = sAmt & "0"
                    End If
                End If

                Logging.LogWarning("", "----------------------BEFORE CALLING MAKEONLINEREFUNDPAYMENT-----------------------")
                ''rev:mia nov3 trapped dps error
                Dim dpsRefundError As String = ""
                If (makeOnlineRefundPayment(TrRefNum, sAmt, sCurr, sSubMethod, sMerchRef, DpsBrdCode, dpsRefundError, txtCVV2.Text) = False) Then
                    Logging.LogWarning("PAYMENT SCREEN: ReversePaymentPopaction: ", vbCrLf & vbCrLf & "makeOnlineRefundPayment returned error: ".ToUpper & vbCrLf & dpsRefundError)
                    Throw New Exception(dpsRefundError)
                End If
                Logging.LogWarning("", "----------------------AFTER CALLING MAKEONLINEREFUNDPAYMENT-----------------------")
            Else
                If (IsRevPmt <> "") Then
                    strXml = String.Concat("<Root>", xmlPayment.OuterXml, xmlTempNewXML.OuterXml, xmlAmountOutStanding.OuterXml, "</Root>")

                    ''rev:mia nov3 trapped dps error
                    ''rev:mia march 12 2012 - added cvc2
                    Dim dpsError As String = ""
                    If makeOnlinePayment(strXml, True, dpsError, txtCVV2.Text) = False Then
                        HidePanels(HidePanel._All)
                        Throw New Exception(dpsError)
                    End If
                End If
            End If


            xmlPaymentDetails.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("PaymentEntryItems/PmtItem/MerchRef").InnerText = sMerchRef

            If xmlPayment.OuterXml = "" Then
                Me.xmlPayment.LoadXml(Server.HtmlDecode(Me.litxmlPaymentmgt.Text))
            End If

            If xmlAmountOutStanding.OuterXml = "" Then
                xmlAmountOutStanding.LoadXml(Server.HtmlDecode(Me.litxmlAmountOutStanding.Text))
            End If


            Dim tempXmlReturn As String = ""
            Dim tempXMLboolean As Boolean

            ''rev:mia march 12 2012 - added cvc2
            Dim strCVV2Element As String = String.Concat("<cvv2>", IIf(String.IsNullOrEmpty(txtCVV2.Text), String.Empty, txtCVV2.Text.TrimEnd), "</cvv2>")


            tempXMLboolean = EncryptCreditCardDataSuccess(methodType, xmlTempNewXML.OuterXml, tempXmlReturn)
            If tempXMLboolean = False Then
                ''try to decrypt it..probably its been encrypted
                strXml = "<Root>" + xmlPayment.OuterXml + xmlTempNewXML.OuterXml + xmlAmountOutStanding.OuterXml + strCVV2Element + "</Root>"
            Else
                strXml = "<Root>" + xmlPayment.OuterXml + tempXmlReturn + xmlAmountOutStanding.OuterXml + strCVV2Element + "</Root>"
            End If



            strXml = strXml.Replace("<Root><Root><Data><Payment>", "<Root><Payment>")
            strXml = strXml.Replace("</Payment></Data></Root>", "</Payment>")

            Dim urlReferrer As String = "PaymentMGT.aspx"
            Logging.LogWarning(Me.AppendedReverseLog & "", "--------start Aurora.Booking.Services.BookingPaymentMaintenance.paymentReversal block---------".ToUpper)
            Logging.LogWarning("", vbCrLf & vbCrLf & "parameter :".ToUpper & vbCrLf & strXml & vbCrLf & vbCrLf)
            Dim strManageResult As String = Aurora.Booking.Services.BookingPaymentMaintenance.paymentReversal(strXml, MyBase.UserCode, urlReferrer, ischanged)
            Logging.LogWarning("", vbCrLf & vbCrLf & "parameter :".ToUpper & vbCrLf & strXml & vbCrLf & vbCrLf & "strManageresult:".ToUpper & strManageResult)
            Logging.LogWarning("", "--------end Aurora.Booking.Services.BookingPaymentMaintenance.paymentReversal block---------".ToUpper)

            Dim xmlTempSavePayment As New XmlDocument
            xmlTempSavePayment.LoadXml(strManageResult)
            If xmlTempSavePayment.OuterXml = "" Or xmlTempSavePayment.ChildNodes.Count = 0 Then
                MyBase.SetErrorShortMessage("Payment reversal failed.")
                HidePanels(HidePanel._All)
                Exit Sub
            End If

            If (xmlTempSavePayment.SelectNodes("Root/Error/ErrStatus").Item(0).InnerText = "True") Then
                MyBase.SetErrorShortMessage(xmlTempSavePayment.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)
                HidePanels(HidePanel._All)
                Exit Sub
            Else
                MyBase.SetInformationShortMessage(xmlTempSavePayment.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)

                ''rev:march 5, 2010
                Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
                If IsReasonsNeedToLog = True AndAlso (sRntCtyCode = "NZ" Or sRntCtyCode = "AU") Then
                    Dim newpaymentid As String = xmlTempSavePayment.SelectSingleNode("Root/Data/PaymentEntryRoot/PmEntryPaymentId").InnerText
                    objEFTPOS.InsertPaymentOverrideReasons(newpaymentid, SupervisorOverrideControl1.Reason, SupervisorOverrideControl1.Login, SupervisorOverrideControl1.OthersReasons)
                    If HiddenIsOverride.Value = "True" Then
                        objEFTPOS.AddEFTPOSHistory(RntId, SupervisorOverrideControl1.Login, "EFTPOS OVERRIDE")
                    End If
                    IsReasonsNeedToLog = False
                End If
            End If

            Dim ssBooId As String = xmlPayment.SelectSingleNode("//Payment/BookingId").InnerText
            Dim ssRntId As String = xmlPayment.SelectSingleNode("//Payment/RentalId").InnerText

            xmlPaymentDetails.LoadXml(xmlTempSavePayment.SelectSingleNode("//PaymentEntryRoot").OuterXml)
            If HiddenIsOverride.Value = "True" Then
                updatePaymentHistory(ssBooId, ssRntId, "REVPAYMENT", SupervisorOverrideControl1.Login)
            Else
                updatePaymentHistory(ssBooId, ssRntId, "REVPAYMENT")
            End If

            ''updatePaymentHistory(ssBooId, ssRntId, "REVPAYMENT")

            Response.Redirect(QS_BOOKING_NOQS & "?&activeTab=10&hdBookingId=" & Me.BookingID & "&hdRentalId=" & Me.RentalID)

        Catch ex As Exception
            MyBase.SetErrorShortMessage("Reverse Payment: " & ex.Message)
            btnReverse.Enabled = False
            HidePanels(HidePanel._All)
        End Try
    End Sub


    Private Sub ReversePayment()
        Dim sPmtMethod As String = Me.ddlMethod.SelectedItem.Text
        Dim bCardPresent As Boolean = False
        Dim TrRefNum As String = ""
        Dim bASKUser As String = ""
        Dim dialogRtn As String = ""
        Dim IsRev As String
        litpaymententrymgt.Text = Server.HtmlEncode(sFinalXml)

        ''Feb 15 2010 - DPS eftpos integration - manny
        ''Dim blnCardPresent As Boolean = rdoCardPresent.SelectedValue

        Dim msgText As String = "Are you sure you wish to reverse this payment?"

        Try
            If xmlpaymententrymgt.OuterXml = "" Then
                xmlpaymententrymgt.LoadXml(Server.HtmlDecode(litpaymententrymgt.Text))
            End If

            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN: ReversePayment", "sPmtMethod".ToUpper & " use is " & sPmtMethod.ToUpper)
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN: ReversePayment", vbCrLf & vbCrLf & "xmlpaymententrymgt object is :".ToUpper & vbCrLf & xmlpaymententrymgt.OuterXml & vbCrLf & vbCrLf)

            If sPmtMethod.Contains("Credit") = True Then
                IsRev = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot/PaymentEntry/IsRevPmt").InnerText
                TrRefNum = xmlpaymententrymgt.DocumentElement.SelectSingleNode("//PaymentEntryRoot").ChildNodes(0).SelectSingleNode("PaymentEntryItems/PmtItem/TxnRef").InnerText
                ''replace this with the Supervisor password
                If (bDPSRole = True) Then
                    If (Me.ddlMethod.SelectedItem.Text.Contains("Credit") = True) AndAlso (bDPSRole = True) AndAlso (Not String.IsNullOrEmpty(TrRefNum)) Then
                        ShowConfirmationDialog("Card Present for reversal?", "Reversal Confirmation", "ConfirmationBoxControlExtended1")
                    ElseIf (Me.ddlMethod.SelectedItem.Text.Contains("Credit") = True) AndAlso (bDPSRole = False) Then
                        ShowConfirmationDialog("Are you sure you wish to reverse this payment?", "Reversal Confirmation", "ConfirmationBoxControl_DoReverseWithoutRole")
                    ElseIf (Me.ddlMethod.SelectedItem.Text.Contains("Credit") = True) AndAlso (String.IsNullOrEmpty(TrRefNum)) Then
                        ShowConfirmationDialog("Are you sure you wish to reverse this payment?", "Reversal Confirmation", "ConfirmationBoxControl_NoDPSTransaction")
                    End If
                Else


                    ''reV:mia July 17 2012 - Rajesh asked me to add extra warning message if the 
                    '' the payment to be reverse are DPZ or DPA and member of CHGTOKEN
                    msgText = "Are you sure you wish to reverse this payment?"
                    Dim addedText As String = "This payment was taken online. The refund will also be done online. Do NOT manually process another refund."
                    Dim CHGTOKEN As Boolean = False

                    Try
                        Dim eft As New BookingPaymentEFTPOSMaintenance
                        Dim result As String = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForTokenCSR"))

                        ''not a member
                        If (result.ToUpper.Contains("not".ToUpper) = True) Then
                            CHGTOKEN = False
                        Else
                            CHGTOKEN = True
                        End If
                    Catch ex As Exception
                        CHGTOKEN = False
                    End Try

                    If (CHGTOKEN = True) Then
                        If (PaymentComeFromDPZorDPAorDPM.ToUpper.Contains("DPZ") = True Or PaymentComeFromDPZorDPAorDPM.ToUpper.Contains("DPA") = True) Then
                            msgText = String.Concat(msgText, "<br/>", addedText)
                        Else
                            If (pmtLocCode.ToUpper.Contains("DPZ") = True Or pmtLocCode.ToUpper.Contains("DPA") = True) Then
                                msgText = String.Concat(msgText, "<br/>", addedText)
                            End If
                        End If
                    Else
                        ''rev:mia August 6 2012 - addition of messages for these conditions
                        ''                      - Refund/Payment and not imprint for billing token- Add a new message
                        ''                      - DPA/DPZ  and doing Reverse Payment and not a member of CHGTOKEN - Add a new message
                        msgText = String.Concat(msgText, "<br/>", CHGTOKEN_REFUND_DPZ_OR_DPA_MSG)
                    End If

                    ShowConfirmationDialog(msgText, "Reversal Confirmation", "ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue")
                End If
            Else
                ShowConfirmationDialog("Are you sure you wish to reverse this payment?", "Reversal Confirmation", "ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue")
            End If

        Catch ex As Exception
            MyBase.SetInformationMessage(ex.Message)
            HidePanels(HidePanel._All)
        End Try
    End Sub

    Private Sub BindGrid()

        If xmlPaymentDetails.OuterXml <> "" Then

            Dim ds As New DataSet
            Dim nodes As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry")

            Dim attr As XmlAttribute
            Dim i As Integer = 0
            For Each node As XmlNode In nodes
                If node.Name = "PaymentEntry" Then
                    attr = xmlPaymentDetails.CreateAttribute("id")
                    attr.Value = i
                    node.Attributes.Append(attr)
                    i = i + 1
                End If
            Next

            If (nodes.Count - 1) = -1 Then
                RepPaymentEntries.DataSource = Nothing
                RepPaymentEntries.DataBind()
                BindData()
                ''rev:june 30 2010
                EFTPOSallowedAndDisplayMSG(True)
                Exit Sub
            End If
            ds.ReadXml(New XmlTextReader(xmlPaymentDetails.OuterXml, System.Xml.XmlNodeType.Document, Nothing))
            litxmlPaymentDetails.Text = Server.HtmlEncode(xmlPaymentDetails.OuterXml)
            RepPaymentEntries.DataSource = ds
            RepPaymentEntries.DataBind()
            ''rev:june 30 2010
            EFTPOSallowedAndDisplayMSG(False)

        Else
            RepPaymentEntries.DataSource = Nothing
            RepPaymentEntries.DataBind()
            ''rev:june 30 2010
            EFTPOSallowedAndDisplayMSG(True)

            If xmlPaymentDetails.OuterXml = "" AndAlso xmlPaymentDetails.HasChildNodes = False Then
                Me.btnAddPayment.Enabled = False
                Me.btnReverse.Enabled = False
                Me.btnSave.Enabled = False

            End If
        End If

        BindData()

        If xmlPayment.OuterXml = "" Then
            xmlPayment.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
            xmlPayment.LoadXml(xmlPayment.SelectSingleNode("//Payment").OuterXml)
        End If
    End Sub

    Private Sub CalculateBalance()
        Dim iRunTotalAmount As Decimal = 0 ''rev:mia nov7
        Dim iBalance As Integer = 0
        Dim iTotal As Decimal ''rev:mia nov7
        Dim i As Integer
        Dim dblAmount As String = "0"
        Dim dblPaymentAdded As Decimal = 0 ''rev:mia nov7

        Try

            If xmlPaymentDetails.OuterXml <> "" Then

                If (xmlPaymentmgt.OuterXml = "") Then
                    xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                End If


                If xmlPaymentmgt.SelectSingleNode("//AmountToPay").InnerText <> "" Then
                    iTotal = xmlPaymentmgt.SelectSingleNode("//AmountToPay").InnerText
                End If
                For i = 0 To xmlPaymentDetails.ChildNodes(0).ChildNodes.Count - 1
                    If (xmlPaymentDetails.DocumentElement.SelectSingleNode("//OriginalAmt").InnerText <> "") Then
                        dblAmount = xmlPaymentDetails.DocumentElement.SelectSingleNode("//OriginalAmt").InnerText
                    Else
                        dblAmount = xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText
                    End If

                    iRunTotalAmount = iRunTotalAmount + dblAmount
                    If String.IsNullOrEmpty(xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText) Then
                        xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText = ""
                    Else
                        xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText = (xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText)
                    End If

                    ''throws an exception when trying to convert to decimal an empty values.
                    ''xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText = CDec(xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(5).InnerText)

                    dblAmount = xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(4).InnerText
                    xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(4).InnerText = dblAmount

                    ''rev:mia nov7
                    dblPaymentAdded = dblPaymentAdded + CDec(xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(13).ChildNodes(0).ChildNodes(8).InnerText)

                Next
                ''rev:mia nov7
                dblPaymentAdded = 0

                ''rev:mia nov11
                'Dim nodelist As XmlNodeList = Nothing
                'If Me.ddlMethod.SelectedItem.Text.Contains("Credit") = True Then
                '    nodelist = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount")
                'Else
                '    nodelist = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtLocalAmount") ''rev:mia nov11
                'End If
                ''Dim nodelist As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtAmount")
                Dim nodelist As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/PdtLocalAmount") ''rev:mia nov11
                If nodelist.Count <> -1 Then
                    For Each node As XmlNode In nodelist
                        dblPaymentAdded += CDec(node.InnerText)
                    Next
                Else
                    dblPaymentAdded = dblPaymentAdded + CDec(xmlPaymentDetails.ChildNodes(0).ChildNodes(i).ChildNodes(13).ChildNodes(0).ChildNodes(10).InnerText)
                End If

                iBalance = iTotal - iRunTotalAmount

                ''rev:mia nov7
                iBalance = iTotal - dblPaymentAdded
                ''iBalance = dblAmount - dblAmount

                Dim szBalance As Decimal = (iBalance)
                If szBalance = "0.00" Or iBalance = 0 Or szBalance = "-0.00" Then
                    btnSave.Enabled = True
                Else

                    ''Only Hold Imprint will allow zero to be save
                    If methodType = "Hold Imprint" Or methodType = HOLD_NUMBER Then
                        btnSave.Enabled = True
                    Else
                        btnSave.Enabled = False
                    End If


                End If

                xmlPaymentmgt.SelectSingleNode("//PaymentRunningTotal").InnerText = dblPaymentAdded ''iRunTotalAmount  ''rev:mia nov7
                xmlPaymentmgt.SelectSingleNode("//Balance").InnerText = iBalance
                litxmlPaymentmgt.Text = Server.HtmlEncode(xmlPaymentmgt.OuterXml)

            ElseIf xmlPaymentDetails.HasChildNodes = False Then

                xmlPaymentmgt.LoadXml(Server.HtmlDecode(litxmlPaymentmgtOrig.Text))
                litxmlPaymentmgt.Text = Server.HtmlEncode(xmlPaymentmgt.OuterXml)

            End If


            ''rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
            ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
            Dim isallowedToRefund As Boolean = checkPaymentIfLessThanAllowedTobeRefunded(CStr(dblPaymentAdded))
            If Not isallowedToRefund Then
                DisableSaveButton = True
                ''Exit Sub
            Else
                DisableSaveButton = False
            End If



        Catch ex As Exception
            MyBase.SetErrorMessage("CalculateBalance Error: " & ex.Message)
        End Try

    End Sub

    Private Sub SavePayment()

        Try

            If xmlPaymentDetails Is Nothing Then xmlPaymentDetails = New XmlDocument
            If xmlPaymentDetails.OuterXml = "" AndAlso (String.IsNullOrEmpty(litxmlPaymentDetails.Text) = False) Then
                xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))
            End If

            Logging.LogWarning(Me.AppendedSaveLog & "PAYMENT SCREEN: SavePayment: ", vbCrLf & vbCrLf & "Payment Details (xmlPaymentDetails): ".ToUpper & vbCrLf & xmlPaymentDetails.OuterXml & vbCrLf)

            If xmlPayment Is Nothing Then xmlPayment = New XmlDocument
            If xmlPayment.OuterXml = "" Then
                xmlPayment.LoadXml(Server.HtmlDecode(litxmlPaymentmgt.Text))
                xmlPayment.LoadXml(xmlPayment.SelectSingleNode("//Payment").OuterXml)
            End If



            If xmlPaymentrequest Is Nothing Then xmlPaymentrequest = New XmlDocument
            If xmlPaymentrequest.OuterXml = "" Then
                xmlPaymentrequest.LoadXml(Server.HtmlDecode(litPaymentRequest.Text))
            End If



            If xmlAmountOutStanding Is Nothing Then xmlAmountOutStanding = New XmlDocument
            If xmlAmountOutStanding.OuterXml = "" Then
                xmlAmountOutStanding.LoadXml(Server.HtmlDecode(litxmlAmountOutStanding.Text))
            End If



            Dim vChecked As Integer
            If (xmlPaymentDetails.DocumentElement.ChildNodes.Count = 1) Then
                If (xmlPaymentDetails.DocumentElement.ChildNodes(0).ChildNodes(0).InnerText <> "" AndAlso _
                    xmlPaymentDetails.DocumentElement.ChildNodes(0).ChildNodes(0).InnerText <> Nothing) Then
                    vChecked = IIf(CheckedRemove = False, 0, 1)
                    xmlPaymentDetails.DocumentElement.ChildNodes(0).ChildNodes(8).InnerText = vChecked
                End If
            ElseIf (xmlPaymentDetails.DocumentElement.ChildNodes.Count > 1) Then
                For i As Integer = 0 To xmlPaymentDetails.DocumentElement.ChildNodes.Count - 1
                    vChecked = IIf(CheckedRemove = False, 0, 1)
                    xmlPaymentDetails.DocumentElement.ChildNodes(0).ChildNodes(8).InnerText = vChecked
                Next
            End If

            Dim ssBooId As String = xmlPayment.SelectSingleNode("//Payment/BookingId").InnerText
            Dim ssRntId As String = xmlPayment.SelectSingleNode("//Payment/RentalId").InnerText

            ''------------------------------------------------------------------------------------------------------
            ''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:NZ USER that is transacting payment and booking in AU
            ''------------------------------------------------------------------------------------------------------
            Dim value As Integer
            Try
                value = rdoCardPresent.SelectedItem.Value
            Catch ex As Exception
                value = 1
            End Try
            Dim hasbillingtoken As Boolean = IsUsingBillingToken()
            If (value = 10 And Not isSameCurrency() And Not hasbillingtoken) Then
                If (sRntCtyCode = "NZ") Then
                    If (xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText = "AUD") Then
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText = "NZD"
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/Currency").InnerText = "4F96F47F-EAAF-41ED-9F09-7686A6B6D4F4"
                    Else
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText = "AUD"
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/Currency").InnerText = "61C1FBAA-06DA-497F-95AA-6958342AF2B8"
                    End If
                Else
                    If (xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText = "NZD") Then
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText = "AUD"
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/Currency").InnerText = "61C1FBAA-06DA-497F-95AA-6958342AF2B8"
                    Else
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText = "NZD"
                        xmlAmountOutStanding.SelectSingleNode("//AmountsOutstanding/Currency").InnerText = "4F96F47F-EAAF-41ED-9F09-7686A6B6D4F4"
                    End If
                End If
            End If

            ''------------------------------------------------------------------------------------------------------


            xmlTempNewXML.LoadXml(xmlPaymentDetails.OuterXml)

            Dim custToPay As Decimal = 0
            If xmlPayment.DocumentElement.SelectSingleNode("PaymentId").InnerText = "" Then
                For i As Integer = 0 To xmlAmountOutStanding.DocumentElement.ChildNodes.Count - 1
                    custToPay = custToPay + (xmlAmountOutStanding.DocumentElement.ChildNodes(i).SelectSingleNode("CustToPay").InnerText)
                Next

                Dim LocCurr As String = xmlAmountOutStanding.DocumentElement.SelectSingleNode("//AmountsOutstanding/CurrCode").InnerText
                Dim NoOfPayment As String = xmlTempNewXML.SelectSingleNode("//PaymentEntryRoot").ChildNodes.Count - 1

                For i As Integer = 0 To NoOfPayment
                    Dim Amt As String = xmlTempNewXML.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("LocalAmount").InnerText
                    xmlTempNewXML.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("Amount").InnerText = getLocalAmountFour(DefaultCurrencyDesc, LocCurr, Amt)
                    Dim NoOfPaymentEntry As Integer = xmlTempNewXML.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems").ChildNodes.Count - 1
                    Dim PdtAmount As Decimal = 0
                    For ii As Integer = 0 To NoOfPaymentEntry
                        Dim LpdtAmt As Decimal = xmlTempNewXML.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("PaymentEntryItems").ChildNodes(ii).SelectSingleNode("PdtAmount").InnerText
                        PdtAmount = PdtAmount + (LpdtAmt)
                    Next
                    xmlTempNewXML.SelectSingleNode("//PaymentEntryRoot").ChildNodes(i).SelectSingleNode("LocalAmount").InnerText = PdtAmount ''String.Format("{0:N}", PdtAmount)
                Next
            End If


            Dim strXml As String = "<Root>" + xmlPayment.OuterXml & xmlTempNewXML.OuterXml & xmlAmountOutStanding.OuterXml + "</Root>"
            ''Dim strXml As String = String.Concat("<Root>", xmlPayment.OuterXml, tempXmlReturn, xmlAmountOutStanding.OuterXml, "</Root>")

            Logging.LogWarning("PAYMENT SCREEN: SavePayment: ", vbCrLf & "----------------------BEFORE calling makeonlinepayment-----------------------".ToUpper)
            Logging.LogWarning(Me.AppendedSaveLog & "PAYMENT SCREEN: SavePayment: ", vbCrLf & vbCrLf & "makeonlinepayment parameter: ".ToUpper & vbCrLf & strXml)

            ''rev:mia nov3 trapped dps error
            ''rev:mia march 12 2012 - added cvc2
            Dim dpsError As String = ""
            Dim strCVV2Element As String = String.Concat("<cvv2>", IIf(String.IsNullOrEmpty(txtCVV2.Text), String.Empty, txtCVV2.Text.TrimEnd), "</cvv2>")
            If makeOnlinePayment(strXml, False, dpsError, txtCVV2.Text) = False Then
                Logging.LogWarning("PAYMENT SCREEN: SavePayment:", "ERROR: makeOnlinePayment: ".ToUpper & dpsError)
                Throw New Exception(dpsError)
            End If
            Logging.LogWarning("PAYMENT SCREEN: SavePayment: ", vbCrLf & "----------------------AFTER calling makeonlinepayment-----------------------".ToUpper)



            Dim tempXmlReturn As String = ""
            Dim tempXMLboolean As Boolean = EncryptCreditCardDataSuccess(methodType, xmlTempNewXML.OuterXml, tempXmlReturn)
            If tempXMLboolean = False Then
                strXml = "<Root>" + xmlPayment.OuterXml + xmlTempNewXML.OuterXml + xmlAmountOutStanding.OuterXml + strCVV2Element + "</Root>"
            Else
                strXml = "<Root>" + xmlPayment.OuterXml + tempXmlReturn + xmlAmountOutStanding.OuterXml + strCVV2Element + "</Root>"
            End If

            Dim urlReferrer As String = "PaymentMGT.aspx"
            Logging.LogWarning(Me.AppendedSaveLog & "PAYMENT SCREEN: SavePayment: ", vbCrLf & "------------- START:Aurora.Booking.Services.BookingPaymentMaintenance.maintainPayment block ------------------------  ".ToUpper)
            ''strXml = StatusToPending(strXml)

            Dim strManageResult As String
            Try
                Logging.LogWarning("", vbCrLf & vbCrLf & "INPUT Parameter:".ToUpper & vbCrLf & strXml & vbCrLf & vbCrLf & "strManageResult:".ToUpper & vbCrLf & strManageResult)
                strManageResult = Aurora.Booking.Services.BookingPaymentMaintenance.maintainPayment(strXml, MyBase.UserCode, urlReferrer)
                Logging.LogWarning("", vbCrLf & vbCrLf & "Parameter:".ToUpper & vbCrLf & strXml & vbCrLf & vbCrLf & "strManageResult:".ToUpper & vbCrLf & strManageResult)
            Catch ex As Exception
                Logging.LogError("SavePayment", ex.Message & ", " & ex.StackTrace & ", " & ex.Source)
            End Try


            Logging.LogWarning("", vbCrLf & "------------- END:Aurora.Booking.Services.BookingPaymentMaintenance.maintainPayment block------------------------  ".ToUpper)

            Dim xmlTempSavePayment As New XmlDocument
            xmlTempSavePayment.LoadXml(strManageResult)

            If xmlTempSavePayment.OuterXml = "" Or xmlTempSavePayment.ChildNodes.Count = 0 Then
                Logging.LogWarning("PAYMENT SCREEN: SavePayment: ", "xmlTempSavePayment object is empty")
                MyBase.SetErrorShortMessage("Failed to update")
                Throw New Exception("Failed to update")
            End If

            If (xmlTempSavePayment.SelectNodes("Root/Error/ErrStatus").Item(0).InnerText = "True") Then
                MyBase.SetErrorShortMessage(xmlTempSavePayment.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)
                Logging.LogWarning("PAYMENT SCREEN: SavePayment: ", "xmlTempSavePayment object errstatus  is " & xmlTempSavePayment.SelectNodes("Root/Error/ErrStatus").Item(0).InnerText)
                Throw New Exception(xmlTempSavePayment.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)
            Else
                MyBase.SetErrorShortMessage(xmlTempSavePayment.SelectNodes("Root/Error/ErrDescription").Item(0).InnerText)

                ''rev:march 5, 2010
                Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
                If IsReasonsNeedToLog = True AndAlso (sRntCtyCode = "NZ" Or sRntCtyCode = "AU") Then
                    Dim tempPaymentID As String = xmlTempSavePayment.SelectSingleNode("Root/Data/PaymentEntryRoot/PmEntryPaymentId").InnerText
                    objEFTPOS.InsertPaymentOverrideReasons(tempPaymentID, SupervisorOverrideControl1.Reason, SupervisorOverrideControl1.Login, SupervisorOverrideControl1.OthersReasons)
                    If HiddenIsOverride.Value = "True" Then
                        objEFTPOS.AddEFTPOSHistory(RntId, SupervisorOverrideControl1.Login, "EFTPOS OVERRIDE")
                    End If
                    IsReasonsNeedToLog = False
                End If

            End If

            xmlPaymentDetails.LoadXml(xmlTempSavePayment.SelectSingleNode("//PaymentEntryRoot").OuterXml)
            Logging.LogWarning(Me.AppendedSaveLog & "PAYMENT SCREEN: SavePayment: ", vbCrLf & vbCrLf & "xmlPaymentDetails object : ".ToUpper & vbCrLf & xmlPaymentDetails.OuterXml & vbCrLf & vbCrLf)
            ''updatePaymentHistory(ssBooId, ssRntId, "PAYMENT")
            If HiddenIsOverride.Value = "True" Then
                updatePaymentHistory(ssBooId, ssRntId, "PAYMENT", SupervisorOverrideControl1.Login)
            Else
                updatePaymentHistory(ssBooId, ssRntId, "PAYMENT")
            End If


            Dim qsBookingID As String = ""
            Dim qsRentalID As String = ""
            If Not Request.QueryString("hdBookingId") Is Nothing Then
                qsBookingID = "&hdBookingId=" & Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                qsRentalID = "&hdRentalId=" & Request.QueryString("hdRentalId").ToString()
            End If
            Response.Redirect(QS_BOOKING & qsBookingID & qsRentalID)

        Catch exThread As Threading.ThreadAbortException
            ''do nothing
        Catch ex As Exception
            MyBase.SetErrorShortMessage("SavePayment : " & ex.Message)
            Logging.LogException("PAYMENT SCREEN: SavePayment", ex)
        End Try


    End Sub

    Private Sub updatePaymentHistory(ByVal sbooid As String, _
                                     ByVal srentid As String, _
                                     ByVal smethod As String, _
                                     Optional ByVal usercode As String = "")
        Dim strData As String = "<Root><Data><paramA>" + sbooid + "</paramA><paramB>" + srentid + "</paramB></Data></Root>"
        Dim strManageResult As String = ""
        Try
            If String.IsNullOrEmpty(usercode) Then usercode = MyBase.UserCode
            Logging.LogWarning("PAYMENT SCREEN: updatePaymentHistory ", vbCrLf & vbCrLf & "updatePaymentHistory parameter: ".ToUpper & vbCrLf & strData & "," & smethod)
            strManageResult = Aurora.Booking.Services.BookingPaymentUpdateSingleItem.UpdateItem(strData, usercode, "Booking", "paym_updatePaymentHistory", smethod)
            Logging.LogWarning("PAYMENT SCREEN: updatePaymentHistory ", vbCrLf & vbCrLf & "updatePaymentHistory result: ".ToUpper & vbCrLf & strManageResult)
            Logging.LogWarning(" ", vbCrLf & vbCrLf & "-----------------------------------ENDING PAYMENT SCREEN TRANSACTION FOR " & Me.RentalID & "----------------------------------" & vbCrLf & vbCrLf & vbCrLf)
        Catch ex As Exception
            Logging.LogWarning("PAYMENT SCREEN: updatePaymentHistory ", "ERROR: updatePaymentHistory: ".ToUpper & ex.StackTrace)
            MyBase.SetErrorShortMessage(ex.Message)
        End Try
    End Sub

    ''rev: aug 9 2010
    Private Sub appendPaymentEntryNode(ByRef objdom As XmlDocument, _
                                       ByRef parentnode As XmlNode, _
                                       Optional ByVal isUsingBillingToken As Boolean = False)
        Dim node As XmlNode
        Try
            node = objdom.CreateElement("MethodValue", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("SubMethod", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("Method", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("Details", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("Amount", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("LocalAmount", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("Currency", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("CurrencyDesc", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("IsRemove", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("IntegrityNo", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("PmEntryPaymentId", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("PaymentDate", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("Status", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("PaymentEntryItems", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("PmtItem", "")
            parentnode.ChildNodes(13).AppendChild(node)

            node = objdom.CreateElement("AccountName", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("PmtPtmId", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("ApprovalRef", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("ExpiryDate", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("ChequeNo", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("Bank", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("Branch", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("AccountNo", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("PdtAmount", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("PdtIntegrityNo", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("PdtLocalAmount", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("PdtId", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("AuthCode", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("MerchRef", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("TxnRef", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            ''rev:mia sept 30 2010 - bday of E.A.
            'node = objdom.CreateElement("PdtDpsEfTxnRef", "")
            'parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            node = objdom.CreateElement("IsCrdPre", "")
            parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)

            If isUsingBillingToken AndAlso IsBillingTokenSaving() Then
                ''rev:mia Aug 8 2010
                node = objdom.CreateElement("DPSBillingToken", "")
                node.InnerText = Me.BillingTokenSelect.SelectedItem.Value
                parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)
                SetHashTableValue(Me.BillingTokenSelect.SelectedIndex, Me.BillingTokenSelect.SelectedItem.Value)

            Else
                ''another change of rules
                If isUsingBillingToken Then
                    node = objdom.CreateElement("DPSBillingToken", "")
                    node.InnerText = Me.BillingTokenSelect.SelectedItem.Value
                    parentnode.ChildNodes(13).ChildNodes(0).AppendChild(node)
                    SetHashTableValue(Me.BillingTokenSelect.SelectedIndex, Me.BillingTokenSelect.SelectedItem.Value)
                End If
            End If

            node = objdom.CreateElement("LocalIdentifier", "")
            parentnode.AppendChild(node)

            node = objdom.CreateElement("OriginalAmt", "")
            parentnode.AppendChild(node)

        Catch ex As Exception
            MyBase.SetErrorShortMessage(ex.Message)

        End Try

    End Sub

#End Region

#Region " Page Load"
    Private ReadOnly Property BookingID() As String
        Get
            Return Request.QueryString("hdBookingId")
        End Get
    End Property

    Private ReadOnly Property RentalID() As String
        Get
            Return Request.QueryString("hdRentalId")
        End Get
    End Property

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Me.BillingTokenSelect.AutoPostBack = True

        Me.ddlCardType.AutoPostBack = True
        Me.BtnCancel.OnClientClick = "return onCancel();"
        Me.BtnbackToBooking.OnClientClick = "return onGoback();"

        Me.txt_Cash_amount1.Attributes.Add("onblur", "Cashdisplay();")
        Me.txt_Cash_amount2.Attributes.Add("onblur", "Cashdisplay();")
        Me.txt_Cash_amount3.Attributes.Add("onblur", "Cashdisplay();")

        Me.txtAmount.Attributes.Add("onblur", "ConvertToDecimal(this);")
        Me.txt_PersonalCheque_Amount.Attributes.Add("onblur", "ConvertToDecimal(this);")
        Me.txt_TravellersCheque_Amount.Attributes.Add("onblur", "ConvertToDecimal(this);")

        Me.DisplayCashMessage()

        tdTansferBond.Visible = False
        tdTansferBondButton.Visible = False
        tdTansferBondLabel.Visible = False
        paymentType = -1
        updateCardNum = 0

        ''rev:mia dec19
        GetCurrencyBasedOnTravelDestination = False
        rdoCardPresent.AutoPostBack = True
        'ConfirmationBoxControlExtended1.LeftButton_AutoPostBack = False
        'ConfirmationBoxControlExtended1.Left_Button.OnClientClick = "ModalRefund();"

        Me.SupervisorOverrideControl1.ViewReason = True
        Me.ConfirmationBoxControlEFTPOS.LeftButton_AutoPostBack = False
        Me.cbBillingTokenWithNovalue.Text = BILLING_EMPTY_MSG


        Me.cbPaymentAgainstBillingToken.HideOKButtonAndDisplaySearch(False)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Dim eft As New BookingPaymentEFTPOSMaintenance
            If Not Page.IsPostBack Then
                EFTButtonForCSRvalue = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForCSR"))
                IsBillingTokenPermittedValue = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForTokenCSR")) ''CHGTOKEN

                LoadXMLSettings()
                HidePanels(HidePanel._All)
                toggleButtons(False)
                LoadScreen()
                bCancel = False
                Dim blnCreditCard As Boolean = Me.ddlMethod.SelectedItem.Text.Contains("Credit Card")
                Me.SetMasking(blnCreditCard)

                btnSave.OnClientClick = "return DisplayWarningMessage()"
                btnReverse.OnClientClick = "return DisplayWarningMessage()"

                PopulateBillingToken(RentalID)
                ImprintBillingTokenRules(True)

                ''rev:mia Sept.2 2013 - add the details on top of the paymetn page
                BookingDetailsUserControl1.BookingNumber = Me.BookNo
                BookingDetailsUserControl1.RentalId = RentalID
                BookingDetailsUserControl1.LoadDetails(Me.UserCode)
            Else

                ''rev:mia dec19
                If String.IsNullOrEmpty(litpaymententrymgt.Text) = False AndAlso Not GetCurrencyBasedOnTravelDestination Then
                    xmlpaymententrymgt.LoadXml(Server.HtmlDecode(litpaymententrymgt.Text))
                End If

            End If
            Dim sc As ScriptManager = ScriptManager.GetCurrent(Me)
            If IsRefund Then
                If Not String.IsNullOrEmpty(txtAmount.Text) Then
                    Me.SupervisorOverrideControl1.PaymentType = "Refund"
                    If CDec(txtAmount.Text) < 0 Then
                        Me.SupervisorOverrideControl1.DisplayType = "PAYMENT"
                        ReasonReload("PAYMENT")
                    Else

                        Me.SupervisorOverrideControl1.DisplayType = "AUTHORITY"
                        ReasonReload("REFUND")
                    End If
                End If
            Else
                If (Me.txtAmountToBePaid.Text.Contains("-")) Then
                    Me.SupervisorOverrideControl1.PaymentType = "Refund"
                    If sMode = 0 And CDec(Me.txtAmountToBePaid.Text) < 0 Then

                        If sc.AsyncPostBackSourceElementID.IndexOf("ctl00$ContentPlaceHolder$BillingTokenSelect") <> -1 Then
                            If (Not String.IsNullOrEmpty(Me.BillingTokenSelect.SelectedValue)) Then
                                Me.SupervisorOverrideControl1.DisplayType = "AUTHORITY"
                            Else
                                Me.SupervisorOverrideControl1.DisplayType = "PAYMENT"
                            End If
                        Else
                            Me.SupervisorOverrideControl1.DisplayType = "AUTHORITY"
                        End If
                    End If
                    ReasonReload("REFUND")
                Else
                    Me.SupervisorOverrideControl1.PaymentType = "Purchase"
                    ReasonReload("PAYMENT")
                End If

            End If

            HiddenFieldUserPrinter.Value = Me.UserCode
            Dim EFTButtonForCSRresult As Boolean = EFTButtonForCSR()
            HiddenIsEFTPOSactive.Value = IIf(EFTButtonForCSRresult = True, "false", "true")

            If (sMode = 1) Then
                If Me.willUseImprint Then
                    Me.btnEFTPOS.Enabled = False
                    HiddenIsEFTPOSactive.Value = "true"
                Else
                    If IsMethodContainsThisParameter("Imprint") Then
                        Me.btnEFTPOS.Enabled = False
                        HiddenIsEFTPOSactive.Value = "true"
                    End If
                End If
            End If

            If sMode = 1 Then Me.trdBillingToken.Visible = False

            ''rev:mia Aug.5 2010
            _GetPrinterName = Server.HtmlEncode(eft.GetPrinterNameAndAssignToJS(Me.UserCode))
            HiddenPrinterName.Value = _GetPrinterName
            ''rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
            ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
            HiddenFieldAllowableAmountToRefund.Value = ConfigurationManager.AppSettings("AllowableAmountToRefund")
            HiddenFieldAllowableAmountToRefundForBondNZ.Value = ConfigurationManager.AppSettings("AllowableAmountToRefundForBondNZ")
            HiddenFieldAllowableAmountToRefundReceiptAndDepositNZ.Value = ConfigurationManager.AppSettings("AllowableAmountToRefundReceiptAndDepositNZ")
            HiddenFieldpTypes.Value = Request.QueryString("pTypes")
        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)

        End Try

    End Sub

#End Region

#Region " Events"

    Protected Sub ddlMethod_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMethod.DataBound
        BlankValueInDropdown(sender)
    End Sub

    Protected Sub ddlMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMethod.SelectedIndexChanged

        WrapClearing()

        If ddlMethod.SelectedItem.Text = "Imprint" = False Then
            willUseImprint = False
            If ddlMethod.SelectedItem.Text.Contains("Refund") Then
                HidePanels(HidePanel._RefundCheque)
                nCurrentPmMethodSelIndex = 4
            ElseIf ddlMethod.SelectedItem.Text = "Hold Imprint" Or ddlMethod.SelectedItem.Text = HOLD_NUMBER Then
                nCurrentPmMethodSelIndex = 1
                HidePanels(HidePanel._CreditCard)
            Else
                HidePanels(ddlMethod.SelectedIndex)
                nCurrentPmMethodSelIndex = ddlMethod.SelectedIndex
            End If

            methodType = ddlMethod.SelectedItem.Text
            GetMethodType(ddlMethod.SelectedItem.Value, methodType)

            If (nCurrentPmMethodSelIndex = 1 Or nCurrentPmMethodSelIndex = 2) AndAlso (Me.pnlPayment_CreditCardAndDebitCard.Visible = True) Then
                Me.btnAddPayment.ValidationGroup = "CreditCardAndDebitCard"
                Me.btnAddPayment.ValidationGroup = "CreditCardAndDebitCard"
                Me.ValidationSummaryPayment.ValidationGroup = "CreditCardAndDebitCard"
                Me.ValidationSummaryPayment.HeaderText = "Error on Credit Card Section"
                Me.txtAmount.Text = Me.txtRunningBalanceChange.Text
                ''rev:mia oct29
                If nCurrentPmMethodSelIndex = 2 Then
                    SetMasking(False)
                Else
                    SetMasking(True)
                    ''rev:mia oct31 -display this control when true
                    If (bDPSRole = True) Then
                        trCardPresent.Visible = True
                    Else
                        RadioButtonMode()
                    End If
                End If
            ElseIf (nCurrentPmMethodSelIndex = 3) AndAlso (Me.pnlTravellersCheque.Visible = True) Then
                Me.btnAddCheck.ValidationGroup = "TravellersCheque"
                Me.ValidationSummaryPayment.ValidationGroup = "TravellersCheque"
                Me.ValidationSummaryPayment.HeaderText = "Error on Travellers Cheque Section"

            ElseIf (nCurrentPmMethodSelIndex = 4) AndAlso (Me.pnlPersonalCheque.Visible = True) Then
                Me.ValidationSummaryPayment.ValidationGroup = "personalcheque"
                Me.ValidationSummaryPayment.HeaderText = "Error on Personal Cheque Section"
                Me.btnAddPayment.ValidationGroup = "personalcheque"
                Me.txt_PersonalCheque_Amount.Text = Me.txtRunningBalanceChange.Text ''Me.txtAmount.Text
                If String.IsNullOrEmpty(Me.txt_PersonalCheque_Amount.Text) Then
                    Me.txt_PersonalCheque_Amount.Text = "0.00"
                End If
            ElseIf (nCurrentPmMethodSelIndex = 5) AndAlso (Me.pnlCash.Visible = True) Then
                Me.ValidationSummaryPayment.ValidationGroup = "cash"
                Me.ValidationSummaryPayment.HeaderText = "Error on Cash Section"
                Me.btnAddPayment.ValidationGroup = "cash"
            ElseIf (nCurrentPmMethodSelIndex = 6) AndAlso (Me.pnlPersonalCheque.Visible = True) Then
                Me.ValidationSummaryPayment.ValidationGroup = "personalcheque"
                Me.ValidationSummaryPayment.HeaderText = "Error on Personal Cheque Section"
                Me.btnAddPayment.ValidationGroup = "personalcheque"
                Me.txt_PersonalCheque_Amount.Text = Me.txtRunningBalanceChange.Text 'Me.txtAmount.Text
                If String.IsNullOrEmpty(Me.txt_PersonalCheque_Amount.Text) Then
                    Me.txt_PersonalCheque_Amount.Text = "0.00"
                End If
            End If

        Else

            HidePanels(HidePanel._CreditCard)
            ''GetMethodType(ddlMethod.SelectedItem.Value, ddlMethod.SelectedItem.Text, ddlMethod.SelectedIndex)
            methodType = ddlMethod.SelectedItem.Text
            GetMethodType(ddlMethod.SelectedItem.Value, methodType)
            Me.btnAddPayment.ValidationGroup = "CreditCardAndDebitCard"
            Me.btnAddPayment.ValidationGroup = "CreditCardAndDebitCard"
            Me.ValidationSummaryPayment.ValidationGroup = "CreditCardAndDebitCard"
            Me.ValidationSummaryPayment.HeaderText = "Error on Credit Card Section"
            Me.trCardPresent.Visible = False

            Me.txtAmount.Text = Me.txtRunningBalanceChange.Text
            If Me.ddlMethod.SelectedItem.Text.Contains("Imprint") Then
                willUseImprint = True
            End If
        End If

        Me.ddlMethod.Enabled = False

        ''rev:mia aug 10 2010
        Dim CHGTOKEN As Boolean = IsBillingTokenPermitted()
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        Dim DEBIT As Boolean = IsMethodContainsDebit()

        DisplayCreditCardAuthority()

        If Me.rdoCardPresent.Items(0).Selected Then
            Me.btnEFTPOS.Enabled = CSREFTPMT
        Else
            Me.btnEFTPOS.Enabled = False
        End If

        If DEBIT Then Me.trdBillingToken.Visible = False
        EnableCardPresentOption()
        If sMode = 1 Then
            If IsMethodContainsThisParameter("Imprint") Then
                Me.btnEFTPOS.Enabled = False
                Me.ddlMethod.Enabled = False
                Me.ddlCardType.Enabled = False
            End If
        Else
            If IsMethodContainsThisParameter("Imprint") Then
                Me.btnEFTPOS.Enabled = False
                Me.ddlMethod.Enabled = False
                Me.ddlCardType.Enabled = False
            End If
        End If

        ' RKS: 26-Jun-2012
        'If Not CSREFTPMT And IsMethodContainsThisParameter("Imprint") Then
        'Me.ddlCardType.Enabled = True
        'End If
        DisabledCVVvalidations()

    End Sub

    Protected Sub ddlCardType_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCardType.DataBound
        BlankValueInDropdown(sender)
    End Sub


    Protected Sub btnAddPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPayment.Click


        Dim validationGroup As String = ""
        Dim validAll As Boolean = True
        If Me.pnlPayment_CreditCardAndDebitCard.Visible = True Then
            If Me.ddlCardType.SelectedItem.Text.Contains("EFT") = True Then
                nCurrentPmMethodSelIndex = 2
            Else
                nCurrentPmMethodSelIndex = 1
            End If

            validationGroup = "CreditCardAndDebitCard"


        ElseIf Me.pnlTravellersCheque.Visible = True Then
            If Me.ddlMethod.SelectedItem.Text.Contains("Travellers Chq") = True Then
                nCurrentPmMethodSelIndex = 3
            End If
            validationGroup = "TravellersCheque"
        ElseIf Me.pnlPersonalCheque.Visible = True Then
            If Me.ddlMethod.SelectedItem.Text.Contains("Personal") = True Then
                nCurrentPmMethodSelIndex = 4
            Else
                nCurrentPmMethodSelIndex = 6
            End If
            validationGroup = "personalcheque"
        ElseIf Me.pnlCash.Visible = True Then
            If Me.ddlMethod.SelectedItem.Text.Contains("Cash") = True Then
                nCurrentPmMethodSelIndex = 5
            End If
            validationGroup = "cash"
        End If

        validAll = IsPageValid(validationGroup)
        If validAll = False Then Exit Sub

        Dim localTxtAmount As Decimal
        If String.IsNullOrEmpty(txtAmount.Text) Then
            localTxtAmount = 0.0
        Else
            localTxtAmount = (txtAmount.Text)
        End If

        Dim localtxtAmountToBePaid As Decimal
        If String.IsNullOrEmpty(txtAmountToBePaid.Text) Then
            localtxtAmountToBePaid = 0.0
        Else
            localtxtAmountToBePaid = (txtAmountToBePaid.Text)
        End If

        ''july23
        Dim cardType As String
        Try
            cardType = Me.ddlCardType.SelectedItem.Value.ToString
        Catch ex As Exception
        End Try

        Select Case nCurrentPmMethodSelIndex
            Case 1, 2 ''credit card

                ''rev:mia april 22 2010
                EncryptedValue = Me.txtCardNumber.Text
                ''rev:mia oct29- validations only in credit card not on eftpos
                If nCurrentPmMethodSelIndex = 1 Then
                    bValidCCardNumber = Me.IsCreditCard(Me.ddlCardType.SelectedItem.Text, Me.txtCardNumber.Text) '' Me.VerifyCreditCard(Me.txtCardNumber.Text)
                    If bValidCCardNumber = False AndAlso String.IsNullOrEmpty(Me.BillingTokenSelect.SelectedValue) Then
                        If Not String.IsNullOrEmpty(Me.litCardNumber.Text) Then
                            Dim cardnumber As String = Me.litCardNumber.Text
                            bValidCCardNumber = Me.IsCreditCard(Me.ddlCardType.SelectedItem.Text, Me.txtCardNumber.Text) ''Me.VerifyCreditCard(cardnumber)
                        End If
                    End If

                    If bValidCCardNumber = False AndAlso Not IsUsingBillingToken() Then
                        MyBase.SetInformationShortMessage("Invalid Card Number")
                        ''rev:mia April 22 2010..if its error..remain encrypted
                        Me.txtCardNumber.Text = EncryptedValue
                        Exit Sub
                    End If

                    Dim isNotExpired As Boolean = IsValidExpirydate(nCurrentPmMethodSelIndex)
                    If isNotExpired = False Then
                        Exit Sub
                    End If
                End If

                AddPayment(PaymentID, _
                             RntId, _
                             cardType, _
                             sType, _
                             localTxtAmount, _
                             localtxtAmountToBePaid, _
                             paymentType, _
                             False, _
                             IsUsingBillingToken)
                ClearCreditCardEftPos()

                HidePanels(HidePanel._All)
                EnableBillingTokenfields(True)

              

            Case 3  ''travellers cheque
                If Me.repTravellersCheque.Items.Count = 0 Then
                    MyBase.SetInformationShortMessage("Please add travellers check!")
                    Exit Sub
                End If

                AddPayment(PaymentID, _
                                  RntId, _
                                  cardType, _
                                  sType, _
                                  localTxtAmount, _
                                  localtxtAmountToBePaid, _
                                  paymentType, _
                                  False)
                HidePanels(HidePanel._All)

            Case 4, 6 ''personal check and refund

                AddPayment(PaymentID, _
                                  RntId, _
                                  cardType, _
                                  sType, _
                                  localTxtAmount, _
                                  localtxtAmountToBePaid, _
                                  paymentType, _
                                  False)
                HidePanels(HidePanel._All)

            Case 5 ''cash

                AddPayment(PaymentID, _
                                                  RntId, _
                                                  cardType, _
                                                  sType, _
                                                  localTxtAmount, _
                                                  localtxtAmountToBePaid, _
                                                  paymentType, _
                                                  False)
                HidePanels(HidePanel._All)


            Case 6 ''refund chq

                AddPayment(PaymentID, _
                                                  RntId, _
                                                  cardType, _
                                                  sType, _
                                                  localTxtAmount, _
                                                  localtxtAmountToBePaid, _
                                                  paymentType, _
                                                  False)
                HidePanels(HidePanel._All)
            Case Else


        End Select

        If Me.ddlMethod.SelectedIndex = -1 Then
            Me.btnAddPayment.Enabled = False
        End If

        If ddlMethod.SelectedItem.Text.Contains("Credit") = True Then
            If Me.pnlPayment_CreditCardAndDebitCard.Visible = False Then
                Me.btnReverse.Enabled = False
            End If
        End If

        ''rev:mia EFTPOS April 8 2010
        If Me.ddlMethod.SelectedIndex = 0 And String.IsNullOrEmpty(ddlMethod.SelectedItem.Text) Then
            Me.btnEFTPOS.Enabled = False
        Else
            EftposButton()
        End If
        Me.trdBillingToken.Visible = IsBillingTokenPermitted()
        Me.BillingTokenSelect.SelectedIndex = -1

        ''rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
        ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
        btnSave.Enabled = Not DisableSaveButton
        DisableSaveButton = False
    End Sub

    Protected Sub chkRemoveRow_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnStatus As Boolean = CType(sender, CheckBox).Checked

        Dim repItem As RepeaterItem = CType(CType(sender, CheckBox).NamingContainer, RepeaterItem)
        If repItem IsNot Nothing Then
            Dim litID As Literal = CType(repItem.FindControl("litID"), Literal)

            If blnStatus = True Then
                Litidtoremove.Text = Litidtoremove.Text & litID.Text & ","
            Else
                Litidtoremove.Text = Litidtoremove.Text.Replace(litID.Text & ",", "")
            End If
        End If

        For Each repCheckitem As RepeaterItem In Me.RepPaymentEntries.Items
            If repCheckitem IsNot Nothing Then
                If Me.RepPaymentEntries.Items.Count = 1 Then
                    Dim chkRemove As CheckBox = CType(repItem.FindControl("chkRemove"), CheckBox)
                    If chkRemove IsNot Nothing Then
                        chkRemove.Checked = blnStatus
                    End If
                End If
            End If
        Next

    End Sub

    Protected Sub chkRemove_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnStatus As Boolean = CType(sender, CheckBox).Checked
        CheckedRemove = blnStatus
        Litidtoremove.Text = ""
        For Each repitem As RepeaterItem In Me.RepPaymentEntries.Items
            If repitem IsNot Nothing Then
                Dim litID As Literal = CType(repitem.FindControl("litID"), Literal)

                If blnStatus = True Then
                    Litidtoremove.Text = Litidtoremove.Text & litID.Text & ","
                Else
                    Litidtoremove.Text = ""
                End If

                Dim chkRemoveRow As CheckBox = CType(repitem.FindControl("chkRemoveRow"), CheckBox)
                If chkRemoveRow IsNot Nothing Then
                    chkRemoveRow.Checked = blnStatus
                End If
            End If
        Next

    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        LabelWarningForEftpos.Text = ""
        If Litidtoremove.Text = "" Then
            MyBase.SetInformationShortMessage("Please select payment entry to be remove!")
            Exit Sub
        End If
        If Litidtoremove.Text.EndsWith(",") Then
            Litidtoremove.Text = Litidtoremove.Text.Remove(Litidtoremove.Text.Length - 1, 1)
        End If

        If xmlPaymentDetails Is Nothing Then xmlPaymentDetails = New XmlDocument
        If xmlPaymentDetails.OuterXml = "" Then
            xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))
        End If

        Dim aryID() As String = Litidtoremove.Text.Split(",")
        For Each id As String In aryID
            Dim node As XmlNode = xmlPaymentDetails.SelectSingleNode("PaymentEntryRoot/PaymentEntry[@id='" & id & "']")
            xmlPaymentDetails.DocumentElement.RemoveChild(node)
        Next
        Litidtoremove.Text = ""
        If xmlPaymentDetails.DocumentElement.HasChildNodes = False Then
            xmlPaymentDetails = Nothing
            xmlPaymentDetails = New XmlDocument
            litxmlPaymentDetails.Text = ""
        End If


        ''rev:mia nov6
        litxmlTravellerChequeDetails.Text = ""
        CalculateBalance()
        BindGrid()
    End Sub

    'Feb 12 2010 - DPS eftpos integration - manny
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        LabelWarningForEftpos.Text = ""
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        Dim appliedOverride As Boolean = appliedOverrideBox()

        ''rev:mia April 13 2011 -- check if this booking is already Checkin then display override0
        '' we need to this one to avoid possible refund even if its already checkin.
        If IsStatusCheckin(Me.RntId) Then
            SupervisorOverrideControl1.Show()
        Else

            'rev:mia Aug 7 2012note: All payments/refund made against billing token regardless of any countries 
            '                  will display a new popup message.

            If (isBillingTokenHasBeenUsedAndNotImprint()) Then
                cbPaymentAgainstBillingToken.Text = BILLING_TOKEN_MSG
                cbPaymentAgainstBillingToken.Show()
            Else

                If (CardPresent) Or DebitCard = "EFT-POS" Then

                    ''if its a member of this role..display popup
                    ''another surprises..
                    If (CSREFTPMT And appliedOverride) Then
                        ''rev:mia rev:mia 07-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1119
                        If (CardPresent) Then
                            SupervisorOverrideControl1.Show()
                        ''ElseIf (Not CardPresent And DebitCard = "EFT-POS") Then
                        ''    SupervisorOverrideControl1.Show()
                        Else
                            HiddenIsOverride.Value = "False"
                            SavingRoutine()
                        End If

                    Else
                        ''rev:mia rev:mia 06-Jan-2017 https://thlonline.atlassian.net/browse/AURORA-1119
                        ''If (Not CSREFTPMT And appliedOverride) Then
                        ''    If (CardPresent) Then
                        ''        SupervisorOverrideControl1.Show()
                        ''    ElseIf (Not CardPresent And DebitCard = "EFT-POS") Then
                        ''        SupervisorOverrideControl1.Show()
                        ''    Else
                        ''        HiddenIsOverride.Value = "False"
                        ''        SavingRoutine()
                        ''    End If
                        ''Else
                        ''    HiddenIsOverride.Value = "False"
                        ''    SavingRoutine()
                        ''End If
						HiddenIsOverride.Value = "False"
                        SavingRoutine()
                    End If

                Else

                    If (CSREFTPMT And appliedOverride) Then
                        If isBillingTokenHasBeenUsed() And sMode = 0 Then
                            If CDec(Me.txtAmountToBePaid.Text) < 0 Then
                                SupervisorOverrideControl1.Show()
                            Else
                                HiddenIsOverride.Value = "False"
                                SavingRoutine()
                            End If
                        Else
                            ''SupervisorOverrideControl1.Show()
                            ''rev:mia rev:mia 07-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1119
                            HiddenIsOverride.Value = "False"
                            SavingRoutine()
                        End If
                    Else
                        HiddenIsOverride.Value = "False"
                        SavingRoutine()
                    End If ''If (CSREFTPMT And appliedOverride) Then

                End If ''If (CardPresent) Or DebitCard = "EFT-POS" Then

            End If
        End If
    End Sub

    Protected Sub BtnbackToBooking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnbackToBooking.Click
        GoBack()
    End Sub

    Protected Sub btnReverse_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReverse.Click
        LabelWarningForEftpos.Text = ""
        Logging.LogWarning("PAYMENT SCREEN: ReversePayment", ">>>>>>>>>>>>>>>>>>>>>>>>>>>START: hitting the btnReverse".ToUpper)
        lblWarning.Visible = True
        Dim DPSPMT As Boolean = CBool(bDPSRole)
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        Logging.LogWarning("PAYMENT SCREEN: DPSPMT ROLE: ", DPSPMT)
        Logging.LogWarning("PAYMENT SCREEN: DPSTransactionId: ", DPSTransactionId)

        ''rev:mia April 13 2011 -- check if this booking is already Checkin then display override
        '' we need to this one to avoid possible refund even if its already checkin.
        If IsStatusCheckin(Me.RntId) Then
            SupervisorOverrideControl1.Show()
        Else

            If Not CSREFTPMT Then
                Logging.LogWarning("PAYMENT SCREEN:", "1. NORMAL REFUND FOR AU AND NZ USER AND CSREFPMT:" & CSREFTPMT)
                Me.ReversePayment()
            Else
                ''rev:mia dec 17 2010 - only for au
                If (Not String.IsNullOrEmpty(Me.DPSTransactionId)) Then
                    If Not DPSPMT Then

                        ''CHECK IF HIS PART OF CSREFPMT
                        Dim isDPZorDPA As Boolean = isPaymentComeFromDPZorDPAorDPM(PaymentComeFromDPZorDPAorDPM)
                        Dim isDPZorDPAimprint As Boolean = isPaymentComeFromDPZorDPAorDPMAndImprint(PaymentComeFromDPZorDPAorDPMandImprint)

                        ''part of csref and its a selfcheckin 
                        If CSREFTPMT And isDPZorDPA And isDPZorDPAimprint Then
                            Logging.LogWarning("PAYMENT SCREEN:", "Call ReversePayment()..  isDPZorDPA and isDPZorDPAimprintis is TRUE")
                            Me.ReversePayment()
                        Else
                            If CSREFTPMT And isDPZorDPA And Not isDPZorDPAimprint Then
                                Logging.LogWarning("PAYMENT SCREEN:", "isDPZorDPA and isDPZorDPAimprintis is FALSE")
                                Me.cbDPSPMTnoRoleWithDPZpayment.Show()
                            Else
                                Logging.LogWarning("PAYMENT SCREEN:", "FALLBACK FOR REVERSE BUTTON")
                                Me.ReversePayment()
                            End If
                        End If

                    Else
                        Logging.LogWarning("PAYMENT SCREEN:", "  REVERSING AFTER CHECKING DPSPYMT")
                        Me.ReversePayment()
                    End If
                Else
                    ''POSSIBLE CCA WAS USED
                    Logging.LogWarning("PAYMENT SCREEN:", "2. NORMAL REFUND FOR AU AND NZ USER AND CSREFPMT:" & CSREFTPMT)
                    Me.ReversePayment()
                End If
            End If
        End If

        Logging.LogWarning("PAYMENT SCREEN: ReversePayment", "END: hitting the btnReverse<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<".ToUpper & vbCrLf & vbCrLf)
    End Sub

    Protected Sub btnTransferBond_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransferBond.Click
        ''TransferBondToOtherBooRnt()
        ConfirmationBoxControl_bondreverse.Show()
    End Sub



    Sub chkRemoveTravellersCheque_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnStatus As Boolean = CType(sender, CheckBox).Checked
        CheckedRemove = blnStatus
        Litidtoremove.Text = ""
        For Each repCheckitem As RepeaterItem In Me.repTravellersCheque.Items
            If repCheckitem IsNot Nothing Then
                Dim litID As Literal = CType(repCheckitem.FindControl("litTravellersCheckRowRemoveID"), Literal)

                If blnStatus = True Then
                    Litidtoremove.Text = Litidtoremove.Text & litID.Text & ","
                Else
                    Litidtoremove.Text = ""
                End If

                Dim chkRemoveRow As CheckBox = CType(repCheckitem.FindControl("chkTravellersCheckRowRemove"), CheckBox)
                If chkRemoveRow IsNot Nothing Then
                    chkRemoveRow.Checked = blnStatus
                End If
            End If
        Next
    End Sub

    Sub btnRemoveTravellersCheque_Click(ByVal sender As Object, ByVal e As EventArgs)
        Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(litxmlTravellerChequeDetails.Text))
        If Litidtoremove.Text = "" Then
            MyBase.SetInformationShortMessage("Please select payment entry to be remove!")
            Exit Sub
        End If
        If Litidtoremove.Text.EndsWith(",") Then
            Litidtoremove.Text = Litidtoremove.Text.Remove(Litidtoremove.Text.Length - 1, 1)
        End If


        If Me.xmlTravellerChequeDetails Is Nothing Then Me.xmlTravellerChequeDetails = New XmlDocument
        If Me.xmlTravellerChequeDetails.OuterXml = "" Then
            Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(litxmlTravellerChequeDetails.Text))
        End If

        Dim aryID() As String = Litidtoremove.Text.Split(",")
        For Each id As String In aryID
            Dim node As XmlNode = Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id='" & id & "']")
            Me.xmlTravellerChequeDetails.DocumentElement.RemoveChild(node)
        Next
        Litidtoremove.Text = ""
        BindToTravellersGrid()
    End Sub

    Sub chkTravellersCheckRowRemove_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnStatus As Boolean = CType(sender, CheckBox).Checked

        Dim repItem As RepeaterItem = CType(CType(sender, CheckBox).NamingContainer, RepeaterItem)
        If repItem IsNot Nothing Then
            Dim litID As Literal = CType(repItem.FindControl("litTravellersCheckRowRemoveID"), Literal)

            If blnStatus = True Then
                Litidtoremove.Text = Litidtoremove.Text & litID.Text & ","
            Else
                Litidtoremove.Text = Litidtoremove.Text.Replace(litID.Text & ",", "")
            End If
        End If

        For Each repCheckitem As RepeaterItem In Me.repTravellersCheque.Items
            If repCheckitem IsNot Nothing Then
                If Me.repTravellersCheque.Items.Count = 1 Then
                    Dim chkRemove As CheckBox = CType(repItem.FindControl("chkTravellersCheckRowRemove"), CheckBox)
                    If chkRemove IsNot Nothing Then
                        chkRemove.Checked = blnStatus
                    End If
                End If
            End If
        Next
    End Sub

    Protected Sub btnAddCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddCheck.Click

        Dim isValid As Boolean = Me.IsPageValid("TravellersCheque")
        If isValid = False Then Exit Sub
        TravellerChequeRoutine()

    End Sub

    ''rev:mia nov11
    Protected Sub repTravellersCheque_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repTravellersCheque.ItemCreated
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim txtbox As TextBox = CType(e.Item.FindControl("rep_Trav_Amount"), TextBox)
            If txtbox IsNot Nothing Then
                txtbox.AutoPostBack = True
                txtbox.Attributes.Add("onblur", "TravellerscheckCurrency(this);")
                AddHandler txtbox.TextChanged, AddressOf txtBox_textchanged
            End If
        End If
    End Sub
    Sub txtBox_textchanged(ByVal sender As Object, ByVal e As EventArgs)
        Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(litxmlTravellerChequeDetails.Text))
        Dim template As RepeaterItem = CType(sender, TextBox).NamingContainer
        If template IsNot Nothing Then
            Dim idLit As Literal = CType(template.FindControl("litTravellersCheckRowRemoveID"), Literal)
            Dim ddl As DropDownList = CType(template.FindControl("rep_TravellersCheque_Currency"), DropDownList)
            Dim szAmount As Decimal
            If sender.text > "0" Then
                szAmount = CDec(sender.text)
            Else
                szAmount = CDec("0.00")
            End If
            Dim szCurrencyDesc As String = ddl.SelectedItem.Text
            Dim szCurrency As String = ddl.Text
            Dim valAmountLocal As String = getLocalAmountGeneric(BaseLocalcurrency, szCurrencyDesc, szAmount)
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/Amount").InnerText = szAmount
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/CurrencyDesc").InnerText = szCurrencyDesc
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/Currency").InnerText = szCurrency
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/PdtLocalAmount").InnerText = CDec(valAmountLocal)
            ''
            Dim ds As New DataSet
            ds.ReadXml(New XmlTextReader(xmlTravellerChequeDetails.OuterXml, System.Xml.XmlNodeType.Document, Nothing))
            Me.repTravellersCheque.DataSource = ds.Tables("CheckEntry")
            Me.repTravellersCheque.DataBind()

            Dim localhash As Hashtable = getCurrencyHashContent(ddl)
            SplitCurrency(CType(ddl, DropDownList), localhash)

            litxmlTravellerChequeDetails.Text = Server.HtmlEncode(xmlTravellerChequeDetails.OuterXml)
        End If

    End Sub

    Protected Sub repTravellersCheque_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repTravellersCheque.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim rep_TravellersCheque_Currency As DropDownList = CType(e.Item.FindControl("rep_TravellersCheque_Currency"), DropDownList)
            If rep_TravellersCheque_Currency IsNot Nothing Then
                ''rev:mia nov10
                Dim template As RepeaterItem = CType(rep_TravellersCheque_Currency, DropDownList).NamingContainer
                Dim idLit As Literal = CType(template.FindControl("litTravellersCheckRowRemoveID"), Literal)
                PopulateDropdowns("", rep_TravellersCheque_Currency, "", False, idLit.Text)
            End If
        End If
    End Sub

    Protected Sub rep_TravellersCheque_Currency_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        ''Dim localhash As Hashtable = getCurrencyHashContent(sender)
        ''SplitCurrency(CType(sender, DropDownList), localhash)

        ''rev:mia nov10
        Me.xmlTravellerChequeDetails.LoadXml(Server.HtmlDecode(litxmlTravellerChequeDetails.Text))
        Dim template As RepeaterItem = CType(sender, DropDownList).NamingContainer
        If template IsNot Nothing Then
            Dim idLit As Literal = CType(template.FindControl("litTravellersCheckRowRemoveID"), Literal)

            Dim szAmount As String = Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/Amount").InnerText
            Dim szCurrencyDesc As String = sender.selecteditem.text
            Dim szCurrency As String = sender.text
            Dim valAmountLocal As String = getLocalAmountGeneric(BaseLocalcurrency, szCurrencyDesc, szAmount)
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/CurrencyDesc").InnerText = szCurrencyDesc
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/Currency").InnerText = szCurrency
            Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/PdtLocalAmount").InnerText = valAmountLocal


            ''rev:mia dec2
            ''----------------------------------------------------------------------------------------------------------------
            Dim localCheque As String = ""
            Dim foreignCheque As String = ""
            Dim strTemp As String = ""
            If xmlPaymentMethods.OuterXml = "" Then
                xmlPaymentMethods.LoadXml(Server.HtmlDecode(litxmlPaymentMethods.Text))
            End If
            Dim tempNode As XmlNode = xmlPaymentMethods.SelectSingleNode("//data")
            For k As Integer = 0 To tempNode.ChildNodes.Count - 1
                strTemp = tempNode.ChildNodes(k).ChildNodes.Item(1).InnerText.ToLower
                Dim indexOfLocal As Integer = strTemp.IndexOf("local")
                If indexOfLocal > -1 Then
                    localCheque = tempNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                Else
                    foreignCheque = tempNode.ChildNodes(k).ChildNodes.Item(0).InnerText
                End If

            Next

            If DefaultCurrency = szCurrency Then
                Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/Method").InnerText = "Travellers Chq - Local"
                Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/SubMethod").InnerText = localCheque
            Else
                Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/Method").InnerText = "Travellers Chq - Foreign"
                Me.xmlTravellerChequeDetails.SelectSingleNode("TravellerChqRoot/CheckEntry[@id= '" & idLit.Text & "']/SubMethod").InnerText = foreignCheque
            End If

            ''----------------------------------------------------------------------------------------------------------------


            Dim ds As New DataSet
            ds.ReadXml(New XmlTextReader(xmlTravellerChequeDetails.OuterXml, System.Xml.XmlNodeType.Document, Nothing))
            Me.repTravellersCheque.DataSource = ds.Tables("CheckEntry")
            Me.repTravellersCheque.DataBind()

            Dim localhash As Hashtable = getCurrencyHashContent(sender)
            SplitCurrency(CType(sender, DropDownList), localhash)

            litxmlTravellerChequeDetails.Text = Server.HtmlEncode(xmlTravellerChequeDetails.OuterXml)
        End If



    End Sub


    Protected Sub txt_Cash_amount1_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Cash_amount1.TextChanged
        CashUpdate(sender, Me.ddl_Cash_Currency1, Me.lbl_Cash_amount1)
    End Sub

    Protected Sub txt_Cash_amount2_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Cash_amount2.TextChanged
        CashUpdate(sender, Me.ddl_Cash_Currency2, Me.lbl_Cash_amount2)
    End Sub

    Protected Sub txt_Cash_amount3_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txt_Cash_amount3.TextChanged
        CashUpdate(sender, Me.ddl_Cash_Currency3, Me.lbl_Cash_amount3)
    End Sub

    Protected Sub btnRemoveCash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemoveCash.Click

        If Me.chk_Cash_Check1.Checked = True Then
            Me.txt_Cash_amount1.Text = ""
            Me.lbl_Cash_amount1.Text = "0.00"
            CashUpdate(Me.txt_Cash_amount1, Me.ddl_Cash_Currency1)
            Me.chk_Cash_Check1.Checked = False
        End If

        If Me.chk_Cash_Check2.Checked = True Then
            Me.txt_Cash_amount2.Text = ""
            Me.lbl_Cash_amount2.Text = "0.00"
            CashUpdate(Me.txt_Cash_amount2, Me.ddl_Cash_Currency2)
            Me.chk_Cash_Check2.Checked = False
        End If

        If Me.chk_Cash_Check3.Checked = True Then
            Me.txt_Cash_amount3.Text = ""
            Me.lbl_Cash_amount3.Text = "0.00"
            CashUpdate(Me.txt_Cash_amount3, Me.ddl_Cash_Currency3)
            Me.chk_Cash_Check3.Checked = False
        End If
    End Sub

    Protected Sub chkCash_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnSenderCheck As Boolean = CType(sender, CheckBox).Checked

        Me.chk_Cash_Check1.Checked = blnSenderCheck
        Me.chk_Cash_Check2.Checked = blnSenderCheck
        Me.chk_Cash_Check3.Checked = blnSenderCheck

    End Sub

    Protected Sub ddl_Cash_Currency1_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        CashUpdate(Me.txt_Cash_amount1, sender, Me.lbl_Cash_amount1)
    End Sub

    Protected Sub ddl_Cash_Currency2_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        CashUpdate(Me.txt_Cash_amount2, sender, Me.lbl_Cash_amount2)
    End Sub

    Protected Sub ddl_Cash_Currency3_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        CashUpdate(Me.txt_Cash_amount3, sender, Me.lbl_Cash_amount3)
    End Sub

    Protected Sub RepPaymentEntries_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepPaymentEntries.ItemCreated
        If e.Item.ItemType = ListItemType.Header Then
            Dim litcur As Literal = CType(e.Item.FindControl("litcur"), Literal)
            If litcur IsNot Nothing Then
                If xmlPaymentmgt.OuterXml <> "" Then
                    litcur.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
                End If
            End If
        End If
    End Sub



    Protected Sub RepPaymentEntries_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles RepPaymentEntries.ItemDataBound
        If e.Item.ItemType = ListItemType.Header Then
            Dim btnremove As Button = CType(e.Item.FindControl("btnremove"), Button) ''CType(Me.repTravellersCheque.Controls(0).FindControl("btnremove"), Button)
            If btnremove IsNot Nothing Then
                btnremove.Attributes.Add("onclick", "return MsgRemoveRecord();")
            End If
            Dim litcur As Literal = CType(e.Item.FindControl("litcur"), Literal)
            If litcur IsNot Nothing Then
                litcur.Text = xmlPaymentmgt.SelectSingleNode("Root/Data/Payment/CurrencyDesc").InnerText
            End If
        End If
    End Sub

    Protected Sub ConfirmationBoxControlExtended_PostBack(ByVal sender As Object, _
                                                  ByVal leftButton As Boolean, _
                                                  ByVal rightButton As Boolean, _
                                                  ByVal centerButton As Boolean, _
                                                  ByVal param As String) Handles ConfirmationBoxControlExtended1.PostBack


        If leftButton Then
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "LeftButton (reverse)".ToUpper)

            ''btneftpos_click(sender, e)
            ''Feb 15 2010 - DPS eftpos integration - manny
            ''ReversePaymentPopaction("1")

            ''rev:mia April 30 2010 - another popup was added.wheew
            IsCardPresentWhenRefunding = True
            ConfirmationBoxControlEFTPOS.Show()
            ''SupervisorOverrideControl1.Show()
        ElseIf rightButton Then
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "RightButton (reverse)".ToUpper)

            ''Feb 15 2010 - DPS eftpos integration - manny
            ''ReversePaymentPopaction("Reverse")
            IsCardPresentWhenRefunding = False
            ReversePaymentPopaction("0")

        Else
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlExtended_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "Cancel (unreverse)".ToUpper)
            ReversePaymentPopaction("UnReverse")
        End If


    End Sub

    Protected Sub ddlCardType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCardType.SelectedIndexChanged
        ''rev:mia sept16
        If ddlCardType.SelectedItem.Value.ToString <> "0" Then
            ViewState("CardType") = ddlCardType.SelectedItem.Value
            DebitCard = ddlCardType.SelectedItem.Text
        End If
        Dim isactive As Boolean = HasPaymentEntries()

        EftposButton()
        DisableEftposButtonWhenMethodIsDebit()
        RadioButtonMode()
        Me.ddlCardType.Enabled = Not IsMethodContainsThisParameter("Imprint")
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        If Not CSREFTPMT And IsMethodContainsThisParameter("Imprint") Then
            Me.ddlCardType.Enabled = True
        End If

        ''rev:mia march 12 2012 -- Added CVC2
        CvvLength()
    End Sub

    Protected Sub ConfirmationBoxControl_bondreverse_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl_bondreverse.PostBack
        If leftButton Then
            TransferBondToOtherBooRnt()
        End If
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
    End Sub


#End Region

#Region "rev:mia december 19,2008"
    Function isSameCurrency() As Boolean
        If String.IsNullOrEmpty(litPaymentRequest.Text) Then Exit Function

        Dim tempXML As New XmlDocument
        tempXML.LoadXml(Server.HtmlDecode(litPaymentRequest.Text))

        If (tempXML.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency")) IsNot Nothing Then
            Return IIf(DefaultCurrency = tempXML.ChildNodes(0).SelectSingleNode("//AmountsOutstandingRoot/AmountsOutstanding/Currency").InnerText, True, False)
        End If
    End Function

    Protected Sub rdoCardPresent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoCardPresent.SelectedIndexChanged

        Dim sameCurrency As Boolean = isSameCurrency()

        ''Feb 12 2010 - DPS eftpos integration - manny
        Dim value As Integer
        Try
            value = rdoCardPresent.SelectedItem.Value
        Catch ex As Exception
            value = 1
        End Try

        btnEFTPOS.Enabled = IIf(value = 1, True, False)
        If btnEFTPOS.Enabled Then
            btnEFTPOS.Enabled = HasPaymentEntries()
        End If
        EftposButton()
        CardPresent = value

        ''Dim DEBIT As Boolean = IsMethodContainsDebit()

        If (value = 1) Then
            Me.btnEFTPOS.Enabled = EFTButtonForCSR()
        Else
            Me.btnEFTPOS.Enabled = False
            
        End If

        ''rev:mia march 12 2012 -- Added CVC2
        DisabledCVVvalidations()

        If IsUsingBillingToken() Then
            HidePanels(HidePanel._CreditCard)
        End If

        If sMode = NEW_PAYMENT Then

            If rdoCardPresent.SelectedIndex = 1 Then
                If sameCurrency AndAlso rdoCardPresent.SelectedIndex = 1 Then Exit Sub
                If Not sameCurrency AndAlso rdoCardPresent.SelectedIndex = 0 Then Exit Sub
                GetCurrencyBasedOnTravelDestination = True
                Me.SetInformationShortMessage(CHANGE_CURRENCY)

                LoadXMLSettings()
                HidePanels(HidePanel._CreditCard)
                LoadScreen(True)
            Else

                GetCurrencyBasedOnTravelDestination = False
                LoadXMLSettings()
                HidePanels(HidePanel._CreditCard)
                LoadScreen(True)
                Me.ddlMethod.SelectedItem.Text = methodType
                Me.btnEFTPOS.Enabled = EFTButtonForCSR()
            End If

            Me.ddlMethod.Enabled = False
        End If

       

    End Sub

    Private Property GetCurrencyBasedOnTravelDestination() As Boolean
        Get
            Return ViewState("GetCurrencyBasedOnTravelDestination")
        End Get
        Set(ByVal value As Boolean)
            ViewState("GetCurrencyBasedOnTravelDestination") = value
        End Set
    End Property

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        LabelWarningForEftpos.Text = ""
        Me.BillingTokenSelect.SelectedIndex = -1
        CancelScreen()
        ''rev:mia dec22
        If GetCurrencyBasedOnTravelDestination = True Then
            GetCurrencyBasedOnTravelDestination = False
            Me.SetInformationShortMessage(CHANGE_CURRENCY_BACK)
            LoadXMLSettings()
            HidePanels(HidePanel._All)
            LoadScreen()
            Me.rdoCardPresent.SelectedIndex = -1
        End If
        ''BreakHere()
        BillingTokenSelect_SelectedIndexChangedExtracted()
        Me.ddlMethod.SelectedIndex = -1
        DisableEftposButtonWhenMethodIsEmpty()

        Me.trdBillingToken.Visible = False

        clearHashTable()

    End Sub
#End Region

#Region "rev:mia Jan 13 2009"
    Function IsValidaRentalCompany(ByVal rentalID As String, ByVal usercode As String, ByRef returnedMessage As String) As Boolean
        returnedMessage = ""
        Dim params(2) As Aurora.Common.Data.Parameter
        params(0) = New Aurora.Common.Data.Parameter("sRntId", DbType.String, rentalID, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sUsrCode", DbType.String, usercode, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sRetErrorMsg", DbType.String, 1000, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Try
            Aurora.Common.Data.ExecuteOutputSP("RES_ValidateRentalCompany", params)
            returnedMessage = params(2).Value

        Catch ex As Exception
            returnedMessage = "Error encountered in validating Rental Company"
            Return False
        End Try

        Logging.LogWarning("PAYMENT SCREEN: IsValidaRentalCompany: ", vbCrLf & vbCrLf & String.Concat("returnedMessage: ", IIf(String.IsNullOrEmpty(returnedMessage), _
                                                                      "<empty>", returnedMessage), _
                                                                      " Parameter passed: ", rentalID & " and " & usercode).ToUpper)
        Return IIf(String.IsNullOrEmpty(returnedMessage), True, False)

    End Function
#End Region

#Region "rev:mia Feb 3 2009"
    Function AppendedSaveLog() As String
        Return "(New Payment: " & Me.RentalID.ToUpper & ")"
    End Function
    Function AppendedReverseLog() As String
        Return "(Reverse Payment: " & Me.RentalID.ToUpper & ")"
    End Function
#End Region

#Region "Nov 10 2009 - MAC DPS"
    Private _macUsername As String
    Private _macPassword As String

    Private ReadOnly Property MACUsername() As String
        Get
            Return _macUsername
        End Get
    End Property

    Private ReadOnly Property MACPassword() As String
        Get
            Return _macPassword
        End Get
    End Property

    Private Sub GetMACCredentials(ByVal country As String)
        Dim xml As New XmlDocument

        Try
            xml.Load(Server.MapPath("~/App_Data/MACdps.xml"))

            Dim xpathusername As String = "mac/mac_" & country & "_username"
            Dim xpathpassword As String = "mac/mac_" & country & "_password"
            _macUsername = xml.SelectSingleNode(xpathusername).InnerText
            _macPassword = xml.SelectSingleNode(xpathpassword).InnerText

        Catch ex As Exception
            _macUsername = ""
            _macPassword = ""
        End Try
    End Sub
#End Region

#Region "EFTPOS"
    ''rev:mia april 16 2010
    Private Property IsEftposPayment() As Boolean
        Get
            Return ViewState("IsEftposPayment")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsEftposPayment") = value
        End Set
    End Property

    Protected Sub SupervisorOverrideControl1_PostBack(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBack
        IsReasonsNeedToLog = True
        Dim sReturnMsg As String = ""
        Dim role As String = ConfigurationManager.AppSettings("OverrideEFTPOSRole").TrimEnd
        If isOkButton Then

            sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, role)
            If (sReturnMsg) = "True" Then
                Me.HiddenIsOverride.Value = "True"
                If (Me.ddlMethod.SelectedItem.Text.Contains("Credit") And HiddenPurchase.Value = "Refund") Then
                    ReversePaymentPopaction("1")
                ElseIf HiddenPurchase.Value = "Refund" And Me.ddlMethod.SelectedItem.Text.Contains("Debit") Then
                    ReversePaymentPopaction("Reverse")
                ElseIf HiddenPurchase.Value = "Refund" And (Not Me.ddlMethod.SelectedItem.Text.Contains("Debit") Or Not Me.ddlMethod.SelectedItem.Text.Contains("Credit")) Then
                    ReversePaymentPopaction("Reverse")
                ElseIf HiddenPurchase.Value = "Payment" And LitMode.Text = 0 Then
                    SavingRoutine()
                ElseIf LitMode.Text = 1 And txtAmount.Text.Contains("-") Then
                    ReversePaymentPopaction("Reverse")
                ElseIf sMode = 1 Then
                    ReversePaymentPopaction("Reverse")
                ElseIf sMode = 0 Then
                    SavingRoutine()
                End If
            Else
                If (btnEFTPOS.Text.Equals("EFTPOS Refund")) Then
                    btnReverse.Enabled = True
                    btnEFTPOS.Enabled = True
                    EftposButton()
                End If
                ''rev:mia issue #111
                '' SupervisorOverrideControl1.SetReason = IIf(HiddenPurchase.Value = "Payment", 7, 8)
                SetErrorShortMessage(sReturnMsg)
            End If
        End If
    End Sub


    Sub SavingRoutine()
        Logging.LogWarning("PAYMENT SCREEN: SavingRoutine", ">>>>>>>>>>>>>>>>>>>>>>>>>>>START: hitting the btnsave".ToUpper)

        Dim retMessage As String = ""
        If Not IsValidaRentalCompany(Me.RntId, Me.UserCode, retMessage) Then
            Me.SetErrorShortMessage(retMessage)
            Exit Sub
        End If

        lblWarning.Visible = True
        SavePayment()
        Logging.LogWarning("PAYMENT SCREEN: SavingRoutine", "END: hitting the btnsave<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<".ToUpper)
    End Sub

    Protected Sub ConfirmationBoxControl_NoDPSTransaction_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles ConfirmationBoxControl_NoDPSTransaction.PostBack
        If leftButton Then
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControl_NoDPSTransaction_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "LeftButton (reverse)".ToUpper)

            SupervisorOverrideControl1.Show()
        End If
    End Sub

    Private _IsReasonsNeedToLog As Boolean
    Private Property IsReasonsNeedToLog() As Boolean
        Get
            Return _IsReasonsNeedToLog
        End Get
        Set(ByVal value As Boolean)
            _IsReasonsNeedToLog = value
        End Set
    End Property

    Private Property IsRefund() As Boolean
        Get
            Return ViewState("IsRefund")
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsRefund") = value
        End Set
    End Property

    Private Property DebitCard() As String
        Get
            Return ViewState("isDebitCard")
        End Get
        Set(ByVal value As String)
            ViewState("isDebitCard") = value
        End Set
    End Property

    Private Property CardPresent() As Boolean
        Get
            Return ViewState("cardpresent")
        End Get
        Set(ByVal value As Boolean)
            ViewState("cardpresent") = value
        End Set
    End Property

    Protected Sub timerStatus_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerStatus.Tick
        timerStatus.Enabled = False
        If IsRefund Then

            If (Me.btnReverse.Enabled) Then Me.btnReverse.Enabled = IsRefund
            If (Me.btnReverse.Enabled) Then Me.btnEFTPOS.Enabled = IsRefund And EFTButtonForCSR()
            If Me.willUseImprint Then
                If (Me.btnReverse.Enabled) Then Me.btnEFTPOS.Enabled = False
            End If


            EftposButton()
        End If
    End Sub

    Sub EftposButton()
        ''If (sRntCtyCode <> "NZ") Then btnEFTPOS.Enabled = False
        If Not String.IsNullOrEmpty(IsRevPmtID) Then
            btnEFTPOS.Enabled = False
        Else
            btnEFTPOS.Enabled = EFTButtonForCSR()
        End If

        HiddenLocation.Value = sRntCtyCode
    End Sub

    Sub MapReasonValue()

        Dim item As ListItem = Nothing


        For Each item In Me.ddlReasonVerified.Items
            If item.Text.Contains("Override Save Payment") Then
                HiddenReasonOverrideSaveValue.Value = item.Value
            End If

            If item.Text.Contains("Override Refund Payment") Then
                HiddenReasonOverridePaymentValue.Value = item.Value
            End If


            If item.Text.Contains("Other") Then
                HiddenReasonOther.Value = item.Value
            End If

        Next
    End Sub

    Sub ReasonReload(Optional ByVal displaytype As String = "")
        Me.ddlReasonVerified.Items.Clear()
        Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
        If (displaytype = "REFUND") Then
            Me.ddlReasonVerified.AppendDataBoundItems = True
            Me.ddlReasonVerified.Items.Add("Please select reason")
        End If

        Me.ddlReasonVerified.DataSource = objEFTPOS.RetrieveEftposReasons(displaytype)
        Me.ddlReasonVerified.DataValueField = "PmtReasonID"
        Me.ddlReasonVerified.DataTextField = "PmtReasonText"
        Me.ddlReasonVerified.DataBind()
        Me.ddlReasonVerified.SelectedIndex = -1
        MapReasonValue()
        objEFTPOS = Nothing
    End Sub

    Private _EncryptedValue As String
    Private Property EncryptedValue() As String
        Get
            Return _EncryptedValue
        End Get
        Set(ByVal value As String)
            _EncryptedValue = value
        End Set
    End Property

    Protected Sub ConfirmationBoxControlEFTPOS_PostBack1(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal centerButton As Boolean, ByVal param As String) Handles ConfirmationBoxControlEFTPOS.PostBack
        If leftButton Then
            ''Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlEFTPOS_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "LeftButton (reverse)".ToUpper)
            willUseEFTPOS = False
        ElseIf rightButton Then
            willUseEFTPOS = True
            Logging.LogWarning(Me.AppendedReverseLog & "PAYMENT SCREEN:ConfirmationBoxControlEFTPOS_PostBack", vbCrLf & vbCrLf & "button click: ".ToUpper & "RightButton (reverse)".ToUpper)
            SupervisorOverrideControl1.Show()
        End If
    End Sub

#End Region

#Region "rev:mia June 30 2010 - add message for miXed payment"
    Sub EFTPOSallowedAndDisplayMSG(ByVal isactive As Boolean)

        If (Me.btnEFTPOS.Enabled) Then
            Me.btnEFTPOS.Enabled = isactive

            If Not isactive Then
                ''Me.SetInformationShortMessage(EFTPOS_MIX_MSG)
                LabelWarningForEftpos.Text = EFTPOS_MIX_MSG
            Else
                LabelWarningForEftpos.Text = ""
            End If
        Else

            If (Me.btnEFTPOS.Enabled) Then
                isactive = HasPaymentEntries()
                Me.btnEFTPOS.Enabled = isactive

                If Not isactive Then
                    LabelWarningForEftpos.Text = EFTPOS_MIX_MSG
                Else
                    LabelWarningForEftpos.Text = ""
                End If
            End If

        End If
    End Sub
    Function HasPaymentEntries() As Boolean
        If (RepPaymentEntries.Items.Count > 0) Then
            LabelWarningForEftpos.Text = EFTPOS_MIX_MSG
        End If
        Return (RepPaymentEntries.Items.Count = 0)
    End Function
#End Region

#Region "FUNCTION - EFTPOS and BILLING TOKEN"

    Function IsUsingBillingToken() As Boolean
        Return Not String.IsNullOrEmpty(Me.BillingTokenSelect.SelectedValue)
    End Function

    Function FormatWSAmount(ByVal samt As String) As String
        If samt.Contains(".") = False Then
            samt = samt & ".00"
        ElseIf samt.Contains(".") = True Then
            Dim decnumber As String = samt.Split(".")(1)
            If Not String.IsNullOrEmpty(decnumber) Then
                If decnumber.Length = 1 Then
                    samt = samt & "0"
                End If
            End If
        End If
        Return samt
    End Function


    ''(ByRef username As String, _
    '     ByRef password As String, _
    '     ByVal _comCode As String, _
    '     ByVal _currencyType As String, _
    '     ByVal _brdCode As String)
    Function SubmitTransaction(ByVal data As PaymentExpressWSData, _
                               Optional ByVal comcode As String = "", _
                               Optional ByVal currencytype As String = "", _
                               Optional ByVal brdcode As String = "") As String
        Dim xDetails As New PaymentXpress.TransactionDetails


        ''GetWSPassword()
        Dim sb As New StringBuilder
        Try
            Me.MapPasswordsForBrands(postUserName, postPassword, comcode, currencytype, brdcode)

            With xDetails
                .amount = data.Amount
                .dpsBillingId = data.DpsBillingId
                ''------------------------------------------------------------------------------------------------------
                ''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:NZ USER that is transacting payment and booking in AU
                ''------------------------------------------------------------------------------------------------------
                .inputCurrency = DefaultCurrencyDesc ''Me.CountryCode + "D"
                ''------------------------------------------------------------------------------------------------------
                .txnType = data.TxnType
                .txnRef = data.TxnRef
                If Not (String.IsNullOrEmpty(data.DpsTxnRef)) Then
                    .dpsTxnRef = data.DpsTxnRef
                End If
            End With
            Logging.LogInformation("PAYMENT SCREEN: SubmitTransaction: ", _
                               "postUserName " & postUserName + ", DpsBillingId " & data.DpsBillingId + ",inputCurrency " & Me.CountryCode + " D" + ",TxnType " & data.TxnType)

            Dim px As New PaymentXpress.PaymentExpressWS
            Dim pxResult As PaymentXpress.TransactionResult = px.SubmitTransaction(postUserName, postPassword, xDetails)

            ''rev:mia november 16 2010-remove responsetext from condition
            ''If (pxResult.authorized = "1" And pxResult.responseText = "APPROVED") Then
            If (pxResult.authorized = "1") Then
                sb.Append("<root>")
                sb.AppendFormat("<status>{0}</status>", "SUCCESS")
                sb.AppendFormat("<msg>{0}</msg>", pxResult.responseText)
                sb.AppendFormat("<AuthCode>{0}</AuthCode>", pxResult.authCode)
                sb.AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", pxResult.dpsTxnRef)
                sb.Append("</root>")
                BreakHere()
                Dim objEFTPOS As New Aurora.Booking.Services.BookingPaymentEFTPOSMaintenance
                If data.TxnType = "Purchase" Then
                    objEFTPOS.AddEFTPOSHistory(RntId, Me.UserCode, "BILLING TOKEN PAYMENT")
                End If

            Else
                sb.Append("<root>")
                sb.AppendFormat("<status>{0}</status>", "ERROR")
                sb.AppendFormat("<msg>{0}</msg>", pxResult.responseText)
                sb.AppendFormat("<AuthCode>{0}</AuthCode>", pxResult.authCode)
                sb.AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", pxResult.dpsTxnRef)
                sb.Append("</root>")
            End If

            Logging.LogInformation("PAYMENT SCREEN: RESULT SubmitTransaction: ", sb.ToString)
            Logging.LogInformation("PAYMENT SCREEN: RESULT SubmitTransaction: pxResult.authorized: ", pxResult.authorized)

        Catch ex As Exception
            Logging.LogError("PAYMENT SCREEN: ERROR SubmitTransaction: ", ex.Message)
            sb.Append("<root>")
            sb.AppendFormat("<status>{0}</status>", "ERROR")
            sb.AppendFormat("<msg>{0}</msg>", ex.Message)
            sb.AppendFormat("<AuthCode>{0}</AuthCode>", "")
            sb.AppendFormat("<DpsTxnRef>{0}</DpsTxnRef>", "")
            sb.Append("</root>")
            Return sb.ToString
        End Try

        Return sb.ToString
    End Function

    Function IsBillingTokenPermitted() As Boolean

        Dim sreturnmsg As String = ""
        Dim cardAndImprintselected As Boolean = Me.ddlMethod.SelectedItem.Text.Contains("Imprint") Or Me.ddlMethod.SelectedItem.Text.Contains("Credit")
        If (String.IsNullOrEmpty(IsBillingTokenPermittedValue)) Then
            Dim eft As New BookingPaymentEFTPOSMaintenance
            IsBillingTokenPermittedValue = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForTokenCSR")) ''CHGTOKEN
            ''BreakHere()
            If (IsBillingTokenPermittedValue <> "True") Then
                sreturnmsg = "False"
                IsBillingTokenPermittedValue = "False"
            Else
                sreturnmsg = "True"
                IsBillingTokenPermittedValue = "True"
            End If
        Else
            sreturnmsg = IsBillingTokenPermittedValue
        End If


        If (sreturnmsg.Equals("True")) Then
            ''Return True And Me.CountryCode = "NZ" And cardAndImprintselected = True
            Return True And cardAndImprintselected = True
        End If
        Return False
    End Function

    Function IsBillingTokenSaving() As Boolean

        Dim sreturnmsg As String = ""

        If (String.IsNullOrEmpty(IsBillingTokenPermittedValue)) Then
            Dim eft As New BookingPaymentEFTPOSMaintenance
            IsBillingTokenPermittedValue = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForTokenCSR"))
            ''BreakHere()
            If (IsBillingTokenPermittedValue <> "True") Then
                sreturnmsg = "False"
                IsBillingTokenPermittedValue = "False"
            Else
                sreturnmsg = "True"
                IsBillingTokenPermittedValue = "True"
            End If

        Else
            sreturnmsg = IsBillingTokenPermittedValue
        End If


        If (sreturnmsg.Equals("True")) Then
            Return True ''And Me.CountryCode = "NZ"
        End If
        Return False
    End Function




    Function EFTButtonForCSR() As Boolean
        Dim sreturnmsg As String = ""
        ''BreakHere()
        If String.IsNullOrEmpty(EFTButtonForCSRvalue) Then
            Dim eft As New BookingPaymentEFTPOSMaintenance
            EFTButtonForCSRvalue = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForCSR"))
            sreturnmsg = EFTButtonForCSRvalue
        Else
            sreturnmsg = EFTButtonForCSRvalue
        End If


        If (sreturnmsg.Equals("True")) Then
            Return True ''And Me.CountryCode = "NZ"
        End If
        Return False
    End Function

    Protected Sub btnHiddenBillingToken_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHiddenBillingToken.Click
        ''if imprint is selected then display message
        If ddlMethod.SelectedItem.Text.Contains("Imprint") And sMode = 0 Then
            If (Me.BillingTokenSelect.Items.Count - 1) = 0 Then
                cbBillingTokenWithNovalue.Show()
            End If
        End If
    End Sub

    Protected Sub cbBillingTokenWithNovalue_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cbBillingTokenWithNovalue.PostBack
        If leftButton Then
            GoBack()
        End If
    End Sub


    ''add warning message if biiling token was
    ''used and user is trying to add an offline payment
    Function isBillingTokenHasBeenUsed() As Boolean
        If litxmlPaymentDetails.Text <> "" Then
            xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))
        End If

        Dim listnode As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem")
        For Each node As XmlNode In listnode
            If Not node("DPSBillingToken") Is Nothing Then
                If Not String.IsNullOrEmpty(node("DPSBillingToken").InnerText) Then
                    Return True
                End If
            End If
        Next
        Return False
    End Function

    Function SetHashTableValue(ByVal index As String, ByVal value As String) As Hashtable
        ''BreakHere()
        Dim localhash As Hashtable = BillingTokenValue
        If localhash Is Nothing Then
            localhash = New Hashtable
        End If

        If Not String.IsNullOrEmpty(value) Then
            If Not localhash.ContainsValue(value) Then
                localhash.Add(index, value)
                BillingTokenValue = localhash
            End If
        End If
        Return localhash
    End Function

    Function IshashTableHasValue() As Boolean
        ''BreakHere()
        Dim localhash As Hashtable = BillingTokenValue
        If localhash Is Nothing Then
            Return False
        End If
        Return IIf((localhash.Count - 1) = -1, False, True)
    End Function

#End Region

#Region "PROPERTIES - EFTPOS and BILLING TOKEN"

    Private Property EFTButtonForCSRvalue As String
        Get
            Return ViewState("EFTButtonForCSRvalue")
        End Get
        Set(ByVal value As String)
            If value.Contains("not") Then
                ViewState("EFTButtonForCSRvalue") = "false"
            Else
                ViewState("EFTButtonForCSRvalue") = value
            End If

        End Set
    End Property

    Private Property willUseEFTPOS() As Boolean
        Get
            Return CBool(ViewState("willUseEFTPOS"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("willUseEFTPOS") = value
        End Set
    End Property

    Private Property willUseImprint() As Boolean
        Get
            Return CBool(ViewState("willUseImprint"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("willUseImprint") = value
        End Set
    End Property



    Private _GetPrinterName As String
    Public ReadOnly Property GetPrinterName As String
        Get
            Return _GetPrinterName
        End Get
    End Property

    Private Property IsBillingTokenPermittedValue As String
        Get
            Return ViewState("IsBillingTokenPermittedValue")
        End Get
        Set(ByVal value As String)
            If value.Contains("not") Then
                ViewState("IsBillingTokenPermittedValue") = "false"
            Else
                ViewState("IsBillingTokenPermittedValue") = value
            End If

        End Set
    End Property


    Private _billingtokenvalue As Hashtable
    Property BillingTokenValue As Hashtable
        Get
            Return CType(ViewState("BillingTokenValue"), Hashtable)
        End Get
        Set(ByVal value As Hashtable)
            ViewState("BillingTokenValue") = value
        End Set
    End Property

    Property BillingTokenSingleValue As String
        Get
            Return ViewState("BillingTokenSingleValue")
        End Get
        Set(ByVal value As String)
            ViewState("BillingTokenSingleValue") = value
        End Set
    End Property
#End Region

#Region "PROCEDURES - EFTPOS and BILLING TOKEN"

    Private postPassword As String
    Private postUserName As String
    Sub GetWSPassword()
        Dim xml As New XmlDocument
        xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))

        postUserName = xml.SelectSingleNode("DpsBillingToken/Country[@code='" + CountryCode + "']/UserNameWS").InnerText
        postPassword = xml.SelectSingleNode("DpsBillingToken/Country[@code='" + CountryCode + "']/PasswordWS").InnerText

    End Sub

    Sub PopulateBillingToken(ByVal rentalID As String)
        Dim TOKEN As New BookingEFTPOSBillingToken
        Me.BillingTokenSelect.AppendDataBoundItems = True
        Me.BillingTokenSelect.Items.Add("")
        Me.BillingTokenSelect.DataSource = TOKEN.GetBillingToken(rentalID).Tables(0).DefaultView
        Me.BillingTokenSelect.DataTextField = "TokenCard"
        Me.BillingTokenSelect.DataValueField = "PmtBillToToken"
        Me.BillingTokenSelect.DataBind()
        ''Me.BillingTokenSelect.Enabled = IsBillingTokenPermitted()
        Me.trdBillingToken.Visible = IsBillingTokenPermitted()
    End Sub

    Sub PopulateCreditCard()
        If (Me.ddlCardType.Items.Count > 0) Then Exit Sub
        Dim TOKEN As New BookingEFTPOSBillingToken
        Me.ddlCardType.AppendDataBoundItems = True
        Me.ddlCardType.Items.Add("")
        Me.ddlCardType.DataSource = TOKEN.GetCreditCards.Tables(0).DefaultView
        Me.ddlCardType.DataTextField = "name"
        Me.ddlCardType.DataValueField = "id"
        Me.ddlCardType.DataBind()
    End Sub

    Private Sub BillingTokenSelectAfterpayment()
        If Not IsBillingTokenPermitted() And Not EFTButtonForCSR() Then
            Me.ddlMethod.SelectedIndex = -1
            Me.ddlMethod.Enabled = True
            Me.trdBillingToken.Visible = False
            Me.HidePanels(HidePanel._All)
        Else
            RemoveFieldValue()
        End If

        Me.rdoCardPresent.Enabled = True
        Me.ddlMethod.Enabled = True
        EnableBillingTokenfields(True)

    End Sub

    Private Sub BillingTokenSelect_SelectedIndexChangedExtracted()
        If sMode = 1 Then

            EnableBillingTokenfields(True)
            Me.ddlMethod.Enabled = True
            Dim isAllowedToView As Boolean = Me.isAllowedToViewCreditCard
            LoadPaymentToControls(1, -1, isAllowedToView)
            Exit Sub
        End If
        BillingTokenSelectAfterpayment()
    End Sub



    Sub clearHashTable()
        ''BreakHere()
        BillingTokenValue = Nothing
    End Sub

    Sub PopulateImprintList(ByVal methodTypeID As String)
        Dim xmlString As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paym_getMethodType", methodTypeID).OuterXml
        xmlString = xmlString.Replace(" ", "")
        Dim dsLocal As New DataSet
        dsLocal.ReadXml(New XmlTextReader(xmlString, System.Xml.XmlNodeType.Document, Nothing))
        Me.ddlCardType.DataSource = dsLocal.Tables("PaymentMethod")
        Me.ddlCardType.DataValueField = "ID"
        Me.ddlCardType.DataTextField = "NAME"
        Me.ddlCardType.DataBind()
    End Sub



    Sub EnableBillingTokenfields(ByVal active As Boolean)
        Me.ddlCardType.Enabled = active
        Me.txtCardNumber.Enabled = active
        msk_txtCardNumber.Enabled = active
        Me.TxtName.Enabled = active
        Me.txtExpiryData.Enabled = active
    End Sub

    Sub RemoveFieldValue()
        Me.ddlMethod.Enabled = False
        Me.txtCardNumber.Text = ""
        Me.TxtName.Text = ""
        Me.txtExpiryData.Text = ""
        Me.txtApprovalReference.Text = ""
        Me.ddlCardType.SelectedIndex = -1
        Me.rdoCardPresent.Items(0).Selected = False
        Me.rdoCardPresent.Items(1).Selected = False
    End Sub

    Sub EnableCardPresentOption()
        Me.trCardPresent.Visible = (CBool(bDPSRole) AndAlso Me.ddlMethod.SelectedItem.Text.Contains("Credit") = True)

        If CBool(bDPSRole) AndAlso sMode = 0 Or CBool(bDPSRole) AndAlso sMode = 1 Then
            If Me.ddlMethod.SelectedItem.Text.Contains("Credit") = True Then
                If sMode = 1 Then
                    Me.rdoCardPresent.Enabled = False
                Else
                    Me.rdoCardPresent.Enabled = True
                End If
            End If

        ElseIf Not CBool(bDPSRole) AndAlso sMode = 0 Or Not CBool(bDPSRole) AndAlso sMode = 1 Then
            Me.rdoCardPresent.Enabled = False
            ''rev:mia 06-dec-2016 - https://thlonline.atlassian.net/browse/AURORA-1119
            CardPresent = True
        End If

    End Sub


    Sub RadioButtonMode()

        Dim CHGTOKEN As Boolean = CBool(IsBillingTokenPermittedValue)
        Dim DPSPMT As Boolean = CBool(bDPSRole)
        Dim ISPAYMENT As Boolean = IIf(sMode = 0, True, False)
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        Dim DEBIT As Boolean = IsMethodContainsDebit()
        Dim METHOD_CREDIT As Boolean = IsMethodContainsThisParameter("Credit")
        Dim METHOD_IMPRINT As Boolean = IsMethodContainsThisParameter("Imprint")

        If ISPAYMENT Then
            DisplayCreditCardAuthority()

            If METHOD_IMPRINT Then
                If CHGTOKEN Then
                    Me.trdBillingToken.Visible = True
                End If

                If CSREFTPMT Then
                    Me.trdBillingToken.Visible = True
                End If
            End If

            If DEBIT Then
                Me.trdBillingToken.Visible = False
            End If
        End If


        ''Me.trdBillingToken.Visible = IsBillingTokenPermitted() And sMode = 0 And EFTButtonForCSR()
        If sMode = 1 Then
            ''rev:mia august 10 2010
            ''BreakHere()
            EnableCardPresentOption()
            Exit Sub
        End If

        ''if its debit card, hide radio button
        If Me.ddlMethod.SelectedItem.Text.IndexOf("Debit") = 0 AndAlso Not String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text) Then
            Me.trCardPresent.Visible = False
            Me.btnEFTPOS.Enabled = CSREFTPMT
            Exit Sub
        Else
            If Me.ddlMethod.SelectedItem.Text.IndexOf("Debit") = 0 And Me.ddlCardType.SelectedItem.Text = "" Then
                Me.btnEFTPOS.Enabled = False
                Exit Sub
            End If

        End If


        ''if its credit card, 
        ''hide radio button if DPSPYMT == false and set the radio button(0) to 1
        ''display radio button if DPSPYMT == true and set the all radio button(0/1) to 0

        Dim EFTButtonForCSRenabled As Boolean = CSREFTPMT

        ''display radio button and is a can use the EFTPOS button
        If (CBool(bDPSRole) = True And EFTButtonForCSRenabled) Then

            ''if its imprint
            If (Me.ddlMethod.SelectedItem.Text.Contains("Imprint")) Then
                Me.trCardPresent.Visible = False
            Else
                Me.trCardPresent.Visible = True
            End If



            ''card present is selected 
            If (Me.rdoCardPresent.Items(0).Selected = True And Me.rdoCardPresent.Items(1).Selected = False) Then
                Me.btnEFTPOS.Enabled = True
                Me.HiddenIsEFTPOSactive.Value = "false"
                Exit Sub
            End If

            ''card present is not selected and cardname is empty
            If (Me.rdoCardPresent.Items(0).Selected = False And _
                Me.rdoCardPresent.Items(1).Selected = False And _
                Not String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text)) Then

                Me.btnEFTPOS.Enabled = False
                Me.HiddenIsEFTPOSactive.Value = "true"
                Exit Sub
            End If

            If (Me.rdoCardPresent.Items(1).Selected = True) Then
                Me.btnEFTPOS.Enabled = False
                Me.HiddenIsEFTPOSactive.Value = "true"
                Exit Sub
            End If

        End If

        ''display radio button and is a not allowed to use the EFTPOS button
        If (CBool(bDPSRole) = True AndAlso Not EFTButtonForCSRenabled) Then
            If (Me.ddlMethod.SelectedItem.Text.Contains("Imprint")) Then
                Me.trCardPresent.Visible = False
            Else
                Me.trCardPresent.Visible = True
            End If

            Me.btnEFTPOS.Enabled = False
            Me.HiddenIsEFTPOSactive.Value = "true"
            Exit Sub
        End If

        ''not display radio button and is a allowed to use the EFTPOS button
        If (CBool(bDPSRole) = False AndAlso EFTButtonForCSRenabled) Then
            Me.trCardPresent.Visible = False
            If Not String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text) Then
                If Not (String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text)) Then
                    Me.btnEFTPOS.Enabled = True
                    Me.HiddenIsEFTPOSactive.Value = "false"
                    Exit Sub
                End If

                If (String.IsNullOrEmpty(Me.ddlCardType.SelectedItem.Text)) Then
                    Me.btnEFTPOS.Enabled = False
                    Me.HiddenIsEFTPOSactive.Value = "true"
                    Exit Sub
                End If
            End If
        End If

        If (CBool(bDPSRole) = False AndAlso Not EFTButtonForCSRenabled) Then
            Me.trCardPresent.Visible = False
            Me.btnEFTPOS.Enabled = False
            Me.HiddenIsEFTPOSactive.Value = "true"
            Exit Sub
        End If
    End Sub


    Sub ImprintBillingTokenRules(Optional ByVal notIspostback As Boolean = False)


        Me.trdBillingToken.Visible = False

        If Me.rdoCardPresent.Items(1).Selected Then Me.btnEFTPOS.Enabled = False
        If notIspostback = True Then Me.btnEFTPOS.Enabled = False

        DisableEftposButtonWhenMethodIsEmpty()

        If sType <> "B" Then Exit Sub

        Dim CHGTOKEN As Boolean = CBool(IsBillingTokenPermittedValue)
        Dim DPSPMT As Boolean = CBool(bDPSRole)
        Dim ISPAYMENT As Boolean = IIf(sMode = 0, True, False)
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()

        Dim method As String = Me.ddlMethod.SelectedItem.Text


        ''this will trigger on page load or when selecting ddlmethod imprint
        If ISPAYMENT And (method.Contains("Imprint") = True) Then
            If notIspostback Then
                ImprintRulesImplentation(2)
                Exit Sub
            End If


            ''RULE # 1
            ''CSREFTPMT is tick, disabled EFTPOS BUTTON but display BILLING TOKEN
            If CSREFTPMT = True Then
                ImprintRulesImplentation(1)
                Exit Sub
            End If

            ''RULE # 2
            ''CSREFTPMT is untick, disabled both
            If CSREFTPMT = False Then
                ImprintRulesImplentation(2)
                Exit Sub
            End If

            ''RULE # 3
            ''CHGTOKEN  will have a precedence over the CSREFTPMT
            If CHGTOKEN And CSREFTPMT Then
                ImprintRulesImplentation(3)
                Exit Sub
            End If

            ''RULE # 4
            ''apply rule # 1
            If Not CHGTOKEN And CSREFTPMT Then
                ImprintRulesImplentation(1)
                Exit Sub
            End If

            ''RULE # 4
            ''apply rule # 2
            If Not CHGTOKEN And Not CSREFTPMT Then
                ImprintRulesImplentation(2)
                Exit Sub
            End If

            ''RULE # 5
            ''apply rule # 2
            If Not CHGTOKEN And Not Not CSREFTPMT Then
                ImprintRulesImplentation(2)
                Exit Sub
            End If
        End If
        If ISPAYMENT And (method.Contains("Debit") = True) Then
            ImprintRulesImplentation(2)
            Exit Sub
        End If
    End Sub

    Sub ImprintRulesImplentation(ByVal rules As Integer)
        If rules = 1 Then
            Me.btnEFTPOS.Enabled = False
            Me.trdBillingToken.Visible = True
            Exit Sub
        End If
        If rules = 2 Then
            Me.btnEFTPOS.Enabled = False
            Me.trdBillingToken.Visible = False
            Exit Sub
        End If
        If rules = 3 Then
            Me.trdBillingToken.Visible = False
            Exit Sub
        End If
    End Sub
#End Region

#Region "CONTROL EVENTS - EFTPOS and BILLING TOKEN"

    Protected Sub BillingTokenSelect_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles BillingTokenSelect.SelectedIndexChanged
        ''BreakHere()
        ''79471D7D-0AB7-48D5-B695-C82144CE5139
        If Not (String.IsNullOrEmpty(BillingTokenSelect.SelectedValue)) Then
            If Not String.IsNullOrEmpty(Me.ddlMethod.SelectedItem.Text) Then
                If (Me.ddlMethod.SelectedItem.Text.IndexOf("Credit") <> 0 AndAlso Me.ddlMethod.SelectedItem.Text.IndexOf("Imprint")) Then
                    Me.SetInformationMessage(BILLING_TYPE_OF_MSG)
                    Me.BillingTokenSelect.SelectedIndex = -1
                    Exit Sub
                End If
            End If

            Dim TOKEN As New BookingEFTPOSBillingToken
            Dim ds As DataSet = TOKEN.BillingTokenSelectPopulate(RentalID, BillingTokenSelect.SelectedValue)
            Dim item As ListItem
            If (Me.ddlMethod.SelectedItem.Text.IndexOf("Credit") <> -1) Then
                Me.ddlMethod.SelectedValue = "00B1F2C5-0112-44A4-BDAE-D21561F8D911"
                If (ddlCardType.Items.Count = 0) Then PopulateCreditCard()
                item = Me.ddlCardType.Items.FindByValue(ds.Tables(0).Rows(0)("PmtBillToCardType").ToString)
                Me.ddlCardType.SelectedValue = item.Value
            Else
                If Me.ddlMethod.SelectedItem.Text.IndexOf("Imprint") <> -1 Then
                    Me.ddlMethod.SelectedValue = "79471D7D-0AB7-48D5-B695-C82144CE5139"
                    item = Me.ddlCardType.Items.FindByValue(ds.Tables(0).Rows(0)("PmtBillToCardTypeImprint").ToString)
                    Me.ddlCardType.SelectedValue = item.Value
                    If IsMethodContainsThisParameter("Imprint") = True Then Me.ddlCardType.Enabled = False
                    Dim CSREFTPMT As Boolean = EFTButtonForCSR()
                    If Not CSREFTPMT And IsMethodContainsThisParameter("Imprint") Then
                        Me.ddlCardType.Enabled = True
                    End If
                End If

            End If

            ''rev:mia march 12 2012 -- Added CVC2
            DisabledCVVvalidations(True)

            Me.ddlMethod.Enabled = False

            Me.txtCardNumber.Text = ds.Tables(0).Rows(0)("PmtBillCardNumber").ToString
            msk_txtCardNumber.Enabled = False

            Me.TxtName.Text = ds.Tables(0).Rows(0)("PmtBillToCardName").ToString
            Me.txtExpiryData.Text = ds.Tables(0).Rows(0)("PmtBillToCardExpDate").ToString.Insert(2, "/")

            Me.HidePanels(HidePanel._CreditCard)
            Me.rdoCardPresent.Enabled = False
            Me.rdoCardPresent.Items(1).Selected = True


            EnableBillingTokenfields(False)

            If Not sMode = NEW_PAYMENT Then
                Me.btnAddPayment.Enabled = False
                Me.txtApprovalReference.Enabled = False
            Else
                Me.txtApprovalReference.Enabled = True
                Me.txtAmount.Text = Me.txtRunningBalanceChange.Text
            End If
            trCardPresent.Visible = CBool(bDPSRole) And Not Me.ddlCardType.SelectedItem.Text.Contains("Imprint")

            ''disable eftpos button since it cant accept an eftpos payment
            Me.btnEFTPOS.Enabled = False
        Else
            BillingTokenSelect_SelectedIndexChangedExtracted()
            ''If IsMethodContainsThisParameter("Imprint") = True Then Me.ddlCardType.Enabled = False
            Me.ddlCardType.Enabled = Not IsMethodContainsThisParameter("Imprint")

            Dim CSREFTPMT As Boolean = EFTButtonForCSR()
            If Not CSREFTPMT And IsMethodContainsThisParameter("Imprint") Then
                Me.ddlCardType.Enabled = True
            End If

        End If

        ''rev:mia issue # 111
        If Not String.IsNullOrEmpty(BillingTokenSelect.SelectedValue) Then
            Me.SupervisorOverrideControl1.DisplayType = "AUTHORITY"
            '' BillingTokenSingleValue = "AUTHORITY"
        Else
            Me.SupervisorOverrideControl1.DisplayType = "PAYMENT"
            '' BillingTokenSingleValue = "PAYMENT"
        End If

        ''------------------------------------------------------------------------------------------------------
        ''rev:mia July 3, 2013 - fixing problem on the mixing up of payment ie:NZ USER that is transacting payment and booking in AU
        ''------------------------------------------------------------------------------------------------------
        GetCurrencyBasedOnTravelDestination = True
        Dim sameCurrency As Boolean = isSameCurrency()
        If sMode = NEW_PAYMENT And Not (String.IsNullOrEmpty(BillingTokenSelect.SelectedValue)) Then

            If sameCurrency AndAlso rdoCardPresent.SelectedIndex = 0 Then Exit Sub
            If sameCurrency AndAlso rdoCardPresent.SelectedIndex = 1 Then Exit Sub
            If Not sameCurrency AndAlso rdoCardPresent.SelectedIndex = 0 Then Exit Sub

            LoadXMLSettings()
            HidePanels(HidePanel._CreditCard)
            Me.rdoCardPresent.Enabled = False
            LoadScreen(True)
            Me.ddlMethod.Enabled = False
        End If
    End Sub




#End Region

#Region "Rules Revision"
    Sub DisableEftposButtonWhenMethodIsEmpty()
        If Me.ddlMethod.SelectedIndex = 0 Or Me.ddlMethod.SelectedIndex = -1 Then
            Me.btnEFTPOS.Enabled = False
        End If
    End Sub

    Sub DisableEftposButtonWhenMethodIsDebit()
        If Me.ddlMethod.SelectedItem.Text.Contains("Debit") = True Then
            ImprintRulesImplentation(2)
        End If
    End Sub

    Function IsMethodContainsDebit() As Boolean
        Return Me.ddlMethod.SelectedItem.Text.Contains("Debit")
    End Function

    Function IsMethodContainsThisParameter(ByVal methodName As String) As Boolean
        Return Me.ddlMethod.SelectedItem.Text.Contains(methodName)
    End Function

    Sub DisplayCreditCardAuthority()
        Dim CHGTOKEN As Boolean = IsBillingTokenPermitted()
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        Dim DEBIT As Boolean = IsMethodContainsDebit()

        ''If CHGTOKEN And Not CSREFTPMT Then Me.trdBillingToken.Visible = True
        ''If Not CHGTOKEN And CSREFTPMT Then Me.trdBillingToken.Visible = True
        ''If CHGTOKEN And CSREFTPMT Then Me.trdBillingToken.Visible = True
        ''If Not CHGTOKEN And Not CSREFTPMT Then Me.trdBillingToken.Visible = False

        If IsMethodContainsThisParameter("Imprint") Then
            '' RKS : 26-Jun-2012
            '' Should be always enable for Imprints for AU and NZ
            '' CSREFTPMT role is true or False we don't care..
            If CSREFTPMT = False Or CSREFTPMT = True Then
                Me.trdBillingToken.Visible = True
            Else
                If CHGTOKEN Then
                    Me.trdBillingToken.Visible = True
                Else
                    Me.trdBillingToken.Visible = False
                End If
            End If

        Else
            If IsMethodContainsThisParameter("Credit") Then
                If CHGTOKEN Then
                    Me.trdBillingToken.Visible = True
                Else
                    Me.trdBillingToken.Visible = False
                End If
            End If
        End If
    End Sub


    Function appliedOverrideBox() As Boolean
        Dim method As String = methodType
        Dim appliedOverride As Boolean = False

        If (method.Contains("Credit") = True) Then
            appliedOverride = True
        ElseIf (method.Contains("Debit") = True) Then 'rev:mia rev:mia 07-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1119
            appliedOverride = True
        Else
            appliedOverride = False
        End If
        Return appliedOverride
    End Function


#End Region

#Region "DYNAMIC PASSWORD FOR MAUI BRITZ AND EXPLOREMORE"
    Sub MapPasswordsForBrands(ByRef username As String, _
                              ByRef password As String, _
                              ByVal _comCode As String, _
                              ByVal _currencyType As String, _
                              ByVal _brdCode As String)

        Dim xml As New XmlDocument
        xml.Load(HttpContext.Current.Server.MapPath("~/App_Data/DPSBillingTokenPasswords.xml"))

        ' ''test
        '_brdCode = ConfigurationManager.AppSettings("BRDCodeTest")
        '_currencyType = ConfigurationManager.AppSettings("currencyTest")
        '_comCode = ConfigurationManager.AppSettings("comCodeTest")

        _brdCode = _brdCode.ToUpper
        _currencyType = _currencyType.ToUpper
        _comCode = _comCode.ToUpper

        Dim path As String = "DpsBillingToken/WSBrands/"
        Try

            If _comCode.Equals("THL") = True Then
                If _currencyType.Equals("NZ") = True Then
                    If _brdCode.Equals("B") = True Then
                        username = xml.SelectSingleNode(path + "britz_username").InnerText   ''"/ComCode/britz_username"
                        password = xml.SelectSingleNode(path + "britz_password").InnerText   ''"/ComCode/britz_password"
                    Else ''maui
                        username = xml.SelectSingleNode(path + "maui_username").InnerText
                        password = xml.SelectSingleNode(path + "maui_password").InnerText

                    End If
                Else ''rentals
                    username = xml.SelectSingleNode(path + "rentals_username").InnerText
                    password = xml.SelectSingleNode(path + "rentals_password").InnerText
                End If

            Else ''kxz ''------ If _comcode.Equals("THL") = True Then
                If _comCode.Equals("KXS") = True Then '------ If _comcode.Equals("KX") = True Then
                    username = xml.SelectSingleNode(path + "kxs_username").InnerText
                    password = xml.SelectSingleNode(path + "kxs_password").InnerText
                Else
                    ' V2
                    ' RKS MOD: 21-Sep-2009 for MAC.COM
                    If _comCode.ToUpper().Equals("MAC") = True Then '------ If _comcode.Equals("MAC") = True Then

                        If _currencyType.Equals("NZ") = True Then '------ If _comcode.Equals("NZ") = True Then
                            username = xml.SelectSingleNode(path + "mac_nz_username").InnerText
                            password = xml.SelectSingleNode(path + "mac_nz_password").InnerText
                        Else
                            If _currencyType.Equals("AU") = True Then
                                username = xml.SelectSingleNode(path + "mac_au_username").InnerText
                                password = xml.SelectSingleNode(path + "mac_au_password").InnerText
                            End If
                        End If '------ If _comcode.Equals("NZ") = True Then
                    End If
                End If '' '------   If _comcode.Equals("KX") = True Then
            End If ''''kxz ''------ If _comcode.Equals("THL") = True Then

            LogInformation(String.Concat("SUBMITTRANSACTION(WEBSERVICE USERNAME AND PASSWORD) INFORMATION: POSTUSERNAME: ", username, " , POSTPASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode))
        Catch ex As Exception
            LogError(String.Concat("SUBMITTRANSACTION(WEBSERVICE USERNAME AND PASSWORD) ERROR: POSTUSERNAME: ", username, " , POSTPASSWORD: ", password, " , COMCODE: ", _comCode, " , USERCURRENCY: ", _currencyType, " , BRDCODE: ", _brdCode, " EXCEPTION: ", ex.Message + ", TRACE: ", ex.StackTrace))
        End Try
    End Sub
#End Region

#Region "DEBUGGER"
    <Diagnostics.DebuggerNonUserCode()> _
    Sub BreakHere()
#If DEBUG Then
        '' System.Diagnostics.Debugger.Break()
#End If

    End Sub
#End Region

#Region "WEBCONFIG CHANGES FOR EFTPO AND BILLING TOKEN"
    '<!--EFTPOS implementations-->
    '<add key="PrinterName" value="Star TSP100"/>
    '<add key="NetworkPrinterName" value=""/>
    '<add key="ReceiptHeaderForPayment" value="Aurora EFTPOS - Payment"/>
    '<add key="ReceiptHeaderForRefund" value="Aurora EFTPOS - Refund"/>
    '<add key="ReceiptWidth" value="30"/>
    '<add key="EnablePrintReceipt" value="0"/>
    '<add key="ReceiptIsSeparate" value="0"/>
    '<add key="ReceiptFontSize" value="8"/>
    '<add key="UseDPSReceipt" value="false"/>

    '<!--use a lowercase on this value-->
    '<add key="DisplayCardMismatch" value="true"/>
    '<add key="OverrideEFTPOSRole"  value="VIEWPMT"/>
    '<add key="EFTPOSroleForCSR"    value="CSREFTPMT"/>
    '<add key="EFTPOSroleForTokenCSR"    value="CHGTOKEN"/>
    '<add key="DPSServiceURL" value="https://www.paymentexpress.com/pxpost.aspx"/>
    '<add key="UseProxy" value="false"/>
    '<add key="httpProxy" value=""/>
    '<add key="PaymentExpress.PxPay" value="https://sec2.paymentexpress.com/pxpay/pxaccess.aspx"/>
    '<add key="PaymentXpress.PXWS" value="https://www.paymentexpress.com/WS/PXWS.asmx"/>
    '<add key="PaymentXpress.TimeOut" value = "100000" />
    '<add key="enableEOV" value = "1" />
    '<add key="emvVersionId" value = "1" /><!--EmvVersionId to 1(this is not required for a live pinpad).-->

#End Region

#Region "DESTINATION BASED REFUNDING"
    Function isRefundBasedOnDestination(ByVal tempSessionNamePaymentMGTClone As String) As Boolean
        isRefundBasedOnDestination = False
        If (Request.QueryString("DestinationBased") = "True" Or Request.QueryString("DestinationBased") = True) Then
            If Session(tempSessionNamePaymentMGTClone) <> "" Then
                litPaymentRequest.Text = Server.HtmlEncode(CType(Session(tempSessionNamePaymentMGTClone), String))
            Else
                If String.IsNullOrEmpty(litPaymentRequest.Text) Then
                    Response.Redirect(QS_RS_VEHREQMGT)
                End If
            End If
            isRefundBasedOnDestination = True
        End If
        Return isRefundBasedOnDestination
    End Function
#End Region

#Region "ISSUE #109"
    ''Reversing a CC not present payment going to the incorrect DPZ location rather than user location
    Private Property IsCardPresentWhenRefunding As Boolean
        Get
            Return CBool(ViewState("IsCardPresentWhenRefunding"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("IsCardPresentWhenRefunding") = value
        End Set
    End Property


#End Region

#Region "ISSUE #113"
    ''Partial Refunds on B2C card not present payments not possible.
    Function isCardNotPresent(ByVal transactionId As String, ByVal methodDesc As String, Optional ByVal isRev As String = "", Optional ByVal amount As Decimal = 0) As Boolean
        If amount < 0 Then Return False
        Return (Not String.IsNullOrEmpty(transactionId) AndAlso methodDesc.ToUpper.Equals("CREDIT CARD") AndAlso sMode = 1 AndAlso (String.IsNullOrEmpty(isRev)))
    End Function

    Function isAmountNotChanged(ByVal newamount As Decimal) As Boolean
        If sMode = 0 Then Return False
        If Not String.IsNullOrEmpty(Me.HiddenFieldTxtAmount.Value) Then
            Dim oldamount As Decimal = CDec(Me.HiddenFieldTxtAmount.Value)
            Return oldamount = newamount
        End If
        Return False
    End Function
#End Region

#Region "ISSUE #103"
    ''Refunding when a booking is balanced to 0.00 outstanding debits or credits.
    
    Property IsRevPmtID As String
        Set(ByVal value As String)
            ViewState("IsRevPmtID") = value
        End Set
        Get
            Return ViewState("IsRevPmtID")
        End Get
    End Property
#End Region

#Region "issue refunding when its DPZ payment and with DPSPMT role"
    Property DPSTransactionId As String
        Set(ByVal value As String)
            ViewState("DPSTransactionId") = value
        End Set
        Get
            Return ViewState("DPSTransactionId")
        End Get
    End Property

#End Region

#Region "#123 manual payment source History and Payment tab identification"
    Function StatusToPending(ByVal ToSave As String) As String

        Dim ccaIsUsed As Boolean = Me.IshashTableHasValue
        Dim CSREFTPMT As Boolean = EFTButtonForCSR()
        Dim xmltoSave As New XmlDocument
        xmltoSave.LoadXml(ToSave)

        If CSREFTPMT And Not ccaIsUsed Then
            Dim iscardpresent As String = xmltoSave.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem/IsCrdPre").InnerText
            If iscardpresent = "1" Then
                xmltoSave.SelectSingleNode("Root/PaymentEntryRoot/PaymentEntry/Status").InnerText = "Pending"
            End If
        End If
        Return xmltoSave.OuterXml
    End Function

#End Region

#Region "ALLOWED BOND TO BE REFUNDED"
    Property PaymentComeFromDPZorDPAorDPM As String
        Get
            Return ViewState("PaymentComeFromDPZorDPAorDPM")
        End Get
        Set(ByVal value As String)
            ViewState("PaymentComeFromDPZorDPAorDPM") = value
        End Set
    End Property

    Function isPaymentComeFromDPZorDPAorDPM(ByVal branchLocationCode As String) As Boolean
        If String.IsNullOrEmpty(branchLocationCode) Then Return False
        branchLocationCode = branchLocationCode.ToUpper
        If branchLocationCode.StartsWith("DP") Then
            Return True
        Else
            Return False
        End If
    End Function

    Property PaymentComeFromDPZorDPAorDPMandImprint As String
        Get
            Return ViewState("PaymentComeFromDPZorDPAorDPMandImprint")
        End Get
        Set(ByVal value As String)
            ViewState("PaymentComeFromDPZorDPAorDPMandImprint") = value
        End Set
    End Property


    Function isPaymentComeFromDPZorDPAorDPMAndImprint(ByVal description As String) As Boolean
        If String.IsNullOrEmpty(description) Then Return False
        description = description.ToUpper
        If description.IndexOf("IMPRINT") <> -1 Then
            Return True
        Else
            Return False
        End If
    End Function
#End Region

#Region "rev:mia April 13 2011 -- check if this booking is already Checkin"
    Private Function IsStatusCheckin(ByVal rentalid As String) As Boolean
        Dim returnvalue As String = ""
        Try
            returnvalue = Aurora.Common.Data.ExecuteScalarSP("Payment_RentalStatusIsCI", rentalid)
        Catch ex As Exception
            LogError("IsStatusCheckin: " & ex.Message)
        End Try
        Return IIf(returnvalue.ToUpper <> "CI", False, True)

    End Function
#End Region

#Region "rev:mia march 12 2012 -- Added CVC2"

    Private Function isActiveCVV2() As Boolean

        Dim memberOfCSR_Australia As String = ""
        Try
            Dim eft As New BookingPaymentEFTPOSMaintenance
            memberOfCSR_Australia = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForCSR"))
        Catch ex As Exception
        End Try

        Dim isCredit As Boolean = Me.ddlMethod.SelectedItem.Text.ToUpper.Contains("Credit".ToUpper)
        Dim isImprint As Boolean = Me.ddlMethod.SelectedItem.Text.ToUpper.Contains("Imprint".ToUpper)

        If (isCredit) Then
            isImprint = False
        End If
        If (isImprint) Then
            isCredit = False
        End If

        ''Return IIf((Me.ddlMethod.SelectedItem.Text.ToUpper.Contains("Credit".ToUpper) Or Me.ddlMethod.SelectedItem.Text.ToUpper.Contains("Imprint".ToUpper)), True, False)
        ''Return IIf((Me.ddlMethod.SelectedItem.Text.ToUpper.Contains("Credit".ToUpper) Or ((memberOfCSR_Australia = "True" And Me.ddlMethod.SelectedItem.Text.ToUpper.Contains("Imprint".ToUpper)))), True, False)
        Return IIf((isCredit = True Or ((isImprint = True))), True, False)
    End Function

    'Private Function HideCVV2() As String
    '    Return IIf(isActiveCVV2() = True, "", "visibility:hidden;")
    'End Function

    Private Sub DisabledCVVvalidations(Optional ByVal isBillingToken As Boolean = False)
        Dim isActive As Boolean = isActiveCVV2()
        If (IsConfigForCVV2Active() = False) Then
            rfv_cvv2.Enabled = False
            flt_cvv2.Enabled = False
            labelCVV2.Visible = False
            tr_cvv2.Visible = False
            Exit Sub
        End If

        If (isBillingToken) Then
            rfv_cvv2.Enabled = False
            flt_cvv2.Enabled = False
            labelCVV2.Visible = False
            tr_cvv2.Visible = False
        Else
            rfv_cvv2.Enabled = isActive
            flt_cvv2.Enabled = isActive
            labelCVV2.Visible = isActive
            tr_cvv2.Visible = isActive
        End If


    End Sub

    Private Sub HideCVVRefund(Optional ByVal cvv2 As String = "")
        Dim isActive As Boolean = isActiveCVV2()

        If (IsConfigForCVV2Active() = False) Then
            rfv_cvv2.Enabled = False
            flt_cvv2.Enabled = False
            cv_cvv2.Enabled = False
            labelCVV2.Visible = False
            tr_cvv2.Visible = False
            Exit Sub
        End If

        If (String.IsNullOrEmpty(cvv2)) Then
            rfv_cvv2.Enabled = False
            flt_cvv2.Enabled = False
            cv_cvv2.Enabled = False
            labelCVV2.Visible = False
            tr_cvv2.Visible = False

        Else
            rfv_cvv2.Enabled = isActive
            flt_cvv2.Enabled = isActive
            labelCVV2.Visible = isActive
            tr_cvv2.Visible = isActive
            cv_cvv2.Enabled = isActive
            txtCVV2.Text = cvv2
        End If

    End Sub

    Private Function IsConfigForCVV2Active() As Boolean
        Return IIf(ConfigurationManager.AppSettings("CVV2_ACTIVE").ToString() = "1", True, False)
    End Function

    Private Sub CvvLength()
        txtCVV2.MaxLength = IIf(ddlCardType.SelectedItem.Text.ToUpper().Contains("AMEX") = True, 4, 3)
    End Sub

    Protected Sub cv_cvv2_ServerValidate(ByVal source As Object, _
                                         ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles cv_cvv2.ServerValidate

        If (IsConfigForCVV2Active() = False) Then
            cv_cvv2.Enabled = False
            Exit Sub
        End If

        Dim isAmex As Boolean = IIf(ddlCardType.SelectedItem.Text.ToUpper().Contains("AMEX") = True, True, False)

        If (isAmex) Then
            cv_cvv2.ErrorMessage = "Please enter 4 digits CVV2 for AMEX card."
            args.IsValid = IIf(txtCVV2.Text.Trim().Length = 4, True, False)
        ElseIf (Not isAmex) Then
            cv_cvv2.ErrorMessage = "Please enter 3 digits CVV2 for Mastercard or Visa Cards."
            args.IsValid = IIf(txtCVV2.Text.Trim().Length = 3, True, False)
        End If

    End Sub

    Private Property CVV2Number As String
        Get
            Return CType(ViewState("CVV2Number"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CVV2Number") = value
        End Set
    End Property
#End Region


#Region " rev:mia Aug 7 2012 note: All payments/refund made against billing token regardless of any countries will display a new popup message."
    Function isBillingTokenHasBeenUsedAndNotImprint() As Boolean
        If litxmlPaymentDetails.Text <> "" Then
            xmlPaymentDetails.LoadXml(Server.HtmlDecode(litxmlPaymentDetails.Text))
        End If

        Dim isImprint As Boolean = False
        Try
            isImprint = IIf(xmlPaymentDetails.SelectSingleNode("PaymentEntryRoot/PaymentEntry/Method").InnerText.Contains("Imprint") = True, True, False)
        Catch ex As Exception

        End Try

        Dim listnode As XmlNodeList = xmlPaymentDetails.SelectNodes("PaymentEntryRoot/PaymentEntry/PaymentEntryItems/PmtItem")
        For Each node As XmlNode In listnode
            If Not node("DPSBillingToken") Is Nothing Then
                If (Not String.IsNullOrEmpty(node("DPSBillingToken").InnerText)) Then
                    If (isImprint = False) Then
                        Return True
                    End If
                End If
            End If
        Next
        Return False
    End Function

    Protected Sub cbPaymentAgainstBillingToken_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cbPaymentAgainstBillingToken.PostBack
        If leftButton Then
            Logging.LogWarning("PAYMENT SCREEN: cbPaymentAgainstBillingToken", ">>>>>>>>>>>>>>>>>>>>>>>>>>>START: hitting the leftButton".ToUpper)
            SavingRoutine()
        End If
    End Sub
#End Region

#Region "rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232"
    Private ReadOnly Property IsAUBooking As Boolean
        Get
            Dim result As Boolean = False
            Try
                If (HiddenCurrency.Value.Contains("|") = True) Then
                    result = IIf(HiddenCurrency.Value.Split("|")(1).Equals("AUD") = True, True, False)
                End If
            Catch ex As Exception
            End Try
            Return result
        End Get
    End Property

    ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
    Private Property DisableSaveButton As Boolean
        Get
            Return CBool(ViewState("DisableSaveButton"))
        End Get
        Set(value As Boolean)
            ViewState("DisableSaveButton") = value
        End Set
    End Property
    Private Function checkPaymentIfLessThanAllowedTobeRefunded(custToPay As String) As Boolean
        System.Diagnostics.Debug.WriteLine(btnEFTPOS.Text)
        System.Diagnostics.Debug.WriteLine(IsRefund)

        If IsAUBooking Then Return True

        Dim pTypes As String = HiddenFieldpTypes.Value
        Dim sallowableAmountToRefund As String = String.Empty ''HiddenFieldAllowableAmountToRefund.Value
        If pTypes.IndexOf("R") <> -1 Then
            sallowableAmountToRefund = HiddenFieldAllowableAmountToRefundReceiptAndDepositNZ.Value
        ElseIf pTypes.IndexOf("D") <> -1 Then
            sallowableAmountToRefund = HiddenFieldAllowableAmountToRefundReceiptAndDepositNZ.Value
        ElseIf pTypes.IndexOf("B") <> -1 Then
            sallowableAmountToRefund = HiddenFieldAllowableAmountToRefundForBondNZ.Value
        End If

        If Not String.IsNullOrEmpty(sallowableAmountToRefund) And (custToPay.IndexOf("-") <> -1 Or IsRefund = True) Then
            Dim dAllowableAmountToRefund As Decimal = Math.Abs(Convert.ToDecimal(sallowableAmountToRefund))
            Dim enteredAmtCusToPay As Decimal = Math.Abs(CDec(custToPay))
            If (dAllowableAmountToRefund < enteredAmtCusToPay) Then
                If pTypes.IndexOf("B") <> -1 Then
                    MyBase.SetInformationShortMessage("The refund amount allowed for Bond type is only up to $" & sallowableAmountToRefund)
                Else
                    MyBase.SetInformationShortMessage("The refund amount allowed for Receipt or Deposit type is only up to $" & sallowableAmountToRefund)
                End If
                Return False
            End If
        End If
        Return True
    End Function
#End Region
End Class
