<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="CustomerFind.aspx.vb" Inherits="Booking_CustomerFind" Title="Untitled Page" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript">

function ClientValidation()
{
    var lastNameTextBox;
    lastNameTextBox = document.getElementById('<%=lastNameTextBox.ClientID%>');
    var lastNameTextBox;
    lastNameTextBox = document.getElementById('<%=lastNameTextBox.ClientID%>');    
    var firstNameTextBox;
    firstNameTextBox = document.getElementById('<%=firstNameTextBox.ClientID%>');
    var vipCheckBox;
    vipCheckBox = document.getElementById('<%=vipCheckBox.ClientID%>');
    var fleetDriverCheckBox;
    fleetDriverCheckBox = document.getElementById('<%=fleetDriverCheckBox.ClientID%>');
    var dobDateControl;
    dobDateControl = $get('<%= dobDateControl.ClientID %>');
    var streetTextBox;
    streetTextBox = document.getElementById('<%=streetTextBox.ClientID%>');
    var cityTextBox;
    cityTextBox = document.getElementById('<%=cityTextBox.ClientID%>');
    var stateTextBox;
    stateTextBox = document.getElementById('<%=stateTextBox.ClientID%>');
    var countryPickerControl;
    countryPickerControl = document.getElementById('<%=countryPickerControl.ClientID%>');
    var licenceTextBox;
    licenceTextBox = document.getElementById('<%=licenceTextBox.ClientID%>');
    
    if(	lastNameTextBox.value == "" && 
			firstNameTextBox.value == "" && 
			vipCheckBox.checked != true && 
			fleetDriverCheckBox.checked != true &&
			dobDateControl.value == "" && 
			streetTextBox.value == "" && 
			cityTextBox.value == "" && 
			stateTextBox.value == "" && 
			countryPickerControl.value == "" &&
			licenceTextBox.value == "" )
	{
	    setInformationShortMessage("You must enter at least one value when searching for a customer")
		return false
	}
	else
	{
	     return true  
	}
 
}
   
		
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td width="150">
                    Last Name:</td>
                <td width="250">
                    <asp:TextBox ID="lastNameTextBox" runat="server" MaxLength="128"></asp:TextBox></td>
                <td width="150">
                    First Name:</td>
                <td>
                    <asp:TextBox ID="firstNameTextBox" runat="server" MaxLength="128"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Date Of Birth:</td>
                <td>
                    <uc1:DateControl id="dobDateControl" runat="server">
                    </uc1:DateControl></td>
                <td>Flags:
                </td>
                <td>
                    <asp:CheckBox ID="vipCheckBox" runat="server" Text="VIP" AutoPostBack="false" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:CheckBox ID="fleetDriverCheckBox" runat="server" Text="Fleet Driver" AutoPostBack="false" /></td>
            </tr>
            <tr>
                <td>
                    Street:</td>
                <td>
                    <asp:TextBox ID="streetTextBox" runat="server" MaxLength="100"></asp:TextBox></td>
                <td>
                    City/Town:</td>
                <td>
                    <asp:TextBox ID="cityTextBox" runat="server" MaxLength="100"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    State:</td>
                <td>
                    <asp:TextBox ID="stateTextBox" runat="server" MaxLength="100"></asp:TextBox></td>
                <td>
                    Licence Number:</td>
                <td>
                    <asp:TextBox ID="licenceTextBox" runat="server" MaxLength="100"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    Country:</td>
                <td>
                    <uc1:PickerControl ID="countryPickerControl" runat="server" PopupType="CUSTOMERCOUNTRY"
                        AppendDescription="true" width="150" /></td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            <tr><td><br /></td></tr>
            
             </table>
          <table width=100%>  
          <tr>
              <td style="border-width: 1px 0px">
                    <i><asp:Label ID="resultLabel" runat="server" text=""></asp:Label></i>
                    </td>
             <td  align="right" style="border-width: 1px 0px">
                    <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" width="126"
                        OnClientClick="return ClientValidation();" />
                    <asp:Button ID="soundexButton" runat="server" Text="Soundex" CssClass="Button_Standard Button_Search" width="126"
                        OnClientClick="return ClientValidation();" />
                    <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" width="126"/>
                    <asp:Button ID="newButton" runat="server" Text="New" CssClass="Button_Standard Button_New" width="126" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" width="126" />
                </td>
            </tr>
        </table>
        <br />
        <asp:GridView ID="resultGridView" runat="server" Width="100%" AutoGenerateColumns="False"
            class="dataTable">
            <AlternatingRowStyle CssClass="oddRow" />
            <RowStyle CssClass="evenRow" />
            <Columns>
                <asp:TemplateField HeaderText="Id" Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="customerIdLabel" runat="server" Text='<%# Bind("CusId") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Name">
                    <ItemTemplate>
                        <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# Bind("CusName") %>' NavigateUrl='<%# GetCustomerUrl(Eval("CusId")) %>'></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CusAddress" HeaderText="Address" />
                <asp:BoundField DataField="CusDOB" HeaderText="Date Of Birth" />
                <asp:BoundField DataField="CusLicence" HeaderText="Licence Number" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
</asp:Content>
