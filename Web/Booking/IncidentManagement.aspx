<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	IncidentManagement.ascx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	15.4.8 / Shoel - Base version
''                      5.6.8  / Shoel - Fix for Issue 5 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=5)
''                      5.6.8  / Shoel - This page now has the "PopupHeader" master page
''                     30.7.8  / Shoel - Confirmation popup js errors fixed
''                     22.8.8  / Shoel - Odometer fields now doesnt allow non-numeric data 
''                     25.8.8  / Shoel - MaxLengths added to some fields
''                     24.9.8  / Shoel - Fixes for page behaviour when called from Change Over tab 
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" AutoEventWireup="false" CodeFile="IncidentManagement.aspx.vb"
    Inherits="Booking_IncidentManagement" MasterPageFile="~/Include/PopupHeader.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl"
    TagPrefix="uc1" %>
<asp:Content ID="cntSripts" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script type="text/javascript" language="javascript">

//        var prm = Sys.WebForms.PageRequestManager.getInstance();
//        prm.add_beginRequest(BeginRequestHandler);
//        prm.add_endRequest(EndRequestHandler);
//        
//        function BeginRequestHandler(sender, args) {
//            // $get('ctl00_ContentPlaceHolder_tabs_inclusiveProductsTabPanel_inclusiveProductsControl_removeInclusiveProductButton').disabled = true;
//            //var details = document.getElementById('ctl00_ContentPlaceHolder_txtDetails')
//            debugger;
//            var detailsCtrl = document.getElementById('txtDetails');
//            detailsCtrl.setAttribute('disabled', 'true');
//        }

//        function EndRequestHandler(sender, args) {
//            //$get('ctl00_ContentPlaceHolder_tabs_inclusiveProductsTabPanel_inclusiveProductsControl_ckoFromDropdown').disabled = false;
//        }

    function CountNoOfChar(field)
    {
	    if(field.value.length > 2000)
	    {
		    setWarningShortMessage("Maximum number of character for details reached");
		    field.value = field.value.substring(0,2000);
	    }	
    }
    
    function TrimText(Item)
    {
        if(Item.value.length > 90)
        {
            Item.value = Item.value.substring(0,90);
        }
    }
    
    
    function DispOtherText(Obj)
    {
	    if(Obj.options[Obj.selectedIndex].text == 'Other')
	    {
		    tdOtherText.style.display = 'block';
		    tdTxaOtherHolder.style.display = 'block';
		    tdTxaOtherHolder.children(0).value = '';
		    
	    }
	    else
	    {
            tdOtherText.style.display = 'none';
		    tdTxaOtherHolder.style.display = 'none';
		    tdTxaOtherHolder.children(0).value = '';
	    }
    }
    
    function ShowConfirm(MessageText,HeaderText)
    {
       var confirmationBoxBehavior = $find('programmaticConfirmationBoxBehavior');
       document.getElementById(confirmationBoxBehavior._PopupDragHandleControlID).children[0].innerText = HeaderText;
       document.getElementById("tdMessageHolder").children[0].innerText = MessageText;
    }
    
    
    function CheckVehicle()
    {
        var sChkBoxId  = event.srcElement.id;
        var oTBody = event.srcElement.parentNode.parentNode.parentNode;
        for (var i=0;i<oTBody.childNodes.length;i++)
        {
            if(oTBody.childNodes[i].childNodes[4].childNodes[1] != null)
            {
                if(oTBody.childNodes[i].childNodes[4].childNodes[1].id != null)
                    if(oTBody.childNodes[i].childNodes[4].childNodes[1].id != "")
                    {
                        if(oTBody.childNodes[i].childNodes[4].childNodes[1].id != sChkBoxId)
                        {
                            if (event.srcElement.checked)
                            {
                                //uncheck the other guys!
                                oTBody.childNodes[i].childNodes[4].childNodes[1].checked = false;
                            }
                        }
                    }
            }
        }
        
    }
    
    
    function SetRemoveAllCauses()
    {
        var sChkBoxId = event.srcElement.id;
        var oTBody = event.srcElement.parentNode.parentNode.parentNode;
        for (var i=0;i<oTBody.childNodes.length;i++)
        {
            if(oTBody.childNodes[i].childNodes[3].childNodes[0] != null)
            {
                if(oTBody.childNodes[i].childNodes[3].childNodes[0].id != null)
                    if(oTBody.childNodes[i].childNodes[3].childNodes[0].id != "")
                    {
                        if(oTBody.childNodes[i].childNodes[3].childNodes[0].id != sChkBoxId)
                        {
                            //uncheck-check the other guys!
                            oTBody.childNodes[i].childNodes[3].childNodes[0].checked = event.srcElement.checked;
                            
                        }
                    }
            }
        }
    }
    
    function SetClosedDateForStatus(DateTextBoxId,DateToSet)
    {
        if (event.srcElement.selectedIndex == 1)
        {
            document.getElementById(DateTextBoxId).value = DateToSet;
        }
        else
        {
            document.getElementById(DateTextBoxId).value = "";
        }
    }
    
    function GoBack(BackPath)
    {
        if((BackPath.indexOf("Booking.aspx") >-1) && (BackPath.indexOf("activeTab=13")  >-1) )
        //if its from the incidents tab
        {
            window.close();
            window.opener.location = BackPath;
        }
        else
        {
            window.location = BackPath;
        }
        return false;
    }
    
    function CheckKey()
    {
        var nVal = window.event.keyCode
        if ((nVal < 48 || nVal > 58)&&(nVal!=45))
	        if(document.selection.type!="Text")
	            window.event.keyCode = 8
    }
    
    function CheckContents(field)
    {
       if(isNaN(field.value))
       {
            field.value="";
       }
    }



//    function cancelPostbackOnSave() {
//        //var value = document.getElementById('<%=txtDetails.ClientID%>').value;
//        //    if (value == "") {
//        //return false;
//            return true;
//        }
    //    }

//    function handleStatusChange(e) {
//        var ctrlText = document.getElementById('<%=hiddenStatus.ClientID%>');
//        var e = document.getElementById('<%=cmbStatus.ClientID%>');
//        ctrlText.value = e.value;

//        } 
    
    
    </script>

</asp:Content>
<asp:Content ID="cntPage" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table align="left" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <table cellspacing="1" cellpadding="1" width="100%" align="center" border="0">
                            <tbody>
                                <tr>
                                    <td>
                                        <table id="tblIncidentDetails" cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tbody>
                                                <tr>
                                                    <td width="150">
                                                        Booking/Rental:</td>
                                                    <td width="250">
                                                        <asp:TextBox ID="lblBookingRentalNumber" runat="server" CssClass="Textbox_Standard"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td width="150">
                                                        AIMS Ref No:</td>
                                                    <td width="250">
                                                        <asp:TextBox ID="lblAIMSRefNo" runat="server" CssClass="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">
                                                        Hirer:</td>
                                                    <td width="250">
                                                        <asp:TextBox ID="lblHirer" runat="server" CssClass="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                    <td width="150">
                                                        Package:</td>
                                                    <td width="250">
                                                        <asp:TextBox ID="lblPackage" runat="server" CssClass="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">
                                                        Agent:
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:TextBox Style="width: 230px" ID="lblAgent" runat="server" CssClass="Textbox_Standard"
                                                            ReadOnly="true"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="tblVehicles" cellspacing="1" cellpadding="1" width="100%" border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <strong>Vehicles</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gwVehicles" class="dataTable" runat="server" Width="100%" AutoGenerateColumns="false">
                                                            <AlternatingRowStyle CssClass="oddRow" />
                                                            <RowStyle CssClass="evenRow" />
                                                            <Columns>
                                                                <asp:BoundField DataField="CheckOut" HeaderText="Check-Out">
                                                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="CheckIn" HeaderText="Check-In">
                                                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Vehicle" HeaderText="Vehicle">
                                                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UnitNo" HeaderText="Unit No/Reg">
                                                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText=" Select* ">
                                                                    <ItemTemplate>
                                                                        <%-- <asp:Label ID="Label1" runat="server" Text='<%# Bind("SelectedVechile") %>'></asp:Label>--%>
                                                                        &nbsp;<asp:RadioButton ID="radVehicle" runat="server" GroupName="rdgVehicle" Checked='<%# Eval("SelectedVechile") %>' /><%--onclick="selectVehicle(this)"--%>
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table id="tblVehicleDetails" cellspacing="1" cellpadding="1" width="100%" border="0">
                                            <tbody>
                                                <tr>
                                                    <td width="150">
                                                        Branch:
                                                    </td>
                                                    <td width="250">
                                                        <uc1:PickerControl ID="pkrBranch" runat="server" Width="150px" PopupType="LOCATION"
                                                            AppendDescription="true"></uc1:PickerControl>
                                                        &nbsp;*
                                                    </td>
                                                    <td width="150">
                                                        Date:
                                                    </td>
                                                    <td width="250">
                                                        <uc1:DateControl ID="dtLoggedDate" runat="server" AutoPostBack="false"></uc1:DateControl>
                                                        &nbsp;*
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">
                                                        Notified:
                                                    </td>
                                                    <td width="250">
                                                        <asp:DropDownList ID="cmbNotified" class="Dropdown_Standard" runat="server">
                                                        </asp:DropDownList>&nbsp;*
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td width="150">
                                                        Details:
                                                    </td>
                                                    <td colspan="3">
                                                        <textarea onblur="CountNoOfChar(this);" style="width: 650px" id="txaDetails" class="TextArea_Standard"
                                                            runat="server"></textarea>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <strong>Causes and Sub-Causes</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%--Update Panel for the combos--%>
                                                        <table cellspacing="0" cellpadding="0" width="100%" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="150">
                                                                        Causes:</td>
                                                                    <td width="250">
                                                                        <asp:DropDownList ID="cmbCauses" class="Dropdown_Standard" runat="server" AutoPostBack="true">
                                                                        </asp:DropDownList>&nbsp;*
                                                                    </td>
                                                                    <td width="150">
                                                                        Sub-Causes:</td>
                                                                    <td width="250">
                                                                        <asp:DropDownList ID="cmbSubCause" class="Dropdown_Standard" runat="server" Width="240px"
                                                                            onchange="DispOtherText(this);">
                                                                        </asp:DropDownList>&nbsp;*
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="display: none" id="tdOtherText" nowrap width="150">
                                                                        Other Text:</td>
                                                                    <td style="display: none" id="tdTxaOtherHolder" colspan="3">
                                                                        <textarea onblur="TrimText(this);" style="width: 100%" id="txaOther" rows="1" runat="server"></textarea>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                        <%--Update Panel for the combos--%>
                                                        <asp:UpdatePanel ID="udpCausesAddRemovePanel" runat="server">
                                                            <ContentTemplate>
                                                                <table id="tblCausesGridHolder" cellspacing="0" cellpadding="0" width="100%">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="right">
                                                                                <br />
                                                                                <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" CssClass="Button_Standard Button_Add"
                                                                                    Text="Add"></asp:Button>&nbsp;
                                                                                <asp:Button ID="btnRemove" OnClick="btnRemove_Click" runat="server" CssClass="Button_Standard Button_Remove"
                                                                                    Text="Remove"></asp:Button>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <br />
                                                                                <asp:GridView ID="gwCauses" runat="server" Width="100%" CssClass="dataTable" AutoGenerateColumns="False"
                                                                                    OnRowDataBound="gwCauses_RowDataBound" EmptyDataText="No Causes Added">
                                                                                    <RowStyle CssClass="evenRow" />
                                                                                    <AlternatingRowStyle CssClass="oddRow" />
                                                                                    <Columns>
                                                                                        <asp:BoundField DataField="SubCauseID">
                                                                                            <ItemStyle BorderStyle="None" HorizontalAlign="Left" />
                                                                                            <HeaderStyle BorderStyle="None" HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="IncidentId">
                                                                                            <ItemStyle BorderStyle="None" HorizontalAlign="Left" />
                                                                                            <HeaderStyle BorderStyle="None" HorizontalAlign="Left" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="CauseDescription" HeaderText="Cause">
                                                                                            <ItemStyle BorderStyle="None" HorizontalAlign="Left" Width="150px" Wrap="false" />
                                                                                            <HeaderStyle BorderStyle="None" HorizontalAlign="Left" Wrap="false" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="SubCauseDescription" HeaderText="Sub-Cause">
                                                                                            <ItemStyle BorderStyle="None" HorizontalAlign="Left" Width="150px" Wrap="false" />
                                                                                            <HeaderStyle BorderStyle="None" HorizontalAlign="Left" Wrap="false" />
                                                                                        </asp:BoundField>
                                                                                        <asp:TemplateField HeaderText="Other Text">
                                                                                            <ItemTemplate>
                                                                                                <asp:TextBox ID="txtOtherText" Style="width: 450px" MaxLength="90" runat="server"
                                                                                                    Text='<%# Bind("OtherText") %>'></asp:TextBox>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle BorderStyle="None" HorizontalAlign="Left" Width="450px" />
                                                                                            <HeaderStyle BorderStyle="None" HorizontalAlign="Left" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Remove">
                                                                                            <HeaderTemplate>
                                                                                                <asp:CheckBox ID="chkRemoveAllCauses" runat="server"></asp:CheckBox>&nbsp;Remove
                                                                                            </HeaderTemplate>
                                                                                            <ItemTemplate>
                                                                                                <asp:CheckBox ID="chkRemove" runat="server" Checked='<%# Eval("RemoveItem") %>'></asp:CheckBox>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle BorderStyle="None" HorizontalAlign="Left" Width="50px" Wrap="false" />
                                                                                            <HeaderStyle BorderStyle="None" HorizontalAlign="Left" Wrap="false" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="CauseID"></asp:BoundField>
                                                                                        <asp:BoundField DataField="IntegrityNo"></asp:BoundField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </ContentTemplate>
                                                        </asp:UpdatePanel>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <!-- Added:Raj Feb 16 2012----------------------------------------------------------->
        <tr>
            <td align="right">
                <br />
                <asp:Button ID="btnSave0" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />&nbsp;
                <asp:Button ID="btnSaveNew0"  runat="server" Text="Save/New" CssClass="Button_Standard Button_New"  />&nbsp;
                <asp:Button ID="btnCancel0" runat="server" Text="Reset/Cancel" CssClass="Button_Standard Button_Reset" />&nbsp;
                <asp:Button ID="btnBack0" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
              <asp:Label ID="lblStatus0" runat="server" Font-Bold="true" Font-Size="Small" Width="610" Height="17" BorderStyle="Solid"                      BorderWidth="0" Text="Status">
              </asp:Label>
            </td>
        </tr>
        <tr>
            <td>
              <asp:DropDownList ID="cmbStatus" class="Dropdown_Standard" runat="server" AutoPostBack="false">
              </asp:DropDownList>
              <asp:HiddenField ID="hiddenStatus" runat="server" EnableViewState="true" />
              &nbsp;&nbsp;&nbsp;&nbsp;
              <asp:Label ID="lblStatus" runat="server" Font-Bold="false" Font-Size="Small" Text="" Width="610" Height="17" BorderStyle="Solid" BorderWidth="0" BorderColor="Gray">
              </asp:Label>

            </td>
        </tr>
         <tr>
            <td>
                &nbsp;
            </td>
        </tr>
         <tr>
            <td>
              <asp:Label ID="lblOnRoad" runat="server" Text="On-Road Assistance Only" Font-Bold="true" Width="200" Height="18" BorderStyle="None" BorderWidth="0">
              </asp:Label>
            </td>
        </tr>        
        <tr>
            <td>
                <asp:GridView ID="gridViewIncidents" RowStyle-Height="25" runat="server" AutoGenerateColumns="false" GridLines="Horizontal" BorderColor="Black" BorderWidth="1" Width="800"  cellpadding="2" cellspacing="1" ShowHeader="true" >
<%--                OnRowEditing="gridViewIncidents_RowEditing" 
--%>

                     <AlternatingRowStyle CssClass="oddRow" />
                     <RowStyle CssClass="evenRow" /> 
                        <Columns>

                      <%--   <asp:TemplateField HeaderText="callBackID" SortExpression="CallBack No.">     
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server" Text='<%#Bind("callBackID") %>'></asp:TextBox>
                                </EditItemTemplate>    
                                <ItemTemplate>
                                    <asp:Label ID="Label1" runat="server" Text='<%#Bind("callBackID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                          <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                            <br />
                                <asp:LinkButton ID="btnDelete" Text="Delete" runat="server" CommandName="Delete" />
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />
                                <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />
                            </EditItemTemplate>

 

                            </asp:TemplateField>
--%>

                            <asp:BoundField DataField="num" HeaderText="No." HeaderStyle-Width="7%" ItemStyle-Width="7%"                 FooterStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top" />
                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top" />
                              </asp:BoundField> 
                            <asp:BoundField DataField="callBackID" HeaderText="CallBack ID" Visible="false" HeaderStyle-Width="10%" ItemStyle-Width="20%"                 FooterStyle-Width="10%">
                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top" />
                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top" />
                              </asp:BoundField> 
                              <asp:BoundField DataField="callBackDetails" HeaderText="Details" HeaderStyle-Width="43%" ItemStyle-Width="43%">
                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top" />
                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" Wrap="true"  VerticalAlign="Top" />
                              </asp:BoundField> 
                              <asp:BoundField DataField="callBackUser" HeaderText="User" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top" />
                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top"  />
                              </asp:BoundField> 
                              <asp:BoundField DataField="callBackTime" HeaderText="Date & Time" HeaderStyle-Width="20%" ItemStyle-Width="20%">
                                    <HeaderStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top"  />
                                    <ItemStyle HorizontalAlign="Left" BorderStyle="None" VerticalAlign="Top"  />
                            </asp:BoundField>
                        </Columns> 
                </asp:GridView> 
            </td>
        </tr>
         <tr>
            <td>
                &nbsp;
            </td>
        </tr>
         <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
              <asp:Label ID="lblInfo" runat="server" Text="Details" Font-Bold="true" Width="200" Height="18" BorderStyle="None" BorderWidth="0">
              </asp:Label>

            </td>
        </tr>
        <tr>
         <td width="800">
                <asp:TextBox ID="txtDetails" runat="server" Width="800" Height="100" ReadOnly="false" TextMode="MultiLine"></asp:TextBox>
          </td>
        </tr>
         
        <!-- Added:Raj Feb 16 2012----------------------------------------------------------->


        <tr>
            <td align="right">
                <br />
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />&nbsp;
                <asp:Button ID="btnSaveAndNew" runat="server" Text="Save/New" CssClass="Button_Standard Button_New"  />&nbsp;
                <asp:Button ID="btnCancel" runat="server" Text="Reset/Cancel" CssClass="Button_Standard Button_Reset" />&nbsp;
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
    </table>
    <uc1:ConfimationBoxControl ID="confimationBoxControl" runat="server" LeftButton_Text="Continue"
        RightButton_Text="Cancel" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
        Text="Are you sure you wish to remove the selected subcauses from this incident?"
        Title="Confirmation Required" />
</asp:Content>
