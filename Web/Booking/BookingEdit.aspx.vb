'' Change Log!
'' 29.10.2008 - Shoel   - Added show/hide on 'AgnIsDirect' = 0/1

Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

<AuroraPageTitleAttribute("Modify Booking :  Booking :")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005,GEN046,GEN090")> _
Partial Class Booking_BookingEdit
    Inherits AuroraPage

    Dim bookingId As String = ""
    Dim rentalId As String = ""
    Dim bookingNumber As String = ""
    Dim isFromSearch As String = "0"

    Private Const BookingEdit_OldAgentId_ViewState As String = "BookingEdit_OldAgentId"
    Private Const BookingEdit_IntNo_ViewState As String = "BookingEdit_IntNo"
    Private Const BookingEdit_AgnIsMisc_ViewState As String = "BookingEdit_AgnIsMisc"
    Private Const BookingEdit_AgnIsDirect_ViewState As String = "BookingEdit_AgnIsDirect"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3133116"
            'rentalId = "3287253-1"

            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If
            If Not Request.QueryString("hdBookingNum") Is Nothing Then
                bookingNumber = Request.QueryString("hdBookingNum").ToString()
            End If
            If Not Request.QueryString("isFromSearch") Is Nothing Then
                isFromSearch = Request.QueryString("isFromSearch").ToString()
            End If

            bookingRequestAgentMgtPopupUserControl1.BookingId = bookingId

            'set page title
            If Not Page.IsPostBack Then
                ''rev:mia Sept.2 2013 - Customer Title from query
                PopulateTitle()

                referralTypeDropDownList_DataBind()
                setBookingValues()

                'Back from 
                If Not Request.QueryString("agentId") Is Nothing Then
                    agentPickerControl.DataId = Request.QueryString("agentId").ToString()
                End If
                If Not Request.QueryString("agentText") Is Nothing Then
                    agentPickerControl.Text = Request.QueryString("agentText").ToString()
                End If
            End If

            'Me.SetValueLink(bookingId, "~\Booking\BookingEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId)
            Me.SetValueLink(bookingNumber, "~\Booking\BookingEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&hdBookingNum=" & bookingNumber & "&isFromSearch=" & isFromSearch)

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub referralTypeDropDownList_DataBind()

        Dim ds As DataSet = Booking.GetReferralType()

        referralTypeDropDownList.DataSource = ds.Tables(0)
        referralTypeDropDownList.DataTextField = "DESCRIPTION"
        referralTypeDropDownList.DataValueField = "ID"
        referralTypeDropDownList.DataBind()
    End Sub

    Private Sub setBookingValues()
        Dim ds As DataSet = Booking.GetBookingDetail2(bookingId, UserCode)
        If ds.Tables.Count > 0 Then

            If ds.Tables(0).Rows(0)("AgnIsDirect").ToString() = "1" Then
                updateAgentLinkButton.Style("display") = "none"
                addAgentLinkButton.Style("display") = "none"
                agentPickerControl.ReadOnly = True
            Else
                updateAgentLinkButton.Style("display") = "block"
                addAgentLinkButton.Style("display") = "block"
                agentPickerControl.ReadOnly = False
            End If

            agentPickerControl.DataId = ds.Tables(0).Rows(0)("AgnId")
            agentPickerControl.Text = ds.Tables(0).Rows(0)("AgnDet")
            ''rev:mia 15jan2014-addition of SlotId, SlotDescription, RentalId
            OriginalAgent = String.Concat(agentPickerControl.DataId, "|", agentPickerControl.Text)

            surnameTextBox.Text = ds.Tables(0).Rows(0)("LName")
            firstNameTextBox.Text = ds.Tables(0).Rows(0)("FName")

            ''rev:mia Sept.2 2013 - Customer Title from query
            ''titleTextBox.Text = ds.Tables(0).Rows(0)("Title")
            Try
                customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(DecodeText(ds.Tables(0).Rows(0)("Title").ToString)))
            Catch ex As Exception
                customertitleDropdown.SelectedIndex = 0
            End Try

            referralTypeDropDownList.SelectedValue = ds.Tables(0).Rows(0)("RefType")
            'statusLabel.Text = ds.Tables(0).Rows(0)("ST")
            'checkOutLabel.Text = ds.Tables(0).Rows(0)("CkoDt")
            'checkInLabel.Text = ds.Tables(0).Rows(0)("CkiDt")
            'transferredLabel.Text = ds.Tables(0).Rows(0)("FinWhen")

            statusTextBox.Text = ds.Tables(0).Rows(0)("ST")
            bookingNumber = ds.Tables(0).Rows(0)("BooNum")

            'checkOutTextBox.Text = ds.Tables(0).Rows(0)("CkoDt")
            'checkInTextBox.Text = ds.Tables(0).Rows(0)("CkiDt")
            'transferredTextBox.Text = ds.Tables(0).Rows(0)("FinWhen")
            checkOutTextBox.Text = Utility.DateDBUIConvert(ds.Tables(0).Rows(0)("CkoDt"))
            checkInTextBox.Text = Utility.DateDBUIConvert(ds.Tables(0).Rows(0)("CkiDt"))
            transferredTextBox.Text = Utility.DateDBUIConvert(ds.Tables(0).Rows(0)("FinWhen"))

            'oldAgentIdLabel.Text = ds.Tables(0).Rows(0)("OLD_AgnId")
            'intNoLabel.Text = ds.Tables(0).Rows(0)("IntNo")

            ViewState(BookingEdit_OldAgentId_ViewState) = ds.Tables(0).Rows(0)("OLD_AgnId")
            ViewState(BookingEdit_IntNo_ViewState) = ds.Tables(0).Rows(0)("IntNo")
            ViewState(BookingEdit_AgnIsMisc_ViewState) = Utility.ParseInt(ds.Tables(0).Rows(0)("AgnIsMisc"), 0)
            ViewState(BookingEdit_AgnIsDirect_ViewState) = Utility.ParseInt(ds.Tables(0).Rows(0)("AgnIsDirect"), 0)


        End If

    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        ''rev:mia issue # 554 <Back> button not functioning as expected
        Dim tempActiveTab As String = Request.QueryString("activeTab")
        tempActiveTab = IIf(String.IsNullOrEmpty(tempActiveTab), "", "&activeTab=" & tempActiveTab)
        Response.Redirect("Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&isFromSearch=" & isFromSearch & tempActiveTab)
    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        'If String.IsNullOrEmpty(agentPickerControl.DataId) Then
        '    Dim errorMessage As String = ""
        '    agentPickerControl.DataId = getAgentId(agentPickerControl.Text, errorMessage)

        '    If Not String.IsNullOrEmpty(errorMessage) Then
        '        SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
        '        'AddErrorMessage(errorMessage)
        '        Return
        '    End If
        'End If

        'Dim returnError As String = ""
        ''Dim returnMessage As String = ""
        ''Dim updateFleet As String = ""
        ''Dim blrIdList As String = ""

        ' ''''rev:mia Sept.2 2013 - Customer Title from query
        ' ''titleTextBox.Text was replace by customertitleDropdown.SelectedItem.Text
        'returnError = Booking.SaveBooking(bookingId, _
        '                                  agentPickerControl.DataId, _
        '                                  firstNameTextBox.Text, _
        '                                  surnameTextBox.Text, _
        '                                  customertitleDropdown.SelectedItem.Text, _
        '                                  referralTypeDropDownList.SelectedValue, _
        '                                  ViewState(BookingEdit_OldAgentId_ViewState), _
        '                                  ViewState(BookingEdit_IntNo_ViewState), _
        '                                  UserCode)

        ''returnError = Booking.SaveBooking(bookingId, agentPickerControl.DataId, firstNameTextBox.Text, _
        ''   surnameTextBox.Text, titleTextBox.Text, referralTypeDropDownList.SelectedValue, _
        ''   oldAgentIdLabel.Text, intNoLabel.Text, UserCode)

        'If Not String.IsNullOrEmpty(returnError) Then
        '    SetShortMessage(AuroraHeaderMessageType.Error, returnError)
        '    'AddErrorMessage(returnError)
        'Else
        '    setBookingValues()
        '    SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        'End If
        PreSave()
    End Sub

    Private Function getAgentId(ByVal text As String, ByRef errorMessage As String) As String

        Dim dr As DataRow = Aurora.Common.Service.SearchPopupItem("AGENT", "Agent", text, errorMessage, UserCode)
        If Not dr Is Nothing Then
            Return dr("ID")
        Else
            Return Nothing
        End If

    End Function

    Protected Sub updateAgentLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles updateAgentLinkButton.Click

        If String.IsNullOrEmpty(agentPickerControl.DataId) Then
            Me.SetWarningShortMessage("No Valid agent has been specified in the Agent field")
        Else

            Dim agentCode As String = agentPickerControl.Text.Split("-")(0).Trim
            Dim dt As New DataTable
            dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
            Dim agnIsMisc As Boolean = False
            If dt.Rows.Count > 0 And dt.Columns.Contains("ISMISC") Then
                agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
            End If

            If agnIsMisc Then
                'Todo
                'window.open("../asp/BookingRequestAgentMgt.asp?funCode=RS-MISAGNMGT&htm_hid_Screen=BookingRequest&hid_html_booId=" + bookingId, "win1", "top=0,left=0,width=600,height=450")
                bookingRequestAgentMgtPopupUserControl1.BookingId = bookingId
                bookingRequestAgentMgtPopupUserControl1.loadPopup()
            Else
                Response.Redirect("~/Agent/Maintenance.aspx?agnId=" & agentPickerControl.DataId)
            End If
        End If

    End Sub

    Protected Sub addAgentLinkButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addAgentLinkButton.Click

        If String.IsNullOrEmpty(agentPickerControl.Text) Then
            'Response.Redirect("~/Agent/Maintenance.aspx?agnId=")
            Response.Redirect("~/../asp/AgentMgt.asp?funCode=AGENT&htm_hid_prevFunFileName=" & Request.RawUrl)
        Else
            If String.IsNullOrEmpty(agentPickerControl.DataId) Then

                Dim agentName As String = ""
                agentName = agentPickerControl.Text
                Dim ds As DataSet
                ds = Booking.ShowBookingRequestAgent(agentName)

                If ds.Tables.Count = 0 OrElse ds.Tables(0).Rows.Count = 0 OrElse ds.Tables(0).Rows(0)(0) = "Error" Then
                    'Response.Redirect("~/Agent/Maintenance.aspx?agnId=")
                    Response.Redirect("~/../asp/AgentMgt.asp?funCode=AGENT&htm_hid_prevFunFileName=" & Request.RawUrl)
                Else
                    'Todo
                    'window.open('../asp/showBookingRequestAgent.asp?funCode=RS-SHOBKRAGN&hid_html_agnCode='+ document.forms[0].sTxtAgent.value ,'win1','top=0,left=0,width=600,height=450')
                    showBookingRequestAgentPopupUserControl1.loadPopup(agentPickerControl.Text)
                End If

            Else
                Dim agentCode As String = agentPickerControl.Text.Split("-")(0).Trim
                Dim dt As New DataTable
                dt = Aurora.Common.Service.GetPopUpData("AGENT", agentCode, "", "", "", "", UserCode)
                Dim agnIsMisc As Boolean = False
                If dt.Rows.Count > 0 Then
                    agnIsMisc = Utility.ParseBoolean(Utility.ParseInt(dt.Rows(0)("ISMISC"), 0), False)
                End If

                If agnIsMisc Then
                    'Todo
                    'window.open('../asp/BookingRequestAgentMgt.asp?funCode=RS-MISAGNMGT&htm_hid_Screen=BookingRequest&hid_html_bkrId=61106829-A7F3-4C4A-8F0A-B78C5E223E9B' ,'win1','top=0,left=0,width=600,height=450')
                    bookingRequestAgentMgtPopupUserControl1.BookingId = bookingId
                    bookingRequestAgentMgtPopupUserControl1.loadPopup()
                    Return
                Else
                    'todo
                    'window.open('../asp/showBookingRequestAgent.asp?funCode=RS-SHOBKRAGN&hid_html_agnCode='+ document.forms[0].sTxtAgent.value ,'win1','top=0,left=0,width=600,height=450')
                    showBookingRequestAgentPopupUserControl1.loadPopup(agentPickerControl.Text)
                    Return
                End If
            End If

        End If

    End Sub

#Region "popup postback"

    Protected Sub showBookingRequestAgentPopupUserControl1_PostBack(ByVal sender As Object, ByVal addButton As Boolean, ByVal selectedAgent As Boolean, ByVal param As Object) Handles showBookingRequestAgentPopupUserControl1.PostBack

        If addButton Then
            'Response.Redirect("~/Agent/Maintenance.aspx?agnId=")
            Response.Redirect("~/../asp/AgentMgt.asp?funCode=AGENT&htm_hid_prevFunFileName=" & Request.RawUrl)
        ElseIf selectedAgent Then

            Dim list As ArrayList
            list = CType(param, ArrayList)

            Dim agentId As String = Utility.ParseString(list.Item(0), "")
            Dim agentCode As String = Utility.ParseString(list.Item(1), "")
            Dim agentName As String = Utility.ParseString(list.Item(2), "")

            agentPickerControl.DataId = agentId
            agentPickerControl.Text = agentCode & " - " & agentName

        End If

    End Sub

    Protected Sub bookingRequestAgentMgtPopupUserControl1_PostBack(ByVal sender As Object, ByVal saveButton As Boolean, ByVal backButton As Boolean, ByVal param As Object) Handles bookingRequestAgentMgtPopupUserControl1.PostBack

        If saveButton Then

        End If

    End Sub

#End Region

#Region "rev:mia Sept.2 2013 - Customer Title from query"
    Sub PopulateTitle()
        customertitleDropdown.Items.Clear()
        customertitleDropdown.AppendDataBoundItems = True
        customertitleDropdown.Items.Add(New ListItem("", 0))
        customertitleDropdown.DataSource = Aurora.Booking.Services.BookingCustomer.GetCustomerTitle
        customertitleDropdown.DataTextField = "CodDesc"
        customertitleDropdown.DataValueField = "CodCode"
        customertitleDropdown.DataBind()
    End Sub
#End Region

#Region "rev:mia 15jan2015-addition of supervisor override password"
    ''rev:mia 15Jan2015 - Today in PH history, Pope Francis is being expected in  Manila and Tacloban for
    ''                    a 3 days visit.
    Private Property OriginalAgent As String
        Get
            Return CStr(ViewState("OriginalAgent"))
        End Get
        Set(value As String)
            ViewState("OriginalAgent") = value
        End Set
    End Property

    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property

    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property

    Sub PreSave()

        Dim newAgentID As String = agentPickerControl.DataId
        Dim newAgentName As String = agentPickerControl.Text

        If (Not String.IsNullOrEmpty(OriginalAgent)) Then
            If (Not OriginalAgent.Split("|")(0).Equals(newAgentID)) And (Not OriginalAgent.Split("|")(1).Equals(newAgentName)) Then
                If (SlotAvailableControl.IsSlotRequired(Nothing, rentalId, AgnId:=newAgentID) = True) Then
                    SlotAvailableControl.NoOfBulkBookings = 1 ''set to one
                    SlotAvailableControl.SupervisorOverride = False
                    SlotAvailableControl.Show(Nothing, rentalId, AgnId:=newAgentID)
                Else
                    Save()
                End If
            Else
                Save()
            End If
        End If

    End Sub

    Protected Sub SlotAvailableControl_PostBack(sender As Object, isSaveButton As Boolean, param As String) Handles SlotAvailableControl.PostBack
        If (isSaveButton) Then
            SlotId = param.Split("|")(1)
            SlotDescription = param.Split("|")(2)
            Save()
            Logging.LogDebug("BookingEdit SlotAvailableControl_PostBack", "SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End If
    End Sub

    Sub Save()
        If String.IsNullOrEmpty(agentPickerControl.DataId) Then
            Dim errorMessage As String = ""
            agentPickerControl.DataId = getAgentId(agentPickerControl.Text, errorMessage)

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return
            End If
        End If

        Dim returnError As String = ""

        returnError = Booking.SaveBooking(bookingId, _
                                          agentPickerControl.DataId, _
                                          firstNameTextBox.Text, _
                                          surnameTextBox.Text, _
                                          customertitleDropdown.SelectedItem.Text, _
                                          referralTypeDropDownList.SelectedValue, _
                                          ViewState(BookingEdit_OldAgentId_ViewState), _
                                          ViewState(BookingEdit_IntNo_ViewState), _
                                          UserCode, SlotId, SlotDescription, rentalId)


        If Not String.IsNullOrEmpty(returnError) Then
            SetShortMessage(AuroraHeaderMessageType.Error, returnError)
        Else
            setBookingValues()
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        End If
    End Sub


#End Region

End Class
