'' Change Log
''
'' 16.6.9       -   Shoel       -   Added the log
'' 16.6.9       -   Shoel       -   Fix for Call#25450 - Location pop up hiding behind booking screen
'' 14.04.2011   -   D'General   -   rev:mia April 14 2011 - added checking for CI

Imports System.Xml
Imports Aurora.Common
Imports System.data
Imports System.Drawing
Imports Aurora.Booking.Services


<AuroraPageTitleAttribute("Maintain Booked Product : Booking: ")> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005,GEN046")> _
Partial Class Booking_BookedProductMgt
    Inherits AuroraPage

    Dim bookingId As String
    Dim rentalId As String
    Dim bookingNum As String
    'Dim bpdId As String

    Private Const BookedProductMgt_SapId_ViewState As String = "BookedProductMgt_SapId"
    Private Const BookedProductMgt_BookedProductXml_ViewState As String = "BookedProductMgt_BookedProductXml"
    Private Const BookedProductMgt_BpdId_ViewState As String = "BookedProductMgt_BpdId"
    ''rev:mia oct.6 
    Private sc As ScriptManager
    ''rev:mia oct.18 
    Private Const INVALID_LOCATION As String = "Found no match for Location, refine your selection"

    Property ReturnedSAPId() As String
        Get
            If ViewState("BPM_ReturnedSAPId") Is Nothing Then
                Return String.Empty
            Else
                Return CStr(ViewState("BPM_ReturnedSAPId"))
            End If


        End Get
        Set(ByVal value As String)
            ViewState("BPM_ReturnedSAPId") = value
        End Set
    End Property

    Private Property BpdId() As String
        Get
            Return ViewState(BookedProductMgt_BpdId_ViewState)
        End Get
        Set(ByVal value As String)
            ViewState(BookedProductMgt_BpdId_ViewState) = value
        End Set
    End Property


    Private Property sOverrideCode() As String
        Get
            Return ViewState("BPM_SUP_OVERRIDE_CODE")
        End Get
        Set(ByVal value As String)
            ViewState("BPM_SUP_OVERRIDE_CODE") = value
        End Set
    End Property

    'Property FirstTimeCallToManageBookedProductChanges() As Boolean
    '    Get
    '        Return CBool(ViewState("BPM_FirstCall2MBPC"))
    '    End Get
    '    Set(ByVal value As Boolean)
    '        ViewState("BPM_FirstCall2MBPC") = value
    '    End Set
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''rev:mia oct.6 
        sc = ScriptManager.GetCurrent(Me)

        If Not Request.QueryString("hdBookingId") Is Nothing Then
            bookingId = Request.QueryString("hdBookingId").ToString()
        End If

        If Not Request.QueryString("hdRentalId") Is Nothing Then
            rentalId = Request.QueryString("hdRentalId").ToString()
        End If

        If Not Request.QueryString("hdBookingNum") Is Nothing Then
            bookingNum = Request.QueryString("hdBookingNum").ToString()
        End If

        If Not Page.IsPostBack Then
            If Not Request.QueryString("bpdId") Is Nothing Then
                BpdId = Request.QueryString("bpdId").ToString()
            End If
            formDataBind()
        End If

        MyBase.SetValueLink(bookingNum, "~/Booking/BookingEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&hdBookingNum=" & bookingNum)

        setLocationPickerControl()

        ''rev:mia 12Aug
        ConfirmationBoxExtendedInterface()

        ''rev:mia Oct.6
        SetfocusOnPostBack(sc.AsyncPostBackSourceElementID)

        '' "GST CHANGES"
        InitialiseGSTStuff()
        '' "GST CHANGES"

        ''rev:mia April 
        ''DisabledStatusButtonAndDropDown()
    End Sub

#Region "Control events"

    Protected Sub SaveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        PreSave()
    End Sub

    Protected Sub BackButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=" & BookingTabIndex.Products)
    End Sub

    Protected Sub SelectProductButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles selectProductButton.Click
        Response.Redirect("BookedProductSelectMgt.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId)
    End Sub

    Protected Sub TravelFromDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles travelFromDateControl.DateChanged
        chargedFromDateControl.Date = travelFromDateControl.Date
    End Sub

    Protected Sub TravelFromTimeControl_TimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles travelFromTimeControl.TimeChanged
        chargedFromTimeControl.Time = travelFromTimeControl.Time
    End Sub

    Protected Sub TravelToDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles travelToDateControl.DateChanged
        chargedToDateControl.Date = travelToDateControl.Date
    End Sub

    Protected Sub TravelToTimeControl_TimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles travelToTimeControl.TimeChanged
        chargedToTimeControl.Time = travelToTimeControl.Time
    End Sub


#End Region

#Region "Private sub"

    Private Sub formDataBind()

        'Dim ds As DataSet
        'ds = BookingProducts.GetMaintainBookedProduct(bpdId)

        Dim oXmlDoc As XmlDocument
        oXmlDoc = BookingProducts.GetMaintainBookedProduct(BpdId)

        Dim ds As DataSet = New DataSet()
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        ''rev:mia 12Aug
        Me.BookedProductString = ds.GetXml ''oXmlDoc.SelectSingleNode("data/BookedProduct").OuterXml

        If ds.Tables.Count = 4 Then

            CKO = ds.Tables("Rental").Rows(0)("CkoDay")
            CKI = ds.Tables("Rental").Rows(0)("CkiDay")

            'Rental
            agentTextBox.Text = ds.Tables("Rental").Rows(0)("AgnName")
            hirerTextBox.Text = ds.Tables("Rental").Rows(0)("Hirer")
            statusTextBox.Text = ds.Tables("Rental").Rows(0)("BookingSt")

            'rental gridview
            rentalGridView.DataSource = ds.Tables("Rental")
            rentalGridView.DataBind()
            SetGridViewStatusColor()

            'Booked Product
            productPickerControl.DataId = ds.Tables("BookedProduct").Rows(0)("PrdId")
            productPickerControl.Text = ds.Tables("BookedProduct").Rows(0)("PrdName")

           

            If ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then
                productPickerControl.ReadOnly = False
            Else
                productPickerControl.ReadOnly = True
            End If

            'aimsAssetTypeDropDownList.SelectedValue = ds.Tables("BookedProduct").Rows(0)("AimsAssetType")
            'unitNumTextBox.Text = ds.Tables("BookedProduct").Rows(0)("UnitNum")
            'If ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then
            '    aimsAssetTypeDropDownList.Enabled = True
            '    unitNumTextBox.ReadOnly = False
            'Else
            '    aimsAssetTypeDropDownList.Enabled = False
            '    unitNumTextBox.ReadOnly = True
            'End If
            unitNumTextBox.Text = ds.Tables("BookedProduct").Rows(0)("UnitNumber")
            regoTextBox.Text = ds.Tables("BookedProduct").Rows(0)("RegoNumber")
            serialTextBox.Text = ds.Tables("BookedProduct").Rows(0)("SrCd")

            'initialStatusTextBox
            initialStatusTextBox.Text = ds.Tables("BookedProduct").Rows(0)("IniSt")

            'Current status
            Dim bpdStatusDs As DataSet
            bpdStatusDs = Data.GetPopUpData("getBpdStatus", ds.Tables("Rental").Rows(0)("Status"), ds.Tables("BookedProduct").Rows(0)("SapId"), BpdId, "", "", Me.UserCode)
            currentDropDownList.DataSource = bpdStatusDs.Tables(0)
            currentDropDownList.DataTextField = "DESCRIPTION"
            currentDropDownList.DataValueField = "CODE"
            currentDropDownList.DataBind()

            If ds.Tables("BookedProduct").Rows(0)("St") = "XX" Or ds.Tables("BookedProduct").Rows(0)("St") = "XN" Then
                currentDropDownList.Enabled = False
                currentDropDownList.Items.Clear()

                'XN	Cancelled Before Confirmation
                'XX	Cancelled After Confirmation

                currentDropDownList.Items.Add(New ListItem("Cancelled Before Confirmation", "XN"))
                currentDropDownList.Items.Add(New ListItem("Cancelled After Confirmation", "XX"))

                currentDropDownList.SelectedValue = ds.Tables("BookedProduct").Rows(0)("St")
            Else
                currentDropDownList.Enabled = True
                currentDropDownList.SelectedValue = ds.Tables("BookedProduct").Rows(0)("St")
            End If



            ''Booked Product Detail Table
            If ds.Tables("BookedProduct").Rows(0)("CkiL") = "1" Or _
                ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                ds.Tables("BookedProduct").Rows(0)("CkoL") = "1" Or _
                ds.Tables("BookedProduct").Rows(0)("CkiD") = "1" Or _
                ds.Tables("BookedProduct").Rows(0)("CkoD") = "1" Or _
                ds.Tables("BookedProduct").Rows(0)("UomId") = "KM" Or _
                ds.Tables("BookedProduct").Rows(0)("UomId") = "KMS" Then
                '<xsl:if test="BookedProduct/.[CkiL='1' or IsVehicle='1' or CkoL ='1' or CkiD='1' or CkoD='1' or UomId ='KM' or UomId ='KMS']">

                'location col
                Dim locationFromId As String = ""
                If Not String.IsNullOrEmpty(ds.Tables("BookedProduct").Rows(0)("CkoLoc")) Then
                    locationFromId = ds.Tables("BookedProduct").Rows(0)("CkoLoc").ToString()
                    Dim a() As String
                    a = locationFromId.Split("-")
                    locationFromId = a(0).Trim()
                    locationFromPickerControl.DataId = locationFromId
                    locationFromPickerControl.Text = ds.Tables("BookedProduct").Rows(0)("CkoLoc")
                End If
                ''rev:mia oct.18 - commented
                'locationFromPickerControl.Param2 = ds.Tables("BookedProduct").Rows(0)("CtyCode")
                'locationFromPickerControl.Param4 = ds.Tables("Rental").Rows(0)("RntId")

                Dim locationToId As String = ""
                If Not String.IsNullOrEmpty(ds.Tables("BookedProduct").Rows(0)("CkiLoc")) Then
                    locationToId = ds.Tables("BookedProduct").Rows(0)("CkiLoc").ToString()
                    Dim a() As String
                    a = locationToId.Split("-")
                    locationToId = a(0).Trim()
                    locationToPickerControl.DataId = locationToId
                    locationToPickerControl.Text = ds.Tables("BookedProduct").Rows(0)("CkiLoc")
                End If
                ''''rev:mia oct.18 - commented
                ''locationToPickerControl.Param2 = ds.Tables("BookedProduct").Rows(0)("CtyCode")
                ''locationToPickerControl.Param4 = ds.Tables("Rental").Rows(0)("RntId")

                'Readonly status
                If (ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("CkoL") = "1") And _
                    ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then
                    locationFromPickerControl.ReadOnly = False
                Else
                    locationFromPickerControl.ReadOnly = True
                End If

                If (ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                ds.Tables("BookedProduct").Rows(0)("CkiL") = "1") And _
                ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then
                    locationToPickerControl.ReadOnly = False
                Else
                    locationToPickerControl.ReadOnly = True
                End If



                'Travel from
                travelFromDateControl.Date = Utility.ParseDateTime(ds.Tables("BookedProduct").Rows(0)("BpdCkoDt"))
                travelFromTimeControl.Text = ds.Tables("BookedProduct").Rows(0)("BpdCkoTm")
                frTextBox.Text = ds.Tables("BookedProduct").Rows(0)("Fr")

                'Travel from view status
                travelFromDateControl.Visible = True
                travelFromTimeControl.Visible = True
                frTextBox.Visible = True

                If ds.Tables("BookedProduct").Rows(0)("UomId") = "KM" Or _
                    ds.Tables("BookedProduct").Rows(0)("UomId") = "KMS" Then

                    If ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                        ds.Tables("BookedProduct").Rows(0)("CkoD") = "1" Then
                        frTextBox.ReadOnly = False
                        travelFromDateControl.Visible = False
                        travelFromTimeControl.Visible = False
                    Else
                        frTextBox.ReadOnly = True
                        travelFromDateControl.Visible = False
                        travelFromTimeControl.Visible = False
                    End If
                Else
                    If ds.Tables("BookedProduct").Rows(0)("IsXFRM") = "1" Then
                        frTextBox.Visible = False
                        travelFromDateControl.ReadOnly = True
                        travelFromTimeControl.ReadOnly = True
                    ElseIf (ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                        ds.Tables("BookedProduct").Rows(0)("CkoD") = "1") And _
                        ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then

                        frTextBox.Visible = False
                        travelFromDateControl.ReadOnly = False
                        travelFromTimeControl.ReadOnly = False
                    Else
                        frTextBox.Visible = False
                        travelFromDateControl.ReadOnly = True
                        travelFromTimeControl.ReadOnly = True
                    End If
                End If


                'Travel to 
                travelToDateControl.Date = Utility.ParseDateTime(ds.Tables("BookedProduct").Rows(0)("BpdCkiDt"))
                travelToTimeControl.Text = ds.Tables("BookedProduct").Rows(0)("BpdCkiTm")
                toTextBox.Text = ds.Tables("BookedProduct").Rows(0)("To")

                'Travel to view status
                travelToDateControl.Visible = True
                travelToTimeControl.Visible = True
                toTextBox.Visible = True

                If ds.Tables("BookedProduct").Rows(0)("UomId") = "KM" Or _
                   ds.Tables("BookedProduct").Rows(0)("UomId") = "KMS" Then

                    If ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                        ds.Tables("BookedProduct").Rows(0)("CkiD") = "1" Then
                        toTextBox.ReadOnly = False
                        travelToDateControl.Visible = False
                        travelToTimeControl.Visible = False
                    Else
                        toTextBox.ReadOnly = True
                        travelToDateControl.Visible = False
                        travelToTimeControl.Visible = False
                    End If
                Else
                    If ds.Tables("BookedProduct").Rows(0)("IsXFRM") = "1" Then
                        toTextBox.Visible = False
                        travelToDateControl.ReadOnly = True
                        travelToTimeControl.ReadOnly = True
                    ElseIf (ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                        ds.Tables("BookedProduct").Rows(0)("CkiD") = "1") And _
                        ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then

                        toTextBox.Visible = False
                        travelToDateControl.ReadOnly = False
                        travelToTimeControl.ReadOnly = False
                    Else
                        toTextBox.Visible = False
                        travelToDateControl.ReadOnly = True
                        travelToTimeControl.ReadOnly = True
                    End If
                End If


                'Charged col
                chargedFromDateControl.Date = Utility.ParseDateTime(ds.Tables("BookedProduct").Rows(0)("ChgFrDt"))
                chargedFromTimeControl.Text = ds.Tables("BookedProduct").Rows(0)("ChgFrTm")
                If (ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("CkoD") = "1") And _
                    ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then

                    chargedFromDateControl.ReadOnly = False
                    chargedFromTimeControl.ReadOnly = False
                Else
                    chargedFromDateControl.ReadOnly = True
                    chargedFromTimeControl.ReadOnly = True
                End If


                chargedToDateControl.Date = Utility.ParseDateTime(ds.Tables("BookedProduct").Rows(0)("ChgToDt"))
                chargedToTimeControl.Text = ds.Tables("BookedProduct").Rows(0)("ChgToTm")
                If (ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("CkiD") = "1") And _
                    ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then

                    chargedToDateControl.ReadOnly = False
                    chargedToTimeControl.ReadOnly = False
                Else
                    chargedToDateControl.ReadOnly = True
                    chargedToTimeControl.ReadOnly = True
                End If


                'odo col
                odoFromTextBox.Text = ds.Tables("BookedProduct").Rows(0)("OddOut")
                odoToTextBox.Text = ds.Tables("BookedProduct").Rows(0)("OddIn")

                If ds.Tables("BookedProduct").Rows(0)("St") = "XX" Or _
                  ds.Tables("BookedProduct").Rows(0)("St") = "XN" Then

                    odoFromTextBox.ReadOnly = True
                    odoToTextBox.ReadOnly = True
                Else
                    odoFromTextBox.ReadOnly = False
                    odoToTextBox.ReadOnly = False
                End If

                'Textbox
                periodTextBox.Text = ds.Tables("BookedProduct").Rows(0)("UomId")
                currOrgTextBox.Text = ds.Tables("BookedProduct").Rows(0)("CurrOrg")

                checkPanel.Visible = True

                'show/hide cols
                If ds.Tables("BookedProduct").Rows(0)("CkiL") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("CkoL") = "1" Then
                    locationTh.Visible = True
                    frLocationTd.Visible = True
                    toLocationTd.Visible = True
                Else
                    locationTh.Visible = False
                    frLocationTd.Visible = False
                    toLocationTd.Visible = False
                End If

                If ds.Tables("BookedProduct").Rows(0)("CkiD") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("CkoD") = "1" Or _
                    ds.Tables("BookedProduct").Rows(0)("UomId") = "KM" Or _
                    ds.Tables("BookedProduct").Rows(0)("UomId") = "KMS" Then
                    travelTh.Visible = True
                    frTravelTd.Visible = True
                    toTravelTd.Visible = True
                Else
                    travelTh.Visible = False
                    frTravelTd.Visible = False
                    toTravelTd.Visible = False
                End If

                If ds.Tables("BookedProduct").Rows(0)("CkoD") = "1" Or _
                   ds.Tables("BookedProduct").Rows(0)("IsVehicle") = "1" Or _
                   ds.Tables("BookedProduct").Rows(0)("CkiD") = "1" Then
                    chargedTh.Visible = True
                    frChargedTd.Visible = True
                    toChargedTd.Visible = True
                Else
                    chargedTh.Visible = False
                    frChargedTd.Visible = False
                    toChargedTd.Visible = False
                End If

                If ds.Tables("BookedProduct").Rows(0)("OddoD") = "1" Then
                    odoTh.Visible = True
                    frOdoTd.Visible = True
                    toOdoTd.Visible = True
                Else
                    odoTh.Visible = False
                    frOdoTd.Visible = False
                    toOdoTd.Visible = False
                End If


            Else
                checkPanel.Visible = False
            End If


            'Charge to
            chargeToDropDownList.SelectedValue = ds.Tables("BookedProduct").Rows(0)("Cus")
            If ds.Tables("BookedProduct").Rows(0)("CusMod") = "1" And ds.Tables("BookedProduct").Rows(0)("NewModStatus") = "1" Then
                chargeToDropDownList.Enabled = True
            Else
                chargeToDropDownList.Enabled = False
            End If

            'BooRef
            bookingRefTextBox.Text = ds.Tables("BookedProduct").Rows(0)("BooRef")
            If ds.Tables("BookedProduct").Rows(0)("BkgRef") = "1" Then
                bookingRefTextBox.Visible = True
                bookingRefLabel.Visible = True
            Else
                bookingRefTextBox.Visible = False
                bookingRefLabel.Visible = False
            End If

            'Trans
            transferredTextBox.Text = ds.Tables("BookedProduct").Rows(0)("Trans")



            focCheckBox.Checked = ds.Tables("BookedProduct").Rows(0)("Foc")
            If ds.Tables("BookedProduct").Rows(0)("St") = "XX" Or _
                ds.Tables("BookedProduct").Rows(0)("St") = "XN" Then
                focCheckBox.Enabled = False
            Else
                focCheckBox.Enabled = True
            End If

            applyOrigRatesCheckBox.Checked = ds.Tables("BookedProduct").Rows(0)("AppOrgRates")
            cancelledTextBox.Text = ds.Tables("BookedProduct").Rows(0)("CancelWhen")



            'Charge table
            fromLabel.Text = GetDateFormat(ds.Tables("Detail").Rows(0)("FrDt") & " ") & ds.Tables("Detail").Rows(0)("FrTm")
            toLabel.Text = GetDateFormat(ds.Tables("Detail").Rows(0)("ToDt") & " ") & ds.Tables("Detail").Rows(0)("ToTm")
            bookedLabel.Text = GetDateFormat(ds.Tables("Detail").Rows(0)("BooDt") & " ")
            rateLabel.Text = ds.Tables("Detail").Rows(0)("Rate")
            qtyLabel.Text = ds.Tables("Detail").Rows(0)("QtUOM")
            currLabel.Text = ds.Tables("Detail").Rows(0)("Curr")
            discountsLabel.Text = ds.Tables("Detail").Rows(0)("Dis")
            agentDiscountLabel.Text = ds.Tables("Detail").Rows(0)("AgnDis")
            totalOwingLabel.Text = ds.Tables("Detail").Rows(0)("TotOw")
            tFocCheckBox.Checked = ds.Tables("Detail").Rows(0)("FOC")


            tGstLabel.Text = ds.Tables("Detail").Rows(0)("TotGst")
            tCurrLabel.Text = ds.Tables("Detail").Rows(0)("Curr")
            tDiscountsLabel.Text = ds.Tables("Detail").Rows(0)("TotDisc")
            tAgentDiscountLabel.Text = ds.Tables("Detail").Rows(0)("TotAgnDisc")
            tTotalOwingLabel.Text = ds.Tables("Detail").Rows(0)("TotOw1")


            ViewState(BookedProductMgt_SapId_ViewState) = ds.Tables("BookedProduct").Rows(0)("SapId")

            Dim tempXml As String
            tempXml = oXmlDoc.SelectSingleNode("//data/BookedProduct").OuterXml
            tempXml = tempXml.Replace("</BookedProduct>", "<RntIntNo>" & ds.Tables("Rental").Rows(0)("RntIntNo") & _
                "</RntIntNo><BooIntNo>" & ds.Tables("Rental").Rows(0)("BooIntNo") & "</BooIntNo></BookedProduct>")

            ViewState(BookedProductMgt_BookedProductXml_ViewState) = tempXml

            ''exec GEN_GetPopUpData @case = 'PRODUCT4BPD', @param1 = '', @param2 = 'NZ', @param3 = 'HA3450709', @param4 = '1', @param5 = '20/07/2008', @sUsrCode = 'ma2'
            productPickerControl.Param2 = ds.Tables("BookedProduct").Rows(0)("CtyCode") 'Me.CountryCode
            productPickerControl.Param3 = ds.Tables("BookedProduct").Rows(0)("BpdId")
            productPickerControl.Param4 = ds.Tables("BookedProduct").Rows(0)("IsVehicle")
            productPickerControl.Param5 = ds.Tables("BookedProduct").Rows(0)("BpdCkiDt")

            ''rev:mia 19jan2015-addition of supervisor override password
            OriginalProduct = productPickerControl.DataId
            OriginalStatus = currentDropDownList.SelectedValue
            OriginalPickupLocation = locationFromPickerControl.DataId
            OriginalPickupDate = travelFromDateControl.Date
        End If

    End Sub

    Private Sub setLocationPickerControl()

        'locationFromPickerControl.Param3 = productPickerControl.DataId
        'locationFromPickerControl.Param5 = locationToPickerControl.DataId

        'locationToPickerControl.Param3 = productPickerControl.DataId
        'locationToPickerControl.Param5 = locationFromPickerControl.DataId


        ''REV:MIA OCT.18
        locationFromPickerControl.Param2 = locationToPickerControl.Text.Split(" ")(0)
        locationFromPickerControl.Param3 = productPickerControl.Text.Split(" ")(0)

        locationToPickerControl.Param2 = locationFromPickerControl.Text.Split(" ")(0)
        locationToPickerControl.Param3 = productPickerControl.Text.Split(" ")(0)
    End Sub

    Private Sub SetGridViewStatusColor()
        'Add color for status
        For Each r As GridViewRow In rentalGridView.Rows
            r.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(r.Cells(7).Text)))
        Next

    End Sub

    Private Sub updateViewStateXmlDoc(ByRef oxmlDoc As XmlDocument)

        'Booked Product
        If Not productPickerControl.ReadOnly Then


            If oxmlDoc.SelectSingleNode("//BookedProduct/PrdName").InnerText = productPickerControl.Text Then
                oxmlDoc.SelectSingleNode("//BookedProduct/PrdId").InnerText = productPickerControl.DataId
            Else
                oxmlDoc.SelectSingleNode("//BookedProduct/PrdId").InnerText = GetProductID(productPickerControl.Text, Me.UserCode)
            End If
            oxmlDoc.SelectSingleNode("//BookedProduct/PrdName").InnerText = productPickerControl.Text
        End If

        'Current status
        If currentDropDownList.Enabled Then
            oxmlDoc.SelectSingleNode("//BookedProduct/St").InnerText = currentDropDownList.SelectedValue
        End If

        ''Booked Product Detail Table
        If checkPanel.Visible Then
            'location col
            'If Not locationFromPickerControl.ReadOnly Then
            oxmlDoc.SelectSingleNode("//BookedProduct/CkoLoc").InnerText = locationFromPickerControl.DataId.ToUpper
            'End If
            'If Not locationToPickerControl.ReadOnly Then
            oxmlDoc.SelectSingleNode("//BookedProduct/CkiLoc").InnerText = locationToPickerControl.DataId.ToUpper
            'End If


            'Travel from
            oxmlDoc.SelectSingleNode("//BookedProduct/BpdCkoDt").InnerText = travelFromDateControl.Date.ToShortDateString & " " & travelFromTimeControl.Text
            oxmlDoc.SelectSingleNode("//BookedProduct/BpdCkoTm").InnerText = travelFromTimeControl.Text
            If String.IsNullOrEmpty(frTextBox.Text) Then
                oxmlDoc.SelectSingleNode("//BookedProduct/Fr").InnerText = "0"
            Else
                oxmlDoc.SelectSingleNode("//BookedProduct/Fr").InnerText = frTextBox.Text
            End If

            'Travel to 
            oxmlDoc.SelectSingleNode("//BookedProduct/BpdCkiDt").InnerText = IIf(travelToDateControl.Date <> DateTime.MinValue, travelToDateControl.Date.ToShortDateString & " " & travelToTimeControl.Text, String.Empty)
            oxmlDoc.SelectSingleNode("//BookedProduct/BpdCkiTm").InnerText = travelToTimeControl.Text
            If String.IsNullOrEmpty(toTextBox.Text) Then
                oxmlDoc.SelectSingleNode("//BookedProduct/To").InnerText = "0"
            Else
                oxmlDoc.SelectSingleNode("//BookedProduct/To").InnerText = toTextBox.Text
            End If

            'Charged col
            oxmlDoc.SelectSingleNode("//BookedProduct/ChgFrDt").InnerText = chargedFromDateControl.Date.ToShortDateString & " " & chargedFromTimeControl.Text
            oxmlDoc.SelectSingleNode("//BookedProduct/ChgFrTm").InnerText = chargedFromTimeControl.Text

            oxmlDoc.SelectSingleNode("//BookedProduct/ChgToDt").InnerText = chargedToDateControl.Date.ToShortDateString & " " & chargedToTimeControl.Text
            oxmlDoc.SelectSingleNode("//BookedProduct/ChgToTm").InnerText = chargedToTimeControl.Text

            'odo
            If String.IsNullOrEmpty(odoFromTextBox.Text) Then
                oxmlDoc.SelectSingleNode("//BookedProduct/OddOut").InnerText = "0"
            Else
                oxmlDoc.SelectSingleNode("//BookedProduct/OddOut").InnerText = odoFromTextBox.Text
            End If

            If String.IsNullOrEmpty(odoToTextBox.Text) Then
                oxmlDoc.SelectSingleNode("//BookedProduct/OddIn").InnerText = "0"
            Else
                oxmlDoc.SelectSingleNode("//BookedProduct/OddIn").InnerText = odoToTextBox.Text
            End If

        End If

        'Charge to
        oxmlDoc.SelectSingleNode("//BookedProduct/Cus").InnerText = chargeToDropDownList.SelectedValue

        'BooRef
        'oxmlDoc.SelectSingleNode("//BookedProduct/BooRef").InnerText = bookingRefTextBox.Text

        If focCheckBox.Checked Then
            oxmlDoc.SelectSingleNode("//BookedProduct/Foc").InnerText = "1"
        Else
            oxmlDoc.SelectSingleNode("//BookedProduct/Foc").InnerText = "0"
        End If

        'oxmlDoc.SelectSingleNode("//BookedProduct/AppOrgRates").InnerText = applyOrigRatesCheckBox.Checked


    End Sub

    Protected Function GetArrivalDepartureTime(ByVal value As Object) As String
        If value Is Nothing _
            OrElse (TypeOf value Is String _
                    AndAlso DirectCast(value, String).Trim().Length = 0 _
                    ) Then

            Return ""
        End If

        Dim dtValue As DateTime = CDate(value)

        Return " [" + dtValue.ToString("HH:mm") + "]"
    End Function

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return ""
        End If
    End Function

#End Region

#Region "Added by Manny"



    Private Property BookedProductString() As String
        Get
            Return Server.HtmlDecode(CType(ViewState("BookedProductString"), String))
        End Get
        Set(ByVal value As String)
            ViewState("BookedProductString") = Server.HtmlEncode(value)
        End Set
    End Property

    Private Property OldIntNo() As String
        Get
            Return ViewState("OldIntNo")
        End Get
        Set(ByVal value As String)
            ViewState("OldIntNo") = value
        End Set
    End Property

    Private Property CKO() As String
        Get
            Return CType(ViewState("CKO"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CKO") = value
        End Set
    End Property

    Private Property CKI() As String
        Get
            Return CType(ViewState("CKI"), String)
        End Get
        Set(ByVal value As String)
            ViewState("CKI") = value
        End Set
    End Property

    Sub ConfirmationBoxExtendedInterface()
        With Me.ConfirmationBoxControlExtended1
            .Title = "Recalculate Booked Product"
            .Text = "This process recalculates Booked Product Values.<br/> Do you wish to also recalculate Charge Rate Bands <br/>"
            .NoteText = "<B>Note:</B><br/> 'YES' always cancels and replaces Booked Products <br/> 'NO' cancels and replaces only if the calculated values differ to the current values <br/> 'CANCEL' exits this process."
        End With

        With Me.ConfirmationBoxControl1
            .RightButton_Visible = False
            .Text = ""
        End With
    End Sub

    Protected Sub RecalculateChargesBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RecalculateChargesBtn.Click
        Me.ConfirmationBoxControlExtended1.Show()
    End Sub

    ''' <summary>
    ''' make the variable force to be a viewstate property
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Property Force As String
        Get
            Return ViewState("Force")
        End Get
        Set(ByVal value As String)
            ViewState("Force") = value
        End Set
    End Property
    Protected Sub ConfirmationBoxControlExtended_PostBack(ByVal sender As Object, _
                                                 ByVal leftButton As Boolean, _
                                                 ByVal rightButton As Boolean, _
                                                 ByVal centerButton As Boolean, _
                                                 ByVal param As String) Handles ConfirmationBoxControlExtended1.PostBack

        ''     If rightButton = True Then Exit Sub

        force = IIf(leftButton = True, "Y", "N")
        Dim message As String = ""

        If force = "Y" Then
            Me.ConfirmationBoxControl1.Text = "Recalculating Booked Products and Charge Rate Bands.<BR/>You will be notified when this process is completed."
        Else
            Me.ConfirmationBoxControl1.Text = "Recalculating Booked Products.<BR/>You will be notified when this process is completed."
        End If

        Me.ConfirmationBoxControl1.Title = "Recalculate Booked Product"
        Me.ConfirmationBoxControl1.Show()


    End Sub

    Private Sub GoBackToBookingSummary()
        If Not Request.QueryString("hdBookingId") Is Nothing Then
            bookingId = Request.QueryString("hdBookingId").ToString()
        End If

        If Not Request.QueryString("hdRentalId") Is Nothing Then
            rentalId = Request.QueryString("hdRentalId").ToString()
        End If

        If Not Request.QueryString("hdBookingNum") Is Nothing Then
            bookingNum = Request.QueryString("hdBookingNum").ToString()
        End If

        Dim tempQry As String = "Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&hdBookingNum=" & bookingNum
        Response.Redirect(tempQry)
    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, _
                                                 ByVal leftButton As Boolean, _
                                                 ByVal rightButton As Boolean, _
                                                 ByVal param As String) Handles ConfirmationBoxControl1.PostBack

        If leftButton Then
            Dim xmlDocBookedProduct As New XmlDocument
            xmlDocBookedProduct.LoadXml(BookedProductString)

            Dim nRntIntNumber As String = ""
            Dim sRentalId As String = ""
            Dim bIsValidIntNo As Boolean
            Dim nBooIntNumber As Integer

            If Not xmlDocBookedProduct.SelectSingleNode("data/BookedProduct/IntNo") Is Nothing Then
                Me.OldIntNo = xmlDocBookedProduct.SelectSingleNode("data/BookedProduct/IntNo").InnerText
            End If

            If Not xmlDocBookedProduct.SelectSingleNode("data/Rental/RntIntNo") Is Nothing Then
                nRntIntNumber = xmlDocBookedProduct.SelectSingleNode("data/Rental/RntIntNo").InnerText
            End If

            If Not xmlDocBookedProduct.SelectSingleNode("data/Rental/BooIntNo") Is Nothing Then
                nBooIntNumber = xmlDocBookedProduct.SelectSingleNode("data/Rental/BooIntNo").InnerText
            End If

            If Not xmlDocBookedProduct.SelectSingleNode("data/Rental/RntId") Is Nothing Then
                sRentalId = xmlDocBookedProduct.SelectSingleNode("data/Rental/RntId").InnerText
            End If

            bIsValidIntNo = Aurora.Reservations.Services.BookedProduct.CheckRentalIntegrity(sRentalId, "", "", nRntIntNumber, nBooIntNumber)

            Dim tempReturnCharges As String = ""

            If bIsValidIntNo = True Then
                tempReturnCharges = Aurora.Reservations.Services.BookedProduct.ManageRecalculateCharges(Me.BpdId, 1, Force, UserCode, "ManageRecalculateCharges()")
            Else
                Me.SetErrorShortMessage("This Rental has been modified by another user. Please refresh the screen.")
            End If
            xmlDocBookedProduct = Nothing

            Dim xmlDocreturnCharges As New XmlDocument
            xmlDocreturnCharges.LoadXml(tempReturnCharges)

            Dim ErrStatus As String = xmlDocreturnCharges.SelectSingleNode("Root/Error/ErrStatus").InnerText
            Dim Errmsg As String = xmlDocreturnCharges.SelectSingleNode("Root/Error/ErrDescription").InnerText

            Dim getMessage As String = "Recalculate Process Complete"
            If ErrStatus = "False" Then
                If Not xmlDocreturnCharges.DocumentElement.SelectSingleNode("Root/Msg") Is Nothing Then
                    If Not xmlDocreturnCharges.DocumentElement.SelectSingleNode("Root/Msg").Attributes(0) Is Nothing Then
                        getMessage = xmlDocreturnCharges.DocumentElement.SelectSingleNode("Root/Msg").Attributes(0).Value
                    End If

                End If

                If Not String.IsNullOrEmpty(Errmsg) Then
                    If Errmsg <> "Success" Then
                        Me.SetInformationShortMessage(Errmsg)
                    End If
                End If
                formDataBind()
                Me.SetInformationShortMessage(getMessage)
            Else

                Me.SetErrorShortMessage(Errmsg)
                If Errmsg.Contains("GEN007") = True Then
                    GoBackToBookingSummary()
                End If
                Me.saveButton.Enabled = False
            End If

        End If

    End Sub

    Protected Sub rentalGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles rentalGridView.RowCreated
        If e.Row.RowType = DataControlRowType.Header Then
            Dim i As Integer
            Dim col As DataControlField
            Dim lit As Literal

            ''  Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            For i = 0 To rentalGridView.Columns.Count - 1
                lit = New Literal

                col = rentalGridView.Columns(i)

                If col.HeaderText.Contains("Check-Out") Then
                    lit.Text = "Check-Out (" & CKO & ")"
                ElseIf col.HeaderText.Contains("Check-In") Then
                    lit.Text = "Check-In (" & CKI & ")"
                End If

                e.Row.Cells(i).Controls.Add(lit)
            Next
        End If
    End Sub



#End Region

#Region "Added By Manny"

    ''r�v:mia 619 Maintain Booked Product page - cannot change a vehicle in the 'Product*' field.
    Private Function GetProductID(ByVal productcode As String, ByVal paramuserid As String, Optional ByVal productGUID As String = "") As String

        Dim splitProductCode As String()
        If productcode.Contains("-") Then
            splitProductCode = productcode.Split("-")
            productcode = splitProductCode(0)
        Else
            Return ""
        End If
        Dim tempReturn As String = Aurora.Common.Data.ExecuteScalarSP("RES_getProduct", _
                                                  productcode, _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  "", _
                                                  paramuserid)

        Dim xmltemp As New XmlDocument
        Try
            If String.IsNullOrEmpty(tempReturn) Then Return ""
            xmltemp.LoadXml(tempReturn)
            Return xmltemp.SelectSingleNode("PRODUCT/Id").InnerText

        Catch ex As Exception
            xmltemp.LoadXml("<Products>" & tempReturn & "</Products>")
            Dim nodes As XmlNodeList = xmltemp.SelectNodes("Products/PRODUCT/Id")
            For Each mynode As XmlNode In nodes
                If mynode.Name = "Id" Then
                    If Not String.IsNullOrEmpty(mynode.InnerText) Then
                        If mynode.InnerText = productGUID Then
                            Return mynode.InnerText
                        End If
                    End If
                End If
            Next
        End Try
    End Function


    Protected Sub SupervisorOverrideControl1_PostBack(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBack
        If isOkButton Then
            Dim sReturnMsg As String = ""

            If String.IsNullOrEmpty(sOverrideCode) Then
                sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, "ORIDCHGVEH") '"OLMAN")
            Else
                sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, sOverrideCode) '"OLMAN")
            End If

            If (sReturnMsg) = "True" Then
                Dim oXmlDoc As New XmlDocument
                oXmlDoc.LoadXml(ViewState(BookedProductMgt_BookedProductXml_ViewState))

                updateViewStateXmlDoc(oXmlDoc)

                Dim messages As String = ""
                Dim errors As String = ""
                Dim strRetBpdId As String = ""

                Dim sbLogText As New StringBuilder

                If sOverrideCode = "OLMAN" Then

                    ' Logging.LogInformation("DVASSCALL", sbLogString.ToString())
                    sbLogText.Append("-- Data before call --")
                    sbLogText.Append("BookingProducts.ModifyBookedProduct(" & vbNewLine _
                                                                            & oXmlDoc.OuterXml & "," & vbNewLine _
                                                                            & 0 & "," & vbNewLine _
                                                                            & "" & "," & vbNewLine _
                                                                            & True & "," & vbNewLine _
                                                                            & SupervisorOverrideControl1.Login & "," & vbNewLine _
                                                                            & ReturnedSAPId & "," & vbNewLine _
                                                                            & messages & "," & vbNewLine _
                                                                            & errors & "," & vbNewLine _
                                                                            & "BookedProductMgt.aspx" & "," & vbNewLine _
                                                                            & strRetBpdId & vbNewLine _
                                                                            & ",False,True" & vbNewLine _
                                                                            & ")" & vbNewLine)



                    BookingProducts.ModifyBookedProduct(oXmlDoc, 0, "", True, SupervisorOverrideControl1.Login, ReturnedSAPId, messages, errors, _
                    "BookedProductMgt.aspx", strRetBpdId, False, True, "", _
                                                SlotId, _
                                                SlotDescription, _
                                                rentalId)

                    sbLogText.Append("-- Data after call --" & vbNewLine)
                    sbLogText.Append("BookingProducts.ModifyBookedProduct(" & vbNewLine _
                                                                            & oXmlDoc.OuterXml & "," & vbNewLine _
                                                                            & 0 & "," & vbNewLine _
                                                                            & "" & "," & vbNewLine _
                                                                            & True & "," & vbNewLine _
                                                                            & SupervisorOverrideControl1.Login & "," & vbNewLine _
                                                                            & ReturnedSAPId & "," & vbNewLine _
                                                                            & messages & "," & vbNewLine _
                                                                            & errors & "," & vbNewLine _
                                                                            & "BookedProductMgt.aspx" & "," & vbNewLine _
                                                                            & strRetBpdId & vbNewLine _
                                                                            & ",False,True" & vbNewLine _
                                                                            & ")" & vbNewLine)


                Else
                    sbLogText.Append("-- Data before call --" & vbNewLine)
                    sbLogText.Append("BookingProducts.ModifyBookedProduct(" & vbNewLine _
                                                                            & oXmlDoc.OuterXml & "," & vbNewLine _
                                                                            & 0 & "," & vbNewLine _
                                                                            & "" & "," & vbNewLine _
                                                                            & False & "," & vbNewLine _
                                                                            & SupervisorOverrideControl1.Login & "," & vbNewLine _
                                                                            & ReturnedSAPId & "," & vbNewLine _
                                                                            & messages & "," & vbNewLine _
                                                                            & errors & "," & vbNewLine _
                                                                            & "BookedProductMgt.aspx" & "," & vbNewLine _
                                                                            & strRetBpdId & vbNewLine _
                                                                            & ",True,False" & vbNewLine _
                                                                            & ")" & vbNewLine)

                    BookingProducts.ModifyBookedProduct(oXmlDoc, 0, "", False, SupervisorOverrideControl1.Login, ReturnedSAPId, messages, errors, _
                    "BookedProductMgt.aspx", strRetBpdId, True, False, "", _
                                                SlotId, _
                                                SlotDescription, _
                                                rentalId)

                    sbLogText.Append("-- Data after call --" & vbNewLine)
                    sbLogText.Append("BookingProducts.ModifyBookedProduct(" & vbNewLine _
                                                                            & oXmlDoc.OuterXml & "," & vbNewLine _
                                                                            & 0 & "," & vbNewLine _
                                                                            & "" & "," & vbNewLine _
                                                                            & False & "," & vbNewLine _
                                                                            & SupervisorOverrideControl1.Login & "," & vbNewLine _
                                                                            & ReturnedSAPId & "," & vbNewLine _
                                                                            & messages & "," & vbNewLine _
                                                                            & errors & "," & vbNewLine _
                                                                            & "BookedProductMgt.aspx" & "," & vbNewLine _
                                                                            & strRetBpdId & vbNewLine _
                                                                            & ",True,False" & vbNewLine _
                                                                            & ")" & vbNewLine)

                End If

                Logging.LogInformation("Override_Info", sbLogText.ToString())

                If Not String.IsNullOrEmpty(errors) Then
                    If errors.Contains("SOFT-FAIL") OrElse errors.Contains("AURORA") OrElse errors.Contains("BLK-MSG") _
                    OrElse (errors.ToUpper().Contains("DVASS") And errors.ToUpper().Contains("NO VEHICLES AVAILABLE")) Then

                        sOverrideCode = "OLMAN"
                        Me.SupervisorOverrideControl1.Show(Text:=errors.Replace("BLK-MSG - GEN172 - ", ""), Role:=sOverrideCode, ErrorMessage:=".")
                    ElseIf errors.Contains("OVABCDEF") Then
                        sOverrideCode = "ORIDCHGVEH"
                        SupervisorOverrideControl1.Show(ErrorMessage:=errors.Split("*"c)(1), Role:="ORIDCHGVEH", Text:=".")
                        Exit Sub
                    Else
                        ReturnedSAPId = String.Empty
                        SetErrorMessage(errors)
                        Exit Sub
                    End If
                Else
                    ReturnedSAPId = String.Empty
                    BpdId = strRetBpdId
                    formDataBind()
                    SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
                End If

            Else
                saveButton.Enabled = True
                Me.SetErrorShortMessage(sReturnMsg)
            End If
        Else
            saveButton.Enabled = False
        End If
    End Sub

    ''rev:mia Oct.6
    Private Sub SetfocusOnPostBack(ByVal controlID As String)
        System.Diagnostics.Debug.WriteLine(controlID)

        Select Case controlID
            Case "ctl00_ContentPlaceHolder_travelFromDateControl_dateTextBox"
                sc.SetFocus(travelFromTimeControl)
            Case "ctl00_ContentPlaceHolder_travelFromTimeControl_timeTextBox"
                sc.SetFocus(chargedFromDateControl)
            Case "ctl00_ContentPlaceHolder_travelToDateControl_dateTextBox"
                sc.SetFocus(travelToTimeControl)
            Case "ctl00_ContentPlaceHolder_travelToTimeControl_timeTextBox"
                sc.SetFocus(chargedFromTimeControl)
        End Select

    End Sub

    'Protected Sub locationFromPickerControl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationFromPickerControl.TextChanged
    '    If locationFromPickerControl.Text.Trim.Length <= 3 Then
    '        Me.SetWarningShortMessage(INVALID_LOCATION)
    '        sc.SetFocus(locationFromPickerControl)
    '    End If
    'End Sub

    'Protected Sub locationToPickerControl_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles locationToPickerControl.TextChanged
    '    If locationToPickerControl.Text.Trim.Length <= 3 Then
    '        Me.SetWarningShortMessage(INVALID_LOCATION)
    '        sc.SetFocus(locationToPickerControl)
    '    End If
    'End Sub

#End Region

#Region "GST CHANGES"

    Sub InitialiseGSTStuff()
        'With Me.ConfirmationBoxControlExtendedGST
        '    .Title = "Recalculate Booked Product"
        '    .Text = "This process recalculates Booked Product Values.<br/> Do you wish to also recalculate Charge Rate Bands <br/>"
        '    .NoteText = "<B>Note:</B><br/> 'YES' always cancels and replaces Booked Products <br/> 'NO' cancels and replaces only if the calculated values differ to the current values <br/> 'CANCEL' exits this process."
        'End With

        'With Me.ConfirmationBoxControl1
        '    .RightButton_Visible = False
        '    .Text = ""
        'End With
        Dim xmlDocBookedProduct As New XmlDocument
        xmlDocBookedProduct.LoadXml(BookedProductString)
        Try
            RecalculateGSTBtn.Visible = (xmlDocBookedProduct.DocumentElement.SelectSingleNode("BookedProduct/GstFlag").InnerText = "1")
        Catch ex As Exception
            RecalculateChargesBtn.Visible = False
        End Try
    End Sub

    Protected Sub RecalculateGSTBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles RecalculateGSTBtn.Click
        ConfirmationBoxControlExtendedGST.Show()
    End Sub

    Protected Sub ConfirmationBoxControlExtendedGST_PostBack(ByVal sender As Object, _
                                                ByVal leftButton As Boolean, _
                                                ByVal rightButton As Boolean, _
                                                ByVal centerButton As Boolean, _
                                                ByVal param As String) Handles ConfirmationBoxControlExtendedGST.PostBack

        Dim xmlDocBookedProduct As New XmlDocument
        xmlDocBookedProduct.LoadXml(BookedProductString)

        Dim nRntIntNumber As String = ""
        Dim sRentalId As String = ""
        Dim bIsValidIntNo As Boolean
        Dim nBooIntNumber As Integer
        Dim tempReturnCharges As String = ""

        If Not xmlDocBookedProduct.SelectSingleNode("data/BookedProduct/IntNo") Is Nothing Then
            Me.OldIntNo = xmlDocBookedProduct.SelectSingleNode("data/BookedProduct/IntNo").InnerText
        End If

        If Not xmlDocBookedProduct.SelectSingleNode("data/Rental/RntIntNo") Is Nothing Then
            nRntIntNumber = xmlDocBookedProduct.SelectSingleNode("data/Rental/RntIntNo").InnerText
        End If

        If Not xmlDocBookedProduct.SelectSingleNode("data/Rental/BooIntNo") Is Nothing Then
            nBooIntNumber = xmlDocBookedProduct.SelectSingleNode("data/Rental/BooIntNo").InnerText
        End If

        If Not xmlDocBookedProduct.SelectSingleNode("data/Rental/RntId") Is Nothing Then
            sRentalId = xmlDocBookedProduct.SelectSingleNode("data/Rental/RntId").InnerText
        End If

        bIsValidIntNo = Aurora.Reservations.Services.BookedProduct.CheckRentalIntegrity(sRentalId, "", "", nRntIntNumber, nBooIntNumber)

        If bIsValidIntNo = True Then
            If leftButton Then
                ' 15%
                tempReturnCharges = Aurora.Reservations.Services.BookedProduct.ManageRecalculateCharges(Me.BpdId, OldIntNo, "N", UserCode, "ManageRecalculateCharges()")
            ElseIf rightButton Then
                ' 12.5%
                tempReturnCharges = Aurora.Reservations.Services.BookedProduct.ManageRecalculateCharges(Me.BpdId, OldIntNo, "O", UserCode, "ManageRecalculateCharges()")
            ElseIf centerButton Then
                ' cancel
                Exit Sub
            End If
        Else
            Me.SetErrorShortMessage("This Rental has been modified by another user. Please refresh the screen.")
        End If

        xmlDocBookedProduct = Nothing

        Dim xmlDocreturnCharges As New XmlDocument
        xmlDocreturnCharges.LoadXml(tempReturnCharges)

        Dim ErrStatus As String '= xmlDocreturnCharges.SelectSingleNode("Root/Error/Error/ErrStatus").InnerText
        Try
            ErrStatus = xmlDocreturnCharges.SelectSingleNode("Root/Error/Error/ErrStatus").InnerText
        Catch ex As Exception
            ' couldnt find that node, try another
            ErrStatus = xmlDocreturnCharges.SelectSingleNode("Root/Error/ErrStatus").InnerText
        End Try

        Dim Errmsg As String '= xmlDocreturnCharges.SelectSingleNode("Root/Error/Error/ErrDescription").InnerText
        Try
            Errmsg = xmlDocreturnCharges.SelectSingleNode("Root/Error/Error/ErrDescription").InnerText
        Catch ex As Exception
            ' couldnt find that node, try another
            Errmsg = xmlDocreturnCharges.SelectSingleNode("Root/Error/ErrDescription").InnerText
        End Try

        Dim getMessage As String = "Recalculate Process Complete"
        If ErrStatus = "False" Then
            If Not xmlDocreturnCharges.DocumentElement.SelectSingleNode("Root/Msg") Is Nothing Then
                If Not xmlDocreturnCharges.DocumentElement.SelectSingleNode("Root/Msg").Attributes(0) Is Nothing Then
                    getMessage = xmlDocreturnCharges.DocumentElement.SelectSingleNode("Root/Msg").Attributes(0).Value
                End If

            End If

            If Not String.IsNullOrEmpty(Errmsg) Then
                If Errmsg <> "Success" Then
                    Me.SetInformationShortMessage(Errmsg)
                End If
            End If
            formDataBind()
            Me.SetInformationShortMessage(getMessage)
        Else

            Me.SetErrorShortMessage(Errmsg)
            If Errmsg.Contains("GEN007") = True Then
                GoBackToBookingSummary()
            End If
            'Me.saveButton.Enabled = False
        End If




    End Sub


#End Region

#Region "rev:mia April 14 2011 - added checking for CI"
    Private Function IsStatusCheckin(ByVal rentalid As String) As Boolean
        Dim returnvalue As String = ""
        Try
            returnvalue = Aurora.Common.Data.ExecuteScalarSP("Payment_RentalStatusIsCI", rentalid)
        Catch ex As Exception
            LogError("IsStatusCheckin: " & ex.Message)
        End Try
        Return IIf(returnvalue.ToUpper <> "CI", False, True)

    End Function
    ''found a hole in Aurora where booking status can still be modified even its already CI
    Sub DisabledStatusButtonAndDropDown()

        If statusTextBox.Text.Equals("CI") = True Then
            ''currentDropDownList.Enabled = IIf(IsStatusCheckin(rentalId) = True, False, True)
        End If
    End Sub
#End Region

#Region "rev:mia 19jan2015-addition of supervisor override password"

    Private Property OriginalProduct As String
        Get
            Return CStr(ViewState("OriginalProduct"))
        End Get
        Set(value As String)
            ViewState("OriginalProduct") = value
        End Set
    End Property

    Private Property OriginalStatus As String
        Get
            Return CStr(ViewState("OriginalStatus"))
        End Get
        Set(value As String)
            ViewState("OriginalStatus") = value
        End Set
    End Property

    Private Property OriginalPickupLocation As String
        Get
            Return CStr(ViewState("OriginalPickupLocation"))
        End Get
        Set(value As String)
            ViewState("OriginalPickupLocation") = value
        End Set
    End Property

    Private Property OriginalPickupDate As String
        Get
            Return CStr(ViewState("OriginalPickupDate"))
        End Get
        Set(value As String)
            ViewState("OriginalPickupDate") = value
        End Set
    End Property

    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property

    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property

    Sub PreSave()

        Dim newProductId As String = productPickerControl.DataId
        Dim newStatus As String = currentDropDownList.SelectedItem.Value
        Dim newPickupLocation As String = locationFromPickerControl.DataId
        Dim newPickupDate As String = travelFromDateControl.Text

        ''   If ((Not OriginalStatus.Equals(newStatus) And (newStatus = "KK" Or newStatus = "NN")) Or (newStatus = "KK" And OriginalStatus = "KK")) Then
        Dim newCheckOutDate As DateTime = Aurora.Common.Utility.ParseDateTime(newPickupDate, Aurora.Common.Utility.SystemCulture)
        Dim oldCheckOutDate As DateTime = Aurora.Common.Utility.ParseDateTime(OriginalPickupDate, Aurora.Common.Utility.SystemCulture)

        'If (Not OriginalProduct.Equals(newProductId) Or _
        '    Not OriginalPickupLocation.Equals(newPickupLocation) Or _
        '    Not oldCheckOutDate.Equals(newCheckOutDate)) Then
        If (SlotAvailableControl.IsSlotRequired(Nothing, _
                                                rentalId, _
                                                Nothing, _
                                                IIf(oldCheckOutDate.Equals(newCheckOutDate), Nothing, newPickupDate), _
                                                IIf(OriginalPickupLocation.Equals(newPickupLocation), Nothing, newPickupLocation), _
                                                Nothing, _
                                                IIf(OriginalProduct.Equals(newProductId), Nothing, newProductId), _
                                                Nothing, _
                                                newStatus)) Then

            SlotAvailableControl.NoOfBulkBookings = 1 ''set to one
            SlotAvailableControl.SupervisorOverride = False
            SlotAvailableControl.Show(Nothing, _
                                      rentalId, _
                                      Nothing, _
                                      IIf(oldCheckOutDate.Equals(newCheckOutDate), Nothing, newPickupDate), _
                                      IIf(OriginalPickupLocation.Equals(newPickupLocation), Nothing, newPickupLocation), _
                                      Nothing, _
                                      IIf(OriginalProduct.Equals(newProductId), Nothing, newProductId), _
                                      Nothing, _
                                      Status:=newStatus)
        Else
            Save()
        End If
        'Else
        'Save()
        'End If

    End Sub

    Sub Save()
        ReturnedSAPId = String.Empty

        ' 16.6.9    -   Shoel   -   Fix for Call#25450 - Location pop up hiding behind booking screen
        ' 18.3.10   -   Shoel   -   Donot check location if the picker is hidden
        If String.IsNullOrEmpty(locationFromPickerControl.Text.Trim()) AndAlso locationFromPickerControl.Visible Then
            SetShortMessage(AuroraHeaderMessageType.Warning, INVALID_LOCATION)
            sc.SetFocus(locationFromPickerControl)
            Return
        End If

        If String.IsNullOrEmpty(locationToPickerControl.Text.Trim()) AndAlso locationToPickerControl.Visible Then
            SetShortMessage(AuroraHeaderMessageType.Warning, INVALID_LOCATION)
            sc.SetFocus(locationToPickerControl)
            Return
        End If
        ' 18.3.10   -   Shoel   -   Donot check location if the picker is hidden
        ' 16.6.9    -   Shoel   -   Fix for Call#25450 - Location pop up hiding behind booking screen

        ''rev:mia oct.23
        If Not (String.IsNullOrEmpty(locationFromPickerControl.Text.Trim()) And String.IsNullOrEmpty(locationFromPickerControl.DataId.Trim())) Then
            If locationFromPickerControl.Text.Length <= 3 And locationFromPickerControl.Text.Contains("-") = False Then
                SetShortMessage(AuroraHeaderMessageType.Warning, INVALID_LOCATION)
                sc.SetFocus(locationFromPickerControl)
                Return
            End If
        End If

        If Not (String.IsNullOrEmpty(locationToPickerControl.Text.Trim()) And String.IsNullOrEmpty(locationToPickerControl.DataId.Trim())) Then
            If locationToPickerControl.Text.Length <= 3 And locationToPickerControl.Text.Contains("-") = False Then
                SetShortMessage(AuroraHeaderMessageType.Warning, INVALID_LOCATION)
                sc.SetFocus(locationToPickerControl)
                Return
            End If
        End If

        If String.IsNullOrEmpty(productPickerControl.Text) Or String.IsNullOrEmpty(ViewState(BookedProductMgt_SapId_ViewState)) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, GetMessage("GEN005"))
            Return
        End If

        Dim oXmlDoc As New XmlDocument
        oXmlDoc.LoadXml(ViewState(BookedProductMgt_BookedProductXml_ViewState))

        If Not productPickerControl.ReadOnly Then
            'If oXmlDoc.SelectSingleNode("//BookedProduct/PrdName").InnerText = productPickerControl.Text Then

            updateViewStateXmlDoc(oXmlDoc)


            Dim messages As String = ""
            Dim errors As String = ""
            Dim strRetBpdId As String = ""
            BookingProducts.ModifyBookedProduct(oXmlDoc, _
                                                1, _
                                                "", _
                                                0, _
                                                UserCode, _
                                                ReturnedSAPId, _
                                                messages, _
                                                errors, _
                                                "BookedProductMgt.aspx", _
                                                strRetBpdId, _
                                                True, _
                                                False, _
                                                 "", _
                                                SlotId, _
                                                SlotDescription, _
                                                rentalId)

            If Not String.IsNullOrEmpty(errors) Then
                ' Shoel : 26.3.9 - Redoing this bit, to properly replicate the old system...

                ' Overrideable stuff
                If errors.Contains("OVABCDEF") Then
                    sOverrideCode = "ORIDCHGVEH"
                    SupervisorOverrideControl1.Show(ErrorMessage:=errors.Split("*"c)(1), Role:="ORIDCHGVEH", Text:=".")
                    Exit Sub
                End If

                'rev:mia Aug 16 2013 - modified based on new DVASS 2 message
                '      - today in history, today elvis presley died. Aug 16 1977
                '>>>orig code: If errors.Contains("SOFT-FAIL") OrElse errors.Contains("AURORA") OrElse errors.Contains("BLK-MSG") OrElse (errors.ToUpper().Contains("DVASS") And errors.ToUpper().Contains("NO VEHICLES AVAILABLE") _
                Dim dvassmessage As String = ConfigurationManager.AppSettings("dvassmessageForNovehicles").ToUpper
                If errors.Contains("SOFT-FAIL") Or errors.Contains("AURORA") Or errors.Contains("BLK-MSG") _
                Or (errors.ToUpper().Contains("DVASS") And errors.ToUpper().Contains("NO VEHICLES AVAILABLE") Or _
                    errors.ToUpper().Contains(dvassmessage)) Then

                    ' heres an interesting bit that was completely left out in the previous .net implementation

                    ' 1. Get Rolecode reqd to override...
                    Dim oUviData As XmlDocument
                    oUviData = Aurora.Booking.Services.BookingCheckInCheckOut.GetUVIData(UserCode)
                    If oUviData.DocumentElement.ChildNodes.Count > 0 And oUviData.DocumentElement.ChildNodes(0).BaseURI = "Error" Then
                        SetErrorMessage("UviValue NZ/AU ORID_AIMS Not Set")
                        Exit Sub
                    End If
                    ' 2. Show SUPOVR POP
                    sOverrideCode = oUviData.DocumentElement.InnerText.Trim()
                    SupervisorOverrideControl1.Show(ErrorMessage:="(" & oUviData.DocumentElement.InnerText.Trim() & ")<BR/>" & errors, Role:=sOverrideCode, Text:=".")
                    Exit Sub
                End If

                ' non-overrideable stuff
                ReturnedSAPId = String.Empty
                SetErrorMessage(errors)

            Else
                ReturnedSAPId = String.Empty
                BpdId = strRetBpdId
                formDataBind()
                SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
            End If

        End If
    End Sub

    Protected Sub SlotAvailableControl_PostBack(sender As Object, isSaveButton As Boolean, param As String) Handles SlotAvailableControl.PostBack
        If (isSaveButton) Then
            SlotId = param.Split("|")(1)
            SlotDescription = param.Split("|")(2)
            Save()
            Logging.LogDebug("BookingProductMGT SlotAvailableControl_PostBack", "SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End If
    End Sub
#End Region

End Class
