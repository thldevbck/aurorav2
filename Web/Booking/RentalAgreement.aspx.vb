Imports System.Xml
Imports System.IO
Imports Aurora
Imports WS.RAPrinter
Imports System.Web.Services

<AuroraPageTitleAttribute("Rental Agreement")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class Booking_RentalAgreement
    Inherits AuroraPage

    Private Shared sRentalId As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim sRentalDetails As String = CStr(Request.QueryString("RntData"))
        sRentalId = sRentalDetails.Split("$")(0)
        Dim sBookingId As String = sRentalDetails.Split("$")(1)
        Dim sUserCode As String = sRentalDetails.Split("$")(2)
        'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
        Dim bPrintFromAmzaon As Boolean = False
        If sRentalDetails.Split("$").Length = 4 AndAlso sRentalDetails.Split("$")(3) IsNot Nothing Then
            bPrintFromAmzaon = True
        End If


        If (Not IsPostBack) Then
            If bPrintFromAmzaon Then
                RentalAgreementUrlFromAmazon(sRentalDetails, sRentalId, sBookingId, sUserCode)
            Else

                RentalAgreementFromLocal(sRentalDetails, sRentalId, sBookingId, sUserCode)
            End If
        End If

        'End nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
    End Sub

#Region "rev:mia 21Aug2015 - Add saved Rental agreement link in Aurora"

    Sub RePrint(sRentalDetails As String, sRentalId As String, sBookingId As String, sUserCode As String)

        ' render the Xml to screen

        Dim xmlTemp As XmlDocument = Aurora.Booking.Services.BookingCheckInCheckOut.GetPrintAgreement(sRentalId)

        Dim xmlDoc As New XmlDocument 'hack -cos our system sends data and the xsl expects Data
        xmlDoc.LoadXml("<Data>" & xmlTemp.DocumentElement.InnerXml & "</Data>")

        Dim unwantedPartsRegex As New Regex("(?:\<\s*tr[^\>]*\>[^\<]*\<\s*td[^\>]*\>[^\<]*\<\s*input[^\>]*\>((tr\s?\s?[^\>])|[^t]|(t[^r]))+tr\s?\s?\>)", RegexOptions.IgnoreCase Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Singleline)

        Dim unwantedPartsRegex2 As New Regex("(?:\<\s*tr[^\>]*\>[^\<]*\<\s*td[^\>]*\>\s*[^\<\>\s]+\s+Signature\s*\.?\.?\.+((tr\s?\s?[^\>])|[^t]|(t[^r]))+tr\s?\s?\>)", RegexOptions.IgnoreCase Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Singleline)

        Dim unwantedPartsRegex3 As New Regex("(?<=\>[^\>]*)\s*\.+\s*\(initial\)(?=[^\<]*\<)", RegexOptions.IgnoreCase Or RegexOptions.IgnorePatternWhitespace Or RegexOptions.Singleline)
        Dim mSourceString As String = xmlTemp.DocumentElement.InnerXml


        mSourceString = unwantedPartsRegex.Replace(mSourceString, "")
        mSourceString = unwantedPartsRegex2.Replace(mSourceString, "")
        mSourceString = unwantedPartsRegex3.Replace(mSourceString, "")


        Me.Xml1.DocumentContent = "<Data>" & mSourceString & "</Data>"

        If Xml1.Document.SelectSingleNode("//Data/AgreementHeader/IsPremium").InnerText = "0" Then
            Me.Xml1.TransformSource = Server.MapPath("~/Booking/xsl/RentalAgreement_NonPrem.xsl")
        Else
            Me.Xml1.TransformSource = Server.MapPath("~/Booking/xsl/RentalAgreement.xsl")
        End If

        Aurora.Booking.Services.BookingCheckInCheckOut.MaintainContractHistory(sBookingId, sRentalId, sUserCode, "BookingCheckinCheckoutUserControl.ascx")

    End Sub

    Private Sub RentalAgreementUrlFromAmazon(sRentalDetails As String, sRentalId As String, sBookingId As String, sUserCode As String)
        Dim wsPhoenix As New AuroraWebService
        Dim result As String = wsPhoenix.RentalAgreementUrlFromAmazon(sRentalId, UserCode, "RentalAgreement.aspx")


        If (result.IndexOf("ERROR") <> -1) Then
            'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
            'RePrint(sRentalDetails, sRentalId, sBookingId, sUserCode)
            'Xml1.Visible = True
            'pdf.Visible = False
            Me.Xml1.DocumentContent = "<Data><AgreementHeader><Licensee>Tourism Holdings Limited</Licensee><Company>COMPNAME_THL_NZ</Company><ContractNum>" + sBookingId + "</ContractNum></AgreementHeader></Data>"


            Me.Xml1.TransformSource = Server.MapPath("~/Booking/xsl/NoRentalAgreement.xsl")
            Xml1.Visible = True
            pdf.Visible = False
            'End nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was

        Else
            Xml1.Visible = False
            pdf.Visible = True
            iframePdf.Attributes.Add("src", "RAReprint/" & sRentalId & ".pdf")

            ''Response.ContentType = "application/pdf"
            ''Response.WriteFile(result)
        End If
    End Sub

    Protected Sub btnClosePage_Click(sender As Object, e As System.EventArgs) Handles btnClosePage.Click
        RAPrinterSelection.DeleteMappedRentalPath(sRentalId)
        Dim close As String = "<script type='text/javascript'>" & vbCr & vbLf & " window.returnValue = true;" & vbCr & vbLf & "                                window.close();" & vbCr & vbLf & "                                </script>"
        MyBase.Response.Write(close)
    End Sub

#End Region
#Region "'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was"
    Private Sub RentalAgreementFromLocal(sRentalDetails As String, sRentalId As String, sBookingId As String, sUserCode As String)

        RePrint(sRentalDetails, sRentalId, sBookingId, sUserCode)
        Xml1.Visible = True
        pdf.Visible = False

    End Sub
#End Region


End Class
