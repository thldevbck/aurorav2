<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="CustomerEdit.aspx.vb" Inherits="Booking_CustomerEdit" Title="Untitled Page" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/RegExTextBox/RegExTextBox.ascx" TagName="RegExTextBox"
    TagPrefix="uc4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">

var errorMessage = <%= MessageJSON %>;
    
function checkNameChars( obj )
{
	var chValue, szValue
	var newString1, newString2
		
	szValue = obj.value
	if ( szValue!= "" && szValue != null )
	{
		chValue = szValue.charAt(0)
		newString1 = chValue.toUpperCase()
		newString2 = szValue.substr( 1 )
		obj.value = newString1 + newString2
	}	
	return
}

function checkFullStop(obj)
{
	var sChar;
	var sStr
	var slen, i
	sStr = obj.value
	slen = sStr.length
	for ( i = 0; i < slen; i++)
		sStr = sStr.replace(".", " ")
	if (sStr.charAt(slen) == " ")
		sStr = sStr.substring(0,slen-1)
	obj.value = sStr
	return
}

function removeSpace(obj)
{ 
    obj.value = obj.value.replace(/(^\s*)|(\s*$)/g, "");
}

function setAllQuantity()
{
    //get target base & child control.
    var TargetBaseControl = document.getElementById('<%= brochuresGridView.ClientID %>');
    var TargetChildControl = "qtyTextBox";

    //get all the control of the type INPUT in the base control.
    var Inputs = TargetBaseControl.getElementsByTagName("input");  

    for(var n = 0; n < Inputs.length; ++n)
        if(Inputs[n].type == 'text' && Inputs[n].id.indexOf(TargetChildControl,0) >= 0)
             Inputs[n].value = '1';
            
    return false;
}

// Mod:Raj- May 18, 2011
 
function ClientValidation()
{ 
    //debugger
    var boolValidate=true;
    var lastNameTextBox;
    lastNameTextBox = $get('<%= lastNameTextBox.ClientID %>');   
    if (! validateAControl(lastNameTextBox))
    {
        lastNameTextBox.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.ErrorColor) %>';
        boolValidate= false;    
    }
    else
    {
        lastNameTextBox.style.background="white";
    }


    var firstNameTextBox;
    firstNameTextBox = $get('<%= firstNameTextBox.ClientID %>');
    if (! validateAControl(firstNameTextBox))
    {
        firstNameTextBox.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
        boolValidate= false;
    }
    else
    {
        firstNameTextBox.style.background="white";
    }
    
    //Get checkboxes
    var hirerCheckBox = $get('<%= hirerCheckBox.ClientID %>');   
    var driverCheckBox = $get('<%= driverCheckBox.ClientID %>');  
    
    //For driver
    if (driverCheckBox.checked )
    {
        var licenceNumTextBox = $get('<%= licenceNumTextBox.ClientID %>');
        if (! validateAControl(licenceNumTextBox))
        {
            return false
        }
        var expiryDateDateControl = $get('<%= expiryDateDateControl.ClientID %>');
        if (! validateAControl(expiryDateDateControl))
        {
            return false
        }
        var issuingAuthorityTextBox = $get('<%= issuingAuthorityTextBox.ClientID %>');
        if (! validateAControl(issuingAuthorityTextBox))
        {
            return false
        }
        var dobDateControl = $get('<%= dobDateControl.ClientID %>');
        if (! validateAControl(dobDateControl))
        {
            return false
        }
    }
    


    //For other
    if ( (!driverCheckBox.checked) || hirerCheckBox.checked)
    {
        var personTypeDropDownList = $get('<%= personTypeDropDownList.ClientID %>');
        if (! validateAControl(personTypeDropDownList))
        {
            personTypeDropDownList.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate= false;
        }
        else
        {
            personTypeDropDownList.style.background="white";
        }


        var txtNumber = $get('<%= txtNumber.ClientID %>');
        if (! validateAControl(txtNumber))
        {
            txtNumber.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate= false;
        }
        else
        {
            txtNumber.style.background="white";
        }


        var physicalStreetTextBox = $get('<%= physicalStreetTextBox.ClientID %>');
        if (! validateAControl(physicalStreetTextBox))
        {
            physicalStreetTextBox.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate= false;
        }
          else
        {
            physicalStreetTextBox.style.background="white";
        }
        

        var physicalCityTextBox = $get('<%= physicalCityTextBox.ClientID %>');
        if (! validateAControl(physicalCityTextBox))
        {
            physicalCityTextBox.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate= false;
        }
           else
        {
            physicalCityTextBox.style.background="white";
        }


        var physicalCountryPickerControl = $get('<%= physicalCountryPickerControl.ClientID %>');
        if (! validateAControl(physicalCountryPickerControl))
        {
            physicalCountryPickerControl.style.background='<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate= false;
        }
            else
        {
            physicalCountryPickerControl.style.background="white";
        }

        if(boolValidate==false)
        {
            return false;
        }
    
        
        var licenceNumTextBox;
        licenceNumTextBox = $get('<%= licenceNumTextBox.ClientID %>');
        if (licenceNumTextBox.value != "")
        {
            var expiryDateDateControl;
            expiryDateDateControl = $get('<%= expiryDateDateControl.ClientID %>');
            var issuingAuthorityTextBox;
            issuingAuthorityTextBox = $get('<%= issuingAuthorityTextBox.ClientID %>');
            var dobDateControl;
            dobDateControl = $get('<%= dobDateControl.ClientID %>');
            
	        if ( expiryDateDateControl.value == "" || issuingAuthorityTextBox.value == "" || dobDateControl.value == "" )
	        {
		        setWarningShortMessage("You must enter Expiry Date, Issuing Authority and Date of Birth for licence details")
		        return false
	        } 
        }
    }	
} 

function validateAControl(obj)
{
    if (obj.value == "")
    {
        setWarningShortMessage(getErrorMessage("GEN005"))
        return false
    }
    else
    {
        return true
    }
}

function checkCheckbox()
{
    //Get checkboxes
    var hirerCheckBox = $get('<%= hirerCheckBox.ClientID %>');   
    var driverCheckBox = $get('<%= driverCheckBox.ClientID %>');    
}

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <asp:UpdatePanel ID="updatePanel10" runat="server">
            <ContentTemplate>
                <div style="display: none">
                    <asp:Label ID="customerIdLabel" runat="server"></asp:Label>
                    <%--<asp:Label id="hirerLabel" runat="server"></asp:Label> --%>
                    <asp:Label ID="integrityNoLabel" runat="server"></asp:Label>
                    <asp:Label ID="dayCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="dayIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="dayIntNumLabel" runat="server"></asp:Label>
                    <asp:Label ID="nightCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="nightIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="nightIntNumLabel" runat="server"></asp:Label>
                    <asp:Label ID="mobileCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="mobileIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="mobileIntNumLabel" runat="server"></asp:Label>
                    <asp:Label ID="faxCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="faxIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="faxIntNumLabel" runat="server"></asp:Label>
                    <asp:Label ID="emailCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="emailIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="emailIntNumLabel" runat="server"></asp:Label>
                    <asp:Label ID="physicalAddressIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="physicalAddressCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="physicalAddressIntegrityNoLabel" runat="server"></asp:Label>
                    <asp:Label ID="postalAddressIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="postalAddressCodIdLabel" runat="server"></asp:Label>
                    <asp:Label ID="postalAddressIntegrityNoLabel" runat="server"></asp:Label>
                </div>
                <table width="100%">
                    <tr>
                        <td width="150">
                            Last Name:</td>
                        <td width="250">
                            <asp:TextBox onblur="checkNameChars(this)" ID="lastNameTextBox" runat="server" MaxLength="20"></asp:TextBox>&nbsp;*</td>
                        <td width="150">
                            First Name:</td>
                        <td>
                            <asp:TextBox onblur="checkNameChars(this); checkFullStop(this); removeSpace(this)"
                                ID="firstNameTextBox" runat="server" MaxLength="20"></asp:TextBox>&nbsp;*</td>
                    </tr>
                    <tr>
                        <td width="150">
                            Title:</td>
                        <td width="250">
                           

                                <%-- rev:mia Sept.2 2013 - Customer Title from query -REPLACE WITH DROPDOWN
                                    <asp:TextBox onblur="checkNameChars(this); checkFullStop(this); removeSpace(this)"
                                    ID="titleTextBox" runat="server" MaxLength="5"></asp:TextBox></td>
                                --%>
                        
                                <asp:DropDownList ID="customertitleDropdown" runat="server" Width="50px">
                                </asp:DropDownList>
            
                    </tr>
                    <tr>
                        <td>
                            <br />
                        </td>
                    </tr>
                    <tr runat="server" id="trFlag">
                        <td>
                            Flags:</td>
                        <td colspan="3">
                            <asp:CheckBox ID="hirerCheckBox" runat="server" Text="Hirer" AutoPostBack="true"
                                onclick="checkCheckbox()"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="primaryHirerCheckBox" runat="server" Text="Primary  Hirer" AutoPostBack="true"
                                onclick="checkCheckbox()"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:CheckBox ID="driverCheckBox" runat="server" Text="Driver" AutoPostBack="true"
                                onclick="checkCheckbox()"></asp:CheckBox>&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <br />
                <uc3:CollapsiblePanel ID="driverCollapsiblePanel" runat="server" Title="Driver Information"
                    TargetControlID="driverContentPanel" />
                <asp:Panel ID="driverContentPanel" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Licence Number:</td>
                            <td width="250" nowrap="nowrap">
                                <asp:TextBox ID="licenceNumTextBox" runat="server" AutoPostBack="True" MaxLength="20"></asp:TextBox>&nbsp;<div
                                    id="div5" runat="server" style="display: none">
                                    *</div>
                            </td>
                            <td width="150">
                                Expiry Date:</td>
                            <td nowrap="nowrap">
                                <uc1:DateControl id="expiryDateDateControl" runat="server">
                                </uc1:DateControl>&nbsp;<div id="div6" runat="server" style="display: none">
                                    *</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Issuing Authority:</td>
                            <td nowrap="nowrap">
                                <asp:TextBox ID="issuingAuthorityTextBox" runat="server" MaxLength="20"></asp:TextBox>&nbsp;<div
                                    id="div7" runat="server" style="display: none">
                                    *</div>
                            </td>
                            <td>
                                Date Of Birth:</td>
                            <td nowrap="nowrap">
                                <uc1:DateControl id="dobDateControl" runat="server" AutoPostBack="true">
                                </uc1:DateControl>&nbsp;<div id="div8" runat="server" style="display: none">
                                    *</div>
                                &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="ageLabel" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <br />
                <uc3:CollapsiblePanel ID="hirerCollapsiblePanel" runat="server" Title="Hirer Information"
                    TargetControlID="hirerContentPanel" />
                <asp:Panel ID="hirerContentPanel" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Flags:</td>
                            <td width="250">
                                <asp:CheckBox ID="vipCheckBox" runat="server" Text="VIP"></asp:CheckBox>&nbsp;&nbsp;<asp:CheckBox
                                    ID="fleetDriverCheckBox" runat="server" Text="Fleet"></asp:CheckBox></td>
                            <td width="150">
                                Type:</td>
                            <td>
                                <asp:DropDownList ID="personTypeDropDownList" runat="server" AppendDataBoundItems="true">
                                    <asp:ListItem Text="" Value=""></asp:ListItem>
                                </asp:DropDownList>&nbsp;<div id="div1" runat="server" style="display: none">
                                    *</div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Passport No:</td>
                            <td>
                                <asp:TextBox ID="passportNoTextBox" runat="server" MaxLength="10"></asp:TextBox>
                            </td>
                            <td>
                                Country of Issue:
                            </td>
                            <td>
                                
                                 <%-- 
                                        rev:mia Sept.2 2013 - country Title from query -REPLACE WITH DROPDOWN
                                        <asp:TextBox ID="passportCountryTextBox" runat="server" MaxLength="10"></asp:TextBox>
                                --%>
                                <uc1:PickerControl ID="passportCountryPickerControl" 
                                                   runat="server" 
                                                   PopupType="POPUPCOUNTRY"
                                                   AppendDescription="true" 
                                                   width="150" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                    </table>
                    <table width="100%" style="border-top-style: dotted; border-top-width: 1px;">
                        <tr>
                            <td width="150">
                            </td>
                            <td width="250">
                            </td>
                            <td width="150">
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <strong>Physical Address</strong></td>
                            <td colspan="2">
                                <strong>Postal Address</strong></td>
                        </tr>
                    </table>
                    <table width="100%">
                        <tbody>
                            <tr width="100%">
                                <td width="400">
                                    <asp:Panel ID="panel1" runat="server" Width="100%">
                                        <table width="100%">
                                            <tr>
                                                <td width="150">
                                                    Number:</td>
                                                <td width="250" nowrap="nowrap">
                                                    <asp:TextBox ID="txtNumber" runat="server" onBlur="removeSpace(this)"
                                                        MaxLength="10"></asp:TextBox>&nbsp;<div id="divIsStreetNumberRequired" runat="server" style="display: none">*</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="150">
                                                    Street:</td>
                                                <td width="250" nowrap="nowrap">
                                                    <asp:TextBox ID="physicalStreetTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="40"></asp:TextBox>&nbsp;<div id="div2" runat="server" style="display: none">
                                                            *</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Suburb:</td>
                                                <td>
                                                    <asp:TextBox ID="physicalSuburbTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="40"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    City / Town:</td>
                                                <td nowrap="nowrap">
                                                    <asp:TextBox ID="physicalCityTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="20"></asp:TextBox>&nbsp;<div id="div3" runat="server" style="display: none">
                                                            *</div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    State:</td>
                                                <td>
                                                    <asp:TextBox ID="physicalStateTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="10"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    PostCode:</td>
                                                <td>
                                                    <asp:TextBox ID="physicalPostCodeTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="10"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Country:</td>
                                                <td nowrap="nowrap">
                                                    <uc1:PickerControl ID="physicalCountryPickerControl" runat="server" PopupType="POPUPCOUNTRY"
                                                        AppendDescription="true" width="150" />&nbsp;<div id="div4" runat="server" style="display: none">
                                                            *</div>
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                                <td>
                                    <asp:Panel ID="panel2" runat="server" Width="100%">
                                        <table width="100%">
                                            <tr>
                                                <td width="150">
                                                    Street:</td>
                                                <td>
                                                    <asp:TextBox ID="postalStreetTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="40"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Suburb:</td>
                                                <td>
                                                    <asp:TextBox ID="postalSuburbTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="40"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    City / Town:</td>
                                                <td>
                                                    <asp:TextBox ID="postalCityTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="10"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    State:</td>
                                                <td>
                                                    <asp:TextBox ID="postalStateTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="10"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    PostCode:</td>
                                                <td>
                                                    <asp:TextBox ID="postalPostcodeTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"
                                                        MaxLength="10"></asp:TextBox></td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Country:</td>
                                                <td>
                                                    <uc1:PickerControl ID="postalCountryPickerControl" runat="server" PopupType="POPUPCOUNTRY"
                                                        AppendDescription="true" width="150" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%">
                        <tbody>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="saveButton" OnClick="saveButton_Click" runat="server" Text="Save"
                                        CssClass="Button_Standard Button_Save" OnClientClick="return ClientValidation();">
                                    </asp:Button>
                                    <asp:Button ID="newButton" OnClick="newButton_Click" runat="server" Text="Save/New"
                                        CssClass="Button_Standard Button_New" OnClientClick="return ClientValidation();">
                                    </asp:Button>
                                    <asp:Button ID="cancelButton" OnClick="cancelButton_Click" runat="server" Text="Reset"
                                        CssClass="Button_Standard Button_Reset"></asp:Button>
                                    <asp:Button ID="deleteButton" OnClick="deleteButton_Click" runat="server" Text="Delete"
                                        CssClass="Button_Standard Button_Delete" OnClientClick='return confirm("Are you sure you wish to delete this customer?");'>
                                    </asp:Button>
                                    <asp:Button ID="backButton" OnClick="backButton_Click" runat="server" Text="Back"
                                        CssClass="Button_Standard Button_Back"></asp:Button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <table width="100%" style="border-top-style: dotted; border-top-width: 1px;">
                        <tbody>
                            <tr width="100%">
                                <td>
                                    <strong>Contact Numbers</strong></td>
                                <td colspan="3">
                                </td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td width="150">
                                </td>
                                <td width="50">
                                    Country</td>
                                <td width="50">
                                    Area</td>
                                <td width="145">
                                    Number</td>
                                <td width="150">
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Day:</td>
                                <td>
                                    <asp:TextBox ID="dayCountryTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="dayAreaTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="dayNumberTextBox" runat="server" Width="80" MaxLength="24"></asp:TextBox></td>
                                <td>
                                    Email:</td>
                                <td>
                                    <asp:TextBox ID="emailTextBox" runat="server" Width="97%" MaxLength="40"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Night:</td>
                                <td>
                                    <asp:TextBox ID="nightCountryTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="nightAreaTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="nightNumberTextBox" runat="server" Width="80" MaxLength="24"></asp:TextBox></td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Mobile:</td>
                                <td>
                                    <asp:TextBox ID="mobileCountryTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="mobileAreaTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="mobileNumberTextBox" runat="server" Width="80" MaxLength="24"></asp:TextBox></td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Fax:</td>
                                <td>
                                    <asp:TextBox ID="faxCountryTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="faxAreaTextBox" runat="server" Width="40" MaxLength="6"></asp:TextBox></td>
                                <td>
                                    <asp:TextBox ID="faxNumberTextBox" runat="server" Width="80" MaxLength="24"></asp:TextBox></td>
                                <td>
                                </td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <table width="100%" style="border-top-style: dotted; border-top-width: 1px;">
                        <tbody>
                            <tr>
                                <td width="150">
                                    <b>Marketing Data </b>
                                </td>
                            </tr>
                            <tr>
                                <td width="150">
                                    Flags:</td>
                                <td width="300">
                                    <asp:CheckBox ID="mailingListCheckBox" runat="server" Text="Opt-In to THL info"></asp:CheckBox>
                                    <asp:CheckBox ID="surveysCheckBox" runat="server" Text="Online Customer Survey"></asp:CheckBox>
                                </td>
                                <td width="150">
                                    Pref. Comm:</td>
                                <td>
                                    <asp:DropDownList ID="prefCommDropDownList" runat="server" AppendDataBoundItems="true">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr style="display: none">
                                <td>
                                    Gender:</td>
                                <td>
                                    <asp:DropDownList ID="genderDropDownList" runat="Server" AppendDataBoundItems="true">
                                        <asp:ListItem Text="" Value=""></asp:ListItem>
                                        <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                                        <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td>
                                    Occupation:</td>
                                <td>
                                    <asp:TextBox ID="occupationTextBox" runat="server" MaxLength="15"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>
                                    Loyalty Card Number:</td>
                                <td>
                                    <%--<uc4:RegExTextBox ID="loyaltyCardNumberTextBox" runat="server" Width="200px" RegExPattern="^([\w]+)$"
                                        Style="float: left" MaxLength="64" ErrorMessage="Enter a valid Loyalty Card Number"
                                        Text='<%# Bind("LoyaltyCardNumber") %>' />--%>
                                    <uc4:RegExTextBox ID="loyaltyCardNumberTextBox" runat="server" Width="200px" RegExPattern="^.+"
                                        Style="float: left" MaxLength="64" ErrorMessage="Enter a valid Loyalty Card Number"
                                        Text='<%# Bind("LoyaltyCardNumber") %>' />
                                </td>
                                <td>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
                    <table width="100%" style="border-top-style: dotted; border-top-width: 1px; visibility: hidden">
                        <tbody>
                            <tr>
                                <td>
                                    <strong>Brochures</strong></td>
                                <td align="right">
                                    <asp:Button ID="requestAllButton" runat="server" Text="Request All" CssClass="Button_Standard"
                                        OnClientClick="return setAllQuantity();"></asp:Button></td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:GridView ID="brochuresGridView" runat="server" Width="100%" CssClass="dataTable"
                        AutoGenerateColumns="False" Visible="false">
                        <AlternatingRowStyle CssClass="oddRow" />
                        <RowStyle CssClass="evenRow" />
                        <Columns>
                            <asp:TemplateField HeaderText="Qty" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="brhIdLabel" runat="server" Text='<%# Bind("BrhId") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="BrdName" HeaderText="Brand" ReadOnly="True" />
                            <asp:BoundField DataField="BrhDateIss" HeaderText="Issue Date" ReadOnly="True" />
                            <asp:BoundField DataField="BrhName" HeaderText="Name" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Total">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="totalLabel" Text='<%# eval("Total") %>' Width="40"
                                        Style="text-align: right" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="DateRequested" HeaderText="Date Requested" ReadOnly="True" />
                            <asp:TemplateField HeaderText="Qty" Visible="true">
                                <ItemTemplate>
                                    <asp:TextBox ID="qtyTextBox" runat="server" Width="50" Text='<%# Bind("Qty") %>'
                                        Style="text-align: right"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </asp:Panel>
                <br />
                <table width="100%">
                    <tbody>
                        <tr>
                            <td align="right">
                                <asp:Button ID="saveButton1" OnClick="saveButton_Click" runat="server" Text="Save"
                                    CssClass="Button_Standard Button_Save" OnClientClick="return ClientValidation();">
                                </asp:Button>
                                <asp:Button ID="newButton1" OnClick="newButton_Click" runat="server" Text="Save/New"
                                    CssClass="Button_Standard Button_New"  OnClientClick="return ClientValidation();">
                                </asp:Button>
                                <asp:Button ID="cancelButton1" OnClick="cancelButton_Click" runat="server" Text="Reset"
                                    CssClass="Button_Standard Button_Reset"></asp:Button>
                                <asp:Button ID="deleteButton1" OnClick="deleteButton_Click" runat="server" Text="Delete"
                                    CssClass="Button_Standard Button_Delete" OnClientClick='return confirm("Are you sure you wish to delete this customer?");'>
                                </asp:Button>
                                <asp:Button ID="backButton1" OnClick="backButton_Click" runat="server" Text="Back"
                                    CssClass="Button_Standard Button_Back"></asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br />
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
