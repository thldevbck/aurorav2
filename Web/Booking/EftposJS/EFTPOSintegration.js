﻿// JScript File
//Feb 12 2010 - DPS eftpos integration - manny rocks
var MethodValue      ='';
var amtWithSurcharge ='';
var cardSelected     ='';
var printername      = '';
var ReceiptHeaderForPayment      = '';
var ReceiptHeaderForRefund       = '';
var overallstatus;
var navigateurl                  = '';
var card                         = '';
var DisplayCardMismatch;
var basecard;         
var isVerifyclick;
var cardValueOfCreditCard         = '';
var cardVerificationFinished      = true;          
var isNegativePayment;
var isNegativePaymentRefunding; //this flag is for refunding a negative payment
var isCardDefault

var errCancel = 'Card validation failed. Please dont click Cancel button in the EFTPOS dialog window.'
var errVerify = 'Please click Verify Card button before swiping your card.'
var errVerifyNotcomplete = 'Card Verication is not complete. Please try again.'
var errNotSame = 'This card type is not the same as the original card type used for payment.'
var errorIconOnSQL = '<img src = ../IMAGES/rental_error.gif />&nbsp;&nbsp;'
var errBond = 'Card validation failed. This card type is not use in Bond payment.'
var cardNoisOkButCardNameIsNot = 'Card validation failed. CardHolder Number match but CardHolder Name is not.'
var chkOrSaveErrorInEftpos = 'WARNING - <b>CHQ or SAV</b> selected by customer will display as an <b>EFTPOS</b> payment on the booking in Aurora'
var amountIsLessZero = 'Value less than zero is not allowed.'
var amountIsMorethanOriginal = 'Value more that than actual amount is not allowed.'
var additionalMessage = ''
var additionalMessageForMethodType = '';
var additionalMessageForSwipedCard = '';
var additionalMessageForDPSCard = '';
var baseCardAgainstOutputrack2 = '';
var baseCardNameAgainstOutputrack1 = '';
var errVerifyTrack = 'ERROR: Please <b> SWIPE</b> card properly.'

$(document).ready(function() {
    dpsEftX.EnablePrintReceipt = 0//'<%= ConfigurationManager.AppSettings("EnablePrintReceipt").ToString %>'
});



//|------------------------------------------------------------------------------------|
//|called when PayButton is click and raised the AuthorizedEvent defined in the script |
//|------------------------------------------------------------------------------------|
function Pay()
{      
       
       //if amount is less than zero..then process this as a refund
       var amounttemp  = $("#AmountTextbox").val();
       var mode        = $("[id$=_HiddenMode]").attr("value")
       var HiddenType  = $("[id$=_HiddenType]").val()

       if((parseFloat(amounttemp) < 0 || amounttemp.indexOf('-') != -1) && !isNegativePaymentRefunding ) {

       
           //it Type = R - --> dont include surcharge when doing refund otherwise include it

           //trim down negative symbol 
           if(mode  == "0") {
               if (HiddenType != 'R') amounttemp = $("#TotalText").val()
               if (amounttemp == 'N/A') amounttemp = $("#AmountTextbox").val();

               if (amounttemp.indexOf("-") == 0) amounttemp = amounttemp.split("-")[1]

               
           }

           dpsEftX.Amount               = amounttemp
           dpsEftX.TxnType = "Refund";
           $("[id$=_HiddenIsOverride]").attr("value", "False")
       }
       else
       {
          isNegativePayment            =  false
          if(!isNegativePaymentRefunding && mode  == "1")
          {
               isNegativePayment            =  false
               amtWithSurcharge             = $("#TotalText").val()
          }
          else
          {
               if(mode  == "1")  
               {
                   amtWithSurcharge = $("#TotalText").val()

                  if (amtWithSurcharge == 'N/A' && (HiddenType == 'R' || HiddenType == 'D')) amtWithSurcharge = $("#AmountTextbox").val(); 

                  if(amtWithSurcharge.indexOf("-") == 0)   amtWithSurcharge         = amtWithSurcharge.split("-")[1]
                  else amtWithSurcharge         = amtWithSurcharge
                  $("[id$=_HiddenPurchase]").attr("value", "Refund")
                  $("[id$=_HiddenIsOverride]").attr("value", "False")
               }     
               else             
               {
                  amounttemp = $("#TotalText").val()
                  if(amounttemp.indexOf("-") == 0)   amtWithSurcharge         = amounttemp.split("-")[1]
                  else amtWithSurcharge         =     amounttemp
               } 
          }
    
               dpsEftX.Amount               = amtWithSurcharge
               dpsEftX.TxnType = "Purchase";

           
       }//if(parseFloat(amounttemp) < 0 || amounttemp.indexOf('-') != -1)

         //alert(dpsEftX.EnablePrintReceipt + ' ' + dpsEftX.PrinterName )  
         dpsEftX.MerchantReference     = $("[id$=_HiddenRentalID]").attr("value") 
         dpsEftX.ReceiptHeader         = ReceiptHeaderForPayment
         dpsEftX.DoAuthorize();
  }//.function Pay()
  
  

//|------------------------------------------------------------------------------------|
//|when a payment is negative..this will be treated as a DPS refund                    |
//|------------------------------------------------------------------------------------|
function CardReadEventForRefundForNegative()
{
    DisplayGrowl("") //remove all message 
    var mode = $("[id$=_HiddenMode]").attr("value")
    var rentalID = $("[id$=_HiddenRentalID]").attr("value") + " (READCARD EVENT - CardReadEventForRefundForNegative())"

     if(!isNegativePayment) return;
    
    
    //prevent username and password from popping in when its already been validated
     if(cardVerificationFinished == true) 
     {
         DisplayGrowl(errVerify, 40) 
         return;
     }// if(cardVerificationFinished == true)

//     if (CardTracksHasError()) {
//         DisplayGrowl(errVerifyTrack, 40)
//         $("#CancelButton").attr("disabled", false);
//         $("#VerifyCard").attr("disabled", false);
//         return;
//     }
     
    isVerifyclick = false;
  
   
   card             = dpsEftX.OutputTrack2;
   basecard         = jQuery.trim($('[id$=_hiddenCardNumber]').val())
   card             = card.replace(';','')
   card             = card.split("=")

   baseCardAgainstOutputrack2 = '[' + basecard + '] compared to Track2 [' + card[0] + ']'
   baseCardNameAgainstOutputrack1 = '[' + jQuery.trim($("#nameText").val()) + '] compared to Track1 [' + jQuery.trim(dpsEftX.OutputTrack1.split("^")[1]) + ']'

   //alert("CardReadEvent() OUTPUTTRACK2: " + card)

   if(card[0].length == 0) 
   {
            DisplayGrowl(errCancel,40)
            $("#CancelButton").attr("disabled", false);
            $("#VerifyCard").attr("disabled", false);
            cardVerificationFinished = true
            isVerifyclick = false
            //rev:mia dec 2 2010 - logged readcard event
            LogReadCardEvent(rentalID)
            return;
   }//if(card[0].length == 0)

   //validation to check if its allowed to refund
   var mode        = $("[id$=_HiddenMode]").attr("value")
   var rentalOnly  = $("[id$=_HiddenRentalID]").attr("value")
   var RptType     = $("[id$=_HiddenType]").val()
   var amountToPay = $("#AmountTextbox").val();
   var CreditCardInBooking;
   var hiddenamt = $('[id$=_HiddenFieldAmount]').val()
   var isChange = "&isChanged=False"

   var surchargeAmt =  $("#SurchargeText").val()

   if (hiddenamt.length == 0) {
       if (surchargeAmt == 'N/A') surchargeAmt = '9999999999'
       hiddenamt = surchargeAmt 
       if (parseFloat(hiddenamt) < 0) {
           hiddenamt = surchargeAmt
       }
   }

   var manualref = '&manualRefund=PAYMENTREF'
   var pdtPmtID = $("[id$=_HiddenPdtPmtID]").attr("value")
  // if (mode == 0) {
   var temp = rentalOnly + "|" + card[0] + "|" + RptType + "|" + amountToPay + "|" + hiddenamt + '|' + pdtPmtID
       var url = "EFTPOSListener/EftposListener.aspx?mode=IsAllowedToRefund" + isChange + manualref
       var nopayment = false;
       var nopaymentforthiscard = false;
       $.post(url, { IsAllowedToRefund: temp }, function (result) {
           if (result.indexOf('ERROR') != -1) {
               DisplayGrowl(result, 40)
               $("#VerifyCard").attr('disabled', false)
               $("#PayButton").attr('disabled', true);
               $("#CancelButton").attr("disabled", false);
               cardVerificationFinished = true
               //rev:mia dec 2 2010 - logged readcard event
               LogReadCardEvent(rentalID)
               return
           }
           else {
               $("#CancelButton").attr("disabled", false);
               var nopaymentresult = '';

               //rule that is overlapping.. this is the result of lack of requirements gathering..a fragile approach.
               if (result.indexOf('NOPAYMENT') != -1) {
                   nopaymentresult = result.split('NOPAYMENT:')[1]
                   nopaymentresult = "(" + nopaymentresult + ")"
                   nopayment = true;
               } else { nopayment = false; }

               //CHECK IF OVERRIDE WILL BE DISPLAY
               if (result.indexOf('OVERRIDE') != -1) {
                   cardVerificationFinished = true
                   if ((card[0].length == 0) && (dpsEftX.CardNumber.length == 0)) {
                       $("#VerifyCard").attr('disabled', false)
                       $("#PayButton").attr('disabled', true);
                       $("#CancelButton").attr("disabled", false);
                       DisplayGrowl(errCancel)
                       //rev:mia dec 2 2010 - logged readcard event
                       LogReadCardEvent(rentalID)
                       return
                   }

                   var resultoverride = result.split('OVERRIDE:')[1]
                   DisplayGrowl(resultoverride)
                   DisplayOverrideBox()
                   //rev:mia dec 2 2010 - logged readcard event
                   LogReadCardEvent(rentalID)
                   return
               }

               //start ------------------------------------------
               //this will retreive all credit cards used in this booking
               url = "EFTPOSListener/EftposListener.aspx?mode=IsCreditCardInBooking"

               if (rentalOnly.indexOf("-") != -1)
                   CreditCardInBooking = $("[id$=_HiddenRentalID]").attr("value").split("-")[0] + "|" + card[0]
               else {
                   if (rentalOnly.indexOf("/") != -1) {
                       CreditCardInBooking = $("[id$=_HiddenRentalID]").attr("value").split("/")[0] + "|" + card[0]
                   } //if(rentalOnly.indexOf("/") != -1)
               } //if(rentalOnly.indexOf("-") != -1)


               CreditCardInBooking = CreditCardInBooking + "|" + GetNameInCreditCard()//$("#nameText").val()
               $.post(url, { IsCreditCardInBooking: CreditCardInBooking }, function (result) {

                   var rptFull = ""
                   if (RptType == "B") rptFull = "BOND"
                   if (RptType == "R") rptFull = "RECEIPT"
                   if (RptType == "D") rptFull = "DEPOSIT"

                   if (result == 'True') {

                       if (nopayment == true) {
                           DisplayGrowl(errorIconOnSQL + 'No payment was taken for the <b>' + rptFull + '</b> type, <b>please do not refund.</b>', 40)
                           cardVerificationFinished = true
                           //rev:mia dec 2 2010 - logged readcard event
                           LogReadCardEvent(rentalID)
                           return
                       }

                       
                       if (RptType == "B") {
                           url = "EFTPOSListener/EftposListener.aspx?mode=IsCreditCardIsFoundInBondPayment"
                           var CreditCardIsFoundInBondPayment;
                           var rentalOnly = $("[id$=_HiddenRentalID]").attr("value")
                           var IsCreditCardIsFoundInBondPayment;
                           IsCreditCardIsFoundInBondPayment = $("[id$=_HiddenRentalID]").attr("value") + "|" + jQuery.trim(card[0])
                           IsCreditCardIsFoundInBondPayment = IsCreditCardIsFoundInBondPayment + "|" + GetNameInCreditCard()
                           $.post(url, { IsCreditCardIsFoundInBondPayment: IsCreditCardIsFoundInBondPayment }, function (result) {
                               if (result == 'True') {
                                   $("#PayButton").show()
                                   $("#PayButton").attr('disabled', false);
                                   $("#VerifyCard").hide()
                                   $("#divVerification").hide();
                                   $("#CancelButton").attr("disabled", false);
                                   $("[id$=_HiddenIsOverride]").attr("value", "False")
                                   //rev:mia dec 2 2010 - logged readcard event
                                   LogReadCardEvent(rentalID)
                               }
                               else {

                                   var errMsg = 'Card validation failed.This card type is not use in Bond payment'
                                   $("#divVerification").show().css("border", "1px solid gray")
                                   $("#VerifyCard").attr('disabled', true)
                                   $("#CancelButton").attr("disabled", true);
                                   $('[id$=trReasonOtherVerified]').hide()
                                   $("[id$=_HiddenIsOverride]").attr("value", "True")
                                   DisplayGrowl(errMsg)
                                   //rev:mia dec 2 2010 - logged readcard event
                                   LogReadCardEvent(rentalID)
                               }

                           })//$.post(url, { IsCreditCardIsFoundInBondPayment: IsCreditCardIsFoundInBondPayment }, function (result) {

                       } else {

                           $("#PayButton").show()
                           $("#PayButton").attr('disabled', false);
                           $("#VerifyCard").hide()
                           $("#divVerification").hide();
                           $("#CancelButton").attr("disabled", false);
                           $("#nameText").attr("value", GetNameInCreditCard())
                           $("[id$=_HiddenIsOverride]").attr("value", "False")
                           DisplayGrowl("")
                           //rev:mia dec 2 2010 - logged readcard event
                           LogReadCardEvent(rentalID)
                       } //if (RptType == "B") {
                      

                   }
                   else {

                       if (RptType == "B") {
                           url = "EFTPOSListener/EftposListener.aspx?mode=IsCreditCardIsFoundInBondPayment"
                           var CreditCardIsFoundInBondPayment;
                           var rentalOnly = $("[id$=_HiddenRentalID]").attr("value")
                           var IsCreditCardIsFoundInBondPayment;
                           IsCreditCardIsFoundInBondPayment = $("[id$=_HiddenRentalID]").attr("value") + "|" + jQuery.trim(card[0])
                           IsCreditCardIsFoundInBondPayment = IsCreditCardIsFoundInBondPayment + "|" + GetNameInCreditCard()
                           $.post(url, { IsCreditCardIsFoundInBondPayment: IsCreditCardIsFoundInBondPayment }, function (result) {
                               if (result == 'True') {
                                   $("#PayButton").show()
                                   $("#PayButton").attr('disabled', false);
                                   $("#VerifyCard").hide()
                                   $("#divVerification").hide();
                                   $("#CancelButton").attr("disabled", false);
                                   $("[id$=_HiddenIsOverride]").attr("value", "False")
                                   //rev:mia dec 2 2010 - logged readcard event
                                   LogReadCardEvent(rentalID)
                               }
                               else {
                                   if (nopayment == true) {
                                       DisplayGrowl(errorIconOnSQL + 'No payment was taken for the <b>' + rptFull + '</b> type, <b>please do not refund.</b>', 40)
                                       $("#VerifyCard").attr('disabled', false)
                                       $("#PayButton").attr('disabled', true);
                                       $("#CancelButton").attr("disabled", false);
                                       cardVerificationFinished = true
                                       //rev:mia dec 2 2010 - logged readcard event
                                       LogReadCardEvent(rentalID)
                                       return
                                   }

                                   //var errMsg = 'Card validation failed. This card type is not use in Bond payment.'
                                   $("#divVerification").show().css("border", "1px solid gray")
                                   $("#VerifyCard").attr('disabled', true)
                                   $("#CancelButton").attr("disabled", true);
                                   //$('[id$=trReasonOtherVerified]').hide()
                                   $("[id$=_HiddenIsOverride]").attr("value", "True")
                                   DisplayGrowl(errBond)
                                   //rev:mia dec 2 2010 - logged readcard event
                                   LogReadCardEvent(rentalID)
                               }
                           })//$.post(url, { IsCreditCardIsFoundInBondPayment: IsCreditCardIsFoundInBondPayment }, function (result) {
                       } else {
                           if (DisplayCardMismatch == 'true' || DisplayCardMismatch == true) {

                               if (nopayment == true) {
                                    DisplayGrowl(errorIconOnSQL + 'No payment was taken for the <b>' + rptFull + '</b> type, <b>please do not refund.</b>', 40)
                                   $("#VerifyCard").attr('disabled', false)
                                   $("#PayButton").attr('disabled', true);
                                   $("#CancelButton").attr("disabled", false);
                                   cardVerificationFinished= true
                                   isVerifyclick = false
                                   //rev:mia dec 2 2010 - logged readcard event
                                   LogReadCardEvent(rentalID)
                                   return
                               }
                               else DisplayGrowl(errNotSame, 40)

                               $("#divVerification").show().css("border", "1px solid gray")
                               $("#VerifyCard").attr('disabled', true)
                               $("#CancelButton").attr("disabled", true);
                               $('[id$=trReasonOtherVerified]').hide()
                               $("[id$=_HiddenIsOverride]").attr("value", "True")
                               //rev:mia dec 2 2010 - logged readcard event
                               LogReadCardEvent(rentalID)
                           } //if(DisplayCardMismatch == 'true' || DisplayCardMismatch == true)
                       } ////if (RptType == "B") {




                   } //if(result == true || result == 'true') 
               });
               //end ------------------------------------------
           }
       })
       cardVerificationFinished = true
       isVerifyclick = false
     
}//function CardReadEventForRefundForNegative()


//|------------------------------------------------------------------------------------|  
//|called when PayButton is click and raised the AuthorizedEvent defined in the script |   
//|------------------------------------------------------------------------------------|
function RefundPayment() {
       $("[id$=AmountTextbox]").attr("disabled", true)
       if(isVerifyclick)
       {
            isVerifyclick = false;
            alert(errVerifyNotcomplete)
            $("#VerifyCard").show()
            $("#PayButton").hide()
            $("#divVerification").hide()
            return;
       }
       
      var HiddenType        = $("[id$=_HiddenType]").val()
      //it Type = R - --> dont include surcharge when doing refund otherwise include it

      if (HiddenType == 'R' && !isNegativePaymentRefunding || HiddenType == 'D' && !isNegativePaymentRefunding) 
      {
            amtWithSurcharge = $("#AmountTextbox").val()
      }
      else
      {
          amtWithSurcharge = $("#TotalText").val()
          if (amtWithSurcharge == 'N/A') amtWithSurcharge = $("#AmountTextbox").val()

      }//if(HiddenType == 'R')   

      if (amtWithSurcharge.indexOf("-") == 0) amtWithSurcharge = amtWithSurcharge.split("-")[1]

      dpsEftX.Amount               = amtWithSurcharge
      dpsEftX.TxnType              = "Refund";
      dpsEftX.ReceiptHeader        = ReceiptHeaderForRefund
      dpsEftX.MerchantReference    = $("[id$=_HiddenRentalID]").attr("value") 
      dpsEftX.DoAuthorize();
        
  }
  

//|------------------------------------------------------------------------------------|
//|called when PayButton is click and raised the CardReadEvent defined in the script    |
//|------------------------------------------------------------------------------------|
  function ReadCard() {
      if (cardVerificationFinished == true ||   isVerifyclick == false) {
          DisplayGrowl(errVerify, 40)
          return;
      }
    $("#PayButton").attr("disabled", true);
  
    dpsEftX.DisplayLine1 = ""
    dpsEftX.DoReadCard();


}

//|------------------------------------------------------------------------------------|
//|this will be call by the ReadCard Event                                             |
//|------------------------------------------------------------------------------------|  
function  CardReadEventForRefund() {
    DisplayGrowl("") //remove all message 
    var rentalID = $("[id$=_HiddenRentalID]").attr("value") + " (READCARD EVENT - CardReadEventForRefund())"

     //prevent username and password from popping in when its already been validated
     if(cardVerificationFinished == true) 
     {
         DisplayGrowl(errVerify, 40) 
         return;
     }
     
//     if (CardTracksHasError()) {
//         DisplayGrowl(errVerifyTrack, 40)
//         $("#CancelButton").attr("disabled", false);
//         $("#VerifyCard").attr("disabled", false);
//         return;
//     }
     
     var typeofpayment = $("[id$=_HiddenPurchase]").val()
     card              = dpsEftX.OutputTrack2;
     basecard          = jQuery.trim($('[id$=_hiddenCardNumber]').val())
     card              = card.replace(';','')
     card              = card.split("=")
     var HiddenType    = $("[id$=_HiddenType]").val()
     var amountToPay   = $("#AmountTextbox").val();

     baseCardAgainstOutputrack2 = '[' + basecard + '] compared to Track2 [' + card[0] + ']'// length is ' + basecard.length + " - " + card[0].length
     baseCardNameAgainstOutputrack1 = '[' + jQuery.trim($("#nameText").val()) + '] compared to Track1 [' + jQuery.trim(dpsEftX.OutputTrack1.split("^")[1]) + ']'


     var errMsg
     var rptFull = ""
     var mode = $("[id$=_HiddenMode]").attr("value")

     if (HiddenType == "B") rptFull = "BOND"
     if (HiddenType == "R") rptFull = "RECEIPT"
     if (HiddenType == "D") rptFull = "DEPOSIT"
     var hiddenamt = $('[id$=_HiddenFieldAmount]').val()
     var nopayment = false;
     
     var isChange = "&isChanged=False"
     var pdtPmtID = $("[id$=_HiddenPdtPmtID]").attr("value")
     var rentalOnly = $("[id$=_HiddenRentalID]").attr("value")
     var temp = rentalOnly + "|" + card[0] + "|" + HiddenType + "|" + amountToPay + '|' + hiddenamt + '|' + pdtPmtID
     var url = "EFTPOSListener/EftposListener.aspx?mode=IsAllowedToRefund" + isChange //+ manualref
     $.post(url, { IsAllowedToRefund: temp }, function (result) {
         if (result.indexOf('ERROR') != -1) {
             DisplayGrowl(result, 40)
             $("#VerifyCard").attr('disabled', false)
             $("#PayButton").attr('disabled', true);
             $("#CancelButton").attr("disabled", false);
             cardVerificationFinished = true
             isVerifyclick = false
             //rev:mia dec 2 2010 - logged readcard event
             LogReadCardEvent(rentalID)
             return
         }
         else {

             //CHECK IF OVERRIDE WILL BE DISPLAY
             if (result.indexOf('OVERRIDE') != -1) {
                 isVerifyclick = false;
                 cardVerificationFinished = true
                 if ((card[0].length == 0) && (dpsEftX.CardNumber.length == 0)) {
                     $("#VerifyCard").attr('disabled', false)
                     $("#PayButton").attr('disabled', true);
                     $("#CancelButton").attr("disabled", false);
                     DisplayGrowl(errCancel)
                     //rev:mia dec 2 2010 - logged readcard event
                     LogReadCardEvent(rentalID)
                     return
                 }
                 nopaymentresult = result.split('OVERRIDE:')[1]
                 DisplayOverrideBox()
                 DisplayGrowl(nopaymentresult)
                 //rev:mia dec 2 2010 - logged readcard event
                 LogReadCardEvent(rentalID)
                 return
             }

             if (HiddenType == 'B') {

                 var url = "EFTPOSListener/EftposListener.aspx?mode=IsCreditCardIsFoundInBondPayment"
                 var CreditCardIsFoundInBondPayment;
                 var rentalOnly = $("[id$=_HiddenRentalID]").attr("value")
                 var IsCreditCardIsFoundInBondPayment;

                 IsCreditCardIsFoundInBondPayment = $("[id$=_HiddenRentalID]").attr("value") + "|" + jQuery.trim(card[0])


                 IsCreditCardIsFoundInBondPayment = IsCreditCardIsFoundInBondPayment + "|" + GetNameInCreditCard()
                 $.post(url, { IsCreditCardIsFoundInBondPayment: IsCreditCardIsFoundInBondPayment }, function (result) {
                     if (result == 'True') {
                         $("#PayButton").show()
                         $("#PayButton").attr('disabled', false);
                         $("#VerifyCard").hide()
                         $("#divVerification").hide();
                         $("#CancelButton").attr("disabled", false);
                         $("[id$=_HiddenIsOverride]").attr("value", "False")
                         //rev:mia dec 2 2010 - logged readcard event
                         LogReadCardEvent(rentalID)
                     }
                     else {


                         if (card[0].length == 0) {
                             DisplayGrowl(errCancel, 40)
                             $("#CancelButton").attr("disabled", false);
                             $("#VerifyCard").attr("disabled", false);
                             isVerifyclick = false;
                             $("[id$=_HiddenIsOverride]").attr("value", "False")
                             cardVerificationFinished = true
                             isVerifyclick = false
                             //rev:mia dec 2 2010 - logged readcard event
                             LogReadCardEvent(rentalID)
                             return;
                         } //if (card[0].length == 0) {

                         errMsg = errNotSame

                         if (jQuery.trim(card[0]) == basecard && !IsSameNameInCreditCard()) {
                             errMsg = cardNoisOkButCardNameIsNot; //'Card validation failed. CardHolder Name does not match.'
                         } //if(card[0] == basecard && !IsSameNameInCreditCard())

                         if (IsCardNumberMatch(card[0], basecard) == true && !IsSameNameInCreditCard()) {
                             errMsg = cardNoisOkButCardNameIsNot;  //'Card validation failed. CardHolder Name does not match.'
                         } //if (IsCardNumberMatch(card[0], basecard) == true && !IsSameNameInCreditCard()) {

                         if (DisplayCardMismatch == 'true' || DisplayCardMismatch == true) {
                             DisplayGrowl(errMsg, 40)
                             $("#divVerification").show().css("border", "1px solid gray")
                             $("#VerifyCard").attr('disabled', true)
                             $("#CancelButton").attr("disabled", true);
                             $('[id$=trReasonOtherVerified]').hide()
                             $("[id$=_HiddenIsOverride]").attr("value", "True")
                             if (typeofpayment == 'Refund') $("[id$=AmountTextbox]").attr("disabled", true)
                             //rev:mia dec 2 2010 - logged readcard event
                             LogReadCardEvent(rentalID)
                         } //if (DisplayCardMismatch == 'true' || DisplayCardMismatch == true && (HiddenType == 'R' || HiddenType == 'D'))

                     } //if (result) {
                 });
             }
             else {

                 if (IsCardNumberMatch(card[0], basecard) == true && IsSameNameInCreditCard())
                 //if (jQuery.trim(card[0]) == basecard && IsSameNameInCreditCard())
                 {
                     $("#PayButton").show()
                     $("#PayButton").attr('disabled', false);
                     $("#VerifyCard").hide()
                     $("#divVerification").hide();
                     $("#CancelButton").attr("disabled", false);
                     $("[id$=_HiddenIsOverride]").attr("value", "False")
                     //rev:mia dec 2 2010 - logged readcard event
                     LogReadCardEvent(rentalID)
                 }
                 else {
                     if (card[0].length == 0) {
                         DisplayGrowl(errCancel, 40)
                         $("#CancelButton").attr("disabled", false);
                         $("#VerifyCard").attr("disabled", false);
                         $("[id$=_HiddenIsOverride]").attr("value", "False")
                         isVerifyclick = false;
                         cardVerificationFinished = true
                         //rev:mia dec 2 2010 - logged readcard event
                         LogReadCardEvent(rentalID)
                         return;
                     }

                     errMsg = 'Card validation failed. Card Number does not match.'

                     if (jQuery.trim(card[0]) == basecard && !IsSameNameInCreditCard()) {
                         errMsg = cardNoisOkButCardNameIsNot //'Card validation failed. CardHolder Name does not match.'
                     } //if(card[0] == basecard && !IsSameNameInCreditCard())

                     if (IsCardNumberMatch(card[0], basecard) == true && !IsSameNameInCreditCard()) {
                         errMsg = cardNoisOkButCardNameIsNot //'Card validation failed. CardHolder Name does not match.'
                     } //if (IsCardNumberMatch(card[0], basecard) == true && !IsSameNameInCreditCard()) {

                     if (DisplayCardMismatch == 'true' || DisplayCardMismatch == true) {

                         if (HiddenType == 'R' || HiddenType == 'D') {
                             DisplayGrowl(errMsg, 40)
                             $("#divVerification").show().css("border", "1px solid gray")
                             $("#VerifyCard").attr('disabled', true)
                             $("#CancelButton").attr("disabled", true);
                             $('[id$=trReasonOtherVerified]').hide()
                             $("[id$=_HiddenIsOverride]").attr("value", "True")
                             if (typeofpayment == 'Refund') $("[id$=AmountTextbox]").attr("disabled", true)
                             //rev:mia dec 2 2010 - logged readcard event
                             LogReadCardEvent(rentalID)
                         }
                         else {

                         }

                     } //if (DisplayCardMismatch == 'true' || DisplayCardMismatch == true && (HiddenType == 'R' || HiddenType == 'D'))

                 }
             } //if (HiddenType == 'B') {

         }
     })
     isVerifyclick = false;
     cardVerificationFinished = true
}

//|------------------------------------------------------------------------------------|
//|get the last receipt                                                                |
//|------------------------------------------------------------------------------------|  

function LastReceipt()
{
    dpsEftX.DoGetLastReceipt()
}

function GetLastReceipt()
{
    PrintReceipt(dpsEftX.Receipt, $("[id$=HiddenFieldUserPrinter]").val())                 
}


//|------------------------------------------------------------------------------------|
//|function that will be called by the AuthorizedEvent. If DPStransaction is not empty |
//|and it its successful..data will be save or refunded                                | 
//|------------------------------------------------------------------------------------|

function DisplayResult()
{
   
    var resTemp = '';
    if ( ! dpsEftX.Success )
		{
				    $("#divResult").html("<label>Terminal Error: " + dpsEftX.ResponseText + "</label>")
		}else
		{
		
		    var typeofpayment    = $("[id$=_HiddenPurchase]").val()
		    var rentalID         = $("[id$=_HiddenRentalID]").attr("value") 
            var usercode         = $("[id$=_HiddenUsercode]").attr("value") 
            
            
            var Method            = '';
            var Name              = ''; 
            var details           = ''; 
            var Amount            = ''; 
            var LocalAmount       = ''; 
            var IsRemove          = ''; 
            var IntegrityNo       = ''; 
            var PmEntryPaymentId  = ''; 
            var PaymentDate       = ''; 
            var Status            = ''; 
            var PaymentEntryItemsStart = ''; 
            var AccountName       = ''; 
            var PmtPtmId          = ''; 
            var ApprovalRef       = ''; 
            var ExpiryDate        = ''; 
            var checkAndBank      = ''; 
            var AccountNo         = ''; 
            var PdtAmount         = ''; 
            var PdtIntegrityNo    = ''; 
            var PdtLocalAmount    = ''; 
            var PdtId             = ''; 
            var AuthCode          = ''; 
            var MerchRef          = ''; 
            var TxnRef            = ''; 
            var IsCrdPre          = ''; 
            var PaymentEntryItemsEnd = ''; 
            var LocalIdentifier      = ''; 
            var OriginalAmt          = ''; 
            var Paymentxml           = ''; 
            var PaymentOutstanding   = ''; 
            var CurrencyInfo         = ''; 
            var Currency         = ''; 
            var CurrencyDesc     = ''; 
            var PaymentEntryRoot ='';
            var xmlvalues        ='';
            
            var url         =   '';
            var xmlvalues   =   '';
            var SubMethod   =   '';
            var cardvalue   =   '';
            var surcharge   = 0;
            var surchargeparameter     = '';
            var HiddenRefundData       = '';
            var AmountsOutstandingRoot = '';
            var PdtDpsEfTxnRef         = '';
            var swipedcard             = '';
            var aryNameFromTrack1      = dpsEftX.OutputTrack1;
            var trace
            var typecode = $("[id$=_HiddenType]").val()

            // rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
            var PdtEFTPOSCardMethodDontMatchValue = '';
            var PdtEFTPOSCardMethodDontMatchElement = '';
            var PdtEFTPOSCardTypeSwipedValue = ''
            var PdtEFTPOSCardTypeSwipedElement = ''
            var PdtEFTPOSCardTypeSurchargeAddedValue = ''
            var PdtEFTPOSCardTypeSurchargeAddedElement = ''
            

            if(dpsEftX.DpsTxnRef.length == 0)
            {
                 $("#PayButton").attr("disabled", false);
                 $("#CancelButton").attr("disabled", false);
                 return;
             }

            swipedcard = $("div.modalPopup #creditcardSelect option:selected").attr("text").toUpperCase()
            var card = $("[id$=_ddlMethod] option:selected").attr("text")

            if(typeofpayment == "Payment")
            {		
    		            resTemp = "<br><b>DPS RESPONSE</b>"
		                +"<hr/>"			
			            +"<br>Merchant Reference:    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.MerchantReference			
			            +"<br>Response:              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.ResponseText
			            +"<br>DpsTxnRef:             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.DpsTxnRef
			            +"<br>TxnRef:                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.TxnRef
			            +"<br>CardType:              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.CardType
			            +"<br>CardHolder Name:       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + GetNameInCreditCard()
                        //+ "<br>Authorized:           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + dpsEftX.Authorized()
			            
            			
                        $("#divResult").html("<pre><label>" + resTemp + "</label></pre>");
                        
                        if(dpsEftX.Authorized || dpsEftX.Authorized == true)
                        {   
                            
                            amtWithSurcharge = $("#TotalText").val()
                            

                            //rev:mia nov 15 2010 - issue #113
                            //latest update....this should be implemented on all payment type
                            
                            if (dpsEftX.CardType == 'EFTPOS' && card != 'Debit Card' ) 
                            {
                                    cardvalue = swipedcard + '^EFTPOS@' + MethodValue;
                            } else {
                                cardvalue = dpsEftX.CardType + '@' + MethodValue;
                            }
                            
                            surchargeparameter = ""             //paymentid
                                                     + "@" 
                                                     + rentalID                          //RentID
                                                     + "@" 
                                                     + $("[id$=_HiddenType]").val()      //Type
                                                     + "@" 
                                                     + usercode                          // userCode
                                                     + "@" 
                                                     + $("#AmountTextbox").val()        //Amt 
                                                     //+ "@"
                                                     //+ submethod

                            additionalMessageForMethodType =  card
                            additionalMessageForSwipedCard =  swipedcard
                            additionalMessageForDPSCard    =  dpsEftX.CardType.toUpperCase() 

                           if(dpsEftX.CardType.toUpperCase() == swipedcard)
                           {
                                SubMethod    = '<SubMethod>' + $("div.modalPopup #creditcardSelect").attr("value") + '</SubMethod>';//<SubMethod>F0A669BF-EFB3-4FCE-81E7-5CB510903500</SubMethod> 
                                Method       = '<Method>Credit Card - ' + dpsEftX.CardType + '</Method>'
                                //alert("CREDIT TO CREDIT SWIPED MATCH: " + swipedcard + " = " + dpsEftX.CardType.toUpperCase())
                                additionalMessage = "Swiped cardname and DPS cardname are equal"
                           }
                           else
                           {
                               if(DisplayCardMismatch == 'true' || DisplayCardMismatch == true)
                               {
                                   
                                   if(dpsEftX.CardType.toUpperCase() == "DEBIT" &&  swipedcard == "EFT-POS") {
                                   }
                                   else {
                                       //sept.27, surprised from DPS, we should use EFTPOS not DEBIT as returned value 
                                       //Confirming that this value will read “EFTPOS” whenever a card is used and “Chq” or “Sav” accounts are selected on the PINpad.
                                       if (dpsEftX.CardType.toUpperCase() == "EFTPOS" && swipedcard == "EFT-POS") {
                                       }
                                       else {
                                           if (dpsEftX.CardType.toUpperCase() == "EFTPOS" && card != 'Debit Card') {
                                               DisplayGrowl(chkOrSaveErrorInEftpos, 130)
                                           }
                                           else {
                                               DisplayGrowl('This is a <b>' + dpsEftX.CardType + '</b> and not <b>' + swipedcard + '.</b> Correct surcharge will be apply!', 130)
                                           }
                                       }
                                   }
                                   
                               }//if(DisplayCardMismatch == 'true' || DisplayCardMismatch == true)



                               $('div.modalPopup #creditcardSelect option').each(function (index, option) {

                                   //after selecting the wrong credit card, program will loop to the dropdown
                                   //to check the correct credit card
                                   if (option.text.toUpperCase() == dpsEftX.CardType.toUpperCase()) {
                                       //alert("CREDIT TO CREDIT SEARCHING: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase())
                                       SubMethod = '<SubMethod>' + option.value + '</SubMethod>';
                                       Method = '<Method>Credit Card - ' + dpsEftX.CardType + '</Method>'
                                       Surcharge(option.value)
                                       amtWithSurcharge = $("#TotalText").val()
                                       additionalMessage = "CREDIT TO CREDIT SEARCHING: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase()
                                   }
                                   else {

                                       //EFT-POS AND DEBIT CARD
                                       //EFTPOS was selected and DEBIT card was used.
                                       //sept.27, surprised from DPS, we should use EFTPOS not DEBIT as returned value 
                                       if (option.text.toUpperCase() == "EFT-POS" && (dpsEftX.CardType.toUpperCase() == "DEBIT" || dpsEftX.CardType.toUpperCase() == "EFTPOS")) {
                                           //alert("DEBIT TO DEBIT SWIPED MATCH: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase())
                                           SubMethod = '<SubMethod>' + option.value + '</SubMethod>'
                                           Method = '<Method>Debit Card - EFT-POS</Method>'
                                           additionalMessage = "DEBIT TO DEBIT SWIPED MATCH: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase()
                                       }
                                       else {
                                           //EFTPOS WAS SELECTED BUT SWIPE WRONG CARD TYPE..ANY CREDITS
                                           //sept.27, surprised from DPS, we should use EFTPOS not DEBIT as returned value 
                                           if (option.text.toUpperCase() == "EFT-POS" && (dpsEftX.CardType.toUpperCase() != "DEBIT" || dpsEftX.CardType.toUpperCase() != "EFTPOS")) {
                                               $('#CardTest option').each(function (index, option) {
                                                   if (option.text.toUpperCase() == dpsEftX.CardType.toUpperCase()) {

                                                       // alert("DEBIT TO CREDIT SEARCH: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase())
                                                       //change the MethodValue element so it will have id of the credit card and not debit
                                                       MethodValue = "<MethodValue>" + cardValueOfCreditCard + "</MethodValue>"
                                                       SubMethod = '<SubMethod>' + option.value + '</SubMethod>'

                                                       //instead of naming this as debit...change this to Credit card since its not debit
                                                       Method = '<Method>Credit Card - ' + dpsEftX.CardType + '</Method>'
                                                       if (card == "Debit Card") {
                                                           PdtEFTPOSCardMethodDontMatchValue = "1"
                                                           PdtEFTPOSCardTypeSwipedValue = swipedcard + "~Debit Card~" + dpsEftX.CardType.toUpperCase()
                                                       }
                                                       additionalMessage = "DEBIT TO CREDIT SEARCH: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase()
                                                   }
                                               });
                                           }
                                           else {
                                               //CREDIT CARD BUT SWIPED A DEBIT CARD 
                                               //sept.27, surprised from DPS, we should use EFTPOS not DEBIT as returned value 
                                               if (option.text.toUpperCase() != "EFT-POS" && (dpsEftX.CardType.toUpperCase() == "DEBIT" || dpsEftX.CardType.toUpperCase() == "EFTPOS")) {
                                                   $('#CardTest option').each(function (index, option) {
                                                       if (option.text.toUpperCase() == "EFT-POS") {
                                                           //alert("CREDIT TO DEBIT SEARCH: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase())
                                                           MethodValue = "<MethodValue>" + cardValueOfCreditCard + "</MethodValue>"
                                                           SubMethod = '<SubMethod>' + option.value + '</SubMethod>'
                                                           Method = '<Method>Debit Card - EFT-POS</Method>'

                                                           //since EFTPOS has no Surcharge, lets change it to default value
                                                           //#113 - surcharge will now be handled in the library
                                                           amtWithSurcharge = $("#AmountTextbox").val();
                                                           if (card == "Credit Card") {
                                                               PdtEFTPOSCardMethodDontMatchValue = "1"
                                                               PdtEFTPOSCardTypeSwipedValue = swipedcard + "~Credit Card~" + dpsEftX.CardType.toUpperCase()
                                                               if($("#SurchargeText").val() != 'N/A') PdtEFTPOSCardTypeSurchargeAddedValue = $("#SurchargeText").val()
                                                               else PdtEFTPOSCardTypeSurchargeAddedValue = 0
                                                           }
                                                           additionalMessage = "CREDIT TO DEBIT SEARCH: " + option.text.toUpperCase() + " = " + dpsEftX.CardType.toUpperCase()
                                                       }
                                                   });

                                               } //if(option.text.toUpperCase() != "EFT-POS" && dpsEftX.CardType.toUpperCase() == "DEBIT" )

                                           } //if(option.text.toUpperCase() == "EFT-POS" && dpsEftX.CardType.toUpperCase != "DEBIT" )

                                       } //if(option.text.toUpperCase() == "EFT-POS" && dpsEftX.CardType.toUpperCase == "DEBIT" )

                                   }
                               });
                           }//if(dpsEftX.CardType.toUpperCase() == swipedcard)
                            
                             
                                    
                             //Method            = '<Method>Credit Card - ' + dpsEftX.CardType + '</Method>'
                             var track1        = '';       
                             Name              =  $("#nameText").val().toUpperCase()
                             
                             //anticipating of possible removal of Name
                             if(aryNameFromTrack1.length != 0)
                             {
                                track1            =  aryNameFromTrack1.split("^")
                                if(track1.length != 0)
                                {
                                    Name = track1[1];
                                    Name = jQuery.trim(Name)
                                    $("#nameText").attr("value",Name);
                                }
                             }//if(aryNameFromTrack1.length != 0)


                            //trapped missing element
                            //rev:mia Dec 1, 2010 - slowing of script drop the value of the ddlmethod..
                            if (MethodValue.length = 0) {
                                additionalMessage = additionalMessage + " , " + MethodValue + " has a problem"
                                MethodValue = "<MethodValue>" + $('[id$=_ddlMethod]').attr("value") + "</MethodValue>"
                                additionalMessage = additionalMessage + " , " + MethodValue + " was fixed"
                                 
                             }
                             else {
                                 //additionalMessage
                                 if (MethodValue.substring(0, 14) == '<MethodValue> ' || MethodValue.substring(0, 14) == '<MethodValue><') {
                                     additionalMessage = additionalMessage + " , " + MethodValue + " has a problem"
                                     MethodValue = "<MethodValue>" + $('[id$=_ddlMethod]').attr("value") + "</MethodValue>"
                                     additionalMessage = additionalMessage + " , " + MethodValue + " was fixed"
                                 }
                                 else {
                                     if (MethodValue.indexOf("-") == -1) {
                                         additionalMessage = additionalMessage + " , " + MethodValue + " has a problem"
                                         MethodValue = "<MethodValue>" + $('[id$=_ddlMethod]').attr("value") + "</MethodValue>"
                                         additionalMessage = additionalMessage + " , " + MethodValue + " was fixed"
                                     }
                                     if (MethodValue.length < 63) {
                                         additionalMessage = additionalMessage + " , " + MethodValue + " has a problem"
                                         MethodValue = "<MethodValue>" + $('[id$=_ddlMethod]').attr("value") + "</MethodValue>"
                                         additionalMessage = additionalMessage + " , " + MethodValue + " was fixed"
                                     }
                                 }

                             }
                             
                             if (amtWithSurcharge == 'N/A') amtWithSurcharge = "0.00"
                             details            = "<Details>" + Name + "," + getOutputTrack2() + '</Details>' //dpsEftX.CardNumber was replace by getOutputTrack2()
                             Amount            = "<Amount>"      + amtWithSurcharge + "</Amount>"; 
                             LocalAmount       = "<LocalAmount>" + amtWithSurcharge + "</LocalAmount>"; 
                             IsRemove          = "<IsRemove>0</IsRemove>"
                             IntegrityNo       = "<IntegrityNo>0</IntegrityNo>"
                             PmEntryPaymentId  = "<PmEntryPaymentId></PmEntryPaymentId>"
                             PaymentDate       = "<PaymentDate></PaymentDate>"
                             Status            = "<Status></Status>"
                             PaymentEntryItemsStart = "<PaymentEntryItems><PmtItem>"
                             AccountName       = "<AccountName>" + Name + "</AccountName>"
                             PmtPtmId          = "<PmtPtmId></PmtPtmId>"
                             ApprovalRef       = "<ApprovalRef>" + $("#ApprovalReferenceText").val().toUpperCase()  + "</ApprovalRef>"
                             ExpiryDate        =  dpsEftX.DateExpiry.substr(0,2) + "/" + dpsEftX.DateExpiry.substr(2)
                             ExpiryDate        = "<ExpiryDate>"  + ExpiryDate + "</ExpiryDate>"
                             checkAndBank      = "<ChequeNo></ChequeNo><Bank></Bank><Branch></Branch>"
                             AccountNo         = "<AccountNo>" + getOutputTrack2() + "</AccountNo>" //dpsEftX.CardNumber
                             PdtAmount         = "<PdtAmount>"   + amtWithSurcharge + "</PdtAmount>";
                             PdtIntegrityNo = "<PdtIntegrityNo>0</PdtIntegrityNo>"

                             //rev:mia nov 15 2010 issue #113
                             //PdtLocalAmount    = "<PdtLocalAmount>" + $("#AmountTextbox").val() + "</PdtLocalAmount>"
                             PdtLocalAmount = "<PdtLocalAmount>" + amtWithSurcharge + "</PdtLocalAmount>"

                             PdtId             = "<PdtId></PdtId>"
                             AuthCode          = "<AuthCode>" + dpsEftX.AuthCode + "</AuthCode>"
                             MerchRef          = "<MerchRef>" + dpsEftX.MerchantReference + "</MerchRef>"
                             TxnRef            = "<TxnRef></TxnRef>"
                             PdtDpsEfTxnRef    = "<PdtDpsEfTxnRef>" + dpsEftX.DpsTxnRef + "</PdtDpsEfTxnRef>"
                             IsCrdPre          = "<IsCrdPre>1</IsCrdPre>"

                             //rev:mia PdtEFTPOSCardMethodDontMatch -- Nov 22 2010
                             PdtEFTPOSCardMethodDontMatchElement    = "<PdtEFTPOSCardMethodDontMatch>" + PdtEFTPOSCardMethodDontMatchValue + "</PdtEFTPOSCardMethodDontMatch>"
                             PdtEFTPOSCardTypeSwipedElement         = "<PdtEFTPOSCardTypeSwiped>" + PdtEFTPOSCardTypeSwipedValue + "</PdtEFTPOSCardTypeSwiped>"
                             PdtEFTPOSCardTypeSurchargeAddedElement = "<PdtEFTPOSCardTypeSurchargeAdded>" + PdtEFTPOSCardTypeSurchargeAddedValue + "</PdtEFTPOSCardTypeSurchargeAdded>"

                             PaymentEntryItemsEnd = "</PmtItem></PaymentEntryItems>"
                             LocalIdentifier      = "<LocalIdentifier>create a function</LocalIdentifier>"
                             OriginalAmt          = "<OriginalAmt>" + $("#AmountTextbox").val() + "</OriginalAmt>"; 
                             Paymentxml           = $("[id$=_hiddenPaymentxml]").attr("value") + "|" 
            	             PaymentOutstanding   = $("[id$=_HiddenPaymentOutstanding]").attr("value")
                             CurrencyInfo         = $("[id$=_HiddenCurrency]").attr("value").split("|")
                             Currency       = '<Currency>' + CurrencyInfo[0] + '</Currency>';
                             CurrencyDesc   = '<CurrencyDesc>' + CurrencyInfo[1] + '</CurrencyDesc>';//<CurrencyDesc>NZD</CurrencyDesc>
                           
                        	
            	            PaymentEntryRoot = "<PaymentEntryRoot><PaymentEntry id='0'>" 
            	                                   + MethodValue  
            	                                   + SubMethod
            	                                   + Method 
            	                                   + details
            	                                   + Amount
            	                                   + LocalAmount
            	                                   + Currency
            	                                   + CurrencyDesc
            	                                   + IsRemove
            	                                   + IntegrityNo
            	                                   + PmEntryPaymentId
            	                                   + PaymentDate
            	                                   + Status
            	                                   + PaymentEntryItemsStart
            	                                   + AccountName
            	                                   + PmtPtmId
            	                                   + ApprovalRef
            	                                   + ExpiryDate 
            	                                   + checkAndBank
            	                                   + AccountNo
            	                                   + PdtAmount
            	                                   + PdtIntegrityNo
            	                                   + PdtLocalAmount
            	                                   + PdtId
            	                                   + AuthCode
            	                                   + MerchRef
            	                                   + TxnRef
            	                                   + PdtDpsEfTxnRef
            	                                   + IsCrdPre + PdtEFTPOSCardMethodDontMatchElement + PdtEFTPOSCardTypeSwipedElement + PdtEFTPOSCardTypeSurchargeAddedElement
            	                                   + PaymentEntryItemsEnd
            	                                   + LocalIdentifier
            	                                   + OriginalAmt
            	                                   + "</PaymentEntry></PaymentEntryRoot>"

            	       

			                xmlvalues = Paymentxml + PaymentEntryRoot + PaymentOutstanding + "|" + cardvalue + "|" + surchargeparameter + "|" + amtWithSurcharge + "@" + $("#AmountTextbox").val()

			                var userinfo = 'N/A'
			                var reasonid = $("[id$=_ddlReasonVerified]").val()
			                if (reasonid == null) reasonid = -1
			                if (isNaN(reasonid)) reasonid = -1

			                var currentid = $('[id=logonTextBoxVerified]').val()
			                var reasonother = $('[id$=_ReasonOtherTextBoxVerified]').val()
			                if (currentid == null || currentid.length == 0) {
			                    currentid = $('[id$=_HiddenUsercode]').val()
			                }

			                if (reasonid.length > 0 && reasonother.length > 0) {
			                    userinfo = currentid +
                                            '|' + reasonid +
                                            '|' + reasonother
			                    xmlvalues = xmlvalues + "|" + userinfo
                               
                            }
                            
			                url = "EFTPOSListener/EftposListener.aspx?mode=Payment"
			                $.post(url, { Payment: escape(xmlvalues) }, function (result) {

			                    trace = "SAVEPAYMENT:" + result + '\n'

			                    if (result.indexOf('ERROR') != -1) {
			                        $("#PayButton").attr("disabled", false);
			                        $("#CancelButton").attr("disabled", false);
			                        $("#divResult").html("<pre><label>" + resTemp + "<br/>" + result + "</label></pre>");
			                    }
			                    else {
			                        //save the receipt
			                        var prevResult = result.split("|");
			                        url = "EFTPOSListener/EftposListener.aspx?mode=Receipt"

			                        xmlvalues = prevResult[1] + "|" + dpsEftX.DpsTxnRef + "|" + usercode + "[eof]" + dpsEftX.Receipt
			                        $.post(url, { Receipt: xmlvalues }, function (result) {
			                            if (result == 'OK') {
			                                trace = trace + "RECEIPT:" + result + '\n '
			                                PrintAnotherCopy()

			                                $("#PayButton").attr("disabled", true);

			                                navigateurl = prevResult[0];
			                                trace = trace + "URL:" + navigateurl + '\nCard Type:' + dpsEftX.CardType.toUpperCase()

			                                TraceError(usercode, trace)

			                                url = "EFTPOSListener/EftposListener.aspx?mode=ACTIVEX"
			                                $.post(url, { ACTIVEX: LogActiveXControl(rentalID) }, function (result) {
			                                    if (result == 'OK') {
			                                        setTimeout(window.location.href = prevResult[0], 5000);
			                                    }
			                                });
			                            }
			                        });
			                    }
			                });    
                        }     
                        else
                        {
                             $("#PayButton").attr("disabled", false);
                             $("#CancelButton").attr("disabled", false);
                        }
            }else
            {
             
                
                resTemp = "<br><b>DPS RESPONSE</b>"
		               +"<hr/>"			
			            +"<br>Merchant Reference:    &nbsp;&nbsp;&nbsp;&nbsp;"       +  dpsEftX.MerchantReference			
			            +"<br>Response:              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.ResponseText
			            +"<br>DpsTxnRef:             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.DpsTxnRef
			            +"<br>TxnRef:                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.TxnRef
			            +"<br>CardType:              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  dpsEftX.CardType
			            +"<br>CardHolderName:        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +  GetNameInCreditCard()
			            
                        $("#divResult").html("<pre><label>" + resTemp + "</label></pre>");
                        
                if(!dpsEftX.Authorized)
                {
                    $("#PayButton").attr("disabled", false);
                    $("#CancelButton").attr("disabled", false);
                    return;
                }

                var HiddenFieldPdtEFTPOSCardMethodDontMatch = $('[id$=_HiddenFieldPdtEFTPOSCardMethodDontMatch]').val()
                var HiddenFieldPdtEFTPOSCardTypeSurchargeAdded = $('[id$=_HiddenFieldPdtEFTPOSCardTypeSurchargeAdded]').val()
                var HiddenFieldPdtEFTPOSCardTypeSwiped = $('[id$=_HiddenFieldPdtEFTPOSCardTypeSwiped]').val()
                if (HiddenFieldPdtEFTPOSCardMethodDontMatch == '1') {
                    //credit card is the method but swiped eftpos
                    if (parseFloat(HiddenFieldPdtEFTPOSCardTypeSurchargeAdded) > 0) {
                        additionalMessage = "When PAYMENT was created, Method Type selected was Credit Card ( " + HiddenFieldPdtEFTPOSCardTypeSwiped + " ) but user/client swiped EFTPOS. This created a surcharge of $" + parseFloat(HiddenFieldPdtEFTPOSCardTypeSurchargeAdded) + " and it will not be included in future REFUND."
                    }
                    //debit card is the method but swiped visa or non eftpos
                    if (HiddenFieldPdtEFTPOSCardTypeSurchargeAdded == '') {
                        additionalMessage = "When PAYMENT was created, Method Type selected was Debit Card ( " + HiddenFieldPdtEFTPOSCardTypeSwiped + " ) but user/client swiped non-EFTPOS card.."
                    }
                    
                }

                additionalMessageForMethodType = card
                additionalMessageForSwipedCard = swipedcard
                additionalMessageForDPSCard = dpsEftX.CardType.toUpperCase() 



                Paymentxml           = $("[id$=_hiddenPaymentxml]").attr("value")            + "|" 
                HiddenRefundData     = $("[id$=_HiddenRefundData]").attr("value")            + "|" 
                AmountsOutstandingRoot = "<AmountsOutstandingRoot></AmountsOutstandingRoot>" + "|"
                $("#nameText").attr("value",GetNameInCreditCard());
                
                var  userinfo  = 'N/A'
                var  reasonid  =   $("[id$=_ddlReasonVerified]").val()
                     if(reasonid == null)    reasonid = -1
                     if(isNaN(reasonid))     reasonid = -1
                     
                var  currentid   = $('[id=logonTextBoxVerified]').val()
                var  reasonother = $('[id$=_ReasonOtherTextBoxVerified]').val()
                     if(currentid == null || currentid.length == 0) 
                      {
                         currentid      = $('[id$=_HiddenUsercode]').val()
                         //reasonother    = "Note: Refund"
                      }
                     
                     userinfo = currentid               + 
                               '|' + reasonid           +
                               '|' + reasonother
                               
           
                xmlvalues = Paymentxml                          + 
                            HiddenRefundData                    + 
                            AmountsOutstandingRoot              + 
                            usercode                            + "|" + 
                            $("#TotalText").val()               + "|" + 
                            $("#AmountTextbox").val()           + "|" + 
                            $("[id$=_HiddenType]").val()        + '|' + 
                            dpsEftX.DpsTxnRef + "|"             +
                            userinfo                            + "|" +            //data will save in PaymentOverrideReasons
                            getOutputTrack2()                   + "|" +            //dpsEftX.CardNumber was replaced by getOutputTrack2()
                            dpsEftX.CardType                    + "|" +
                            GetNameInCreditCard()

                var origAmt = $('[id$=_HiddenFieldAmount]').val()
                var amtText = $("#AmountTextbox").val()

                var amtQS;
                if (parseFloat(origAmt) != parseFloat(amtText)) {
                    amtQS = "&isPaymentChange=True"
                }
                else {
                    amtQS = "&isPaymentChange=False"
                }
                url = "EFTPOSListener/EftposListener.aspx?mode=Refund&IsOveride=" + $("[id$=_HiddenIsOverride]").val() + amtQS
                $.post(url, { Refund: xmlvalues }, function (result) {
                    trace = "REFUNDFPAYMENT:" + result + '\n' 
			                        if(result.indexOf('ERROR') != -1)
                                        {
                                            $("#PayButton").attr("disabled", false);
                                            $("#CancelButton").attr("disabled", false);
                                            $("#divResult").html("<pre><label>" + resTemp + "<br/>" + result + "</label></pre>");
                                        }    
                                    else 
                                        {
                                            var prevResult = result.split("|");
                                            url = "EFTPOSListener/EftposListener.aspx?mode=Receipt"
                                            
                                            xmlvalues = prevResult[1]  + "|" + dpsEftX.DpsTxnRef + "|" + usercode +  "[eof]"   + dpsEftX.Receipt

                                            $.post(url, { Receipt: xmlvalues }, function (result) {
                                                trace = trace + "RECEIPT:" + result + '\n' 
                                                   if(result == 'OK') {
                                                       PrintAnotherCopy()

                                                        $("#PayButton").attr("disabled", true);
                                                        navigateurl = prevResult[0];

                                                        trace = trace + "URL:" + navigateurl + '\nCard Type:' + dpsEftX.CardType.toUpperCase()
                                                        TraceError(usercode, trace)

                                                        url = "EFTPOSListener/EftposListener.aspx?mode=ACTIVEX"
                                                        $.post(url, { ACTIVEX: LogActiveXControl(rentalID) }, function (result) {
                                                            if (result == 'OK') {
                                                                setTimeout(window.location.href = prevResult[0], 5000);
                                                            }
                                                        }); //$.post(url, { ACTIVEX: LogActiveXControl(rentalID) }, function (result) {

                                                   }
                                            });
                                            
                                        }//if(result.indexOf('ERROR') != -1)
                                      
                              });    
            }  
        }
}

function ValidateAmount() {
    
    var HiddenFieldPdtEFTPOSCardTypeSurchargeAdded = $('[id$=_HiddenFieldPdtEFTPOSCardTypeSurchargeAdded]').val()
    var HiddenFieldPdtEFTPOSCardTypeSwiped = $('[id$=_HiddenFieldPdtEFTPOSCardTypeSwiped]').val()
    var HiddenFieldPdtEFTPOSCardMethodDontMatch = $('[id$=_HiddenFieldPdtEFTPOSCardMethodDontMatch]').val()
    var mode = $("[id$=_HiddenMode]").attr("value")
    var HiddenType = $("[id$=_HiddenType]").val()
    var surcharge = ''
    var amountTextbox = $("#AmountTextbox").val()

    if (HiddenFieldPdtEFTPOSCardTypeSwiped.length > 0 && mode == '1' && HiddenFieldPdtEFTPOSCardMethodDontMatch == '1') {

        var selectedCard = HiddenFieldPdtEFTPOSCardTypeSwiped.split("~")[0] //actual selected card in the popup
        var method = HiddenFieldPdtEFTPOSCardTypeSwiped.split("~")[1] //will determine if its debit card or credit card
        var actualcard = HiddenFieldPdtEFTPOSCardTypeSwiped.split("~")[2] // will determine if its eftpos or any type of card

        
        if (HiddenType == 'R' || HiddenType == 'D') {
            //CREDIT CARD - VISA|MASTERCARD - EFTPOS
            if (method == 'Credit Card' && (actualcard == 'EFTPOS' || actualcard == 'EFT-POS') && (selectedCard != 'EFTPOS' && selectedCard != 'EFT-POS')) {
                amountTextbox = parseFloat(amountTextbox) - parseFloat(HiddenFieldPdtEFTPOSCardTypeSurchargeAdded)
            }
            //DEBIT CARD - EFT-POS - VISA|MASTERCARD
            if (method == 'Debit Card' && (actualcard != 'EFTPOS' && actualcard != 'EFT-POS') && (selectedCard == 'EFT-POS' || selectedCard == 'EFTPOS')) {
                amountTextbox = parseFloat(amountTextbox) - parseFloat(HiddenFieldPdtEFTPOSCardTypeSurchargeAdded)
            }
        }
    }
    $("#AmountTextbox").attr("text",amountTextbox)
}


//will retrieve the surcharge amount mapped to a certain credit cards
function Surcharge(submethod)
{
    if(submethod.length == 0) return;
    var isNegative = $("#AmountTextbox").val().indexOf("-") == 0
    
    var rentalID = $("[id$=_HiddenRentalID]").attr("value") 
                var usercode =  $("[id$=_HiddenUsercode]").attr("value") 
                var cardvalue = dpsEftX.CardType + '@' + MethodValue;
                var surchargeparameter = ""         //paymentid
                                         + "@" 
                                         + rentalID //RentID
                                         + "@" 
                                         + $("[id$=_HiddenType]").val()      //Type
                                         + "@" 
                                         + usercode // userCode
                                         + "@" 
                                         + Math.abs($("#AmountTextbox").val()) //Amt 
                                         + "@"
                                         + submethod
    var amt             =0;
    var surcharge       =0;
    var tempsurcharge   =0;
    var currentsurcharge=0;

    //rev:mia november 22 2010
    var HiddenFieldPdtEFTPOSCardMethodDontMatch = $('[id$=_HiddenFieldPdtEFTPOSCardMethodDontMatch]').val()
    var mode = $("[id$=_HiddenMode]").attr("value")
    var HiddenType = $("[id$=_HiddenType]").val()
    
    var    url = "EFTPOSListener/EftposListener.aspx?mode=Surcharge"
    $.post(url, { Surcharge: escape(surchargeparameter) }, function (result) {



        //rev:mia november 22 2010
        //this will check if its refund and type "B".
        //if HiddenFieldPdtEFTPOSCardMethodDontMatch is 1 then payment was created by selecting debitcard method and swiping non-eftpos card
        //we should not add surcharge since when doing payment, we forced it not to include this surcharge
        var HiddenFieldPdtEFTPOSCardTypeSurchargeAdded = $('[id$=_HiddenFieldPdtEFTPOSCardTypeSurchargeAdded]').val()
        var HiddenFieldPdtEFTPOSCardTypeSwiped = $('[id$=_HiddenFieldPdtEFTPOSCardTypeSwiped]').val()

        surcharge = parseFloat(result);

        if (HiddenFieldPdtEFTPOSCardTypeSwiped.length > 0 && mode == '1' && HiddenFieldPdtEFTPOSCardMethodDontMatch == '1') {

            var selectedCard = HiddenFieldPdtEFTPOSCardTypeSwiped.split("~")[0] //actual selected card in the popup
            var method = HiddenFieldPdtEFTPOSCardTypeSwiped.split("~")[1] //will determine if its debit card or credit card
            var actualcard = HiddenFieldPdtEFTPOSCardTypeSwiped.split("~")[2] // will determine if its eftpos or any type of card

            if (HiddenType == 'B') {

                //if its credit card and the actual card is eftpos then combination is
                //CREDIT CARD - VISA|MASTERCARD - EFTPOS
                if (method == 'Credit Card' && (actualcard == 'EFTPOS' || actualcard == 'EFT-POS') && (selectedCard != 'EFTPOS' && selectedCard != 'EFT-POS')) {
                        surcharge = parseFloat("0.00");

                }

                //DEBIT CARD - EFT-POS - VISA|MASTERCARD
                if (method == 'Debit Card' && (actualcard != 'EFTPOS' && actualcard != 'EFT-POS') && (selectedCard == 'EFT-POS' || selectedCard == 'EFTPOS')) {
                    surcharge = parseFloat('0.00')
                }
            } //if (HiddenType == 'B') {

            if (HiddenType == 'R' || HiddenType == 'D') {
                    //CREDIT CARD - VISA|MASTERCARD - EFTPOS
                     if (method == 'Credit Card' && (actualcard == 'EFTPOS' || actualcard == 'EFT-POS') && (selectedCard != 'EFTPOS' && selectedCard != 'EFT-POS')) {
                                surcharge = parseFloat('0.00')
                     }
                     //DEBIT CARD - EFT-POS - VISA|MASTERCARD
                     if (method == 'Debit Card' && (actualcard != 'EFTPOS' && actualcard != 'EFT-POS') && (selectedCard == 'EFT-POS' || selectedCard == 'EFTPOS')) {
                            surcharge = parseFloat('0.00')
                     }
            }
        }
        else
        { surcharge = parseFloat(result); }

        amt = $("[id$=AmountTextbox]").val();
        if (!isNegative) {
            currentsurcharge = parseFloat(amt) + parseFloat(surcharge)
            $("[id$=SurchargeText]").attr("value", CurrencyFormatted(surcharge));
            $("[id$=TotalText]").attr("value", CurrencyFormatted(currentsurcharge))
        }
        else {

            if ($("[id$=_HiddenType]").val() == "B") {
                surcharge = parseFloat(result);
                currentsurcharge = parseFloat(amt) + parseFloat(-surcharge)
                $("[id$=SurchargeText]").attr("value", CurrencyFormatted(-surcharge));
                $("[id$=TotalText]").attr("value", CurrencyFormatted(currentsurcharge))
            }
            else {

                $("[id$=SurchargeText]").attr("value", "N/A")
                $("[id$=TotalText]").attr("value", "N/A")

            } //if ($("[id$=_HiddenType]").val() == "B") {

        } //if(!isNegative)

        //$("[id$=TotalText]").attr("value", CurrencyFormatted(currentsurcharge))

    });
            
}


//this will reset the data for Payment Processing
function CancelOK()
{
    var typeofpayment = document.getElementById('ctl00_ContentPlaceHolder_HiddenPurchase');
    if(typeofpayment.value == "Refund")
    {
     $("#divResult").html("")
     $("#divResult").css("border","")
     return;
    } 
    
    $("#creditcardSelect").val("");
    $("#nameText").attr("value","")
    $("#ApprovalReferenceText").attr("value","")
    $("[id$=SurchargeText]").attr("value","");
    $("[id$=TotalText]").attr("value","")
    $("[id$=AmountTextbox]").attr("value",$("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val());      
    $("#divResult").html("<label></label>")
}

//check if pinpad and dps is online and very much active
function StatusChecking() {
    overallstatus = dpsEftX.Ready;
    DisableButtonWhenItsImprint()
}


function DisableButtonWhenItsImprint() {
    if ($('[id$=_HiddenMode]').val() == '1') {
        if ($('[id$=_ddlMethod] option:selected').attr("text") == 'Imprint') {
            $('[id$=_btnEFTPOS]').attr("disabled", true).attr('class', '').attr('class', 'Button_Standard submitdisabled')
            $('[id$=_ddlCardType]').attr("disabled", true)
        }

        if ($('[id$=_ddlMethod] option:selected').attr("text") == 'Debit Card') {
            return
        }

        if ($('[id$=_ddlMethod] option:selected').attr("text") == 'Credit Card') {
            return
        }

        
         $('[id$=_btnEFTPOS]').attr("disabled", true).attr('class', '').attr('class', 'Button_Standard submitdisabled')
         $('[id$=_ddlCardType]').attr("disabled", true)
        

    }
    else {
        if ($('[id$=_ddlMethod] option:selected').attr("text") == 'Imprint') {
            $('[id$=_ddlCardType]').attr("disabled", "disabled")
        }
    }
    
}
//not use
function ModalRefund(){  var modal = $find('ctl00_ContentPlaceHolder_ModalPopupExtender1');    modal.show(); }


function NotReady()
{
        var imgok    = "../IMAGES/tick_green.gif"
        var imgerror = "../IMAGES/rental_error.gif"
        var imgCard = "../IMAGES/creditcards.ico"
                                               
        if(dpsEftX.Ready) 
        {   
            if(dpsEftX.ReadyPinPad) $("#imgpinpad").attr("src",imgok)
             else $("#imgpinpad").attr("src",imgerror)
                                        
             if(dpsEftX.ReadyLink) $("#imglink").attr("src",imgok)
             else $("#imglink").attr("src",imgerror)

             $("#imgCredit").attr("src", imgCard)

              $("div.modalPopup #PayButton").attr("disabled", false);     
        }
        else
        {
             $("#imgpinpad").attr("src",imgerror);
             $("#imglink").attr("src", imgerror);
             $("#imgCredit").attr("src", imgCard)
             $("div.modalPopup #PayButton").attr("disabled", true);     
        }                            
        
        $("#tdstatustext").html("<b>" + dpsEftX.StatusText + "</b>")
}



function PrintReceipt(receipt,usercode)
{
           
            var    url = "EFTPOSListener/EftposListener.aspx?mode=PrintReceipt&usercode=" + usercode
            $.post(url,{PrintReceipt: receipt},function(result) {
                     if(result.indexOf('ERROR') != -1)
                     alert(result);

            });
}

function overridePassword(userinfo)
{
            var    url = "EFTPOSListener/EftposListener.aspx?mode=RefundOverridePassword"
            $.post(url,{RefundOverridePassword: userinfo },function(result) {
                    var msg = "<label><b>" + result + "</b></label><br/>"
		
		            if(result.indexOf('not') != -1 || result.indexOf('Invalid') != -1) $("#divResult").html(msg)
                    else
                    {
                         $("#PayButton").attr("disabled", false).show();
                         $("#divResult").html("")
                         $("#divVerification").hide();
                         $("#VerifyCard").hide()
                         $("#CancelButton").attr("disabled", false);
                         $("[id$=_HiddenUsercode]").attr("value", $('[id=logonTextBoxVerified]').val())
                    }     
            });
}

function logOverridePasswordToHistory() {
    var url = "EFTPOSListener/EftposListener.aspx?mode=logOverridePasswordToHistory"
    var history = $("[id$=_HiddenRentalID]").attr("value") + "|" + $("[id$=_HiddenUsercode]").attr("value") + "|EFTPOS OVERRIDE"
    $.post(url, { logOverridePasswordToHistory: history }, function (result) {
    });
}

function DisplayGrowl(msg, mytop) {
    //errorIconOnSQL

    if (msg != "") {
        if (msg.indexOf('ERROR') != -1) {
            msg = errorIconOnSQL + msg
        }
        $("#divResult").html("<label>" + msg + "</label>")//.css("border", "1px solid")
    }
    else $("#divResult").html("<label></label>")
    return;
    //skip rest of the code
            var left  = $("div.modalPopup").position().left
            var width = $("div.modalPopup").width();
            var top =   $("div.modalPopup").position().top
         
            var msgCard = '<h4>' + msg  + '</h4>'   
             $.blockUI({ 
                message: msgCard,
                centerY: 0,
                centerX: 1
                ,css: { 
                        top:        '50px',//'160px',//(top - mytop) + 'px',//'160px', 
                        left:       '0px',//'630px', 
                        right:      '',
                        textAlign:  'center',
                        cursor:     'wait',
                        width:      width + 'px' } 
             }); 
             setTimeout($.unblockUI, 2000); 
}


function IsSameNameInCreditCard()
{
 var track1                 = '';       
 var aryNameFromTrack1      = dpsEftX.OutputTrack1;
 var Name                   =  $("#nameText").val()
 Name = jQuery.trim(Name)

     if(aryNameFromTrack1.length != 0)
     {
        track1            =  aryNameFromTrack1.split("^")
        if(track1.length != 0)
        {
            if(jQuery.trim(track1[1]) == Name) return true
            else return false;
        }//if(track1.length != 0)
        
   }//if(aryNameFromTrack1.length != 0)
}

function GetNameInCreditCard()
{
 var track1                 = '';       
 var aryNameFromTrack1      = dpsEftX.OutputTrack1;
   
   if(aryNameFromTrack1.length != 0)
   {
        track1            =  aryNameFromTrack1.split("^")
        if(track1.length != 0) return jQuery.trim(track1[1])
        else jQuery.trim($("#nameText").val())
   
   }//if(aryNameFromTrack1.length != 0)
}

function RefundNegativePayment()
{
    var paymentid = document.getElementById('ctl00_ContentPlaceHolder_HiddenPaymentID').value
    var url = "EFTPOSListener/EftposListener.aspx?mode=GetPayment"
                 
    $.post(url, {GetPayment: escape(paymentid)} ,function(result) {
                       
                        var info             = result.split("@")
                        var typeofpayment    = $('[id$=_HiddenPurchase]').val()
                        var HiddenType       = $("[id$=_HiddenType]").val()
                       
                        $("#creditcardSelect").html("<option>" + info[0] + "</option>")
                        
                        $("#nameText").attr("value",info[1])
                        $("#ApprovalReferenceText").attr("value",info[2])
                        
                        
                        if(info[5] == 0) $("#AmountTextbox").attr("value",parseFloat(info[3]).toFixed(2))
                        else $("#AmountTextbox").attr("value",parseFloat(info[5]).toFixed(2))
                        
                        //alert(info[0] + ' -- ' + HiddenType + ' ' + info[0].length);
                        if(info[0].indexOf('Amex') != -1)
                        {
                            $("#SurchargeText").attr("value","N/A")
                            $("#TotalText").attr("value",$("#AmountTextbox").val())
                        }
                        else
                        {
                            if(info[0].indexOf('Amex') == -1 && HiddenType == 'R')
                            {
                                    $("#SurchargeText").attr("value","N/A")
                                    $("#TotalText").attr("value",$("#AmountTextbox").val())
                            }
                            else
                            {
                                   $("#SurchargeText").attr("value",parseFloat(info[3] - info[5]).toFixed(2))
                                   $("#TotalText").attr("value",info[3])
                            }
                        }
    });                                
}


function LastTransactionReceipt()
{
  if(dpsEftX.Ready && dpsEftX.ReadyPinPad) LastReceipt();
}

//not use
function IsCreditCardIsFoundInBondPayment(cardNumber) {
    var url = "EFTPOSListener/EftposListener.aspx?mode=IsCreditCardIsFoundInBondPayment"
    var CreditCardIsFoundInBondPayment;
    var rentalOnly  = $("[id$=_HiddenRentalID]").attr("value")
    var IsCreditCardIsFoundInBondPayment;
   
   IsCreditCardIsFoundInBondPayment = $("[id$=_HiddenRentalID]").attr("value") + "|" + cardNumber 
   
    IsCreditCardIsFoundInBondPayment = IsCreditCardIsFoundInBondPayment + "|" + GetNameInCreditCard()
    $.post(url, { IsCreditCardIsFoundInBondPayment: IsCreditCardIsFoundInBondPayment }, function (result) {
        if (result == 'True') return true;
        else return false
   });

}

//not use
function IsAllowedToRefund() {
    var RptRntId   = $("[id$=_HiddenRentalID]").attr("value")
    var cardNumber = dpsEftX.OutputTrack2
        cardNumber = cardNumber.replace(';', '')
        cardNumber = cardNumber.split("=")
   
    var RptType     = $("[id$=_HiddenType]").val()
    var amountToPay = $("#AmountTextbox").val(); 
    

    var temp = RptRntId + "|" + cardNumber + "|" + RptType + "|" + amountToPay
    var url = "EFTPOSListener/EftposListener.aspx?mode=IsAllowedToRefund"
    $.post(url, { IsAllowedToRefund: temp }, function (result) {
        if (result.indexOf('ERROR') != -1) return false;
        else return true
    })
}


//----------------------------------------------------
//|capturing of billing token

function BillingTokenSelectForPayment(rentalID) {
    var url = "EFTPOSListener/EftposListener.aspx?mode=BillingTokenSelectForPayment"

    $.post(url, { BillingTokenSelectForPayment: rentalID }, function (result) {
        $('[id$=_BillingTokenSelect]').html(result)
    })
}

function DefaultMethodToCard(mode) {
    if (mode == 'set') {
        $('[id$=_ddlMethod]').attr("value", "00B1F2C5-0112-44A4-BDAE-D21561F8D911")
        $('[id$=_ddlMethod]').attr("disabled", true)
    }
    else {
        $('[id$=_ddlMethod]').attr("value", "")
        $('[id$=_ddlMethod]').attr("disabled", false)
    }
}

function DisplayCreditInfo(mode) {
    if (mode == 'set') {
        $("#ctl00_ContentPlaceHolder_pnlPayment_CreditCardAndDebitCard").css("display", "")
     } else {
        $("#ctl00_ContentPlaceHolder_pnlPayment_CreditCardAndDebitCard").css("display", "none")
    }

}

function BillingTokenSelectPopulateCardInfo(rentalid, tokenid) {
    var url = "EFTPOSListener/EftposListener.aspx?mode=BillingTokenSelectPopulateCardInfo"
    var temrq = rentalid + "|" + tokenid
    $.post(url, { BillingTokenSelectPopulateCardInfo: temrq }, function (result) {
        if (result.indexOf("ERROR") == -1) {
            $("[id$=_ddlCardType]").html(result.split("|")[0])
            $("[id$=_ddlCardType]").attr("value", result.split("|")[1]).attr("disabled",true)

            $("[id$=_txtCardNumber]").attr("value", result.split("|")[2]).attr("disabled", true)
            $("[id$=_TxtName]").attr("value", result.split("|")[3]).attr("disabled", true)
            $("[id$=_txtExpiryData]").attr("value", result.split("|")[4]).attr("disabled", true)


        }
    });
}

//instead of using the cardnumber.
//get the outputtrack2
function getOutputTrack2() {
    var card = '';
    card = dpsEftX.OutputTrack2;
    card = card.replace(';', '')
    card = card.split("=")

    if (card[0] != '') {
        return card[0]
    }
    else {
        return dpsEftX.CardNumber
    }
}


function IsCardNumberMatch(track2, base) {
    var track2length    = track2.length
    var track2FirstSix = track2.substr(0, 6)
    var track2LastTwo = track2.substr(track2length - 2, 2)

    var baselength = base.length
    var baseFirstSix = base.substr(0, 6)
    var baseLastTwo = base.substr(track2length - 2, 2)

    

    return ((track2FirstSix == baseFirstSix) && (track2LastTwo == baseLastTwo))
}


function DisplayOverrideBox() {
     $("#divVerification").show().css("border", "1px solid gray")
     $("#VerifyCard").attr('disabled', true)
     $("#CancelButton").attr("disabled", true);
     //alert($('[id$=_ddlReasonVerified]').val())
     if ($('[id$=_ddlReasonVerified]').text().indexOf('refund') == -1) {
         $('[id$=trReasonOtherVerified]').hide()
     }
     
     $("[id$=_HiddenIsOverride]").attr("value", "True")
 }

 function PrintAnotherCopy() {
    var usercode         = $("[id$=_HiddenUsercode]").attr("value") 
     var card = '';
     card = dpsEftX.OutputTrack2;
     card = card.replace(';', '')
     card = card.split("=")
     if (card[0] != '') PrintReceipt(dpsEftX.Receipt, usercode)
 }


 function TraceError(usercode, message) {
    if (usercode == 'MA22' || usercode == 'ma22') {
        alert(message)
    }
    if (usercode == 'DN2' || usercode == 'dn2') {
        alert(message)
    }
    if (usercode == 'JT8' || usercode == 'jt8') {
        alert(message)
    }
}

function PropertiesHasvalue(property) {
    if (property.length == 0) {
        return "property has no value"
    }
    return property 
}
function LogActiveXControl(rentalid) {
    var propandvalue;
            propandvalue = 'RentalID,'  + rentalid
            propandvalue = propandvalue + ',MerchantReference,'   + PropertiesHasvalue(dpsEftX.MerchantReference)
            propandvalue = propandvalue + ',ResponseText,'        + PropertiesHasvalue(dpsEftX.ResponseText)
            propandvalue = propandvalue + ',AccountSelected,'     + PropertiesHasvalue(dpsEftX.AccountSelected)
            propandvalue = propandvalue + ',AcquirerPort,'        + PropertiesHasvalue(dpsEftX.AcquirerPort)
            propandvalue = propandvalue + ',AuthCode,'            + PropertiesHasvalue(dpsEftX.AuthCode)
            propandvalue = propandvalue + ',Authorized,'          + PropertiesHasvalue(dpsEftX.Authorized)
            propandvalue = propandvalue + ',CardType,'            + PropertiesHasvalue(dpsEftX.CardType)
            propandvalue = propandvalue + ',DateSettlement,'      + PropertiesHasvalue(dpsEftX.DateSettlement)
            propandvalue = propandvalue + ',DateTimeTransaction,' + PropertiesHasvalue(dpsEftX.DateTimeTransaction)
            propandvalue = propandvalue + ',DpsBillingId,'        + PropertiesHasvalue(dpsEftX.DpsBillingId)
            propandvalue = propandvalue + ',DpsTxnRef,'           + PropertiesHasvalue(dpsEftX.DpsTxnRef)
            propandvalue = propandvalue + ',EnableCurrencyConversion,' + PropertiesHasvalue(dpsEftX.EnableCurrencyConversion)
            propandvalue = propandvalue + ',Rid,'     + PropertiesHasvalue(dpsEftX.Rid)
            propandvalue = propandvalue + ',Pix,'     + PropertiesHasvalue(dpsEftX.Pix)
            propandvalue = propandvalue + ',Reco,'    + PropertiesHasvalue(dpsEftX.Reco)
            propandvalue = propandvalue + ',Stan,'    + PropertiesHasvalue(dpsEftX.Stan)
            propandvalue = propandvalue + ',Success,' + PropertiesHasvalue(dpsEftX.Success)
            propandvalue = propandvalue + ',TxnRef,'  + PropertiesHasvalue(dpsEftX.TxnRef)
            propandvalue = propandvalue + ',OutputTrack1,' + PropertiesHasvalue(dpsEftX.OutputTrack1)
            propandvalue = propandvalue + ',OutputTrack2,' + PropertiesHasvalue(dpsEftX.OutputTrack2)
            propandvalue = propandvalue + ',OutputTrack3,' + PropertiesHasvalue(dpsEftX.OutputTrack3)
            propandvalue = propandvalue + ',CardNumber,'   + PropertiesHasvalue(dpsEftX.CardNumber)
            propandvalue = propandvalue + ',Receipt,'      + PropertiesHasvalue(dpsEftX.Receipt)
            propandvalue = propandvalue + ',ReceiptWidth,' + PropertiesHasvalue(dpsEftX.ReceiptWidth)
            propandvalue = propandvalue + ',DateExpiry,' + PropertiesHasvalue(dpsEftX.DateExpiry);
            propandvalue = propandvalue + ',MethodType selected in payment page,' + PropertiesHasvalue(additionalMessageForMethodType);
            propandvalue = propandvalue + ',CardName selected in popup page,'     + PropertiesHasvalue(additionalMessageForSwipedCard);
            propandvalue = propandvalue + ',DPS CardName returned,'               + PropertiesHasvalue(additionalMessageForDPSCard);
            propandvalue = propandvalue + ',AdditionalMessage,'                   + PropertiesHasvalue(additionalMessage);
            propandvalue = propandvalue + ',PaymentID,' + PropertiesHasvalue($("[id$=_HiddenPdtPmtID]").attr("value"));
            propandvalue = propandvalue + ',BaseCard,' + PropertiesHasvalue(baseCardAgainstOutputrack2)
            propandvalue = propandvalue + ',BaseCardName,' + PropertiesHasvalue(baseCardNameAgainstOutputrack1) 
            return propandvalue
        }

        function RetrieveEftposReasons(display) {
            var url = "EFTPOSListener/EftposListener.aspx?mode=RETREIVEEFTPOSREASONS"
            $.post(url, { RETREIVEEFTPOSREASONS: escape(display) }, function (result) {
                if (result != 'ERROR') {
                    $('[id$=_ddlReasonVerified]').html(result)
                   
                }
            });
        }


        function LogReadCardEvent(rentalID) {
            var url = "EFTPOSListener/EftposListener.aspx?mode=ACTIVEX"
            $.post(url, { ACTIVEX: LogActiveXControl(rentalID) }, function (result) {
                if (result == 'OK') {//do nothing
                }
            });
        }

        function CardTracksHasError() {
            if (dpsEftX.OutputTrack1.length < 5 && dpsEftX.OutputTrack2.length < 5) {
             return true
         }
            return false
        }