﻿/**
 * @author Sam Mueller
 * @version 0.1 
 */
(function($) {

	$.dynamicLoader = function(o) { };

	$.dynamicLoader.extend = $.extend;

	var loadUCDefaults = {
		ucName: '',
		queryString: '',
		eventBindings: {
			loaded: null, //default events
			busy: null,
			unbusy: null,
			finished: null
		}
	};

	var registeredScripts = {};

	$.dynamicLoader.extend({
		register: function(scriptUrl, onInit) {
			//only register the script if it hasn't been registered already
			if (!registeredScripts[scriptUrl]) //TODO: don't just check the dynamicLoader hash, but also check the dom for static-loaded scripts
				registeredScripts[scriptUrl] = onInit;
		},

		//right now, registerUC expects the javascript resource to share the same name as the user control
		registerUC: function(ucName, onInit) {
			var scriptUrl = ucName.toLowerCase() + '.js';
			$.dynamicLoader.register(scriptUrl, onInit);
		},

		bind: function(scriptUrl, uiCtx, eventBindings) {
			var bindAndRun = function() {
				//the script that we just received should have registered itself to the scriptloader
				//bind to all of the events
				var script = registeredScripts[scriptUrl];
				for (eventName in eventBindings) {
					if (eventBindings[eventName]) {
						//TODO: we need to check to make sure we don't double bind
						if (script) {
							$.event.add(script, eventName, function(evt) {
								if (eventBindings[evt.type])
									eventBindings[evt.type](uiCtx, evt.type);
							});
						}
					}
				}
				//now that we've bound to the events, we run the script
				registeredScripts[scriptUrl](uiCtx, registeredScripts[scriptUrl]);
			}

			if (!registeredScripts[scriptUrl]) {
				$.dynamicLoader.load(scriptUrl, bindAndRun);
			} else {
				//script is already loaded
				bindAndRun();
			}
		},

		load: function(scriptUrl, callback) {
			$.getScript(scriptUrl, callback); //TODO: getScript doesn't cache scripts!  write a $.getHashedScript that does this for us
		},

		loadUC: function(o) {
			var options = $.extend({}, loadUCDefaults, o || {});
			$.ajax({
				url: '/ajax.svc/renderuc?path=' + options.ucName.toLowerCase() + '&' + options.queryString,
				dataType: 'html',
				success: function(data) {
					var scriptUrl = options.ucName.toLowerCase().replace('.ascx', '.js');  //js url is based on the same name and directory as the user control
					//now bind to js (which will be dynamically loaded if it doesnt yet exist
					var userControl = $(data);
					$.dynamicLoader.bind(scriptUrl, userControl, options.eventBindings);
				},
				error: function showError(xhr, status, exc) {
					alert('error');
				}
			});
		}
	});
})(jQuery);