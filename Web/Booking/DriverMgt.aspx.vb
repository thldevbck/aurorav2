
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

<AuroraPageTitleAttribute("Add Drivers")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005,GEN045,GEN046")> _
Partial Class Booking_DriverMgt
    Inherits AuroraPage

    Dim bookingId As String = ""
    Dim rentalId As String = ""
    Dim bookingNumber As String = ""
    Dim rentalNumber As String = ""
    'Delegate Sub DelPopupPostBackObject(ByVal requireRefresh As Boolean)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3285499"
            'rentalId = "3285499-2"

            Dim dateTextBox As TextBox
            dateTextBox = dobDateControl.FindControl("dateTextBox")
            dateTextBox.Attributes.Add("onBlur", "dobDateControlOnChange('" + dateTextBox.ClientID + "');")

            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If
            If Not Request.QueryString("hdBookingNum") Is Nothing Then
                bookingNumber = Request.QueryString("hdBookingNum").ToString()
            End If
            If Not Request.QueryString("hdRentalNum") Is Nothing Then
                rentalNumber = Request.QueryString("hdRentalNum").ToString()
            End If

            If Not Page.IsPostBack Then
                dataBindDriverDetail()
                editPanel.Enabled = False

            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub dataBindDriverDetail()

        addDriverButton.Enabled = True

        'renatlLabel.Text = rentalId
        renatlTextBox.Text = bookingNumber & "/" & rentalNumber
        Dim dt As DataTable

        dt = BookingCustomer.GetDrivers(rentalId)

        If dt.Rows.Count = 1 Then
            driverGridView.DataSource = dt
            driverGridView.DataBind()
            driverGridView.Rows(0).Enabled = False
            removeDriverButton.Enabled = False
        Else
            driverGridView.Columns(10).Visible = True
            dt.Rows.RemoveAt(dt.Rows.Count - 1)
            driverGridView.DataSource = dt
            driverGridView.DataBind()
            removeDriverButton.Enabled = True
        End If

    End Sub

    'Protected Sub driverGridView_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles driverGridView.RowCreated

    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        'Dim driverIdLabel1 As Label = e.Row.FindControl("driverIdLabel1")
    '        Dim removeCheckBox As CheckBox = e.Row.FindControl("removeCheckBox")
    '        If String.IsNullOrEmpty(e.Row.DataItem("DriverId").ToString()) Then
    '            removeCheckBox.Enabled = False
    '        Else
    '            removeCheckBox.Enabled = True
    '        End If
    '    End If
    'End Sub

    Protected Sub driverGridView_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles driverGridView.RowUpdating
        Dim driverGridViewRow As GridViewRow
        driverGridViewRow = driverGridView.Rows(e.RowIndex)

        Dim driverIdLabel1 As System.Web.UI.WebControls.Label
        driverIdLabel1 = driverGridViewRow.FindControl("driverIdLabel1")
        Dim integrityNoLabel1 As System.Web.UI.WebControls.Label
        integrityNoLabel1 = driverGridViewRow.FindControl("integrityNoLabel1")
        Dim licenceNoLabel As System.Web.UI.WebControls.Label
        licenceNoLabel = driverGridViewRow.FindControl("licenceNoLabel")
        Dim dobLabel As System.Web.UI.WebControls.Label
        dobLabel = driverGridViewRow.FindControl("dobLabel")
        Dim ageLabel1 As System.Web.UI.WebControls.Label
        ageLabel1 = driverGridViewRow.FindControl("ageLabel")
        Dim lastNameLabel As System.Web.UI.WebControls.Label
        lastNameLabel = driverGridViewRow.FindControl("lastNameLabel")
        Dim firstNameLabel As System.Web.UI.WebControls.Label
        firstNameLabel = driverGridViewRow.FindControl("firstNameLabel")
        Dim titleLabel As System.Web.UI.WebControls.Label
        titleLabel = driverGridViewRow.FindControl("titleLabel")
        Dim issuingAuthorityLabel As System.Web.UI.WebControls.Label
        issuingAuthorityLabel = driverGridViewRow.FindControl("issuingAuthorityLabel")
        Dim expiryDateLabel As System.Web.UI.WebControls.Label
        expiryDateLabel = driverGridViewRow.FindControl("expiryDateLabel")


        driverIdLabel.Text = driverIdLabel1.Text
        integrityNoLabel.Text = integrityNoLabel1.Text
        licenceTextBox.Text = licenceNoLabel.Text
        'dobTextBox.Text = dobLabel.Text
        dobDateControl.Date = Utility.ParseDateTime(dobLabel.Text)
        ageLabel.Text = ageLabel1.Text
        lastNameTextBox.Text = lastNameLabel.Text
        firstNameTextBox.Text = firstNameLabel.Text
        titleTextBox.Text = titleLabel.Text
        issuingAuthorityTextBox.Text = issuingAuthorityLabel.Text
        'expiryDateTextBox.Text = expiryDateLabel.Text
        expiryDateDateControl.Date = Utility.ParseDateTime(expiryDateLabel.Text)
        statusLabel.Text = "Edit Driver"
        editPanel.Enabled = True


        addDriverButton.Enabled = False
        removeDriverButton.Enabled = False

    End Sub

    Protected Sub licenceTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles licenceTextBox.TextChanged
        findDriver()
    End Sub

    'Protected Sub dobTextBox_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles dobTextBox.TextChanged
    '    findDriver()
    'End Sub

    Private Sub findDriver()
        If (Not String.IsNullOrEmpty(rentalId)) And (Not String.IsNullOrEmpty(licenceTextBox.Text)) Then
            Dim dt As DataTable

            dt = BookingCustomer.FindDriverByLicence(rentalId, licenceTextBox.Text, dobDateControl.Date)

            If dt.Rows.Count > 0 Then
                If dt.Rows(0)("DriverId") <> driverIdLabel.Text Then
                    lastNameTextBox.Text = dt.Rows(0)("LastName")
                    firstNameTextBox.Text = dt.Rows(0)("FirstName")
                    titleTextBox.Text = dt.Rows(0)("Title")
                    issuingAuthorityTextBox.Text = dt.Rows(0)("IssuingAuthority")
                    expiryDateDateControl.Date = Utility.ParseDateTime(dt.Rows(0)("ExpiryDate"))
                    ageLabel.Text = dt.Rows(0)("Age")
                Else
                    SetShortMessage(AuroraHeaderMessageType.Information, dt.Rows(0)("LastName") & " " & dt.Rows(0)("FirstName") & " is already in the driver table !")
                End If
            End If

            '<RentalId>3285499-2</RentalId>
            '<DriverId>FA4A7B55-CFC5-4909-8F8C-2F1E6F09594D</DriverId>
            '<LicenceNo>1</LicenceNo>
            '<DOB>19/12/1980</DOB>
            '<LastName>Fdfd</LastName>
            '<FirstName>C</FirstName>
            '<Title></Title>
            '<IssuingAuthority>1</IssuingAuthority>
            '<ExpiryDate>16/02/2008</ExpiryDate>
            '<RemoveDriver>0</RemoveDriver>
            '<InterityNo>106</InterityNo>
            '<Age>27</Age>

        End If
    End Sub

    Protected Sub addDriverButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addDriverButton.Click
        editPanel.Enabled = True
        driverIdLabel.Text = ""
        lastNameTextBox.Text = ""
        firstNameTextBox.Text = ""
        titleTextBox.Text = ""
        issuingAuthorityTextBox.Text = ""
        expiryDateDateControl.Date = Today
        dobDateControl.Date = Today
        ageLabel.Text = "0"
        integrityNoLabel.Text = ""
        licenceTextBox.Text = ""
        statusLabel.Text = "Add Driver"

        addDriverButton.Enabled = False
        removeDriverButton.Enabled = False

    End Sub

    Protected Sub removeDriverButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeDriverButton.Click

        For Each r As GridViewRow In driverGridView.Rows
            Dim removeCheckBox As CheckBox
            removeCheckBox = r.FindControl("removeCheckBox")

            If removeCheckBox.Checked Then
                Dim driverIdLabel1 As Label
                driverIdLabel1 = r.FindControl("driverIdLabel1")

                Dim errorMessage As String = ""
                errorMessage = BookingCustomer.DeleteDriver(driverIdLabel1.Text, rentalId)
                If Not String.IsNullOrEmpty(errorMessage) Then
                    SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                End If
            End If
        Next

        dataBindDriverDetail()
        editPanel.Enabled = False

    End Sub


    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        If Not (dobDateControl.IsValid And expiryDateDateControl.IsValid) Then
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
            Return
        End If

        Dim updateType As String

        ''validate the driver if the driver licence field is not empty
        Dim errorMessage As String = ""

        errorMessage = BookingCustomer.ValidateDriver(rentalId, dobDateControl.Date, expiryDateDateControl.Date)
        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Warning, errorMessage)
            Return
        End If


        If String.IsNullOrEmpty(driverIdLabel.Text) Then
            'insert
            updateType = "I"
            ''Generate Customer ID

            'sMessage = "I"
            'Guid = oAuroraDAL.getRecord("sp_get_newId")
            'Guid = Replace(Guid, "<Id>", vbNullString)
            'Guid = Replace(Guid, "</Id>", vbNullString)
            'CusId = Trim(Guid)
            'QueryAction = "I"
            'bAction = "I"

            driverIdLabel.Text = Utility.ParseString(Data.GetNewId(), "")

            errorMessage = BookingCustomer.UpdateCustomer(driverIdLabel.Text, firstNameTextBox.Text, lastNameTextBox.Text, False, False, "", _
                titleTextBox.Text, dobDateControl.Date, licenceTextBox.Text, expiryDateDateControl.Date, _
                issuingAuthorityTextBox.Text, "", "", "", False, False, 1, "NA", "NA", "", _
                UserCode, UserCode, "DriverMgt.aspx", "")

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return
            End If

            'exec(cust_updateCustomer) 'F2E3B894-B178-41D8-A18B-983E74219E37', 'A', 'A', 0, 0, default, '', '20/10/1980', '1', 'Dec 20 2010 12:00:00:000AM', 
            ''a', default, default, default, 0, 
            '0, 1, 'NA', 'NA', '', 'jl3', 'jl3', 'customerlistener.asp'

        Else
            'update

            updateType = "U"
            'sXmlString = oAuroraDAL.updateRecords("cust_updateDriver")
            'If sXmlString <> "SUCCESS" Then
            '    GoTo ErrorSection
            'End If

            errorMessage = BookingCustomer.UpdateDriver(driverIdLabel.Text, firstNameTextBox.Text, lastNameTextBox.Text, titleTextBox.Text, _
                dobDateControl.Date, licenceTextBox.Text, expiryDateDateControl.Date, issuingAuthorityTextBox.Text, _
                integrityNoLabel.Text, UserCode, UserCode, "DriverMgt.aspx")

            If Not String.IsNullOrEmpty(errorMessage) Then
                SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
                Return
            End If
            'exec(cust_updateDriver) 'F2E3B894-B178-41D8-A18B-983E74219E37', 'A', 'A', '', '20/10/1980', '1', 
            ''Dec 20 2010 12:00:00:000AM',
            ''a', 2, 'jl3', 'jl3', 'customerlistener.asp'

        End If

        'exec cust_updateTraveller 'F2E3B894-B178-41D8-A18B-983E74219E37', '3285499-2', 1, 0, 'jl3', 'jl3', 'customerlistener.asp'
        '' create/update the traveller table

        errorMessage = BookingCustomer.UpdateTraveller(driverIdLabel.Text, rentalId, True, False, False, UserCode, UserCode, "DriverMgt.aspx")

        If Not String.IsNullOrEmpty(errorMessage) Then
            SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            Return
        End If

        If updateType = "I" Then
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN045"))
        ElseIf updateType = "U" Then
            SetShortMessage(AuroraHeaderMessageType.Information, GetMessage("GEN046"))
        End If

        dataBindDriverDetail()

        editPanel.Enabled = False
        driverIdLabel.Text = ""
        lastNameTextBox.Text = ""
        firstNameTextBox.Text = ""
        titleTextBox.Text = ""
        issuingAuthorityTextBox.Text = ""
        expiryDateDateControl.Date = Today
        dobDateControl.Date = Today
        ageLabel.Text = ""
        licenceTextBox.Text = ""

        'addDriverButton.Enabled = True
        'If driverGridView.Rows.Count > 0 Then
        '    removeDriverButton.Enabled = True
        'Else
        '    removeDriverButton.Enabled = False
        'End If

    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(".\Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=" & BookingTabIndex.Customer)
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        editPanel.Enabled = False
        driverIdLabel.Text = ""
        lastNameTextBox.Text = ""
        firstNameTextBox.Text = ""
        titleTextBox.Text = ""
        issuingAuthorityTextBox.Text = ""
        expiryDateDateControl.Date = Today
        dobDateControl.Date = Today
        ageLabel.Text = ""
        licenceTextBox.Text = ""

        addDriverButton.Enabled = True
        'removeDriverButton.Enabled = True
        If driverGridView.Rows.Count > 0 Then
            removeDriverButton.Enabled = True
        Else
            removeDriverButton.Enabled = False
        End If
    End Sub

    Protected Sub DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dobDateControl.DateChanged
        findDriver()
    End Sub

    Protected Function showCommand(ByVal driverId As Object) As Boolean
        Dim driverIdInt As Integer
        driverIdInt = Utility.ParseInt(driverId, 0)

        If driverIdInt = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

End Class


