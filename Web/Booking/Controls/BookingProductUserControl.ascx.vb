Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common


Partial Class Booking_Controls_BookingProductUserControl
    Inherits BookingUserControl

#Region " Constant"
    ''Const SESSION_XMLBookedProductList As String = "XMLBookedProductList"
    Const SESSION_hashproperties As String = "hashproperties"
    Const SESSION_saveForDetail As String = "saveForDetail"
    Const QS_BPDDETAILLIS As String = "BookedProductDetails.aspx?funCode=BPDDETAILLIS"
    ''Const QS_BPDDETAILLIS As String = "../BookedProductDetails.aspx?funCode=BPDDETAILLIS"
    Const IntegrityErrorMsg As String = "This Rental has been modified by another user. Please refresh the screen."
#End Region

#Region " Protected Variables"
  

    Protected ErrMSg As String = "<Root><Error><ErrStatus>True</ErrStatus><ErrNumber/><ErrType>System</ErrType><ErrDescription>e1</ErrDescription></Error></Root>"
    Protected XMLpopupData As New XmlDocument
    Protected XMLBookedProductList As New XmlDocument
    Protected hashproperties As New Hashtable
    Protected serverflag As Boolean = False
    Protected ShowallRentals As Boolean
  
    Protected GTSprice As String
    Delegate Sub SupervisorOverridePostBack(ByVal IsOkButton As Boolean, ByVal param As String)
    Public isFirstLoad As Boolean
    Property booPrd() As String
        Get
            Return ViewState("Boo_Prd_booPrd")
        End Get
        Set(ByVal value As String)
            ViewState("Boo_Prd_booPrd") = value
        End Set
    End Property

    ''' <summary>
    ''' added PPF for displaying OVERRIDE PASSWORD
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Property RateFromTable As Boolean
        Get
            Return ViewState("RateFromTable")
        End Get
        Set(ByVal value As Boolean)
            ViewState("RateFromTable") = value
        End Set
    End Property
#End Region

#Region " Protected Property "


    Protected Property ErrorMsg() As String
        Get
            Return CType(hashproperties("ErrorMsg"), String)
        End Get
        Set(ByVal value As String)
            hashproperties("ErrorMsg") = value
        End Set
    End Property

    Protected Property BooID() As String
        Get
            Return CType(hashproperties("BooID"), String)
        End Get
        Set(ByVal value As String)
            hashproperties("BooID") = value
            Me.HidBooid.Value = value
        End Set
    End Property

    Protected Property RentID() As String
        Get
            Return CType(hashproperties("RentID"), String)
        End Get
        Set(ByVal value As String)
            hashproperties("RentID") = value
            Me.HidRentalID.Value = value
        End Set
    End Property

    Protected ReadOnly Property CityCode() As String
        Get
            'JL 2008-06-23
            'Use country code instead of cityCode
            Return MyBase.CurrentPage.CountryCode
        End Get
    End Property

    Private _hiddenrentalid As String

    Public Property HiddenRentalID() As String
        Get
            Return _hiddenrentalid
        End Get
        Set(ByVal value As String)
            _hiddenrentalid = value
        End Set
    End Property

    Private _hiddenBookingid As String
    Public Property HiddenBookingID() As String
        Get
            Return _hiddenBookingid
        End Get
        Set(ByVal value As String)
            _hiddenBookingid = value
        End Set
    End Property


#End Region

#Region " Function"

    Protected Function AddIDinChosenProduct(ByVal xmlstring As String, ByVal xpath As String, ByVal nodename As String) As String
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(xmlstring)
        Dim xmlatt As XmlAttribute

        Dim newNode As XmlNode = Nothing

        Dim elem As XmlElement = xmldoc.DocumentElement

        Dim nodes As XmlNodeList = elem.SelectNodes(xpath)
        Dim ctr As Integer = 0
        For Each node As XmlNode In nodes
            If node.Name = "Rental" Then
                xmlatt = xmldoc.CreateAttribute(nodename)
                xmlatt.Value = ctr
                node.Attributes.Append(xmlatt)
            ElseIf node.Name = "Detail" Then

                newNode = xmldoc.CreateElement(nodename & "Temp")
                newNode.InnerText = ctr
                node.AppendChild(newNode)

                xmlatt = xmldoc.CreateAttribute(nodename)
                xmlatt.Value = ctr
                node.Attributes.Append(xmlatt)

            End If
            ctr = ctr + 1
        Next
        Return xmldoc.OuterXml
    End Function

    Protected Function ParseEvalItemToBoolean(ByVal objCurr As Object) As Boolean

        If objCurr Is DBNull.Value Then
            Return False
        End If
        If objCurr Is Nothing Then
            Return False
        End If
        If objCurr = 0 Then
            Return False
        End If
        If objCurr = "1" Then
            Return True
        End If

    End Function

    Protected Function GetChargeData() As String
        Dim tempxml As String

        ''------------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''------------------------------------------------------------------------------
        'If IsNothing(Session("ShowChargesDetailsDS")) Then
        '    tempxml = CType(Session("ShowChargesDetailsDS"), DataSet).GetXml
        'Else
        '    tempxml = Server.HtmlDecode(HidChargeSummary.Value)
        'End If

        If Not String.IsNullOrEmpty(ShowChargesDetails) Then
            tempxml = Me.ShowChargesDetails
        Else
            tempxml = Server.HtmlDecode(HidChargeSummary.Value)
        End If
        ''------------------------------------------------------------------------------

        Dim xmldso As New XmlDocument
        xmldso.LoadXml(tempxml)

        Dim inode As Integer = xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes.Count - 1
        Dim i As Integer = 0
        Dim otxt As TextBox = Nothing
        Dim ddl As DropDownList = Nothing

        For Each repItem As RepeaterItem In Me.repShowChargeDetails.Items

            otxt = CType(repItem.FindControl("txtChgDetRate"), TextBox)
            If otxt IsNot Nothing Then
                xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetRate").InnerText = otxt.Text
            End If

            otxt = CType(repItem.FindControl("txtChgDetQty"), TextBox)
            If otxt IsNot Nothing Then
                xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetQty").InnerText = otxt.Text
            End If

            otxt = CType(repItem.FindControl("TxtChgDetRateOverRide"), TextBox)
            If otxt IsNot Nothing Then
                xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetRateOverRide").InnerText = otxt.Text
            End If

            ddl = CType(repItem.FindControl("ddlrate"), DropDownList)
            If ddl IsNot Nothing Then
                xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetOverRideType").InnerText = ddl.SelectedItem.Value
            End If

            otxt = CType(repItem.FindControl("TxtChgDetAgnDisOverRide"), TextBox)
            If otxt IsNot Nothing Then
                Dim tempValue As String = otxt.Text.Replace("%", "")
                xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetAgnDisOverRide").InnerText = tempValue
            End If

            otxt = CType(repItem.FindControl("txtChgDetTotOverRide"), TextBox)
            If otxt IsNot Nothing Then
                xmldso.SelectSingleNode("//Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetTotOverRide").InnerText = otxt.Text
            End If

            i = i + 1
        Next

        Return xmldso.SelectSingleNode("//Details/ChgDetails").OuterXml
    End Function

    Protected Function ValidateData() As Boolean
        Dim rep As RepeaterItem = Nothing
        Dim ddl As DropDownList
        Dim txt As TextBox

        For Each rep In Me.repShowChargeDetails.Items
            txt = CType(rep.FindControl("TxtChgDetRateOverRide"), TextBox)
            ddl = CType(rep.FindControl("ddlRate"), DropDownList)
            If ddl IsNot Nothing Then
                If ddl.SelectedItem.Value = "Flex" AndAlso ddl.SelectedItem.Text = "Flex" Then

                    If txt IsNot Nothing Then
                        If txt.Text = "" Then Return True
                        If txt.Text.Length <> 2 Then
                            MyBase.CurrentPage.SetErrorMessage("For Flex Type rate should be of 2 character")
                            Return False
                        End If
                    End If

                Else ''for flex
                    If txt IsNot Nothing Then

                        If txt.Text.Length <> 0 Then
                            Try
                                Dim resut As Integer = Convert.ToDecimal(txt.Text)
                                Return True
                            Catch ex As Exception
                                MyBase.CurrentPage.SetErrorMessage("Only Flex Type Can Have Alpha Numeric Values.")
                                Return False
                            End Try
                        End If
                        
                    End If
                End If
            End If
        Next
        Return True
    End Function

    Private Function CheckRentalIntegrity(ByVal sRentalId As String, _
              ByVal sBPDId As String, _
              ByVal sBooID As String, _
              ByVal nScreenRentalIntegrityNo As String, _
              ByVal nScreenBookingIntegrityNo As String) As Boolean


        Dim bDataIsCurrent As Boolean = False


        Dim sDataFromDB As String
        Dim oXml As New XmlDocument
        Dim nDataBaseRentalIntegrityNo As String = "0"
        Dim nDataBaseBooIntNo As String = "0"



        sDataFromDB = Aurora.Common.Data.ExecuteScalarSP("RES_GetRentalIntegrity", sRentalId, sBPDId, sBooID)
        oXml.LoadXml(sDataFromDB)


        If Not oXml.SelectSingleNode("/Rental/IntegrityNo") Is Nothing Then
            nDataBaseRentalIntegrityNo = (oXml.SelectSingleNode("/Rental/IntegrityNo").InnerText)
        End If

        If nDataBaseRentalIntegrityNo.Equals(nScreenRentalIntegrityNo) Then

            If Not oXml.SelectSingleNode("/Rental/Booking/BooIntNo") Is Nothing Then
                nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("/Rental/Booking/BooIntNo").InnerText)
            Else

                If Not oXml.SelectSingleNode("/Booking/BooIntNo") Is Nothing Then
                    nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("/Booking/BooIntNo").InnerText)
                End If
            End If


            If nDataBaseBooIntNo.Equals(nScreenBookingIntegrityNo) Then
                bDataIsCurrent = True
            Else
                bDataIsCurrent = False
            End If


        Else
            'failed very first check!
            bDataIsCurrent = False
        End If

        Return bDataIsCurrent

    End Function

    Protected Function SaveChargeDetails() As Boolean
        Dim returnGetChargeData As String = Me.GetChargeData
        Dim tempXMLobj As New XmlDocument
        Dim BpdId As String = ""

        Try
            If String.IsNullOrEmpty(returnGetChargeData) Then
                MyBase.CurrentPage.SetErrorMessage("Invalid Xml ChargeDetails")
                Return False
            End If

            Dim tempxml As String
            ''--------------------------------------------------------------------------
            ''rev:mia jan 30 2009 -- Changes from session to viewstate
            ''--------------------------------------------------------------------------
            'If IsNothing(Session("ShowChargesDetailsDS")) Then
            '    tempxml = CType(Session("ShowChargesDetailsDS"), DataSet).GetXml
            'Else
            '    tempxml = Server.HtmlDecode(HidChargeSummary.Value)
            'End If

            If Not String.IsNullOrEmpty(ShowChargesDetails) Then
                tempxml = ShowChargesDetails
            Else
                tempxml = Server.HtmlDecode(HidChargeSummary.Value)
            End If
            ''--------------------------------------------------------------------------

            Dim xmldso As New XmlDocument
            xmldso.LoadXml(tempxml)

            If ViewState("ChgDetQty") <> xmldso.SelectSingleNode("//Details/ChgDetails/ChgDet/ChgDetQty").InnerText AndAlso _
                (xmldso.SelectSingleNode("//Details/ChgDetails/ChgDet/ChgDetQty").InnerText = "0" OrElse xmldso.SelectSingleNode("//Details/ChgDetails/ChgDet/ChgDetQty").InnerText = "") AndAlso _
                    xmldso.SelectSingleNode("//Details/ChgDetails/ChgDet/ChgDetBaseTableName").InnerText.ToUpper = "PRR" Then
                MyBase.CurrentPage.SetErrorMessage("Quantity can't be zero")
                Return False
            End If

            Dim sBPDId, nIntNo, nBooIntNo
            Dim bValidIntNo As Boolean
            If Not xmldso.SelectSingleNode("//data/Details/Detail/BpdId") Is Nothing Then
                If Not xmldso.SelectSingleNode("//data/Rental/IntegrityNo") Is Nothing Then
                    If Not xmldso.SelectSingleNode("//data/Rental/Booking/BooIntNo") Is Nothing Then
                        sBPDId = CStr(xmldso.SelectSingleNode("//data/Details/Detail/BpdId").InnerText)
                        nIntNo = CInt(xmldso.SelectSingleNode("//data/Rental/IntegrityNo").InnerText)
                        nBooIntNo = CInt(xmldso.SelectSingleNode("//data/Rental/Booking/BooIntNo").InnerText)
                        bValidIntNo = CheckRentalIntegrity("", sBPDId, "", nIntNo, nBooIntNo)
                    End If
                End If
            End If

            Dim retXML As String = Nothing
            Dim temp As String = CType(ViewState("ShowChargeDetailsParameters"), String)
            Dim strAry() As String = temp.Split("|")

            If bValidIntNo = True Then
                Dim bpdids As String = strAry(0)
                Dim ChgFromDate As String = strAry(1)
                Dim ChgToDate As String = strAry(2)
                Dim ChgFrom As String = strAry(3)
                Dim ChgTo As String = strAry(4)
                Dim sBaseId As String = strAry(5)
                Dim retBlnValue As Boolean
                retXML = Aurora.Booking.Services.ChargeDetails.manageChargeDetails(bpdids, ChgFromDate, ChgToDate, ChgFrom, ChgTo, MyBase.CurrentPage.UserCode, "ChargeDetails.Js", returnGetChargeData, "0", retBlnValue)

            Else
                MyBase.CurrentPage.SetErrorMessage(IntegrityErrorMsg)
                Exit Function
            End If

            If Not String.IsNullOrEmpty(retXML) Then

                tempXMLobj.LoadXml(retXML)

           
                If (tempXMLobj.SelectSingleNode("//Root/Error/ErrNumber").InnerText = "POPUP") Then
                    ''  supervisorOverrideControl.Show(ErrorMessage:=tempXMLobj.SelectSingleNode("//Root/Error/ErrDescription").InnerText, Role:="xxx") 'todo: what role?
                End If

                If (tempXMLobj.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "True") Then
                    Exit Function
                End If

                BpdId = tempXMLobj.SelectSingleNode("//Root/RetVal/NewBpdId").InnerText
                ViewState("BpdId") = BpdId

                If tempXMLobj.SelectSingleNode("//Root/Error/ErrStatus").InnerText = False Then
                    MyBase.CurrentPage.SetInformationShortMessage(tempXMLobj.SelectSingleNode("//Root/Error/ErrDescription").InnerText)
                Else
                    MyBase.CurrentPage.SetErrorShortMessage(tempXMLobj.SelectSingleNode("//Root/Error/ErrDescription").InnerText)
                    Exit Function
                End If


                If (tempXMLobj.SelectSingleNode("//Root/RetVal/CallConvoy").InnerText = "0") Then

                    If CType(ViewState("PromptQty"), String) = "0" Then
                        Me.WrapShowChargeDetails("")
                    Else
                        Me.WrapShowChargeDetails("", True)
                        
                        ShowCharges(BpdId)
                        ''---------------------------------------------------------------------------
                        ''rev:mia jan 30 2009 -- Changes from session to viewstate
                        ''---------------------------------------------------------------------------
                        ''Dim dsShowCharges As DataSet = CType(Session("ShowChargesDS"), DataSet)

                        Dim dsShowCharges As New DataSet
                        dsShowCharges.ReadXml(New XmlTextReader(ShowChargesData, System.Xml.XmlNodeType.Document, Nothing))
                        ''---------------------------------------------------------------------------

                        Dim xmlstring As String = dsShowCharges.GetXml
                        dsShowCharges.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
                        Me.HidShowChargeDetails.Value = Server.HtmlEncode(xmlstring)

                        Call Me.EnabledPanels(1)
                        
                    End If

                    Exit Function
                End If

                'ShowConfirmationDialog("Should the overrides just entered be processed on all of the convoy^rental of this booking?(Where the Saleable Product, Check-out/Check-In^dates, and charge period are the same as this booked product, and which^are also charged to the xxx1.)", "Confirmation Required")
                'If ViewState("DialogReturn") = "0" Then
                '    Exit Function
                'End If

                tempXMLobj = Nothing

            End If

            If bValidIntNo = True Then
                Dim bpdids As String = strAry(0)
                Dim ChgFromDate As String = strAry(1)
                Dim ChgToDate As String = strAry(2)
                Dim ChgFrom As String = strAry(3)
                Dim ChgTo As String = strAry(4)
                Dim sBaseId As String = strAry(5)
                Dim retBlnValue As Boolean
                retXML = Aurora.Booking.Services.ChargeDetails.manageChargeDetails(bpdids, ChgFromDate, ChgToDate, ChgFrom, ChgTo, MyBase.CurrentPage.UserCode, "ChargeDetails.Js", returnGetChargeData, "1", retBlnValue)
            Else
                MyBase.CurrentPage.SetErrorShortMessage(IntegrityErrorMsg)
                Exit Function
            End If

            If String.IsNullOrEmpty(retXML) Then
                tempXMLobj.LoadXml(retXML)
                MyBase.CurrentPage.SetInformationMessage(tempXMLobj.SelectSingleNode("//Root/Error/ErrDescription").InnerText)

                BpdId = tempXMLobj.SelectSingleNode("//Root/RetVal/NewBpdId").InnerText
                ViewState("BpdId") = BpdId

                If CType(ViewState("PromptQty"), String) = "0" Then
                    Me.WrapShowChargeDetails("")
                Else
                    Me.WrapShowChargeDetails("", True)
                End If

                If (tempXMLobj.SelectSingleNode("//Root/Error/ErrStatus").InnerText = "True") Then
                    Exit Function
                End If

                tempXMLobj = Nothing
            End If


            tempXMLobj.LoadXml(retXML)

            If (tempXMLobj.SelectSingleNode("//Details/ChgDetails").ChildNodes.Count = 0) Then
                goToChargeSummary(BpdId)
            End If
        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("SaveChargeDetails > " & ex.Message)
        End Try

    End Function

    Function goToChargeSummary(ByVal param As String) As String
        Return Aurora.Common.Data.ExecuteSqlXmlSPDoc("tab_getChargeSummary", param).OuterXml
    End Function

    Protected Function GetRentalUrl(ByVal rentalId As Object) As String
        Dim rentalIdString As String
        rentalIdString = Convert.ToString(rentalId)

        Return "../RentalEdit.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & rentalIdString & "&hdBookingNum=" & BookingNumber & "&activeTab=9"
    End Function

    Protected Function GetProductUrl(ByVal rentalId As Object, ByVal bpdId As Object) As String
        Dim rentalIdString As String
        rentalIdString = Convert.ToString(RentalId)

        Dim bpdIdString As String
        bpdIdString = Convert.ToString(bpdId)

        Return "../BookedProductMgt.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & rentalIdString & "&hdBookingNum=" & BookingNumber & "&bpdId=" & bpdIdString & "&activeTab=9"
    End Function

    Protected Function GetDate(ByVal d As Object) As Date
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Dim dDate As Date
            dDate = Utility.ParseDateTime(d, Utility.SystemCulture)
            Return dDate
        Else
            Return Date.MinValue
        End If


    End Function

#End Region

#Region " Sub "

    
    Protected Function URLDisplayPopup(ByVal chgdtid As String) As String

        Dim dstemp As New DataSet
        Dim xmlstring As String

        ''------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''------------------------------------------------------------------------
        'If Session("ShowChargesDetailsDS") IsNot Nothing Then
        '    dstemp = CType(Session("ShowChargesDetailsDS"), DataSet)
        '    xmlstring = dstemp.GetXml
        'Else
        '    xmlstring = Server.HtmlEncode(HidChargeSummary.Value)
        'End If

        If Not String.IsNullOrEmpty(ShowChargesDetails) Then
            dstemp.ReadXml(New XmlTextReader(ShowChargesDetails, System.Xml.XmlNodeType.Document, Nothing))
            xmlstring = dstemp.GetXml
        Else
            xmlstring = Server.HtmlEncode(HidChargeSummary.Value)
        End If
        ''------------------------------------------------------------------------

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml(xmlstring)
        Dim inode As Integer = xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDetails").ChildNodes.Count - 1

        Dim sBapId As String = ""
        Dim chgDet As String = ""
        Dim chgDisc As String = ""

        For i As Integer = 0 To inode
            If xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetId").InnerText = chgdtid Then
                sBapId = xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDetails").ChildNodes(i).SelectSingleNode("ChgDetBpdId").InnerText
                chgDet = xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDetails").ChildNodes(i).OuterXml

                For ii As Integer = 0 To xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDiscount").ChildNodes.Count - 1
                    If xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDiscount").ChildNodes(ii).SelectSingleNode("@ChgDetId").Value = chgdtid Then
                        chgDisc = chgDisc & xmlTemp.DocumentElement.SelectSingleNode("//data/Details/ChgDiscount").ChildNodes(ii).OuterXml
                    End If
                Next
                Exit For
            End If
        Next
        chgDisc = String.Concat("<ChgDiscount>", chgDisc, "</ChgDiscount>")
        Dim chgData As String = "<Root>" & "<Msg>" & xmlTemp.SelectSingleNode("//data/Msg").InnerText & "</Msg>" & chgDet & chgDisc & "</Root>"
        Session("hid_htm_chgData_2013") = chgData ''rev:mia feb 5 2013 - quickfix
        Return "ChargeCalculatedRate.aspx?sBapId=" & sBapId & "&sChgDetId=" & chgdtid & "&hid_htm_chgData=" & Server.HtmlEncode(chgData) & "&htm_hid_BookNum=" & Me.BookingId


    End Function

    Protected Sub SubmitDataForProduct(ByVal myval As String, Optional ByVal usercode As String = "")
        If String.IsNullOrEmpty(myval) Then
            myval = 1
        Else
            myval = 0
        End If
        Dim param1 As String = Me.RentalId
        Dim param4 As String = Me.BookingId

        If hasError = True Then
            param1 = Me.HiddenRentalID
            param4 = Me.HiddenBookingID
        End If

        Dim param2 As String = myval
        Dim param3 As String = Me.XMLBookedProductList.OuterXml

        ''--------------------------------------------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''--------------------------------------------------------------------------------------------------------------
        Session(SESSION_saveForDetail) = String.Concat(param1, "|", param2, "|", param3, "|", Me.BookingId, "|", param4)

        ''SaveForDetail = String.Concat(param1, "|", param2, "|", param3, "|", Me.BookingId, "|", param4)
        ''--------------------------------------------------------------------------------------------------------------
    End Sub

    Protected Sub AddToLocalSession()

        Me.litXMLBookedProductList.Text = Server.HtmlEncode(Me.XMLBookedProductList.OuterXml)
        ''--------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''--------------------------------------------------------------------------
        ''Me.hashproperties(SESSION_XMLBookedProductList) = Me.XMLBookedProductList
        ''Session(SESSION_hashproperties) = Me.hashproperties

        Me.XMLBookedProductListData = Me.XMLBookedProductList.OuterXml
        ''--------------------------------------------------------------------------
    End Sub

    Protected Sub GetLocalSession()
        Try
            ''--------------------------------------------------------------------------
            ''rev:mia jan 30 2009 -- Changes from session to viewstate
            ''--------------------------------------------------------------------------
            ''Me.hashproperties = CType(Session(SESSION_hashproperties), Hashtable)
            ''Me.XMLBookedProductList = CType(Me.hashproperties(SESSION_XMLBookedProductList), XmlDocument)

            Me.XMLBookedProductList.LoadXml(Me.XMLBookedProductListData)
            ''--------------------------------------------------------------------------
        Catch ex As Exception
            Me.XMLBookedProductList.LoadXml(Server.HtmlDecode(Me.litXMLBookedProductList.Text))
            MyBase.CurrentPage.SetErrorMessage("GetLocalSession > " & ex.Message)
        End Try
        
    End Sub

    Protected Sub GetIDfromPopUp()
        Me.GetLocalSession()

        Dim ucLocalA As UserControls_PickerControl = Nothing
        Dim ucLocalB As UserControls_PickerControl = Nothing
        Dim fotRow As GridViewRow = GridView1.FooterRow
        Dim tblfooter As HtmlTable = CType(fotRow.FindControl("tblfooter"), HtmlTable)

        If fotRow Is Nothing Then
            AddNewBpd()
            Exit Sub
        End If

        If Me.XMLBookedProductList.OuterXml = "" Then
            Me.XMLBookedProductList.LoadXml(Server.HtmlDecode(Me.litXMLBookedProductList.Text))
        End If
        Dim i As Integer = Me.XMLBookedProductList.DocumentElement.ChildNodes.Count - 1
        ucLocalA = CType(fotRow.FindControl("PickerControlAllProductA"), UserControls_PickerControl)

        If (ucLocalA IsNot Nothing) AndAlso (Not String.IsNullOrEmpty(ucLocalA.DataId)) Then

            If PickerCounter = -1 AndAlso (tblfooter.Rows.Count = 13) Then
                XMLBookedProductList.DocumentElement.ChildNodes(i - 1).SelectSingleNode("PrdId").InnerText = ucLocalA.DataId
                XMLBookedProductList.DocumentElement.ChildNodes(i - 1).SelectSingleNode("PrdShortName").InnerText = ucLocalA.Text
            Else
                XMLBookedProductList.DocumentElement.ChildNodes((i - 1) - PickerCounter).SelectSingleNode("PrdId").InnerText = ucLocalA.DataId
                XMLBookedProductList.DocumentElement.ChildNodes((i - 1) - PickerCounter).SelectSingleNode("PrdShortName").InnerText = ucLocalA.Text
            End If

        End If

        ucLocalB = CType(fotRow.FindControl("PickerControlAllProductB"), UserControls_PickerControl)
        If (ucLocalB IsNot Nothing) AndAlso (Not String.IsNullOrEmpty(ucLocalB.DataId)) Then

            If PickerCounter = -1 AndAlso (tblfooter.Rows.Count = 13) Then
                XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("PrdId").InnerText = ucLocalB.DataId
                XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("PrdShortName").InnerText = ucLocalB.Text
            Else
                XMLBookedProductList.DocumentElement.ChildNodes(i - PickerCounter).SelectSingleNode("PrdId").InnerText = ucLocalB.DataId
                XMLBookedProductList.DocumentElement.ChildNodes(i - PickerCounter).SelectSingleNode("PrdShortName").InnerText = ucLocalB.Text
            End If

        End If


        Dim ctr As Integer = PickerCounter
        Dim ictr As Integer
        For ii As Integer = 10 To 0 Step -1
            If CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl) IsNot Nothing AndAlso CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl).Visible = True Then
                Dim picker As UserControls_PickerControl = CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl)
                XMLBookedProductList.DocumentElement.ChildNodes(i - ii).SelectSingleNode("PrdId").InnerText = picker.DataId
                XMLBookedProductList.DocumentElement.ChildNodes(i - ii).SelectSingleNode("PrdShortName").InnerText = picker.Text
                ictr = ictr - 1
            End If
        Next
        ''-----------------------------------------------------------------------------------------------------------------------------
        
        'Dim ictr As Integer = Me.GridView1.Rows.Count
        'Dim ctr As Integer = i - ictr ''subtract rows from the total childnodes of the xml
        'Dim xmlcounterstart As Integer = ctr - 1
        'For ii As Integer = 10 To 0 Step -1
        '    If CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl) IsNot Nothing AndAlso CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl).Visible = True Then
        '        Dim picker As UserControls_PickerControl = CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl)

        '        If XMLBookedProductList.DocumentElement.ChildNodes(xmlcounterstart) IsNot Nothing Then
        '            XMLBookedProductList.DocumentElement.ChildNodes(xmlcounterstart).SelectSingleNode("PrdId").InnerText = picker.DataId
        '            XMLBookedProductList.DocumentElement.ChildNodes(xmlcounterstart).SelectSingleNode("PrdShortName").InnerText = picker.Text
        '            xmlcounterstart += 1
        '        End If

        '    End If
        'Next

       
    End Sub

    Property hasError() As Boolean
        Get
            Return CBool(ViewState("boo_prd_hasError"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("boo_prd_hasError") = value
        End Set
    End Property

    Protected Sub LoadXMLpopupdata()
        If ((Not String.IsNullOrEmpty(MyBase.RentalId)) AndAlso (Not String.IsNullOrEmpty(MyBase.BookingId))) Then
            Me.BooID = MyBase.BookingId
            Dim xmltemp As String = Aurora.Common.Data.ExecuteScalarSP("cus_GetBookingDetail", MyBase.BookingId, "BookRntRec", MyBase.RentalId, "", MyBase.CurrentPage.UserCode)
            Try
                XMLpopupData.LoadXml(xmltemp)
                Me.HiddenBookingID = MyBase.BookingId
                Me.HiddenRentalID = MyBase.RentalId
            Catch ex As Exception
                hasError = True
            End Try

            If (XMLpopupData Is Nothing) Or (XMLpopupData.OuterXml = "") Then
                XMLpopupData = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_GetBookingDetail", "", "BookRec", "", Me.BookingId, MyBase.CurrentPage.UserCode)
                Me.HiddenBookingID = XMLpopupData.DocumentElement.ChildNodes(0).SelectSingleNode("BooId").InnerText
                Me.HiddenRentalID = XMLpopupData.DocumentElement.ChildNodes(0).SelectSingleNode("RntId").InnerText
            End If

            LitHiddenBookingID.Text = Me.HiddenBookingID
            LitHiddenRentalID.Text = Me.HiddenRentalID

            serverflag = True


           
        End If
    End Sub

    Protected Sub GetProductList(ByVal strvalue As String)
        If String.IsNullOrEmpty(strvalue) Then
            Me.getXmlDataNew(0)
        Else
            Me.getXmlDataNew(1)
        End If
    End Sub

    Sub getBPDIdforDecryption()
        If Me.BookingId = "" Then Exit Sub
        Dim tempXmlObj As New XmlDocument
        tempXmlObj = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_GetBookingDetail", "", "BookRec", "", Me.BookingId, MyBase.CurrentPage.UserCode)

        If (tempXmlObj.DocumentElement.ChildNodes(0).SelectSingleNode("BooId") Is Nothing) Then
            If (tempXmlObj.DocumentElement.ChildNodes(0).SelectSingleNode("RntId") Is Nothing) Then
                Exit Sub
            End If
        End If
        Me.HiddenBookingID = tempXmlObj.DocumentElement.ChildNodes(0).SelectSingleNode("BooId").InnerText
        Me.HiddenRentalID = tempXmlObj.DocumentElement.ChildNodes(0).SelectSingleNode("RntId").InnerText



        Dim _rentalid As String = Me.HiddenRentalID ''MyBase.RentalId 
        Dim _bookingid As String = Me.HiddenBookingID


        Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getBookedProductList", _bookingid, _rentalid, Me.HidBpdIds.Value.ToString, 0, MyBase.CurrentPage.UserCode).OuterXml
        xmlstring = AddIDinChosenProduct(xmlstring, "Rental", "id")

        tempXmlObj.LoadXml(xmlstring)

        Dim tempBDDid As String = ""

        Try
            tempBDDid = tempXmlObj.SelectSingleNode("data/Rental[@id='0']/BpdId").InnerText

            ''---------------------------------------------------------
            ''rev:mia jan 30 2009 -- Changes from session to viewstate
            ''---------------------------------------------------------
            Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode) = tempBDDid

            BpdIDDforDecryptionData = tempBDDid
            ''---------------------------------------------------------
            ViewState("bpdIDforDecryption") = tempBDDid

        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("getBPDIdforDecryption > " & ex.Message)
        End Try

        tempXmlObj = Nothing
    End Sub

    Protected Sub getXmlDataNew(ByVal intmode As Integer)
        '@sBooId		VARCHAR	 (64)	=	'' ,
        '@sRntId		VARCHAR	 (64)	=	'' ,
        '@BpdIdsList	VARCHAR	 (8000)	=	'',
        '@bAll			BIT			=	1 ,
        '@UserCode	VARCHAR	(64)	 	=	''
        Dim _rentalid As String = Me.HiddenRentalID ''MyBase.RentalId 
        Dim _bookingid As String = Me.HiddenBookingID

        If String.IsNullOrEmpty(_rentalid) Then
            _rentalid = Me.LitHiddenRentalID.Text
        End If

        If String.IsNullOrEmpty(_bookingid) Then
            _bookingid = Me.LitHiddenBookingID.Text
        End If

        ''orig
        ''Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getBookedProductList", MyBase.BookingId, rentalid, Me.HidBpdIds.Value.ToString, intmode, MyBase.CurrentPage.UserCode).OuterXml
        Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_getBookedProductList", _bookingid, _rentalid, Me.HidBpdIds.Value.ToString, intmode, MyBase.CurrentPage.UserCode).OuterXml

        xmlstring = AddIDinChosenProduct(xmlstring, "Rental", "id")
        If XMLBookedProductList Is Nothing Then
            XMLBookedProductList = New XmlDocument
        End If
        Me.XMLBookedProductList.LoadXml(xmlstring)

        ''rev:mia jan5-cancellation product
        If Me.XMLBookedProductList.SelectSingleNode("data/Rental/BpdId") IsNot Nothing Then
            If Me.XMLBookedProductList.SelectSingleNode("data/Rental/BpdId").InnerText <> "" Then

                ''---------------------------------------------------------
                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                ''---------------------------------------------------------
                Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode) = Me.XMLBookedProductList.SelectSingleNode("data/Rental[@id='0']/BpdId").InnerText

                Me.BpdIDDforDecryptionData = Me.XMLBookedProductList.SelectSingleNode("data/Rental[@id='0']/BpdId").InnerText
                ''---------------------------------------------------------
                ViewState("bpdIDforDecryption") = Me.XMLBookedProductList.SelectSingleNode("data/Rental[@id='0']/BpdId").InnerText

                Dim ds As New DataSet
                ds.ReadXml(New XmlTextReader(Me.XMLBookedProductList.OuterXml, System.Xml.XmlNodeType.Document, Nothing))
                Dim dview As DataView = ds.Tables("Rental").DefaultView

                ''rev:mia 11Aug
                ''dview.Sort = "PrdId desc"
                dview.RowFilter = "PrdId <> ''"
                GridView1.DataSource = dview

                Dim arykey(1) As String
                arykey(0) = "id"
                arykey(1) = "BpdId"
                GridView1.DataKeyNames = arykey
                GridView1.DataBind()

                ''rev:mia  oct.18
                Dim pickercontrol As UserControls_PickerControl
                pickercontrol = GridView1.FooterRow.FindControl("PickerControlAllProductA")
                pickercontrol.Param2 = Me.XMLBookedProductList.SelectSingleNode("data/Rental/CtyCode").InnerText
                pickercontrol = GridView1.FooterRow.FindControl("PickerControlAllProductB")
                pickercontrol.Param2 = Me.XMLBookedProductList.SelectSingleNode("data/Rental/CtyCode").InnerText
            End If
        Else
            GridView1.DataSource = Nothing
            GridView1.DataBind()
        End If

        
       
    End Sub

    Protected Sub GetXMLChangesFromGrid()

        If Me.XMLBookedProductList.SelectSingleNode("data/Rental/BpdId").InnerText = "" Then Exit Sub
        Dim txtloccode As TextBox = Nothing
        Dim txttoloc As TextBox = Nothing

        Dim frDateControl As UserControls_DateControl
        Dim toDateControl As UserControls_DateControl

        Dim i As Integer = 0

        ''rev:mia 11Aug
        Dim ckoLoc As UserControls_PickerControl = Nothing
        Dim ckiLoc As UserControls_PickerControl = Nothing

        For Each row As GridViewRow In GridView1.Rows

            If row.RowType = DataControlRowType.DataRow Then
                If Not String.IsNullOrEmpty(GridView1.DataKeys(row.DataItemIndex).Value) Then

                    txtloccode = CType(row.FindControl("txtLocCode"), TextBox)
                    txttoloc = CType(row.FindControl("txtToLoc"), TextBox)

                    ckoLoc = CType(row.FindControl("PickerControlCKO"), UserControls_PickerControl)
                    ckiLoc = CType(row.FindControl("PickerControlCKI"), UserControls_PickerControl)

                    If XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("LocCode") IsNot Nothing Then
                        ''XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("LocCode").InnerText = txtloccode.Text
                        XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("LocCode").InnerText = ckoLoc.Text
                    End If


                    If XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("CKLocCode") IsNot Nothing Then
                        ''XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("CKLocCode").InnerText = txttoloc.Text
                        XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("CKLocCode").InnerText = ckiLoc.Text
                    End If


                    'JL 2008/07/01
                    frDateControl = CType(row.FindControl("frDateControl"), UserControls_DateControl)
                    toDateControl = CType(row.FindControl("toDateControl"), UserControls_DateControl)

                    If XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("FrDate") IsNot Nothing Then

                        If frDateControl.Date = Date.MinValue Then
                            XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("FrDate").InnerText = ""
                        Else
                            Dim s As String = Utility.DateDBToString(frDateControl.Date)
                            XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("FrDate").InnerText = s ''.Substring(0, s.IndexOf(" "))
                        End If

                    End If


                    If XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("ToDt") IsNot Nothing Then

                        If toDateControl.Date = Date.MinValue Then
                            XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("ToDt").InnerText = ""
                        Else
                            Dim s As String = Utility.DateDBToString(toDateControl.Date)
                            XMLBookedProductList.DocumentElement.ChildNodes(i).SelectSingleNode("ToDt").InnerText = s ''.Substring(0, s.IndexOf(" "))
                        End If

                    End If


                    i = i + 1
                End If
            End If

        Next

    End Sub

    Protected Sub AddXMLBookedProductID()
        Dim tempRentalElement As XmlElement = XMLBookedProductList.CreateElement("Rental")
        XMLBookedProductList.DocumentElement.AppendChild(tempRentalElement)

        Dim bpdidElement As XmlElement = XMLBookedProductList.CreateElement("BpdId")
        tempRentalElement.AppendChild(bpdidElement)

        Dim PrdShortNameElement As XmlElement = XMLBookedProductList.CreateElement("PrdShortName")
        tempRentalElement.AppendChild(PrdShortNameElement)

        Dim PrdIdElement As XmlElement = XMLBookedProductList.CreateElement("PrdId")
        tempRentalElement.AppendChild(PrdIdElement)

    End Sub

    Protected Sub AddNewBpd()
        Me.GetLocalSession()
        Dim tempRentalElement As XmlElement = XMLBookedProductList.CreateElement("Rental")
        XMLBookedProductList.DocumentElement.AppendChild(tempRentalElement)

        Dim bpdidElement As XmlElement = XMLBookedProductList.CreateElement("BpdId")
        tempRentalElement.AppendChild(bpdidElement)

        Dim PrdShortNameElement As XmlElement = XMLBookedProductList.CreateElement("PrdShortName")
        tempRentalElement.AppendChild(PrdShortNameElement)

        Dim PrdIdElement As XmlElement = XMLBookedProductList.CreateElement("PrdId")
        tempRentalElement.AppendChild(PrdIdElement)


        Me.AddToLocalSession()
       
    End Sub

    Protected Sub ShowCharges(ByVal bpdid As String)
        Try
            Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("tab_getChargeSummary", bpdid, "", "").OuterXml
            xmlstring = AddIDinChosenProduct(xmlstring, "Details/Detail", "id")

            Dim arykey(1) As String
            arykey(0) = "idTemp"

            Dim dsShowCharges As New DataSet
            dsShowCharges.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

            ''---------------------------------------------------------
            ''rev:mia jan 30 2009 -- Changes from session to viewstate
            ''---------------------------------------------------------
            ''Session("ShowChargesDS") = dsShowCharges

            ShowChargesData = dsShowCharges.GetXml
            ''---------------------------------------------------------

            ''HidChargeSummary.Value = Server.HtmlEncode(xmlstring)
            Me.HidShowChargeDetails.value = Server.HtmlEncode(xmlstring)

            '' Me.gridChargeSummary.DataKeyNames = arykey
            Me.gridChargeSummary.DataSource = dsShowCharges.Tables("Detail")
            Me.gridChargeSummary.DataBind()
            Dim fotRow As GridViewRow = gridChargeSummary.FooterRow
            Dim litObject As Literal = Nothing

            ''rev: 
            Me.GridShowChargeDetails.DataSource = dsShowCharges.Tables("Detail")
            Me.GridShowChargeDetails.DataBind()


            litObject = CType(fotRow.FindControl("litGSTprice"), Literal)
            If litObject IsNot Nothing Then
                litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).TotGst")
            End If


            litObject = CType(fotRow.FindControl("litCurr"), Literal)
            If litObject IsNot Nothing Then
                litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).Curr")
            End If

            litObject = CType(fotRow.FindControl("litDis"), Literal)
            If litObject IsNot Nothing Then
                litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).TotDisc")
            End If

            litObject = CType(fotRow.FindControl("litAgnDis"), Literal)
            If litObject IsNot Nothing Then
                litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).TotAgnDisc")
            End If

            litObject = CType(fotRow.FindControl("litTotOw"), Literal)
            If litObject IsNot Nothing Then
                litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).TotOw1")
                ''litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).BpdId")
            End If

            litObject = CType(fotRow.FindControl("litbpdid"), Literal)
            If litObject IsNot Nothing Then
                litObject.Text = DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).BpdId")
            End If

            If Not String.IsNullOrEmpty(dsShowCharges.Tables("Detail").Rows(0)("Msg")) Then
                MyBase.CurrentPage.SetErrorMessage(dsShowCharges.Tables("Detail").Rows(0)("Msg"))
            End If

            ' Me.gridChargeSummary.CaptionAlign = TableCaptionAlign.Left

            'JL 20080723
            'change the formate of the dates
            'Me.gridChargeSummary.Caption = "<b><font color='black'>Charge Summary:&nbsp;&nbsp;&nbsp;Booked Product: &nbsp;&nbsp;&nbsp;</font><font color='black'>" & DataBinder.Eval(dsShowCharges, "Tables(1).DefaultView(0).BooPrd") & "</font></b>"
            Dim fromDateString As String = dsShowCharges.Tables("Detail").Rows(0)("ChgFrDt")
            Dim toDateString As String = dsShowCharges.Tables("Detail").Rows(0)("ChgToDt")
            ''Dim booPrd As String = dsShowCharges.Tables("Detail").Rows(0)("BooPrd")
            booPrd = dsShowCharges.Tables("Detail").Rows(0)("BooPrd")
            If Not String.IsNullOrEmpty(fromDateString) Then
                booPrd = booPrd.Replace(fromDateString, Utility.DateDBUIConvert(fromDateString))
            End If
            If Not String.IsNullOrEmpty(toDateString) Then
                booPrd = booPrd.Replace(toDateString, Utility.DateDBUIConvert(toDateString))
            End If
           

            Dim chargeSummaryStr As String = "<b>Charge Summary : Booked Product:</b> &nbsp;&nbsp;&nbsp;" & Server.HtmlEncode(booPrd)

            Me.litChargeSummary.Text = chargeSummaryStr
            'Me.gridChargeSummary.Caption = chargeSummaryStr

        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("ShowCharges > " & ex.Message)
        End Try

    End Sub

    Protected Sub ShowChargeDetails(ByVal strArg As String)

        Dim strAry() As String = strArg.Split("|")
        If (strAry.Length > -1 AndAlso strAry.Length = 6) Then
            Dim bpdid As String = strAry(0)
            Dim ChgFromDate As String = strAry(1)
            Dim ChgToDate As String = strAry(2)
            Dim ChgFrom As String = strAry(3)
            Dim ChgTo As String = strAry(4)
            Dim sBaseId As String = strAry(5)

            ViewState("ShowChargeDetailsParameters") = String.Concat(bpdid, "|", ChgFromDate, "|", ChgToDate, "|", ChgFrom, "|", ChgTo, "|", sBaseId)

            Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetChargeDetails", bpdid, ChgFromDate, ChgToDate, ChgFrom, ChgTo, "Booking.aspx", MyBase.CurrentPage.UserCode, sBaseId).OuterXml
            Dim dsShowChargesDetails As New DataSet
            dsShowChargesDetails.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

            ''REV:MIA AUG 19 2011
            Try
                If dsShowChargesDetails.Tables("ChgDet").Rows(0)("RateFromTable") Is Nothing Then
                    RateFromTable = False
                Else
                    RateFromTable = CBool(dsShowChargesDetails.Tables("ChgDet").Rows(0)("RateFromTable"))
                End If

            Catch ex As Exception
                Logging.LogException("ShowChargeDetails: ", ex)
                RateFromTable = False
            End Try

            Try
                ViewState("BpdIsVeh") = dsShowChargesDetails.Tables("Detail").Rows(0)("BpdIsVeh")
                ViewState("PromptQty") = dsShowChargesDetails.Tables("Detail").Rows(0)("PromptQty")
                ViewState("ChgDetQty") = dsShowChargesDetails.Tables("ChgDet").Rows(0)("ChgDetQty")

            Catch ex As Exception

            End Try

            ''------------------------------------------------------------
            ''rev:mia jan 30 2009 -- Changes from session to viewstate
            ''------------------------------------------------------------
            ''Session("ShowChargesDetailsDS") = dsShowChargesDetails

            Me.ShowChargesDetails = dsShowChargesDetails.GetXml
            ''------------------------------------------------------------

            HidChargeSummary.Value = Server.HtmlEncode(xmlstring)

            Me.GridShowChargeDetails.DataSource = dsShowChargesDetails.Tables("Detail")
            Me.GridShowChargeDetails.DataBind()

            repShowChargeDetails.DataSource = dsShowChargesDetails.Tables("ChgDet")
            repShowChargeDetails.DataBind()

            litShowChargeDetails.Text = "<b>Charge Details : Booked Product : </b>" & Server.HtmlEncode(booPrd)

        End If
    End Sub

    Property PickerCounter() As Integer
        Get
            Return CInt(ViewState("boo_prd_PickerCounter"))
        End Get
        Set(ByVal value As Integer)
            ViewState("boo_prd_PickerCounter") = value
        End Set
    End Property

    Private Sub DisplayAdditionPickerControl()
        If PickerCounter <= 10 Then
            PickerCounter = PickerCounter + 1
        End If

        ''------------------------------------------------------------------

        If Me.XMLBookedProductList.OuterXml = "" Then
            Me.XMLBookedProductList.LoadXml(Server.HtmlDecode(Me.litXMLBookedProductList.Text))
        End If
        Dim i As Integer = Me.XMLBookedProductList.DocumentElement.ChildNodes.Count - 1

        Dim fotRow As GridViewRow = GridView1.FooterRow
        Dim tblfooter As HtmlTable = CType(fotRow.FindControl("tblfooter"), HtmlTable)
        Dim ictr As Integer = Me.GridView1.Rows.Count
        Dim ctr As Integer = i - ictr ''subtract rows from the total childnodes of the xml
        ''Dim xmlcounterstart As Integer = ctr - 1

        Dim controlcounter As Integer = 0
        Dim xmlcounterstart As Integer = 0
        Dim nodes As XmlNodeList = XMLBookedProductList.SelectNodes("data/Rental")
        For Each node As XmlNode In nodes
            If node.Attributes("id") Is Nothing Then
                Dim picker As UserControls_PickerControl = CType(fotRow.FindControl("PickerControlAllProduct" & xmlcounterstart), UserControls_PickerControl)
                If picker IsNot Nothing Then
                    picker.Visible = True
                    CType(tblfooter.FindControl("trPickerControlAllProduct" & xmlcounterstart), HtmlTableRow).Visible = True
                    xmlcounterstart += 1
                End If
            End If
        Next
    End Sub

    Private Sub CreateAdditionalfooter(Optional ByVal blnCreate As Boolean = False)

        If blnCreate = False Then Exit Sub

        Dim rowFooter As GridViewRow = GridView1.FooterRow
        Dim tblfooter As HtmlTable = CType(rowFooter.FindControl("tblfooter"), HtmlTable)
        ''Dim tblfooter As Table = CType(rowFooter.FindControl("tblfooter"), Table)

        Dim newrow As HtmlTableRow
        Dim newCell As HtmlTableCell

        Dim picker As Control
        If PickerCounter = -1 Then
            PickerCounter = 0
        End If
        For i As Integer = 0 To PickerCounter
            newCell = New HtmlTableCell
            newrow = New HtmlTableRow

            picker = CType(Page.LoadControl("~/UserControls/PickerControl/PickerControl.ascx"), Control)
            ''picker = New UserControls_PickerControl
            picker.Visible = True

            CType(picker, UserControls_PickerControl).Param2 = MyBase.CurrentPage.CountryCode
            CType(picker, UserControls_PickerControl).Param3 = MyBase.RentalId
            CType(picker, UserControls_PickerControl).ID = "PickerControlAllProduct_" & i
            CType(picker, UserControls_PickerControl).Width = Unit.Pixel(85)
            CType(picker, UserControls_PickerControl).AutoPostBack = True

            ''AddHandler picker.Load, AddressOf LogBPDID


            newCell.Attributes.Add("runat", "server")
            newCell.Attributes.Add("id", "tdPickerControlAllProduct_" & i)
            newCell.Controls.Add(picker)
            newrow.Controls.Add(newCell)
            ''newrow.Attributes.Add("runat", "server")

            tblfooter.Rows.Add(newrow)
        Next
        PickerCounter += 1

    End Sub

    Protected Sub EnabledPanels(ByVal index As Integer)
        If index = 0 Then ''default
            Me.pnlBookedProducts.Visible = True
            Me.pnlChargeSummary.Visible = False
            Me.pnlShowChargeDetails.Visible = False

            Me.pnlBookedProductsButton.Visible = True
            Me.pnlChargeSummaryButton.Visible = False
            Me.pnlShowChargeDetailsButtons.Visible = False

            Me.chkShowallrentals.Visible = True
        ElseIf index = 1 Then
            Me.pnlBookedProducts.Visible = False
            Me.pnlChargeSummary.Visible = True
            Me.pnlShowChargeDetails.Visible = False

            Me.pnlBookedProductsButton.Visible = False
            Me.pnlChargeSummaryButton.Visible = True
            Me.pnlShowChargeDetailsButtons.Visible = False
            Me.chkShowallrentals.Visible = False
        ElseIf index = 2 Then
            Me.pnlBookedProducts.Visible = False
            Me.pnlChargeSummary.Visible = False
            Me.pnlShowChargeDetails.Visible = True

            Me.pnlBookedProductsButton.Visible = False
            Me.pnlChargeSummaryButton.Visible = False
            Me.pnlShowChargeDetailsButtons.Visible = True
            Me.chkShowallrentals.Visible = False
        Else
            Me.pnlBookedProducts.Visible = False
            Me.pnlChargeSummary.Visible = False
            Me.pnlShowChargeDetails.Visible = False

            Me.pnlBookedProductsButton.Visible = False
            Me.pnlChargeSummaryButton.Visible = False
            Me.pnlShowChargeDetailsButtons.Visible = False
            Me.chkShowallrentals.Visible = False
        End If

    End Sub

    Protected Sub WrapShowChargeDetails(ByVal gridID As String, Optional ByVal keychange As Boolean = False)
        If gridID = "" Then
            gridID = HidShowChargeDetailsArgument.Value
        Else
            HidShowChargeDetailsArgument.Value = gridID
        End If

        Call Me.EnabledPanels(2)

        Dim tempXmlString As String = ""
        Dim dsTemp As New DataSet

        ''------------------------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''------------------------------------------------------------------------------------------
        'If Session("ShowChargesDS") Is Nothing Then
        '    tempXmlString = Server.HtmlDecode(HidShowChargeDetails.Value)
        '    dsTemp.ReadXml(New XmlTextReader(tempXmlString, System.Xml.XmlNodeType.Document, Nothing))
        'Else
        '    dsTemp = CType(Session("ShowChargesDS"), DataSet)
        'End If

        If String.IsNullOrEmpty(ShowChargesData) Then
            tempXmlString = Server.HtmlDecode(HidShowChargeDetails.Value)
            dsTemp.ReadXml(New XmlTextReader(tempXmlString, System.Xml.XmlNodeType.Document, Nothing))
        Else
            dsTemp.ReadXml(New XmlTextReader(ShowChargesData, System.Xml.XmlNodeType.Document, Nothing))
        End If
        ''------------------------------------------------------------------------------------------

        Dim xmltemp As New XmlDocument
        xmltemp.LoadXml(dsTemp.GetXml)

        Dim bpdid As String
        Dim fotRow As GridViewRow = gridChargeSummary.FooterRow
        Dim litObject As Literal = Nothing
        litObject = CType(fotRow.FindControl("litbpdid"), Literal)
        If litObject IsNot Nothing Then
            bpdid = litObject.Text
        End If

        If CType(ViewState("BpdId"), String) = "" Then
            bpdid = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/BpdId").InnerText
        Else
            ''bpdid = CType(ViewState("BpdId"), String)
            bpdid = litObject.Text
            
        End If

        Dim ChgFromDate As String = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/ChgFrDtTime").InnerText
        Dim ChgToDate As String = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/ChgToDtTime").InnerText
        Dim ChgFrom As String = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/ChgFr").InnerText
        Dim ChgTo As String = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/ChgTo").InnerText

        If keychange = True Then
            ChgTo = CType(Me.repShowChargeDetails.Items(0).FindControl("txtChgDetQty"), TextBox).Text
        Else
            ChgTo = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/ChgTo").InnerText
        End If

        Dim sBaseId As String = xmltemp.DocumentElement("Details").SelectSingleNode("Detail[@id=" & gridID & "]/ChgBaseId").InnerText
        Dim tempxml As String = String.Concat(bpdid, "|", ChgFromDate, "|", ChgToDate, "|", ChgFrom, "|", ChgTo, "|", sBaseId)

        ShowChargeDetails(tempxml)

    End Sub

    Public Sub LoadPage()
        If Me.isFirstLoad Then
            ''rev:mia oct.20
            chkShowallrentals.Checked = False
            Me.ButNew.Enabled = True
            Me.ButSave.Enabled = True

            Me.HidBpdIds.Value = CType(Session("BookedProductIdList_" & MyBase.CurrentPage.UserCode), String)
            Session("BookedProductIdList_" & MyBase.CurrentPage.UserCode) = Nothing
            chkShowallrentals.Enabled = True

            If Session("Err") IsNot Nothing Then
                MyBase.CurrentPage.SetInformationMessage(CType(Session("Err"), String))
                Session.Remove("Err")
            Else
                Session.Remove(SESSION_saveForDetail)
            End If

            Me.LoadXMLpopupdata()
            Me.GetProductList(String.Empty)
            Me.AddToLocalSession()

            AddFooterProductID()

            Call Me.EnabledPanels(0)
            If GridView1.Rows.Count = 0 Then
                Me.tblBookedProducts.Visible = True
                ''REV:MIA DEC23
                PickerControlAllProductC.Param2 = MyBase.CurrentPage.CountryCode
                PickerControlAllProductC.Param3 = MyBase.RentalId

                PickerControlAllProductD.Param2 = MyBase.CurrentPage.CountryCode
                PickerControlAllProductD.Param3 = MyBase.RentalId
                BookedProductsPickerCounter = 0
                HideDefaultRows()
            Else
                Me.tblBookedProducts.Visible = False
            End If
            ' addition by Shoel - The call to BPD Search
            ButSearch.Attributes.Add("OnClick", "window.location='BookedProductSearch.aspx?BooId=" & BookingId & "&RntId=" & RentalId & "';")
            ' addition by Shoel - The call to BPD Search
            Me.ButSelectProduct.Attributes.Add("OnClick", "window.location='BookedProductSelectMgt.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "';")
        Else
            '' GetFooterRow()
            'LoadXMLpopupdata()
            'Me.AddToLocalSession()
            Me.GetLocalSession()
        End If
       
    End Sub

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString)
        Else
            Return ""
        End If
    End Function

#End Region

#Region " Events "

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                ' addition by Shoel - The call to BPD Search
                hasError = False
                PickerCounter = -1

                ButSearch.Attributes.Add("OnClick", "window.location='BookedProductSearch.aspx?BooId=" & BookingId & "&RntId=" & RentalId & "';")
                ' addition by Shoel - The call to BPD Search

                Me.ButSelectProduct.Attributes.Add("OnClick", "window.location='BookedProductSelectMgt.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "';")
                If Not isFirstLoad Then
                    EnabledPanels(10)
                    chkShowallrentals.Enabled = False
                Else
                    PickerCounter = -1
                End If
            Else
                If Me.isFirstLoad = True Then
                    Me.GetLocalSession()
                End If

                ''---------------------------------------------------------
                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                ''---------------------------------------------------------
                'If Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode) IsNot Nothing Then
                '    If CType(ViewState("bpdIDforDecryption"), String) <> CType(Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode), String) Then
                '        Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode) = ViewState("bpdIDforDecryption")
                '    End If
                'Else
                '    Me.getBPDIdforDecryption()
                'End If

                If Not String.IsNullOrEmpty(BpdIDDforDecryptionData) Then
                    If CType(ViewState("bpdIDforDecryption"), String) <> BpdIDDforDecryptionData Then
                        BpdIDDforDecryptionData = ViewState("bpdIDforDecryption")
                    End If
                    Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode) = ViewState("bpdIDforDecryption")
                Else
                    Me.getBPDIdforDecryption()
                End If
                ''---------------------------------------------------------



                End If


        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("Page_Load > " & ex.Message & vbCrLf & ex.StackTrace)
            EnabledPanels(10)
        End Try
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.DataItem IsNot Nothing AndAlso Not String.IsNullOrEmpty(CType(e.Row.DataItem, DataRowView)("St").ToString()) Then
            e.Row.BackColor = DataConstants.GetBookingStatusColor("" & CType(e.Row.DataItem, DataRowView)("St"))
        End If
    End Sub

    Protected Sub GridView1_DataBound(ByVal sender As Object, ByVal e As System.EventArgs) Handles GridView1.DataBound
        ' ''exec GEN_GetPopUpData @case = 'PRD4CTY', @param1 = '', @param2 = 'NZ', @param3 = '3294283-1', @param4 = '', @param5 = '', @sUsrCode = 'ma1'

        Dim ucLocalA As UserControls_PickerControl = Nothing
        Dim ucLocalB As UserControls_PickerControl = Nothing

        Dim fotRow As GridViewRow = GridView1.FooterRow
        If fotRow Is Nothing Then Exit Sub
        ucLocalA = CType(fotRow.FindControl("PickerControlAllProductA"), UserControls_PickerControl)
        If ucLocalA IsNot Nothing Then
            ucLocalA.Param2 = MyBase.CurrentPage.CountryCode
            ucLocalA.Param3 = MyBase.RentalId
        End If

        ucLocalB = CType(fotRow.FindControl("PickerControlAllProductB"), UserControls_PickerControl)
        If ucLocalB IsNot Nothing Then
            ucLocalB.Param2 = MyBase.CurrentPage.CountryCode
            ucLocalB.Param3 = MyBase.RentalId
        End If
        If ShowallRentals = True Then
            If ucLocalA IsNot Nothing AndAlso ucLocalB IsNot Nothing Then
                ucLocalA.Visible = False
                ucLocalB.Visible = False
            End If
        End If
    End Sub

    Private todelete As Boolean = False
    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.GetLocalSession()
        Dim checkbox As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(checkbox.NamingContainer, GridViewRow)
        Dim item As String = GridView1.DataKeys(row.DataItemIndex).Value.ToString

        If checkbox.Checked = True Then
            Me.XMLBookedProductList.SelectSingleNode("/data/Rental[@id='" & item & "']/Cancel").InnerText = 1
            todelete = True
        Else
            Me.XMLBookedProductList.SelectSingleNode("/data/Rental[@id='" & item & "']/Cancel").InnerText = 0
            todelete = False
        End If
        AddToLocalSession()
    End Sub

    Protected Sub ButSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButSave.Click

        ' Addition by Shoel - 30.10.2008
        ''--------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''--------------------------------------------------------------------------
        ''Dim xmlTemp As XmlDocument = CType(CType(Session(SESSION_hashproperties), Hashtable)(SESSION_XMLBookedProductList), System.Xml.XmlDocument)

        ''rev:mia 2Feb2015 - commented to give way for the Slot Popup
        PreSave()

        'Dim xmlTemp As New XmlDocument
        'xmlTemp.LoadXml(Me.XMLBookedProductListData)
        ' ''--------------------------------------------------------------------------

        'Dim nScreenBooIntNo, nScreenRntIntNo As Integer

        'With xmlTemp.DocumentElement.SelectSingleNode("Rental")
        '    If .SelectSingleNode("BooIntNo") Is Nothing Then
        '        nScreenBooIntNo = CInt(.SelectSingleNode("Booking/BooIntNo").InnerText)
        '    Else
        '        nScreenBooIntNo = CInt(.SelectSingleNode("BooIntNo").InnerText)
        '    End If
        '    nScreenRntIntNo = CInt(.SelectSingleNode("IntNo").InnerText)
        'End With
        'xmlTemp = Nothing
        'If Not Aurora.Booking.Services.Booking.CheckIntegrity(Me.BookingId, nScreenRntIntNo, nScreenBooIntNo, Me.RentalId) Then
        '    CurrentPage.AddErrorMessage("This rental has been modified by another user. Please refresh the screen.")
        '    Exit Sub
        'End If
        '' Addition by Shoel - 30.10.2008


        ' ''Call GetIDfromPopUp()
        'Call GetIDfromPopUpDynamic()
        'Call GetXMLChangesFromGrid()

        'If Not IsControlHasValue() Then Exit Sub

        'If todelete = False Then
        '    Call SubmitDataForProduct(String.Empty)
        'Else
        '    Call SubmitDataForProduct(0)
        'End If

        'PickerCounter = -1
        'Dim qstring As String = QS_BPDDETAILLIS & "&hdBookingId=" & MyBase.BookingId

        'If String.IsNullOrEmpty(Request.QueryString("sTxtSearch")) Then
        '    qstring = qstring & "&hdRentalId=" & MyBase.RentalId & "&sTxtSearch="
        'Else
        '    qstring = qstring & "&hdRentalId=" & MyBase.RentalId & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString()
        'End If

        ' ''--------------------------------------------------------------------------
        ' ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ' ''--------------------------------------------------------------------------
        ' ''qstring = qstring & "&SubmitDataForProduct=" & Server.HtmlEncode(SaveForDetail)
        ' ''--------------------------------------------------------------------------
        'Response.Redirect(qstring)

    End Sub

    Protected Sub chkShowallrentals_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowallrentals.CheckedChanged
        ShowallRentals = chkShowallrentals.Checked

        'rev:mia oct.20
        Me.ButNew.Enabled = Not chkShowallrentals.Checked
        Me.ButSave.Enabled = Not chkShowallrentals.Checked
        If ShowallRentals = True Then
            getXmlDataNew(1)
        Else
            getXmlDataNew(0)
        End If
        Me.AddToLocalSession()

        Dim fotRow As GridViewRow = GridView1.FooterRow
        If fotRow IsNot Nothing Then
            fotRow.Visible = Not ShowallRentals
        End If
    End Sub

    
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        If e.CommandName = "ShowCharge" Then
            If e.CommandArgument.ToString.Length > 0 Then
                Call Me.EnabledPanels(1)
                ShowCharges(e.CommandArgument.ToString)
            End If

        End If
    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        Me.GetProductList(String.Empty)
        Call Me.EnabledPanels(0)
    End Sub

    Protected Sub btnShowChargeDetails_back_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowChargeDetails_back.Click
        If ViewState("BpdId") <> "" Then
            ShowCharges(ViewState("BpdId"))
        End If
        Call Me.EnabledPanels(1)
    End Sub

    Protected Sub gridChargeSummary_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gridChargeSummary.RowCommand
        If e.CommandName = "ShowChargeDetails" Then
            If e.CommandArgument.ToString.Length > 0 Then
                Dim Arg As String = e.CommandArgument.ToString
                WrapShowChargeDetails(Arg)
            Else
                Call Me.EnabledPanels(1)
            End If
        End If
    End Sub

    Protected Sub repShowChargeDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repShowChargeDetails.ItemDataBound
        Dim objtxt As TextBox = Nothing
        Dim ddlrate As DropDownList = Nothing
        Dim lnk As HtmlAnchor
        Dim lnkURL As String = "ChargeCalculatedRate.asp?sBapId="
        If e.Item.ItemType = ListItemType.Item Then

            lnk = CType(e.Item.FindControl("lnkTotalowing"), HtmlAnchor)
            If lnk IsNot Nothing Then
                lnk.Attributes.Add("onmouseover", "this.style.cursor='hand'")
                lnk.Attributes.Add("onclick", "window.open('" & Me.URLDisplayPopup(DataBinder.Eval(e.Item.DataItem, "ChgDetId")) & "','CustomerSumm','ScrollBars=yes,Toolbars=no,top=0,left=0,height=550,width=680')")
            End If

            Dim ModRate As String = DataBinder.Eval(e.Item.DataItem, "ModRate")
            objtxt = CType(e.Item.FindControl("txtChgDetRate"), TextBox)
            If ModRate = "1" Then
                If objtxt IsNot Nothing Then
                    objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetRate")
                    objtxt.Enabled = True
                End If
            Else
                If objtxt IsNot Nothing Then
                    objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetRate")
                    objtxt.Enabled = False
                End If
            End If


            Dim ModQTY As String = DataBinder.Eval(e.Item.DataItem, "ModQty")
            objtxt = CType(e.Item.FindControl("txtChgDetQty"), TextBox)
            If ModQTY = "1" Then
                If objtxt IsNot Nothing Then
                    objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetQty")
                    objtxt.Enabled = True
                End If
            Else
                If objtxt IsNot Nothing Then
                    objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetQty")
                    objtxt.Enabled = False
                End If
            End If

            Dim NewModifiable As String = DataBinder.Eval(e.Item.DataItem, "NewModifiable")
            objtxt = CType(e.Item.FindControl("TxtChgDetRateOverRide"), TextBox)
            If objtxt IsNot Nothing Then
                objtxt.Attributes.Add("onchange", "document.getElementById('" & CType(e.Item.FindControl("txtChgDetTotOverRide"), TextBox).ClientID & "').value='';")
            End If

            Dim rateTxt As String = DataBinder.Eval(e.Item.DataItem, "ChgDetRateOverRide")
            Dim rateType As String = DataBinder.Eval(e.Item.DataItem, "ChgDetOverRideType")
            ddlrate = CType(e.Item.FindControl("ddlrate"), DropDownList)
            If NewModifiable = "1" Then
                If objtxt IsNot Nothing Then

                    If rateType = "Flex" Then
                        objtxt.Text = rateTxt
                        ddlrate.SelectedIndex = 2
                    Else
                        objtxt.Text = rateTxt
                        If rateType = "Rate" Then
                            ddlrate.SelectedIndex = 0
                        Else
                            ddlrate.SelectedIndex = 1
                        End If
                    End If
                    objtxt.Enabled = True
                End If
            Else
                If objtxt IsNot Nothing Then
                    If rateType = "Flex" Then
                        objtxt.Text = rateTxt
                        ddlrate.SelectedIndex = 2

                    Else
                        objtxt.Text = rateTxt
                        If rateType = "Rate" Then
                            ddlrate.SelectedIndex = 0
                        Else
                            ddlrate.SelectedIndex = 1
                        End If
                    End If
                    objtxt.Enabled = False
                End If
            End If

            Dim PkgType As String = DataBinder.Eval(e.Item.DataItem, "PkgType")
            Dim ChgDetBpdIsVeh As String = CType(ViewState("BpdIsVeh"), String)

            If PkgType = "Flex" AndAlso ChgDetBpdIsVeh = "1" Then
                ddlrate.Items.Add(New ListItem("Flex"))
                If rateType = "Flex" Then
                    ddlrate.SelectedIndex = 2
                    ddlrate.SelectedIndex = -1
                End If
            End If

            Dim IsCusChg As String = DataBinder.Eval(e.Item.DataItem, "IsCusChg")

            objtxt = CType(e.Item.FindControl("TxtChgDetAgnDisOverRide"), TextBox)
            If IsCusChg = "1" Then
                If objtxt IsNot Nothing Then

                    objtxt.Text = "%" & CType(DataBinder.Eval(e.Item.DataItem, "ChgDetAgnDisOverRide"), String)
                    objtxt.Enabled = True
                End If
            Else
                If objtxt IsNot Nothing Then

                    If NewModifiable = "1" Then
                        objtxt.Text = "%" & CType(DataBinder.Eval(e.Item.DataItem, "ChgDetAgnDisOverRide"), String)
                        objtxt.Enabled = True
                    Else
                        objtxt.Text = "%" & CType(DataBinder.Eval(e.Item.DataItem, "ChgDetAgnDisOverRide"), String)
                        objtxt.Enabled = False
                    End If

                End If
            End If


            objtxt = CType(e.Item.FindControl("txtChgDetTotOverRide"), TextBox)
            If objtxt IsNot Nothing Then
                objtxt.Attributes.Add("onchange", "document.getElementById('" & CType(e.Item.FindControl("TxtChgDetRateOverRide"), TextBox).ClientID & "').value='';")
            End If
            If IsCusChg = "1" Then
                If NewModifiable = "1" Then
                    If objtxt IsNot Nothing Then

                        objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetTotOverRide")
                        objtxt.Enabled = True
                    End If
                Else
                    If objtxt IsNot Nothing Then
                        objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetTotOverRide")
                        objtxt.Enabled = False
                    End If
                End If

            Else
                If objtxt IsNot Nothing Then
                    If NewModifiable = "1" Then
                        If objtxt IsNot Nothing Then

                            objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetTotOverRide")
                            objtxt.Enabled = True
                        End If
                    Else
                        If objtxt IsNot Nothing Then
                            objtxt.Text = DataBinder.Eval(e.Item.DataItem, "ChgDetTotOverRide")
                            objtxt.Enabled = False
                        End If
                    End If
                End If
            End If


        End If
    End Sub

    Protected Sub btnShowChargeDetails_Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowChargeDetails_Cancel.Click
        If CType(ViewState("PromptQty"), String) = "0" Then
            Me.WrapShowChargeDetails("")
        Else
            Me.WrapShowChargeDetails("", True)
        End If

    End Sub

    Sub AddFooterProductID()
        Dim ctr As Integer = 0
        Dim xmltemp As New XmlDocument
        Dim tempString As String

        ''--------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''--------------------------------------------------------------------------
        ''If Session(SESSION_saveForDetail) Is Nothing Then Exit Sub
        ''tempString = CType(Session(SESSION_saveForDetail), String)


        If String.IsNullOrEmpty(SaveForDetail) Then Exit Sub
        tempString = SaveForDetail

        ''--------------------------------------------------------------------------


        tempString = tempString.Replace("<Data>", "<data>")
        tempString = tempString.Replace("</Data>", "</data>")
        Dim temp As String = ""
        If tempString.StartsWith("<") = False Then
            Dim index As Integer = tempString.IndexOf("<")
            temp = tempString.Remove(0, index)
            index = temp.LastIndexOf(">")
            temp = temp.Substring(0, index + 1)
            xmltemp.LoadXml(temp)
        Else
            xmltemp.LoadXml(tempString)
        End If

        Dim nodes As XmlNodeList = xmltemp.SelectNodes("data/Rental")
        Dim node As XmlNode
        Dim pickercontrol As UserControls_PickerControl
        Dim row As System.Web.UI.HtmlControls.HtmlTableRow = Nothing

        ''rev:mia  sept25
        'pickercontrol = GridView1.FooterRow.FindControl("PickerControlAllProductA")
        'pickercontrol.Text = node("PrdShortName").InnerText
        'pickercontrol.DataId = node("PrdId").InnerText
        'pickercontrol.Param2 = node("CtyCode").InnerText

        For Each node In nodes
            If node.ChildNodes.Count = 7 Or node.ChildNodes.Count = 3 Then


                If ctr = 0 Then
                    pickercontrol = GridView1.FooterRow.FindControl("PickerControlAllProductA")
                    pickercontrol.Text = node("PrdShortName").InnerText
                    pickercontrol.DataId = node("PrdId").InnerText
                    pickercontrol.Param2 = node("CtyCode").InnerText
                    ctr = ctr + 1
                Else
                    If ctr = 1 Then
                        pickercontrol = GridView1.FooterRow.FindControl("PickerControlAllProductB")
                        pickercontrol.Text = node("PrdShortName").InnerText
                        pickercontrol.DataId = node("PrdId").InnerText
                        pickercontrol.Param2 = node("CtyCode").InnerText
                        ctr = ctr + 1
                    Else

                        pickercontrol = GridView1.FooterRow.FindControl("PickerControlAllProduct" & ctr - 2)
                        row = CType(GridView1.FooterRow.FindControl("trPickerControlAllProduct" & ctr - 2), System.Web.UI.HtmlControls.HtmlTableRow)
                        pickercontrol.Text = node("PrdShortName").InnerText
                        pickercontrol.DataId = node("PrdId").InnerText
                        ''rev:mia oct.3
                        If node("CtyCode") IsNot Nothing Then
                            If String.IsNullOrEmpty(node("CtyCode").InnerText) = False Then
                                pickercontrol.Param2 = node("CtyCode").InnerText
                            End If
                        End If
                        row.Visible = True
                        pickercontrol.Visible = True
                        ctr = ctr + 1


                    End If
                End If
            End If
        Next


        ''--------------------------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''--------------------------------------------------------------------------
        ''Me.hashproperties(SESSION_XMLBookedProductList) = xmltemp
        ''Session(SESSION_hashproperties) = Me.hashproperties

        XMLBookedProductListData = xmltemp.OuterXml
        ''--------------------------------------------------------------------------



    End Sub
#End Region

#Region "REV:mia nov24-adding supervisor override"
    Private Function ProductCancelledIdentifier() As String
        Return Aurora.Common.Data.ExecuteScalarSP("RES_ProductCancelledIdentifier", CityCode).ToString
    End Function
    Protected Sub btnShowChargeDetails_Save_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowChargeDetails_Save.Click
        If Me.ValidateData = False Then Exit Sub

        If (Server.HtmlEncode(booPrd).Contains(ProductCancelledIdentifier)) Then
            SupervisorOverrideControl1.Show(Param:="CCOVR")
        ElseIf RateFromTable = True Then
            If ButtonForPPFUsers() = True Then

                SupervisorOverrideControl1.Show(Param:="PPFMGTOVR")
            End If
        Else
            Me.SaveChargeDetails()
        End If
    End Sub

    Protected Sub SupervisorOverrideControl1_PostBack(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBack
        If isOkButton Then
            Dim sReturnMsg As String = ""
            ''sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, "CCOVR")
            sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, SupervisorOverrideControl1.Param)
            If (sReturnMsg) = "True" Then
                Me.SaveChargeDetails()
            Else
                MyBase.CurrentPage.SetErrorShortMessage(sReturnMsg)
            End If
        End If
    End Sub
#End Region

#Region "rev:mia december 23 fixing adding cancel problem"

    Sub AddCancelData()
        If Me.XMLBookedProductList.OuterXml = "" Then
            Me.XMLBookedProductList.LoadXml(Server.HtmlDecode(Me.litXMLBookedProductList.Text))
        End If
        Dim nodes As XmlNodeList = Me.XMLBookedProductList.SelectNodes("data/Rental")
        Dim ctr As Integer = 0
        For Each node As XmlNode In nodes

            If node.Attributes("id") IsNot Nothing AndAlso node.SelectSingleNode("RntNum") IsNot Nothing Then
                ''do nothing
            ElseIf node.Attributes("id") IsNot Nothing AndAlso node.SelectSingleNode("RntNum") Is Nothing Then
                If ctr = 0 Then
                    node.SelectSingleNode("PrdId").InnerText = Me.PickerControlAllProductC.DataId
                    node.SelectSingleNode("PrdShortName").InnerText = Me.PickerControlAllProductC.Text
                    ctr = 1
                ElseIf ctr = 1 Then
                    node.SelectSingleNode("PrdId").InnerText = Me.PickerControlAllProductD.DataId
                    node.SelectSingleNode("PrdShortName").InnerText = Me.PickerControlAllProductD.Text
                End If
            End If
        Next
    End Sub

    Private Property BookedProductsPickerCounter() As Integer
        Get
            Return CInt(ViewState("BookedProductsPickerCounter"))
        End Get
        Set(ByVal value As Integer)
            ''Dim tempvalue As Integer = CInt(ViewState("BookedProductsPickerCounter"))
            ViewState("BookedProductsPickerCounter") = value ''CInt(tempvalue + value)
        End Set
    End Property

    Protected Sub ButNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ButNew.Click
        Call AddNewBpd()

        If tblBookedProducts.Visible Then
            If BookedProductsPickerCounter > 10 Then Exit Sub
            BookedProductsPickerCounter = BookedProductsPickerCounter + 1
            For i As Integer = 1 To BookedProductsPickerCounter
                Dim row As HtmlTableRow = CType(tblBookedProducts.FindControl("tblBookedProducts_tr" & i), HtmlTableRow)
                If row IsNot Nothing Then
                    row.Visible = True
                    Dim Picker As UserControls_PickerControl = CType(row.FindControl("PickerControl_tr" & i), UserControls_PickerControl)
                    If Picker IsNot Nothing Then
                        Picker.Param2 = MyBase.CurrentPage.CountryCode
                        Picker.Param3 = MyBase.RentalId
                        Picker.EnableViewState = False
                    End If
                End If
            Next
        Else
            Call DisplayAdditionPickerControl()
        End If
    End Sub

    Sub HideDefaultRows()
        For i As Integer = 1 To 10
            Dim row As HtmlTableRow = CType(tblBookedProducts.FindControl("tblBookedProducts_tr" & i), HtmlTableRow)
            If row IsNot Nothing Then
                row.Visible = False
                Dim Picker As UserControls_PickerControl = CType(row.FindControl("PickerControl_tr" & i), UserControls_PickerControl)
                If Picker IsNot Nothing Then
                    Picker.Param2 = MyBase.CurrentPage.CountryCode
                    Picker.Param3 = MyBase.RentalId
                    Picker.EnableViewState = False
                End If
            End If
        Next
    End Sub

    Protected Sub GetIDfromPopUpDynamic()
        Me.GetLocalSession()
        ''If Me.XMLBookedProductList.SelectSingleNode("data/Rental/BpdId").InnerText = "" Then Exit Sub

        Dim ucLocalA As UserControls_PickerControl = Nothing
        Dim ucLocalB As UserControls_PickerControl = Nothing
        Dim fotRow As GridViewRow = GridView1.FooterRow

        ''REV:MIA DEC23 -- tblBookedProducts is a dummy grid that visible when no data is receive
        Dim tblfooter As HtmlTable = Nothing
        If Not tblBookedProducts.Visible Then
            tblfooter = CType(fotRow.FindControl("tblfooter"), HtmlTable)
        End If

        If fotRow Is Nothing Then
            AddNewBpd()
            AddCancelData()
            Exit Sub
        End If


        If Me.XMLBookedProductList.OuterXml = "" Then
            Me.XMLBookedProductList.LoadXml(Server.HtmlDecode(Me.litXMLBookedProductList.Text))
        End If
        Dim nodes As XmlNodeList = Me.XMLBookedProductList.SelectNodes("data/Rental")
        Dim ctr As Integer = 0
        Dim ii As Integer = 0
        For Each node As XmlNode In nodes

            If node.Attributes("id") IsNot Nothing AndAlso node.SelectSingleNode("RntNum") IsNot Nothing Then
                ''do nothing
            ElseIf node.Attributes("id") IsNot Nothing AndAlso node.SelectSingleNode("RntNum") Is Nothing Then
                If ctr = 0 Then
                    ucLocalA = CType(fotRow.FindControl("PickerControlAllProductA"), UserControls_PickerControl)
                    node.SelectSingleNode("PrdId").InnerText = ucLocalA.DataId
                    node.SelectSingleNode("PrdShortName").InnerText = ucLocalA.Text
                    ctr = 1
                ElseIf ctr = 1 Then
                    ucLocalB = CType(fotRow.FindControl("PickerControlAllProductB"), UserControls_PickerControl)
                    node.SelectSingleNode("PrdId").InnerText = ucLocalB.DataId
                    node.SelectSingleNode("PrdShortName").InnerText = ucLocalB.Text
                End If

            Else
                If CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl) IsNot Nothing AndAlso CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl).Visible = True Then
                    Dim picker As UserControls_PickerControl = CType(fotRow.FindControl("PickerControlAllProduct" & ii), UserControls_PickerControl)
                    node.SelectSingleNode("PrdId").InnerText = picker.DataId
                    node.SelectSingleNode("PrdShortName").InnerText = picker.Text
                    ii += 1
                End If
            End If

        Next
    End Sub

    Function IsControlHasValue() As Boolean
        Const MSG_NO_VALUES As String = "Please enter product(s) before clicking Save Button"
        Const MSG_NO_VALUES_FIRST As String = "Please enter value on the 'FIRST' product field before clicking Save Button"
        Const MSG_NO_VALUES_SECOND As String = "Please enter value on the 'SECOND' product field before clicking Save Button"
        If tblBookedProducts.Visible Then
            If BookedProductsPickerCounter = 0 Or BookedProductsPickerCounter > 0 Then
                If String.IsNullOrEmpty(PickerControlAllProductC.Text) Then
                    MyBase.CurrentPage.SetWarningShortMessage(MSG_NO_VALUES_FIRST)
                    Return False
                End If

                If BookedProductsPickerCounter > 0 Then
                    For i As Integer = 1 To BookedProductsPickerCounter
                        Dim row As HtmlTableRow = CType(tblBookedProducts.FindControl("tblBookedProducts_tr" & i), HtmlTableRow)
                        If row IsNot Nothing Then
                            Dim Picker As UserControls_PickerControl = CType(row.FindControl("PickerControl_tr" & i), UserControls_PickerControl)
                            If Picker IsNot Nothing Then
                                If Not String.IsNullOrEmpty(Picker.Text) AndAlso String.IsNullOrEmpty(PickerControlAllProductD.Text) Then
                                    MyBase.CurrentPage.SetWarningShortMessage(MSG_NO_VALUES_SECOND)
                                    Return False
                                End If
                                If i > 0 Then
                                    Dim PickerPrev As UserControls_PickerControl = CType(row.FindControl("PickerControl_tr" & i - 1), UserControls_PickerControl)
                                    If PickerPrev IsNot Nothing Then
                                        If Not String.IsNullOrEmpty(Picker.Text) AndAlso String.IsNullOrEmpty(PickerPrev.Text) Then
                                            MyBase.CurrentPage.SetWarningShortMessage(MSG_NO_VALUES_SECOND)
                                            Return False
                                        End If
                                    End If
                                End If
                                If i < BookedProductsPickerCounter Then
                                    If (i + 1) <= BookedProductsPickerCounter Then
                                        Dim PickerNext As UserControls_PickerControl = CType(row.FindControl("PickerControl_tr" & i + 1), UserControls_PickerControl)
                                        If PickerNext IsNot Nothing Then
                                            If Not String.IsNullOrEmpty(Picker.Text) AndAlso String.IsNullOrEmpty(PickerNext.Text) Then
                                                MyBase.CurrentPage.SetWarningShortMessage(MSG_NO_VALUES)
                                                Return False
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next

                End If

            End If
        End If
        Return True
    End Function
#End Region

#Region "rev:mia jan 30 2009 -- Changes from session to viewstate"
    Private Property ShowChargesDetails() As String
        Get
            Return ViewState("ShowChargesDetails")
        End Get
        Set(ByVal value As String)
            ViewState("ShowChargesDetails") = value
        End Set
    End Property

    Private Property ShowChargesData() As String
        Get
            Return ViewState("ShowChargesData")
        End Get
        Set(ByVal value As String)
            ViewState("ShowChargesData") = value
        End Set
    End Property

    Public Property SaveForDetail() As String
        Get
            Return ViewState("SaveForDetail")
        End Get
        Set(ByVal value As String)
            ViewState("SaveForDetail") = value
        End Set
    End Property

    Private Property XMLBookedProductListData() As String
        Get
            Return ViewState("XMLBookedProductListData")
        End Get
        Set(ByVal value As String)
            ViewState("XMLBookedProductListData") = value
        End Set
    End Property

    Public ReadOnly Property QSstring() As String
        Get
            Dim qstring As String = QS_BPDDETAILLIS & "&hdBookingId=" & MyBase.BookingId
            ''Dim qstring As String = "&hdBookingId=" & MyBase.BookingId

            If String.IsNullOrEmpty(Request.QueryString("sTxtSearch")) Then
                qstring = qstring & "&hdRentalId=" & MyBase.RentalId & "&sTxtSearch="
            Else
                qstring = qstring & "&hdRentalId=" & MyBase.RentalId & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString()
            End If
            Return qstring
        End Get
    End Property

    Public Property BpdIDDforDecryptionData() As String
        Get
            Return ViewState("BpdIDDforDecryptionData")
        End Get
        Set(ByVal value As String)
            ViewState("BpdIDDforDecryptionData") = value
        End Set
    End Property
#End Region


#Region "rev:PPF changes"

    Private Function CheckCSRRole(ByVal id As String, ByVal role As String) As String
        Dim sReturnMsg As String = "Error"
        Dim result As String = Aurora.Common.Data.ExecuteScalarSP("dialog_checkUserForCSR", id, role)
        Dim oXmlDoc As New System.Xml.XmlDocument
        oXmlDoc.LoadXml(result)
        If Not oXmlDoc.DocumentElement.SelectSingleNode("/Error") Is Nothing Then
            sReturnMsg = oXmlDoc.DocumentElement.SelectSingleNode("/Error").InnerText
        Else
            If Not oXmlDoc.DocumentElement.SelectSingleNode("/isUser") Is Nothing Then
                If CBool(oXmlDoc.DocumentElement.SelectSingleNode("/isUser").InnerText) Then
                    sReturnMsg = "True"
                End If
            End If
        End If
        Return sReturnMsg
    End Function

    Function ButtonForPPFUsers() As Boolean

        Dim sreturnmsg As String = CheckCSRRole(MyBase.CurrentPage.UserCode, "PPFMGTOVR")
        If (sreturnmsg.Equals("True")) Then
            Return True
        End If
        MyBase.CurrentPage.SetErrorShortMessage(sreturnmsg)
        Return False
    End Function
#End Region


#Region "rev:mia 30jan2015-addition of supervisor override password"

    Function CheckForProductsBeingDeletedIsExtension(xmldata As String) As Boolean
        Dim xmldom As New XmlDocument
        xmldom.LoadXml(xmldata)
        For Each oNode As XmlNode In xmldom.DocumentElement.ChildNodes
            If Not IsNothing(oNode.SelectSingleNode("Cancel")) AndAlso oNode.SelectSingleNode("Cancel").InnerText = 1 AndAlso oNode.SelectSingleNode("Typ").InnerText = "EXT" Then
                BpdIdExtension = oNode.SelectSingleNode("BpdId").InnerText
                NextCheckOutData = xmldom.SelectSingleNode("/data/Rental[PrdShortName='" + oNode.SelectSingleNode("PrdShortName").InnerText + "' and Typ != 'EXT']/FrDate").InnerText
                Return True
            End If
        Next
        Return False
    End Function

    Private Property NextCheckOutData As String
        Get
            Return ViewState("NextCheckOutData")
        End Get
        Set(value As String)
            ViewState("NextCheckOutData") = value
        End Set
    End Property

    Private Property BpdIdExtension As String
        Get
            Return ViewState("BpdIdExtension")
        End Get
        Set(value As String)
            ViewState("BpdIdExtension") = value
        End Set
    End Property

    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property

    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property

    Sub GetDataForSaving()
        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml(Me.XMLBookedProductListData)
        ''--------------------------------------------------------------------------

        Dim nScreenBooIntNo, nScreenRntIntNo As Integer

        With xmlTemp.DocumentElement.SelectSingleNode("Rental")
            If .SelectSingleNode("BooIntNo") Is Nothing Then
                ' use Booking/BooIntNo
                nScreenBooIntNo = CInt(.SelectSingleNode("Booking/BooIntNo").InnerText)
            Else
                ' use BooIntNo
                nScreenBooIntNo = CInt(.SelectSingleNode("BooIntNo").InnerText)
            End If
            nScreenRntIntNo = CInt(.SelectSingleNode("IntNo").InnerText)
        End With
        xmlTemp = Nothing
        If Not Aurora.Booking.Services.Booking.CheckIntegrity(Me.BookingId, nScreenRntIntNo, nScreenBooIntNo, Me.RentalId) Then
            CurrentPage.AddErrorMessage("This rental has been modified by another user. Please refresh the screen.")
            Exit Sub
        End If

        Call GetIDfromPopUpDynamic()
        Call GetXMLChangesFromGrid()

        If Not IsControlHasValue() Then Exit Sub

        If todelete = False Then
            Call SubmitDataForProduct(String.Empty)
        Else
            Call SubmitDataForProduct(0)
        End If

        PickerCounter = -1

    End Sub

    Sub PreSave()

        GetDataForSaving()

        Dim xmldata As String = Me.XMLBookedProductList.OuterXml
        Dim isExtProductDeleted As Boolean = CheckForProductsBeingDeletedIsExtension(xmldata)
        If (isExtProductDeleted) Then
            Dim newCheckOutDate As Date = Aurora.Common.Utility.ParseDateTime(NextCheckOutData, Aurora.Common.Utility.SystemCulture)

            If (SlotAvailableControl.IsSlotRequired(Nothing, RentalId, BpdIdExtension, PickupDate:=newCheckOutDate) = True) Then
                SlotAvailableControl.NoOfBulkBookings = 1 ''set to one
                SlotAvailableControl.SupervisorOverride = False
                SlotAvailableControl.Show(Nothing, RentalId, BpdIdExtension, PickupDate:=newCheckOutDate)
            Else
                Save()
            End If
        Else
            Save()
        End If
    End Sub

    Sub Save()

        Dim qstring As String = QS_BPDDETAILLIS & "&hdBookingId=" & MyBase.BookingId

        If String.IsNullOrEmpty(Request.QueryString("sTxtSearch")) Then
            qstring = qstring & "&hdRentalId=" & MyBase.RentalId & "&sTxtSearch="
        Else
            qstring = qstring & "&hdRentalId=" & MyBase.RentalId & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString()
        End If

        Response.Redirect(qstring)
    End Sub

    Protected Sub SlotAvailableControl_PostBack(sender As Object, isSaveButton As Boolean, param As String) Handles SlotAvailableControl.PostBack
        If (isSaveButton) Then
            SlotId = param.Split("|")(1)
            SlotDescription = param.Split("|")(2)
            Dim sessionvalue As String = Session(SESSION_saveForDetail)
            sessionvalue = String.Concat(sessionvalue, "|", SlotId, "|", SlotDescription)
            Session(SESSION_saveForDetail) = sessionvalue
            Logging.LogDebug("BookingProductUserControl SlotAvailableControl_PostBack", "SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
            Save()
        End If
    End Sub
#End Region

End Class
