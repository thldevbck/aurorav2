''  Change Log
''  15.9.8   -   Shoel - branch picker no longer restricts locations to user's country
''  25.9.8   -   Shoel - Fixes to carry the vehicle details to the incident mgt page + tabout foucs fixes for ajax postback focus loss
'' 21.10.8   -   Shoel - Fixes for time comparison :)
'' 3.5.10    -   Shoel - Call : 23532 - Changeover to already forced unit- add pop up with override required (validate the unit number - 23532 - Changeover to already forced unit- add pop up with override required)

Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_BookingExchangeUserControl
    Inherits BookingUserControl

    Delegate Sub AddNoteRatePopupPostBackObject(ByVal isLeftButton As Boolean, ByVal param As Object)

    Public isFirstLoad As Boolean = False

    Public Const BookingExchange_Exchange_SessionName As String = "BookingExchange_Exchange"

    Dim role As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking change over tab.")
        End Try
    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then

                printButton.Attributes.Add("onclick", "ShowAgreement('" & RentalId & "$" & BookingId & "$" & CurrentPage.UserCode & "');")

                typeDataBind()
                'exchangeDataBind()
                reVehiclePickerControl.ReadOnly = True
                exchangeDataBindXmlDoc()


                'reUnitTextBox.Text = "78052"
                'reRegTextBox.Text = "UIU25"
                'reOdoOutTextBox.Text = "29389"


            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking change over tab.")
        End Try
    End Sub

    Private _xmlDoc As XmlDocument
    Public Property xmlDoc() As XmlDocument
        Get
            If _xmlDoc Is Nothing Then
                If Session.Item(BookingExchange_Exchange_SessionName) Is Nothing Then
                    _xmlDoc = BookingExchange.GetExchangeDataXmlDoc(BookingId, RentalId, CurrentPage.UserCode)
                    Session.Add(BookingExchange_Exchange_SessionName, _xmlDoc)
                Else
                    _xmlDoc = Session.Item(BookingExchange_Exchange_SessionName)
                End If

            End If
            Return _xmlDoc

        End Get
        Set(ByVal value As XmlDocument)
            _xmlDoc = value

            Session.Remove(BookingExchange_Exchange_SessionName)
            Session.Add(BookingExchange_Exchange_SessionName, value)

        End Set
    End Property

    Private Sub typeDataBind()

        Dim ds As DataSet
        ds = Aurora.Common.Data.GetPopUpData("PRDTYPE4EXCH", "", "", "", "", "", CurrentPage.UserCode)
        reTypeDropDownList.DataSource = ds.Tables(0)
        reTypeDropDownList.DataValueField = "CODE"
        reTypeDropDownList.DataTextField = "DESCRIPTION"
        reTypeDropDownList.DataBind()

    End Sub


    'Private Sub exchangeDataBind()

    '    Dim dt As DataTable
    '    dt = BookingExchange.GetExchangeData(BookingId, RentalId, CurrentPage.UserCode)

    '    Dim status As String
    '    status = dt.Rows(0).Item("RntStCode")
    '    statusCodeLabel.Text = dt.Rows(0).Item("RntStCode")

    '    statusLabel.Text = dt.Rows(0).Item("RntStDes")

    '    'exchange detils
    '    exchangeTypeDropDownList.SelectedValue = dt.Rows(0).Item("ExchangeTyp")
    '    branchPickerControl.Param1 = CurrentPage.CountryCode
    '    branchPickerControl.DataId = CurrentPage.LocationCode
    '    branchPickerControl.Text = CurrentPage.LocationCode
    '    currDateControl.Date = Utility.ParseDateTime(dt.Rows(0).Item("CurrDt"), Utility.systemCulture)
    '    currTimeControl.Text = dt.Rows(0).Item("CurrTm")

    '    'Current vehicle
    '    unitNumLabel.Text = dt.Rows(0).Item("UnitNum")
    '    regLabel.Text = dt.Rows(0).Item("RegNum")
    '    vehicleLabel.Text = dt.Rows(0).Item("Vehicle")
    '    odoOutLabel.Text = dt.Rows(0).Item("OdoOut")
    '    odoInTextBox.Text = dt.Rows(0).Item("OdoIn")
    '    vehicleOtherLabel.Text = dt.Rows(0).Item("Vehicle")

    '    'Replacement vehicle
    '    reUnitTextBox.Text = dt.Rows(0).Item("RUnitNum")
    '    reRegTextBox.Text = dt.Rows(0).Item("RRegNum")
    '    reVehicleLabel.Text = dt.Rows(0).Item("RVehicle")
    '    reOdoOutTextBox.Text = dt.Rows(0).Item("ROdoOut")

    '    'reTypeDropDownList
    '    reVehiclePickerControl.Param3 = CurrentPage.CountryCode
    '    reVehiclePickerControl.Param4 = RentalId

    '    If status = "CO" Then
    '        vehiclePanel.Visible = True
    '        vehicleOtherPanel.Visible = False
    '        replacementPanel.Visible = True
    '        replacementOtherPanel.Visible = False
    '    Else
    '        vehiclePanel.Visible = False
    '        vehicleOtherPanel.Visible = True
    '        replacementPanel.Visible = False
    '        replacementOtherPanel.Visible = True
    '    End If

    '    If Not (status = "CO" Or status = "KK" Or status = "NN") Then
    '        saveButton.Enabled = False
    '        printButton.Enabled = False
    '    End If

    '    'hidden field
    '    crossHrCodeLabel.Text = dt.Rows(0).Item("CrossHrCode")
    '    crossHireTextBox.Text = dt.Rows(0).Item("CrossHire")
    '    isSupervisorLabel.Text = dt.Rows(0).Item("UviOddo")
    '    noteTextLabel.Text = dt.Rows(0).Item("NoteText")
    '    rateLabel.Text = dt.Rows(0).Item("Rate")


    'End Sub

    Private Sub exchangeDataBindXmlDoc()

        xmlDoc = BookingExchange.GetExchangeDataXmlDoc(BookingId, RentalId, CurrentPage.UserCode)

        'Session.Remove(BookingExchange_Exchange_SessionName)
        'Session.Add(BookingExchange_Exchange_SessionName, xmlDoc)

        If Not String.IsNullOrEmpty(xmlDoc.InnerText) Then
            Dim status As String
            status = xmlDoc.SelectSingleNode("//data/Rental/RntStCode").InnerText
            'statusCodeLabel.Text = dt.Rows(0).Item("RntStCode")

            statusTextBox.Text = status & " - " & xmlDoc.SelectSingleNode("//data/Rental/RntStDes").InnerText
            statusTextBox.BackColor = Aurora.Common.DataConstants.GetBookingStatusColor(status)

            'exchange detils
            exchangeTypeDropDownList.SelectedValue = xmlDoc.SelectSingleNode("//data/Rental/ExchangeTyp").InnerText
            'branchPickerControl.Param1 = CurrentPage.CountryCode
            branchPickerControl.DataId = UserSettings.Current.LocCode
            branchPickerControl.Text = UserSettings.Current.LocCode
            currDateControl.Date = Utility.ParseDateTime(xmlDoc.SelectSingleNode("//data/Rental/CurrDt").InnerText, Utility.systemCulture)
            currTimeControl.Text = xmlDoc.SelectSingleNode("//data/Rental/CurrTm").InnerText

            'Current vehicle
            unitNumTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/UnitNum").InnerText
            regTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/RegNum").InnerText

            ''rev:mia feb17,2009 -- Vehicle its a buggy element..lets trap this out
            If xmlDoc.SelectSingleNode("//data/Rental/Vehicle") IsNot Nothing Then
                vehicleTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/Vehicle").InnerText
                vehicleOtherTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/Vehicle").InnerText
            End If


            odoOutTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/OdoOut").InnerText
            odoInTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/OdoIn").InnerText


            'Replacement vehicle
            reUnitTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/RUnitNum").InnerText
            reRegTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/RRegNum").InnerText
            reVehicleTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/RVehicle").InnerText
            reOdoOutTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/ROdoOut").InnerText

            'reTypeDropDownList
            reVehiclePickerControl.Param3 = CurrentPage.CountryCode
            reVehiclePickerControl.Param4 = RentalId

            If status = "CO" Then
                vehiclePanel.Visible = True
                vehicleOtherPanel.Visible = False
                replacementPanel.Visible = True
                replacementOtherPanel.Visible = False
            Else
                vehiclePanel.Visible = False
                vehicleOtherPanel.Visible = True
                replacementPanel.Visible = False
                replacementOtherPanel.Visible = True
            End If

            If Not (status = "CO" Or status = "KK" Or status = "NN") Then
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, "Exchanges can only occur when the Rental is Confirmed, Provisional, Checked-Out")
                saveButton.Enabled = False
                printButton.Enabled = False

                'Disable all the fields
                cancelButton.Enabled = False
                exchangeTypeDropDownList.Enabled = False
                branchPickerControl.ReadOnly = True
                currDateControl.ReadOnly = True
                currTimeControl.ReadOnly = True
                reTypeDropDownList.Enabled = False
                reVehiclePickerControl.ReadOnly = True


                Return
            End If

            'hidden field
            crossHrCodeLabel.Text = xmlDoc.SelectSingleNode("//data/Rental/CrossHrCode").InnerText
            crossHireTextBox.Text = xmlDoc.SelectSingleNode("//data/Rental/CrossHire").InnerText
            'isSupervisorLabel.Text = xmlDoc.SelectSingleNode("//data/Rental/UviOddo").InnerText
            'noteTextLabel.Text = xmlDoc.SelectSingleNode("//data/Rental/NoteText").InnerText
            'rateLabel.Text = xmlDoc.SelectSingleNode("//data/Rental/Rate").InnerText

            saveButton.Enabled = True
            printButton.Enabled = True
        Else
            vehiclePanel.Visible = False
            vehicleOtherPanel.Visible = True
            replacementPanel.Visible = False
            replacementOtherPanel.Visible = True

            saveButton.Enabled = False
            printButton.Enabled = False

            'Disable all the fields
            cancelButton.Enabled = False
            exchangeTypeDropDownList.Enabled = False
            branchPickerControl.ReadOnly = True
            currDateControl.ReadOnly = True
            currTimeControl.ReadOnly = True
            reTypeDropDownList.Enabled = False
            reVehiclePickerControl.ReadOnly = True


        End If

    End Sub

#Region "Event"
    Protected Sub reUnitTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reUnitTextBox.TextChanged
        validateUnitNumber()
        validateRegNum()
        ' focus fix by Shoel
        ScriptManager.GetCurrent(Page).SetFocus(reRegTextBox)
        ' focus fix by Shoel
    End Sub

    Protected Sub reRegTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reRegTextBox.TextChanged
        validateRegNum()
        ' focus fix by Shoel
        ScriptManager.GetCurrent(Page).SetFocus(reOdoOutTextBox)
        ' focus fix by Shoel
    End Sub

    'Protected Sub reOdoOutTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reOdoOutTextBox.TextChanged
    '    validateOdometerOut()
    'End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click
        Try

            If xmlDoc.SelectSingleNode("//data/Rental/RntStCode").InnerText = "CO" Then
                If Not currDateControl.IsValid Then
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in dd/mm/yyyy format.")
                    Return
                ElseIf Not currTimeControl.IsValid Then
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter time in hh:mm format.")
                    Return
                End If
            End If

            'Step 1
            getNoteRate()


        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while saving.")
        End Try
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click

        'refresh
        exchangeDataBindXmlDoc()

    End Sub

    Protected Sub printButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles printButton.Click

        'PrintAggrement

    End Sub

#End Region

    'Step 1
    Private Sub getNoteRate()

        Dim exchangeType As String
        exchangeType = exchangeTypeDropDownList.SelectedValue

        If exchangeType <> "Breakdown" And exchangeType <> "Accident" Then

            Dim noteText As String = xmlDoc.SelectSingleNode("//data/Rental/NoteText").InnerText 'noteTextLabel.Text
            Dim rate As String = xmlDoc.SelectSingleNode("//data/Rental/Rate").InnerText 'rateLabel.Text

            If String.IsNullOrEmpty(noteText) Or String.IsNullOrEmpty(rate) Then

                Dim noteRate As String = ""

                'Dim addNoteRatePopupPostBack As New AddNoteRatePopupPostBackObject(AddressOf Me.AddNoteRatePopupPostBack)
                'addNoteRatePopupUserControl1.PostBack1 = addNoteRatePopupPostBack

                addNoteRatePopupUserControl1.loadPopup()


                'var(NoteRate = ShowNoteRatePopup())

                'If Not String.IsNullOrEmpty(noteRate) Then
                '    'var rate = NoteRate.split('^')[0] 
                '    'var Note = NoteRate.split('^')[1] 
                '    'xmlDso.documentElement.selectSingleNode('Rental').selectSingleNode('NoteText').text = Note
                '    'xmlDso.documentElement.selectSingleNode('Rental').selectSingleNode('Rate').text = rate

                '    noteTextLabel.Text = ""
                '    rateLabel.Text = ""

                'Else
                '    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "Note & Rate is required when Exchange Type is Customer Request!!")
                '    Return False

                'End If
            Else
                'Step 2
                validation()
            End If
        Else
            'Step 2
            validation()
        End If
    End Sub

    'Step 2
    Private Sub validation(Optional ByVal SkipUnitNumberValidation As Boolean = False)

        If Not SkipUnitNumberValidation Then
            ' validate the unit number - 23532 - Changeover to already forced unit- add pop up with override required
            Dim xmlValidatedResult As XmlDocument = Aurora.OpsAndLogs.Services.PreCheckOutVehicleAssignment.ValidateOrGetUnitNumber(reUnitTextBox.Text.Trim(), _xmlDoc.DocumentElement.SelectSingleNode("Rental/CtyCode").InnerText.Trim().ToUpper())

            If ( _
                    xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/ErrorMessage") IsNot Nothing _
                    AndAlso _
                    Not String.IsNullOrEmpty(xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/ErrorMessage").InnerText) _
               ) _
               OrElse _
               xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/UnitNumber") Is Nothing _
               OrElse _
               String.IsNullOrEmpty(xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/UnitNumber").InnerText) _
            Then
                ' errors found...
                If xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/ErrorMessage").InnerText.ToLower().Contains("already been assigned") Then
                    ' already been assigned error, allow override with OLMAN
                    supervisorOverrideControl.Show(xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/ErrorMessage").InnerText, "OLMAN", "", "3")
                Else
                    CurrentPage.SetErrorMessage(xmlValidatedResult.DocumentElement.SelectSingleNode("Vehicle/ErrorMessage").InnerText)
                End If
                Exit Sub
            End If
            ' validate the unit number - 23532 - Changeover to already forced unit- add pop up with override required
        End If

        'If statusCodeLabel.Text = "CO" Then
        If xmlDoc.SelectSingleNode("//data/Rental/RntStCode").InnerText = "CO" Then
            If checkForMandatory() Then

                If checkOddometer() Then

                    validateOdometerOutStep1()
                End If

            End If

        Else
            saveExchange()
        End If

    End Sub


    Private Sub validateUnitNumber()
        If reUnitTextBox.Text <> "" Then
            If reUnitTextBox.Text = crossHrCodeLabel.Text Then
                crossHireTextBox.Text = "1"
            Else
                crossHireTextBox.Text = "0"
            End If
        End If
    End Sub

    Private Sub validateRegNum()
        Dim unit As String
        unit = reUnitTextBox.Text
        unit = unit.Replace(" ", "")
        Dim rego As String
        rego = reRegTextBox.Text
        rego = rego.Replace(" ", "")

        If (Not String.IsNullOrEmpty(unit)) And (Not String.IsNullOrEmpty(rego)) Then
            If crossHireTextBox.Text = "1" Then
                reVehicleTextBox.Text = "**CROSSHIRE Vehicle"
            Else
                'get vehicle

                Dim dt As DataTable
                ''rev:mia oct10
                dt = BookingExchange.GetAIMSVehicleName(rego, unit, RentalId, "", CurrentPage.UserCode)
                ''dt = BookingExchange.GetAIMSVehicleNameExchangeTab(rego, unit, RentalId, "", CurrentPage.UserCode)

                reVehicleTextBox.Text = dt.Rows(0)("Vehicle")

                reOdoOutTextBox.Text = dt.Rows(0)("OdoMeter")
                odoMeterTextBox.Text = dt.Rows(0)("OdoMeter")

                odoMeterDueTextBox.Text = dt.Rows(0)("OdoMeterDue")

                '<Vehicle>**Invalid Vehicle</Vehicle>
                '<OdoMeter></OdoMeter>
                '<OdoMeterDue></OdoMeterDue>
                '<RegoError>The Rego number does not match the AIMS record for this unit.Do you want to continue?</RegoError>
                '<Rego></Rego>
                '<UnAvail></UnAvail>

                If Not String.IsNullOrEmpty(dt.Rows(0)("UnAvail")) Then
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, dt.Rows(0)("UnAvail"))
                End If


            End If

        End If

        'var UnitNum = trimSpace(document.forms[0].txtRUnitNum.value)
        '	var RegoNum = trimSpace(document.forms[0].txtRRegoNum.value)
        '	document.forms[0].txtRRegoNum.value = RegoNum.toUpperCase()
        '	if(UnitNum!='' && RegoNum!=''){ 
        '		if(xmlDso.documentElement.childNodes[0].selectSingleNode('CrossHire').text == 1)
        '			xmlDso.documentElement.childNodes[0].selectSingleNode('RVehicle').text = "**CROSSHIRE Vehicle"
        '		else{ 
        '                window.status = "Fetching the Vehicle Detail from AIMS..."
        '			var UniNum = xmlDso.documentElement.childNodes[0].selectSingleNode('RUnitNum').text
        '			var RntId = xmlDso.selectSingleNode('/Data/Rental/RntId').text
        '                var(param = "Action=GetVehicle^Function=Exchange^param1=" + RegoNum + "^param2=" + UnitNum + "^param3=" + RntId)

        'var RetXML = getXmlDataNew("../asp/TabListner.asp",param) ; 
        '			var TempXML = new ActiveXObject('Microsoft.XMLDom')
        '                window.status = "Loading Vehicle Data..."
        '                TempXML.loadXML(RetXML)
        '                var(Msg = TempXML.documentElement.selectSingleNode("UnAvail").text)

        '			xmlDso.documentElement.childNodes[0].selectSingleNode('RVehicle').text = TempXML.documentElement.selectSingleNode("Vehicle").text
        '                OddoCheck = TempXML.documentElement.selectSingleNode("OdoMeterDue").text
        '                OdoMeter = TempXML.documentElement.selectSingleNode("OdoMeter").text

        '			document.forms[0].txtOddometerOut.value = OdoMeter
        '			if(Msg!=''){ 
        '                    alert(Msg)
        '				document.forms[0].txtOddometerOut.value = ''
        '				Obj.value = ''
        '                    Obj.focus()
        '			}
        '                    window.status = "Done."
        '		}
        '	}
    End Sub

    Private Function checkForMandatory() As Boolean

        Dim exchange As String = exchangeTypeDropDownList.SelectedValue
        Dim branch As String = branchPickerControl.DataId
        Dim dat As Date = currDateControl.Date
        Dim time As TimeSpan = currTimeControl.Time
        Dim odoIn As String = odoInTextBox.Text
        Dim reOdoOut As String = reOdoOutTextBox.Text
        Dim reRegNum As String = reRegTextBox.Text
        'Dim reVehicle As String = reVehicleLabel.Text

        If String.IsNullOrEmpty(exchange.Trim()) Or String.IsNullOrEmpty(branch.Trim()) Or _
                    String.IsNullOrEmpty(odoIn.Trim()) Or String.IsNullOrEmpty(reOdoOut.Trim()) Or _
                    String.IsNullOrEmpty(reRegNum.Trim()) Or dat = Date.MinValue Or _
                    time = TimeSpan.MinValue Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, CurrentPage.GetMessage("GEN005"))
            Return False

        Else
            Return True
        End If

        '   var vExchange = xmlDso.documentElement.childNodes[0].selectSingleNode('ExchangeTyp').text
        'var vBranch = xmlDso.documentElement.childNodes[0].selectSingleNode('Branch').text
        'var vDate = xmlDso.documentElement.childNodes[0].selectSingleNode('CurrDt').text
        'var vTime = xmlDso.documentElement.childNodes[0].selectSingleNode('CurrTm').text
        'var vOdoIn = xmlDso.documentElement.childNodes[0].selectSingleNode('OdoIn').text
        'var vROdoOut = xmlDso.documentElement.childNodes[0].selectSingleNode('ROdoOut').text
        'var vRRegNum = xmlDso.documentElement.childNodes[0].selectSingleNode('RRegNum').text
        'var vRVehicle = xmlDso.documentElement.childNodes[0].selectSingleNode('RVehicle').text
        'if(trimSpace(vExchange)=='' || trimSpace(vBranch)=='' || trimSpace(vDate)=='' || trimSpace(vTime)=='' || trimSpace(vOdoIn)=='' || trimSpace(vROdoOut)=='' || trimSpace(vRRegNum)==''){ 
        '	displayMessage(1,0,DispClientMessage('GEN005',''))
        '	return false;}
        'return true;}
    End Function

    Private Function checkOddometer() As Boolean

        Dim odoMeterOut As Integer = Utility.ParseInt(odoOutTextBox.Text, 0)
        Dim odoMeterIn As Integer = Utility.ParseInt(odoInTextBox.Text, 0)

        If odoMeterOut > odoMeterIn Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "The Odometer In reading cannot be less than the Odometer Out")
            Return False
        Else
            Return True
        End If

        '       var OddoMeterOut = xmlDso.documentElement.childNodes[0].selectSingleNode('OdoOut').text
        '    var OddoMeterIn = xmlDso.documentElement.childNodes[0].selectSingleNode('OdoIn').text

        '   if(parseFloat(OddoMeterOut)>parseFloat(OddoMeterIn)){ 
        '           displayMessage(1, 0, "The Odometer In reading cannot be less than the Odometer Out")
        '	return false;}
        'return true;}

    End Function

    Private Sub validateOdometerOutStep1()

        Dim rOdoOut As String
        rOdoOut = Utility.ParseInt(reOdoOutTextBox.Text, 0)
        Dim isSupervisor As Boolean

        'isSupervisor = Convert.ToBoolean(Utility.ParseInt(isSupervisorLabel.Text, 0))
        isSupervisor = Convert.ToBoolean(Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/UviOddo").InnerText, 0))

        Dim odoMeter As Integer
        odoMeter = Utility.ParseInt(odoMeterTextBox.Text, 0)

        If odoMeter > rOdoOut Then
            confimationBoxControl.Text = "The Oddometer reading is less then the last reading for replacement vehicle. Do you wish to override this error?"
            confimationBoxControl.Param = 1
            confimationBoxControl.Show()

            'If Not isSupervisor Then
            '    Dim ds As DataSet
            '    ds = Aurora.Common.Data.GetPopUpData("GETUVI", CurrentPage.UserCode, "CKOI_SUPROLE", "", "", "", CurrentPage.UserCode)
            '    Dim dt As DataTable
            '    dt = ds.Tables(0)

            '    If dt.Rows.Count > 0 And dt.Columns(0).ColumnName = "Error" Then
            '        CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "UviValue NZ/AU CKOI_SUPROLE Not Set")
            '        Return
            '    End If
            '    'TO do
            '    'strPopupRtn = GENCheckUsrRole( "(" + objTempXML.documentElement.text + ")<BR/>" + 'The Oddometer reading is less then the last reading for replacement vehicle.', document.forms[0].htm_hid_userCode.value, objTempXML.documentElement.text)
            '    'if (strPopupRtn == -1) 
            '    '   return false;
            'End If
            ''Else
            ''    Return False
            ''end if 

        Else
            validateOdometerOutStep2()
        End If

    End Sub

    Private Sub validateOdometerOutStep2()

        Dim rOdoOut As String
        rOdoOut = reOdoOutTextBox.Text

        If Not String.IsNullOrEmpty(odoMeterDueTextBox.Text) Then
            If Utility.ParseInt(rOdoOut, 0) >= Utility.ParseInt(odoMeterDueTextBox.Text, 0) Then
                confimationBoxControl.Text = "This replacement vehicle is now due for a service. Do you wish to override this?"
                confimationBoxControl.Param = 2
                confimationBoxControl.Show()

                'If Not isSupervisor Then
                '    Dim ds As DataSet
                '    ds = Aurora.Common.Data.GetPopUpData("GETUVI", CurrentPage.UserCode, "CKOI_SUPROLE", "", "", "", CurrentPage.UserCode)
                '    Dim dt As DataTable
                '    dt = ds.Tables(0)

                '    If dt.Rows.Count > 0 And dt.Columns(0).ColumnName = "Error" Then
                '        CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "UviValue NZ/AU CKOI_SUPROLE Not Set")
                '        Return
                '    End If
                '    'TO do
                '    'strPopupRtn = GENCheckUsrRole( "(" + objTempXML.documentElement.text + ")<BR/>" + 'This replacement vehicle is now due for a service.', document.forms[0].htm_hid_userCode.value, objTempXML.documentElement.text)
                '    'if (strPopupRtn == -1) 
                '    '   return false;
                'End If
                ''Else
                ''    Return False
                ''end if 
            Else
                saveExchange()
            End If
        Else
            saveExchange()
        End If



    End Sub

    'Step 3
    Private Sub saveExchange()

        'exchange detils
        xmlDoc.SelectSingleNode("//data/Rental/ExchangeTyp").InnerText = exchangeTypeDropDownList.SelectedValue
        xmlDoc.SelectSingleNode("//data/Rental/Branch").InnerText = branchPickerControl.DataId


        xmlDoc.SelectSingleNode("//data/Rental/CurrDt").InnerText = currDateControl.Date
        xmlDoc.SelectSingleNode("//data/Rental/CurrTm").InnerText = currTimeControl.Text

        Dim status As String = xmlDoc.SelectSingleNode("//data/Rental/RntStCode").InnerText
        If status = "CO" Then
            'Current vehicle
            xmlDoc.SelectSingleNode("//data/Rental/OdoIn").InnerText = odoInTextBox.Text


            'Replacement vehicle
            xmlDoc.SelectSingleNode("//data/Rental/RUnitNum").InnerText = reUnitTextBox.Text
            xmlDoc.SelectSingleNode("//data/Rental/RRegNum").InnerText = reRegTextBox.Text
            xmlDoc.SelectSingleNode("//data/Rental/RVehicle").InnerText = reVehicleTextBox.Text
            xmlDoc.SelectSingleNode("//data/Rental/ROdoOut").InnerText = reOdoOutTextBox.Text
            xmlDoc.SelectSingleNode("//data/Rental/VCurrDt").InnerText = xmlDoc.SelectSingleNode("//data/Rental/RntCkiWhen").InnerText.Split(" "c)(0)
            xmlDoc.SelectSingleNode("//data/Rental/VCurrTm").InnerText = xmlDoc.SelectSingleNode("//data/Rental/RntCkiWhen").InnerText.Split(" "c)(1)
        Else
            'reTypeDropDownList

            xmlDoc.SelectSingleNode("//data/Rental/RVehicle").InnerText = reVehiclePickerControl.DataId
            xmlDoc.SelectSingleNode("//data/Rental/VCurrDt").InnerText = xmlDoc.SelectSingleNode("//data/Rental/RntCkoWhen").InnerText.Split(" "c)(0)
            xmlDoc.SelectSingleNode("//data/Rental/VCurrTm").InnerText = xmlDoc.SelectSingleNode("//data/Rental/RntCkoWhen").InnerText.Split(" "c)(1)
            ''rev:mia 03-feb-2015- https://thlonline.atlassian.net/browse/AURORA-613-Change over Tab in Aurora error... e.g. W3921319 
            If (String.IsNullOrEmpty(xmlDoc.SelectSingleNode("//data/Rental/UnitNum").InnerText)) Then
                xmlDoc.SelectSingleNode("//data/Rental/UnitNum").InnerText = "0"
            End If
        End If

        'hidden field
        xmlDoc.SelectSingleNode("//data/Rental/CrossHire").InnerText = crossHireTextBox.Text

        'Save exchange 
        Dim errorMessage As String = ""

        errorMessage = BookingExchange.ManageBPDExchange(xmlDoc, "<F9NotReq>", CurrentPage.UserCode, "booking.aspx")

        If String.IsNullOrEmpty(errorMessage) Then
            If exchangeTypeDropDownList.SelectedValue = "Breakdown" Or exchangeTypeDropDownList.SelectedValue = "Accident" Then
                'Redirct to IncidentMgt.asp
                Response.Redirect("IncidentManagement.aspx?BooId=" & BookingId & _
                                                         "&RntId=" & RentalId & _
                                                         "&IncidentId=&BranchId=" & branchPickerControl.DataId & _
                                                         "&ActiveTab=" & BookingTabIndex.ChangeOver & _
                                                         "&IncidentType=" & exchangeTypeDropDownList.SelectedValue & _
                                                         "&UnitNum=" & xmlDoc.DocumentElement.FirstChild.SelectSingleNode("UnitNum").InnerText & _
                                                         "&RegNum=" & xmlDoc.DocumentElement.FirstChild.SelectSingleNode("RegNum").InnerText & _
                                                         "&OdometerIn=" & xmlDoc.DocumentElement.FirstChild.SelectSingleNode("OdoIn").InnerText)
            Else
                'Go to product tab
                CurrentPage.Response.Redirect("Booking.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&activeTab=" & BookingTabIndex.Products)
            End If
        Else
            'error
            'If dt.Rows(0)("ErrStatus") = "True" Then

            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            'End If
        End If

    End Sub




#Region "Popup postback"

    Protected Sub AddNoteRatePopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles addNoteRatePopupUserControl1.PostBack
        AddNoteRatePopupPostBack(leftButton, param)
    End Sub

    Public Sub AddNoteRatePopupPostBack(ByVal isLeftButton As Boolean, ByVal param As Object)

        If isLeftButton Then
            Dim list As ArrayList
            list = CType(param, ArrayList)

            xmlDoc.SelectSingleNode("//data/Rental/NoteText").InnerText = list.Item(0)
            xmlDoc.SelectSingleNode("//data/Rental/Rate").InnerText = list.Item(1)

            'Step 2
            validation()

        Else
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, "Note & Rate are required when Exchange Type is Customer Request!!")
        End If

    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        If leftButton Then

            Dim stepNo As Integer
            stepNo = Convert.ToInt32(param)

            Dim isSupervisor As Boolean
            'isSupervisor = isSupervisorLabel.Text
            isSupervisor = Convert.ToBoolean(Utility.ParseInt(xmlDoc.SelectSingleNode("//data/Rental/UviOddo").InnerText, 0))


            Select Case stepNo
                Case 1
                    If isSupervisor Then

                        'Dim ds As DataSet
                        'ds = Aurora.Common.Data.GetPopUpData("GETUVI", CurrentPage.UserCode, "CKOI_SUPROLE", "", "", "", CurrentPage.UserCode)
                        'Dim dt As DataTable
                        'dt = ds.Tables(0)

                        'If dt.Rows.Count > 0 And dt.Columns(0).ColumnName = "Error" Then
                        '    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "UviValue NZ/AU CKOI_SUPROLE Not Set")
                        '    Exit Sub
                        'End If

                        'something about "UVI" value
                        Dim oUviData As New XmlDocument
                        oUviData = BookingExchange.GetUVIData(CurrentPage.UserCode, "CKOI_SUPROLE")

                        If oUviData.DocumentElement.ChildNodes.Count > 0 And oUviData.DocumentElement.ChildNodes(0).BaseURI = "Error" Then
                            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "UviValue NZ/AU CKOI_SUPROLE Not Set")
                            Exit Sub
                        End If

                        role = oUviData.DocumentElement.InnerText

                        'TO do
                        'strPopupRtn = GENCheckUsrRole( "(" + objTempXML.documentElement.text + ")<BR/>" + 'The Oddometer reading is less then the last reading for replacement vehicle.', document.forms[0].htm_hid_userCode.value, objTempXML.documentElement.text)
                        'if (strPopupRtn == -1) 
                        '   return false;

                        'Session(SESSION_POPUPTYPE) = "POPUP1"
                        'Session(SESSION_USERROLE) = oUviData.DocumentElement.InnerText

                        supervisorOverrideControl.Show(ErrorMessage:="(" & oUviData.DocumentElement.InnerText & ")<BR/>" & "The Oddometer reading is less then the last reading for replacement vehicle.", Param:="1", Role:=role)


                        'Dim oSupervisorOverrideControl As UserControls_SupervisorOverrideUserControl
                        'oSupervisorOverrideControl = CurrentPage.Controls(0).FindControl("socSupervisorOverride")
                        'oSupervisorOverrideControl.Error()


                        'validateOdometerOutStep2()
                    End If

                Case 2
                    If isSupervisor Then
                        'Dim ds As DataSet
                        'ds = Aurora.Common.Data.GetPopUpData("GETUVI", CurrentPage.UserCode, "CKOI_SUPROLE", "", "", "", CurrentPage.UserCode)
                        'Dim dt As DataTable
                        'dt = ds.Tables(0)

                        'If dt.Rows.Count > 0 And dt.Columns(0).ColumnName = "Error" Then
                        '    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "UviValue NZ/AU CKOI_SUPROLE Not Set")
                        '    Exit Sub
                        'End If

                        'something about "UVI" value
                        Dim oUviData As New XmlDocument
                        oUviData = BookingExchange.GetUVIData(CurrentPage.UserCode, "CKOI_SUPROLE")

                        If oUviData.DocumentElement.ChildNodes.Count > 0 And oUviData.DocumentElement.ChildNodes(0).BaseURI = "Error" Then
                            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "UviValue NZ/AU CKOI_SUPROLE Not Set")
                            Exit Sub
                        End If

                        role = oUviData.DocumentElement.InnerText

                        supervisorOverrideControl.Show(ErrorMessage:="(" & oUviData.DocumentElement.InnerText & ")<BR/>" & "This replacement vehicle is now due for a service.", Param:="2", Role:=role)

                        'TO do
                        'strPopupRtn = GENCheckUsrRole( "(" + objTempXML.documentElement.text + ")<BR/>" + 'This replacement vehicle is now due for a service.', document.forms[0].htm_hid_userCode.value, objTempXML.documentElement.text)
                        'if (strPopupRtn == -1) 
                        '   return false;
                    End If

            End Select
        End If
    End Sub

    Public Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal param As String) Handles supervisorOverrideControl.PostBack
        If isOkButton Then
            Dim stepNo As Integer = CInt(param)
            Select Case stepNo
                Case 1
                    validateOdometerOutStep2()
                Case 2
                    saveExchange()
                Case 3
                    validation(True)
            End Select
        End If
    End Sub

#End Region

    Protected Sub reTypeDropDownList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles reTypeDropDownList.SelectedIndexChanged
        reVehiclePickerControl.Param2 = reTypeDropDownList.SelectedValue
        If Not reTypeDropDownList.SelectedValue.Trim().Equals(String.Empty) Then
            ' something selected
            reVehiclePickerControl.ReadOnly = False
        Else
            ' nothing selected
            reVehiclePickerControl.ReadOnly = True
        End If

    End Sub
End Class
