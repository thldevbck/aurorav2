<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingRequestAgentMgtPopupUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingRequestAgentMgtPopupUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc2" %>

<script type="text/javascript">
        
//        // Add click handlers for buttons to show and hide modal popup on pageLoad
//        function pageLoad() {
//            $addHandler($get("backButton"), 'click', hideBookingRequestAgentMgtPopupViaClient);        
//        }      
//        function hideBookingRequestAgentMgtPopupViaClient(ev) {
//            ev.preventDefault();        
//            var bookingRequestAgentMgtPopupBehavior = $find('programmaticBookingRequestAgentMgtPopupBehavior');
//            bookingRequestAgentMgtPopupBehavior.hide();
//        }
        
        function hideBookingRequestAgentMgtPopupViaClient(id) 
        {    
            var bookingRequestAgentMgtPopupBehavior = $find(id);
            bookingRequestAgentMgtPopupBehavior.hide();
            return false;
        }
        
        
function validateClient(){

	 var agentNameTextBox;
     agentNameTextBox = $get('<%= agentNameTextBox.ClientID %>'); 
 	 var phCityTextBox;
     phCityTextBox = $get('<%= phCityTextBox.ClientID %>');  
     var phAreaTextBox;
     phAreaTextBox = $get('<%= phAreaTextBox.ClientID %>');     
     var phNumberTextBox;
     phNumberTextBox = $get('<%= phNumberTextBox.ClientID %>');  
     var faxCityTextBox;
     faxCityTextBox = $get('<%= faxCityTextBox.ClientID %>');  
     var faxAreaTextBox;
     faxAreaTextBox = $get('<%= faxAreaTextBox.ClientID %>');     
     var faxNumberTextBox;
     faxNumberTextBox = $get('<%= faxNumberTextBox.ClientID %>');  
     var emailTextBox;
     emailTextBox = $get('<%= emailTextBox.ClientID %>');  
     var streetTextBox;
     streetTextBox = $get('<%= streetTextBox.ClientID %>');   
     var cityTextBox;
     cityTextBox = $get('<%= cityTextBox.ClientID %>');     
     var countryPickerControl;
     countryPickerControl = $get('<%= countryPickerControl.ClientID %>');            
     
          
	if (agentNameTextBox.value == ''){
		MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Agent Name","Entered"));
		return false
	}

    if((phCityTextBox.value + phAreaTextBox.value + phNumberTextBox.value) == '' &&
        (faxCityTextBox.value + faxAreaTextBox.value + faxNumberTextBox.value) == '' &&
        emailTextBox.value == '' &&
        streetTextBox.value == '' &&
        cityTextBox.value == '' &&
        countryPickerControl.value == '' ){
		MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Phone/Fax/Email/Billing Address","Entered"));
		return false
	}

    if(phCityTextBox.value + phAreaTextBox.value + phNumberTextBox.value != ''){
		if (phCityTextBox.value == ''){
		    MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Phone Country Code","Entered"));
			return false
		}
		if(phAreaTextBox.value == ''){
		    MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Phone Area Code","Entered"));
			return false
		}
		if(phNumberTextBox.value == ''){
		    MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Phone Number","Entered"));
			return false
		}
	}

    if(faxCityTextBox.value + faxAreaTextBox.value + faxNumberTextBox.value != ''){
		if (faxCityTextBox.value == ''){
		    MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Fax Country Code","Entered"));
			return false
		}
		if(faxAreaTextBox.value == ''){
		    MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Fax Area Code","Entered"));
			return false
		}
		if(faxNumberTextBox.value == ''){
		    MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Fax Number","Entered"));
			return false
		}
	}
	if ( streetTextBox.value + cityTextBox.value + countryPickerControl.value != '' ){
		if (streetTextBox.value == ''){
			MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Street","Entered"));
			return false
		}
		if (cityTextBox.value == ''){
			MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","City/Town","Entered"));
			return false
		}
		if (countryPickerControl.value == ''){
			MessagePanelControl.setMessagePanelWarning('<%= bookingRequestAgentMgtPopupMessagePanelControl.ClientID %>', getErrorMessage("GEN090","Country","Entered"));
			return false
		}
	}
	return true
}        

</script>

<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticBookingRequestAgentMgtPopup"
    BehaviorID="programmaticBookingRequestAgentMgtPopupBehavior" TargetControlID="hiddenTargetControlForModalPopup"
    PopupControlID="programmaticPopup" BackgroundCssClass="modalBackground" DropShadow="True"
    PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 500px; height: 430px">
   
   <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        Miscellaneous Agent Details
    </asp:Panel>
    <br />
     <uc1:MessagePanelControl ID="bookingRequestAgentMgtPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />
 
    <table>
        <tr>
            <td width="150">
                Agent Name:</td>
            <td width="250">
                <asp:TextBox ID="agentNameTextBox" runat="server"></asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td>
                Contact:</td>
            <td>
                <asp:TextBox ID="contactTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Phone Number:</td>
            <td>
                <asp:TextBox ID="phCityTextBox" runat="server" Width="50"></asp:TextBox>
                <asp:TextBox ID="phAreaTextBox" runat="server" Width="50"></asp:TextBox>
                <asp:TextBox ID="phNumberTextBox" runat="server" Width="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>Fax Number:</td>
            <td>
                <asp:TextBox ID="faxCityTextBox" runat="server" Width="50"></asp:TextBox>
                <asp:TextBox ID="faxAreaTextBox" runat="server" Width="50"></asp:TextBox>
                <asp:TextBox ID="faxNumberTextBox" runat="server" Width="100"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>E-mail:</td>
            <td>
                <asp:TextBox ID="emailTextBox" runat="server"></asp:TextBox>
            </td>
        </tr>
    </table>
    <br />
    <b>Billing Address</b>
    <asp:Panel ID="billingAddressPanel" runat=server >
    <table>
        <tr>
            <td width="150">Street:</td>
            <td width="250">
                <asp:TextBox ID="streetTextBox" runat="server" onblur="replaceIllegalChar(this)" ></asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td>
                Suburb:</td>
            <td>
                <asp:TextBox ID="suburbTextBox" runat="server" onblur="replaceIllegalChar(this)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                City / Town:</td>
            <td>
                <asp:TextBox ID="cityTextBox" runat="server" onblur="replaceIllegalChar(this)"></asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td>
                State:</td>
            <td>
                <asp:TextBox ID="stateTextBox" runat="server" onblur="replaceIllegalChar(this)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Postcode:</td>
            <td>
                <asp:TextBox ID="postCodeTextBox" runat="server" onblur="replaceIllegalChar(this)"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Country:</td>
            <td>
                <uc2:PickerControl ID="countryPickerControl" runat="server" PopupType="POPUPCOUNTRY"
                    AppendDescription="true" width="250" Nullable=false />&nbsp;*
            </td>
        </tr>
    </table>
    </asp:Panel>
    <br />
    <table width=100%>
        <tr>
            <td align=right>
                <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" 
                    OnClientClick="return validateClient();" />
                <asp:Button ID="backButton" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel" />
                <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>
