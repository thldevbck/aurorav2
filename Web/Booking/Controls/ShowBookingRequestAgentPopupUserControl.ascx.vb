
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_ShowBookingRequestAgentPopupUserControl
    Inherits AuroraUserControl

    'Private Const InfringementManagementPopupUserControl_Xml_ViewState = "InfringementManagementPopupUserControl_Xml"
    'Private Const InfringementManagementPopupUserControl_IcdId_ViewState = "InfringementManagementPopupUserControl_IcdId"

    'Private Const InfringementManagementPopupUserControl_bookedProdId_ViewState = "InfringementManagementPopupUserControl_bookedProdId"
    'Private Const InfringementManagementPopupUserControl_iid_ViewState = "InfringementManagementPopupUserControl_iid"

    Public Delegate Sub ShowBookingRequestAgentPopupUserControlEventHandler(ByVal sender As Object, ByVal addButton As Boolean, ByVal selectedAgent As Boolean, ByVal param As Object)
    Public Event PostBack As ShowBookingRequestAgentPopupUserControlEventHandler

    'Public Property AgentCode() As String
    '    Get
    '        Return ViewState.Item(InfringementManagementPopupUserControl_IcdId_ViewState)
    '    End Get
    '    Set(ByVal Value As String)
    '        ViewState.Add(InfringementManagementPopupUserControl_IcdId_ViewState, Value)
    '    End Set
    'End Property

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        backButton.Attributes.Add("onclick", "return hideShowBookingRequestAgentPopupViaClient('programmaticShowBookingRequestAgentPopupBehavior');")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the pop up.")
        End Try
    End Sub

    'Protected Sub notifiedDropDownList_DataBinding()
    '    Try
    '        Dim dt As DataTable = BookingInfringement.GetNotified()
    '        'Dim notifiedDropDownList As DropDownList = infringementFormView.FindControl("notifiedDropDownList")
    '        notifiedDropDownList.DataSource = dt
    '        notifiedDropDownList.DataTextField = "DESCRIPTION"
    '        notifiedDropDownList.DataValueField = "ID"
    '        notifiedDropDownList.DataBind()
    '    Catch ex As Exception
    '        CurrentPage.LogException(ex)
    '        infringementManagementPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading the pop up.")
    '    End Try
    'End Sub

    Public Sub loadPopup(ByVal agentCode As String)

        formDataBind(agentCode)
        programmaticShowBookingRequestAgentPopup.Show()

    End Sub

    Private Sub formDataBind(ByVal agentCode As String)
        Try
            Dim ds As New DataSet
            ds = Booking.ShowBookingRequestAgent(agentCode)

            If ds.Tables.Count = 0 Then
                Dim oXmlDoc As New XmlDocument
                oXmlDoc.LoadXml("<data><Agent id='' code='' name='' address='' country='' AgnIsMisc='0' /></data>")

                ds.ReadXml(New XmlNodeReader(oXmlDoc))

                showBookingRequestAgentPopupMessagePanelControl.SetMessagePanelWarning("No Agent found with '" & agentCode & "'.")
            End If

            agentGridView.DataSource = ds.Tables(0)
            agentGridView.DataBind()

        Catch ex As Exception
            CurrentPage.LogException(ex)
            showBookingRequestAgentPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading.")
        End Try

    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles addButton.Click
        Try
            programmaticShowBookingRequestAgentPopup.Hide()
            RaiseEvent PostBack(Me, True, False, Nothing)
        Catch ex As Exception
            CurrentPage.LogException(ex)
            showBookingRequestAgentPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading.")
        End Try

    End Sub

    'Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click

    '    programmaticInfringementPopup.Hide()
    '    'Invoke PopupPostBack event to redatabind the grid
    '    '_delPostBack.DynamicInvoke(True)
    '    RaiseEvent PostBack(Me, True, False, True)

    'End Sub

    'Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newButton.Click
    '    programmaticInfringementPopup.Show()
    '    If validateFields() Then
    '        saveInfringement()
    '        IcdId = ""
    '        formDataBind()
    '    End If
    'End Sub

    'Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
    '    programmaticInfringementPopup.Show()
    '    formDataBind()
    'End Sub

    Protected Sub agentGridView_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles agentGridView.RowCommand
        Try
            If e.CommandName.ToString() = "update" Then

                programmaticShowBookingRequestAgentPopup.Hide()

                Dim text As String
                text = e.CommandArgument.ToString()

                Dim list() As String = text.Split(",")

                Dim param As ArrayList = New ArrayList(3)
                param.Insert(0, list(0))
                param.Insert(1, list(1))
                param.Insert(2, list(2))

                RaiseEvent PostBack(Me, False, True, param)

            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            showBookingRequestAgentPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading.")
        End Try

    End Sub

    Protected Sub agentGridView_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles agentGridView.RowUpdating

    End Sub


    Protected Function GetAgentCode(ByVal id As Object, ByVal code As Object, ByVal name As Object) As String
        Dim idString As String = Convert.ToString(id)
        Dim codeString As String = Convert.ToString(code)
        Dim nameString As String = Convert.ToString(name)

        Return idString & "," & codeString & "," & nameString

    End Function



End Class


