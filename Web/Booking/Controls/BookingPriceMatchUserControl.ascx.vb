﻿Imports Aurora.Common
Imports System.IO
Imports System.Xml

Partial Class Booking_Controls_PriceMatchUserControl
    Inherits AuroraUserControl

    ''PM - Price Matching

    Private _PMPriceMSeqId As Int32
    Public Property PMPriceMSeqId As Int32
        Get
            Return _PMPriceMSeqId
        End Get
        Set(value As Int32)
            _PMPriceMSeqId = value
        End Set
    End Property


    Private _PMRentalId As String
    Public Property PMRentalId As String
        Get
            Return _PMRentalId
        End Get
        Set(value As String)
            _PMRentalId = value
        End Set
    End Property

    Private _PMCheckout As String
    Public Property PMCheckout As String
        Get
            Return _PMCheckout
        End Get
        Set(value As String)
            _PMCheckout = value
        End Set
    End Property

    Private _PMCheckin As String
    Public Property PMCheckin As String
        Get
            Return _PMCheckin
        End Get
        Set(value As String)
            _PMCheckin = value
        End Set
    End Property

    Private _PMHirePeriod As String
    Public Property PMHirePeriod As String
        Get
            Return _PMHirePeriod
        End Get
        Set(value As String)
            _PMHirePeriod = value
        End Set
    End Property

    Private _PMPackage As String
    Public Property PMPackage As String
        Get
            Return _PMPackage
        End Get
        Set(value As String)
            _PMPackage = value
        End Set
    End Property

    Private _PMBrand As String
    Public Property PMBrand As String
        Get
            Return _PMBrand
        End Get
        Set(value As String)
            _PMBrand = value
        End Set
    End Property

    Private _PMVehicle As String
    Public Property PMVehicle As String
        Get
            Return _PMVehicle
        End Get
        Set(value As String)
            _PMVehicle = value
        End Set
    End Property

    Private _PMVehicleName As String
    Public Property PMVehicleName As String
        Get
            Return _PMVehicleName
        End Get
        Set(value As String)
            _PMVehicleName = value
        End Set
    End Property


    Private _PMStatus As String
    Public Property PMStatus As String
        Get
            Return _PMStatus
        End Get
        Set(value As String)
            _PMStatus = value
        End Set
    End Property

    Private _PMBaseRate As Decimal ''Daily Rate
    Public Property PMBaseRate As Decimal
        Get
            Return _PMBaseRate
        End Get
        Set(value As Decimal)
            _PMBaseRate = value
        End Set
    End Property

    Private _PMDiscountPrice As Decimal
    Public Property PMDiscountPrice As Decimal
        Get
            Return _PMDiscountPrice
        End Get
        Set(value As Decimal)
            _PMDiscountPrice = value
        End Set
    End Property

    Public ReadOnly Property PMrequestTypeDropDown As DropDownList
        Get
            Return ddlPMrequestType
        End Get
    End Property

    Public ReadOnly Property PMCompetitorDropDown As DropDownList
        Get
            Return ddlPMCompetitorName
        End Get
    End Property

    Public ReadOnly Property PMMatchingDropDown As DropDownList
        Get
            Return ddlPMmatchingStatus
        End Get
    End Property

    Public ReadOnly Property Backgroundcolor As System.Drawing.Color
        Get
            Return DataConstants.GetBookingStatusColor(PMStatus)
        End Get
    End Property


    'Public ReadOnly Property CompetitorVehicleTextBox As TextBox
    '    Get
    '        Return CompVehicleTextbox
    '    End Get
    'End Property

    'Public ReadOnly Property CompetitorCurrentPriceTextBox As TextBox
    '    Get
    '        Return CompCurrentPriceTextbox
    '    End Get
    'End Property

    'Public ReadOnly Property CompetitorDateQuoteTextBox As UserControls_DateControl
    '    Get
    '        Return CompQNDateTextbox
    '    End Get
    'End Property

    'Public ReadOnly Property CompetitorLinktoQuoteTextBox As TextBox
    '    Get
    '        Return CompLinktoQuoteTextBox
    '    End Get
    'End Property

    'Public ReadOnly Property NoteTextBox As TextBox
    '    Get
    '        Return PMnoteTextBox
    '    End Get
    'End Property

    Public ReadOnly Property RequestedTextBox As TextBox
        Get
            Return RequestedByTextBox
        End Get
    End Property

    Public ReadOnly Property DecisionTextBox As TextBox
        Get
            Return decisionByTextBox
        End Get
    End Property

    'Public ReadOnly Property CompetitorOtherTextBox As TextBox
    '    Get
    '        Return CompOtherTextBox
    '    End Get
    'End Property

    Private _PMAlternateVehicle As String
    Public Property PMAlternateVehicle As String
        Get
            Return _PMAlternateVehicle
        End Get
        Set(value As String)
            _PMAlternateVehicle = value
        End Set
    End Property

    Private _PMAlternatePrice As String
    Public Property PMAAlternatePrice As Decimal
        Get
            Return _PMAlternatePrice
        End Get
        Set(value As Decimal)
            _PMAlternatePrice = value
        End Set
    End Property

   

    'Public ReadOnly Property THLAlternateVehicleTextBox As TextBox
    '    Get
    '        Return AlternateVehicleTextBox
    '    End Get
    'End Property

    'Public ReadOnly Property THLAlternatePriceTextBox As TextBox
    '    Get
    '        Return AlternatePriceTextBox
    '    End Get
    'End Property

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Not IsPostBack) Then
            PMRentalIdLabel.Text = PMRentalId

            'If (String.IsNullOrEmpty(StorePriceMatchTable)) Then
            '    StorePriceMatchTable = ProductsMatrix()
            '    priceMatchXslt.Text = StorePriceMatchTable
            'Else
            '    priceMatchXslt.Text = StorePriceMatchTable
            'End If

        End If
    End Sub

#Region "rev:mia July 13 2013"

    Private Property StorePriceMatchTable As String
        Get
            Return ViewState("StorePriceMatchTable")
        End Get
        Set(value As String)
            ViewState("StorePriceMatchTable") = value
        End Set
    End Property

    Private _PMBookingId As String
    Public Property PMBookingId As String
        Get
            Return _PMBookingId
        End Get
        Set(value As String)
            _PMBookingId = value
        End Set
    End Property

    'Public Property HTMLstructure As String
    '    Get
    '        Return ViewState("HTMLstructure")
    '    End Get
    '    Set(value As String)
    '        ViewState("HTMLstructure") = value
    '    End Set
    'End Property

#End Region

    
End Class
