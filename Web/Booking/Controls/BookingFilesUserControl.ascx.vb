﻿Imports Aurora.Booking.Services
Imports System.Data
Imports System.IO

Partial Class Booking_Controls_BookingFilesUserControl
    Inherits BookingUserControl

    Public IsFirstLoad As Boolean = False
    Public BasePath As String = ""

    Private Property CurrentSortDirection() As SortDirection
        Get
            If ViewState("dirState") Is Nothing Then
                ViewState("dirState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("dirState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("dirState") = value
        End Set
    End Property

    Public Sub LoadPage()
        Try
            If IsFirstLoad Then
                RefreshFilesGrid()
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking files tab.")
        End Try
    End Sub

#Region "Grid Events"
    Private Function GetDataSource() As DataView
        Return BookingFiles.GetFiles(RentalId, ConfigurationManager.AppSettings.Get("BookingFilesPath"))
    End Function
    Private Function GetFileTypeDataSource() As DataView
        Return BookingFiles.GetFileTypes()
    End Function

    Private Sub RefreshFilesGrid()
        RefreshFilesGrid(GetDataSource())
        RefreshFileTypes()
    End Sub

    Private Sub RefreshFileTypes()
        fileType.DataSource = GetFileTypeDataSource()
        fileType.DataTextField = "CodCode"
        fileType.DataValueField = "CodId"
        fileType.DataBind()
    End Sub

    Private Sub RefreshFilesGrid(ByRef data As DataView)
        If data.Count = 0 Then
            bookingFilesGridView.Visible = False
            noFilesPanel.Visible = True
            Return
        End If

        bookingFilesGridView.Visible = True
        noFilesPanel.Visible = False

        bookingFilesGridView.DataSource = data
        bookingFilesGridView.DataBind()
    End Sub

    Protected Sub bookingFilesGridView_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs) Handles bookingFilesGridView.Sorting
        Dim sortingDirection As String = ToggleSort()
        Dim dataView As DataView = GetDataSource()
        dataView.Sort = e.SortExpression + " " + sortingDirection
        RefreshFilesGrid(dataView)
    End Sub

    Protected Sub bookingFilesGridView_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs) Handles bookingFilesGridView.RowCommand
        Select Case e.CommandName
            Case "Delete"
                Dim source As GridView = CType(e.CommandSource, GridView)
                Dim fileId As Integer = Integer.Parse(CType(source.DataKeys.Item(0).Value, String))
                '        Dim fileName As String = CType(source.DataKeys.Item(1).Value, String)
                '        Dim filePath As String = CType(source.DataKeys.Item(2).Value, String)

                Dim serverPath As String = Server.MapPath(ConfigurationManager.AppSettings.Get("BookingFilesPath"))

                '        BookingFiles.DeleteFile(fileId, fileName, filePath, serverPath)
                BookingFiles.DeleteFile(fileId, "", "", serverPath)
            Case "Sort"
            Case Else
                Return
        End Select
    End Sub

    Protected Sub bookingFilesGridView_RowDeleting(ByVal sender As Object, ByVal e As GridViewDeleteEventArgs) Handles bookingFilesGridView.RowDeleting
        RefreshFilesGrid(GetDataSource())
    End Sub


    Private Function ToggleSort() As String
        Dim sortingDirection As String
        If CurrentSortDirection = SortDirection.Ascending Then

            CurrentSortDirection = SortDirection.Descending
            sortingDirection = "Desc"

        Else

            CurrentSortDirection = SortDirection.Ascending
            sortingDirection = "Asc"
        End If
        Return sortingDirection
    End Function
#End Region

#Region "Click Events"
    Protected Sub uploadFile_Click(ByVal sender As Object, ByVal e As EventArgs) Handles uploadFileButton.Click
        Try
            ValidateSaveNew()
        Catch ex As Aurora.Common.ValidationException
            programmaticModalPopup.Show()
            bookingNotesPopupMessagePanelControl.SetMessagePanelWarning(ex.Message)
            Return
        End Try

        Try
            SaveFile()
        Catch ex As Exception
            programmaticModalPopup.Show()
            bookingNotesPopupMessagePanelControl.SetMessagePanelWarning(ex.Message)
        End Try

        RefreshFilesGrid()
        ClosePopup()
    End Sub

    Protected Sub showUploadScreen_Click(ByVal sender As Object, ByVal e As EventArgs) Handles showUploadScreen.Click
        programmaticModalPopup.Show()
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        ClosePopup()
    End Sub

    Private Sub ClosePopup()
        programmaticModalPopup.Hide()
        description.Text = ""
    End Sub

    Private Sub ValidateSaveNew()
        If Not fileUpload.HasFile Then
            Throw New Aurora.Common.ValidationException("You must select a file to upload")
        ElseIf description.Text.Length > 8000 Then
            Throw New Aurora.Common.ValidationException("Description cannot be greater than 8000 characters!")
        End If
    End Sub
#End Region

#Region "Save File"
    Private Sub SaveFile()
        Dim serverPath As String = Server.MapPath(ConfigurationManager.AppSettings.Get("BookingFilesPath"))
        Dim fullPath As String = BookingFiles.GetNewFilePath(RentalId, Path.GetFileName(fileUpload.FileName), BasePath, serverPath)
        Dim directoryName As String = Path.GetDirectoryName(fullPath)
        If Not Directory.Exists(directoryName) Then
            Directory.CreateDirectory(directoryName)
        End If
        fileUpload.PostedFile.SaveAs(fullPath)
        BookingFiles.SaveNewFile(RentalId, fileType.SelectedValue, description.Text, Path.GetFileName(fullPath), BasePath, CurrentPage.UserCode, "BookingFilesControl")
    End Sub
#End Region
End Class
