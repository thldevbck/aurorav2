<%@ Control Language="VB" 
            AutoEventWireup="false" 
            CodeFile="BookingCustomerUserControl.ascx.vb"
            Inherits="Booking_Controls_BookingCustomerUserControl" %>

<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" 
             TagName="DateControl"
             TagPrefix="uc1" %>

<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" 
             TagName="ConfimationBoxControl"
             TagPrefix="uc1" %>

<%@ Register Src="~/UserControls/RegExTextBox/RegExTextBox.ascx" 
             TagName="RegExTextBox"
             TagPrefix="uc1" %>

<%@ Register src="BookingCustomerUserEditControl.ascx" 
             tagname="BookingCustomerUserEditControl" 
             tagprefix="uc2" %>

<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx"
             TagName="CountryPickerControl"
             TagPrefix="uc1" %>


<asp:Panel ID="customerSummaryPanel" runat="server" Width="100%">

    <script language="javascript" type="text/javascript">
    
    var postBackControl = null;
    var pageRequestManager = Sys.WebForms.PageRequestManager.getInstance();

    pageRequestManager.add_beginRequest(BeginRequestHandler);
    pageRequestManager.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) 
    {
        postBackControl = args.get_postBackElement();  //the control causing the postback
        
        if(postBackControl.id.indexOf('removeButton') > -1 || postBackControl.id.indexOf('saveButton') > -1 )
        {   
            //alert(postBackControl.id)
            postBackControl.disabled = true;
        }
    }

    function EndRequestHandler(sender, args) 
    {
        if(postBackControl.id.indexOf('removeButton') > -1 || postBackControl.id.indexOf('saveButton') > -1 )
        {
            postBackControl.disabled = false;
        }
        postBackControl = null;
    }

    </script>

    <table width="100%">
        <tr>
            <td>
                <b>Customer Summary for this Rental:</b></td>
            <td align="right">
                <asp:Button ID="removeButton" runat="server" Text="Remove" OnClientClick="return confirm('Are you sure you wish to remove the selected\ncustomer(s) from this rental?');"
                    CssClass="Button_Standard Button_Remove" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="rentalCustomerGridView" runat="server" Width="100%" AutoGenerateColumns="False"
        CssClass="dataTableGrid" CellPadding="2" CellSpacing="0">
        <AlternatingRowStyle CssClass="oddRow" VerticalAlign="top" />
        <RowStyle CssClass="evenRow" VerticalAlign="top" />
        <Columns>
            <asp:TemplateField Visible="true" ItemStyle-VerticalAlign="Middle">
                <ItemTemplate>
                      <asp:ImageButton runat="server" id="ImageButton1"  ImageUrl="~/Images/right.gif" OnCommand="LoadCustomerNameParameter" CommandName="Load" CommandArgument='<%# Bind("CusId") %>' CausesValidation="false"/>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Id" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="rnhIdLabel" runat="server" Text='<%# Bind("CusId") %>' Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Name / Email Address" ItemStyle-Wrap="false">
                <ItemTemplate>
                    <asp:HyperLink ID="HyperLink1" runat="server" Text='<%# System.Web.HttpUtility.HtmlEncode(eval("CName")) %>' NavigateUrl='<%# GetCustomerUrl(Eval("CusId")) %>'></asp:HyperLink>
                    <br />
                     <asp:TextBox ID="EmailTextBox" runat="server" Width="155" Text=''  MaxLength="100"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField ReadOnly="True" DataField="Address" HeaderText="Address"></asp:BoundField>
            <asp:TemplateField HeaderText="Hirer" Visible="true">
                <ItemTemplate>
                    <asp:CheckBox ID="hirerCheckBox" runat="server" Checked='<%# Bind("Hirer") %>' AutoPostBack="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Pri<br/>Hirer" HeaderStyle-Width="20" HeaderStyle-VerticalAlign="Top">
                <ItemTemplate>
                    <asp:CheckBox ID="priHirerCheckBox" runat="server" Checked='<%# Bind("PriHirer") %>'
                        AutoPostBack="false" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Is<br/>Drv" HeaderStyle-VerticalAlign="Top">
                <ItemTemplate>
                    <asp:CheckBox ID="driverCheckBox" runat="server" Checked='<%# Bind("IsDriver") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Licence No / Passport No" HeaderStyle-VerticalAlign="Top" HeaderStyle-Width="100">
                <ItemTemplate>
                    <asp:TextBox ID="licNoTextBox" runat="server" Width="95%" Text='<%# System.Web.HttpUtility.Htmldecode(eval("LicNo")) %>'
                        MaxLength="64"></asp:TextBox>
                    <asp:TextBox ID="passportNoTextBox" runat="server" Width="95%" Text='<%# System.Web.HttpUtility.Htmldecode(eval("PassportNo")) %>'
                        MaxLength="22"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Issuing Authority / Country" HeaderStyle-VerticalAlign="Top" HeaderStyle-Width="100">
                <ItemTemplate>
                    <asp:TextBox ID="issueAuthorityTextBox" runat="server" Width="90%" Text='<%# System.Web.HttpUtility.Htmldecode(eval("IssueAuthority")) %>'
                        MaxLength="64"></asp:TextBox>
                        <%--<asp:TextBox ID="passportCountryTextBox" runat="server" Width="90%" Text='<%# System.Web.HttpUtility.Htmldecode(eval("PassportCountry")) %>'
                        MaxLength="22"></asp:TextBox>--%>
                        <uc1:CountryPickerControl ID="CountryPickerControl" 
                                                   runat="server" 
                                                   PopupType="POPUPCOUNTRY"
                                                   AppendDescription="false" 
                                                   width="60px" 
                                                   Text='<%# eval("PassportCountry") %>'/>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Expiry Date / Loyalty Card No." ItemStyle-Width="95" HeaderStyle-VerticalAlign="Top">
                <ItemTemplate>
                    <uc1:DateControl ID="expiryDateControl" runat="server" Nullable="true" />
                    <br />
                    <%--<uc1:RegExTextBox ID="loyaltyCardNumberTextBox" runat="server" Width="90%" RegExPattern="^([\w]+)$"
                        style="float: left" MaxLength="64" ErrorMessage="Enter a valid Loyalty Card Number"
                        Text='<%# Bind("LoyaltyCardNumber") %>' />--%>
                    <uc1:RegExTextBox ID="loyaltyCardNumberTextBox" 
                                     runat="server" 
                                     Width="80px" 
                                     RegExPattern="^.+"
                                     Style="float: left" 
                                     MaxLength="64" 
                                     ErrorMessage="Enter a valid Loyalty Card Number"
                                      />
                                     
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="DOB" ItemStyle-Width="90">
                <ItemTemplate>
                    <uc1:DateControl ID="dobDateControl" runat="server" Nullable="true" OnDateChanged="dobDateChanged"
                        AutoPostBack="true" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Age">
                <ItemTemplate>
                    <asp:Label ID="ageLabel" runat="server" Text='<%# Bind("Age") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Sel">
                <ItemTemplate>
                    <asp:CheckBox ID="checkCheckBox" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <table id="emptyRentalCustomerTable" runat="server" class="dataTableGrid" width="100%">
        <thead>
            <tr>
                <th>
                    Name</th>
                <th>
                    Address</th>
                <th>
                    Hirer</th>
                <th>
                    Primary<br />
                    Hirer</th>
                <th>
                    Driver</th>
                <th>
                    Licence No /Passport No</th>
                <th>
                    Issuing Authority / Country</th>
                <th>
                    Expiry Date</th>
                <th>
                    DOB</th>
                <th>
                    Age</th>
                <th>
                    Check</th>
            </tr>
        </thead>
        <tr>
            <td colspan="11">
                <br />
            </td>
        </tr>
    </table>
    <br />
    <table width="100%" style="border-top-width: 1px">
        <tr>
            <td>
                <b>Other Customers on this Booking:</b>
            </td>
            <td align="right">
                <asp:Button ID="addButton" runat="server" Text="Add" CssClass="Button_Standard Button_Add" />
            </td>
        </tr>
    </table>
    <asp:GridView ID="otherBookingCustomerGridView" runat="server" Width="100%" AutoGenerateColumns="False"
        CssClass="dataTableGrid" CellPadding="2" CellSpacing="0">
        <AlternatingRowStyle CssClass="oddRow" />
        <RowStyle CssClass="evenRow" />
        <Columns>
            <asp:TemplateField HeaderText="Id" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="rnhIdLabel" runat="server" Text='<%# Bind("CusId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField ReadOnly="True" DataField="CName" HeaderText="Name"></asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="Address" HeaderText="Address"></asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="RentalNo" HeaderText="Rentals"></asp:BoundField>
            <asp:BoundField ReadOnly="True" DataField="LicNo" HeaderText="Licence No"></asp:BoundField>
            <asp:TemplateField HeaderText="DOB">
                <ItemTemplate>
                    <asp:Label ID="dobLabel" runat="server" Text='<%# GetDateFormat(eval("DOB")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField ReadOnly="True" DataField="IssueAuthority" HeaderText="Issuing Authority">
            </asp:BoundField>
            <asp:TemplateField HeaderText="Expiry Date">
                <ItemTemplate>
                    <asp:Label ID="expiryDtLabel" runat="server" Text='<%# GetDateFormat(eval("ExpiryDt")) %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Check">
                <ItemTemplate>
                    <asp:CheckBox ID="checkCheckBox" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <table id="emptyOtherBookingCustomerTable" runat="server" class="dataTableGrid" width="100%">
        <thead>
            <tr>
                <th>
                    Name</th>
                <th>
                    Address</th>
                <th>
                    Rentals</th>
                <th>
                    Licence No</th>
                <th>
                    DOB</th>
                <th>
                    Issuing Authority</th>
                <th>
                    Expiry Date</th>
                <th>
                    Check</th>
            </tr>
        </thead>
        <tr>
            <td colspan="8">
                <br />
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <asp:Button ID="addCustomerButton" runat="server" Text="Add Customer" CssClass="Button_Standard Button_Add" />
                <asp:Button ID="addDriverButton" runat="server" Text="Add Driver" CssClass="Button_Standard Button_Add" />
                <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
                
            </td>
        </tr>
    </table>

    <uc1:ConfimationBoxControl ID="confimationBoxControl" runat="server" LeftButton_Text="OK"
        RightButton_Text="Cancel" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
        Title="Booking Customer" />

<%--rev:mia Sept.2 2013 - Customer Title from query--%>
     <uc2:BookingCustomerUserEditControl ID="BookingCustomerUserEditControl1"   runat="server"/>
</asp:Panel>
