
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_BookingMatchProductUserControl
    Inherits BookingUserControl

    Private Const BookingMatchProductUserControl_MatchProduct_SessionState = "BookingMatchProductUserControl_MatchProduct"
    Private Const BookingMatchProductUserControl_ProductToMatch_SessionState = "BookingMatchProductUserControl_ProductToMatch"
    Private Const BookingMatchProductUserControl_TravellerToMatch_SessionState = "BookingMatchProductUserControl_TravellerToMatch"
    'Private Const BookingMatchProductUserControl_MatchProduct_ViewState = "BookingMatchProductUserControl_MatchProduct"
    'Private Const BookingMatchProductUserControl_ProductToMatch_ViewState = "BookingMatchProductUserControl_ProductToMatch"
    'Private Const BookingMatchProductUserControl_TravellerToMatch_ViewState = "BookingMatchProductUserControl_TravellerToMatch"

    Public isFirstLoad As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking match product tab.")
        End Try
    End Sub

    Public Sub LoadPage()
        Try
            'BookingId = "W6322696"
            'RentalId = "W6322696-1"
            If isFirstLoad Then
                matchProductGridViewDatabind()
            End If
            'Dim allCheckBox As CheckBox = matchedProductGridView.HeaderRow.FindControl("allCheckBox")
            'allCheckBox.Attributes.Add("onclick", "gridViewCheckUncheckCheckboxes('" & allCheckBox.ClientID & "','" & matchedProductGridView.ClientID & "')")

            'Dim allCheckBox1 As CheckBox = travellerToMatchGridView.HeaderRow.FindControl("allCheckBox")
            'allCheckBox1.Attributes.Add("onclick", "gridViewCheckUncheckCheckboxes('" & allCheckBox1.ClientID & "','" & travellerToMatchGridView.ClientID & "')")
            setAttributes()

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking match product tab.")
        End Try
    End Sub

    Private Sub setAttributes()
        Dim allCheckBox As CheckBox = matchedProductGridView.HeaderRow.FindControl("allCheckBox")
        allCheckBox.Attributes.Add("onclick", "gridViewCheckUncheckCheckboxes('" & allCheckBox.ClientID & "','" & matchedProductGridView.ClientID & "')")

        Dim allCheckBox1 As CheckBox = travellerToMatchGridView.HeaderRow.FindControl("allCheckBox")
        allCheckBox1.Attributes.Add("onclick", "gridViewCheckUncheckCheckboxes('" & allCheckBox1.ClientID & "','" & travellerToMatchGridView.ClientID & "')")

    End Sub

    Private Sub matchProductGridViewDatabind()

        Dim ds As DataSet
        ds = BookingMatchProduct.GetMatchProducts(BookingId, RentalId)

        matchedProductGridView.DataSource = ds.Tables(2)
        matchedProductGridView.DataBind()
        Session.Item(BookingMatchProductUserControl_MatchProduct_SessionState) = ds.Tables(2)
        'ViewState(BookingMatchProductUserControl_MatchProduct_ViewState) = ds.Tables(2)

        productToMatchGridView.DataSource = ds.Tables(4)
        productToMatchGridView.DataBind()
        Session.Item(BookingMatchProductUserControl_ProductToMatch_SessionState) = ds.Tables(4)
        'ViewState(BookingMatchProductUserControl_ProductToMatch_ViewState) = ds.Tables(4)

        travellerToMatchGridView.DataSource = ds.Tables(6)
        travellerToMatchGridView.DataBind()
        Session.Item(BookingMatchProductUserControl_TravellerToMatch_SessionState) = ds.Tables(6)
        'ViewState(BookingMatchProductUserControl_TravellerToMatch_ViewState) = ds.Tables(6)

        If ds.Tables(4).Rows.Count = 0 Or _
            (ds.Tables(4).Rows.Count = 1 AndAlso String.IsNullOrEmpty(ds.Tables(4).Rows(0)("BpdId"))) Then
            saveButton.Enabled = False
            matchButton.Enabled = False
            unmatchButton.Enabled = False
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, "There is no product for this rental which need to be matched to customer")
        Else
            saveButton.Enabled = True
            matchButton.Enabled = True
            unmatchButton.Enabled = True
        End If

    End Sub

    Protected Sub matchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles matchButton.Click

        Dim matchProductDt As DataTable
        matchProductDt = Session.Item(BookingMatchProductUserControl_MatchProduct_SessionState)
        'matchProductDt = ViewState(BookingMatchProductUserControl_MatchProduct_ViewState)
        Dim productToMatchDt As DataTable
        productToMatchDt = Session.Item(BookingMatchProductUserControl_ProductToMatch_SessionState)
        'productToMatchDt = ViewState(BookingMatchProductUserControl_ProductToMatch_ViewState)
        Dim travellerToMatchDt As DataTable
        travellerToMatchDt = Session.Item(BookingMatchProductUserControl_TravellerToMatch_SessionState)
        'travellerToMatchDt = ViewState(BookingMatchProductUserControl_TravellerToMatch_ViewState)

        'View state matchProductGridViewDatabind
        For Each vr As GridViewRow In matchedProductGridView.Rows
            Dim matchCheckBox As CheckBox = vr.FindControl("matchCheckBox")
            Dim productLabel As Label = vr.FindControl("productLabel")
            Dim customerLabel As Label = vr.FindControl("customerLabel")

            For Each dr As DataRow In matchProductDt.Rows
                If dr.Item("BpdId") = productLabel.Text And dr.Item("CusId") = customerLabel.Text Then
                    dr.Item("chk") = Convert.ToInt32(matchCheckBox.Checked)
                End If
            Next
        Next


        'Populate Checked list
        Dim checkedProductToMatchDt As New DataTable
        Dim checkedTravellerToMatchDt As New DataTable
        'clone the schema
        checkedProductToMatchDt = productToMatchDt.Clone()
        checkedTravellerToMatchDt = travellerToMatchDt.Clone()

        'Populate checkedProductToMatchDt
        For Each dr As GridViewRow In productToMatchGridView.Rows
            Dim matchCheckBox As CheckBox = dr.FindControl("matchCheckBox")
            Dim productLabel As Label = dr.FindControl("productLabel")

            If matchCheckBox.Checked And (Not String.IsNullOrEmpty(productLabel.Text)) Then
                Dim prdDet As String = dr.Cells(1).Text
                Dim prdQtyAs As String = dr.Cells(2).Text

                If Utility.ParseInt(prdQtyAs, 0) = 0 Then
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "You cannot select the product which is having Quantity with Zero")
                    Return
                End If

                Dim newRow As DataRow = checkedProductToMatchDt.NewRow()
                newRow.Item("BpdId") = productLabel.Text
                newRow.Item("PrdDet") = prdDet
                newRow.Item("PrdQty") = prdQtyAs
                newRow.Item("chk") = 1
                checkedProductToMatchDt.Rows.Add(newRow)
                'checkedProductToMatchDt.Rows.Add(productLabel.Text, prdDet, prdQtyAs, 1, 0)

            End If
        Next

        'Populate checkedTravellerToMatchDt
        For Each dr As GridViewRow In travellerToMatchGridView.Rows
            Dim matchCheckBox As CheckBox = dr.FindControl("matchCheckBox")
            Dim customerLabel As Label = dr.FindControl("customerLabel")

            If matchCheckBox.Checked And (Not String.IsNullOrEmpty(customerLabel.Text)) Then
                Dim trvDet As String = dr.Cells(1).Text

                Dim newRow As DataRow = checkedTravellerToMatchDt.NewRow()
                newRow.Item("CusId") = customerLabel.Text
                newRow.Item("TrvDet") = trvDet
                newRow.Item("chk") = 1
                checkedTravellerToMatchDt.Rows.Add(newRow)
                'checkedTravellerToMatchDt.Rows.Add(customerLabel.Text, trvDet, 1, 0)
            End If
        Next


        'Validations
        If checkedProductToMatchDt.Rows.Count = 0 Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "You must select product you want to match the traveller(s) to")
            Return
        End If

        If checkedTravellerToMatchDt.Rows.Count = 0 Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "You must select at least one traveller to match the product")
            Return
        End If

        If checkedProductToMatchDt.Rows.Count > 1 Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "You must select only one product to match the traveller(s) to")
            Return
        End If

        Dim productId As String = checkedProductToMatchDt.Rows(0)("BpdId")
        Dim productDet As String = checkedProductToMatchDt.Rows(0)("PrdDet")
        Dim productQty As String = checkedProductToMatchDt.Rows(0)("PrdQty")

        If checkedTravellerToMatchDt.Rows.Count > productQty Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "You have selected more travellers than is available for the selected product")
            Return
        End If

        Dim addedQty As Integer = 0

        'Add matched Product
        For Each checkedTravellerToMatchDr As DataRow In checkedTravellerToMatchDt.Rows
            Dim customerId As String = checkedTravellerToMatchDr.Item("CusId")
            Dim trvDet As String = checkedTravellerToMatchDr.Item("TrvDet")
            For Each matchProductDr As DataRow In matchProductDt.Rows
                If matchProductDr.Item("CusId") = checkedTravellerToMatchDr.Item("CusId") And matchProductDr.Item("BpdId") = productId Then
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "A selected customer has already been matched to the selected product")
                    Return
                End If
            Next

            'Add matched Product
            If matchProductDt.Rows.Count = 1 AndAlso String.IsNullOrEmpty(matchProductDt.Rows(0).Item("BpdId")) Then
                matchProductDt.Rows(0).Delete()
            End If

            Dim newRow As DataRow = matchProductDt.NewRow()
            newRow.Item("BpdId") = productId
            newRow.Item("CusId") = customerId
            newRow.Item("PrdDet") = productDet
            newRow.Item("TrvDet") = trvDet
            newRow.Item("chk") = 0
            matchProductDt.Rows.Add(newRow)
            'matchProductDt.Rows.Add(productId, customerId, productDet, trvDet, 0, 0)
            addedQty = addedQty + 1
        Next

        'Modify productQty
        For Each dr As DataRow In productToMatchDt.Rows
            If dr.Item("BpdId") = productId Then
                dr("PrdQty") = dr("PrdQty") - addedQty
            End If
        Next


        'add view state
        Session.Item(BookingMatchProductUserControl_MatchProduct_SessionState) = matchProductDt
        Session.Item(BookingMatchProductUserControl_ProductToMatch_SessionState) = productToMatchDt
        Session.Item(BookingMatchProductUserControl_TravellerToMatch_SessionState) = travellerToMatchDt
        'ViewState(BookingMatchProductUserControl_MatchProduct_ViewState) = matchProductDt
        'ViewState(BookingMatchProductUserControl_ProductToMatch_ViewState) = productToMatchDt
        'ViewState(BookingMatchProductUserControl_TravellerToMatch_ViewState) = travellerToMatchDt

        'databind
        matchedProductGridView.DataSource = matchProductDt
        matchedProductGridView.DataBind()
        productToMatchGridView.DataSource = productToMatchDt
        productToMatchGridView.DataBind()
        travellerToMatchGridView.DataSource = travellerToMatchDt
        travellerToMatchGridView.DataBind()

        setAttributes()

    End Sub

    Protected Sub unmatchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles unmatchButton.Click

        Dim matchProductDt As DataTable
        matchProductDt = Session.Item(BookingMatchProductUserControl_MatchProduct_SessionState)
        'matchProductDt = ViewState(BookingMatchProductUserControl_MatchProduct_ViewState)
        Dim productToMatchDt As DataTable
        productToMatchDt = Session.Item(BookingMatchProductUserControl_ProductToMatch_SessionState)
        'productToMatchDt = ViewState(BookingMatchProductUserControl_ProductToMatch_ViewState)
       
        'View state productToMatchGridView
        For Each vr As GridViewRow In productToMatchGridView.Rows
            Dim matchCheckBox As CheckBox = vr.FindControl("matchCheckBox")
            Dim productLabel As Label = vr.FindControl("productLabel")

            For Each dr As DataRow In productToMatchDt.Rows
                If dr.Item("BpdId") = productLabel.Text Then
                    dr.Item("chk") = Convert.ToInt32(matchCheckBox.Checked)
                End If
            Next
        Next

        ''View state travellerToMatchGridView
        'For Each vr As GridViewRow In travellerToMatchGridView.Rows
        '    Dim matchCheckBox As CheckBox = vr.FindControl("matchCheckBox")
        '    Dim customerLabel As Label = vr.FindControl("customerLabel")

        '    For Each dr As DataRow In travellerToMatchDt.Rows
        '        If dr.Item("CusId") = customerLabel.Text Then
        '            dr.Item("chk") = Convert.ToInt32(matchCheckBox.Checked)
        '        End If
        '    Next
        'Next


        'Populate Checked list
        Dim checkedMatchProductDt As New DataTable
        'clone the schema
        checkedMatchProductDt = matchProductDt.Clone()

        'Populate checkedMatchProductDt
        For Each dr As GridViewRow In matchedProductGridView.Rows
            Dim matchCheckBox As CheckBox = dr.FindControl("matchCheckBox")
            Dim productLabel As Label = dr.FindControl("productLabel")
            Dim customerLabel As Label = dr.FindControl("customerLabel")

            If matchCheckBox.Checked And (Not String.IsNullOrEmpty(productLabel.Text)) And (Not String.IsNullOrEmpty(customerLabel.Text)) Then
                Dim prdDet As String = dr.Cells(2).Text
                Dim trvDet As String = dr.Cells(3).Text

                Dim newRow As DataRow = checkedMatchProductDt.NewRow()
                newRow.Item("BpdId") = productLabel.Text
                newRow.Item("CusId") = customerLabel.Text
                newRow.Item("PrdDet") = prdDet
                newRow.Item("TrvDet") = trvDet
                newRow.Item("chk") = 1
                checkedMatchProductDt.Rows.Add(newRow)

                'checkedMatchProductDt.Rows.Add(productLabel.Text, customerLabel.Text, prdDet, trvDet, 1, 0)
            End If
        Next


        'Validations
        If checkedMatchProductDt.Rows.Count = 0 Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "You must select at least one Match Product To Un-Match")
            Return
        End If



        For Each checkedMatchProductDr As DataRow In checkedMatchProductDt.Rows
            Dim productId As String = checkedMatchProductDr.Item("BpdId")
            Dim customerId As String = checkedMatchProductDr.Item("CusId")

            'Add to productToMatchDt
            For Each productToMatchDr As DataRow In productToMatchDt.Rows
                If productToMatchDr.Item("BpdId") = productId Then
                    productToMatchDr.Item("PrdQty") = Utility.ParseInt(productToMatchDr.Item("PrdQty"), 0) + 1
                End If
            Next

            'Remove from matchProductDt
            Dim rowIndex As Integer = -1
            For Each matchProductDr As DataRow In matchProductDt.Rows
                If matchProductDr.Item("BpdId") = productId And matchProductDr.Item("CusId") = customerId Then
                    rowIndex = matchProductDt.Rows.IndexOf(matchProductDr)
                End If
            Next
            If rowIndex > -1 Then
                matchProductDt.Rows.RemoveAt(rowIndex)
            End If
        Next

        If matchProductDt.Rows.Count = 0 Then

            Dim newRow As DataRow = matchProductDt.NewRow()
            newRow.Item("BpdId") = ""
            newRow.Item("CusId") = ""
            newRow.Item("PrdDet") = ""
            newRow.Item("TrvDet") = ""
            newRow.Item("chk") = 0
            matchProductDt.Rows.Add(newRow)

            'matchProductDt.Rows.Add("", "", "", "", 0, 0)
        End If

        'add view state
        Session.Item(BookingMatchProductUserControl_MatchProduct_SessionState) = matchProductDt
        Session.Item(BookingMatchProductUserControl_ProductToMatch_SessionState) = productToMatchDt
        'ViewState(BookingMatchProductUserControl_MatchProduct_ViewState) = matchProductDt
        'ViewState(BookingMatchProductUserControl_ProductToMatch_ViewState) = productToMatchDt

        'databind
        matchedProductGridView.DataSource = matchProductDt
        matchedProductGridView.DataBind()
        productToMatchGridView.DataSource = productToMatchDt
        productToMatchGridView.DataBind()

        setAttributes()

    End Sub

    Protected Function matchedProductCheckBoxEnabled(ByVal productId As Object, ByVal customerId As Object) As Boolean
        Dim productIdString As String
        productIdString = Convert.ToString(productId)
        Dim customerIdString As String
        customerIdString = Convert.ToString(customerId)

        If String.IsNullOrEmpty(productIdString) And String.IsNullOrEmpty(customerIdString) Then
            Return False
        Else
            Return True
        End If

    End Function

    Protected Function productToMatchCheckBoxEnabled(ByVal productId As Object, ByVal qty As Object) As Boolean
        Dim productIdString As String
        productIdString = Convert.ToString(productId)
        Dim qtyInt As Integer
        qtyInt = Utility.ParseInt(qty, 0)

        If String.IsNullOrEmpty(productIdString) Or qtyInt = 0 Then
            Return False
        Else
            Return True
        End If

    End Function

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        Dim productToMatchDt As DataTable
        productToMatchDt = Session.Item(BookingMatchProductUserControl_ProductToMatch_SessionState)
        'productToMatchDt = ViewState(BookingMatchProductUserControl_ProductToMatch_ViewState)

        For Each dr As DataRow In productToMatchDt.Rows
            If Utility.ParseInt(dr.Item("PrdQty"), 0) > 0 Then
                confimationBoxControl.Text = "There are still product that require matched travellers.<br/>Do you want to continue?"
                confimationBoxControl.Param = 1
                confimationBoxControl.Show()
                Return
            End If
        Next

        save()
        setAttributes()

    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        If leftButton Then
            save()
        End If
    End Sub

    Private Sub save()

        'BookingId = "W6322696"
        'RentalId = "W6322696-1"

        Dim matchProductDt As DataTable
        matchProductDt = Session.Item(BookingMatchProductUserControl_MatchProduct_SessionState)
        'matchProductDt = ViewState(BookingMatchProductUserControl_MatchProduct_ViewState)

        'Build xmlDoc
        Dim xmlString As New StringBuilder
        Dim xmlDoc As New XmlDocument
        xmlString.Append("<Root>")
        xmlString.Append("<BooId>" & BookingId & "</BooId>")
        xmlString.Append("<RntId>" & RentalId & "</RntId>")

        For Each dr As DataRow In matchProductDt.Rows
            xmlString.Append("<Id>")
            xmlString.Append("<BpdId>" & dr.Item("BpdId") & "</BpdId>")
            xmlString.Append("<CusId>" & dr.Item("CusId") & "</CusId>")
            xmlString.Append("</Id>")
        Next

        xmlString.Append("<UsrCode>" & CurrentPage.UserCode & "</UsrCode>")
        xmlString.Append("<AddProgramName>Booking.aspx</AddProgramName>")
        xmlString.Append("</Root>")

        xmlDoc.LoadXml(xmlString.ToString())

        'Save
        Dim returnMessage As String
        returnMessage = BookingMatchProduct.UpdateMatchProducts(xmlDoc)

        If returnMessage.Contains("GEN046") Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, returnMessage)
        Else
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, returnMessage)
        End If
    End Sub


End Class
