﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="EuropcarDataLog.ascx.vb" Inherits="Booking_Controls_EuropcarDataLog" %>


<%@ Import Namespace="Aurora.Package.Data" %>
<%@ Import Namespace="Aurora.Common" %>
<%@ Import Namespace="Aurora.Booking.Services" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%--Dummy control, required by the modal popup dialog--%>
<asp:Button 
    runat="server" 
    ID="dummyEuropcarTargetButton" 
    Style="display: none" />


<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="europcarModalDialog" 
    BehaviorID="europcarModalDialogBehavior"
    TargetControlID="dummyEuropcarTargetButton" 
    PopupControlID="europcarModalDialogContent"
    BackgroundCssClass="modalBackground" 
    DropShadow="True" 
    PopupDragHandleControlID="europcarModalDialogDragHandle" />






<asp:PlaceHolder ID="plhStyle" runat="server">
<style type="text/css">
    table#LogSearchTable
    {
        border-collapse: collapse;
    }
    
    table#LogSearchTable td
    {        
        border-bottom: 1px dotted #000;
    }
    
    table#LogSearchTable td, table#LogSearchTable th
    {
        padding: 5px 10px;
        white-space:nowrap;
    }
    
    table#LogSearchTable pre
    {
        display: none;
    }
    
    div.LogSearchPanel
    {
        width: 800px;
        height: 600px;
    }
    
    div.europcarLogSearch
    {
        width: 800px;
        height: 500px;
        position:relative;
        overflow:scroll;
    }
    
    a.xmlLogExpand, a.xmlLogCopy
    {
        font:normal bold 14px Courier New;
        text-decoration: none;
        white-space:nowrap;
    }
    
    table#LogSearchTable tr.xmlLogResponseRow td
    {
        border-bottom: 1px solid #000;
    }
    
</style>
</asp:PlaceHolder>

<asp:Panel runat="server" CssClass="modalPopup LogSearchPanel" ID="europcarModalDialogContent" >
    <asp:PlaceHolder ID="plhEuropCarDialogInner" runat="server" Visible="false">
        <asp:Panel runat="Server" ID="europcarModalDialogDragHandle" CssClass="modalPopupTitle">
            <asp:Label ID="titleLabel" runat="server">View Europcar Log</asp:Label>
        </asp:Panel>

        <div class="europcarLogSearch">
        
            <br />

            <asp:Repeater ID="rptLog" EnableViewState="false" runat="server">
                <HeaderTemplate>
                    <table id="LogSearchTable">
                        <thead>
                            <tr>
                                <th>Timestamp</th>
                                <th>Action</th>
                                <th>Europcar Reference</th>
                                <th>Category</th>
                                <th>Cki Station</th>
                                <th>Cko Station</th>
                                <th>Cki date</th>
                                <th>Cko date</th>
                                <th>Contract ID</th>
                                <th>Account</th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                     <tr>
                        <td><%# DirectCast(Container.DataItem, EuropcarServiceLogEntry).TimeStamp.ToString()%></td>
                        <td><%# DirectCast(Container.DataItem, EuropcarServiceLogEntry).Action%></td>
                        <td><%# DirectCast(Container.DataItem, EuropcarServiceLogEntry).ReferenceNumber%></td>
                        <td class="xmlLogCategory"></td>
                        <td class="xmlLogCkiStation"></td>
                        <td class="xmlLogCkoStation"></td>
                        <td class="xmlLogCkiDate"></td>
                        <td class="xmlLogCkoDate"></td>
                        <td class="xmlLogContractId"></td>
                        <td class="xmlLogAccount"></td>
                    </tr>
                    <tr class="xmlLogRequestRow">
                        <td>
                        </td>
                        <td valign="top" colspan="2">
                            <a href="" class="xmlLogExpand">[+]</a>&nbsp;<a href="" class="xmlLogCopy">[Copy]</a>&nbsp;Request:
                        </td>
                        <td colspan="7">
                            <pre><%# EncodeHtml(DirectCast(Container.DataItem, EuropcarServiceLogEntry).RequestXml)%></pre>
                        </td>
                    </tr>
                    <tr class="xmlLogResponseRow">
                        <td>
                        </td>
                        <td valign="top" colspan="2">
                            <a href="" class="xmlLogExpand">[+]</a>&nbsp;<a href="" class="xmlLogCopy">[Copy]</a>&nbsp;Response:
                        </td>
                        <td colspan="7">
                            <pre><%# EncodeHtml(DirectCast(Container.DataItem, EuropcarServiceLogEntry).ResponseXml)%></pre>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                        </tbody>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>

        <br />
        <br />
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button 
                        ID="btnClose" 
                        runat="server" 
                        Text="Back" 
                        CssClass="Button_Back Button_Medium" />
                </td>
            </tr>
        </table>

        <asp:PlaceHolder ID="plhEuropcarLogScript" runat="server" Visible="false">
            <script type="text/javascript">
                (function () {
                    var setupLinks = function () {
                        var oTable = document.getElementById('LogSearchTable');

                        if (!oTable)
                            return;

                        var aLinks = oTable.getElementsByTagName('a'),
                        aPre = oTable.getElementsByTagName('pre'),
                        iMatchIndex = -1,
                        i = -1,
                        iLen = aLinks.length,
                        oLink,
                        oPre;

                        while (++i < iLen) {
                            oLink = aLinks[i];
                            if (oLink.className == 'xmlLogExpand') {
                                oPre = aPre[++iMatchIndex];
                                assignHandlers(oLink, aLinks[++i], oPre);
                            }
                        }
                    },
                    setupXml = function () {
                        var oDataRows = $('#LogSearchTable tbody tr:has(td.xmlLogCategory)'),
                        oXmlRows = $('#LogSearchTable tbody tr.xmlLogRequestRow pre'),
                        oXmlDoc = new ActiveXObject("Microsoft.XMLDOM"),
                        oXmlQuery,
                        i = -1,
                        iLen = oDataRows.length,
                        oRow,
                        oPre,
                        sXml,
                        oXmlDoc;

                        //Parsing XML and populating the data cells if possible
                        while (++i < iLen) {
                            oPre = oXmlRows[i];
                            oRow = oDataRows[i],
                            sXml = '';
                            if (oPre.firstChild)
                                sXml = oPre.firstChild.nodeValue;

                            if (sXml) {
                                //Parse the XML value and extract values
                                //oXmlDoc = jQuery.parseXML(sXml); //(not supported in JQuery 1.3.2)
                                oXmlDoc.loadXML(sXml);
                                oXmlQuery = $('reservation', oXmlDoc);
                                $('td.xmlLogCategory', oRow).text(oXmlQuery.attr('carCategory'));
                                $('td.xmlLogContractId', oRow).text(oXmlQuery.attr('contractID'));

                                //Mean of payment
                                oXmlQuery = $('meanOfPayment', oXmlDoc);
                                $('td.xmlLogAccount', oRow).text(oXmlQuery.attr('businessAccount'));

                                //Checkin information
                                oXmlQuery = $('checkin', oXmlDoc);
                                $('td.xmlLogCkiStation', oRow).text(oXmlQuery.attr('stationID'));
                                $('td.xmlLogCkiDate', oRow).text((oXmlQuery.attr('date') || '') + ' ' + (oXmlQuery.attr('time') || ''));

                                //Checkout information
                                oXmlQuery = $('checkout', oXmlDoc);
                                $('td.xmlLogCkoStation', oRow).text(oXmlQuery.attr('stationID'));
                                $('td.xmlLogCkoDate', oRow).text((oXmlQuery.attr('date') || '') + ' ' + (oXmlQuery.attr('time') || ''));
                            }
                        }
                    },
                    getXmlNodeValue = function (query, selector) {
                        var oNode = query(selector)[0];
                        if (oNode && oNode.firstChild)
                            return oNode.nodeValue;
                    },
                    assignHandlers = function (link, copyLink, preElement) {
                        var expandAction = function () {
                            link.firstChild.nodeValue = '[-]';
                            preElement.style.display = 'block';
                            link.onclick = collapseAction;
                            return false;
                        },
                        collapseAction = function () {
                            document.selection.empty ();
                            link.firstChild.nodeValue = '[+]';
                            preElement.style.display = 'none';
                            link.onclick = expandAction;
                            return false;
                        };

                        link.onclick = expandAction;
                        copyLink.onclick = function () {
                            expandAction();
                            window.clipboardData.setData("Text", selectNodeContents(preElement));
                            return false;
                        };
                    },
                    selectNodeContents = function (el) {                        
                        if (window.getSelection && document.createRange) {
                            var sel = window.getSelection();
                            var range = document.createRange();
                            range.selectNodeContents(el);
                            sel.removeAllRanges();
                            sel.addRange(range);
                        } else if (document.selection && document.body.createTextRange) {
                            var textRange = document.body.createTextRange();
                            textRange.moveToElementText(el);
                            textRange.select();
                        }

                        return document.selection.createRange().text;
                    };
                    setupLinks();
                    setupXml();
                })();
            </script>
        </asp:PlaceHolder>
    </asp:PlaceHolder>
</asp:Panel>