﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingDetailsUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingDetailsUserControl" %>
<table width="100%">
    <tr>
        <td width="6%">
            Agent:
        </td>
        <td width="45%">
            <asp:TextBox ID="agentTextBox" runat="server" ReadOnly="true" Width="330" />
        </td>
        <td width="6%">
            Hirer:
        </td>
        <td width="30%">
            <asp:TextBox ID="hirerTextBox" runat="server" ReadOnly="true" Width="200" />
        </td>
        <td width="6%">
            Status:
        </td>
        <td>
            <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="50" />
        </td>
    </tr>
</table>
<br />
<table width="100%">
    <tr>
        <td width="100%" colspan="2">
            <asp:GridView ID="rentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                CssClass="dataTableGrid">
                <AlternatingRowStyle CssClass="oddRow" />
                <RowStyle CssClass="evenRow" />
                <Columns>
                    <asp:BoundField DataField="RentalNo" HeaderText="Rental" ReadOnly="True" />
                    <asp:TemplateField HeaderText="Check-out">
                        <ItemTemplate>
                            <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-Out")) + GetArrivalDepartureTime(eval("RntArrivalAtBranchDateTime")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Check-in">
                        <ItemTemplate>
                            <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-In")) + GetArrivalDepartureTime(eval("RntDropOffAtBranchDateTime")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                    <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" />
                    <asp:BoundField DataField="Brand" HeaderText="Brand" ReadOnly="True" />
                    <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
                 
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>
<br />