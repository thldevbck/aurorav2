'' Change Log 
'' 11.6.8 - Shoel -fixed major bug with Check-In Issue 38 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=38)
'' 12.6.8 - Shoel -fixed Issue 42 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=42)
''  1.7.8 - Shoel - added the date utility thingie + removed commented/unused code bits
''  2.7.8 - Shoel - got the date thingie working - so i think...
''  2.7.8 - Shoel - fixed 'Starship Troopers'-sized bugs in CheckIn -> looks good-n-working now :)
'' 18.7.8 - Shoel - fixed picker name issue (AKL - Auckland is now displayed, instead of just AKL)
'' 21.7.8 - Shoel - Session replaced with ViewState
'' 22.7.8 - Shoel - Last remaining session bit replaced with ViewState :)
'' 22.7.8 - Shoel - some changes to check-in / check-out screen display
''  5.8.8 - Shoel - Added raisebubble thingie - refreshes the rental grid on parent page
'' 15.8.8 - Shoel - Fixes for SQUISH# 559,560,562
'' 15.8.8 - Shoel - Allows check-in from anylocation and not just from users country
'' 18.8.8 - Shoel - Fixes for checkincheckout popup and forced validations
'' 20.8.8 - Shoel - Fixes for checkincheckout popup and forced validations - SQUISH# 564,568,563,562,559
'' 22.8.8 - Shoel - Fix for squish # 582, Method moved to BookingUserControl.vb
'' 15.10.8 -Shoel - Fix for missing unit and rego info in checked in rental data!
'' 16.10.8 -Shoel - Fix for checkin location changing on initial popup!!!!
'' 18.2.10 -Shoel - Added supervisor override for precheckout
'' 8.6.10  -Shoel - Unit number and rego are now trimmed
'' 26.8.10 -Shoel - Added supervisor override stuff for billing tokens

'' List of Stored procedures used (donot delete)
'' cus_GetBookingDetail
'' RES_getLocStatus
'' GEN_GetPopUpData
'' RES_managePreCheckOut
'' RES_manageCheckOut
'' RES_manageCheckIn
'' RES_getCrossHire
'' RES_maitainContractHistory
'' RES_Generate_Rental_Agreement
'' RES_getAIMSVehicleName
'' RES_GetProductsRequiringSerialNumber
'' RES_UpdateBPDSerialNumbers
'' RES_GetRentalIntegrity
'' RES_UpdateRentalIntegrity
'' RES_UpdateRentalIntegrityForBooking

Imports System.Xml
Imports Aurora.Common.Utility

<AuroraMessageAttribute("GEN005,GEN155,GEN156,GEN157,GEN046")> _
Partial Class Booking_Controls_BookingCheckInCheckOutUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean

    Const VIEWSTATE_CICOFIRSTLOAD As String = "VIEWSTATE_CICO_FirstLoad"
    Const VIEWSTATE_CICOXMLDATA As String = "VIEWSTATE_CICO_XMLDATA"
    Const VIEWSTATE_SECONDTIME As String = "VIEWSTATE_CICO_SecondTime"
    Const VIEWSTATE_RENTALINFO As String = "VIEWSTATE_CICO_RentalInfo"

    Const VIEWSTATE_CICOERRORTYPE As String = "VIEWSTATE_CICO_ErrorType"

    Const VIEWSTATE_BOOKINGID As String = "VIEWSTATE_CICO_BooId"
    Const VIEWSTATE_RENTALID As String = "VIEWSTATE_CICO_RntId"

    'popup thingies
    Const VIEWSTATE_POPUPTYPE As String = "VIEWSTATE_CICO_PopupType"
    Const VIEWSTATE_GFIRSTTIME_SCREENVALUE As String = "VIEWSTATE_CICO_gFirstTime_ScreenValue"
    Const VIEWSTATE_GFORCED As String = "VIEWSTATE_CICO_gForced"
    Const VIEWSTATE_GUSERCODE As String = "VIEWSTATE_CICO_gUsrCode"

    Const CHECK_IN_CHECK_OUT As String = "Booking/CheckInCheckOut"
    Const VIEWSTATE_USERCODE As String = "VIEWSTATE_CICO_UserCode"

    Const VIEWSTATE_USERROLE As String = "VIEWSTATE_CICO_UserRole"

    Const VIEWSTATE_UNITNOOLD As String = "VIEWSTATE_CICO_UnitNoOld"
    Const VIEWSTATE_VEHICLEOLD As String = "VIEWSTATE_CICO_VehicleOld"

    Const VIEWSTATE_SHOULD_PRINT As String = "ViewState_CICO_ShouldPrint"

    Const VIEWSTATE_CHECKOUT_VALIDATIONS_DONE As String = "Viewstate_CheckoutValidationsDone"
    Const VIEWSTATE_CHECKIN_VALIDATIONS_DONE As String = "Viewstate_CheckinValidationsDone"

    ' Added by Nimesh on 20th may 2015 to store RntAllowVehicleSpec
    Const VIEWSTATE_RNTALLOWVEHSPEC As String = "ViewState_RntAllowVehSpec"
    ' End Added by Nimesh on 20th may to store RntAllowVehicleSpec
    ' Added by Nimesh on 17th July 2015 to store IsServiceDone
    Const VIEWSTATE_ISSERVICEDONE As String = "ViewState_IsServiceDone"
    ' End Added by Nimesh on 17th July 2015 to store IsServiceDone
    Public Property RentalCountryCode() As String
        Get
            Return ViewState("CICO_RntCtyCode")
        End Get
        Set(ByVal value As String)
            ViewState("CICO_RntCtyCode") = value.Trim().ToUpper()
        End Set
    End Property

    Public Property RntAllowVehSpec() As String
        Get
            Return ViewState("RntAllowVehSpec")
        End Get
        Set(ByVal value As String)
            ViewState("RntAllowVehSpec") = value.Trim().ToUpper()
        End Set
    End Property

    Public Property FleetModelCode() As String
        Get
            Return ViewState("FleetModelCode")
        End Get
        Set(ByVal value As String)
            ViewState("FleetModelCode") = value.Trim().ToUpper()
        End Set
    End Property
    Sub NullifyOldVIEWSTATEData()
        ViewState(VIEWSTATE_CICOFIRSTLOAD) = Nothing
        ViewState(VIEWSTATE_CICOXMLDATA) = Nothing
        ViewState(VIEWSTATE_SECONDTIME) = Nothing
        ViewState(VIEWSTATE_RENTALINFO) = Nothing

        ViewState(VIEWSTATE_CICOERRORTYPE) = Nothing

        ViewState(VIEWSTATE_BOOKINGID) = Nothing
        ViewState(VIEWSTATE_RENTALID) = Nothing
        'Session(SESSION_RENTALID) = Nothing

        'popup thingies
        ViewState(VIEWSTATE_POPUPTYPE) = Nothing
        ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = Nothing
        ViewState(VIEWSTATE_GFORCED) = Nothing
        ViewState(VIEWSTATE_GUSERCODE) = Nothing

        ViewState(CHECK_IN_CHECK_OUT) = Nothing
        ViewState(VIEWSTATE_USERCODE) = Nothing

        ViewState(VIEWSTATE_USERROLE) = Nothing

        ViewState(VIEWSTATE_UNITNOOLD) = Nothing
        ViewState(VIEWSTATE_VEHICLEOLD) = Nothing

        ' reset the user control
        popupSerialNos.CurrentRentalId = ""
        popupSerialNos.MSNMode = ""
        popupSerialNos.SerialNumbersCheckPassed = False

        ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = False
        ViewState(VIEWSTATE_CHECKIN_VALIDATIONS_DONE) = False
        'Session(SESSION_SERIALNOSCHECKPASSED) = Nothing
        ViewState(VIEWSTATE_RNTALLOWVEHSPEC) = Nothing
        ViewState(VIEWSTATE_ISSERVICEDONE) = Nothing
        RentalCountryCode = String.Empty
        RntAllowVehSpec = String.Empty
    End Sub

    Public Sub LoadPage()
        If isFirstLoad Then
            ClearScreenElements()
            NullifyOldVIEWSTATEData()
            trOdometerOut.Visible = False
            trOdometerIn.Visible = False
        End If

        Dim oScreenData As System.Xml.XmlDocument = New XmlDocument
        Dim oRentalData As XmlDocument = New XmlDocument

        ViewState(VIEWSTATE_BOOKINGID) = BookingId
        ViewState(VIEWSTATE_RENTALID) = RentalId
        ' Session(SESSION_RENTALID) = RentalId
        popupSerialNos.CurrentRentalId = RentalId
        '  ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ConfirmCheckInOut", "ConfirmCheckInOutMsgBox();", True)
        'something in VIEWSTATE
        If ViewState(VIEWSTATE_CICOFIRSTLOAD) Is Nothing Or CBool(ViewState(VIEWSTATE_CICOFIRSTLOAD)) Then

            oScreenData = Aurora.Booking.Services.BookingCheckInCheckOut.GetDataForCheckInCheckOut(BookingId, RentalId, CurrentPage.UserCode, 0, "", RentalCountryCode)
            'check if there is an error in retrieving the xml
            If Not oScreenData.SelectSingleNode("//Error/Message") Is Nothing Then
                'If Trim(oScreenData.SelectSingleNode("//Error/Message").InnerText).Length > 100 Then
                CurrentPage.AddErrorMessage(Trim(oScreenData.SelectSingleNode("//Error/Message").InnerText))
                'Else
                'CurrentPage.AddErrorMessage(Trim(oScreenData.SelectSingleNode("//Error/Message").InnerText))
                '   End If

                tblInitial.Style.Item("display") = "none"
                tblMain.Style.Item("display") = "none"
                Exit Sub
            End If
            ' no error , proceed
            ViewState.Add(VIEWSTATE_CICOXMLDATA, oScreenData.OuterXml)
            ViewState(VIEWSTATE_CICOFIRSTLOAD) = False
            'rental info
            oRentalData = Aurora.Booking.Services.BookingCheckInCheckOut.GetRentalInfo(BookingId, RentalId)
            ViewState(VIEWSTATE_RENTALINFO) = oRentalData.OuterXml

            'store this
        Else
            ' not the first time its loaded , so retrieve from VIEWSTATE
            oScreenData.LoadXml(CStr(ViewState(VIEWSTATE_CICOXMLDATA)))
            oRentalData.LoadXml(CStr(ViewState(VIEWSTATE_RENTALINFO)))
        End If

        ' check the popup value
        ' Added by Nimesh on 13th may 15 to check if Terms and conditions is accepted or not
        'Commented on 18th may by nimesh as this is no longer required - https://thlonline.atlassian.net/browse/DSD-623 
        ' Additionally Changed stored procedure - RES_validateCheckInOut
        'Dim isTCAccepted As Boolean = CBool(oScreenData.SelectSingleNode("//data/IsAcceptTC").InnerText)

        'hdnIsTCAccepted.Value = isTCAccepted

        ' End Added by Nimesh on 13th may 15 to check if Terms and conditions is accepted or not
        If oScreenData.SelectSingleNode("//data/PopupFlag") Is Nothing Then
            Exit Sub
        End If
        ' Added by Nimesh on 20th may 15 For retriving rntallowvehspec and fleetmodelcode -- https://thlonline.atlassian.net/browse/DSD-686
        If oScreenData.SelectSingleNode("//data/ForcedRenAllowVehSpec") IsNot Nothing Then
            RntAllowVehSpec = oScreenData.SelectSingleNode("//data/ForcedRenAllowVehSpec").InnerText
        End If
        If oScreenData.SelectSingleNode("//data/ForcedFleetModelCode") IsNot Nothing Then
            FleetModelCode = oScreenData.SelectSingleNode("//data/ForcedFleetModelCode").InnerText
        End If
        'End Added by Nimesh on 20th may 15 For retriving rntallowvehspec and fleetmodelcode -- https://thlonline.atlassian.net/browse/DSD-686
        If (oScreenData.SelectSingleNode("//data/PopupFlag").InnerText = "1" Or CBool(ViewState(VIEWSTATE_SECONDTIME))) And pkrCheckInLocation.IsValid Then ' there is no popup
            'hide the popup
            'tblInitial.Visible = False
            tblInitial.Style.Item("display") = "none"
            tblMain.Style.Item("display") = "block"

            '   VIEWSTATE.Add(VIEWSTATE_CICOXMLDATA, oScreenData) lblCkiLocation setting
            If pkrCheckInLocation.Visible = True Then
                oScreenData.SelectSingleNode("//data/RntCkiLocCode").InnerText = pkrCheckInLocation.Text
                ' override the user specified loc code as well
                oScreenData.SelectSingleNode("//data/UserLocCode").InnerText = pkrCheckInLocation.Text
                ViewState(VIEWSTATE_CICOXMLDATA) = oScreenData.OuterXml
            End If

        Else
            ' there is popup
            ' show the new table

            tblInitial.Style.Item("display") = "block"
            tblMain.Style.Item("display") = "none"

            ' check in / check out fix
            If oScreenData.SelectSingleNode("//data/What").InnerText = "Check In" Then
                lblTextForCICOInitital1.Text = "Please select the correct"
                lblTextForCICOInitital2.Text = "location for this rental :"
                trDisplayForCO.Style.Item("display") = "none"
                lblCkoCkiLocInitialProcess.Visible = False
                pkrCheckInLocation.Visible = True
                ' this is commented out to allow checkins from anywhere
                'pkrCheckInLocation.Param1 = oScreenData.SelectSingleNode("//data/CtyCode").InnerText
                pkrCheckInLocation.Text = oScreenData.SelectSingleNode("//data/YrLoc").InnerText

            ElseIf oScreenData.SelectSingleNode("//data/What").InnerText = "Check Out" Then
                lblTextForCICOInitital1.Text = "The"
                lblTextForCICOInitital2.Text = "will be processed for this location :"
                trDisplayForCO.Style.Item("display") = "block"
                lblCkoCkiLocInitialProcess.Visible = True
                pkrCheckInLocation.Visible = False
            End If
            'set data
            'lblInitialCkiCkoStatus.Text = oScreenData.SelectSingleNode("//data/What").InnerText
            txtInitialCkiCkoStatus.Text = oScreenData.SelectSingleNode("//data/What").InnerText
            lblCkoCkiInitialWord1.Text = oScreenData.SelectSingleNode("//data/What").InnerText
            lblCkoCkiLocInitialRnt.Text = oScreenData.SelectSingleNode("//data/DispLoc").InnerText
            lblCkoCkiLocInitialYour.Text = oScreenData.SelectSingleNode("//data/YrLoc").InnerText
            lblCkoCkiInitialWord2.Text = oScreenData.SelectSingleNode("//data/What").InnerText
            lblCkoCkiLocInitialProcess.Text = oScreenData.SelectSingleNode("//data/RntCkoLocCode").InnerText
            lblCkoCkiInitialWord3.Text = oScreenData.SelectSingleNode("//data/What").InnerText

        End If

        ' other laodings

        Select Case oScreenData.SelectSingleNode("//data/RntStaus").InnerText.Trim().ToUpper()
            Case "CO"
                ShowCheckInItems()

                ' if 6 berth vehicle in NZ then show message
                If RentalCountryCode = "NZ" _
                   AndAlso lblVehicleDetails.Text.Trim().IndexOf("6") = 0 Then
                    trOdometerIn.Visible = True
                    lblOdometerIn.Text = "Hubbo In:"
                End If

                txtOdometerIn.Attributes.Add("OnBlur", _
                    "document.getElementById('" & dcCheckInDate.ClientID & "').focus();" & _
                    "return validateOdometerData('" & _
                        oScreenData.DocumentElement.SelectSingleNode("CtyCode").InnerText & "','" & _
                        oScreenData.DocumentElement.SelectSingleNode("RntStaus").InnerText & "','" & _
                        oScreenData.DocumentElement.SelectSingleNode("OddometerOut").InnerText & "','" & _
                        oRentalData.DocumentElement.SelectSingleNode("Rental/HirePd").InnerText & "','" & _
                        txtOdometerIn.ClientID & "','" & _
                        lblOdometerDiff.ClientID & "');")

                txtOdometerIn.Attributes.Add("OnChange", _
                    "document.getElementById('" & dcCheckInDate.ClientID & "').focus();" & _
                    "return validateOdometerData('" & _
                        oScreenData.DocumentElement.SelectSingleNode("CtyCode").InnerText & "','" & _
                        oScreenData.DocumentElement.SelectSingleNode("RntStaus").InnerText & "','" & _
                        oScreenData.DocumentElement.SelectSingleNode("OddometerOut").InnerText & "','" & _
                        oRentalData.DocumentElement.SelectSingleNode("Rental/HirePd").InnerText & "','" & _
                        txtOdometerIn.ClientID & "','" & _
                        lblOdometerDiff.ClientID & "');")

                Exit Select
            Case "KK"
                'check out only
                ShowCheckOutItems()
                ' more attribs being added
                'txtRegNo.Attributes.Add("OnChange", "return UnitValidate('" & _
                '    txtUnitNo.ClientID & "','" & _
                '    lblVehicleDetails.ClientID & "','" & _
                '    txtOdometerOut.ClientID & "','" & _
                '    "" & "','" & _
                '    txtRegNo.ClientID & "','" & _
                '    hdnWhoWasClicked.ClientID & "');")
                'txtUnitNo.Attributes.Add("OnChange", "return UnitValidate('" & _
                '    txtUnitNo.ClientID & "','" & _
                '    lblVehicleDetails.ClientID & "','" & _
                '    txtOdometerOut.ClientID & "','" & _
                '    "" & "','" & _
                '    txtRegNo.ClientID & "','" & _
                '    hdnWhoWasClicked.ClientID & "');")

                Exit Select
            Case Else
                ShowPostCIItems()
                Exit Select
        End Select
        'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was

        btnReprint.Attributes.Add("onclick", "ShowAgreement('" & _
            RentalId & "$" & BookingId & "$" & CurrentPage.UserCode & "');")

        BtnReprintAmazon.Attributes.Add("onclick", "ShowAgreementAmazon('" & _
           RentalId & "$" & BookingId & "$" & CurrentPage.UserCode & "$Amazon');")
        Dim tempReturn As String = Aurora.Common.Data.ExecuteScalarSP("TCX_GetRentalFilename", RentalId)

        If String.IsNullOrEmpty(tempReturn) Then
            BtnReprintAmazon.Enabled = False
        Else
            BtnReprintAmazon.Enabled = True
        End If

        'If Aurora.Common.Data.ExecuteScalarSQL("SELECT Count(*) from dbo.Rentalfiles where RntId=" + RentalId) Then
        'end nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
        'lblInitialCkiCkoStatus.Text = oScreenData.DocumentElement.SelectSingleNode("RntStausDes").InnerText
        txtInitialCkiCkoStatus.Text = _
            oScreenData.DocumentElement.SelectSingleNode("RntStaus").InnerText _
            & " - " _
            & oScreenData.DocumentElement.SelectSingleNode("RntStausDes").InnerText
        txtInitialCkiCkoStatus.BackColor = Aurora.Common.DataConstants.GetBookingStatusColor(oScreenData.DocumentElement.SelectSingleNode("RntStaus").InnerText)

        'lblCICOStatus.Text = oScreenData.DocumentElement.SelectSingleNode("RntStausDes").InnerText
        txtCICOStatus.Text = oScreenData.DocumentElement.SelectSingleNode("RntStausDes").InnerText

        txtUnitNo.Text = oScreenData.DocumentElement.SelectSingleNode("UnitNo").InnerText.Trim()
        txtRegNo.Text = oScreenData.DocumentElement.SelectSingleNode("Rego").InnerText.Trim()
        txtAreaOfUse.Text = Server.HtmlDecode(oScreenData.DocumentElement.SelectSingleNode("RntAreaUse").InnerText)

        txtVoucherNo.Text = Server.HtmlDecode(oScreenData.DocumentElement.SelectSingleNode("Voucher").InnerText)

        txtOdometerOut.Text = oScreenData.DocumentElement.SelectSingleNode("OddometerOut").InnerText
        txtOdometerIn.Text = oScreenData.DocumentElement.SelectSingleNode("OddometerIn").InnerText

        'dcCheckOutDate.Text = oScreenData.DocumentElement.SelectSingleNode("RntCkoWhen").InnerText
        ' system fmt -> date obj
        dcCheckOutDate.Date = ParseDateTime(oScreenData.DocumentElement.SelectSingleNode("RntCkoWhen").InnerText, SystemCulture)
        'dcCheckInDate.Text = oScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText
        ' system fmt -> date obj
        dcCheckInDate.Date = ParseDateTime(oScreenData.DocumentElement.SelectSingleNode("RntCkiWhen").InnerText, SystemCulture)

        tcCheckOutTime.Text = oScreenData.DocumentElement.SelectSingleNode("RntCkoTime").InnerText
        ''rev:mia Dec 5 2011 - Change Request - remove default Check In time field in Check In/Out tab in Aurora
        ''tcCheckInTime.Text = oScreenData.DocumentElement.SelectSingleNode("RntCkiTime").InnerText


        ' fix for missing CI info!
        txtCIRegNo.Text = oScreenData.DocumentElement.SelectSingleNode("CkiRego").InnerText
        txtCIUnitNo.Text = oScreenData.DocumentElement.SelectSingleNode("ChkInUnitNo").InnerText

        lblArrivalConnection.Text = oScreenData.DocumentElement.SelectSingleNode("RntArrConn").InnerText
        lblDepartureConnection.Text = oScreenData.DocumentElement.SelectSingleNode("RntDeptConn").InnerText

        lblVehicleDetails.Text = oScreenData.DocumentElement.SelectSingleNode("Vehicle").InnerText

        lblCkoLocation.Text = oScreenData.DocumentElement.SelectSingleNode("RntCkoLocCode").InnerText
        lblCkiLocation.Text = oScreenData.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText

        lblOdometerDiff.Text = oScreenData.DocumentElement.SelectSingleNode("OddoDiff").InnerText

        ViewState(VIEWSTATE_CICOFIRSTLOAD) = False

        If oScreenData.DocumentElement.SelectSingleNode("EnableSerialButt").InnerText = "1" Then
            ' enable edit button
            btnMaintainSerialNumbers.Enabled = True
        Else
            ' disable edit button
            btnMaintainSerialNumbers.Enabled = False
        End If

        '<Data>
        '	<Err></Err>
        '	<Msg></Msg>
        '	<BooId>3294375</BooId>
        '	<RntId>3294375-1</RntId>
        '	<BooRntNum>3294375/1</BooRntNum>
        '	<BpdId>HA2715665</BpdId>
        '	<CrossHr>0</CrossHr>
        '	<Vehicle>Cross Hire Vehicle</Vehicle>
        '	<RntAreaUse></RntAreaUse>
        '	<RntCkoLocCode>AKL - Auckland</RntCkoLocCode>
        '	<RntCkiLocCode>AKL - Auckland</RntCkiLocCode>
        '	<UnitNo></UnitNo>
        '	<Rego></Rego>
        '	<Voucher></Voucher>
        '	<UserLocCode>AKL - Auckland</UserLocCode>
        '	<RntStaus>QN</RntStaus>
        '	<RntStausDes>Quoted</RntStausDes>
        '	<YrLoc>AKL - Auckland</YrLoc>
        '	<DispLoc></DispLoc>
        '	<CtyCode>NZ</CtyCode>
        '	<PopupFlag>1</PopupFlag>
        '	<What></What>
        '	<OddometerOut>0</OddometerOut>
        '	<OddometerIn>0</OddometerIn>
        '	<OddoDiff>0</OddoDiff>
        '	<RntCkoWhen>06/04/2007</RntCkoWhen>
        '	<RntCkoTime></RntCkoTime>
        '	<ChkInUnitNo></ChkInUnitNo>
        '	<CkiRego></CkiRego>
        '	<RntCkiWhen>10/04/2007</RntCkiWhen>
        '	<RntCkiTime>17:00</RntCkiTime>
        '	<RntArrConn></RntArrConn>
        '	<RntDeptConn></RntDeptConn>
        '	<DesAll>1</DesAll>
        '	<DesHd1>0</DesHd1>
        '	<DesHd2>0</DesHd2>
        '	<DesHd3>0</DesHd3>
        '	<DesCO>0</DesCO>
        '	<DesCI>0</DesCI>
        '	<DesBut1>0</DesBut1>
        '	<DesBut2>0</DesBut2>
        '	<DesBut3>0</DesBut3>
        '	<DesBut4>0</DesBut4>
        '	<DesBut5>0</DesBut5>
        '	<DVASSCall>0</DVASSCall>
        '	<PmtDone>0</PmtDone>
        '	<IntNo>1</IntNo>
        '	<KMOdo>0</KMOdo>
        '	<DefDate>08/05/2008</DefDate>
        '	<DefTime>16:20</DefTime>
        '	<RntIntNo>1</RntIntNo>
        '	<BooIntNo>1</BooIntNo>
        '	<EnableSerialButt>0</EnableSerialButt>
        '</Data>



    End Sub

    Public Sub ShowCheckOutItems()
        btnCheckOut.Enabled = True
        btnPreCheckOut.Enabled = True
        btnReprint.Enabled = False
        'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
        'BtnReprintAmazon.Enabled = False
        btnCheckIn.Enabled = False
        btnCheckInNoPrint.Enabled = False

        dcCheckInDate.ReadOnly = True
        tcCheckInTime.ReadOnly = True

        txtCIRegNo.ReadOnly = True
        txtCIUnitNo.ReadOnly = True
        txtOdometerIn.ReadOnly = True

        dcCheckOutDate.ReadOnly = True
        tcCheckOutTime.ReadOnly = False

        txtUnitNo.ReadOnly = False
        txtRegNo.ReadOnly = False
        txtOdometerOut.ReadOnly = False
        txtAreaOfUse.ReadOnly = False

    End Sub

    Public Sub ShowCheckInItems()
        btnCheckOut.Enabled = False
        btnPreCheckOut.Enabled = False
        btnReprint.Enabled = True
        'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
        'BtnReprintAmazon.Enabled = True
        btnCheckIn.Enabled = True
        btnCheckInNoPrint.Enabled = True

        dcCheckOutDate.ReadOnly = True
        tcCheckOutTime.ReadOnly = True

        txtUnitNo.ReadOnly = True
        txtRegNo.ReadOnly = True
        txtOdometerOut.ReadOnly = True
        txtAreaOfUse.ReadOnly = True

        dcCheckInDate.ReadOnly = False
        tcCheckInTime.ReadOnly = False

        txtCIUnitNo.ReadOnly = False
        txtCIRegNo.ReadOnly = False
        txtOdometerIn.ReadOnly = False
        txtVoucherNo.ReadOnly = True


    End Sub

    Sub ShowPostCIItems()
        btnCheckOut.Enabled = False
        btnPreCheckOut.Enabled = False
        btnReprint.Enabled = True
        'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was
        'BtnReprintAmazon.Enabled = True
        btnCheckIn.Enabled = False
        btnCheckInNoPrint.Enabled = False
        btnMaintainSerialNumbers.Enabled = True

        txtUnitNo.ReadOnly = True
        txtRegNo.ReadOnly = True
        txtOdometerOut.ReadOnly = True
        txtAreaOfUse.ReadOnly = True

        dcCheckInDate.ReadOnly = True
        tcCheckInTime.ReadOnly = True

        dcCheckOutDate.ReadOnly = True
        tcCheckOutTime.ReadOnly = True

        txtCIUnitNo.ReadOnly = True
        txtCIRegNo.ReadOnly = True
        txtOdometerIn.ReadOnly = True

        txtVoucherNo.ReadOnly = True
    End Sub

    Protected Sub btnCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckOut.Click
        ''rev:mia nov 30 2011 - bonifacio day - 25084 Issue with Mandatory Fields in Customer Maintenance Screen

        Try


            Dim cusId As String = Aurora.Booking.Data.DataRepository.ValidateCustomerFieldsBeforeCheckOut(Me.BookingNumber, Me.RentalNo)

            Aurora.Common.Logging.LogDebug("btnCheckOut_Click", cusId)
            If (String.IsNullOrEmpty(cusId)) Then
                CurrentPage.SetWarningShortMessage("Please complete your address details in the Customer Maintenance screen")

                Exit Sub
            End If



            Dim isForcedSatisfied As Boolean = False


            ' Added by Nimesh on 20th may 2015 for validating forced booking -https://thlonline.atlassian.net/browse/DSD-686
            If Not String.IsNullOrEmpty(Me.RntAllowVehSpec.Trim) Then
                If Not IsForcedRentalCheckSatisfied(RntAllowVehSpec.Trim) Then
                    ViewState(VIEWSTATE_USERROLE) = "OLMAN"
                    ViewState(VIEWSTATE_POPUPTYPE) = "ALWAYS"
                    supervisorOverrideControl.Show(ErrorMessage:="(OLMAN)<br/>Supervisor override is needed to complete this checkout as this vehicle is been forced")
                Else

                    isForcedSatisfied = True
                End If
            ElseIf Not String.IsNullOrEmpty(Me.FleetModelCode) Then
                'If Not lblVehicleDetails.Text.Trim.Substring(0, FleetModelCode.Trim.Length).Equals(FleetModelCode.Trim) Then
                If Not IsForcedRentalCheckSatisfied(FleetModelCode.Trim) Then
                    ViewState(VIEWSTATE_USERROLE) = "OLMAN"
                    ViewState(VIEWSTATE_POPUPTYPE) = "ALWAYS"
                    supervisorOverrideControl.Show(ErrorMessage:="(OLMAN)<br/>Supervisor override is needed to complete this checkout as this vehicle is been forced")
                Else

                    'CurrentPage.SetWarningShortMessage("forced condition is satisfied for vehicle type")
                    isForcedSatisfied = True
                End If
            Else
                isForcedSatisfied = True
            End If


            ' nimesh comment the line below
            'Exit Sub
            ' End Added by Nimesh on 20th may 2015 for validating forced booking -https://thlonline.atlassian.net/browse/DSD-686

            '' Commented by nimesh and added new method on 25th May 2015
            'Dim xmlReturn As XmlDocument
            'xmlReturn = Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber(CStr(ViewState(VIEWSTATE_RENTALID)), False)
            'If xmlReturn.DocumentElement.InnerXml.Trim() = "<Products></Products>" Then
            '    ' not applicable
            '    CheckOut()
            'Else
            '    ' applicable
            '    ' Session(SESSION_MAINTAINSERIALNUMBERMODE) = "Empty"
            '    popupSerialNos.MSNMode = "Empty"
            '    popupSerialNos.CurrentRentalId = RentalId
            '    popupSerialNos.LoadPopUpControl()
            'End If
            If isForcedSatisfied Then
                PerformCheckOut()
            End If

            'End Commented by nimesh and added new method on 25th May 2015

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub popupSerialNos_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles popupSerialNos.PostBack
        ' check if everything was done! - the serial nos biz should have left a TRUE in teh ViewState Field

        'If Not Session(SESSION_MAINTAINSERIALNUMBERMODE) = "All" Then ' ignoring the Edit mode return
        '    If CBool(Session(SESSION_SERIALNOSCHECKPASSED)) Then
        '        CheckOut()
        '    Else
        '        CurrentPage.SetErrorShortMessage("Serial Numbers must be entered for all products that require it")
        '    End If
        'End If

        'Session(SESSION_MAINTAINSERIALNUMBERMODE) = Nothing ' resetting this

        If Not popupSerialNos.MSNMode = "All" Then ' ignoring the Edit mode (MSNMode=All) return
            If popupSerialNos.SerialNumbersCheckPassed Then
                CheckOut()
            Else
                CurrentPage.SetWarningShortMessage("Serial Numbers must be entered for all products that require it")
            End If
        End If

        popupSerialNos.MSNMode = "" ' resetting this

    End Sub

    Public Sub CheckOut()

        If Not IsFormDataValid("CO") Then
            Exit Sub
        End If
        ' Added by Nimesh on 17thJuly 2015 - to display message if vehicle is due for service
        'If Not ViewState(VIEWSTATE_ISSERVICEDONE) Then
        '    Dim IsServiceRequired As Boolean = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.CheckIfServiceDUE(" + txtUnitNo.Text.Trim() + ") ")
        '    If Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.CheckIfServiceDUE(" + txtUnitNo.Text.Trim() + ") ") Then
        '        ShowConfirmationDialog("This Vehicle is DUE for service", "Due For Service", "POPUP-SOD")
        '        Exit Sub
        '    Else
        '        ViewState(VIEWSTATE_ISSERVICEDONE) = True
        '    End If
        'End If

        'Exit Sub
        ' End Added by Nimesh on 17thJuly 2015 - to display message if vehicle is due for service
        'preliminary validations assumed complete
        Dim oXmlScreenData As System.Xml.XmlDocument = New XmlDocument
        oXmlScreenData.LoadXml(CStr(ViewState(VIEWSTATE_CICOXMLDATA)))

        'something weird here...looking for global data????
        oXmlScreenData.DocumentElement.InnerXml = oXmlScreenData.DocumentElement.InnerXml & _
                                                  "<UnitNoOld>" & CStr(ViewState(VIEWSTATE_UNITNOOLD)) & "</UnitNoOld>" & _
                                                  "<VehicleOld>" & CStr(ViewState(VIEWSTATE_VEHICLEOLD)) & "</VehicleOld>" & _
                                                  "<Forced>" & IIf(CStr(ViewState(VIEWSTATE_GFORCED)) Is Nothing, "0", CStr(ViewState(VIEWSTATE_GFORCED))) & "</Forced>"





        With oXmlScreenData.DocumentElement
            'updating data from screeen
            .SelectSingleNode("Voucher").InnerText = Server.HtmlEncode(txtVoucherNo.Text)
            ' ui format -> sys format
            .SelectSingleNode("RntCkoWhen").InnerText = DateTimeToString(dcCheckOutDate.Date, SystemCulture).Split(" "c)(0) ' dcCheckOutDate.Text
            .SelectSingleNode("RntCkoTime").InnerText = tcCheckOutTime.Text
            .SelectSingleNode("OddometerOut").InnerText = txtOdometerOut.Text
            .SelectSingleNode("RntAreaUse").InnerText = Server.HtmlEncode(txtAreaOfUse.Text)
            .SelectSingleNode("UnitNo").InnerText = txtUnitNo.Text.Trim()
            .SelectSingleNode("Rego").InnerText = txtRegNo.Text.Trim()

            If NodeExistsAndIsNotEmpty(.SelectSingleNode("Vehicle")) Then
                If Not .SelectSingleNode("Vehicle").InnerText.Trim().ToUpper().Equals("CROSS HIRE VEHICLE") Then
                    .SelectSingleNode("Vehicle").InnerText = .SelectSingleNode("Vehicle").InnerText.Split("-"c)(0).Trim()
                End If
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("UserLocCode")) Then
                .SelectSingleNode("UserLocCode").InnerText = .SelectSingleNode("UserLocCode").InnerText.Split("-"c)(0).Trim()
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("RntCkiLocCode")) Then
                .SelectSingleNode("RntCkiLocCode").InnerText = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0).Trim()
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("RntCkoLocCode")) Then
                .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0).Trim()
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("YrLoc")) Then
                .SelectSingleNode("YrLoc").InnerText = .SelectSingleNode("YrLoc").InnerText.Split("-"c)(0).Trim()
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("DispLoc")) Then
                .SelectSingleNode("DispLoc").InnerText = .SelectSingleNode("DispLoc").InnerText.Split("-"c)(0).Trim()
            End If

        End With



        Dim sErr As String = ""
        Dim oRetXml As System.Xml.XmlDocument
        Aurora.Common.Logging.LogDebug("Checkout", "oXmlScreenData: " & oXmlScreenData.OuterXml & vbCrLf & "CurrentPage.UserCode:" & CurrentPage.UserCode & vbCrLf & "CHECK_IN_CHECK_OUT:" & CHECK_IN_CHECK_OUT & vbCrLf & "CStr(ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE)):" & CStr(ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE)) & vbCrLf & "sErr:" & sErr & vbCrLf & "CBool(ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE)):" & CBool(ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE)))
        oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.ManageCheckOut(oXmlScreenData, _
                                CurrentPage.UserCode, _
                                CHECK_IN_CHECK_OUT, _
                                CStr(ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE)), _
                                sErr, _
                                CBool(ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE)), _
                                 IIf(ViewState(VIEWSTATE_GUSERCODE) Is Nothing, String.Empty, CStr(ViewState(VIEWSTATE_GUSERCODE))), _
                            )

        ' erase supervisor data after attempt
        ViewState(VIEWSTATE_GUSERCODE) = String.Empty
        'commented after discussion with Rajesh
        'ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "0"
        ViewState(VIEWSTATE_GFORCED) = "0"
        ' erase supervisor data after attempt

        If Not oRetXml.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            'theres some error!
            'look for a popup error

            Select Case oRetXml.DocumentElement.SelectSingleNode("Error/Type").InnerText.Trim().ToUpper()
                Case "VALIDATIONERRORS"
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("~", "<br/>") & _
                                        "<br/><br/><B>Note:</B> Errors must be corrected before Check-Out or Check-In<br/>processing can occur.", _
                                        "Errors have been found on this Booking", "VALIDATIONERRORS")
                    Exit Select
                Case "VALIDATIONWARNINGS"
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Warning", "VALIDATIONWARNINGS")
                    Exit Select
                Case "POPUP"
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Confirmation Required", "POPUP")
                    Exit Select
                Case "POPUP1"

                    'something about "UVI" value
                    Dim oUviData As XmlDocument
                    oUviData = Aurora.Booking.Services.BookingCheckInCheckOut.GetUVIData(CurrentPage.UserCode)

                    If oUviData.DocumentElement.ChildNodes.Count > 0 And oUviData.DocumentElement.ChildNodes(0).BaseURI = "Error" Then
                        'displayMessage(1, 0, "UviValue NZ/AU ORID_AIMS Not Set")
                        Me.CurrentPage.SetWarningShortMessage("UviValue NZ/AU ORID_AIMS Not Set")
                        Exit Sub
                    End If

                    ViewState(VIEWSTATE_POPUPTYPE) = "POPUP1"
                    ViewState(VIEWSTATE_USERROLE) = oUviData.DocumentElement.InnerText
                    supervisorOverrideControl.Show(ErrorMessage:="(" & oUviData.DocumentElement.InnerText & ")<BR/>" & oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText)

                    '1)call CheckUserRole
                    '2) it sends back -1 for fail or a usercode if successful
                    '3) say "gForced=1"
                    '4)return true....

                    ' '' '' some user validation??
                    ' '' '' call made to this -->>
                    ' '' '' function GENCheckUsrRole(sMsg, sUsrCode, sRoleCode){ var rtnStr = '-1'
                    ' '' '' var dialog = '../asp/Gen_CheckUserRole.asp'; var varOption= 'dialogHeight:250px;dialogWidth=400px;resizable:=yes;status:No;'
                    ' '' '' var varArgIn= new Object; if (sMsg == '')
                    ' '' '' sMsg = 'Enter UserCode And Password'
                    ' '' '' varArgIn.str = sMsg + '^' + sUsrCode + '^' + sRoleCode
                    ' '' '' varArgIn.caseSensitive = false; window.showModalDialog(dialog, varArgIn, varOption); rtnStr = varArgIn.str
                    ' '' '' varArgIn = null
                    ' '' '' Return rtnStr
                    ' '' '' }



                    '' ''var param = "Action=POPUP^param1=GETUVI^param2=" + document.forms[0].htm_hid_userCode.value + "^param3=ORID_AIMS"
                    '' '' UviVal = getXmlDataNew("../asp/CommonFindListener.asp",param);
                    '' '' objTempXML.loadXML(UviVal)
                    '' '' if (objTempXML.documentElement.childNodes.length > 0 && objTempXML.documentElement.childNodes(0).baseName == 'Error'){
                    '' '' displayMessage(1, 0, "UviValue NZ/AU ORID_AIMS Not Set")
                    '' '' Return False
                    '' '' }
                    ' strPopupRtn = GENCheckUsrRole( "(" + objTempXML.documentElement.text + ")<BR/>" + receiveXML.selectSingleNode('//Root/Error/ErrDescription').text, 
                    ' document.forms[0].htm_hid_userCode.value, 
                    ' objTempXML.documentElement.text)
                    ' if (strPopupRtn == -1) return false
                    ' gUsrCode = strPopupRtn
                    ' gForced = '1'
                    ' Return True
                    Exit Select
                Case "POPUP2"
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Confirmation Required", "POPUP2")
                    Exit Select
                Case "POPUP3"
                    ViewState(VIEWSTATE_POPUPTYPE) = "POPUP3"
                    ViewState(VIEWSTATE_USERROLE) = oRetXml.DocumentElement.SelectSingleNode("Error/AdditionalParam").InnerText
                    supervisorOverrideControl.Show(ErrorMessage:="(" & oRetXml.DocumentElement.SelectSingleNode("Error/AdditionalParam").InnerText & ")<BR/>" & oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("~", "<BR/>"))
                    Exit Select
                Case "POPUP4"
                    ' Billing tokens missing
                    ViewState(VIEWSTATE_POPUPTYPE) = "POPUP4"
                    ViewState(VIEWSTATE_USERROLE) = oRetXml.DocumentElement.SelectSingleNode("Error/AdditionalParam").InnerText
                    supervisorOverrideControl.Show(ErrorMessage:="(" & oRetXml.DocumentElement.SelectSingleNode("Error/AdditionalParam").InnerText & ")<BR/>" & oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("~", "<BR/>"))
                    Exit Select
                Case "POPUP-SOD"
                    ' Billing tokens missing
                    'If Not ViewState(VIEWSTATE_ISSERVICEDONE) Then

                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Confirmation Required", "POPUP-SOD")
                    'End If
                    Exit Select
                Case "PROMPTCONTINUE" ''rev:mia 30-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1146
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Confirmation Required", "PROMPTCONTINUE")
                    Exit Select
                Case Else
                    CurrentPage.SetErrorMessage(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText)
            End Select

        Else
            '' Added by Nimesh on 17thJuly 2015 - to display message if vehicle is due for service
            'Dim IsServiceRequired As Boolean = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.CheckIfServiceDUE(" + txtUnitNo.Text.Trim() + ") ")
            'If Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.CheckIfServiceDUE(" + txtUnitNo.Text.Trim() + ") ") Then
            '    ShowConfirmationDialog("This Vehicle is DUE for service", "Due For Service", "POPUP-SOD")
            'End If

            ''Exit Sub
            '' End Added by Nimesh on 17thJuly 2015 - to display message if vehicle is due for service
            ' no error!!!
            ' show agreement
            ShowAgreementForPrinting()
            ' save the returned data
            ViewState(VIEWSTATE_CICOXMLDATA) = oRetXml.OuterXml
            ' refresh tab - check in now available!
            ''''''LoadPage()
            btnInitialOK_Click(New Object, New System.EventArgs)

            ' show success message
            ' force reload
            RefreshRentalGrids()
            ' force reload
            CurrentPage.SetInformationMessage(CurrentPage.GetMessage("GEN046"))


            Exit Sub
        End If


        ViewState(VIEWSTATE_SECONDTIME) = False
        ViewState(VIEWSTATE_CICOFIRSTLOAD) = False
    End Sub

    Protected Sub btnPreCheckOut_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPreCheckOut.Click
        Dim cusId As String = Aurora.Booking.Data.DataRepository.ValidateCustomerFieldsBeforeCheckOut(Me.BookingNumber, Me.RentalNo)
        Aurora.Common.Logging.LogDebug("btnCheckOut_Click", cusId)
        If (String.IsNullOrEmpty(cusId)) Then
            '' CurrentPage.SetWarningShortMessage("Please complete your address details in the Customer Maintenance screen")
            Exit Sub
        End If

        If Not IsFormDataValid("CO") Then
            Exit Sub
        End If

        ViewState(VIEWSTATE_USERROLE) = "OLMAN"
        ViewState(VIEWSTATE_POPUPTYPE) = "PRECKO"
        supervisorOverrideControl.Show(ErrorMessage:="(OLMAN)<br/>Supervisor override is needed to complete Pre-Checkout")
    End Sub

    Sub DoPreCheckOut()
        'If Not IsFormDataValid("CO") Then
        '    Exit Sub
        'End If


        'unpack screen data
        Dim oScreenDataXml As XmlDocument = New XmlDocument
        oScreenDataXml.LoadXml(CStr(ViewState(VIEWSTATE_CICOXMLDATA)))
        'update the xml

        ' some more cut/splice
        If NodeExistsAndIsNotEmpty(oScreenDataXml.DocumentElement.SelectSingleNode("Vehicle")) Then
            If Not oScreenDataXml.DocumentElement.SelectSingleNode("Vehicle").InnerText.Trim().ToUpper().Equals("CROSS HIRE VEHICLE") Then
                oScreenDataXml.DocumentElement.SelectSingleNode("Vehicle").InnerText = oScreenDataXml.DocumentElement.SelectSingleNode("Vehicle").InnerText.Split("-"c)(0)
            End If
        End If
        If NodeExistsAndIsNotEmpty(oScreenDataXml.DocumentElement.SelectSingleNode("UserLocCode")) Then
            oScreenDataXml.DocumentElement.SelectSingleNode("UserLocCode").InnerText = oScreenDataXml.DocumentElement.SelectSingleNode("UserLocCode").InnerText.Split("-"c)(0)
        End If
        If NodeExistsAndIsNotEmpty(oScreenDataXml.DocumentElement.SelectSingleNode("RntCkiLocCode")) Then
            oScreenDataXml.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText = oScreenDataXml.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0)
        End If
        If NodeExistsAndIsNotEmpty(oScreenDataXml.DocumentElement.SelectSingleNode("RntCkoLocCode")) Then
            oScreenDataXml.DocumentElement.SelectSingleNode("RntCkoLocCode").InnerText = oScreenDataXml.DocumentElement.SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0)
        End If
        If NodeExistsAndIsNotEmpty(oScreenDataXml.DocumentElement.SelectSingleNode("YrLoc")) Then
            oScreenDataXml.DocumentElement.SelectSingleNode("YrLoc").InnerText = oScreenDataXml.DocumentElement.SelectSingleNode("YrLoc").InnerText.Split("-"c)(0)
        End If
        If NodeExistsAndIsNotEmpty(oScreenDataXml.DocumentElement.SelectSingleNode("DispLoc")) Then
            oScreenDataXml.DocumentElement.SelectSingleNode("DispLoc").InnerText = oScreenDataXml.DocumentElement.SelectSingleNode("DispLoc").InnerText.Split("-"c)(0)
        End If

        With oScreenDataXml.DocumentElement
            .SelectSingleNode("RntCkoTime").InnerText = tcCheckOutTime.Text.Trim()
            .SelectSingleNode("Voucher").InnerText = Server.HtmlEncode(txtVoucherNo.Text.Trim())
            .SelectSingleNode("RntAreaUse").InnerText = Server.HtmlEncode(txtAreaOfUse.Text.Trim())
            .SelectSingleNode("OddometerOut").InnerText = txtOdometerOut.Text.Trim()
        End With




        Dim oRetXml As New XmlDocument
        oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.ManagePreCheckOut(oScreenDataXml, CurrentPage.UserCode, CHECK_IN_CHECK_OUT)

        If Not oRetXml.DocumentElement.SelectSingleNode("Message") Is Nothing Then
            'show the error
            CurrentPage.ClearMessages()
            CurrentPage.AddErrorMessage(oRetXml.DocumentElement.SelectSingleNode("Message").InnerText)
            Exit Sub
        Else
            ' no error
            'load ret xml into VIEWSTATE
            ViewState(VIEWSTATE_CICOXMLDATA) = oRetXml.OuterXml
            'show some success message
            CurrentPage.SetInformationShortMessage(CurrentPage.GetMessage("GEN046"))
            'print agreement
            ShowAgreementForPrinting()
            Exit Sub
        End If


    End Sub


    Protected Sub btnCheckIn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckIn.Click
        CheckIn(True)
    End Sub

    Public Sub CheckIn(ByVal ShouldPrint As Boolean)

        If Not IsFormDataValid("CI") Then
            Exit Sub
        End If

        'check in process
        Dim oXmlScreenData As System.Xml.XmlDocument = New XmlDocument
        oXmlScreenData.LoadXml(CStr(ViewState(VIEWSTATE_CICOXMLDATA)))

        ' unpack doc and make changes

        ' also get some stuff in hand to validate data
        Dim sUnitNo, sRegNo, sOdometerOut, sChkInUnitNo, sCkiRego, sOdometerIn, sRntCkiTime As String

        With oXmlScreenData.DocumentElement
            'updating screen data here
            .SelectSingleNode("ChkInUnitNo").InnerText = txtCIUnitNo.Text
            .SelectSingleNode("CkiRego").InnerText = txtCIRegNo.Text
            .SelectSingleNode("OddometerIn").InnerText = txtOdometerIn.Text
            ' ui fmt -> sys fmt
            .SelectSingleNode("RntCkiWhen").InnerText = DateTimeToString(dcCheckInDate.Date, SystemCulture).Split(" "c)(0) ' dcCheckInDate.Text
            .SelectSingleNode("RntCkiTime").InnerText = tcCheckInTime.Text
            .SelectSingleNode("Voucher").InnerText = Server.HtmlEncode(txtVoucherNo.Text)

            .SelectSingleNode("OddoDiff").InnerText = CInt(txtOdometerIn.Text) - CInt(txtOdometerOut.Text) 'lblOdometerDiff.Text.Split(" "c)(0)


            'gather data to validate with
            sUnitNo = .SelectSingleNode("UnitNo").InnerText
            sRegNo = .SelectSingleNode("Rego").InnerText
            sOdometerOut = .SelectSingleNode("OddometerOut").InnerText
            sChkInUnitNo = .SelectSingleNode("ChkInUnitNo").InnerText
            sCkiRego = .SelectSingleNode("CkiRego").InnerText
            sOdometerIn = .SelectSingleNode("OddometerIn").InnerText
            sRntCkiTime = .SelectSingleNode("RntCkiTime").InnerText

            'modify some stuff
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("Vehicle")) Then
                If Not .SelectSingleNode("Vehicle").InnerText.Trim().ToUpper().Equals("CROSS HIRE VEHICLE") Then
                    .SelectSingleNode("Vehicle").InnerText = .SelectSingleNode("Vehicle").InnerText.Split("-"c)(0)
                End If
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("UserLocCode")) Then
                .SelectSingleNode("UserLocCode").InnerText = .SelectSingleNode("UserLocCode").InnerText.Split("-"c)(0)
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("RntCkiLocCode")) Then
                .SelectSingleNode("RntCkiLocCode").InnerText = .SelectSingleNode("RntCkiLocCode").InnerText.Split("-"c)(0)
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("RntCkoLocCode")) Then
                .SelectSingleNode("RntCkoLocCode").InnerText = .SelectSingleNode("RntCkoLocCode").InnerText.Split("-"c)(0)
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("YrLoc")) Then
                .SelectSingleNode("YrLoc").InnerText = .SelectSingleNode("YrLoc").InnerText.Split("-"c)(0)
            End If
            If NodeExistsAndIsNotEmpty(.SelectSingleNode("DispLoc")) Then
                .SelectSingleNode("DispLoc").InnerText = .SelectSingleNode("DispLoc").InnerText.Split("-"c)(0)
            End If

        End With

        'validations here
        If Trim(sChkInUnitNo) = String.Empty Or Trim(sCkiRego) = String.Empty Or Trim(sOdometerIn) = String.Empty Or Trim(sOdometerOut) = String.Empty Or Trim(sRntCkiTime) = String.Empty Then
            'get message
            CurrentPage.SetErrorShortMessage(CurrentPage.GetMessage("GEN005"))

            Exit Sub
        End If


        If Not LCase(sUnitNo) = LCase(sChkInUnitNo) Then
            'get message
            CurrentPage.SetErrorShortMessage(CurrentPage.GetMessage("GEN155"))

            Exit Sub
        End If


        If Not LCase(sRegNo) = LCase(sCkiRego) Then
            CurrentPage.SetErrorShortMessage(CurrentPage.GetMessage("GEN156"))

            Exit Sub
        End If

        If CSng(sOdometerOut) > CSng(sOdometerIn) Then
            CurrentPage.SetErrorShortMessage(CurrentPage.GetMessage("GEN157"))

            Exit Sub
        End If

        Dim oRetXml As XmlDocument

        oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.ManageCheckIn(oXmlScreenData, _
                                                                               CurrentPage.UserCode, _
                                                                               CHECK_IN_CHECK_OUT, _
                                                                               IIf(CStr(ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE)) Is Nothing, "0", CStr(ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE))), _
                                                                               CBool(ViewState(VIEWSTATE_CHECKIN_VALIDATIONS_DONE)), _
                                                                               IIf(CStr(ViewState(VIEWSTATE_GUSERCODE)) Is Nothing, String.Empty, CStr(ViewState(VIEWSTATE_GUSERCODE))))

        ' erase supervisor data after attempt

        ViewState(VIEWSTATE_GUSERCODE) = String.Empty
        ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "0"
        ViewState(VIEWSTATE_GFORCED) = "0"
        ' erase supervisor data after attempt

        ' save the shouldprintViewState(VIEWSTATE_SHOULD_PRINT) = ShouldPrint
        ViewState(VIEWSTATE_SHOULD_PRINT) = ShouldPrint
        ' save the shouldprintViewState(VIEWSTATE_SHOULD_PRINT) = ShouldPrint


        If oRetXml.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
            ' no error

            ' save the data
            ViewState(VIEWSTATE_CICOXMLDATA) = oRetXml.OuterXml

            If ShouldPrint Then
                ShowAgreementForPrinting()
            End If

            If NodeExistsAndIsNotEmpty(oRetXml.DocumentElement.SelectSingleNode("Message/Type")) Then
                Aurora.Common.Logging.LogInformation("Nimesh 2 ", oRetXml.OuterXml)
                If UCase(oRetXml.DocumentElement.SelectSingleNode("Message/Type").InnerText) = "MESSAGE" Then
                    NullifyOldVIEWSTATEData()
                    ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(Page), UniqueID, "window.location=""Booking.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&activeTab=9&msgToShow=" & oRetXml.DocumentElement.SelectSingleNode("Message/Message").InnerText & """;", True)
                    Exit Sub
                    'ElseIf UCase(oRetXml.DocumentElement.SelectSingleNode("Message/Type").InnerText) = "POPUP3" Then
                    '    supervisorOverrideControl.Show(ErrorMessage:="(" & oRetXml.DocumentElement.SelectSingleNode("Message/AdditionalParam").InnerText & ")<BR/>" & oRetXml.DocumentElement.SelectSingleNode("Message/Message").InnerText.Replace("~", "<BR/>"))
                    '    Exit Sub
                ElseIf UCase(oRetXml.DocumentElement.SelectSingleNode("Message/Type").InnerText) = "VALIDATIONERRORS" Then

                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("~", "<br/>") & _
                                       "<br/><br/><B>Note:</B> Errors must be corrected before Check-Out or Check-In<br/>processing can occur.", _
                                       "Errors have been found on this Booking", "VALIDATIONERRORS-CI")
                    Exit Sub
                ElseIf UCase(oRetXml.DocumentElement.SelectSingleNode("Message/Type").InnerText) = "VALIDATIONWARNINGS" Then

                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Warning", "VALIDATIONWARNINGS-CI")
                    Exit Sub
                End If

            Else
                Aurora.Common.Logging.LogInformation("Nimesh 3 ", oRetXml.OuterXml)
                ' must be a successful check-in :)
                ViewState(VIEWSTATE_SHOULD_PRINT) = Nothing
                ViewState(VIEWSTATE_SECONDTIME) = False

                ' force reload
                RefreshRentalGrids()
                ' force reload

                LoadPage()

                ' show success message
                CurrentPage.SetInformationMessage(CurrentPage.GetMessage("GEN046"))


                Exit Sub
            End If

            ' end - no error

        Else
            ' some error  - see what it is
            'Aurora.Common.Logging.LogInformation("Nimesh 5 ", oRetXml.OuterXml)
            Select Case UCase(oRetXml.DocumentElement.SelectSingleNode("Error/Type").InnerText)
                Case "POPUP3"
                    ViewState(VIEWSTATE_POPUPTYPE) = "POPUP3-CI"
                    ViewState(VIEWSTATE_USERROLE) = oRetXml.DocumentElement.SelectSingleNode("Error/AdditionalParam").InnerText
                    supervisorOverrideControl.Show(ErrorMessage:="(" & oRetXml.DocumentElement.SelectSingleNode("Error/AdditionalParam").InnerText & ")<BR/>" & oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText.Replace("~", "<BR/>"))
                    'if (strPopupRtn == -1) return false
                    'gUsrCode = strPopupRtn
                    'gFirstTime = '1'
                    ''Return True
                Case "POPUP"
                    ViewState(VIEWSTATE_POPUPTYPE) = "POPUP-CI"
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Confirmation Required", "POPUP-CI")
                    Exit Select
                Case "POPUP2"
                    ViewState(VIEWSTATE_POPUPTYPE) = "POPUP2-CI"
                    ShowConfirmationDialog(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText, "Confirmation Required", "POPUP2-CI")
                    Exit Select
                Case Else
                    CurrentPage.SetErrorMessage(oRetXml.DocumentElement.SelectSingleNode("Error/Message").InnerText)
                    Aurora.Common.Logging.LogInformation("Nimesh Test", oRetXml.OuterXml)
            End Select


        End If

        ViewState(VIEWSTATE_SECONDTIME) = False



    End Sub

    Public Sub ShowAgreementForPrinting()

        ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "ShowAgreement('" & CStr(ViewState(VIEWSTATE_RENTALID)) & "$" & CStr(ViewState(VIEWSTATE_BOOKINGID)) & "$" & CurrentPage.UserCode & "');", True)

    End Sub

    Protected Sub btnCheckInNoPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheckInNoPrint.Click
        CheckIn(False)
    End Sub

    Protected Sub btnInitialOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnInitialOK.Click
        'unpack xml
        Dim oXmlData As XmlDocument = New XmlDocument
        oXmlData.LoadXml(CStr(ViewState(VIEWSTATE_CICOXMLDATA)))
        If oXmlData.DocumentElement.SelectSingleNode("What").InnerText.ToUpper() = "CHECK OUT" Then
            oXmlData.DocumentElement.SelectSingleNode("UserLocCode").InnerText = oXmlData.DocumentElement.SelectSingleNode("RntCkoLocCode").InnerText
        ElseIf oXmlData.DocumentElement.SelectSingleNode("What").InnerText.ToUpper() = "CHECK IN" Then
            ' by default the user specified loc matches the cki loc
            oXmlData.DocumentElement.SelectSingleNode("UserLocCode").InnerText = oXmlData.DocumentElement.SelectSingleNode("RntCkiLocCode").InnerText
        End If

        ViewState(VIEWSTATE_CICOXMLDATA) = oXmlData.OuterXml
        'pack
        'tblInitial.Visible = False
        'tblMain.Visible = True
        tblInitial.Style.Item("display") = "none"
        tblMain.Style.Item("display") = "block"
        ShowCheckOutItems()
        ViewState(VIEWSTATE_SECONDTIME) = True

        LoadPage()

    End Sub

    Protected Sub UnitAndRegoValidation(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtRegNo.TextChanged, txtUnitNo.TextChanged

        ' stuff added
        If sender.ID = txtUnitNo.ID Then
            ' come here from checkout unitno
            If txtRegNo.Text.Trim.Equals(String.Empty) Then
                ScriptManager.GetCurrent(Page).SetFocus(txtRegNo)
                Exit Sub
            End If
        End If

        Dim oRetXml As XmlDocument = New XmlDocument
        Dim bInvalidDataReturned As Boolean = False

        'this handles the 99999 case
        If txtUnitNo.Text.Trim() = "99999" Then
            lblVehicleDetails.Text = "Cross Hire Vehicle"
            txtOdometerOut.Text = "0"
        Else
            ' not keyword, hit the db
            oRetXml = Aurora.Booking.Services.BookingCheckInCheckOut.GetAIMSVehicleName(txtRegNo.Text.Trim(), txtUnitNo.Text.Trim(), RentalId, CurrentPage.UserCode)

            With oRetXml.DocumentElement
                If .InnerText = "" OrElse .SelectSingleNode("Vehicle").InnerText.ToUpper().Contains("INVALID") Then
                    'show error
                    'empty the rego
                    txtRegNo.Text = ""
                    lblVehicleDetails.Text = "**Invalid Vehicle"
                    txtOdometerOut.Text = "0"
                    bInvalidDataReturned = True
                    CurrentPage.SetErrorShortMessage("The Rego number does not match the AIMS record for this unit.")
                Else
                    lblVehicleDetails.Text = .SelectSingleNode("Vehicle").InnerText
                    txtOdometerOut.Text = .SelectSingleNode("OdoMeter").InnerText
                End If

            End With
        End If


        'update VIEWSTATE doc

        Dim oVIEWSTATEXml As XmlDocument = New XmlDocument
        oVIEWSTATEXml.LoadXml(CStr(ViewState(VIEWSTATE_CICOXMLDATA)))

        oVIEWSTATEXml.DocumentElement.SelectSingleNode("Vehicle").InnerText = lblVehicleDetails.Text
        oVIEWSTATEXml.DocumentElement.SelectSingleNode("OddometerOut").InnerText = txtOdometerOut.Text
        If lblVehicleDetails.Text.ToUpper().Contains("CROSS HIRE VEHICLE") Then
            oVIEWSTATEXml.DocumentElement.SelectSingleNode("CrossHr").InnerText = "1"
        Else
            oVIEWSTATEXml.DocumentElement.SelectSingleNode("CrossHr").InnerText = "0"
        End If

        oVIEWSTATEXml.DocumentElement.SelectSingleNode("UnitNo").InnerText = txtUnitNo.Text.Trim()
        oVIEWSTATEXml.DocumentElement.SelectSingleNode("Rego").InnerText = txtRegNo.Text.Trim()

        'pack into VIEWSTATE
        ViewState(VIEWSTATE_CICOXMLDATA) = oVIEWSTATEXml.OuterXml

        '<data>
        '  <Vehicle>**Invalid Vehicle</Vehicle>
        '  <OdoMeter></OdoMeter>
        '  <OdoMeterDue></OdoMeterDue>
        '  <RegoError>The Rego number does not match the AIMS record for this unit.Do you want to continue?</RegoError>
        '  <Rego></Rego>
        '  <UnAvail></UnAvail>
        '</data>
        'Dim sCtyCode As String = oVIEWSTATEXml.DocumentElement.SelectSingleNode("CtyCode").InnerText.Trim().ToUpper()
        If RentalCountryCode = "NZ" AndAlso lblVehicleDetails.Text.Trim().IndexOf("6") = 0 Then
            trOdometerOut.Visible = True
            lblOdometerOut.Text = "Hubbo Out:"
        End If

        Dim sPreviousOdometerReading As String = ""
        Dim sControlToValidate As String = ""


        If oVIEWSTATEXml.DocumentElement.SelectSingleNode("RntStaus").InnerText = "CO" Then
            'its going to be checked in!
            sPreviousOdometerReading = oVIEWSTATEXml.DocumentElement.SelectSingleNode("OddometerOut").InnerText
            sControlToValidate = txtOdometerIn.ClientID
        Else
            If txtUnitNo.Text.Trim() = "99999" Or bInvalidDataReturned Then
                sPreviousOdometerReading = 0
            Else
                'has to be a checkout!
                sPreviousOdometerReading = oRetXml.DocumentElement.SelectSingleNode("OdoMeter").InnerText
            End If
            sControlToValidate = txtOdometerOut.ClientID
        End If

        Dim oRentalData As XmlDocument = New XmlDocument
        oRentalData.LoadXml(CStr(ViewState(VIEWSTATE_RENTALINFO)))

        txtOdometerOut.Attributes.Add("onblur", _
            "document.getElementById('" & dcCheckOutDate.ClientID & "').focus();" & _
            "return validateOdometerData('" & _
                oVIEWSTATEXml.DocumentElement.SelectSingleNode("CtyCode").InnerText & "','" & _
                oVIEWSTATEXml.DocumentElement.SelectSingleNode("RntStaus").InnerText & "','" & _
                sPreviousOdometerReading & "','" & _
                oRentalData.DocumentElement.SelectSingleNode("Rental/HirePd").InnerText & "','" _
                & sControlToValidate & "');")


        If sender.ID = txtUnitNo.ID Then
            ScriptManager.GetCurrent(Page).SetFocus(txtRegNo)
        Else
            ScriptManager.GetCurrent(Page).SetFocus(txtVoucherNo)
        End If

        'If hdnWhoWasClicked.Value = txtRegNo.ClientID Then
        '    ScriptManager.GetCurrent(Page).SetFocus(txtVoucherNo)
        'Else
        '    ScriptManager.GetCurrent(Page).SetFocus(txtRegNo)
        'End If
    End Sub

    Public Function IsFormDataValid(ByVal Purpose As String) As Boolean
        Dim bFormValid As Boolean = True

        If Purpose = "CO" Then

            If txtUnitNo.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Unit Number is a required field")
                Return False
            End If

            If txtRegNo.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Rego Number is a required field")
                Return False
            End If

            If txtOdometerOut.Text.Trim().Equals(String.Empty) Then 'Or txtOdometerOut.Text = "0" Then
                Me.CurrentPage.SetWarningShortMessage("Odometer Out is a required field")
                Return False
            End If

            If Not IsNumeric(txtOdometerOut.Text) Then
                Me.CurrentPage.SetWarningShortMessage("Invalid Odometer Out reading entered")
                Return False
            End If

            If dcCheckOutDate.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Check-out date is a required field")
                Return False
            End If

            If Not dcCheckOutDate.IsValid Then
                CurrentPage.SetWarningShortMessage("Please enter valid Check-out date")
                Return False
            End If

            If tcCheckOutTime.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Check-out time is a required field")
                Return False
            End If

            If Not tcCheckOutTime.IsValid Then
                CurrentPage.SetWarningShortMessage("Please enter valid Check-out time")
                Return False
            Else
                ' theres a valid time entered
                If dcCheckOutDate.IsValid Then ' also valid date
                    If dcCheckOutDate.Date.Add(tcCheckOutTime.Time) > Now Then ' date time entered greater than NOW
                        CurrentPage.SetWarningShortMessage("Checkout Date cannot be greater than today's date")
                        Return False
                    End If
                End If
            End If
        End If

        If Purpose = "CI" Then

            If txtCIUnitNo.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Unit Number is a required field")
                Return False
            End If

            If txtCIRegNo.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Rego Number is a required field")
                Return False
            End If

            If txtOdometerIn.Text.Trim().Equals(String.Empty) Then 'Or txtOdometerIn.Text = "0" Then
                Me.CurrentPage.SetWarningShortMessage("Odometer In is a required field")
                Return False
            End If

            If IsNumeric(txtOdometerIn.Text) Then
                If CInt(txtOdometerIn.Text) < CInt(txtOdometerOut.Text) Then
                    Me.CurrentPage.SetWarningShortMessage(CurrentPage.GetMessage("GEN157"))
                    Return False
                End If
            Else
                Me.CurrentPage.SetWarningShortMessage("Invalid Odometer In reading entered")
                Return False
            End If

            If dcCheckInDate.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Check-in date is a required field")
                Return False
            End If

            If Not dcCheckInDate.IsValid Then
                CurrentPage.SetWarningShortMessage("Please enter valid Check-in date")
                Return False
            End If

            If tcCheckInTime.Text.Trim().Equals(String.Empty) Then
                Me.CurrentPage.SetWarningShortMessage("Check-in time is a required field")
                Return False
            End If

            If Not tcCheckInTime.IsValid Then
                CurrentPage.SetWarningShortMessage("Please enter valid Check-in time")
                Return False
            End If

        End If

        Return True

    End Function

    Public Sub ShowConfirmationDialog(ByVal Message As String, ByVal Title As String, ByVal ConfirmationType As String)
        Select Case ConfirmationType
            Case "VALIDATIONERRORS", "VALIDATIONERRORS-CI"
                confimationBoxControl.LeftButton_Text = "Correct Errors Now"
                confimationBoxControl.RightButton_Text = "Correct Errors Later"
                confimationBoxControl.Left_Button.CssClass = "Button_Standard"
                If confimationBoxControl.Left_Button.Style.Item("width") Is Nothing Then
                    confimationBoxControl.Left_Button.Style.Add("width", "175px")
                End If
                confimationBoxControl.Right_Button.CssClass = "Button_Standard"
                If confimationBoxControl.Right_Button.Style.Item("width") Is Nothing Then
                    confimationBoxControl.Right_Button.Style.Add("width", "175px")
                End If
                confimationBoxControl.RightButton_AutoPostBack = True

            Case "POPUP", "POPUP1", "POPUP2", "POPUP3", "POPUP4"
                confimationBoxControl.LeftButton_Text = "Continue"
                confimationBoxControl.RightButton_Text = "Cancel"
                confimationBoxControl.Left_Button.CssClass = "Button_Standard Button_OK"
                confimationBoxControl.Right_Button.CssClass = "Button_Standard Button_Cancel"
            Case "POPUP-CI", "POPUP2-CI"
                confimationBoxControl.LeftButton_Text = "Continue"
                confimationBoxControl.RightButton_Text = "Cancel"
                confimationBoxControl.Left_Button.CssClass = "Button_Standard Button_OK"
                confimationBoxControl.Right_Button.CssClass = "Button_Standard Button_Cancel"
            Case "POPUP-SOD", "PROMPTCONTINUE" ''rev:mia 30-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1146
                confimationBoxControl.LeftButton_Text = "Continue"
                confimationBoxControl.RightButton_Text = "Cancel"
                confimationBoxControl.Left_Button.CssClass = "Button_Standard Button_OK"
                confimationBoxControl.Right_Button.CssClass = "Button_Standard Button_Cancel"
            Case Else
                confimationBoxControl.LeftButton_Text = "OK"
                confimationBoxControl.RightButton_Text = "Cancel"
                confimationBoxControl.Left_Button.CssClass = "Button_Standard Button_OK"
                confimationBoxControl.Right_Button.CssClass = "Button_Standard Button_Cancel"
                If Not confimationBoxControl.Left_Button.Style.Item("width") Is Nothing Then
                    confimationBoxControl.Left_Button.Style.Remove("width")
                End If
                If Not confimationBoxControl.Right_Button.Style.Item("width") Is Nothing Then
                    confimationBoxControl.Right_Button.Style.Remove("width")
                End If
                confimationBoxControl.RightButton_AutoPostBack = False

        End Select
        ViewState(VIEWSTATE_CICOERRORTYPE) = ConfirmationType
        confimationBoxControl.Title = Title
        confimationBoxControl.Text = Message
        confimationBoxControl.Show()
    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack

        Dim sPopupType As String = ""
        sPopupType = CStr(ViewState(VIEWSTATE_CICOERRORTYPE))

        If leftButton Then
            'here is the logic

            'the various conditions are as follows
            Select Case sPopupType
                Case "POPUP"
                    ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    Exit Select
                Case "POPUP2"
                    ViewState(VIEWSTATE_GFORCED) = "1"
                    Exit Select
                Case "POPUP-CI"
                    ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    CheckIn(CBool(ViewState(VIEWSTATE_SHOULD_PRINT)))
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    Exit Sub
                Case "POPUP2-CI"
                    ViewState(VIEWSTATE_GFORCED) = "1"
                    CheckIn(CBool(ViewState(VIEWSTATE_SHOULD_PRINT)))
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    Exit Sub
                Case "VALIDATIONERRORS"
                    ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = False
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    Exit Sub
                Case "VALIDATIONWARNINGS"
                    ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = False
                    ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    ' do nothing
                Case "VALIDATIONERRORS-CI"
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    ViewState(VIEWSTATE_CHECKIN_VALIDATIONS_DONE) = False
                    Exit Sub
                Case "VALIDATIONWARNINGS-CI"
                    ' do nothing
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    ViewState(VIEWSTATE_CHECKIN_VALIDATIONS_DONE) = False
                    Exit Sub
                Case "POPUP-SOD"
                    'do nothing
                    'ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    ViewState(VIEWSTATE_ISSERVICEDONE) = True
                    ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "3"

                    CheckOut()
                    ' ViewState(VIEWSTATE_ISSERVICEDONE) = True
                    Exit Sub
                Case "PROMPTCONTINUE" ''rev:mia 30-Nov-2016 https://thlonline.atlassian.net/browse/AURORA-1146
                    ''Me.CurrentPage.AddWarningMessage("herer")		
                    'ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = True
                    'ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "3"
                    'CheckOut()
                    'ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = False

                    ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = True
                    ViewState(VIEWSTATE_ISSERVICEDONE) = True
                    ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "3"
                    CheckOut()
                    Exit Sub
                Case Else
                    'no idea what sort of popup this is
                    'give an error and die X(
                    Me.CurrentPage.AddErrorMessage("Error occured while processing confirmation")
                    Exit Sub
            End Select
            ViewState(VIEWSTATE_POPUPTYPE) = Nothing
            'jump back in!
            CheckOut()
        Else
            ' right button
            Select Case sPopupType

                Case "VALIDATIONERRORS"
                    'ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = True
                    Exit Select
                Case "VALIDATIONWARNINGS"
                    'ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    ViewState(VIEWSTATE_CHECKOUT_VALIDATIONS_DONE) = True
                    Exit Select
                Case "VALIDATIONERRORS-CI"
                    'ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                    ViewState(VIEWSTATE_CHECKIN_VALIDATIONS_DONE) = True

                    CheckIn(CBool(ViewState(VIEWSTATE_SHOULD_PRINT)))
                    Exit Sub
                Case "VALIDATIONWARNINGS-CI"
                    'ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                    ViewState(VIEWSTATE_POPUPTYPE) = Nothing

                    ViewState(VIEWSTATE_CHECKIN_VALIDATIONS_DONE) = True
                    CheckIn(CBool(ViewState(VIEWSTATE_SHOULD_PRINT)))
                    Exit Sub
            End Select
            ViewState(VIEWSTATE_POPUPTYPE) = Nothing
            'jump back in!
            CheckOut()
        End If
    End Sub

    Public Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal IsOkButton As Boolean, ByVal param As String) Handles supervisorOverrideControl.PostBack
        If IsOkButton Then
            'see if its a valid supervisor
            Dim sReturnMsg As String = ""
            sReturnMsg = Aurora.Common.Service.CheckUser(supervisorOverrideControl.Login, supervisorOverrideControl.Password, CStr(ViewState(VIEWSTATE_USERROLE)))

            If sReturnMsg = "True" Then
                Dim sPopupType As String = ""
                sPopupType = CStr(ViewState(VIEWSTATE_POPUPTYPE))
                Select Case sPopupType
                    Case "POPUP1"
                        ViewState(VIEWSTATE_GFORCED) = "1"
                        ViewState(VIEWSTATE_GUSERCODE) = supervisorOverrideControl.Login
                        Exit Select
                    Case "POPUP3"
                        'gFirstTime = '1'
                        ViewState(VIEWSTATE_GUSERCODE) = supervisorOverrideControl.Login
                        ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"
                        Exit Select

                    Case "POPUP4"
                        ViewState(VIEWSTATE_GUSERCODE) = supervisorOverrideControl.Login
                        ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "2"
                        Exit Select

                    Case "POPUP3-CI"
                        ViewState(VIEWSTATE_GUSERCODE) = supervisorOverrideControl.Login
                        ViewState(VIEWSTATE_GFIRSTTIME_SCREENVALUE) = "1"

                        ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                        CheckIn(CBool(ViewState(VIEWSTATE_SHOULD_PRINT)))
                        Exit Sub
                    Case "PRECKO"
                        ViewState(VIEWSTATE_USERROLE) = Nothing
                        ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                        DoPreCheckOut()
                        Exit Sub
                        'check Condition added by Nimesh on 25th May 2015 to check checkout always condition
                    Case "ALWAYS"
                        ViewState(VIEWSTATE_USERROLE) = Nothing
                        ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                        PerformCheckOut()
                        Exit Sub
                        'End check condition added by Nimesh on 25th May 2015 to check checkout always condition
                    Case Else
                        Me.CurrentPage.AddErrorMessage("Error occured while processing Supervisor Override")
                        ViewState(VIEWSTATE_POPUPTYPE) = Nothing

                        Exit Sub
                End Select
                ViewState(VIEWSTATE_POPUPTYPE) = Nothing
                CheckOut()
            Else
                supervisorOverrideControl.Show(ErrorMessage:=Server.HtmlEncode(sReturnMsg))
            End If
        End If

    End Sub

    Protected Sub btnMaintainSerialNumbers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMaintainSerialNumbers.Click
        ' handles the "Edit" mode show of the serial nos thingie!
        popupSerialNos.MSNMode = "All"
        popupSerialNos.CurrentRentalId = RentalId
        popupSerialNos.LoadPopUpControl()
    End Sub

    Sub ClearScreenElements()
        txtAreaOfUse.Text = ""
        txtCIRegNo.Text = ""
        txtCIUnitNo.Text = ""
        txtOdometerIn.Text = ""
        txtOdometerOut.Text = ""
        txtRegNo.Text = ""
        txtUnitNo.Text = ""
        txtVoucherNo.Text = ""
        tcCheckInTime.Text = ""
        tcCheckOutTime.Text = ""
        dcCheckInDate.Text = ""
        dcCheckOutDate.Text = ""
    End Sub

    Function NodeExistsAndIsNotEmpty(ByVal Node As XmlNode) As Boolean
        If Node Is Nothing Then
            Return False
        End If
        If Node.InnerText.Trim().Equals(String.Empty) Then
            Return False
        End If
        Return True
    End Function

    Private Function IsForcedRentalCheckSatisfied(forcedUnitNumber As String) As Boolean
        Dim retValue As Boolean = False
        Dim enteredUnitNumber As String = txtUnitNo.Text
        If forcedUnitNumber.Contains(",") Then
            For Each tempNumber As String In forcedUnitNumber.Split(",")
                If tempNumber.Contains("-") Then
                    Dim initialNumber As Integer
                    Dim finalNumber As Integer
                    Dim arr() As String = forcedUnitNumber.Split("-")
                    initialNumber = Integer.Parse(arr(0))
                    finalNumber = Integer.Parse(arr(1))

                    If NumberIsInRange(initialNumber, finalNumber, enteredUnitNumber) Then
                        retValue = True
                        Return retValue
                        Exit Function
                    End If
                Else
                    If enteredUnitNumber.Trim.Equals(tempNumber) Then
                        retValue = True
                        Return retValue
                        Exit Function
                    End If
                End If
            Next
        ElseIf forcedUnitNumber.Contains("-") Then
            Dim initialNumber As Integer
            Dim finalNumber As Integer
            Dim arr() As String = forcedUnitNumber.Split("-")
            initialNumber = Integer.Parse(arr(0))
            finalNumber = Integer.Parse(arr(1))

            If NumberIsInRange(initialNumber, finalNumber, enteredUnitNumber) Then
                retValue = True
            End If

        Else
            If IsNumeric(forcedUnitNumber) Then
                If enteredUnitNumber.Trim.Equals(forcedUnitNumber) Then
                    retValue = True
                End If
            ElseIf lblVehicleDetails.Text.Trim.Substring(0, forcedUnitNumber.Trim.Length).Equals(forcedUnitNumber.Trim) Then
                retValue = True
            End If

        End If
        Return retValue
    End Function

    Private Function NumberIsInRange(initialNumber As Integer, finalNumber As Integer, enteredUnitNumber As String) As Boolean
        If enteredUnitNumber >= initialNumber AndAlso enteredUnitNumber <= finalNumber Then
            Return True
        Else
            Return False

        End If
    End Function

    Private Sub PerformCheckOut()

        Dim xmlReturn As XmlDocument
        xmlReturn = Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber(CStr(ViewState(VIEWSTATE_RENTALID)), False)
        If xmlReturn.DocumentElement.InnerXml.Trim() = "<Products></Products>" Then
            ' not applicable
            CheckOut()
        Else
            ' applicable
            ' Session(SESSION_MAINTAINSERIALNUMBERMODE) = "Empty"
            popupSerialNos.MSNMode = "Empty"
            popupSerialNos.CurrentRentalId = RentalId
            popupSerialNos.LoadPopUpControl()
        End If


    End Sub

End Class