﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingExperienceUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingExperienceUserControl" %>
<style type="text/css">
    #headerTable
    {
        border: 1px;
    }
    div.divheader
    {
        float: left;
        width: 80px;
        background-color: #FFFFDD;
    }
    .dataTableGrid1 th
    {
        visibility:collapse;
    }
    
</style>
<script  type="text/javascript">

</script>
<asp:Panel ID="Panel1" runat="server" Width="770px">
    <div style="visibility: hidden; position: absolute; background-color: #dcdcdc; border: solid 1px black;
        padding: 2px; color: red; font-size: 8pt;" id="popupHistory">
        hello, world
    </div>
    <br />
    <asp:GridView ID="bookingExperienceGridView" runat="server" AutoGenerateColumns="False"
        CssClass="dataTableGrid1" GridLines="None">
        <AlternatingRowStyle Width="100%" />
        <HeaderStyle BackColor="White" />
        <Columns>
            <asp:TemplateField>
            <HeaderTemplate>
                     
            </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td width="40px;">
                            Status:
                        </td>
                        <td>
                            <b><%# Eval("Status")%></b>
                        </td>
                        <td width="50px;">
                             Date/Time:
                        </td>
                        <td>
                            <b><%# String.Format("{0:d/M/yyyy HH:mm}", Convert.ToDateTime(Eval("DateAdded").ToString()))%> </b>
                        </td>
                        <td width="115px;">
                            CallBack Date/Time:
                        </td>
                        <td width="120px;">
                            <b>
                                <%# CallBackDate(Eval("CallBackDate"))%>
                            </b>
                        </td>
                        <td width="40px;">
                            Agent:
                        </td>
                        <td width="30px;">
                            <b>
                                <%# Eval("Agent")%>
                            </b>
                        </td>
                        <td style="text-align:right;">
                            <a id="viewEditLink" runat="server" href="#">View/Edit</a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9" style="width: 14px;" />
                    </tr>
                    <tr id="trHeader" runat="server" />
                    <tr id="trData" runat="server">
                        <td id="tdData" runat="server" colspan="9">
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                        
                    </table>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Panel>
<br />
<div style="float: right;">
    <asp:Button ID="addBtn" runat="server" Text="Add" CssClass="Button_Standard Button_New" />
</div>
