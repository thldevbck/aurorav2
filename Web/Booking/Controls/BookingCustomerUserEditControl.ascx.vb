﻿Imports Aurora.Booking.Services.BookingCustomer
Imports System.Data
Imports Aurora.Common.Utility

Partial Class Booking_Controls_BookingCustomerUserEditControl
    Inherits BookingUserControl


    Private Const EMPTY_FIELDS As String = "FirstName and LastName are required."
    Private Const NO_RECORDS As String = "Customer information can't be found."
    Private Const SAVE_OK As String = "Customer Name and Title are updated successfully."
    Private Const SAVE_NOT_OK As String = "Failed to update Customer Name and Title."

    Public Property CustomerId As String
        Get
            Return ViewState("CustomerId_BookingCustomerUserEditControl")
        End Get
        Set(value As String)
            ViewState("CustomerId_BookingCustomerUserEditControl") = value
        End Set
    End Property

    Public Sub ShowMe()
        LoadMe()
        programmaticModalPopup_BookingCustomerUserEditControl.Show()
    End Sub

    Private Sub HideMe()
        programmaticModalPopup_BookingCustomerUserEditControl.Hide()
    End Sub

    
    Protected Sub saveButton_Click(sender As Object, e As System.EventArgs) Handles saveButtonCustomerEdit.Click
        If (ValidateFields(FirstNameTextBox.Text, LastNameTextBox.Text) = False) Then
            Me.CurrentPage.SetErrorShortMessage(EMPTY_FIELDS)
            programmaticModalPopup_BookingCustomerUserEditControl.Show()
            Exit Sub
        End If

        Dim firstName As String = FirstNameTextBox.Text.TrimEnd
        Dim lastname As String = LastNameTextBox.Text.TrimEnd
        Dim custtitle As String = ""
        Dim sb As New StringBuilder
        Dim retvalue As Boolean
        Try
            custtitle = IIf(String.IsNullOrEmpty(customertitleDropdown.SelectedItem.Text), "", customertitleDropdown.SelectedItem.Text)

            sb.AppendFormat("Updating customer firstname with {0}{1}{2}", vbCrLf, firstName, vbCrLf)
            sb.AppendFormat("Updating customer lastname with {0}{1}", lastname, vbCrLf)
            sb.AppendFormat("Updating customer title with {0}{1}", custtitle, vbCrLf)
            retvalue = UpdateCustomerTitle(EncodeText(firstName), EncodeText(lastname), EncodeText(custtitle), CustomerId)

        Catch ex As Exception
            sb.AppendFormat("Result with exception: {0}{1}", ex.Message & ", " & ex.StackTrace, vbCrLf)
            retvalue = False
        End Try


        If (retvalue = True) Then
            sb.AppendFormat("Result {0}{1}", SAVE_OK, vbCrLf)
            Me.CurrentPage.SetInformationShortMessage(SAVE_OK)
        Else
            sb.AppendFormat("Result {0}{1}", SAVE_NOT_OK, vbCrLf)
            Me.CurrentPage.SetInformationShortMessage(SAVE_NOT_OK)
            programmaticModalPopup_BookingCustomerUserEditControl.Show()
            Exit Sub
        End If


        Dim qry As String = Request.RawUrl
        sb.AppendFormat("current url is  {0}{1}", qry, vbCrLf)
        If Not Request.QueryString("activeTab") Is Nothing Then
            If (Request.QueryString("activeTab").ToString <> "8") Then
                Dim activeTab As String = "activeTab=" & Request.QueryString("activeTab")
                Dim newActiveTab As String = "activeTab=8"
                qry = qry.Replace(activeTab, newActiveTab)

            End If
        Else
            If (qry.Contains("&") = True) Then
                qry = qry & "&activeTab=8"
            Else
                qry = qry & "?activeTab=8"
            End If

        End If
        sb.AppendFormat("redirect to url: {0}{1}", qry, vbCrLf)
        Aurora.Common.Logging.LogDebug("BookingCustomerUserEditControl", sb.ToString())
        Response.Redirect(qry)
    End Sub

    Function ValidateFields(firstName As String, lastname As String) As Boolean
        If String.IsNullOrEmpty(firstName) Then
            Return False
        End If
        If String.IsNullOrEmpty(lastname) Then
            Return False
        End If

        Return True
    End Function

    Protected Sub cancelButton_Click(sender As Object, e As System.EventArgs) Handles cancelButtonCustomerEdit.Click
        HideMe()
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If (Not Page.IsPostBack) Then
            PopulateTitle()
        End If

    End Sub

    Sub LoadMe()
        If (String.IsNullOrEmpty(CustomerId)) Then Return
        Dim dsCustomer As New DataSet
        dsCustomer = GetCustomerTitleAndName(CustomerId)

        If (dsCustomer.Tables(0).Rows.Count <> -1) Then
            FirstNameTextBox.Text = DecodeText(dsCustomer.Tables(0).Rows(0)("CusFirstName").ToString)
            LastNameTextBox.Text = DecodeText(dsCustomer.Tables(0).Rows(0)("CusLastName").ToString)

            Try
                customertitleDropdown.SelectedIndex = customertitleDropdown.Items.IndexOf(customertitleDropdown.Items.FindByText(DecodeText(dsCustomer.Tables(0).Rows(0)("CusTitle").ToString)))
            Catch ex As Exception
                customertitleDropdown.SelectedIndex = 0
            End Try

        Else
        Me.CurrentPage.SetWarningShortMessage(NO_RECORDS)
        End If
    End Sub

    Sub PopulateTitle()
        customertitleDropdown.Items.Clear()
        customertitleDropdown.AppendDataBoundItems = True
        customertitleDropdown.Items.Add(New ListItem("", 0))
        customertitleDropdown.DataSource = GetCustomerTitle()
        customertitleDropdown.DataTextField = "CodDesc"
        customertitleDropdown.DataValueField = "CodCode"
        customertitleDropdown.DataBind()
    End Sub
End Class
