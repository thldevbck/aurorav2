<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingProductUserControl.ascx.vb" Inherits="Booking_Controls_BookingProductUserControl" %>
    
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx" TagName="SupervisorOverrideControl" TagPrefix="uc1" %>    
<%@ Register Src="../../UserControls/SlotAvailableControl/SlotAvailableControl.ascx"     TagName="SlotAvailableControl" TagPrefix="uc1" %>   


<asp:Panel ID="Panel1" runat="server" Width="765px" ScrollBars="auto">

    <asp:Panel ID="pnlBookedProducts" runat="server" ScrollBars="Auto" Width="100%" >
        <table style="width:100%">
            <tr>
                <td><b>Booked Products:</b></td>
                <td align="right">
                    <asp:CheckBox runat="server" ID="chkShowallrentals" AutoPostBack="true" Text="Show All Rentals"/>
                </td>
            </tr>
        </table>
      

        <table id="tblBookedProducts" runat="server" visible="false" class="dataTableGrid">
            <thead>
                <tr>
                    <th>Rnt</th>
                    <th>Product</th>
                    <th>From Date</th>
                    <th>Loc</th>
                    <th>To Date</th>
                    <th>Loc</th>
                    <th>Hire Pd</th>
                    <th>St</th>
                    <th>Typ</th>
                    <th>Crnt</th>
                    <th>Customer Charges</th>
                    <th>Agent Charges</th>
                    <th>Curr</th>
                    <th>SS</th>
                    <th>Cancel</th>
                </tr>
            </thead>
            <tr align="center" class="evenRow">
                <td></td>
                <td>
                    <uc1:PickerControl ID="PickerControlAllProductC" runat="server" PopupType="pRD4CTY" Width="50" />
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr align="center" class="oddRow">
                <td></td>
                <td>
                    <uc1:PickerControl ID="PickerControlAllProductD" runat="server" PopupType="pRD4CTY" Width="50" />
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </table>
        
        <asp:GridView 
            ID="GridView1" 
            runat="server" 
            AutoGenerateColumns="False" 
            Width="100%"
            ShowFooter="True" 
            OnRowCommand="GridView1_RowCommand" 
            CssClass="dataTableGrid" >
            <HeaderStyle />
            <RowStyle CssClass="evenRow"/>
            <AlternatingRowStyle CssClass="oddRow" />
            <FooterStyle BorderColor="LightGray" CssClass="evenRow" />
            <Columns>
                <asp:TemplateField HeaderText="Rnt">
                    <ItemTemplate>
                        <asp:HyperLink 
                            ID="HPrnt" 
                            runat="server" 
                            Text='<%# eval("RntNum") %>' 
                            NavigateUrl='<%# GetRentalUrl(Eval("RntId"))%>' />
                    </ItemTemplate>
                    <ItemStyle Width="30px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Product">
                    <ItemTemplate>
                        <asp:HyperLink 
                            ID="HPProduct" 
                            runat="server" 
                            Text='<%# eval("PrdShortName") %>' 
                            NavigateUrl='<%# GetProductUrl(Eval("RntId"),Eval("BpdId"))%>' />
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                    <FooterTemplate>
                        <table id="tblfooter" runat="server" enableviewstate="true" class="dataTableGrid">
                            <tr class="evenRow">
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProductA" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" />
                                </td>
                            </tr>
                            <tr class="oddRow">
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProductB" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct0" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct0" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>

                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct1" runat="server"  >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct1" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>

                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct2" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct2" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct3" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct3"
                                        EnableViewState="true" 
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct4" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct4" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct5" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct5" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct6" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct6" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct7" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct7" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct8" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct8" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct9" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                        ID="PickerControlAllProduct9" 
                                        EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                            <tr class="evenRow" visible="false" id="trPickerControlAllProduct10" runat="server" >
                                <td>
                                    <uc1:PickerControl 
                                       ID="PickerControlAllProduct10"
                                       EnableViewState="true"
                                        runat="server" 
                                        PopupType="pRD4CTY"
                                        Width="65" 
                                        Param2 = "<%# MyBase.CurrentPage.CountryCode %>"
                                        Param3 = "<%# MyBase.RentalId %>"
                                        Visible="false" />
                                </td>
                            </tr>
                        </table>
                        <asp:PlaceHolder runat="server" ID="phPicker" />
                    </FooterTemplate>
                    <FooterStyle Width="85" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From" >
                    <ItemTemplate>
                        <uc1:DateControl 
                            ID="frDateControl" 
                            runat="server" 
                            Date='<%# GetDate(eval("FrDate")) %>'
                            Visible='<%# ParseEvalItemToBoolean(eval("DispCkoD")) %>' ReadOnly="true" />                          
                        <%--<asp:TextBox 
                            runat="server" 
                            ID="txtLocCode" 
                            Visible='<%# ParseEvalItemToBoolean(eval("DispCkoL")) %>'
                            Text='<%# eval("LocCode") %>' 
                            Width="40" />--%>
                            <uc1:PickerControl ID="PickerControlCKO" 
                                runat="server" 
                                PopupType="location" 
                                Visible='<%# ParseEvalItemToBoolean(eval("DispCkiL")) %>'
                                Text='<%# eval("LocCode") %>'
                                Width="75" ReadOnly="true"/>
                    </ItemTemplate>
                    <ItemStyle Width="40px"/>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To">
                    <ItemTemplate>
                        <uc1:DateControl 
                            ID="toDateControl" 
                            runat="server" 
                            Date='<%# GetDate(eval("ToDt")) %>'
                            Visible='<%# ParseEvalItemToBoolean(eval("DispCkiD")) %>' ReadOnly="true"/>   
                        <%--<asp:TextBox 
                            runat="server" 
                            ID="txtToLoc" 
                            Visible='<%# ParseEvalItemToBoolean(eval("DispCkiL")) %>'
                            Text='<%# eval("CKLocCode") %>' 
                            Width="40" />--%>
                            <uc1:PickerControl ID="PickerControlCKI" 
                                runat="server" 
                                PopupType="location" 
                                Text='<%# eval("CKLocCode") %>' 
                                Visible='<%# ParseEvalItemToBoolean(eval("DispCkiL")) %>'
                                Width="75" ReadOnly="true"/>
                    </ItemTemplate>
                    <ItemStyle Width="40px"  />
                </asp:TemplateField>
                <asp:BoundField HeaderText="Hire Pd" DataField="HrPrd">
                    <ItemStyle Width="50px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="St" DataField="St">
                    <ItemStyle Width="25px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="Typ" DataField="Typ">
                    <ItemStyle Width="30px"  />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Crnt" ConvertEmptyStringToNull="False">
                    <ItemTemplate>
                        <asp:CheckBox 
                            runat="server" 
                            ID="chkCurr" 
                            AutoPostBack="true" 
                            Checked='<%# (ParseEvalItemToBoolean(eval("Curr"))) %>'
                            Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Customer &lt;br/&gt;Charges" ItemStyle-HorizontalAlign="Right" >
                    <ItemTemplate>
                        <asp:LinkButton 
                            Text='<%# eval("CusChg") %>' 
                            ID="lnkcusagentcharge" 
                            runat="server"
                            CommandArgument='<%# eval("BpdId") %>' 
                            CommandName="ShowCharge" />
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Agent &lt;br/&gt;Charges">
                    <ItemTemplate>
                        <asp:LinkButton 
                            Text='<%# eval("AgnChg") %>' 
                            ID="lnkagentcharge" 
                            runat="server" 
                            CommandArgument='<%# eval("BpdId") %>'
                            CommandName="ShowCharge" />
                    </ItemTemplate>
                    <ItemStyle Width="50px" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="Curr" DataField="Currency">
                    <ItemStyle Width="30px" />
                </asp:BoundField>
                <asp:BoundField HeaderText="SS" DataField="SSPost">
                    <ItemStyle Width="30px"  />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Cancel">
                    <ItemTemplate>
                        <asp:CheckBox 
                            runat="server" 
                            ID="chkCancel" 
                            AutoPostBack="true" 
                            Visible='<%# ParseEvalItemToBoolean(eval("DispCancel")) %>'
                            OnCheckedChanged="CheckBox1_CheckedChanged" />
                    </ItemTemplate>
                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    
    <br />

    <asp:Panel ID="pnlBookedProductsButton" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button 
                        ID="ButSave" 
                        runat="server" 
                        Text="Save" 
                        CssClass="Button_Standard Button_Save" />
                    <asp:Button 
                        ID="ButNew" 
                        runat="server" 
                        Text="New" 
                        CssClass="Button_Standard Button_New" />
                
                    <asp:Button 
                        ID="ButSelectProduct" 
                        runat="server" Text="Select Product" 
                        CssClass="Button_Standard" Visible="false" />
                
                    <asp:Button 
                        ID="ButSearch" 
                        runat="server" 
                        Text="Search"  
                        CssClass="Button_Standard Button_Search"  />
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="pnlChargeSummary" runat="server" Width="100%" ScrollBars="auto">
    
        <table width="100%">
            <tr>
                <td>
                    <asp:Literal ID="litChargeSummary" runat="server" EnableViewState="true" />
                </td>
            </tr>
        </table>
        
        <br />
    
        <asp:GridView 
            runat="server" 
            ID="gridChargeSummary" 
            Width="100%" 
            AutoGenerateColumns="False"
            ShowFooter="True" 
            CssClass="dataTableGrid">
            <HeaderStyle HorizontalAlign="center" />
            <RowStyle CssClass="evenRow" />
            <AlternatingRowStyle CssClass="oddRow" />
            <FooterStyle BorderColor="LightGray" CssClass="oddRow" />
            <Columns>
                <asp:TemplateField HeaderText="From">
                    <ItemTemplate>
                        <%#GetDateFormat(Eval("FrDt"))%>
                        <%# Eval("FrTm") %>
                    </ItemTemplate>
                    <ItemStyle Width="15%" VerticalAlign="Bottom" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To">
                    <ItemTemplate>
                        <%#GetDateFormat(Eval("ToDt"))%>
                        <%# Eval("ToTm") %>
                    </ItemTemplate>
                    <ItemStyle Width="15%" VerticalAlign="Bottom" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <b>Totals</b>
                    </FooterTemplate>
                    <FooterStyle ForeColor="Black" HorizontalAlign="Left" Width="40px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Booked">
                    <ItemTemplate>
                        <%#GetDateFormat(Eval("BooDt"))%>
                    </ItemTemplate>
                    <ItemStyle Width="9%" VerticalAlign="Bottom" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <b>GST</b>
                    </FooterTemplate>
                    <FooterStyle ForeColor="Black" HorizontalAlign="Left" Width="40px" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rate">
                    <ItemTemplate>
                        <%# Eval("Rate") %> &nbsp;
                    </ItemTemplate>
                    <ItemStyle Width="9%" VerticalAlign="Bottom" HorizontalAlign=right  />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <asp:Literal ID="litGSTprice" runat="server" /> &nbsp;
                    </FooterTemplate>
                    <FooterStyle Width="9%" ForeColor="Black" HorizontalAlign="right" />
                </asp:TemplateField>
                <asp:BoundField HeaderText="Qty/UOM" DataField="QtUOM">
                    <ItemStyle Width="8%" VerticalAlign="Bottom" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Curr">
                    <ItemTemplate>
                        <%# Eval("Curr") %>
                    </ItemTemplate>
                    <ItemStyle Width="7%" VerticalAlign="Bottom" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <asp:Literal ID="litCurr" runat="server" />
                    </FooterTemplate>
                    <FooterStyle ForeColor="Black" HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Discounts">
                    <ItemTemplate>
                        <%# Eval("Dis") %>
                    </ItemTemplate>
                    <ItemStyle Width="9%" VerticalAlign="Bottom" HorizontalAlign=right  />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <asp:Literal ID="litDis" runat="server" />
                    </FooterTemplate>
                    <FooterStyle Width="9%" ForeColor="Black" HorizontalAlign="right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Agent Discounts">
                    <ItemTemplate>
                        <%# Eval("AgnDis") %>
                    </ItemTemplate>
                    <ItemStyle Width="14%" VerticalAlign="Bottom" HorizontalAlign=right  />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <asp:Literal ID="litAgnDis" runat="server" />
                    </FooterTemplate>
                    <FooterStyle Width="14%" ForeColor="Black" HorizontalAlign="right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Owing">
                    <ItemTemplate>
                        <asp:LinkButton Text='<%# eval("TotOw") %>' ID="lnkTotalOwing" runat="server" CommandArgument='<%# eval("id") %>'
                            CommandName="ShowChargeDetails"   />
                       
                    </ItemTemplate>
                    <ItemStyle Width="10%" VerticalAlign="Bottom" HorizontalAlign=right />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                    <FooterTemplate>
                        <asp:Literal ID="litTotOw" runat="server" />
                         <asp:Literal ID="litbpdid" runat="server"  Visible="false"/>
                    </FooterTemplate>
                    <FooterStyle Width="10%" ForeColor="Black" HorizontalAlign="right" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FOC?">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkFOC" AutoPostBack="true" Checked='<%# ParseEvalItemToBoolean(eval("FOC")) %>'
                            Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle Width="4%" VerticalAlign="Bottom" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>

    <asp:Panel ID="pnlChargeSummaryButton" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td align="right">
                    <br />
                    <asp:Button ID="btnback" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <asp:Panel ID="pnlShowChargeDetails" runat="server" Width="100%" ScrollBars="Auto" Height="100%">
        
        <table width="100%">
            <tr>
                <td>
                    <asp:Literal ID="litShowChargeDetails" runat="server" EnableViewState="true"  />
                </td>
            </tr>
        </table>
        
        <asp:GridView 
            runat="server" 
            ID="GridShowChargeDetails" 
            Width="100%" 
            AutoGenerateColumns="False"
            CssClass="dataTableGrid">
            <HeaderStyle HorizontalAlign="center" />
            <RowStyle CssClass="evenRow" />
            <AlternatingRowStyle CssClass="oddRow" />
            <FooterStyle BorderColor="LightGray" CssClass="evenRow" />
            <Columns>
                <asp:TemplateField HeaderText="From">
                    <ItemTemplate>
                        <%#GetDateFormat(Eval("FrDt"))%>
                        
                        <%# Eval("FrTm") %>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="left"  Width="15%"/>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To">
                    <ItemTemplate>
                        <%#GetDateFormat(Eval("ToDt"))%>
                        
                        <%# Eval("ToTm") %>
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="left"  Width="15%"/>
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Booked" >
                    <ItemTemplate>
                        <%#GetDateFormat(Eval("BooDt"))%>
                    </ItemTemplate>
                    <FooterStyle ForeColor="White" HorizontalAlign="Center" />
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="left" Width="9%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Rate">
                    <ItemTemplate>
                        <%# Eval("Rate") %> &nbsp;
                    </ItemTemplate>
                    <FooterStyle ForeColor="White" HorizontalAlign="Center" />
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="right" Width="9%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:BoundField HeaderText="Qty/UOM" DataField="QtUOM">
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="left" Width="8%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="Curr">
                    <ItemTemplate>
                        <%# Eval("Curr") %>
                    </ItemTemplate>
                    <FooterStyle ForeColor="White" HorizontalAlign="Center" />
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="left" Width="7%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Discounts">
                    <ItemTemplate>
                        <%# Eval("Dis") %>
                    </ItemTemplate>
                    <FooterStyle ForeColor="White" HorizontalAlign="Center" />
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="right" Width="9%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Agent Discounts">
                    <ItemTemplate>
                        <%# Eval("AgnDis") %>
                    </ItemTemplate>
                    <FooterStyle ForeColor="White" HorizontalAlign="Center" />
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="right" Width="14%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Total Owing">
                    <ItemTemplate>
                        <%# Eval("TotOw") %>
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Literal ID="litTotOw" runat="server" />
                    </FooterTemplate>
                    <FooterStyle ForeColor="White" HorizontalAlign="Center" />
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="right" Width="10%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FOC?">
                    <ItemTemplate>
                        <asp:CheckBox runat="server" ID="chkFOC" AutoPostBack="true" Checked='<%# ParseEvalItemToBoolean(eval("FOC")) %>'
                            Enabled="false" />
                    </ItemTemplate>
                    <ItemStyle VerticalAlign="Bottom" HorizontalAlign="center" Width="4%" />
                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                </asp:TemplateField>
                
            </Columns>
            
        </asp:GridView>

        <br />

        <asp:Repeater ID="repShowChargeDetails" runat="server">
            <HeaderTemplate>
                <table width="100%" class="dataTableGrid">
                    <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>&nbsp;</th>
                            <th>Rate</th>
                            <th>Qty/UOM</th>
                            <th>Curr</th>
                            <th>Discounts</th>
                            <th>Agent Discounts</th>
                            <th>Total Owing</th>
                            <th>FOC?</th>
                        </tr>
                    </thead>
             
            </HeaderTemplate>
            <ItemTemplate>
                    <tr class="evenRow">
                        <td rowspan="2" valign="middle" align="left" width="15%">
                            <b>
                                <%# eval("ChgDetDesc") %>
                            </b>
                        </td>
                        <td width="24%"  align="left">
                            <a target="_blank" runat="server" id="lnkTotalowing">
                                <%# eval("ChgDetCalcRateLabel") %>
                            </a>
                        </td>
                        <td width="9%" align="left">
                            <asp:TextBox runat="server" ID="txtChgDetRate" Text="" Width="60" MaxLength="12" />
                        </td>
                        <td width="8%" align="left">
                            <asp:TextBox runat="server" ID="txtChgDetQty" Text="" Width="46" MaxLength="12"/>
                        </td>
                        <td width="7%" align="left">
                         <%# eval("ChgDetCurr") %>
                        </td>
                        <td  align="right" width="9%" >
                         <%# eval("ChgDetCusDisAmt") %>
                        </td>
                        <td  align="right" width="14%" >
                           <%# eval("ChgDetAgnDisAmt") %>
                        </td>
                        <td  align="right" width="10%" >
                           <%# eval("ChgDetTotOwing") %>
                        </td>
                        <td  align="center" width="4%" >
                          <asp:CheckBox runat="server" ID="chkFOCSub" Enabled="false" Checked='<%# ParseEvalItemToBoolean(eval("ChgDetIsFoc")) %>' />
                        </td>
                    </tr>
                    <tr class="evenRow">
                        <td align="left">
                            Overrides:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="TxtChgDetRateOverRide" Text="" Width="60" MaxLength="12"/>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRate" runat="server" AutoPostBack="true" Width="52">
                                <asp:ListItem Value='Rate'>Rate</asp:ListItem>
                                <asp:ListItem Value='%'>%</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td >
                            Agt.Disc<br />
                            <b><%# eval("ChgDetAgnDiscPerc") %></b>
                            %
                        </td>
                        <td>
                            GST <br />
                            Content
                            <br />
                            <b><%# eval("ChgDetGstAmt") %></b>
                        </td>
                        <td align="right">
                            <asp:TextBox runat="server" ID="TxtChgDetAgnDisOverRide" Text="" Width="50" MaxLength="12"/>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FLT_TxtChgDetAgnDisOverRide" runat="server"
                                FilterType="Numbers,Custom" TargetControlID="TxtChgDetAgnDisOverRide" ValidChars="%." />
                        </td>
                        <td align="right">
                            <asp:TextBox runat="server" ID="txtChgDetTotOverRide" Text="" Width="50" MaxLength="12"/>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FLT_txtChgDetTotOverRide" runat="server"
                                FilterType="Numbers,Custom" TargetControlID="txtChgDetTotOverRide" ValidChars="." />
                        </td>
                        <td>
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
        </asp:Repeater>
                  
    </asp:Panel>
    
    <br />
    
    <asp:Panel ID="pnlShowChargeDetailsButtons" runat="server" Width="100%">
        <table width="100%">
            <tr >
                <td align="right"> 
                    <asp:Button ID="btnShowChargeDetails_Save" runat="server" Text="Save" 
                        CssClass="Button_Standard Button_Save" />
                    <asp:Button ID="btnShowChargeDetails_Cancel" runat="server" Text="Reset" 
                        CssClass="Button_Standard Button_Reset" />
                    <asp:Button ID="btnShowChargeDetails_back" runat="server" Text="Back" 
                        CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
        
        <ajaxToolkit:ConfirmButtonExtender 
            ID="CBE_ShowChargeDetails" 
            TargetControlID="btnShowChargeDetails_Cancel"
            ConfirmText="Do you want to reset the charge to the system calculation?" 
            runat="server" />
    </asp:Panel>
    
    <asp:HiddenField ID="HidBooid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidVHRid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidRentalID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidValidValue" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidBpdSearchXml" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidBpdIds" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidChargeSummary" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidShowChargeDetails" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidShowChargeDetailsArgument" runat="server" EnableViewState="true" />
    <asp:Literal ID="litXMLBookedProductList" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="LitHiddenBookingID" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="LitHiddenRentalID" runat="server" EnableViewState="true" Visible="false" />

    <asp:UpdatePanel ID="panelextra" runat="server">
        <ContentTemplate>
            <uc1:SupervisorOverrideControl ID="SupervisorOverrideControl1" runat="server"  ContinueButtonVisible="false"  />    

            <%--rev:mia 30jan2015-addition of supervisor override password..--%>
            <uc1:SlotAvailableControl runat="server" ID="SlotAvailableControl" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Panel>
