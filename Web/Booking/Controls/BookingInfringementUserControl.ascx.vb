
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_BookingInfringementUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False
    'Delegate Sub InfringementPopupPostBackObject(ByVal requireRefresh As Boolean)

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim delPopupPostBack As New InfringementPopupPostBackObject(AddressOf Me.InfringementManagementPopupPostBack)
            'Me.infringementManagementPopupUserControl1.PostBack = delPopupPostBack

            popupUserControlLoad()

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking infringement tab.")
        End Try
    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                infringementGridViewDatabind()
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking infringement tab.")
        End Try
    End Sub

    Protected Sub showAllRentalCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles showAllRentalCheckBox.CheckedChanged
        infringementGridViewDatabind()
    End Sub

    Private Sub infringementGridViewDatabind()

        'BookingId = "3135377"
        'RentalId = "3135377-1"

        Dim showAllRental As Boolean
        showAllRental = showAllRentalCheckBox.Checked

        Dim dt As DataTable
        dt = BookingInfringement.GetInfringements(showAllRental, BookingId, RentalId)

        infringementGridView.DataSource = dt
        infringementGridView.DataBind()

    End Sub

    'Protected Function GetInfringementUrl(ByVal icdId As Object) As String
    '    Dim icdIdString As String
    '    icdIdString = Convert.ToString(icdId)
    '    Return "./InfringementManagement.aspx?icdId=" & icdIdString & "&bookingId=" & BookingId
    'End Function

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click
        '   Response.Redirect("./InfringementManagement.aspx?icdId=&bookingId=" & BookingId & "&rentalId=" & RentalId)
        popupUserControlLoad()
        infringementManagementPopupUserControl1.loadInsertTemplate()

    End Sub

    Private Sub popupUserControlLoad()
        infringementManagementPopupUserControl1.BookingId = BookingId
        infringementManagementPopupUserControl1.RentalId = RentalId

    End Sub

    Protected Sub infringementGridView_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles infringementGridView.RowCommand

        If e.CommandName.ToString() = "update" Then
            popupUserControlLoad()
            Dim icdId As String
            icdId = e.CommandArgument.ToString()
            infringementManagementPopupUserControl1.loadEditTemplate(icdId)
        End If

    End Sub

    Protected Sub infringementGridView_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs) Handles infringementGridView.RowUpdating

    End Sub

    Protected Sub infringementManagementPopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles infringementManagementPopupUserControl1.PostBack
        InfringementManagementPopupPostBack(param)
    End Sub

    Public Sub InfringementManagementPopupPostBack(ByVal requireRefresh As Boolean)
        If requireRefresh Then
            infringementGridViewDatabind()
        End If
    End Sub

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)

        If String.IsNullOrEmpty(dString) Then
            Return ""
        ElseIf dString.IndexOf(" ") > 0 Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return Utility.DateDBUIConvert(dString)
        End If
    End Function

End Class