<!--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookingHistoryUserControl.ascx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	15.4.8 / Base version
''                      26.9.8 / Shoel - fixed the currency code in header row :)
''
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
-->
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingHistoryUserControl.ascx.vb" Inherits="Booking_Controls_BookingHistoryUserControl" %>

<asp:Panel ID="Panel1" runat="server" Width="765px" ScrollBars="horizontal">
    <div style="visibility: hidden; position: absolute; background-color: #dcdcdc; border: solid 1px black; padding: 2px; color:red; font-size:8pt;" id="popupHistory">hello, world</div>

    <asp:GridView 
        ID="bookingHistoryGridView" 
        runat="server" 
        AutoGenerateColumns="False"  
        CssClass="dataTableGrid"
        GridLines="None">
        <AlternatingRowStyle CssClass="oddRow" Width="100%" />
        <RowStyle CssClass="evenRow" />
        <Columns>
            <asp:TemplateField HeaderText="Id" Visible="False">
                <ItemTemplate>
                    <asp:Label ID="rnhIdLabel" runat="server" Text='<%# Bind("RnhId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ChgType" HeaderText="Events" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="UsrName" HeaderText="User" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="LocCode" HeaderText="Loc" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Dt" HeaderText="Date/Time" ReadOnly="True"  ItemStyle-Wrap="False" />
            <asp:BoundField DataField="prdname" HeaderText="Vehicle" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="UntNum" HeaderText="Unit No" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="RntSt" HeaderText="St" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Cko" HeaderText="Checked Out" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Cki" HeaderText="Checked In" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="AgnName" HeaderText="Agent" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="PkgCode" HeaderText="Package" ReadOnly="True" />
            <asp:BoundField DataField="HirePd" HeaderText="Hire Period" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="VhRate" HeaderText="Vehicle Rate" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="GrossAmt" HeaderText="Gross" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="GrossOver" HeaderText="Total Override" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="AgnDesc" HeaderText="Agent Disc" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="ToTow" HeaderText="Total Owing" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="GST" HeaderText="GST" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="TotPaid" HeaderText="Total Paid" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Sec" HeaderText="Security Paid" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Exist" HeaderText="Exist" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Bph_ChEv" HeaderText="Bph_ChEv" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Bph_BphPrd" HeaderText="Bph_BphPrd" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Bph_Qty" HeaderText="Bph_Qty" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Bph_UOM" HeaderText="Bph_UOM" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Bph_Rate" HeaderText="Bph_Rate" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Curr" HeaderText="Curr" ReadOnly="True" ItemStyle-Wrap="False" />
        </Columns>
    </asp:GridView>

</asp:Panel>
