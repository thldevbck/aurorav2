'' Change Log!
''  1.07.2008 - Shoel - Added the date utility thingie + removed commented/unused code bits
''  2.07.2008 - Shoel - got the date thingie right this time!
''  24.09.2008 -Shoel - changes for incident mgt loading via exchange tab

'' list of sps
'' IncdntGetForTab
'' incdnt_GetIncident
'' sp_get_codecodetype
'' incdnt_GetCauseType
'' incdnt_GetSubCauses
'' incdnt_UpdateIncident
'' incdnt_UpdateNote
'' incdnt_UpdateIncidentSubCause
'' incdnt_DeleteIncidentSubCause
'' incdnt_getIncidentType
'' incdnt_UpdateAIMSReference


Imports System.Xml
Imports Aurora.Common.Utility
Imports Aurora.Common
Imports System.Data

Partial Class Booking_Controls_BookingIncidentsUserControl
    Inherits BookingUserControl

public  isFirstLoad as boolean

    Const SESSION_RENTALID As String = "Session_Incidents_RentalId"
    Const SESSION_BOOKINGID As String = "Session_Incidents_BookingId"
    Const SESSION_BACKPATH As String = "Session_Incidents_BackPath"

    Public Sub LoadPage()
        Dim oParentPage As AuroraPage
        oParentPage = Page
        'RentalId = "3239315-1"
        'BookingId = "3239315"
        Session(SESSION_RENTALID) = RentalId
        Session(SESSION_BOOKINGID) = BookingId

        'get the Rental's booking status
        Dim xmlReturned As XmlDocument
        xmlReturned = Aurora.Booking.Services.BookingExtension.GetRentalForExtension(BookingId, RentalId)

        If Not xmlReturned.DocumentElement.SelectSingleNode("ThisRental/Rental/RntSt") Is Nothing Then
            If Not "CI,CO".Contains(Trim(xmlReturned.DocumentElement.SelectSingleNode("ThisRental/Rental/RntSt").InnerText)) Then
                ' not checked out/in
                ' so cannot do anything

                btnAddIncidents.Enabled = False
                CurrentPage.SetInformationShortMessage("No vehicle data for this rental")
            Else
                'additionally check subsequent page rental list FMOD list

                xmlReturned = Aurora.Booking.Services.BookingIncidents.GetIncident(BookingId, RentalId, "", CurrentPage.UserCode)
                If xmlReturned.DocumentElement.SelectSingleNode("VechileData").ChildNodes.Count = 0 Then
                    'nothing here
                    btnAddIncidents.Enabled = False
                    CurrentPage.SetInformationShortMessage("No vehicle data for this rental")
                Else
                    btnAddIncidents.Attributes.Add("onclick", "ShowIncidentDetails('" & RentalId & "','" & BookingId & "','');")
                    btnAddIncidents.Enabled = True
                End If

            End If
        End If
        LoadIncidentsGrid(False)
        Session(SESSION_BACKPATH) = Server.HtmlEncode(Page.Request.RawUrl.Substring(Page.Request.RawUrl.LastIndexOf("/") + 1))

    End Sub


    Protected Sub gwIncidents_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwIncidents.RowCreated
        e.Row.Cells(0).Visible = False
    End Sub

    Protected Sub gwIncidents_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwIncidents.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            CType(e.Row.Cells(1).FindControl("lblLink"), Label).Attributes.Add("onclick", "ShowIncidentDetails('" & Session(SESSION_RENTALID) & "','" & Session(SESSION_BOOKINGID) & "','" & e.Row.Cells(0).Text & "');")
            CType(e.Row.Cells(1).FindControl("lblLink"), Label).Attributes.Add("class", "HyperLink_Label_MouseOut")
            CType(e.Row.Cells(1).FindControl("lblLink"), Label).Attributes.Add("onmouseover", "this.className='HyperLink_Label_MouseOver';")
            CType(e.Row.Cells(1).FindControl("lblLink"), Label).Attributes.Add("onmouseout", "this.className='HyperLink_Label_MouseOut';")

            ' the date thingie for cells #1 & #4
            Dim dtTemp As Date
            If Not CType(e.Row.Cells(1).FindControl("lblLink"), Label).Text.Trim().Equals(String.Empty) And Not CType(e.Row.Cells(1).FindControl("lblLink"), Label).Text.Trim().Equals("&nbsp;") Then
                dtTemp = ParseDateTime(CType(e.Row.Cells(1).FindControl("lblLink"), Label).Text.Split("-"c)(1).Trim(), SystemCulture)
                CType(e.Row.Cells(1).FindControl("lblLink"), Label).Text = CType(e.Row.Cells(1).FindControl("lblLink"), Label).Text.Split("-"c)(0).Trim() & " - " & dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) 'dtTemp.ToShortDateString()
            End If

            'If Not e.Row.Cells(4).Text.Trim().Equals(String.Empty) And Not e.Row.Cells(4).Text.Trim().Equals("&nbsp;") Then
            '    dtTemp = ParseDateTime(e.Row.Cells(4).Text, SystemCulture)
            '    e.Row.Cells(4).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) 'dtTemp.ToShortDateString()
            'End If
            ' the date thingie for cells #1 & #4
        End If
    End Sub

    Protected Sub chkShowAllRentals_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If chkShowAllRentals.Checked Then
            'show all rentals
            LoadIncidentsGrid(True)
        Else
            LoadIncidentsGrid(False)
        End If
    End Sub


    Sub LoadIncidentsGrid(Optional ByVal ShowAllRentals As Boolean = False)
        Dim dtIncidents As DataTable
        If ShowAllRentals Then
            'all being shown
            dtIncidents = Aurora.Booking.Services.BookingIncidents.GetIncidents("", Session(SESSION_BOOKINGID))
        Else
            'only selected rental
            dtIncidents = Aurora.Booking.Services.BookingIncidents.GetIncidents(Session(SESSION_RENTALID), Session(SESSION_BOOKINGID))
        End If

        If dtIncidents.Rows.Count = 0 Then
            'nothing inside
            'show that GEN049 error
            Console.Write("")
        Else
            gwIncidents.DataSource = dtIncidents
            gwIncidents.DataBind()
        End If

    End Sub

    

End Class
