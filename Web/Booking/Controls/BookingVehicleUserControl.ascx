<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingVehicleUserControl.ascx.vb" Inherits="Booking_Controls_BookingVehicleUserControl" %>

<table width="100%">
    <tr>
        <td><b>Vehicle Summary:</b></td>
        <td align="right">
            <asp:CheckBox ID="showAllRentalCheckBox" runat="server" Text="Show All Rentals" AutoPostBack="true" />
        </td>
    </tr>
</table>

<table width="100%">
    <tr>
        <td colspan="2">
            <asp:GridView 
                ID="vehicleGridView" 
                runat="server" 
                Width="100%" 
                AutoGenerateColumns="False"
                CssClass="dataTableGrid"
                GridLines="none">
                <AlternatingRowStyle CssClass="oddRow" />
                <RowStyle CssClass="evenRow" />
                <Columns>
                    <asp:BoundField DataField="RntNum" HeaderText="Rental" ReadOnly="True" />
                    <asp:TemplateField HeaderText="Check-Out" >
                        <ItemTemplate>
                            <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("CkoDate")) %>' />
                            &nbsp;&nbsp;
                            <asp:Label ID="cKOLocLabel" runat="server" Text='<%# eval("CkoLoc") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Check-In" >
                        <ItemTemplate>
                            <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("CkiDate")) %>' />
                            &nbsp;&nbsp;
                            <asp:Label ID="cKILocLabel" runat="server" Text='<%# eval("CkiLoc") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                    <asp:BoundField DataField="UnitNum" HeaderText="UnitNo" ReadOnly="True" />
                    <asp:BoundField DataField="Rego" HeaderText="Rego" ReadOnly="True" />
                    <asp:BoundField DataField="OdoOut" HeaderText="Odometer Out" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Width="80" HeaderStyle-Width="100" />
                    <asp:BoundField DataField="OdoIn" HeaderText="Odometer In" ReadOnly="True" ItemStyle-HorizontalAlign="right" ItemStyle-Width="80" HeaderStyle-Width="100" />
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>

