<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingExtensionUserControl.ascx.vb" Inherits="Booking_Controls_BookingExtensionUserControl" %>

<%@ Register Src="../../UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc1" %>

<%@ Register Src="../../UserControls/SlotAvailableControl/SlotAvailableControl.ascx"
    TagName="SlotAvailableControl" TagPrefix="uc1" %>

    
<table cellspacing="0" cellpadding="0" width="100%">
    <tr>
        <td width="50%">
            <b>Customer Check-out Extension: </b><br /><br />
            
            <table width="100%" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="150px">Current Date/Time:</td>
                    <td>
                        <uc1:DateControl ID="checkOutDateTextBox" runat="server" ReadOnly="true" />
                        <uc1:TimeControl ID="checkOutTimeTextBox" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>New Date/Time:</td>
                    <td>
                        <uc1:DateControl ID="dtcCheckOutDate" runat="server" AutoPostBack="true" />
                        <uc1:TimeControl ID="tcCheckOutTime" runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>Extension Period:</td>
                    <td>
                        <asp:TextBox ID="txtCheckOutExtensionPeriod" runat="server" ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </td>
        <td width="50%">
            <b>Customer Check-In Extension:</b><br /><br />
            
            <table width="100%" cellpadding="2" cellspacing="0">
                <tr>
                    <td width="150px">Current Date/Time:</td>
                    <td>
                        <uc1:DateControl ID="checkInDateTextBox" runat="server" ReadOnly="true" />
                        <uc1:TimeControl ID="checkInTimeTextBox" runat="server" ReadOnly="true" />
                    </td>
                </tr>
                <tr>
                    <td>New Date/Time:</td>
                    <td>
                        <uc1:DateControl ID="dtcCheckInDate" runat="server" AutoPostBack="true" />
                        <uc1:TimeControl ID="tcCheckInTime" runat="server" AutoPostBack="true" />
                    </td>
                </tr>
                <tr>
                    <td>Extension Period:</td>
                    <td>
                        <asp:TextBox ID="txtCheckInExtensionPeriod" runat="server" ReadOnly="true" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <br />
        </td>
    </tr>
    <tr>
        <td colspan="2" align="right">
            <asp:Button 
                ID="btnSave" 
                Text="Save" 
                runat="server" 
                CssClass="Button_Standard Button_Save" />
                
            <asp:Button 
                ID="btnCancel" 
                Text="Reset" 
                runat="server" 
                CssClass="Button_Standard Button_Reset" />
        </td>
    </tr>
</table>
<uc1:SupervisorOverrideControl ID="supervisorOverrideControl" runat="server" />

<%--rev:mia 16jan2014-addition of supervisor override password..--%>
<uc1:SlotAvailableControl runat="server" ID="SlotAvailableControl" />