
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_BookingRequestAgentMgtPopupUserControl
    Inherits BookingUserControl

    Public Delegate Sub BookingRequestAgentMgtPopupUserControlEventHandler(ByVal sender As Object, ByVal saveButton As Boolean, ByVal backButton As Boolean, ByVal param As Object)
    Public Event PostBack As BookingRequestAgentMgtPopupUserControlEventHandler

    Public Const BookingRequestAgentMgt_XmlDoc_ViewState As String = "BookingRequestAgentMgt_XmlDoc"


    Private Property agentXmlDoc() As String
        Get
            If String.IsNullOrEmpty(ViewState.Item(BookingRequestAgentMgt_XmlDoc_ViewState)) Then
                Dim xmlDoc As New XmlDocument
                xmlDoc = Booking.GetBookingRequestAgent("", BookingId)
                ViewState.Item(BookingRequestAgentMgt_XmlDoc_ViewState) = xmlDoc.InnerXml
                Return xmlDoc.InnerXml
            Else
                Return ViewState.Item(BookingRequestAgentMgt_XmlDoc_ViewState)
            End If
        End Get
        Set(ByVal value As String)
            ViewState.Item(BookingRequestAgentMgt_XmlDoc_ViewState) = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the pop up.")
        End Try
    End Sub

    Public Sub loadPopup()

        programmaticBookingRequestAgentMgtPopup.Show()
        agentXmlDoc = ""
        formDatabind()

    End Sub

    Private Sub formDatabind()

        Dim xmlDoc As New XmlDocument
        'xmlDoc = Booking.GetBookingRequestAgent("", BookingId)
        xmlDoc.LoadXml(agentXmlDoc)

        agentNameTextBox.Text = xmlDoc.SelectSingleNode("//data/BookingRequest/MiscAgentName").InnerText
        contactTextBox.Text = xmlDoc.SelectSingleNode("//data/BookingRequest/MiscAgentContact").InnerText
        phCityTextBox.Text = xmlDoc.SelectSingleNode("//data/Phone/CountryCode").InnerText
        phAreaTextBox.Text = xmlDoc.SelectSingleNode("//data/Phone/AreaCode").InnerText
        phNumberTextBox.Text = xmlDoc.SelectSingleNode("//data/Phone/Number").InnerText
        faxCityTextBox.Text = xmlDoc.SelectSingleNode("//data/Fax/CountryCode").InnerText
        faxAreaTextBox.Text = xmlDoc.SelectSingleNode("//data/Fax/AreaCode").InnerText
        faxNumberTextBox.Text = xmlDoc.SelectSingleNode("//data/Fax/Number").InnerText
        emailTextBox.Text = xmlDoc.SelectSingleNode("//data/Email/Email").InnerText
        streetTextBox.Text = xmlDoc.SelectSingleNode("//data/BillingAddress/Street").InnerText
        suburbTextBox.Text = xmlDoc.SelectSingleNode("//data/BillingAddress/Suburb").InnerText
        cityTextBox.Text = xmlDoc.SelectSingleNode("//data/BillingAddress/CityTown").InnerText
        stateTextBox.Text = xmlDoc.SelectSingleNode("//data/BillingAddress/State").InnerText
        postCodeTextBox.Text = xmlDoc.SelectSingleNode("//data/BillingAddress/Postcode").InnerText
        countryPickerControl.DataId = xmlDoc.SelectSingleNode("//data/BillingAddress/Country").InnerText

        'ViewState.Item(BookingRequestAgentMgt_XmlDoc_ViewState) = xmlDoc.InnerXml


    End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles saveButton.Click
        Try

            programmaticBookingRequestAgentMgtPopup.Show()

            Dim xmlString As String = ""
            xmlString = agentXmlDoc 'ViewState.Item(BookingRequestAgentMgt_XmlDoc_ViewState)
            Dim xmlDoc As New XmlDocument

            If String.IsNullOrEmpty(xmlString) Then
                xmlString = Booking.GetBookingRequestAgent("", BookingId).InnerXml
            End If

            xmlString = xmlString.Replace("<data>", "<Root>")
            xmlString = xmlString.Replace("</data>", "<AddModUserId></AddModUserId><AddPrgName></AddPrgName></Root>")
            xmlDoc.LoadXml(xmlString)

            xmlDoc.SelectSingleNode("//Root/BookingRequest/MiscAgentName").InnerText = agentNameTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BookingRequest/MiscAgentContact").InnerText = contactTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Phone/CountryCode").InnerText = phCityTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Phone/AreaCode").InnerText = phAreaTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Phone/Number").InnerText = phNumberTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Fax/CountryCode").InnerText = faxCityTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Fax/AreaCode").InnerText = faxAreaTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Fax/Number").InnerText = faxNumberTextBox.Text
            xmlDoc.SelectSingleNode("//Root/Email/Email").InnerText = emailTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BillingAddress/Street").InnerText = streetTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BillingAddress/Suburb").InnerText = suburbTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BillingAddress/CityTown").InnerText = cityTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BillingAddress/State").InnerText = stateTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BillingAddress/Postcode").InnerText = postCodeTextBox.Text
            xmlDoc.SelectSingleNode("//Root/BillingAddress/Country").InnerText = countryPickerControl.DataId

            xmlDoc.SelectSingleNode("//Root/AddModUserId").InnerText = CurrentPage.UserCode
            xmlDoc.SelectSingleNode("//Root/AddPrgName").InnerText = "BookingRequestAgentMgtPopupUserControl.ascx"

            Dim returnMessage As String
            returnMessage = Booking.UpdateBookingRequestAgent(xmlDoc, "BookingRequest")

            If Not String.IsNullOrEmpty(returnMessage) Then

                If returnMessage.Contains("GEN046") Then
                    returnMessage = returnMessage.Substring(0, returnMessage.LastIndexOf("/"))
                    bookingRequestAgentMgtPopupMessagePanelControl.AddMessagePanelInformation(returnMessage)

                Else
                    bookingRequestAgentMgtPopupMessagePanelControl.AddMessagePanelError(returnMessage)
                End If

                'Else
                'programmaticBookingRequestAgentMgtPopup.Hide()
                'RaiseEvent PostBack(Me, True, False, Nothing)
            End If

        Catch ex As Exception
            programmaticBookingRequestAgentMgtPopup.Show()
            CurrentPage.LogException(ex)
            bookingRequestAgentMgtPopupMessagePanelControl.SetMessagePanelError("An error occurred while saving.")
        End Try

    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        Try
            programmaticBookingRequestAgentMgtPopup.Show()

            formDatabind()

            'agentNameTextBox.Text = ""
            'contactTextBox.Text = ""
            'phCityTextBox.Text = ""
            'phAreaTextBox.Text = ""
            'phNumberTextBox.Text = ""
            'faxCityTextBox.Text = ""
            'faxAreaTextBox.Text = ""
            'faxNumberTextBox.Text = ""
            'emailTextBox.Text = ""
            'streetTextBox.Text = ""
            'suburbTextBox.Text = ""
            'cityTextBox.Text = ""
            'stateTextBox.Text = ""
            'postCodeTextBox.Text = ""
            'countryPickerControl.DataId = ""
            'countryPickerControl.Text = ""

        Catch ex As Exception
            CurrentPage.LogException(ex)
            bookingRequestAgentMgtPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading.")
        End Try

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender

        backButton.Attributes.Add("onclick", "return hideBookingRequestAgentMgtPopupViaClient('programmaticBookingRequestAgentMgtPopupBehavior');")

    End Sub


End Class



