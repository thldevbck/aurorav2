﻿Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Drawing

Partial Class Booking_Controls_BookingDetailsUserControl
    Inherits BookingUserControl

    ''3719749

    Public Sub LoadDetails(usercode As String)
        Dim dsRental As New DataSet
        dsRental = Booking.GetBookingDetail(Me.BookingNumber, "BookRntRec", Me.RentalId, "", usercode)
        Dim dr As DataRow
        dr = dsRental.Tables(0).Rows(0)
        Dim RentalId As String = dr.Item("RntId")

        agentTextBox.Text = dr.Item("AgnName")
        hirerTextBox.Text = dr.Item("Hirer")
        statusTextBox.Text = dr.Item("BookingSt")
        statusTextBox.BackColor = DataConstants.GetBookingStatusColor(dr.Item("Status").ToString())

        Dim dv As DataView
        dv = New DataView(dsRental.Tables(0))
        dv.RowFilter = "RntId = '" & RentalId & "'"
        rentalGridView.DataSource = dv
        rentalGridView.DataBind()

        SetGridViewStatusColor()
    End Sub

    Private Sub SetGridViewStatusColor()

        For Each r As GridViewRow In rentalGridView.Rows
            r.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(r.Cells(7).Text)))
        Next
        
    End Sub

    Public Function GetRentalUrl(ByVal rentalId As Object) As String
        Dim rentalIdString As String
        rentalIdString = Convert.ToString(rentalId)

        Return "~/booking/RentalEdit.aspx?hdBookingId=" & BookingId.Split("-")(0) & "&hdRentalId=" & rentalIdString & "&hdBookingNum=" & BookingNumber & "&isFromSearch=0"
    End Function

    Public Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return ""
        End If
    End Function

    Public Function GetArrivalDepartureTime(ByVal value As Object) As String
        If value Is Nothing _
            OrElse (TypeOf value Is String _
                    AndAlso DirectCast(value, String).Trim().Length = 0 _
                    ) Then

            Return ""
        End If

        Dim dtValue As DateTime = CDate(value)

        Return " [" + dtValue.ToString("HH:mm") + "]"
    End Function

End Class
