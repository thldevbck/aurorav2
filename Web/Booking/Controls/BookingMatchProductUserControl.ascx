<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingMatchProductUserControl.ascx.vb" Inherits="Booking_Controls_BookingMatchProductUserControl" %>

<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl" TagPrefix="uc1" %>
    

<b>Match Products to Traveller:</b>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td colspan="2" width="100%" valign="top">
            <asp:GridView 
                ID="matchedProductGridView" 
                runat="server" 
                Width="100%" 
                CssClass="dataTableGrid"
                GridLines="none"
                AutoGenerateColumns="false">
                <AlternatingRowStyle CssClass="oddRow" />
                <RowStyle CssClass="evenRow" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="productLabel" runat="server" Text='<%# Bind("BpdId") %>' />
                            <asp:Label ID="customerLabel" runat="server" Text='<%# Bind("CusId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PrdDet" HeaderText="Matched Product" />
                    <asp:BoundField DataField="TrvDet" HeaderText="Travellers" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="allCheckBox" runat="server" />Un-Match
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="matchCheckBox" runat="server" Checked='<%# Eval("chk") %>' Enabled='<%# matchedProductCheckBoxEnabled(Eval("BpdId"),Eval("CusId")) %>'/>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>

<br />

<table width="100%" cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td width="50%" valign="top" style="padding-right: 2px">
            <asp:GridView 
                ID="productToMatchGridView" 
                runat="server" 
                Width="100%" 
                CssClass="dataTableGrid"
                AutoGenerateColumns="false" 
                GridLines="none"
                HeaderStyle-Height="20">
                <AlternatingRowStyle CssClass="oddRow" />
                <RowStyle CssClass="evenRow" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="productLabel" runat="server" Text='<%# Bind("BpdId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PrdDet" HeaderText="Product To Match" />
                    <asp:BoundField DataField="PrdQty" HeaderText="Qty To Match" />
                    <asp:TemplateField>
                        <HeaderTemplate>Match</HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="matchCheckBox" runat="server" Checked='<%# Eval("chk") %>' Enabled='<%# productToMatchCheckBoxEnabled(Eval("BpdId"),Eval("PrdQty")) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
        <td width="50%" valign="top" style="padding-left: 2px">
            <asp:GridView 
                ID="travellerToMatchGridView" 
                runat="server" 
                Width="100%" 
                CssClass="dataTableGrid"
                AutoGenerateColumns="false">
                <AlternatingRowStyle CssClass="oddRow" />
                <RowStyle CssClass="evenRow" />
                <Columns>
                    <asp:TemplateField Visible="false">
                        <ItemTemplate>
                            <asp:Label ID="customerLabel" runat="server" Text='<%# Bind("CusId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="TrvDet" HeaderText="Traveller To Match" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            <asp:CheckBox ID="allCheckBox" runat="server" />Match
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="matchCheckBox" runat="server" Checked='<%# Eval("chk") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
    </tr>
</table>

<br />

<table width="100%">
    <tr>
        <td align="right">
            <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
            <asp:Button ID="matchButton" runat="server" Text="Match" CssClass="Button_Standard Button_Add" />
            <asp:Button ID="unmatchButton" runat="server" Text="Un-Match" CssClass="Button_Standard Button_Remove" />
        </td>
    </tr>
</table>

<uc1:ConfimationBoxControl 
    ID="confimationBoxControl" 
    runat="server"
    LeftButton_Text="OK" 
    RightButton_Text="Cancel" 
    LeftButton_AutoPostBack="true" 
    RightButton_AutoPostBack="false" 
    Title="Booking Customer" />

