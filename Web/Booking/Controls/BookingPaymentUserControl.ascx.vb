
Imports Aurora.Common
Imports System.Xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common


Partial Class Booking_Controls_BookingPaymentUserControl
    Inherits BookingUserControl


#Region " Constants"
    Const QS_Payment As String = "PaymentMgt.aspx?funCode=PAYMGT"
#End Region

#Region " Protected Variables"
    Protected XMLpayment As New XmlDocument
    Protected XMLPaymentReceived As New XmlDocument
    Public isFirstLoad As Boolean
    Protected dsPayment As New DataSet
    Protected dsPaymentReceived As New DataSet
    Private Shared ctrForPayment As Integer
    Dim sb As New StringBuilder
#End Region

#Region " Properties"

    Public Property HiddenBookingID() As String
        Get
            Return CType(ViewState("HiddenBookingID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("HiddenBookingID") = value
        End Set
    End Property

    Public Property HiddenRentalID() As String
        Get
            Return CType(ViewState("HiddenRentalID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("HiddenRentalID") = value
        End Set
    End Property

    ''rev:mia sept.30 
    Public Property PaymentRentalID() As String
        Get
            Return CType(ViewState("PaymentRentalID"), String)
        End Get
        Set(ByVal value As String)
            ViewState("PaymentRentalID") = value
        End Set
    End Property
#End Region

#Region " Protected Properties"

    Protected ReadOnly Property Wrapper() As String
        Get
            Return ""
        End Get
    End Property


    Protected Property Total_Curr() As String
        Get
            Return ViewState("Total_Curr")
        End Get
        Set(ByVal value As String)
            ViewState("Total_Curr") = value
        End Set
    End Property


    Protected Property Total_LTotDue() As String
        Get
            Return ViewState("Total_LTotDue")
        End Get
        Set(ByVal value As String)
            ViewState("Total_LTotDue") = value
        End Set
    End Property


    Protected Property Total_LTotalPaidLo() As String
        Get
            Return ViewState("Total_LTotalPaidLo")
        End Get
        Set(ByVal value As String)
            ViewState("Total_LTotalPaidLo") = value
        End Set
    End Property


    Protected Property Total_LBlnDueLoc() As String
        Get
            Return ViewState("Total_LBlnDueLoc")
        End Get
        Set(ByVal value As String)
            ViewState("Total_LBlnDueLoc") = value
        End Set
    End Property


    Protected Property Total_LCustToPay() As String
        Get
            Return ViewState("Total_LCustToPay")
        End Get
        Set(ByVal value As String)
            ViewState("Total_LCustToPay") = value
        End Set
    End Property



    Protected Property Footer_TotalPaymentAmt() As String
        Get
            Return ViewState("Footer_TotalPaymentAmt")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_TotalPaymentAmt") = value
        End Set
    End Property


    Protected Property Footer_TotalPaymentCurrCode() As String
        Get
            Return ViewState("Footer_TotalPaymentCurrCode")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_TotalPaymentCurrCode") = value
        End Set
    End Property



    Protected Property Footer_LessBndRefdPaymtTot() As String
        Get
            Return ViewState("Footer_LessBndRefdPaymtTot")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_LessBndRefdPaymtTot") = value
        End Set
    End Property

    Protected Property Footer_LessBndRefdPaymtCurrCode() As String
        Get
            Return ViewState("Footer_LessBndRefdPaymtCurrCode")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_LessBndRefdPaymtCurrCode") = value
        End Set
    End Property

    Protected Property Footer_PlBPaidPaymtTot() As String
        Get
            Return ViewState("Footer_PlBPaidPaymtTot")
        End Get
        Set(ByVal value As String)

            ViewState("Footer_PlBPaidPaymtTot") = value
        End Set
    End Property

    Protected Property Footer_PlBPaidPaymtCurrCode() As String
        Get
            Return ViewState("Footer_PlBPaidPaymtCurrCode")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_PlBPaidPaymtCurrCode") = value
        End Set
    End Property



    Protected Property Footer_TotalPaymtCurrCode() As String
        Get
            Return ViewState("Footer_TotalPaymtCurrCode")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_TotalPaymtCurrCode") = value
        End Set
    End Property

    Private mFooter_TotalPaymtTot As String
    Protected Property Footer_TotalPaymtTot() As String
        Get
            Return ViewState("Footer_TotalPaymtTot")
        End Get
        Set(ByVal value As String)
            ViewState("Footer_TotalPaymtTot") = value
        End Set
    End Property

    Protected Property DefCurr() As String
        Get
            Return ViewState("DefCurr")
        End Get
        Set(ByVal value As String)
            ViewState("DefCurr") = value
        End Set
    End Property

#End Region

#Region " Subs"

    Sub ChangeColor(ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs)
        Dim row As DataRowView = CType(e.Item.DataItem, DataRowView)
        Dim EvenRow As HtmlTableRow = CType(e.Item.FindControl("trEvenRow"), HtmlTableRow)
        Dim OddRow As HtmlTableRow = CType(e.Item.FindControl("trOddRow"), HtmlTableRow)

        Try
            If row Is Nothing Then Exit Sub

            If row("Status") = "Ope" Then
                If EvenRow IsNot Nothing Then
                    EvenRow.BgColor = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.CurrentColor)
                Else
                    If OddRow IsNot Nothing Then
                        OddRow.BgColor = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.CurrentColor)
                    End If
                End If
            ElseIf row("Status") = "Clo" Then
                If EvenRow IsNot Nothing Then
                    EvenRow.BgColor = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor)
                Else
                    If OddRow IsNot Nothing Then
                        OddRow.BgColor = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.InactiveColor)
                    End If
                End If
            ElseIf row("Status") = "Rec" Then
                If EvenRow IsNot Nothing Then
                    EvenRow.BgColor = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.PastColor)
                Else
                    If OddRow IsNot Nothing Then
                        OddRow.BgColor = System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.PastColor)
                    End If
                End If
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub GetAmountOutstanding(ByVal vBookingID As String, ByVal vRentalID As String, ByVal vMyval As String)

        Try
            If String.IsNullOrEmpty(vBookingID) AndAlso String.IsNullOrEmpty(vRentalID) Then Exit Sub

            Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paymt_getAmountsOutstanding", MyBase.CurrentPage.UserCode, vBookingID, vRentalID, CBool(vMyval)).OuterXml.ToString
            xmlstring = AddIDinChosenPayment(xmlstring, "data/RentalDetail/Rental", "id")
            XMLpayment.LoadXml(xmlstring)
            litXPayment.Text = Server.HtmlEncode(xmlstring)
            dsPayment.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

            Me.repAmountOutstanding.DataSource = dsPayment.Tables("Rental")
            Me.repAmountOutstanding.DataBind()
            RefreshfooterOutstandingPayment()


        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("GetAmountOutstanding > " & ex.Message)
        End Try

    End Sub

    Protected Sub RefreshFooterForpaymentReceived(ByVal xmlstring As String)
        Dim xmlTempDoc As New XmlDocument
        If xmlstring = "" Then
            xmlstring = Server.HtmlDecode(Me.litXOutstandingPayment.Text)
        End If
        xmlTempDoc.LoadXml(xmlstring)
        Try

            Footer_TotalPaymtTot = xmlTempDoc.DocumentElement("PaymentMade").SelectSingleNode("Total/Paymt/Tot").InnerText
            Footer_TotalPaymtCurrCode = xmlTempDoc.DocumentElement("PaymentMade").SelectSingleNode("Total/Paymt/CurrCode").InnerText

            Footer_PlBPaidPaymtCurrCode = xmlTempDoc.DocumentElement("PaymentMade").SelectSingleNode("PlBPaid/Paymt/CurrCode").InnerText
            Footer_PlBPaidPaymtTot = xmlTempDoc.DocumentElement("PaymentMade").SelectSingleNode("PlBPaid/Paymt/Tot").InnerText

            Footer_LessBndRefdPaymtCurrCode = xmlTempDoc.DocumentElement("PaymentMade").SelectSingleNode("LessBndRefd/Paymt/CurrCode").InnerText
            Footer_LessBndRefdPaymtTot = xmlTempDoc.DocumentElement("PaymentMade").SelectSingleNode("LessBndRefd/Paymt/Tot").InnerText

            Footer_TotalPaymentCurrCode = Footer_LessBndRefdPaymtCurrCode
            Footer_TotalPaymentAmt = CDec(Footer_TotalPaymtTot) + CDec(Footer_PlBPaidPaymtTot)

        Catch ex As Exception

        End Try

        xmlTempDoc = Nothing
    End Sub

    Protected Sub RefreshfooterOutstandingPayment(Optional ByVal refreshToPay As Boolean = False)
        XMLpayment.LoadXml(Server.HtmlDecode(litXPayment.Text))
        dsPayment.ReadXml(New XmlTextReader(Server.HtmlDecode(litXPayment.Text), System.Xml.XmlNodeType.Document, Nothing))
        Try


            Me.Total_Curr = dsPayment.Tables("Total").Rows(0)("Curr")
            Me.Total_LTotDue = dsPayment.Tables("Total").Rows(0)("LTotDue")
            Me.Total_LTotalPaidLo = dsPayment.Tables("Total").Rows(0)("LTotalPaidLo")
            Me.Total_LCustToPay = dsPayment.Tables("Total").Rows(0)("LCustToPay")
            Me.Total_LBlnDueLoc = dsPayment.Tables("Total").Rows(0)("LBlnDueLoc")

            Dim blnDueNow As Boolean = CBool(CInt(dsPayment.Tables("Total").Rows(0)("LBlnDueLoc")) < 0)
            Dim lblRefund As Label = CType(pnlMisc.FindControl("lblRefund"), Label)
            If (dsPayment.Tables("RentalDetail").Rows.Count - 1) = -1 Then
                If blnDueNow = True Then
                    If Not lblRefund Is Nothing Then
                        lblRefund.Visible = True
                        imginfo.visible = True
                    End If
                Else
                    ''TODO:DispClientMessage('GEN164','')
                    lblRefund.Visible = False
                    imginfo.visible = False
                End If
            Else
                If blnDueNow = True Then
                    If Not lblRefund Is Nothing Then
                        lblRefund.Visible = True
                        imginfo.visible = True
                    End If
                Else
                    lblRefund.Visible = False
                    imginfo.visible = False
                End If
            End If

        Catch ex As Exception

        End Try

        Dim display As String = XMLpayment.SelectSingleNode("//data/RntAut").Attributes(0).Value
        Dim DefCurrs As String = XMLpayment.SelectSingleNode("//data/TotalDetail").Attributes(0).Value
        Dim AutChk As String = XMLpayment.SelectSingleNode("//data/RntAut").Attributes(1).Value
        Dim prepay As String = XMLpayment.SelectSingleNode("//data/RntAut").Attributes(2).Value
        Dim AllowMod As String = XMLpayment.SelectSingleNode("//data/RntAut").Attributes(3).Value
        Dim modUsr As String = XMLpayment.SelectSingleNode("//data/RntAut").Attributes(4).Value
        DefCurr = XMLpayment.SelectSingleNode("//data/TotalDetail").Attributes(0).Value
        Dim chkAuToTr As CheckBox = CType(Me.pnlMisc.FindControl("chkAuToTr"), CheckBox)
        If display = "1" Then
            If Not chkAuToTr Is Nothing Then
                chkAuToTr.Checked = IIf(AutChk = "1", True, False)
                chkAuToTr.Enabled = IIf(AllowMod = "1", True, False)
                chkAuToTr.Visible = True
                Me.litOutstanding.Text = "[ " & prepay & "  Agent with Payments Outstanding]"
                Me.litOutstanding.Visible = True
                Me.litMod.Text = modUsr
                Me.litMod.Visible = True
                lblTravelMSG.Visible = True
            End If
        Else
            If Not chkAuToTr Is Nothing Then
                Me.litOutstanding.Visible = False
                Me.litMod.Visible = False
                Me.lblTravelMSG.Visible = False
                chkAuToTr.Visible = False
            End If
        End If

        If refreshToPay = True Then
            DisplayTotal()
        End If
    End Sub

    Protected Sub DisplayTotal()
        Dim txtCustToPay As TextBox = Nothing
        Dim value As Decimal
        For Each repItem As RepeaterItem In Me.repAmountOutstanding.Items
            txtCustToPay = CType(repItem.FindControl("txtCustToPay"), TextBox)
            If txtCustToPay IsNot Nothing Then
                If txtCustToPay.Text.IndexOf(".") = -1 Then
                    txtCustToPay.Text = txtCustToPay.Text & ".00"
                End If
                value = value + CDec(txtCustToPay.Text)
            End If
        Next
        Me.Total_LCustToPay = value
    End Sub

    Protected Sub GetPaymentReceived(ByVal vBookingID As String, ByVal vRentalID As String, ByVal vMyval As String)

        Try
            ''System.Diagnostics.Debug.Assert(repPaymentReceived.Items.Count - 1 > 0, "repPaymentReceived control has no data")
            ''rev:mia Nov10
            If repPaymentReceived.Items.Count - 1 > 0 Then Exit Sub

            Dim xmlstring As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paymt_getPaymentReceived", vBookingID, vRentalID, CBool(vMyval)).OuterXml.ToString
            litXOutstandingPayment.Text = Server.HtmlEncode(xmlstring)
            xmlstring = AddIDinChosenPayment(xmlstring, "Master", "id")
            xmlstring = xmlstring.Replace("<Details>", "")
            xmlstring = xmlstring.Replace("<Detail>", "")
            xmlstring = xmlstring.Replace("</Details>", "")
            xmlstring = xmlstring.Replace("</Detail>", "")
            XMLPaymentReceived.LoadXml(xmlstring)
            dsPaymentReceived.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
            ctrForPayment = 0
            Me.repPaymentReceived.DataSource = dsPaymentReceived.Tables("Master")
            Me.repPaymentReceived.DataBind()

            RefreshFooterForpaymentReceived(xmlstring)
        Catch ex As Exception
            Me.btnAddPayment.Enabled = False
            MyBase.CurrentPage.SetErrorMessage("GetPaymentReceived > " & ex.Message)
        End Try

    End Sub

    Protected Sub BuiltDiv(ByVal vBookingID As String, ByVal vRentalID As String, ByVal vMyval As String)
        Try
            ViewState("myValue") = vMyval
            GetAmountOutstanding(vBookingID, vRentalID, vMyval)
            GetPaymentReceived(vBookingID, vRentalID, vMyval)

        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("BuiltDiv > " & ex.Message)
        End Try


    End Sub

    Public Sub LoadPage()
        Try


            If isFirstLoad Then
                If Me.HiddenRentalID = "" AndAlso Me.HiddenBookingID = "" Then

                    Dim xmlTempObject As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("cus_GetBookingDetail", "", "BookRec", "", Me.BookingNumber, MyBase.CurrentPage.UserCode)
                    If (xmlTempObject.DocumentElement.ChildNodes(0).SelectSingleNode("BooId") Is Nothing) AndAlso (xmlTempObject.DocumentElement.ChildNodes(0).SelectSingleNode("RntId") Is Nothing) Then
                        Me.HiddenBookingID = ""
                        Me.HiddenRentalID = ""

                    Else
                        Me.HiddenBookingID = xmlTempObject.DocumentElement.SelectSingleNode("Rental/BooId").InnerText
                        Me.HiddenRentalID = xmlTempObject.DocumentElement.SelectSingleNode("Rental/RntId").InnerText
                        If Not Me.PaymentRentalID.Equals(Me.HiddenRentalID) Then
                            Me.HiddenRentalID = Me.PaymentRentalID
                        End If

                    End If
                    xmlTempObject = Nothing
                End If

                BuiltDiv(Me.HiddenBookingID, Me.HiddenRentalID, 0)
            End If

            If Me.repPaymentReceived.Items.Count = 0 AndAlso Me.repAmountOutstanding.Items.Count = 0 Then
                lblPaymentReceived.Visible = True
                lblAmountOutstanding.Visible = True
                Me.btnAddPayment.Enabled = False
                Me.chkShowallrentals.Enabled = False
                pnlPaymentReceivedHeader.Visible = True
                pnlAmountOutstandingHeader.Visible = True
            Else
                lblPaymentReceived.Visible = False
                lblAmountOutstanding.Visible = False
                Me.btnAddPayment.Enabled = True
                Me.chkShowallrentals.Enabled = True
                pnlPaymentReceivedHeader.Visible = False
                pnlAmountOutstandingHeader.Visible = False
            End If

            If Me.repPaymentReceived.Items.Count = 0 Then
                pnlPaymentReceivedHeader.Visible = True
                lblPaymentReceived.Visible = True
            Else
                pnlPaymentReceivedHeader.Visible = False
                lblPaymentReceived.Visible = False
            End If

            If Me.repAmountOutstanding.Items.Count = 0 Then
                pnlAmountOutstandingHeader.Visible = True
                lblAmountOutstanding.Visible = True
            Else
                pnlAmountOutstandingHeader.Visible = False
                lblAmountOutstanding.Visible = False
            End If


        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("LoadPage > " & ex.Message & vbCrLf & ex.StackTrace)
        End Try
        

    End Sub


    ''rev:mia dec2 -- added checking of local or foreign
    Protected Sub GoToPayment(ByVal PmtID As String, ByVal PmtDate As String, ByVal MyVal As String, ByVal PmtPaymentOut As String, Optional ByVal onContinueNoDecryption As Boolean = False, Optional ByVal Localstring As String = "")
        Dim totalAmount As Decimal = 0
        Dim localAmount As Decimal = 0
        Dim bookingid As String = Me.HiddenBookingID
        Dim rntid As String = Me.HiddenRentalID
        Dim boonum As String = Me.BookingNumber
        Dim TypeStr As String = ""

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml(Server.HtmlDecode(litXPayment.Text))
        Dim ictr As Integer = xmlTemp.DocumentElement("TotalDetail").ChildNodes.Count - 1

        ''rev:mia dec19
        Dim xmlTempclone As New XmlDocument
        xmlTempclone.LoadXml(xmlTemp.OuterXml)
        Dim totalAmountClone As Decimal
        Dim localAmountClone As Decimal

        For i As Integer = 0 To ictr
            If CDec(Total_LCustToPay) <> CDec(0.0) Then
                If xmlTemp.DocumentElement("TotalDetail").ChildNodes(i).SelectSingleNode("Curr").InnerText = Me.DefCurr Then
                    ''-----rev:mia july 11 2013
                    localAmount = Total_LCustToPay
                    localAmountClone = Total_LCustToPay
                Else
                    'JL 2008-06-13
                    'Dim param1 As Decimal = localAmount
                    Dim param1 As Decimal = CDec(Total_LCustToPay)
                    Dim param2 As String = ""
                    Dim param3 As String = ""
                    If xmlTemp.DocumentElement("TotalDetail").ChildNodes(i).SelectSingleNode("Curr").InnerText <> "" Then
                        param2 = xmlTemp.DocumentElement("TotalDetail").ChildNodes(i).SelectSingleNode("Curr").InnerText
                    End If
                    If Me.DefCurr <> "" Then
                        param3 = Me.DefCurr
                    End If

                    ''rev:mia nov26 - bypass the currency conversion.Currency will always be based on the country of destination
                    ''param3 = param2
                    Dim xmlTempstrExchangeRate As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("ADM_CurrencyConverter", param1, param3, param2, param3).OuterXml
                    Dim xmlTempExchangeRate As New XmlDocument
                    xmlTempExchangeRate.LoadXml(xmlTempstrExchangeRate)
                    Try
                        If xmlTempExchangeRate.DocumentElement.ChildNodes(0).Name = "Error" Then
                            MyBase.CurrentPage.SetErrorMessage(xmlTempExchangeRate.DocumentElement.SelectSingleNode("Error").SelectSingleNode("ErrDesc").InnerText)
                            Exit Sub
                        Else
                            localAmount = xmlTempExchangeRate.DocumentElement.SelectSingleNode("Root").SelectSingleNode("ConRate").InnerText
                            localAmount = FormatNumber(localAmount)
                        End If
                    Catch ex As Exception
                        MyBase.CurrentPage.SetErrorShortMessage("No Exchange rates available for " & xmlTemp.DocumentElement("TotalDetail").ChildNodes(i).SelectSingleNode("Curr").InnerText & " -> " & Me.DefCurr)
                    End Try

                    ''rev:mia dec19
                    param3 = param2
                    xmlTempstrExchangeRate = Aurora.Common.Data.ExecuteSqlXmlSPDoc("ADM_CurrencyConverter", param1, param3, param2, param3).OuterXml
                    xmlTempExchangeRate.LoadXml(xmlTempstrExchangeRate)
                    Try
                        If xmlTempExchangeRate.DocumentElement.ChildNodes(0).Name = "Error" Then
                            MyBase.CurrentPage.SetErrorMessage(xmlTempExchangeRate.DocumentElement.SelectSingleNode("Error").SelectSingleNode("ErrDesc").InnerText)
                            Exit Sub
                        Else
                            localAmountClone = xmlTempExchangeRate.DocumentElement.SelectSingleNode("Root").SelectSingleNode("ConRate").InnerText
                            localAmountClone = FormatNumber(localAmountClone)
                        End If
                    Catch ex As Exception
                        MyBase.CurrentPage.SetErrorShortMessage("No Exchange rates available for " & xmlTemp.DocumentElement("TotalDetail").ChildNodes(i).SelectSingleNode("Curr").InnerText & " -> " & Me.DefCurr)
                    End Try
                End If

                totalAmount = totalAmount + localAmount
                ''rev:mia dec19
                totalAmountClone = totalAmountClone + localAmountClone

            End If
        Next

        If MyVal <> "1" Then
            If totalAmount = 0 Then
                MyBase.CurrentPage.SetInformationShortMessage("Payment amount has not been entered")
                Exit Sub
            End If
        End If

        ictr = xmlTemp.DocumentElement("RentalDetail").ChildNodes.Count - 1
        Dim sendXML As String = ""
        Dim sendXMLClone As String = ""

        If totalAmount.ToString.IndexOf(".") = -1 Then
            totalAmount = totalAmount.ToString & ".00"
            totalAmountClone = totalAmountClone.ToString & ".00"
        End If

        If MyVal = "1" Then
            sendXML = "<root><BookingId>" & bookingid & "</BookingId><BookingNo>" & boonum & "</BookingNo><RentalId>" & rntid & "</RentalId><AmountToPay>" & totalAmount & "</AmountToPay><PmPaymentDate>" & PmtDate & "</PmPaymentDate><Mode>1</Mode><PaymentId>" & PmtID & "</PaymentId><PmtPaymentOut>" & PmtPaymentOut & "</PmtPaymentOut>"
            sendXMLClone = "<root><BookingId>" & bookingid & "</BookingId><BookingNo>" & boonum & "</BookingNo><RentalId>" & rntid & "</RentalId><AmountToPay>" & totalAmountClone & "</AmountToPay><PmPaymentDate>" & PmtDate & "</PmPaymentDate><Mode>1</Mode><PaymentId>" & PmtID & "</PaymentId><PmtPaymentOut>" & PmtPaymentOut & "</PmtPaymentOut>"
        Else
            sendXML = "<root><BookingId>" & bookingid & "</BookingId><BookingNo>" & boonum & "</BookingNo><RentalId>" & rntid & "</RentalId><AmountToPay>" & totalAmount & "</AmountToPay><PmPaymentDate>" & PmtDate & "</PmPaymentDate><Mode>0</Mode><PaymentId></PaymentId><PmtPaymentOut></PmtPaymentOut>"
            sendXMLClone = "<root><BookingId>" & bookingid & "</BookingId><BookingNo>" & boonum & "</BookingNo><RentalId>" & rntid & "</RentalId><AmountToPay>" & totalAmountClone & "</AmountToPay><PmPaymentDate>" & PmtDate & "</PmPaymentDate><Mode>0</Mode><PaymentId></PaymentId><PmtPaymentOut></PmtPaymentOut>"
        End If
        sendXML = sendXML & "<AmountsOutstandingRoot>"
        sendXMLClone = sendXMLClone & "<AmountsOutstandingRoot>"

        Dim chk As CheckBox = Nothing
        Dim txt As TextBox = Nothing

        Dim ctrForRep As Integer = 0
        For Each repItem As RepeaterItem In Me.repAmountOutstanding.Items
            Dim xmlrntID As String = xmlTemp.DocumentElement("RentalDetail").ChildNodes(ctrForRep).SelectSingleNode("RntId").InnerText
            Dim type As String = xmlTemp.DocumentElement("RentalDetail").ChildNodes(ctrForRep).SelectSingleNode("Type").InnerText
            Dim CurrId As String = xmlTemp.DocumentElement("RentalDetail").ChildNodes(ctrForRep).SelectSingleNode("CurrId").InnerText

            Dim CurrCode As String = xmlTemp.DocumentElement("RentalDetail").ChildNodes(ctrForRep).SelectSingleNode("Curr").InnerText
            Dim AmountOwing As String = xmlTemp.DocumentElement("RentalDetail").ChildNodes(ctrForRep).SelectSingleNode("TotalDueLoc").InnerText
            ''CustToPay = eval('document.forms[0].txtCustToPay' + i).value

            chk = CType(repItem.FindControl("chkRental"), CheckBox)
            txt = CType(repItem.FindControl("txtCustToPay"), TextBox)
            If chk IsNot Nothing And txt IsNot Nothing Then
                Dim custTopay As String = txt.Text
                If (custTopay <> "0") AndAlso (custTopay <> "0.00") Then
                    sendXML = sendXML & "<AmountsOutstanding><RntId> " & xmlrntID & "</RntId>"
                    sendXML = sendXML & "<Type>" & type & "</Type><Currency>" & CurrId & "</Currency>"
                    sendXML = sendXML & "<CurrCode>" & CurrCode & "</CurrCode>"

                    sendXML = sendXML & "<AmountOwing>" & AmountOwing & "</AmountOwing>"
                    sendXML = sendXML & "<CustToPay>" & custTopay & "</CustToPay>"
                    sendXML = sendXML & "<CustToPay>" & custTopay & "</CustToPay>"
                    sendXML = sendXML & "</AmountsOutstanding>"
                    TypeStr = TypeStr + type & ","
                End If


                If (custTopay <> "0") AndAlso (custTopay <> "0.00") Then
                    sendXMLClone = sendXMLClone & "<AmountsOutstanding><RntId> " & xmlrntID & "</RntId>"
                    sendXMLClone = sendXMLClone & "<Type>" & type & "</Type><Currency>" & CurrId & "</Currency>"
                    sendXMLClone = sendXMLClone & "<CurrCode>" & CurrCode & "</CurrCode>"

                    sendXMLClone = sendXMLClone & "<AmountOwing>" & AmountOwing & "</AmountOwing>"
                    sendXMLClone = sendXMLClone & "<CustToPay>" & custTopay & "</CustToPay>"
                    sendXMLClone = sendXMLClone & "<CustToPay>" & custTopay & "</CustToPay>"
                    sendXMLClone = sendXMLClone & "</AmountsOutstanding>"
                End If
            End If
            ctrForRep = ctrForRep + 1
        Next

        If TypeStr.Contains("B") Then
            If TypeStr.Length > 2 Then
                MyBase.CurrentPage.SetInformationShortMessage("Please pay the Bonds Payment 'B' separately.")
                Exit Sub
            End If
        End If
        ''rev:mia 02-FEB-2016 https://thlonline.atlassian.net/browse/AURORA-682 - Modify Payment payment in Aurora to stop X payment with any other payment
        If TypeStr.Contains("X") Then
            If TypeStr.Length > 2 Then
                MyBase.CurrentPage.SetInformationShortMessage("Please pay the Bonds Payment 'X' separately.")
                Exit Sub
            End If
        End If

        ''rev:dec2
        sendXML = sendXML & "</AmountsOutstandingRoot><Local>" & Localstring & "</Local></root>"
        sendXMLClone = sendXMLClone & "</AmountsOutstandingRoot><Local>" & Localstring & "</Local></root>"

        ''---------------------------------------------------------
        ''rev:mia jan 30 2009 -- Changes from session to viewstate
        ''---------------------------------------------------------
        ''Session("PAYMGT" & MyBase.CurrentPage.UserCode) = sendXML
        ''Session("PAYMGTCLONE" & MyBase.CurrentPage.UserCode) = sendXMLClone
        ''Session("onContinueNoDecryption") = onContinueNoDecryption

        Session("PAYMGT" & MyBase.CurrentPage.UserCode & "_" & Me.HiddenRentalID) = sendXML
        Session("PAYMGTCLONE" & MyBase.CurrentPage.UserCode & "_" & Me.HiddenRentalID) = sendXMLClone
        Session("onContinueNoDecryption" & "_" & Me.HiddenRentalID) = onContinueNoDecryption
        ''---------------------------------------------------------




        Dim qsBookingID As String = "&hdBookingId=" & Me.HiddenBookingID
        Dim qsRentalID As String = "&hdRentalId=" & Me.HiddenRentalID
        ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
        Dim qsType As String = "&pTypes=" & TypeStr
        qsRentalID = String.Concat(qsRentalID, qsType)

        ''reV:mia nov3
        Response.Redirect(QS_Payment & qsBookingID & qsRentalID)


    End Sub

#End Region

#Region " Function"

    Protected Function AddIDinChosenPayment(ByVal xmlstring As String, ByVal xpath As String, ByVal nodename As String) As String
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(xmlstring)
        Dim xmlatt As XmlAttribute

        Dim newNode As XmlNode = Nothing

        Dim elem As XmlElement = xmldoc.DocumentElement

        Dim nodes As XmlNodeList = elem.SelectNodes(xpath)

        If nodes.Count - 1 = -1 Then
            nodes = xmldoc.SelectNodes(xpath)
        End If

        Dim ctr As Integer = 0
        For Each node As XmlNode In nodes
            If node.Name = "Rental" Then
                ''xmlatt = xmldoc.CreateAttribute(nodename)
                ''xmlatt.Value = ctr
                ''node.Attributes.Append(xmlatt)
                newNode = xmldoc.CreateElement(nodename & "Temp")
                newNode.InnerText = ctr
                node.AppendChild(newNode)

                xmlatt = xmldoc.CreateAttribute(nodename)
                xmlatt.Value = ctr
                node.Attributes.Append(xmlatt)

            ElseIf node.Name = "Master" Then

                newNode = xmldoc.CreateElement(nodename & "Temp")
                newNode.InnerText = ctr
                node.AppendChild(newNode)

                xmlatt = xmldoc.CreateAttribute(nodename)
                xmlatt.Value = ctr
                node.Attributes.Append(xmlatt)

            End If
            ctr = ctr + 1
        Next
        Return xmldoc.OuterXml
    End Function

#End Region

#Region " Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ''LoadPage()
            ''BuiltDiv(Me.HiddenBookingID, Me.HiddenRentalID, 0)
        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("Page_Load > " & ex.Message & vbCrLf & ex.StackTrace)
        End Try
    End Sub

#End Region

#Region " Events"

    Protected Sub repAmountOutstanding_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repAmountOutstanding.ItemCreated
        If e.Item.ItemType = ListItemType.Header Then
            Dim chkall As CheckBox = CType(e.Item.FindControl("chkAll"), CheckBox)
            If chkall IsNot Nothing Then
                chkall.Checked = CBool(ViewState("myValue"))
            End If
        End If
    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Try

            Dim blnCheckStatus As Boolean = CType(sender, CheckBox).Checked
            Dim chk As CheckBox = Nothing
            Dim lbltempID As Label = Nothing
            Dim txtCustToPay As TextBox
            Dim ssRntId As String = Nothing

            Dim xmlTemp As New XmlDocument
            xmlTemp.LoadXml(Server.HtmlDecode(litXPayment.Text))

            For Each repItem As RepeaterItem In Me.repAmountOutstanding.Items
                chk = CType(repItem.FindControl("chkRental"), CheckBox)
                lbltempID = CType(repItem.FindControl("lbltempID"), Label)
                txtCustToPay = CType(repItem.FindControl("txtCustToPay"), TextBox)

                If chk IsNot Nothing Then
                    chk.Checked = blnCheckStatus

                    If blnCheckStatus = True Then
                        If lbltempID IsNot Nothing Then
                            Dim tempID As String = lbltempID.Text
                            ssRntId = xmlTemp.DocumentElement("RentalDetail").SelectSingleNode("Rental[@id='" & tempID & "']/RntId").InnerText
                            Dim validXMLtemp As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paymt_validateRentalStatus", ssRntId)


                            If validXMLtemp.SelectSingleNode("data/Rental/validate").InnerText = "true" Then
                                If txtCustToPay IsNot Nothing Then
                                    txtCustToPay.Text = xmlTemp.DocumentElement("RentalDetail").SelectSingleNode("Rental[@id='" & tempID & "']/BlnDue").InnerText
                                    txtCustToPay.Focus()
                                End If
                            Else
                                Dim strAlert As String = "Payments may not be added for vehicles with a " & validXMLtemp.SelectSingleNode("data/Rental/RntStatus").InnerText & " status."
                                chk.Checked = False
                                txtCustToPay.Text = "0.00"

                                If (xmlTemp.DocumentElement.ChildNodes.Count - 1) < 2 Then
                                    CType(sender, CheckBox).Checked = False
                                End If


                                'Dim sb As New StringBuilder
                                'sb.Append("<script language=""javascript"">")
                                'sb.AppendFormat("alert({0})", strAlert)
                                'sb.Append("</script>")
                                'Response.Write(sb.ToString)
                                MyBase.CurrentPage.SetInformationShortMessage(strAlert)
                            End If
                        End If
                    Else
                        txtCustToPay.Focus()
                        txtCustToPay.Text = "0.00"
                    End If

                End If
            Next
            RefreshfooterOutstandingPayment(True)
            RefreshFooterForpaymentReceived("")
        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("chkAll_CheckedChanged >  " & ex.Message)
            Me.btnAddPayment.Enabled = False
        End Try
    End Sub

    Function CheckCustomerToPay(ByVal sender As Object) As Boolean

        ''rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
        ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
        ''R and D <= 8000
        ''B <= 7500
        Dim sAllowableAmountToRefund As String = String.Empty
        Dim sAllowableAmountToRefundForBondNZ As String = ConfigurationManager.AppSettings("AllowableAmountToRefundForBondNZ")
        Dim sAllowableAmountToRefundReceiptAndDepositNZ As String = ConfigurationManager.AppSettings("AllowableAmountToRefundReceiptAndDepositNZ")

        Dim CustToPayRental As TextBox = CType(sender, TextBox)
        Dim tempRepItem As RepeaterItem = CType(CustToPayRental.NamingContainer, RepeaterItem)
        Dim chkBox As CheckBox = CType(tempRepItem.FindControl("chkRental"), CheckBox)
        

        Dim litTypeText As System.Web.UI.DataBoundLiteralControl = CType(tempRepItem.Controls(0), System.Web.UI.DataBoundLiteralControl)
        Dim arrayOfTDs As String() = litTypeText.Text.Split("<td")
        Dim paymentType As String = ""

        For Each td As String In arrayOfTDs
            If td.IndexOf(">") <> -1 Then
                Try
                    paymentType = td.Substring(td.IndexOf(">") + 1, 1)
                    If (paymentType.Equals("R") = True Or paymentType.Equals("B") = True Or paymentType.Equals("D") = True) Then
                        Exit For
                    End If
                Catch ex As Exception
                End Try
            End If
        Next

        Select Case paymentType
            Case "D", "R"
                sAllowableAmountToRefund = sAllowableAmountToRefundReceiptAndDepositNZ
            Case "B"
                sAllowableAmountToRefund = sAllowableAmountToRefundForBondNZ
        End Select

        Dim lbltempID As Label = CType(tempRepItem.FindControl("lbltempID"), Label)

        Dim txtCustToPay As TextBox = CType(tempRepItem.FindControl("txtCustToPay"), TextBox)
        Dim ssRntId As String = Nothing

        Dim xmlTemp As New XmlDocument
        xmlTemp.LoadXml(Server.HtmlDecode(litXPayment.Text))

        If CustToPayRental IsNot Nothing Then

            If lbltempID IsNot Nothing Then
                Dim tempID As String = lbltempID.Text
                ssRntId = xmlTemp.DocumentElement("RentalDetail").SelectSingleNode("Rental[@id='" & tempID & "']/RntId").InnerText
                Dim countryCurrency As String = xmlTemp.DocumentElement("RentalDetail").SelectSingleNode("Rental[@id='" & tempID & "']/Curr").InnerText
                Dim validXMLtemp As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paymt_validateRentalStatus", ssRntId)

                If validXMLtemp.SelectSingleNode("data/Rental/validate").InnerText = "true" Then
                    If txtCustToPay IsNot Nothing Then
                        ''rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
                        ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
                        If Not String.IsNullOrEmpty(sAllowableAmountToRefund) And txtCustToPay.Text.IndexOf("-") <> -1 And countryCurrency.Equals("NZD") = True Then
                            Dim dAllowableAmountToRefund As Decimal = Math.Abs(Convert.ToDecimal(sAllowableAmountToRefund))
                            Dim enteredAmtCusToPay As Decimal = Math.Abs(CDec(txtCustToPay.Text))
                            If (dAllowableAmountToRefund < enteredAmtCusToPay) Then
                                txtCustToPay.Text = "0.00"
                                txtCustToPay.Focus()
                                If paymentType = "B" Then
                                    MyBase.CurrentPage.SetInformationShortMessage("The refund amount allowed for Bond type is only up to $" & sAllowableAmountToRefund)
                                Else
                                    MyBase.CurrentPage.SetInformationShortMessage("The refund amount allowed for Receipt or Deposit type is only up to $" & sAllowableAmountToRefund)
                                End If

                                If Not chkBox Is Nothing Then
                                    chkBox.Checked = False
                                End If
                                Return False
                            End If
                        End If


                        txtCustToPay.Enabled = True
                        btnAddPayment.Enabled = True

                        txtCustToPay.Focus()
                    End If
                Else
                    Dim strAlert As String = "Payments may not be added for vehicles with a " & validXMLtemp.SelectSingleNode("data/Rental/RntStatus").InnerText & " status."
                    txtCustToPay.Text = "0.00"
                    txtCustToPay.Enabled = False
                    btnAddPayment.Enabled = False

                    If (xmlTemp.DocumentElement.ChildNodes.Count - 1) < 2 Then
                        CType(sender, CheckBox).Checked = False
                    End If
                    MyBase.CurrentPage.SetInformationShortMessage(strAlert)

                End If
            End If
        Else
            txtCustToPay.Focus()
            txtCustToPay.Text = "0.00"
        End If
        Return True
    End Function

    Protected Sub chkRental_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)

        Try


            Dim chkRental As CheckBox = CType(sender, CheckBox)
            Dim tempRepItem As RepeaterItem = CType(chkRental.NamingContainer, RepeaterItem)
            Dim lbltempID As Label = CType(tempRepItem.FindControl("lbltempID"), Label)

            Dim txtCustToPay As TextBox = CType(tempRepItem.FindControl("txtCustToPay"), TextBox)
            Dim ssRntId As String = Nothing

            Dim xmlTemp As New XmlDocument
            xmlTemp.LoadXml(Server.HtmlDecode(litXPayment.Text))

            If chkRental IsNot Nothing Then
                If chkRental.Checked = True Then
                    If lbltempID IsNot Nothing Then
                        Dim tempID As String = lbltempID.Text
                        ssRntId = xmlTemp.DocumentElement("RentalDetail").SelectSingleNode("Rental[@id='" & tempID & "']/RntId").InnerText
                        Dim validXMLtemp As XmlDocument = Aurora.Common.Data.ExecuteSqlXmlSPDoc("paymt_validateRentalStatus", ssRntId)

                        If validXMLtemp.SelectSingleNode("data/Rental/validate").InnerText = "true" Then
                            If txtCustToPay IsNot Nothing Then
                                txtCustToPay.Enabled = True
                                btnAddPayment.Enabled = True
                                txtCustToPay.Text = xmlTemp.DocumentElement("RentalDetail").SelectSingleNode("Rental[@id='" & tempID & "']/BlnDue").InnerText
                                txtCustToPay.Focus()
                            End If
                        Else
                            Dim strAlert As String = "Payments may not be added for vehicles with a " & validXMLtemp.SelectSingleNode("data/Rental/RntStatus").InnerText & " status."
                            chkRental.Checked = False
                            txtCustToPay.Text = "0.00"
                            txtCustToPay.Enabled = False
                            btnAddPayment.Enabled = False

                            If (xmlTemp.DocumentElement.ChildNodes.Count - 1) < 2 Then
                                CType(sender, CheckBox).Checked = False
                            End If
                            MyBase.CurrentPage.SetInformationShortMessage(strAlert)

                            'Dim sb As New StringBuilder
                            'sb.Append("<script language=""javascript"">")
                            'sb.AppendFormat("alert({0})", strAlert)
                            'sb.Append("</script>")
                            'Response.Write(sb.ToString)
                        End If
                    End If
                    'Dim repHeader As RepeaterItem = CType(Me.repAmountOutstanding.HeaderTemplate, RepeaterItem)
                    'If repHeader IsNot Nothing Then

                    '    Dim chkall As CheckBox = CType(repHeader.FindControl("chkAll"), CheckBox)
                    '    If chkall IsNot Nothing Then
                    '        chkall.Checked = chkRental.Checked
                    '    End If

                    'End If


                Else
                    txtCustToPay.Focus()
                    txtCustToPay.Text = "0.00"
                End If

            End If
            GetPaymentReceived(Me.HiddenBookingID, Me.HiddenRentalID, ViewState("myValue"))
            RefreshfooterOutstandingPayment(True)
            RefreshFooterForpaymentReceived("")
           


        Catch ex As Exception
            MyBase.CurrentPage.SetErrorMessage("chkRental_CheckedChanged > " & ex.Message)
            Me.btnAddPayment.Enabled = False
        End Try
    End Sub

    Protected Sub txtCustToPay_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        ''rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
        ''rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
        If CheckCustomerToPay(sender) = True Then
            Logging.LogWarning("txtCustToPay_TextChanged", "exceeded refunding amount")
        End If
        GetPaymentReceived(Me.HiddenBookingID, Me.HiddenRentalID, ViewState("myValue"))
        RefreshfooterOutstandingPayment(True)
        RefreshFooterForpaymentReceived("")
    End Sub

    Protected Sub btnAddPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddPayment.Click
        Me.GoToPayment("", "", "", "")
    End Sub

#End Region


    Protected Sub repPaymentReceived_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles repPaymentReceived.ItemCommand
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If e.CommandName = "VisaImprint" Then




                Dim repItem As RepeaterItem = CType(e.Item.FindControl("lnkMethod").NamingContainer, RepeaterItem)
                Dim litGotoPayment As Literal = CType(repItem.FindControl("litGotoPayment"), Literal)


                If Not litGotoPayment Is Nothing Then




                    Dim splitParameter As String() = litGotoPayment.Text.Split(",")
                    If (splitParameter.Length - 1 = 4) OrElse (splitParameter.Length - 1 = 3) Then
                        ViewState("p0") = splitParameter(0)
                        ViewState("p1") = splitParameter(1)
                        ViewState("p2") = splitParameter(2)
                        ViewState("p3") = splitParameter(3)

                        If litGotoPayment.Text.Contains("NO") = True Then
                            Dim islocalstring As String = ""
                            If litGotoPayment.Text.Contains("Local") Then
                                islocalstring = "Local"
                            ElseIf litGotoPayment.Text.Contains("Foreign") Then
                                islocalstring = "Foreign"
                            End If
                            GoToPayment(splitParameter(0), splitParameter(1), splitParameter(2), splitParameter(3), False, islocalstring)
                        Else
                            Me.SupervisorOverrideControl1.Show()
                        End If


                    End If

                End If
            End If
        End If
    End Sub

    Protected Sub repPaymentReceived_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repPaymentReceived.ItemCreated
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            ChangeColor(e)
        End If
    End Sub

    Protected Sub repPaymentReceived_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles repPaymentReceived.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim row As DataRowView = CType(e.Item.DataItem, DataRowView)
            ChangeColor(e)
            Dim litGotoPayment As Literal = CType(e.Item.FindControl("litGotoPayment"), Literal)
            If Not litGotoPayment Is Nothing Then
                ''litGotoPayment.Text = String.Concat(row("Pmtid"), ",", row("PmtDate"), ",", 1, ",", row("PmtPaymentOut"))

                ''REV:18aug - Include EFT-POS
                If row("Method").ToString.Contains("Chq") OrElse _
                    row("Method").ToString.Contains("Cheque") OrElse _
                    row("Method").ToString.Contains("Cash") OrElse _
                    row("Method").ToString.Contains("EFT") Then

                    ''rev:mia dec2
                    Dim isLocalstring As String = ""
                    If row("Method").ToString.Contains("Local") Then
                        isLocalstring = "Local"
                    ElseIf row("Method").ToString.Contains("Foreign") Then
                        isLocalstring = "Foreign"
                    End If
                    litGotoPayment.Text = String.Concat(row("Pmtid"), ",", row("PmtDate"), ",", 1, ",", row("PmtPaymentOut"), ",", "NO", isLocalstring)
                Else
                    litGotoPayment.Text = String.Concat(row("Pmtid"), ",", row("PmtDate"), ",", 1, ",", row("PmtPaymentOut"))
                End If

            End If
        End If
    End Sub

    ''reV:mia nov3
    Public Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal IsOkButton As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBack
        'If IsOkButton Then
        '    Dim sReturnMsg As String = ""
        '    sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, "VIEWPMT")
        '    If (sReturnMsg) = "True" Then
        '        Session("SupervisorName") = SupervisorOverrideControl1.Login
        '        GoToPayment(ViewState("p0"), ViewState("p1"), ViewState("p2"), ViewState("p3"))
        '    Else
        '        MyBase.CurrentPage.SetErrorShortMessage(sReturnMsg)
        '    End If
        'End If
    End Sub


    ''rev:mia Oct 3
    Protected Sub chkAuToTr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkAuToTr.CheckedChanged
        Dim xmltemp As XmlDocument = New XmlDocument
        Try
            Dim value As Integer = IIf(chkAuToTr.Checked, 1, 0)
            Dim str As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("SP_UpdatePaymentAuth", Me.HiddenRentalID, value, MyBase.CurrentPage.UserCode, "PaymentControl").OuterXml.ToString
            xmltemp.LoadXml(str)
            If xmltemp.DocumentElement.InnerText <> "" Then
                MyBase.CurrentPage.SetErrorShortMessage(xmltemp.DocumentElement.InnerText)
            Else
                Me.litMod.Text = "By: " & MyBase.CurrentPage.UserCode
            End If
        Catch ex As Exception
            MyBase.CurrentPage.SetErrorShortMessage("Error in PaymentAuthorization")
        End Try
    End Sub

#Region "Added new button functionality for CreditCard no decryption - changes after releases!!!"
    ''rev:mia nov3
    Protected Sub SupervisorOverrideControl1_PostBackContinue(ByVal sender As Object, ByVal isOkButton As Boolean, ByVal isContinue As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBackContinue
        If isOkButton Then
            Dim sReturnMsg As String = ""
            sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, "VIEWPMT")
            If (sReturnMsg) = "True" Then

                ''---------------------------------------------------------
                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                ''---------------------------------------------------------
                Session("SupervisorName") = SupervisorOverrideControl1.Login

                Try
                    Dim objPage As Page = MyBase.Page
                    Dim notecontrol As Object = objPage.Controls(0).Controls(2).Controls(15).Controls(7).FindControl("bookingNotesUserControl")
                    If notecontrol IsNot Nothing Then
                        notecontrol.SupervisorName = SupervisorOverrideControl1.Login
                    End If

                Catch ex As Exception
                End Try
                ''---------------------------------------------------------

                GoToPayment(ViewState("p0"), ViewState("p1"), ViewState("p2"), ViewState("p3"), False)
            Else
                MyBase.CurrentPage.SetErrorShortMessage(sReturnMsg)
            End If

        ElseIf isContinue Then
            GoToPayment(ViewState("p0"), ViewState("p1"), ViewState("p2"), ViewState("p3"), True)
        End If
    End Sub
#End Region

End Class
