<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingSummaryUserControl.ascx.vb" Inherits="Booking_Controls_BookingSummaryUserControl" %>

<style type="text/css">
.bookingSummaryGridView th
{
    text-align: right;
}
</style>

<table width="100%">
    <tr>
        <td>
            <b>Payment & Charge Summary:</b>
        </td>
        <td align="right">
            <asp:CheckBox 
                ID="showAllRentalCheckBox" 
                runat="server" 
                Text="Show All Rentals" 
                AutoPostBack="true" />
        </td>
    </tr>
</table>

<asp:GridView 
    ID="bookingSummaryGridView" 
    runat="server" 
    Width="100%" 
    CssClass="dataTable bookingSummaryGridView"
    ShowHeader="true" 
    AutoGenerateColumns="false" 
    GridLines="None"
    CellPadding="2" 
    CellSpacing="0">
    <AlternatingRowStyle CssClass="oddRow" />
    <RowStyle CssClass="evenRow" />
    <Columns>
        <asp:BoundField ReadOnly="True" DataField="0" ItemStyle-HorizontalAlign="Left" />
        <asp:BoundField ReadOnly="True" DataField="1" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120" />
        <asp:BoundField ReadOnly="True" DataField="2" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120" />
        <asp:BoundField ReadOnly="True" DataField="3" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120" />
        <asp:BoundField ReadOnly="True" DataField="4" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="120" />
    </Columns>
</asp:GridView>

<table width="100%">
    <tr>
        <td align="right">
            <asp:RadioButtonList 
                ID="filterRadioButtonList" 
                runat="server" 
                AutoPostBack="True" 
                RepeatDirection="Horizontal" 
                Enabled="true">
                <asp:ListItem Text="All" Value="All" />
                <asp:ListItem Text="Customer" Value="Customer" Selected="True" />
            </asp:RadioButtonList>
        </td>
    </tr>
</table>

