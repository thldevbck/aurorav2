<%--<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingComplaintsUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingComplaintsUserControl" %>--%>
    <%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingComplaintsUserControl.ascx.vb" Inherits="BookingComplaintsUserControl" %>
    
<table width="100%">
    <tr>
        <td>
            <b>Complaint Summary:</b>
        </td>
    </tr>
</table>
<asp:GridView ID="grdComplaints" runat="server" AutoGenerateColumns="False" CssClass="dataTable"
    GridLines="None" Width="100%">
    <AlternatingRowStyle CssClass="oddRow" />
    <RowStyle CssClass="evenRow" />
    <Columns>
        <%--Hidden Fields--%>
        <%--<asp:BoundField DataField="CmpId" HeaderText="CmpId" />
        <asp:BoundField DataField="CmpLocCode" HeaderText="CmpLocCode" />
        <asp:BoundField DataField="CmpOwnerGroupRolId" HeaderText="CmpOwnerGroupRolId" />
        <asp:BoundField DataField="CmpOwnerUserUsrCode" HeaderText="CmpOwnerUserUsrCode" />
        <asp:BoundField DataField="CmpStatus" HeaderText="CmpStatus" />--%>
        <%--Hidden Fields--%>
        <asp:HyperLinkField DataTextField="LocName" DataNavigateUrlFields="CmpId,RntId" Target="_blank"
            DataNavigateUrlFormatString="~\Booking\ComplaintManagement.aspx?CmpId={0}&RntId={1}" HeaderText="Branch" />
        <asp:BoundField DataField="CmpStatusText" HeaderText="Status" />
        <asp:BoundField DataField="RolDesc" HeaderText="Complaint Owner (Group)" />
        <asp:BoundField DataField="UsrName" HeaderText="Complaint Owner (Individual)" />
        <asp:BoundField DataField="CmpDateReceived" HeaderText="Date Received" />
        <asp:BoundField DataField="CmpDateResolved" HeaderText="Date Resolved" />
    </Columns>
</asp:GridView>
<br />
<table width="100%">
    <tr>
        <td align="right">
            <asp:Button Text="Add Complaint" CssClass="Button_Standard Button_Add" runat="server"
                ID="btnAddComplaints" />
        </td>
    </tr>
</table>
