<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookingCancellationUserControl.ascx
''	Date Created	-	?
''	Author		    -	Jack Leong
''	Modified Hist	-	?.?.? / Base version
''                      22.8.8 / Shoel / Fixes for SQUISH# 582 - buttons moved into update panel
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingCancellationUserControl.ascx.vb"  Inherits="Booking_Controls_BookingCancellationUserControl" %>
    
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl" TagPrefix="uc1" %> 
 
 <script type="text/javascript">

     /////////////////////////////////////////////
     // Mod: Raj - 17 May, 2011

     function ClientValidation() {
         var boolValidate = true;
         var reasonDropDownList;
         reasonDropDownList = $get('<%= reasonDropDownList.ClientID %>');
         if (!validateAControl(reasonDropDownList)) {
             reasonDropDownList.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;

         }
         else {
             reasonDropDownList.style.background = "white";
         }

         var notificationDropDownList;
         notificationDropDownList = $get('<%= notificationDropDownList.ClientID %>');
         if (!validateAControl(notificationDropDownList)) {
             notificationDropDownList.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;

         }
         else {

             notificationDropDownList.style.background = "white";
         }

         var typeDropDownList;
         typeDropDownList = $get('<%= typeDropDownList.ClientID %>');
         if (!validateAControl(typeDropDownList)) {
             typeDropDownList.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
             boolValidate = false;

         }
         else {
             typeDropDownList.style.background = "white";
         }

         if (boolValidate == false) {
             return false;
         }

         return true;

     }

     function validateAControl(obj) {
         if (obj.value == "") {
             //setWarningShortMessage(getErrorMessage("GEN005"));
             return false
         }
         else {
             return true
         }
     }

</script>



<b>Cancellation:</b>

<table width="100%" cellpadding="2" cellspacing="0">
    <tr>
        <td width="150">Date / Time:</td>
        <td width="100">
            <uc1:DateControl Id="dateDateControl" runat="server" ReadOnly=true ></uc1:DateControl>
            
        </td>
        <td><uc1:TimeControl Id="timeTimeControl" runat="server" ReadOnly=true ></uc1:TimeControl></td>
    </tr>
    <tr>
        <td>Reason:</td>
        <td colspan="2">
            <asp:DropDownList ID="reasonDropDownList" runat="server" AppendDataBoundItems=true>
                <asp:ListItem Text="" Value=""></asp:ListItem>
            </asp:DropDownList>&nbsp;*
        </td>
    </tr>
    <tr>
        <td>Notification:</td>
        <td colspan="2">
            <asp:DropDownList ID="notificationDropDownList" runat="server" AppendDataBoundItems=true>
                <asp:ListItem Text="" Value=""></asp:ListItem>
            </asp:DropDownList>&nbsp;*
        </td>
    </tr>
    <tr>
        <td>Type:</td>
        <td colspan="2">
            <asp:DropDownList ID="typeDropDownList" runat="server" AppendDataBoundItems=true>
                <asp:ListItem Text="" Value=""></asp:ListItem>
            </asp:DropDownList>&nbsp;*
        </td>
    </tr>
</table>

<br />

<table width="100%">
    <tr>
        <td align="right">
        <asp:UpdatePanel ID="updMain" runat="server">
            <ContentTemplate>            
                <asp:Button 
                        ID="cancelRentalButton" 
                        runat="server" 
                        Text="Cancel Rental" 
                        Enabled="false"
                        CssClass="Button_Standard Button_Cancel" OnClientClick="return ClientValidation();"/>
                <asp:Button 
                        ID="cancelBookingButton" 
                        runat="server" 
                        Text="Cancel Booking" 
                        Enabled="false" 
                        CssClass="Button_Standard Button_Cancel" OnClientClick="return ClientValidation();"/>
            </ContentTemplate>
        </asp:UpdatePanel>

        </td>
    </tr>
</table>

<uc1:ConfimationBoxControl 
    ID="confimationBoxControl" 
    runat="server"
    LeftButton_Text="Yes" 
    RightButton_Text="No" 
    LeftButton_AutoPostBack="true" 
    RightButton_AutoPostBack="false"
    Title="Confirm Cancellation" />

