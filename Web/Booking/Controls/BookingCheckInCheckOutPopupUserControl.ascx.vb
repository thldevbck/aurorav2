'' Change Log 
'' 21.7.8 - Shoel - Session replaced with ViewState exposed via properties!

Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_BookingCheckInCheckOutPopupUserControl
    Inherits BookingUserControl

    Const VIEWSTATE_RENTALID As String = "VIEWSTATE_CICO_RntId"
    Const VIEWSTATE_SERIALNOSCHECKPASSED As String = "VIEWSTATE_CICO_SerialNosCheckPassed"
    Const VIEWSTATE_MAINTAINSERIALNUMBERMODE As String = "VIEWSTATE_CICO_MSNMode"
    Const THISPAGE_NAME As String = "BookingCheckInCheckOutPopupUserControl.ascx"

    Public Delegate Sub BookingCheckInCheckOutPopupUserControlEventHandler(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object)
    Public Event PostBack As BookingCheckInCheckOutPopupUserControlEventHandler

    

#Region "PUBLIC PROPERTIES"

    Public Property MSNMode() As String
        Get
            Return CStr(ViewState(VIEWSTATE_MAINTAINSERIALNUMBERMODE))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_MAINTAINSERIALNUMBERMODE) = value
        End Set
    End Property

    Public Property CurrentRentalId() As String
        Get
            Return CStr(ViewState(VIEWSTATE_RENTALID))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_RENTALID) = value
        End Set
    End Property

    Public Property SerialNumbersCheckPassed() As Boolean
        Get
            Return CBool(ViewState(VIEWSTATE_SERIALNOSCHECKPASSED))
        End Get
        Set(ByVal value As Boolean)
            ViewState(VIEWSTATE_SERIALNOSCHECKPASSED) = value
        End Set
    End Property

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub


    Public Sub LoadPopUpControl()
        programmaticModalPopup.Show()
        LoadSerialNumberTable()
    End Sub


    Public Sub LoadSerialNumberTable()

        Dim xmlReturn As XmlDocument = New XmlDocument
        ' check the mode
        If MSNMode = "All" Then
            ' Edit mode
            xmlReturn = Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber(CurrentRentalId, True)
        ElseIf MSNMode = "Empty" Then
            ' fill-the-missing-numbers-only mode
            xmlReturn = Aurora.Booking.Services.BookingCheckInCheckOut.GetProductsRequiringSerialNumber(CurrentRentalId, False)
        End If

        xmlReturn.LoadXml(xmlReturn.OuterXml.Replace("<data>", "").Replace("</data>", ""))
        Dim dstSerialNumbers As New DataSet
        dstSerialNumbers.ReadXml(New XmlNodeReader(xmlReturn))
        If dstSerialNumbers.Tables.Count = 0 Then
            xmlReturn.LoadXml("<Products><Prd BpdId="""" PrdName="""" SerialNum="""" /></Products>")
            dstSerialNumbers.ReadXml(New XmlNodeReader(xmlReturn))
        End If
        rptMain.DataSource = dstSerialNumbers.Tables(0)

        rptMain.DataBind()
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveSerialNumbers() Then
            programmaticModalPopup.Hide()
            RaiseEvent PostBack(Me, True, False, True)
        Else
            programmaticModalPopup.Show()
        End If

    End Sub

    Public Function SaveSerialNumbers() As Boolean

        ' on success
        Dim xmlReturn As New XmlDocument
        For Each oItem As RepeaterItem In rptMain.Items
            ' check dummy row
            If CType(oItem.FindControl("hdnBPDID"), HiddenField).Value.Trim().Equals(String.Empty) And _
                           CType(oItem.FindControl("txtSerialNumber"), TextBox).Text.Trim().Equals(String.Empty) Then
                SaveSerialNumbers = False
                Exit Function
            End If

            If CType(oItem.FindControl("txtSerialNumber"), TextBox).Text.Trim().Equals(String.Empty) Then
                bookingCICOPopupMessagePanelControl.AddMessagePanelError("Serial Number must be entered")
                SaveSerialNumbers = False
                Exit Function
            End If

            xmlReturn = Aurora.Booking.Services.BookingCheckInCheckOut.SaveProductSerialNumber( _
            CType(oItem.FindControl("hdnBPDID"), HiddenField).Value, _
            CType(oItem.FindControl("txtSerialNumber"), TextBox).Text, CurrentPage.UserCode, _
            CType(oItem.FindControl("hdnPrdName"), HiddenField).Value, THISPAGE_NAME)

            If xmlReturn.DocumentElement.SelectSingleNode("Error/Message") Is Nothing Then
                SerialNumbersCheckPassed = True
            Else
                ' theres an error!
                CurrentPage.SetErrorShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Error/Message").InnerText)
                SerialNumbersCheckPassed = False
                Exit Function
            End If
        Next

        ' check at the end 
        If SerialNumbersCheckPassed Then
            CurrentPage.SetInformationShortMessage(xmlReturn.DocumentElement.SelectSingleNode("Message").InnerText)
            LoadSerialNumberTable()
            SaveSerialNumbers = True
        End If
    End Function

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        programmaticModalPopup.Hide()
        RaiseEvent PostBack(Me, False, True, False)
    End Sub
End Class
