Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_BookingNotesUserControl
    Inherits BookingUserControl

    Private Const BookingNotes_dvBookingNotes_SessionName As String = "BookingNotes_dvBookingNotes"
    'Delegate Sub DelPopupPostBackObject(ByVal requireRefresh As Boolean)

    Private mShowEuropcarLogLink As Boolean

    Public isFirstLoad As Boolean = False

    Public Property ShowEuropcarLogLink As Boolean
        Get
            Return mShowEuropcarLogLink
        End Get
        Set(ByVal value As Boolean)
            mShowEuropcarLogLink = value
        End Set
    End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'Dim delPopupPostBack As New DelPopupPostBackObject(AddressOf Me.PopupPostBack)
            'Me.BookingNotesPopupUserControl1.PostBack = delPopupPostBack

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                'If Not Page.IsPostBack Then
                bookingNotesRepeater_dataBind()
            End If
            popupUserControlLoad()

            ''rev:mia Oct.24 2012 - Addition of ThirdParty Reference Report
            tdShowMoreNotes.Visible = HideShowMoreReportButton()

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Public Sub bookingNotesRepeater_dataBind()

        Dim ds As DataSet
        If showAllRentalCheckBox.Checked Then
            ds = BookingNotes.GetBookingNotes(BookingId, "", True)
        Else
            ds = BookingNotes.GetBookingNotes(BookingId, RentalId, False)
        End If

        'remove the existing session 
        Session.Remove(BookingNotes_dvBookingNotes_SessionName)
        'messageLabel.Text = ""

        If Utility.ParseString(ds.Tables(0).Rows(0)(0), "") = "GEN049/GEN049 - No Records Found" Or _
            ds.Tables.Count < 2 Then
            '"GEN049/GEN049 - No Records Found"
            'messageLabel.Text = "No Records Found"
            'CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, CurrentPage.GetMessage("GEN049"))

            'Disable the sort radio button list
            headerRadioButtonList.Enabled = False

            'Clear gridview
            bookingNotesRepeater.DataSource = Nothing
            bookingNotesRepeater.DataBind()

        Else

            ' Create an Datetime type col for sorting
            Dim dt As DataTable
            dt = ds.Tables(1)
            dt.Columns.Add(New DataColumn("AddDtTmDate", GetType(DateTime)))

            For Each dr As DataRow In dt.Rows
                dr.Item("AddDtTmDate") = Convert.ToDateTime(dr.Item("AddDtTm"))
            Next

            Dim dv As DataView
            dv = New DataView(dt)

            'Add the dataview to session
            Session.Add(BookingNotes_dvBookingNotes_SessionName, dv)

            'Sort the dv
            dv.Sort = headerRadioButtonList.SelectedValue

            If "AddDtTmDate" = headerRadioButtonList.SelectedValue Then
                dv.Sort = dv.Sort & " desc"
            End If

            'Bind to gridview
            bookingNotesRepeater.DataSource = dv
            bookingNotesRepeater.DataBind()

            headerRadioButtonList.Enabled = True
        End If

    End Sub

    'Sorting
    Protected Sub headerRadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles headerRadioButtonList.SelectedIndexChanged
        Try
            If Not Session.Item(BookingNotes_dvBookingNotes_SessionName) Is Nothing Then

                Dim dv As DataView
                dv = Session.Item(BookingNotes_dvBookingNotes_SessionName)

                dv.Sort = headerRadioButtonList.SelectedValue

                If "AddDtTmDate" = headerRadioButtonList.SelectedValue Then
                    dv.Sort = dv.Sort & " desc"
                End If

                bookingNotesRepeater.DataSource = dv
                bookingNotesRepeater.DataBind()

            Else
                'session has been expired - research again
                'CurrentPage.AddErrorMessage("The session has been expired, please search again.")
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "The session has been expired, please search again.")
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while sorting the booking notes.")
        End Try
    End Sub

    Protected Sub showAllRentalCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles showAllRentalCheckBox.CheckedChanged
        Try
            bookingNotesRepeater_dataBind()
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    'search notes
    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        'CurrentPage.referenceCurrentPageAsBackUrl()
        Response.Redirect("BookingNoteSearch.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&hdBookingNum=" & BookingNumber & "&isFromSearch=" & IsFromSearch)
    End Sub

    'Add note event
    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click

        popupUserControlLoad()

        BookingNotesPopupUserControl1.loadInsertTemplate()

    End Sub


    Public Sub popupUserControlLoad()
        BookingNotesPopupUserControl1.BookingId = BookingId
        BookingNotesPopupUserControl1.BookingNumber = BookingNumber
        BookingNotesPopupUserControl1.RentalId = RentalId
        BookingNotesPopupUserControl1.RentalNo = RentalNo

    End Sub

    Protected Sub BookingNotesPopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles BookingNotesPopupUserControl1.PostBack
        PopupPostBack(param)
    End Sub

    Public Sub PopupPostBack(ByVal requireRefresh As Boolean)
        If requireRefresh Then
            bookingNotesRepeater_dataBind()
        End If
    End Sub


#Region "Added/Modified By Manny"
    Private Const card As String = "Card"

    Protected Sub bookingNotesRepeater_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles bookingNotesRepeater.ItemCreated

        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then


            ''masked first the controls
            Dim row As DataRowView = CType(e.Item.DataItem, DataRowView)
            If row IsNot Nothing Then
                Dim TypeDesc As String = row("Type").ToString
                ''If TypeDesc.Contains("Card") Then
                If TypeDesc.Contains(card) Then

                    Dim textDesc As String = row("NoteDesc").ToString
                    Dim textDescTemp = textDesc

                    textDesc = textDesc.Replace(" ", "")
                    'Dim unmaskedChars As String = textDesc.Trim.Substring(textDesc.Length - 4)
                    Dim maskedChars As String = "XXXX-XXXX-XXXX-XXXX" ''& unmaskedChars
                    textDesc = textDesc.Replace(textDescTemp, maskedChars)

                    ''check if is allowed to view the content of the credit card
                    Dim lnkButton As LinkButton = CType(e.Item.FindControl("updateLinkButton"), LinkButton)
                    If lnkButton IsNot Nothing Then
                        Dim blnAllowedToViewCreditCard As Boolean = isAllowedToViewCreditCard()
                        If blnAllowedToViewCreditCard = False Then
                            ''disable the linkButton
                            lnkButton.Enabled = False
                        Else
                            ''display supervisorpassword
                            lnkButton.Enabled = True
                        End If
                    End If

                    row("NoteDesc") = textDesc
                Else
                End If
            End If

        End If
    End Sub


    Public Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal IsOkButton As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBack
        If IsOkButton Then
            Dim sReturnMsg As String = ""
            sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, "VIEWPMT")
            If (sReturnMsg) = "True" Then
                ''---------------------------------------------------------
                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                ''---------------------------------------------------------
                ''Session("SupervisorName") = SupervisorOverrideControl1.Login

                SupervisorName = SupervisorOverrideControl1.Login
                ''---------------------------------------------------------

                popupUserControlLoad()
                BookingNotesPopupUserControl1.loadEditTemplate(ViewState("VIEWMPT"))
            Else
                MyBase.CurrentPage.SetErrorShortMessage(sReturnMsg)
            End If
        End If

    End Sub

    Private Function isAllowedToViewCreditCard() As Boolean
        ''Return Convert.ToBoolean(Aurora.Common.Data.ExecuteScalarSP("getfunctionpermission", MyBase.CurrentPage.UserCode, "NOTE-CCARD"))
        ''sp_ViewCreditCard

        Dim xmldoc As New System.Xml.XmlDocument
        xmldoc.LoadXml(Aurora.Common.Data.ExecuteScalarSP("sp_ViewCreditCard", MyBase.CurrentPage.UserCode))
        If xmldoc.SelectSingleNode("isUser") IsNot Nothing Then
            If xmldoc.SelectSingleNode("isUser").InnerText = "True" Then Return True
        End If
        Return False
    End Function


    Protected Sub bookingNotesRepeater_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles bookingNotesRepeater.ItemCommand

        If e.CommandName.ToString() = "update" Then

            Dim litCardFlag As Literal = CType(e.Item.FindControl("litCardFlag"), Literal)
            Dim litCardFlagValue As String

            If litCardFlag IsNot Nothing Then
                litCardFlagValue = litCardFlag.Text

                If litCardFlagValue = 0 Then ''ordinary 
                    popupUserControlLoad()
                    Dim noteId As String
                    noteId = e.CommandArgument.ToString()
                    BookingNotesPopupUserControl1.loadEditTemplate(noteId)
                Else
                    ViewState("VIEWMPT") = e.CommandArgument.ToString
                    Me.SupervisorOverrideControl1.Show()
                End If

            End If
        End If

    End Sub

#End Region

    Protected Sub bookingNotesRepeater_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles bookingNotesRepeater.ItemDataBound
        If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
            Dim litCardFlag As Literal = CType(e.Item.FindControl("litCardFlag"), Literal)
            Dim lnkButton As LinkButton = CType(e.Item.FindControl("updateLinkButton"), LinkButton)
            ''masked first the controls
            Dim row As DataRowView = CType(e.Item.DataItem, DataRowView)
            If row IsNot Nothing Then
                Dim TypeDesc As String = row("Type").ToString
                ''If TypeDesc.Contains("Card") Then
                If TypeDesc.Contains(card) Then

                    Dim blnAllowedToViewCreditCard As Boolean = isAllowedToViewCreditCard()
                    If blnAllowedToViewCreditCard = False Then
                        litCardFlag.Text = 0
                    Else
                        litCardFlag.Text = 1
                    End If

                    If lnkButton IsNot Nothing Then
                        If blnAllowedToViewCreditCard = False Then
                            ''disable the linkButton
                            lnkButton.Enabled = False
                        Else
                            ''display supervisorpassword
                            lnkButton.Enabled = True
                        End If
                    End If
                Else
                    lnkButton.Enabled = True
                    litCardFlag.Text = 0
                End If
            End If

        End If
    End Sub

    Protected Function AllowUpdate(ByVal type As Object) As Boolean
        Dim typeString As String
        typeString = Convert.ToString(type)

        If typeString = "Rental" Or typeString = "Booking" Or typeString = "Package" Or typeString.ToString.Contains(card) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)

        If String.IsNullOrEmpty(dString) Then
            Return ""
        ElseIf dString.IndexOf(" ") > 0 Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return Utility.DateDBUIConvert(dString)
        End If
    End Function

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
        lnkEuropcarLog.Visible = ShowEuropcarLogLink
        MyBase.Render(writer)
    End Sub

#Region "rev:mia jan 30 2009 -- Changes from session to viewstate"
    ''---------------------------------------------------------
    ''rev:mia jan 30 2009 -- Changes from session to viewstate
    ''---------------------------------------------------------
    Public Property SupervisorName() As String
        Get
            Return ViewState("SupervisorName")
        End Get
        Set(ByVal value As String)
            ViewState("SupervisorName") = value
        End Set
    End Property
#End Region

#Region "rev:mia Oct.24 2012 - Addition of ThirdParty Reference Report"
    Private Function HideShowMoreReportButton() As Boolean
        ''Dim brdcode As String = Aurora.Confirmation.Services.Confirmation.ConfirmationGetBrand(RentalId)
        ''Dim brandname As String = Aurora.Confirmation.Services.Confirmation.ConfirmationGetBrandName(brdcode).ToUpper.Trim

        Return IIf(BookingNumber.Contains("M") = True, True, False)
    End Function

#End Region

    Protected Sub ShowEuropcatLog(ByVal sender As Object, ByVal e As EventArgs) Handles lnkEuropcarLog.Click
        EuropcarDataLogControl.RentalId = RentalId
        EuropcarDataLogControl.ShowPopup()
    End Sub

End Class

