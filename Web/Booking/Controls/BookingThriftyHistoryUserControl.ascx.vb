'' Change Log!
''  27.8.2008 - Shoel - "Let there be light!"


'' List of sps called!
'' RES_GetThriftyHistory

Imports Aurora.Common
Imports Aurora.Common.Utility
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_BookingThriftyHistoryUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                Dim sRentalNumber As String = BookingNumber & "/" & RentalNo
                gwThriftyHistory.DataSource = Aurora.Booking.Services.BookingThriftyHistory.GetThriftyHistory(sRentalNumber)
                gwThriftyHistory.DataBind()
            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking thrifty history tab.")
        End Try
    End Sub


    Protected Sub gwThriftyHistory_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwThriftyHistory.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim dtTemp As Date

            If Not CType(e.Row.Cells(1), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
               And Not CType(e.Row.Cells(1), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dtTemp = ParseDateTime(CType(e.Row.Cells(1), System.Web.UI.WebControls.TableCell).Text.Trim(), SystemCulture)
                CType(e.Row.Cells(1), System.Web.UI.WebControls.TableCell).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) _
                                                                                    & " " _
                                                                                    & dtTemp.TimeOfDay.Hours.ToString().PadLeft(2, "0"c) _
                                                                                    & ":" _
                                                                                    & dtTemp.TimeOfDay.Minutes.ToString().PadLeft(2, "0"c)

            End If

            If Not CType(e.Row.Cells(3), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
               And Not CType(e.Row.Cells(3), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dtTemp = ParseDateTime(CType(e.Row.Cells(3), System.Web.UI.WebControls.TableCell).Text.Trim(), SystemCulture)
                CType(e.Row.Cells(3), System.Web.UI.WebControls.TableCell).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) _
                                                                                    & " " _
                                                                                    & dtTemp.TimeOfDay.Hours.ToString().PadLeft(2, "0"c) _
                                                                                    & ":" _
                                                                                    & dtTemp.TimeOfDay.Minutes.ToString().PadLeft(2, "0"c) _
                                                                                    & " " _
                                                                                    & CType(e.Row.Cells(4), System.Web.UI.WebControls.TableCell).Text

            End If

            If Not CType(e.Row.Cells(5), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
               And Not CType(e.Row.Cells(5), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dtTemp = ParseDateTime(CType(e.Row.Cells(5), System.Web.UI.WebControls.TableCell).Text.Trim(), SystemCulture)
                CType(e.Row.Cells(5), System.Web.UI.WebControls.TableCell).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) _
                                                                                    & " " _
                                                                                    & dtTemp.TimeOfDay.Hours.ToString().PadLeft(2, "0"c) _
                                                                                    & ":" _
                                                                                    & dtTemp.TimeOfDay.Minutes.ToString().PadLeft(2, "0"c) _
                                                                                    & " " _
                                                                                    & CType(e.Row.Cells(6), System.Web.UI.WebControls.TableCell).Text

            End If

            Dim dAmount As Double = 0.0

            If Not CType(e.Row.Cells(12), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
               And Not CType(e.Row.Cells(12), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dAmount = CDbl(CType(e.Row.Cells(12), System.Web.UI.WebControls.TableCell).Text.Trim())
                CType(e.Row.Cells(12), System.Web.UI.WebControls.TableCell).Text = dAmount.ToString("C", System.Globalization.CultureInfo.CreateSpecificCulture(UserSettings.Current.ComCulture))

            End If

            If Not CType(e.Row.Cells(13), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
                   And Not CType(e.Row.Cells(13), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dAmount = CDbl(CType(e.Row.Cells(13), System.Web.UI.WebControls.TableCell).Text.Trim())
                CType(e.Row.Cells(13), System.Web.UI.WebControls.TableCell).Text = dAmount.ToString("C", System.Globalization.CultureInfo.CreateSpecificCulture(UserSettings.Current.ComCulture))

            End If

            If Not CType(e.Row.Cells(14), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
                   And Not CType(e.Row.Cells(14), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dAmount = CDbl(CType(e.Row.Cells(14), System.Web.UI.WebControls.TableCell).Text.Trim())
                CType(e.Row.Cells(14), System.Web.UI.WebControls.TableCell).Text = dAmount.ToString("C", System.Globalization.CultureInfo.CreateSpecificCulture(UserSettings.Current.ComCulture))

            End If

            If Not CType(e.Row.Cells(15), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) _
                   And Not CType(e.Row.Cells(15), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then

                dAmount = CDbl(CType(e.Row.Cells(15), System.Web.UI.WebControls.TableCell).Text.Trim())
                CType(e.Row.Cells(15), System.Web.UI.WebControls.TableCell).Text = dAmount.ToString("C", System.Globalization.CultureInfo.CreateSpecificCulture(UserSettings.Current.ComCulture))

            End If

        End If

    End Sub

    Protected Sub gwThriftyHistory_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwThriftyHistory.RowCreated
        e.Row.Cells(4).Visible = False
        e.Row.Cells(6).Visible = False
    End Sub
End Class
