
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_BookingVehicleUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                vehicleGridViewDataBind()
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking vehicle tab.")
        End Try
    End Sub

    Protected Sub showAllRentalCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles showAllRentalCheckBox.CheckedChanged
        Try
            vehicleGridViewDataBind()
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Private Sub vehicleGridViewDataBind()

        Dim ds As New DataSet
        If showAllRentalCheckBox.Checked Then
            ds = BookingVehicle.GetVehicle(BookingId, "")
        Else
            ds = BookingVehicle.GetVehicle(BookingId, RentalId)
        End If

        vehicleGridView.DataSource = ds.Tables(0)
        vehicleGridView.DataBind()


        ''rev:MIA put a comment on these dummy data
        'Dim dt As DataTable = New DataTable

        'dt.Columns.Add(New DataColumn("RntNum", GetType(String)))
        'dt.Columns.Add(New DataColumn("CkoDate", GetType(String)))
        'dt.Columns.Add(New DataColumn("CkoLoc", GetType(String)))
        'dt.Columns.Add(New DataColumn("CkiDate", GetType(String)))
        'dt.Columns.Add(New DataColumn("CkiLoc", GetType(String)))
        'dt.Columns.Add(New DataColumn("Vehicle", GetType(String)))

        'dt.Columns.Add(New DataColumn("UnitNum", GetType(String)))
        'dt.Columns.Add(New DataColumn("Rego", GetType(String)))
        'dt.Columns.Add(New DataColumn("OdoOut", GetType(String)))
        'dt.Columns.Add(New DataColumn("OdoIn", GetType(String)))

        'Dim dr As DataRow
        'dr = dt.NewRow()
        'dr(0) = "1"
        'dr(1) = "15/04/2006 10:30"
        'dr(2) = "DRW"
        'dr(3) = "24/04/2006 10:40"
        'dr(4) = "DRW"
        'dr(5) = "ZSALE"
        'dr(6) = "35208"
        'dr(7) = "TDU834"
        'dr(8) = "91325"
        'dr(9) = "93458"

        'dt.Rows.Add(dr)
        'vehicleGridView.DataSource = dt
        'vehicleGridView.DataBind()

    End Sub

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return ""
        End If
    End Function



End Class
