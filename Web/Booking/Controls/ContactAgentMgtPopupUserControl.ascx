<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ContactAgentMgtPopupUserControl.ascx.vb"
    Inherits="Booking_Controls_ContactAgentMgtPopupUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc1" %>

<script type="text/javascript">
        
        // Add click handlers for buttons to show and hide modal popup on pageLoad
        function pageLoad() {
            //$addHandler($get("showModalPopupClientButton"), 'click', showModalPopupViaClient);
            $addHandler($get("noteBackButton"), 'click', hideModalPopupViaClient);        
        }      
        function hideModalPopupViaClient(ev) {
            ev.preventDefault();        
            var modalPopupBehavior = $find('programmaticModalPopupBehavior');
            modalPopupBehavior.hide();
        }
</script>

<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticModalPopupBehavior"
    TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground" DropShadow="True" PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 600px; padding: 10px; height: 400px">
    <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        Maintain Contact
    </asp:Panel>
    <br />
    <uc1:MessagePanelControl ID="contactAgentMgtPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />

    <%--
    <div style="display: none">
        <asp:Label ID="agentIdLabel" runat="server" Text=""></asp:Label>
    </div>
    --%>
    <table width="100%">
        <tr>
            <td width="15%">
                Agent:</td>
            <td width="85%">
                <asp:TextBox ID="agentTextBox" runat="server" ReadOnly=true Width="99%" ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="15%">
                Agent Type:</td>
            <td width="35%">
                <asp:TextBox ID="agentTypeTextBox" runat="server" ReadOnly=true ></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td width="15%">
                </td>
        </tr>
    </table>
    <br />
    Contacts:
    <asp:Panel ID="Panel2" runat="server" ScrollBars="both" Height=220 Width="100%">
        <asp:GridView ID="contactGridView" runat="server" Width="100%" AutoGenerateColumns="False"
            CssClass="dataTable" RowStyle-Wrap="false">
            <AlternatingRowStyle CssClass="oddRow" />
            <RowStyle CssClass="evenRow" />
            <Columns>
                <asp:TemplateField HeaderText="PhoneId" Visible="false">
                    <ItemTemplate>
                        <asp:Label ID="contactIdLabel" runat="server" Text='<%# Bind("ContactId") %>'></asp:Label>
                        <asp:Label ID="integrityNoLabel" runat="server" Text='<%# Bind("IntegrityNo") %>'></asp:Label>
                        <asp:Label ID="emailPhnIdLabel" runat="server" Text='<%# Bind("EmailPhnId") %>'></asp:Label>
                        <asp:Label ID="emailIdLabel" runat="server" Text='<%# Bind("EmailId") %>'></asp:Label>
                        <asp:Label ID="emailIntegrityNoLabel" runat="server" Text='<%# Bind("EmailIntegrityNo") %>'></asp:Label>
                        <asp:Label ID="faxPhnIdLabel" runat="server" Text='<%# Bind("FaxPhnId") %>'></asp:Label>
                        <asp:Label ID="faxIdLabel" runat="server" Text='<%# Bind("FaxId") %>'></asp:Label>
                        <asp:Label ID="faxIntegrityNoLabel" runat="server" Text='<%# Bind("FaxIntegrityNo") %>'></asp:Label>
                        <asp:Label ID="phonePhnIdLabel" runat="server" Text='<%# Bind("PhonePhnId") %>'></asp:Label>
                        <asp:Label ID="phoneIdLabel" runat="server" Text='<%# Bind("PhoneId") %>'></asp:Label>
                        <asp:Label ID="phoneIntegrityNoLabel" runat="server" Text='<%# Bind("PhoneIntegrityNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remove">
                    <ItemTemplate>
                        <asp:CheckBox ID="removeCheckBox" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type">
                    <ItemTemplate>
                        <asp:DropDownList ID="typeIdDropDownList" runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Text="" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Surname">
                    <ItemTemplate>
                        <asp:TextBox ID="lNameTextBox" runat="server" Text='<%# Bind("LName") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="First Name">
                    <ItemTemplate>
                        <asp:TextBox ID="fNameTextBox" runat="server" Text='<%# Bind("FName") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Title">
                    <ItemTemplate>
                        <asp:TextBox ID="titleTextBox" runat="server" Width="50" Text='<%# Bind("Title") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Email">
                    <ItemTemplate>
                        <asp:TextBox ID="emailTextBox" runat="server" Text='<%# Bind("Email") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fax" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:TextBox ID="faxACodeTextBox" runat="server" Text='<%# Bind("FaxACode") %>' Width="50"></asp:TextBox>
                        <asp:TextBox ID="faxCCodeTextBox" runat="server" Text='<%# Bind("FaxCCode") %>' Width="50"></asp:TextBox>
                        <asp:TextBox ID="faxNoTextBox" runat="server" Text='<%# Bind("FaxNo") %>' Width="70"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Phone" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:TextBox ID="phoneACodeTextBox" runat="server" Text='<%# Bind("PhoneACode") %>' Width="50"></asp:TextBox>
                        <asp:TextBox ID="phoneCCodeTextBox" runat="server" Text='<%# Bind("PhoneCCode") %>' Width="50"></asp:TextBox>
                        <asp:TextBox ID="phoneNoTextBox" runat="server" Text='<%# Bind("PhoneNo") %>' Width="70"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Position">
                    <ItemTemplate>
                        <asp:TextBox ID="positionTextBox" runat="server" Text='<%# Bind("Position") %>'></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Comment">
                    <ItemTemplate>
                        <asp:TextBox ID="commentTextBox" runat="server" Text='<%# Bind("Comment") %>' Width="500"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <br />
    <table width="100%">
        <tr>
            <td align=right >
                <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <input id="noteBackButton" type="button" value="Cancel" class="Button_Standard Button_Cancel" />
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>
