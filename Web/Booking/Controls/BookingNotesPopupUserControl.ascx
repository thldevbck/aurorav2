<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingNotesPopupUserControl.ascx.vb" Inherits="Booking_Controls_BookingNotesPopupUserControl" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl" TagPrefix="uc1" %>

<script type="text/javascript">

    function validate() 
    {
        var boolValidate = true;       
        var  noteTextBox = $get('<%= noteTextBox.ClientID %>');
        boolValidate=va(noteTextBox)
        
        
        if (boolValidate == false) 
        {
            return false;
        }
        return true;
    }

    function va(obj) 
    {
        if (obj.value == "") 
        {
           obj.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
           return false;
        }
        else 
        {
            obj.style.background = "white";
        }
    }



    // Mod: Raj- 17 May, 2011  
//    function validate() {
//        var boolValidate = true;      
//        var noteTextBox;
//        noteTextBox = $get('<%= noteTextBox.ClientID %>');
//        if (!validateAControl(noteTextBox)) {
//            noteTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
//            boolValidate= false;
//        }
//        else {
//            noteTextBox.style.background = "white";
//        }

//        if (boolValidate == false) {
//            return false;
//        }
//        return true;
//    }

//    function validateAControl(obj) {
//        if (obj.value == "") {
//            return false
//        }
//        else {
//            return true
//        }
//    }
//</script>

<asp:Button 
    runat="server" 
    ID="hiddenTargetControlForModalPopup" 
    Style="display: none" />

<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="programmaticModalPopup" 
    BehaviorID="programmaticBookingNotesPopupBehavior"
    TargetControlID="hiddenTargetControlForModalPopup" 
    PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground" 
    DropShadow="True" 
    PopupDragHandleControlID="programmaticPopupDragHandle" />

<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 550px; height: 420px">
    <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        <asp:Label ID="titleLabel" runat="server">Maintain Note</asp:Label>
    </asp:Panel>
    
    <br />
    
    <uc1:MessagePanelControl ID="bookingNotesPopupMessagePanelControl" runat="server"  CssClass="popupMessagePanel" />

    <div style="display: none">
        <asp:TextBox ID="oldRentalNo" runat="server" />
        <asp:TextBox ID="oldType" runat="server" />
        <asp:TextBox ID="oldAudience" runat="server" />
        <asp:TextBox ID="oldPriority" runat="server" />
        <asp:CheckBox ID="oldActive" runat="server" />
        <asp:TextBox ID="oldNote" runat="server" />
    </div>
    
    <table cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td width="100px">Booking No:</td>
            <td width="125px">
                <asp:TextBox 
                    ID="bookingIdTextBox" 
                    runat="server" 
                    Width="100px"
                    ReadOnly="true" />
            </td>
            <td width="100px">Rental No:</td>
            <td width="125px">
                <asp:TextBox 
                    ID="rentalNoTextBox" 
                    runat="server" 
                    ReadOnly="true" 
                    Width="100px" />
            </td>
        </tr>
        <tr>
            <td>Type:</td>
            <td>
                <asp:DropDownList 
                    ID="typeDropDownList" 
                    runat="server" 
                    OnDataBinding="typeDropDownList_DataBinding"
                    Width="100px" />
            </td>
            <td>Audience:</td>
            <td>
                <asp:DropDownList ID="audienceDropDownList" runat="server" OnDataBinding="audienceDropDownList_DataBinding" Width="100px" />
            </td>
        </tr>
        <tr>
            <td>Priority:</td>
            <td>
                <asp:DropDownList ID="priorityDropDownList" runat="server" AppendDataBoundItems="true" Width="50px">
                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                    <asp:ListItem Text="3" Value="3" Selected="true"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>Active:</td>
            <td>
                <asp:CheckBox ID="activeCheckBox" runat="server" Checked="true" />
            </td>
        </tr>
        <tr>
            <td valign="top">Description:</td>
            <td colspan="3">
                <asp:TextBox 
                    ID="noteTextBox" 
                    runat="server" 
                    TextMode="multiLine" 
                    Rows="10" 
                    Width="400px"
                    MaxLength="2040" />
                &nbsp;*</td>
        </tr>
        <tr>
            <td>Added:</td>
            <td>
                <asp:TextBox ID="addTextBox" runat="server" ReadOnly="true" />
            </td>
            <td>Last Modified:</td>
            <td>
                <asp:TextBox ID="modifiedTextBox" runat="server" ReadOnly="true" />
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:TextBox ID="addTextBox1" runat="server" ReadOnly="true" />
            </td>
            <td>&nbsp;</td>
            <td>
                <asp:TextBox ID="modifiedTextBox1" runat="server" ReadOnly="true" />
            </td>
        </tr>
    </table>

    <br />

    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button 
                    ID="noteSaveButton" 
                    runat="server" 
                    Text="Save" 
                    CssClass="Button_Medium Button_Save" OnClientClick="return validate();" />
                <asp:Button 
                    ID="saveNewButton" 
                    runat="server" 
                    Text="Save/New" 
                    CssClass="Button_Medium Button_New" OnClientClick="return validate();" />
                <asp:Button 
                    ID="backButton" 
                    runat="server" 
                    Text="Cancel" 
                    CssClass="Button_Medium Button_Cancel" />
                <asp:Button 
                    ID="cancelButton" 
                    runat="server" 
                    Text="Reset" 
                    CssClass="Button_Reset Button_Medium" />
                <asp:Button 
                    ID="copyButton" 
                    runat="server" 
                    Text="Copy" 
                    CssClass="Button_Medium" />
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>
