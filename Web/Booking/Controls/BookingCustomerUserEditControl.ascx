﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingCustomerUserEditControl.ascx.vb" Inherits="Booking_Controls_BookingCustomerUserEditControl" %>

<asp:Button 
    runat="server" 
    ID="hiddenTargetControlForModalPopup_BookingCustomerUserEditControl" 
    Style="display: none" />

<ajaxToolkit:ModalPopupExtender 
    runat="server" 
    ID="programmaticModalPopup_BookingCustomerUserEditControl" 
    BehaviorID="programmaticBookinPopupBehavior_BookingCustomerUserEditControl"
    TargetControlID="hiddenTargetControlForModalPopup_BookingCustomerUserEditControl" 
    PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground" 
    DropShadow="True" 
    PopupDragHandleControlID="programmaticPopupDragHandle" />

<asp:Panel runat="server" 
           CssClass="modalPopup" 
           ID="programmaticPopup" 
           Style="display: none;
           width: 550px; height: 140px">

        <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
            <asp:Label ID="titleLabel" runat="server">Update Customer Name and Title</asp:Label>
        </asp:Panel>
    
    <br />
    
    
    <table cellpadding="2" cellspacing="0" width="100%">
        <tr>
            <td width="100px">Title:</td>
            <td width="125px">
                <asp:DropDownList ID="customertitleDropdown" runat="server" Width="50px">
                </asp:DropDownList>
            </td>
            <td width="100px">FirstName:</td>
            <td width="125px">
                <asp:TextBox 
                    ID="FirstNameTextBox" 
                    runat="server" 
                    ReadOnly="false" 
                    Width="150px" />
                    <ajaxToolkit:FilteredTextBoxExtender ID="Filter_FirstNameTextBox" 
                    runat="server" 
                    FilterType="Custom"  
                    FilterMode="InvalidChars" 
                    TargetControlID="FirstNameTextBox" InvalidChars="`~!@#$%^&*()_-=+[{]}\|':;,<>/?&quot;&quot;"/> 
            </td>

            <td width="100px">LastName:</td>
            <td width="125px">
                <asp:TextBox 
                    ID="LastNameTextBox" 
                    runat="server" 
                    ReadOnly="false" 
                    Width="150px" />
                    <ajaxToolkit:FilteredTextBoxExtender ID="Filter_LastNameTextBox" 
                    runat="server" 
                    FilterType="Custom"  
                    FilterMode="InvalidChars" 
                    TargetControlID="LastNameTextBox" InvalidChars="`~!@#$%^&*()_-=+[{]}\|':;,<>/?&quot;&quot;"/> 
            </td>
        </tr>
      
    </table>

    <br />

    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button 
                    ID="saveButtonCustomerEdit" 
                    runat="server" 
                    Text="Save" 
                    CssClass="Button_Medium Button_New" />

                <asp:Button 
                    ID="cancelButtonCustomerEdit" 
                    runat="server" 
                    Text="Cancel" 
                    CssClass="Button_Medium Button_Cancel" 
                     />
               
            </td>
        </tr>
    </table>
    
</asp:Panel>
