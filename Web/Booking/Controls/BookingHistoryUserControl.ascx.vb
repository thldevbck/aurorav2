'' Change Log!
''  1.07.2008 - Shoel - Added the date utility thingie + removed commented/unused code bits
''  1.07.2008 - Shoel - got the date thingie right this time :) i think
''  26.09.2008- Shoel - fixed the currency code in header row :)

'' list of sps
'' tab_getHistory
Imports Aurora.Common
Imports Aurora.Common.Utility
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_BookingHistoryUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False

    Property IsHeaderCurrencyChanged() As Boolean
        Get
            Return CBool(ViewState("Hist_IsHeadrCurrChngd"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("Hist_IsHeadrCurrChngd") = value
        End Set
    End Property

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                IsHeaderCurrencyChanged = False
                bookingHistoryGridView.DataSource = BookingHistory.GetHistory(RentalId)
                bookingHistoryGridView.DataBind()
            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking history tab.")
        End Try
    End Sub

    Protected Sub bookingHistoryGridView_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles bookingHistoryGridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            If e.Row.Cells(21).Text <> "0" Then
                CType(e.Row.Cells(1), System.Web.UI.WebControls.TableCell).Attributes.Add("OnMouseOver", "showHistoryPopUp(event, true,'" & e.Row.Cells(23).Text & "','" & e.Row.Cells(22).Text & "','" & e.Row.Cells(24).Text & "','" & e.Row.Cells(25).Text & "','" & e.Row.Cells(26).Text & "');")
                CType(e.Row.Cells(2), System.Web.UI.WebControls.TableCell).Attributes.Add("OnMouseOver", "showHistoryPopUp(event, true,'" & e.Row.Cells(23).Text & "','" & e.Row.Cells(22).Text & "','" & e.Row.Cells(24).Text & "','" & e.Row.Cells(25).Text & "','" & e.Row.Cells(26).Text & "');")
                CType(e.Row.Cells(3), System.Web.UI.WebControls.TableCell).Attributes.Add("OnMouseOver", "showHistoryPopUp(event, true,'" & e.Row.Cells(23).Text & "','" & e.Row.Cells(22).Text & "','" & e.Row.Cells(24).Text & "','" & e.Row.Cells(25).Text & "','" & e.Row.Cells(26).Text & "');")


                CType(e.Row.Cells(1), System.Web.UI.WebControls.TableCell).Attributes.Add("OnMouseOut", "showHistoryPopUp(event, false);")
                CType(e.Row.Cells(2), System.Web.UI.WebControls.TableCell).Attributes.Add("OnMouseOut", "showHistoryPopUp(event, false);")
                CType(e.Row.Cells(3), System.Web.UI.WebControls.TableCell).Attributes.Add("OnMouseOut", "showHistoryPopUp(event, false);")
            End If

            'function showHistoryPopUp(MyVal,Exists,Prd,ChEv,Qty,UOM,Rate)

            ' the date thingie for cells #4, #8 & #9
            Dim dtTemp As Date
            If Not CType(e.Row.Cells(4), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) And Not CType(e.Row.Cells(4), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then
                dtTemp = ParseDateTime(CType(e.Row.Cells(4), System.Web.UI.WebControls.TableCell).Text.Split(" "c)(0).Trim(), SystemCulture)
                CType(e.Row.Cells(4), System.Web.UI.WebControls.TableCell).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) & " " & CType(e.Row.Cells(4), System.Web.UI.WebControls.TableCell).Text.Split(" "c)(1).Trim()
            End If

            If Not CType(e.Row.Cells(8), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) And Not CType(e.Row.Cells(8), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then
                dtTemp = ParseDateTime(CType(e.Row.Cells(8), System.Web.UI.WebControls.TableCell).Text.Split(" "c)(0).Trim(), SystemCulture)
                CType(e.Row.Cells(8), System.Web.UI.WebControls.TableCell).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) & "  " & CType(e.Row.Cells(8), System.Web.UI.WebControls.TableCell).Text.Split(" "c)(2).Trim()
            End If

            If Not CType(e.Row.Cells(9), System.Web.UI.WebControls.TableCell).Text.Trim().Equals(String.Empty) And Not CType(e.Row.Cells(9), System.Web.UI.WebControls.TableCell).Text.Trim().Equals("&nbsp;") Then
                dtTemp = ParseDateTime(CType(e.Row.Cells(9), System.Web.UI.WebControls.TableCell).Text.Split(" "c)(0).Trim(), SystemCulture)
                CType(e.Row.Cells(9), System.Web.UI.WebControls.TableCell).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat).Split(" "c)(0) & "  " & CType(e.Row.Cells(9), System.Web.UI.WebControls.TableCell).Text.Split(" "c)(2).Trim()
            End If
            ' the date thingie for cells #4, #8 & #9

            For Each cell As System.Web.UI.WebControls.TableCell In e.Row.Cells
                cell.Text = "&nbsp;" & cell.Text & "&nbsp;"
            Next

            e.Row.BackColor = DataConstants.GetBookingStatusColor("" & CType(e.Row.DataItem, DataRowView)("RntSt"))
            If Not IsHeaderCurrencyChanged Then
                CType(e.Row.Parent.Parent, System.Web.UI.WebControls.GridView).HeaderRow.Cells(14).Text = "Gross (" & e.Row.Cells(27).Text & ")"
                IsHeaderCurrencyChanged = True
                ' do this just once!
            End If

        End If
        ' hide last cell
        e.Row.Cells(27).Visible = False
    End Sub

    Protected Sub bookingHistoryGridView_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles bookingHistoryGridView.RowCreated
        e.Row.Cells(21).Visible = False
        e.Row.Cells(22).Visible = False
        e.Row.Cells(23).Visible = False
        e.Row.Cells(24).Visible = False
        e.Row.Cells(25).Visible = False
        e.Row.Cells(26).Visible = False
    End Sub

End Class