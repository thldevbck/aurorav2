<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingIncidentsUserControl.ascx.vb" Inherits="Booking_Controls_BookingIncidentsUserControl" %>



<table width="100%">
    <tr>
        <td><b>Incident Summary:</b></td>
        <td align="right">
            <asp:CheckBox 
                ID="chkShowAllRentals" 
                runat="server" 
                AutoPostBack="true" 
                OnCheckedChanged="chkShowAllRentals_CheckedChanged"
                Text="Show All Rentals" />
        </td>
    </tr>
</table>

<asp:GridView 
    ID="gwIncidents" 
    runat="server" 
    AutoGenerateColumns="False" 
    EmptyDataText="No Incidents found"
    CssClass="dataTable" 
    GridLines="None"
    Width="100%">
    <AlternatingRowStyle CssClass="oddRow" />
    <RowStyle CssClass="evenRow" />
    <Columns>
        <asp:BoundField DataField="IncId" HeaderText="IncId" />
        <asp:TemplateField HeaderText="Branch/Date">
            <ItemTemplate>
                <asp:Label ID="lblLink" runat="server" Text='<%# Bind("Lodge") %>' Style="hyperlink_label_mouseout"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <%--<asp:BoundField DataField="Type" HeaderText="Type" />--%>
        <asp:BoundField DataField="Cause" HeaderText="Cause(s)" />
        <%--<asp:BoundField DataField="ResolveDtTm" HeaderText="Resolved Date" />--%>
        <asp:BoundField DataField="UnitNo" HeaderText="Unit No/Reg" />
        <%--<asp:BoundField DataField="Status" HeaderText="Status" />--%>
        <asp:BoundField DataField="AIMSRefNo" HeaderText="AIMS Ref" />
        <asp:BoundField DataField="Status" HeaderText="Status" />
        <asp:BoundField DataField="RentalNo" HeaderText="Rental" />
    </Columns>
    <EmptyDataTemplate>
        No Incidents Added
    </EmptyDataTemplate>
</asp:GridView>

<br />

<table width="100%">
    <tr>
        <td align="right">
            <asp:Button 
                Text="Add Incident" 
                CssClass="Button_Standard Button_Add" 
                runat="server" 
                ID="btnAddIncidents" />
        </td>
    </tr>
</table>
