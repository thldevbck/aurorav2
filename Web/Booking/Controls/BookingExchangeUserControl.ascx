<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingExchangeUserControl.ascx.vb" Inherits="Booking_Controls_BookingExchangeUserControl" %>
    
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl" TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx" TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/AddNoteRatePopupUserControl.ascx" TagName="AddNoteRatePopupUserControl" TagPrefix="uc1" %>

    <script language="javascript" type="text/javascript">

        /////////////////////////////////////////////
        // Mod: Raj - 17 May, 2011

        function validate() {
            var boolValidate = true;

            var exchangeTypeDropDownList;
            exchangeTypeDropDownList = $get('<%= exchangeTypeDropDownList.ClientID %>');
            if (!validateAControl(exchangeTypeDropDownList)) {
                exchangeTypeDropDownList.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                exchangeTypeDropDownList.style.background = "white";
            }


            var currDateControl;
            currDateControl = $get('<%= currDateControl.ClientID %>');
            if (!validateAControl(currDateControl)) {
                currDateControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                currDateControl.style.background = "white";
            }

            var currTimeControl;
            currTimeControl = $get('<%= currTimeControl.ClientID %>');
            if (!validateAControl(currTimeControl)) {
                currTimeControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                currTimeControl.style.background = "white";
            }


            var branchPickerControl;
            branchPickerControl = $get('<%= branchPickerControl.ClientID %>');
            if (!validateAControl(branchPickerControl)) {
                branchPickerControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                branchPickerControl.style.background = "white";
            }


            var reTypeDropDownList;
            reTypeDropDownList = $get('<%= reTypeDropDownList.ClientID %>');
            if (!validateAControl(reTypeDropDownList)) {
                reTypeDropDownList.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                reTypeDropDownList.style.background = "white";
            }

            var reVehiclePickerControl;
            reVehiclePickerControl = $get('<%= reVehiclePickerControl.ClientID %>');
            if (!validateAControl(reVehiclePickerControl)) {
                reVehiclePickerControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                reVehiclePickerControl.style.background = "white";
            }


            if (boolValidate == false) {
                return false;
            }

            return true;

        }

        function validateAControl(obj) {
            if (obj.value == "") {
                //setWarningShortMessage(getErrorMessage("GEN005"))
                return false
            }
            else {
                return true
            }
        }

        /////////////////////////////////////////////


    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);

    function BeginRequestHandler(sender, args) {
        try
        {
            pbControl = args.get_postBackElement();  //the control causing the postback
            if(pbControl.id.indexOf('saveButton') > -1)
            {
                pbControl.disabled = true;
                MessagePanelControl.clearMessagePanel('ctl00_shortMessagePanel_messagePanel');
                MessagePanelControl.clearMessagePanel('ctl00_messagePanel_messagePanel');
            }
        }
        catch(err)
        {
        }
    }

    function EndRequestHandler(sender, args) {
        try
        {
            if(pbControl.id.indexOf('saveButton') > -1)
            {
                pbControl.disabled = false;
            }
            pbControl = null;
        }
        catch(err)
        {
        }
    }

 
    </script>

<asp:Panel ID="Panel1" runat="server" Width="100%">
    <div style="display: none">
        <asp:Label ID="crossHrCodeLabel" runat="server" />
        <asp:TextBox ID="crossHireTextBox" runat="server" />
        <asp:TextBox ID="odoMeterTextBox" runat="server" />
        <asp:TextBox ID="odoMeterDueTextBox" runat="server" />
    </div>

    <table width="100%">
        <tr>
            <td width="402px"><b>Vehicle Exchange:</b></td>
            <td width="150px">Status:</td>
            <td>
                <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" /></td>
        </tr>
    </table>

    <hr />

    <asp:Panel ID="exchangeDetailPanel" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td colspan="2"><b>Exchange Details:</b></td>
            </tr>
            <tr>
                <td width="150">Exchange Type:</td>
                <td width="250">
                    <asp:DropDownList ID="exchangeTypeDropDownList" runat="Server" AppendDataBoundItems="true" Width="200px">
                        <asp:ListItem Text="Accident" Value="Accident"></asp:ListItem>
                        <asp:ListItem Text="Breakdown" Value="Breakdown"></asp:ListItem>
                        <asp:ListItem Text="Customer Request" Value="Customer Request"></asp:ListItem>
                    </asp:DropDownList>&nbsp;*
                </td>
                <td width="150">Branch:</td>
                <td>
                    <uc1:PickerControl 
                        ID="branchPickerControl" 
                        runat="server" 
                        AppendDescription="false"
                        PopupType="LOCATIONFORCOUNTRY" 
                        Width="50" 
                        Nullable="false" />
                    &nbsp;*
                </td>
            </tr>
            <tr>
                <td>Date:</td>
                <td colspan="3">
                    <uc1:DateControl ID="currDateControl" runat="server" />
                    <uc1:TimeControl ID="currTimeControl" runat="server" />
                    &nbsp;*
                </td>
            </tr>
        </table>
    </asp:Panel>

    <br />
    
    <asp:Panel ID="vehiclePanel" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td colspan="4"><b>Current Vehicle:</b></td>
            </tr>
            <tr>
                <td width="150">Unit No.:</td>
                <td width="250">
                    <asp:TextBox ID="unitNumTextBox" runat="server" ReadOnly="true" />
                </td>
                <td width="150">Reg:</td>
                <td>
                    <asp:TextBox ID="regTextBox" runat="server" ReadOnly="true" />
                </td>
            </tr>
            <tr>
                <td>Vehicle:</td>
                <td>
                    <asp:TextBox ID="vehicleTextBox" runat="server" ReadOnly="true" Width="220" />
                </td>
                <td>Odometer Out:</td>
                <td>
                    <asp:TextBox ID="odoOutTextBox" runat="server" ReadOnly="true" style="text-align: right" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>Odometer In:</td>
                <td>
                    <asp:TextBox 
                        ID="odoInTextBox" 
                        runat="server" 
                        Text='<%# Bind("OdoIn") %>' 
                        style="text-align: right" 
                        onkeypress="keyStrokeInt();" MaxLength="10" />
                    &nbsp;*
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    
    <asp:Panel ID="vehicleOtherPanel" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td colspan="2"><b>Current Vehicle:</b></td>
            </tr>
            <tr>
                <td width="150">Vehicle: </td>
                <td>
                    <asp:TextBox 
                        ID="vehicleOtherTextBox" 
                        runat="server" 
                        ReadOnly="true" 
                        Text='<%# Bind("Vehicle") %>' 
                        Width="300" MaxLength="64" />
                </td>
            </tr>
        </table>
    </asp:Panel>

    <br />
    
    <asp:Panel ID="replacementPanel" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td colspan="4"><b>Replacement Vehicle:</b></td>
            </tr>
            <tr>
                <td width="150">Unit No.:</td>
                <td width="250">
                    <asp:TextBox 
                        ID="reUnitTextBox" 
                        runat="server" 
                        MaxLength="18" 
                        AutoPostBack="true" />
                    &nbsp;*
                </td>
                <td width="150">Reg:</td>
                <td>
                    <asp:TextBox 
                        ID="reRegTextBox" 
                        runat="server" 
                        MaxLength="18" 
                        AutoPostBack="true" />
                    &nbsp;*
                </td>
            </tr>
            <tr>
                <td>Vehicle:</td>
                <td>
                    <asp:TextBox 
                        ID="reVehicleTextBox" 
                        runat="server" 
                        ReadOnly="true" 
                        Width="200" />
                </td>
                <td>Odometer Out:</td>
                <td>
                    <asp:TextBox 
                        ID="reOdoOutTextBox" 
                        runat="server" 
                        MaxLength="18" 
                        style="text-align: right"
                        onkeypress="keyStrokeInt();" />
                    &nbsp;*
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <asp:Panel ID="replacementOtherPanel" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td colspan="2"><b>Replacement Vehicle:</b></td>
            </tr>
            <tr>
                <td width="150">Type:</td>
                <td>
                    <asp:DropDownList ID="reTypeDropDownList" runat="server" AppendDataBoundItems="true" AutoPostBack="true" Width="200px">
                        <asp:ListItem Text="" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;*
                </td>
            </tr>
            <tr>
                <td>Vehicle:</td>
                <td>
                    <uc1:PickerControl 
                        ID="reVehiclePickerControl" 
                        runat="server" 
                        AppendDescription="true"
                        PopupType="PRODUCT4EXCHANGE" 
                        Width="300" 
                        Nullable="false" />
                    &nbsp;*
                </td>
            </tr>
        </table>
    </asp:Panel>
    
    <br />
    
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button 
                    ID="saveButton" 
                    runat="server" 
                    Text="Save" 
                    CssClass="Button_Standard Button_Save"  OnClientClick="return validate();"/>
                <asp:Button 
                    ID="cancelButton" 
                    runat="server" 
                    Text="Cancel" 
                    CssClass="Button_Standard Button_Reset" />
                <asp:Button 
                    ID="printButton" 
                    runat="server" 
                    Text="Print Contract" 
                    CssClass="Button_Standard Button_Print" />
            </td>
        </tr>
    </table>
    
    <uc1:ConfimationBoxControl 
        ID="confimationBoxControl" 
        runat="server" 
        LeftButton_Text="Yes"
        RightButton_Text="No" 
        LeftButton_AutoPostBack="true" 
        RightButton_AutoPostBack="false"
        Title="Booking Exchange" />
        
    <uc1:SupervisorOverrideControl ID="supervisorOverrideControl" runat="server" />
    
    <uc1:AddNoteRatePopupUserControl ID="addNoteRatePopupUserControl1" runat="server" />
    
</asp:Panel>
