<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingPaymentUserControl.ascx.vb" Inherits="Booking_Controls_BookingPaymentUserControl" %>
    
<%@ Register Src="../../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx" TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<asp:UpdatePanel ID="updatePanelForPayment" runat="server">
    <ContentTemplate>
         <asp:Panel ID="Panel1" runat="server" Width="100%">
            <table width="100%">
                <tr>
                    <td align="right">
                        <asp:CheckBox runat="server" ID="chkShowallrentals" AutoPostBack="true" Text="Show All Rentals" />
                    </td>
                </tr>
            </table>

            <asp:Panel ID="pnlBookingPayment" runat="server" Width="765" ScrollBars="Auto">
                <table width="100%">
                    <tr>
                        <td>
                            <img src="../../Images/informat.gif"  runat="server" id="imgInfo" visible="false"/>
                            <asp:Label 
                                ID="lblRefund" 
                                Text="REFUND IS DUE TO CUSTOMER" 
                                runat="server" 
                                Visible="false" 
                                Font-Bold="true" ForeColor="Red" />
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td width="330" align="Left" valign="top">
                            <b>Payment Amounts Outstanding:</b> 
                            <asp:Label 
                                ID="lblAmountOutstanding" 
                                Text="(..No amount outstanding)" 
                                Visible="false" 
                                runat="server" 
                                Font-Italic="true" />
    
                            <br />
                            <asp:Panel ID="pnlAmountOutstandingHeader" runat="server" Width="100%" Visible="false">
                                <table width="100%" class="dataTableGrid">
                                    <thead>
                                        <tr>
                                            <th width="35" height="45">Rent</th>
                                            <th width="35">Type</th>
                                            <th width="40">Curr</th>
                                            <th width="50">Total Owing</th>
                                            <th width="50">Total Paid</th>
                                            <th width="70">Balance Due</th>
                                            <th width="50">Cust To Pay</th>
                                            <th width="30" style="text-align: center">Check
                                                <br />
                                                <asp:CheckBox ID="chkAllTest" runat="server" AutoPostBack="false" Enabled="false" />
                                            </th>
                                        </tr>
                                    </thead>
                                    
                                    <tr class="evenRow">
                                        <td width="35" align="center"></td>
                                        <td width="35" align="center"></td>
                                        <td width="40" align="center"></td>
                                        <td width="50" align="center"></td>
                                        <td width="50" align="center"></td>
                                        <td width="70" align="center"></td>
                                        <td width="40" align="center">
                                            <asp:TextBox 
                                                ID="txtCustToPayHeader" 
                                                runat="server" 
                                                Width="40" 
                                                Text="0.00" 
                                                OnTextChanged="txtCustToPay_TextChanged" 
                                                Enabled="false" />
                                            
                                        </td>
                                        <td width="30" align="center">
                                            <asp:CheckBox 
                                                ID="chkRentalHeader" 
                                                runat="server" 
                                                AutoPostBack="true" 
                                                OnCheckedChanged="chkRental_CheckedChanged"  
                                                Enabled="false" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        
                            <asp:Panel ID="pnlAmountOutstanding" runat="server" Width="100%">
                                <asp:Repeater ID="repAmountOutstanding" runat="server">
                                    <HeaderTemplate>
                                        <table valign="top" width="100%" class="dataTableGrid">
                                            <thead>
                                                <tr align="center">
                                                    <th width="35" height="45">Rent</th>
                                                    <th width="35">Type</th>
                                                    <th width="40">Curr</th>
                                                    <th width="50">Total Owing</th>
                                                    <th width="50">Total Paid</th>
                                                    <th width="70">Balance Due</th>
                                                    <th width="40">Cust To Pay</th>
                                                    <th width="30" style="text-align: center">Check<br />
                                                        <asp:CheckBox 
                                                            ID="chkAll" 
                                                            runat="server" 
                                                            AutoPostBack="true" 
                                                            OnCheckedChanged="chkAll_CheckedChanged" />
                                                    </th>
                                                </tr>
                                            </thead>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                            <tr class="evenRow" align="center">
                                                <td width="35"><%#Eval("Rntl")%></td>
                                                <td width="35"><%#Eval("Type")%></td>
                                                <td width="40"><%#Eval("Curr")%></td>
                                                <td width="60" align="right"><%#Eval("TotalDue")%></td>
                                                <td width="50" align="right"><%#Eval("TotalPaid")%></td>
                                                <td width="70" align="right"><%#Eval("BlnDue")%></td>
                                                <td width="40">
                                                    <asp:TextBox 
                                                        ID="txtCustToPay" 
                                                        runat="server" 
                                                        Width="40" 
                                                        Text="0.00" 
                                                        OnTextChanged="txtCustToPay_TextChanged"
                                                        AutoPostBack="true" 
                                                        style="text-align: right" MaxLength="8" />
                                                    <ajaxToolkit:FilteredTextBoxExtender 
                                                        ID="FLT_txtCustToPay" 
                                                        runat="server" 
                                                        FilterType="Numbers,Custom"
                                                        TargetControlID="txtCustToPay" 
                                                        ValidChars=".-+" />
                                                </td>
                                                <td width="30" style="text-align: center">
                                                    <asp:CheckBox 
                                                        ID="chkRental" 
                                                        runat="server" 
                                                        AutoPostBack="true" 
                                                        OnCheckedChanged="chkRental_CheckedChanged" />
                                                    <asp:Label 
                                                        ID="lbltempID" 
                                                        runat="server" 
                                                        EnableViewState="true" 
                                                        Text='<%#Eval("idTemp") %>'
                                                        Visible="false" />
                                                </td>
                                            </tr>
                                    </ItemTemplate>
                                    <AlternatingItemTemplate>
                                            <tr class="oddRow" align="center">
                                                <td width="30"><%#Eval("Rntl")%></td>
                                                <td width="30"><%#Eval("Type")%></td>
                                                <td width="40"><%#Eval("Curr")%></td>
                                                <td width="60" align="right"><%#Eval("TotalDue")%></td>
                                                <td width="50" align="right"><%#Eval("TotalPaid")%></td>
                                                <td width="70" align="right"><%#Eval("BlnDue")%></td>
                                                <td width="40">
                                                    <asp:TextBox 
                                                        ID="txtCustToPay" 
                                                        runat="server" 
                                                        Width="40" 
                                                        Text="0.00" 
                                                        OnTextChanged="txtCustToPay_TextChanged"
                                                        AutoPostBack="true"  
                                                        style="text-align: right"  MaxLength="8"/>
                                                    <ajaxToolkit:FilteredTextBoxExtender 
                                                        ID="FLT_txtCustToPay" 
                                                        runat="server" 
                                                        FilterType="Numbers,Custom"
                                                        TargetControlID="txtCustToPay" 
                                                        ValidChars=".-+" />
                                                </td>
                                                <td width="30" style="text-align: center">
                                                    <asp:CheckBox 
                                                        ID="chkRental" 
                                                        runat="server" 
                                                        AutoPostBack="true" 
                                                        OnCheckedChanged="chkRental_CheckedChanged" />
                                                    <asp:Label 
                                                        ID="lbltempID" 
                                                        runat="server" 
                                                        EnableViewState="true" 
                                                        Text='<%#Eval("idTemp") %>'
                                                        Visible="false" />
                                                </td>
                                            </tr>
                                    </AlternatingItemTemplate>
                                    <FooterTemplate>
                                            <tr class="evenRow" >
                                                <td colspan="2" rowspan="2" width="60" >TOTALS by<br />Currency</td>
                                                <td width="40" align="center"><%=Total_Curr%></td>
                                                <td width="50" align="right"><%=Total_LTotDue%></td>
                                                <td width="50" align="right"><%=Total_LTotalPaidLo%></td>
                                                <td width="50" align="right"><%=Total_LBlnDueLoc%></td>
                                                <td width="50" align="right">
                                                    <b><font-size="100">
                                                    <%=Total_LCustToPay%>
                                                    </font-size></b>
                                                </td>
                                                <td width="30"><%=Wrapper%></td>
                                            </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </asp:Panel>
                            
                            <br />
                            
                            <asp:Panel ID="pnlMisc" runat="server" Width="100%">
                                <table width="100%">
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="right" >
                                            <asp:Button 
                                                ID="btnAddPayment" 
                                                runat="server" 
                                                Text="Add Payment" 
                                                CssClass="Button_Standard Button_Add" />
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td>
                                            <asp:Literal 
                                                ID="lblTravelMSG" 
                                                runat="server" Text="Rental Authorised to Travel?" />
                                            <asp:CheckBox 
                                                ID="chkAuToTr" 
                                                runat="server" 
                                                AutoPostBack="true" 
                                                Visible="false" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Literal 
                                                ID="litMod" 
                                                runat="server" />
                                            <br />
                                            <asp:Literal ID="litOutstanding" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        
                        </td>
                    
                        <td width="430" valign="top" align="Left">
                            <b>Payments Received:</b> 
                            <asp:Label 
                                ID="lblPaymentReceived" 
                                Text="(..No payment received)" 
                                Visible="false"
                                runat="server" 
                                Font-Italic="true" />
                            <br />
        
                            <asp:Panel ID="pnlPaymentReceivedHeader" runat="server" Width="430" >
                                <table valign="top" width="430" class="dataTableGrid">
                                    <thead>
                                        <tr>
                                            <th width="40" height="45">Date</th>
                                            <th width="40">Method</th>
                                            <th width="30">Curr</th>
                                            <th width="50">Total Pymt</th>
                                            <th width="30">Status</th>
                                            <th width="20">Rent</th>
                                            <th width="20">Type</th>
                                            <th width="30">Charge Curr</th>
                                            <th width="50">Amount Paid</th>
                                        </tr>
                                    </thead>
                                    
                                    <tr class="evenRow" width="100%">
                                        <td width="100%" colspan="9">
                                            <asp:TextBox 
                                                ID="txtDateHeader" 
                                                runat="server" 
                                                Enabled="false" 
                                                Width="420" />
                                        </td>
                                    </tr>
                                </table>
                        </asp:Panel>
                        
                        <asp:Panel ID="pnlPaymentReceived" runat="server" Width="430">
                            <asp:Repeater ID="repPaymentReceived" runat="server">
                                <HeaderTemplate>
                                    <table valign="top" width="430" class="dataTableGrid">
                                        <thead>
                                            <tr>
                                                <th width="40" height="45">Date</th>
                                                <th width="120">Method</th>
                                                <th width="30">Curr</th>
                                                <th width="70">Total Pymt</th>
                                                <th width="30">Status</th>
                                                <th width="20">Rent</th>
                                                <th width="20">Type</th>
                                                <th width="30">Charge Curr</th>
                                                <th width="60">Amount Paid</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                        <tr runat="server" id="trEvenRow">
                                            <td width="40"><%# Eval("Date")%></td>
                                            <td width="120" align="left">
                                                <asp:LinkButton 
                                                    ID="lnkMethod" 
                                                    runat="Server" 
                                                    Text='<%# Eval("Method")%>' 
                                                    CommandName="VisaImprint"
                                                    CommandArgument='<%#eval("idTemp") %>' />
                                                <asp:Literal 
                                                    ID="litGotoPayment" 
                                                    Text="" 
                                                    runat="server" 
                                                    EnableViewState="true" 
                                                    Visible="false" />
                                            </td>
                                            <td width="30"><%#Eval("Curr")%></td>
                                            <td width="70" align="right" ><%#Eval("TotPayment")%></td>
                                            <td width="30"><%#Eval("Status")%></td>
                                            <td width="20"><%#Eval("Rntl")%></td>
                                            <td width="20"><%#Eval("Type")%></td>
                                            <td width="30"><%#Eval("PymtCurr")%></td>
                                            <td width="60" align="right" ><%#Eval("PymtLoc")%></td>
                                        </tr>
                                </ItemTemplate>
                                <AlternatingItemTemplate>
                                        <tr runat="server" id="trOddRow">
                                            <td width="40"><%# Eval("Date")%></td>
                                            <td width="120" align="left">
                                                <asp:LinkButton 
                                                    ID="lnkMethod" 
                                                    runat="Server" 
                                                    Text='<%# Eval("Method")%>' 
                                                    CommandName="VisaImprint"
                                                    CommandArgument='<%#eval("idTemp") %>' />
                                                <asp:Literal 
                                                    ID="litGotoPayment" 
                                                    Text="" 
                                                    runat="server" 
                                                    EnableViewState="true" 
                                                    Visible="false" />
                                            </td>
                                            <td width="30"><%#Eval("Curr")%></td>
                                            <td width="70" align="right" ><%#Eval("TotPayment")%></td>
                                            <td width="30"><%#Eval("Status")%></td>
                                            <td width="20"><%#Eval("Rntl")%></td>
                                            <td width="20"><%#Eval("Type")%></td>
                                            <td width="30"><%#Eval("PymtCurr")%></td>
                                            <td width="60" align="right"><%#Eval("PymtLoc")%></td>
                                        </tr>
                                </AlternatingItemTemplate>
                                <FooterTemplate>
                                        <tr class="evenRow">
                                            <td colspan="7" align="right"><b>Net Total (Excl Bond)</b></td>
                                            <td width="30" align="center"><%=Footer_TotalPaymtCurrCode%></td>
                                            <td width="50" align="right"><b><%=Footer_TotalPaymtTot%></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="right" >PLUS Bond Paid</td>
                                            <td width="30" align="center"><%=Footer_PlBPaidPaymtCurrCode%></td>
                                            <td width="50" align="right" ><%=Footer_PlBPaidPaymtTot%></td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="right"><b>Total Payments Made</b></td>
                                            <td width="30" align="center"><%=Footer_TotalPaymentCurrCode%></td>
                                            <td width="50" align="right"><b><%= Footer_TotalPaymentAmt  %></b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="7" align="right">Less Bond Refunded</td>
                                            <td width="30" align="center"><%=Footer_LessBndRefdPaymtCurrCode%></td>
                                            <td width="50" align="right"><%=Footer_LessBndRefdPaymtTot%></td>
                                        </tr>
                                    </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </asp:Panel>
                        
                    </td>
                </tr>
            </table>

            <asp:Literal ID="litXPayment" runat="server" EnableViewState="true" Visible="false" />
            <asp:Literal ID="litXOutstandingPayment" runat="server" EnableViewState="true" Visible="false" />
            <asp:Literal ID="LitJS" runat="server" EnableViewState="true" Visible="false" />

        </asp:Panel>

        <uc1:SupervisorOverrideControl ID="SupervisorOverrideControl1" runat="server"  ContinueButtonVisible="true" ContinueButtonText="Reverse Payment Only" ContinueButtonWidth="200"  />
    
    </asp:Panel>
    </ContentTemplate>
         
</asp:UpdatePanel>


