﻿''Imports Aurora.Booking.Services
Imports Aurora.BookingExperiences.Service
Imports System.Data
Imports System.Data.Sql


Partial Class Booking_Controls_BookingExperienceUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False
    Dim ds As New DataSet

    Public Function CallBackDate(_callbackdate As String) As String
        If String.IsNullOrEmpty(_callbackdate) Then
            Return "N/A"
        End If
        Dim _datetime As DateTime = Convert.ToDateTime(_callbackdate)
        If (_datetime.Equals(New Date(1900, 1, 1))) Then
            Return "N/A"
        End If

        Return String.Format("{0:d/MM/yyyy HH:mm}", CDate(_callbackdate))
    End Function

    Public Sub LoadPage()

        Try
            If isFirstLoad Then
                ds = BookingExperiences.CallTicketsSummary(RentalId)
                If (ds.Tables.Count - 1 <> -1) Then
                    bookingExperienceGridView.DataSource = ds.Tables(0)
                    bookingExperienceGridView.DataBind()
                End If

            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking Experience tab.")
        End Try
        ''todo
    End Sub

    Protected Sub bookingExperienceGridView_RowCreated(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles bookingExperienceGridView.RowCreated

    End Sub

    Protected Sub bookingExperienceGridView_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles bookingExperienceGridView.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Const tdWidth As String = "80"
            Dim trData As HtmlTableRow = CType(e.Row.FindControl("trData"), HtmlTableRow)
            Dim tdData As HtmlTableCell
            Dim trSpace As New HtmlTableRow
            Dim tdSpace As New HtmlTableCell
            tdSpace.Height = "15"
            tdSpace.InnerHtml = "&nbsp;"
            tdSpace.ColSpan = 10
            trSpace.Controls.Add(tdSpace)

            ''links
            Dim linkView As HtmlAnchor = CType(e.Row.FindControl("viewEditLink"), HtmlAnchor)


            If (Not trData Is Nothing) Then
                tdData = CType(e.Row.FindControl("tdData"), HtmlTableCell)

                Dim table As HtmlTable = New HtmlTable
                table.Attributes.Add("class", "dataTableGrid")
                Dim tablerow As HtmlTableRow


                Dim tdCellDateAdded As HtmlTableCell
                Dim tdCellAgent As HtmlTableCell
                Dim tdCellSupplier As HtmlTableCell
                Dim tdCellExperience As HtmlTableCell
                Dim tdCellTicket As HtmlTableCell
                Dim tdCellPrice As HtmlTableCell
                Dim tdCellTotal As HtmlTableCell
                Dim tdCellTicketDate As HtmlTableCell
                Dim tdCellStatus As HtmlTableCell
                Dim tdCellVoucher As HtmlTableCell


                Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
                Dim rows() As DataRow = ds.Tables(1).Select("HeaderId='" & CInt(rowview("Id").ToString()) & "'")
                If Not rows Is Nothing Then
                    Dim trHeader As HtmlTableRow = CType(e.Row.FindControl("trHeader"), HtmlTableRow)

                    If (Not linkView Is Nothing) Then
                        linkView.Attributes.Add("name", rowview("Id").ToString())
                        linkView.HRef = "~/Booking/BookingExperiences.aspx?Mode=Edit&hdRentalId=" & RentalId & "&hdBookingId=" & BookingId & "&expCallId=" & rowview("Id").ToString()
                    End If

                    If (Not trHeader Is Nothing) Then
                        tdCellDateAdded = New HtmlTableCell
                        tdCellDateAdded.Width = "90"
                        tdCellDateAdded.InnerHtml = "<b>Date Added</b>"
                        trHeader.Controls.Add(tdCellDateAdded)

                        tdCellAgent = New HtmlTableCell
                        tdCellAgent.Width = "40"
                        tdCellAgent.InnerHtml = "<b>Agent</b>"
                        trHeader.Controls.Add(tdCellAgent)

                        tdCellSupplier = New HtmlTableCell
                        tdCellSupplier.Width = "150"
                        tdCellSupplier.InnerHtml = "<b>Supplier</b>"
                        trHeader.Controls.Add(tdCellSupplier)

                        tdCellExperience = New HtmlTableCell
                        tdCellExperience.Width = "150"
                        tdCellExperience.InnerHtml = "<b>Experience</b>"
                        trHeader.Controls.Add(tdCellExperience)

                        tdCellTicket = New HtmlTableCell
                        tdCellTicket.Width = "150"
                        tdCellTicket.InnerHtml = "<b>Ticket</b>"
                        trHeader.Controls.Add(tdCellTicket)

                        tdCellPrice = New HtmlTableCell
                        tdCellPrice.Width = "50"
                        tdCellPrice.InnerHtml = "<b>Price</b>"
                        trHeader.Controls.Add(tdCellPrice)

                        tdCellTotal = New HtmlTableCell
                        tdCellTotal.Width = "50"
                        tdCellTotal.InnerHtml = "<b>Total</b>"
                        trHeader.Controls.Add(tdCellTotal)

                        tdCellTicketDate = New HtmlTableCell
                        tdCellTicketDate.Width = tdWidth
                        tdCellTicketDate.InnerHtml = "<b>Ticket Date</b>"
                        trHeader.Controls.Add(tdCellTicketDate)

                        tdCellStatus = New HtmlTableCell
                        tdCellStatus.Width = "80"
                        tdCellStatus.InnerHtml = "<b>Status</b>"
                        trHeader.Controls.Add(tdCellStatus)

                        tdCellVoucher = New HtmlTableCell
                        tdCellVoucher.Width = tdWidth
                        tdCellVoucher.InnerHtml = "<b>Voucher</b>"
                        trHeader.Controls.Add(tdCellVoucher)

                        table.Rows.Add(trHeader)
                    End If


                    For Each row As DataRow In rows
                        tablerow = New HtmlTableRow
                        ''tablerow.Attributes.Add("border-bottom", "solid red 1px")

                        tdCellDateAdded = New HtmlTableCell
                        tdCellAgent = New HtmlTableCell
                        tdCellSupplier = New HtmlTableCell
                        tdCellExperience = New HtmlTableCell
                        tdCellTicket = New HtmlTableCell
                        tdCellPrice = New HtmlTableCell
                        tdCellTotal = New HtmlTableCell
                        tdCellTicketDate = New HtmlTableCell
                        tdCellStatus = New HtmlTableCell
                        tdCellVoucher = New HtmlTableCell

                        tdCellDateAdded.InnerText = String.Format("{0:d/M/yyyy HH:mm}", Convert.ToDateTime(row("DateAdded").ToString()))
                        tablerow.Controls.Add(tdCellDateAdded)

                        tdCellAgent.InnerText = row("Agent").ToString
                        tablerow.Controls.Add(tdCellAgent)

                        tdCellSupplier.InnerText = row("Supplier").ToString
                        tablerow.Controls.Add(tdCellSupplier)

                        tdCellExperience.InnerText = row("Experience").ToString
                        tablerow.Controls.Add(tdCellExperience)

                        tdCellTicket.InnerText = row("Ticket").ToString
                        tablerow.Controls.Add(tdCellTicket)

                        tdCellPrice.InnerText = String.Format("{0:c2}", Convert.ToDecimal(row("Price").ToString())) ''row("Price").ToString
                        tablerow.Controls.Add(tdCellPrice)

                        tdCellTotal.InnerText = String.Format("{0:c2}", Convert.ToDecimal(row("Total").ToString())) ''("Total").ToString
                        tablerow.Controls.Add(tdCellTotal)

                        If (IsDBNull(row("TicketDate"))) Then
                            tdCellTicketDate.InnerText = ""
                        Else
                            tdCellTicketDate.InnerText = String.Format("{0:d/M/yyyy HH:mm}", Convert.ToDateTime(row("TicketDate").ToString())) ''row("TicketDate").ToString
                        End If
                        tablerow.Controls.Add(tdCellTicketDate)

                        tdCellStatus.InnerText = row("Status").ToString
                        tablerow.Controls.Add(tdCellStatus)

                        tdCellVoucher.InnerText = row("Voucher").ToString
                        tablerow.Controls.Add(tdCellVoucher)

                        If (tdCellStatus.InnerText.Contains("Confirmed") = True) Then
                            tablerow.Attributes.Add("bgcolor", "#DDDDFF")
                            table.Rows.Add(tablerow)
                        Else
                            table.Rows.Add(tablerow)
                        End If

                    Next
                    tdData.Controls.Add(table)
                    table.Rows.Add(trSpace)

                End If
            End If
        End If
    End Sub

    Protected Sub addBtn_Click(sender As Object, e As System.EventArgs) Handles addBtn.Click
        Response.Redirect("BookingExperiences.aspx?Mode=New&hdRentalId=" & RentalId & "&hdBookingId=" & BookingId)
    End Sub

    Sub TestData()
        Dim dtHeader As New DataTable
        dtHeader.Columns.Add("Id", GetType(Integer))
        dtHeader.Columns.Add("Status", GetType(String))
        dtHeader.Columns.Add("DateAdded", GetType(String))
        dtHeader.Columns.Add("CallBackDate", GetType(String))
        dtHeader.Columns.Add("Agent", GetType(String))

        dtHeader.Rows.Add(1, "Decline", DateTime.Now.ToString("MM/dd/yyyy"), DateTime.Now.AddDays(10).ToString("MM/dd/yyyy"), "Agent1")
        dtHeader.Rows.Add(2, "Completed", DateTime.Now.AddDays(10).ToString("MM/dd/yyyy"), DateTime.Now.AddDays(20).ToString("MM/dd/yyyy"), "Agent1")
        dtHeader.TableName = "headerTable"
        ds.Tables.Add(dtHeader)

        Dim dt As New DataTable


        dt.Columns.Add("Id", GetType(Integer))
        dt.Columns.Add("DateAdded", GetType(String))
        dt.Columns.Add("Agent", GetType(String))
        dt.Columns.Add("Supplier", GetType(String))
        dt.Columns.Add("Experience", GetType(String))
        dt.Columns.Add("Ticket", GetType(String))
        dt.Columns.Add("Price", GetType(String))
        dt.Columns.Add("Total", GetType(String))
        dt.Columns.Add("TicketDate", GetType(String))
        dt.Columns.Add("Status", GetType(String))
        dt.Columns.Add("Voucher", GetType(String))
        dt.Columns.Add("HeaderId", GetType(Integer))


        dt.Rows.Add(1, DateTime.Now.ToString("MM/dd/yyyy"), "Agent1", "Supplier1", "Experience#1", "Ticket#1", "$1.00", "$1.00", DateTime.Now.AddDays(10).ToString("MM/dd/yyyy"), "Status1", "xxxx-xxx1", 1)


        dt.Rows.Add(2, DateTime.Now.ToString("MM/dd/yyyy"), "Agent2", "Supplier2", "Experience#2", "Ticket#2", "$1.00", "$1.00", DateTime.Now.AddDays(10).ToString("MM/dd/yyyy"), "Status2", "xxxx-xxx2", 2)
        dt.Rows.Add(3, DateTime.Now.ToString("MM/dd/yyyy"), "Agent2", "Supplier2", "Experience#2", "Ticket#2", "$2.00", "$2.00", DateTime.Now.AddDays(10).ToString("MM/dd/yyyy"), "Status2", "xxxx-xxx2", 2)
        dt.TableName = "dataTable"
        ds.Tables.Add(dt)

        bookingExperienceGridView.DataSource = ds.Tables("headerTable")
        bookingExperienceGridView.DataBind()
    End Sub

End Class
