<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingInfringementUserControl.ascx.vb" Inherits="Booking_Controls_BookingInfringementUserControl" %>

<%@ Register Src="~/Booking/Controls/InfringementManagementPopupUserControl.ascx" TagName="InfringementManagementPopupUserControl" TagPrefix="uc1" %>
    
<table width="100%">
    <tr>
        <td><b>Infringement Summary:</b></td>
        <td align="right">
            <asp:CheckBox 
                ID="showAllRentalCheckBox" 
                runat="server" 
                Text="Show All Rentals" 
                AutoPostBack="true" />
        </td>
    </tr>
</table>

<asp:GridView 
    ID="infringementGridView" 
    runat="server" 
    Width="100%" 
    GridLines="None"
    CssClass="dataTable"
    AutoGenerateColumns="false">
    <AlternatingRowStyle CssClass="oddRow" HorizontalAlign="right" />
    <RowStyle CssClass="evenRow" HorizontalAlign="right" />
    <Columns>
        <asp:TemplateField ItemStyle-HorizontalAlign="left">
            <HeaderTemplate>Offence Date/Time</HeaderTemplate>
            <ItemTemplate>
                <asp:LinkButton 
                    ID="updateLinkButton" 
                    runat="server" 
                    CommandName="update" 
                    CommandArgument='<%# Eval("IcdId") %>' 
                    Text='<%# GetDateFormat(eval("OffenceDtTm")) %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="Cause" HeaderText="Cause" ItemStyle-HorizontalAlign="left" />
        <asp:BoundField DataField="UnitNo" HeaderText="Unit No / Rego" ItemStyle-HorizontalAlign="left" />
        <asp:BoundField DataField="Cadvised" HeaderText="Customer Advised" ItemStyle-HorizontalAlign="left" />
        <asp:BoundField DataField="Status" HeaderText="Status" ItemStyle-HorizontalAlign="left" />
        <asp:BoundField DataField="RntNum" HeaderText="Rental" ItemStyle-HorizontalAlign="left" />
    </Columns>
</asp:GridView>

<br />

<table width="100%">
    <tr>
        <td align="right">
            <asp:Button ID="addButton" runat="server" Text="Add Infringement" CssClass="Button_Standard Button_Add" />
        </td>
    </tr>
</table>

<uc1:InfringementManagementPopupUserControl ID="infringementManagementPopupUserControl1" runat="server" />
