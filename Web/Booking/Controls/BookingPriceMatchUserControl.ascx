﻿<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingPriceMatchUserControl.ascx.vb"
    Inherits="Booking_Controls_PriceMatchUserControl" %>
<style>
    .rightAlign
    {
        text-align: right;
    }
    
    #divPriceMatchLoad table tr th
    {
        font-size:xx-small;
    }
    
    #divPriceMatchLoad table tr td
    {
        font-size:xx-small;
    }
</style>
<h3>
    Price Variation Request
</h3>
<div>
    <table style="width: 100%; border-collapse: collapse;" id="tableMatchPriceHeader"
        class="dataTableGrid" cellspacing="0">
        <thead>
            <tr>
                <th scope="col">
                    Rental
                </th>
                <th scope="col">
                    Check-out
                </th>
                <th scope="col">
                    Check-in
                </th>
                <th scope="col">
                    Hire Pd
                </th>
                <th scope="col">
                    Package
                </th>
                <th scope="col">
                    Brand
                </th>
                <th scope="col">
                    Vehicle
                </th>
                <%-- <th scope="col">
                    Status
                </th>
                <th scope="col">
                    &nbsp;
                </th>--%>
            </tr>
        </thead>
        <tr  class="evenRow" id="trHeader" runat="server">
            <td style="width: 30px;">
                <asp:Label ID= "PMRentalIdLabel" runat="server" />
            </td>
            <td>
                <%=PMCheckout%>
            </td>
            <td>
                <%=PMCheckin%>
            </td>
            <td>
                <%=PMHirePeriod%>
            </td>
            <td>
                <%=PMPackage%>
            </td>
            <td>
                <%=PMBrand%>
            </td>
            <td>
                <%=PMVehicle%>
            </td>
            <%-- <td>
                <%=PMStatus%>
            </td>--%>
        </tr>
    </table>
</div>
<br />

<div>
    <table style="width: 100%; border-collapse: collapse;" id="table1" class="dataTableGrid"
        cellspacing="0">
        <thead>
            <tr>
                <th scope="col">
                    Request Type
                </th>
                <th scope="col">
                    Competitor Name
                </th>
                <th scope="col">
                    Matching Status
                </th>
               
            </tr>
        </thead>
        <tr style="background-color: rgb(255, 255, 255);" class="evenRow" >
            <td style="width: 110px;"  valign="top" >
                <asp:DropDownList ID="ddlPMrequestType" runat="server" Width="220" class="PriceMatchingList">
                </asp:DropDownList>
            </td>
            <td style="width: 110px;" valign="top">
                <asp:DropDownList ID="ddlPMCompetitorName" runat="server" Width="220" class="PriceMatchingList">
                </asp:DropDownList>
            </td>
            <td style="width: 110px;" valign="top">
                <asp:DropDownList ID="ddlPMmatchingStatus" runat="server" Width="220" class="PriceMatchingList">
                </asp:DropDownList>
            </td>
           
        </tr>
    </table>
</div>
<br />
<b>All rates in the below table are Gross of Commission, inclusive of GST</b>

<div id="divPriceMatchLoad"  style="height:270px; overflow-y:auto;">
</div>
<br />
<div align="right">
    Approved  By:  <asp:TextBox ID="decisionByTextBox" runat="server" Width="100" class="PriceMatchingTextWhenApproved" MaxLength="10"></asp:TextBox>
    Requested By:  <asp:TextBox ID="requestedByTextBox" runat="server" Width="100" class="PriceMatchingText" MaxLength="10"></asp:TextBox>
</div>

<br />
<div align="right">
    <input type="button" class="Button_Standard Button_Cancel" style="width: 100px;" value="Cancel"
        id="PriceMatchingCancelButtonButton" />
    <input type="button" class="Button_Standard Button_Save" style="width: 100px;" value="Save"
        id="PriceMatchingSaveButton" />
</div>
