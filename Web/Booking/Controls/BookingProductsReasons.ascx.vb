
Partial Class Booking_Controls_BookingProductsReasons
    Inherits BookingUserControl

    Private _NoOfLabelAndReasonsToDisplay As Integer
    Public Property NoOfLabelAndReasonsToDisplay() As Integer
        Get
            Return CInt(ViewState("NoOfLabelAndReasonsToDisplay"))
        End Get
        Set(ByVal value As Integer)
            ViewState("NoOfLabelAndReasonsToDisplay") = value
        End Set
    End Property

    ''this should be in this format eg: dvdport|<reason>,wineglw|<reason>
    Private _ProductAndReasonValues As String
    Public Property ProductAndReasonValues() As String
        Get
            Return CStr(ViewState("ProductAndReasonValues"))
        End Get
        Set(ByVal value As String)
            ViewState("ProductAndReasonValues") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim label As Label = Nothing
        Dim txtbox As TextBox = Nothing

        If Not String.IsNullOrEmpty(ProductAndReasonValues) AndAlso NoOfLabelAndReasonsToDisplay > 0 Then
            Dim aryProductAndReasonValue() As String = ProductAndReasonValues.Split(",")

            For i As Integer = 0 To NoOfLabelAndReasonsToDisplay - 1

                Dim labelID As String = "lbl" & aryProductAndReasonValue(i).Substring(0, aryProductAndReasonValue(i).IndexOf("|"))
                Dim textID As String = "txt" & aryProductAndReasonValue(i).Substring(0, aryProductAndReasonValue(i).IndexOf("|"))
                Dim labelName As String = aryProductAndReasonValue(i).Substring(0, aryProductAndReasonValue(i).IndexOf("|"))

                label = New Label
                label.ID = labelID
                label.Text = labelName
                label.Visible = True

                txtbox = New TextBox
                txtbox.ID = textID
                txtbox.Visible = True
                txtbox.TextMode = TextBoxMode.MultiLine
                txtbox.Height = Unit.Pixel(50)
                txtbox.Width = Unit.Percentage(100)

                txtbox.Attributes.Add("onkeyup", "JSwarning(this);")
                AddHandler txtbox.TextChanged, AddressOf TextChanged

                ''add label
                Panel1.Controls.Add(New LiteralControl((i + 1) & ".)"))
                Panel1.Controls.Add(New LiteralControl("<b>"))
                Panel1.Controls.Add(label)
                Panel1.Controls.Add(New LiteralControl("</b>"))
                Panel1.Controls.Add(New LiteralControl("<br/>"))

                ''add textbox
                Panel1.Controls.Add(txtbox)
                Panel1.Controls.Add(New LiteralControl("<br/>"))


            Next

        End If
        
    End Sub

    Public Function SubmitReasonData(ByVal inputXmlstring As String) As String
        Dim outputxmlstring As String = ""
        Dim errormessage As String = ""

        If checkReasonData(inputXmlstring, _
            outputxmlstring, _
            errormessage) = True Then

            Return outputxmlstring
        Else
            Return "ERROR:" & errormessage
        End If
    End Function
    Private Function checkReasonData(ByVal inputXmlstring As String, _
        ByRef xmlString As String, ByRef MessageError As String) As Boolean
        Dim XMLaddRes As New System.Xml.XmlDocument
        XMLaddRes.LoadXml(inputXmlstring)

        Dim i As Integer = XMLaddRes.DocumentElement.ChildNodes.Count - 1
        For ii As Integer = 0 To i

            Dim Product As String = XMLaddRes.DocumentElement.ChildNodes(ii).SelectSingleNode("PrdShortName").InnerText

            Dim Reason As String = CType(Me.Panel1.FindControl("txt" & Product), TextBox).Text
            If Reason.Length > 999 Then
                Reason = Reason.Substring(0, 999)
            End If

            If (Reason = "") Then
                MessageError = ("Reason text for " & Product & " cannot be blank and should be more than one word!")
                StopProcessing = True
                Return False
            End If

            Dim reasons() As String = Reason.Split(" ")

            If reasons.Length = 1 Then
                MessageError = ("Please enter more than one word for " & Product & " reasons")
                StopProcessing = True
                Return False
            End If

            'If reasons(0).Length = 1 Then
            '    MessageError = ("Invalid text for " & Product & " reasons. Please re-enter more than one letter for your first word.")
            '    StopProcessing = True
            '    Return False
            'End If

            'If reasons(1).Length = 1 Then
            '    MessageError = ("Invalid text for " & Product & " reasons. Please re-enter more than one letter for your second word.")
            '    Return False
            'End If

            If reasons(0).Equals(reasons(1)) Then
                MessageError = ("Invalid text for " & Product & " - no more occurence of word is allowed.")
                StopProcessing = True
                Return False
            End If

            XMLaddRes.DocumentElement.ChildNodes(ii).SelectSingleNode("Reason").InnerText = Reason
        Next

        xmlString = XMLaddRes.OuterXml
        StopProcessing = False
        Return True

    End Function

    Sub TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txt As TextBox = CType(sender, TextBox)
        If txt.Text.TrimEnd.Length > 999 Then
            MyBase.CurrentPage.SetInformationShortMessage("Reason's limit is up to 1000 characters only!")
            ''txt.Text = txt.Text.Substring(0, 999)
            StopProcessing = True
        Else
            StopProcessing = False
        End If
    End Sub
    Public Property StopProcessing() As Boolean
        Get
            Return ViewState("StopProcessing")
        End Get
        Set(ByVal value As Boolean)
            ViewState("StopProcessing") = value
        End Set
    End Property

End Class
