
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_BookingSummaryUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking history tab.")
        End Try
    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                'Dim dt As DataTable
                'dt = BookingSummary.GetBookingummary(BookingId, "", True)

                'Dim dtNew As DataTable = New DataTable
                'Dim dr As DataRow

                '' Define the columns for the table.
                'dtNew.Columns.Add(New DataColumn("Title", GetType(String)))
                'dtNew.Columns.Add(New DataColumn("Customer", GetType(String)))
                'dtNew.Columns.Add(New DataColumn("Agent", GetType(String)))

                'Dim title() As String = {"", "", "Gross", "Agent Discount", "Total Owing", "Total Paid", "Total Due", "Deposit Owing", "Deposit Paid", "Deposit Due", "Security Owing", "Security Paid", "Security Due", "GST Content"}

                'For i As Integer = 0 To 13
                '    dr = dtNew.NewRow()
                '    dr(0) = title(i)
                '    dr(1) = dt.Rows(i)(1)
                '    dr(2) = dt.Rows(i)(2)
                '    dtNew.Rows.Add(dr)
                'Next

                'bookingSummaryGridView.DataSource = dtNew
                'bookingSummaryGridView.DataBind()
                ''bookingSummaryGridView.Columns.Item(2).Visible = False
                ''filterRadioButtonList.SelectedValue = "Customer"

                'If filterRadioButtonList.SelectedValue = "All" Then
                '    bookingSummaryGridView.Columns.Item(2).Visible = True
                'Else
                '    bookingSummaryGridView.Columns.Item(2).Visible = False
                'End If

                bookingSummaryGridViewDatabind()


            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking summary tab.")
        End Try
    End Sub

    Protected Sub filterRadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles filterRadioButtonList.SelectedIndexChanged
        Try
            bookingSummaryGridViewDatabind()
            'If filterRadioButtonList.SelectedValue = "All" Then
            '    bookingSummaryGridView.Columns.Item(2).Visible = True
            'Else
            '    bookingSummaryGridView.Columns.Item(2).Visible = False
            'End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while sorting the booking notes.")
        End Try
    End Sub

    Protected Sub showAllRentalCheckBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles showAllRentalCheckBox.CheckedChanged
        bookingSummaryGridViewDatabind()
    End Sub

    Private Sub bookingSummaryGridViewDatabind()
        Dim showAllRental As Boolean
        Dim showAgent As Boolean

        showAllRental = showAllRentalCheckBox.Checked
        If filterRadioButtonList.SelectedValue = "All" Then
            showAgent = True
        Else
            showAgent = False
        End If

        Dim dt As DataTable
        If showAllRental Then
            dt = BookingSummary.GetBookingummary(BookingId, "", showAgent)
        Else
            dt = BookingSummary.GetBookingummary(BookingId, RentalId, showAgent)
        End If


        For j As Integer = dt.Columns.Count To 4
            dt.Columns.Add(New DataColumn(j.ToString(), GetType(String)))
        Next j


        For i As Integer = 0 To dt.Columns.Count - 1
            dt.Columns(i).Caption = dt.Rows(0)(i.ToString()) & " (" & dt.Rows(1)(i.ToString()) & ")"
            bookingSummaryGridView.Columns(i).HeaderText = dt.Rows(0)(i.ToString()) & " " & dt.Rows(1)(i.ToString())
        Next i

        dt.Rows(1).Delete()
        dt.Rows(0).Delete()

        bookingSummaryGridView.DataSource = dt
        bookingSummaryGridView.DataBind()

    End Sub

End Class
