
Partial Class Booking_Controls_BookingProcessSteps
    Inherits BookingUserControl

#Region " Enum"
    Enum BookingProcess
        NewBooking = 0
        CheckDuplicate = 1
        DisplayAvailability = 2
        Summary = 3
    End Enum
#End Region

    Public Sub DisplayImageURL(ByVal proc As BookingProcess)
        Select Case proc
            Case BookingProcess.NewBooking
                img1.ImageUrl = "~/Images/wizard_left_on.gif"
                Img2.ImageUrl = "~/Images/Wizard_first_left_off.gif"
                Img3.ImageUrl = "~/Images/wizard_mid_off.gif"
                Img4.ImageUrl = "~/Images/wizard_right_off.gif"

            Case BookingProcess.CheckDuplicate
                img1.ImageUrl = "~/Images/wizard_left_on.gif"
                Img2.ImageUrl = "~/Images/Wizard_first_left_on.gif"
                Img3.ImageUrl = "~/Images/wizard_mid_off.gif"
                Img4.ImageUrl = "~/Images/wizard_right_off.gif"

            Case BookingProcess.DisplayAvailability
                img1.ImageUrl = "~/Images/wizard_left_on.gif"
                Img2.ImageUrl = "~/Images/Wizard_first_left_on.gif"
                Img3.ImageUrl = "~/Images/wizard_mid_on.gif"
                Img4.ImageUrl = "~/Images/wizard_right_off.gif"

            Case BookingProcess.Summary
                img1.ImageUrl = "~/Images/wizard_left_on.gif"
                Img2.ImageUrl = "~/Images/Wizard_first_left_on.gif"
                Img3.ImageUrl = "~/Images/wizard_mid_on.gif"
                Img4.ImageUrl = "~/Images/wizard_right_on.gif"
        End Select

    End Sub

    
End Class
