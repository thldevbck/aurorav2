'' Change Log 
'' 21.8.8 - Shoel - SQUISH #580  High Cancel Booking - status not updating on the Booking page after a Cancel Rental or Cancel Booking 
'' 22.8.8 - Shoel - Fix for squish # 582, Method moved to BookingUserControl.vb

Imports Aurora.Common
Imports System.Data
Imports Aurora.Booking.Services

Partial Class Booking_Controls_BookingCancellationUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False

    Public Const BookingCancellation_IntNo_ViewState As String = "BookingCancellation_IntNo"
    Public Const BookingCancellation_RentalStatus_ViewState As String = "BookingCancellation_RentalStatus"
    Public Const BookingCancellation_CityCode_ViewState As String = "BookingCancellation_CityCode"
    Public Const BookingCancellation_ExtnRef_ViewState As String = "BookingCancellation_ExtnRef"



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking cancellation tab.")
        End Try
    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                reasonDataBind()
                notificationDataBind()
                typeDataBind()
                formDataBind()
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking cancellation tab.")
        End Try
    End Sub

    Private Sub reasonDataBind()
        If reasonDropDownList.Items.Count = 1 Then
            Dim ds As DataSet
            ds = Data.GetCodecodetype("27", "")
            reasonDropDownList.DataSource = ds.Tables(0)
            reasonDropDownList.DataValueField = "ID"
            reasonDropDownList.DataTextField = "DESCRIPTION"
            reasonDropDownList.DataBind()
        End If
    End Sub

    Private Sub notificationDataBind()
        If notificationDropDownList.Items.Count = 1 Then
            Dim ds As DataSet
            ds = Data.GetCodecodetype("11", "")
            notificationDropDownList.DataSource = ds.Tables(0)
            notificationDropDownList.DataValueField = "ID"
            notificationDropDownList.DataTextField = "DESCRIPTION"
            notificationDropDownList.DataBind()
        End If
    End Sub

    Private Sub typeDataBind()
        If typeDropDownList.Items.Count = 1 Then
            Dim ds As DataSet
            ds = Data.GetCodecodetype("28", "")
            typeDropDownList.DataSource = ds.Tables(0)
            typeDropDownList.DataValueField = "ID"
            typeDropDownList.DataTextField = "DESCRIPTION"
            typeDropDownList.DataBind()
        End If
    End Sub

    Private Sub formDataBind()

        Dim ds As New DataSet
        ds = BookingCancellation.GetCancellationDetail(BookingId, RentalId, CurrentPage.UserCode)

        If ds.Tables.Count > 0 Then

            dateDateControl.Date = Utility.ParseDateTime(ds.Tables(0).Rows(0)("CurrDt"), Utility.SystemCulture)
            timeTimeControl.Text = ds.Tables(0).Rows(0)("CurrTm")

            If ds.Tables(0).Rows(0)("BooCancelWhen") <> 1 Then
                cancelBookingButton.Enabled = True
            End If

            If ds.Tables(0).Rows(0)("RntCancelWhen") <> 1 Then
                cancelRentalButton.Enabled = True
            End If

            If Not String.IsNullOrEmpty(ds.Tables(0).Rows(0)("Msg")) Then
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, ds.Tables(0).Rows(0)("Msg"))
            End If

            reasonDropDownList.SelectedValue = ds.Tables(0).Rows(0)("Reason")
            Dim item As ListItem
            Try
                If (reasonDropDownList.SelectedItem.Text = "Agent Error") Then

                    item = notificationDropDownList.Items.FindByText("Travel agent via B2B")
                    If (Not item Is Nothing) Then
                        notificationDropDownList.SelectedValue = item.Value
                    End If

                    item = reasonDropDownList.Items.FindByText("As per request from Agent")
                    If (Not item Is Nothing) Then
                        reasonDropDownList.SelectedValue = item.Value
                    End If

                    item = typeDropDownList.Items.FindByText("Cancellation")
                    If (Not item Is Nothing) Then
                        typeDropDownList.SelectedValue = item.Value
                    End If

                Else
                    notificationDropDownList.SelectedValue = ds.Tables(0).Rows(0)("Notifi")
                    typeDropDownList.SelectedValue = ds.Tables(0).Rows(0)("Type")
                End If

            Catch ex As Exception
                If (reasonDropDownList.SelectedItem.Text = "Agent Error") Then

                    item = notificationDropDownList.Items.FindByText("Travel agent via B2B")
                    If (Not item Is Nothing) Then
                        notificationDropDownList.SelectedValue = item.Value
                    End If

                    item = reasonDropDownList.Items.FindByText("As per request from Agent")
                    If (Not item Is Nothing) Then
                        reasonDropDownList.SelectedValue = item.Value
                    End If

                    item = typeDropDownList.Items.FindByText("Cancellation")
                    If (Not item Is Nothing) Then
                        typeDropDownList.SelectedValue = item.Value
                    End If

                Else
                    notificationDropDownList.SelectedValue = ds.Tables(0).Rows(0)("Notifi")
                    typeDropDownList.SelectedValue = ds.Tables(0).Rows(0)("Type")
                End If

            End Try



            ViewState(BookingCancellation_IntNo_ViewState) = ds.Tables(0).Rows(0)("IntNo")
            ViewState(BookingCancellation_RentalStatus_ViewState) = ds.Tables(0).Rows(0)("RntSt")
            ViewState(BookingCancellation_CityCode_ViewState) = ds.Tables(0).Rows(0)("CtyCode")
            ViewState(BookingCancellation_ExtnRef_ViewState) = ds.Tables(0).Rows(0)("ExtnRef")


        End If
    End Sub

    Protected Sub cancelRentalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelRentalButton.Click

        If String.IsNullOrEmpty(reasonDropDownList.SelectedValue) Or String.IsNullOrEmpty(notificationDropDownList.SelectedValue) Or _
        String.IsNullOrEmpty(typeDropDownList.SelectedValue) Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, CurrentPage.GetMessage("GEN005"))
            Return
        End If

        confimationBoxControl.Text = "Are you sure you want to cancel this Rental?"
        confimationBoxControl.Param = "Rental"
        confimationBoxControl.Show()

    End Sub

    Protected Sub cancelBookingButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelBookingButton.Click

        If String.IsNullOrEmpty(reasonDropDownList.SelectedValue) Or String.IsNullOrEmpty(notificationDropDownList.SelectedValue) Or _
             String.IsNullOrEmpty(typeDropDownList.SelectedValue) Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, CurrentPage.GetMessage("GEN005"))
            Return
        End If

        confimationBoxControl.Text = "Are you sure you want to cancel this Booking and ALL the associated Rentals?"
        confimationBoxControl.Param = "Booking"
        confimationBoxControl.Show()

    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        Dim returnMessage As String = ""

        Select Case param
            Case "Rental"
                If BookingCancellation.CancelRental(RentalId, ViewState(BookingCancellation_ExtnRef_ViewState), _
                    ViewState(BookingCancellation_RentalStatus_ViewState), BookingId, dateDateControl.Text, _
                    timeTimeControl.Text, reasonDropDownList.SelectedValue, notificationDropDownList.SelectedValue, _
                    typeDropDownList.SelectedValue, ViewState(BookingCancellation_IntNo_ViewState), ViewState(BookingCancellation_CityCode_ViewState), _
                    CurrentPage.UserCode, returnMessage) Then

                    ' force reload
                    RefreshRentalGrids()
                    ' force reload

                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, CurrentPage.GetMessage("GEN046"))
                    formDataBind()
                Else
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, returnMessage)
                End If
            Case "Booking"
                If BookingCancellation.CancelBooking(BookingId, dateDateControl.Text, _
                    timeTimeControl.Text, reasonDropDownList.SelectedValue, notificationDropDownList.SelectedValue, _
                    typeDropDownList.SelectedValue, ViewState(BookingCancellation_IntNo_ViewState), ViewState(BookingCancellation_CityCode_ViewState), _
                    CurrentPage.UserCode, returnMessage) Then

                    ' force reload
                    RefreshRentalGrids()
                    ' force reload

                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, CurrentPage.GetMessage("GEN046"))
                    formDataBind()
                Else
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, returnMessage)
                End If
        End Select
    End Sub

    'Private Function cancelRental(ByVal rId As String, ByVal rExtnRef As String, ByVal rStatus As String) As Boolean

    '    'Validation for IntegrityNo 
    '    Dim errorMessage As String
    '    errorMessage = BookingCancellation.GetIntegrityNoForRnt(rId, "", ViewState(BookingCancellation_IntNo_ViewState))

    '    If Not (String.IsNullOrEmpty(errorMessage) Or errorMessage.ToUpper().Trim() = "SUCCESS") Then
    '        CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
    '        Return False
    '    End If

    '    'Cancel Rental
    '    errorMessage = BookingCancellation.ManageCancelRental(BookingId, rId, dateDateControl.Text & " " & timeTimeControl.Text, _
    '        reasonDropDownList.SelectedValue, notificationDropDownList.SelectedValue, typeDropDownList.SelectedValue, _
    '        False, CurrentPage.UserCode, "CancelRental")

    '    If Not (String.IsNullOrEmpty(errorMessage) Or errorMessage.ToUpper().Trim() = "SUCCESS") Then
    '        CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
    '        Return False
    '    End If

    '    'Update DVASS
    '    If rStatus = "KK" Or rStatus = "NN" Then

    '        If ViewState(BookingCancellation_CityCode_ViewState).ToString().ToUpper = "AU" Then
    '            Aurora.Reservations.Services.AuroraDvass.selectCountry(0)
    '        ElseIf ViewState(BookingCancellation_CityCode_ViewState).ToString().ToUpper = "NZ" Then
    '            Aurora.Reservations.Services.AuroraDvass.selectCountry(1)
    '        End If

    '        Dim pDvassStatus As String = ""

    '        Aurora.Reservations.Services.AuroraDvass.rentalCancel(rExtnRef, 1, 0, pDvassStatus)

    '        If pDvassStatus.ToUpper.Trim <> "SUCCESS" Then

    '            If InStr(pDvassStatus, "333") = 0 Then
    '                'sErrorString = GetErrorTextFromResource(True, "", "Application", pDvassStatus)
    '                'manageCancelRental = "<Root>" & sErrorString & "<Data></Data></Root>"

    '                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, pDvassStatus)
    '                Return False
    '            End If ' If InStr(pDvassStatus, "333") = 0 Then

    '            'ErrorLog(sUsercode & " - " & "DVASS Error 333 By-Passed ", 2

    '        End If 'If pDvassStatus.ToUpper.Trim <> "SUCCESS" Then
    '    End If 'If ViewState(BookingCancellation_RentalStatus_ViewState) = "KK" Or ViewState(BookingCancellation_RentalStatus_ViewState) = "NN" Then

    '    Return True


    'End Function

    'Private Function cancelBooking() As Boolean

    '    Dim dt As DataTable
    '    dt = BookingCancellation.GetAllRentalFORBooking(BookingId)

    '    For Each dr As DataRow In dt.Rows

    '        If Not cancelRental(dr.Item("RntId"), dr.Item("ExtnRef"), dr.Item("RntStatus")) Then
    '            Return False
    '        End If
    '    Next

    '    Dim errorMessage As String
    '    errorMessage = BookingCancellation.ManageCancelBooking(BookingId, CurrentPage.UserCode, "CancelBooking")

    '    If Not (String.IsNullOrEmpty(errorMessage) Or errorMessage.ToUpper().Trim() = "SUCCESS") Then
    '        CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
    '        Return False
    '    End If

    '    Return True

    'End Function


End Class
