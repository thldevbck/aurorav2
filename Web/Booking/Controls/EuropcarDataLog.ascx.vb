﻿Option Strict On
Option Explicit On

Imports System.Collections.Generic
Imports Aurora.Booking.Services


Partial Class Booking_Controls_EuropcarDataLog
    Inherits System.Web.UI.UserControl

    Private mRentalId As String

    Public Property RentalId() As String
        Get
            Return mRentalId
        End Get
        Set(ByVal value As String)
            mRentalId = value
        End Set
    End Property

    Public Sub ShowPopup()
        'europcarModalDialogContent.Visible = True
        plhEuropCarDialogInner.Visible = True
        GetData()
        europcarModalDialog.Show()

        plhEuropcarLogScript.Visible = True
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "XmlLogInit", RenderControlToString(plhEuropcarLogScript), False)
        plhEuropcarLogScript.Visible = False
    End Sub

    Public Sub GetData()
        Dim rentalId As String = Me.RentalId

        If String.IsNullOrEmpty(rentalId) Then
            Return
        End If

        Dim logEntries As List(Of EuropcarServiceLogEntry) = (New EuropcarServiceRepository()).GetServiceLog(rentalId)

        rptLog.DataSource = logEntries
        rptLog.DataBind()
    End Sub

    Protected Function EncodeHtml(ByVal value As String) As String
        Return HttpUtility.HtmlEncode(value)
    End Function

    Protected Function RenderControlToString(ByVal control As Control) As String
        Dim objStringWriter As System.IO.StringWriter = Nothing
        Dim objHtmlWriter As HtmlTextWriter = Nothing
        Try
            objStringWriter = New System.IO.StringWriter()
            objHtmlWriter = New HtmlTextWriter(objStringWriter)
            control.RenderControl(objHtmlWriter)
            Return objStringWriter.ToString()
        Catch

        Finally
            If objHtmlWriter IsNot Nothing Then
                objHtmlWriter.Close()
            End If

            If objStringWriter IsNot Nothing Then
                objStringWriter.Close()
            End If
        End Try

        Return Nothing
    End Function

    Protected Overrides Sub OnPreRender(ByVal e As System.EventArgs)
        Dim page As Page = Me.Page

        plhStyle.Parent.Controls.Remove(plhStyle)

        If page IsNot Nothing Then
            Dim header As HtmlHead = page.Header

            If header IsNot Nothing Then
                header.Controls.Add(plhStyle)
            End If
        End If

        MyBase.OnPreRender(e)
    End Sub

    


End Class

