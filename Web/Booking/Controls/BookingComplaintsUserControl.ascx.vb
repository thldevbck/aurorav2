'' Change Log!
''  29.10.9 - Guess Who? - Created!

Imports System.Xml
Imports Aurora.Common.Utility
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class BookingComplaintsUserControl
    Inherits BookingUserControl

    Public Property isFirstLoad() As Boolean
        Get
            If ViewState("BooCmp_isFirstLoad") IsNot Nothing Then
                Return CBool(ViewState("BooCmp_isFirstLoad"))
            Else
                Return True
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("BooCmp_isFirstLoad") = value
        End Set
    End Property




    Public Sub LoadPage()
        grdComplaints.datasource = BookingComplaints.GetComplaintSummary(RentalId)
        grdComplaints.DataBind()

        Dim iResult As Integer
        iResult = BookingComplaints.CheckVehicleDataExists(BookingNumber & "/" & RentalNo, CurrentPage.UserCode)

        If iResult = 2 Then
            btnAddComplaints.Attributes.Add("onclick", "window.open('ComplaintManagement.aspx?CmpId=-1&RntId=" & RentalId & "');return false;")
        Else
            btnAddComplaints.Enabled = False
            If iResult = 0 Then
                CurrentPage.SetInformationShortMessage("No vehicle data exists")
            End If
        End If

    End Sub



End Class
