<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookingConfirmationUserControl.ascx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	?.?.? / Base version
''                      22.5.8 / table changes - for popup resize issue
''                       5.6.8 / Arrival connection and departure connection are now readonly textboxes
''                               as per Issue 4 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=4)
''                      11.6.8 / Shoel / Fixed major bug with Check-In Issue 38 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=38
''                       9.7.8 / Shoel / Made change to the icon for Reprint and OK buttons
''                      18.7.8 / Shoel / fixed picker name issue (AKL - Auckland is now displayed, instead of just AKL). Also picker resized
''                      21.7.8 / Shoel / fix for AIMS message 1003 - pkrCheckOutLoc is now hidden by default.
''                      21.7.8 / Shoel / spellings are now Check-in and Check-out
''                      30.7.8 / Shoel / Cosmetic changes as per printout 
''                      15.8.8 / Shoel / Fixes for SQUISH# 559,560,562
''                      16.10.8/ Shoel / Renamed the initial location picker to cki location picker
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingCheckInCheckOutUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingCheckInCheckOutUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="BookingCheckInCheckOutPopupUserControl.ascx" TagName="BookingCheckInCheckOutPopupUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/TimeControl/TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/MessagePanelControl/MessagePanelControl.ascx"
    TagName="MessagePanelControl" TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx"
    TagName="ConfimationBoxControl" TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<%@ Register Src="../../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<uc1:ConfimationBoxControl ID="confimationBoxControl" runat="server" LeftButton_Text="Continue"
    RightButton_Text="Cancel" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
    Title="Booking Check-in/Check-out" />
<uc1:SupervisorOverrideControl ID="supervisorOverrideControl" runat="server" />
<uc1:BookingCheckInCheckOutPopupUserControl ID="popupSerialNos" runat="server" />
<asp:HiddenField ID="hdnWhoWasClicked" runat="server" />
<%--<asp:HiddenField ID="hdnIsTCAccepted" runat="server" /> https://thlonline.atlassian.net/browse/DSD-623 --%>
<asp:Panel ID="tblInitial" Style="display: none;" runat="server">
    <table width="100%" cellspacing="0" cellpadding="2">
        <tr>
            <td width="404px">
                <b>Check-out / Check-in:</b></td>
            <td width="150px">
                Status:</td>
            <td>
                <asp:TextBox ID="txtInitialCkiCkoStatus" runat="server" ReadOnly="true" CssClass="Textbox_Medium" />
            </td>
        </tr>
    </table>
    <hr />
    <table cellspacing="0" cellpadding="2" width="100%">
        <tr>
            <td width="558px">
                The planned
                <asp:Label ID="lblCkoCkiInitialWord1" runat="server" Text="[cko/cki]" Font-Bold="true" />
                location for this rental is :&nbsp
            </td>
            <td>
                <asp:TextBox ID="lblCkoCkiLocInitialRnt" runat="server" ReadOnly="true" CssClass="Textbox_Medium" />
            </td>
        </tr>
        <tr>
            <td>
                Your Location is :&nbsp</td>
            <td>
                <asp:TextBox ID="lblCkoCkiLocInitialYour" runat="server" ReadOnly="true" CssClass="Textbox_Medium" />
            </td>
        </tr>
        <tr runat="server">
            <td>
                <!-- Fix for CO/CI issue -->
                <asp:Label ID="lblTextForCICOInitital1" runat="server" />
                &nbsp;
                <asp:Label ID="lblCkoCkiInitialWord2" runat="server" Font-Bold="true" />
                <asp:Label ID="lblTextForCICOInitital2" runat="server" />
                &nbsp
            </td>
            <td>
                <asp:TextBox ID="lblCkoCkiLocInitialProcess" runat="server" ReadOnly="true" CssClass="Textbox_Medium" />
                <uc1:PickerControl ID="pkrCheckInLocation" runat="server" PopupType="LOCATIONFORCOUNTRY"
                    AppendDescription="true" Width="148px" Visible="false" />
            </td>
        </tr>
        <tr id="trDisplayForCO" runat="server">
            <td colspan="2">
                To process this
                <asp:Label ID="lblCkoCkiInitialWord3" runat="server" />
                through another location, you must modify the booking.
            </td>
        </tr>
        <tr>
            <td colspan="2" align="right">
                <asp:Button ID="btnInitialOK" runat="server" Text="OK" CssClass="Button_Standard Button_OK" />
            </td>
        </tr>
    </table>
    <br />
    <br />
</asp:Panel>
<%--The User control--%>
<asp:Panel runat="server" ID="tblMain" Style="display: none">
    <table width="100%">
        <tr>
            <td width="150px">
                <b>Check-out / Check-in:</b></td>
            <td width="250px">
                &nbsp;</td>
            <td width="150px">
                Status:</td>
            <td>
                <asp:TextBox ID="txtCICOStatus" runat="server" ReadOnly="true" CssClass="Textbox_Small" />
            </td>
        </tr>
    </table>
    <hr />
    <%--The Actual controls for CI/CO--%>
    <b>Check-out&nbsp;<asp:Label ID="lblCkoLocation" runat="server" />:</b>
    <br />
    <!-- Table for CO follows-->
    <table width="100%">
        <tr>
            <td width="150px">
                Unit No:</td>
            <td width="250px">
                <asp:TextBox ID="txtUnitNo" runat="server" CssClass="Textbox_Small" MaxLength="12"
                    AutoPostBack="true" />&nbsp;*
            </td>
            <td width="150px">
                Reg.:</td>
            <td>
                <!--/* Added by Shoel - THL IT Change Request Odometer - also call 7173 */-->
                <asp:TextBox ID="txtRegNo" runat="server" CssClass="Textbox_Small" MaxLength="12"
                    AutoPostBack="true" />&nbsp;*
                <%--<asp:Button ID="btnHiddenRegoChecker" runat="server" Text="" OnClick="btnHiddenRegoChecker_Click"
                            Style="display: none" />--%>
            </td>
        </tr>
        <tr>
            <td>
                Vehicle:</td>
            <td>
                <asp:TextBox ID="lblVehicleDetails" runat="server" ReadOnly="true" CssClass="Textbox_Large"
                    TabIndex="-1" />
            </td>
            <td>
                Voucher No.:</td>
            <td>
                <asp:TextBox ID="txtVoucherNo" runat="server" CssClass="Textbox_Small" MaxLength="12" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblOdometerOut" runat="server" Text="Odometer Out:" /></td>
            <td>
                <asp:TextBox ID="txtOdometerOut" runat="server" CssClass="Textbox_Small_Right_Aligned"
                    onkeypress="keyStrokeInt();" MaxLength="12" />&nbsp;*
            </td>
            <td>
                Date/Time:</td>
            <td>
                <uc1:DateControl ID="dcCheckOutDate" runat="server" ReadOnly="true" />
                &nbsp;
                <uc1:TimeControl ID="tcCheckOutTime" runat="server" />
                &nbsp;*
            </td>
        </tr>
        <tr id="trOdometerOut" runat="server">
            <td colspan="4">
                <uc1:MessagePanelControl ID="msgOdometerOut" runat="server" MessageType="Information"
                    Text="Please enter hubbo reading instead of odometer for 6B" CssClass="popupMessagePanel"
                    Visible="true" />
            </td>
        </tr>
        <tr>
            <td>
                Area of use:</td>
            <td>
                <asp:TextBox ID="txtAreaOfUse" runat="server" MaxLength="24" />
            </td>
            <td>
                &nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="4" align="right">
                <%--
                    Commented as this feature is no longer required
                    <asp:Button ID="btnCheckOut" runat="server" Text="Check-out" CssClass="Button_Standard" OnClientClick="return ConfirmCheckInOut();" />--%>
                <asp:Button ID="btnCheckOut" runat="server" Text="Check-out" CssClass="Button_Standard" />

                <asp:Button ID="btnPreCheckOut" runat="server" Text="Pre Check-out" CssClass="Button_Standard" />
            </td>
        </tr>
    </table>
    <!-- Table for CO Complete-->
    <br />
    <b>Check-in&nbsp;<asp:Label ID="lblCkiLocation" runat="server" />:</b>
    <br />
    <!-- Table for CI follows-->
    <table width="100%">
        <tr>
            <td width="150px">
                Unit No.:</td>
            <td width="250px">
                <asp:TextBox ID="txtCIUnitNo" runat="server" CssClass="Textbox_Small" MaxLength="12" />&nbsp;*
            </td>
            <td width="150px">
                Reg.:</td>
            <td>
                <asp:TextBox ID="txtCIRegNo" runat="server" CssClass="Textbox_Small" MaxLength="12" />&nbsp;*
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblOdometerIn" runat="server" Text="Odometer In:" /></td>
            <td>
                <asp:TextBox ID="txtOdometerIn" runat="server" CssClass="Textbox_Small" onkeypress="keyStrokeInt();"
                    Style="text-align: right" MaxLength="12" />&nbsp;*&nbsp;&nbsp;
                <asp:Label ID="lblOdometerDiff" runat="server" />&nbsp;kms
            </td>
            <td>
                Date/Time:</td>
            <td>
                <uc1:DateControl ID="dcCheckInDate" runat="server" />
                &nbsp;
                <uc1:TimeControl ID="tcCheckInTime" runat="server" />
                &nbsp;*
            </td>
        </tr>
        <tr id="trOdometerIn" runat="server">
            <td colspan="4">
                <uc1:MessagePanelControl ID="msgOdometerIn" runat="server" MessageType="Information"
                    Text="Please enter hubbo reading instead of odometer for 6B" CssClass="popupMessagePanel"
                    Visible="true" />
            </td>
        </tr>
    </table>
    <!-- Table for CI Complete-->
    <br />
    <table width="100%" cellspacing="0" cellpadding="2">
        <tr>
            <td width="150px">
                Arrival Connection:</td>
            <td width="250px">
                <asp:TextBox ID="lblArrivalConnection" runat="server" CssClass="Textbox_Large" ReadOnly="true" />
            </td>
            <td width="150px">
                Departure Connection:</td>
            <td>
                <asp:TextBox ID="lblDepartureConnection" runat="server" CssClass="Textbox_Large"
                    ReadOnly="true" />
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">
                <asp:Button ID="btnCheckIn" runat="server" Text="Check-in" CssClass="Button_Standard" />
                <asp:Button ID="btnCheckInNoPrint" runat="server" Text="Check-in (No Print)" CssClass="Button_Standard" />
            </td>
        </tr>
    </table>
    <%--The Actual controls for CI/CO--%>
    <hr />
    <table width="100%">
        <tr>
            <td align="right">
                <%--'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was--%>
                <asp:Button ID="BtnReprintAmazon" runat="server" Text="Signed Rental Agreement" Style="width: 190px;"  CssClass="Button_Standard Button_Print" />
                
                <asp:Button ID="btnReprint" runat="server" Text="Reprint" CssClass="Button_Standard Button_Print" />
                <%--'End nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was--%>
                <asp:Button ID="btnMaintainSerialNumbers" runat="server" Style="width: 175px;" Text="Maintain Serial Numbers"
                    CssClass="Button_Standard" />
            </td>
        </tr>
    </table>
    <%--Added by Nimesh on 12th May 15 to display alert if terms and conditions are not checked by user https://thlonline.atlassian.net/browse/DSD-623--%>
    <script type = "text/javascript">
        // Commented by Nimesh on 15th as this feature is no more required - https://thlonline.atlassian.net/browse/DSD-623
        //function ConfirmCheckInOut() {
        //    var retvalue = true 
        //    var a = $('#ctl00_ContentPlaceHolder_bookingCheckInCheckOutUserControl_hdnIsTCAccepted').val();
        //    var isTrueSet = (a.toLowerCase() === 'true');
        //    if (isTrueSet === false) {
        //        if (confirm("The non-premium customer has not accepted the terms and conditions via SCI. If you choose to continue check out, please get the customer to sign the rental agreement")) {
        //            retvalue = true;
        //        } else {
        //            retvalue = false;
        //        }
        //    }
        //    return retvalue;
        //}
        //function ConfirmCheckInOutMsgBox() {
            
        //    alert('The non-premium customer has not accepted the terms and conditions via SCI. If you choose to continue check out, please get the customer to sign the rental agreement');
        //    //jQuery.jAlert('This is a custom alert box', 'Alert Dialog');
        //}
    </script>
    <%-- End Added by Nimesh on 12th May 15 to display alert if terms and conditions are not checked by user https://thlonline.atlassian.net/browse/DSD-623--%>
</asp:Panel>
