'' Change Log!
'' 19.9.8   -   Shoel   - Active check box is checked by default for a new note :)

Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.xml

Partial Class Booking_Controls_BookingNotesPopupUserControl
    Inherits BookingUserControl

    Private Const BookingNotesPopupUserControl_NoteId_ViewState = "BookingNotesPopupUserControl_NoteId"
    Private Const BookingNotesPopupUserControl_IntegrityNo_ViewState = "BookingNotesPopupUserControl_IntegrityNo"

    Public Delegate Sub BookingNotesPopupUserControlEventHandler(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object)
    Public Event PostBack As BookingNotesPopupUserControlEventHandler


    Private Function DecryptCreditCardDataSuccess(ByVal cardNumberEncrypted As String, ByRef cardNumberdecrypted As String, Optional ByVal isAllowed As Boolean = False) As Boolean

        Try
            Dim mKeyfile As String = Server.MapPath("~/app_code/" & ConfigurationManager.AppSettings("KEYFILE"))
            Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = ConfigurationManager.AppSettings("PROTECTKEY")
            Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = ConfigurationManager.AppSettings("ALGORITHM")
            Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)
            cardNumberdecrypted = Aurora.Booking.Services.BookingPaymentCardEncryption.DecryptCardNumber(Convert.FromBase64String(cardNumberEncrypted), mKeyfile, isAllowed)
            Return True

        Catch ex As Exception
            Logging.LogInformation("NoteUserControl", "DecryptCreditCardDataSuccess(" & cardNumberEncrypted & ")")
            Return False
        End Try

    End Function

    Private Function EncryptCreditCardDataSuccess(ByVal strxml As String, ByRef returnedXML As String) As Boolean




        Dim mKeyfile As String = Server.MapPath("~/app_code/" & ConfigurationManager.AppSettings("KEYFILE"))

        Aurora.Booking.Services.BookingPaymentCardEncryption.ProtectKey = ConfigurationManager.AppSettings("PROTECTKEY")
        Aurora.Booking.Services.BookingPaymentCardEncryption.AlgorithmName = ConfigurationManager.AppSettings("ALGORITHM")
        Dim bookingEncryption As New Aurora.Booking.Services.BookingPaymentCardEncryption(mKeyfile)
        Dim encryptedCreditcard As Byte()

        Try

            encryptedCreditcard = Aurora.Booking.Services.BookingPaymentCardEncryption.EncyrptCardNumber(strxml, mKeyfile)
            returnedXML = Convert.ToBase64String(encryptedCreditcard)
            Return True

        Catch ex As Exception
            ''some error occured...just make it as xxxxxxxxx
            Return False
        End Try


    End Function


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                typeDropDownList_DataBinding(sender, Nothing)
                audienceDropDownList_DataBinding(sender, Nothing)
                typeDropDownList.DataBind()
                audienceDropDownList.DataBind()
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Protected Sub typeDropDownList_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim dt As DataTable = BookingNotes.GetNoteType()
            typeDropDownList.DataSource = dt
            typeDropDownList.DataTextField = "DESCRIPTION"
            typeDropDownList.DataValueField = "ID"
        Catch ex As Exception
            CurrentPage.LogException(ex)
            bookingNotesPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading the booking notes tab.")
            'CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Protected Sub audienceDropDownList_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim dt As DataTable = BookingNotes.GetAudienceType()
            audienceDropDownList.DataSource = dt
            audienceDropDownList.DataTextField = "DESCRIPTION"
            audienceDropDownList.DataValueField = "ID"
        Catch ex As Exception
            CurrentPage.LogException(ex)
            bookingNotesPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Private Sub changeInsertMode()
        rentalNoTextBox.ReadOnly = True
        typeDropDownList.Enabled = True
        noteTextBox.ReadOnly = False
        titleLabel.Text = "Add Note"
    End Sub


    Private Sub changeEditMode()
        rentalNoTextBox.ReadOnly = True
        typeDropDownList.Enabled = False
        noteTextBox.ReadOnly = True
        titleLabel.Text = "Edit Note"
    End Sub

    Public Sub loadInsertTemplate()

        changeInsertMode()

        ViewState(BookingNotesPopupUserControl_NoteId_ViewState) = ""
        ViewState(BookingNotesPopupUserControl_IntegrityNo_ViewState) = ""

        Dim ds As DataSet
        ds = BookingNotes.GetBookingNote("", BookingId, RentalId)

        noteFormViewDataBound(ds.Tables(0))

        programmaticModalPopup.Show()

    End Sub

    Public Sub loadEditTemplate(ByVal noteId As String)

        changeEditMode()

        ViewState(BookingNotesPopupUserControl_NoteId_ViewState) = noteId

        Dim ds As DataSet
        ds = BookingNotes.GetBookingNote(noteId, "", "")
        noteFormViewDataBound(ds.Tables(0))

        programmaticModalPopup.Show()

    End Sub

    Protected Sub noteFormViewDataBound(ByVal dt As DataTable)
        Try

            bookingIdTextBox.Text = dt.Rows(0)("BNo")
            rentalNoTextBox.Text = dt.Rows(0)("RNo")
            typeDropDownList.SelectedValue = dt.Rows(0)("CTId")
            audienceDropDownList.SelectedValue = dt.Rows(0)("AuId")
            priorityDropDownList.SelectedValue = dt.Rows(0)("pr")
            noteTextBox.Text = dt.Rows(0)("Dsc")
            addTextBox.Text = dt.Rows(0)("AUS")
            modifiedTextBox.Text = dt.Rows(0)("MUS")

            Dim s As String = dt.Rows(0)("ADT")
            If Not String.IsNullOrEmpty(s) Then
                addTextBox1.Text = Utility.DateDBUIConvert(s.Substring(0, s.IndexOf(" "))) & s.Substring(s.IndexOf(" "))
            Else
                addTextBox1.Text = ""
            End If
            s = dt.Rows(0)("MDT")
            If Not String.IsNullOrEmpty(s) Then
                modifiedTextBox1.Text = Utility.DateDBUIConvert(s.Substring(0, s.IndexOf(" "))) & s.Substring(s.IndexOf(" "))
            Else
                modifiedTextBox1.Text = ""
            End If

            'addTextBox1.Text = Utility.DateDBUIConvert(dt.Rows(0)("ADT"))
            'modifiedTextBox1.Text = Utility.DateDBUIConvert(dt.Rows(0)("MDT"))


            Dim noteId As String
            noteId = ViewState(BookingNotesPopupUserControl_NoteId_ViewState)

            If Not String.IsNullOrEmpty(noteId) Then
                'Dim activeCheckBox As CheckBox = noteFormView.FindControl("activeCheckBox")
                activeCheckBox.Checked = Utility.ParseInt(dt.Rows(0)("IsActive"))

                ''rev:mia - for security card enhancement
                'Dim noteTextBox As TextBox = noteFormView.FindControl("noteTextBox")
                If noteTextBox IsNot Nothing Then
                    '' Dim sc As ScriptManager = ScriptManager.GetCurrent(MyBase.CurrentPage)

                    If Not String.IsNullOrEmpty(noteTextBox.Text) AndAlso typeDropDownList.SelectedItem.Text.Contains("Credit") Then

                        Dim decryptedCard As String = ""
                        If Me.DecryptCreditCardDataSuccess(noteTextBox.Text, decryptedCard, True) Then
                            noteTextBox.Text = decryptedCard
                        End If

                        ''---------------------------------------------------------
                        ''rev:mia jan 30 2009 -- Changes from session to viewstate
                        ''---------------------------------------------------------
                        ''NOTE: regardless whether data is encrypted or not...always log a history
                        Dim getBPDIdforDecryption As String = ""
                        If Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode) IsNot Nothing Then
                            getBPDIdforDecryption = Session("bpdIDforDecryption" & MyBase.CurrentPage.UserCode)
                        End If

                        Dim objPage As Page = MyBase.Page
                        Try

                            Dim productcontrol As Object = objPage.Controls(0).Controls(2).Controls(15).Controls(7).FindControl("BookingProductUserControl1")
                            If productcontrol IsNot Nothing Then
                                getBPDIdforDecryption = IIf(String.IsNullOrEmpty(productcontrol.BpdIDDforDecryptionData), getBPDIdforDecryption, productcontrol.BpdIDDforDecryptionData)
                            End If

                        Catch ex As Exception
                        End Try


                        Dim tempSupervisor As String = MyBase.CurrentPage.UserCode
                        If Not String.IsNullOrEmpty(CType(Session("SupervisorName"), String)) Then
                            tempSupervisor = CType(Session("SupervisorName"), String)
                        End If

                        Try
                            Dim notecontrol As Object = objPage.Controls(0).Controls(2).Controls(15).Controls(7).FindControl("bookingNotesUserControl")
                            If notecontrol IsNot Nothing Then
                                tempSupervisor = IIf(String.IsNullOrEmpty(notecontrol.SupervisorName), tempSupervisor, notecontrol.SupervisorName)
                            End If
                        Catch ex As Exception
                        End Try
                        ''---------------------------------------------------------

                        Aurora.Booking.Services.BookingPaymentMaintenance.AddDecryptHistory(MyBase.RentalId, tempSupervisor, getBPDIdforDecryption, "From Notes - ")

                    End If
                End If

                'Set Viewstate
                ViewState(BookingNotesPopupUserControl_NoteId_ViewState) = dt.Rows(0)("NId")
                ViewState(BookingNotesPopupUserControl_IntegrityNo_ViewState) = dt.Rows(0)("IntNo")

            Else

                typeDropDownList.SelectedValue = "471AEF57-626D-434A-ACA8-9E5E9112AC3F" ' "Rental"

                'Set Viewstate
                ViewState(BookingNotesPopupUserControl_NoteId_ViewState) = ""
                ViewState(BookingNotesPopupUserControl_IntegrityNo_ViewState) = ""

                activeCheckBox.Checked = True ' checked by default
            End If

            oldRentalNo.Text = rentalNoTextBox.Text
            oldType.Text = typeDropDownList.SelectedValue
            oldAudience.Text = audienceDropDownList.SelectedValue
            oldPriority.Text = priorityDropDownList.SelectedValue
            oldActive.Checked = activeCheckBox.Checked
            oldNote.Text = noteTextBox.Text

        Catch ex As Exception
            CurrentPage.LogException(ex)
            bookingNotesPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading the booking notes tab.")
        End Try
    End Sub


    Private Sub ValidateSaveNew()
        If typeDropDownList.SelectedValue = "471AEF57-626D-434A-ACA8-9E5E9112AC3F" _
         AndAlso String.IsNullOrEmpty(rentalNoTextBox.Text.Trim) Then
            Throw New Aurora.Common.ValidationException("Rental No is mandatory if type is Rental")
        ElseIf String.IsNullOrEmpty(noteTextBox.Text.Trim) Then
            Throw New Aurora.Common.ValidationException("Note Description is Mandatory")
        ElseIf noteTextBox.Text.Length > 2048 Then
            Throw New Aurora.Common.ValidationException("Description cannot be greater than 2048 characters!")
        End If
    End Sub


   

    Protected Sub noteSaveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles noteSaveButton.Click
        Try
            ValidateSaveNew()
        Catch ex As Aurora.Common.ValidationException
            programmaticModalPopup.Show()
            bookingNotesPopupMessagePanelControl.SetMessagePanelWarning(ex.Message)
            Return
        End Try

        Dim noteId As String = SaveNote()

        ''rev:mia Sept. 2, 2013 - As requested by Rajesh. Redisplaying for popup will be disabled
        ''loadEditTemplate(noteId)
        ''showPopup()
        RaiseEvent PostBack(Me, True, False, True)
    End Sub


    Protected Sub saveNewButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveNewButton.Click
        Try
            ValidateSaveNew()
        Catch ex As Aurora.Common.ValidationException
            programmaticModalPopup.Show()
            bookingNotesPopupMessagePanelControl.SetMessagePanelWarning(ex.Message)
            Return
        End Try

        SaveNote()
        loadInsertTemplate()
        rentalNoTextBox.ReadOnly = False
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        'Invoke PopupPostBack event to redatabind the grid
        RaiseEvent PostBack(Me, True, False, True)
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        Dim noteId As String = ViewState(BookingNotesPopupUserControl_NoteId_ViewState)
        If Not String.IsNullOrEmpty(noteId) Then
            loadEditTemplate(noteId)
        Else
            loadInsertTemplate()
        End If
    End Sub

    Private Sub ValidateCopy()
        If oldRentalNo.Text <> rentalNoTextBox.Text _
         OrElse oldType.Text <> typeDropDownList.SelectedValue _
         OrElse oldAudience.Text <> audienceDropDownList.SelectedValue _
         OrElse oldPriority.Text <> priorityDropDownList.SelectedValue _
         OrElse oldActive.Checked <> activeCheckBox.Checked _
         OrElse oldNote.Text <> noteTextBox.Text Then
            Throw New Aurora.Common.ValidationException("Save Data before Copying")
        End If
    End Sub


    Protected Sub copyButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles copyButton.Click
        Try
            ValidateCopy()
        Catch ex As Aurora.Common.ValidationException
            programmaticModalPopup.Show()
            bookingNotesPopupMessagePanelControl.SetMessagePanelWarning(ex.Message)
            Return
        End Try

        changeInsertMode()

        ViewState(BookingNotesPopupUserControl_NoteId_ViewState) = ""
        ViewState(BookingNotesPopupUserControl_IntegrityNo_ViewState) = ""
        addTextBox.Text = ""
        modifiedTextBox.Text = ""
        addTextBox1.Text = ""
        modifiedTextBox1.Text = ""

        programmaticModalPopup.Show()
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        typeDropDownList.Attributes.Add("onchange", "notesPopup_typeDropDownList_onChange (this, '" & rentalNoTextBox.ClientID & "');")
        rentalNoTextBox.Attributes.Add("onchange", "notesPopup_rentalNoTextBox_onBlur (this, '" & typeDropDownList.ClientID & "');")
    End Sub

#Region "rev:mia jan6,2009"
    Private Function SaveNote() As String

        Dim noteId As String = ViewState(BookingNotesPopupUserControl_NoteId_ViewState)
        Dim integrityNo As String = ViewState(BookingNotesPopupUserControl_IntegrityNo_ViewState)

        Dim textNodeDesc As String
        Dim returnValueEncrypted As String = ""
        If Not String.IsNullOrEmpty(noteTextBox.Text) AndAlso typeDropDownList.SelectedItem.Text.Contains("Credit") Then
            textNodeDesc = noteTextBox.Text
            Dim BooleanValue As Boolean = Me.EncryptCreditCardDataSuccess(textNodeDesc, returnValueEncrypted)
            If BooleanValue = False Then
                returnValueEncrypted = noteTextBox.Text
            End If
        Else
            returnValueEncrypted = noteTextBox.Text
        End If

        ''rev:mia jan6,2009 -squsih #652 issues 'Note' tab - <Add Notes> - A 'Booking' type note doesn't add to all rentals within a booking
        Dim rentalNoText As String = rentalNoTextBox.Text
        If typeDropDownList.SelectedItem.Text.IndexOf("Booking") <> -1 Then
            rentalNoText = String.Empty
        End If
        Dim returnMessage As String = BookingNotes.ManageNote(noteId, bookingIdTextBox.Text, rentalNoText, _
            typeDropDownList.SelectedValue, returnValueEncrypted, _
            audienceDropDownList.SelectedValue, priorityDropDownList.SelectedValue, activeCheckBox.Checked, _
            Utility.ParseInt(integrityNo), CurrentPage.UserCode, CurrentPage.UserName)

        If Not String.IsNullOrEmpty(returnMessage) And Not returnMessage = "True" Then
            If returnMessage.Contains("GEN045") Or returnMessage.Contains("GEN046") Or returnMessage.Contains("GEN047") Then

                Dim message As String
                message = returnMessage.Substring(0, returnMessage.LastIndexOf("/"))
                bookingNotesPopupMessagePanelControl.SetMessagePanelInformation(message)

                noteId = returnMessage.Substring(returnMessage.LastIndexOf("/") + 1)
                ViewState(BookingNotesPopupUserControl_NoteId_ViewState) = noteId
            Else
                bookingNotesPopupMessagePanelControl.SetMessagePanelError(returnMessage)
                Return ""
            End If
        End If

        Return noteId
    End Function
#End Region

End Class
