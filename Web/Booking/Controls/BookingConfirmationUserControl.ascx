<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookingConfirmationUserControl.ascx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	15.4.8 / Base version
''                  -   31.7.8 / Change made as per comments from Saskia
''
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingConfirmationUserControl.ascx.vb" Inherits="Booking_Controls_BookingConfirmationUserControl" %>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td><b>Confirmation Summary:</b></td>
        <td align="right">
            <asp:CheckBox runat="server" ID="chkShowAll" AutoPostBack="true" Text ="Show All Rentals"  />
        </td>
    </tr>
</table>

<asp:GridView 
    ID="gwConfirmations" 
    runat="server" 
    CssClass="dataTableGrid" 
    AutoGenerateColumns="False" 
    GridLines="none"
    Width="100%" >
    <AlternatingRowStyle CssClass="oddRow" />
    <RowStyle CssClass="evenRow" />
    <Columns>
        <asp:BoundField DataField="CfnId" HeaderText="CfnId" HeaderStyle-Wrap="false" ItemStyle-Width="0%" />
        <asp:BoundField DataField="Rntl" HeaderText="Rnt" HeaderStyle-Wrap="false" ItemStyle-Width="5%" />
        <asp:TemplateField HeaderText="Audience" HeaderStyle-Wrap="false" ItemStyle-Width="10%">
            <ItemTemplate>
                <asp:HyperLink 
                    ID="lblLink" 
                    runat="server" 
                    NavigateUrl="#"
                    Text='<%# Bind("Audience") %>' />
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="LastSaved" HeaderText="Last Saved" HeaderStyle-Wrap="false" ItemStyle-Width="10%" />
        <asp:BoundField DataField="DateSent" HeaderText="Date Sent" HeaderStyle-Wrap="false" ItemStyle-Width="15%" />
        <asp:BoundField DataField="SentBy" HeaderText="Sent By" HeaderStyle-Wrap="false" ItemStyle-Width="13%" />
        <asp:BoundField DataField="Address" HeaderText="Address(es)" HeaderStyle-Wrap="false" ItemStyle-Width="51%" />
    </Columns>
</asp:GridView>

<br />

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="right">
            <asp:Button 
                runat="server" 
                ID="btnAddConfirmation" 
                Text="Add Confirmation" 
                CssClass="Button_Standard Button_Add" 
                Width="175px" />
        </td>
    </tr>
</table>

<asp:Button ID="btnHiddenGridRefresher" runat="server" style="display:none"/>