<!--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookingHistoryUserControl.ascx
''	Date Created	-	27.8.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	27.8.8 / Shoel - Base version
''
''
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
-->
<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingThriftyHistoryUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingThriftyHistoryUserControl" %>
<asp:Panel ID="Panel1" runat="server" Width="765px" Height="400px" ScrollBars="horizontal">
    <asp:GridView ID="gwThriftyHistory" runat="server" AutoGenerateColumns="False" CssClass="dataTableGrid"
        GridLines="None" EmptyDataText="No records">
        <AlternatingRowStyle CssClass="oddRow" Width="100%" />
        <RowStyle CssClass="evenRow" />
        <Columns>
            <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="TimeStamp" HeaderText="Time Stamp" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="CustomerName" HeaderText="Customer Name" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="CheckedOutDateTime" HeaderText="Checked Out" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="CheckedOutLocation" HeaderText="CheckedOutLocation" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="CheckedInDateTime" HeaderText="Checked In" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="CheckedInLocation" HeaderText="CheckedInLocation" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="AgencyReference" HeaderText="Agency Reference" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="BoosterSeat" HeaderText="Booster Seat" ReadOnly="True"
                ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Airport" HeaderText="Airport" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Product" HeaderText="Product" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" ItemStyle-Wrap="False" />
            <asp:BoundField DataField="DailyRate" HeaderText="Daily Rate" ReadOnly="True" ItemStyle-Wrap="False"
                ItemStyle-HorizontalAlign="right" />
            <asp:BoundField DataField="NetValue" HeaderText="Net Value" ReadOnly="True" ItemStyle-Wrap="False"
                ItemStyle-HorizontalAlign="right" />
            <asp:BoundField DataField="DepositAmount" HeaderText="Deposit Amount" ReadOnly="True"
                ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="right" />
            <asp:BoundField DataField="TotalAmount" HeaderText="Total Amount" ReadOnly="True"
                ItemStyle-Wrap="False" ItemStyle-HorizontalAlign="right" />
        </Columns>
    </asp:GridView>
</asp:Panel>
