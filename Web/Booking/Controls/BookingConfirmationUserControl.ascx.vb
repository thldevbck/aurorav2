'' Change Log!
'' 23.05.2008 - Shoel - Added dummy row if there is no confirmation data
''  1.07.2008 - Shoel - Added the date utility thingie + removed commented/unused code bits
''  2.07.2008 - Shoel - got the date thingie right
'' 30.07.2008 - Shoel - removed session usage
'' 30.07.2008 - Shoel - Booking Number also passed to Conf Mgt page for display purpose

'' sps used
'' cfn_get_ConfirmationSummary
'' cfn_getRentalsFromBooking
'' cfn_get_NoteSpec
'' cfn_getRentalCountry
'' cfn_update_confirmations
'' cfn_update_confirmationNoteSpec
'' cfn_update_OnlyAddressDetail
'' RES_Generate_Confirmation_Report
'' cfn_getFileName
'' cfn_getInfo_efax
'' cfn_update_reSend


Imports System.Data
Imports System.Xml
Imports System.Xml.Xsl
Imports Aurora.Common.Utility
Imports Aurora.Common


Partial Class Booking_Controls_BookingConfirmationUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False
    Const ViewState_BOOKINGID As String = "ViewState_Confirmation_BookingId"
    Const ViewState_RENTALID As String = "ViewState_Confirmation_RentalId"
    Const ViewState_BOOKINGNUM As String = "ViewState_Confirmation_BookingNum"

    Const ViewState_HIDDENREFRESHBUTTONID As String = "ViewState_Confirmation_HiddenRefreshButtonClientId"




    Protected Sub gwConfirmations_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwConfirmations.RowCreated
        e.Row.Cells(0).Visible = False
    End Sub

    Protected Sub chkShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowAll.CheckedChanged
        If chkShowAll.Checked Then
            LoadGrid(True)
        Else
            LoadGrid(False)
        End If
    End Sub

    Sub LoadGrid(Optional ByVal ShowAll As Boolean = False)
        Dim dtConfirmations As DataTable
        dtConfirmations = Aurora.Booking.Services.BookingConfirmation.GetConfirmationSummary(ViewState(ViewState_BOOKINGID), ViewState(ViewState_RENTALID), ShowAll)
        gwConfirmations.DataSource = dtConfirmations
        gwConfirmations.DataBind()

        ' check rows
        If gwConfirmations.Rows.Count = 0 Then
            ' add dummy
            Dim xmlTemp As New XmlDocument
            xmlTemp.LoadXml("<Confirmations><Confirmation><CfnId/><Rntl/><Audience/><LastSaved/><DateSent/><SentBy/><Address/></Confirmation></Confirmations>")
            Dim dstTemp As New DataSet
            dstTemp.ReadXml(New XmlNodeReader(xmlTemp.DocumentElement))
            gwConfirmations.DataSource = dstTemp.Tables(0)
            gwConfirmations.DataBind()
        End If
    End Sub


    Protected Sub gwConfirmations_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwConfirmations.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'if the date sent is empty then first load
            Try
                ''rev:mia 08-march-2016 ttps://thlonline.atlassian.net/browse/AURORA-777
                Dim ConfirmationId As String = CType(e.Row.DataItem, DataRowView)("CfnId")
                Dim BookingId As String = ViewState(ViewState_BOOKINGID)
                Dim DateSent As String = e.Row.Cells(4).Text
                Dim RentalId As String = ViewState(ViewState_RENTALID)
                Dim BookingNumber As String = e.Row.Cells(1).Text
                Dim RentalNo As String = e.Row.Cells(1).Text
                Dim ActiveRentalCountry As String = Me.CurrentPage.CountryCode
                Dim Audience As String = CType(e.Row.Cells(2).FindControl("lblLink"), HyperLink).Text


                Dim linkValue As String = CType(e.Row.Cells(2).FindControl("lblLink"), HyperLink).Text
                CType(e.Row.Cells(2).FindControl("lblLink"), HyperLink).Attributes.Add("OnClick", "LoadConfirmation('" _
                                                                                       & ConfirmationId _
                                                                                       & "','" & BookingId _
                                                                                       & "','" & DateSent _
                                                                                       & "','" & RentalId _
                                                                                       & "','" & BookingNumber _
                                                                                       & "','" & RentalNo _
                                                                                       & "','" & ActiveRentalCountry _
                                                                                       & "','" & linkValue _
                                                                                       & "'); return false;")

                'CType(e.Row.Cells(2).FindControl("lblLink"), HyperLink).Attributes.Add("OnClick", "LoadConfirmation('" & CType(e.Row.DataItem, DataRowView)("CfnId") _
                '                                                                       & "','" & ViewState(ViewState_BOOKINGID) _
                '                                                                       & "','" & e.Row.Cells(4).Text _
                '                                                                       & "','" & ViewState(ViewState_RENTALID) _
                '                                                                       & "','" & e.Row.Cells(1).Text _
                '                                                                       & "','" & linkValue & "'); return false;")
            Catch ex As Exception
                Diagnostics.Debug.Write(ex.Message)
            End Try

            ' the date thingie for cells #3 and #4
            Dim dtTemp As Date
            If Not e.Row.Cells(3).Text = "&nbsp;" Then
                dtTemp = ParseDateTime(e.Row.Cells(3).Text, SystemCulture)
                e.Row.Cells(3).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If

            If Not e.Row.Cells(4).Text = "&nbsp;" Then
                dtTemp = ParseDateTime(e.Row.Cells(4).Text.Split(" "c)(0), SystemCulture)
                e.Row.Cells(4).Text = dtTemp.ToString(UserSettings.Current.ComDateFormat) & " " & e.Row.Cells(4).Text.Split(" "c)(1)
            End If
            ' the date thingie for cells #3 and #4
        End If
    End Sub

    Protected Sub btnHiddenGridRefresher_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHiddenGridRefresher.Click
        If chkShowAll.Checked Then
            LoadGrid(True)
        Else
            LoadGrid(False)
        End If
    End Sub

#Region "rev:mia jan6,2009"
    Public Sub LoadPage()
        If isFirstLoad Then
            ViewState(ViewState_BOOKINGID) = BookingId
            ViewState(ViewState_RENTALID) = RentalId
            ViewState(ViewState_BOOKINGNUM) = BookingNumber
            ''rev:mia 08-march-2016 ttps://thlonline.atlassian.net/browse/AURORA-777
            btnAddConfirmation.Attributes.Add("OnClick", "return LoadConfirmation('','" & BookingId & "','','" & RentalId & "','" & BookingNumber & "','" & Me.RentalNo & "','" & ActiveRentalCountry & "','" & "" & "');")
            ViewState(ViewState_HIDDENREFRESHBUTTONID) = btnHiddenGridRefresher.ClientID
        End If
        LoadGrid(False)
    End Sub

    Public Property ActiveRentalCountry() As String
        Get
            Dim qrytext As String = "select dbo.getCountryforlocation('" & ViewState("ActiveRentalCountry") & "',NULL,NULL)"
            Dim countrytext As String = ""
            Try
                countrytext = Aurora.Common.Data.ExecuteScalarSQL(qrytext)
            Catch ex As Exception
            End Try
            Return countrytext
        End Get
        Set(ByVal value As String)
            ViewState("ActiveRentalCountry") = value
        End Set
    End Property
#End Region
End Class
