Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_ContactAgentMgtPopupUserControl
    Inherits BookingUserControl

    Public Delegate Sub ContactAgentMgtPopupUserControlEventHandler(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object)

    Public Event PostBack As ContactAgentMgtPopupUserControlEventHandler

    'Private _delPostBack As System.Delegate

    Private Const ContactAgentMgtPopupUserControl_AgentId_ViewState = "ContactAgentMgtPopupUserControl_AgentId"

    'Private _agentId As String
    Public Property AgentId() As String
        Get
            Return ViewState(ContactAgentMgtPopupUserControl_AgentId_ViewState)
            '       Return _agentId
        End Get
        Set(ByVal value As String)
            '      _agentId = value
            ViewState(ContactAgentMgtPopupUserControl_AgentId_ViewState) = value
        End Set
    End Property

    'Public WriteOnly Property PostBack() As System.Delegate
    '    Set(ByVal Value As System.Delegate)
    '        _delPostBack = Value
    '    End Set
    'End Property


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then

            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    'Protected Sub typeDropDownList_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        Dim typeDropDownList As DropDownList = noteFormView.FindControl("typeDropDownList")

    '        Dim dt As DataTable = BookingNotes.GetNoteType()
    '        typeDropDownList.DataSource = dt
    '        typeDropDownList.DataTextField = "DESCRIPTION"
    '        typeDropDownList.DataValueField = "ID"
    '    Catch ex As Exception
    '        CurrentPage.LogException(ex)
    '        CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
    '    End Try
    'End Sub

    'Protected Sub audienceDropDownList_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        Dim audienceDropDownList As DropDownList = noteFormView.FindControl("audienceDropDownList")

    '        Dim dt As DataTable = BookingNotes.GetAudienceType()
    '        audienceDropDownList.DataSource = dt
    '        audienceDropDownList.DataTextField = "DESCRIPTION"
    '        audienceDropDownList.DataValueField = "ID"
    '    Catch ex As Exception
    '        CurrentPage.LogException(ex)
    '        CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
    '    End Try
    'End Sub


    Public Sub LoadPopup()
        Try
            'noteFormView.ChangeMode(FormViewMode.Insert)

            'Dim ds As DataSet
            'ds = BookingNotes.GetBookingNote("", BookingId, RentalId)
            'noteFormView.DataSource = ds.Tables(0)
            'noteFormView.DataBind()

            'GetCodecodetype(0)
            'agentIdLabel.Text = AgentId

            GetContactAgentDetails()

            programmaticModalPopup.Show()
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub




    Private Sub GetContactAgentDetails()

        'exec sp_getAgentContact 'A18D9FC9-54AF-44A0-8388-19789C9F5DE8'
        Dim ds As DataSet
        Try
            ds = Agent.GetAgentContact(AgentId)
            agentTextBox.Text = ds.Tables(1).Rows(0)("AgnName")
            agentTypeTextBox.Text = ds.Tables(1).Rows(0)("AgnType")

            contactGridView.DataSource = ds.Tables(3)
            contactGridView.DataBind()

        Catch ex As Exception
            Me.saveButton.Enabled = False
        End Try

    End Sub


    Protected Sub contactGridView_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles contactGridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ds As DataSet
                ds = Aurora.Common.Data.GetCodecodetype(25, "")

                Dim typeIdDropDownList As DropDownList = e.Row.FindControl("typeIdDropDownList")
                typeIdDropDownList.DataSource = ds.Tables(0)
                typeIdDropDownList.DataTextField = "CODE"
                typeIdDropDownList.DataValueField = "ID"
                typeIdDropDownList.DataBind()
                typeIdDropDownList.SelectedValue = e.Row.DataItem("TypeId")
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            contactAgentMgtPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading the page.")
            'CurrentPage.AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub



    'Public Sub loadEditTemplate(ByVal noteId As String)

    '    noteFormView.ChangeMode(FormViewMode.Edit)

    '    Dim ds As DataSet
    '    ds = BookingNotes.GetBookingNote(noteId, "", "")
    '    noteFormView.DataSource = ds.Tables(0)
    '    noteFormView.DataBind()

    '    programmaticModalPopup.Show()

    'End Sub


    'Protected Sub noteFormView_DataBound(ByVal sender As Object, ByVal e As EventArgs)
    '    Try
    '        If noteFormView.CurrentMode = FormViewMode.Edit Then
    '            Dim activeCheckBox As CheckBox = noteFormView.FindControl("activeCheckBox")
    '            activeCheckBox.Checked = Utility.ParseInt(noteFormView.DataSource.rows(0)("IsActive"))

    '        ElseIf noteFormView.CurrentMode = FormViewMode.Insert Then
    '            Dim typeDropDownList As DropDownList = noteFormView.FindControl("typeDropDownList")
    '            typeDropDownList.SelectedValue = "471AEF57-626D-434A-ACA8-9E5E9112AC3F" ' "Rental"

    '            Dim rentalNoTextBox As TextBox = noteFormView.FindControl("rentalNoTextBox")
    '            rentalNoTextBox.Text = RentalNo
    '            Dim bookingIdTextBox As TextBox = noteFormView.FindControl("bookingIdTextBox")
    '            bookingIdTextBox.Text = BookingId
    '        End If
    '    Catch ex As Exception
    '        CurrentPage.LogException(ex)
    '        CurrentPage.AddErrorMessage("An error occurred while loading the booking notes tab.")
    '    End Try
    'End Sub

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles saveButton.Click
        saveContactAgents()
        programmaticModalPopup.Hide()

        'Invoke PopupPostBack event to redatabind the grid
        '_delPostBack.DynamicInvoke(True)
        RaiseEvent PostBack(Me, True, False, True)
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        programmaticModalPopup.Show()
        'AgentId = agentIdLabel.Text
        GetContactAgentDetails()
    End Sub

    'Protected Sub backButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles backButton.Click
    '    programmaticModalPopup.Hide()
    '    RaiseEvent PostBack(Me, True, False, True)
    'End Sub

    Private Sub saveContactAgents()

        Dim col As Collection = New Collection

        For Each r As GridViewRow In contactGridView.Rows

            Dim sList As ArrayList = New ArrayList

            '0<ContactId>B8117C033AD040F0BC78F27FEB0C88DC</ContactId>
            '1<TypeId>53EB5FA6BA564964B520BE458F6594E1</TypeId>
            '2<LName>Leong</LName>
            '3<FName>Jack</FName>
            '4<Title>Mr</Title>
            '5<Position></Position>
            '6<Comment></Comment>
            '7<IntegrityNo>2</IntegrityNo>
            '8<Remove>0</Remove
            '9<EmailPhnId>A5FB77E275B847AFA00C0456DDAA96B8</EmailPhnId>
            '10<EmailId>A2634A50296946A4BFAAA77056AEB634</EmailId>
            '11<Email></Email>
            '12<EmailIntegrityNo>3</EmailIntegrityNo>
            '13<FaxPhnId>A51EC9BF9FDB482490CC11D36A93BEC6</FaxPhnId>
            '14<FaxId>E17DA52692AE4488B5E562EAC43D2D29</FaxId>
            '15<FaxACode></FaxACode>
            '16<FaxCCode></FaxCCode>
            '17<FaxNo></FaxNo>
            '18<FaxIntegrityNo>3</FaxIntegrityNo>
            '19<PhonePhnId>9D27472F18B44283BFF15B622B8FEAB0</PhonePhnId>
            '20<PhoneId>4AF1D888FF0C4538BA1465374D294308</PhoneId>
            '21<PhoneACode>1</PhoneACode>
            '22<PhoneCCode>2</PhoneCCode>
            '23<PhoneNo>3</PhoneNo>
            '24<PhoneIntegrityNo>3</PhoneIntegrityNo>
            '</ConData>

            Dim contactIdLabel As Label = r.FindControl("contactIdLabel")
            sList.Insert(0, contactIdLabel.Text)
            Dim typeIdDropDownList As DropDownList = r.FindControl("typeIdDropDownList")
            sList.Insert(1, typeIdDropDownList.SelectedValue)
            Dim lNameTextBox As TextBox = r.FindControl("lNameTextBox")
            sList.Insert(2, lNameTextBox.Text)
            Dim fNameTextBox As TextBox = r.FindControl("fNameTextBox")
            sList.Insert(3, fNameTextBox.Text)
            Dim titleTextBox As TextBox = r.FindControl("titleTextBox")
            sList.Insert(4, titleTextBox.Text)
            Dim positionTextBox As TextBox = r.FindControl("positionTextBox")
            sList.Insert(5, positionTextBox.Text)
            Dim commentTextBox As TextBox = r.FindControl("commentTextBox")
            sList.Insert(6, commentTextBox.Text)
            Dim integrityNoLabel As Label = r.FindControl("integrityNoLabel")
            sList.Insert(7, IntegrityNoLabel.Text)
            Dim removeCheckBox As CheckBox = r.FindControl("removeCheckBox")
            sList.Insert(8, removeCheckBox.checked)
            Dim emailPhnIdLabel As Label = r.FindControl("emailPhnIdLabel")
            sList.Insert(9, EmailPhnIdLabel.Text)
            Dim emailIdLabel As Label = r.FindControl("emailIdLabel")
            sList.Insert(10, EmailIdLabel.Text)
            Dim emailTextBox As TextBox = r.FindControl("emailTextBox")
            sList.Insert(11, emailTextBox.Text)
            Dim emailIntegrityNoLabel As Label = r.FindControl("emailIntegrityNoLabel")
            sList.Insert(12, emailIntegrityNoLabel.Text)
            Dim faxPhnIdLabel As Label = r.FindControl("faxPhnIdLabel")
            sList.Insert(13, FaxPhnIdLabel.Text)
            Dim faxIdLabel As Label = r.FindControl("faxIdLabel")
            sList.Insert(14, FaxIdLabel.Text)
            Dim faxACodeTextBox As TextBox = r.FindControl("faxACodeTextBox")
            sList.Insert(15, faxACodeTextBox.Text)
            Dim faxCCodeTextBox As TextBox = r.FindControl("faxCCodeTextBox")
            sList.Insert(16, faxCCodeTextBox.Text)
            Dim faxNoTextBox As TextBox = r.FindControl("faxNoTextBox")
            sList.Insert(17, faxNoTextBox.Text)
            Dim faxIntegrityNoLabel As Label = r.FindControl("faxIntegrityNoLabel")
            sList.Insert(18, FaxIntegrityNoLabel.Text)
            Dim phonePhnIdLabel As Label = r.FindControl("phonePhnIdLabel")
            sList.Insert(19, PhonePhnIdLabel.Text)
            Dim phoneIdLabel As Label = r.FindControl("phoneIdLabel")
            sList.Insert(20, PhoneIdLabel.Text)
            Dim phoneACodeTextBox As TextBox = r.FindControl("phoneACodeTextBox")
            sList.Insert(21, phoneACodeTextBox.Text)
            Dim phoneCCodeTextBox As TextBox = r.FindControl("phoneCCodeTextBox")
            sList.Insert(22, phoneCCodeTextBox.Text)
            Dim phoneNoTextBox As TextBox = r.FindControl("phoneNoTextBox")
            sList.Insert(23, phoneNoTextBox.Text)
            Dim phoneIntegrityNoLabel As Label = r.FindControl("phoneIntegrityNoLabel")
            sList.Insert(24, phoneIntegrityNoLabel.Text)

            If Not (String.IsNullOrEmpty(contactIdLabel.Text) And String.IsNullOrEmpty(typeIdDropDownList.SelectedValue) And _
                String.IsNullOrEmpty(lNameTextBox.Text) And String.IsNullOrEmpty(fNameTextBox.Text) And _
                String.IsNullOrEmpty(titleTextBox.Text) And String.IsNullOrEmpty(positionTextBox.Text) And _
                String.IsNullOrEmpty(commentTextBox.Text) And String.IsNullOrEmpty(emailTextBox.Text) And _
                String.IsNullOrEmpty(phoneACodeTextBox.Text) And String.IsNullOrEmpty(phoneCCodeTextBox.Text) And _
                String.IsNullOrEmpty(phoneNoTextBox.Text) And String.IsNullOrEmpty(faxACodeTextBox.Text) And _
                String.IsNullOrEmpty(faxCCodeTextBox.Text) And String.IsNullOrEmpty(faxNoTextBox.Text)) Then

                col.Add(sList)
            End If

        Next

        'AgentId = agentIdLabel.Text
        Aurora.Booking.Services.Agent.UpdateAgentContacts(AgentId, CurrentPage.UserCode, CurrentPage.Page.ToString(), col)

    End Sub

End Class
