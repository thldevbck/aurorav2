<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddNoteRatePopupUserControl.ascx.vb"
    Inherits="Booking_Controls_AddNoteRatePopUpUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc1" %>

<script type="text/javascript">
        
        function showAddNoteRatePopupViaClient() {      
            var addNoteRatePopupBehavior = $find('programmaticAddNoteRatePopupBehavior');
            addNoteRatePopupBehavior.show();
        }
        
        function hideAddNoteRatePopupViaClient() {    
            var addNoteRatePopupBehavior = $find('programmaticAddNoteRatePopupBehavior');
            addNoteRatePopupBehavior.hide();
            return false;
        }
        
//        function submitData(){
//        
//            var noteTextBox;
//            noteTextBox = $get('<%= noteTextBox.ClientID %>');  
//            var varianceTextBox;
//            varianceTextBox = $get('<%= varianceTextBox.ClientID %>');  
//	       
//	        noteTextBox.value = trimSpace(noteTextBox.value)
//	        if (noteTextBox.value == ""){
//	            MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', "Notes text cannot be blank and should be more than 1 character!");
//		        //alert('Notes text cannot be blank and should be more than 1 character!')
//		        return false
//	        }
//	        else if (varianceTextBox.value == "" || varianceTextBox.value < 0){
//	            MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', "Rate cannot be blank and must be greater than or equal to zero!");
//		        //alert('Rate cannot be blank and must be greater than or equal to zero!')
//		        return false 
//	        }
//	        else 
//	        {
//	            var error 
//	            error = ValidateText(noteTextBox.value)
//	            if ( error != ''){
//	                MessagePanelControl.setMessagePanelError('<%= addNoteRatePopupMessagePanelControl.ClientID %>', error);
//		            return false
//	            }
//	            else
//	            {
//		            return true 
//		        }
//		    }
//        }

        // Mod:Raj - 16 may, 2011

        function ClientVal() {
            var boolValidate = true;
            var noteTextBox;
            noteTextBox = $get('<%= noteTextBox.ClientID %>');
            
            if (!validateControl(noteTextBox)) {
                noteTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                noteTextBox.style.background = "white";
            }

            var varianceTextBox;
            varianceTextBox = $get('<%= varianceTextBox.ClientID %>');

            if (!validateControl(varianceTextBox)) {
                varianceTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
                boolValidate = false;
            }
            else {
                varianceTextBox.style.background = "white";
            }


            if (boolValidate == false) {
                return false;
            }

            return true;

        }

        function validateControl(obj) {
            if (obj.value == "") {
                //setWarningShortMessage(getErrorMessage("GEN005"))
                return false
            }
            else {
                return true
            }
        }        

</script>

<asp:Button runat="server" ID="hiddenTargetControlForAddNoteRatePopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticAddNoteRatePopup"
    BehaviorID="programmaticAddNoteRatePopupBehavior" TargetControlID="hiddenTargetControlForAddNoteRatePopup"
    PopupControlID="programmaticAddNoteRatePopupPanel" BackgroundCssClass="modalBackground"
    DropShadow="True" PopupDragHandleControlID="programmaticAddNoteRatePopupDragHandle">
</ajaxToolkit:ModalPopupExtender>

<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticAddNoteRatePopupPanel"
    Style="display: none; width: 400px; height: 250px">
    
    <asp:Panel runat="Server" ID="programmaticAddNoteRatePopupDragHandle" CssClass="modalPopupTitle">
        <asp:Label ID="titleLabel" runat="server"></asp:Label>
    </asp:Panel>
    
    <br />
    <uc1:MessagePanelControl ID="addNoteRatePopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />
    
    <table width=95%>
        <tr>
            <td colspan="2" align="left" id="tdMessageHolder">
                <asp:Label ID="textLabel" runat="server" ></asp:Label>
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td width="30%" valign="top" >
                Note:</td>
            <td width="70%">
                <asp:TextBox ID="noteTextBox" runat="server" TextMode="multiLine" Rows="5" Width="90%"></asp:TextBox>&nbsp;*</td>
        </tr>
        <tr>
            <td>
                Changeover Variance:</td>
            <td>
                <asp:TextBox ID="varianceTextBox" runat="server"></asp:TextBox>&nbsp;*</td>
        </tr>
        <tr>
            <td align="right" colspan=2>
                <asp:Button ID="okButton" runat="server" CssClass="Button_Standard Button_OK" Text="OK" OnClientClick="return ClientVal();" />
                <asp:Button ID="cancelButton" runat="server" CssClass="Button_Standard Button_Reset" Text="Reset" />
            </td>
        </tr>
    </table>
</asp:Panel>
