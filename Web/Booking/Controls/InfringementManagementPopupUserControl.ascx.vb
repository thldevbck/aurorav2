
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_InfringementManagementPopupUserControl
    Inherits BookingUserControl

    Private Const InfringementManagementPopupUserControl_Xml_ViewState = "InfringementManagementPopupUserControl_Xml"
    Private Const InfringementManagementPopupUserControl_IcdId_ViewState = "InfringementManagementPopupUserControl_IcdId"

    Private Const InfringementManagementPopupUserControl_bookedProdId_ViewState = "InfringementManagementPopupUserControl_bookedProdId"
    Private Const InfringementManagementPopupUserControl_iid_ViewState = "InfringementManagementPopupUserControl_iid"

    Public Delegate Sub InfringementManagementPopupUserControlEventHandler(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object)

    Public Event PostBack As InfringementManagementPopupUserControlEventHandler

    'Private _delPostBack As System.Delegate
    'Public WriteOnly Property PostBack() As System.Delegate
    '    Set(ByVal Value As System.Delegate)
    '        _delPostBack = Value
    '    End Set
    'End Property

    'Private _icdId As String
    Public Property IcdId() As String
        Get
            Return ViewState.Item(InfringementManagementPopupUserControl_IcdId_ViewState)
        End Get
        Set(ByVal Value As String)
            ViewState.Add(InfringementManagementPopupUserControl_IcdId_ViewState, Value)
            '_icdId = Value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                statusDropDownList.Attributes.Add("onchange", "javascript:return checkStatus('" + statusDropDownList.ClientID + "','" + resolvedDateDateControl.ClientID + "');")
                statusDropDownList.Attributes.Add("onblur", "javascript:return valStatus('" + statusDropDownList.ClientID + "','" + resolvedDateDateControl.ClientID + "','" + todayHidden.ClientID + "');")
                paymentReferenceTextBox.Attributes.Add("onblur", "javascript:return valReference('" + paymentReferenceTextBox.ClientID + "','" + paymentReceivedDateControl.ClientID + "','" + todayHidden.ClientID + "');")

            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking infringement tab.")
        End Try
    End Sub

    Protected Sub notifiedDropDownList_DataBinding()
        Try
            notifiedDropDownList.Items.Clear()
            notifiedDropDownList.Items.Add("")
            Dim dt As DataTable = BookingInfringement.GetNotified()
            'Dim notifiedDropDownList As DropDownList = infringementFormView.FindControl("notifiedDropDownList")
            notifiedDropDownList.DataSource = dt
            notifiedDropDownList.DataTextField = "DESCRIPTION"
            notifiedDropDownList.DataValueField = "ID"
            notifiedDropDownList.DataBind()
        Catch ex As Exception
            CurrentPage.LogException(ex)
            infringementManagementPopupMessagePanelControl.SetMessagePanelError("An error occurred while loading the booking infringement tab.")
        End Try
    End Sub

    Public Sub loadInsertTemplate()

        IcdId = ""
        notifiedDropDownList_DataBinding()
        formDataBind()
        programmaticInfringementPopup.Show()

    End Sub

    Public Sub loadEditTemplate(ByVal id As String)

        IcdId = id
        notifiedDropDownList_DataBinding()
        formDataBind()
        programmaticInfringementPopup.Show()

    End Sub

    Private Sub formDataBind()

        Dim oXmlDoc As XmlDocument
        oXmlDoc = BookingInfringement.GetInfringement(IcdId, BookingId, RentalId, CurrentPage.UserCode)

        ViewState.Add(InfringementManagementPopupUserControl_Xml_ViewState, oXmlDoc.InnerXml)

        Dim ds As New DataSet
        ds.ReadXml(New XmlNodeReader(oXmlDoc.DocumentElement))

        'causeCheckBoxList
        If ds.Tables.Count >= 3 Then
            causeCheckBoxList.DataSource = ds.Tables(1)
            causeCheckBoxList.DataTextField = "SbcDesc"
            causeCheckBoxList.DataValueField = "SbcId"
            causeCheckBoxList.DataBind()

            For Each r As DataRow In ds.Tables(1).Rows
                If r.Item("isselected") = "1" Then
                    For Each l As ListItem In causeCheckBoxList.Items
                        If l.Value = r.Item("SbcId") Then
                            l.Selected = True
                        End If
                    Next
                End If
            Next

            'bookingNoLabel.Text = ds.Tables(2).Rows(0)("bno")
            'rentalNoLabel.Text = ds.Tables(2).Rows(0)("rno")
            bookingNoTextBox.Text = ds.Tables(2).Rows(0)("bno")
            rentalNoTextBox.Text = ds.Tables(2).Rows(0)("rno")
            unitNoTextBox.Text = ds.Tables(2).Rows(0)("uno")
            regoTextBox.Text = ds.Tables(2).Rows(0)("rgno")
            'vehicleLabel.Text = ds.Tables(2).Rows(0)("prd")
            vehicleTextBox.Text = ds.Tables(2).Rows(0)("prd")


            branchPickerControl.Text = ds.Tables(2).Rows(0)("brn")

            If Not String.IsNullOrEmpty(ds.Tables(2).Rows(0)("dtlogwhen")) Then
                dateDateControl.Date = Utility.ParseDateTime(ds.Tables(2).Rows(0)("dtlogwhen"), Utility.systemCulture)
            Else
                dateDateControl.Text = ""
            End If

            If Not String.IsNullOrEmpty(ds.Tables(2).Rows(0)("tmlogwhen")) Then
                timeTimeControl.Text = ds.Tables(2).Rows(0)("tmlogwhen")
            Else
                timeTimeControl.Text = ""
            End If

            notifiedDropDownList.SelectedValue = ds.Tables(2).Rows(0)("notify")
            datailTextBox.Text = ds.Tables(2).Rows(0)("det")

            referenceTextBox.Text = ds.Tables(2).Rows(0)("ref")

            ''rev:mis
            ''infringementAmtTextBox.Text = Convert.ToDouble(ds.Tables(2).Rows(0)("amt"))
            infringementAmtTextBox.Text = checkDBvalue(ds.Tables(2).Rows(0)("amt").ToString)

            ''rev:mis
            ''adminFeeTextBox.Text = Convert.ToDouble(ds.Tables(2).Rows(0)("adm"))
            adminFeeTextBox.Text = checkDBvalue(ds.Tables(2).Rows(0)("adm").ToString)

            If Not String.IsNullOrEmpty(ds.Tables(2).Rows(0)("duedte")) Then
                infringementDueDateControl.Date = Utility.ParseDateTime(ds.Tables(2).Rows(0)("duedte"), Utility.systemCulture)
            Else
                infringementDueDateControl.Text = ""
            End If
            If Not String.IsNullOrEmpty(ds.Tables(2).Rows(0)("notdte")) Then
                customerAdvisedDateControl.Date = Utility.ParseDateTime(ds.Tables(2).Rows(0)("notdte"), Utility.systemCulture)
            Else
                customerAdvisedDateControl.Text = ""
            End If

            statusDropDownList.SelectedValue = ds.Tables(2).Rows(0)("sts")
            If Not String.IsNullOrEmpty(ds.Tables(2).Rows(0)("resdte")) Then
                resolvedDateDateControl.Date = Utility.ParseDateTime(ds.Tables(2).Rows(0)("resdte"), Utility.systemCulture)
            Else
                resolvedDateDateControl.Text = ""
            End If

            paymentReferenceTextBox.Text = ds.Tables(2).Rows(0)("notref")
            If Not String.IsNullOrEmpty(ds.Tables(2).Rows(0)("recvd")) Then
                paymentReceivedDateControl.Date = Utility.ParseDateTime(ds.Tables(2).Rows(0)("recvd"), Utility.systemCulture)
            Else
                paymentReceivedDateControl.Text = ""
            End If

            'Hidden field
            iidHidden.Value = ds.Tables(2).Rows(0)("iid")
            todayHidden.Value = ds.Tables(2).Rows(0)("today")
            Try
                ''rev:mia oct31
                ''not all records has a bpdid so trapped it
                If oXmlDoc.SelectSingleNode("data/Incident/bpid") IsNot Nothing Then
                    bookedProdIdHidden.Value = ds.Tables(2).Rows(0)("bpid")
                End If

            Catch ex As Exception
                bookedProdIdHidden.Value = ""
            End Try

            ViewState(InfringementManagementPopupUserControl_bookedProdId_ViewState) = ds.Tables(2).Rows(0)("bpid")
            ViewState(InfringementManagementPopupUserControl_iid_ViewState) = ds.Tables(2).Rows(0)("iid")

        End If

    End Sub

#Region "Validation"

    Protected Sub unitNoTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles unitNoTextBox.TextChanged
        programmaticInfringementPopup.Show()
        populateGeneric(unitNoTextBox, "UNITNO")
    End Sub

    Protected Sub regoTextBox_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles regoTextBox.TextChanged
        programmaticInfringementPopup.Show()
        populateGeneric(regoTextBox, "REGNO")
    End Sub

    Private Sub populateGeneric(ByRef control As TextBox, ByVal type As String)
        control.Text = Trim(control.Text)
        If String.IsNullOrEmpty(control.Text) Then
            bookedProdIdHidden.Value = ""
            ViewState(InfringementManagementPopupUserControl_bookedProdId_ViewState) = ""
            regoTextBox.Text = ""
            unitNoTextBox.Text = ""
            'vehicleLabel.Text = ""
            vehicleTextBox.Text = ""
        Else
            Dim dt As DataTable
            dt = BookingInfringement.GetDataFromRentalId(RentalId, ViewState(InfringementManagementPopupUserControl_iid_ViewState), control.Text, type)

            'dt = BookingInfringement.GetDataFromRentalId(RentalId, iidHidden.Value, control.Text, type)

            bookedProdIdHidden.Value = dt.Rows(0)("bpid")
            ViewState(InfringementManagementPopupUserControl_bookedProdId_ViewState) = dt.Rows(0)("bpid")

            regoTextBox.Text = dt.Rows(0)("regno")
            unitNoTextBox.Text = dt.Rows(0)("unitno")
            'vehicleLabel.Text = dt.Rows(0)("prd")
            vehicleTextBox.Text = dt.Rows(0)("prd")

            If String.IsNullOrEmpty(unitNoTextBox.Text) Then
                infringementManagementPopupMessagePanelControl.SetMessagePanelWarning("No Valid Product Found")
                bookedProdIdHidden.Value = ""
                ViewState(InfringementManagementPopupUserControl_bookedProdId_ViewState) = ""
                regoTextBox.Text = ""
                'vehicleLabel.Text = ""
                vehicleTextBox.Text = ""
            End If
        End If

    End Sub

    Protected Sub dateDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dateDateControl.DateChanged
        programmaticInfringementPopup.Show()
        checkDateNotFuture(dateDateControl, "Date cannot be greater than todays date")
    End Sub

    Protected Sub customerAdvisedDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles customerAdvisedDateControl.DateChanged
        programmaticInfringementPopup.Show()
        checkDateNotFuture(customerAdvisedDateControl, "Customer advised date cannot be greater than todays date")
    End Sub

    Protected Sub resolvedDateDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles resolvedDateDateControl.DateChanged
        programmaticInfringementPopup.Show()
        checkDateNotFuture(resolvedDateDateControl, "Resolved date cannot be greater than todays date")
    End Sub

    Protected Sub paymentReceivedDateControl_DateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles paymentReceivedDateControl.DateChanged
        programmaticInfringementPopup.Show()
        checkDateNotFuture(paymentReceivedDateControl, "Received date cannot be greater than todays date")
    End Sub

    Private Sub checkDateNotFuture(ByRef dateControl As UserControls_DateControl, ByVal errorMessage As String)

        If dateControl.IsValid Then
            If Not dateControl.Date = Date.MinValue Then
                Dim today As Date
                today = Utility.ParseDateTime(todayHidden.Value, Utility.SystemCulture)

                If dateControl.Date > today Then
                    infringementManagementPopupMessagePanelControl.SetMessagePanelWarning(errorMessage)
                End If
            End If
        Else
            infringementManagementPopupMessagePanelControl.SetMessagePanelWarning("Enter date in dd/MM/yyyy format")
        End If

    End Sub

#End Region

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles saveButton.Click
        programmaticInfringementPopup.Show()
        If validateFields() Then
            saveInfringement()
        End If
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click

        programmaticInfringementPopup.Hide()
        'Invoke PopupPostBack event to redatabind the grid
        '_delPostBack.DynamicInvoke(True)
        RaiseEvent PostBack(Me, True, False, True)

    End Sub

    Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newButton.Click
        programmaticInfringementPopup.Show()
        If validateFields() Then
            saveInfringement()
            IcdId = ""
            formDataBind()
        End If
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        programmaticInfringementPopup.Show()
        formDataBind()
    End Sub

    Private Function validateFields()

        Dim returnValue As Boolean = True

        If Not (dateDateControl.IsValid And _
            infringementDueDateControl.IsValid And customerAdvisedDateControl.IsValid _
            And resolvedDateDateControl.IsValid And paymentReceivedDateControl.IsValid) Then
            infringementManagementPopupMessagePanelControl.SetMessagePanelWarning("Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
            returnValue = False
        ElseIf Not timeTimeControl.IsValid Then
            infringementManagementPopupMessagePanelControl.SetMessagePanelWarning("Enter time in hh:mm format.")
            returnValue = False
        ElseIf String.IsNullOrEmpty(branchPickerControl.DataId) Or _
            String.IsNullOrEmpty(bookingNoTextBox.Text) Or _
            String.IsNullOrEmpty(rentalNoTextBox.Text) Or _
            String.IsNullOrEmpty(unitNoTextBox.Text) Or _
            String.IsNullOrEmpty(notifiedDropDownList.SelectedValue) Or _
            String.IsNullOrEmpty(referenceTextBox.Text) Or _
            String.IsNullOrEmpty(infringementAmtTextBox.Text) Or _
            String.IsNullOrEmpty(datailTextBox.Text) Then


            infringementManagementPopupMessagePanelControl.SetMessagePanelWarning("Please enter all Mandatory fields Marked *")
            returnValue = False

        ElseIf dateDateControl.Date = Date.MinValue Or timeTimeControl.Time = TimeSpan.MinValue Then
            infringementManagementPopupMessagePanelControl.SetMessagePanelWarning("Invalid Logged Date/Time")
            returnValue = False
        End If

        Return returnValue

    End Function

    Private Sub saveInfringement()

        infringementAmtTextBox.Text = Trim(infringementAmtTextBox.Text)
        adminFeeTextBox.Text = Trim(adminFeeTextBox.Text)

        Dim xml As String
        xml = ViewState.Item(InfringementManagementPopupUserControl_Xml_ViewState)
        xml = xml.Replace("<data>", "<Root>")
        xml = xml.Replace("</data>", "<Parameters><sAction>M</sAction><usercode>" & CurrentPage.UserCode _
            & "</usercode><addprgmname>Booking.aspx</addprgmname></Parameters></Root>")

        Dim oXmlDoc As New XmlDocument
        oXmlDoc.LoadXml(xml)

        Dim node As XmlNode

        node = oXmlDoc.SelectSingleNode("/Root/Incident/uno")
        node.InnerText = unitNoTextBox.Text
        node = oXmlDoc.SelectSingleNode("/Root/Incident/rgno")
        node.InnerText = regoTextBox.Text
        node = oXmlDoc.SelectSingleNode("/Root/Incident/prd")
        'node.InnerText = vehicleLabel.Text
        node.InnerText = vehicleTextBox.Text

        node = oXmlDoc.SelectSingleNode("/Root/Incident/brn")
        node.InnerText = branchPickerControl.DataId

        node = oXmlDoc.SelectSingleNode("/Root/Incident/dtlogwhen")
        If Not dateDateControl.Date = Date.MinValue Then
            node.InnerText = Utility.ParseString(dateDateControl.Date, "")
        Else
            node.InnerText = ""
        End If

        node = oXmlDoc.SelectSingleNode("/Root/Incident/tmlogwhen")
        If Not timeTimeControl.Time = TimeSpan.MinValue Then
            node.InnerText = Utility.ParseString(timeTimeControl.Time, "")
        Else
            node.InnerText = ""
        End If

        node = oXmlDoc.SelectSingleNode("/Root/Incident/notify")
        node.InnerText = notifiedDropDownList.SelectedValue
        node = oXmlDoc.SelectSingleNode("/Root/Incident/det")
        node.InnerText = datailTextBox.Text
        node = oXmlDoc.SelectSingleNode("/Root/Incident/ref")
        node.InnerText = referenceTextBox.Text
        node = oXmlDoc.SelectSingleNode("/Root/Incident/amt")
        node.InnerText = infringementAmtTextBox.Text
        node = oXmlDoc.SelectSingleNode("/Root/Incident/adm")
        node.InnerText = adminFeeTextBox.Text

        node = oXmlDoc.SelectSingleNode("/Root/Incident/duedte")
        If Not infringementDueDateControl.Date = Date.MinValue Then
            node.InnerText = Utility.ParseString(infringementDueDateControl.Date, "")
        Else
            node.InnerText = ""
        End If
        node = oXmlDoc.SelectSingleNode("/Root/Incident/notdte")
        If Not customerAdvisedDateControl.Date = Date.MinValue Then
            node.InnerText = Utility.ParseString(customerAdvisedDateControl.Date, "")
        Else
            node.InnerText = ""
        End If

        node = oXmlDoc.SelectSingleNode("/Root/Incident/sts")
        node.InnerText = statusDropDownList.SelectedValue

        node = oXmlDoc.SelectSingleNode("/Root/Incident/duedte")
        If Not resolvedDateDateControl.Date = Date.MinValue Then
            node.InnerText = Utility.ParseString(resolvedDateDateControl.Date, "")
        Else
            node.InnerText = ""
        End If

        node = oXmlDoc.SelectSingleNode("/Root/Incident/notref")
        node.InnerText = paymentReferenceTextBox.Text

        node = oXmlDoc.SelectSingleNode("/Root/Incident/recvd")
        If Not paymentReceivedDateControl.Date = Date.MinValue Then
            node.InnerText = Utility.ParseString(paymentReceivedDateControl.Date, "")
        Else
            node.InnerText = ""
        End If

        node = oXmlDoc.SelectSingleNode("/Root/Incident/resdte")
        If Not resolvedDateDateControl.Date = Date.MinValue Then
            node.InnerText = Utility.ParseString(resolvedDateDateControl.Date, "")
        Else
            node.InnerText = ""
        End If

        node = oXmlDoc.SelectSingleNode("/Root/Incident/bpid")
        node.InnerText = ViewState(InfringementManagementPopupUserControl_bookedProdId_ViewState)
        node.InnerText = bookedProdIdHidden.Value

        'causeCheckBoxList
        For Each l As ListItem In causeCheckBoxList.Items
            Dim nodeList As XmlNodeList
            nodeList = oXmlDoc.SelectNodes("/Root/cause/subcause")
            Dim nodeEnum As IEnumerator = nodeList.GetEnumerator()
            nodeEnum.Reset()
            Dim subcauseNode As XmlNode
            While nodeEnum.MoveNext()
                subcauseNode = nodeEnum.Current()

                If subcauseNode.ChildNodes(0).InnerText = l.Value Then
                    subcauseNode.ChildNodes(2).InnerText = Convert.ToInt32(l.Selected)
                End If
            End While
        Next


        Dim returnMessage As String = ""
        returnMessage = BookingInfringement.ManageInfringement(oXmlDoc)

        '==== Check for error/success ====
        Dim errorSrc As String()
        errorSrc = Split(returnMessage, "/")

        Dim errorNo As String = errorSrc(0)
        Dim errorDesc As String = errorSrc(1)

        If String.IsNullOrEmpty(errorDesc) Then
            errorDesc = "Success"
            infringementManagementPopupMessagePanelControl.SetMessagePanelInformation(errorDesc)
        End If

        Select Case errorNo.Trim()
            Case "GEN045", "GEN046"
                infringementManagementPopupMessagePanelControl.SetMessagePanelInformation(errorDesc)
                IcdId = errorSrc(2)
            Case Else
                infringementManagementPopupMessagePanelControl.SetMessagePanelError(errorDesc)
        End Select

    End Sub

    ''rev:mia 
    Function checkDBvalue(ByVal strvalue As String) As String
        If String.IsNullOrEmpty(strvalue) Then
            Return ""
        Else
            Try
                Return Convert.ToDouble(strvalue)
            Catch ex As Exception
                Return ""
            End Try
        End If
    End Function
End Class

