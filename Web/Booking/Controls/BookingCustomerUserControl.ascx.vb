''change control
''REV:MIA MAY 16 2011 -- addition of EmailAddress
''REV:MIA SEP.4 2013 -- Addition of Country pickercontrol and changing the textbox for passport country to pickercontrol
''      

Imports Aurora.Common.Utility
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml
Imports Aurora.Booking.Data
Imports System.Drawing


Partial Class Booking_Controls_BookingCustomerUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False
    Public Const BookingCustomer_RentalCustomer_SessionName As String = "BookingCustomer_RentalCustomer"
    Public Const BookingCustomer_OtherBookingCustomer_SessionName As String = "BookingCustomer_OtherBookingCustomer"
    Public Const BookingCustomer_RemoveCustomer_SessionName As String = "BookingCustomer_RemoveCustomer"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then

                Dim dt1 As DataTable
                dt1 = BookingCustomer.GetCustomerSummary(True, BookingId, RentalId)
                rentalCustomerGridView.DataSource = dt1
                rentalCustomerGridView.DataBind()

                If dt1.Rows.Count <= 0 Then
                    removeButton.Enabled = False
                    dt1 = New DataTable()
                    dt1.Columns.Add("CusId")
                    dt1.Columns.Add("CName")
                    dt1.Columns.Add("Address")

                    dt1.Columns.Add("Hirer")
                    dt1.Columns.Add("PriHirer")
                    dt1.Columns.Add("IsDriver")
                    dt1.Columns.Add("IssueAuthority")
                    dt1.Columns.Add("DOB")
                    dt1.Columns.Add("LicNo")
                    dt1.Columns.Add("ExpiryDt")
                    dt1.Columns.Add("Check")
                    dt1.Columns.Add("Addable")
                    dt1.Columns.Add("RntId")
                    dt1.Columns.Add("PassportNo")
                    dt1.Columns.Add("PassportCountry")
                    dt1.Columns.Add("Age")

                    ' Change by Shoel on 10.6.10 for Loyalty Card info
                    dt1.Columns.Add("LoyaltyCardNumber")
                    ' Change by Shoel on 10.6.10 for Loyalty Card info


                    ''REV:MIA MAY 16 2011
                    dt1.Columns.Add("EmailAddress")


                    emptyRentalCustomerTable.Visible = True
                Else
                    emptyRentalCustomerTable.Visible = False
                End If

                Dim dt2 As DataTable
                dt2 = BookingCustomer.GetCustomerSummary(False, BookingId, RentalId)
                otherBookingCustomerGridView.DataSource = dt2
                otherBookingCustomerGridView.DataBind()

                If dt2.Rows.Count <= 0 Then
                    addButton.Enabled = False
                    dt2 = New DataTable()
                    dt2.Columns.Add("CusId")
                    dt2.Columns.Add("CName")
                    dt2.Columns.Add("Address")
                    dt2.Columns.Add("RentalNo")
                    dt2.Columns.Add("Hirer")
                    dt2.Columns.Add("PriHirer")
                    dt2.Columns.Add("IsDriver")
                    dt2.Columns.Add("IssueAuthority")
                    dt2.Columns.Add("DOB")
                    dt2.Columns.Add("LicNo")
                    dt2.Columns.Add("ExpiryDt")
                    dt2.Columns.Add("Check")
                    dt2.Columns.Add("Addable")
                    dt2.Columns.Add("RntId")

                    emptyOtherBookingCustomerTable.Visible = True
                Else
                    emptyOtherBookingCustomerTable.Visible = False
                End If

                Session.Remove(BookingCustomer_RentalCustomer_SessionName)
                Session.Add(BookingCustomer_RentalCustomer_SessionName, dt1)

                Session.Remove(BookingCustomer_OtherBookingCustomer_SessionName)
                Session.Add(BookingCustomer_OtherBookingCustomer_SessionName, dt2)

                Session.Remove(BookingCustomer_RemoveCustomer_SessionName)
                Session.Add(BookingCustomer_RemoveCustomer_SessionName, New Collection)
                removeButton.Enabled = True
                addButton.Enabled = True
            End If

        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking customer tab.")
        End Try
    End Sub

    Protected Sub rentalCustomerGridView_RowCreated(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles rentalCustomerGridView.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim expiryDateControl As UserControls_DateControl = e.Row.FindControl("expiryDateControl")
                If Not String.IsNullOrEmpty(e.Row.DataItem("ExpiryDt")) Then
                    expiryDateControl.Date = e.Row.DataItem("ExpiryDt")
                End If

                Dim dobDateControl As UserControls_DateControl = e.Row.FindControl("dobDateControl")
                If Not String.IsNullOrEmpty(e.Row.DataItem("DOB")) Then
                    dobDateControl.Date = e.Row.DataItem("DOB")
                End If


                Dim ageLabel As Label = e.Row.FindControl("ageLabel")
                'set age
                If dobDateControl.Date <> Date.MinValue Then
                    ageLabel.Text = Utility.ParseString(Utility.GetAge(dobDateControl.Date), "")
                End If

                'JL 2008/07/10
                'Do the logic on the server side
                'Dim dateTextBox As TextBox
                'dateTextBox = dobDateControl.FindControl("dateTextBox")
                'dateTextBox.Attributes.Add("onBlur", "dobDateControlOnChange('" + dateTextBox.ClientID + "','" + ageLabel.ClientID + "');")

                ''rev:mia - use clientside script to avoid postback
                'dobDateControl.AutoPostBack = False
                'dobDateControl.Attributes.Add("onchange", "GetAge('" & dobDateControl.FindControl("dateTextBox").ClientID & "','" & ageLabel.ClientID & "');")
                'dobDateControl.Attributes.Add("onblur", "GetAge('" & dobDateControl.FindControl("dateTextBox").ClientID & "','" & ageLabel.ClientID & "');")



                Dim hirerCheckBox As CheckBox = e.Row.FindControl("hirerCheckBox")
                Dim priHirerCheckBox As CheckBox = e.Row.FindControl("priHirerCheckBox")

                ''REV:MIA MAY 16 2011 -- addition of EmailAddress
                Dim textboxEmail As TextBox = e.Row.FindControl("EmailTextBox")
                If Not textboxEmail Is Nothing And Not IsDBNull(e.Row.DataItem("EmailAddress")) Then
                    textboxEmail.Text = e.Row.DataItem("EmailAddress")
                End If

                priHirerCheckBox.Attributes.Add("OnClick", "javascript:return priHirerCheckBoxOnClick('" + hirerCheckBox.ClientID + "','" + priHirerCheckBox.ClientID + "');")
                hirerCheckBox.Attributes.Add("OnClick", "javascript:return hirerCheckBoxOnClick('" + hirerCheckBox.ClientID + "','" + priHirerCheckBox.ClientID + "');")


                If (IsDBNull(e.Row.DataItem("LoyaltyCardNumber")) = False) Then
                    Dim loyaltyCardNumberTextBox As UserControls_RegExTextBox = e.Row.FindControl("loyaltyCardNumberTextBox")
                    loyaltyCardNumberTextBox.Text = e.Row.DataItem("LoyaltyCardNumber").ToString
                End If
            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking customer tab.")
        End Try
    End Sub

    Protected Sub removeButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles removeButton.Click

        Dim rentalCustomerDt As DataTable = New DataTable
        If Not Session.Item(BookingCustomer_RentalCustomer_SessionName) Is Nothing Then
            rentalCustomerDt = Session.Item(BookingCustomer_RentalCustomer_SessionName)
        End If

        'Validate checkbox
        Dim selFlag As Boolean = False
        For Each r As GridViewRow In rentalCustomerGridView.Rows
            Dim checkCheckBox As CheckBox = r.FindControl("checkCheckBox")
            If checkCheckBox.Checked Then
                selFlag = True
                Exit For
            End If
        Next
        If Not selFlag Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "Please select record(s) to remove")
            Return
        End If

        'View state of rentalCustomerGridView
        setGirdViewState(rentalCustomerDt)
        'Dim i As Integer = 0
        'For Each r As GridViewRow In rentalCustomerGridView.Rows

        '    Dim hirerCheckBox As CheckBox = r.FindControl("hirerCheckBox")
        '    Dim priHirerCheckBox As CheckBox = r.FindControl("priHirerCheckBox")
        '    Dim driverCheckBox As CheckBox = r.FindControl("driverCheckBox")
        '    Dim licNoTextBox As TextBox = r.FindControl("licNoTextBox")
        '    Dim passportNoTextBox As TextBox = r.FindControl("passportNoTextBox")
        '    Dim issueAuthorityTextBox As TextBox = r.FindControl("issueAuthorityTextBox")
        '    Dim passportCountryTextBox As TextBox = r.FindControl("passportCountryTextBox")
        '    Dim expiryDateControl As UserControls_DateControl = r.FindControl("expiryDateControl")
        '    Dim dobDateControl As UserControls_DateControl = r.FindControl("dobDateControl")

        '    Dim rr As DataRow = rentalCustomerDt.Rows(i)
        '    rr.Item("Hirer") = Convert.ToInt32(hirerCheckBox.Checked)
        '    rr.Item("PriHirer") = Convert.ToInt32(priHirerCheckBox.Checked)
        '    rr.Item("IsDriver") = Convert.ToInt32(driverCheckBox.Checked)

        '    rr.Item("LicNo") = licNoTextBox.Text
        '    rr.Item("PassportNo") = passportNoTextBox.Text
        '    rr.Item("IssueAuthority") = issueAuthorityTextBox.Text
        '    rr.Item("PassportCountry") = passportCountryTextBox.Text

        '    rr.Item("ExpiryDt") = expiryDateControl.Text
        '    rr.Item("DOB") = dobDateControl.Text

        '    i = i + 1
        'Next


        Dim otherBookingCustomerDt As DataTable = New DataTable
        If Not Session.Item(BookingCustomer_OtherBookingCustomer_SessionName) Is Nothing Then
            otherBookingCustomerDt = Session.Item(BookingCustomer_OtherBookingCustomer_SessionName)
        End If

        Dim removeCustomerCol As Collection = New Collection
        If Not Session.Item(BookingCustomer_RemoveCustomer_SessionName) Is Nothing Then
            removeCustomerCol = Session.Item(BookingCustomer_RemoveCustomer_SessionName)
        End If

        'remove record from rentalCustomerDt
        For Each r As GridViewRow In rentalCustomerGridView.Rows
            Dim checkCheckBox As CheckBox = r.FindControl("checkCheckBox")
            If checkCheckBox.Checked Then
                Dim customerId As String
                Dim rnhIdLabel As Label
                rnhIdLabel = r.FindControl("rnhIdLabel")
                customerId = rnhIdLabel.Text

                Dim indexOfRow As Integer = -1

                For Each rr As DataRow In rentalCustomerDt.Rows
                    If rr("CusId").ToString() = customerId Then

                        Dim cusId As String = Utility.ParseString(rr("CusId"), "")
                        Dim name As String = Utility.ParseString(rr("CName"), "")
                        Dim address As String = Utility.ParseString(rr("Address"), "")
                        Dim hirer As String = Utility.ParseString(rr("Hirer"), "")
                        Dim priHirer As String = Utility.ParseString(rr("PriHirer"), "")
                        Dim isDriver As String = Utility.ParseString(rr("IsDriver"), "")
                        Dim issueAuthority As String = Utility.ParseString(rr("IssueAuthority"), "")
                        Dim dob As String = Utility.ParseString(rr("DOB"), "")
                        Dim licNo As String = Utility.ParseString(rr("LicNo"), "")
                        Dim expiryDate As String = Utility.ParseString(rr("ExpiryDt"), "")
                        Dim addable As String = Utility.ParseString(rr("Addable"), "").ToUpper
                        Dim rntId As String = Utility.ParseString(rr("RntId"), "")
                        Dim passportNo As String = ""
                        Dim passportCountry As String = ""
                        Dim rentalNo As String = rntId.Substring(rntId.IndexOf("-") + 1)

                        If addable = "Y" Then
                            otherBookingCustomerDt.Rows.Add(cusId, name, address, rentalNo, hirer, priHirer, isDriver, _
                                issueAuthority, dob, licNo, expiryDate, 0, addable, rntId)
                        End If

                        removeCustomerCol.Add(cusId)
                        indexOfRow = rentalCustomerDt.Rows.IndexOf(rr)

                        ' actual delete!
                        'Dim sErrorMessage As String = ""
                        'sErrorMessage = Aurora.Booking.Services.BookingCustomer.DeleteCustomer(cusId)
                        'If Not String.IsNullOrEmpty(sErrorMessage.Trim()) Then
                        '    CurrentPage.SetErrorMessage(sErrorMessage)
                        '    Exit Sub
                        'End If
                        ' actual delete!
                        Exit For
                    End If
                Next
                If indexOfRow > -1 Then
                    rentalCustomerDt.Rows.RemoveAt(indexOfRow)
                End If
            End If
        Next

        rentalCustomerGridView.DataSource = rentalCustomerDt
        rentalCustomerGridView.DataBind()

        If rentalCustomerDt.Rows.Count <= 0 Then
            emptyRentalCustomerTable.Visible = True
            removeButton.Enabled = False
        Else
            removeButton.Enabled = True
            emptyRentalCustomerTable.Visible = False
        End If

        otherBookingCustomerGridView.DataSource = otherBookingCustomerDt
        otherBookingCustomerGridView.DataBind()
        If otherBookingCustomerDt.Rows.Count <= 0 Then
            emptyOtherBookingCustomerTable.Visible = True
            addButton.Enabled = False
        Else
            emptyOtherBookingCustomerTable.Visible = False
            addButton.Enabled = True
        End If

    End Sub

    Protected Sub addButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addButton.Click

        Dim rentalCustomerDt As DataTable = New DataTable
        'rentalCustomerDt = rentalCustomerGridView.DataSource
        If Not Session.Item(BookingCustomer_RentalCustomer_SessionName) Is Nothing Then
            rentalCustomerDt = Session.Item(BookingCustomer_RentalCustomer_SessionName)
        End If

        Dim selFlag As Boolean = False

        For Each r As GridViewRow In otherBookingCustomerGridView.Rows
            Dim checkCheckBox As CheckBox = r.FindControl("checkCheckBox")
            If checkCheckBox.Checked Then
                selFlag = True
            End If
        Next
        If Not selFlag Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "Please select record(s) to add")
            Return
        End If


        'View state of rentalCustomerGridView
        setGirdViewState(rentalCustomerDt)


        Dim otherBookingCustomerDt As DataTable = New DataTable
        'otherBookingCustomerDt = otherBookingCustomerGridView.DataSource
        If Not Session.Item(BookingCustomer_OtherBookingCustomer_SessionName) Is Nothing Then
            otherBookingCustomerDt = Session.Item(BookingCustomer_OtherBookingCustomer_SessionName)
        End If

        'Add record to rentalCustomerDt
        For Each r As GridViewRow In otherBookingCustomerGridView.Rows
            Dim checkCheckBox As CheckBox = r.FindControl("checkCheckBox")
            If checkCheckBox.Checked Then
                Dim customerId As String
                Dim rnhIdLabel As Label
                rnhIdLabel = r.FindControl("rnhIdLabel")
                customerId = rnhIdLabel.Text

                Dim indexOfRow As Integer = -1

                For Each rr As DataRow In otherBookingCustomerDt.Rows
                    If rr("CusId").ToString() = customerId Then

                        Dim cusId As String = Utility.ParseString(rr("CusId"), "")
                        Dim name As String = Utility.ParseString(rr("CName"), "")
                        Dim address As String = Utility.ParseString(rr("Address"), "")
                        Dim hirer As String = Utility.ParseString(rr("Hirer"), "")
                        Dim priHirer As String = Utility.ParseString(rr("PriHirer"), "")
                        Dim isDriver As String = Utility.ParseString(rr("IsDriver"), "")
                        Dim issueAuthority As String = Utility.ParseString(rr("IssueAuthority"), "")
                        Dim dob As String = Utility.ParseString(rr("DOB"), "")
                        Dim licNo As String = Utility.ParseString(rr("LicNo"), "")
                        Dim expiryDate As String = Utility.ParseString(rr("ExpiryDt"), "")
                        Dim addable As String = Utility.ParseString(rr("Addable"), "")
                        Dim rntId As String = Utility.ParseString(rr("RntId"), "")
                        Dim passportNo As String = ""
                        Dim passportCountry As String = ""

                        rentalCustomerDt.Rows.Add(cusId, _
                                                  name, _
                                                  address, _
                                                  hirer, _
                                                  priHirer, _
                                                  isDriver, _
                                                  issueAuthority, _
                                                  dob, _
                                                  licNo, _
                                                  expiryDate, _
                                                  0, _
                                                  addable, _
                                                  rntId, _
                                                  passportNo, _
                                                  passportCountry)

                        indexOfRow = otherBookingCustomerDt.Rows.IndexOf(rr)
                    End If
                Next
                If indexOfRow > -1 Then
                    otherBookingCustomerDt.Rows.RemoveAt(indexOfRow)
                End If
            End If
        Next

        rentalCustomerGridView.DataSource = rentalCustomerDt
        rentalCustomerGridView.DataBind()
        If rentalCustomerDt.Rows.Count <= 0 Then
            emptyRentalCustomerTable.Visible = True
            removeButton.Enabled = False
        Else
            emptyRentalCustomerTable.Visible = False
            removeButton.Enabled = True
        End If

        otherBookingCustomerGridView.DataSource = otherBookingCustomerDt
        otherBookingCustomerGridView.DataBind()
        If otherBookingCustomerDt.Rows.Count <= 0 Then
            emptyOtherBookingCustomerTable.Visible = True
            addButton.Enabled = False
        Else
            emptyOtherBookingCustomerTable.Visible = False
            addButton.Enabled = True
        End If

    End Sub

    Private Function validateDateControl()
        For Each r As GridViewRow In rentalCustomerGridView.Rows
            Dim expiryDateControl As UserControls_DateControl = r.FindControl("expiryDateControl")
            Dim dobDateControl As UserControls_DateControl = r.FindControl("dobDateControl")

            If Not (expiryDateControl.IsValid And dobDateControl.IsValid) Then
                Return False
            End If
        Next
        Return True
    End Function

    Protected Sub saveButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveButton.Click

        If Not validateDateControl() Then
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
            Return
        End If

        Dim rentalCustomerDt As DataTable = New DataTable
        If Not Session.Item(BookingCustomer_RentalCustomer_SessionName) Is Nothing Then
            rentalCustomerDt = Session.Item(BookingCustomer_RentalCustomer_SessionName)
        End If

        'View state of the rentalCustomerGridView
        setGirdViewState(rentalCustomerDt)

        If Not isValidEmailAddress Then
            Exit Sub
        End If

        Dim removeCustomerCol As Collection = New Collection
        If Not Session.Item(BookingCustomer_RemoveCustomer_SessionName) Is Nothing Then
            removeCustomerCol = Session.Item(BookingCustomer_RemoveCustomer_SessionName)
        End If


        Dim errorMessage As String

        ''rev:mia jan 9 bypass bp2bb
        ''errorMessage = BookingCustomer.UpdateCustomerSummary(BookingId, RentalId, rentalCustomerDt, removeCustomerCol, "0", CurrentPage.UserCode, "SAVE", False)
        errorMessage = UpdateCustomerSummary(BookingId, RentalId, rentalCustomerDt, removeCustomerCol, "0", CurrentPage.UserCode, "SAVE", False)


        If errorMessage.Contains("GEN100") Then
            confimationBoxControl.Text = errorMessage
            confimationBoxControl.Show()
        Else
            If errorMessage.Contains("GEN046") Then
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, errorMessage)
            Else
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, CurrentPage.GetMessage(errorMessage))
            End If
        End If

    End Sub

    Private Sub setGirdViewState(ByRef rentalCustomerDt As DataTable)

        'Dim rentalCustomerDt As DataTable = New DataTable
        If Not Session.Item(BookingCustomer_RentalCustomer_SessionName) Is Nothing Then
            rentalCustomerDt = Session.Item(BookingCustomer_RentalCustomer_SessionName)
        End If

        'View state of the rentalCustomerGridView
        Dim i As Integer = 0
        For Each r As GridViewRow In rentalCustomerGridView.Rows

            Dim hirerCheckBox As CheckBox = r.FindControl("hirerCheckBox")
            Dim priHirerCheckBox As CheckBox = r.FindControl("priHirerCheckBox")
            Dim driverCheckBox As CheckBox = r.FindControl("driverCheckBox")
            Dim licNoTextBox As TextBox = r.FindControl("licNoTextBox")
            Dim passportNoTextBox As TextBox = r.FindControl("passportNoTextBox")
            Dim issueAuthorityTextBox As TextBox = r.FindControl("issueAuthorityTextBox")

            ''REV:MIA SEP.4 2013 -- Addition of Country pickercontrol and changing the textbox for passport country to pickercontrol
            ''Dim passportCountryTextBox As TextBox = r.FindControl("passportCountryTextBox")
            Dim CountryPickerControl As UserControls_PickerControl = r.FindControl("CountryPickerControl")

            Dim expiryDateControl As UserControls_DateControl = r.FindControl("expiryDateControl")
            Dim dobDateControl As UserControls_DateControl = r.FindControl("dobDateControl")

            ' Change by Shoel on 10.6.10 for Loyalty Card info
            Dim loyaltyCardNumberTextBox As UserControls_RegExTextBox = r.FindControl("loyaltyCardNumberTextBox")
            ' Change by Shoel on 10.6.10 for Loyalty Card info

            ''REV:MIA MAY 16 2011 -- addition of EmailAddress
            Dim textboxEmail As TextBox = r.FindControl("EmailTextBox")


            Dim rr As DataRow = rentalCustomerDt.Rows(i)
            rr.Item("Hirer") = Convert.ToInt32(hirerCheckBox.Checked)
            rr.Item("PriHirer") = Convert.ToInt32(priHirerCheckBox.Checked)
            rr.Item("IsDriver") = Convert.ToInt32(driverCheckBox.Checked)

            rr.Item("LicNo") = licNoTextBox.Text
            rr.Item("PassportNo") = passportNoTextBox.Text
            rr.Item("IssueAuthority") = issueAuthorityTextBox.Text
            ''REV:MIA SEP.4 2013 -- Addition of Country pickercontrol and changing the textbox for passport country to pickercontrol
            ''rr.Item("PassportCountry") = passportCountryTextBox.Text
            rr.Item("PassportCountry") = CountryPickerControl.Text

            ' Change by Shoel on 10.6.10 for Loyalty Card info
            rr.Item("LoyaltyCardNumber") = loyaltyCardNumberTextBox.Text
            ' Change by Shoel on 10.6.10 for Loyalty Card info

            'rr.Item("ExpiryDt") = expiryDateControl.Text
            'rr.Item("DOB") = dobDateControl.Text

            ''rev:MIA fixed issue related to 549
            Dim ExpiryDt As String = IIf(expiryDateControl.Date = Date.MinValue, "", Utility.DateDBToString(expiryDateControl.Date))
            Dim DOB As String = IIf(dobDateControl.Date = Date.MinValue, "", Utility.DateDBToString(dobDateControl.Date))
            rr.Item("ExpiryDt") = ExpiryDt
            rr.Item("DOB") = DOB

            ''Orig
            'rr.Item("ExpiryDt") = Utility.DateDBToString(expiryDateControl.Date)
            'rr.Item("DOB") = Utility.DateDBToString(dobDateControl.Date)

            ''REV:MIA MAY 16 2011 -- addition of EmailAddress
            If Not textboxEmail Is Nothing Then
                If Not String.IsNullOrEmpty(textboxEmail.Text) Then

                    If CheckEmailAddress(textboxEmail) Then
                        rr("EmailAddress") = textboxEmail.Text
                        isValidEmailAddress = True
                    Else
                        isValidEmailAddress = False
                        Exit For
                    End If

                Else
                    isValidEmailAddress = True
                    rr("EmailAddress") = String.Empty
                End If

            Else
                rr("EmailAddress") = String.Empty
                isValidEmailAddress = True
            End If
            i = i + 1
        Next

    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        isFirstLoad = True
        LoadPage()
    End Sub

    Protected Sub addCustomerButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addCustomerButton.Click
        Response.Redirect("CustomerFind.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&isFromSearch=" & IsFromSearch)
        'Response.Redirect("CustomerEdit.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId)
    End Sub

    Protected Sub addDriverButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addDriverButton.Click
        Response.Redirect("CustomerEdit.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&isDriver=1" & "&isFromSearch=" & IsFromSearch)
        'Response.Redirect("DriverMgt.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&hdBookingNum=" & BookingNumber & "&hdRentalNum=" & RentalNo)
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Response.Redirect("CustomerFind.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&isFromSearch=" & IsFromSearch)
    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        If leftButton Then
            Dim rentalCustomerDt As DataTable = New DataTable
            If Not Session.Item(BookingCustomer_RentalCustomer_SessionName) Is Nothing Then
                rentalCustomerDt = Session.Item(BookingCustomer_RentalCustomer_SessionName)
            End If

            Dim removeCustomerCol As Collection = New Collection
            If Not Session.Item(BookingCustomer_RemoveCustomer_SessionName) Is Nothing Then
                removeCustomerCol = Session.Item(BookingCustomer_RemoveCustomer_SessionName)
            End If

            Dim errorMessage As String = ""
            errorMessage = BookingCustomer.UpdateCustomerSummary(BookingId, RentalId, rentalCustomerDt, removeCustomerCol, "1", CurrentPage.UserCode, "SAVE")

            If errorMessage.Contains("GEN046") Then
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Information, errorMessage)
            Else
                CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, errorMessage)
            End If
        End If
    End Sub

    Protected Function GetCustomerUrl(ByVal customerId As Object) As String
        Dim customerIdString As String
        customerIdString = Convert.ToString(customerId)

        Return "~/Booking/CustomerEdit.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&customerId=" & customerIdString
        'Return "~/Booking/CustomerMgt.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&customerId=" & customerIdString
    End Function

    Protected Sub dobDateChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'CalC age
        Dim dobTextBox As TextBox = sender
        Dim dobDateControl As UserControls_DateControl = dobTextBox.Parent
        Dim ageLabel As Label = dobDateControl.Parent.Parent.FindControl("ageLabel")

        If dobDateControl.IsValid Then
            Dim age As Integer

            ''rev:mia Issue #549
            If Not (dobDateControl.Date = "#12:00:00 AM#") Then
                If dobDateControl.Date > Date.Today Then
                    CurrentPage.SetShortMessage(AuroraHeaderMessageType.Warning, "Customer's date of birth must be ealier than today!")
                    ageLabel.Text = ""
                Else
                    age = Utility.GetAge(dobDateControl.Date)
                    ageLabel.Text = Utility.GetAge(dobDateControl.Date)
                End If
            Else
                ageLabel.Text = ""
            End If
        End If

    End Sub

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString)
        Else
            Return ""
        End If
    End Function

#Region "rev:mia jan 20 2009"
    Public Property BypassAgeCheckingforBP2BB() As String
        Get
            Return ViewState("CheckingVehicle")
        End Get
        Set(ByVal value As String)
            ViewState("CheckingVehicle") = value.ToUpper
        End Set
    End Property
#End Region

#Region "rev:mia may 16 2011"
    Private Function UpdateCustomerSummary(ByVal bookingId As String, _
                                           ByVal rentalId As String, _
                                           ByVal rentalCustomerDt As DataTable, _
                                           ByVal removeCustomerCol As Collection, _
                                           ByVal msg As String, _
                                           ByVal userCode As String, _
                                           ByVal programName As String, _
                                           Optional ByVal isBp2bb As Boolean = False) As String

        'Validations
        Dim noOfPriHirer As Integer = 0
        For Each rr As DataRow In rentalCustomerDt.Rows
            noOfPriHirer = noOfPriHirer + Utility.ParseInt(rr("PriHirer"), 0)
        Next
        If noOfPriHirer = 0 Then
            Return "GEN132"
        ElseIf noOfPriHirer > 1 Then
            Return "GEN133"
        End If


        Dim xmlDoc As XmlDocument = New XmlDocument
        xmlDoc.LoadXml("<Data><ThisRental></ThisRental><Remove></Remove></Data>")

        Dim root As XmlElement = xmlDoc.DocumentElement.FirstChild

        For Each rr As DataRow In rentalCustomerDt.Rows


            Dim rentalNode As XmlNode = xmlDoc.CreateNode("element", "Rental", "")

            Dim cusId As String = Utility.ParseString(rr("CusId"), "")
            Dim custIdNode As XmlNode = xmlDoc.CreateNode("element", "CusId", "")
            custIdNode.InnerText = cusId
            rentalNode.AppendChild(custIdNode)

            Dim name As String = Utility.ParseString(rr("CName"), "")
            Dim nameNode As XmlNode = xmlDoc.CreateNode("element", "CName", "")
            nameNode.InnerText = EncodeText(name)
            rentalNode.AppendChild(nameNode)

            Dim address As String = Utility.ParseString(rr("Address"), "")
            Dim addressNode As XmlNode = xmlDoc.CreateNode("element", "Address", "")
            addressNode.InnerText = EncodeText(address)
            rentalNode.AppendChild(addressNode)

            Dim hirer As String = Utility.ParseString(rr("Hirer"), "")
            Dim hirerNode As XmlNode = xmlDoc.CreateNode("element", "Hirer", "")
            hirerNode.InnerText = hirer
            rentalNode.AppendChild(hirerNode)

            Dim priHirer As String = Utility.ParseString(rr("PriHirer"), "")
            Dim priHirerNode As XmlNode = xmlDoc.CreateNode("element", "PriHirer", "")
            priHirerNode.InnerText = priHirer
            rentalNode.AppendChild(priHirerNode)

            Dim isDriver As String = Utility.ParseString(rr("IsDriver"), "")
            Dim isDriverNode As XmlNode = xmlDoc.CreateNode("element", "IsDriver", "")
            isDriverNode.InnerText = isDriver
            rentalNode.AppendChild(isDriverNode)

            Dim issueAuthority As String = Utility.ParseString(rr("IssueAuthority"), "")
            Dim issueAuthorityNode As XmlNode = xmlDoc.CreateNode("element", "IssueAuthority", "")
            issueAuthorityNode.InnerText = EncodeText(issueAuthority)
            rentalNode.AppendChild(issueAuthorityNode)

            Dim dob As String = Utility.ParseString(rr("DOB"), "")
            Dim dobNode As XmlNode = xmlDoc.CreateNode("element", "DOB", "")
            dobNode.InnerText = dob
            rentalNode.AppendChild(dobNode)

            Dim licNo As String = Utility.ParseString(rr("LicNo"), "")
            Dim licNoNode As XmlNode = xmlDoc.CreateNode("element", "LicNo", "")
            licNoNode.InnerText = EncodeText(licNo)
            rentalNode.AppendChild(licNoNode)

            Dim expiryDt As String = Utility.ParseString(rr("ExpiryDt"), "")
            Dim expiryDtNode As XmlNode = xmlDoc.CreateNode("element", "ExpiryDt", "")
            expiryDtNode.InnerText = expiryDt
            rentalNode.AppendChild(expiryDtNode)

            Dim check As String = Utility.ParseString(rr("Check"), "")
            Dim checkNode As XmlNode = xmlDoc.CreateNode("element", "Check", "")
            checkNode.InnerText = check
            rentalNode.AppendChild(checkNode)

            Dim addable As String = Utility.ParseString(rr("Addable"), "")
            Dim addableNode As XmlNode = xmlDoc.CreateNode("element", "Addable", "")
            addableNode.InnerText = addable
            rentalNode.AppendChild(addableNode)

            Dim rntId As String = Utility.ParseString(rr("RntId"), "")
            Dim rntIdNode As XmlNode = xmlDoc.CreateNode("element", "RntId", "")
            rntIdNode.InnerText = rntId
            rentalNode.AppendChild(rntIdNode)

            Dim passportNo As String = Utility.ParseString(rr("PassportNo"), "")
            Dim passportNoNode As XmlNode = xmlDoc.CreateNode("element", "PassportNo", "")
            passportNoNode.InnerText = EncodeText(passportNo)
            rentalNode.AppendChild(passportNoNode)

            Dim passportCountry As String = Utility.ParseString(rr("PassportCountry"), "")
            Dim passportCountryNode As XmlNode = xmlDoc.CreateNode("element", "PassportCountry", "")
            passportCountryNode.InnerText = EncodeText(passportCountry)
            rentalNode.AppendChild(passportCountryNode)

            Dim ageNode As XmlNode = xmlDoc.CreateNode("element", "Age", "")
            ageNode.InnerText = ""
            rentalNode.AppendChild(ageNode)

            ' Change by Shoel on 10.6.10 for Loyalty Card info
            Dim loyaltyCardNumber As String = Utility.ParseString(rr("LoyaltyCardNumber"), "")
            Dim loyaltyCardNumberNode As XmlNode = xmlDoc.CreateNode("element", "LoyaltyCardNumber", "")
            loyaltyCardNumberNode.InnerText = EncodeText(loyaltyCardNumber)
            rentalNode.AppendChild(loyaltyCardNumberNode)
            ' Change by Shoel on 10.6.10 for Loyalty Card info

            Dim emailaddress As String = rr("EmailAddress")
            Dim emailaddressNode As XmlNode = xmlDoc.CreateNode("element", "EmailAddress", "")
            emailaddressNode.InnerText = EncodeText(emailaddress)
            rentalNode.AppendChild(emailaddressNode)

            If isDriver = "1" Then
                If String.IsNullOrEmpty(licNo) Then
                    Return "GEN134"
                ElseIf String.IsNullOrEmpty(dob) Then
                    Return "GEN135"
                ElseIf String.IsNullOrEmpty(issueAuthority) Then
                    Return "GEN136"
                ElseIf String.IsNullOrEmpty(expiryDt) Then
                    Return "GEN137"
                End If
            End If
            If Not String.IsNullOrEmpty(licNo) Then
                If String.IsNullOrEmpty(dob) Then
                    Return "GEN138"
                ElseIf String.IsNullOrEmpty(issueAuthority) Then
                    Return "GEN139"
                ElseIf String.IsNullOrEmpty(expiryDt) Then
                    Return "GEN140"
                End If
            End If

            root.AppendChild(rentalNode)

        Next


        Dim root1 As XmlElement = xmlDoc.DocumentElement.LastChild

        For Each s As String In removeCustomerCol

            Dim recoNode As XmlNode = xmlDoc.CreateNode("element", "Reco", "")

            Dim idNode As XmlNode = xmlDoc.CreateNode("element", "ID", "")
            idNode.InnerText = s
            recoNode.AppendChild(idNode)

            root1.AppendChild(recoNode)

        Next

        Dim root2 As XmlElement = xmlDoc.DocumentElement
        Dim msgNode As XmlNode = xmlDoc.CreateNode("element", "Msg", "")
        msgNode.InnerText = EncodeText(msg)
        root2.AppendChild(msgNode)
        Dim addModUserIdNode As XmlNode = xmlDoc.CreateNode("element", "AddModUserId", "")
        addModUserIdNode.InnerText = userCode
        root2.AppendChild(addModUserIdNode)
        Dim addProgramNameNode As XmlNode = xmlDoc.CreateNode("element", "AddProgramName", "")
        addProgramNameNode.InnerText = programName
        root2.AppendChild(addProgramNameNode)


        Dim errorMessage As String
        ''rev:mia jan 21 2009 bypass bp2bb
        If Not isBp2bb Then
            errorMessage = DataRepository.UpdateCustomerSummary(bookingId, rentalId, xmlDoc)
        Else
            errorMessage = DataRepository.UpdateCustomerSummaryAndBypassBP2BB(bookingId, rentalId, xmlDoc)
        End If


        Return errorMessage

    End Function

    Private _isValidEmailAddress As Boolean
    Property isValidEmailAddress As Boolean
        Get
            Return _isValidEmailAddress
        End Get
        Set(ByVal value As Boolean)
            _isValidEmailAddress = value
        End Set
    End Property

    Function CheckEmailAddress(ByVal emailaddress As TextBox) As Boolean
        Dim oRegex As Regex = New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")
        If Not oRegex.IsMatch(emailaddress.Text.Trim) Then
            ScriptManager.GetCurrent(Page).SetFocus(emailaddress.Text)
            emailaddress.BackColor = Aurora.Common.DataConstants.RequiredColor
            CheckEmailAddress = False
            CurrentPage.SetShortMessage(AuroraHeaderMessageType.Error, "Invalid Email address")
        Else
            emailaddress.BackColor = Drawing.Color.White
            Return True
        End If
    End Function
#End Region

#Region "rev:mia Sept.2 2013 - Customer Title from query"
    
    Sub LoadCustomerNameParameter(sender As Object, e As CommandEventArgs)
        If e.CommandName = "Load" Then
            BookingCustomerUserEditControl1.CustomerId = e.CommandArgument
            BookingCustomerUserEditControl1.ShowMe()
        End If
    End Sub

#End Region

End Class
