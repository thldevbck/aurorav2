﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingFilesUserControl.ascx.vb" Inherits="Booking_Controls_BookingFilesUserControl" %>
<%@ Register TagPrefix="uc1" TagName="MessagePanelControl" Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" %>

<style>
    #uploadFileTable {
        border-collapse:separate;
        border-spacing:0 5px;
    }

    #uploadFileTable > tbody > tr > td:nth-child(2) {
         width: 100%;
    }

    #uploadFileTable > tbody > tr > td:nth-child(2) > * {
        padding-left: 0;
        padding-right: 0;
        overflow: auto;
    }
    
    #uploadFileTable {
        width: 100%;
    }
</style>

<ajaxToolkit:ModalPopupExtender
    runat="server"
    ID="programmaticModalPopup"
    BehaviorID="programmaticBookingFilesPopupBehavior"
    TargetControlID="hiddenTargetControlForModalPopup"
    PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground"
    DropShadow="True"
    PopupDragHandleControlID="programmaticPopupDragHandle" />
<asp:Button
    runat="server"
    ID="hiddenTargetControlForModalPopup"
    Style="display: none" />

<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none; width: 550px; ">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
                <asp:Label runat="server">Upload a new file</asp:Label>
            </asp:Panel>

            <uc1:MessagePanelControl ID="bookingNotesPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />
            
            <table id="uploadFileTable">
                <tr>
                    <td>
                        <asp:Label runat="server" for="fileType">File Type: </asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="fileType" Width="100%">
                            <asp:ListItem>Driver License</asp:ListItem>
                            <asp:ListItem>Damage Sheet</asp:ListItem>
                            <asp:ListItem>Rental Agreement</asp:ListItem>
                            <asp:ListItem>Infringement</asp:ListItem>
                            <asp:ListItem>Incident</asp:ListItem>
                            <asp:ListItem>Other</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" for="description">Description: </asp:Label>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="description" Rows="10" TextMode="MultiLine" Width="100%"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" for="browseFile">File: </asp:Label>
                    </td>
                    <td>
                        <asp:FileUpload runat="server" ID="fileUpload" Width="100%"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="uploadFileButton" runat="server" Text="Upload" CssClass="Button_Medium Button_New" />
                    </td>
                    <td>
                        <asp:Button ID="cancelButton" runat="server" Text="Cancel" CssClass="Button_Medium" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="uploadFileButton" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>

<div><strong>Files associated with this booking: </strong></div>
<asp:Panel runat="server" ID="noFilesPanel">
    <div>There are no files associated with this booking.</div>
</asp:Panel>
<asp:GridView ID="bookingFilesGridView" runat="server" AutoGenerateColumns="False" CssClass="dataTableGrid"
    GridLines="None" DataKeyNames="RntFileId,RntFileName,RntFilePath" AllowSorting="True" Width="100%">
    <AlternatingRowStyle CssClass="oddRow" Width="100%" />
    <RowStyle CssClass="evenRow" />
    <Columns>
        <asp:BoundField DataField="RntFileId" Visible="False" HeaderText="File Id" ReadOnly="True" ItemStyle-Wrap="False"/>
        <asp:BoundField DataField="RntFileName" Visible="False" HeaderText="File Name" ReadOnly="True" ItemStyle-Wrap="False"/>
        <asp:BoundField DataField="RntFilePath" Visible="False" HeaderText="File Path" ReadOnly="True" ItemStyle-Wrap="False"/>
        <asp:BoundField DataField="RntType" HeaderText="File Type" ReadOnly="True" ItemStyle-Wrap="False" SortExpression="RntType" ItemStyle-Width="50px"/>
        <asp:BoundField DataField="AddUsrId" HeaderText="User" ReadOnly="True" ItemStyle-Wrap="False" SortExpression="AddUsrId" ItemStyle-Width="25px" />
        <asp:BoundField DataField="FormatedAddDateTime" HeaderText="Date" ReadOnly="True" ItemStyle-Wrap="False" SortExpression="AddDateTime" ItemStyle-Width="50px"/>
        <asp:BoundField DataField="RntDescription" HeaderText="Description" ReadOnly="True" ItemStyle-Wrap="True" SortExpression="RntDescription"/>
        <asp:HyperLinkField Target="_blank" HeaderText="File Name" DataTextField="RntFileName" DataNavigateUrlFields="LinkPath" SortExpression="RntFileName" ItemStyle-Width="100px"/>
<%--        <asp:CommandField ShowDeleteButton="True" />--%>
    </Columns>
</asp:GridView>
<br/>
<asp:Button
    ID="showUploadScreen"
    runat="server"
    Text="Upload file"
    CssClass="Button_Standard Button_New floatRight" />