<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingNotesUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingNotesUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="../../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingNotesPopupUserControl.ascx" TagName="BookingNotesPopupUserControl"
    TagPrefix="uc1" %>

<%@ Register Src="~/Booking/Controls/EuropcarDataLog.ascx" TagName="EuropcarDataLog" TagPrefix="uc1" %>

<table width="100%">
    <tr>
        <td width="150px">
            Sort Notes By:
        </td>
        <td>
            <asp:RadioButtonList ID="headerRadioButtonList" runat="server" AutoPostBack="True"
                RepeatDirection="Horizontal" RepeatLayout="Table" Enabled="true" CssClass="repeatLabelTable">
                <asp:ListItem Text="Priority" Value="Priority" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Date Added" Value="AddDtTmDate"></asp:ListItem>
                <asp:ListItem Text="Audience" Value="Audience"></asp:ListItem>
            </asp:RadioButtonList>
        </td>
        <td align="right">
            <asp:CheckBox ID="showAllRentalCheckBox" runat="server" Text="Show All Rentals" AutoPostBack="true" />
        </td>
    </tr>
</table>
<asp:Repeater ID="bookingNotesRepeater" runat="server">
    <HeaderTemplate>
        <table width="100%" class="dataTable" cellpadding="2" cellspacing="0">
            <tr>
                <th width="50px">
                    Rnt
                </th>
                <th width="75px">
                    Audience
                </th>
                <th width="75px">
                    Type
                </th>
                <th width="75px">
                    Priority
                </th>
                <th>
                    Added
                </th>
                <th width="50px" align="right">
                    &nbsp;
                </th>
            </tr>
    </HeaderTemplate>
    <ItemTemplate>
        <tr class="evenRow">
            <td>
                <asp:Label runat="server" ID="Label1" Text='<%# Bind("RntNo") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label2" Text=' <%# Bind("Audience") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label3" Text=' <%# Bind("Type") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label4" Text=' <%# Bind("Priority") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label5" Text=' <%# GetDateFormat(eval("AddDtTm")) %> ' />
                &nbsp;by&nbsp;
                <asp:Label runat="server" ID="Label6" Text=' <%# Bind("By") %>' />
            </td>
            <td>
                <asp:LinkButton ID="updateLinkButton" runat="server" CommandName="update" Enabled='<%# AllowUpdate(Eval("Type")) %>'
                    CommandArgument='<%# Eval("NId") %>' Text="Edit" />
                <asp:Literal ID="litCardFlag" runat="server" EnableViewState="true" Visible="false" />
            </td>
        </tr>
        <tr class="evenRow">
            <td colspan="6" style="border-top-style: dotted; border-top-width: 1px; border-bottom-width: 1px">
                <asp:Label ID="lblNotes" runat="server" Width="95%" Text='<%# Eval("NoteDesc").ToString.Replace(chr(10), "<br />") %>'
                    Style="display: block; float: left; width: 750px; word-wrap: break-word" />
            </td>
        </tr>
    </ItemTemplate>
    <AlternatingItemTemplate>
        <tr class="oddRow">
            <td>
                <asp:Label runat="server" ID="Label1" Text='<%# Bind("RntNo") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label2" Text=' <%# Bind("Audience") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label3" Text=' <%# Bind("Type") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label4" Text=' <%# Bind("Priority") %>' />
            </td>
            <td>
                <asp:Label runat="server" ID="Label5" Text=' <%# GetDateFormat(eval("AddDtTm")) %> ' />
                &nbsp;by&nbsp;
                <asp:Label runat="server" ID="Label6" Text=' <%# Bind("By") %>' />
            </td>
            <td>
                <asp:LinkButton ID="updateLinkButton" runat="server" CommandName="update" Enabled='<%# AllowUpdate(Eval("Type")) %>'
                    CommandArgument='<%# Eval("NId") %>' Text="Edit" />
                <asp:Literal ID="litCardFlag" runat="server" EnableViewState="true" Visible="false" />
            </td>
        </tr>
        <tr class="oddRow">
            <td colspan="6" style="border-top-style: dotted; border-top-width: 1px; border-bottom-width: 1px">
                <asp:Label ID="lblNotes" runat="server" Width="95%" Text='<%# Eval("NoteDesc").ToString.Replace(chr(10), "<br />") %>'
                    Style="display: block; float: left; width: 750px; word-wrap: break-word" />
            </td>
        </tr>
    </AlternatingItemTemplate>
    <FooterTemplate>
        </table>
    </FooterTemplate>
</asp:Repeater>
<br />
<table width="100%">
    <tr>
        <td align="right">
            <asp:Button ID="addButton" runat="server" Text="Add Notes" CssClass="Button_Standard Button_Add" />
            <asp:Button ID="searchButton" runat="server" Text="Search" CssClass="Button_Standard Button_Search" />
        </td>
    </tr>
    <tr style="height:150px;"><td></td></tr>
    <tr>
        <td id="tdShowMoreNotes" align="right" runat="server">
            <a href="#" id="linkToShowMoreNotes" >Show More Notes</a>
        </td>
    </tr>
    <tr>    
        <td align="right">
                  <asp:LinkButton ID="lnkEuropcarLog" runat="server">Show Europcar Log</asp:LinkButton>            
        </td>
    </tr>
</table>
<uc1:BookingNotesPopupUserControl ID="BookingNotesPopupUserControl1" runat="server" />
<uc1:SupervisorOverrideControl ID="SupervisorOverrideControl1" runat="server" />
<uc1:EuropcarDataLog ID="EuropcarDataLogControl" runat="server" />
