<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ShowBookingRequestAgentPopupUserControl.ascx.vb"
    Inherits="Booking_Controls_ShowBookingRequestAgentPopupUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc1" %>

<script type="text/javascript">
        
//        // Add click handlers for buttons to show and hide modal popup on pageLoad
//        function pageLoad() {
//            $addHandler($get("backButton1"), 'click', hideShowBookingRequestAgentPopupViaClient);        
//        }      
//        function hideShowBookingRequestAgentPopupViaClient(ev) {
//            ev.preventDefault();        
//            var showBookingRequestAgentPopupBehavior = $find('programmaticShowBookingRequestAgentPopupBehavior');
//            showBookingRequestAgentPopupBehavior.hide();
//        }
        function hideShowBookingRequestAgentPopupViaClient(id) 
        {    
            var bookingRequestAgentMgtPopupBehavior = $find(id);
            bookingRequestAgentMgtPopupBehavior.hide();
            return false;
        }
        
</script>

<script type="text/javascript">

</script>

<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticShowBookingRequestAgentPopup"
    BehaviorID="programmaticShowBookingRequestAgentPopupBehavior" TargetControlID="hiddenTargetControlForModalPopup"
    PopupControlID="programmaticPopup" BackgroundCssClass="modalBackground" DropShadow="True"
    PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 650px; height: 530px">
    <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        Agent Search Result
    </asp:Panel>
    <br />
    <uc1:MessagePanelControl ID="showBookingRequestAgentPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />

    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button ID="addButton" runat="server" Text="Add Agent" CssClass="Button_Standard Button_Add" />
            </td>
        </tr>
        <tr>
            <td>
            <asp:Panel ID="panel1" runat=server ScrollBars=Vertical Height=400>
                <asp:GridView ID="agentGridView" runat="server" Width="97%" AutoGenerateColumns="False"
                    CssClass="dataTable">
                    <AlternatingRowStyle CssClass="oddRow" />
                    <RowStyle CssClass="evenRow" />
                    <Columns>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="nameLabel" runat="server" Text='<%# Bind("code") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Name</HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="updateLinkButton" runat="server" CommandName="update" CommandArgument='<%# GetAgentCode(Eval("id"),Eval("code"),Eval("name")) %>'>
                                    <%# Eval("name") %> </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="address" HeaderText="Address" ReadOnly="True" />
                        <asp:BoundField DataField="country" HeaderText="Country" ReadOnly="True" />
                    </Columns>
                </asp:GridView>
            </asp:Panel>
            </td>
            <tr>
                <td align="right">
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
    </table>
    <br />
</asp:Panel>
