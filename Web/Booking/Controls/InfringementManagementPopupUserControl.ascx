<%@ Control Language="VB" AutoEventWireup="false" CodeFile="InfringementManagementPopupUserControl.ascx.vb"
    Inherits="Booking_Controls_InfringementManagementPopupUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="~\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc3" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc4" %>

<script type="text/javascript">

    /////////////////////////
    // Mod: Raj - 17 May, 2011
    

    function ClientValidation() {
        var boolValidate = true;
        var bookingNoTextBox;
        bookingNoTextBox = $get('<%= bookingNoTextBox.ClientID %>');
        if (!validateAControl(bookingNoTextBox)) {
            bookingNoTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            bookingNoTextBox.style.background = "white";
        }


        var rentalNoTextBox;
        rentalNoTextBox = $get('<%= rentalNoTextBox.ClientID %>');
        if (!validateAControl(rentalNoTextBox)) {
            rentalNoTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            rentalNoTextBox.style.background = "white";
        }


        var unitNoTextBox;
        unitNoTextBox = $get('<%= unitNoTextBox.ClientID %>');
        if (!validateAControl(unitNoTextBox)) {
            unitNoTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            unitNoTextBox.style.background = "white";
        }


        var branchPickerControl;
        branchPickerControl = $get('<%= branchPickerControl.ClientID %>');
        if (!validateAControl(branchPickerControl)) {
            branchPickerControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            branchPickerControl.style.background = "white";
        }

        var dateDateControl;
        dateDateControl = $get('<%= dateDateControl.ClientID %>');
        if (!validateAControl(dateDateControl)) {
            dateDateControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            dateDateControl.style.background = "white";
        }


        var timeTimeControl;
        timeTimeControl = $get('<%= timeTimeControl.ClientID %>');
        if (!validateAControl(timeTimeControl)) {
            timeTimeControl.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            timeTimeControl.style.background = "white";
        }

        var notifiedDropDownList;
        notifiedDropDownList = $get('<%= notifiedDropDownList.ClientID %>');
        if (!validateAControl(notifiedDropDownList)) {
            notifiedDropDownList.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            notifiedDropDownList.style.background = "white";
        }

        var datailTextBox;
        datailTextBox = $get('<%= datailTextBox.ClientID %>');
        if (!validateAControl(datailTextBox)) {
            datailTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            datailTextBox.style.background = "white";
        }


        var referenceTextBox;
        referenceTextBox = $get('<%= referenceTextBox.ClientID %>');
        if (!validateAControl(referenceTextBox)) {
            referenceTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;

        }
        else {
            referenceTextBox.style.background = "white";
        }


        var infringementAmtTextBox;
        infringementAmtTextBox = $get('<%= infringementAmtTextBox.ClientID %>');
        if (!validateAControl(infringementAmtTextBox)) {
            infringementAmtTextBox.style.background = '<%= System.Drawing.ColorTranslator.ToHtml(Aurora.Common.DataConstants.RequiredColor) %>';
            boolValidate = false;
        }
        else {
            infringementAmtTextBox.style.background = "white";
        }
    
    
        if (boolValidate == false) {
            return false;
        }

        return true;
    }


    function validateAControl(obj) {
        if (obj.value == "") {
            //setWarningShortMessage(getErrorMessage("GEN005"));
            return false
        }
        else {
            return true
        }
    }        
        
   ///////////////////////// 


//        // Add click handlers for buttons to show and hide modal popup on pageLoad
//        function pageLoad() {
//            //$addHandler($get("showModalPopupClientButton"), 'click', showModalPopupViaClient);
//            $addHandler($get("backButton"), 'click', hideInfringementPopupViaClient);        
//        }      
//        function hideInfringementPopupViaClient(ev) {
//            ev.preventDefault();        
//            var infringementPopupBehavior = $find('programmaticInfringementPopupBehavior');
//            infringementPopupBehavior.hide();
//        }
</script>

<script type="text/javascript">

//function checkStatus(statusDropDownListId, resolvedDateDateControlId){

//    var statusDropDownList = document.getElementById(statusDropDownListId)
//    var resolvedDateDateControl = document.getElementById(resolvedDateDateControlId)

//    if (statusDropDownList.value=='Open' || statusDropDownList.value=='Cancelled'){
//        resolvedDateDateControl.value = ""
//    }
//}

//function valStatus(statusDropDownListId, resolvedDateDateControlId,todayHiddenId){

//    var statusDropDownList = document.getElementById(statusDropDownListId)
//    var resolvedDateDateControl = document.getElementById(resolvedDateDateControlId)
//    var todayHidden = document.getElementById(todayHiddenId)
// 
//    if (statusDropDownList.value=='Closed'){
//      resolvedDateDateControl.value= todayHidden.value;
//    }
//    else{
//      resolvedDateDateControl.value='';
//    }
//}
//  

//function valReference(paymentReferenceTextBoxId, paymentReceivedDateControlId, todayHiddenId){

//    var paymentReferenceTextBox = document.getElementById(paymentReferenceTextBoxId)
//    var paymentReceivedDateControl = document.getElementById(paymentReceivedDateControlId)
//    var todayHidden = document.getElementById(todayHiddenId)
//    
//    removeSpace(paymentReferenceTextBox);
//    if (paymentReferenceTextBox.value!=''){
//        paymentReceivedDateControl.value = todayHidden.value;
//    }
//    else{
//        paymentReceivedDateControl.value='';
//    }
//}

</script>

<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticInfringementPopup"
    BehaviorID="programmaticInfringementPopupBehavior" TargetControlID="hiddenTargetControlForModalPopup"
    PopupControlID="programmaticPopup" BackgroundCssClass="modalBackground" DropShadow="True"
    PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 605px; height: 540px">
    <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        Maintain Infringement
    </asp:Panel>
    <br />
    <uc4:MessagePanelControl ID="infringementManagementPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel" />
    <div style="visibility:hidden">
        <INPUT type="hidden" ID="todayHidden" runat=server  />
        <INPUT type="hidden" ID="bookedProdIdHidden" runat=server />
        <INPUT type="hidden" ID="iidHidden" runat=server />
    </div>
    
    <table width="100%">
        <tr>
            <td width="20%">
                Booking No:
            </td>
            <td width="30%">
                <asp:Label ID="bookingNoLabel" runat="server"></asp:Label>
                <asp:TextBox ID="bookingNoTextBox" runat="server" ReadOnly="true"></asp:TextBox>&nbsp;*
            </td>
            <td width="20%">
                Rental No:
            </td>
            <td width="30%">
                <asp:Label ID="rentalNoLabel" runat="server"></asp:Label>
                <asp:TextBox ID="rentalNoTextBox" runat="server" ReadOnly="true"></asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td>
                Unit No:
            </td>
            <td>
                <asp:TextBox ID="unitNoTextBox" runat="server" onkeypress="keyStrokeInt();" AutoPostBack="true"
                MaxLength="12">
                </asp:TextBox>&nbsp;*
            </td>
            <td>
                Reg No:
            </td>
            <td>
                <asp:TextBox ID="regoTextBox" runat="server" AutoPostBack="true" 
                MaxLength="12">
                </asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
            <br />
            </td>
            <td>
            </td>
            <td>
            </td>
            <td>
                <asp:Label ID="vehicleLabel" runat="server"></asp:Label>
                <asp:TextBox ID="vehicleTextBox" runat="server" ReadOnly="true"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Branch:
            </td>
            <td>
                <uc1:PickerControl ID="branchPickerControl" runat="server" PopupType="LOCATION" width="130" 
                    Nullable=false />&nbsp;*
            </td>
            <td>
                Date / Time:
            </td>
            <td>
                <uc2:DateControl ID="dateDateControl" runat="server" Nullable="true" 
                    AutoPostBack=true MessagePanelControlID="infringementManagementPopupMessagePanelControl" />
                <uc3:TimeControl ID="timeTimeControl" runat="server" Nullable="true" MessagePanelControlID="infringementManagementPopupMessagePanelControl" />&nbsp;*
            </td>
        </tr>
        <tr>
            <td>
                Notified:
            </td>
            <td colspan=3>
                <asp:DropDownList ID="notifiedDropDownList" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                </asp:DropDownList>&nbsp;*
            </td>
       
        </tr>
        <tr>
            <td valign=top >
                Details:
            </td>
            <td colspan="3">
                <asp:TextBox ID="datailTextBox" runat="server" Width="97%" TextMode="multiLine" Rows="2"
                MaxLength="2048" >
                </asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td><br />
            </td>
        </tr>
        <tr>
            <td>Cause:</td>
            <td rowspan="3" >
                <asp:CheckBoxList ID="causeCheckBoxList" runat="server">
                </asp:CheckBoxList>
            </td>
            <td>
                Reference:
            </td>
            <td>
                <asp:TextBox ID="referenceTextBox" runat="server" MaxLength=24>
                </asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td colspan=2></td>
            <td>
                Infringement Amt: $
            </td>
            <td>
                <asp:TextBox ID="infringementAmtTextBox" runat="server" onkeypress="keyStrokeDecimal();"
                    onblur="this.value=trimDecimal(this.value,2)" style="text-align: right"
                    MaxLength=10></asp:TextBox>&nbsp;*
            </td>
        </tr>
        <tr>
            <td colspan=2></td>
            <td>
                Admin Fee: $
            </td>
            <td>
                <asp:TextBox ID="adminFeeTextBox" runat="server" onkeypress="keyStrokeDecimal();" 
                     onblur="this.value=trimDecimal(this.value,2)" style="text-align: right"
                     MaxLength=10></asp:TextBox>
            </td>
        </tr>
         <tr>
            <td colspan="2">
            </td>
            <td>
                Infringement Due:
            </td>
            <td>
                <uc2:DateControl ID="infringementDueDateControl" runat="server" Nullable="true" MessagePanelControlID="infringementManagementPopupMessagePanelControl" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td>
                Customer Advised (date):
            </td>
            <td>
                <uc2:DateControl ID="customerAdvisedDateControl" runat="server" Nullable="true" 
                    AutoPostBack=true MessagePanelControlID="infringementManagementPopupMessagePanelControl" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td>
                Status:
            </td>
            <td>
                <asp:DropDownList ID="statusDropDownList" runat="server">
                    <asp:ListItem Text="Open" Value="Open">
                    </asp:ListItem>
                    <asp:ListItem Text="Closed" Value="Closed">
                    </asp:ListItem>
                    <asp:ListItem Text="Cancelled" Value="Cancelled">
                    </asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
            </td>
            <td>
                Resolved Date:
            </td>
            <td>
                <uc2:DateControl ID="resolvedDateDateControl" runat="server" Nullable="true"
                    AutoPostBack=true MessagePanelControlID="infringementManagementPopupMessagePanelControl" />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <hr />
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <b>Received Payment</b>
            </td>
        </tr>
        <tr>
            <td>
                Reference:
            </td>
            <td>
                <asp:TextBox ID="paymentReferenceTextBox" runat="server" MaxLength=20 >
                </asp:TextBox>
            </td>
            <td>
                Received:
            </td>
            <td>
                <uc2:DateControl ID="paymentReceivedDateControl" runat="server" Nullable="true" 
                    AutoPostBack=true MessagePanelControlID="infringementManagementPopupMessagePanelControl"/>
            </td>
        </tr>
    </table>
    <br />
    <table width="100%">
        <tr>
            <td align=right>
                <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save" OnClientClick="return ClientValidation();" />
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                <asp:Button ID="newButton" runat="server" Text="Save & New" CssClass="Button_Standard Button_New"  OnClientClick="return ClientValidation();" />
                <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>

