''  Change Log
''  6.6.8   -   Fixed several issues to make this pass the regression test
''  9.6.8   -   Issue 14 fixed (date, time validations) (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=14)
'' 10.6.8   -   Now it doesnt allow blank date/time values to mess up processing
'' 17.6.8   -   Shoel : Now doesnt allow editing when it can be saved
''  1.7.8   -   Shoel : added the date utility thingie + removed commented/unused code bits
'' 17.7.8   -   Shoel : fixed the issue of saving an extension successfully, but not going to prod tab/showing success msg 
'' 22.7.8   -   Shoel : session usage removed
'' 28.6.8   -   Shoel : Fix for SQISH #535 - Added UI validations so that new CO < old CO and new CI > old CI

'' names of sps
'' RES_getRentalForExtension
'' RES_getHirePeriod
'' RES_DeleteTProcessedBpds
'' RES_ssTransToFinance
'' RES_GetRentalIntegrity
'' RES_UpdateRentalIntegrity
'' RES_UpdateRentalIntegrityForBooking
'' RES_ManageBookedProductExtension
'' RES_FetchDetailForExtension
'' RES_checkRentalHistory


Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Xml

Partial Class Booking_Controls_BookingExtensionUserControl
    Inherits BookingUserControl

    Public isFirstLoad As Boolean = False
    'page specific stuff
    Public CheckOutDateTime As DateTime
    Public CheckInDateTime As DateTime
    Public UOMId As String
    Public BookedProductId As String
    Public RentalStatus As String
    'end pagespecific stuff
    Const VIEWSTATE_BPDID As String = "VIEWSTATE_Extension_BPDID"
    Const VIEWSTATE_UOMID As String = "VIEWSTATE_Extension_UOMID"
    Const VIEWSTATE_XMLDOC As String = "VIEWSTATE_Extension_XMLDOC"
    Const VIEWSTATE_RENTALINTEGRITYNO As String = "VIEWSTATE_Extension_RentalIntNo"
    Const VIEWSTATE_BOOKINGINTEGRITYNO As String = "VIEWSTATE_Extension_BookingIntNo"

#Region "public properties"
    Public Property ExtensionBPDId() As String
        Get
            Return CStr(ViewState(VIEWSTATE_BPDID))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_BPDID) = value
        End Set
    End Property

    Public Property ExtensionUOM() As String
        Get
            Return CStr(ViewState(VIEWSTATE_UOMID))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_UOMID) = value
        End Set
    End Property

    Public Property ExtensionRentalIntegrityNumber() As String
        Get
            Return CStr(ViewState(VIEWSTATE_RENTALINTEGRITYNO))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_RENTALINTEGRITYNO) = value
        End Set
    End Property

    Public Property ExtensionBookingIntegrityNumber() As String
        Get
            Return CStr(ViewState(VIEWSTATE_BOOKINGINTEGRITYNO))
        End Get
        Set(ByVal value As String)
            ViewState(VIEWSTATE_BOOKINGINTEGRITYNO) = value
        End Set
    End Property

    Public Property ExtensionXMLDoc() As XmlDocument
        Get
            Dim oTemp As XmlDocument = New XmlDocument
            oTemp.LoadXml(CStr(ViewState(VIEWSTATE_XMLDOC)))
            Return oTemp
        End Get
        Set(ByVal value As XmlDocument)
            ViewState(VIEWSTATE_XMLDOC) = value.OuterXml
        End Set
    End Property



#End Region


    Sub CleanVIEWSTATE()
        ViewState(VIEWSTATE_BPDID) = Nothing
        ViewState(VIEWSTATE_UOMID) = Nothing
        ViewState(VIEWSTATE_XMLDOC) = Nothing
        ViewState(VIEWSTATE_RENTALINTEGRITYNO) = Nothing
        ViewState(VIEWSTATE_BOOKINGINTEGRITYNO) = Nothing
        txtCheckInExtensionPeriod.Text = ""
        txtCheckOutExtensionPeriod.Text = ""

    End Sub

    Public Sub LoadPage()
        Try
            If isFirstLoad Then
                CleanVIEWSTATE()
            End If

            'call the database
            Dim oDataDocument As System.Xml.XmlDocument

            oDataDocument = BookingExtension.GetRentalForExtension(BookingId, RentalId)

            'save to VIEWSTATE
            ' ViewState.Add(VIEWSTATE_XMLDOC, oDataDocument)
            ExtensionXMLDoc = oDataDocument


            ' extract the following
            ' CkoDate
            ' CkoTm
            ' ----NewCkoDate
            ' ----NewCkoTm
            ' CkiDate
            ' CkiTm
            ' ----NewCkiDate
            ' ----NewCkiTm
            ' UOMId
            ' FirstBpdId
            ' RntSt

            ''GEN143 - This rental is not available for extension. Only THL Rentals with status of Waitlist, Confirmed, Provisional or Checkout can be extended.
            'the statuses are
            'CI,CO,IN,KB,KK,NN,QN,WL,XN,XX

            '   RentalStatus = ds.Tables(0).Rows(0).Item("RntSt")

            RentalStatus = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/RntSt").InnerText

            If Not "WL KK NN CO".Contains(RentalStatus) Then
                'show an error message
                CurrentPage.ClearMessages()
                'CurrentPage.AddInformationMessage("GEN143 - This rental is not available for extension. Only THL Rentals with status of Waitlist, Confirmed, Provisional or Checkout can be extended.")
                CurrentPage.AddInformationMessage(CurrentPage.GetMessage("GEN143"))
                btnSave.Enabled = False
                'JL 2008/07/07
                'set the cancel button to be disabled
                btnCancel.Enabled = False
                btnCancel.Attributes.Add("OnClick", "clearMessages();return false;")
                dtcCheckInDate.ReadOnly = True
                dtcCheckOutDate.ReadOnly = True
                tcCheckInTime.ReadOnly = True
                tcCheckOutTime.ReadOnly = True
                'Exit Sub
            Else
                CurrentPage.ClearMessages()
                btnSave.Enabled = True
                'JL 2008/07/07
                'set the cancel button to be enabled
                btnCancel.Enabled = True
                dtcCheckInDate.ReadOnly = False
                dtcCheckOutDate.ReadOnly = False
                tcCheckInTime.ReadOnly = False
                tcCheckOutTime.ReadOnly = False
            End If

            If RentalStatus = "CO" Then
                ShowCheckIn()
            End If

            Dim strCkoDate, strCkoTime, strCkiDate, strCkiTime As String

            strCkoDate = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/CkoDate").InnerText
            strCkoTime = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/CkoTm").InnerText


            'CheckOutDateTime = Convert.ToDateTime(strCkoDate)
            CheckOutDateTime = Aurora.Common.Utility.ParseDateTime(strCkoDate, Aurora.Common.Utility.SystemCulture)
            Dim tsCkoTime As New TimeSpan(CInt(strCkoTime.Split(":"c)(0)), CInt(strCkoTime.Split(":"c)(1)), 0)
            CheckOutDateTime.Add(tsCkoTime)
            ''rev:mia 16jan2014-addition of supervisor override password
            OriginalCheckOutDate = CheckOutDateTime

            strCkiDate = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/CkiDate").InnerText
            strCkiTime = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/CkiTm").InnerText

            'CheckInDateTime = Convert.ToDateTime(strCkiDate)
            CheckInDateTime = Aurora.Common.Utility.ParseDateTime(strCkiDate, Aurora.Common.Utility.SystemCulture)
            Dim tsCkiTime As New TimeSpan(CInt(strCkiTime.Split(":"c)(0)), CInt(strCkiTime.Split(":"c)(1)), 0)
            CheckInDateTime.Add(tsCkiTime)


            UOMId = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/UOMId").InnerText
            ' ViewState.Add(VIEWSTATE_UOMID, UOMId)
            ExtensionUOM = UOMId


            If Not oDataDocument.SelectSingleNode("//data/ThisRental/Rental/FirstBpdId") Is Nothing Then
                BookedProductId = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/FirstBpdId").InnerText
                ' ViewState.Add(VIEWSTATE_BPDID, BookedProductId)
                ExtensionBPDId = BookedProductId
            End If

            ' catch hold of the integrity nos
            'ViewState.Add(VIEWSTATE_RENTALINTEGRITYNO, oDataDocument.SelectSingleNode("//data/ThisRental/Rental/RntIntNo").InnerText)
            'ViewState.Add(VIEWSTATE_BOOKINGINTEGRITYNO, oDataDocument.SelectSingleNode("//data/ThisRental/Rental/BooIntNo").InnerText)
            ExtensionRentalIntegrityNumber = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/RntIntNo").InnerText
            ExtensionBookingIntegrityNumber = oDataDocument.SelectSingleNode("//data/ThisRental/Rental/BooIntNo").InnerText

            'populate gui

            'checkInDateTextBox.Text = strCkiDate
            'checkInDateTextBox.Text = Aurora.Common.Utility.ParseDateTime(strCkiDate, Aurora.Common.Utility.SystemCulture).ToShortDateString()
            'checkOutDateTextBox.Text = strCkoDate
            'checkOutDateTextBox.Text = Aurora.Common.Utility.ParseDateTime(strCkoDate, Aurora.Common.Utility.SystemCulture).ToShortDateString()

            'JL 2008/07/11
            checkInDateTextBox.Text = Utility.DateDBUIConvert(strCkiDate)
            checkOutDateTextBox.Text = Utility.DateDBUIConvert(strCkoDate)


            dtcCheckInDate.Date = CheckInDateTime.Date
            dtcCheckOutDate.Date = CheckOutDateTime.Date

            tcCheckInTime.Text = strCkiTime
            tcCheckOutTime.Text = strCkoTime

            checkOutTimeTextBox.Text = strCkoTime
            checkInTimeTextBox.Text = strCkiTime

        Catch ex As Exception
            CurrentPage.LogException(ex)

        End Try
    End Sub

    Protected Sub CheckOutDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtcCheckOutDate.DateChanged

        ' fix for Squish #535
        CurrentPage.ClearMessages()
        CurrentPage.ClearShortMessage()
        txtCheckOutExtensionPeriod.Text = String.Empty
        ' fix for Squish #535

        If Not DatesAndTimesEnteredAreValid() Then
            Exit Sub
        End If
        Dim dtmCheckOutDateTime As DateTime
        Dim dtmCheckInDateTime As DateTime

        'dtmCheckOutDateTime = Convert.ToDateTime(dtcCheckOutDate.Text).Add(tcCheckOutTime.Time)
        dtmCheckOutDateTime = dtcCheckOutDate.Date.Add(tcCheckOutTime.Time)

        ' fix for Squish #535
        ' compare new co date to old co date , if new > old DONT allow -> show error
        Dim dtmOldCheckOutDateTime As DateTime
        dtmOldCheckOutDateTime = ParseDateTime(checkOutDateTextBox.Text, SystemCulture).Add(New TimeSpan(CInt(checkOutTimeTextBox.Text.Split(":"c)(0)), CInt(checkOutTimeTextBox.Text.Split(":"c)(1)), 0))
        If dtmCheckOutDateTime > dtmOldCheckOutDateTime Then
            CurrentPage.SetInformationShortMessage("This new checkout date must be prior to the original check out date")
            Exit Sub
        End If
        ' fix for Squish #535

        'dtmCheckInDateTime = Convert.ToDateTime(checkInDateTextBox.Text).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))
        dtmCheckInDateTime = Aurora.Common.Utility.ParseDateTime(checkInDateTextBox.Text, Aurora.Common.Utility.SystemCulture).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))


        txtCheckOutExtensionPeriod.Text = BookingExtension.GetHirePeriod(ExtensionBPDId, dtmCheckOutDateTime, dtmCheckInDateTime, ExtensionUOM)


    End Sub

    Protected Sub CheckOutTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcCheckOutTime.TimeChanged
        ' fix for Squish #535
        CurrentPage.ClearMessages()
        CurrentPage.ClearShortMessage()
        txtCheckOutExtensionPeriod.Text = String.Empty
        ' fix for Squish #535

        If Not DatesAndTimesEnteredAreValid() Then
            Exit Sub
        End If

        Dim dtmCheckOutDateTime As DateTime
        Dim dtmCheckInDateTime As DateTime

        'dtmCheckOutDateTime = Convert.ToDateTime(dtcCheckOutDate.Text).Add(tcCheckOutTime.Time)
        dtmCheckOutDateTime = dtcCheckOutDate.Date.Add(tcCheckOutTime.Time)

        ' fix for Squish #535
        ' compare new co date to old co date , if new > old DONT allow -> show error
        Dim dtmOldCheckOutDateTime As DateTime
        dtmOldCheckOutDateTime = ParseDateTime(checkOutDateTextBox.Text, SystemCulture).Add(New TimeSpan(CInt(checkOutTimeTextBox.Text.Split(":"c)(0)), CInt(checkOutTimeTextBox.Text.Split(":"c)(1)), 0))
        If dtmCheckOutDateTime > dtmOldCheckOutDateTime Then
            CurrentPage.SetInformationShortMessage("This new checkout date must be prior to the original check out date")
            Exit Sub
        End If
        ' fix for Squish #535

        'dtmCheckInDateTime = Convert.ToDateTime(checkInDateTextBox.Text).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))
        dtmCheckInDateTime = Aurora.Common.Utility.ParseDateTime(checkInDateTextBox.Text, Aurora.Common.Utility.SystemCulture).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))

        txtCheckOutExtensionPeriod.Text = BookingExtension.GetHirePeriod(ExtensionBPDId, dtmCheckOutDateTime, dtmCheckInDateTime, ExtensionUOM)

    End Sub

    Protected Sub CheckInDateChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtcCheckInDate.DateChanged

        ' fix for Squish #535
        CurrentPage.ClearMessages()
        CurrentPage.ClearShortMessage()
        txtCheckInExtensionPeriod.Text = String.Empty
        ' fix for Squish #535

        If Not DatesAndTimesEnteredAreValid() Then
            Exit Sub
        End If

        Dim dtmCheckOutDateTime As DateTime
        Dim dtmCheckInDateTime As DateTime

        Dim xmlVIEWSTATEDoc As System.Xml.XmlDocument
        'xmlVIEWSTATEDoc = ViewState(VIEWSTATE_XMLDOC)
        xmlVIEWSTATEDoc = ExtensionXMLDoc

        Dim sBPDId As String = ""
        sBPDId = xmlVIEWSTATEDoc.DocumentElement.SelectSingleNode("ThisRental/Rental/LastBpdId").InnerText

        'dtmCheckOutDateTime = Convert.ToDateTime(checkInDateTextBox.Text).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))
        dtmCheckOutDateTime = Aurora.Common.Utility.ParseDateTime(checkInDateTextBox.Text, Aurora.Common.Utility.SystemCulture).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))

        'dtmCheckInDateTime = Convert.ToDateTime(dtcCheckInDate.Text).Add(tcCheckInTime.Time)
        dtmCheckInDateTime = dtcCheckInDate.Date.Add(tcCheckInTime.Time)

        ' fix for Squish #535
        ' compare new co date to old co date , if new > old DONT allow -> show error
        Dim dtmOldCheckInDateTime As DateTime
        dtmOldCheckInDateTime = ParseDateTime(checkInDateTextBox.Text, SystemCulture).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))
        If dtmCheckInDateTime < dtmOldCheckInDateTime Then
            CurrentPage.SetInformationShortMessage("This new check in date must be after the original check in date")
            Exit Sub
        End If
        ' fix for Squish #535

        txtCheckInExtensionPeriod.Text = BookingExtension.GetHirePeriod(sBPDId, dtmCheckOutDateTime, dtmCheckInDateTime, ExtensionUOM)



    End Sub

    Protected Sub CheckInTimeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tcCheckInTime.TimeChanged

        ' fix for Squish #535
        CurrentPage.ClearMessages()
        CurrentPage.ClearShortMessage()
        txtCheckInExtensionPeriod.Text = String.Empty
        ' fix for Squish #535

        If Not DatesAndTimesEnteredAreValid() Then
            Exit Sub
        End If

        Dim dtmCheckOutDateTime As DateTime
        Dim dtmCheckInDateTime As DateTime

        Dim xmlVIEWSTATEDoc As System.Xml.XmlDocument
        'xmlVIEWSTATEDoc = ViewState(VIEWSTATE_XMLDOC)
        xmlVIEWSTATEDoc = ExtensionXMLDoc

        Dim sBPDId As String = ""
        sBPDId = xmlVIEWSTATEDoc.DocumentElement.SelectSingleNode("ThisRental/Rental/LastBpdId").InnerText

        'dtmCheckOutDateTime = Convert.ToDateTime(checkInDateTextBox.Text).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))
        dtmCheckOutDateTime = Aurora.Common.Utility.ParseDateTime(checkInDateTextBox.Text, Aurora.Common.Utility.SystemCulture).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))

        'dtmCheckInDateTime = Convert.ToDateTime(dtcCheckInDate.Text).Add(tcCheckInTime.Time)
        dtmCheckInDateTime = dtcCheckInDate.Date.Add(tcCheckInTime.Time)

        ' fix for Squish #535
        ' compare new co date to old co date , if new > old DONT allow -> show error
        Dim dtmOldCheckInDateTime As DateTime
        dtmOldCheckInDateTime = ParseDateTime(checkInDateTextBox.Text, SystemCulture).Add(New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":"c)(0)), CInt(checkInTimeTextBox.Text.Split(":"c)(1)), 0))
        If dtmCheckInDateTime < dtmOldCheckInDateTime Then
            CurrentPage.SetInformationShortMessage("This new check in date must be after the original check in date")
            Exit Sub
        End If
        ' fix for Squish #535

        txtCheckInExtensionPeriod.Text = BookingExtension.GetHirePeriod(sBPDId, dtmCheckOutDateTime, dtmCheckInDateTime, ExtensionUOM)



    End Sub

    Public Sub ShowCheckIn()
        'dtcCheckOutDate.Enabled = False
        'tcCheckOutTime.Enabled = False
        'dtcCheckInDate.Enabled = True
        'tcCheckInTime.Enabled = True
        dtcCheckOutDate.ReadOnly = True
        tcCheckOutTime.ReadOnly = True
        dtcCheckInDate.ReadOnly = False
        tcCheckInTime.ReadOnly = False
    End Sub

    Function ValidateCODateChanges(ByRef COHasChanged As Boolean) As Boolean
        Dim NewCODate, OldCODate As DateTime
        NewCODate = dtcCheckOutDate.Date
        'OldCODate = CDate(checkOutDateTextBox.Text)
        OldCODate = Aurora.Common.Utility.ParseDateTime(checkOutDateTextBox.Text, Aurora.Common.Utility.SystemCulture)

        Dim NewCOTime, OldCOTime As TimeSpan
        NewCOTime = New TimeSpan(tcCheckOutTime.Time.Hours, tcCheckOutTime.Time.Minutes, 0)
        OldCOTime = New TimeSpan(CInt(checkOutTimeTextBox.Text.Split(":")(0)), CInt(checkOutTimeTextBox.Text.Split(":")(1)), 0)

        NewCODate = NewCODate.Add(NewCOTime)
        OldCODate = OldCODate.Add(OldCOTime)

        If NewCODate = OldCODate Then
            COHasChanged = False
        Else
            COHasChanged = True
        End If

        If NewCODate >= OldCODate Then
            Return False
        Else
            Return True
        End If
    End Function

    Function ValidateCIDateChanges(ByRef CIHasChanged As Boolean) As Boolean
        Dim NewCIDate, OldCIDate As DateTime
        NewCIDate = dtcCheckInDate.Date
        'OldCIDate = CDate(checkInDateTextBox.Text)
        OldCIDate = Aurora.Common.Utility.ParseDateTime(checkInDateTextBox.Text, Aurora.Common.Utility.SystemCulture)

        Dim NewCITime, OldCITime As TimeSpan
        NewCITime = New TimeSpan(tcCheckInTime.Time.Hours, tcCheckInTime.Time.Minutes, 0)
        OldCITime = New TimeSpan(CInt(checkInTimeTextBox.Text.Split(":")(0)), CInt(checkInTimeTextBox.Text.Split(":")(1)), 0)

        NewCIDate = NewCIDate.Add(NewCITime)
        OldCIDate = OldCIDate.Add(OldCITime)

        If NewCIDate = OldCIDate Then
            CIHasChanged = False
        Else
            CIHasChanged = True
        End If

        If NewCIDate <= OldCIDate Then
            Return False
        Else
            Return True
        End If
    End Function

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        PreSave()
        ' save, without dvass force
        ''SaveExtensionData(False)
    End Sub

    Sub SaveExtensionData(ByVal ForceDVASSCall As Boolean)
        If Not DatesAndTimesEnteredAreValid() Then
            Exit Sub
        End If

        ' validate CO date change
        Dim COHasChanged, CIHasChanged As Boolean
        COHasChanged = False
        CIHasChanged = False


        If (Not ValidateCODateChanges(COHasChanged)) And (Not ValidateCIDateChanges(CIHasChanged)) Then
            If COHasChanged Then
                CurrentPage.SetInformationShortMessage("This new checkout date must be prior to the original check out date")
            End If

            If CIHasChanged Then
                CurrentPage.SetInformationShortMessage("This new check in date must be after the original check in date")
            End If
            Exit Sub
        End If

        Dim oScreenData As System.Xml.XmlDocument
        'oScreenData = CType(ViewState(VIEWSTATE_XMLDOC), System.Xml.XmlDocument)
        oScreenData = ExtensionXMLDoc
        'update the doc
        UpdateScreenDataXml(oScreenData)

        Dim nScreenRentalIntegrityNumber, nScreenBookingIntegrityNumber As Integer
        'nScreenRentalIntegrityNumber = CInt(ViewState(VIEWSTATE_RENTALINTEGRITYNO))
        'nScreenBookingIntegrityNumber = CInt(ViewState(VIEWSTATE_BOOKINGINTEGRITYNO))
        nScreenRentalIntegrityNumber = CInt(ExtensionRentalIntegrityNumber)
        nScreenBookingIntegrityNumber = CInt(ExtensionBookingIntegrityNumber)

        Dim sReturnedError As String
        ' 21.1.9 - Shoel - DVASS Call is not forced intially
        sReturnedError = BookingExtension.SaveExtension(oScreenData, nScreenRentalIntegrityNumber, nScreenBookingIntegrityNumber, RentalId, BookingId, ForceDVASSCall, CurrentPage.UserCode, "BookingExtensionUserControl.ascx", SlotId, SlotDescription)
        '<todo> check return for SUPR OVRD
        If sReturnedError.IndexOf("DVASSMESSAGE->") > -1 Then
            ' some DVASS message, display and try a forced extension...
            supervisorOverrideControl.Show(Text:="Supervisor Override Required", Role:="OLMAN", ErrorMessage:=sReturnedError.Replace("ERROR/DVASSMESSAGE->", String.Empty))
            Exit Sub
        End If

        If sReturnedError = "" Then
            'CurrentPage.SetInformationShortMessage(CurrentPage.GetMessage("GEN046"))   'GEN046
            GoToProductsTab(CurrentPage.GetMessage("GEN046"))
        ElseIf Len(sReturnedError) > 100 Then
            Replace(sReturnedError, "|", vbNewLine)

            CurrentPage.SetErrorMessage(sReturnedError)
        Else

            CurrentPage.SetErrorShortMessage(sReturnedError)
        End If
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        isFirstLoad = True
        LoadPage()
    End Sub

    Sub UpdateScreenDataXml(ByRef oScreenDataXml As System.Xml.XmlDocument)

        With oScreenDataXml.DocumentElement.SelectSingleNode("//data/ThisRental/Rental")
            '.SelectSingleNode("NewCkoDate").InnerText = dtcCheckOutDate.Text
            .SelectSingleNode("NewCkoDate").InnerText = Aurora.Common.Utility.ParseDateTime(dtcCheckOutDate.Text, Aurora.Common.Utility.SystemCulture).ToShortDateString()
            .SelectSingleNode("NewCkoTm").InnerText = tcCheckOutTime.Text

            '.SelectSingleNode("NewCkiDate").InnerText = dtcCheckInDate.Text
            .SelectSingleNode("NewCkiDate").InnerText = Aurora.Common.Utility.ParseDateTime(dtcCheckInDate.Text, Aurora.Common.Utility.SystemCulture).ToShortDateString()
            .SelectSingleNode("NewCkiTm").InnerText = tcCheckInTime.Text

        End With

        'ViewState.Add(VIEWSTATE_XMLDOC, oScreenDataXml)
        ExtensionXMLDoc = oScreenDataXml

    End Sub

    Function ValidateData(ByVal DoCIDVal As Boolean, ByVal DoCODVal As Boolean, ByVal DoCITVal As Boolean, ByVal DoCOTVal As Boolean) As Boolean

        'Dim oRegexDate As New System.Text.RegularExpressions.Regex("(0[1-9]|[12][0-9]|3[01])[/](0[1-9]|1[012])[/](19|20)\d\d")
        Dim oRegexTime As New System.Text.RegularExpressions.Regex("([0-2]\d):([0-5]\d)")

        Dim bReturnValue As Boolean = True

        If DoCIDVal Then
            If Not dtcCheckInDate.Text.Trim = String.Empty Then
                ' some data
                ' commented cos the new date thingie will kill this
                'If Not oRegexDate.IsMatch(dtcCheckInDate.Text.Trim) Then
                '    'bad data
                '    bReturnValue = False
                '    dtcCheckInDate.Text = checkInDateTextBox.Text
                'End If
            Else
                dtcCheckInDate.Text = checkInDateTextBox.Text
                bReturnValue = False
                ' no data
            End If
        End If


        If DoCODVal Then
            If Not dtcCheckOutDate.Text.Trim = String.Empty Then
                ' some data
                ' commented cos the new date thingie will kill this
                'If Not oRegexDate.IsMatch(dtcCheckOutDate.Text.Trim) Then
                '    'bad data
                '    dtcCheckOutDate.Text = checkOutDateTextBox.Text
                '    bReturnValue = False
                'End If
            Else
                'no data
                bReturnValue = False
                dtcCheckOutDate.Text = checkOutDateTextBox.Text
            End If
        End If


        If DoCITVal Then
            If Not tcCheckInTime.Text.Trim = String.Empty Then
                'some data
                If Not oRegexTime.IsMatch(tcCheckInTime.Text.Trim) Then
                    'bad data
                    bReturnValue = False
                    tcCheckInTime.Text = checkInTimeTextBox.Text
                End If
            Else
                'no data
                bReturnValue = False
                tcCheckInTime.Text = checkInTimeTextBox.Text
            End If
        End If

        If DoCOTVal Then
            If Not tcCheckOutTime.Text.Trim = String.Empty Then
                'some data
                If Not oRegexTime.IsMatch(tcCheckOutTime.Text.Trim) Then
                    'bad data
                    bReturnValue = False
                    tcCheckOutTime.Text = tcCheckOutTime.Text
                End If
            Else
                'no data
                bReturnValue = False
                tcCheckOutTime.Text = tcCheckOutTime.Text
            End If
        End If

        Return bReturnValue

    End Function

    Public Sub GoToProductsTab(Optional ByVal MessageToShow = "")

        Dim sScript As String
        sScript = vbNewLine & "window.location='Booking.aspx?hdBookingId=" & BookingId & "&hdRentalId=" & RentalId & "&activeTab=9&msgToShow=" & MessageToShow & "';"
        ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, sScript, True)

    End Sub

    Function DatesAndTimesEnteredAreValid() As Boolean

        Dim bIsDateValid, bIsTimeValid As Boolean
        bIsDateValid = True
        bIsTimeValid = True

        If Not dtcCheckOutDate.IsValid Or dtcCheckOutDate.Date = DateTime.MinValue Then
            bIsDateValid = False
        End If
        If Not dtcCheckInDate.IsValid Or dtcCheckInDate.Date = DateTime.MinValue Then
            bIsDateValid = False
        End If

        If Not tcCheckInTime.IsValid Or tcCheckInTime.Time = TimeSpan.MinValue Then
            bIsTimeValid = False
        End If
        If Not tcCheckOutTime.IsValid Or tcCheckOutTime.Time = TimeSpan.MinValue Then
            bIsTimeValid = False
        End If

        If bIsDateValid Then
            If bIsTimeValid Then
                ' Good Dates and Times
                Return True
            Else
                ' Good Dates and Bad Times
                CurrentPage.SetWarningShortMessage("Enter time in HH:MM format")
            End If
        Else
            ' Bad Dates
            CurrentPage.SetWarningShortMessage("Enter date in dd/mm/yyyy format")
            Return False
        End If

    End Function

    Public Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal IsOkButton As Boolean, ByVal param As String) Handles supervisorOverrideControl.PostBack
        If IsOkButton Then
            ' see if its a valid supervisor
            Dim sReturnMsg As String = ""
            sReturnMsg = Aurora.Common.Service.CheckUser(supervisorOverrideControl.Login, supervisorOverrideControl.Password, "OLMAN")

            If sReturnMsg = "True" Then
                ' save, with dvass force
                SaveExtensionData(True)
            Else
                supervisorOverrideControl.Show(ErrorMessage:=Server.HtmlEncode(sReturnMsg))
            End If
        End If
    End Sub

#Region "rev:mia 16jan2014-addition of supervisor override password"
    ''rev:mia 16Jan2015 - Today in PH history, Pope Francis is in Manila

    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property

    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property

    Private Property OriginalCheckOutDate As String
        Get
            Return CStr(ViewState("OriginalCheckOutDate"))
        End Get
        Set(value As String)
            ViewState("OriginalCheckOutDate") = value
        End Set
    End Property

    Sub PreSave()


        Dim newCheckOutDateString As String = dtcCheckOutDate.Text
        Dim newCheckOutDate As Date = Aurora.Common.Utility.ParseDateTime(newCheckOutDateString, Aurora.Common.Utility.SystemCulture)

        If (Not String.IsNullOrEmpty(OriginalCheckOutDate)) Then
            If (Not OriginalCheckOutDate.Equals(newCheckOutDateString)) Then
                If (SlotAvailableControl.IsSlotRequired(Nothing, RentalId, PickupDate:=newCheckOutDateString) = True) Then
                    SlotAvailableControl.NoOfBulkBookings = 1 ''set to one
                    SlotAvailableControl.SupervisorOverride = False
                    SlotAvailableControl.Show(Nothing, RentalId, PickupDate:=newCheckOutDateString)
                Else
                    Save()
                End If
            Else
                Save()
            End If
        End If

    End Sub

    Sub Save()
        SaveExtensionData(False)
    End Sub

    Protected Sub SlotAvailableControl_PostBack(sender As Object, isSaveButton As Boolean, param As String) Handles SlotAvailableControl.PostBack
        If (isSaveButton) Then
            SlotId = param.Split("|")(1)
            SlotDescription = param.Split("|")(2)
            Save()
            Logging.LogDebug("BookingExtension SlotAvailableControl_PostBack", "SlotId: " & SlotId & ",SlotDescription: " & SlotDescription)
        End If
    End Sub

 

    
#End Region
End Class
