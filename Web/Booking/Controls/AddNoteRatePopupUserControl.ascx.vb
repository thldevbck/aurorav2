
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

Partial Class Booking_Controls_AddNoteRatePopUpUserControl
    Inherits AuroraUserControl

    Public Delegate Sub AddNoteRatePopUpUserControlEventHandler(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object)

    Public Event PostBack As AddNoteRatePopUpUserControlEventHandler

    'Private _delPostBack As System.Delegate
    'Public Property PostBack1() As System.Delegate
    '    Get
    '        Return _delPostBack 'ViewState(AddNoteRatePopUpUserControl_DelPostBack_ViewState)
    '    End Get
    '    Set(ByVal Value As System.Delegate)
    '        _delPostBack = Value
    '        'ViewState(AddNoteRatePopUpUserControl_DelPostBack_ViewState) = Value
    '    End Set
    'End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

            End If
        Catch ex As Exception
            CurrentPage.LogException(ex)
            CurrentPage.AddErrorMessage("An error occurred while loading the booking change over tab.")
        End Try
    End Sub

    Public Sub loadPopup()
        textLabel.Text = "Please enter reason for exchange and how you calculated the rate!!"
        titleLabel.Text = "Add note and rate"
        programmaticAddNoteRatePopup.Show()

        okButton.Attributes.Add("OnClick", "javascript:return submitData('" + noteTextBox.ClientID + "','" + varianceTextBox.ClientID + "','" + addNoteRatePopupMessagePanelControl.ClientID + "');")

        ' showAddNoteRatePopupViaClient()

        'programmaticAddNoteRatePopup.BehaviorID
        'submitData(noteTextBoxId, varianceTextBoxId, addNoteRatePopupMessagePanelControlId)

    End Sub

    Protected Sub okButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles okButton.Click
        'saveNote()
        programmaticAddNoteRatePopup.Hide()

        'Invoke PopupPostBack event to redatabind the grid
        Dim param As ArrayList = New ArrayList(2)
        param.Insert(0, noteTextBox.Text)
        param.Insert(1, varianceTextBox.Text)
        'PostBack.DynamicInvoke(True, param)

        RaiseEvent PostBack(Me, True, False, param)
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As EventArgs) Handles cancelButton.Click
        programmaticAddNoteRatePopup.Hide()

        'Invoke PopupPostBack event to redatabind the grid
        'PostBack.DynamicInvoke(False, Nothing)
        RaiseEvent PostBack(Me, False, True, Nothing)
    End Sub



End Class
