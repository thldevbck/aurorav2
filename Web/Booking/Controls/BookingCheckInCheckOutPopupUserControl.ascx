<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookingCheckInCheckOutPopupUserControl.ascx
''	Date Created	-	1.5.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	Shoel : 1.5.8 / Base version
''                      Shoel :  ID4  / Messages in the popup now use the 'popupMessagePanel' css class
''
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BookingCheckInCheckOutPopupUserControl.ascx.vb"
    Inherits="Booking_Controls_BookingCheckInCheckOutPopupUserControl" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/MessagePanelControl/MessagePanelControl.ascx" TagName="MessagePanelControl"
    TagPrefix="uc1" %>

<%--<script type="text/javascript">
        
        // Add click handlers for buttons to show and hide modal popup on pageLoad
        function pageLoad() {
            //$addHandler($get("showModalPopupClientButton"), 'click', showModalPopupViaClient);
            $addHandler($get("programmaticModalPopup"), 'click', hideModalPopupViaClient);        
        }      
        function hideModalPopupViaClient() {
            //ev.preventDefault();        
            var modalPopupBehavior = $find('programmaticBookingCICOPopupBehavior');
            modalPopupBehavior.hide();
        }
</script>--%>

<asp:Button runat="server" ID="hiddenTargetControlForModalPopup" Style="display: none" />
<ajaxToolkit:ModalPopupExtender runat="server" ID="programmaticModalPopup" BehaviorID="programmaticBookingCICOPopupBehavior"
    TargetControlID="hiddenTargetControlForModalPopup" PopupControlID="programmaticPopup"
    BackgroundCssClass="modalBackground" DropShadow="True" PopupDragHandleControlID="programmaticPopupDragHandle">
</ajaxToolkit:ModalPopupExtender>
<asp:Panel runat="server" CssClass="modalPopup" ID="programmaticPopup" Style="display: none;
    width: 520px; height: 400px">
    <asp:Panel runat="Server" ID="programmaticPopupDragHandle" CssClass="modalPopupTitle">
        Enter Serial Numbers
    </asp:Panel>
    <br />
    <uc1:MessagePanelControl ID="bookingCICOPopupMessagePanelControl" runat="server" CssClass="popupMessagePanel"/>
    
    <table width="100%">
        <tr>
            <td>
                <div style="overflow: auto; height: 300px;width:100%">
                    <asp:Repeater ID="rptMain" runat="server" EnableViewState="true">
                        <HeaderTemplate>
                            <table cellspacing="0" cellpadding="0" class="dataTable" id="tblMain">
                                <tr>
                                    <th style="display: none;">
                                        ProductId
                                    </th>
                                    <th style="width:100px;">
                                        Product Name
                                    </th>
                                    <th>
                                        Serial Number
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr valign="top" class="evenRow">
                                <td valign="baseline" style="display: none">
                                    <%--<span datafld="BpdId"></span>--%>
                                    <asp:HiddenField ID="hdnBPDID" runat="server" Value='<%# Bind("BpdId") %>' />
                                </td>
                                <td valign="baseline">
                                    <%#Container.DataItem("PrdName")%>
                                    <asp:HiddenField ID="hdnPrdName" runat="server" Value='<%# Bind("PrdName") %>' />
                                    <%--    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span datafld="PrdName"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                </td>
                                <td valign="baseline">
                                    <asp:TextBox CssClass="Textbox_Large" runat="server" ID="txtSerialNumber" Text='<%#Bind("SerialNum") %>'></asp:TextBox>
                                    <%--    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input datafld="SerialNum" style="width: 250px"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr valign="top" class="oddRow">
                                <td valign="baseline" style="display: none">
                                    <%--<span datafld="BpdId"></span>--%>
                                    <asp:HiddenField ID="hdnBPDID" runat="server" Value='<%# Bind("BpdId") %>' />
                                </td>
                                <td valign="baseline">
                                    <%#Container.DataItem("PrdName")%>
                                    <asp:HiddenField ID="hdnPrdName" runat="server" Value='<%# Bind("PrdName") %>' />
                                    <%--    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span datafld="PrdName"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                </td>
                                <td valign="baseline">
                                    <asp:TextBox CssClass="Textbox_Large" runat="server" ID="txtSerialNumber" Text='<%#Bind("SerialNum") %>'></asp:TextBox>
                                    <%--    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input datafld="SerialNum" style="width: 250px"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;--%>
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
    </table>
    <br />
</asp:Panel>
