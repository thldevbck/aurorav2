﻿Imports System.Data
Imports Aurora.Common
Imports Aurora.Common.Logging
Imports System.Xml
Imports Aurora.Reservations.Services


Partial Class UserControls_ConfirmationBox_ExtensionListener_ExtensionListener
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim qs As String = Request.QueryString("Mode")
        Dim result As String = ""

        If qs.Equals("EXTENSION") = True Then
            Dim requests As String = Request("EXTENSION")

            Dim branchIn As String = requests.Split("|")(0)
            Dim branchOut As String = requests.Split("|")(1)
            Dim dteCKO As String = requests.Split("|")(2)
            Dim dteCKI As String = requests.Split("|")(3)
            Dim countrytext As String = requests.Split("|")(4)
            Dim sagent As String = requests.Split("|")(5)
            Dim product As String = requests.Split("|")(6)
            Dim bkrid As String = requests.Split("|")(7)
            Dim hirePeriod As String = requests.Split("|")(8)
            Dim Booid As String = requests.Split("|")(9)
            Dim userCode As String = requests.Split("|")(10)
            Dim brandtosearch As String = requests.Split("|")(13)
            ''''rev:mia Oct 16 2014 - addition of PromoCode
            Dim promocode As String = requests.Split("|")(14)

            If brandtosearch = "null" Then
                brandtosearch = "ALL"
            Else
                If brandtosearch.EndsWith(",") Then
                    brandtosearch = brandtosearch.Substring(0, brandtosearch.Length - 1)
                End If
                Dim aryBrands() As String = brandtosearch.Split(",")
                brandtosearch = ""
                For Each item As String In aryBrands
                    brandtosearch = brandtosearch & item & ","
                Next
                If brandtosearch.EndsWith(",") Then
                    brandtosearch = brandtosearch.Substring(0, brandtosearch.Length - 1)
                End If
            End If
            result = QuickAvailExtension(branchIn, _
                                                                   branchOut, _
                                                                   dteCKO, _
                                                                   dteCKI, _
                                                                   countrytext, _
                                                                   sagent, _
                                                                   product, _
                                                                   bkrid, _
                                                                   hirePeriod, _
                                                                   Booid, _
                                                                   userCode, _
                                                                   brandtosearch, _
                                                                   promocode) ''rev:mia Oct 16 2014 - addition of PromoCode


        ElseIf qs.Equals("EXTENSIONMANAGEREQUEST") Then
            Dim requests As String = Request("EXTENSIONMANAGEREQUEST")
            Dim branchOut As String = requests.Split("|")(1)
            Dim sagent As String = requests.Split("|")(5)
            Dim product As String = requests.Split("|")(6)
            Dim bkrid As String = requests.Split("|")(7)
            Dim newVhriD As String = requests.Split("|")(11)
            Dim newBkriD As String = requests.Split("|")(12)

            result = ManageBookingRequestInVehicleExtension(sagent, _
                                                            branchOut, _
                                                            product, _
                                                            bkrid, _
                                                            newVhriD, _
                                                            newBkriD)
        End If

        Response.Write(result)
        Response.End()
    End Sub


    Private Function ManageBookingRequestInVehicleExtension(ByVal sagent As String, _
                                          ByVal branchOut As String, _
                                          ByVal product As String, _
                                          ByVal bkrid As String, _
                                          ByVal Newvhrid As String, _
                                                                   ByVal Newbkrid As String) As String
        Dim sourceDs As New DataSet
        Try
            Aurora.Common.Data.ExecuteDataSetSP("BookingProcess_getQuickAvails", sourceDs, _
                                                                                sagent, _
                                                                                branchOut, _
                                                                                product)

            For Each drow As DataRow In sourceDs.Tables(0).Rows
                Dim extensionProduct As String = ExtensionXMLwithProductID(drow("prdid"), Newbkrid, Newvhrid)
                Dim strResult As String = Aurora.Booking.Services.BookingRequest.ManageBookingRequest(extensionProduct)
                ''LogInformation("ManageBookingRequestInVehicleExtension: ", drow("prdid") & " / " & Newbkrid & " >>> return error: " & Aurora.Booking.Services.BookingRequest.ReturnError)
            Next

        Catch ex As Exception
            Return "ERROR"
        End Try
        Return "OK"
    End Function

    Private Function QuickAvailExtension(ByVal branchIn As String, _
                                                                   ByVal branchOut As String, _
                                                                   ByVal dteCKO As String, _
                                                                   ByVal dteCKI As String, _
                                                                   ByVal countrytext As String, _
                                                                   ByVal sagent As String, _
                                                                   ByVal product As String, _
                                                                   ByVal bkrid As String, _
                                                                   ByVal hirePeriod As String, _
                                                                   ByVal Booid As String, _
                                                                   ByVal userCode As String, _
                                                                   ByVal brandtoselect As String, _
                                                                   ByVal promocode As String) As String ''''rev:mia Oct 16 2014 - addition of PromoCode

        Dim objExtension As New ExtensionParemeter
        objExtension.branchIn = branchIn
        objExtension.branchOut = branchOut
        objExtension.dteCKO = dteCKO
        objExtension.dteCKI = dteCKI
        objExtension.countrytext = countrytext
        objExtension.agentId = sagent
        objExtension.product = product
        objExtension.bkrid = bkrid
        objExtension.hirePeriod = hirePeriod
        objExtension.Booid = Booid
        objExtension.userCode = userCode


        Dim qrytext As String = ""

        Dim sb As New StringBuilder
        Dim sourceDs As New DataSet
        Dim count As Integer = 0
        Try


            ''LogInformation("QuickAvailExntesion", "BookingProcess_getQuickAvails")
            Aurora.Common.Data.ExecuteDataSetSP("BookingProcess_getQuickAvails", sourceDs, _
                                                                                 objExtension.agentId, _
                                                                                 objExtension.branchOut, _
                                                                                 objExtension.product, _
                                                                                 brandtoselect, _
                                                                                 objExtension.branchIn, _
                                                                                 objExtension.dteCKO, _
                                                                                 objExtension.dteCKI)



            sb.Append("<table width='100%' border='0' class='dataTableColor searchResultTable' id='quickVehAvailTable'>")

            'If (objExtension.userCode = "ma2") Then
            sb.AppendFormat("<tr><td width='10'>{0}</td><td width='150'>{1}</td><td width='10'>{2}</td><td width='10'>{3}</td><td width='10'>{4}</td></tr>", "<b>NAME</b>", "<B>FULLNAME</B>", "<b>QTY</B>", "<B>BRAND</B>", "<B>SELECT</B>")
            'Else
            '    sb.AppendFormat("<tr><td width='10'>{0}</td><td width='150'>{1}</td><td width='10'>{2}</td><td width='10'>{3}</td></tr>", "<b>NAME</b>", "<B>FULLNAME</B>", "<b>QTY</B>", "<B>BRAND</B>")
            'End If


            Dim rowclass As String = ""
            For Each drow As DataRow In sourceDs.Tables(0).Rows

                Dim extensionProduct As String = ExtensionXMLwithProductID(drow("prdid"), "", "")
                Dim strResult As String = Aurora.Booking.Services.BookingRequest.ManageBookingRequest(extensionProduct)


                Dim rows As String = ImmediateAvailableCheckingExtension("PM", _
                                                     "PM", _
                                                     objExtension.branchOut, _
                                                     objExtension.branchIn, _
                                                     objExtension.dteCKO, _
                                                     objExtension.dteCKI, _
                                                     drow("prdid"), _
                                                     drow("Name"), _
                                                     drow("FullName"), _
                                                     objExtension.countrytext, _
                                                     drow("PrdbrdCode"), _
                                                     objExtension.bkrid, _
                                                     objExtension.hirePeriod, _
                                                     objExtension.Booid, _
                                                     objExtension.userCode, _
                                                     promocode) ''rev:mia Oct 16 2014 - addition of PromoCode

                '' LogInformation("QuickAvailExntesion end", rows)

                If Not String.IsNullOrEmpty(rows) Then
                    count = count + 1
                    sb.AppendFormat("{0}", rows)
                End If

            Next
            sb.Append("</table>")
            sb.AppendFormat("<br/><center>Press <b>'Esc'</b> to close this window</br>or</br>double click on the Vehicle <b>'Select'</b> checkbox</center>")
            rowclass = ""
        Catch ex As Exception
            Return "Failed"
            Logging.LogInformation("QuickAvailExtension ERROR: ", ex.Message & vbCrLf & ex.StackTrace)
        End Try

        If count > 0 Then
            Logging.LogInformation("QuickAvailExtensior: ", sb.ToString)
            Return sb.ToString
        Else
            sb = Nothing
            sb = New StringBuilder
            sb.AppendFormat("<center><h2>No vehicle available...</h2>")
            sb.AppendFormat("Press <b>'Esc'</b> to close this window</center>")
            Return sb.ToString
        End If


    End Function

    Private Shared _rowclass As String
    Private Shared Property rowClassExtension As String
        Get
            Return _rowclass
        End Get
        Set(ByVal value As String)
            _rowclass = value
        End Set
    End Property


    Function ImmediateAvailableCheckingExtension(ByVal ampmCKO As String, _
                                               ByVal ampmCKI As String, _
                                               ByVal branchOut As String, _
                                               ByVal branchIn As String, _
                                               ByVal dteCKO As String, _
                                               ByVal dteCKI As String, _
                                               ByVal productid As String, _
                                               ByVal product As String, _
                                               ByVal productname As String, _
                                               ByVal countrytext As String, _
                                               ByVal brand As String, _
                                               ByVal bkrid As String, _
                                               ByVal hirePeriod As String, _
                                               ByVal Booid As String, _
                                               ByVal userCode As String, _
                                               ByVal promocode As String) As String ''rev:mia Oct 16 2014 - addition of PromoCode


        Dim xmlstringAvailable As String = ""
        Dim sb As New StringBuilder


        Try

            'LogInformation("EXTENSION Listener START: ", vbCrLf & "------------------------------------------------------" & vbCrLf)
            'LogInformation("EXTENSION Listener ImmediateAvailableCheckingExtension: ", _
            '                vbCrLf & "BkrID: " & bkrid _
            '                & vbCrLf & " ,branchOut: " & branchOut _
            '                & vbCrLf & " ,dteCKO: " & dteCKO _
            '                & vbCrLf & " ,ampmCKO(PM): " & ampmCKO _
            '                & vbCrLf & " ,branchIn: " & branchIn _
            '                & vbCrLf & " ,dteCKI: " & dteCKI _
            '                & vbCrLf & " ,ampmCKI(PM): " & ampmCKI _
            '                & vbCrLf & " ,txtHirePeriod: " & hirePeriod _
            '                & vbCrLf & " ,BooID: " & Booid _
            '                & vbCrLf & " ,CountryCode: " & countrytext _
            '                & vbCrLf & " ,ProductID: " & productid & "/" & productname _
            '                & vbCrLf & " ,rowClassExtension: " & rowClassExtension & vbCrLf & vbCrLf)
            'LogInformation("EXTENSION Listener END: ", vbCrLf & "------------------------------------------------------" & vbCrLf)

            xmlstringAvailable = ManageAvailability.GetAvailabilityWithCost _
                                                  (bkrid, _
                                                   branchOut, _
                                                   dteCKO, _
                                                   "PM", _
                                                   branchIn, _
                                                   dteCKI, _
                                                   "PM", _
                                                   hirePeriod, _
                                                   productid, _
                                                   "0", _
                                                   Booid, _
                                                   "", _
                                                   String.Empty, _
                                                   String.Empty, _
                                                   String.Empty, _
                                                   String.Empty, _
                                                   userCode, _
                                                   "BookingProcess.aspx/ImmediateAvailableCheckingExtension", _
                                                   countrytext, _
                                                   String.Empty, _
                                                   True, _
                                                    promocode) ''rev:mia Oct 16 2014 - addition of PromoCode



            Dim xmlAvail As New XmlDocument
            Dim avs As String = ""
            Try
                xmlAvail.LoadXml(xmlstringAvailable)


                If Not xmlAvail.SelectSingleNode("//Data/AvailableVehicle/NumberOfVehicles") Is Nothing Then
                    avs = xmlAvail.SelectSingleNode("//Data/AvailableVehicle/NumberOfVehicles").InnerText

                    If (CInt(avs) = 0) Then Return ""

                    Dim errors As String = IIf(String.IsNullOrEmpty(Aurora.Booking.Services.BookingRequest.ReturnError), "", Aurora.Booking.Services.BookingRequest.ReturnError)
                    Dim warnings As String = Aurora.Booking.Services.BookingRequest.ReturnWarning

                    Dim linkForProduct As String
                    Dim fullname As String = String.Concat(product, " - ", productname)
                    If String.IsNullOrEmpty(warnings) Then
                        linkForProduct = "<input id = '" & product & "_chkQuickAvail' type='checkbox'   value='" & fullname & "' />"
                    Else
                        linkForProduct = "<input id = '" & product & "_chkQuickAvail' type='checkbox'   value='" & product & "'  disabled='disabled' title='" & warnings & "'/><i>n.a.</i>"
                    End If




                    If String.IsNullOrEmpty(rowClassExtension) Then
                        rowClassExtension = "evenRow"
                    Else
                        If rowClassExtension = "evenRow" Then
                            rowClassExtension = "oddRow"
                        ElseIf rowClassExtension = "oddRow" Then
                            rowClassExtension = "evenRow"
                        End If
                    End If
                    Select Case brand.ToUpper
                        Case "B"
                            brand = "Britz"
                            Exit Select
                        Case "XX" ''rev:mia may 23, 2012 -- no longer use
                            brand = "ExploreMore"
                            Exit Select
                        Case "M"
                            brand = "Maui"
                            Exit Select
                        Case "P"
                            brand = "BackPacker"
                            Exit Select
                        Case "Y"
                            brand = "Mighty"
                            Exit Select

                            ''rev:mia July 20, 2012 -- Added KEA
                        Case "Q"
                            brand = "KEA"
                            Exit Select

                            ''rev:mia Oct. 16 2012 -- Added UNITED/ECONO/ALPINE
                        Case "U"
                            brand = "United"
                            Exit Select
                        Case "E"
                            brand = "Econo"
                            Exit Select
                        Case "A"
                            brand = "Alpha"
                            Exit Select
                    End Select

                    sb.AppendFormat("<tr class={0}><td width='10'>{1}</td><td width='150'>{2}</td><td width='10'>{3}</td><td width='10'>{4}</td><td width='10'>{5}</td></tr>", rowClassExtension, Left(product, 10), productname, avs, brand.Trim, linkForProduct)

                End If

            Catch ex As Exception
                Logging.LogInformation("ImmediateAvailableChecking ERROR: ", xmlstringAvailable)
                Return ""
            End Try
        Catch ex As Exception
            Logging.LogInformation("ImmediateAvailableChecking ERROR: ", xmlstringAvailable)
        End Try

        Return sb.ToString
    End Function

    Function ExtensionXMLwithProductID(ByVal productID As String, ByVal bkrid As String, ByVal vhrid As String) As String
        Dim xmlextension As New XmlDocument
        ''comes from the bookingprocess
        Dim extensionxml As String = Session("ExtensionXML")
        xmlextension.LoadXml(extensionxml)
        xmlextension.SelectSingleNode("Root/NewBookingRequest/LastName").InnerText = "QuickAvail"
        xmlextension.SelectSingleNode("Root/NewBookingRequest/PrdId").InnerText = productID
        ''xmlextension.SelectSingleNode("Root/NewBookingRequest/BkrId").InnerText = bkrid
        ''xmlextension.SelectSingleNode("Root/NewBookingRequest/VhrId").InnerText = vhrid
        Return xmlextension.OuterXml

    End Function

End Class


Public Class ExtensionParemeter
    Public branchOut As String
    Public branchIn As String
    Public dteCKO As String
    Public dteCKI As String
    Public product As String
    Public countrytext As String
    Public agentId As String
    Public bkrid As String
    Public hirePeriod As String
    Public Booid As String
    Public userCode As String
    Public Newvhrid As String
    Public Newbkrid As String

End Class