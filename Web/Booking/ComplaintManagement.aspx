<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="ComplaintManagement.aspx.vb" Inherits="Booking_ComplaintManagement"
    Title="Complaint Management" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript" src="../OpsAndLogs/JS/OpsAndLogsCommon.js"></script>

    <script language="javascript" type="text/javascript">
    function GetBackURL()
    {
        if(window.opener.location.search.indexOf('&activeTab=')!=-1)
        {
	        if(window.opener.location.search.indexOf('&',window.opener.location.search.indexOf('&activeTab=')+1)!=-1)
	        {
		        return window.opener.location.pathname + window.opener.location.search.substring(0,window.opener.location.search.indexOf('=',window.opener.location.search.indexOf('&activeTab='))+1) + '4' + window.opener.location.search.substring(window.opener.location.search.indexOf('&',window.opener.location.search.indexOf('&activeTab=')+1));
	        }
	        else
	        {
		        return window.opener.location.pathname + window.opener.location.search.substring(0,window.opener.location.search.indexOf('=',window.opener.location.search.indexOf('&activeTab='))+1) + '4';
	        }
        }
        else
        {
            return window.opener.location.pathname + window.opener.location.search + '&activeTab=4'
        }
    }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <%--    <asp:UpdatePanel ID="updMain" runat="server">
        <ContentTemplate>--%>
    <table id="tblMain" runat="server">
        <tr>
            <td>
                <table id="tblIncidentDetails" cellspacing="0" cellpadding="0" width="800px" border="0">
                    <tbody>
                        <tr>
                            <td width="150">
                                Booking/Rental:</td>
                            <td width="250">
                                <asp:TextBox ID="lblBookingRentalNumber" runat="server" CssClass="Textbox_Standard"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                            <td width="150">
                                AIMS Ref No:</td>
                            <td width="250">
                                <asp:TextBox ID="lblAIMSRefNo" runat="server" CssClass="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="150">
                                Hirer:</td>
                            <td width="250">
                                <asp:TextBox ID="lblHirer" runat="server" CssClass="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                            </td>
                            <td width="150">
                                Package:</td>
                            <td width="250">
                                <asp:TextBox ID="lblPackage" runat="server" CssClass="Textbox_Standard" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="150">
                                Agent:
                            </td>
                            <td colspan="3">
                                <asp:TextBox Style="width: 230px" ID="lblAgent" runat="server" CssClass="Textbox_Standard"
                                    ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table id="tblVehicles" runat="server" cellspacing="1" cellpadding="1" width="800px"
                    border="0">
                    <tbody>
                        <tr>
                            <td>
                                <strong>Vehicles</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="grdVehicles" class="dataTable" runat="server" Width="100%" AutoGenerateColumns="false"
                                    DataKeyNames="ActivityId">
                                    <AlternatingRowStyle CssClass="oddRow" />
                                    <RowStyle CssClass="evenRow" />
                                    <Columns>
                                        <asp:BoundField DataField="CheckOut" HeaderText="Check-Out">
                                            <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                            <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CheckIn" HeaderText="Check-In">
                                            <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                            <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Vehicle" HeaderText="Vehicle">
                                            <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                            <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UnitNo" HeaderText="Unit No/Reg">
                                            <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                            <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText=" Select* ">
                                            <ItemTemplate>
                                                <input type="radio" name="rdgVehicle" <%# Eval("Selected") %> <%# Eval("Disabled") %>
                                                    value='<%# Eval("ActivityId") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" BorderStyle="None" />
                                            <ItemStyle HorizontalAlign="Left" BorderStyle="None" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server" ID="updSummary">
                    <ContentTemplate>
                        <asp:Panel ID="pnlComplaintOverview" runat="server">
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <strong>Complaint Overview</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150px">
                                        Status:
                                    </td>
                                    <td width="250px">
                                        <%-- 
                                    Complaint status : 
                                    0 - open
            				        1 - in progress
					                2 - resolved
					                --%>
                                        <asp:DropDownList ID="cmbStatus" runat="server" AutoPostBack="true" CssClass="Dropdown_Standard">
                                            <%--onchange="if(this.value==2){return ShowLabels();}">--%>
                                            <asp:ListItem Text="Open" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="In Progress" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Resolved" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Raised in Error" Value="3"></asp:ListItem>
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                    <td width="150px">
                                        Branch:
                                    </td>
                                    <td width="250px">
                                        <uc1:PickerControl ID="pkrBranch" runat="server" Width="147px" PopupType="LOCATION"
                                            AppendDescription="true"></uc1:PickerControl>
                                        &nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Notified:
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cmbNotified" runat="server" CssClass="Dropdown_Standard">
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                    <td>
                                        Complaint Owner (Group):
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cmbComplaintOwnerGroup" runat="server" OnSelectedIndexChanged="cmbComplaintOwnerGroup_SelectedIndexChanged"
                                            AutoPostBack="true" CssClass="Dropdown_Standard">
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Complaint Handler:
                                    </td>
                                    <td>
                                        <uc1:PickerControl ID="pkrHandler" runat="server" PopupType="GETCOMPLAINTHANDLERS"
                                            AppendDescription="true" Width="147px" />
                                    </td>
                                    <td>
                                        Complaint Owner (Individual):
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="cmbComplaintOwnerIndividual" runat="server" CssClass="Dropdown_Standard">
                                        </asp:DropDownList>&nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Date Received:
                                    </td>
                                    <td>
                                        <uc1:DateControl ID="dtReceived" runat="server" AutoPostBack="true"></uc1:DateControl>
                                        &nbsp;*
                                    </td>
                                    <td>
                                        Date Resolved:
                                    </td>
                                    <td>
                                        <uc1:DateControl ID="dtResolved" runat="server" AutoPostBack="true"></uc1:DateControl>
                                        <asp:Label runat="server" ID="lblCompulsory1">&nbsp;*</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Stat Holidays During
                                        <br />
                                        Resolution Period (Days):
                                    </td>
                                    <td nowrap="nowrap">
                                        <asp:DropDownList ID="cmbStatHols" runat="server" AutoPostBack="true" CssClass="Dropdown_Small">
                                            <asp:ListItem Value="" Text="" Selected="true" />
                                            <asp:ListItem Value="0" Text="0" />
                                            <asp:ListItem Value="1" Text="1" />
                                            <asp:ListItem Value="2" Text="2" />
                                            <asp:ListItem Value="3" Text="3" />
                                            <asp:ListItem Value="4" Text="4" />
                                            <asp:ListItem Value="5" Text="5" />
                                        </asp:DropDownList><asp:Label runat="server" ID="lblCompulsory2">&nbsp;*</asp:Label>
                                    </td>
                                    <td>
                                        Working Days to Resolve:
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" ID="txtWorkingDaysToResolve" TabIndex="-1" ReadOnly="true"
                                            CssClass="Textbox_Readonly_Tiny"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlSummaryOfComplaint" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <strong>Summary of Complaint</strong>&nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSummaryOfComplaint" runat="server" MaxLength="1024" TextMode="MultiLine"
                                            onblur="if(this.value.length>1024){this.value=this.value.substring(0,1024);}"
                                            Style="width: 800px; height: 100px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlSummaryOfResolution" runat="server">
                            <table>
                                <tr>
                                    <td nowrap="nowrap">
                                        <strong>Summary of Resolution</strong><asp:Label runat="server" ID="lblCompulsory3">&nbsp;*</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtSummaryOfResolution" runat="server" MaxLength="1024" TextMode="MultiLine"
                                            onblur="if(this.value.length>1024){this.value=this.value.substring(0,1024);}"
                                            Style="width: 800px; height: 100px" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlOriginalsHeldAt" runat="server">
                            <table>
                                <tr>
                                    <td>
                                        <strong>Originals held at:</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtOriginalsHeldAt" runat="server" MaxLength="1024" onblur="if(this.value.length>1024){this.value=this.value.substring(0,1024);}"
                                            Style="width: 400px;" />&nbsp;*
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="updRCA" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlRootCauseAnalysis" runat="server">
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <strong>Root Cause Analysis</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150px" nowrap="nowrap">
                                        Primary Root Cause:
                                    </td>
                                    <td width="250px" nowrap="nowrap">
                                        <asp:DropDownList ID="cmbPrimaryRootCause" runat="server" OnSelectedIndexChanged="cmbPrimaryRootCause_SelectedIndexChanged"
                                            CssClass="Dropdown_Large" AutoPostBack="true">
                                        </asp:DropDownList><asp:Label runat="server" ID="lblCompulsory4">&nbsp;*</asp:Label>
                                    </td>
                                    <td width="100px" nowrap="nowrap">
                                        Sub Cause:
                                    </td>
                                    <td width="350px" nowrap="nowrap">
                                        <asp:DropDownList ID="cmbPrimarySubCause" runat="server" CssClass="Dropdown_Large"
                                            Style="width: 350px">
                                        </asp:DropDownList><asp:Label runat="server" ID="lblCompulsory5">&nbsp;*</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        Detail:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtPrimaryDetail" runat="server" Style="width: 345px;"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap">
                                        Secondary Root Cause:
                                    </td>
                                    <td nowrap="nowrap">
                                        <asp:DropDownList ID="cmbSecondaryRootCause" runat="server" OnSelectedIndexChanged="cmbSecondaryRootCause_SelectedIndexChanged"
                                            CssClass="Dropdown_Large" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td nowrap="nowrap">
                                        Sub Cause:
                                    </td>
                                    <td nowrap="nowrap">
                                        <asp:DropDownList ID="cmbSecondarySubCause" runat="server" CssClass="Dropdown_Large"
                                            Style="width: 350px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        Detail:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtSecondaryDetail" runat="server" Style="width: 345px;"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td nowrap="nowrap">
                                        Tertiary Root Cause:
                                    </td>
                                    <td nowrap="nowrap">
                                        <asp:DropDownList ID="cmbTertiaryRootCause" runat="server" OnSelectedIndexChanged="cmbTertiaryRootCause_SelectedIndexChanged"
                                            CssClass="Dropdown_Large" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td nowrap="nowrap">
                                        Sub Cause:
                                    </td>
                                    <td nowrap="nowrap">
                                        <asp:DropDownList ID="cmbTertiarySubCause" runat="server" CssClass="Dropdown_Large"
                                            Style="width: 350px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                    <td>
                                        Detail:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTertiaryDetail" runat="server" Style="width: 345px;"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>
                        <asp:Panel ID="pnlCompensation" runat="server">
                            <table>
                                <tr>
                                    <td colspan="4">
                                        <strong>Compensation</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150">
                                        Paid to customer:
                                    </td>
                                    <td width="250">
                                        $&nbsp;<asp:TextBox ID="txtCompensationPaidToCustomer" runat="server" MaxLength="8"
                                            onkeypress="return keyStrokeDecimal(event);" onblur="CheckPositiveDecimal(this,'');"
                                            CssClass="Textbox_Small_Right_Aligned"></asp:TextBox>
                                    </td>
                                    <td width="150">
                                        Credit Note:
                                    </td>
                                    <td width="250">
                                        $&nbsp;<asp:TextBox ID="txtCompensationCreditNote" runat="server" MaxLength="8" onkeypress="return keyStrokeDecimal(event);"
                                            onblur="CheckPositiveDecimal(this,'');" CssClass="Textbox_Small_Right_Aligned"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        FOC (Number of Days):
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtCompensationFOCDays" runat="server" MaxLength="3"
                                            onkeypress="return keyStrokeInt(event);" onblur="CheckContents(this,'');" CssClass="Textbox_Small_Right_Aligned"></asp:TextBox>
                                    </td>
                                    <td>
                                        Other:
                                    </td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtCompensationOther" runat="server" MaxLength="64"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
        <tr>
            <td align="right">
                <br />
                <asp:UpdatePanel ID="updButtons" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                            OnClick="btnSave_Click" />&nbsp;
                        <asp:Button ID="btnSaveAndNew" runat="server" Text="Save/New" CssClass="Button_Standard Button_New"
                            OnClick="btnSaveAndNew_Click" />&nbsp;
                        <asp:Button ID="btnReset" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />&nbsp;
                        <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
    <%--    </ContentTemplate>
    </asp:UpdatePanel>--%>
    <uc1:ConfimationBoxControl ID="cbcConfirmation" runat="server" LeftButton_Text="Save"
        RightButton_Text="Cancel" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
        Text="This Complaint can only be edited by a Supervisor once it has been saved.<br/>Do you want to save the record?"
        Title="Confirmation Required" />
</asp:Content>
