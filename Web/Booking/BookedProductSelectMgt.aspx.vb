Imports System.Xml
Imports Aurora.Common
Imports System.data
Imports System.Drawing

<AuroraPageTitleAttribute("Application : Booking: ")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookedProductSelectMgt)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingSearch)> _
Partial Class Booking_BookedProductSelectMgt
    Inherits AuroraPage

#Region " Constant"
    Private Const BOOKEDPRODUCTDETAILS = "BookedProductDetails.asp?funCode=BPDDETAILLIS"
    Private Const SESSION_BPDDATA As String = "BPDDATA"
    Const SESSION_saveForDetail As String = "saveForDetail"
#End Region

#Region " Variables"
    Private XMLDSO As XmlDocument
    Private XMLDSOavail As XmlDocument
    Private XMLDSOsel As XmlDocument

    Private XMLCLASS As XmlDocument
    Private XMLTYPE As XmlDocument
    Private XMLPOPUPDATA As XmlDocument


    Property rentalID() As String
        Get
            Return ViewState("BPSM_RntId")
        End Get
        Set(ByVal value As String)
            ViewState("BPSM_RntId") = value
        End Set
    End Property
    'Private rentalID As String

    Property bookingID() As String
        Get
            Return ViewState("BPSM_BooId")
        End Get
        Set(ByVal value As String)
            ViewState("BPSM_BooId") = value
        End Set
    End Property
    'Private bookingID As String

    Property User_Code() As String
        Get
            Return ViewState("BPSM_User_Code")
        End Get
        Set(ByVal value As String)
            ViewState("BPSM_User_Code") = value
        End Set
    End Property
    'Private User_Code As String

    Dim sXmlBlankAvail As String = "<Avail><Prd><Id/><Desc/><Chk/></Prd></Avail>"
    Dim sXmlBlankSel As String = "<Sel><Prd><Id/><Desc/><Chk/><Rem/></Prd></Sel>"

#End Region

#Region " Enum"
    Enum xmlloading
        toXMLDSO = 1
        toXMLCLASS = 2
        toXMLTYPE = 3
        toXMLPOPUPDATA = 4
        toALL = 5
        toXMLDSOavail = 6
        toXMLDSOsel = 7
    End Enum

    Enum Submit
        Add = 0
        Remove = 1
        Save = 2
    End Enum
#End Region

#Region " Procedures"

    Sub LoadXMLonFirstLoad()
        'User_Code = "ma2"  ''mybase.usercode   
        'rentalID = "3294379-1" ''Request.QueryString("hdRentalId")
        'bookingID = "3294379" ''Request.QueryString("hdBookingId")

        User_Code = MyBase.UserCode
        rentalID = Request.QueryString("hdRentalId")
        bookingID = Request.QueryString("hdBookingId")

        XMLDSO = New XmlDocument
        XMLDSO = Me.Load_xmldso(rentalID)

        XMLCLASS = New XmlDocument
        XMLCLASS = Me.Load_xmlclass

        XMLTYPE = New XmlDocument
        XMLTYPE = Me.Load_xmltype

        If Not String.IsNullOrEmpty(rentalID) AndAlso Not String.IsNullOrEmpty(bookingID) Then
            XMLPOPUPDATA = New XmlDocument
            XMLPOPUPDATA = Me.Load_xmlpopupdata(bookingID, rentalID)
        End If

        ParseXMLtoLiteral(xmlloading.toALL)

        DisplayHeaderBooking()

    End Sub

    Sub ParseXMLtoLiteral(ByVal xmlType As xmlloading)
        Try

            Select Case xmlType
                Case xmlloading.toALL
                    DecodeXMLtoLiteral(Me.LitXMLCLASS, Me.XMLCLASS)
                    DecodeXMLtoLiteral(Me.LitXMLTYPE, Me.XMLTYPE)
                    DecodeXMLtoLiteral(Me.LitXMLPOPUPDATA, Me.XMLPOPUPDATA)
                    DecodeXMLtoLiteral(Me.litXMLDSO, Me.XMLDSO)
                    ''DecodeXMLtoLiteral(Me.litXMLDSOavail, Me.XMLDSOavail)
                    ''DecodeXMLtoLiteral(Me.litXMLDSOsel, Me.XMLDSOsel)
                Case xmlloading.toXMLCLASS
                    DecodeXMLtoLiteral(Me.LitXMLCLASS, Me.XMLCLASS)
                Case xmlloading.toXMLDSO
                    DecodeXMLtoLiteral(Me.litXMLDSO, Me.XMLDSO)
                Case xmlloading.toXMLPOPUPDATA
                    DecodeXMLtoLiteral(Me.LitXMLPOPUPDATA, Me.XMLPOPUPDATA)
                Case xmlloading.toXMLTYPE
                    DecodeXMLtoLiteral(Me.LitXMLTYPE, Me.XMLTYPE)
                Case xmlloading.toXMLDSOavail
                    DecodeXMLtoLiteral(Me.litXMLDSOavail, Me.XMLDSOavail)
                Case xmlloading.toXMLDSOsel
                    DecodeXMLtoLiteral(Me.litXMLDSOsel, Me.XMLDSOsel)
            End Select

        Catch ex As Exception
            Me.SetErrorMessage("ParseXMLtoLiteral > " & ex.Message)
        End Try

    End Sub

    Sub ParseLiteralToXML(ByVal xmlType As xmlloading)
        Try
            Select Case xmlType
                Case xmlloading.toALL
                    EncodeLiteralToXML(Me.LitXMLCLASS, Me.XMLCLASS)
                    EncodeLiteralToXML(Me.LitXMLTYPE, Me.XMLTYPE)
                    EncodeLiteralToXML(Me.LitXMLPOPUPDATA, Me.XMLPOPUPDATA)
                    EncodeLiteralToXML(Me.litXMLDSO, Me.XMLDSO)
                    EncodeLiteralToXML(Me.litXMLDSOavail, Me.XMLDSOavail)
                    EncodeLiteralToXML(Me.litXMLDSOsel, Me.XMLDSOsel)
                Case xmlloading.toXMLCLASS
                    EncodeLiteralToXML(Me.LitXMLCLASS, Me.XMLCLASS)
                Case xmlloading.toXMLDSO
                    EncodeLiteralToXML(Me.litXMLDSO, Me.XMLDSO)
                Case xmlloading.toXMLPOPUPDATA
                    EncodeLiteralToXML(Me.LitXMLPOPUPDATA, Me.XMLPOPUPDATA)
                Case xmlloading.toXMLTYPE
                    EncodeLiteralToXML(Me.LitXMLTYPE, Me.XMLTYPE)
                Case xmlloading.toXMLDSOavail
                    EncodeLiteralToXML(Me.litXMLDSOavail, Me.XMLDSOavail)
                Case xmlloading.toXMLDSOsel
                    EncodeLiteralToXML(Me.litXMLDSOsel, Me.XMLDSOsel)
            End Select

        Catch ex As Exception
            Me.SetErrorMessage("ParseLiteralToXML > " & ex.Message)
        End Try

    End Sub

    Sub EncodeLiteralToXML(ByVal litObject As Literal, ByRef xmldoc As XmlDocument)
        Try
            If xmldoc Is Nothing Then
                xmldoc = New XmlDocument
            End If

            Dim strTemp As String = CType(litObject, Literal).Text
            If strTemp = "" Then Exit Sub
            xmldoc.LoadXml(strTemp)

        Catch ex As Exception
            Me.SetErrorMessage("EncodeLiteralToXML > " & ex.Message)
        End Try

    End Sub

    Sub DecodeXMLtoLiteral(ByVal litObject As Literal, ByRef xmldoc As XmlDocument)
        Try
            CType(litObject, Literal).Text = Server.HtmlDecode(xmldoc.OuterXml)
        Catch ex As Exception
            Me.SetErrorMessage("DecodeXMLtoLiteral > " & ex.Message)
        End Try
    End Sub

    Sub DisplayXMLPopupData()

        Try
            'lblAgent.Text = Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/AgnName").InnerText
            'Lblhirer.Text = Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/Hirer").InnerText
            'LblStatus.Text = Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BookingSt").InnerText

            agentTextBox.Text = Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/AgnName").InnerText
            hirerTextBox.Text = Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/Hirer").InnerText
            statusTextBox.Text = Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BookingSt").InnerText

            Dim ds As New DataSet
            Dim xmlTemp As String = XMLPOPUPDATA.OuterXml.Replace("<data><Rental>", "<data><Rentals>").Replace("</Rental></data>", "</Rentals></data>")

            ds.ReadXml(New XmlTextReader(xmlTemp, System.Xml.XmlNodeType.Document, Nothing))
            'Me.repDisplayXMLPopupData.DataSource = ds.Tables("Rentals")
            'Me.repDisplayXMLPopupData.DataBind()

            Me.rentalGridView.DataSource = ds.Tables("Rentals")
            Me.rentalGridView.DataBind()

            SetGridViewStatusColor()


        Catch ex As Exception
            Me.SetErrorMessage("DisplayXMLPopupData > " & ex.Message)
        End Try

    End Sub

    Sub DisplayXMLClass()

        Try
            Dim ds As New DataSet
            Dim xmlTemp As String = XMLCLASS.OuterXml
            ds.ReadXml(New XmlTextReader(xmlTemp, System.Xml.XmlNodeType.Document, Nothing))

            Me.ddlClass.DataSource = ds.Tables("COMBO")
            Me.ddlClass.DataTextField = "DESCRIPTION"
            Me.ddlClass.DataValueField = "ID"
            Me.ddlClass.DataBind()

            BlankValueInDropdown(ddlClass)
        Catch ex As Exception
            Me.SetErrorMessage("DisplayXMLClass > " & ex.Message)
        End Try
    End Sub

    Sub DisplayXMLType(Optional ByVal ClassSelectedValue As String = "")

        Try
            If ClassSelectedValue = "" Then Exit Sub

            Dim xmlTemp As String
            If String.IsNullOrEmpty(XMLTYPE.OuterXml) = True AndAlso (String.IsNullOrEmpty(Me.LitXMLTYPE.Text) = True) Then
                Exit Sub
            Else

                If String.IsNullOrEmpty(XMLTYPE.OuterXml) = False Then
                    xmlTemp = XMLTYPE.OuterXml

                Else
                    Me.ParseLiteralToXML(xmlloading.toXMLTYPE)
                    xmlTemp = XMLTYPE.OuterXml
                End If
            End If

            Dim ds As New DataSet
            ds.ReadXml(New XmlTextReader(xmlTemp, System.Xml.XmlNodeType.Document, Nothing))


            Dim tempClassvalue As String = ClassSelectedValue.Trim
            Dim dv As DataView = ds.Tables("COMBO").DefaultView
            dv.RowFilter = "PRNTID='" & tempClassvalue & "'"

            ''ddlType.DataSource = ds.Tables("COMBO").Select("PRNTID='" & tempClassvalue & "'")
            ddlType.DataSource = dv
            ddlType.DataTextField = "DESCRIPTION"
            ddlType.DataValueField = "ID"
            ddlType.DataBind()

            BlankValueInDropdown(ddlType)
        Catch ex As Exception
            Me.SetErrorMessage("DisplayXMLType > " & ex.Message)
        End Try
    End Sub

    Sub DisplayHeaderBooking()
        'If Not Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BooNum") Is Nothing Then
        '    If Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BooNum").InnerText <> "" Then
        '        MyBase.SetValueLink((": Booking :" & Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BooNum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BooNum").InnerText)
        '    Else
        '        MyBase.SetValueLink("", "")
        '    End If
        'End If

        MyBase.SetValueLink((Me.XMLPOPUPDATA.SelectSingleNode("data/Rental/BooNum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=9&funcode=RS-BOKSUMMGT&hdBookingId=" & bookingID & "&hdRentalId=" & rentalID)

    End Sub

    Sub BlankValueInDropdown(ByVal ddlControl As DropDownList)
        Dim item As ListItem = Nothing
        item = New ListItem(" ", 0)
        ddlControl.Items.Insert(0, item)
        ddlControl.SelectedIndex = -1
    End Sub

    Sub LoadScreen()
        DisplayXMLPopupData()

        DisplayXMLClass()
        DisplayXMLType()

        Load_xmldsoAvail()
        Load_xmldsoSel()

        LoadProductParameters(rentalID, "p")

        Me.ParseXMLtoLiteral(xmlloading.toXMLDSOavail)
        Me.ParseXMLtoLiteral(xmlloading.toXMLDSOsel)

    End Sub

    Sub Load_xmldsoAvail()
        Try
            XMLDSOavail = New XmlDocument
            XMLDSOavail.LoadXml(String.Concat("", XMLDSO.SelectSingleNode("data/Avail").OuterXml, ""))
            Me.ParseXMLtoLiteral(xmlloading.toXMLDSOavail)
        Catch ex As Exception
            Me.SetErrorMessage("Load_xmldsoAvail > " & ex.Message)
        End Try
    End Sub

    Sub Load_xmldsoSel()
        Try
            XMLDSOsel = New XmlDocument
            ''Me.ParseXMLtoLiteral(xmlloading.toXMLDSOsel)
            If Me.litXMLDSOsel.Text = "" Then
                XMLDSOsel.LoadXml(String.Concat("", XMLDSO.SelectSingleNode("data/Sel").OuterXml, ""))
            Else
                Me.ParseLiteralToXML(xmlloading.toXMLDSOsel)
            End If


        Catch ex As Exception
            Me.SetErrorMessage("Load_xmldsoSel > " & ex.Message)
        End Try
    End Sub

    Sub LoadProductParameters(ByVal param1 As String, ByVal param3 As String)
        ''exec GEN_GetPopUpData @case = 'SELBOOPRD', @param1 = '3294379-1', @param2 = '', @param3 = 'b', @param4 = '', @param5 = '', @sUsrCode = 'ma2'
        Me.PickerControlProduct.Param1 = param1
        Me.PickerControlProduct.Param3 = param3

    End Sub

    Sub LoadAvailability()
        Try
            Dim ds As New DataSet
            ds.ReadXml(New XmlTextReader(Me.XMLDSOavail.OuterXml, System.Xml.XmlNodeType.Document, Nothing))

            If ds.Tables("Prd") Is Nothing Then
                Me.tblAvailabilityNoRecord.Visible = True
                Me.repAvailability.Visible = False
            Else
                Me.tblAvailabilityNoRecord.Visible = False
                Me.repAvailability.DataSource = ds.Tables("Prd")
                Me.repAvailability.DataBind()
                Me.repAvailability.Visible = True
                Me.ParseXMLtoLiteral(xmlloading.toXMLDSOavail)
            End If

        Catch ex As Exception
            Me.SetErrorMessage("LoadAvailability > " & ex.Message)
        End Try
    End Sub

    Sub LoadSelected()
        Try

            Dim ds As New DataSet
            ds.ReadXml(New XmlTextReader(Me.XMLDSOsel.OuterXml, System.Xml.XmlNodeType.Document, Nothing))
            If ds.Tables("Prd").Rows.Count = 0 Then
                Me.tblSelectedNoRecord.Visible = True
                Me.repSelected.Visible = False
            Else
                Me.tblSelectedNoRecord.Visible = False
                Me.repSelected.DataSource = ds.Tables("Prd")
                Me.repSelected.DataBind()
                Me.repSelected.Visible = True
                Me.ParseXMLtoLiteral(xmlloading.toXMLDSOsel)
            End If


        Catch ex As Exception
            Me.SetErrorMessage("LoadSelected > " & ex.Message)
        End Try
    End Sub

    Sub CheckDuplicate()

        Try
            Dim exist As Boolean = False
            Dim stravail As String = ""

            Dim i As Integer = XMLDSOavail.DocumentElement.ChildNodes.Count - 1
            Dim ii As Integer = XMLDSOsel.DocumentElement.ChildNodes.Count - 1

            For ictr As Integer = 0 To i
                exist = False
                For iictr As Integer = 0 To ii
                    Dim selectedProduct As String = Me.XMLDSOsel.SelectSingleNode("//Sel").ChildNodes(ii).SelectSingleNode("PrdId").InnerText
                    Dim availProduct As String = XMLDSOavail.SelectSingleNode("//Avail").ChildNodes(i).SelectSingleNode("PrdId").InnerText
                    If selectedProduct.Trim.Equals(availProduct.Trim) Then
                        exist = True
                        Exit For
                    End If
                Next

                If Not exist Then
                    stravail = String.Concat(stravail, XMLDSOavail.SelectSingleNode("//Avail").ChildNodes(i).OuterXml)
                End If
            Next

            Me.XMLDSOavail.LoadXml("<Avail>" & stravail & "</Avail>")
            BuildTable()
        Catch ex As Exception
            Me.SetErrorMessage("CheckDuplicate > " & ex.Message)
        End Try
    End Sub

    Sub BuildTable()
        LoadAvailability()
        LoadSelected()
    End Sub

    Sub CheckAvailableProduct(ByVal blnCheck As Boolean)
        Try
            Dim repItem As RepeaterItem
            For Each repItem In Me.repAvailability.Items
                Dim chkAvailability As CheckBox = CType(repItem.FindControl("chkAvailability"), CheckBox)
                chkAvailability.Checked = blnCheck
                hidFlag.Value = IIf(blnCheck = True, "YES", "NO")
            Next
        Catch ex As Exception
            Me.SetErrorMessage("CheckAvailableProduct > " & ex.Message)
        End Try
    End Sub

    Sub CheckSelectedProduct(ByVal blnCheck As Boolean)
        Try
            Dim repItem As RepeaterItem
            For Each repItem In Me.repSelected.Items
                Dim chkselected As CheckBox = CType(repItem.FindControl("chkselected"), CheckBox)
                If chkselected IsNot Nothing Then
                    If chkselected.Enabled = True Then
                        chkselected.Checked = blnCheck
                        hidFlag.Value = IIf(blnCheck = True, "YES", "NO")
                    End If
                End If
            Next

        Catch ex As Exception
            Me.SetErrorMessage("CheckAvailableProduct > " & ex.Message)
        End Try
    End Sub

    Sub DefaultValue()
        Me.PickerControlProduct.Text = ""
        Me.rdoPackage.SelectedIndex = 0
        Me.ddlClass.SelectedIndex = -1
        Me.ddlType.SelectedIndex = -1
    End Sub

    Sub SubmitData(ByVal enumSubmit As Submit)
        Select Case enumSubmit
            Case Submit.Add
                Add()
            Case Submit.Remove
                Remove()
            Case Submit.Save
                Save()
        End Select
    End Sub

    Sub Add()

        Dim strXml As String = ""
        Dim strXMLAvailNew As String = ""
        Dim strXmlSelNew As String = ""
        Dim i As Integer = 0

        Try

            hidFlag.Value = "NO"
            Me.ParseLiteralToXML(xmlloading.toXMLDSOavail)
            Me.ParseLiteralToXML(xmlloading.toXMLDSOsel)

            Dim repItem As RepeaterItem
            For Each repItem In Me.repAvailability.Items
                Dim chkAvailability As CheckBox = CType(repItem.FindControl("chkAvailability"), CheckBox)
                If chkAvailability IsNot Nothing Then
                    If chkAvailability.Checked = True Then
                        strXml = String.Concat(strXml, XMLDSOavail.DocumentElement.ChildNodes(i).OuterXml)
                    Else
                        strXMLAvailNew = String.Concat(strXMLAvailNew, XMLDSOavail.DocumentElement.ChildNodes(i).OuterXml)
                    End If
                End If
                i = i + 1
            Next

            If String.IsNullOrEmpty(strXml) Then
                Me.SetInformationMessage("Please select the Available Product(s) to Add")
                Exit Sub
            End If

            For ii As Integer = 0 To XMLDSOsel.DocumentElement.ChildNodes.Count - 1
                strXmlSelNew = String.Concat(strXmlSelNew, XMLDSOsel.DocumentElement.ChildNodes(ii).OuterXml)
            Next

            strXmlSelNew = String.Concat(strXmlSelNew, strXml)

            XMLDSOavail.LoadXml(String.Concat("<Avail>", strXMLAvailNew, "</Avail>"))
            XMLDSOsel.LoadXml(String.Concat("<Sel>", strXmlSelNew, "</Sel>"))

            Me.BuildTable()
            DefaultValue()


        Catch ex As Exception
            Me.SetErrorMessage("Add > " & ex.Message)
        End Try

    End Sub

    Sub Remove()

        Dim strXml As String = ""
        Dim strXmlSelNew As String = ""
        Dim i As Integer = 0
        Dim strXMLAvailNew As String = ""

        Try


            hidFlag.Value = "NO"
            Me.ParseLiteralToXML(xmlloading.toXMLDSOavail)
            Me.ParseLiteralToXML(xmlloading.toXMLDSOsel)


            Dim repItem As RepeaterItem
            For Each repItem In Me.repSelected.Items
                Dim chkselected As CheckBox = CType(repItem.FindControl("chkselected"), CheckBox)
                If chkselected IsNot Nothing Then
                    If chkselected.Checked = True AndAlso chkselected.Enabled = True Then
                        strXml = String.Concat(strXml, XMLDSOsel.DocumentElement.ChildNodes(i).OuterXml)
                    Else
                        strXmlSelNew = String.Concat(strXmlSelNew, XMLDSOsel.DocumentElement.ChildNodes(i).OuterXml)
                    End If
                End If
                i = i + 1
            Next

            If String.IsNullOrEmpty(strXml) Then
                Me.SetInformationMessage("Please select the Selected Product(s) to Remove")
                Exit Sub
            End If

            strXMLAvailNew = String.Concat(strXMLAvailNew, strXml)

            XMLDSOavail.LoadXml(String.Concat("<Avail>", strXMLAvailNew, "</Avail>"))
            XMLDSOsel.LoadXml(String.Concat("<Sel>", strXmlSelNew, "</Sel>"))

            Me.BuildTable()

        Catch ex As Exception
            Me.SetErrorMessage("Remove > " & ex.Message)
        End Try

    End Sub

    Sub Save()

        Dim strXml As String = ""
        Dim shortName As String() = Nothing
        Dim strmanageBookedPrdList As String = ""

        Try

            Dim isCheckBeforeSaving As Boolean = CheckBeforeSaving()
            If Not isCheckBeforeSaving Then
                Me.SetInformationMessage("You have not completed Adding or Removing items from the above lists")
                Exit Sub
            End If

            Me.ParseLiteralToXML(xmlloading.toXMLDSOavail)
            Me.ParseLiteralToXML(xmlloading.toXMLDSOsel)

            Dim i As Integer = Me.XMLDSOavail.DocumentElement.ChildNodes.Count - 1
            Dim ii As Integer = Me.XMLDSOsel.DocumentElement.ChildNodes.Count - 1

            For ictr As Integer = 0 To ii
                Dim remValue As String = XMLDSOsel.DocumentElement.ChildNodes(ictr).SelectSingleNode("Rem").InnerText
                If remValue = "1" Then
                    shortName = XMLDSOsel.DocumentElement.ChildNodes(ictr).SelectSingleNode("Desc").InnerText.Split("-")
                    Dim productid As String = XMLDSOsel.DocumentElement.ChildNodes(ictr).SelectSingleNode("PrdId").InnerText
                    strXml = String.Concat(strXml, "<Rental><BpdId></BpdId><PrdShortName>", shortName(0).Trim, "</PrdShortName><PrdId>", productid, "</PrdId></Rental>")
                End If
            Next

            Dim newXMLDoc As XmlDocument = Me.GetBookedProduct(bookingID, rentalID, "", False)
            Dim newXmlStr As String = ""

            i = (newXMLDoc.DocumentElement.ChildNodes.Count - 3)
            For ictr As Integer = 0 To i
                newXmlStr = String.Concat(newXmlStr, newXMLDoc.DocumentElement.ChildNodes(ictr).OuterXml)
            Next

            newXmlStr = String.Concat("<Data>", newXmlStr, strXml, "</Data>")
            Dim tempXML As New XmlDocument
            tempXML.LoadXml(newXmlStr)

            Dim blnCheckRentalIntegrity As Boolean

            If Not tempXML.SelectSingleNode("//Data/Rental/RntId") Is Nothing Then
                If Not tempXML.SelectSingleNode("//Data/Rental/IntNo") Is Nothing Then
                    If Not tempXML.SelectSingleNode("//Data/Rental/BooIntNo") Is Nothing Then

                        Dim sRntId As String = tempXML.SelectSingleNode("//Data/Rental/RntId").InnerText
                        Dim nIntNo As Integer = CInt(tempXML.SelectSingleNode("//Data/Rental/IntNo").InnerText)
                        Dim nBooIntNo As Integer = CInt(tempXML.SelectSingleNode("//Data/Rental/BooIntNo").InnerText)

                        blnCheckRentalIntegrity = CheckRentalIntegrity(sRntId, "", "", nIntNo, nBooIntNo)

                    End If
                End If
            End If



            If blnCheckRentalIntegrity Then
                strmanageBookedPrdList = Aurora.ChkInOutRntCan.Services.ManageBPDList.manageBookedPrdList(rentalID, newXmlStr, True, User_Code)

                tempXML.LoadXml(strmanageBookedPrdList)
                If tempXML.DocumentElement.ChildNodes(0).Name = "Error" Then
                    Me.SetErrorShortMessage(tempXML.SelectSingleNode("//Data/Error/ErrDescription").InnerText)
                    tempXML = Nothing
                    Exit Sub
                End If

                If tempXML.SelectSingleNode("//Data/Errors").ChildNodes.Count >= 1 Then
                    Me.SetErrorShortMessage(tempXML.SelectSingleNode("//Data/Error/error").InnerText)
                    tempXML = Nothing
                    Exit Sub
                End If

            Else
                Me.SetInformationMessage("This Rental has been modified by another user. Please refresh the screen.")
                tempXML = Nothing
                Exit Sub
            End If
            tempXML = Nothing


            Dim param1 As String = rentalID
            Dim param4 As String = bookingID
            Dim param2 As String = 1
            Dim param3 As String = strmanageBookedPrdList
            Session(SESSION_saveForDetail) = String.Concat(param1, "|", param2, "|", param3, "|", Me.bookingID, "|", param4)
            Response.Redirect(BOOKEDPRODUCTDETAILS)


        Catch ex As Exception
            Me.SetErrorMessage("Save > " & ex.Message)
        End Try


    End Sub


    Private Sub SetGridViewStatusColor()
        'Add color for status
        For Each r As GridViewRow In rentalGridView.Rows
            r.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(r.Cells(7).Text)))
        Next


    End Sub
#End Region

#Region " Function"

    Function CheckRentalIntegrity(ByVal sRentalId, ByVal sBPDId, ByVal sBooID, ByVal nScreenRentalIntegrityNo, ByVal nScreenBookingIntegrityNo) As Boolean


        Dim bDataIsCurrent As Boolean = False
        Dim oXml As XmlDocument
        Dim nDataBaseRentalIntegrityNo As Integer = 0

        Dim nDataBaseBooIntNo As Integer

        Try


            oXml = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_GetRentalIntegrity", sRentalId, sBPDId, sBooID) ''objGenericFind.getInformation(sCommandString)

            If Not oXml.SelectSingleNode("//Data/Rental/IntegrityNo") Is Nothing Then
                nDataBaseRentalIntegrityNo = CInt(oXml.SelectSingleNode("//Data/Rental/IntegrityNo").InnerText)
            End If

            If nDataBaseRentalIntegrityNo = nScreenRentalIntegrityNo Then


                ' if passed first check
                ' look at the booking integrity
                If Not oXml.SelectSingleNode("//Data/Rental/Booking/BooIntNo") Is Nothing Then
                    nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("//Data/Rental/Booking/BooIntNo").InnerText)
                Else
                    'another place the booking number can be inside is
                    If Not oXml.SelectSingleNode("//Data/Booking/BooIntNo") Is Nothing Then
                        nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("//Data/Booking/BooIntNo").InnerText)
                    End If
                End If


                If nDataBaseBooIntNo = nScreenBookingIntegrityNo Then
                    bDataIsCurrent = True
                Else
                    bDataIsCurrent = False
                End If


            Else
                'failed very first check!
                bDataIsCurrent = False
            End If

            Return bDataIsCurrent


        Catch ex As Exception
            Me.SetErrorMessage("CheckRentalIntegrity > " & ex.Message)
        Finally
            oXml = Nothing
        End Try

    End Function

    Function CheckBeforeSaving() As Boolean
        Dim isReady As Boolean = True

        Dim repItem As RepeaterItem
        For Each repItem In Me.repSelected.Items
            Dim chkselected As CheckBox = CType(repItem.FindControl("chkselected"), CheckBox)
            If chkselected IsNot Nothing Then
                If chkselected.Enabled = True Then
                    If chkselected.Checked Then
                        Return False
                    End If
                End If
            End If
        Next


        For Each repItem In Me.repAvailability.Items
            Dim chkAvailability As CheckBox = CType(repItem.FindControl("chkAvailability"), CheckBox)
            If chkAvailability IsNot Nothing Then
                If (chkAvailability.Checked) AndAlso (Me.repAvailability.Visible = True) Then
                    Return False
                End If
            End If
        Next

        Return isReady
    End Function

    Function IsAvailableProductReady() As Boolean
        Try
            Dim isCheck As Boolean = False
            Dim repItem As RepeaterItem
            For Each repItem In Me.repAvailability.Items
                Dim chkAvailability As CheckBox = CType(repItem.FindControl("chkAvailability"), CheckBox)
                isCheck = chkAvailability.Checked
                If isCheck Then
                    Return True
                End If
            Next
            Return isCheck
        Catch ex As Exception
            Me.SetErrorMessage("IsAvailableProductReady > " & ex.Message)
        End Try
    End Function

    Function IsSelectedProductReadyForRemove() As Boolean
        Try
            Dim isCheck As Boolean = False
            Dim repItem As RepeaterItem
            For Each repItem In Me.repSelected.Items
                Dim chkselected As CheckBox = CType(repItem.FindControl("chkselected"), CheckBox)
                isCheck = chkselected.Checked
                If isCheck AndAlso chkselected.Enabled = True Then
                    Return True
                End If
            Next
            Return isCheck
        Catch ex As Exception
            Me.SetErrorMessage("IsSelectedProductReadyForRemove > " & ex.Message)
        End Try
    End Function

    Private Function Load_xmldso(ByVal RntID As String) As XmlDocument
        Try
            Return Data.ExecuteSqlXmlSPDoc("RES_GetSelecteBookedProducts", RntID, "", "Package", "", "", 0, User_Code)
        Catch ex As Exception
            Me.SetErrorMessage("Load_xmldso > " & ex.Message)
        End Try
    End Function

    Private Function Load_xmldso(ByVal RntID As String, _
                                 ByVal sShortName As String, _
                                 ByVal sPkgBase As String, _
                                 ByVal sClaId As String, _
                                 ByVal sTypId As String, _
                                 ByVal showAvail As Boolean, _
                                 ByVal sUsrCode As String) As XmlDocument
        Try
            Return Data.ExecuteSqlXmlSPDoc("RES_GetSelecteBookedProducts", RntID, sShortName, sPkgBase, sClaId, sTypId, showAvail, sUsrCode)
        Catch ex As Exception
            Me.SetErrorMessage("Load_xmldso > " & ex.Message)
        End Try
    End Function

    Private Function Load_xmlclass() As XmlDocument
        Try
            Return Data.ExecuteSqlXmlSPDoc("GEN_getComboData", "CLASS", "", "", User_Code)
        Catch ex As Exception
            Me.SetErrorMessage("Load_xmlclass > " & ex.Message)
        End Try
    End Function

    Private Function Load_xmltype() As XmlDocument
        Try
            Return Data.ExecuteSqlXmlSPDoc("GEN_getComboData", "TYPE", "", "", User_Code)
        Catch ex As Exception
            Me.SetErrorMessage("Load_xmltype > " & ex.Message)
        End Try
    End Function

    Private Function Load_xmlpopupdata(ByVal bookingid As String, ByVal rentalid As String) As XmlDocument
        Try
            Return Data.ExecuteSqlXmlSPDoc("cus_GetBookingDetail", bookingid, "BookRntRec", rentalid, "", User_Code)
        Catch ex As Exception
            Me.SetErrorMessage("Load_xmlpopupdate > " & ex.Message)
        End Try
    End Function

    ''sfunctionName = "RES_getBookedProductList '" & param1 & "','" & param2 & "','" & param4 & "','" & param3 & "','" & userCode & "'"
    ''@sBooId		VARCHAR	 (64)	=	'' ,
    ''@sRntId		VARCHAR	 (64)	=	'' ,
    ''@BpdIdsList	VARCHAR	 (8000)	=	'',
    ''@bAll			BIT			=	1 ,
    ''@UserCode	VARCHAR	(64)	 	=	''
    Private Function GetBookedProduct(ByVal sBooId As String, ByVal sRental As String, ByVal BpdIdsList As String, ByVal bAll As Boolean) As XmlDocument
        Try
            Return Data.ExecuteSqlXmlSPDoc("RES_getBookedProductList", sBooId, sRental, BpdIdsList, bAll, User_Code)
        Catch ex As Exception
            Me.SetErrorMessage("GetBookedProduct > " & ex.Message)
        End Try
    End Function


#End Region

#Region " Page Init"

    'JL 2008/06/13
    'Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    'btnBack.Attributes.Add("onclick", "window.location='Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & bookingID & "&hdRentalId=" & rentalID & "&activeTab=9';return false;")
    'End Sub

#End Region

#Region " Page Load"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        btnSaveNext.Attributes.Add("onclick", "return displayCompletion();")

        If Not Page.IsPostBack Then
            LoadXMLonFirstLoad()
            LoadScreen()
            BuildTable()
        Else
            Me.ParseLiteralToXML(xmlloading.toALL)
            'JL 2008/06/03
            'Need to reset the params for postback
            LoadProductParameters(rentalID, IIf(rdoPackage.SelectedIndex = 0, "p", "b"))
        End If

        'JL 2008/06/03
        'Set btnBack attributes after bookingid and rentalid are declared, rather than on page_int()
        btnBack.Attributes.Add("onclick", "window.location='Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & bookingID & "&hdRentalId=" & rentalID & "&activeTab=9';return false;")



    End Sub


#End Region

#Region " Control Events"

    Protected Sub ddlClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClass.SelectedIndexChanged
        DisplayXMLType(ddlClass.SelectedItem.Value.ToString)
    End Sub

    Protected Sub rdoPackage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdoPackage.SelectedIndexChanged
        LoadProductParameters(rentalID, IIf(rdoPackage.SelectedIndex = 0, "p", "b"))
    End Sub

    Protected Sub btnShowAvailable_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowAvailable.Click
        Try

            Dim RntID As String = rentalID
            Dim sShortName As String = IIf(Me.PickerControlProduct.Text = "", "", Me.PickerControlProduct.Text)
            If sShortName.Contains("-") Then
                sShortName = Me.PickerControlProduct.DataId ''sShortName.Substring(0, sShortName.IndexOf("-")).Trim
            End If

            Dim sPkgBase As String = IIf(Me.rdoPackage.SelectedIndex = 0, "Package", "Base")

            Dim sClaId As String = ""
            If Not String.IsNullOrEmpty(Me.ddlClass.SelectedItem.Text.Trim) Then
                sClaId = ddlClass.SelectedItem.Value
            End If

            Dim sTypId As String = ""
            If Not String.IsNullOrEmpty(Me.ddlType.Text.Trim) Then
                sTypId = Me.ddlType.SelectedItem.Value
            End If

            Dim showAvail As Boolean = True
            Dim sUsrCode As String = User_Code
            Me.XMLDSO = Me.Load_xmldso(RntID, sShortName, sPkgBase, sClaId, sTypId, showAvail, sUsrCode)
            If Me.XMLDSO Is Nothing Then
                MyBase.SetErrorShortMessage("Failed to get product!")
                Exit Sub
            End If
            ParseXMLtoLiteral(xmlloading.toXMLDSO)
            Load_xmldsoAvail()
            Load_xmldsoSel()

            If XMLDSOavail.OuterXml = "" Then
                MyBase.SetErrorShortMessage("No Record found!")
                Exit Sub
            End If

            If XMLDSOavail.DocumentElement.ChildNodes.Count = 0 Then
                MyBase.SetErrorMessage("Invalid Product")
            End If

            CheckDuplicate()

        Catch ex As Exception
            Me.SetErrorMessage("ShowAvailable > " & ex.Message)
        End Try


    End Sub

    Protected Sub chkAllAvailability_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        CheckAvailableProduct(CType(sender, CheckBox).Checked)
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnIsAvailableProductReady As Boolean = IsAvailableProductReady()
        If Not blnIsAvailableProductReady Then
            Me.SetInformationShortMessage("Please select the Available Product(s) to Add")
            Exit Sub
        End If
        SubmitData(Submit.Add)
    End Sub

    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As EventArgs)
        Dim blnIsSelectedProductReadyForRemove As Boolean = Me.IsSelectedProductReadyForRemove
        If Not blnIsSelectedProductReadyForRemove Then
            Me.SetInformationShortMessage("Please select the Selected Product(s) to Remove")
            Exit Sub
        End If
        SubmitData(Submit.Remove)

    End Sub

    Protected Sub chkAllselected_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        CheckSelectedProduct(CType(sender, CheckBox).Checked)
    End Sub

    Protected Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
        SubmitData(Submit.Save)
    End Sub

    Protected Sub chkselected_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        hidFlag.Value = IIf(CType(sender, CheckBox).Checked, "YES", "NO")
    End Sub

    Protected Sub chkAvailability_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        hidFlag.Value = IIf(CType(sender, CheckBox).Checked, "YES", "NO")
    End Sub
#End Region

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return ""
        End If
    End Function


End Class
