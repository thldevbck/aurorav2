'' Change Log
'' 6.6.8 - Shoel - Added error code GEN143 to the list for Extensions
'' 2.7.8 - Shoel - Added a Query String param : msgToShow for the purpose of displaying messages on programmatic tab changes
'' 5.8.8 - Shoel - Added OnBubble handler - this refreshes the grid with rental data
'' 22.8.8 - Shoel - Fix for squish # 582, additional optional param added to method dataBindBookingDetail that indicates if the rental grids should rebind
'' 28.8.8 - Shoel - Modified for THRIFTY HISTORY
'' 22.9.8 - Shoel - Added a Query String param : errToShow for the purpose of displaying errors on programmatic tab changes
'' 10.10.8- Shoel - Hotfix for thrifty tab JS issues
'' 5.11.8 - Shoel - Added catches for Thread.Abort Exceptions - that ought to bring down the log size a bit!
'' RKS : 26-Jun-2012
'' Sorry Manny i have to change your code to enable billing token button
'' Decision is to always enable this button

''july 20 2010 - mia - addition of capture billing token
''sept 1 2014 -  mia - Merge Files Tab created by EB6. Files needed  are
''                    app_code/BookingUserControl.vb
''                    booking/controls/BookingFilesUserControl.ascx/vb
''                    booking/UploadFile.aspx/vb
''                    booking/booking.aspx/vb
''                    Aurora.Booking.Service and Aurora.Booking.Data
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports System.Drawing
Imports Aurora.Reports.Services

<AuroraPageTitleAttribute("Booking : ")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN132,GEN133,GEN134,GEN135,GEN136,GEN137,GEN138,GEN139,GEN140,GEN143,GEN156,GEN157,GEN155,GEN005,GEN046,GEN049")> _
Partial Class Booking_Booking
    Inherits AuroraPage

    Private textSearch As String = ""
    'Private bookingId As String = ""
    'Private rentalId As String = ""
    'Private bookingNumber As String = ""

    Private bFromConfirmationView As Boolean = False

    Private Const Booking_BookingId_ViewState As String = "Booking_BookingId"
    Private Const Booking_BookingNumber_ViewState As String = "Booking_BookingNumber"
    Private Const Booking_RentalId_ViewState As String = "Booking_RentalId"
    Private Const Booking_RentalNo_ViewState As String = "Booking_RentalNo"
    Private Const Booking_IsFromSearch_ViewState As String = "Booking_IsFromSearch"

    Private Property bookingId() As String
        Get
            Return ViewState(Booking_BookingId_ViewState)
        End Get
        Set(ByVal value As String)
            ViewState(Booking_BookingId_ViewState) = value
        End Set
    End Property

    Private Property bookingNumber() As String
        Get
            Return ViewState(Booking_BookingNumber_ViewState)
        End Get
        Set(ByVal value As String)
            ViewState(Booking_BookingNumber_ViewState) = value
        End Set
    End Property

    Public ReadOnly Property RentalIdMerchantReference As String
        Get
            Return rentalId
        End Get
    End Property
    Public ReadOnly Property RentalIDBillingToken As String
        Get
            Return rentalId
        End Get
    End Property

    Private Property rentalId() As String
        Get
            Return ViewState(Booking_RentalId_ViewState)
        End Get
        Set(ByVal value As String)
            ViewState(Booking_RentalId_ViewState) = value
        End Set
    End Property

    Private Property rentalNo() As String
        Get
            Return ViewState(Booking_RentalNo_ViewState)
        End Get
        Set(ByVal value As String)
            ViewState(Booking_RentalNo_ViewState) = value
        End Set
    End Property

    Private Property isFromSearch() As String
        Get
            Return ViewState(Booking_IsFromSearch_ViewState)
        End Get
        Set(ByVal value As String)
            ViewState(Booking_IsFromSearch_ViewState) = value
        End Set
    End Property

    Property IsThriftyBooking() As Boolean
        Get
            If ViewState("Booking_IsThriftyBooking") IsNot Nothing Then
                Return CBool(ViewState("Booking_IsThriftyBooking"))
            Else
                Return False
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("Booking_IsThriftyBooking") = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3285499"
            'rentalId = "3285499-1"
            'bookingId = "6279034"
            'rentalId = "6279034-1"

            If Not Request.QueryString("isFromSearch") Is Nothing Then
                isFromSearchTextBox.Text = Request.QueryString("isFromSearch").ToString()
                isFromSearch = isFromSearchTextBox.Text
            Else
                isFromSearchTextBox.Text = "0"
                isFromSearch = "0"
            End If

            If Not Page.IsPostBack Then
                Session("VhrID") = Nothing
                
                bookingId = ""
                rentalId = ""
                rentalNo = ""
                'If Not Request.QueryString("sTxtSearch") Is Nothing Then
                '    textSearch = Request.QueryString("sTxtSearch").ToString()
                'End If

                If Not String.IsNullOrEmpty(Me.SearchParam) Then
                    bookingNumber = Me.SearchParam
                    'ElseIf Not Request.QueryString("sTxtSearch") Is Nothing Then
                    '    bookingNumber = Me.Request.QueryString("sTxtSearch").ToString()
                Else
                    If Not Request.QueryString("hdBookingId") Is Nothing Then
                        bookingId = Request.QueryString("hdBookingId").ToString()
                        ''rev:mia Jan 6 2014 - what lies ahead?
                        bookingNumber = bookingId
                    End If
                    If Not Request.QueryString("hdRentalId") Is Nothing Then
                        rentalId = Request.QueryString("hdRentalId").ToString()
                        ''rev:mia Jan 6 2014 - what lies ahead?
                        If (rentalId.Contains("-")) Then
                            bookingNumber = rentalId.Replace("-", "/")
                        End If
                    End If
                End If

                If Not Request.QueryString("activeTab") Is Nothing Then
                    Dim activeTabString As String
                    activeTabString = Request.QueryString("activeTab").ToString()
                    Tabs.ActiveTabIndex = Utility.ParseInt(activeTabString)
                    'setActiveTab(activeTabString)
                End If

                ' added by Shoel - i need this to show messages from db on page switch
                If Not Request.QueryString("msgToShow") Is Nothing Then
                    SetInformationMessage(CStr(Request.QueryString("msgToShow")))
                End If
                ' added by Shoel - i need this to show messages from db on page switch
                If Not Request.QueryString("errToShow") Is Nothing Then
                    SetErrorMessage(CStr(Request.QueryString("errToShow")))
                End If
                dataBindBookingDetail()

                ' added by Shoel - this hides the Thrifty History for non-thrifty rentals + arranges the tabs
                IsThriftyBooking = Aurora.Booking.Services.BookingThriftyHistory.IsRentalThrifty(rentalId)
                If Not IsThriftyBooking Then
                    Tabs.FindControl("thriftyhistoryTabPanel").Visible = False
                    For Each oControl As System.Web.UI.Control In Tabs.FindControl("thriftyhistoryTabPanel").Controls
                        oControl.Visible = False
                    Next

                End If
                ' added by Shoel - this hides the Thrifty History for non-thrifty rentals + arranges the tabs

                ''rev:mia Oct.24 2012 - Addition of ThirdParty Reference Report"
                GetThirdPartyReport()


            End If
            If (String.IsNullOrEmpty(HiddenFieldPriceMatchParameter.Value)) Then
                HiddenFieldPriceMatchParameter.Value = PriceMatchParameter
                PriceMatchParameter = HiddenFieldPriceMatchParameter.Value
            End If

            If Not (String.IsNullOrEmpty(bookingId) Or String.IsNullOrEmpty(rentalId)) Then

                Me.SetValueLink(bookingNumber, "~\Booking\BookingEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&hdBookingNum=" & bookingNumber & "&isFromSearch=" & isFromSearch)

                configTabControls()
                If Not Page.IsPostBack Then
                    loadTabControls(True)
                End If
                ' for complaints
                CheckComplaints()

            Else
                Dim activeTab As String = Request.QueryString("activeTab")
                If Request.QueryString("funcode") = "RS-BOKSUMMGT" Then
                    If String.IsNullOrEmpty(activeTab) = False Then
                        Tabs.ActiveTabIndex = Utility.ParseInt(activeTab)
                        If Not Page.IsPostBack Then
                            loadTabControls(True)
                        End If
                    End If
                Else
                    Response.Redirect("./BookingSearch.aspx")
                End If
            End If

            ''rev:mia JUly 20 added billing token
            ''capture billing token is available for KK booking
            '' RKS : 26-Jun-2012
            '' Sorry Manny i have to change your code
            '' Decision is to always enable this button
            ''Me.captureBillingToken.Enabled = IIf((RentalStatusIsKK = True) _
            ''                                    And EFTButtonForCSR() = True, _
            ''                                    True, False)

            'Following Line of Code was Changes by Nimesh on 31st Mar 2015 (Desk-2430 Jira)
            'Me.captureBillingToken.Enabled = IIf((RentalStatusIsKK = True), _
            '                                     True, False)
            Me.captureBillingToken.Enabled = IIf((RentalStatusIsKK = True Or RentalStatusIsCO = True Or RentalStatusIsXX = True), _
                                                 True, False)
            'End Changed by nimesh

            ''reV:mia June 30 2011 - CI will be allowed to use
            ''billing token if its a member of CHGTOKEN
            ''tomorrow is Princess D's bday
            '' RKS : 26-Jun-2012
            '' Sorry Manny i have to change your code
            '' Decision is to always enable this button
            If RentalStatusIsCI = True Then
                Me.captureBillingToken.Enabled = IIf((RentalStatusIsCI = True), _
                                                 True, False)
            End If

            bookingNotesUserControl.ShowEuropcarLogLink = ViewState("ShowEuropcarLogLink")

            ''rev:mia June.03 2013 - Addition of Price Match.
            PopulatePriceMatchingControl()

        Catch exA As System.Threading.ThreadAbortException
            ' do nothing - needed to stop log file getting bigger than godzilla!
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub


    Private Sub configTabControls()

        Select Case Tabs.ActiveTabIndex
            Case BookingTabIndex.Files
                bookingFilesUserControl.BookingId = bookingId
                bookingFilesUserControl.BookingNumber = bookingNumber
                bookingFilesUserControl.RentalId = rentalId
                bookingFilesUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Notes
                bookingNotesUserControl.BookingId = bookingId
                bookingNotesUserControl.BookingNumber = bookingNumber
                bookingNotesUserControl.RentalId = rentalId
                bookingNotesUserControl.RentalNo = rentalNo
                bookingNotesUserControl.IsFromSearch = isFromSearch
                bookingNotesUserControl.popupUserControlLoad()
            Case BookingTabIndex.History
                bookingHistoryUserControl.BookingId = bookingId
                bookingHistoryUserControl.BookingNumber = bookingNumber
                bookingHistoryUserControl.RentalId = rentalId
                bookingHistoryUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Summary
                bookingSummaryUserControl.BookingId = bookingId
                bookingSummaryUserControl.BookingNumber = bookingNumber
                bookingSummaryUserControl.RentalId = rentalId
                bookingSummaryUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Extension
                'extensions part
                bookingExtensionUserControl.BookingId = bookingId
                bookingExtensionUserControl.BookingNumber = bookingNumber
                bookingExtensionUserControl.RentalId = rentalId
                bookingExtensionUserControl.RentalNo = rentalNo
                'extensions part
            Case BookingTabIndex.Customer
                bookingCustomerUserControl.BookingId = bookingId
                bookingCustomerUserControl.BookingNumber = bookingNumber
                bookingCustomerUserControl.RentalId = rentalId
                bookingCustomerUserControl.RentalNo = rentalNo
                bookingCustomerUserControl.IsFromSearch = isFromSearch
            Case BookingTabIndex.ChangeOver
                'Change over
                bookingExchangeUserControl.BookingId = bookingId
                bookingExchangeUserControl.BookingNumber = bookingNumber
                bookingExchangeUserControl.RentalId = rentalId
                bookingExchangeUserControl.RentalNo = rentalNo
            Case BookingTabIndex.CheckOutIn
                'checkin checkout
                bookingCheckInCheckOutUserControl.BookingId = bookingId
                bookingCheckInCheckOutUserControl.BookingNumber = bookingNumber
                bookingCheckInCheckOutUserControl.RentalId = rentalId
                bookingCheckInCheckOutUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Infringements
                'Infringement
                bookingInfringementUserControl.BookingId = bookingId
                bookingInfringementUserControl.BookingNumber = bookingNumber
                bookingInfringementUserControl.RentalId = rentalId
                bookingInfringementUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Confirmation
                'confirmation
                bookingConfirmationUserControl.BookingId = bookingId
                bookingConfirmationUserControl.BookingNumber = bookingNumber
                bookingConfirmationUserControl.RentalId = rentalId
                bookingConfirmationUserControl.RentalNo = rentalNo
                bookingConfirmationUserControl.IsFromSearch = isFromSearch
            Case BookingTabIndex.Complaints
                'Match product
                bookingComplaintsUserControl.BookingId = bookingId
                bookingComplaintsUserControl.BookingNumber = bookingNumber
                bookingComplaintsUserControl.RentalId = rentalId
                bookingComplaintsUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Products
                'Products
                BookingProductUserControl1.BookingId = bookingId
                BookingProductUserControl1.BookingNumber = bookingNumber
                BookingProductUserControl1.RentalId = rentalId
            Case BookingTabIndex.Payments
                'payment
                BookingPaymentUserControl1.BookingId = bookingId
                BookingPaymentUserControl1.BookingNumber = bookingNumber
            Case BookingTabIndex.Incidents
                'Incident
                bookingIncidentsUserControl.BookingId = bookingId
                bookingIncidentsUserControl.BookingNumber = bookingNumber
                bookingIncidentsUserControl.RentalId = rentalId
                bookingIncidentsUserControl.RentalNo = rentalNo
                bookingIncidentsUserControl.IsFromSearch = isFromSearch
            Case BookingTabIndex.Cancel
                'Cancel
                bookingCancellationUserControl.BookingId = bookingId
                bookingCancellationUserControl.BookingNumber = bookingNumber
                bookingCancellationUserControl.RentalId = rentalId
                bookingCancellationUserControl.RentalNo = rentalNo
            Case BookingTabIndex.Vehicle
                'vehicle
                bookingVehicleUserControl.BookingId = bookingId
                bookingVehicleUserControl.BookingNumber = bookingNumber
                bookingVehicleUserControl.RentalId = rentalId
                bookingVehicleUserControl.RentalNo = rentalNo
            Case BookingTabIndex.ThriftyHistory
                ' Thrifty history
                bookingThriftyHistoryUserControl.BookingId = bookingId
                bookingThriftyHistoryUserControl.BookingNumber = bookingNumber
                bookingThriftyHistoryUserControl.RentalId = rentalId
                bookingThriftyHistoryUserControl.RentalNo = rentalNo

                ''rev:mia 24Sept2015 - Experience Tab
            Case BookingTabIndex.BookingExperience
                BookingExperienceUserControl.BookingId = bookingId
                BookingExperienceUserControl.BookingNumber = bookingNumber
                BookingExperienceUserControl.RentalId = rentalId
                BookingExperienceUserControl.RentalNo = rentalNo
        End Select
    End Sub


    Private Sub dataBindBookingDetail(Optional ByVal ReloadGridForRentalUpdate As Boolean = False)

        Dim sbAddRentalParameters As New StringBuilder()
        ' 1 rentalGridView

        ''postback happened in productusercontrol
        If (Tabs.ActiveTabIndex = 9) AndAlso (bookingId = "" Or rentalId = "") Then
            bookingId = Me.BookingProductUserControl1.HiddenBookingID
            rentalId = Me.BookingProductUserControl1.HiddenRentalID
        End If

        Dim dsRental As DataSet
        If Not String.IsNullOrEmpty(bookingNumber) And Not ReloadGridForRentalUpdate Then
            dsRental = Booking.GetBookingDetail("", "BookRec", "", bookingNumber, UserCode)
        Else
            dsRental = Booking.GetBookingDetail(bookingId, "BookRntRec", rentalId, "", UserCode)
        End If


        If dsRental.Tables.Count = 1 Then
            If dsRental.Tables(0).Rows.Count > 0 Then

                ' 1 rentalGridView
                Dim dr As DataRow
                dr = dsRental.Tables(0).Rows(0)

                ''rev:mia jan6,2009
                If dr.Item("Check-out").ToString.TrimEnd.Split(" ").Length = 3 Then
                    bookingConfirmationUserControl.ActiveRentalCountry = dr.Item("Check-out").ToString.Split(" ")(2).TrimEnd
                Else
                    bookingConfirmationUserControl.ActiveRentalCountry = ""
                End If
                ' ''rev:mia jan19,2009
                'bookingCustomerUserControl.BypassAgeCheckingforBP2BB = dr.Item("Vehicle").ToString.ToUpper.TrimEnd

                bookingId = dr.Item("BooId")
                bookingNumber = dr.Item("BooNum")
                rentalId = dr.Item("RntId")
                rentalNo = dr.Item("RentalNo")

                'agentLabel.Text = dr.Item("AgnName")
                'hirerLabel.Text = dr.Item("Hirer")
                'statusLabel.Text = dr.Item("Status")
                agentTextBox.Text = dr.Item("AgnName")
                hirerTextBox.Text = dr.Item("Hirer")
                ''rev:mia sept 30
                ''dr.Item("Status")
                statusTextBox.Text = dr.Item("BookingSt")
                statusTextBox.BackColor = DataConstants.GetBookingStatusColor(dr.Item("Status").ToString())
                RentalStatusIsKK = IIf(dr.Item("Status").ToString() = "KK", True, False)
                'Following two lines were added by nimesh on 31st mar 15 (Desk-2430-Jira)
                RentalStatusIsCO = IIf(dr.Item("Status").ToString() = "CO", True, False)
                RentalStatusIsXX = IIf(dr.Item("Status").ToString() = "XX", True, False)

                ''rev:mia June 30 2011
                RentalStatusIsCI = IIf(dr.Item("Status").ToString() = "CI", True, False)

                Dim dv As DataView
                dv = New DataView(dsRental.Tables(0))
                dv.RowFilter = "RntId = '" & rentalId & "'"
                rentalGridView.DataSource = dv
                rentalGridView.DataBind()

                ''rev:mia June.03 2013 - Addition of Price Match.
                If (dr.Item("Status").ToString() = "CO" Or dr.Item("Status").ToString() = "CI") Then
                    HidePriceMatchButton = True
                Else
                    HidePriceMatchButton = False
                End If
                BookingPriceMatchUserControl.PMBookingId = bookingId

                HiddenFieldPriceMatchParameter.Value = String.Concat(bookingId, "|", rentalId, "|", UserCode, "|", dr.Item("HirePd"))
                PriceMatchParameter = HiddenFieldPriceMatchParameter.Value
                ''-----------------------------------------------------------------------------------
                ''rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
                ''-----------------------------------------------------------------------------------
                Try
                    Dim tempText As String = dsRental.Tables(0).Rows(0)("Check-Out").ToString
                    sbAddRentalParameters.Append("&outD=" & tempText.Split(" ")(0)) ''date
                    sbAddRentalParameters.Append("&outT=" & tempText.Split(" ")(1)) ''time
                    sbAddRentalParameters.Append("&outL=" & tempText.Split(" ")(2)) ''location


                    tempText = dsRental.Tables(0).Rows(0)("Check-In").ToString
                    sbAddRentalParameters.Append("&inD=" & tempText.Split(" ")(0))
                    sbAddRentalParameters.Append("&inT=" & tempText.Split(" ")(1))
                    sbAddRentalParameters.Append("&inL=" & tempText.Split(" ")(2)) ''location

                    sbAddRentalParameters.Append("&ln=" & dsRental.Tables(0).Rows(0)("Lname").ToString)
                    sbAddRentalParameters.Append("&fn=" & dsRental.Tables(0).Rows(0)("Fname").ToString)
                    sbAddRentalParameters.Append("&tit=" & dsRental.Tables(0).Rows(0)("Title").ToString)

                    sbAddRentalParameters.Append("&ad=" & dsRental.Tables(0).Rows(0)("Adult").ToString)
                    sbAddRentalParameters.Append("&ch=" & dsRental.Tables(0).Rows(0)("Child").ToString)
                    sbAddRentalParameters.Append("&in=" & dsRental.Tables(0).Rows(0)("Infant").ToString)
                    sbAddRentalParameters.Append("&ea=" & dsRental.Tables(0).Rows(0)("EmailAddress").ToString)
                    sbAddRentalParameters.Append("&veh=" & dsRental.Tables(0).Rows(0)("vehicle").ToString)
                    sbAddRentalParameters.Append("&ar=" & dsRental.Tables(0).Rows(0)("AgentRef").ToString)
                    If Not Request.QueryString("hdRentalId") Is Nothing Then
                        If (Request.QueryString("hdRentalId").ToString() = "") Then
                            sbAddRentalParameters.Append("&hdRentalId=" & rentalId)
                        Else
                            sbAddRentalParameters.Append("&hdRentalId=" & Request.QueryString("hdRentalId").ToString())
                        End If

                    End If
                    AddRentalParameters = sbAddRentalParameters.ToString
                Catch ex As Exception
                    AddRentalParameters = "<empty>"
                End Try
                

                ' 2 otherRentalLabel
                Dim warningMessage As String = ""
                warningMessage = Booking.ValidateBooking(bookingId, rentalId, UserCode)

                If Not String.IsNullOrEmpty(warningMessage) Then
                    otherRentalLabel.Text = "<b>Error have been found on this Booking </b> <br/>"
                    otherRentalLabel.Text = otherRentalLabel.Text & warningMessage & "<br/>"
                    otherRentalLabel.Text = otherRentalLabel.Text & "<b>Notes:</b> Errors must be corrected before Check-Out or Check-In processing can occur."

                    ''rev:mia oct31
                    lblError.Text = warningMessage
                    lblErrorNote.Text = "<b>Notes:</b> Errors must be corrected before Check-Out or Check-In processing can occur."
                End If

                ' 3 otherRentalGridView
                Dim dsBooking As DataSet
                dsBooking = Booking.GetBookingDetail(bookingId, "RentalRecord", "", "", UserCode)

                Dim dvBooking As DataView

                If dsBooking.Tables.Count > 0 Then
                    dvBooking = New DataView(dsBooking.Tables(0))
                    dvBooking.RowFilter = "RntId <> '' and RntId <> '" & rentalId & "'"

                    otherRentalGridView.DataSource = dvBooking
                    otherRentalGridView.DataBind()

                    If dvBooking.Count > 0 Then
                        detailsCollapsiblePanel.Visible = True
                        detailsContentPanel.Visible = True
                    Else
                        detailsCollapsiblePanel.Visible = False
                        detailsContentPanel.Visible = False
                    End If
                Else
                    detailsCollapsiblePanel.Visible = False
                    detailsContentPanel.Visible = False
                End If

                'Only displaying the Europcar Log link in the notes tab if the rental is for a car (not a van)
                ViewState("ShowEuropcarLogLink") = _
                    CBool(dr.Item("IsCar"))

                SetGridViewStatusColor()

                ''rev:mia March 13 2013 - added for Quick Availability in Booking
                Dim oQuickAvailParameter As New Aurora.Booking.Services.QuickAvailParameter
                Dim sCheckoutLoc As String = dr.Item("Check-Out").Split(" "c)(2).Trim
                Dim sCheckInLoc As String = dr.Item("Check-In").Split(" "c)(2).Trim

                Dim sCheckoutDate As String = dr.Item("Check-Out").ToString.Replace(sCheckoutLoc, "").TrimEnd
                Dim sCheckInDate As String = dr.Item("Check-In").ToString.Replace(sCheckInLoc, "").TrimEnd
                Dim svehicle As String = dr.Item("Vehicle").ToString
                Dim sHirePeriod As String = dr.Item("HirePd")
                Dim sAgent As String = dr.Item("AgnName").ToString.Split("-")(0).TrimEnd
                With oQuickAvailParameter
                    .branchOut = sCheckoutLoc
                    .branchIn = sCheckInLoc
                    .dteCKO = sCheckoutDate
                    .dteCKI = sCheckInDate
                    .hirePeriod = sHirePeriod
                    .product = svehicle
                    .Booid = rentalId
                    .userCode = MyBase.UserCode
                    .programName = "Booking.aspx"
                    .agentId = sAgent
                End With
                QuickAvailParameterData = oQuickAvailParameter
                QuickAvailButton.Enabled = IIf(dr.Item("Status").ToString() = "QN", True, False)
                QuickAvailButton.Visible = IIf(dr.Item("Status").ToString() = "QN", True, False)
                VehicleCode = svehicle

                If (Date.Compare(Convert.ToDateTime(sCheckInDate).ToShortDateString, Now.ToShortDateString) < 0) Then
                    QuickAvailButton.Enabled = False
                    QuickAvailButton.Visible = False
                ElseIf (Date.Compare(Convert.ToDateTime(sCheckoutDate).ToShortDateString, Now.ToShortDateString) < 0) Then
                    QuickAvailButton.Enabled = False
                    QuickAvailButton.Visible = False
                Else
                    QuickAvailButton.Enabled = IIf(dr.Item("Status").ToString() = "QN", True, False)
                    QuickAvailButton.Visible = IIf(dr.Item("Status").ToString() = "QN", True, False)

                End If
            End If 'dsRental.Tables(0).Rows.Count > 0

        End If 'dsRental.Tables.Count = 1 

        ''rev:mia oct.31
        tblmessages.Visible = Not String.IsNullOrEmpty(otherRentalLabel.Text)

    End Sub

    Protected Function GetRentalUrl(ByVal rentalId As Object) As String
        Dim rentalIdString As String
        rentalIdString = Convert.ToString(rentalId)

        Return "./RentalEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalIdString & "&hdBookingNum=" & bookingNumber & "&isFromSearch=" & isFromSearch
    End Function

    Private hasTabChanged As Boolean = False

    Protected Sub Tabs_ActiveTabChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Tabs.ActiveTabChanged
        Try
            ' added by Shoel - this arranges the tabs for Thrifty History
            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(Page), "BlockThriftyHistory", "try{ArrangeTabs('" & Tabs.ClientID & "');}catch(err){}", True)
            ' added by Shoel - this arranges the tabs for Thrifty History
            If Not hasTabChanged Then

                configTabControls()
                loadTabControls(True)

                hasTabChanged = True
            End If
        Catch ex As System.Threading.ThreadAbortException
            ' do nothing - needed to stop log file getting bigger than godzilla!
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the tab.")
        End Try
    End Sub

    Private Sub loadTabControls(ByVal isFirstLoad As Boolean)
        bookingNotesUserControl.Visible = False
        bookingHistoryUserControl.Visible = False
        bookingSummaryUserControl.Visible = False
        bookingExtensionUserControl.Visible = False
        bookingCustomerUserControl.Visible = False
        bookingExchangeUserControl.Visible = False
        bookingCheckInCheckOutUserControl.Visible = False
        bookingIncidentsUserControl.Visible = False
        bookingInfringementUserControl.Visible = False
        bookingConfirmationUserControl.Visible = False
        bookingComplaintsUserControl.Visible = False
        BookingProductUserControl1.Visible = False
        BookingPaymentUserControl1.Visible = False
        bookingCancellationUserControl.Visible = False
        bookingVehicleUserControl.Visible = False
        bookingFilesUserControl.Visible = False

        bookingNotesDiv.Visible = False
        bookingHistoryDiv.Visible = False
        bookingSummaryDiv.Visible = False
        bookingExtensionDiv.Visible = False
        bookingCustomerDiv.Visible = False
        bookingExchangeDiv.Visible = False
        bookingCheckInCheckOutDiv.Visible = False
        bookingIncidentsDiv.Visible = False
        bookingInfringementDiv.Visible = False
        bookingConfirmationDiv.Visible = False
        bookingComplaintsDiv.Visible = False
        bookingProductDiv.Visible = False
        BookingPaymentDiv.Visible = False
        bookingCancellationDiv.Visible = False
        bookingVehicleDiv.Visible = False
        bookingFilesDiv.Visible = False

        ' added for thrifty history
        bookingThriftyHistoryDiv.Visible = False
        ' added for thrifty history

        ''rev:mia 24Sept2015 - Experience Tab
        BookingExperienceUserControl.Visible = False

        Select Case Tabs.ActiveTabIndex
            Case BookingTabIndex.Files
                bookingFilesDiv.Visible = True
                bookingFilesUserControl.Visible = True
                bookingFilesUserControl.isFirstLoad = isFirstLoad
                bookingFilesUserControl.LoadPage()
            Case BookingTabIndex.Notes
                bookingNotesUserControl.Visible = True
                bookingNotesDiv.Visible = True
                bookingNotesUserControl.isFirstLoad = isFirstLoad
                bookingNotesUserControl.LoadPage()
            Case BookingTabIndex.History
                bookingHistoryDiv.Visible = True
                bookingHistoryUserControl.Visible = True
                bookingHistoryUserControl.isFirstLoad = isFirstLoad
                bookingHistoryUserControl.LoadPage()
            Case BookingTabIndex.Summary
                bookingSummaryDiv.Visible = True
                bookingSummaryUserControl.Visible = True
                bookingSummaryUserControl.isFirstLoad = isFirstLoad
                bookingSummaryUserControl.LoadPage()
            Case BookingTabIndex.Extension
                bookingExtensionDiv.Visible = True
                bookingExtensionUserControl.Visible = True
                bookingExtensionUserControl.isFirstLoad = isFirstLoad
                bookingExtensionUserControl.LoadPage()
            Case BookingTabIndex.Customer
                bookingCustomerDiv.Visible = True
                bookingCustomerUserControl.Visible = True
                bookingCustomerUserControl.isFirstLoad = isFirstLoad
                bookingCustomerUserControl.LoadPage()
            Case BookingTabIndex.ChangeOver
                bookingExchangeDiv.Visible = True
                bookingExchangeUserControl.Visible = True
                bookingExchangeUserControl.isFirstLoad = isFirstLoad
                bookingExchangeUserControl.LoadPage()
            Case BookingTabIndex.CheckOutIn
                bookingCheckInCheckOutDiv.Visible = True
                bookingCheckInCheckOutUserControl.Visible = True
                bookingCheckInCheckOutUserControl.isFirstLoad = isFirstLoad
                bookingCheckInCheckOutUserControl.LoadPage()
            Case BookingTabIndex.Incidents
                bookingIncidentsDiv.Visible = True
                bookingIncidentsUserControl.Visible = True
                bookingIncidentsUserControl.isFirstLoad = isFirstLoad
                bookingIncidentsUserControl.LoadPage()
            Case BookingTabIndex.Infringements
                bookingInfringementDiv.Visible = True
                bookingInfringementUserControl.Visible = True
                bookingInfringementUserControl.isFirstLoad = isFirstLoad
                bookingInfringementUserControl.LoadPage()
            Case BookingTabIndex.Confirmation
                bookingConfirmationDiv.Visible = True
                bookingConfirmationUserControl.Visible = True
                bookingConfirmationUserControl.isFirstLoad = isFirstLoad
                bookingConfirmationUserControl.LoadPage()
            Case BookingTabIndex.Complaints
                bookingComplaintsDiv.Visible = True
                bookingComplaintsUserControl.Visible = True
                bookingComplaintsUserControl.isFirstLoad = isFirstLoad
                bookingComplaintsUserControl.LoadPage()
            Case BookingTabIndex.Products
                bookingProductDiv.Visible = True
                BookingProductUserControl1.Visible = True
                BookingProductUserControl1.isFirstLoad = isFirstLoad
                BookingProductUserControl1.LoadPage()
            Case BookingTabIndex.Payments
                BookingPaymentDiv.Visible = True
                BookingPaymentUserControl1.Visible = True
                BookingPaymentUserControl1.isFirstLoad = isFirstLoad
                BookingPaymentUserControl1.PaymentRentalID = rentalId
                BookingPaymentUserControl1.LoadPage()
            Case BookingTabIndex.Cancel
                bookingCancellationDiv.Visible = True
                bookingCancellationUserControl.Visible = True
                bookingCancellationUserControl.isFirstLoad = isFirstLoad
                bookingCancellationUserControl.LoadPage()
            Case BookingTabIndex.Vehicle
                bookingVehicleDiv.Visible = True
                bookingVehicleUserControl.Visible = True
                bookingVehicleUserControl.isFirstLoad = isFirstLoad
                bookingVehicleUserControl.LoadPage()
            Case BookingTabIndex.ThriftyHistory
                bookingThriftyHistoryDiv.Visible = True
                bookingThriftyHistoryUserControl.Visible = True
                bookingThriftyHistoryUserControl.isFirstLoad = isFirstLoad
                bookingThriftyHistoryUserControl.LoadPage()

                ''rev:mia 24Sept2015 - Experience Tab
            Case BookingTabIndex.BookingExperience
                BookingExperienceUserControl.Visible = True
                BookingExperienceUserControl.Visible = True
                BookingExperienceUserControl.isFirstLoad = isFirstLoad
                BookingExperienceUserControl.LoadPage()
            Case Else

        End Select
    End Sub

    Protected Sub AddRentalButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addRentalButton.Click
        ''-----------------------------------------------------------------------------------
        ''rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx
        ''-----------------------------------------------------------------------------------
        Dim tempAgent As String = IIf(Not String.IsNullOrEmpty(agentTextBox.Text), "&agentId=" & agentTextBox.Text, "")
        bookingValidation("./BookingProcess.aspx?booId=" & bookingId & tempAgent & AddRentalParameters)
    End Sub

    Protected Sub BookingSearchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles bookingSearchButton.Click
        Response.Redirect("./BookingSearch.aspx")
    End Sub

    Protected Sub addBookingButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles addBookingButton.Click
        bookingValidation("./BookingProcess.aspx")
    End Sub

    Protected Sub otherRentalGridView_RowUpdating(ByVal sender As Object, ByVal e As GridViewUpdateEventArgs) Handles otherRentalGridView.RowUpdating
        Dim otherRentalGridViewRow As GridViewRow
        otherRentalGridViewRow = otherRentalGridView.Rows(e.RowIndex)
        'Dim rentalId As String = otherRentalGridViewRow.Cells(0).Text
        Dim rentalIdLabel As System.Web.UI.WebControls.Label
        rentalIdLabel = otherRentalGridViewRow.FindControl("rentalIdLabel")

        bookingValidation("./Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & bookingId & "&hdRentalId=" & rentalIdLabel.Text & "&activeTab=" & Tabs.ActiveTabIndex.ToString() & "&isFromSearch=" & isFromSearch)

    End Sub

    Private Sub bookingValidation(ByVal url As String)
        'Private Sub bookingValidation(ByVal bookingId, ByVal rentalId, ByVal userCode, ByVal message)
        Dim msg As String = ""
        Dim wrn As String = ""

        'If String.IsNullOrEmpty(message) Then

        Booking.ValidateBooking1(bookingId, rentalId, UserCode, msg, wrn)

        msg = msg.Replace("~", "<br />")

        If Not String.IsNullOrEmpty(msg) Then
            msg = "<b>Errors have been found on this Booking</b><br/>" & msg
        End If

        If Not String.IsNullOrEmpty(wrn) Then
            wrn = "<b>The following warnings have been found for this Booking</b>" & wrn
            If Not String.IsNullOrEmpty(msg) Then
                msg = wrn & "<br/><br/>" & msg
            End If
        End If
        'Else
        'msg = message
        'End If

        If Not String.IsNullOrEmpty(wrn) Then
            'Confirmation Box
            confimationBoxControl.Text = wrn '"This replacement vehicle is now due for a service. Do you wish to override this?"
            confimationBoxControl.LeftButton_AutoPostBack = True
            confimationBoxControl.LeftButton_Text = "Warning"
            confimationBoxControl.RightButton_Visible = False
            confimationBoxControl.Param = "1" & vbCrLf & url & vbCrLf & msg
            confimationBoxControl.Show()

        Else
            step2(url, msg)
        End If
    End Sub

    Private Sub step2(ByVal url As String, ByVal msg As String)
        If Not String.IsNullOrEmpty(msg) Then
            confimationBoxControl.Text = msg & " <br/><br/><B>Note:</B>&nbsp;Errors must be corrected before Check-Out or Check-In^processing can occur."
            confimationBoxControl.LeftButton_AutoPostBack = False
            confimationBoxControl.RightButton_AutoPostBack = True
            confimationBoxControl.RightButton_Visible = True
            confimationBoxControl.LeftButton_Text = "Correct Errors Now"
            confimationBoxControl.RightButton_Text = "Correct Errors Later"
            confimationBoxControl.Param = "2" + vbCrLf + url

            confimationBoxControl.Left_Button.CssClass = "Button_Standard"
            confimationBoxControl.Right_Button.CssClass = "Button_Standard"

            confimationBoxControl.Show()
        Else
            Response.Redirect(url)
        End If

    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        'If Not sLeftButton Then

        Dim stringSeparators() As String = {vbCrLf}

        Dim list As New ArrayList()
        list.AddRange(param.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries))

        Dim stepNo As Integer = CInt(list.Item(0))
        Dim url As String = list.Item(1)

        Select Case stepNo
            Case 1
                Dim msg As String
                msg = list.Item(2)
                step2(url, msg)
            Case 2

                ' ConfirmationBoxUserControl1 = Me.Controls(0).FindControl("ConfimationBoxUserControl1")
                ' ConfirmationBoxUserControl1.RedirectUrl = url
                Response.Redirect(url)

        End Select

        'End If

    End Sub

    Private Sub SetGridViewStatusColor()
        'Add color for status
        For Each r As GridViewRow In rentalGridView.Rows
            r.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(r.Cells(7).Text)))
        Next
        For Each rr As GridViewRow In otherRentalGridView.Rows
            rr.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(rr.Cells(8).Text)))
        Next

    End Sub

    Protected Sub dailyActivityButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dailyActivityButton.Click
        Response.Redirect("~/CustomerService/DailyActivity.aspx")
        'Response.Redirect("~/../ASP/DailyActivityMgt1.asp?funCode=CS-DLYACTMGT")
    End Sub

    Protected Function GetArrivalDepartureTime(ByVal value As Object) As String
        If value Is Nothing _
            OrElse (TypeOf value Is String _
                    AndAlso DirectCast(value, String).Trim().Length = 0 _
                    ) Then

            Return ""
        End If

        Dim dtValue As DateTime = CDate(value)

        Return " [" + dtValue.ToString("HH:mm") + "]"
    End Function

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return ""
        End If
    End Function

    'Protected Sub coReportButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles coReportButton.Click
    '    Response.Redirect("~/Reports/Default.aspx?funCode=RPT_COREPORT")
    'End Sub

    ' this thingie is needed to refresh the Rental Grid and status when Check-out/Check-in completes - Added by Shoel
    Protected Overrides Function OnBubbleEvent(ByVal sender As Object, ByVal e As EventArgs) As Boolean
        If IsThriftyBooking Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(Page), "BlockThriftyHistoryBubble", "try{ArrangeTabs('" & Tabs.ClientID & "');}catch(err){}", True)
        End If
        If sender Is Nothing And CType(e, System.Web.UI.WebControls.CommandEventArgs).CommandName = "Rebind" Then
            dataBindBookingDetail(True)
        End If
        Return True
        'CheckComplaints()
    End Function
    ' this thingie is needed to refresh the Rental Grid and status when Check-out/Check-in completes - Added by Shoel

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If IsThriftyBooking Then
            ScriptManager.RegisterClientScriptBlock(Me.Page, GetType(Page), "BlockThriftyHistoryBubble", "try{ArrangeTabs('" & Tabs.ClientID & "');}catch(err){}", True)
        End If
        ''rev:mia issue # 554 Modify Booking: page - two missing buttons and <Back> button not functioning as expected
        Dim tempURL As String = "\BookingEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&hdBookingNum=" & bookingNumber & "&isFromSearch=" & isFromSearch & "&activeTab=" & Tabs.ActiveTabIndex
        ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID & "1", "ChangeURL('" & tempURL & "');", True)
        'CheckComplaints()

        cbQuickAvailinBooking.Right_Button.Width = Unit.Pixel(70)
    End Sub

    Sub CheckComplaints()
        Dim oResult As Object
        oResult = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.Complaints_CheckOpenComplaints('" & bookingId & "')")
        If oResult IsNot Nothing AndAlso TypeName(oResult).ToUpper().Equals("BOOLEAN") Then
            If CBool(oResult) Then
                SetWarningShortMessage("There is an open Complaint logged against this booking. Refer to Complaints tab.")
            End If
        End If
    End Sub

#Region "Capture Billing Token"
    ''new stored procedures for Capture Billing Token
    ''BillingTokenInsert
    ''BillingTokenSelect
    ''BillingTokenSelectForPayment
    ''BillingTokenSelectPopulateCardInfo
    Function EFTButtonForCSR() As Boolean
        Dim eft As New BookingPaymentEFTPOSMaintenance
        Dim sreturnmsg As String = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForCSR"))
        If (sreturnmsg.Equals("True")) Then
            Return True
        End If
        Return False
    End Function


    Function EFTButtonForCIStatus() As Boolean
        Dim eft As New BookingPaymentEFTPOSMaintenance
        Dim sreturnmsg As String = eft.CheckCSRRole(Me.UserCode, ConfigurationManager.AppSettings("EFTPOSroleForTokenCSR"))
        If (sreturnmsg.Equals("True")) Then
            Return True
        End If
        Return False
    End Function



    Private Property RentalStatusIsKK() As Boolean
        Get
            Return CBool(ViewState("RentalStatusIsKK"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("RentalStatusIsKK") = value
        End Set

    End Property

    ' following two properties RentalStatusIsXX and RentalStatusIsCO was added by nimesh on 31st mar 15 (Desk-2430 Jira)
    Private Property RentalStatusIsXX() As Boolean
        Get
            Return CBool(ViewState("RentalStatusIsXX"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("RentalStatusIsXX") = value
        End Set

    End Property

    Private Property RentalStatusIsCO() As Boolean
        Get
            Return CBool(ViewState("RentalStatusIsCO"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("RentalStatusIsCO") = value
        End Set

    End Property

    Private Property RentalStatusIsCI() As Boolean
        Get
            Return CBool(ViewState("RentalStatusIsCI"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("RentalStatusIsCI") = value
        End Set

    End Property

#End Region

#Region "rev:mia Oct.24 2012 - Addition of ThirdParty Reference Report"

    Private Function GetBrandsforReport() As Boolean
        '' Dim brdcode As String = Aurora.Confirmation.Services.Confirmation.ConfirmationGetBrand(rentalId)
        '' Dim brandname As String = Aurora.Confirmation.Services.Confirmation.ConfirmationGetBrandName(brdcode).ToUpper.Trim

        Return IIf(bookingNumber.Contains("M") = True, True, False)
    End Function

    Sub GetThirdPartyReport()

        If (GetBrandsforReport() = False) Then Exit Sub
        ''http://tdc-sql-006/ReportServer/Pages/ReportViewer.aspx?%2fTHL+Rentals%2fUAT%2fReservations%2fMigrated+Notes&rs:Command=Render
        ''http://tdc-sql-006/Reports/Pages/Report.aspx?ItemPath=%2fAurora+Reports%2fCustomer+Service%2fMigrated+Notes&SelectedSubTabId=ReportDataSourcePropertiesTab&SelectedTabId=ViewTab

        Dim reportpath As String = ConfigurationManager.AppSettings("migratedUrlReport").ToString
        If Request.ServerVariables("HTTP_HOST").ToLower.Contains("localhost") = True Or _
           Request.ServerVariables("HTTP_HOST").ToLower.Contains("pap-app") = True Then
            reportpath = "%2fTHL+Rentals%2fUAT%2fReservations%2fMigrated+Notes"
        End If

        Dim thirdpartyreference As String = Aurora.Booking.Data.DataRepository.GetThirdPartyRef(rentalId, bookingId)

        Dim iframeUrl As String = ReportsConfiguration.Instance.ReportsUrl _
         & "/Pages/ReportViewer.aspx?" & Server.UrlPathEncode(reportpath) _
         & "&BookingRef=" & thirdpartyreference _
         & "&rs:Command=Render&rc:Parameters=true" & "&rc:LinkTarget=" & ThirdPartyReferenceIFrame.ClientID

        ThirdPartyReferenceIFrame.Attributes("src") = iframeUrl
    End Sub
#End Region

#Region "rev:mia March 13 2013 - added for Quick Availability in Booking"

    Private Property VehicleCode As String
        Get
            Return ViewState("VehicleCode")
        End Get
        Set(value As String)
            ViewState("VehicleCode") = value
        End Set
    End Property

    ''today is my my Mom's birthday
    Private Property QuickAvailParameterData As QuickAvailParameter
        Get
            Return DirectCast(ViewState("QuickAvailParameterData"), QuickAvailParameter)
        End Get
        Set(value As QuickAvailParameter)
            ViewState("QuickAvailParameterData") = value
        End Set
    End Property

    Protected Sub QuickAvailButton_Click(sender As Object, e As System.EventArgs)

        Dim objextenion As Aurora.Booking.Services.QuickAvailParameter = Me.QuickAvailParameterData
        Dim oQuickAvail As New Aurora.Booking.Services.BookingQuickAvail(objextenion)
        oQuickAvail.DisplayOtherVehicleOption = False
        Dim errormessage As String = oQuickAvail.ErrorMessages
        Dim warningmessage As String = oQuickAvail.WarningMessage
        Dim isAvailable As Boolean = oQuickAvail.CheckAvailability
        Dim allmessage As String = oQuickAvail.GetAllMessages


        objextenion.product = oQuickAvail.ProductId
        ''objextenion.Newvhrid = oQuickAvail.VhrId
        ''objextenion.bkrid = oQuickAvail.BkrIdCurrent
        objextenion.countrytext = oQuickAvail.CountryToSearch



        HiddenFieldConfirmationBehaviorID.Value = cbQuickAvailinBooking.ConfirmationBehaviorID

        cbQuickAvailinBooking.HideOKButtonAndDisplaySearch(False)
        cbQuickAvailinBooking.Right_Button.Visible = False
        cbQuickAvailinBooking.Right_Button.Visible = False

        If Not isAvailable Then

            Dim sb As New StringBuilder
            sb.AppendFormat("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}|{12}", objextenion.branchIn, _
                                                                             objextenion.branchOut, _
                                                                             objextenion.dteCKO, _
                                                                             objextenion.dteCKI, _
                                                                             objextenion.countrytext, _
                                                                             objextenion.agentId, _
                                                                             objextenion.product, _
                                                                             objextenion.bkrid, _
                                                                             objextenion.hirePeriod, _
                                                                             objextenion.Booid, _
                                                                             objextenion.userCode, _
                                                                             objextenion.Newvhrid, _
                                                                             objextenion.Newbkrid)

            HiddenFieldVehicleExtension.Value = sb.ToString
        End If
        ''assign the new value to the viewstate
        objextenion.product = VehicleCode
        Me.QuickAvailParameterData = objextenion

        cbQuickAvailinBooking.Text = allmessage
        cbQuickAvailinBooking.Show()
    End Sub
#End Region

    
#Region "rev:mia May 28 2013 - addition of querystring parameters starting from Booking.aspx"
    Private Property AddRentalParameters As String
        Get
            If ("<empty>" = ViewState("AddRentalParameters").ToString) Then Return ""

            Return ViewState("AddRentalParameters").ToString
        End Get
        Set(value As String)
            ViewState("AddRentalParameters") = value
        End Set
    End Property

#End Region

#Region "rev:mia June.03 2013 - Addition of Price Match."
    Public property PriceMatchParameter As String
        Get
            Return ViewState("PriceMatchParameter")
        End Get
        Set(value As String)
            ViewState("PriceMatchParameter") = value
        End Set
    End Property
    Public Property HidePriceMatchButton As Boolean
        Get
            Return Convert.ToBoolean(ViewState("HidePriceMatchButton"))
        End Get
        Set(value As Boolean)
            ViewState("HidePriceMatchButton") = value
        End Set
    End Property
    Sub PopulatePriceMatchingControl()

        Dim pm As BookingPriceMatching = Nothing
        pm = New BookingPriceMatching(Me.rentalId)

        Dim PriceMRequestCodTypeId_Value As String = String.Empty
        Dim PriceMCompetitorCodId_Value As String = String.Empty
        Dim PriceMStatusCodId_Value As String = String.Empty
        Try
            If (pm.BookingPriceMatchingDataTable.Rows.Count - 1 > -1) Then
                BookingPriceMatchUserControl.PMPriceMSeqId = Convert.ToInt32(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMSeqId"))
                BookingPriceMatchUserControl.RequestedTextBox.Text = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMRequestor").ToString)
                BookingPriceMatchUserControl.DecisionTextBox.Text = DecodeText(pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMDecisionBy").ToString)
                PriceMRequestCodTypeId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMRequestCodTypeId")
                PriceMCompetitorCodId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMCompetitorCodId")
                PriceMStatusCodId_Value = pm.BookingPriceMatchingDataTable.Rows(0).Item("PriceMStatusCodId")
            End If

        Catch ex As Exception
            BookingPriceMatchUserControl.PMPriceMSeqId = -1
        End Try

        Try
            BookingPriceMatchUserControl.PMRentalId = pm.BookingInformationDataTable.Rows(0).Item("BookingRef").ToString
            BookingPriceMatchUserControl.PMCheckout = pm.BookingInformationDataTable.Rows(0).Item("Check-Out").ToString
            BookingPriceMatchUserControl.PMCheckin = pm.BookingInformationDataTable.Rows(0).Item("Check-in").ToString
            BookingPriceMatchUserControl.PMHirePeriod = pm.BookingInformationDataTable.Rows(0).Item("HirePd").ToString
            BookingPriceMatchUserControl.PMPackage = pm.BookingInformationDataTable.Rows(0).Item("Package").ToString
            BookingPriceMatchUserControl.PMBrand = pm.BookingInformationDataTable.Rows(0).Item("Brand").ToString
            BookingPriceMatchUserControl.PMVehicleName = pm.BookingInformationDataTable.Rows(0).Item("VehicleName").ToString
            BookingPriceMatchUserControl.PMVehicle = pm.BookingInformationDataTable.Rows(0).Item("Vehicle").ToString.ToUpper
            BookingPriceMatchUserControl.PMStatus = pm.BookingInformationDataTable.Rows(0).Item("BookingStatus").ToString


            If (Not Page.IsPostBack) Then

                BookingPriceMatchUserControl.PMrequestTypeDropDown.Items.Clear()
                BookingPriceMatchUserControl.PMrequestTypeDropDown.Items.Add(New ListItem("--please select--", "-1"))
                BookingPriceMatchUserControl.PMrequestTypeDropDown.AppendDataBoundItems = True
                BookingPriceMatchUserControl.PMrequestTypeDropDown.DataSource = pm.PriceMatchingCodeDataTable.DefaultView
                BookingPriceMatchUserControl.PMrequestTypeDropDown.DataTextField = "CodDesc"
                BookingPriceMatchUserControl.PMrequestTypeDropDown.DataValueField = "CodId"
                BookingPriceMatchUserControl.PMrequestTypeDropDown.DataBind()
                BookingPriceMatchUserControl.PMrequestTypeDropDown.SelectedValue = PriceMRequestCodTypeId_Value


                BookingPriceMatchUserControl.PMCompetitorDropDown.Items.Clear()
                BookingPriceMatchUserControl.PMCompetitorDropDown.Items.Add(New ListItem("--please select--", "-1"))
                BookingPriceMatchUserControl.PMCompetitorDropDown.AppendDataBoundItems = True
                BookingPriceMatchUserControl.PMCompetitorDropDown.DataSource = pm.CompetitorsDataTable.DefaultView
                BookingPriceMatchUserControl.PMCompetitorDropDown.DataTextField = "CodDesc"
                BookingPriceMatchUserControl.PMCompetitorDropDown.DataValueField = "CodId"
                BookingPriceMatchUserControl.PMCompetitorDropDown.DataBind()
                BookingPriceMatchUserControl.PMCompetitorDropDown.SelectedValue = PriceMCompetitorCodId_Value

                BookingPriceMatchUserControl.PMMatchingDropDown.Items.Clear()
                BookingPriceMatchUserControl.PMMatchingDropDown.Items.Add(New ListItem("--please select--", "-1"))
                BookingPriceMatchUserControl.PMMatchingDropDown.AppendDataBoundItems = True
                BookingPriceMatchUserControl.PMMatchingDropDown.DataSource = pm.PriceMatchingStatusDataTable.DefaultView
                BookingPriceMatchUserControl.PMMatchingDropDown.DataTextField = "CodDesc"
                BookingPriceMatchUserControl.PMMatchingDropDown.DataValueField = "CodId"
                BookingPriceMatchUserControl.PMMatchingDropDown.DataBind()
                BookingPriceMatchUserControl.PMMatchingDropDown.SelectedValue = PriceMStatusCodId_Value
            End If



        Catch ex As Exception

        End Try

    End Sub

    Public ReadOnly Property PMMatchingDropDownText As String
        Get
            Try
                If (BookingPriceMatchUserControl.PMMatchingDropDown.Items.Count > 0) Then
                    Return IIf(String.IsNullOrEmpty(BookingPriceMatchUserControl.PMMatchingDropDown.SelectedItem.Text), "Initiated", BookingPriceMatchUserControl.PMMatchingDropDown.SelectedItem.Text)
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public ReadOnly Property PMCompetitorDropDownText As String
        Get
            Try
                If (BookingPriceMatchUserControl.PMCompetitorDropDown.Items.Count > 0) Then
                    Return IIf(String.IsNullOrEmpty(BookingPriceMatchUserControl.PMCompetitorDropDown.SelectedItem.Text), "--please select--", BookingPriceMatchUserControl.PMCompetitorDropDown.SelectedItem.Text)
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            End Try
        End Get
    End Property

    Public ReadOnly Property PMrequestTypeDropDownText As String
        Get
            Try
                If (BookingPriceMatchUserControl.PMrequestTypeDropDown.Items.Count > 0) Then
                    Return IIf(String.IsNullOrEmpty(BookingPriceMatchUserControl.PMrequestTypeDropDown.SelectedItem.Text), "--please select--", BookingPriceMatchUserControl.PMrequestTypeDropDown.SelectedItem.Text)
                Else
                    Return ""
                End If
            Catch ex As Exception
                Return ""
            End Try

        End Get
    End Property

#End Region

    

End Class



