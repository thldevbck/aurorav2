
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data

<AuroraPageTitleAttribute("Notes Search")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class Booking_BookingNoteSearch
    Inherits AuroraPage

    Private bookingId As String = ""
    Private rentalId As String = ""
    Private noteId As String = ""
    Private bookingNumber As String = ""
    Private isFromSearch As String = ""

    'Delegate Sub DelPopupPostBackObject(ByVal requireRefresh As Boolean)
    Public isFirstLoad As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3285499"

            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If

            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If

            If Not Request.QueryString("hdBookingNum") Is Nothing Then
                bookingNumber = Request.QueryString("hdBookingNum").ToString()
            End If

            If Not Request.QueryString("isFromSearch") Is Nothing Then
                isFromSearch = Request.QueryString("isFromSearch").ToString()
            End If


            'Dim delPopupPostBack As New DelPopupPostBackObject(AddressOf Me.PopupPostBack)
            'Me.BookingNotesPopupUserControl1.PostBack = delPopupPostBack

            'addedBySearchUser.UserID = UserCode

            If Not Page.IsPostBack Then
                bookingNoTextBox.Text = bookingNumber
                rentalDropDownList_dataBind()
                typeDropDownList_DataBind()
                audienceDropDownList_DataBind()
                'setDefaultValues()
            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub rentalDropDownList_dataBind()
        Try
            Dim maxRental As Integer = 0

            maxRental = Booking.GetMaxRentalNumber(bookingId)

            For i As Integer = 1 To maxRental
                rentalFromDropDownList.Items.Add(Utility.ParseString(i))
                rentalToDropDownList.Items.Add(Utility.ParseString(i))
            Next

            rentalToDropDownList.SelectedValue = Utility.ParseString(maxRental)

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub typeDropDownList_DataBind()
        Try
            'Dim typeDropDownList As DropDownList = noteFormView.FindControl("typeDropDownList")
            Dim dt As DataTable = BookingNotes.GetNoteType()
            typeDropDownList.DataSource = dt
            typeDropDownList.DataTextField = "DESCRIPTION"
            typeDropDownList.DataValueField = "CODE"
            typeDropDownList.DataBind()
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub audienceDropDownList_DataBind()
        Try
            'Dim audienceDropDownList As DropDownList = noteFormView.FindControl("audienceDropDownList")
            Dim dt As DataTable = BookingNotes.GetAudienceType()

            audienceDropDownList.DataSource = dt
            audienceDropDownList.DataTextField = "DESCRIPTION"
            audienceDropDownList.DataValueField = "CODE"
            audienceDropDownList.DataBind()
            audienceDropDownList.SelectedValue = "All"
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the booking notes tab.")
        End Try
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        Try
            If Page.IsValid Then
                bookingNotesRepeater_dataBind()
            End If
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while searching notes.")
        End Try

    End Sub

    Private Sub bookingNotesRepeater_dataBind()
        Dim rentalNoFrom As String = rentalFromDropDownList.SelectedValue
        Dim rentalNoTo As String = rentalToDropDownList.SelectedValue
        Dim type As String = typeDropDownList.SelectedValue
        Dim addedBy As String = userPickerControl.Text

        Dim fromDate As DateTime
        If String.IsNullOrEmpty(fromDateControl.Text) Then
            fromDate = Nothing
        Else
            fromDate = Convert.ToDateTime(fromDateControl.Text)
        End If
        Dim toDate As DateTime
        If String.IsNullOrEmpty(toDateControl.Text) Then
            toDate = Nothing
        Else
            toDate = Convert.ToDateTime(toDateControl.Text)
        End If
        Dim audience As String = audienceDropDownList.SelectedValue
        Dim active As String = activeDropDownList.SelectedValue
        Dim containingText As String = textTextBox.Text

        Dim ds As DataSet
        ds = BookingNotes.SearchNotes(bookingId, rentalNoFrom, rentalNoTo, type, addedBy, _
            fromDate, toDate, audience, active, containingText, UserCode)

        'remove the existing session 
        Session.Remove("BookingNoteSearch_dvNotesSearch")

        If ds.Tables.Count < 1 Then

            'Disable the sort radio button list
            headerRadioButtonList.Enabled = False

            'Clear repeater
            bookingNotesRepeater.DataSource = Nothing
            bookingNotesRepeater.DataBind()
            resultLabel.Text = "No notes found"
            resultPanel.Visible = False
            SetShortMessage(AuroraHeaderMessageType.Warning, "No Records Found")
            sortTable.Visible = True
            'SetErrorMessage("No Records Found")

        ElseIf ds.Tables(0).TableName = "Error" Then
            SetShortMessage(AuroraHeaderMessageType.Error, ds.Tables(0).Rows(0)("ErrDescription"))
            sortTable.Visible = False
            'SetErrorMessage(ds.Tables(0).Rows(0)("ErrDescription"))
        Else

            ' Create an Datetime typ  e col for sorting
            Dim dt As DataTable
            dt = ds.Tables(0)
            dt.Columns.Add(New DataColumn("ADTDate", GetType(DateTime)))

            For Each dr As DataRow In dt.Rows
                dr.Item("ADTDate") = Convert.ToDateTime(dr.Item("ADT"))
            Next

            Dim dv As DataView
            dv = New DataView(dt)

            'Add the dataview to session
            Session.Add("BookingNoteSearch_dvNotesSearch", dv)

            'Sort the dv
            dv.Sort = headerRadioButtonList.SelectedValue

            If "ADTDate" = headerRadioButtonList.SelectedValue Then
                dv.Sort = dv.Sort & " desc"
            End If

            'Bind to repeater
            bookingNotesRepeater.DataSource = dv
            bookingNotesRepeater.DataBind()

            headerRadioButtonList.Enabled = True
            resultPanel.Visible = True

            resultLabel.Text = Utility.ParseString(dt.Rows.Count) & " notes found"
            sortTable.Visible = True
        End If
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        Response.Redirect(Request.Url.AbsoluteUri)
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        'Response.Redirect(BackUrl)
        Response.Redirect("Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&isFromSearch=" & isFromSearch)
    End Sub

    Protected Sub headerRadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles headerRadioButtonList.SelectedIndexChanged
        Try
            If Not Session.Item("BookingNoteSearch_dvNotesSearch") Is Nothing Then
                Dim dv As DataView
                dv = Session.Item("BookingNoteSearch_dvNotesSearch")

                dv.Sort = headerRadioButtonList.SelectedValue

                If "ADTDate" = headerRadioButtonList.SelectedValue Then
                    dv.Sort = dv.Sort & " desc"
                End If

                bookingNotesRepeater.DataSource = dv
                bookingNotesRepeater.DataBind()

            Else
                'session has been expired - research again
                AddErrorMessage("The session has been expired, please search again.")
            End If
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while sorting the notes.")
        End Try
    End Sub

    Protected Sub dateCustomValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        Try

            If Not (fromDateControl.IsValid And toDateControl.IsValid) Then
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
                e.IsValid = False
                Return
            End If

            Dim fromString As String = fromDateControl.Text
            Dim toString As String = toDateControl.Text

            If Not (String.IsNullOrEmpty(fromString) Or String.IsNullOrEmpty(toString)) Then
                Dim fromDate As DateTime = Convert.ToDateTime(fromString)
                Dim toDate As DateTime = Convert.ToDateTime(toString)

                If fromDate <= toDate Then
                    e.IsValid = True
                Else
                    e.IsValid = False
                    SetShortMessage(AuroraHeaderMessageType.Warning, "To date cannot be before the From date.")
                    'SetErrorMessage("To date cannot be before the From date.")
                End If
            Else
                e.IsValid = True
            End If

        Catch ex As Exception
            e.IsValid = False
            SetShortMessage(AuroraHeaderMessageType.Warning, "Date is invalid.")
            'SetErrorMessage("Date is invalid.")
        End Try
    End Sub

    'update note event
    Protected Sub bookingNotesRepeater_ItemCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles bookingNotesRepeater.ItemCommand
        Try
            If e.CommandName.ToString() = "update" Then
                popupUserControlLoad()
                Dim noteId As String
                noteId = e.CommandArgument.ToString()
                BookingNotesPopupUserControl1.loadEditTemplate(noteId)
            End If
        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub popupUserControlLoad()
        BookingNotesPopupUserControl1.BookingId = bookingId

    End Sub

    Protected Sub BookingNotesPopupUserControl1_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As Object) Handles BookingNotesPopupUserControl1.PostBack
        PopupPostBack(param)
    End Sub

    Public Sub PopupPostBack(ByVal requireRefresh As Boolean)
        If requireRefresh Then
            bookingNotesRepeater_dataBind()
            audienceDropDownList.Visible = True
        End If
    End Sub

    Private Const card As String = "Card"
    Protected Function AllowUpdate(ByVal type As Object) As Boolean
        Dim typeString As String
        typeString = Convert.ToString(type)

        ''If typeString = "Rental" Or typeString = "Booking" Or typeString = "Package" Then
        If typeString = "Rental" Or typeString = "Booking" Or typeString = "Package" Or typeString.ToString.Contains(card) Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)

        If String.IsNullOrEmpty(dString) Then
            Return ""
        ElseIf dString.IndexOf(" ") > 0 Then
            Return Utility.DateDBUIConvert(dString.Substring(0, dString.IndexOf(" "))) & dString.Substring(dString.IndexOf(" "))
        Else
            Return Utility.DateDBUIConvert(dString)
        End If
    End Function

End Class
