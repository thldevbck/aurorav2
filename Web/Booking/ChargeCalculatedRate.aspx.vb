Imports Aurora.Common
Imports Aurora.Common.Data
Imports System.Xml
Imports System.Drawing

<AuroraPageTitleAttribute("Booking : ")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class Booking_ChargeCalculatedRate
    Inherits AuroraPage

    Public Overrides ReadOnly Property PageTitle() As String
        Get
            Return "Calculated Rate: Booking: " & Request.QueryString("htm_hid_BookNum")
        End Get
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            Dim sBapId As String = Request.QueryString("sBapId")
            Dim sChgDetId As String = Request.QueryString("sChgDetId")
            Dim hid_htm_chgData As String = Server.HtmlDecode(Request.QueryString("hid_htm_chgData"))

            hid_htm_chgData = hid_htm_chgData.Replace("<", "^")
            hid_htm_chgData = hid_htm_chgData.Replace(">", "~")
            Dim htm_hid_BookNum As String = Request.QueryString("htm_hid_BookNum")

            ''''rev:mia feb 5 2013 - quickfix
            Try
                ''Session("hid_htm_chgData_2013")
                Dim xml As New XmlDocument
                xml.LoadXml(hid_htm_chgData)
            Catch ex As Exception
                ''invalid xml so use the session
                hid_htm_chgData = Session("hid_htm_chgData_2013")
                hid_htm_chgData = hid_htm_chgData.Replace("<", "^")
                hid_htm_chgData = hid_htm_chgData.Replace(">", "~")
            End Try
            ''@sChgDetId varchar(64),  
            ''@sBpdId  varchar(64),  
            ''@tChgData varchar(8000)  

            ''spName = "RES_ChargeCalculateRateRpt '" + Request("sChgDetId") + "', '" + Request("sBapId") + "', '" + Replace(Replace(Request("hid_htm_chgData"), "<", "^"), ">", "~") + "'"
            Dim tempString As String = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_ChargeCalculateRateRpt", sChgDetId, sBapId, hid_htm_chgData).OuterXml

            'JL 2008/06/13
            'Get the status color and put it in the xml
            Dim xmlDoc As New XmlDocument
            xmlDoc.LoadXml(tempString)
            Dim rentalStatus As String
            rentalStatus = xmlDoc.GetElementsByTagName("Status")(0).InnerText
            Dim color As String
            color = ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(rentalStatus))
            tempString = tempString.Replace("</data>", "<ui><statusColor>" & color & "</statusColor></ui></data>")
            'JL 2008/06/13

            Me.Xml1.DocumentContent = tempString
            Me.Xml1.TransformSource = Server.MapPath("~/Booking/xsl/ChargeCalculatedRate.xsl")

        End If
    End Sub
End Class
