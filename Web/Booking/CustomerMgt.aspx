<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="CustomerMgt.aspx.vb" Inherits="Booking_CustomerMgt" Title="Untitled Page" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript">

var errorMessage = <%= MessageJSON %>;
    
function checkNameChars( obj )
{
	var chValue, szValue
	var newString1, newString2
		
	szValue = obj.value
	if ( szValue!= "" && szValue != null )
	{
		chValue = szValue.charAt(0)
		newString1 = chValue.toUpperCase()
		newString2 = szValue.substr( 1 )
		obj.value = newString1 + newString2
	}	
	return
}

function checkFullStop(obj)
{
	var sChar;
	var sStr
	var slen, i
	sStr = obj.value
	slen = sStr.length
	for ( i = 0; i < slen; i++)
		sStr = sStr.replace(".", " ")
	if (sStr.charAt(slen) == " ")
		sStr = sStr.substring(0,slen-1)
	obj.value = sStr
	return
}

function removeSpace(obj)
{ 
    s=obj.value
    String.prototype.trim = function() { return this.replace(/(^\s*)|(\s*$)/g, "");}
    sTrim = s.trim()
    obj.value=sTrim
}

function setAllQuantity()
{
    //get target base & child control.
    var TargetBaseControl = document.getElementById('<%= brochuresGridView.ClientID %>');
    var TargetChildControl = "qtyTextBox";

    //get all the control of the type INPUT in the base control.
    var Inputs = TargetBaseControl.getElementsByTagName("input");  

    for(var n = 0; n < Inputs.length; ++n)
        if(Inputs[n].type == 'text' && Inputs[n].id.indexOf(TargetChildControl,0) >= 0)
             Inputs[n].value = '1';
            
    return false;
}

function ClientValidation()
{ 
    var lastNameTextBox;
    lastNameTextBox = $get('<%= lastNameTextBox.ClientID %>');   
    if (! validateAControl(lastNameTextBox))
    {
        return false
    }
    var firstNameTextBox;
    firstNameTextBox = $get('<%= firstNameTextBox.ClientID %>');
    if (! validateAControl(firstNameTextBox))
    {
        return false
    }
    var personTypeDropDownList;
    personTypeDropDownList = $get('<%= personTypeDropDownList.ClientID %>');
    if (! validateAControl(personTypeDropDownList))
    {
        return false
    }

    var licenceNumTextBox;
    licenceNumTextBox = $get('<%= licenceNumTextBox.ClientID %>');
    if (licenceNumTextBox.value != "")
    {
        var expiryDateDateControl;
        expiryDateDateControl = $get('<%= expiryDateDateControl.ClientID %>');
        var issuingAuthorityTextBox;
        issuingAuthorityTextBox = $get('<%= issuingAuthorityTextBox.ClientID %>');
        var dobDateControl;
        dobDateControl = $get('<%= dobDateControl.ClientID %>');
        
		if ( expiryDateDateControl.value == "" || issuingAuthorityTextBox.value == "" || dobDateControl.value == "" )
		{
			setWarningShortMessage("You must enter Expiry Date, Issuing Authority and Date of Birth for licence details")
			return false
		} 
	}	   	
} 

function validateAControl(obj)
{
    if (obj.value == "")
    {
        setWarningShortMessage(getErrorMessage("GEN005"))
        return false
    }
    else
    {
        return true
    }
}

function dobDateControlOnChange(dobTextBoxId)
{
    var dobTextBox;
    dobTextBox = $get(dobTextBoxId);  
    var ageLabel;
    ageLabel = $get('<%= ageLabel.ClientID %>');	
        
	var sDob = dobTextBox.value

	if(sDob == "") 
	{
	    ageLabel.innerText = ""
		return
    }
		
	var sYear, sMonth, sDay, dArray
	sYear = sDob.split("/")[2]
	sMonth = sDob.split("/")[1]
	sDay = sDob.split("/")[0]
	
	var newDate = new Date()
	var nYear = newDate.getFullYear()
	var nMonth = newDate.getMonth() + 1
	var nDay = newDate.getDate()

	var sAge	
		
	if(parseInt(nDay,10) - parseInt(sDay,10) >= 0){			
		if(parseInt(nMonth,10) - parseInt(sMonth,10) >= 0){
			sAge = parseInt(nYear,10) - parseInt(sYear,10)
		} 
		else{
			sAge = parseInt(nYear,10) - parseInt(sYear,10) - 1
		}		
	}
	else{
		if(parseInt(nMonth,10) - parseInt(sMonth,10) > 0){
			sAge = parseInt(nYear,10) - parseInt(sYear,10)
		} 
		else{
			sAge = parseInt(nYear,10) - parseInt(sYear,10) - 1
		}
	}

	if(sAge < 0){
		alert("Customer's date of birth must be ealier than today!")
		return
	}
	ageLabel.innerText = sAge
}
    
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <asp:UpdatePanel ID="updatePanel10" runat="server">
            <contenttemplate>
<DIV style="DISPLAY: none">
    <asp:Label id="customerIdLabel" runat="server"></asp:Label> 
    <asp:Label id="hirerLabel" runat="server"></asp:Label> 
    <asp:Label id="integrityNoLabel" runat="server"></asp:Label> 
    <asp:Label id="dayCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="dayIdLabel" runat="server"></asp:Label> 
    <asp:Label id="dayIntNumLabel" runat="server"></asp:Label> 
    <asp:Label id="nightCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="nightIdLabel" runat="server"></asp:Label> 
    <asp:Label id="nightIntNumLabel" runat="server"></asp:Label> 
    <asp:Label id="mobileCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="mobileIdLabel" runat="server"></asp:Label> 
    <asp:Label id="mobileIntNumLabel" runat="server"></asp:Label> 
    <asp:Label id="faxCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="faxIdLabel" runat="server"></asp:Label> 
    <asp:Label id="faxIntNumLabel" runat="server"></asp:Label> 
    <asp:Label id="emailCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="emailIdLabel" runat="server"></asp:Label> 
    <asp:Label id="emailIntNumLabel" runat="server"></asp:Label> 
    <asp:Label id="physicalAddressIdLabel" runat="server"></asp:Label> 
    <asp:Label id="physicalAddressCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="physicalAddressIntegrityNoLabel" runat="server"></asp:Label> 
    <asp:Label id="postalAddressIdLabel" runat="server"></asp:Label> 
    <asp:Label id="postalAddressCodIdLabel" runat="server"></asp:Label> 
    <asp:Label id="postalAddressIntegrityNoLabel" runat="server"></asp:Label> 
</DIV>
<TABLE width="100%">
<TBODY>
    <TR>
        <TD width=150>Last Name:</TD>
        <TD width=250><asp:TextBox onblur="checkNameChars(this)" id="lastNameTextBox" runat="server"></asp:TextBox>&nbsp;*</TD>
        <TD width=150>First Name:</TD>
        <TD ><asp:TextBox onblur="checkNameChars(this); checkFullStop(this); removeSpace(this)" id="firstNameTextBox" runat="server"></asp:TextBox>&nbsp;*</TD>
     </TR>
     <TR>   
        <TD width=150>Title:</TD>
        <TD width=250><asp:TextBox onblur="checkNameChars(this); checkFullStop(this); removeSpace(this)" id="titleTextBox" runat="server"></asp:TextBox></TD>
    </TR>
    <TR>
        <TD>Licence Number:</TD>
        <TD><asp:TextBox id="licenceNumTextBox" runat="server"></asp:TextBox></TD>
        <TD>Expiry Date:</TD>
        <TD><uc1:DateControl id="expiryDateDateControl" runat="server"></uc1:DateControl></TD>
    </TR>
    <TR>
        <TD>Issuing Authority:</TD>
        <TD><asp:TextBox id="issuingAuthorityTextBox" runat="server"></asp:TextBox></TD>
    </TR>
    <TR>
        <TD>Date Of Birth:</TD>
        <TD><uc1:DateControl id="dobDateControl" runat="server"></uc1:DateControl></TD>
        <TD>Age:</TD>
        <TD><asp:Label id="ageLabel" runat="server" Text=""></asp:Label></TD>
    </TR>
    <TR>
        <TD>Flags:</TD>
        <TD><asp:CheckBox id="vipCheckBox" runat="server" Text="VIP"></asp:CheckBox>&nbsp;&nbsp;<asp:CheckBox id="fleetDriverCheckBox" runat="server" Text="Fleet"></asp:CheckBox></TD>
        
        <TD>Type:</TD>
        <TD><asp:DropDownList id="personTypeDropDownList" runat="server" AppendDataBoundItems="true">
            <asp:ListItem Text="" Value=""></asp:ListItem>
        </asp:DropDownList>&nbsp;* </TD>
    </TR>
    <TR>
        <TD>Passport No:</TD>
        <TD><asp:TextBox id="passportNoTextBox" runat="server"></asp:TextBox> </TD>
        <TD>Country of Issue: </TD>
        <TD><asp:TextBox id="passportCountryTextBox" runat="server"></asp:TextBox> </TD>
    </TR>
</TBODY></TABLE>
                
<TABLE width="100%"><TBODY>
    <TR>
        <TD width=150></TD>
        <TD width=250></TD>
        <TD width=150></TD>
        <TD ></TD>
    </TR>
    <TR>
        <TD colSpan=4><HR /></TD>
    </TR>
    <TR>
        <TD colSpan=2><STRONG>Physical Address</STRONG></TD>
        <TD colSpan=2><STRONG>Postal Address</STRONG></TD>
    </TR>
</TBODY></TABLE>
<TABLE width="100%"><TBODY>
    <TR width="100%">
        <TD width=400>
            <asp:Panel id="panel1" runat="server" Width="100%">
                <table width="100%">
                        <tr>
                            <td width="150">
                                Street:</td>
                            <td width="250">
                                <asp:TextBox ID="physicalStreetTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox>&nbsp;*</td>
                        </tr>
                        <tr>
                            <td>
                                Suburb:</td>
                            <td>
                                <asp:TextBox ID="physicalSuburbTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                City / Town:</td>
                            <td>
                                <asp:TextBox ID="physicalCityTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox>&nbsp;*</td>
                        </tr>
                        <tr>
                            <td>
                                State:</td>
                            <td>
                                <asp:TextBox ID="physicalStateTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                PostCode:</td>
                            <td>
                                <asp:TextBox ID="physicalPostCodeTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Country:</td>
                            <td>
                                <uc1:PickerControl ID="physicalCountryPickerControl" runat="server" PopupType="POPUPCOUNTRY"
                                    AppendDescription="true" width="150" />&nbsp;*
                            </td>
                        </tr>
                    </table>
            </asp:Panel> 
        </TD>     
        <TD>
            <asp:Panel id="panel2" runat="server" Width="100%">
                    <table width="100%">
                        <tr>
                            <td width="150">
                                Street:</td>
                            <td>
                                <asp:TextBox ID="postalStreetTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Suburb:</td>
                            <td>
                                <asp:TextBox ID="postalSuburbTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                City / Town:</td>
                            <td>
                                <asp:TextBox ID="postalCityTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                State:</td>
                            <td>
                                <asp:TextBox ID="postalStateTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                PostCode:</td>
                            <td>
                                <asp:TextBox ID="postalPostcodeTextBox" runat="server" onBlur="checkFullStop(this); removeSpace(this)"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>
                                Country:</td>
                            <td>
                                <uc1:PickerControl ID="postalCountryPickerControl" runat="server" PopupType="POPUPCOUNTRY"
                                    AppendDescription="true" width="150" />
                            </td>
                        </tr>
                    </table>
            </asp:Panel> 
        </TD>
     </TR>
</TBODY></TABLE>

<TABLE width="100%"><TBODY>
    <TR>
        <TD align=right>
        <asp:Button id="saveButton" onclick="saveButton_Click" runat="server" Text="Save" 
            CssClass="Button_Standard Button_Save" OnClientClick="return ClientValidation();"></asp:Button> 
        <asp:Button id="newButton" onclick="newButton_Click" runat="server" Text="Save/New" 
            CssClass="Button_Standard Button_New" OnClientClick="return ClientValidation();"></asp:Button> 
        <asp:Button id="cancelButton" onclick="cancelButton_Click" runat="server" Text="Reset" 
            CssClass="Button_Standard Button_Reset" OnClientClick='return confirm("Are you sure you wish to delete this customer?");'></asp:Button>
         <asp:Button id="deleteButton" onclick="deleteButton_Click" runat="server" Text="Delete" 
            CssClass="Button_Standard Button_Delete"></asp:Button> 
        <asp:Button id="backButton" onclick="backButton_Click" runat="server" Text="Back" 
            CssClass="Button_Standard Button_Back"></asp:Button> 
     </TD></TR></TBODY></TABLE>
<HR />


<TABLE width="100%" ><TBODY>
    <tr width="100%">
        <td ><STRONG>Contact Numbers</STRONG></td>
        <td  colspan=3></td>
        <td ></td>
        <td></td>
    </tr>
    <TR>
        <TD style="HEIGHT: 17px" width=150></TD>
        <TD style="HEIGHT: 17px" width=50>Country</TD>
        <TD style="HEIGHT: 17px" width=50>Area</TD>
        <TD style="HEIGHT: 17px" width=145>Number</TD>
        <TD style="HEIGHT: 17px" width=150></TD>
        <TD style="HEIGHT: 17px" ></TD>
    </TR>
    <TR>
        <TD>Day:</TD>
        <TD><asp:TextBox id="dayCountryTextBox" runat="server" Width="40"></asp:TextBox></TD>
        <TD><asp:TextBox id="dayAreaTextBox" runat="server" Width="40"></asp:TextBox></TD>
        <TD><asp:TextBox id="dayNumberTextBox" runat="server" Width="80"></asp:TextBox></TD>
        <TD>Email:</TD>
        <TD><asp:TextBox id="emailTextBox" runat="server" width="97%"></asp:TextBox></TD>
    </TR>
    <TR>
        <TD>Night:</TD>
        <TD><asp:TextBox id="nightCountryTextBox" runat="server" Width="40"></asp:TextBox></TD>
        <TD><asp:TextBox id="nightAreaTextBox" runat="server" Width="40"></asp:TextBox></TD>
        <TD><asp:TextBox id="nightNumberTextBox" runat="server" Width="80"></asp:TextBox></TD>
        <TD></TD><TD></TD>
    </TR>
    <TR>
        <TD>Mobile:</TD>
    <TD><asp:TextBox id="mobileCountryTextBox" runat="server" Width="40"></asp:TextBox></TD>
    <TD><asp:TextBox id="mobileAreaTextBox" runat="server" Width="40"></asp:TextBox></TD>
    <TD><asp:TextBox id="mobileNumberTextBox" runat="server" Width="80"></asp:TextBox></TD>
    <TD></TD><TD></TD></TR>
    <TR><TD>Fax:</TD>
    <TD><asp:TextBox id="faxCountryTextBox" runat="server" Width="40"></asp:TextBox></TD>
    <TD><asp:TextBox id="faxAreaTextBox" runat="server" Width="40"></asp:TextBox></TD>
    <TD><asp:TextBox id="faxNumberTextBox" runat="server" Width="80"></asp:TextBox></TD>
    <TD></TD><TD></TD></TR></TBODY></TABLE>
    
<HR />
<STRONG>Marketing Data </STRONG>

<TABLE width="100%"><TBODY>
<TR><TD width=150>Flags:</TD><TD width=250>
<asp:CheckBox id="mailingListCheckBox" runat="server" Text="Mailing List"></asp:CheckBox>&nbsp;&nbsp;
<asp:CheckBox id="surveysCheckBox" runat="server" Text="Surveys"></asp:CheckBox></TD>
<TD width=150>Pref. Comm:</TD>

<TD ><asp:DropDownList id="prefCommDropDownList" runat="server" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                </asp:DropDownList></TD></TR><TR><TD>Gender:</TD><TD>
                
                <asp:DropDownList id="genderDropDownList" runat="Server" AppendDataBoundItems="true">
                    <asp:ListItem Text="" Value=""></asp:ListItem>
                    <asp:ListItem Text="Male" Value="Male"></asp:ListItem>
                    <asp:ListItem Text="Female" Value="Female"></asp:ListItem>
                </asp:DropDownList>
                
                
                </TD><TD>Occupation:</TD><TD><asp:TextBox id="occupationTextBox" runat="server"></asp:TextBox></TD></TR></TBODY></TABLE>
<HR />
<TABLE width="100%"><TBODY><TR><TD><STRONG>Brochures</STRONG></TD><TD align=right><asp:Button id="requestAllButton" runat="server" Text="Request All" CssClass="Button_Standard" OnClientClick="return setAllQuantity();"></asp:Button></TD></TR></TBODY></TABLE>
<asp:GridView id="brochuresGridView" runat="server" Width="100%" CssClass="dataTable" AutoGenerateColumns="False">
        <AlternatingRowStyle CssClass="oddRow" />
        <RowStyle CssClass="evenRow" />
        <Columns>
            <asp:TemplateField HeaderText="Qty" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="brhIdLabel" runat="server" Text='<%# Bind("BrhId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="BrdName" HeaderText="Brand" ReadOnly="True" />
            <asp:BoundField DataField="BrhDateIss" HeaderText="Issue Date" ReadOnly="True" />
            <asp:BoundField DataField="BrhName" HeaderText="Name" ReadOnly="True" />
            
            <asp:TemplateField HeaderText="Total" >
                    <ItemTemplate>
                                               
                        <asp:Label runat="server" ID="totalLabel" 
                            Text='<%# eval("Total") %>' Width="40" style="text-align: right" />
                    </ItemTemplate>
                </asp:TemplateField>
                
            <asp:BoundField DataField="DateRequested" HeaderText="Date Requested" ReadOnly="True" />
            <asp:TemplateField HeaderText="Qty" Visible="true">
                <ItemTemplate>
                    <asp:TextBox ID="qtyTextBox" runat="server" Width="50" Text='<%# Bind("Qty") %>'
                        style="text-align: right"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView> 
    
    <TABLE width="100%"><TBODY><TR><TD align=right>
    <asp:Button id="saveButton1" onclick="saveButton_Click" runat="server" Text="Save" 
        CssClass="Button_Standard Button_Save" OnClientClick="return ClientValidation();"></asp:Button> 
    <asp:Button id="newButton1" onclick="newButton_Click" runat="server" Text="Save/New" 
        CssClass="Button_Standard Button_New" OnClientClick="return ClientValidation();"></asp:Button> 
    <asp:Button id="cancelButton1" onclick="cancelButton_Click" runat="server" Text="Reset" 
        CssClass="Button_Standard Button_Reset" OnClientClick='return confirm("Are you sure you wish to delete this customer?");'></asp:Button> 
    <asp:Button id="deleteButton1" onclick="deleteButton_Click" runat="server" Text="Delete" 
        CssClass="Button_Standard Button_Delete"></asp:Button> 
    <asp:Button id="backButton1" onclick="backButton_Click" runat="server" Text="Back" 
        CssClass="Button_Standard Button_Back"></asp:Button> 
        
        </TD></TR></TBODY></TABLE><BR />
</contenttemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
