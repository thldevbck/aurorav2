<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="DriverMgt.aspx.vb" Inherits="Booking_DriverMgt" Title="Untitled Page" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript">
    
    var errorMessage = <%= MessageJSON %>;
    
    function CheckUncheckCheckboxes(CheckBox)
    {
        //get target base & child control.
        var TargetBaseControl = document.getElementById('<%= driverGridView.ClientID %>');
        var TargetChildControl = "removeCheckBox";

        //get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");  

        for(var n = 0; n < Inputs.length; ++n)
            if(Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl,0) >= 0)
                 Inputs[n].checked = CheckBox.checked;
    }
        
    function checkNameChars( obj )
	{
		var chValue, szValue
		var newString1, newString2
		
		szValue = obj.value
		if ( szValue!= "" && szValue != null )
		{
			chValue = szValue.charAt(0)
			newString1 = chValue.toUpperCase()
			newString2 = szValue.substr( 1 )
			obj.value = newString1 + newString2
		}	
		return
	}
	
	function checkFullStop(obj)
	{
		var sChar;
		var sStr
		var slen, i
		sStr = obj.value
		slen = sStr.length
		for (i = 0; i < slen; i++)
			sStr = sStr.replace(".", " ")
		if (sStr.charAt(slen) == " ")
			sStr = sStr.substring(0,slen-1)
		obj.value = sStr
		return
	}
	
	function removeSpace(obj)
	{ 
	    s=obj.value
        String.prototype.trim = function() { return this.replace(/(^\s*)|(\s*$)/g, "");}
        sTrim = s.trim()
        obj.value=sTrim
    }
	
	function ClientValidation()
    { 

        var licenceTextBox;
        licenceTextBox = $get('<%= licenceTextBox.ClientID %>');
        if (! validateAControl(licenceTextBox))
        {
            return false
        }
        var lastNameTextBox;
        lastNameTextBox = $get('<%= lastNameTextBox.ClientID %>');   
        if (! validateAControl(lastNameTextBox))
        {
            return false
        }
        var firstNameTextBox;
        firstNameTextBox = $get('<%= firstNameTextBox.ClientID %>');
        if (! validateAControl(firstNameTextBox))
        {
            return false
        }
        var issuingAuthorityTextBox;
        issuingAuthorityTextBox = $get('<%= issuingAuthorityTextBox.ClientID %>');
        if (! validateAControl(issuingAuthorityTextBox))
        {
            return false
        }
    
    } 
    
    function validateAControl(obj)
    {
        if (obj.value == "")
        {
            setWarningShortMessage(getErrorMessage("GEN005"))
            return false
        }
        else
        {
            return true
        }
    }
    
function dobDateControlOnChange(dobTextBoxId)
{
    var dobTextBox;
    dobTextBox = $get(dobTextBoxId);  
    var ageLabel;
    ageLabel = $get('<%= ageLabel.ClientID %>');	
        
	var sDob = dobTextBox.value

	if(sDob == "") 
	{
	    ageLabel.innerText = ""
		return
    }
		
	var sYear, sMonth, sDay, dArray
	sYear = sDob.split("/")[2]
	sMonth = sDob.split("/")[1]
	sDay = sDob.split("/")[0]
	
	var newDate = new Date()
	var nYear = newDate.getFullYear()
	var nMonth = newDate.getMonth() + 1
	var nDay = newDate.getDate()

	var sAge	
		
	if(parseInt(nDay,10) - parseInt(sDay,10) >= 0){			
		if(parseInt(nMonth,10) - parseInt(sMonth,10) >= 0){
			sAge = parseInt(nYear,10) - parseInt(sYear,10)
		} 
		else{
			sAge = parseInt(nYear,10) - parseInt(sYear,10) - 1
		}		
	}
	else{
		if(parseInt(nMonth,10) - parseInt(sMonth,10) > 0){
			sAge = parseInt(nYear,10) - parseInt(sYear,10)
		} 
		else{
			sAge = parseInt(nYear,10) - parseInt(sYear,10) - 1
		}
	}

	if(sAge < 0){
		alert("Customer's date of birth must be ealier than today!")
		return
	}
	ageLabel.innerText = sAge
}
    
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panel11" runat="server" Width="100%">
      <%--  <asp:UpdatePanel ID="updatePanel12" runat="server">
            <contenttemplate>--%>
     <table width="100%">
        <tr width="100%">
            <td width="150">
                Booking/Rental:
            </td>
            <td width="250">
                <asp:TextBox ID="renatlTextBox" runat="server" readOnly=true ></asp:TextBox>
            </td>
            <td width="50%">
            </td>
        </tr>
        <tr>
        <td><b>
                Drivers</b></td>
            <td colspan="2" align="right">
            <asp:Button ID="addDriverButton" runat="server" Text="Add Driver" CssClass="Button_Standard Button_Add"/>
            <asp:Button ID="removeDriverButton" runat="server" Text="Remove Driver" CssClass="Button_Standard Button_Remove"/>
<%--                <asp:LinkButton ID="addDriverLinkButton" runat="server">Add Driver</asp:LinkButton>
                <asp:LinkButton ID="removeDriverLinkButton" runat="server">Remove Driver</asp:LinkButton>--%>
            </td>
        </tr>
        <tr>
            <td colspan="3" width="100%">
                <asp:GridView ID="driverGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                    CssClass="dataTable">
                    <AlternatingRowStyle CssClass="oddRow" />
                    <RowStyle CssClass="evenRow" />
                    <Columns>
                        <asp:TemplateField HeaderText="Id" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="driverIdLabel1" runat="server" Text='<%# Bind("DriverId") %>'></asp:Label>
                                <asp:Label ID="integrityNoLabel1" runat="server" Text='<%# Bind("IntegrityNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Licence *">
                            <ItemTemplate>
                                <asp:Label ID="licenceNoLabel" runat="server" Text='<%# Bind("LicenceNo") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DOB *">
                            <ItemTemplate>
                                <asp:Label ID="dobLabel" runat="server" Text='<%# Bind("DOB") %>' onblur='findDriver();'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Age">
                            <ItemTemplate>
                                <asp:Label ID="ageLabel" runat="server" Text='<%# Bind("Age") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Name *">
                            <ItemTemplate>
                                <asp:Label ID="lastNameLabel" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="First Name *">
                            <ItemTemplate>
                                <asp:Label ID="firstNameLabel" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Title">
                            <ItemTemplate>
                                <asp:Label ID="titleLabel" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Issuing Authority *">
                            <ItemTemplate>
                                <asp:Label ID="issuingAuthorityLabel" runat="server" Text='<%# Bind("IssuingAuthority") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expiry Date *">
                            <ItemTemplate>
                                <asp:Label ID="expiryDateLabel" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox ID="removeAllCheckBox" runat="server" onclick="javascript:CheckUncheckCheckboxes(this);" />Remove
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="removeCheckBox" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField CommandName="Update" Text="Edit" />
                    </Columns>
                 
                </asp:GridView>
                <br />
           
                        <asp:Panel ID="editPanel" runat="server" BorderWidth="1">
                            <div style="display: none">
                                <asp:Label ID="driverIdLabel" runat="server" Text=""></asp:Label>
                                <asp:Label ID="integrityNoLabel" runat="server" Text=""></asp:Label>
                            </div>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <b><asp:Label ID="statusLabel" runat="server" Text=" "></asp:Label></b>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="150">
                                        Licence: </td>
                                    <td width="250">
                                        <asp:TextBox ID="licenceTextBox" runat="server" Text='<%# Bind("LicenceNo") %>' 
                                            AutoPostBack="True"></asp:TextBox>&nbsp;*
                                    </td>
                                    <td width="150">
                                        DOB:</td>
                                    <td width="250">
                                        <uc1:DateControl ID="dobDateControl" runat="server" Nullable="false" />&nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Age:</td>
                                    <td>
                                        <asp:Label ID="ageLabel" runat="server" Text='' ></asp:Label>
                                    </td>
                                    <td>
                                        Last Name:</td>
                                    <td>
                                        <asp:TextBox ID="lastNameTextBox" runat="server" Text='<%# Bind("LastName") %>' 
                                            onBlur="checkNameChars(this)"></asp:TextBox>&nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        First Name:</td>
                                    <td>
                                        <asp:TextBox ID="firstNameTextBox" runat="server" Text='<%# Bind("FirstName") %>'
                                            onBlur="checkNameChars(this); checkFullStop(this); removeSpace(this)"></asp:TextBox>&nbsp;*
                                    </td>
                                    <td>
                                        Title:</td>
                                    <td>
                                        <asp:TextBox ID="titleTextBox" runat="server" Text='<%# Bind("Title") %>' 
                                            onBlur="checkNameChars(this); checkFullStop(this)"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Issue Authority:</td>
                                    <td>
                                        <asp:TextBox ID="issuingAuthorityTextBox" runat="server" Text='<%# Bind("IssuingAuthority") %>'></asp:TextBox>&nbsp;*
                                    </td>
                                    <td>
                                        Expiry Date:</td>
                                    <td>
                                        <uc1:DateControl ID="expiryDateDateControl" runat="server" Nullable="false" />&nbsp;*
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=4 align="right">
                                    <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                                            OnClientClick="return ClientValidation();" />
                                        <asp:Button ID="cancelButton" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset"/>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
             
            </td>
        </tr>
        <tr width="100%">
            <td colspan="3" align="right">
            <br />
                <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back"/>
            </td>
        </tr>
    </table>
    <%--
       </contenttemplate>
        </asp:UpdatePanel>--%>
    </asp:Panel>
</asp:Content>
