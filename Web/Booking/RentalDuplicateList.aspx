<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="RentalDuplicateList.aspx.vb" Inherits="Booking_DuplicateRentals" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:Panel ID="panelinfo" runat="server" Width="900">
        <table width="100%">
            <tr width="100%">
                <td width="15%">
                    Agent:
                </td>
                <td width="35%">
                    <input style="width: 297px" id="txtagent" readonly type="text" runat="server"  />
                </td>
                <td width="50%">
                </td>
            </tr>
            <tr width="100%">
                <td width="15%" align="left">
                    Hirer:
                </td>
                <td width="35%">
                    <input style="width: 297px" id="txtHirer" readonly type="text" runat="server"  />
                </td>
                <td width="50%">
                <asp:Button ID="btnBrochure" runat="server" Text="Request Brochure" CssClass="Button_Standard"/>
                </td>
            </tr>
            
                
        </table>
                
    </asp:Panel>
    <br />
    <asp:Panel ID="panelForDuplicate" runat="server" Width="100%"  ScrollBars="Auto">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <contenttemplate>
                <TABLE>
                    <TBODY>
                            <TR>
                                <TD height=15></TD>
                            </TR>
                            <TR>
                                <TD vAlign=middle colSpan=5>
                                    <asp:GridView id="GridView1" runat="server" width="880px" AutoGenerateColumns="False" DataKeyNames="BooId" cssClass="dataTableGrid">
                                        <RowStyle CssClass="evenRow"></RowStyle>
                                            <Columns>
                                            <asp:TemplateField HeaderText="Booking Rental">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hpLink" runat="server" NavigateUrl=""   Text='<%# eval("BookingRental") %>' />
                                            </ItemTemplate>
                                            </asp:TemplateField>
                                                <asp:BoundField DataField="AgentHirer" HeaderText="Agent Hirer">
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                                <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AgentRefPackage" HeaderText="Agent Ref Package"></asp:BoundField>
                                            <asp:BoundField DataField="CheckOut" HeaderText="Check Out"></asp:BoundField>
                                            <asp:BoundField DataField="CheckIn" HeaderText="Check In"></asp:BoundField>
                                            <asp:BoundField DataField="HirePdStatus" HeaderText="Hire Pd Status"></asp:BoundField>
                                            <asp:BoundField DataField="RentalValue" HeaderText="Rental Value"></asp:BoundField>
                                            <asp:TemplateField HeaderText="Check">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkError" Checked="false" runat="server" AutoPostBack="false" OnCheckedChanged="CheckBox1_CheckedChanged"   Visible='<%# ParseEvalItemToBoolean(Eval("HasError"),eval("BookingRental")) %>' />
                                                    </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                <AlternatingRowStyle CssClass="evenRow"></AlternatingRowStyle>
                                </asp:GridView> 
                              </TD></TR><TR><TD height=10></TD></TR></TBODY></TABLE>
</contenttemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <table width="100%">
        <tr width="100%">
            <td align=right >
                <asp:Button ID="btnNext" TabIndex="1" runat="server" Width="150px" Text="Next" CssClass="Button_Standard"></asp:Button>
            
                <asp:Button ID="btnModifyBookingRequest" TabIndex="2" runat="server" Text="Modify Booking Request"
                    Width="150px" CssClass="Button_Standard"></asp:Button>
            
                <asp:Button ID="btnNewBookingRequest" TabIndex="3" runat="server" Text="New Booking Request"
                    Width="150px" CssClass="Button_Standard"></asp:Button>
            
                <asp:Button ID="btnAddToBooking" TabIndex="4" runat="server" Width="150px" Text="Add To Booking" CssClass="Button_Standard">
                </asp:Button>
            
                <asp:Button ID="btnUnset" TabIndex="5" runat="server" Width="150px" Text="Unset Add To Booking" CssClass="Button_Standard">
                </asp:Button>
            </td>
        </tr>
    </table>
    <ajaxToolkit:ConfirmButtonExtender ID="Confirm_NBR" runat="server" ConfirmText="Are you sure you want to start a new Booking Request?"
        TargetControlID="btnNewBookingRequest">
    </ajaxToolkit:ConfirmButtonExtender>
    <asp:HiddenField ID="Hidbkrid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="Hidbooid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="Hidbutsel" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="Hidvhrid" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="Hidbpdid" runat="server" EnableViewState="true" />
    <asp:Label ID="lblbooid" runat="server" EnableViewState="true"  Visible="false"/>
    <asp:HiddenField ID="hidContactID" runat="server" EnableViewState="true" />
</asp:Content>
