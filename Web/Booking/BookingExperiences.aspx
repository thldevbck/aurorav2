﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookingExperiences.aspx.vb" Inherits="Booking_BookingExperiences" %>

<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="dt" %>
<%@ Register Src="~\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="tm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        table
        {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <asp:UpdatePanel ID="experienceUpdatePanel" runat="server">
        <ContentTemplate>
            <asp:Panel ID="panelExperiences" runat="server" Width="100%">
                <table class="dataTableGrid">
                    <tr class="oddRow" style="background-color:#AAA">
                        <td>
                            <b>Booking Id</b>
                        </td>
                         <td>
                            <b>Hirer</b>
                        </td>
                        <td>
                            <b>Email</b>
                        </td>
                    </tr>
                    <tr class = "evenRow">
                        
                        <td>
                                <asp:Label ID="bookingIdLabel" runat="server" Width="100"></asp:Label>
                        </td>
                       
                        <td>
                                <asp:Label ID="HirerLabel" runat="server" Width="150"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="EmailLabel" runat="server" Width="150"></asp:Label>
                        </td>
                    </tr>
                </table>
               
                <h2> 1. Call / Contact</h2>
                <table>
                    <tr>
                        <td style="width: 100px;">
                            Status:
                        </td>
                        <td>
                            <asp:DropDownList ID="StatusDropDown" runat="server" Width="165" AutoPostBack="true" />
                        </td>
                        <td width="40px">
                        </td>
                        <td id="tdoutboundcall" runat="server">
                            Outbound Call?
                        </td>
                        <td id="tdtimeDate" runat="server">
                            <dt:DateControl ID="outboundDateControl" runat="server" Nullable="true" />
                            &nbsp;
                            <tm:TimeControl ID="outboundTimeControl" runat="server" />
                            &nbsp; (NZ Time)
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px;" />
                        <td />
                        <td width="40px" />
                        <td id="tdphonelabel" runat="server">
                            Phone Number:
                        </td>
                        <td id="tdphone" runat="server">
                            <asp:TextBox runat="server" ID="phonenumbertext" MaxLength="20" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="height: 10px;">
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="5">
                            Notes
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:TextBox TextMode="MultiLine" runat="server" ID="notesTextBox" Width="100%" Height="30"
                                MaxLength="2000"></asp:TextBox>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td style="height: 10px;">
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="5">
                            History
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:GridView ID="gridviewHistory" runat="server" CssClass="dataTableGrid" AutoGenerateColumns="false">
                                <EmptyDataTemplate>
                                    <table style="width: 100%" cellpadding="0" cellspacing="0" class="dataTableGrid">
                                        <thead>
                                            <tr class="oddRow">
                                                <td>
                                                    <b>Date/Time </b>
                                                </td>
                                                <td>
                                                    <b>Status </b>
                                                </td>
                                                <td>
                                                    <b>Call Back Date? </b>
                                                </td>
                                                <td>
                                                    <b>Notes </b>
                                                </td>
                                                <td>
                                                    <b>Agent </b>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tr class="evenRow">
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField HeaderText="Date/Time" DataField="AddDateTime" DataFormatString="{0:dd/MM/yyyy hh:mm}"
                                        ItemStyle-Width="90" />
                                    <asp:BoundField HeaderText="Status" DataField="ExpCallStatusDesc" ItemStyle-Width="100" />
                                    <asp:BoundField HeaderText="CallBack Date?" DataField="ExpCallOutboundCallDt" DataFormatString="{0:dd/MM/yyyy hh:mm}"
                                        ItemStyle-Width="90" />
                                    <asp:BoundField HeaderText="Notes" DataField="ExpCallNotes" ItemStyle-Width="200" />
                                    <asp:BoundField HeaderText="Agent" DataField="AddUsrId" ItemStyle-Width="40" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                   <%-- <tr>
                        <td style="height: 15px;">
                            &nbsp;
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="5" align="right">
                          <asp:Button ID="BackButtonTop" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                                Width="100" />
                                &nbsp;
                            <asp:Button ID="CallContactButtonSave" runat="server" Text="Save" CssClass="Button_Standard Button_Add" width="100"/>
                                &nbsp;
                             <asp:Button ID="CallContactButtonSaveAndContinue" runat="server" Text="Save & Continue" CssClass="Button_Standard Button_Add" width="150"/>
                        </td>
                    </tr>
                </table>
                
                <h2> 2. Purchase Experience</h2>
                <table cellspacing="15px" cellpadding="1px">
                    <tr>
                        <td colspan="5">
                            <asp:GridView ID="addExperiencegridview" runat="server" CssClass="dataTableGrid"
                                AutoGenerateColumns="false">
                                <EmptyDataTemplate>
                                    <table style="width: 100%" cellpadding="0" cellspacing="0" class="dataTableGrid">
                                        <thead>
                                            <tr class="oddRow">
                                                <td>
                                                    <b>Date Added</b>
                                                </td>
                                                <td>
                                                    <b>Agent</b>
                                                </td>
                                                <td>
                                                    <b>Supplier</b>
                                                </td>
                                                <td>
                                                    <b>Experiences</b>
                                                </td>
                                                <td>
                                                    <b>Ticket</b>
                                                </td>
                                                <td>
                                                    <b>Price</b>
                                                </td>
                                                <td>
                                                    <b>Total</b>
                                                </td>
                                                <td>
                                                    <b>Ticket Date</b>
                                                </td>
                                                <td>
                                                    <b>Status</b>
                                                </td>
                                                <td>
                                                    <b>Voucher</b>
                                                </td>
                                            </tr>
                                        </thead>
                                        <tr class="evenRow">
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                            <td />
                                        </tr>
                                    </table>
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:BoundField DataField="AddDateTime" HeaderText="Date Added" DataFormatString="{0:d/MM/yyyy hh:mm}" />
                                    <asp:BoundField DataField="AddUsrId" HeaderText="Agent" />
                                    <asp:BoundField DataField="Supplier" HeaderText="Supplier" />
                                    <asp:BoundField DataField="Experience" HeaderText="Experience" />
                                    <asp:BoundField DataField="Ticket" HeaderText="Ticket" />
                                    <asp:BoundField DataField="Price" HeaderText="Price" DataFormatString="{0:c2}" />
                                    <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:c2}" />
                                    <asp:BoundField DataField="TicketDate" HeaderText="Ticket Date" DataFormatString="{0:d/MM/yyyy hh:mm}" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" />
                                    <asp:BoundField DataField="Voucher" HeaderText="Voucher" />
                                    <asp:ButtonField ButtonType="Button" CommandName="Select" ControlStyle-CssClass="Button_Standard"
                                        ControlStyle-Width="70" Text="Edit/View" />
                                    <asp:TemplateField >
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hiddenId" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <h3>
                                Add Experience</h3>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 75px">
                            Supplier:
                        </td>
                        <td colspan="1">
                            <asp:DropDownList ID="SupplierDropDown" runat="server" Width="350" AutoPostBack="true" />* 
                        </td>
                        <td />
                        <td />
                        
                    </tr>
                    <tr>
                        <td>
                            Tag:
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="CategoryDropDownList" runat="server" Width="350" AutoPostBack="true"  />*
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Experience:
                        </td>
                        <td colspan="5">
                            <asp:DropDownList ID="ExperienceDropDown" runat="server" Width="350" AutoPostBack="true"  />*
                        </td>
                    </tr>
                    <tr>
                        <td />
                        <td colspan="5">
                            <asp:TextBox TextMode="MultiLine" runat="server" ID="ExperienceDetailsTextBox" Height="100"
                                Width="100%" ReadOnly="true"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ticket:
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="TicketDropDown" runat="server" Width="350" AutoPostBack="true" />*
                        </td>
                       
                    </tr>
                   <%-- <tr>
                        
                    </tr>--%>
                    <tr>
                     <td>
                            Price:
                        </td>
                        <td>
                            <asp:TextBox ID="priceTextBox" runat="server" Width="160" AutoPostBack="true"></asp:TextBox>*
                        </td>
                        <td>
                            Quantity:
                        </td>
                        <td>
                            <asp:DropDownList ID="QuantiytDropDown" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="6">6</asp:ListItem>
                                <asp:ListItem Value="7">7</asp:ListItem>
                                <asp:ListItem Value="8">8</asp:ListItem>
                                <asp:ListItem Value="9">9</asp:ListItem>
                                <asp:ListItem Value="10">10</asp:ListItem>
                                <asp:ListItem Value="11">11</asp:ListItem>
                                <asp:ListItem Value="12">12</asp:ListItem>
                                <asp:ListItem Value="13">13</asp:ListItem>
                                <asp:ListItem Value="14">14</asp:ListItem>
                                <asp:ListItem Value="15">15</asp:ListItem>
                                <asp:ListItem Value="16">16</asp:ListItem>
                                <asp:ListItem Value="17">17</asp:ListItem>
                                <asp:ListItem Value="18">18</asp:ListItem>
                                <asp:ListItem Value="19">19</asp:ListItem>
                                <asp:ListItem Value="20">20</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="trExperienceDateTime" runat="server">
                        <td>
                            Total:
                        </td>
                        <td>
                            <asp:TextBox ID="TotalTextBox" runat="server" ReadOnly="true" Width="158"></asp:TextBox>
                        </td>
                        <td id="tdExperienceDateTimeLabel" runat="server">
                            Date:
                        </td>
                        <td id="tdExperienceDateTime" runat="server">
                            <dt:DateControl ID="dateDateControl" runat="server" Nullable="true" />
                            &nbsp;
                            <tm:TimeControl ID="dateTimeControl" runat="server" />*
                        </td>
                    </tr>
                    
                    <tr>
                        <td>
                            Status:
                        </td>
                        <td>
                            <asp:DropDownList ID="StatusExDropDown" runat="server" Width="165" />
                        </td>
                    </tr>

                    <tr>
                        <td>
                            Supplier Ref:
                        </td>
                        <td>
                            <asp:TextBox ID="SupplierRefTextBox" runat="server"  Width="158"></asp:TextBox>
                        </td>
                    </tr>
                                        
                    <tr>
                        <td colspan="5">
                            <h3>
                                Extra Information For Supplier</h3>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:TextBox TextMode="MultiLine" runat="server" ID="ExtraTextBox" Height="30" Width="100%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right">
                            <asp:Button ID="BackButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back"
                                Width="100" />
                                &nbsp;
                            <asp:Button ID="CancelButton" runat="server" Text="Cancel" CssClass="Button_Standard Button_Cancel"
                                Width="100" />
                            &nbsp;
                            <asp:Button ID="AddExperienceButton" runat="server" Text="Add Experience" CssClass="Button_Standard Button_Add"
                                Width="150" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:HiddenField ID="HiddenVoucher" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
