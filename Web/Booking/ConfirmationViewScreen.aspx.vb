﻿'' Change Log!
'' 22.10.8 / Shoel - Fixes for Rental No not being passed
'' 06.11.8 / Shoel - Added HTML Encode/Decode for Header & Footer Notes

''change control
''REV:MIA MAY 16 2011 -- addition of EmailAddress
''REV:MIA MAY  2011   -- saving of Confirmation in the database
''                    -- ConfirmationHtmlText                 - table
''                    -- cfn_update_reSend_TextConfirmation   - SP
''                    -- Confirmation_GetHTMLContent          - SP
''                    -- Confirmation_InsUpdHTML              - SP


Imports System.Xml
Imports System.IO
Imports System.Xml.Xsl
Imports System.Xml.XPath
Imports Aurora
Imports System.Data
Imports Aurora.Common.Data
Imports Aurora.Common
''Imports Aurora.Booking.Services
Imports Aurora.BookingExperiences.Service
Imports WS.Helper
Imports WS.Utility

<AuroraPageTitleAttribute("Confirmation")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
Partial Class Booking_ConfirmationViewScreen
    Inherits AuroraPage

    Private showAllRentals As Boolean
    Private confirmationId As String
    Private confirmationUserId As String
    Private calledFrom As String
    Private bookingId As String
    Private rentalId As String
    Private rentalNo As String
    Private audienceType As String
    ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
    Private excludeRentalNumberFromConfirmation As String = ""
    Private confirmationIds As String ''handling multiple confirmation ids

    Private confirmationXml As New XmlDocument
    Private confirmationHtml As String
    Private Const CUSTOMER_ID As String = "7519EC4A-2601-4855-B703-CDD6DD22B108"
    Private Const AGENT_ID As String = "34CE97B1-5807-41C5-B043-5B10D43685C7"
    Private Const EOF_FLAG As String = "~EOF~"
    Private audienceParam As String = ""
    Private BrandFooterText As String = ""

    Const SESSION_HIDDENREFRESHBUTTONID As String = "Session_Confirmation_HiddenRefreshButtonClientId"

    Const HTML_HEADER_STATIC As String = "<HTML>" & _
                                            "<HEAD>" & _
                                                "<style>" & _
                                                    "H2{page-break-after: always;}" & _
                                                    "TD{FONT-SIZE: 8pt;FONT-FAMILY:Courier New;}" & _
                                                    "A{FONT-SIZE: 8pt;FONT-FAMILY:Courier New;}" & _
                                                "</style>" & _
                                            "</HEAD>" & _
                                            "<BODY>"

    Const HTML_FOOTER_STATIC As String = "</BODY></HTML>"

    'Private _brandFooterText As String
    'Private Property BrandFooterText As String
    '    Get
    '        Return _brandFooterText
    '    End Get
    '    Set(value As String)
    '        Select Case value.ToUpper
    '            Case "B"
    '                _brandFooterText = "Britz"
    '            Case "Y"
    '                _brandFooterText = "Mighty"
    '            Case "M"
    '                _brandFooterText = "Maui"
    '            Case "U"
    '                _brandFooterText = "United"
    '            Case "A"
    '                _brandFooterText = "Alpha"
    '            Case "Q"
    '                _brandFooterText = "Kea"
    '            Case Else
    '                _brandFooterText = "THL"
    '        End Select
    '        '_brandFooterText = value
    '    End Set
    'End Property



    Private Sub BuildForm()
        Dim sCnfDetails As String = CStr(Request.QueryString("CnfData"))

        If Not String.IsNullOrEmpty(sCnfDetails) Then
            showAllRentals = CBool(sCnfDetails.Split("$")(0))

            confirmationIds = sCnfDetails.Split("$")(1)
            If (confirmationIds.Contains(",") = True) Then
                confirmationId = confirmationIds.Split(",")(0)
            Else
                confirmationId = sCnfDetails.Split("$")(1)
            End If


            confirmationUserId = sCnfDetails.Split("$")(2)
            calledFrom = sCnfDetails.Split("$")(3)
            bookingId = sCnfDetails.Split("$")(4)
            rentalId = sCnfDetails.Split("$")(5)
            rentalNo = sCnfDetails.Split("$")(6)
            audienceType = sCnfDetails.Split("$")(7)

            If (sCnfDetails.Split("$").Length - 1 = 8) Then
                excludeRentalNumberFromConfirmation = IIf(sCnfDetails.Split("$")(8) = "none", "", sCnfDetails.Split("$")(8))
            Else
                excludeRentalNumberFromConfirmation = ""
            End If


            ''REV:MIA MAY  2011   -- saving of Confirmation in the database
            ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
            ''https://thlonline.atlassian.net/browse/AURORA-777 - As QA I want voucher from Aurora and Experiences catalogue should be exactly same to simplify testing and also re-use of code. 
            confirmationHtml = TransformedDocuments(confirmationId, showAllRentals, excludeRentalNumberFromConfirmation, bookingId, rentalId, UserCode, CUSTOMER_ID) ''.Replace("<body style=""background: #f2f2f2;", "<body style=""background: #ffffff;")

        Else
            confirmationId = Request.QueryString("ConfirmationId")
            bookingId = Request.QueryString("BookingId")
            calledFrom = Request.QueryString("CalledFrom")
            audienceParam = Request.QueryString("Audience")

            ''rev:mia OCT 16 2013  - MODIFIED: this will allow confirmation email to be process by adding multiple checkboxes.
            excludeRentalNumberFromConfirmation = Request.QueryString("ExcludedRntl")

            If Not String.IsNullOrEmpty(Request.QueryString("BookingNum")) AndAlso Request.QueryString("BookingNum").ToUpper().Equals("ALL") Then
                showAllRentals = True
            Else
                showAllRentals = False
            End If

            Dim sConfPath As String


            'already will have the XML
            Dim xmlPathsInfo As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GetPathsForConfirmation(confirmationId)
            Dim xmlMain As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(bookingId, confirmationId)

            Dim xmlTemp As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(confirmationId, showAllRentals)

            confirmationXml.LoadXml(xmlTemp.DocumentElement.InnerXml)

            rentalId = xmlMain.DocumentElement.SelectSingleNode("Confirmations/rentId").InnerText

            Try
                ''REV:MIA 25-JAN-2016 - as a Finance user I want to see invoice details on all Experience vouchers
                _GSTCountry = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/GSTCountry").InnerText
            Catch ex As Exception
            End Try


            sConfPath = xmlPathsInfo.DocumentElement.SelectSingleNode("eFaxPart/CnfPath").InnerText & "\"
            Dim Confirmationhtmltext As String = GetConfirmationhtml(confirmationId) ''.Replace("<body style=""background: #f2f2f2;", "<body style=""background: #ffffff;")
            ''Nyt Nov 2015


            ''REV:MIA MAY  2011   -- saving of Confirmation in the database
            If Not String.IsNullOrEmpty(Confirmationhtmltext) AndAlso Confirmationhtmltext <> "Error" Then
                confirmationHtml = Confirmationhtmltext
                LogWarning("Confirmationhtmltext coming from db for " & confirmationId & " found")
            Else
                confirmationHtml = Aurora.Booking.Services.BookingConfirmation.ReadSavedHTMLConfirmation(confirmationId, sConfPath)
                If String.IsNullOrEmpty(confirmationHtml) Then
                    LogWarning("Confirmationhtmltext coming from flatfile for " & confirmationId & " and path " & sConfPath & " not found")
                End If

            End If

            If String.IsNullOrEmpty(confirmationHtml.Trim) Then
                Dim sOriginalFileName As String = Aurora.Booking.Services.BookingConfirmation.GetPreviouslySavedFileName(confirmationId)
                sOriginalFileName = sOriginalFileName.Split("\"c)(sOriginalFileName.Split("\"c).Length - 1).Split("."c)(0)
                confirmationHtml = Aurora.Booking.Services.BookingConfirmation.ReadSavedHTMLConfirmation(sOriginalFileName, sConfPath)

                ''REV:MIA MAY  2011   -- saving of Confirmation in the database
                ''first attempt to get old confirmation but no avail
                If String.IsNullOrEmpty(confirmationHtml.Trim) Then

                    ''so try to regenerate confirmation 

                    LogWarning("Cannot find a previous record of a confirmationID  " & confirmationId & ". Will regenerate one")
                    ''rev:mia sept 4 2013  - this will allow confirmation email to be process by adding multiple checkboxes.
                    ''https://thlonline.atlassian.net/browse/AURORA-777 - As QA I want voucher from Aurora and Experiences catalogue should be exactly same to simplify testing and also re-use of code. 
                    confirmationHtml = TransformedDocuments(confirmationId, showAllRentals, excludeRentalNumberFromConfirmation, bookingId, rentalId, UserCode, CUSTOMER_ID)

                    ''last attempt...failed...record and throw the exceptions
                    If String.IsNullOrEmpty(confirmationHtml.Trim) Then
                        LogWarning("failed attempt to regenerate  " & confirmationId)
                        Throw New Exception("Error loading confirmation")
                    End If
                End If
            End If
        End If

        ''REV:MIA JULY 13  2011   -- FIXED PROBLEM ABOUT NOT SENDING EMAIL FOR OLD BOOKINGS
        Dim vouchers As String = ""
        If Not String.IsNullOrEmpty(confirmationHtml) AndAlso Not String.IsNullOrEmpty(confirmationId) Then
            InsertConfirmationHTML(confirmationId, confirmationHtml, UserCode, "")
        End If


        'If (audienceType = CUSTOMER_ID) Then ''customer
        '    vouchers = BuildVouchers(bookingId, rentalId, UserCode)
        '    confirmationXmlLiteral.Text = confirmationHtml + vouchers
        '    confirmationHtml = confirmationHtml + vouchers
        '    confirmationXmlLiteral.Text = confirmationHtml
        '    ''rev:mia 15-dec-2015 https://thlonline.atlassian.net/browse/PHOENIXWS-169
        '    ''Experiences Catalogue booking not going to confirmation page but passing the payment and ticket to Aurora
        'ElseIf (audienceType = AGENT_ID) Then
        '    confirmationXmlLiteral.Text = confirmationHtml
        'Else
        '    vouchers = BuildVouchers(bookingId, rentalId, UserCode)
        '    confirmationXmlLiteral.Text = confirmationHtml + vouchers
        '    confirmationHtml = confirmationHtml + vouchers
        'End If
        confirmationXmlLiteral.Text = StrippedEOFMarker(confirmationHtml, audienceType)
        btnSend.Attributes.Add("onClick", "return PrintIfNeeded('" & radPrint.ClientID & "');")
    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If IsPostBack Then Return

        Try
            BuildForm()
            GetConfirmationEmail(bookingId, rentalId)
        Catch ex As Exception
            Me.SetErrorMessage("Error loading confirmation")
            pnlParams.Enabled = False
            Return
        End Try
    End Sub

    Private Sub ValidateParams()
        If radEmail.Checked Then
            Dim oRegex As Regex = New Regex("^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$")
            If txtEmailAddress.Text.IndexOf(";") > -1 Then 'has ;
                Dim saEmailAddresses As String() = txtEmailAddress.Text.Split(";")
                For Each sEmailAddress As String In saEmailAddresses
                    If Not oRegex.IsMatch(sEmailAddress) Then
                        ScriptManager.GetCurrent(Page).SetFocus(txtEmailAddress)
                        Throw New Aurora.Common.ValidationException("Please enter valid email addresses delimited by semi-colons")
                    End If
                Next
            Else
                ' just 1 email address
                If Not oRegex.IsMatch(txtEmailAddress.Text) Then
                    ScriptManager.GetCurrent(Page).SetFocus(txtEmailAddress)
                    Throw New Aurora.Common.ValidationException("Please enter a valid email addresse")
                End If
            End If
        End If


        Dim oRegexFax As Regex = New Regex("^\d+$")
        If radFax.Checked AndAlso Not oRegexFax.IsMatch(txtFaxNumber.Text) Then
            ScriptManager.GetCurrent(Page).SetFocus(txtFaxNumber)
            Throw New Aurora.Common.ValidationException("Enter a valid Fax Number")
        End If

        'If radFax.Checked AndAlso Not txtFaxNumber.IsTextValid Then
        '    ScriptManager.GetCurrent(Page).SetFocus(txtFaxNumber)
        '    Throw New Aurora.Common.ValidationException(txtFaxNumber.ErrorMessage)
        'End If
    End Sub

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Try
            BuildForm()
        Catch ex As Exception
            Me.SetErrorMessage("Error loading confirmation")
            pnlParams.Enabled = False
            Return
        End Try

        Try
            ValidateParams()
        Catch ex As Aurora.Common.ValidationException
            LogException(ex)
            Me.SetWarningShortMessage(ex.Message)
            Return
        End Try

        Try
            Dim xmlConfirmationInfo As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GetDataForConfirmation(bookingId, confirmationId)

            Dim sHeaderText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/headnote").InnerText
            Dim sFooterText As String = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/footnote").InnerText
            Dim bHighlight As Boolean = CBool(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/highlight").InnerText)
            Dim nIntegrityNo As Integer = CInt(xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/integrityno").InnerText)

            ''rev:mia April 3, 2013 - addition of BCC as requested by Rajesh.
            ''sp change is cfn_getRentalsFromBooking
            Dim bccEmail As String = String.Empty
            Try
                bccEmail = xmlConfirmationInfo.DocumentElement.SelectSingleNode("Confirmations/BCCEmail").InnerText
            Catch ex As Exception
                LogInformation("BCC Email is empty")
            End Try


            Dim sPath As String = confirmationXml.DocumentElement.SelectSingleNode("CnfPath").InnerText & "\"
            Dim sCompany As String = confirmationXml.DocumentElement.SelectSingleNode("Company").InnerText
            Dim sAttention As String = confirmationXml.DocumentElement.SelectSingleNode("Contact").InnerText
            Dim sEFaxPath As String = confirmationXml.DocumentElement.SelectSingleNode("EFaxPath").InnerText
            Dim sReference As String = confirmationXml.DocumentElement.SelectSingleNode("Reference").InnerText
            Dim sEFaxNotify As String = confirmationXml.DocumentElement.SelectSingleNode("EFaxNotify").InnerText
            Dim sFaxBackup As String = confirmationXml.DocumentElement.SelectSingleNode("EfaxBackup").InnerText

            Dim sCustSurname As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/CustomerSurname").InnerText
            Dim sBookingRef As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/OurRef").InnerText
            Dim sEmailSubject As String = sCustSurname & " " & sBookingRef & " " & confirmationXml.DocumentElement.SelectSingleNode("doc/Header/ConfirmationTitle").InnerText

















            Dim brandLogo As String = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/brandlogo").InnerText
            Dim slashlastindex As Integer = brandLogo.LastIndexOf("/")
            If (slashlastindex <> -1) Then
                If (brandLogo.Length > slashlastindex + 1) Then
                    brandLogo = brandLogo.Substring(slashlastindex + 1).Trim
                Else
                    brandLogo = brandLogo.Substring(slashlastindex).Trim
                End If

                brandLogo = brandLogo.Substring(0, brandLogo.LastIndexOf(".")).Trim
                brandLogo = IIf(brandLogo.IndexOf("MAC") <> -1, "MOTORHOMESANDCARS.COM", brandLogo)
            End If
            sEmailSubject = String.Concat(brandLogo, " -  ", sEmailSubject)


            Dim Confirmationhtmltext As String = String.Empty
            Dim skipSendingFromFile As Boolean = True


            Try


                ''REV:MIA MAY  2011   -- saving of Confirmation in the database
                Confirmationhtmltext = GetConfirmationhtml(confirmationId)

                ''no record in the ConfirmationHtmlText..marked this for saving so use the filesystem data
                If (String.IsNullOrEmpty(Confirmationhtmltext) And Confirmationhtmltext <> "Error") Then


                    skipSendingFromFile = False
                Else
                    If (Confirmationhtmltext = "Error") Then Throw New Exception("Error encountered when calling GetConfirmationHtml")
                    skipSendingFromFile = True
                End If


            Catch ex As Exception
                ''REV:MIA MAY  2011   -- saving of Confirmation in the database
                LogError("Error found when calling GetConfirmationID: ConfirmationID: " & confirmationId)
                ''error happened,,,use the old function for sending
                Aurora.Booking.Services.BookingConfirmation.CreateTextFile(sPath & confirmationId, confirmationXml, "txt")
                Aurora.Booking.Services.BookingConfirmation.SaveFile(sPath, confirmationId, StrippedEOFMarker(confirmationHtml, audienceType), "htm")
                skipSendingFromFile = False
            End Try




            Dim sSavedFileName As String = sPath & confirmationId & ".htm"

            Dim sEmailId, sFaxNumber, sSentBy As String
            ''https://thlonline.atlassian.net/browse/AURORA-777
            Dim localHtmlContent As String = confirmationHtml
            confirmationHtml = StrippedEOFMarker(confirmationHtml, audienceType)

            If radEmail.Checked Then

                'email
                sEmailId = txtEmailAddress.Text
                sFaxNumber = ""
                sSentBy = "Email"

                Try

                    ''REV:MIA MAY  2011   -- saving of Confirmation in the database
                    ''érror happened in when calling and inserting the new confirmation
                    If skipSendingFromFile = False Then
                        LogInformation("(OLD) Sending PDF Email using  confirmationid " & confirmationId)
                        Aurora.Booking.Services.BookingConfirmation.EMailPdf( _
                        sPath, _
                        confirmationId, _
                        sEmailId, _
                        sEFaxNotify, _
                        sEmailSubject)

                    Else
                        LogInformation("(NEW) Sending PDF Email using  confirmationid " & confirmationId)
                        ''rev:mia April 3, 2013 - addition of BCC as requested by Rajesh.
                        ''sp change is cfn_getRentalsFromBooking
                        'Aurora.Booking.Services.BookingConfirmation.EMailDataPDF(confirmationHtml, _
                        '             sEmailId, _
                        '             sEFaxNotify, _
                        '             sEmailSubject, _
                        '             bccEmail)
                        WebServiceHelper.EMailConfirmation(confirmationHtml, _
                                         sEmailId, _
                                         sEFaxNotify, _
                                         sEmailSubject, _
                                         bccEmail)
                        ''rev:mia 08-march-2016 https://thlonline.atlassian.net/browse/AURORA-777
                        confirmationHtml = localHtmlContent
                    End If

                Catch ex As Exception
                    LogInformation("ConfirmationID: " & confirmationId & ". Exception was thrown when sending of email.Check the LogError folder")
                    LogError("Sending PDF Email using  confirmationid " & confirmationId)
                    Aurora.Booking.Services.BookingConfirmation.EMailPdf( _
                    sPath, _
                    confirmationId, _
                    sEmailId, _
                    sEFaxNotify, _
                    sEmailSubject)
                End Try



            ElseIf radFax.Checked Then
                'fax
                sEmailId = ""
                sFaxNumber = txtFaxNumber.Text
                sSentBy = "Fax"

                Aurora.Booking.Services.BookingConfirmation.FaxPdf( _
                    sFaxNumber, _
                    sAttention, _
                    sCompany, _
                    sReference, _
                    sEFaxNotify, _
                    sEFaxPath, _
                    sPath, _
                    confirmationId, _
                    sFaxBackup) 'Then

            Else
                'print
                sEmailId = ""
                sFaxNumber = ""
                sSentBy = "Printing"
            End If

            'update the database abt the way the thing was handled
            'exec cfn_update_confirmations '07C40166-2A07-4D3F-B2E9-3D284C6E212A', '6353412', '6353412-1', '1', '34CE97B1-5807-41C5-B043-5B10D43685C7', 'test header', '', 'test@test.com', '',  'Email', 'Confirmations\07C40166-2A07-4D3F-B2E9-3D284C6E212A.htm', 0, 2, 'sp7', 'sp7', 'confirmationlistener.asp', 'U'
            Dim oRetXml As XmlDocument
            If calledFrom = "ConfirmationManagement.aspx" Then

                ''IIf(rentalNo.ToUpper().Trim().Equals("ALL"), rentalNo, rentalId), _
                If rentalNo.ToUpper().Trim().Equals("ALL") = True Then
                    oRetXml = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                    confirmationId, _
                    bookingId, _
                    rentalNo, _
                    rentalNo, _
                    audienceType, _
                    sHeaderText, _
                    sFooterText, _
                    sEmailId, _
                    sFaxNumber, _
                    sSentBy, _
                    sSavedFileName, _
                    bHighlight, _
                    nIntegrityNo, _
                    Me.UserCode, _
                    Me.UserCode, _
                    Me.PrgmName, _
                    "U", _
                    Nothing, _
                    "", _
                    "")
                Else

                    Dim confirmIds As String() = confirmationIds.Split(",")
                    Dim ctrId As Integer = 0
                    If (rentalNo.LastIndexOf(",") <> -1) Then
                        rentalNo = rentalNo.Remove(rentalNo.LastIndexOf(","))
                    End If

                    For Each singlerental As String In rentalNo.Split(",")
                        oRetXml = Aurora.Booking.Services.BookingConfirmation.SaveUpdateConfirmation( _
                            confirmIds(ctrId), _
                            bookingId, _
                            bookingId & "-" & singlerental, _
                            rentalNo, _
                            audienceType, _
                            sHeaderText, _
                            sFooterText, _
                            sEmailId, _
                            sFaxNumber, _
                            sSentBy, _
                            sSavedFileName, _
                            bHighlight, _
                            nIntegrityNo, _
                            Me.UserCode, _
                            Me.UserCode, _
                            Me.PrgmName, _
                            "U", _
                            Nothing, _
                            "", _
                            "")
                        ctrId = ctrId + 1
                    Next
                End If



                ' look for error
                If oRetXml IsNot Nothing Then
                    If oRetXml.SelectSingleNode("Root/Error") IsNot Nothing Then
                        ' error 
                        SetErrorMessage(oRetXml.SelectSingleNode("Root/Error/Message").InnerText)
                    Else

                        ''REV:MIA MAY  2011   -- saving of Confirmation in the database
                        InsertConfirmationHTML(confirmationId, confirmationHtml, UserCode, "")

                        'need to change parent window to refreshed booking tab...ie load booking.aspx with confirmation tab selected + reloaded
                        ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "RefreshBookingTab('" & calledFrom & "','','" & bookingId & "','" & rentalId & "');", True)
                    End If
                End If

            ElseIf calledFrom = "BookingConfirmationUserControl.ascx" Then

                ''REV:MIA MAY  2011   -- saving of Confirmation in the database
                ''COMMENTED THIS PART
                'Aurora.Booking.Services.BookingConfirmation.UpdateResendConfirmation( _
                '    confirmationId, _
                '    sEmailId, _
                '    sFaxNumber, _
                '    sSentBy, _
                '    Me.UserCode, _
                '    Me.PrgmName, _
                '    "")
                Dim newID As String = ResendAndGetNewID(confirmationId, sEmailId, sFaxNumber, sSentBy, Me.UserCode, Me.PrgmName, "")
                If Not String.IsNullOrEmpty(newID) Then
                    InsertConfirmationHTML(newID, confirmationHtml, UserCode, confirmationId)
                End If

                'close and redirect to parent page?
                ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "RefreshBookingTab('" & calledFrom & "','','" & bookingId & "','" & IIf(rentalId.Trim().Equals(String.Empty), Request.QueryString("RentalId"), rentalId) & "');", True)
            End If

        Catch ex As Exception
            Me.SetErrorMessage(ex.Message)
            LogException(ex)
        End Try

    End Sub

#Region "REV:MIA MAY 16 2011 -- addition of EmailAddress"

    Sub GetConfirmationEmail(ByVal bookingid As String, ByVal rentalid As String)

        'LogDebug("GetConfirmationEmail for booking " & bookingid & " is available only on LIVE Environment and not " & Request.ServerVariables("HTTP_HOST").ToLower)
        'If Request.ServerVariables("HTTP_HOST").ToLower.Contains("localhost") = True Then
        '    If (UserCode = "ma2") Then
        '        txtEmailAddress.Text = "manuel.agbayani@thlonline.com"
        '    Else
        '        txtEmailAddress.Text = ""
        '    End If

        '    Exit Sub
        'End If

        'If Request.ServerVariables("HTTP_HOST").ToLower.Contains("pap-app") = True Then
        '    If (UserCode = "ma2") Then
        '        txtEmailAddress.Text = "manuel.agbayani@thlonline.com"
        '    Else
        '        txtEmailAddress.Text = ""
        '    End If
        '    Exit Sub
        'End If



        If String.IsNullOrEmpty(bookingid) Then Exit Sub
        If String.IsNullOrEmpty(rentalid) Then Exit Sub

        Dim autoemail As String = ""

        Try



            Dim CnfData As String = Request.QueryString("CnfData")
            Dim CnfDataArray As String() = CnfData.Split("$")
            Dim audiencetype As String = ""

            ''rev:mia Nov. 16 added fix to allow email to be populate for customer only.
            Const AGENTID As String = "34CE97B1-5807-41C5-B043-5B10D43685C7"
            Const CUSTOMERID As String = "7519EC4A-2601-4855-B703-CDD6DD22B108"


            If ((CnfDataArray.Length - 1 <> -1) And (CnfDataArray.Length - 1 = 7)) Then
                audiencetype = CnfDataArray(CnfDataArray.Length - 1)
            Else
                If ((CnfDataArray.Length - 1 <> -1) And (CnfDataArray.Length - 1 = 8)) Then
                    audiencetype = CnfDataArray(CnfDataArray.Length - 2)
                End If
            End If

            If (audiencetype = CUSTOMERID) Then
                LogDebug("Calling GetConfirmationEmail: " & bookingid & vbCrLf & ", rentalid: " & rentalid)
                autoemail = Aurora.Common.Data.ExecuteScalarSP("GetConfirmationEmail", bookingid.Trim, rentalid.Trim)
                txtEmailAddress.Text = autoemail
                LogDebug("Calling GetConfirmationEmail succesfull: " & bookingid & vbCrLf & ", rentalid: " & rentalid)
            Else
                If (audiencetype = AGENTID) Then
                    autoemail = ""

                End If
                txtEmailAddress.Text = autoemail
            End If


        Catch ex As Exception
            LogError("GetConfirmationEmail Error: " & ex.Message & " , BookingID: " & bookingid & ", rentalid: " & rentalid)
            txtEmailAddress.Text = autoemail
        End Try


    End Sub


#End Region

#Region "REV:MIA MAY  2011   -- saving of Confirmation in the database"

    Sub InsertConfirmationHTML(ByVal confirmationid As String, _
                               ByVal confirmationtext As String, _
                               ByVal userid As String, _
                               ByVal newconfirmationid As String)
        ''Aurora.Common.ConfirmationExtension.InsertConfirmationHTML(confirmationid, confirmationtext, userid, newconfirmationid)
        Aurora.Confirmation.Services.Confirmation.InsertConfirmationHTML(confirmationid, confirmationtext, userid, newconfirmationid)
    End Sub


    Function GetConfirmationhtml(ByVal confirmationid As String) As String
        If String.IsNullOrEmpty(confirmationid) Then Return False
        Try
            LogDebug("Calling GetConfirmationhtml: " & confirmationid)
            Dim confirmationresult As String = Aurora.Common.Data.ExecuteScalarSP("Confirmation_GetHTMLContent", confirmationid)
            LogDebug("Calling GetConfirmationhtml successfull: " & confirmationid)
            Return confirmationresult
        Catch ex As Exception
            LogError("GetConfirmationhtml Error: " & ex.Message & " , ConfirmationID: " & confirmationid)
            Return "Error"
        End Try

    End Function


    'Private Sub EMailDataPDF(ByVal sCfnText As String, _
    '                                ByVal sEmailAddress As String, _
    '                                ByVal EFaxNotify As String, _
    '                                ByVal emailSubject As String, _
    '                                Optional BCC As String = "")

    '    Dim sText As String = sCfnText

    '    Try

    '        If sText <> "" Then
    '            sText = Replace(sText, "../Images/", "")

    '            Dim emailArray As Array
    '            Dim emailImg As String
    '            emailArray = Split(emailSubject, " - ", -1, 1)
    '            emailImg = Trim(emailArray(0))

    '            ''rev:mia May 15 2012 - dynamic images in confirmation
    '            'If emailImg = "BACKPACKER" Then
    '            '    sText = Replace(sText, "Backpackerlogo.gif", CStr(ConfigurationManager.AppSettings("BackpackerLogoURL")))
    '            'ElseIf emailImg = "MAUI" Then
    '            '    sText = Replace(sText, "Mauilogosmall.gif", CStr(ConfigurationManager.AppSettings("MauiLogoURL")))
    '            'ElseIf emailImg = "BRITZ" Then
    '            '    sText = Replace(sText, "Britz.gif", CStr(ConfigurationManager.AppSettings("BritzLogoURL")))
    '            'ElseIf emailImg = "EXPLORE MORE" Then
    '            '    sText = Replace(sText, "ExploreMore-logo.gif", CStr(ConfigurationManager.AppSettings("ExploreMoreLogoURL")))
    '            'ElseIf emailImg = "MOTORHOMESANDCARS.COM" Then
    '            '    sText = Replace(sText, "MACLOGO.gif", CStr(ConfigurationManager.AppSettings("MACLogoURL")))
    '            'End If
    '            sText = Aurora.Booking.Services.BookingConfirmation.GetBrandLogoImage(emailImg, sText)
    '        End If

    '        If sText <> "" Then
    '            Dim strBody As String = ""

    '            Dim oMailMsg As System.Web.Mail.MailMessage = New System.Web.Mail.MailMessage
    '            oMailMsg.From = EFaxNotify
    '            oMailMsg.To = sEmailAddress
    '            If (Not String.IsNullOrEmpty(BCC)) Then
    '                oMailMsg.Bcc = BCC
    '            End If

    '            ' fix for weird mapping problem
    '            oMailMsg.Headers.Add("Reply-To", EFaxNotify)
    '            ' fix for weird mapping problem
    '            oMailMsg.BodyEncoding = System.Text.Encoding.ASCII
    '            oMailMsg.Subject = emailSubject
    '            oMailMsg.Body = sText
    '            oMailMsg.BodyFormat = Web.Mail.MailFormat.Html

    '            Web.Mail.SmtpMail.SmtpServer = CStr(ConfigurationManager.AppSettings("MailServerURL"))
    '            Web.Mail.SmtpMail.Send(oMailMsg)

    '        Else
    '            LogDebug("Calling EMailDataPDF sText is empty")
    '        End If

    '    Catch ex As Exception
    '        LogError("EMailDataPDF Error: " & ex.Message & " , ConfirmationText: " & sCfnText)
    '    End Try

    'End Sub


    Private Function ResendAndGetNewID(ByVal ConfirmationId As String, _
                                  ByVal Email As String, _
                                  ByVal FaxNumber As String, _
                                  ByVal OutputType As String, _
                                  ByVal AddUserId As String, _
                                  ByVal AddProgramName As String, _
                                  ByVal Parameter As String) As String


        Dim params(7) As Aurora.Common.Data.Parameter
        LogDebug("Calling ResendAndGetNewID: " & ConfirmationId)

        params(0) = New Aurora.Common.Data.Parameter("sCfnId", DbType.String, ConfirmationId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(1) = New Aurora.Common.Data.Parameter("sEmai", DbType.String, Email, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(2) = New Aurora.Common.Data.Parameter("sFax", DbType.String, FaxNumber, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(3) = New Aurora.Common.Data.Parameter("sOutputType", DbType.String, OutputType, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(4) = New Aurora.Common.Data.Parameter("sAddUsrId", DbType.String, AddUserId, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(5) = New Aurora.Common.Data.Parameter("sAddPrgmName", DbType.String, AddProgramName, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(6) = New Aurora.Common.Data.Parameter("sParam", DbType.String, Parameter, Aurora.Common.Data.Parameter.ParameterType.AddInParameter)
        params(7) = New Aurora.Common.Data.Parameter("NewConfirmationID", DbType.String, 64, Aurora.Common.Data.Parameter.ParameterType.AddOutParameter)

        Try
            Aurora.Common.Data.ExecuteOutputSP("cfn_update_reSend_TextConfirmation", params)
            LogDebug("Calling ResendAndGetNewID successfull: " & ConfirmationId)

            If (Not String.IsNullOrEmpty(params(7).Value)) Then
                Return params(7).Value
            Else
                Return ""
            End If

        Catch ex As Exception
            LogError("ResendAndGetNewID Error: " & ex.Message & " , ConfirmationID: " & ConfirmationId)
            Return "Error"
        End Try

    End Function

    ''rev:mia 08-march-2016 https://thlonline.atlassian.net/browse/AURORA-777 - As QA I want voucher from Aurora and Experiences catalogue should be exactly same to simplify testing and also re-use of code. 
    Function TransformedDocuments(ByVal confirmId As String, _
                                  ByVal showAll As Boolean, _
                                  Optional RentalExclustion As String = "", _
                                  Optional BookingId As String = "", _
                                  Optional RentalId As String = "", _
                                  Optional UserId As String = "", _
                                  Optional AudienceType As String = "") As String

        Dim returnedxml As String = ""

        Try

            LogDebug("Calling TransformedDocuments: " & confirmId)
            Dim xmlTemp As XmlDocument = Aurora.Booking.Services.BookingConfirmation.GenerateConfirmationReport(confirmId, showAll, RentalExclustion)

            confirmationXml.LoadXml(xmlTemp.DocumentElement.InnerXml)
            confirmationXml.DocumentElement.SelectSingleNode("viewbutton").InnerText = "1" 'hack - following old sys , AS-IS :)

            BrandFooterText = Aurora.BookingExperiences.Service.BookingExperiences.GetBrandFullName(confirmationXml.DocumentElement.SelectSingleNode("doc/Header/Brand").InnerText)

            Dim transform As New XslTransform()
            transform.Load(Server.MapPath(Me.ResolveUrl("~/Booking/xsl/confreport.xsl")))

            Using writer As New StringWriter
                transform.Transform(confirmationXml, Nothing, writer, Nothing)
                returnedxml = Server.HtmlDecode(writer.ToString())
            End Using

            Try
                ''REV:MIA 25-JAN-2016 - as a Finance user I want to see invoice details on all Experience vouchers
                _GSTCountry = confirmationXml.DocumentElement.SelectSingleNode("doc/Header/GSTCountry").InnerText

            Catch ex As Exception
            End Try

            ''rev:mia 08-march-2016 https://thlonline.atlassian.net/browse/AURORA-777
            Try
                If (Not String.IsNullOrEmpty(BookingId) And Not String.IsNullOrEmpty(RentalId) And Not String.IsNullOrEmpty(UserCode) And Not String.IsNullOrEmpty(AudienceType)) Then
                    returnedxml = String.Concat(returnedxml, EOF_FLAG, BuildVouchers(BookingId, RentalId, UserCode))
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            LogError("TransformedDocuments Error: " & ex.Message & " , ConfirmationID: " & confirmId)
            returnedxml = ""
        End Try

        Return returnedxml
    End Function



#End Region


#Region "Old Html Vouchers"
    Function ExperienceSummary(Rows As DataRow, username As String, nameofclient As String, contactdetails As String, ctr As String) As String
        Dim expName As String = ""
        Dim expitemdescription As String = ""
        Dim expcalltktqty As String = ""
        Dim expcalltktresrefnum As String = ""
        Dim supplier As String = ""
        Dim expcalltkttotal As String = ""
        Dim expDescription As String = ""
        Dim expCondition As String = ""
        Dim expItemId As String = ""
        Dim imageheader As String = ""
        Dim sb As New StringBuilder

        If (Not Rows("ExpItemId") Is Nothing) Then
            expItemId = Rows("ExpItemId").ToString
        End If



        If (Not Rows("ExpName") Is Nothing) Then
            expName = Rows("ExpName").ToString
        End If

        If (Not Rows("SupName") Is Nothing) Then
            supplier = Rows("SupName").ToString
        End If

        If (Not Rows("SupImageHeader") Is Nothing) Then
            imageheader = Rows("SupImageHeader").ToString
        End If

        If (Not Rows("ExpCallTktQty") Is Nothing) Then
            expcalltktqty = Rows("ExpCallTktQty").ToString
        End If

        If (Not Rows("ExpItemDescription") Is Nothing) Then
            expitemdescription = Rows("ExpItemDescription").ToString
        End If

        If (Not Rows("ExpCallTktResRefNum") Is Nothing) Then
            expcalltktresrefnum = Rows("ExpCallTktResRefNum").ToString
        End If

        If (Not Rows("ExpCallTktTotal") Is Nothing) Then
            expcalltkttotal = Rows("ExpCallTktTotal").ToString
        End If

        If (Not Rows("ExpDescription") Is Nothing) Then
            expDescription = Rows("ExpDescription").ToString
        End If

        If (Not Rows("ExpConditionText") Is Nothing) Then
            expCondition = Rows("ExpConditionText").ToString
        End If

        sb.Append(BuildTables(imageheader, _
                         expCondition, _
                         ctr, _
                         supplier, _
                         expcalltktqty, _
                         expitemdescription, _
                         expcalltktresrefnum, _
                         nameofclient, _
                         contactdetails, _
                         expName, _
                         expcalltkttotal, _
                         expDescription, _
                         username))

        Return sb.ToString

    End Function

    Function ExperienceSummaryWithSubItems(Rows As DataRow, subRows As DataRow, username As String, nameofclient As String, contactdetails As String, ctr As String) As String
        Dim expName As String = ""
        Dim expitemdescription As String = ""
        Dim expcalltktqty As String = ""
        Dim expcalltktresrefnum As String = ""
        Dim supplier As String = ""
        Dim expcalltkttotal As String = ""
        Dim expDescription As String = ""
        Dim expCondition As String = ""
        Dim expItemId As String = ""
        Dim imageheader As String = ""
        Dim sb As New StringBuilder

        'If (Not Rows("ExpItemId") Is Nothing) Then
        '    expItemId = Rows("ExpItemId").ToString
        'End If


        If (Not Rows("SupName") Is Nothing) Then
            supplier = Rows("SupName").ToString
        End If

        If (Not Rows("SupImageHeader") Is Nothing) Then
            imageheader = Rows("SupImageHeader").ToString
        End If

        ''----------------------------------------------
        ''this is a subitems. 

        If (Not subRows("Name") Is Nothing) Then
            expName = subRows("Name").ToString
        End If

        If (Not Rows("ExpItemDescription") Is Nothing) Then
            expitemdescription = Rows("ExpItemDescription").ToString
        End If

        If (Not subRows("Qty") Is Nothing) Then
            expcalltktqty = subRows("Qty").ToString
        End If

        If (Not subRows("Notes") Is Nothing) Then
            expDescription = subRows("Notes").ToString
        End If


        '-----------------------------------------------

        If (Not Rows("ExpCallTktResRefNum") Is Nothing) Then
            expcalltktresrefnum = Rows("ExpCallTktResRefNum").ToString
        End If

        If (Not Rows("ExpCallTktTotal") Is Nothing) Then
            expcalltkttotal = Rows("ExpCallTktTotal").ToString
        End If


        If (Not Rows("ExpConditionText") Is Nothing) Then
            expCondition = Rows("ExpConditionText").ToString
        End If

        sb.Append(BuildTables(imageheader, _
                         expCondition, _
                         ctr, _
                         supplier, _
                         expcalltktqty, _
                         expitemdescription, _
                         expcalltktresrefnum, _
                         nameofclient, _
                         contactdetails, _
                         expName, _
                         expcalltkttotal, _
                         expDescription, _
                         username))
        Return sb.ToString

    End Function

    Function BuildTables(imageheader As String, _
                         expCondition As String, _
                         ctr As String, _
                         supplier As String, _
                         expcalltktqty As String, _
                         expitemdescription As String, _
                         expcalltktresrefnum As String, _
                         nameofclient As String, _
                         contactdetails As String, _
                         expName As String, _
                         expcalltkttotal As String, _
                         expDescription As String, _
                         username As String) As String

        Dim sb As New StringBuilder
        sb.Append("<table width='600' border='0' cellspacing='0' cellpadding='0' style='font-size: 11px; border: 1px solid #cccccc; background-color:#FFF; border-style:dotted solid;'  align='center'>")
        sb.AppendFormat("<tr><td style='height:25px'>{0}</td></tr>", "<img src='http://res.cloudinary.com/thl/image/upload/v1447274811/tcx/partner-network/voucher/generic/scissor.png' width='100%'>")
        sb.AppendFormat("</table>", "")

        sb.Append("<table width='600' border='0' cellspacing='0' cellpadding='0' style='font-size: 11px; border: 1px solid #cccccc; background-color:#FFF; border-style:dotted solid;'  align='center'>")
        sb.AppendFormat("<tr><td>{0}</td></tr>", "<img src='" + imageheader + "' width='100%'>") ''images
        sb.AppendFormat("<tr><td>{0}</td></tr>", "<a href='" + expCondition + "' target='_blank'>Terms and Conditions</a>") ''T and C
        sb.AppendFormat("<tr><td style='text-align:center'><h1><b>{0}</b></h1></td></tr>", "Payment Voucher") ''Payment Voucher
        sb.AppendFormat("<tr><td nowrap>Thank You for booking with {0}</td></tr>", supplier.Trim)
        sb.AppendFormat("<tr><td>{0}</td></tr>", "<hr/>")

        sb.AppendFormat("<tr><td style='text-align:right'><b>Ticket #{0} &nbsp;&nbsp;</b></td></tr>", ctr)
        sb.AppendFormat("<tr><td><h2><b>{0}</b><h2></td></tr>", "RESERVATION DETAILS")
        sb.AppendFormat("<tr><td><b>{0} x {1}</b></td></tr>", expcalltktqty, expitemdescription)
        sb.AppendFormat("<tr><td>Reservation Ref:&nbsp;<b>{0}</b> </td></tr>", expcalltktresrefnum)
        sb.AppendFormat("<tr><td>Name Of Client:&nbsp;&nbsp;<b>{0}</b></td></tr>", nameofclient)
        sb.AppendFormat("<tr><td>Contact Details:<b> {0}</b></td></tr>", contactdetails)

        sb.AppendFormat("<tr><td>{0}</td></tr>", "<hr/>")
        sb.AppendFormat("<tr><td><h2><b>{0}</b><h2></td></tr>", "BOOKING DETAILS")
        sb.AppendFormat("<tr><td><b>{0} x {1} {2} {3}</td></tr>", expName, expcalltktqty, expitemdescription, CDec(expcalltkttotal).ToString("c"))
        sb.AppendFormat("<tr><td><b>{0}</b></td></tr>", expDescription)

        sb.AppendFormat("<tr><td>{0}</td></tr>", "<hr/>")
        sb.AppendFormat("<tr><td style='height:15px'>{0}</td></tr>", "")
        sb.AppendFormat("<tr><td>Please contact us on <b>{0}</b> or {1}</td></tr>", "0800 000 123", "<a href='mailto:bookings@thlonline.com'>bookings@thlonline.com</a>")
        sb.AppendFormat("<tr><td>{0}</td></tr>", "should you have any enquiries.")
        sb.AppendFormat("<tr><td>{0}</td></tr>", "")
        sb.AppendFormat("<tr><td style='height:25px'>{0}</td></tr>", "")
        sb.AppendFormat("<tr><td>{0}</td></tr>", "Kind regards,")
        sb.AppendFormat("<tr><td style='height:15px'>{0}</td></tr>", "")
        sb.AppendFormat("<tr><td>{0}</td></tr>", username)
        sb.AppendFormat("<tr><td>{0}</td></tr>", "Customer Service")
        sb.AppendFormat("<tr><td style='height:25px'>{0}</td></tr>", "")
        sb.AppendFormat("<tr><td>{0}</td></tr>", "<a href='www.thlonline.com' target='_blank'>Tourism Holding Limited</a>")
        sb.Append("<table>")

        Return sb.ToString
    End Function

    Function ExperienceSummary(bookingId As String, rentalId As String, userid As String) As String
        Dim dsInfo As DataSet = BookingExperiences.ConfirmationSummary(bookingId, rentalId, userid)
        Dim sb As New StringBuilder


        Dim nameOfClient As String = ""
        Dim email As String = ""
        Dim expName As String = ""
        Dim expitemdescription As String = ""
        Dim expcalltktqty As String = ""
        Dim expcalltktresrefnum As String = ""
        Dim supplier As String = ""
        Dim expcalltkttotal As String = ""
        Dim expDescription As String = ""
        Dim expCondition As String = ""
        Dim usrname As String = ""
        Dim imageheader As String = ""

        Dim dsCus As DataSet = BookingExperiences.GetCustomerInfo(bookingId, rentalId)
        If (dsInfo.Tables(0).Rows.Count - 1 = -1) Then Return "</body></html>"

        If (Not dsCus.Tables(0).Rows(0)("CName") Is Nothing) Then
            nameOfClient = dsCus.Tables(0).Rows(0)("CName").ToString
        End If
        If (Not dsCus.Tables(0).Rows(0)("EmailAddress") Is Nothing) Then
            email = dsCus.Tables(0).Rows(0)("EmailAddress").ToString
        End If


        If (dsInfo.Tables.Count - 1 <> -1) Then
            If (Not dsInfo.Tables(1).Rows(0)("UsrName") Is Nothing) Then
                usrname = dsInfo.Tables(1).Rows(0)("UsrName").ToString
            End If
            Dim ctr As Integer = 1


            For Each myrow As DataRow In dsInfo.Tables(0).Rows
                Dim expItemId As String = myrow("ExpItemId")
                Dim dsSubItems As DataSet = BookingExperiences.GetSubItems(CInt(expItemId))
                If (dsSubItems.Tables.Count - 1 <> -1) Then
                    If (dsSubItems.Tables(0).Rows.Count - 1 <> -1) Then
                        For Each subrow As DataRow In dsSubItems.Tables(0).Rows
                            sb.Append(ExperienceSummaryWithSubItems(myrow, subrow, usrname, nameOfClient, email, ctr))
                            ctr = ctr + 1
                        Next
                    Else
                        sb.Append(ExperienceSummary(myrow, usrname, nameOfClient, email, ctr))
                        ctr = ctr + 1
                    End If
                Else
                    sb.Append(ExperienceSummary(myrow, usrname, nameOfClient, email, ctr))
                    ctr = ctr + 1
                End If
            Next
        End If

        sb.Append("</body></html>")

        Return sb.ToString()
    End Function

#End Region

#Region "New HTML Vouchers"
    Function BuildVouchers(bookingId As String, rentalId As String, usercode As String) As String
        Return Aurora.BookingExperiences.Service.BookingExperiences.BuildVouchers(bookingId, rentalId, usercode, GSTCountry, BrandFooterText)
    End Function
#End Region

#Region "REV:MIA 25-JAN-2016 - as a Finance user I want to see invoice details on all Experience vouchers"
    Private _GSTCountry As String
    Private Property GSTCountry As String
        Get
            Return _GSTCountry
        End Get
        Set(value As String)
            _GSTCountry = value
        End Set
    End Property

    Private Function StrippedEOFMarker(confirmationtext As String, audience As String) As String
        Dim returntext As String = ""
        If (String.IsNullOrEmpty(audience)) Then
            If audienceParam = "Agent" Then
                audience = AGENT_ID
            Else
                audience = CUSTOMER_ID
            End If
        End If
        If (audience = AGENT_ID) Then ''customer
            Dim flagindex As Integer = confirmationtext.IndexOf(EOF_FLAG)
            If (flagindex <> -1) Then
                returntext = confirmationtext.Substring(0, flagindex)
            Else
                flagindex = confirmationtext.IndexOf("<!DOCTYPE html>")
                If (flagindex <> -1) Then
                    returntext = confirmationtext.Substring(0, flagindex)
                Else
                    returntext = confirmationtext
                End If
            End If
        Else
            returntext = confirmationtext.Replace(EOF_FLAG, "")
        End If
        Return returntext
    End Function
#End Region
End Class
