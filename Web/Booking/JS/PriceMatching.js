﻿//REV:MIA JULY 23, 2013 - TODAY IN HISTORY. 
//                      - A new royal baby boy was born for Prince William
//rev:mia Aug 13 2013 - modified
//                      - today in history, Berlin wall was constructed

//rev:mia Aug 16 2013 - modified
//                      - today in history, today elvis presley died. Aug 16 1977

//rev:mia Aug 23 2013 - modified - added "Book Now Campaign"
//                      - today in history, Last day of Konstantin and Zipho

function TableEnablingOrDisabling() {
    req = $('[id$=_ddlPMrequestType]').find('option:selected').text()
    comp = $("[id$=_ddlPMCompetitorName]").find("option:selected").text()
    match = $('[id$=_ddlPMmatchingStatus]').find('option:selected').text()


    //its KK and already approved. disabled everything
    if ((PMStatus == 'KK' || PMStatus == 'XX') && match.indexOf("Approved") == 0 && $("[id$=_decisionByTextBox]").val() != "") {
        $("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").attr("disabled", true)
        $("[id=PriceMatchingSaveButton]").removeAttr("disabled").attr("disabled", true)
        $("textarea.priceMatchNoteDesc").removeAttr("disabled").attr("disabled", "disabled")
        $("select.PriceMatchingList").removeAttr("disabled").attr("disabled", true)
        return
    }

    //both Request and competitor name is not selected, disabled everything
    if (req.indexOf(PLS_SELECT_CONSTANT) == 0 && comp.indexOf(PLS_SELECT_CONSTANT) == 0) {
        $("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").attr("disabled", true)
        $("[id=PriceMatchingSaveButton]").removeAttr("disabled").attr("disabled", true)
        $("textarea.priceMatchNoteDesc").removeAttr("disabled").attr("disabled", "disabled")
        $("textarea.priceMatchNote").removeAttr("disabled").attr("disabled", "disabled")
        return
    }

    //Request is not selected, disabled everything
    if (req.indexOf(PLS_SELECT_CONSTANT) == 0) {
        $("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").attr("disabled", true)
        $("[id=PriceMatchingSaveButton]").removeAttr("disabled").attr("disabled", true)
        $("textarea.priceMatchNoteDesc").removeAttr("disabled").attr("disabled", "disabled")
        $("textarea.priceMatchNote").removeAttr("disabled").attr("disabled", "disabled")
        return
    }

    //Competitor is not selected, disabled everything
    if (comp.indexOf(PLS_SELECT_CONSTANT) != -1) {
        $("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").attr("disabled", true)
        $("[id=PriceMatchingSaveButton]").removeAttr("disabled").attr("disabled", true)
        $("textarea.priceMatchNoteDesc").removeAttr("disabled").attr("disabled", "disabled")
        $("textarea.priceMatchNote").removeAttr("disabled").attr("disabled", "disabled")
        return
    }

    $("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").attr("disabled", false)
    $("[id=PriceMatchingSaveButton]").removeAttr("disabled")
    $("textarea.priceMatchNoteDesc").removeAttr("disabled")
    $("textarea.priceMatchNote").removeAttr("disabled")

    ApprovedPriceEnablingOrDisabling()
}

function ApprovedPriceEnablingOrDisabling() {
    match = $('[id$=_ddlPMmatchingStatus]').find('option:selected').text()
    if (match.indexOf(INITIATED_CONSTANT) == 0) {
        $("[id=priceMatchingTable] input.approvedpriceSubTotal,input.approvedpriceSubTotal_oum, input.approvedpriceTotal").attr("disabled", true)
    }
    else {
        if (match.indexOf(PLS_SELECT_CONSTANT) == 0) {
            $("[id=priceMatchingTable] input.approvedpriceSubTotal, input.approvedpriceSubTotal_oum,input.approvedpriceTotal").attr("disabled", true)
        }
        else
            $("[id=priceMatchingTable] input.approvedpriceSubTotal,input.approvedpriceSubTotal_oum,input.approvedpriceTotal").removeAttr("disabled")
    }
}

function InitPricesDefaultValues() {

    textboxes = $("#priceMatchingTable").find("input.Vehicle, input.Insurance,input.Insurance_oum, input.Total,input.AddOns, input.approvedpriceSubTotal,input.approvedpriceSubTotal_oum, input.approvedpriceTotal, input.approvedpriceAddOns").filter(function () {
                         return $(this).val() == ""
                        });
    textboxes.each(function () {
            $(this).val('0.00')
    });
}

function LoadPriceMatchDetails() {
    $("[id$=HiddenFieldHtmlStructure]").val("")
    $("[id$=HiddenFieldHtmlStructureForNotes]").val("")
    

    req = $('[id$=_ddlPMrequestType]').find('option:selected').text()//.indexOf("Price Match")

    var parameter = $('[id$=_PMRentalIdLabel]').text() + '|'                                                    //0 PriceMRntId
    parameter = parameter + $('[id$=_ddlPMrequestType]').find('option:selected').val() + '|'                    //1 PriceMRequestCodTypeId
    parameter = parameter + $('[id$=_BookingPriceMatchUserControl_requestedByTextBox]').val().toUpperCase() + '|'             //2 PriceMRequestor
    parameter = parameter + $('[id$=_ddlPMCompetitorName]').find('option:selected').val() + '|'                 //3 PriceMCompetitorCodId
    parameter = parameter + $('[id$=_ddlPMmatchingStatus]').find('option:selected').val() + '|'                 //4 PriceMStatusCodId
    parameter = parameter +  'NA|'                                                                          //5 PriceMCompetitorVehicle
    parameter = parameter +  '0.00|'                                                                        //6 PriceMCompetitorPrice
    parameter = parameter + 'NA|'                                                                          //7 PriceMCompetitorQuoteDate
    parameter = parameter + 'NA|'                                                                          //8 PriceMCompetitorQuoteLink

    if (req.indexOf(PRICE_MATCH_CONSTANT) != -1 || req.indexOf(BOOK_NOW_CAMPAIGN_CONSTANT) != -1) {
        parameter = parameter + $('[name$=priceMatchNoteDesc]').val().toUpperCase() + '|'                       //9 PriceMNote
    }
    else {
        parameter = parameter + $('[name$=priceMatchNote]').val().toUpperCase() + '|'
    }
    parameter = parameter + 'NA|'                                                                          //10 PriceMCompetitorOthers
    parameter = parameter + $('[id$=_BookingPriceMatchUserControl_decisionByTextBox]').val().toUpperCase() + '|'              //11 PriceMDecisionBy
    //parameter = parameter + '<%= UserCode %>' + '|'                                                             //12
    parameter = parameter + usercode + '|'                                                             //12
    //parameter = parameter + '<%= BookingPriceMatchUserControl.PMPriceMSeqId %>' + '|'                           //13
    parameter = parameter + PMPriceMSeqId + '|'                           //13
    parameter = parameter + $('[id$=_ddlPMCompetitorName]').find('option:selected').text().toUpperCase() + '|'                //14

    parameter = parameter + 'NA|'                                                                          //15 AlternateVehicleTextBox
    parameter = parameter +  '0|'                                                                           //16 AlternateVehicleTextBox

    var agent = $("#ctl00_ContentPlaceHolder_agentTextBox").val().split("-")[0]
    parameter = parameter + agent + '|'                                                                      //17 

    var tdindex = 0
    var xmlstring = "<PriceMatch><Products>"
    var xmlstringrow = ""
    var tb;
    var sequenceID;
    var masterkey;
    var htmltext = ''

    $("#divPriceMatchLoad tr.productMatchingItem").each(function () {
        xmlstringrow = ""
        //$this = $(this)
        sequenceID = $(this).attr("id").split("_")[1]
        masterkey = $(this).attr("id").split("_")[0]


        $(this).find('td').each(function () {
            switch (tdindex) {
                case 0:
                    xmlstringrow = xmlstringrow + "<Masterkey>" + masterkey + "</Masterkey>"
                    xmlstringrow = xmlstringrow + "<SequenceID>" + sequenceID + "</SequenceID>"
                    break;
                case 1:
                    xmlstringrow = xmlstringrow + "<THLProduct>" + $(this).text().toUpperCase() + "</THLProduct>"
                    break;
                case 2:
                    xmlstringrow = xmlstringrow + "<THLPrice>" + $(this).text() + "</THLPrice>"
                    break;
                case 3:
                    xmlstringrow = xmlstringrow + "<THLOffer>" + $(this).text() + "</THLOffer>"
                    break;
                case 4:
                    //percentage
                    break;
                case 5:
                    tb = $(this).find("input")
                    xmlstringrow = xmlstringrow + "<CompPrice>" + $(tb).val() + "</CompPrice>"

                    htmltext = $(this).html()
                    htmltext = htmltext.replace("value=", "value='" + $(tb).val() + "'")
                    $(this).html(htmltext)

                    break;
                case 6:
                    tb = $(this).find("input")
                    xmlstringrow = xmlstringrow + "<CompProduct>" + $(tb).val().toUpperCase() + "</CompProduct>"

                    htmltext = $(this).html()
                    htmltext = htmltext.replace("value=", "value='" + $(tb).val() + "'")
                    $(this).html(htmltext)
                    break;
                case 7:
                    xmlstringrow = xmlstringrow + "<PriceVariance>" + $(this).text() + "</PriceVariance>"
                    break;
                case 8:
                    tb = $(this).find("input")
                    xmlstringrow = xmlstringrow + "<ApprovedPrice>" + $(tb).val() + "</ApprovedPrice>"
                    
                    htmltext = $(this).html()
                    htmltext = htmltext.replace("value=", "value='" + $(tb).val() + "'")
                    $(this).html(htmltext)
                    break;


            }
            tdindex += 1


        });
        xmlstring = xmlstring + "<Item>" + xmlstringrow + "</Item>"
        tdindex = 0
    });
   

    xmlstring = xmlstring + "</Products></PriceMatch>"
    ExecutePriceMatchingRequest(parameter, xmlstring, EmailFormatting())
}

function LoadPopupShowMoreNotes() {
    centerPopupShowMoreNotes()
    $("[id$=_panelPopupThirdPartyReference]").show()
    $('[id$=divDescriptionMessage]').html("<center><a href='#' id='linkClose'>Close this window</a> </center>")

    $('[id$=linkClose]').bind('click', function () {
        UnloadPopupShowMoreNotes()
    });
}

function UnloadPopupShowMoreNotes() {
    $("[id$=_panelPopupThirdPartyReference]").hide();
}

function centerPopupShowMoreNotes() {

    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelPopupThirdPartyReference]").height();
    popupWidth = $("[id$=_panelPopupThirdPartyReference]").width();


    $("[id$=_panelPopupThirdPartyReference]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
}

function Validate() {

    req = $('[id$=_ddlPMrequestType]').find('option:selected').text()

    var $EmptyItemsList = $(".PriceMatchingList").filter(function () {
        return $(this).val() == "-1"
    });

    if ($EmptyItemsList.length != 0) {
        setErrorShortMessage(SELECT_REQ_CONSTANT)
        return false;
    } else {
        clearShortMessage()
    }


    var $EmptyItemsText = $(".PriceMatchingText").filter(function () {
        return $(this).val() == ""
    });

    if ($EmptyItemsText.length != 0) {
        setErrorShortMessage(REQUESTED_REQ_CONSTANT)
        return false;
    } else {
        clearShortMessage()
    }

    
    var matchingstatus = $('[id$=_ddlPMmatchingStatus]').find('option:selected').text()
    if (matchingstatus == "Approved") {
        if ($('[id$=_BookingPriceMatchUserControl_decisionByTextBox]').val() == '') {
            setErrorShortMessage(APPROVED_REQ_CONSTANT)
            return false;
        }
        clearShortMessage()
    }

    if (emptyPrice == true) {
        if (matchingstatus.indexOf(INITIATED_CONSTANT) == -1) {
            setInformationShortMessage(INITIATED_REQ_CONSTANT)
            $('select[id$="_ddlPMmatchingStatus"]>option:eq(1)').attr('selected', true);
            return false;
        }
    }
    else
        clearShortMessage()

   


    var $EmptyPriceItems = $("input.Vehicle, input.Insurance, input.Total,input.AddOns").filter(function () {
        return (parseFloat($(this).val()) == 0 || $(this).val() == "");
    });

    var $EmptyProductItems = $("input.nonthlproduct").filter(function () {
        return jQuery.trim($(this).val()) == "";
    });



    if (matchingstatus.indexOf(INITIATED_CONSTANT) == 0) {
       /* 
        if ($EmptyPriceItems.length == $EmptyProductItems.length) {
            //will return true
        }
        else {
            setWarningShortMessage(COMPETITOR_REQ_CONSTANT)
            return false;
        }
      */
    }
    else {

        var $labelVariance = $("label._Vehicle, input._Insurance, input._Total,input._AddOns").filter(function () {
            return $(this).text() != 0
        });
        
        if (matchingstatus.indexOf(PLS_SELECT_CONSTANT) == 0) {
            setWarningShortMessage(INVALID_PRICE_CONSTANT)
            return false;
        }
        else {
       
            /*
            if (matchingstatus == APPROVED_CONSTANT && (req == PRICE_MATCH_CONSTANT || req == BOOK_NOW_CAMPAIGN_CONSTANT)) {

               

                var $EmptyPriceItemsForvehicle = $("input.Vehicle").filter(function () {
                    return $(this).val() > 0;
                });

                var $EmptyPriceItemsForInsurance = $("input.Insurance").filter(function () {
                    return $(this).val() > 0;
                });

                var $EmptyPriceItemsForFinalPrice = $("input.approvedpriceSubTotal").filter(function () {
                    return $(this).val() > 0;
                });

                if ($EmptyPriceItemsForvehicle.length == 0 && $EmptyPriceItemsForInsurance.length == 0 && $EmptyPriceItemsForFinalPrice.length == 0) {
                    setWarningShortMessage(COMPETITOR_APPROVED_REQ_CONSTANT)
                    return false
                }
                if ($EmptyPriceItemsForFinalPrice.length == 0) {
                    setWarningShortMessage(COMPETITOR_APPROVED_REQ_CONSTANT)
                    return false
                }
            }
            */
            
            /*
            var $EmptyApprovedPriceItems = $("input.approvedpriceSubTotal, input.approvedpriceTotal").filter(function () {
                return ($(this).val() == "");
            });

            if ($EmptyPriceItems.length == $EmptyApprovedPriceItems.length && $EmptyApprovedPriceItems.length == $EmptyProductItems.length) {
            }
            else {
                setWarningShortMessage(COMPETITOR_APPROVED_REQ_CONSTANT)
                return false;
            }
            */
        }
    }
    return true;
}

function centerPopupPriceMatch() {
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelPriceMatch]").height();
    popupWidth = $("[id$=_panelPriceMatch]").width();


    $("[id$=_panelPriceMatch]").css({
        "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
}


function UnloadPopupPriceMatch() {
    $("[id$=_panelPriceMatch]").hide();
    clearShortMessage()
}

function LoadPopupPriceMatch() {
    centerPopupPriceMatch()
    $("[id$=_panelPriceMatch]").show();

}

function EmailFormatting() {

    var tdindex = 0
    var xmlstringrow = ""
    var tb;

    var htmltext =

    $("#divPriceMatchLoad tr.rowNotes").each(function () {
        $(this).find('td').each(function () {
            tb = $(this).find("input,textarea")
            $(this).html($(tb).val())
        });
    });

    $("#divPriceMatchLoad tr.rowGrouping").each(function () {
        $(this).find('td').each(function ()
        {
            switch (tdindex) 
            {
                case 7:
                    //price variance
                    tb = $(this).find("label")
                    $(this).html("<b>" + $(tb).text() + "</b>")
                    break;
                case 8:
                    //approve price
                    tb = $(this).find("label")
                    $(this).html("<b>" + $(tb).text() + "</b>")
                    break;
            }
            tdindex += 1
            
            });
        tdindex = 0
    });

    tdindex = 0

    $("#divPriceMatchLoad tr.productMatchingItem").each(function () {
        $(this).find('td').each(function ()
        {
            switch (tdindex) {
                case 0:
                    //type
                    break;
                case 1:
                    //thlproduct
                    break;
                case 2:
                    //orig price
                    break;
                case 3:
                    //offer
                    tb = $(this).find("label")
                    $(this).html($(tb).text())
                    
                    break;
                case 4:
                    //percentage
                    break;
                case 5:
                    //comp.price
                    tb = $(this).find("input")
                    $(this).html($(tb).val()).css("text-align","center")

                    break;
                case 6:
                    //comp. product
                    tb = $(this).find("input")
                    $(this).html($(tb).val())
                    break;
                case 7:
                    //price variance
                    tb = $(this).find("label")
                    $(this).html($(tb).text())
                    break;
                case 8:
                    //approve price
                    tb = $(this).find("input")
                    $(this).html($(tb).val())
                    break;
            }
            tdindex += 1
        });
        tdindex = 0
    });

    return $("#divPriceMatchLoad").html()
}


