﻿// JScript File - for VehicleRequestMGT.aspx - rev:MIA - Feb24

           var requestURL =window.document.location.toString();
           var tempURLindex = requestURL.indexOf(".") //Get the url string using dot as reference
           var tempURL = requestURL.substring(0,tempURLindex)
           requestURL = tempURL
           
           var start=0
           var end=requestURL.lastIndexOf("/") + 1;
           
           var remotePage= 'GetVehicleRequestAjax.aspx?mode=';
           

           //for getting the current url dynamically 
           requestURL=requestURL.substring(start,end) + remotePage;     
           
           
             
           var xmlHttp;        
           var is_ie = (navigator.userAgent.indexOf('MSIE') >= 0) ? 1 : 0; 
           var is_ie5 = (navigator.appVersion.indexOf("MSIE 5.5")!=-1) ? 1 : 0; 
           var strGetpackagexml 
           var modeCKOorCKI; //this will identify the control that trigger the calls 
       
        var date1 //= new Date( gDdToday.split('/')[2], gDdToday.split('/')[1]-1, gDdToday.split('/')[0] );
	    var date2 //= new Date( ckoDate.split('/')[2], ckoDate.split('/')[1]-1, ckoDate.split('/')[0] );
	        
        function yyyymmddFormat(objDate)
        {
        
            
            var dd;
            var mm;
            var yy;
            
              
            var ary = new Array() 
            ary=objDate.split('-')
           
            if(ary.length != 3)
            {
               ary = objDate.split('/')      
            }
            
            if(ary.length == 3)
            {
               
                
                //check dd as the first digits
                if((ary[0] <= 31 && ary[0].length == 2) && (ary[2] >= 2000 && ary[2].length == 4))
                 {// alert('1');
                  
                   dd = ary[0];
                   mm = ary[1]-1;
                   yy = ary[2];
                   return new Date(yy,mm,dd)
                  
                 }
                 else
                 {
                  //check month as the first digits
                  if((ary[0] <= 12 && ary[0].length == 2) && (ary[1] <= 31 && ary[1].length == 2) && (ary[2] >= 2000 && ary[2].length == 4))
                    {
                        
                       // alert('2');
                       
                        mm = ary[0];
                        dd = ary[1]-1;
                        yy = ary[2];
                        return new Date(yy,mm,dd)
                   
                    }
                    else
                    {
                        //check yyyy as the first digits
                        if((ary[0] > 2000 && ary[0].length == 4) && (ary[1] <= 12 && ary[1].length == 2) && (ary[2] <= 31 && ary[2].length == 2))
                        {
                           // alert('3');
                          
                            yy = ary[0];
                            mm = ary[1]-1;
                            dd = ary[2];
                            return new Date(yy,mm,dd)
                          
                        } 
                        else
                        {
                              return  'Error:Invalid Date'   
                        }                     
                    }
                 }
                 
            }   
            else
            {
             return  'Error:Invalid Date'   
            }   
          
        }

       
       function AddDays()
       {//debugger
        if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value != '')
        {
            var noofdays = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value
            var ckODate = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox').value
            var ckIDate = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox').value
            var tempdate = yyyymmddFormat(ckODate)
            
            var dateOffset = (24*60*60*1000) * noofdays;
            tempdate.setTime(tempdate.getTime() + dateOffset);
            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox').value = tempdate
        } 
       }
       
       function TimeFormat(objDate)
           {
                var dd;
                var mm;
                var yy;
                var dmy = false
              
                var ary = new Array() 
                ary=objDate.split('-')
               
                if(ary.length != 3)
                {
                   ary = objDate.split('/')      
                   dmy = true
                }
                 
                if(ary.length == 3)
                {
                    if(dmy==true)
                    {
                      dd = ary[0];
                      mm = ary[1];
                      yy = ary[2];
                    }
                    else
                    {
                      yy = ary[0];
                      mm = ary[1];
                      dd = ary[2];
                    }
                }
                  // This instruction will create a date object
                  mm = mm -1
                  var source_date = new Date(yy,mm,dd);

                  if(yy != source_date.getFullYear())
                  {
                     alert('Year is not valid!');
                     return false;
                  }

                  if(mm != source_date.getMonth())
                  {
                     alert('Month is not valid!');
                     return false;
                  }

                  if(dd != source_date.getDate())
                  {
                     alert('Day is not valid!');
                     return false;
                  }
              return source_date
           }
       
       //Compute date before Check-out date
       //----------------------------------------------------------------------------------------
       
       function setTillDays()
       {
          // debugger;
	        var ckoDate = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox').value
	        var gDdToday =  document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox').value
	        var todaydate = new Date()
	        var str 
		    var strdate 
		    
		  
		    
		    
	        if (ckoDate != '' && gDdToday != '' )
	            { 
	                    if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox'))==false) return
		                if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox'))==false) return
	                    
	                    //date1 = yyyymmddFormat(gDdToday)
	                    date1 = TimeFormat(gDdToday)
	                    str =  date1.toDateString()
	                    strdate = str.substring(0,3)
  	                    document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKI').innerHTML=strdate
	                    
	                    //date2 = yyyymmddFormat(ckoDate)
	                    date2 = TimeFormat(ckoDate)
	                    str =  date2.toDateString()
	                    strdate = str.substring(0,3)
	                    document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKO').innerHTML=strdate
	                    document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lblDaysMessage').innerHTML=( Math.round( (date2.getTime()- todaydate.getTime()) / 86400000 ) + 1 )	
	                    
	                   // if (ckoDate <=  gDdToday)
	                   if(date2 <=  date1)
	                    {
	                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value = ( Math.round( ( date1.getTime() - date2.getTime() ) / 86400000 ) + 1 )	
	                    }
	                    else
	                    {
	                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value = ''
	                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lblDaysMessage').innerHTML =''
	                    }
	                    
	                    
                }
		        else
		        {
		                if (ckoDate != '' && gDdToday == '')
		                {
		                    if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox'))==false) return
		                    date2 = TimeFormat(ckoDate)
	                        str =  date2.toDateString()
	                        strdate = str.substring(0,3)
	                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKO').innerHTML=strdate
	                       
	                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lblDaysMessage').innerHTML=( Math.round( (date2.getTime()- todaydate.getTime()) / 86400000 ) + 1 )	
		                }
		                
		                if (ckoDate == '' && gDdToday != '')
		                {
		                    if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox'))==false) return
		                    date1 = TimeFormat(gDdToday)
	                        str =  date1.toDateString()
	                        strdate = str.substring(0,3)
  	                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKI').innerHTML=strdate
		                }
		                
		                document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value = ""
		                return
		        }
	    } 
       
       //Validate date entered
       //----------------------------------------------------------------------------------------
       function validateDate(fld) {
         //  debugger;
            var RegExPattern =  /^\d{1,2}\/\d{1,2}\/\d{4}$/;
            if ((fld.value.match(RegExPattern)) && (fld.value!='')) {
                return true;
            } else {
                    RegExPattern = /^((((19|20)(([02468][048])|([13579][26]))-02-29))|((20[0-9][0-9])|(19[0-9][0-9]))-((((0[1-9])|(1[0-2]))-((0[1-9])|(1\d)|(2[0-8])))|((((0[13578])|(1[02]))-31)|(((0[1,3-9])|(1[0-2]))-(29|30)))))$/;
                    if ((fld.value.match(RegExPattern)) && (fld.value!='')) {
                        return true;
                       }else{
                        // fld.focus();
                        return false;
                       } 
            } 
        }
       
       
       //Return date in word format
       //----------------------------------------------------------------------------------------
       function getDayofWeek(formate,lblindex){
                var strdate
                var cko
                var cki
                var myDate
                //debugger;
	            if(lblindex == 1)
	            {
	            
	                    cko = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
	                    myDate = cko.value 
	                    if(validateDate(cko)==false)
	                        {
	                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value = ""
	                            ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKO.innerHTML = ""
	                            return
	                        }
	                    
	            }
	            else if(lblindex == 2)
	            {
	                    
	                    cki = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox')
	                    myDate = cki.value 
	                    if(validateDate(cki)==false)
	                     {
	                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value = ""
	                            ctl00_ContentPlaceHolder_wizBookingProcess_lblDayCKI.innerHTML = ""
	                            return
	                        }
	            }
	            if (myDate.indexOf("/") != -1)
	            {
	                    dt = new Date(myDate.split('/')[1]+'/'+myDate.split('/')[0]+'/'+myDate.split('/')[2])
	                    if (formate == 1) //for cko
	                        strdate = dt.getDay()
	                    else{
		                    var str = dt.toDateString()
		                    strdate = str.substring(0,3)
	                    }
	            }else{
	            
	            }
	            
	            
	            
                
    	           //checkout date
	            if(lblindex==1)
	                {
	                     ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKO.innerHTML=strdate
	                     document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout').focus()
	                     
	                }
	                else if(lblindex==2)
	                {
	                     ctl00_ContentPlaceHolder_wizBookingProcess_lblDayCKI.innerHTML=strdate
	                     document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin').focus()
	                }         
	        
	            setTillDays(); 
	            
       }
       
      
         function GetPackageXML()
           {
                var agentid = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerHidden')
                if (agentid.value.length == 0)
                {
                        agentid=document.getElementById('ctl00_ContentPlaceHolder_hidAgentID')
                }
                
              //  alert(document.getElementById('ctl00_ContentPlaceHolder_PickerControlForCheckOut_pickerHidden').value);
               
                var productid= document.getElementById('ctl00_ContentPlaceHolder_hidPackageID')
                var hidCKOandCKIinfo = document.getElementById('ctl00_ContentPlaceHolder_hidcompanion')
                var cko = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
                    //debugger;
                var ckoAry=[];
                ckoAry = cko.value.split("-")
                var tempCKO
                var tempCKI
                if(ckoAry.length > 1)
                {
                    tempCKO = ckoAry[2] + '/' + ckoAry[1] + '/' + ckoAry[0]
                }else{
                    ckoAry = cko.value.split("/")
                    if(ckoAry.length > 1)
                    {
                        tempCKO = ckoAry[0] + '/' + ckoAry[1] + '/' + ckoAry[2]
                    }
                    
                }
                
                
                var ckoAMPM = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout')
                var ckobranch = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                var cki = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox')
                var ckiAry = [];
                ckiAry = cki.value.split("-")
                if(ckiAry.length > 1)
                {
                    tempCKI = ckiAry[2] + '/' + ckiAry[1] + '/' + ckiAry[0]
                }else{
                    ckiAry = cki.value.split("/")
                    if(ckiAry.length > 1)
                    {
                        tempCKI = ckiAry[0] + '/' + ckiAry[1] + '/' + ckiAry[2]
                    }
                }
                
                var ckiAMPM = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin')
                var ckibranch = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox')
                
                
                var adult = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAdultNO')
                var infants = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlChildNO')
                var children = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlInfantsNO')
                
                var packagexml
                var ckostr
                var ckistr
                
                strCko= ckobranch.value.split('-')
                strCki =ckibranch.value.split('-')     
                
                        
                        if (ckoAMPM.value == 'AM')
                            {
                                ckostr = tempCKO + ' ' + '07:00:00'
                                
                            }
                            else
                            {
                                if (ckoAMPM.value == 'PM')
                                    {
                                        ckostr = tempCKO + ' ' + '13:00:00'
                                    }
                                    else
                                    {
                                       
                                        ckostr = tempCKO + ' ' + ckoAMPM.value
                                    }    
                            }
                            
                         if (ckiAMPM.value == 'AM')
                            {
                                ckistr = tempCKI + ' ' + '07:00:00'
                            }
                            else
                            {
                                
                                if (ckiAMPM.value == 'PM')
                                    {
                                        ckistr = tempCKI + ' ' + '13:00:00'
                                    }
                                    else
                                    {
                                        ckistr = tempCKI + ' ' + ckiAMPM.value
                                    }    
                            }
                            
                        packagexml = agentid.value + ',' + productid.value + ',' + ckostr + ',' + strCko[0] + ',' + strCki[0] + ','  + adult.value + ',' + children.value + ',' + infants.value  + ',' + ckistr
                        
                        strGetpackagexml =packagexml
                       //rev:mia 594 Booking Request' page - 'Select Agent Package' pop up window displays more options than required
                       var locationCKO= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                       var locationCKI= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox')

                       if (agentid != '' && productid.value != '' && ckostr.indexOf('undefined') == -1 && ckistr.indexOf('undefined') == -1 && locationCKO != '' && locationCKI != '') {
                           document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo').value = packagexml
                       }
                       else {
                           //rev:mia Dec.20 2013 - Fixes for filtering of packages in BookingProcess after adding a new rental
                           var qs = getQueryStrings();
                           var outL = qs["outL"];
                           var outD = qs["outD"];
                           if (outL != '' && outD != '') {
                               document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo').value = packagexml
                           }
                       }
                            //paramThreeDynamic()
                        
                        
           }

            
       
           function CalculateDays()
           {
            
                    var hirerdays = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod')
                    var cko = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
                    var cki = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox')
                    var ckiAMPM = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin')
                    var ckoAMPM = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout')
                    var days = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlDays')
                    var hidCKOandCKIinfo = document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo')
                    var ckoValue 
                        if(ckoAMPM.value.indexOf('AM'))
                            {
                                ckoValue = 'AM'
                            }else{
                                if(ckoAMPM.value.indexOf('PM'))
                                    {
                                        ckoValue = 'PM'
                                    }
                            }    
                    
                    var ckiValue
                    if(ckiAMPM.value.indexOf('AM'))
                            {
                                ckiValue = 'AM'
                            }else{
                                if(ckiAMPM.value.indexOf('PM'))
                                    {
                                        ckiValue = 'PM'
                                    }
                            }    
                    
                    var datehiredparam = cko.value + '|' + ckoValue + '|' + cki.value + '|' + ckiValue + '|' + hirerdays.value + '|' + days.value
                    
                    if (hirerdays.value.length > 0 && cko.value.length > 0 && cki.value.length > 0)
                        {
                          
                            hidCKOandCKIinfo.value = datehiredparam
                            var url=requestURL + 'getdatehired&paramdatehired=' + datehiredparam 
                            xmlHttp = GetXmlHttpObject(stateChangeHandler)
                            xmlHttp_Get(xmlHttp, url);     
                        }
                        
           }
            
           function GetProductID(userid)
           {//debugger
                    var productid= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox')
                    var url=requestURL + 'getproductid&paramProductid=' + productid.value + '&uid=' + userid
                    xmlHttp = GetXmlHttpObject(stateChangeHandler)
                    xmlHttp_Get(xmlHttp, url); 
               
           }
           
           //REV:MIA May-5
       
         
           //-----------------------------------------------------------------------------------------------------------------------------
           //changes on seasonal schedule - rev:mia nov18
           //-----------------------------------------------------------------------------------------------------------------------------
           function GetAvailableTimeCKO(userid)
           { // alert('GetAvailableTimeCKO')
                var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                
                   var url=requestURL + 'GetAvailableTimeCKO&paramcko=' + location.value + '&uid=' + userid + '&controltype=1' 
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
          function GetAvailableTimeCKI(userid)
           { //alert('GetAvailableTimeCKI')
                var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox')
                
                   var url=requestURL + 'GetAvailableTimeCKI&paramcki=' + location.value + '&uid=' + userid + '&controltype=2' 
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
           
           function GetAvailableTimeCKOv2()
           { 
                var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                var mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
                
                 
                  if(mydate.value=='' || location.value == '')return
                   //alert('GetAvailableTimeCKOv2:' + mydate.value ) 
                   var url=requestURL + 'GetAvailableTimeCKOv2&paramcko=' + location.value   + '&controltype=1' + '&traveldate='  + mydate.value
                   modeCKOorCKI = 1
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
          function GetAvailableTimeCKIv2()
           { 
                var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox')
                var mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox')
                if(mydate.value=='' || location.value == '')return
                //alert('GetAvailableTimeCKIv2:' + mydate.value )
                   var url=requestURL + 'GetAvailableTimeCKIv2&paramcki=' + location.value  + '&controltype=2' + '&traveldate='  + mydate.value
                   modeCKOorCKI = 2
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
          
          
            function GetAvailableRequestTimeCKO(userid)
               {  //alert('GetAvailableTimeCKO')
                    var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox')
                    
                       var url=requestURL + 'GetAvailableTimeCKO&paramcko=' + location.value + '&uid=' + userid + '&controltype=1' 
                       xmlHttp = GetXmlHttpObject(stateChangeHandler)
                       xmlHttp_Get(xmlHttp, url); 
                
               }
          
           function GetAvailableRequestTimeCKI(userid)
               {  //alert('GetAvailableTimeCKi')
                    var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox')
                    
                       var url=requestURL + 'GetAvailableTimeCKI&paramcki=' + location.value + '&uid=' + userid + '&controltype=2' 
                       xmlHttp = GetXmlHttpObject(stateChangeHandler)
                       xmlHttp_Get(xmlHttp, url); 
                
               }
          
          function GetAvailableRequestTimeCKOv2()
               {  //alert('GetAvailableTimeCKO')
                    var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox')
                    var mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_DateControlForCKO_dateTextBox')
                    if(mydate.value=='' || location.value == '')return
                       var url=requestURL + 'GetAvailableTimeCKOv2&paramcko=' + location.value   + '&controltype=1' + '&traveldate='  + mydate.value
                       modeCKOorCKI = 3
                       xmlHttp = GetXmlHttpObject(stateChangeHandler)
                       xmlHttp_Get(xmlHttp, url); 
                
               }
          
           function GetAvailableRequestTimeCKIv2()
          { // alert('GetAvailableTimeCKi')
                    var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox')
                    var mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_DateControlForCKI_dateTextBox')
                    
                    if(mydate.value=='' || location.value == '')return
                       var url=requestURL + 'GetAvailableTimeCKIv2&paramcki=' + location.value  + '&controltype=2' + '&traveldate='  + mydate.value
                       modeCKOorCKI = 4
                       xmlHttp = GetXmlHttpObject(stateChangeHandler)
                       xmlHttp_Get(xmlHttp, url);

          }



          //-----------------------------------------------------------------------------------------------------------------------------
           function ClearLocationTimeAndSet(locationtime)
           {           // alert(locationtime.xml)
               var location
               //rev:mia July 5 2013 - part of addrental
               var defTime
                        if(locationtime.selectSingleNode('mode')!= null)
                            {
                                if(locationtime.selectSingleNode('mode').text == '1') 
                                    {
                                        location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout');
                                        if(location == null)
                                            {
                                                location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlamppmCKO');
                                            }
                                           else
                                             {
                                                document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = false;
                                                document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = false;
                                                //rev:mia July 5 2013 - part of addrental
                                                defTime = location.value
                                             }
                                    }
                                    else
                                    {
                                       
                                        location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin');
                                        if(location == null)
                                            {
                                                location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlAMPMcki');
                                            }
                                            else
                                            {
                                                 document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = false;
                                                 document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = false;
                                                 //rev:mia July 5 2013 - part of addrental
                                                 defTime = location.value
                                            }
                                           
                                    }
                            } 	     	       
                        
                        //alert('mode: ' + location.id);
                        var locationNodes = locationtime.getElementsByTagName('ckocki');	     	       
                        
	                    var textValueName; 
	                    var optionItemName;

	                    

	                     //Clears the location dropdown list        	       
	                    for (var count = location.options.length-1; count >-1; count--)
	                    {
		                    location.options[count] = null;
		                }        

	                    
	                    
	                    var hasvalue = false;
	                    var valueForCKI = '';
	                    for (var count = 0; count < locationNodes.length; count++)
	                    {
                                           
						    var textNamevalue = locationNodes[count].selectSingleNode("data").text; 
						    var textIDvalue = locationNodes[count].selectSingleNode("data").text; 
		                    optionItemName = new Option(textNamevalue, textIDvalue,  false, false);
		                    //alert(textNamevalue)
		                    if(textIDvalue == '15:00') 
		                        {
		                            valueForCKI = textIDvalue;
		                           // alert(valueForCKI);
		                        }

		                        location.options[location.length] = optionItemName;

            		        hasvalue=true;
            		    }
            		    //rev:mia July 5 2013 - part of addrental
            		    if (defTime != "AM" && defTime != "PM" && defTime != "") {
            		        location.value = defTime
	                    }

	                    if(hasvalue==true)
	                    {   
	                            var HidhasValueCKO =document.getElementById('ctl00_ContentPlaceHolder_HidhasValueCKO');	       
	                            var HidhasValueCKI =document.getElementById('ctl00_ContentPlaceHolder_HidhasValueCKI');
	                          

	                            if(location.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout') > -1  || location.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlamppmCKO') > -1)
	                            {
	                                HidhasValueCKO.value = true;
	                            }
	                            else 
                                {
    	                                 HidhasValueCKI.value = true;
	                                    //rev:mia June 3,2009 added to display the last hour for checkin
    	                                 if (valueForCKI != '')
    	                                 //rev:mia July 5 2013 - part of addrental
    	                                     if (defTime != "AM" && defTime != "PM" && defTime != "" && defTime != "15:00") {
    	                                         location.value = defTime
    	                                     }
                                            else
                                                location.value = "15:00"
	                                    else
	                                        location.selectedIndex = location.length - 1
	                            }    
	                    }
	                    
                            if(location.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout') > -1 || location.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin') > -1)
                                    {
                                    revalidateMessagesStep1()    
                                   }
                            //debugger;       
                            if(location.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlamppmCKO') > -1 || location.id.indexOf('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlAMPMcki') > -1)
                                    {
                                    revalidateMessagesStep3()    
                                   }                                   
                                  
	    
           }
         
           //rev:mia july 25
           function getAgentID()
           {
                     var url
                    var ddl=document.getElementById("ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox"); 
                    if (ddl.value != '')
                    {
                        url = requestURL + 'getAgentID&paramagentid=' + ddl.value 
                        xmlHttp = GetXmlHttpObject(stateChangeHandler);
                        xmlHttp_Get(xmlHttp, url);     
                    }
                      
                     
        
           }
           function getContacts(contact)
           {
                    var url
                    if (contact.length > 0)
                    { 
                        //Append the name to search for to the requestURL 
                        url = requestURL + 'getcontact&paramContactID=' + contact 
                    }
                    else
                    {
                        var ddl=document.getElementById("ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox"); 
                        url = requestURL + 'getcontact&paramContactName=' + ddl.value 
                    }
                    
                      
                        xmlHttp = GetXmlHttpObject(stateChangeHandler);
                        xmlHttp_Get(xmlHttp, url);     
        
                      
           }
           
           function GetXmlHttpObject(handler)
                     { 
                        var objXmlHttp = null;    //Holds the local xmlHTTP object instance             
                        if (is_ie)
                        { 
                            //The object to create depends on version of IE 
                            //If it isn't ie5, then default to the Msxml2.XMLHTTP object           
                            var strObjName = (is_ie5) ? 'Microsoft.XMLHTTP' : 'Msxml2.XMLHTTP'; 
                         
                            //Attempt to create the object 
                            try
                            { 
                            objXmlHttp = new ActiveXObject(strObjName);                
                            objXmlHttp.onreadystatechange = handler; 
                            } 
                            catch(e)
                            { 
                            //Object creation errored 
                                alert('Object could not be created'); 
                                return; 
                            }    
                         } 
                         
                          return objXmlHttp;
                          alert(objXmlHttp);
                           
                     } 

                function xmlHttp_Get(xmlhttp, url)
                     {                     
                        xmlhttp.open('GET', url, true);             
                        xmlhttp.send(null); 
                     } 
    
    
                function GetHiredDate(datexml)
                { //  debugger;
                        
                        
                        var doc1 = new ActiveXObject("MSXML2.DOMDocument"); 
                        doc1.loadXML(datexml)
                        
                            var d1 = doc1.selectSingleNode('//Root/D1').text
                            var d2 = doc1.selectSingleNode('//Root/D2').text
                            var dif = doc1.selectSingleNode('//Root/Dif').text
                            
                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox').value = d1
                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox').value = d2
                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_txtHirePeriod').value = dif
                           
                    
                }
                
                function ClearContactsAndSetContacts(userNodes)
                {
                        var usersList = document.getElementById("ctl00_ContentPlaceHolder_wizBookingProcess_ddlContact");           
                        var userNameNodes = userNodes.getElementsByTagName('Contact');	     	       
	                    var textValueName; 
	                    var optionItemName;
            	       
            	        
	                     //Clears the ddl_users dropdown list        	       
	                    for (var count = usersList.options.length-1; count >-1; count--)
	                    {
		                    usersList.options[count] = null;	        
            		        
	                    }        
	                    //Add new users list to the users ddl_users
	                    optionItemName = new Option('','');
	                    usersList.add(optionItemName)
	                    for (var count = 0; count < userNameNodes.length; count++)
	                    {
                                           
                           
						    var textNamevalue = userNameNodes[count].selectSingleNode("ConLastName").text + ", " + userNameNodes[count].selectSingleNode("ConFirstName").text
						    var textIDvalue = userNameNodes[count].selectSingleNode("ConId").text 
		                    optionItemName = new Option(textNamevalue, textIDvalue,  false, false);
		                    usersList.options[usersList.length] = optionItemName;  
            		        
	                    }	       
	                    
            	        document.getElementById('ctl00_ContentPlaceHolder_hidAgentID').value = userNodes.selectSingleNode("AgentID").text
                }

           function stateChangeHandler() 
                { //debugger;
                   //readyState of 4 or 'complete' represents that data has been returned            
                    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete')
                    { 
                        //Gather the results from the callback          
                        var str = xmlHttp.responseText; 
                        //alert(str)
                        
                        if(str.search('<AddAgentID>') != -1)
                            {
                                var agentid = str.substr(str.indexOf('<AddAgentID>'))
                                agentid = agentid.replace('<AddAgentID>','')
                                agentid = agentid.replace('</AddAgentID>','')
                                document.getElementById('ctl00_ContentPlaceHolder_hidAgentID').value = agentid
                                str  = str.substr(0,str.indexOf('<AddAgentID>'))
                            } 
                            
                        var doc = new ActiveXObject("MSXML2.DOMDocument"); 
                        doc.loadXML(str)
                        //alert(str)
                        // for populating  ddl_users                
                        if (doc.documentElement != null)
                        {
                          
                            if (doc.selectSingleNode('//Root/D1') != null)
                                {  
                                    GetHiredDate(str);
                                }
                                else
                                {
                                    
                                    if(doc.selectSingleNode('//PRODUCT/Id') != null)
                                        {   
                                            document.getElementById('ctl00_ContentPlaceHolder_hidPackageID').value = doc.selectSingleNode('//PRODUCT/Id').text
                                            GetPackageXML();
                                             
                                        }
                                        else
                                        {
                                            
                                            if(doc.selectSingleNode('/root/ckocki') != null)
                                            {
                                                //alert('inside')
                                                ClearLocationTimeAndSet(doc.documentElement);
                                            }
                                            else
                                            {
                                                if (doc.selectSingleNode('Contacts/Contact/ConId') != null) 
                                                    {
                                                        ClearContactsAndSetContacts(doc.documentElement);
                                                        GetPackageXML();
                                                    }
                                                    else
                                                    {
                                                        //document.getElementById('ctl00_ContentPlaceHolder_hidAgentID')=doc.selectSingleNode('AgentID').text
                                                    }        
                                            }   
                                        }
                                }    
                        }
                        else
                        { 
                        
                           if(modeCKOorCKI == 1)
                            {    
                                   modeCKO();
                            }
                           else
                            {  
                                if(modeCKOorCKI == 2)
                                {   
                                    modeCKI();
                                }
                                
                                if(modeCKOorCKI == 3)
                                {   
                                    var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlamppmCKO');
                                    for (var count = location.options.length-1; count >-1; count--)
	                                {
		                                location.options[count] = null;
	                                }
	                                var HidhasValueCKO= document.getElementById('ctl00_ContentPlaceHolder_HidhasValueCKO');	       
	                                HidhasValueCKO.value = false;    
	                                 revalidateMessagesStep3();     
                                }
                                
                                if(modeCKOorCKI == 4)
                                {   
                                    var location= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlAMPMcki');
                                    for (var count = location.options.length-1; count >-1; count--)
	                                {
		                                location.options[count] = null;
	                                }
	                                var HidhasValueCKI= document.getElementById('ctl00_ContentPlaceHolder_HidhasValueCKI');	       
	                                HidhasValueCKI.value = false;  
	                                revalidateMessagesStep3();                
                                }
                                
                            }
                        }                        
                   }   
               } 

function Navigate()
                        {
                            if (confirm("Are you sure you want to start a new Booking Request?.")){			
			                    document.forms[0].action="VehicleRequestMgt.aspx"
			                    document.forms[0].method="POST"
			                    document.forms[0].submit()
		                    }
	                    }	
          
           
                 function IsNumberValid(Source,Args)
                        {
                            Args.IsValid = IsNumeric(Args.Value)
                
                        }
           
function IsNumeric(sText)
        {
                           var ValidChars = "0123456789";
                           var IsNumber=true;
                           var Char;

                         
                           for (i = 0; i < sText.length && IsNumber == true; i++) 
                              { 
                              Char = sText.charAt(i); 
                              if (ValidChars.indexOf(Char) == -1) 
                                 {
                                 IsNumber = false;
                                 }
                              }
                           return IsNumber;
                           
                        }



function ChangeTitle(NewTitle,LabelId,url)
{
                                document.getElementById(LabelId).innerText = NewTitle;
                                document.getElementById(LabelId).href = '~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=' + url;
}

function refreshPackageCallBack(result,context)
{
                                setTimeout('__doPostBack(\'ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox\',\'\')', 0);
}


function paramThreeDynamic()
{
                                PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 1, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForAgent_pickerTextBox').value.toString().indexOf(' -')));
                                if(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox').value.indexOf('-') == -1)
                                    {
                                        PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox').value);
                                    }
                                    else
                                    {
                                        PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 2, document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox').value.toString().substring(0,document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox').value.toString().indexOf(' -')));
                                    }
                                PickerControl.setParam ('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForPackage_pickerTextBox', 3, document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo').value);
}

//rev:mia dec5
function modeCKO()
{
                                var HidhasValueCKO= document.getElementById('ctl00_ContentPlaceHolder_HidhasValueCKO');	       
                                var locationOut = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout');
                                var CKO         = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                                var mydate      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
                                var daycko      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKO')
                                for (var count = locationOut.options.length-1; count >-1; count--)
	                            {
		                            locationOut.options[count] = null;	 
	                            }       
	                                 HidhasValueCKO.value = false;
	                                 if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox'))==false) return
		                             if(CKO.value == '' || CKO.value.length <= 3 || CKO.value.indexOf('-') == -1)
		                                {
           	                                 document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
        	                                 document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;
		                                     return
		                                } 
		                             
		                             if(CKO.value != '' && CKO.value.indexOf('-')  > -1 && locationOut.options.length-1 != -1 ) return
		                             
	                                 setWarningShortMessage(CKO.value + ' branch is closed on ' + daycko.innerText + ' ' + mydate.value)
	                                 document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
	                                 document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;

}

function modeCKI()
{
                                   
                                    var HidhasValueCKI= document.getElementById('ctl00_ContentPlaceHolder_HidhasValueCKI');	       
                                    var locationIn= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin');
                                    var CKI= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox')
                                    var mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox')
                                    var daycki      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKI')
                                    for (var count = locationIn.options.length-1; count >-1; count--)
	                                {
		                                locationIn.options[count] = null;
	                                }
	                                HidhasValueCKI.value = false;
	                                
		                            if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox'))==false) return
		                            if(CKI.value == '' || CKI.value.length <= 3 || CKI.value.indexOf('-') == -1)
		                                {
		                                     document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
	                                         document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;
		                                     return
		                                }
		                            if(CKI.value != '' && CKI.value.indexOf('-')  > -1 && locationIn.options.length-1 != -1 ) return
		                            
		                            setWarningShortMessage(CKI.value + ' branch is closed on ' + daycki.innerText + ' ' + mydate.value)
	                                document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
	                                document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;

}

function revalidateMessagesStep1()
{
                                    var locationOut = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckout');
	                                var locationIn= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_ddlAMPMcheckin');
	                                if(locationOut.options.length-1 <= -1 || locationIn.options.length-1 <= -1)
                                               {
                                                     var CKO         = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckOut_pickerTextBox')
                                                     var mydate      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox')
                                                     var daycko      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKO')
                                                     if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckOut_dateTextBox'))==false) return
                                                     if(CKO.value == '' || CKO.value.length <= 3 || CKO.value.indexOf('-') == -1)
                                                        {
                                                          document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
                                                          document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;   
                                                          return
                                                        }
                                                     
                                                     if(locationOut.options.length-1 <= -1)
                                                       {  
                                                          setWarningShortMessage(CKO.value + ' branch is closed on ' + daycko.innerText + ' ' + mydate.value)
	                                                      document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
                                                          document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;
                                                           return
                                                       }

			                                          var CKI= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForCheckIn_pickerTextBox')
                                                      mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox')
                                                      var daycki      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_lbldayCKI') 	
			                                          if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_DateControlForCheckIn_dateTextBox'))==false) return
	                                                  if(CKI.value == '' || CKI.value.length <= 3 || CKI.value.indexOf('-') != -1)
	                                                    {
	                                                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
                                                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;
	                                                        return
	                                                    } 
	                                                  
			                                          if(locationIn.options.length-1 <= -1)
                                                       {
			                                                setWarningShortMessage(CKI.value + ' branch is closed on ' + daycki.innerText + ' ' + mydate.value)
	                                                        document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_StartNextButton').disabled = true;
                                                            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StartNavigationTemplateContainerID_btnReloc').disabled = true;
                                                       }
                            			   	   }
}


function revalidateMessagesStep3()
{//debugger;
                                    var locationOut = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlamppmCKO');
	                                var locationIn= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_ddlAMPMcki');
	                                revalidateButtonsStep3(false)
	                                if(locationOut.options.length-1 <= -1 || locationIn.options.length-1 <= -1 )
                                               {
                                                     var CKO         = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKO_pickerTextBox')
                                                     var mydate      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_DateControlForCKO_dateTextBox')
                                                     var daycko      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_lblckoDay')
                                                     if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_DateControlForCKO_dateTextBox'))==false) return
                                                     if(CKO.value == '' || CKO.value.length <= 3 || CKO.value.indexOf('-') == -1)
                                                        {
                                                            revalidateButtonsStep3(true)
                                                            return
                                                        }
                                                     if(locationOut.options.length-1 <= -1)
                                                       {
                                                          setWarningShortMessage(CKO.value + ' branch is closed on ' + daycko.innerText + ' ' + mydate.value)
                                                           return
                                                       }

			                                          var CKI= document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_PickerControlForBranchCKI_pickerTextBox')
                                                      mydate  = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_DateControlForCKI_dateTextBox')
                                                      var daycki      = document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_lblckiDay') 	
			                                          if(validateDate(document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_DateControlForCKI_dateTextBox'))==false) return
	                                                  if(CKI.value == '' || CKI.value.length <= 3 || CKI.value.indexOf('-') == -1)
	                                                  {
                                                            revalidateButtonsStep3(true)
                                                            return
                                                      }
			                                          if(locationIn.options.length-1 <= -1)
                                                       {
			                                                setWarningShortMessage(CKI.value + ' branch is closed on ' + daycki.innerText + ' ' + mydate.value)
                                                       }
                            			   	   }
}



function revalidateButtonsStep3(value)
{
            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnResubmit').disabled = value;
            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnOneDayBack').disabled = value;
            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_Step3_btnOneDayForward').disabled = value;
            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSaveNext').disabled = value;
            document.getElementById('ctl00_ContentPlaceHolder_wizBookingProcess_StepNavigationTemplateContainerID_btnSubmit').disabled = value;
}


//rev:mia Dec.20 2013 - Fixes for filtering of packages in BookingProcess after adding a new rental
function getQueryStrings() {
    var assoc = {};
    var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
    var queryString = location.search.substring(1);
    var keyValues = queryString.split('&');

    for (var i in keyValues) {
        var key = keyValues[i].split('=');
        if (key.length > 1) {
            assoc[decode(key[0])] = decode(key[1]);
        }
    }

    return assoc;
} 
