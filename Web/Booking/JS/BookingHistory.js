﻿function showHistoryPopUp(e, show, Prd, ChEv, Qty, UOM, Rate)
{ 
    var target;
    var x = 0;
    var y = 0;
    if (window.event) 
    {
        e = window.event;
        target = e.srcElement;
        x = e.x;
        y = e.y;
        if (document.documentElement && document.documentElement.scrollTop)	 // Explorer 6 Strict
		    y += document.documentElement.scrollTop;
	    else if (document.body) // all other Explorers
		    y += document.body.scrollTop;
    }
    else
    {
        target = e.target;
        x = e.pageX;
        y = e.pageY;
    }
    
    var popupHistory = document.getElementById ('popupHistory');
    
    if (!show)
    { 
        popupHistory.style.visibility='hidden';
    }
    else 
    { 
    
        //modification by Shoel
        
        var QtyArray = Qty.split('#')
        var RateArray = Rate.split('#')
        var UOMArray = UOM.split('#')
        var ChEvArray = ChEv.split('#')
        var PrdArray = Prd.split('#')
        
        //clear it!
        var sTableString = '<table>'
        
        for(var i=0;i<QtyArray.length-1;i++)
        {
            sTableString += '<tr>'
                                        + '<td>' +ChEvArray[i] + '</td>'
                                        + '<td>&nbsp;&nbsp;' + PrdArray[i] + ' - ' + QtyArray[i] + ' ' + UOMArray[i]  + '</td>'
                                        + '<td align="right">&nbsp;&nbsp;' + '$' + trimDecimal(RateArray[i], 2) + ' Total Owing' +  '</td>'
                                    + '</tr>'
        }
        sTableString += '</table>'
        
        popupHistory.innerHTML = sTableString

        target.style.cursor='pointer';
        popupHistory.style.visibility = 'visible';
        popupHistory.style.left = (x + 5) + 'px';
        popupHistory.style.top = (y - 5) + 'px';
    }
}
