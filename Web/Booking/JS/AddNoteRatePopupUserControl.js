﻿// JScript File

        function submitData(noteTextBoxId, varianceTextBoxId, addNoteRatePopupMessagePanelControlId  ){
        
            var noteTextBox;
            noteTextBox = document.getElementById(noteTextBoxId);  
            var varianceTextBox;
            varianceTextBox = document.getElementById(varianceTextBoxId);  
	      
	        noteTextBox.value = trimSpace(noteTextBox.value)
	        if (noteTextBox.value == ""){
	            MessagePanelControl.setMessagePanelError(addNoteRatePopupMessagePanelControlId, "Notes text cannot be blank and should be more than 1 character!");
		        //alert('Notes text cannot be blank and should be more than 1 character!')
		        return false
	        }
	        else if (varianceTextBox.value == "" || varianceTextBox.value < 0){
	            MessagePanelControl.setMessagePanelError(addNoteRatePopupMessagePanelControlId, "Rate cannot be blank and must be greater than or equal to zero!");
		        //alert('Rate cannot be blank and must be greater than or equal to zero!')
		        return false 
	        }
	        else 
	        {
	            var error 
	            error = ValidateText(noteTextBox.value)
	            if ( error != ''){
	                MessagePanelControl.setMessagePanelError(addNoteRatePopupMessagePanelControlId, error);
		            return false
	            }
	            else
	            {
		            return true 
		        }
		    }
        }
        
        

