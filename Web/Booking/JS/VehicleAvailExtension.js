﻿var brandtoSearch = null;
var brandtoSearchDynamic = null;
var popupmsg = null;

var windowWidth = null;
var windowHeight = null;
var popupHeight = null;
var popupWidth = null;

var vehicleIds = null;

$(document).ready(function () {


   // $("[id$=_panelPopupExtension]").draggable();

    $('[id$=SearchButton]').click(function () {
        $("[id$=HiddenFieldSelectionInQuickAvailPopup]").attr("value", "No")
        DisablingButtons(true);
        //debugger
        LoadPopup()
    });

    $('[id$=rightButton]').click(function () {
        UnloadPopup()
    });

    $(document).keypress(function (e) {
        if (e.keyCode == 27) {}

        //stop displaying confirmationBox
        if (e.keyCode == 8) { }
    });



    $(document).keydown(function (e) {
        if (e.keyCode == 27) {
            UnloadPopup()
        }

        //stop displaying confirmationBox
        if (e.keyCode == 8) { }
    });

    $("select[id^=SelectBrands]").change(function () {
        brandtoSearch = $(this).attr("value")
    });


    $('input[type=checkbox],panel,text').click(function (e) {
        var brandCode = '';
        var code = '';
        brandtoSearch = ''

        if (!e) var e = window.event;
        e.cancelBubble = true;
        if (e.stopPropagation) e.stopPropagation();

      

        $("#ctl00_ContentPlaceHolder_cbQuickAvail_cblBrands input").each(function (e) {
            if (this.checked) {
                //alert($(this).parent().attr('alt'));
                code = $(this).parent().attr('alt');

                switch (code) {
                    case 'XX': //rev:mia May 23,2012 - no longer use
                        brandCode = brandCode + 'ExploreMore' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                    case 'M':
                        brandCode = brandCode + 'Maui' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                    case 'B':
                        brandCode = brandCode + 'Britz' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                    case 'P':
                        brandCode = brandCode + 'Backpacker' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                    case 'Y':
                        brandCode = brandCode + 'Mighty' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;

                    case 'Q':
                        brandCode = brandCode + 'Kea' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;

                    //rev:mia Oct. 16 2012 -- Added UNITED/ECONO/ALPINEs 
                    case 'U':
                        brandCode = brandCode + 'United' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                    case 'E':
                        brandCode = brandCode + 'Econo' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                    case 'A':
                        brandCode = brandCode + 'Alpha' + ','
                        brandtoSearch = brandtoSearch + code + ','
                        break;
                }

            }

        });
        var lastIndex = brandCode.lastIndexOf(',')
        if (lastIndex != -1) {
            brandCode = brandCode.substring(0, lastIndex)
            brandtoSearch = brandtoSearch.substring(0, lastIndex)
            popupmsg = brandCode;
        }

        
        //if ((brandCode.indexOf("Maui") != -1) && (brandCode.indexOf("Britz") != -1) && (brandCode.indexOf("Backpacker") != -1) && (brandCode.indexOf("Mighty") != -1) && (brandCode.indexOf("Kea") != -1)) {
        if ((brandCode.indexOf("Econo") != -1) && (brandCode.indexOf("Alpha") != -1) && (brandCode.indexOf("United") != -1) && (brandCode.indexOf("Maui") != -1) && (brandCode.indexOf("Britz") != -1) && (brandCode.indexOf("Backpacker") != -1) && (brandCode.indexOf("Mighty") != -1) && (brandCode.indexOf("Kea") != -1)) {
            $('[id$=_txtBrands]').attr("value", "all brands")
            brandtoSearch = 'null'
            popupmsg = "all brand"
        }


        $('[id$=_txtBrands]').attr("value", brandCode)
        if (brandCode = '') $('[id$=_txtBrands]').attr("value", "all brands")

        //alert(brandtoSearch);
    });

});

function UnloadPopup() {
    $("[id$=_panelPopupExtension]").hide();
    DisablingButtons(false);
    $('#ctl00_ContentPlaceHolder_btnQuickAvailButton').trigger('click')
    
}

function LoadPopup() {
    SetToDefault();
    HideUsercontrol()
    centerPopup()

    //$('#ctl00_SessionRedirectorUserControl1_ConfirmationBoxControl1_leftButton').trigger('click')

    if (popupmsg == 'null' || popupmsg  == null || popupmsg == '') popupmsg = 'all brands'
    $('[id$=divAvailMessage]').html("<center><h3 id='h3searching'>Searching for " + popupmsg + "</h3>Press <b>'Esc'</b> to close this window</center>")

    $("[id$=_panelPopupExtension]").show()
    ExecuteExtension()
     
}

function HideUsercontrol() {
    var confirmationBoxBehavior = $find($('[id$=HiddenFieldConfirmationBehaviorID]').attr("value"))
    confirmationBoxBehavior.hide();
    return false;

}

function centerPopup() {
    //request data for centering   
    windowWidth = document.documentElement.clientWidth;
    windowHeight = document.documentElement.clientHeight;
    popupHeight = $("[id$=_panelPopupExtension]").height();
    popupWidth = $("[id$=_panelPopupExtension]").width();
    //centering   
    
    $("[id$=_panelPopupExtension]").css(
    { "position": "absolute", "top": windowHeight / 2 - popupHeight / 2, "left": windowWidth / 2 - popupWidth / 2
    });
    
}

function ExecuteManageBookingRequest() {
    var parameter = $("[id$=HiddenFieldVehicleExtension]").val()
    var url = "ExtensionListener/ExtensionListener.aspx?mode=EXTENSIONMANAGEREQUEST"
    $.post(url, { EXTENSIONMANAGEREQUEST: parameter }, function (result) {
        if (result == 'ERROR') {
            $('[id$=divAvailMessage]').html("<center><h3>Failed to get Alternate vehicles.Please try again</h3></center>")
        }
        else {
            ExecuteExtension()
        }
    });
}


function ExecuteExtension() {
    var parameter = $("[id$=HiddenFieldVehicleExtension]").val() + "|" + brandtoSearch + "|" + $("[id$=_promocodeTextbox]").text() //rev:mia Oct 16 2014 - addition of PromoCode
    vehicleIds = '';
        
        var url = "ExtensionListener/ExtensionListener.aspx?mode=EXTENSION"
        $.post(url, { EXTENSION: parameter }, function (result) {
            if (result == 'Failed') {
                $('[id$=divAvailMessage]').html("<center><h3>Failed to get Alternate vehicles</h3></center>")
                DisablingButtons(false);
            }
            else {
                $('[id$=divAvailMessage]').html(result)
                SetToDefault()
                $("[id$=_panelPopupExtension]").css(
                    { "position": "absolute", "top": "25%",
                        "left": windowWidth / 2 - popupWidth / 2
                    });


                $('[id$=divAvailMessage] input:checkbox').bind("click", function (e) {

                    $('[id$=divAvailMessage] input:checkbox').attr('checked', false);
                    $(this).attr("checked", true)

                    vehicleIds = vehicleIds + $(this).attr("value") + ","

                    var lastIndex = vehicleIds.lastIndexOf(',')
                    if (lastIndex != -1) {
                        vehicleIds = vehicleIds.substring(0, lastIndex)
                    }

                    vehicleIds = $(this).attr("value")

                    if (vehicleIds != '') $('#ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox').attr("value", vehicleIds)

                });

                $('[id$=divAvailMessage] input:checkbox').bind("dblclick", function (e) {

                    $('[id$=divAvailMessage] input:checkbox').attr('checked', false);
                    $(this).attr("checked", true)

                    vehicleIds = vehicleIds + $(this).attr("value") + ","

                    var lastIndex = vehicleIds.lastIndexOf(',')
                    if (lastIndex != -1) {
                        vehicleIds = vehicleIds.substring(0, lastIndex)
                    }

                    vehicleIds = $(this).attr("value")

                    if (vehicleIds != '') $('#ctl00_ContentPlaceHolder_wizBookingProcess_PickerControlForProduct_pickerTextBox').attr("value", vehicleIds)

                    UnloadPopup();

                });

            }
        });
    }

    function SetToDefault() {
        $('[id$=_txtBrands]').attr("value", "all brands")
        $("#ctl00_ContentPlaceHolder_cbQuickAvail_cblBrands input").each(function (e) {
            if (this.checked) {
                $(this).removeAttr('checked')
            }

        });
    }


    function DisablingButtons(status) {
        $('div  :input[type=submit]').attr('disabled', status);
        $('div  :input[type=text]').attr('disabled', status);
        $('div  :input[type=select]').attr('disabled', status);
    }