﻿// Change Log
// 14.8.8   -   Shoel - This now passes the BookingNum to confmgt page :)
//https://thlonline.atlassian.net/browse/AURORA-777
function LoadConfirmation(ConfirmationId,BookingId,DateSent,RentalId,BookingNumber,RentalNo,ActiveRentalCountry,Audience)
{
    if(!((DateSent != null) && (DateSent != "") && (DateSent != "&nbsp;") ))
    {
        //apparently a "First Load" ??
        //rev:mia jan6,2009 added ActiveRentalCountry
        if(ActiveRentalCountry != null)
            {
                window.location = "ConfirmationManagement.aspx?ConfirmationId=" + ConfirmationId + "&BookingId=" + BookingId + "&RentalId=" + RentalId + "&BookingNum=" + BookingNumber + "&RentalNo=" + RentalNo + "&ActiveRentalCountry=" + ActiveRentalCountry;
            }
            else
            {
                window.location = "ConfirmationManagement.aspx?ConfirmationId=" + ConfirmationId + "&BookingId=" + BookingId + "&RentalId=" + RentalId + "&BookingNum=" + BookingNumber + "&RentalNo=" + RentalNo;
            }        
        return false;
    }
    else
    {
        //direct to the "View" screen!
        //https://thlonline.atlassian.net/browse/AURORA-777
        var Win = window.open("ConfirmationViewScreen.aspx?ConfirmationId=" + ConfirmationId + "&BookingId=" + BookingId + "&CalledFrom=BookingConfirmationUserControl.ascx" + "&RentalId=" + RentalId + "&BookingNum=" + BookingNumber + "&Audience=" + Audience, "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=" + screen.availHeight + " ,width=" + screen.availWidth)
        Win.focus();
        Win.resizeTo(Win.screen.availWidth,Win.screen.availHeight);
        Win.moveTo(0,0);
        return false;
    }
    
}
