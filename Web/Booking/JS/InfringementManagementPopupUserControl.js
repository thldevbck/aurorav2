﻿// JScript File

function checkStatus(statusDropDownListId, resolvedDateDateControlId){

    var statusDropDownList = document.getElementById(statusDropDownListId)
    var resolvedDateDateControl = document.getElementById(resolvedDateDateControlId)

    if (statusDropDownList.value=='Open' || statusDropDownList.value=='Cancelled'){
        resolvedDateDateControl.value = ""
    }
}

function valStatus(statusDropDownListId, resolvedDateDateControlId,todayHiddenId){

    var statusDropDownList = document.getElementById(statusDropDownListId)
    var resolvedDateDateControl = document.getElementById(resolvedDateDateControlId)
    var todayHidden = document.getElementById(todayHiddenId)
 
    if (statusDropDownList.value=='Closed'){
      resolvedDateDateControl.value= todayHidden.value;
    }
    else{
      resolvedDateDateControl.value='';
    }
}
  

function valReference(paymentReferenceTextBoxId, paymentReceivedDateControlId, todayHiddenId){

    var paymentReferenceTextBox = document.getElementById(paymentReferenceTextBoxId)
    var paymentReceivedDateControl = document.getElementById(paymentReceivedDateControlId)
    var todayHidden = document.getElementById(todayHiddenId)
    
    removeSpace(paymentReferenceTextBox);
    if (paymentReferenceTextBox.value!=''){
        paymentReceivedDateControl.value = todayHidden.value;
    }
    else{
        paymentReceivedDateControl.value='';
    }
}

