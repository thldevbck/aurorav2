﻿function ShowAgreement(sRentalData)	
{	
    if (sRentalData == "") return;

    var win = window.open("RentalAgreement.aspx?RntData=" + escape(sRentalData), "", "top=0,left=0,menubar=yes, scrollbars=yes ,height="+ screen.availHeight + ",width="+ screen.availWidth)
    win.focus();	
    win.resizeTo(win.screen.availWidth,win.screen.availHeight);	
    win.moveTo(0,0);	
}
/* 'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was*/
function ShowAgreementAmazon(sRentalData) {
    if (sRentalData == "") return;

    var win = window.open("RentalAgreement.aspx?RntData=" + escape(sRentalData), "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=" + screen.availHeight + ",width=" + screen.availWidth)
    win.focus();
    win.resizeTo(win.screen.availWidth, win.screen.availHeight);
    win.moveTo(0, 0);
}
/*End 'nyt 9th Oct 2015, Ref: https://thlonline.atlassian.net/browse/AURORA-294 Able to retrieve/see Signed Rentals from Aurora from new link pulling data from AWS and also having the Reprint button back as it was*/
function validateOdometerData (
    CountryCode,
    RentalStatus,
    PrevOdoReading,
    HirePeriod,
    CtrlToValidate,
    //MsgPanelId,
    LblDiffId)
{	
    // fix for lead zeroes in parseInt
    for(var i=0;i<document.getElementById(CtrlToValidate).value.length;i++)
    {
        if(document.getElementById(CtrlToValidate).value.charAt(0) == "0")
        {
            document.getElementById(CtrlToValidate).value = document.getElementById(CtrlToValidate).value.substring(1);
        }
    }
    // check if its become an empty string
    if(document.getElementById(CtrlToValidate).value == "")
    {
        document.getElementById(CtrlToValidate).value = "0";
    }
    
    if (LblDiffId != null)
    {
        if(isNaN(parseInt(document.getElementById(CtrlToValidate).value) - parseInt(PrevOdoReading)))
        {
            MessagePanelControl.setMessagePanelWarning (null,"Invalid Odometer In reading entered");
            return false;
        }
        document.getElementById(LblDiffId).innerText = parseInt(document.getElementById(CtrlToValidate).value) - parseInt(PrevOdoReading);
    }
    
    // only for NZ
    if(CountryCode == 'NZ')
    {
	        var previousOdometerReading;
	        //Checkin part
	        if (RentalStatus == 'CO') 
	        // status = checked-out , means we are going to check-in
	        {
		        previousOdometerReading = parseInt(PrevOdoReading);
		        if (parseInt(document.getElementById(CtrlToValidate).value) < previousOdometerReading)
		        {
			        //MessagePanelControl.setMessagePanelWarning (null,"The reading entered may be incorrect");
			        MessagePanelControl.setMessagePanelWarning (null,"GEN157 - The Odometer In reading cannot be less than the Odometer Out reading.");
			        return false;
		        }
		        //another checkin validation
		        /*
		         calc max travelling
			        var maxDist = lastOdoReading + 	400 x numberOfDaysInHire
			        if enteredOdo > maxDist
			        {
				        show message "your odometer reading is beyond expected k's of around 61200 . Please check your entry"
			        }
		        */
		        var hirePeriod = parseInt(HirePeriod);
		        var maxDist = previousOdometerReading + 400 * hirePeriod;
		        if (parseInt(document.getElementById(CtrlToValidate).value) > maxDist)
		        {
			        MessagePanelControl.setMessagePanelWarning (null,"The reading is beyond expected km's of around " + maxDist.toString());
			        return false;
		        }
		        // no problems
		        MessagePanelControl.clearMessagePanel (null);
	        }
	        else
	        {	
		        //check out part
		        previousOdometerReading = parseInt(PrevOdoReading);
    		
		        if (parseInt(document.getElementById(CtrlToValidate).value) < previousOdometerReading)
		        {		//show error!
			        MessagePanelControl.setMessagePanelWarning (null,"The reading entered may be incorrect");
			        return false;
		        }
		        
		        // no problems
		        MessagePanelControl.clearMessagePanel (null);
	        }
    }
}

function UnitValidate(
    txtUnitNoClientId,
    lblVehicleDetailsClientId,
    txtOdometerOutClientId,
    btnHiddenRegoCheckerClientId,
    txtRegNoClientId,
    hdnWhosNextClientId )
{   
    document.getElementById(hdnWhosNextClientId).value = event.srcElement.id;

    if(document.getElementById(txtUnitNoClientId).value == '99999')
    {
        // skip actions if its already known that Unit No = 99999    
        if(event.srcElement.id != txtRegNoClientId)
        {
            document.getElementById(lblVehicleDetailsClientId).innerText='Cross Hire Vehicle';
            document.getElementById(txtOdometerOutClientId).value='0';
            //document.getElementById(btnHiddenRegoCheckerClientId).click();
            return true;
        }
    } 
    else 
    {
        document.getElementById(lblVehicleDetailsClientId).innerText='';
        document.getElementById(txtOdometerOutClientId).value='0';
    } 
    
    if (document.getElementById(txtUnitNoClientId).value == '' || document.getElementById(txtRegNoClientId).value == '' )
    {
        return false;
    }
    else
    {
        return true;
        //document.getElementById(btnHiddenRegoCheckerClientId).click();
    }
}

//Merged from a version checked in by Manny
function ChangeURL(url)
{
    if(document.getElementById('ctl00_titleLink')!=null)
    {
        document.getElementById('ctl00_titleLink').href= url;
    }
}
//Merged from a version checked in by Manny