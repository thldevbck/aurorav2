﻿// JScript File

    function gridViewCheckUncheckCheckboxes(headerCheckBoxId, gridViewId)
    {
        var CheckBox = document.getElementById(headerCheckBoxId);
        var TargetBaseControl = document.getElementById(gridViewId);
        
        checkUncheckCheckboxes(TargetBaseControl, CheckBox)
    }
    
//    function matchedProductGridViewCheckUncheckCheckboxes(CheckBoxId, matchedProductGridViewId)
//    {
//        var CheckBox = document.getElementById(CheckBoxId);
//        var TargetBaseControl = document.getElementById(matchedProductGridViewId);
//        
//        checkUncheckCheckboxes(TargetBaseControl, CheckBox)
//    }
//    
//    function travellerToMatchGridViewCheckUncheckCheckboxes(CheckBoxId,travellerToMatchGridViewId)
//    {
//        var CheckBox = document.getElementById(CheckBoxId);
//        var TargetBaseControl = document.getElementById(travellerToMatchGridViewId);
//        checkUncheckCheckboxes(TargetBaseControl, CheckBox)
//    }
     
    function checkUncheckCheckboxes(TargetBaseControl, CheckBox)
    {
        //get target base & child control.
        var TargetChildControl = "allCheckBox";

        //get all the control of the type INPUT in the base control.
        var Inputs = TargetBaseControl.getElementsByTagName("input");  

        for(var n = 0; n < Inputs.length; ++n)
            if(Inputs[n].type == 'checkbox')
                Inputs[n].checked = CheckBox.checked;
    }

    
