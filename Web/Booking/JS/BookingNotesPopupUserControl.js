﻿function notesPopup_typeDropDownList_onChange(oControl, rentalNoTextBoxId)
{
	var rentalNoTextBox = document.getElementById(rentalNoTextBoxId);

	if(oControl.value == '6EC7AE26-098F-40F5-8348-B154A63708A2')
	{
        rentalNoTextBox.value = ''
        rentalNoTextBox.readOnly = true;
		rentalNoTextBox.className = 'inputreadonly';
    } 
    else
    {
        rentalNoTextBox.readOnly = false;
		rentalNoTextBox.className = '';
    }
}

function notesPopup_rentalNoTextBox_onBlur(oControl, typeDropDownListId)
{
    var typeDropDownList = document.getElementById(typeDropDownListId);
    if (oControl.value == '')
        typeDropDownList.value = "6EC7AE26-098F-40F5-8348-B154A63708A2" //booking type
	else
		typeDropDownList.value = "471AEF57-626D-434A-ACA8-9E5E9112AC3F" //retnal type
}

