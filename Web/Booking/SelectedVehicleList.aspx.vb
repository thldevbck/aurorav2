Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports Aurora.ChkInOutRntCan.Services
Imports Aurora.Reservations.Services


<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.SelectedVehiclelList)> _
Partial Class Booking_SeletedVehicleList
    Inherits AuroraPage

#Region " Constants"
    Private Const SESSION_selectedVehicleList As String = "RS-SELVEHLST"
    Private Const SESSION_selectedVehicleListProperties As String = "selectedVehicleListProperties"
    Private Const SESSION_selectedVehicleList_xmlChosenVehicle As String = "xmlChosenVehicle"
    Private Const SESSION_selectedVehicleList_xmlCur As String = "xmlCur"
    Private Const SESSION_selectedVehicleList_xmlErrorMsg As String = "xmlErrorMsg"
    Private Const SESSION_selectedVehicleList_xmlRequest As String = "xmlRequest"

    ''rev:mia Feb.16
    Private Const QS_RS_VEHREQMGT As String = "VehicleRequestMgt.aspx?funCode=RS-VEHREQMGT"
    Private Const QS_RS_VEHREQRES As String = "VehicleRequestResultList.aspx?funCode=RS-VEHREQRES"
    Private Const QS_BOOKING As String = "Booking.aspx?funCode=RS-BOKSUMMGT"
    ''Private Const QS_BOOKING As String = "TestProductControl.aspx?funCode=RS-BOKSUMMGT"
    Private Const QS_RS_VEHREQMGTNBR As String = "VehicleRequestMgt.aspx"

#End Region

#Region " Variables"

    Protected xmlRequest As XmlDocument
    Protected xmlRequestSource As XmlDocument
    Protected xmlRequestKnockBack As XmlDocument
    Protected xmlErrorMsg As XmlDocument
    Protected xmlChosenVehicle As XmlDocument
    Protected xmlCur As XmlDocument
    Protected hashProperties As New Hashtable
    Protected xmlDefaultstring As String = "<xml><Data><Curr><code><value>0</value></code></Curr></Data></xml>"
    Protected gRecExist As Boolean = True
    Protected mTotalCheckItems As Decimal = 0.0


    ''rev:mia Feb.16
    Protected dictObject As New StringDictionary
    Private SESSION_VehicleRequestMgt As String '= "RS-VEHREQMGT" & Me.UserCode

#End Region

#Region " Methods"

   

    Function DispClientMessage(ByVal msgcode As String, ByVal msgparam As String) As String
        Dim msg As String = String.Empty
        Dim retvalue As String = String.Empty
        Dim param As String = String.Empty
        Dim paramString() As String = Nothing
        Dim i As Integer

        Dim ctr As Integer = Me.xmlErrorMsg.DocumentElement.ChildNodes(0).ChildNodes.Count - 1
        For i = 0 To ctr
            msg = Me.xmlErrorMsg.DocumentElement.SelectSingleNode("Msg").InnerText ''Me.xmlErrorMsg.DocumentElement.ChildNodes(0).ChildNodes(i).ChildNodes(0).InnerText
            retvalue = msg.IndexOf(msgcode)
            If retvalue <> -1 Then
                Exit For
            End If
        Next

        If i = ctr Then
            Return String.Empty
        End If

        param = msgparam
        If Not String.IsNullOrEmpty(param) Then
            paramString = param.Split(",")
            For ii As Integer = 0 To paramString.Length - 1
                If Not String.IsNullOrEmpty(paramString(ii)) Then
                    msg = msg.Replace("xxx" & (ii + 1), paramString(ii))
                End If
            Next
        End If

        For iii As Integer = 0 To paramString.Length - 1
            msg = msg.Replace("xxx" & (iii + 1), "")
        Next

    End Function

    Function checkVehicleSchedule(ByVal Parameter As String) As Boolean
        If Not (Parameter.Equals("NN") OrElse Parameter.Equals("KK")) Then
            Return True
        End If

        Dim ChkValue As String = "-1"
        Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
        Dim slvID As String = String.Empty
        Dim xmlstring As String = String.Empty
        Dim VehSel As String = String.Empty

        For i As Integer = 0 To ctr
            ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
            slvID = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("SlvId").InnerText
            VehSel = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("VehSel").InnerText
            If ChkValue.Equals("1") Then
                If Not String.IsNullOrEmpty(slvID) Then
                    If Parameter.Equals(slvID) Then
                        xmlstring = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.RES_IsAuroraVehicleSchedule('" & Parameter & "')")
                    Else
                        xmlstring = Aurora.Common.Data.ExecuteScalarSQL("SELECT dbo.RES_IsAuroraVehicleSchedule('" & slvID & "')")
                    End If

                    If Not String.IsNullOrEmpty(xmlstring) Then
                        xmlstring = xmlstring.Replace("<Data>", String.Empty)
                        xmlstring = xmlstring.Replace("</Data>", String.Empty).TrimEnd

                        If xmlstring.Equals("0") = True OrElse xmlstring.Equals(0) Then
                            Me.AlertMessage = "Status for Product " & VehSel
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, DispClientMessage("GEN095", "KK & NN," & Me.AlertMessage))
                            Return False
                        End If
                    End If
                End If
            End If
        Next

        Return True

    End Function

    Function ButtonValidation(ByVal modetype As String) As Boolean
        If modetype.Equals("IN") Or modetype.Equals("QN") Then Return True
        Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1

        Dim ChkValue As String = "-1"
        Dim avail As String = String.Empty
        Dim AvpBlocking As String = String.Empty


        For i As Integer = 0 To ctr
            ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
            avail = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Avail").InnerText
            AvpBlocking = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("AvpBlocking").InnerText

            If ChkValue.Equals("1") Then
                If avail.Equals("NA") AndAlso AvpBlocking.Equals("Bypass") Then
                    Me.AlertMessage = "Option only available when vehicle availability has been checked or \n when processing a \'free sell\' request."
                    Return False
                End If
            End If
        Next
        Return True
    End Function

    Function ChangeChoosen(ByVal myvalue As String) As Boolean

        If Me.gRecExist = False Then Exit Function
        Dim bcheck As Boolean = False
        Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1

        Dim ChkValue As String = "-1"

        For i As Integer = 0 To ctr
            ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
            If ChkValue.Equals("1") Then
                Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Status").InnerText = myvalue
                bcheck = True
            End If
            Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "0"
        Next
        If bcheck = False Then
            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select the record")
            Return False
        End If
        CType(GridView1.HeaderRow.Controls(10).FindControl("chkCheckHeader"), CheckBox).Checked = False
        CType(GridView1.FooterRow.FindControl("lblfootercost"), Label).Text = ""
        Return True
    End Function

    Function XMLRequestSourceDataset() As DataSet

        xmlRequestSource = New XmlDocument
        xmlRequestSource = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_CodeCodeType", "11", "", "")

        Dim xmlstring As String = xmlRequestSource.OuterXml.ToString
        Dim ds As DataSet
        ds = New DataSet
        ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
        Return ds

    End Function

    Function ValidData() As Boolean
        Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
        Dim status As String = String.Empty

        For i As Integer = 0 To ctr
            status = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Status").InnerText
            If String.IsNullOrEmpty(status) Then
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select the status")
                Return False
            End If
            If status.Equals("KB") AndAlso (String.IsNullOrEmpty(Me.ddlKnockBack.SelectedItem.Text)) Then
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "For Status KB KnockBack type must be selected")
                Return False
            End If
        Next
        Return True
    End Function

#End Region

#Region " Protected Properties"


    Protected Property Msg() As String
        Get
            Return CType(hashProperties("Msg"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("Msg") = value
        End Set
    End Property

    Protected Property TotalCheckItems() As String
        Get
            Return CType(hashProperties("TotalCheckItems"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("TotalCheckItems") = value
        End Set
    End Property


#End Region

#Region " Private Properties"

    Private Property VHRid() As String
        Get
            If String.IsNullOrEmpty(Me.HidVHRID.Value) Then
                Return CType(hashProperties("VHRid"), String)
            Else
                Return Me.HidVHRID.Value
            End If
        End Get
        Set(ByVal value As String)
            Me.HidVHRID.Value = value
            hashProperties("VHRid") = value
        End Set
    End Property
    Private Property AlertMessage() As String
        Get
            Return CType(hashProperties("AlertMessage"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("AlertMessage") = value
        End Set
    End Property

    Private Property SLVid() As String
        Get
            Return CType(hashProperties("SLVid"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("SLVid") = value
        End Set
    End Property

    Private Property CityCode() As String
        Get
            Return CType(hashProperties("CityCode"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("CityCode") = value
        End Set
    End Property

    Private Property BKRid() As String
        Get
            If String.IsNullOrEmpty(CType(hashProperties("BKRid"), String)) Then
                Return Me.HidBKRID.Value
            Else
                Return CType(hashProperties("BKRid"), String)
            End If
        End Get
        Set(ByVal value As String)
            hashProperties("BKRid") = value
        End Set
    End Property

    Private Property BooID() As String
        Get
            If String.IsNullOrEmpty(CType(hashProperties("BooID"), String)) Then
                Return Me.HidBOOID.Value
            Else
                Return CType(hashProperties("BooID"), String)
            End If
        End Get
        Set(ByVal value As String)
            hashProperties("BooID") = value
        End Set
    End Property

    Private Property ButtonSelection() As String
        Get
            If String.IsNullOrEmpty(CType(hashProperties("ButtonSelection"), String)) Then
                Return Me.Hidbuttonselection.Value
            Else
                Return CType(hashProperties("ButtonSelection"), String)
            End If
        End Get
        Set(ByVal value As String)
            hashProperties("ButtonSelection") = value
        End Set
    End Property

    Private Property BPDid() As String
        Get
            If String.IsNullOrEmpty(CType(hashProperties("BPDid"), String)) Then
                Return Me.HidBPDID.Value
            Else
                Return CType(hashProperties("BPDid"), String)
            End If
        End Get
        Set(ByVal value As String)
            hashProperties("BPDid") = value
        End Set
    End Property

    Private Property AgnDir() As String
        Get
            Return CType(hashProperties("AgnDir"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("AgnDir") = value
        End Set
    End Property

    Private Property BYpass() As String
        Get
            Return CType(hashProperties("BYpass"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("BYpass") = value
        End Set
    End Property

    Private Property HeaderMsg() As String
        Get
            Return CType(hashProperties("HeaderMsg"), String)
        End Get
        Set(ByVal value As String)
            hashProperties("HeaderMsg") = value
        End Set
    End Property

#End Region

#Region " Subs"

    Sub ToggleButton(ByVal bln As Boolean)
        If bln = True Then
            Me.BtnBack.Enabled = True
            Me.BtnConfirm.Enabled = True
            Me.BtnInquiry.Enabled = True
            Me.BtnKnockBack.Enabled = True
            Me.btnModifyBookingRequest.Enabled = True
            Me.btnNewModifyBookingRequest.Enabled = True
            Me.BtnProvisional.Enabled = True
            Me.BtnQuote.Enabled = True
            Me.BtnSave.Enabled = True
            Me.BtnSaveNext.Enabled = True
            Me.BtnWaitlist.Enabled = True
        Else
            Me.BtnBack.Enabled = False
            Me.BtnConfirm.Enabled = False
            Me.BtnInquiry.Enabled = False
            Me.BtnKnockBack.Enabled = False
            Me.btnModifyBookingRequest.Enabled = True
            Me.btnNewModifyBookingRequest.Enabled = True
            Me.BtnProvisional.Enabled = False
            Me.BtnQuote.Enabled = False
            Me.BtnSave.Enabled = False
            Me.BtnSaveNext.Enabled = False
            Me.BtnWaitlist.Enabled = False
        End If
    End Sub
    Sub NavigationTab()
        Dim index As Integer = 1
        Me.chkconvoy.TabIndex = index + 1

        Me.BtnInquiry.TabIndex = index + 1
        Me.BtnQuote.TabIndex = index + 1
        Me.BtnKnockBack.TabIndex = index + 1
        Me.BtnWaitlist.TabIndex = index + 1
        Me.BtnProvisional.TabIndex = index + 1
        Me.BtnConfirm.TabIndex = index + 1


        Me.txtNewBookingNumber.TabIndex = index + 1
        Me.ddlKnockBack.TabIndex = index + 1

        Me.BtnBack.TabIndex = index + 1
        Me.BtnSaveNext.TabIndex = index + 1
        Me.btnModifyBookingRequest.TabIndex = index + 1
        Me.btnNewModifyBookingRequest.TabIndex = index + 1

    End Sub

    Function AddIDinChosenVehicle(ByVal xmlstring As String, ByVal xpath As String, ByVal nodename As String) As String
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(xmlstring)
        Dim xmlatt As XmlAttribute
        Dim elem As XmlElement = xmldoc.DocumentElement

        Dim nodes As XmlNodeList = elem.SelectNodes(xpath)
        For Each node As XmlNode In nodes
            If node.Name = "SelectedVehicle" Then
                xmlatt = xmldoc.CreateAttribute(nodename)
                If Not node.ChildNodes(0).ChildNodes(0) Is Nothing Then
                    xmlatt.Value = node.ChildNodes(0).ChildNodes(0).InnerText
                Else
                    xmlatt.Value = ""
                End If
                ''xmlatt.Value = node.ChildNodes(0).ChildNodes(0).InnerText
                node.Attributes.Append(xmlatt)
            End If
        Next
        Return xmldoc.OuterXml
    End Function

    Sub SubmitData(ByVal modeCase As String)
        If (Me.gRecExist = False AndAlso modeCase.Equals("mbr") = False AndAlso modeCase.Equals("nbr")) Then
            Exit Sub
        End If
        If Me.AgnDir.Equals("0") OrElse Me.AgnDir.Equals(0) Then

        End If

        Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1

        Dim ChkValue As String = "-1"
        For i As Integer = 0 To ctr
            ChkValue = Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText
            If ChkValue.Equals("-1") Then
                Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "1"
            End If
        Next

        Select Case modeCase.ToUpper
            Case "IN"
                ButtonValidation("IN")
                Me.ChangeChoosen("IN")
            Case "QN", "NN", "KK"
                If Me.ButtonValidation(modeCase) = False Then Exit Sub
                If Me.checkVehicleSchedule(modeCase) = False Then
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select the record")
                    Exit Sub
                End If

                If Me.ChangeChoosen(modeCase) = False Then Exit Sub
                Me.RecalculateRates()
            Case "KB"
                If String.IsNullOrEmpty(Me.ddlKnockBack.SelectedItem.Text) Then
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please select a valid KnockBack type")
                    Exit Sub
                End If
                If Me.ButtonValidation(modeCase) = False Then Exit Sub
                If Me.checkVehicleSchedule(modeCase) = False Then Exit Sub
                If Me.ChangeChoosen(modeCase) = False Then Exit Sub
            Case "WL"
                If Me.ButtonValidation(modeCase) = False Then Exit Sub
                If Me.ChangeChoosen(modeCase) = False Then Exit Sub
            Case "MBR"
                Me.PutSessionForMBR()
                Response.Redirect(QS_RS_VEHREQMGT)
            Case "NBR"
                Me.HidBPDID.Value = String.Empty
                Me.HidBOOID.Value = String.Empty
                Me.HidBKRID.Value = String.Empty
                Me.RemoveSessionForNBR()
                Response.Redirect(QS_RS_VEHREQMGTNBR)
            Case "SAVE", "SAVENEXT"
                If Me.ValidData = False Then Exit Sub
                Me.txtNewBookingNumber.Text = Me.txtNewBookingNumber.Text.Replace(" ", "")
                Me.SaveAndNext(modeCase)
            Case "BACK"
                Me.PutSessionForMBR()
                Response.Redirect(QS_RS_VEHREQRES)
        End Select
        Me.BindGrid()
    End Sub

    Sub AddToSession()
        Session(SESSION_selectedVehicleList_xmlCur) = Me.xmlCur
        Session(SESSION_selectedVehicleList_xmlChosenVehicle) = xmlChosenVehicle
        Session(SESSION_selectedVehicleList_xmlErrorMsg) = xmlErrorMsg
        Session(SESSION_selectedVehicleListProperties) = hashProperties
        Session(SESSION_selectedVehicleList_xmlRequest) = Me.xmlRequest
    End Sub

    Sub GetFromSession()
        Me.xmlCur = CType(Session(SESSION_selectedVehicleList_xmlCur), XmlDocument)
        Me.xmlChosenVehicle = CType(Session(SESSION_selectedVehicleList_xmlChosenVehicle), XmlDocument)
        Me.xmlErrorMsg = CType(Session(SESSION_selectedVehicleList_xmlErrorMsg), XmlDocument)
        Me.hashProperties = CType(Session(SESSION_selectedVehicleListProperties), Hashtable)
        Me.xmlRequest = CType(Session(SESSION_selectedVehicleList_xmlRequest), XmlDocument)
    End Sub

    Sub RecalculateRates()
        Dim strBuilder As New StringBuilder
        With strBuilder
            .Append("<Root>")
            .Append(Me.xmlRequest.SelectSingleNode("/Data/Header").OuterXml)
            .Append(Me.xmlChosenVehicle.OuterXml)
            .AppendFormat("<BooId>{0}</BooId>", Me.HidBOOID.Value)
            .AppendFormat("<BpdId>{0}</BpdId>", Me.HidBPDID.Value)
            .AppendFormat("<UsrCode>{0}</UsrCode>", UserCode)
            .Append("</Root>")
        End With
        Dim xmlstring As String = SelectedVehicle.manageRecalculateRates(strBuilder.ToString)
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(xmlstring)
        If xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText.Equals("True") Then
            Dim msg As String = xmldoc.SelectSingleNode("//Root/Error/ErrDescription").InnerText
            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, msg)
        End If
        xmlstring = xmldoc.OuterXml
        xmlstring = xmlstring.Replace("</data>", "")
        xmlstring = xmlstring.Replace("<data>", "")
        xmldoc = Nothing
        xmldoc = New XmlDocument
        xmldoc.LoadXml(xmlstring)
        xmldoc.LoadXml(xmldoc.SelectSingleNode("//Data/SelectedVehicles").OuterXml)
        For i As Integer = 0 To xmldoc.DocumentElement.ChildNodes.Count - 1
            Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Cost").InnerText = xmldoc.DocumentElement.ChildNodes(i).SelectSingleNode("Cost").InnerText
        Next
        xmldoc = Nothing
    End Sub

    Sub PutSessionForMBR()
        
        If dictObject IsNot Nothing Then
            dictObject.Clear()
        End If

        dictObject("bkrid") = Me.BKRid
        dictObject("booid") = Me.BooID
        dictObject("bpdid") = Me.BPDid
        dictObject("butSel") = Me.ButtonSelection
        dictObject("vhrid") = Me.VHRid
        dictObject("Contact") = Me.hidContactID.Value
        
        Session(SESSION_VehicleRequestMgt) = dictObject
    End Sub

    Sub GetSessionContent()

        
        dictObject = CType(Session(SESSION_VehicleRequestMgt), StringDictionary)

        If dictObject IsNot Nothing Then
            Me.BKRid = dictObject("bkrid")
            Me.BooID = dictObject("booid")
            Me.BPDid = dictObject("bpdid")
            Me.ButtonSelection = dictObject("butSel")
            Me.VHRid = dictObject("vhrid")
            Me.hidContactID.Value = dictObject("Contact")
        End If


        ''for testing
        If Me.BKRid = "" Then
            Me.BKRid = "0AF3D455-9932-4357-AE05-6AB95F331867"
            Me.ButtonSelection = "Display Availability With Cost"
        End If

    End Sub

    ''rev:mia Feb.16
    Sub RemoveSessionForNBR()
        If Session(SESSION_VehicleRequestMgt) IsNot Nothing Then
            Session.Remove(SESSION_VehicleRequestMgt)
        End If
    End Sub


    Sub LoadXMLDocument(ByVal bkrid As String, ByVal booid As String, ByVal bpdid As String, ByVal butSel As String)
        Me.LoadXMLRequest(bkrid, booid, bpdid, butSel)

        Me.LoadXMLRequestKnockBack()
        Me.LoadXMLErrorMsg()
        Me.LoadXMLcur()
    End Sub

    Sub LoadXMLRequest(ByVal bkrid As String, ByVal booid As String, ByVal bpdid As String, ByVal butSel As String)

        xmlRequest = New XmlDocument
        xmlRequest = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_SummaryOfChosenVehicles", bkrid, booid, bpdid, butSel)

        Dim xmlstring As String = xmlRequest.OuterXml.ToString
        xmlstring = xmlstring.Replace("<data>", "<Data>")
        xmlstring = xmlstring.Replace("</data>", "</Data>")

        xmlRequest.LoadXml(xmlstring)

        Dim ds As DataSet
        ds = New DataSet
        ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

    End Sub

    Sub LoadXMLRequestKnockBack()

        xmlRequestKnockBack = New XmlDocument
        xmlRequestKnockBack = Aurora.Common.Data.ExecuteSqlXmlSPDoc("sp_get_CodeCodeType", "30", "", "")

        Dim xmlstring As String = xmlRequestKnockBack.OuterXml.ToString
        Dim ds As DataSet
        ds = New DataSet
        ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))

        Me.ddlKnockBack.DataSource = ds
        Me.ddlKnockBack.DataTextField = "CODE"
        Me.ddlKnockBack.DataValueField = "ID"
        Me.ddlKnockBack.DataBind()

        Me.ddlKnockBack.Items.Insert(0, String.Empty)
        Me.ddlKnockBack.SelectedIndex = -1

    End Sub

    Sub LoadXMLErrorMsg()

        xmlErrorMsg = New XmlDocument
        Dim xmlstring As String = Aurora.Common.Data.ExecuteScalarSP("GEN_getMsgToClient", "GEN095")
        xmlErrorMsg.LoadXml(xmlstring)

    End Sub

    Sub LoadXMLcur()
        xmlCur = New XmlDocument
        xmlCur.LoadXml(Me.xmlDefaultstring)
    End Sub

    Sub LoadScreen()
        GetSessionContent()
        LoadXMLDocument(Me.BKRid, Me.BooID, Me.BPDid, Me.ButtonSelection)
        Me.CityCode = xmlRequest.SelectSingleNode("//Data/Cty").InnerText
        Me.Msg = xmlRequest.SelectSingleNode("//Data/Msg").InnerText
        Me.SplitData()
        Me.GetCurrency()
        Me.ChangeData(False)

        If Me.xmlRequest.SelectSingleNode("//Data/Header[@BooNum]").Attributes(0).Value = "" Then
            MyBase.SetValueLink("", "")
        Else
            MyBase.SetValueLink((" :Booking :" & Me.xmlRequest.SelectSingleNode("//Data/Header[@BooNum]").Attributes(0).Value.ToString).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & Me.xmlRequest.SelectSingleNode("//Data/Header[@BooNum]").Attributes(0).Value.ToString)
        End If

        Dim ctr As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
        For i As Integer = 0 To ctr
            Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Poc").InnerText = "1"
        Next

        ''(xmlDso.selectSingleNode('//Data/EnabNewBoo').text == '0' || document.forms[0].hid_html_booId.value != '')
        If Me.xmlRequest.SelectSingleNode("//Data/EnabNewBoo").InnerText = "0" Or Not Me.HidBOOID.Value = "" Then
            Me.tr.Visible = False
        End If

        Me.AddToSession()
    End Sub

    Sub SplitData()
        xmlChosenVehicle = New XmlDocument
        Me.AgnDir = xmlRequest.SelectSingleNode("//Data/AgnDir").InnerText
        Me.Msg = xmlRequest.SelectSingleNode("//Data/Msg").InnerText
        Me.HeaderMsg = xmlRequest.SelectSingleNode("//Data/Header[@value]").Attributes(2).Value
        Me.lblselectedmsg.Text = Me.HeaderMsg


        Dim xmlstring As String = xmlRequest.SelectSingleNode("//Data/SelectedVehicles").OuterXml
        xmlstring = Me.AddIDinChosenVehicle(xmlstring, "/SelectedVehicles/SelectedVehicle", "id")

        xmlChosenVehicle.LoadXml(xmlstring)

        If xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle/SlvId").InnerText = "" Then
            xmlChosenVehicle = Nothing
            gRecExist = False
        End If

    End Sub

    Sub GetCurrency()
        Dim sb As New StringBuilder
        If gRecExist = False Then Exit Sub
        Dim bExist As Boolean = False

        Dim ctrChildnodes As Integer = xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
        Dim ctrXmlcurnodes As Integer = xmlCur.DocumentElement.ChildNodes.Count - 1

        Dim Xmlcurdata As String = String.Empty
        Dim Xmlchosendata As String = String.Empty

        For i As Integer = 0 To ctrChildnodes
            For ii As Integer = 0 To ctrXmlcurnodes
                Xmlcurdata = xmlCur.SelectSingleNode("//Data").ChildNodes(ii).SelectSingleNode("code").InnerText
                Xmlchosendata = xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Curr").InnerText
                If String.IsNullOrEmpty(Xmlcurdata) = False Then
                    If Xmlcurdata.Equals(Xmlchosendata) Then
                        bExist = True
                    End If
                Else
                    bExist = False
                End If
            Next

            If bExist = False Then
                With sb
                    .Append("<Curr>")
                    .AppendFormat("<code>{0}</code>", xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Curr").InnerText)
                    .Append("<value>0</value>")
                    .Append("</Curr>")
                End With
            End If
            If xmlCur IsNot Nothing Then
                xmlCur = Nothing
                xmlCur = New XmlDocument
            End If
            xmlCur.LoadXml("<Data>" & sb.ToString & "</Data>")
        Next



    End Sub

    Sub ChangeData(ByVal chkValue As Boolean)
        If gRecExist = False Then Exit Sub
        Dim ctrNode As Integer = xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
        For i As Integer = 0 To ctrNode
            xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Chk").InnerText = "0"
        Next

        If chkValue = False Then
            BindGrid()
        End If

    End Sub

    '<SelectedVehicle>
    '      <SlvId>0ECEE91C-ECC4-410F-8325-7BE391BF5A7E</SlvId>
    '      <PrdId>80646</PrdId>
    '      <RentalId>3294275/</RentalId>
    '      <CoDw>Thu</CoDw>
    '      <CoDate>21/02/2008</CoDate>
    '      <CoLoc>AKL</CoLoc>
    '      <CkoDayPart>AM</CkoDayPart>

    '      <CiDw>Fri</CiDw>
    '      <CiDate>21/03/2008</CiDate>
    '      <CiLoc>AKL</CiLoc>
    '      <CkiDayPart>PM</CkiDayPart>

    '      <Hper>30 DAY</Hper>
    '      <Hpkg>BFLY7NZ</Hpkg>

    '      <Veh>Britz</Veh>
    '      <VehSel>4BB</VehSel>

    '      <Avail>Yes</Avail>

    '      <AgnRef></AgnRef>
    '      <ReqSrc>FA3DD48685A048B2B8CB5E3829F4D2DA</ReqSrc>

    '      <Cost>4788.00</Cost>
    '      <Curr>NZD</Curr>

    '      <Status></Status>
    '      <Chk>0</Chk>
    '      <Poc>0</Poc>
    '      <AvpBlocking></AvpBlocking>
    '      <Force>0</Force>
    '      <VehicleRanges></VehicleRanges>
    '      <Visibility>10</Visibility>
    '      <Priority>1</Priority>
    '      <Revenue>4788.00</Revenue>
    '      <DvassNotified>0</DvassNotified>
    '      <IsThriftyUpdateDvass>1</IsThriftyUpdateDvass>
    '      <CtyCode>NZ</CtyCode>
    '    </SelectedVehicle>

    Sub BindGrid()
        Dim ds As New DataSet
        ds.ReadXml(New XmlTextReader(xmlChosenVehicle.OuterXml.ToString, System.Xml.XmlNodeType.Document, Nothing))
        GridView1.DataSource = ds
        GridView1.DataBind()
        Me.lblselectedmsg.Text = Me.HeaderMsg
    End Sub

    Sub CheckAll(ByVal controlname As String, ByVal check As Boolean)
        Dim chk As CheckBox = Nothing
        For Each row As GridViewRow In GridView1.Rows
            chk = row.FindControl(controlname)
            If chk IsNot Nothing Then
                chk.Checked = check
                If controlname = "chkCheck" Then
                    chk = GridView1.HeaderRow.Controls(10).FindControl("chkCheckHeader")
                    If chk IsNot Nothing Then
                        chk.Checked = check
                    End If
                End If
            End If
        Next
    End Sub

    Sub DisplayPOC(ByVal check As Boolean)
        Dim col As DataControlField = Nothing
        Dim i As Integer
        For i = 0 To GridView1.Columns.Count - 1
            col = GridView1.Columns(i)
            If col.HeaderText.Contains("Part") Then
                col.Visible = check
                CheckAll("chkPOC", True)
                CheckAll("chkCheck", False)
            End If
        Next

    End Sub

    Sub ClickCheck()
        If Me.gRecExist = False Then Exit Sub
        Dim curtotal As Decimal = 0
        Dim curStr As String = String.Empty
        Dim curr As String = String.Empty
        Dim cost As String = String.Empty
        Dim code As String = String.Empty
        Dim value As String = String.Empty

        Dim ctrChildnodes As Integer = Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
        Dim ctrXmlcurnodes As Integer = xmlCur.DocumentElement.ChildNodes.Count - 1

        Dim chk As String = String.Empty
        For i As Integer = 0 To ctrChildnodes
            chk = Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Chk").InnerText
            If chk.Equals("-1") Then
                Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Chk").InnerText = "1"
            ElseIf chk.Equals("1") Then
                For ii As Integer = 0 To ctrXmlcurnodes
                    curr = Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Curr").InnerText
                    code = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(ii).SelectSingleNode("code").InnerText
                    cost = Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles").ChildNodes(i).SelectSingleNode("Cost").InnerText
                    value = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(ii).SelectSingleNode("value").InnerText

                    If curr.Equals(code) AndAlso Not String.IsNullOrEmpty(cost) Then
                        curtotal = Decimal.Parse((value) + Decimal.Parse(cost))
                        Me.xmlCur.SelectSingleNode("/Data").ChildNodes(ii).SelectSingleNode("value").InnerText = curtotal

                    End If
                Next
            End If
        Next

        ctrXmlcurnodes = xmlCur.DocumentElement.ChildNodes.Count - 1
        For i As Integer = 0 To ctrXmlcurnodes
            value = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(i).SelectSingleNode("value").InnerText
            code = Me.xmlCur.SelectSingleNode("/Data").ChildNodes(i).SelectSingleNode("code").InnerText
            If Not value.Equals("0") Or Not value.Equals(0) Then
                curStr = curStr & value & "&nbsp;" & code
            End If
            Me.xmlCur.SelectSingleNode("/Data").ChildNodes(i).SelectSingleNode("value").InnerText = "0"
        Next
        If curStr.Length > 0 Then
            CType(GridView1.FooterRow.FindControl("lblfootercost"), Label).Text = curStr
        Else
            CType(GridView1.FooterRow.FindControl("lblfootercost"), Label).Text = ""
        End If

    End Sub

    Sub SaveAndNext(ByVal modecase As String)
        Dim sCitycode As String = IIf(String.IsNullOrEmpty(Me.CityCode), "", Me.CityCode)
        Dim parameter As String = "0"
        If modecase.ToUpper.Equals("SAVENEXT") Then
            parameter = "1"
        End If
        Dim sb As New StringBuilder
        With sb
            .Append("<Root>")
            .Append(Me.xmlRequest.SelectSingleNode("/Data/Header").OuterXml)
            .Append(Me.xmlChosenVehicle.OuterXml)
            .AppendFormat("<NewBooNum>{0}</NewBooNum>", Me.txtNewBookingNumber.Text)
            .AppendFormat("<BooId>{0}</BooId>", Me.HidBOOID.Value)
            .AppendFormat("<KBType>{0}</KBType>", Me.ddlKnockBack.SelectedValue)
            .AppendFormat("<UserCode>{0}</UserCode>", UserCode)
            .AppendFormat("<PrgName>{0}</PrgName>", Me.PrgmName)
            .Append("</Root>")
        End With
        Dim xmlstring As String = Aurora.ChkInOutRntCan.Services.BookingRentalMod.ManageSummaryOfChoosenVehicle(sb.ToString, sCitycode, parameter)
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml(xmlstring)
        Dim msg As String = xmldoc.SelectSingleNode("//Root/Error/ErrDescription").InnerText
        MyBase.SetShortMessage(AuroraHeaderMessageType.Information, msg)
        msg = xmldoc.SelectSingleNode("//Root/Error/ErrStatus").InnerText

        Me.PutSessionForMBR()

        If msg.Equals("False") Then
            If modecase.Equals("SAVENEXT") Then
                Dim booid As String = xmldoc.SelectSingleNode("//Root/Id/BooId").InnerText
                Dim rentid As String = xmldoc.SelectSingleNode("//Root/Id/RntId").InnerText
                If Not String.IsNullOrEmpty(booid) Then
                    Dim qstring As String = QS_BOOKING & "&hdBookingId=" & booid
                    qstring = qstring & "&hdRentalId=" & rentid
                    qstring = qstring & "&activeTab=9"
                    Response.Redirect(qstring)
                Else
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Booking Not-Yet Created")
                End If
            Else
                Me.xmlRequest.LoadXml(xmldoc.SelectSingleNode("//Root/Data").OuterXml)
                Me.SplitData()
            End If
        End If

    End Sub


#End Region

#Region " Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        SESSION_VehicleRequestMgt = "RS-VEHREQMGT" & Me.UserCode

        Try
            ToggleButton(True)
        
            If Not Page.IsPostBack Then
                Me.NavigationTab()
                Me.LoadScreen()
                Me.confirm_Inquiry.ConfirmText = Me.Msg
                Me.Confirm_Knockback.ConfirmText = Me.Msg
                Me.Confirm_Qoute.ConfirmText = Me.Msg
                Me.Confirm_Waitlisted.ConfirmText = Me.Msg
            Else
                Me.GetFromSession()
            End If

        Catch ex As Exception
            MyBase.AddErrorMessage(ex.Message)
            ToggleButton(False)
        End Try


    End Sub

    Protected Sub chkAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim chk As CheckBox = CType(sender, CheckBox)
        CheckAll("chkCheck", chk.Checked)
        
        For i As Integer = 0 To Me.xmlChosenVehicle.DocumentElement.ChildNodes.Count - 1
            If chk.Checked = True Then
                Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "1"
            Else
                Me.xmlChosenVehicle.DocumentElement.ChildNodes(i).SelectSingleNode("Chk").InnerText = "0"
            End If
        Next
        ClickCheck()
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        Dim index As Integer
        Dim col As DataControlField = Nothing
        Dim chkAll As CheckBox = Nothing

        Dim chk As CheckBox = Nothing

        Dim litControl As New Literal

        If e.Row.RowType = DataControlRowType.Header Then
            For index = 0 To GridView1.Columns.Count - 1
                col = GridView1.Columns(index)
                If col.HeaderText = "Check" Then
                    chkAll = New CheckBox
                    chkAll.ID = "chkCheckHeader"

                    chkAll.Visible = True
                    chkAll.AutoPostBack = "true"
                    litControl.Text = ("Checked")
                    litControl.Visible = True
                    AddHandler chkAll.CheckedChanged, AddressOf Me.chkAll_CheckedChanged
                    e.Row.Cells(index).Controls.Add(litControl)
                    e.Row.Cells(index).Controls.Add(chkAll)
                Else
                    If BYpass <> "" Then
                        If col.HeaderText = "Bypass" Then
                            If col.Visible = False Then
                                col.Visible = True
                            End If
                        End If
                    Else
                        If col.HeaderText = "Bypass" Then
                            If col.Visible = True Then
                                col.Visible = False
                            End If
                        End If
                    End If
                End If
            Next
        End If
    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound


        Dim ddlRS As DropDownList = Nothing
        If e.Row.RowType = DataControlRowType.DataRow Then

            ddlRS = CType(e.Row.FindControl("ddlRS"), DropDownList)

            ddlRS.DataSource = XMLRequestSourceDataset()
            ddlRS.DataTextField = "CODE"
            ddlRS.DataValueField = "ID"
            ddlRS.DataBind()
            ddlRS.SelectedValue = DataBinder.Eval(e.Row.DataItem, "ReqSrc")
        
        End If
    End Sub

    Protected Sub chkconvoy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkconvoy.CheckedChanged
        DisplayPOC(chkconvoy.Checked)
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim checkbox As CheckBox = CType(sender, CheckBox)
        Dim row As GridViewRow = CType(checkbox.NamingContainer, GridViewRow)
        Me.SLVid = GridView1.DataKeys(row.DataItemIndex).Value.ToString
        Dim item As String = GridView1.DataKeys(row.DataItemIndex).Value.ToString

        If Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & Me.SLVid & "']").Attributes(0).Value = GridView1.DataKeys(row.DataItemIndex).Value.ToString Then

            If checkbox.Checked = True Then
                If Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & Me.SLVid & "']").Attributes(0).Value = GridView1.DataKeys(row.DataItemIndex).Value.ToString Then
                    Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/Chk").InnerText = "1"
                End If
            Else
                Me.xmlChosenVehicle.SelectSingleNode("/SelectedVehicles/SelectedVehicle[@id='" & item & "']/Chk").InnerText = "0"

                CType(GridView1.FooterRow.FindControl("lblfootercost"), Label).Text = ""
            End If
            ClickCheck()
        End If
    

    End Sub

    Protected Sub BtnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConfirm.Click
        Me.SubmitData("KK")
    End Sub

    Protected Sub BtnInquiry_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnInquiry.Click
        Me.SubmitData("IN")
    End Sub

    Protected Sub btnNewModifyBookingRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewModifyBookingRequest.Click
        Me.SubmitData("NBR")
    End Sub

    Protected Sub BtnKnockBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnKnockBack.Click
        Me.SubmitData("KB")
    End Sub

    Protected Sub BtnWaitlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnWaitlist.Click
        Me.SubmitData("WL")
    End Sub

    Protected Sub BtnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSave.Click
        Me.SubmitData("SAVE")
    End Sub

    Protected Sub BtnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSaveNext.Click
        Me.SubmitData("SAVENEXT")
    End Sub

    Protected Sub BtnQuote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnQuote.Click
        Me.SubmitData("QN")
    End Sub

    Protected Sub BtnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnBack.Click
        Me.SubmitData("BACK")
    End Sub

    Protected Sub BtnProvisional_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnProvisional.Click
        Me.SubmitData("NN")
    End Sub

    Protected Sub btnModifyBookingRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.SubmitData("MBR")
    End Sub

#End Region

    
    
   
 
End Class
