<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	Booking.aspx
''	Date Created	-	?
''	Author		    -	Jack Leong
''	Modified Hist	-	?.?.? / Base version
''                      22.8.8 / Shoel / Fixes for SQUISH# 582 - otherrentalsgridview moved into update panel
''                      28.8.8 / Shoel / Modified for THRIFTY HISTORY 
''                      24.9.8 / Shoel / Added BookingIncidents.js
''                      10.10.8/ Shoel / Hotfix for thrifty tab JS issues
''                      july 20 2010 - mia - addition of capture billing token
''                      aug 13, 2013 - mia - addition of pricematching v2
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="Booking.aspx.vb" Inherits="Booking_Booking" Title="Untitled Page" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Src="~/Booking/Controls/BookingFilesUserControl.ascx" TagName="BookingFilesUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/BookingConfirmationUserControl.ascx" TagName="BookingConfirmationUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/BookingIncidentsUserControl.ascx" TagName="BookingIncidentsUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="Controls/BookingExtensionUserControl.ascx" TagName="BookingExtensionUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingNotesUserControl.ascx" TagName="BookingNotesUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingHistoryUserControl.ascx" TagName="BookingHistoryUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingSummaryUserControl.ascx" TagName="BookingSummaryUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingCustomerUserControl.ascx" TagName="BookingCustomerUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingCheckInCheckOutUserControl.ascx" TagName="BookingCheckInCheckOutUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingExchangeUserControl.ascx" TagName="BookingExchangeUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingInfringementUserControl.ascx" TagName="BookingInfringementUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfimationBoxControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingComplaintsUserControl.ascx" TagName="BookingComplaintsUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingProductUserControl.ascx" TagName="BookingProductUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingPaymentUserControl.ascx" TagName="BookingPaymentUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingCancellationUserControl.ascx" TagName="BookingCancellationUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingVehicleUserControl.ascx" TagName="BookingVehicleUserControl"
    TagPrefix="uc1" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc1" %>
<%--Added For Thrifty history--%>
<%@ Register Src="~/Booking/Controls/BookingThriftyHistoryUserControl.ascx" TagName="BookingThriftyHistoryUserControl"
    TagPrefix="uc1" %>
<%--Added For Thrifty history--%>
<%--rev:mia March 20, 2013 - addition for QuickAvail in booking. Note: it's my mom birthday today.--%>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="cbQuickAvailInBooking"
    TagPrefix="uc1" %>
<%@ Register Src="~/Booking/Controls/BookingPriceMatchUserControl.ascx" TagName="BookingPriceMatchUserControl"
    TagPrefix="uc1" %>

<%@ Register Src="~/Booking/Controls/BookingExperienceUserControl.ascx" TagName="BookingExperienceUserControl"
    TagPrefix="uc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
    <style type="text/css">
        .tabDivTable
        {
            border-right: #999999 1px solid;
            border-left: #999999 1px solid;
            border-bottom: #999999 1px solid;
            background-color: white;
        }
        .tabDiv
        {
            width: 100%;
            min-height: 250px;
            height: auto !important;
            height: 250px;
        }
        
        .ajax__tab_xp .ajax__tab_body
        {
            border-style: none solid;
            display: none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
    <script src="JS/CheckInCheckOut.js" type="text/javascript" language="javascript"></script>
    <%--<script src="JS/BookingMatchProductUserControl.js" type="text/javascript" language="javascript"></script>--%>
    <script src="JS/BookingCustomerUserControl.js" type="text/javascript" language="javascript"></script>
    <script src="JS/InfringementManagementPopupUserControl.js" type="text/javascript"
        language="javascript"></script>
    <script src="JS/AddNoteRatePopupUserControl.js" type="text/javascript" language="javascript"></script>
    <script src="JS/BookingHistory.js" type="text/javascript" language="javascript"></script>
    <script src="JS/BookingConfirmation.js" type="text/javascript" language="javascript"></script>
    <script src="JS/BookingNotesPopupUserControl.js" type="text/javascript" language="javascript"></script>
    <script src="JS/BookingIncident.js" type="text/javascript" language="javascript"></script>
    <%--<script src="../include/tiny_mce/tiny_mce.js" type="text/javascript" language="javascript"></script>--%>
    <script src="JS/PriceMatching.js" type="text/javascript"></script>
     <script type="text/javascript">

        function onPageSearch(searchParam) {
            if (searchParam != '') {
                GoToBooking()
            }
            else {
                var isFromSearchTextBox = document.getElementById('<%= isFromSearchTextBox.ClientID %>');
                if (isFromSearchTextBox.value == 1) {
                    document.forms[0].action = "./BookingSearch.aspx"
                    document.forms[0].method = "POST"
                    document.forms[0].submit();
                }
                else if (confirm("Are you sure you want to start a new Booking Request?")) {
                    document.forms[0].action = "./BookingProcess.aspx?funcode=RS-VEHREQMGT"
                    document.forms[0].method = "POST"
                    document.forms[0].submit();
                }
            }
        }

        function ShowCOReport() {
            window.open("../Reports/Default.aspx?funCode=RPT_COREPORT&popup=True", "", "height=650,width=810,resizable=no,scrollbars=yes,toolbar=no,status=yes,menubar=no");
            return false;
        }

        function ArrangeTabs(TabId) {
            try {
                var oTabs = document.getElementById(TabId);
                var oTabsHeader = oTabs.firstChild;
                var oStringArray = new Array();
                for (var i = 0; i < oTabsHeader.childNodes.length; i++) {
                    oStringArray[i] = oTabsHeader.childNodes[i].outerHTML;
                }
                // dont reshuffle!
                if (oStringArray[15].indexOf("Thrifty") > -1) {
                    oTabsHeader.innerHTML = oStringArray[0] + oStringArray[1] + oStringArray[2] + oStringArray[3] + oStringArray[4] + oStringArray[5] + oStringArray[6] + oStringArray[15] + oStringArray[7] + oStringArray[8] + oStringArray[9] + oStringArray[10] + oStringArray[11] + oStringArray[12] + oStringArray[13] + oStringArray[14];
                }
            }
            catch (err)
            { }
        }

        var pbControl = null;
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);

        function BeginRequestHandler(sender, args) {
            pbControl = args.get_postBackElement();  //the control causing the postback
            //alert(pbControl.id)
            if (pbControl.id.indexOf('okButton') > -1) {
                pbControl.disabled = true;
            }
            if (pbControl.id.indexOf('ctl00_ContentPlaceHolder_BookingPaymentUserControl1_repAmountOutstanding_') > -1) {
                $get('ctl00_ContentPlaceHolder_BookingPaymentUserControl1_btnAddPayment').disabled = true;
            }

            if (pbControl.id.indexOf('btnAddPayment') > -1) {
                pbControl.disabled = true;
            }
           
        }

        function EndRequestHandler(sender, args) {
           // alert(pbControl.id);
            if (pbControl.id.indexOf('okButton') > -1) {
                pbControl.disabled = false;
            }
            if (pbControl.id.indexOf('ctl00_ContentPlaceHolder_BookingPaymentUserControl1_repAmountOutstanding_') > -1) {
                $get('ctl00_ContentPlaceHolder_BookingPaymentUserControl1_btnAddPayment').disabled = false;
            }
            if (pbControl.id.indexOf('btnAddPayment') > -1) {
                pbControl.disabled = false;
            }

            //if its save then clear out the value of these hidden controls
            if (pbControl.id.indexOf('ButSave') > -1) {
                $("[id$=HiddenFieldHtmlStructure]").val("")
                $("[id$=HiddenFieldHtmlStructureForNotes]").val("")
            }

             pbControl = null;

        }

    </script>
   




 <script lang="javascript" type="text/javascript">
        

        function CurrencyFormatted(amount) {
            var i = parseFloat(amount);
            if (isNaN(i)) { i = 0.00; }
            var minus = '';
            if (i < 0) { minus = '-'; }
            i = Math.abs(i);
            i = parseInt((i + .005) * 100);
            i = i / 100;
            s = new String(i);
            if (s.indexOf('.') < 0) { s += '.00'; }
            if (s.indexOf('.') == (s.length - 2)) { s += '0'; }
            s = minus + s;
            return s;
        }

        var emptyPrice = false;
        var tempHtml = "";
        var subtotal = 0;
        var name = '';
        var offeredprice = 0;
        var pricevariance = 0;
        var value = 0;
        var total = 0;
        var textboxes;
        var label;
        var tempSubtotal = 0;
        var tempTotal = 0;
        var uom = 1;
        var oldRequest = '';
        var selvalue = '';
        var req = '';
        var hiddenFieldPriceMatchParameter = '';
        var PMPriceMSeqId = '';
        var usercode = '';
        var PMStatus = '';
        var approveTextBox;
        var changed = false;
        var rowcount = 0;
        var insuranceonly = 0
        var vehicleonly = 0
        var addononly = 0
        var insurancePrice_Orig_SingleItem = 0

        var SELECT_REQ_CONSTANT = "'Request Type' and  'Competitor Name' and 'Matching Status' must be selected."
        var REQUESTED_REQ_CONSTANT = "'Requested By:' field is empty."
        var APPROVED_REQ_CONSTANT = "'Approved By:' field is empty."
        var COMPETITOR_REQ_CONSTANT = "Competitor Price must have matching Competitor Product"
        var COMPETITOR_APPROVED_REQ_CONSTANT = "Competitor Price and Competitor Product must have a final Approved Price"
        var INVALID_PRICE_CONSTANT = "Invalid Price Matching Status"
        var INITIATED_REQ_CONSTANT = "Only 'Initiated' status is allowed at this stage."
        var ERROR_SAVING_CONSTANT = "Failed to save Price Matching.Please try again."
        var ERROR_NOTE_CONSTANT = "Failed to Load Note Price Matching.Please try again."
        var ERROR_PRICE_CONSTANT = "Failed to Load Price Matching.Please try again."
        var PLS_SELECT_CONSTANT = "--please select--"
        var PRICE_MATCH_CONSTANT = "Price Match"
        var INITIATED_CONSTANT = "Initiated"
        var OFFERED_CONSTANT = "Offered"
        var APPROVED_CONSTANT = "Approved"
        var BOOK_NOW_CAMPAIGN_CONSTANT = "Book Now Campaign"

        $(document).ready(function () {
       
            //rev:mia June.03 2013 - Addition of Price Match. 
            //                     - Today in history: june.03 1944 - 3 days before the DDay WWW2 Normandy landing--

            req = $('[id$=_ddlPMrequestType]').find('option:selected').text()
            comp = $("[id$=_ddlPMCompetitorName]").find("option:selected").text()
            match = $('[id$=_ddlPMmatchingStatus]').find('option:selected').text()
            hiddenFieldPriceMatchParameter = $("[id$=_HiddenFieldPriceMatchParameter]").val()
            PMPriceMSeqId = '<%= BookingPriceMatchUserControl.PMPriceMSeqId %>'
            usercode = '<%= UserCode %>'
            PMStatus = '<%=BookingPriceMatchUserControl.PMStatus %>'

            if ('<%= page.ispostback %>' == 'False') {
                oldRequest = req
            }

            if ('<%= HidePriceMatchButton%>' == 'True')
                $('[id=PriceMatchingDisplayButton]').hide()
            else
                $('[id=PriceMatchingDisplayButton]').show()

            if ('<%= BookingPriceMatchUserControl.PMPriceMSeqId %>' == '0') emptyPrice = true;


            if ('<%= BookingPriceMatchUserControl.PMPriceMSeqId %>' == '0') $('select[id$="_ddlPMmatchingStatus"]>option:eq(1)').attr('selected', true);


            $('[id=PriceMatchingDisplayButton]').click(function () {

                req = $('[id$=_ddlPMrequestType]').find('option:selected').text()
                if (req == PRICE_MATCH_CONSTANT || req == PLS_SELECT_CONSTANT || req == BOOK_NOW_CAMPAIGN_CONSTANT)
                    tempHtml = $("[id$=HiddenFieldHtmlStructure]").val()
                else
                    tempHtml = $("[id$=HiddenFieldHtmlStructureForNotes]").val()

                if (jQuery.trim(tempHtml) != '') {
                    $("[id=divPriceMatchLoad]").html(tempHtml)
                    PriceMatchComputation()
                    TableEnablingOrDisabling()
                }
                else {
                    LoadingMode()
                }
                LoadPopupPriceMatch()
            }); //$('[id=PriceMatchingDisplayButton]').click

            $('[id$=PriceMatchingCancelButtonButton]').click(function () {
                try {

                    //BookingPriceMatchUserControl.PMMatchingDropDown.SelectedItem.Text
                    selvalue = "<%= PMMatchingDropDownText %>"
                    $("[id$=_ddlPMmatchingStatus]  option:contains(" + selvalue + ")").attr('selected', true);

                    //BookingPriceMatchUserControl.PMCompetitorDropDown.SelectedItem.Text
                    selvalue = "<%= PMCompetitorDropDownText %>"
                    $("[id$=_ddlPMCompetitorName]  option:contains(" + selvalue + ")").attr('selected', true);

                    //BookingPriceMatchUserControl.PMrequestTypeDropDown.SelectedItem.Text
                    selvalue = "<%= PMrequestTypeDropDownText %>"
                    $("[id$=_ddlPMrequestType]  option:contains(" + selvalue + ")").attr('selected', true);

                    UnloadPopupPriceMatch()
                }
                catch (ex)
        { }


            }); //$('[id$=PriceMatchingCancelButtonButton]').click

            $('[id$=PriceMatchingSaveButton]').click(function () {
                if (Validate() == false) return false
                LoadPriceMatchDetails()
            }); //$('[id$=PriceMatchingSaveButton]').click

            //-------------------------------------------------------------------
            $('[id=linkToShowMoreNotes]').click(function () {
                LoadPopupShowMoreNotes()
            }); //$('[id=linkToShowMoreNotes]')

            $("[id$=_ddlPMCompetitorName]").change(function () {
                TableEnablingOrDisabling()
            }); //$("[id$=_ddlPMCompetitorName]").change

            $("[id$=_ddlPMmatchingStatus]").change(function () {
                match = $('[id$=_ddlPMmatchingStatus]').find('option:selected').text()
                var matchingstatus = match
                if (emptyPrice == false) {

                    if (matchingstatus.indexOf(INITIATED_CONSTANT) != -1) {
                        $("[id=ctl00_ContentPlaceHolder_BookingPriceMatchUserControl_decisionByTextBox]").attr("readonly", true)
                    }
                    else
                        $("[id=ctl00_ContentPlaceHolder_BookingPriceMatchUserControl_decisionByTextBox]").removeAttr("readonly")
                }
                else {
                    if (matchingstatus.indexOf(INITIATED_CONSTANT) == -1) {
                        setInformationShortMessage(INITIATED_REQ_CONSTANT)
                        $('select[id$="_ddlPMmatchingStatus"]>option:eq(1)').attr('selected', true);
                    }
                }
                ApprovedPriceEnablingOrDisabling()
            }); //$("[id$=_ddlPMmatchingStatus]").change


            $("[id$=_ddlPMrequestType]").change(function () {
                req = $('[id$=_ddlPMrequestType]').find('option:selected').text()
                if (req == PLS_SELECT_CONSTANT) {
                    setErrorShortMessage(SELECT_REQ_CONSTANT)
                    return
                }

                if (req == PRICE_MATCH_CONSTANT || req == BOOK_NOW_CAMPAIGN_CONSTANT) {
                    tempHtml = $("[id$=HiddenFieldHtmlStructure]").val()
                }
                else {
                    tempHtml = $("[id$=HiddenFieldHtmlStructureForNotes]").val()
                }

                if (jQuery.trim(tempHtml) != '') {
                    $("[id=divPriceMatchLoad]").html(tempHtml)
                    PriceMatchComputation()
                    TableEnablingOrDisabling()
                }
                else
                    LoadingMode()

            }); //$("[id$=_ddlPMrequestType]").change



        });                     //$(document).ready


        function LoadingMode() {
            req = $('[id$=_ddlPMrequestType]').find('option:selected').text()
            if (req.indexOf(PLS_SELECT_CONSTANT) != -1 || req.indexOf(PRICE_MATCH_CONSTANT) != -1 || req.indexOf(BOOK_NOW_CAMPAIGN_CONSTANT) != -1) {
                ExecuteLoadPriceMatching(hiddenFieldPriceMatchParameter)
            }
            else {
                ExecuteLoadNotePriceMatching(hiddenFieldPriceMatchParameter, "DONTDISPLAY")
            }
        }

        //rev:mia June.03 2013 - Addition of Price Match. 
        //                     - Today in history: june.03 1944 - 3 days before the DDay WWW2 Normandy landing--

        function ExecutePriceMatchingRequest(parameter, products, extraparameter) {
            $("[id=PriceMatchingSaveButton]").attr("disabled", "disabled")
            $("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").attr("disabled", true)
            var url = "PriceMatchingListener/PriceMatchingListener.aspx?mode=PRICEMATCHING"
            $.post(url, { PRICEMATCHING: parameter, PRICEMATCHINGPRODUCTS: products, EXTRAPARAMETER: extraparameter }, function (result) {
                if (result.indexOf('ERROR') != -1) {
                    setErrorShortMessage(ERROR_SAVING_CONSTANT)
                }
                else {
                    $("[id$=HiddenFieldHtmlStructure]").val("")
                    $("[id$=HiddenFieldHtmlStructureForNotes]").val("")
                    //$("[id=PriceMatchingSaveButton]").removeAttr("disabled")
                    //$("[id=priceMatchingTable] input, [id$=_decisionByTextBox], [id$=_requestedByTextBox]").removeAttr("disabled")
                    //window.location.reload(true);
                    window.location = window.location.href;
                }
            });
        }

        function PriceMatchComputation() {

            var hireperiod = $(".HirePeriod").text()



            //---------------------------------------------------------------------------
            //           DURING FIRST LOADING
            //---------------------------------------------------------------------------
            $(function () {

                $("input:text").focus(function () { $(this).select(); });
                $(".Vehicle,.Insurance,.Insurance_oum,.AddOns,.Total, .approvedpriceSubTotal,.approvedpriceSubTotal_oum,.approvedpriceTotal,.approvedpriceAddOns").keyup(function () {
                    this.value = this.value.replace(/[^0-9\.]/g, '');
                });

                //---------------------------------------------------------------------------
                // computation for Competitor Price
                //---------------------------------------------------------------------------

                //get subtotal
                total = 0
                tempSubtotal = 0;
                tempTotal = 0
                insuranceonly = 0
                vehicleonly = 0
                insurancePrice_Orig_SingleItem = 0
                textboxes = $("#priceMatchingTable").find("input.Vehicle,input.Insurance").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    if ($(this).attr("class").indexOf("Vehicle") != -1) {
                        vehicleonly = parseFloat(vehicleonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    if ($(this).attr("class").indexOf("Insurance") != -1) {
                        insuranceonly = parseFloat(insuranceonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    total = parseFloat(total) + (parseFloat($(this).val()) * parseInt(uom))
                });

                textboxes = $("#priceMatchingTable").find("input.Insurance_oum").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    insurancePrice_Orig_SingleItem = parseFloat(insurancePrice_Orig_SingleItem) + (parseFloat($(this).val()) * parseInt(uom))
                });

                total = total + parseFloat(insurancePrice_Orig_SingleItem)
                $("label.DailySubTotal").text(CurrencyFormatted(total))
                $("label.DailyTotal").text(CurrencyFormatted(((parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod)) + insurancePrice_Orig_SingleItem))

                //tempSubtotal = (parseFloat(vehicleonly) * parseInt(hireperiod)) + parseFloat(insuranceonly)
                tempSubtotal = (parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod)
                tempSubtotal = tempSubtotal + parseFloat(insurancePrice_Orig_SingleItem);
                insuranceonly = 0
                vehicleonly = 0
                //-----------------------------------------------------------------------------


                //get addons
                total = 0
                textboxes = $("#priceMatchingTable").find("input.AddOns,input.Total").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    //uom = $(this).closest("tr").attr("name")
                    //if (isNaN(uom)) uom = 1
                    total = parseFloat(total) + (parseFloat($(this).val()) * parseInt(uom))
                });
                tempTotal = total + tempSubtotal;


                //get total
                $("label.BookingTotalEOD").text(CurrencyFormatted(tempTotal))

                //---------------------------------------------------------------------------
                // computation for Price Variance 
                //---------------------------------------------------------------------------

                //price variance =  competitor price-offered price
                total = 0
                tempTotal = 0
                tempSubTotal = 0
                uom = 1
                textboxes = $("#priceMatchingTable").find("input.Vehicle,input.Insurance,input.Insurance_oum,input.AddOns,input.Total").filter(function () {
                    return $(this).val() >= 0
                });
                textboxes.each(function () {
                    name = $(this).attr("name")
                    offeredprice = $(this).closest('td').prev('td').prev('td').text()//$("label[name=' + name + ']").text()
                    pricevariance = parseFloat($(this).val()) - parseFloat(offeredprice)
                    $("label[id=" + name + "]").text(CurrencyFormatted(pricevariance))

                    approveTextBox = $(this).closest('td').next('td').next('td').next('td').find("input")
                    if (parseFloat(offeredprice) > parseFloat($(this).val())) {
                        if (match == INITIATED_CONSTANT) $(approveTextBox).val(jQuery.trim($(this).val()))
                        $(this).closest('td').next('td').next('td').css("background-color", "#E77471")
                    }
                    else {
                        if (parseFloat(offeredprice) < parseFloat($(this).val())) {
                            if (match == INITIATED_CONSTANT) $(approveTextBox).val(jQuery.trim((parseFloat(offeredprice) == 0 ? $(this).val() : offeredprice)))
                            $(this).closest('td').next('td').next('td').css("background-color", "#85BB65")
                        }
                        else {
                            if (match == INITIATED_CONSTANT) $(approveTextBox).val("0.00")
                            $(this).closest('td').next('td').next('td').css("background-color", "#FFFFFF")
                        }
                    }
                });

                //price variance sub total row
                total = 0
                label = $("#priceMatchingTable").find("._Vehicle, ._Insurance").filter(function () {
                    return $(this).text() != 0
                });
                label.each(function () {
                    total = parseFloat(total) + parseFloat($(this).text())
                });
                $("label.pricevarianceSubTotalRow").text(CurrencyFormatted(total))
                tempSubtotal = total //* hireperiod

                //price variance addon row
                total = 0
                label = $("#priceMatchingTable").find("._AddOns,._Total").filter(function () {
                    return $(this).text() != 0
                });
                label.each(function () {
                    total = parseFloat(total) + parseFloat($(this).text())
                });
                tempTotal = total + tempSubtotal

                //price variance for total row
                $("label.BookingTotalPriceVarianceEOD").text(CurrencyFormatted(tempTotal))


                //---------------------------------------------------------------------------
                // computation for APPROVED PRICE
                //---------------------------------------------------------------------------
                total = 0
                tempSubtotal = 0;
                insurancePrice_Orig_SingleItem = 0
                textboxes = $("#priceMatchingTable").find("input.approvedpriceSubTotal,input.approvedpriceSubTotal_oum").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    if ($(this).attr("id").indexOf("Vehicle") != -1) {
                        vehicleonly = parseFloat(vehicleonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    if ($(this).attr("id").indexOf("Insurance") != -1) {
                        if ($(this).attr("class").indexOf("oum") != -1)
                            insurancePrice_Orig_SingleItem = parseFloat(insurancePrice_Orig_SingleItem) + (parseFloat($(this).val()) * parseInt(uom))
                        else
                            insuranceonly = parseFloat(insuranceonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    if ($(this).attr("id").indexOf("AddOns") != -1) {
                        addononly = parseFloat(addononly) + (parseFloat($(this).val()) * parseInt(uom))
                    }

                    total = parseFloat(total) + (parseFloat($(this).val()) * parseInt(uom))
                });
                $("label.approvedpriceSubTotalRow").text(CurrencyFormatted(total))
                tempTotal = (((parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod)) + parseFloat(addononly))
                tempTotal = tempTotal + parseFloat(insurancePrice_Orig_SingleItem)
                $("label.BookingDailyTotalApprovedEOD").text(CurrencyFormatted(tempTotal))

                total = 0
                textboxes = $("#priceMatchingTable").find("input.approvedpriceTotal").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    total = parseFloat(total) + parseFloat($(this).val())
                });
                tempTotal = parseFloat(tempTotal) + parseFloat(total)
                $("label.BookingTotalApprovedEOD").text(CurrencyFormatted(tempTotal))




                //Total percentage
                total = 0
                var DailytotalOrigPrice = $("label.DailytotalOrigPrice").text()
                var DailytotalOfferedPrice = $("label.DailytotalOfferedPrice").text()
                total = (parseFloat(DailytotalOrigPrice) - parseFloat(DailytotalOfferedPrice)) / parseFloat(DailytotalOrigPrice)
                total = parseFloat(total) * 100
                $("label.DailytotalPercentage").text(CurrencyFormatted(total))

                //Booking Total percentage
                total = 0
                var BookingtotalOrigPrice = $("label.BookingtotalOrigPrice").text()
                var BookingtotalOfferedPrice = $("label.BookingtotalOfferedPrice").text()
                total = (parseFloat(BookingtotalOrigPrice) - parseFloat(BookingtotalOfferedPrice)) / parseFloat(BookingtotalOrigPrice)
                total = parseFloat(total) * 100
                $("label.BookingtotalPercentage").text(CurrencyFormatted(total))

                total = 0
                tempTotal = 0
                tempSubTotal = 0
                insuranceonly = 0
                vehicleonly = 0
                addononly = 0
                //uom = 1
                insurancePrice_Orig_SingleItem = 0


            });

            $("input.Vehicle, input.Insurance,input.Insurance_oum, input.Total,input.AddOns, input.approvedpriceSubTotal,input.approvedpriceSubTotal_oum, input.approvedpriceTotal, input.approvedpriceAddOns").blur(function () {
                value = $(this).val()
                if (!isNaN(value) && value != "") {
                    $(this).val(CurrencyFormatted(value))
                }

                //---------------------------------------------------------------------------
                // computation for Competitor Price
                //---------------------------------------------------------------------------
                //get subtotal
                total = 0
                tempSubtotal = 0;
                insuranceonly = 0
                vehicleonly = 0

                textboxes = $("#priceMatchingTable").find("input.Vehicle,input.Insurance").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    if ($(this).attr("class").indexOf("Vehicle") != -1) {
                        vehicleonly = parseFloat(vehicleonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    if ($(this).attr("class").indexOf("Insurance") != -1) {
                        insuranceonly = parseFloat(insuranceonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }

                    total = parseFloat(total) + (parseFloat($(this).val()) * parseInt(uom))
                });



                textboxes = $("#priceMatchingTable").find("input.Insurance_oum").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    insurancePrice_Orig_SingleItem = parseFloat(insurancePrice_Orig_SingleItem) + (parseFloat($(this).val()) * parseInt(uom))
                });

                total = total + parseFloat(insurancePrice_Orig_SingleItem)
                $("label.DailySubTotal").text(CurrencyFormatted(total))
                $("label.DailyTotal").text(CurrencyFormatted(((parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod)) + insurancePrice_Orig_SingleItem))

                //$("label.DailyTotal").text(CurrencyFormatted(((parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod))))


                tempSubtotal = (parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod)
                tempSubtotal = tempSubtotal + parseFloat(insurancePrice_Orig_SingleItem);
                insuranceonly = 0
                vehicleonly = 0
                insurancePrice_Orig_SingleItem = 0
                //-----------------------------------------------------------------------------


                //get addons
                total = 0
                textboxes = $("#priceMatchingTable").find("input.AddOns,input.Total").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    // uom = $(this).closest("tr").attr("name")
                    // if (isNaN(uom)) uom = 1
                    total = parseFloat(total) + (parseFloat($(this).val()) * parseInt(uom))
                });
                tempTotal = total + tempSubtotal;


                //get total
                $("label.BookingTotalEOD").text(CurrencyFormatted(tempTotal))

                //---------------------------------------------------------------------------
                // computation for Price Variance 
                //---------------------------------------------------------------------------

                //price variance = offered price - competitor price
                total = 0
                tempTotal = 0
                tempSubTotal = 0
                //uom =1
                textboxes = $("#priceMatchingTable").find("input.Vehicle,input.Insurance,input.Insurance_oum,input.AddOns,input.Total").filter(function () {
                    return $(this).val() >= 0
                });

                textboxes.each(function () {
                    name = $(this).attr("name")
                    offeredprice = $(this).closest('td').prev('td').prev('td').text()//$("label[name=' + name + ']").text()
                    pricevariance = parseFloat($(this).val()) - parseFloat(offeredprice)
                    $("label[id=" + name + "]").text(CurrencyFormatted(pricevariance))

                    approveTextBox = $(this).closest('td').next('td').next('td').next('td').find("input")
                    if (parseFloat(offeredprice) > parseFloat($(this).val())) {
                        if (match == INITIATED_CONSTANT) $(approveTextBox).val(jQuery.trim($(this).val()))
                        $(this).closest('td').next('td').next('td').css("background-color", "#E77471")
                    }
                    else {
                        if (parseFloat(offeredprice) < parseFloat($(this).val())) {
                            if (match == INITIATED_CONSTANT) {
                                $(approveTextBox).val(jQuery.trim((parseFloat(offeredprice) == 0 ? $(this).val() : offeredprice)))
                                $(this).closest('td').next('td').next('td').css("background-color", "#85BB65")
                            }
                        }
                        else {
                            if (match == INITIATED_CONSTANT) $(approveTextBox).val("0.00")
                            $(this).closest('td').next('td').next('td').css("background-color", "#FFFFFF")

                        }
                    }
                });

                //price variance sub total row
                total = 0
                label = $("#priceMatchingTable").find("._Vehicle, ._Insurance").filter(function () {
                    return $(this).text() != 0
                });

                label.each(function () {
                    total = parseFloat(total) + parseFloat($(this).text())
                });
                $("label.pricevarianceSubTotalRow").text(CurrencyFormatted(total))
                tempSubtotal = total

                //price variance addon row
                total = 0
                label = $("#priceMatchingTable").find("._AddOns,._Total").filter(function () {
                    return $(this).text() != 0
                });
                label.each(function () {
                    total = parseFloat(total) + parseFloat($(this).text())
                });
                tempTotal = total + tempSubtotal

                //price variance for total row
                $("label.BookingTotalPriceVarianceEOD").text(CurrencyFormatted(tempTotal))

                //---------------------------------------------------------------------------
                // computation for APPROVED PRICE
                //---------------------------------------------------------------------------
                total = 0
                tempSubtotal = 0;
                textboxes = $("#priceMatchingTable").find("input.approvedpriceSubTotal,input.approvedpriceSubTotal_oum").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    if ($(this).attr("id").indexOf("Vehicle") != -1) {
                        vehicleonly = parseFloat(vehicleonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    if ($(this).attr("id").indexOf("Insurance") != -1) {
                        if ($(this).attr("class").indexOf("oum") != -1)
                            insurancePrice_Orig_SingleItem = parseFloat(insurancePrice_Orig_SingleItem) + (parseFloat($(this).val()) * parseInt(uom))
                        else
                            insuranceonly = parseFloat(insuranceonly) + (parseFloat($(this).val()) * parseInt(uom))
                    }

                    if ($(this).attr("id").indexOf("AddOns") != -1) {
                        addononly = parseFloat(addononly) + (parseFloat($(this).val()) * parseInt(uom))
                    }
                    total = parseFloat(total) + parseFloat($(this).val())
                });
                $("label.approvedpriceSubTotalRow").text(CurrencyFormatted(total))
                tempTotal = (((parseFloat(vehicleonly) + parseFloat(insuranceonly)) * parseInt(hireperiod)) + parseFloat(addononly))
                tempTotal = tempTotal + parseFloat(insurancePrice_Orig_SingleItem)
                $("label.BookingDailyTotalApprovedEOD").text(CurrencyFormatted(tempTotal))

                total = 0
                textboxes = $("#priceMatchingTable").find("input.approvedpriceTotal").filter(function () {
                    return $(this).val() > 0
                });
                textboxes.each(function () {
                    total = parseFloat(total) + parseFloat($(this).val())
                });
                tempTotal = parseFloat(tempTotal) + parseFloat(total)
                $("label.BookingTotalApprovedEOD").text(CurrencyFormatted(tempTotal))

                total = 0
                tempTotal = 0
                tempSubTotal = 0
                insuranceonly = 0
                vehicleonly = 0
                addononly = 0
                insurancePrice_Orig_SingleItem = 0
            });



        }

        function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) { return false; }
        }



        function ExecuteLoadPriceMatching(parameter) {
            $("[id=PriceMatchingSaveButton]").attr("disabled", "disabled")

            if (parameter === undefined) parameter = '<%= PriceMatchParameter %>'
            var url = "PriceMatchingListener/PriceMatchingListener.aspx?mode=LOADPRICEMATCH"
            $.post(url, { LOADPRICEMATCH: parameter }, function (result) {
                if (result.indexOf('ERROR') != -1) {
                    setErrorShortMessage(ERROR_PRICE_CONSTANT)
                }
                else {

                    clearMessages()
                    clearShortMessage()
                    $("[id=divPriceMatchLoad]").html(result)

                    $("[id$=HiddenFieldHtmlStructure]").val(result)
                    $("[id=PriceMatchingSaveButton]").removeAttr("disabled")
                    PriceMatchComputation()
                    TableEnablingOrDisabling()


                    document.onkeypress = stopRKey;

                }
            });
        }

        function ExecuteLoadNotePriceMatching(parameter, mode) {

            $("[id=PriceMatchingSaveButton]").attr("disabled", "disabled")
            if (parameter === undefined) parameter = '<%= PriceMatchParameter %>'
            var url = "PriceMatchingListener/PriceMatchingListener.aspx?mode=LOADNOTEPRICEMATCH"
            $.post(url, { LOADNOTEPRICEMATCH: parameter }, function (result) {
                if (result.indexOf('ERROR') != -1) {
                    setErrorShortMessage(ERROR_NOTE_CONSTANT)
                }
                else {
                    if (mode == "SHOW") setInformationShortMessage("Note Price Matching Loaded!")
                    $("[id=divPriceMatchLoad]").html(result)

                    TableEnablingOrDisabling()
                    $("[id$=HiddenFieldHtmlStructureForNotes]").val(result)
                    $("[id=PriceMatchingSaveButton]").removeAttr("disabled")

                    document.onkeypress = stopRKey;
                }
            });
        }






        function DateComparison(DateA, DateB) {

            var a = new Date(DateA);
            var b = new Date(DateB);

            var msDateA = Date.UTC(a.getFullYear(), a.getMonth() + 1, a.getDate());
            var msDateB = Date.UTC(b.getFullYear(), b.getMonth() + 1, b.getDate());

            if (parseFloat(msDateA) < parseFloat(msDateB))
                return -1;  // lt
            else if (parseFloat(msDateA) == parseFloat(msDateB))
                return 0;  // eq
            else if (parseFloat(msDateA) > parseFloat(msDateB))
                return 1;  // gt
            else
                return null;  // error
        }
       
   
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <script src="EftposJS/EFTPOSbillingToken.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        Sys.Application.add_load(loadhandler);

        var dpsSubmitbtn = null;

        var pbControl = null;
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_beginRequest(BeginRequestHandler);
        prm.add_endRequest(EndRequestHandler);

        function EndRequestHandler(sender, args) { }

        function BeginRequestHandler(sender, args) { }

        function pageLoad(sender, args) { }

        function loadhandler() {
            var url = ''
            $("[id$=captureBillingToken]").click(function () {

                //alert('<%= countrycode %>')
                //alert('<%= RentalIdMerchantReference %>')
                //alert('<%= UserCode %>')

                $("#divMessage").html("<h4>Connecting to DPS...please wait</h4>")

                url = "EFTPOSListener/EftposPXPayListener.aspx?mode=NewBillingToken"
                var NewBillingTokenParameter = "<%= countrycode %>" + "|" + "<%=RentalIdMerchantReference %>" + "|" + "<%= UserCode %>"
                $.post(url, { NewBillingToken: NewBillingTokenParameter }, function (result) {

                    if (result.indexOf("ERROR") != -1) {
                        $("#divMessage h4").remove()
                        alert(result)
                        $("#divMessage").html("<h4>" + result + "</h4>")
                        //alert(result)
                    }
                    else {
                        $("#divMessage h4").remove()
                        $("[id$=_dpsFrame]").attr("src", result)

                    }
                })//$.post

            });    //$("[id$=_captureBillingToken]").click(function () {

            $("#btnTokenCancel").click(function () {
                $("#divMessage h4").remove()
                $("[id$=_dpsFrame]").attr("src", "")
            });


            $("[id=btnViewToken]").click(function () {
                url = "EFTPOSListener/EftposPXPayListener.aspx?mode=viewtoken"
                $.post(url, { viewtoken: "<%=RentalIdMerchantReference %>" }, function (result) {
                    $("#divCardlist").html("<br />" + result)
                })
                $("#divCardlist").slideToggle(500)
            });


            $('[id$=displayBillingToken]').click(function () {
                var viewtoken = "<%=RentalIdMerchantReference %>"
                url = "EFTPOSListener/EftposPXPayListener.aspx?mode=viewtoken"
                $.post(url, { viewtoken: viewtoken }, function (result) {
                    $('#divBillingToken').html(result)
                })
            })
        }

         
    </script>
    <div style="display: none">
        <uc1:DateControl ID="dateTemp" runat="server" />
        <asp:TextBox ID="isFromSearchTextBox" runat="server" />
    </div>
    <%--//rev:mia June.03 2013 - Addition of Price Match. 
    //                     - Today in history: june.03 1944 - 3 days before the DDay WWW2 Normandy landing----%>
    <asp:Panel ID="panelPriceMatch" runat="server" CssClass="modalPopup" Style="display: none;
        width: 700px; height: 550px; border-color: Black; border-width: 2px;">
        <uc1:BookingPriceMatchUserControl ID="BookingPriceMatchUserControl" runat="server" />
        <div id="divPriceMatch" style="position: relative; border: 0px solid #ccc">
        </div>
    </asp:Panel>
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <asp:UpdatePanel ID="updHeader" runat="server">
            <ContentTemplate>
                <table width="100%">
                    <tr>
                        <td width="6%">
                            Agent:
                        </td>
                        <td width="45%">
                            <asp:TextBox ID="agentTextBox" runat="server" ReadOnly="true" Width="330" />
                        </td>
                        <td width="6%">
                            Hirer:
                        </td>
                        <td width="30%">
                            <asp:TextBox ID="hirerTextBox" runat="server" ReadOnly="true" Width="200" />
                        </td>
                        <td width="6%">
                            Status:
                        </td>
                        <td>
                            <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="50" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
        <table width="100%">
            <tr>
                <td width="100%" colspan="2">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:GridView ID="rentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                                CssClass="dataTableGrid">
                                <AlternatingRowStyle CssClass="oddRow" />
                                <RowStyle CssClass="evenRow" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="30">
                                        <HeaderTemplate>
                                            Rental
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# GetRentalUrl(Eval("RntId")) %>'><%# Eval("RentalNo") %></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check-out">
                                        <ItemTemplate>
                                            <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-Out")) + GetArrivalDepartureTime(eval("RntArrivalAtBranchDateTime")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Check-in">
                                        <ItemTemplate>
                                            <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-In")) + GetArrivalDepartureTime(eval("RntDropOffAtBranchDateTime")) %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                                    <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" />
                                    <asp:BoundField DataField="Brand" HeaderText="Brand" ReadOnly="True" />
                                    <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                                    <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
                                    <asp:ButtonField Text="   " CommandName="Select" ItemStyle-Width="30" />
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <input id="dailyActivityButton1" value="Daily Activity" runat="server" type="button"
                        class="Button_Standard" visible="False" style="width: 100px;" />
                    <asp:Button ID="dailyActivityButton" runat="server" Text="Daily Activity" CssClass="Button_Standard"
                        Width="100" />
                    <asp:Button ID="coReportButton" runat="server" Text="CO Report" CssClass="Button_Standard Button_Report"
                        Width="100" OnClientClick="return ShowCOReport();" />
                </td>
                <td align="right">
                    <asp:Button ID="QuickAvailButton" runat="server" Text="Quick Avail" CssClass="Button_Standard Button_Search"
                        Width="100" Visible="true" OnClick="QuickAvailButton_Click" />
                    <input type="button" class="Button_Standard" style='width: 100px; visibility: <%= iif((cbool(HidePriceMatchButton) = true),"hidden","visible") %>'
                        value="Price Match" id="PriceMatchingDisplayButton" />
                    <asp:Button ID="addRentalButton" runat="server" Text="Add Rental" CssClass="Button_Standard Button_Add"
                        Width="100" />
                    <asp:Button ID="addBookingButton" runat="server" Text="Add Booking" CssClass="Button_Standard Button_Add"
                        Width="100" />
                    <asp:Button ID="bookingSearchButton" runat="server" Text="Booking Search" CssClass="Button_Standard Button_Search"
                        Width="130" />
                </td>
            </tr>
        </table>
        <table width="100%" runat="server" id="tblmessages">
            <tr>
                <td width="100%">
                    <b>Error have been found on this Booking</b>
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="red" />&nbsp;
                </td>
            </tr>
            <tr>
                <td width="100%">
                    <asp:Label ID="lblErrorNote" runat="server" Text="" />&nbsp;
                </td>
            </tr>
        </table>
        <uc1:CollapsiblePanel ID="detailsCollapsiblePanel" runat="server" Title="List of Other Rentals"
            TargetControlID="detailsContentPanel" />
        <asp:Panel ID="detailsContentPanel" runat="server" Width="100%">
            <table width="100%">
                <tr>
                    <td width="100%">
                        <asp:Label ID="otherRentalLabel" runat="server" Text="" Visible="false" />&nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                            <ContentTemplate>
                                <asp:GridView ID="otherRentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                                    CssClass="dataTableGrid">
                                    <AlternatingRowStyle CssClass="oddRow" />
                                    <RowStyle CssClass="evenRow" />
                                    <Columns>
                                        <asp:TemplateField Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="rentalIdLabel" runat="server" Text='<%# Bind("RntId") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="RentalNo" HeaderText="Rental" ReadOnly="True" />
                                        <asp:TemplateField HeaderText="Check-out">
                                            <ItemTemplate>
                                                <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-Out")) + GetArrivalDepartureTime(eval("RntArrivalAtBranchDateTime")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Check-in">
                                            <ItemTemplate>
                                                <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-In")) + GetArrivalDepartureTime(eval("RntDropOffAtBranchDateTime")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                                        <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" />
                                        <asp:BoundField DataField="Brand" HeaderText="Brand" ReadOnly="True" />
                                        <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                                        <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
                                        <asp:ButtonField Text="View" CommandName="Update" />
                                    </Columns>
                                </asp:GridView>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <%--<asp:UpdatePanel ID="tabUpdatePanel" runat="server">
            <ContentTemplate>--%>
        <ajaxToolkit:TabContainer ID="Tabs" runat="server" Width="100%" Height="0px" ActiveTabIndex="7"
            AutoPostBack="true">
            <ajaxToolkit:TabPanel runat="server" HeaderText="Check-out/in" ID="checkInOutTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label1" runat="server" Text="Check-out/in" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Changeover" ID="changeOverTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label2" runat="server" Text="Changeover" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Extension" ID="extensionTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label3" runat="server" Text="Extension" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Confirmation" ID="confirmationTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label4" runat="server" Text="Confirmation" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Complaints" ID="complaintsTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label5" runat="server" Text="Complaints" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Cancel" ID="cancelTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label6" runat="server" Text="Cancel" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Vehicle" ID="vehicleTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="Label7" runat="server" Text="Vehicle" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Notes" ID="notesTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Notes" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Customer" ID="customerTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Customer" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Products" ID="productTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Products" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Payments" ID="PaymentTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Payments" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Summary" ID="summaryTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Summary" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Infringements" ID="infringementTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Infringements" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Incidents" ID="incidentTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Incidents" Width="83" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="History" ID="historyTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="History" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Files" ID="fileTabPanel">
                <HeaderTemplate>
                    <asp:Label runat="server" Text="Files" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

         
            <ajaxToolkit:TabPanel runat="server" HeaderText="Experience" ID="ExperienceTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="lblExperience" runat="server" Text="Experience" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

            <%--Added For Thrifty history--%>
            <ajaxToolkit:TabPanel runat="server" HeaderText="Thrifty History" ID="thriftyhistoryTabPanel">
                <HeaderTemplate>
                    <asp:Label ID="lblThriftyHistory" runat="server" Text="Thrifty History" Width="82" />
                </HeaderTemplate>
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <%--Added For Thrifty history--%>

             
        </ajaxToolkit:TabContainer>
        <%--</ContentTemplate>
        </asp:UpdatePanel>--%>
    </asp:Panel>
    <asp:UpdatePanel ID="tabContentUpdatePanel" runat="server">
        <ContentTemplate>
            <table cellpadding="5" width="100%" class="tabDivTable">
                <tbody>
                    <tr>
                        <td style="width: 100%; padding-top: 10px;">
                            <div id="bookingCheckInCheckOutDiv" runat="server" class="tabDiv">
                                <uc1:BookingCheckInCheckOutUserControl ID="bookingCheckInCheckOutUserControl" runat="server" />
                            </div>
                            <div id="bookingExchangeDiv" runat="server" class="tabDiv">
                                <uc1:BookingExchangeUserControl ID="bookingExchangeUserControl" runat="server" />
                            </div>
                            <div id="bookingExtensionDiv" runat="server" class="tabDiv">
                                <uc1:BookingExtensionUserControl ID="bookingExtensionUserControl" runat="server" />
                            </div>
                            <div id="bookingConfirmationDiv" runat="server" class="tabDiv">
                                <uc1:BookingConfirmationUserControl ID="bookingConfirmationUserControl" runat="server" />
                            </div>
                            <div id="bookingComplaintsDiv" runat="server" class="tabDiv">
                                <uc1:BookingComplaintsUserControl ID="bookingComplaintsUserControl" runat="server" />
                            </div>
                            <div id="bookingCancellationDiv" runat="server" class="tabDiv">
                                <uc1:BookingCancellationUserControl ID="bookingCancellationUserControl" runat="server" />
                            </div>
                            <div id="bookingVehicleDiv" runat="server" class="tabDiv">
                                <uc1:BookingVehicleUserControl ID="bookingVehicleUserControl" runat="server" />
                            </div>
                            <div id="bookingNotesDiv" runat="server" class="tabDiv">
                                <uc1:BookingNotesUserControl ID="bookingNotesUserControl" runat="server" />
                            </div>
                            <div id="bookingCustomerDiv" runat="server" class="tabDiv">
                                <uc1:BookingCustomerUserControl ID="bookingCustomerUserControl" runat="server" />
                            </div>
                            <div id="bookingProductDiv" runat="server" class="tabDiv">
                                <uc1:BookingProductUserControl ID="BookingProductUserControl1" runat="server" />
                            </div>
                            <div id="BookingPaymentDiv" runat="server" class="tabDiv">
                                <uc1:BookingPaymentUserControl ID="BookingPaymentUserControl1" runat="server" />
                                <table width="100%">
                                    <tr align="right">
                                        <td>
                                            <asp:Button ID="displayBillingToken" runat="server" Text="Display Credit Card Authority"
                                                CssClass="Button_Standard" Width="200px" />
                                        </td>
                                    </tr>
                                    <tr align="right">
                                        <td>
                                            <asp:Button ID="captureBillingToken" runat="server" Text="Credit Card Authority"
                                                CssClass="Button_Standard  Button_Add" Width="200px" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="bookingSummaryDiv" runat="server" class="tabDiv">
                                <uc1:BookingSummaryUserControl ID="bookingSummaryUserControl" runat="server" />
                            </div>
                            <div id="bookingInfringementDiv" runat="server" class="tabDiv">
                                <uc1:BookingInfringementUserControl ID="bookingInfringementUserControl" runat="server" />
                            </div>
                            <div id="bookingFilesDiv" runat="server" class="tabDiv">
                                <uc1:BookingFilesUserControl ID="bookingFilesUserControl" runat="server" BasePath="BookingFiles"/>
                            </div>
                            <div id="bookingIncidentsDiv" runat="server" class="tabDiv">
                                <uc1:BookingIncidentsUserControl ID="bookingIncidentsUserControl" runat="server" />
                            </div>
                            <div id="bookingHistoryDiv" runat="server" class="tabDiv">
                                <uc1:BookingHistoryUserControl ID="bookingHistoryUserControl" runat="server" />
                            </div>

                            <div id="BookingExperienceUserControlDiv" runat="server" class="tabDiv">
                                <uc1:BookingExperienceUserControl ID="BookingExperienceUserControl" runat="server" />
                            </div>

                            <%--Added For Thrifty history--%>
                            <script language="javascript" type="text/javascript">
                                ArrangeTabs('<%= Tabs.ClientID %>');
                            </script>
                            <div id="bookingThriftyHistoryDiv" runat="server" class="tabDiv">
                                <uc1:BookingThriftyHistoryUserControl ID="bookingThriftyHistoryUserControl" runat="server" />
                            </div>
                            <%--Added For Thrifty history--%>
                              
                        </td>
                    </tr>
                </tbody>
            </table>
            <uc1:ConfimationBoxControl ID="confimationBoxControl" Title="Booking" runat="server" />
            <!--for capture billing token .css("border","1px solid white")-->
            <asp:Panel ID="panelBillingToken" runat="server" CssClass="modalPopup" Style="display: none;
                width: 500px; height: 500px;">
                <h2>
                    Credit Card Authority</h2>
                <div id="divMessage" style="position: relative; border: 1px solid #ccc">
                </div>
                <div id="container">
                    <iframe scrolling='yes' height='380px' width='100%' id='dpsFrame' runat="server" />
                </div>
                <hr />
                <table>
                    <tr align="right">
                        <td style="width: 360px;">
                        </td>
                        <td>
                            <input id="btnTokenCancel" type="button" value="Close" class="Button_Standard Button_Cancel"
                                style="position: absolute; bottom: 0.3em; right: 0.3em" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender BehaviorID="programmaticModalPopupBehaviorBillingToken"
                ID="ModalPopupExtenderBillingToken" runat="server" TargetControlID="captureBillingToken"
                PopupControlID="panelBillingToken" BackgroundCssClass="modalBackground" CancelControlID="btnTokenCancel">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="panelViewBillingToken" runat="server" CssClass="modalPopup" Style="display: none;
                width: 500px; height: 500px;">
                <h2>
                    Credit Card Authority</h2>
                <div id="divBillingToken" style="position: relative; border: 1px solid #ccc">
                </div>
                <table>
                    <tr align="right">
                        <td style="width: 360px;">
                        </td>
                        <td>
                            <input id="btnViewTokenCancel" type="button" value="Close" class="Button_Standard Button_Cancel"
                                style="position: absolute; bottom: 0.3em; right: 0.3em" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <ajaxToolkit:ModalPopupExtender BehaviorID="programmaticModalPopupBehaviorViewBillingToken"
                ID="ModalPopupExtenderViewBillingToken" runat="server" TargetControlID="displayBillingToken"
                PopupControlID="panelViewBillingToken" BackgroundCssClass="modalBackground" CancelControlID="btnViewTokenCancel">
            </ajaxToolkit:ModalPopupExtender>
            <uc1:cbQuickAvailInBooking ID="cbQuickAvailinBooking" runat="server" Title="Quick Availability"
                Text="" MessageType="Information" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="false"
                RightButton_Visible="false" />
            <%--rev:mia March 20, 2013 - addition for QuickAvail in booking
                                   - it's my mom birthday today.
            --%>
            <asp:Panel ID="panelPopupExtension" runat="server" CssClass="modalPopup" Style="display: none;
                width: 400px;">
                <div id="divAvailMessage" style="position: relative; border: 0px solid #ccc" />
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--rev:mia Oct.24 2012 - Addition of ThirdParty Reference Report. 
                            - Today in history: Oct.20 1944 - Gen.Douglas Macarthur landed in palo beach Leyte--%>
    <asp:Panel ID="panelPopupThirdPartyReference" runat="server" CssClass="modalPopup"
        Style="display: none; width: 900px; height: 580px;">
        <iframe id="ThirdPartyReferenceIFrame" scrolling='yes' runat="server" width="100%"
            height="550px" frameborder="0" style="overflow: hidden; background-color: #FFF">
        </iframe>
        <div id="divDescriptionMessage" style="position: relative; border: 0px solid #ccc" />
    </asp:Panel>
    <asp:Button ID="btnQuickAvailButtonInBooking" runat="server" Style="visibility: hidden" />
    <asp:HiddenField ID="HiddenFieldConfirmationBehaviorID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HiddenFieldVehicleExtension" runat="server" />
    <asp:HiddenField ID="HiddenFieldPriceMatchParameter" runat="server" />
    <asp:HiddenField ID="HiddenFieldHtmlStructure" runat="server" />
    <asp:HiddenField ID="HiddenFieldHtmlStructureForNotes" runat="server" />
  
</asp:Content>
