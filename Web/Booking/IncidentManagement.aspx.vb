'' Change Log
''   2.7.8 / Shoel - i think i have got the date format right. for now...  :)
''  30.7.8 / Shoel - Some misc bugs with date format, xml in ViewState fixed
''  22.8.8 / Shoel - Odometer fields now doesnt allow non-numeric data . fix for Squish#558 
''  24.9.8 / Shoel - Fixes for page behaviour when called from Change Over tab
''  6.11.8 / Shoel - HTML tag chars and spl chars now supported + the causes grid other text areas are now updated when u hit save :)
''  17.7.9 / Shoel - Fix for the bug where all incidents would be associated with the first vehicle in the list! 
''                   Also fixed the issue of the 01/01/1900 Resolved date appearing if the incident is unresolved

Imports System.Xml
Imports System.Data
Imports Aurora.Common.Utility
Imports Aurora.Common
Imports System.Transactions

<AuroraPageTitleAttribute("Incident Maintenance")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.Booking)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN005")> _
Partial Public Class Booking_IncidentManagement
    Inherits AuroraPage
    Dim RentalId, BookingId, IncidentId, BranchId, ActiveTabId, IncidentType As String

    Private Const codCdtNum As Integer = 48
    Dim details, status As String
    Dim sbLogText As StringBuilder
    Dim _loadException As Exception
    Dim usrFullName As String
    Dim blnResolved As Boolean

    Const ViewState_CAUSESTABLEDATA As String = "ViewState_Incidents_CausesTableData"
    Const ViewState_INCIDENTDATA As String = "ViewState_Incidents_IncidentData"
    Const ViewState_RENTALID As String = "ViewState_Incidents_RentalId"
    Const ViewState_BOOKINGID As String = "ViewState_Incidents_BookingId"
    Const ViewState_INCIDENTID As String = "ViewState_Incidents_IncidentId"
    Const ViewState_DETAILID As String = "ViewState_Incidents_DetailId"
    Const ViewState_DETAILSINTEGRITY As String = "ViewState_Incidents_DetailsIntegrity"
    Const ViewState_INCIDENTINTEGRITY As String = "ViewState_Incidents_IncidentIntegrity"
    Const ViewState_VECHICLETABLEDATA As String = "ViewState_Incidents_VehicleTableData"
    Const ViewState_ACTIONONCONFIRM As String = "ViewState_Incidents_ActionOnConfirm"
    Const ViewState_DUMMYCAUSEPRESENT As String = "ViewState_Incidents_DummyCausePresent"
    Const ViewState_BACKPATH As String = "ViewState_Incidents_BackPath"
    Const ViewState_UNITNO As String = "ViewState_Incidents_UnitNo"
    Const ViewState_NEWINCIDENTID As String = "ViewState_Incidents_NewIncidentId"

    Public TypeId As String = Aurora.Common.Data.ExecuteScalarSQL("select dbo.getcodeid(8,'Complaints') AS TypId")

    Delegate Sub ConfirmationBoxPostBackObject(ByVal isLeftButton As Boolean)


    Public Property UnitNumber() As String
        Get
            Return CStr(ViewState("IncidentMgt_UnitNo"))
        End Get
        Set(ByVal value As String)
            ViewState("IncidentMgt_UnitNo") = UCase(value)
        End Set
    End Property

    Public Property RegistrationNumber() As String
        Get
            Return CStr(ViewState("IncidentMgt_RegNo"))
        End Get
        Set(ByVal value As String)
            ViewState("IncidentMgt_RegNo") = UCase(value)
        End Set
    End Property

    Public Property FoundAMatchingRowInTheGrid() As Boolean
        Get
            Return CBool(ViewState("IncidentMgt_FoundMatch"))
        End Get
        Set(ByVal value As Boolean)
            ViewState("IncidentMgt_FoundMatch") = True
        End Set
    End Property


#Region "Form Events"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If (Session("usrFullName") Is Nothing) Then
            Dim dsUser As DataSet = Aurora.Booking.Services.BookingIncidents.GetUsernameforID(Aurora.Common.UserSettings.Current.UsrId)
            Session("usrFullName") = dsUser.Tables(0).Rows(0)(4).ToString
        Else
            usrFullName = Session("usrFullName")
        End If

        lblStatus.Style.Add("text-align", "right")
        blnResolved = False

        If Not IsPostBack Then
            ViewStateClearForNew()

            RentalId = Request.QueryString("RntId")
            ViewState(ViewState_RENTALID) = Request.QueryString("RntId")

            BookingId = Request.QueryString("BooId")
            ViewState(ViewState_BOOKINGID) = Request.QueryString("BooId")

            IncidentId = Request.QueryString("IncidentId")
            ViewState(ViewState_INCIDENTID) = Request.QueryString("IncidentId")

            BranchId = Request.QueryString("BranchId")
            IncidentType = Request.QueryString("IncidentType")

            ActiveTabId = Request.QueryString("ActiveTab")

            UnitNumber = Request.QueryString("UnitNum")
            RegistrationNumber = Request.QueryString("RegNum")

            dtLoggedDate.MessagePanelControlID = Me.Page.Controls(0).FindControl("shortMessagePanel").ClientID
            LoadNotificationTypes()
            LoadPage()

            If Not String.IsNullOrEmpty(BranchId) And Not String.IsNullOrEmpty(IncidentType) Then
                ' some branch is passed
                pkrBranch.DataId = BranchId

                ' hit the db
                Dim oXMLTemp As XmlDocument = New XmlDocument
                oXMLTemp = Aurora.Booking.Services.BookingIncidents.GetLocationDescriptionAndIncidentTypeId(BranchId, IncidentType)

                pkrBranch.Text = oXMLTemp.DocumentElement.SelectSingleNode("LocDesc").InnerText
            End If

            btnBack.Attributes.Add("OnClick", "return GoBack('" & _
                        "Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & BookingId & _
                        "&hdRentalId=" & RentalId & _
                        "&activeTab=" & ActiveTabId & _
                        "');")

            btnBack0.Attributes.Add("OnClick", "return GoBack('" & _
                       "Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & BookingId & _
                       "&hdRentalId=" & RentalId & _
                       "&activeTab=" & ActiveTabId & _
                       "');")
            Try

                If (IncidentId.Trim = "") Then
                    Session("IsNewIncident") = 1
                Else
                    Session("IsNewIncident") = 0
                End If
                populateIncidentStatusDropdown()

                If (ViewState(ViewState_INCIDENTID).ToString() <> "") Then
                    Dim dsIncidentStatus As DataSet = Aurora.Booking.Services.BookingIncidents.GetIncidentStatus(ViewState(ViewState_INCIDENTID).ToString())

                    If (dsIncidentStatus.Tables(0).Rows.Count > 0) Then
                        status = dsIncidentStatus.Tables(0).Rows(0)(0).ToString()
                        Dim dateStatus As DateTime = dsIncidentStatus.Tables(0).Rows(0)(1)
                        usrFullName = dsIncidentStatus.Tables(0).Rows(0)(2).ToString()
                        If (status.ToLower() = "resolved") Then
                            lblStatus.Text = "Issue resolved by user '" & usrFullName & "' on '" & dateStatus & "'"
                            lblStatus.Visible = True
                            cmbStatus.Enabled = False
                            txtDetails.Enabled = False
                            blnResolved = True
                            bindIncidentGridToIncidents()
                            cmbStatus.SelectedIndex = 2
                            Exit Sub
                        Else
                            lblStatus.Text = ""
                            lblStatus.Visible = False
                            txtDetails.Enabled = True
                            cmbStatus.Enabled = True
                            Dim indx As Integer
                            If (status.ToLower = "feedback") Then
                                indx = 0
                            ElseIf (status.ToLower = "open") Then
                                indx = 1
                            End If
                            cmbStatus.SelectedIndex = indx
                        End If
                    Else
                        lblStatus.Text = ""
                        lblStatus.Visible = False
                        txtDetails.Enabled = True
                        cmbStatus.Enabled = True
                        txtDetails.Enabled = True
                        status = "feedback"
                    End If

                Else
                    lblStatus.Text = ""
                    lblStatus.Visible = False
                    txtDetails.Enabled = True
                    cmbStatus.Enabled = True
                    txtDetails.Enabled = True
                    status = "feedback"
                    txtDetails.Enabled = True
                End If

                details = ""
                txtDetails.Text = ""


            Catch ex As Exception
                Me.AddErrorMessage("Error - Error on page load 'Is not PostBack' section : " & ex.Message)
            End Try

        Else

            'Try
            If (Session("IsNewIncident") = 1 Or ViewState(ViewState_INCIDENTID) Is Nothing) Then
                status = cmbStatus.SelectedItem.Text.ToLower
                Exit Sub
            End If

            IncidentId = ViewState(ViewState_INCIDENTID)

            Dim dsIncidentStatus As DataSet = Aurora.Booking.Services.BookingIncidents.GetIncidentStatus(ViewState(ViewState_INCIDENTID).ToString())

            If (dsIncidentStatus.Tables(0).Rows.Count > 0) Then
                status = dsIncidentStatus.Tables(0).Rows(0)(0).ToString()
                Dim dateStatus As DateTime = dsIncidentStatus.Tables(0).Rows(0)(1)
                usrFullName = dsIncidentStatus.Tables(0).Rows(0)(2).ToString()

                'If (status.ToLower = "open" And cmbStatus.SelectedItem.Text.ToLower = "feedback") Then
                'cmbStatus.ClearSelection()
                'cmbStatus.Items.FindByText(status).Selected = True
                'End If

                If (status.ToLower() = "resolved") Then
                    If (status.ToLower() = "resolved") Then
                        lblStatus.Text = "Issue resolved by user '" & usrFullName & "' on '" & dateStatus & "'"
                        lblStatus.Visible = True
                        cmbStatus.Enabled = False
                        txtDetails.Enabled = False
                        blnResolved = True
                        cmbStatus.SelectedIndex = 2
                        Exit Sub
                    Else
                        cmbStatus.Enabled = True
                        lblStatus.Visible = False
                        txtDetails.Enabled = True
                    End If


                    If (cmbStatus.SelectedItem.Text.ToLower = "resolved" And status = "resolved") Then
                        txtDetails.Enabled = False
                    Else
                        ''If (status = "open" Or cmbStatus.SelectedItem.Text.ToLower = "open" Or cmbStatus.SelectedItem.Text.ToLower = "resolved") Then
                        txtDetails.Enabled = True
                        ''Else
                        ''    txtDetails.Enabled = True
                        ''End If

                    End If
                Else
                    lblStatus.Text = ""
                    lblStatus.Visible = False
                    txtDetails.Enabled = True
                    'Dim indx As Integer
                    'If (cmbStatus.SelectedItem.Text.ToLower = "feedback") Then
                    '    indx = 0
                    '    status = "feedback"
                    'ElseIf (cmbStatus.SelectedItem.Text.ToLower = "open") Then
                    '    indx = 1
                    '    status = "open"
                    'ElseIf (cmbStatus.SelectedItem.Text.ToLower = "open") Then
                    '    indx = 1
                    '    status = "resolved"
                    'End If
                    'cmbStatus.SelectedIndex = indx
                End If
            Else
                'If (txtDetails.Text.Trim <> "") Then
                status = cmbStatus.SelectedItem.Text
                '    blnNoCurRows = True
                'Else
                '    status = "open"
                'End If
                txtDetails.Enabled = True
                lblStatus.Visible = False
                cmbStatus.Enabled = True
                'If (cmbStatus.SelectedItem.Text.ToLower = "open" Or cmbStatus.SelectedItem.Text.ToLower = "resolved") Then
                '    txtDetails.Enabled = True
                'Else
                '    txtDetails.Enabled = False
                'End If

            End If
            ViewState("details") = txtDetails.Text.Trim
            'txtDetails.Text = ""
            'Catch ex As Exception
            '    Me.AddErrorMessage("Error - Error on page load 'Is PostBack' section : " & ex.Message)
            '    Exit Sub
            'End Try

        End If

        bindIncidentGridToIncidents()

    End Sub

    Protected Sub populateIncidentStatusDropdown()
        Dim dsStatusCodes As New DataSet
        Dim dsIncidentStatus As New DataSet


        Try
            dsStatusCodes = Aurora.Booking.Services.BookingIncidents.GetStatusCodes(codCdtNum)
            cmbStatus.Items.Clear()
            cmbStatus.DataSource = dsStatusCodes.Tables(0)
            cmbStatus.DataTextField = "codcode"
            cmbStatus.DataValueField = "codid"
            cmbStatus.DataBind()

            'If (ViewState(ViewState_INCIDENTID).ToString() <> "") Then
            '    cmbStatus.ClearSelection()
            '    'Check the status of the incident
            '    dsIncidentStatus = Aurora.Booking.Services.BookingIncidents.GetIncidentStatus(ViewState(ViewState_INCIDENTID).ToString())
            '    If (dsIncidentStatus.Tables(0).Rows.Count > 0) Then
            '        'get username and datetime for the recorded status
            '        Dim status As String = dsIncidentStatus.Tables(0).Rows(0)(0).ToString()
            '        Dim dateStatus As DateTime = dsIncidentStatus.Tables(0).Rows(0)(1)
            '        usrFullName = dsIncidentStatus.Tables(0).Rows(0)(2).ToString()

            '        If (status.ToLower = "open" And cmbStatus.SelectedItem.Text.ToLower = "feedback") Then
            '            cmbStatus.ClearSelection()
            '            cmbStatus.Items.FindByText(status).Selected = True
            '        End If

            '        If (status.ToLower() = "resolved") Then

            '            If (status.ToLower() = "resolved") Then
            '                lblStatus.Text = "Issue resolved by user '" & usrFullName & "' on '" & dateStatus & "'"
            '                lblStatus.Visible = True
            '                cmbStatus.Enabled = False
            '            Else
            '                lblStatus.Visible = False
            '                cmbStatus.Enabled = True
            '            End If

            '            If (cmbStatus.SelectedItem.Text.ToLower = "resolved" And status = "resolved") Then
            '                txtDetails.Enabled = False
            '            Else
            '                If (status = "open" Or cmbStatus.SelectedItem.Text.ToLower = "open" Or cmbStatus.SelectedItem.Text.ToLower = "resolved") Then
            '                    txtDetails.Enabled = True
            '                Else
            '                    txtDetails.Enabled = False
            '                End If
            '            End If
            '            cmbStatus.ClearSelection()
            '            cmbStatus.Items.FindByText(status).Selected = True
            '        Else
            '            lblStatus.Text = ""
            '            lblStatus.Visible = False
            '            txtDetails.Enabled = True
            '            cmbStatus.ClearSelection()
            '            cmbStatus.Items.FindByText(status).Selected = True
            '        End If

            '    Else
            '        cmbStatus.Enabled = True
            '        If (cmbStatus.SelectedItem.Text.ToLower = "open" Or cmbStatus.SelectedItem.Text.ToLower = "resolved") Then
            '            txtDetails.Enabled = True
            '        Else
            '            If (status Is Nothing) Then
            '                txtDetails.Enabled = True
            '            Else
            '                If (status.ToLower = "resolved") Then
            '                    txtDetails.Enabled = False
            '                End If
            '            End If


            '        End If
            '    End If
            'Else
            '    status = "feedback"
            'End If
        Catch ex As Exception
            Me.AddErrorMessage("Error - Populating Incident Status Dropdown : " & ex.Message)
        End Try
    End Sub

    Protected Sub bindIncidentGridToIncidents()
        Dim dsIncidents As New DataSet
        Dim dtIncidentsNew As New DataTable
        Dim j As Integer = 0

        Dim num As DataColumn = New DataColumn("num")
        num.DataType = System.Type.GetType("System.Int32")
        dtIncidentsNew.Columns.Add(num)

        Dim callBackID As DataColumn = New DataColumn("callBackID")
        callBackID.DataType = System.Type.GetType("System.Int64")
        dtIncidentsNew.Columns.Add(callBackID)

        Dim callBackDetails As DataColumn = New DataColumn("callBackDetails")
        callBackDetails.DataType = System.Type.GetType("System.String")
        dtIncidentsNew.Columns.Add(callBackDetails)

        Dim callBackUser As DataColumn = New DataColumn("callBackUser")
        callBackUser.DataType = System.Type.GetType("System.String")
        dtIncidentsNew.Columns.Add(callBackUser)

        Dim callBackTime As DataColumn = New DataColumn("callBackTime")
        callBackTime.DataType = System.Type.GetType("System.DateTime")
        dtIncidentsNew.Columns.Add(callBackTime)


        If (Not ViewState(ViewState_INCIDENTID) Is Nothing) Then
            If (ViewState(ViewState_INCIDENTID).ToString() <> "") Then
                dsIncidents = Aurora.Booking.Services.BookingIncidents.GetIncidentCallbacks(ViewState(ViewState_INCIDENTID).ToString())

                For j = 0 To dsIncidents.Tables(0).Rows.Count - 1
                    Dim dr As DataRow
                    dr = dtIncidentsNew.NewRow()
                    dr(0) = j + 1
                    dr(1) = dsIncidents.Tables(0).Rows(j)(0)
                    dr(2) = dsIncidents.Tables(0).Rows(j)(1)
                    dr(3) = dsIncidents.Tables(0).Rows(j)(2)
                    dr(4) = dsIncidents.Tables(0).Rows(j)(3)
                    dtIncidentsNew.Rows.Add(dr)
                Next

                gridViewIncidents.DataSource = dtIncidentsNew
                gridViewIncidents.DataBind()
            End If
        Else
            dtIncidentsNew.Rows.Clear()
            gridViewIncidents.DataSource = dtIncidentsNew
            gridViewIncidents.DataBind()
        End If

    End Sub

    'Protected Sub gridViewIncidents_RowEditing(ByVal sender As Object, ByVal e As GridViewEditEventArgs)
    '    gridViewIncidents.EditIndex = e.NewEditIndex
    'End Sub

    Protected Sub SaveCallbackData()
        Dim intCallbackID As Int64 = 0
        Dim intCallbackStatusID As Int64 = 0
        Dim err As Integer = 0
        Dim dsIncidentStatus As DataSet

        Try
            Using scope As New TransactionScope()
                sbLogText = New StringBuilder
                If (details <> "") Then
                    intCallbackID = Aurora.Booking.Services.BookingIncidents.SaveCallbackData(ViewState(ViewState_INCIDENTID).ToString(), details)
                End If
                intCallbackStatusID = Aurora.Booking.Services.BookingIncidents.SaveCallbackStatus(ViewState(ViewState_INCIDENTID).ToString(), cmbStatus.SelectedValue.ToString())
                err = Aurora.Booking.Services.BookingIncidents.SaveCallbackStatusDate(intCallbackStatusID)

                scope.Complete()
            End Using
        Catch ex As TransactionAbortedException
            sbLogText.Length = 0
            sbLogText.Append("Exception while adding callbacks to incident callback tables.")
            sbLogText.Append("Message :" & ex.Message)
            sbLogText.AppendLine("Trace :" & ex.StackTrace)
            Logging.LogError("Exception", sbLogText.ToString())
            _loadException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        Catch ex As Exception
            sbLogText.Length = 0
            sbLogText.AppendLine("Exception while adding callbacks to incident callback tables.")
            sbLogText.AppendLine("Message :" & ex.Message)
            sbLogText.AppendLine("Trace :" & ex.StackTrace)
            Logging.LogError("Exception", sbLogText.ToString())
            _loadException = ex
            Me.AddErrorMessage(Server.HtmlEncode(ex.Message))
        End Try

        bindIncidentGridToIncidents()

        'Check the status of the incident
        dsIncidentStatus = Aurora.Booking.Services.BookingIncidents.GetIncidentStatus(ViewState(ViewState_INCIDENTID).ToString())

        If (dsIncidentStatus.Tables(0).Rows.Count > 0) Then
            If (cmbStatus.SelectedItem.Text.ToLower() = "resolved") Then

                'get username and datetime for the recorded status
                Dim status As String = dsIncidentStatus.Tables(0).Rows(0)(0)
                Dim dateStatus As DateTime = dsIncidentStatus.Tables(0).Rows(0)(1)
                Dim user As String = dsIncidentStatus.Tables(0).Rows(0)(2)

                lblStatus.Text = "Issue resolved by user '" & user & "' on '" & dateStatus & "'"
                lblStatus.Visible = True
            Else
                lblStatus.Text = ""
                lblStatus.Visible = False
            End If
        End If



    End Sub


    Protected Sub gwCauses_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwCauses.RowCreated
        e.Row.Cells(0).Visible = False
        e.Row.Cells(1).Visible = False
        e.Row.Cells(6).Visible = False
        e.Row.Cells(7).Visible = False
    End Sub

    Protected Sub cmbCauses_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCauses.SelectedIndexChanged
        'look for its sub causes
        If Not cmbCauses.SelectedValue = "" Then
            LoadSubCausesCombo(cmbCauses.SelectedValue)
        Else
            'blank selected
            For i As Int16 = cmbSubCause.Items.Count - 1 To 0 Step -1
                cmbSubCause.Items.RemoveAt(i)
                'clearin the combo
            Next
        End If

    End Sub




    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'check if the drop downs have info
        If cmbCauses.SelectedValue = "" Then
            SetErrorMessage("Cause must be selected")
            Exit Sub
        End If
        If cmbSubCause.SelectedValue = "" Then
            SetErrorMessage("Sub-Cause must be selected")
            Exit Sub
        End If
        'nothing missing.. GO!!
        'as per old sys
        'lock out the type combo!
        BuildCauseDetailsXML(True)
        cmbCauses.ClearSelection()
        cmbSubCause.ClearSelection()

    End Sub


    Protected Sub gwCauses_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            If CType(e.Row.Cells(5).FindControl("chkRemove"), CheckBox).Checked Then
                e.Row.Visible = False
            End If
            CType(e.Row.Cells(4).FindControl("txtOtherText"), TextBox).Text = Server.HtmlDecode(CType(e.Row.Cells(4).FindControl("txtOtherText"), TextBox).Text)
            If CType(e.Row.Cells(4).FindControl("txtOtherText"), TextBox).Text.Trim() = "" And Not e.Row.Cells(3).Text.Trim().ToUpper().Equals("OTHER") Then
                CType(e.Row.Cells(4).FindControl("txtOtherText"), TextBox).Visible = False
            End If

        ElseIf e.Row.RowType = DataControlRowType.Header Then
            CType(e.Row.Cells(5).FindControl("chkRemoveAllCauses"), CheckBox).Attributes.Add("onclick", "SetRemoveAllCauses();")

        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click, btnSave0.Click

        SaveData()
        If (vehSelectionError = 1) Then
            Exit Sub
        End If

        If (blnResolved = True) Then
            Exit Sub
        End If

        'If (status.ToLower = "open" And cmbStatus.SelectedItem.Text.ToLower = "feedback") Then
        '    cmbStatus.SelectedIndex = 1
        '    SetErrorShortMessage("Cannot change 'Open' status to 'Feedback'")
        '    Exit Sub
        'End If

        status = cmbStatus.SelectedItem.Text.ToLower


        If (Not ViewState(ViewState_INCIDENTID) Is Nothing) Then
            If (ViewState(ViewState_INCIDENTID).ToString() <> "") Then
                If (status.ToLower = "resolved") Then
                    If (txtDetails.Text.Trim <> "") Then
                        details = txtDetails.Text.Trim
                        SaveCallbackData()
                        status = "resolved"
                        lblStatus.Text = "Issue resolved by user '" & usrFullName & "' on " & DateTime.Now.ToString()
                        lblStatus.Visible = True
                        txtDetails.Enabled = False
                        cmbStatus.Enabled = False
                    Else
                        SetErrorShortMessage("No callback details entered")
                        Exit Sub
                    End If
                End If

                If (status.ToLower = "feedback" Or status.ToLower = "open") Then
                    If (txtDetails.Text.Trim = "") Then
                        'details = "No details entered by the user ( " & status & " )"
                        details = ""
                    Else
                        details = txtDetails.Text.Trim
                    End If

                    SaveCallbackData()
                    lblStatus.Visible = False
                    txtDetails.Enabled = True
                    cmbStatus.Enabled = True
                    'End If
                End If
                Session("IsNewIncident") = 0
                Dim indx As Integer
                If (status.ToLower = "feedback") Then
                    indx = 0
                ElseIf (status.ToLower = "open") Then
                    indx = 1
                ElseIf (status.ToLower = "resolved") Then
                    indx = 2
                End If
                cmbStatus.ClearSelection()
                cmbStatus.SelectedIndex = indx
                txtDetails.Text = ""
                details = ""
            End If
        End If

    End Sub



    Protected Sub btnSaveAndNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAndNew.Click, btnSaveNew0.Click
        SaveData()
        If (vehSelectionError = 1) Then
            Exit Sub
        End If

        If (blnResolved = True) Then
            Exit Sub
        End If

        'If (status.ToLower = "open" And cmbStatus.SelectedItem.Text.ToLower = "feedback") Then
        '    cmbStatus.SelectedIndex = 1
        '    SetErrorShortMessage("Cannot change 'Open' status to 'Feedback'")
        '    Exit Sub
        'End If


        status = cmbStatus.SelectedItem.Text.ToLower

        If (Not ViewState(ViewState_INCIDENTID) Is Nothing) Then
            If (ViewState(ViewState_INCIDENTID).ToString() <> "") Then
                If (status.ToLower = "resolved") Then
                    If (txtDetails.Text.Trim <> "") Then
                        details = txtDetails.Text.Trim
                        SaveCallbackData()
                    Else
                        SetErrorShortMessage("No callback details entered")
                        Exit Sub
                    End If
                End If

                If (status.ToLower = "feedback" Or status.ToLower = "open") Then
                    If (txtDetails.Text.Trim = "") Then
                        'details = "No details entered by the user ( " & status & " )"
                        details = ""
                    Else
                        details = txtDetails.Text.Trim
                    End If
                    SaveCallbackData()
                    lblStatus.Visible = False
                    txtDetails.Enabled = True
                    cmbStatus.Enabled = True
                End If
                Session("IsNewIncident") = 1
                Dim indx As Integer
                If (status.ToLower = "feedback") Then
                    indx = 0
                ElseIf (status.ToLower = "open") Then
                    indx = 1
                ElseIf (status.ToLower = "resolved") Then
                    indx = 2
                End If
                cmbStatus.ClearSelection()
                cmbStatus.SelectedIndex = indx
                txtDetails.Text = ""
                details = ""
            End If
        End If

        ViewStateClearForNew()
        bindIncidentGridToIncidents()
        cmbStatus.SelectedIndex = 0
        status = "feedback"
        lblStatus.Text = ""
        txtDetails.Enabled = True
        cmbStatus.Enabled = True
        lblStatus.Visible = False
        LoadPage()
    End Sub


    Protected Sub cmbStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbStatus.SelectedIndexChanged
        ViewState("lastSel") = cmbStatus.SelectedItem.Text
    End Sub

#End Region

#Region "User Defined Functions"

    Sub LoadPage()

        Dim oXmlData As System.Xml.XmlDocument
        oXmlData = Aurora.Booking.Services.BookingIncidents.GetIncident( _
                                            ViewState(ViewState_BOOKINGID), _
                                            ViewState(ViewState_RENTALID), _
                                            ViewState(ViewState_INCIDENTID), _
                                            UserCode)

        ViewState(ViewState_INCIDENTDATA) = oXmlData.OuterXml
        Dim sCauseType As String


        With oXmlData.DocumentElement
            If Not .SelectSingleNode("Incident/Type") Is Nothing Then
                sCauseType = .SelectSingleNode("Incident/Type").InnerText 'objDom.selectSingleNode("//Root/Data/Incident/Type").text

                'rest of loading
                'the main table
                lblBookingRentalNumber.Text = .SelectSingleNode("Incident/IncdntBooking").InnerText
                lblAIMSRefNo.Text = .SelectSingleNode("Incident/AIMSRefNo").InnerText
                lblHirer.Text = .SelectSingleNode("Incident/Hirer").InnerText
                lblPackage.Text = .SelectSingleNode("Incident/Package").InnerText
                lblAgent.Text = .SelectSingleNode("Incident/Agent").InnerText
                'main table

                'ViewState
                ViewState(ViewState_DETAILID) = .SelectSingleNode("Incident/DetailId").InnerText
                ViewState(ViewState_DETAILSINTEGRITY) = .SelectSingleNode("Incident/DetailsIntegrity").InnerText
                ViewState(ViewState_INCIDENTINTEGRITY) = .SelectSingleNode("Incident/IntegrityNo").InnerText
                ViewState(ViewState_VECHICLETABLEDATA) = oXmlData.DocumentElement.SelectSingleNode("VechileData").OuterXml
                'vehicles table
                Dim dstVechicles As DataSet = New DataSet()
                dstVechicles.ReadXml(New XmlNodeReader(oXmlData.DocumentElement.SelectSingleNode("VechileData")))
                If Not dstVechicles.Tables.Count = 0 Then
                    gwVehicles.DataSource = dstVechicles.Tables(0)
                Else
                    Dim xmlTemp As New XmlDocument
                    xmlTemp.LoadXml("<VechileData>" & _
                                        "<FMOD>" & _
                                            "<CheckOut></CheckOut>" & _
                                            "<CheckIn></CheckIn>" & _
                                            "<Vehicle></Vehicle>" & _
                                            "<UnitNo></UnitNo>" & _
                                            "<SelectedVechile>0</SelectedVechile>" & _
                                        "</FMOD>" & _
                                    "</VechileData>")
                    dstVechicles.ReadXml(New XmlNodeReader(xmlTemp))
                    '<FMOD>
                    '<CheckOut>CHC - 05/08/2002</CheckOut>
                    '<CheckIn>CHC - 05/08/2002</CheckIn>
                    '<Vehicle>CCMR</Vehicle>
                    '<UnitNo>562051 - ZM9227</UnitNo>
                    '<SelectedVechile>0</SelectedVechile>
                    '</FMOD>


                    gwVehicles.DataSource = dstVechicles.Tables(0)
                    SetErrorShortMessage("No vehicle data!")
                End If


                gwVehicles.DataBind()
                'vehicles table
                If pkrBranch.DataId.Trim().Equals(String.Empty) Then
                    ' something was not set already
                    pkrBranch.Text = .SelectSingleNode("Incident/Branch").InnerText
                End If

                'dtLoggedDate.Text = .SelectSingleNode("Incident/LoggedDate").InnerText
                ' system format -> date obj
                dtLoggedDate.Date = ParseDateTime(.SelectSingleNode("Incident/LoggedDate").InnerText, SystemCulture)

                cmbNotified.SelectedValue = .SelectSingleNode("Incident/Notified").InnerText


                'now comes the variable part

                If Not Trim(CStr(ViewState(ViewState_INCIDENTID))) = String.Empty Then
                    ' if this is a old record
                    'loading tblVehicleDetails

                    txaDetails.Value = Server.HtmlDecode(.SelectSingleNode("Incident/Details").InnerText)
                    'done with tblVehicleDetails

                    'the causes table'
                    Dim oCauseData As New XmlDocument
                    oCauseData.LoadXml(.SelectSingleNode("CauseDetails").OuterXml)
                    'if no records
                    If oCauseData.OuterXml = "<CauseDetails><IncidentSubCause><SubCauseID /><IncidentId /><CauseDescription /><SubCauseDescription /><OtherText /><RemoveItem /><CauseID /><IntegrityNo /></IncidentSubCause></CauseDetails>" Then
                        ' if this is a new record
                        ViewState(ViewState_DUMMYCAUSEPRESENT) = True
                        oCauseData.LoadXml("<IncidentSubCause>" & _
                                            "<SubCauseID></SubCauseID>" & _
                                            "<IncidentId></IncidentId>" & _
                                            "<CauseDescription></CauseDescription>" & _
                                            "<SubCauseDescription></SubCauseDescription>" & _
                                            "<OtherText></OtherText>" & _
                                            "<RemoveItem>0</RemoveItem>" & _
                                            "<CauseID></CauseID>" & _
                                            "<IntegrityNo></IntegrityNo>" & _
                                           "</IncidentSubCause>")
                    End If
                    'got the causes
                    ' now load the grid with the darn xml!
                    ViewState(ViewState_CAUSESTABLEDATA) = oCauseData.OuterXml
                    LoadCausesGrid(oCauseData)
                    'load the cause combo
                    LoadCausesCombo(.SelectSingleNode("Incident/Type").InnerText)

                    ' no vehicle changes allowed now!
                    gwVehicles.Enabled = False
                Else
                    ' if this is a new record
                    'Mod: Raj Feb 22 2012
                    gwVehicles.Enabled = True
                    'Mod: Raj Feb 22 2012

                    txaDetails.Value = ""

                    ViewState(ViewState_DUMMYCAUSEPRESENT) = True
                    Dim oCauseData As New XmlDocument
                    oCauseData.LoadXml("<IncidentSubCause>" & _
                                        "<SubCauseID></SubCauseID>" & _
                                        "<IncidentId></IncidentId>" & _
                                        "<CauseDescription></CauseDescription>" & _
                                        "<SubCauseDescription></SubCauseDescription>" & _
                                        "<OtherText></OtherText>" & _
                                        "<RemoveItem>0</RemoveItem>" & _
                                        "<CauseID></CauseID>" & _
                                        "<IntegrityNo></IntegrityNo>" & _
                                       "</IncidentSubCause>")
                    ViewState(ViewState_CAUSESTABLEDATA) = oCauseData.OuterXml
                    LoadCausesGrid(oCauseData)
                    LoadCausesCombo(TypeId)
                End If
            Else
                sCauseType = ""
            End If 'If Not .SelectSingleNode("Incident/Type") Is Nothing Then

        End With ' With oXmlData.DocumentElement
    End Sub

    Sub LoadNotificationTypes()
        Dim dtNotificationTypes As DataTable
        dtNotificationTypes = Aurora.Booking.Services.BookingIncidents.GetNotificationTypes()
        cmbNotified.DataSource = dtNotificationTypes
        cmbNotified.DataTextField = "CODE"
        cmbNotified.DataValueField = "ID"
        cmbNotified.DataBind()
    End Sub

    Sub LoadCausesGrid(ByVal CausesXmlData As XmlDocument)
        Dim dstCauses As New DataSet
        dstCauses.ReadXml(New XmlNodeReader(CausesXmlData.DocumentElement))
        gwCauses.DataSource = dstCauses.Tables("IncidentSubCause")
        gwCauses.DataBind()
    End Sub

    Sub LoadCausesCombo(ByVal CauseTypeId As String)
        Dim dtCauses As DataTable
        dtCauses = Aurora.Booking.Services.BookingIncidents.GetCauseTypes(CauseTypeId)
        ''rev:mia oct.20
        Dim dview As DataView = dtCauses.DefaultView
        dview.Sort = "CODE ASC"
        cmbCauses.DataSource = dview ''dtCauses
        cmbCauses.DataTextField = "CODE"
        cmbCauses.DataValueField = "ID"
        cmbCauses.DataBind()
        cmbCauses.Items.Insert(0, "")

    End Sub

    Sub LoadSubCausesCombo(ByVal SubCauseTypeId As String)
        Dim dtSubCauses As DataTable
        dtSubCauses = Aurora.Booking.Services.BookingIncidents.GetSubCauses(SubCauseTypeId)
        ''rev:mia oct.20

        If (dtSubCauses.Columns.Count > 0) Then
            Dim dview As DataView = dtSubCauses.DefaultView
            dview.Sort = "Description ASC"
            cmbSubCause.DataSource = dview
            cmbSubCause.DataTextField = "Description"
            cmbSubCause.DataValueField = "SubCauseId"
            cmbSubCause.DataBind()
            cmbSubCause.Items.Insert(0, "")
        Else
            cmbSubCause.Items.Clear()
        End If
    End Sub

    ''' <summary>
    ''' This Function Allows manipulation of the Causes gridview
    ''' </summary>
    ''' <param name="AddNew">Specifies that a row is being added</param>
    ''' <param name="RemoveNode">Specifies that a row is being removed</param>
    ''' <remarks></remarks>
    Sub BuildCauseDetailsXML(Optional ByVal AddNew As Boolean = False, Optional ByVal RemoveNode As Boolean = False, Optional ByVal SkipReload As Boolean = False, Optional ByVal IsNewRecord As Boolean = False, Optional ByVal SaveDataHappening As Boolean = False)


        Dim oCausesDataXml As XmlDocument = New XmlDocument
        oCausesDataXml.LoadXml(CStr(ViewState(ViewState_CAUSESTABLEDATA)))

        If AddNew Then

            If Not CBool(ViewState(ViewState_DUMMYCAUSEPRESENT)) Then
                'its a new row so no duplicate possible
                Dim nMatchingRow As Int16
                'take this cause node and look at table
                For j As Int16 = 0 To gwCauses.Rows.Count - 1
                    If gwCauses.Rows(j).Cells(0).Text = cmbSubCause.SelectedItem.Value Then
                        If gwCauses.Rows(j).Cells(6).Text = cmbCauses.SelectedItem.Value Then
                            'same node
                            nMatchingRow = j
                            SetErrorMessage("This Cause and Sub-Cause is already added at row " & j + 1)
                            Exit Sub
                        End If
                    End If
                Next
            End If

            '<IncidentSubCause>
            '	<SubCauseID>05EB35A3-9D2F-4CBC-9063-740422592707</SubCauseID>
            '	<IncidentId>6A568E0C66EB4505A4C71E8E24E09C9B</IncidentId>
            '	<CauseDescription>General</CauseDescription>
            '	<SubCauseDescription>Unjustifiable complaint</SubCauseDescription>
            '	<OtherText></OtherText>
            '	<RemoveItem>0</RemoveItem>
            '	<CauseID>87ACDE37-D434-42EF-9599-1EF58DC33D2B</CauseID>
            '	<IntegrityNo>2</IntegrityNo>
            '</IncidentSubCause>

            If CBool(ViewState(ViewState_DUMMYCAUSEPRESENT)) Then
                'remove the junk row
                oCausesDataXml.DocumentElement.RemoveAll()
                ViewState(ViewState_DUMMYCAUSEPRESENT) = False
            End If

            Dim oNewRowNode, oSubCauseIDNode, oIncidentIdNode, oCauseDescriptionNode, oSubCauseDescriptionNode, oOtherTextNode, oRemoveItemNode, oCauseIDNode, oIntegrityNoNode As XmlNode
            oNewRowNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "IncidentSubCause", Nothing)

            oSubCauseIDNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "SubCauseID", Nothing)
            oIncidentIdNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "IncidentId", Nothing)
            oCauseDescriptionNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "CauseDescription", Nothing)
            oSubCauseDescriptionNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "SubCauseDescription", Nothing)
            oOtherTextNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "OtherText", Nothing)
            oRemoveItemNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "RemoveItem", Nothing)
            oCauseIDNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "CauseID", Nothing)
            oIntegrityNoNode = oCausesDataXml.CreateNode(XmlNodeType.Element, "IntegrityNo", Nothing)

            Dim oIncidentData As XmlDocument = New XmlDocument
            oIncidentData.LoadXml(CStr(ViewState(ViewState_INCIDENTDATA)))

            oSubCauseIDNode.InnerText = cmbSubCause.SelectedItem.Value
            oIncidentIdNode.InnerText = oIncidentData.DocumentElement.SelectSingleNode("Incident/IncidentId").InnerText
            oCauseDescriptionNode.InnerText = cmbCauses.SelectedItem.Text
            oSubCauseDescriptionNode.InnerText = cmbSubCause.SelectedItem.Text
            oOtherTextNode.InnerText = Server.HtmlEncode(txaOther.Value) 'txaOther.Text 
            oRemoveItemNode.InnerText = 0
            oCauseIDNode.InnerText = cmbCauses.SelectedItem.Value
            oIntegrityNoNode.InnerText = 0

            oNewRowNode.AppendChild(oSubCauseIDNode)
            oNewRowNode.AppendChild(oIncidentIdNode)
            oNewRowNode.AppendChild(oCauseDescriptionNode)
            oNewRowNode.AppendChild(oSubCauseDescriptionNode)
            oNewRowNode.AppendChild(oOtherTextNode)
            oNewRowNode.AppendChild(oRemoveItemNode)
            oNewRowNode.AppendChild(oCauseIDNode)
            oNewRowNode.AppendChild(oIntegrityNoNode)

            oCausesDataXml.DocumentElement.AppendChild(oNewRowNode)
        End If

        If RemoveNode Then
            For i As Int16 = 0 To oCausesDataXml.DocumentElement.ChildNodes.Count - 1
                'ref to each row
                Dim oCauseNode As XmlNode
                oCauseNode = oCausesDataXml.DocumentElement.ChildNodes(i)
                'take this cause node and look at table
                For j As Int16 = 0 To gwCauses.Rows.Count - 1
                    If gwCauses.Rows(j).Cells(0).Text = oCauseNode.SelectSingleNode("SubCauseID").InnerText Then
                        If gwCauses.Rows(j).Cells(6).Text = oCauseNode.SelectSingleNode("CauseID").InnerText Then
                            If CType(gwCauses.Rows(i).Cells(5).FindControl("chkRemove"), CheckBox).Checked Then
                                'same node
                                'tag for destruction!
                                oCauseNode.SelectSingleNode("RemoveItem").InnerText = 1
                            End If
                        End If
                    End If
                Next
            Next
            If IsNewRecord Then
                For i As Int16 = oCausesDataXml.DocumentElement.ChildNodes.Count - 1 To 0 Step -1
                    If oCausesDataXml.DocumentElement.ChildNodes(i).SelectSingleNode("RemoveItem").InnerText = 1 Then
                        ' remove node
                        oCausesDataXml.DocumentElement.RemoveChild(oCausesDataXml.DocumentElement.ChildNodes(i))
                    End If
                Next
                ' if causes are empty!
                If oCausesDataXml.DocumentElement.ChildNodes.Count = 0 Then
                    oCausesDataXml.LoadXml("<IncidentSubCause><IncidentSubCause><SubCauseID></SubCauseID><IncidentId /><CauseDescription></CauseDescription><SubCauseDescription></SubCauseDescription><OtherText></OtherText><RemoveItem>0</RemoveItem><CauseID></CauseID><IntegrityNo></IntegrityNo></IncidentSubCause></IncidentSubCause>")
                    ViewState(ViewState_DUMMYCAUSEPRESENT) = True
                End If
            End If
        End If

        If SaveDataHappening Then
            For i As Int16 = 0 To oCausesDataXml.DocumentElement.ChildNodes.Count - 1
                'ref to each row
                Dim oCauseNode As XmlNode
                oCauseNode = oCausesDataXml.DocumentElement.ChildNodes(i)
                'take this cause node and look at table
                For j As Int16 = 0 To gwCauses.Rows.Count - 1
                    If gwCauses.Rows(j).Cells(0).Text = oCauseNode.SelectSingleNode("SubCauseID").InnerText Then
                        If gwCauses.Rows(j).Cells(6).Text = oCauseNode.SelectSingleNode("CauseID").InnerText Then
                            oCauseNode.SelectSingleNode("OtherText").InnerText = Server.HtmlEncode(CType(gwCauses.Rows(j).Cells(4).FindControl("txtOtherText"), TextBox).Text)
                            'same node
                        End If
                    End If
                Next
            Next
        End If

        ViewState(ViewState_CAUSESTABLEDATA) = oCausesDataXml.OuterXml
        If Not SkipReload Then
            'only used in final saves
            LoadCausesGrid(oCausesDataXml)
        End If


    End Sub
    Dim vehSelectionError As Integer = 0
    ''' <summary>
    ''' This Function will collect the data fragments from the screen/ViewState and save to DB
    ''' </summary>
    ''' <remarks></remarks>
    Sub SaveData()
        'basically the method in the biz layer expects 
        '"<Root>" + IncidentDetails.xml + VehicleStr + Causes.xml + "</Root>"
        'If CBool(ViewState(ViewState_DUMMYCAUSEPRESENT)) Then
        '    SetErrorShortMessage("No causes entered")
        '    'the message for dummy row
        '    Exit Sub
        'End If

        'Mod: Raj Mar 26 2013
        If (CStr(ViewState(ViewState_INCIDENTID)) = "") Then

            Dim blnValIsBlank As Boolean = False
            For j As Int16 = 0 To gwCauses.Rows.Count - 1
                If gwCauses.Rows(j).Cells(0).Text = "&nbsp;" Then
                    blnValIsBlank = True
                    Exit For
                End If
            Next


            If (blnValIsBlank = True) Then

                If (cmbCauses.SelectedItem.Text = "") Then
                    'Me.AddErrorMessage("Please select Cause and Sub-Cause for the incident.")
                    SetErrorMessage("Cause must be selected")
                    'SetErrorShortMessage("Cause and Sub-Cause are mandatory")
                    Exit Sub
                Else
                    If (cmbSubCause.Items.Count = 0) Then
                        'Me.AddErrorMessage("Please select Cause and Sub-Cause for the incident.")
                        SetErrorMessage("Sub-Cause must be selected")
                        Exit Sub
                    Else
                        If (cmbSubCause.SelectedItem.Text = "") Then
                            'Me.AddErrorMessage("Please select Cause and Sub-Cause for the incident.")
                            SetErrorMessage("Sub-Cause must be selected")
                            Exit Sub
                        End If

                    End If
                End If


                SetErrorMessage("Cause and Sub-Cause must be added")
                Exit Sub
            End If
        End If

        If (CStr(ViewState(ViewState_INCIDENTID)) <> "") Then

            Dim blnValIsBlank As Boolean = False
            For j As Int16 = 0 To gwCauses.Rows.Count - 1
                If gwCauses.Rows(j).Cells(0).Text = "&nbsp;" Then
                    SetErrorMessage("Cause and Sub-Cause must be added")
                    Exit Sub
                End If
            Next
        End If

        Dim checkedRows As Integer = 0
        For Each oGridRow As GridViewRow In gwCauses.Rows
            If CType(oGridRow.FindControl("chkRemove"), CheckBox).Checked Then
                checkedRows = checkedRows + 1
            End If
        Next

        If (gwCauses.Rows.Count = checkedRows) Then
            SetErrorMessage("At least one Cause and Sub-Cause must remain")
            Exit Sub
        End If

        'Mod: Raj Mar 26 2013


        Dim oXmlDataToSave As XmlDocument = New XmlDocument
        oXmlDataToSave.LoadXml("<Root></Root>")

        Dim sUnitNo As String = ""

        Dim bAtleastOneVehicleSelected As Boolean = False
        For i As Int16 = 0 To gwVehicles.Rows.Count - 1
            If CType(gwVehicles.Rows(i).Cells(4).FindControl("radVehicle"), RadioButton).Checked Then
                'check for dummy row
                If Server.HtmlDecode(gwVehicles.Rows(i).Cells(0).Text).Trim().Equals(String.Empty) _
                    And Server.HtmlDecode(gwVehicles.Rows(i).Cells(1).Text).Trim().Equals(String.Empty) _
                    And Server.HtmlDecode(gwVehicles.Rows(i).Cells(2).Text).Trim().Equals(String.Empty) _
                    And Server.HtmlDecode(gwVehicles.Rows(i).Cells(3).Text).Trim().Equals(String.Empty) Then
                    bAtleastOneVehicleSelected = False
                Else
                    bAtleastOneVehicleSelected = True
                    sUnitNo = Server.HtmlDecode(gwVehicles.Rows(i).Cells(3).Text).Trim()
                End If

                Exit For
            End If
        Next
        vehSelectionError = 0
        If Not bAtleastOneVehicleSelected Then
            vehSelectionError = 1
            SetErrorShortMessage("No vehicles selected")
            Exit Sub
        End If



        'validations

        If pkrBranch.Text.Trim() = String.Empty Or dtLoggedDate.Text.Trim() = String.Empty _
            Or cmbNotified.SelectedItem.Value = String.Empty _
            Or Not bAtleastOneVehicleSelected _
        Then
            'missing fields
            'display a GEN005 error
            SetErrorShortMessage(GetMessage("GEN005"))
            Exit Sub
        End If
        'validations

        'hack for the cause xml
        Dim oCausesData As XmlDocument = New XmlDocument
        Dim oTempXML As XmlDocument = New XmlDocument


        'CauseDetails 
        'each vehicle in vehicle table needs to be "marked in XML
        'so also each cause
        If Not CBool(ViewState(ViewState_DUMMYCAUSEPRESENT)) Then
            BuildCauseDetailsXML(False, True, False, False, True)
            oTempXML.LoadXml(CStr(ViewState(ViewState_CAUSESTABLEDATA)))
            oCausesData.LoadXml("<CauseDetails>" & oTempXML.DocumentElement.InnerXml & "</CauseDetails>")
        Else
            ' no causes!
            oCausesData.LoadXml("<CauseDetails/>")
        End If


        oXmlDataToSave.DocumentElement.InnerXml = BuildIncidentDetialsXML().OuterXml & BuildVehicleXML().OuterXml & oCausesData.OuterXml

        Dim oRetXml As XmlDocument
        Dim NewIncidentId As String = ""
        oRetXml = Aurora.Booking.Services.BookingIncidents.MaintainIncident(oXmlDataToSave, UserCode, "IncidentManagement.aspx", NewIncidentId)

        '"<Error><ErrorMessage>Error/AKL - Invalid Location</ErrorMessage><ErrorType>Other</ErrorType></Error>"

        Select Case oRetXml.SelectSingleNode("Error/ErrorType").InnerText
            Case "Application"
                SetErrorShortMessage(oRetXml.SelectSingleNode("//Error/ErrorMessage").InnerText)
            Case "GEN045"
                SetInformationShortMessage(oRetXml.SelectSingleNode("//Error/ErrorMessage").InnerText)
            Case "GEN046"
                SetInformationShortMessage(oRetXml.SelectSingleNode("//Error/ErrorMessage").InnerText)
            Case Else
                SetErrorShortMessage(oRetXml.SelectSingleNode("//Error/ErrorMessage").InnerText)
        End Select

        'the popup comes here
        If ViewState(ViewState_INCIDENTID) = "" Then
            'this was a new record
            'If cmbIncidentType.SelectedItem.Text.ToUpper().Trim() = "ACCIDENT" _
            'Or cmbIncidentType.SelectedItem.Text.ToUpper().Trim() = "BREAKDOWN" Then
            '    ViewState(ViewState_ACTIONONCONFIRM) = "REPAIRMAINTCREATE"
            '    If sUnitNo.Contains("-") Then
            '        sUnitNo = sUnitNo.Split("-"c)(0).Trim()
            '    End If
            '    ViewState(ViewState_UNITNO) = sUnitNo
            '    ViewState(ViewState_NEWINCIDENTID) = NewIncidentId
            '    ViewState(ViewState_INCIDENTID) = NewIncidentId
            '    LoadPage()
            '    ShowConfirmationDialog("Do you want to create a Repairs and Maintanance record?", "Confirmation required")
            'End If
        End If
        ViewState(ViewState_INCIDENTID) = NewIncidentId

        ' Reload the data from DB, to avoid integrity issues
        LoadPage()

        'If Not oRetXml.SelectSingleNode("//Error/ErrorMessage") Is Nothing Then
        '    'someerror
        '    If oRetXml.SelectSingleNode("Error/ErrorType").InnerText = "Application" Then
        '        
        '        Exit Sub
        '    End If
        'End If

        'IncidentDetails looks like this
        '<Incident>
        '	<TodaysDate>18/02/2008</TodaysDate>
        '	<BookingId>3239315</BookingId>
        '	<RentalId>3239315-1</RentalId>
        '	<IncidentId>17E3B5ABBBFF4D5E8E19A3286575F663</IncidentId>
        '	<IncdntBooking>3239315 - 1</IncdntBooking>
        '	<AIMSRefNo>382131</AIMSRefNo>
        '	<Hirer>EBDON, MR ALAN</Hirer>
        '	<Package>TTCAR4</Package>
        '	<Agent>VALUE TOURS</Agent>
        '	<Branch>AKL - Auckland</Branch>
        '	<LoggedDate>15/02/2008</LoggedDate>
        '	<Type>7407F7D1-FF2F-4087-BB92-D5C79B15A9E6</Type>
        '	<Notified>B4CB6CAA-F93B-4FAA-8F25-786DD9618D03</Notified>
        '	<Odometer>3345</Odometer>
        '	<DetailId>96412EF742934D5DAC9399CE20D49B03</DetailId>
        '	<DetailsIntegrity>2</DetailsIntegrity>
        '	<Details>afciddent</Details>
        '	<CustSatisfaction>1</CustSatisfaction>
        '	<IncdntStatus>Closed</IncdntStatus>
        '	<ResolutionWhen>15/02/2008</ResolutionWhen>
        '	<IntegrityNo>2</IntegrityNo>
        '</Incident>

    End Sub

    Function BuildIncidentDetialsXML() As XmlDocument
        Dim sResolvedDate As String = ""

        Dim oIncidentDetails As XmlDocument = New XmlDocument
        oIncidentDetails.LoadXml("<Incident>" & _
                                     "<TodaysDate>" & Now.Date.ToShortDateString() & "</TodaysDate>" & _
                                     "<BookingId>" & ViewState(ViewState_BOOKINGID) & "</BookingId>" & _
                                     "<RentalId>" & ViewState(ViewState_RENTALID) & "</RentalId>" & _
                                     "<IncidentId>" & ViewState(ViewState_INCIDENTID) & "</IncidentId>" & _
                                     "<IncdntBooking>" & lblBookingRentalNumber.Text & "</IncdntBooking>" & _
                                     "<AIMSRefNo>" & lblAIMSRefNo.Text & "</AIMSRefNo>" & _
                                     "<Hirer>" & lblHirer.Text & "</Hirer>" & _
                                     "<Package>" & lblPackage.Text & "</Package>" & _
                                     "<Agent>" & lblAgent.Text & "</Agent>" & _
                                     "<Branch>" & pkrBranch.Text & "</Branch>" & _
                                     "<LoggedDate>" & IIf(dtLoggedDate.Text.Trim.Equals(String.Empty), String.Empty, DateTimeToString(dtLoggedDate.Date, SystemCulture).Split(" "c)(0)) & "</LoggedDate>" & _
                                     "<Type>" & TypeId & "</Type>" & _
                                     "<Notified>" & cmbNotified.SelectedItem.Value & "</Notified>" & _
                                     "<DetailId>" & ViewState(ViewState_DETAILID) & "</DetailId>" & _
                                     "<DetailsIntegrity>" & ViewState(ViewState_DETAILSINTEGRITY) & "</DetailsIntegrity>" & _
                                     "<Details>" & Server.HtmlEncode(txaDetails.Value) & "</Details>" & _
                                     "<ResolutionWhen>" & sResolvedDate & "</ResolutionWhen>" & _
                                     "<IntegrityNo>" & ViewState(ViewState_INCIDENTINTEGRITY) & "</IntegrityNo>" & _
                                "</Incident>")
        Return oIncidentDetails
    End Function

    Sub ResetCauseGridSelection()
        Dim oCausesDataXml As XmlDocument = New XmlDocument
        oCausesDataXml.LoadXml(CStr(ViewState(ViewState_CAUSESTABLEDATA)))

        For i As Int16 = 0 To oCausesDataXml.DocumentElement.ChildNodes.Count - 1
            'ref to each row
            Dim oCauseNode As XmlNode
            oCauseNode = oCausesDataXml.DocumentElement.ChildNodes(i)
            oCauseNode.SelectSingleNode("RemoveItem").InnerText = 0
        Next

        ViewState(ViewState_CAUSESTABLEDATA) = oCausesDataXml.OuterXml
        LoadCausesGrid(oCausesDataXml)
    End Sub

    Function BuildVehicleXML() As XmlDocument

        Dim oVehicleData As New XmlDocument
        oVehicleData.LoadXml("<VechileData></VechileData>")

        Dim oViewStateVehicleData As XmlElement
        Dim oTempXML As XmlDocument = New XmlDocument
        oTempXML.LoadXml("<Root>" & CStr(ViewState(ViewState_VECHICLETABLEDATA)) & "</Root>")

        oViewStateVehicleData = oTempXML.FirstChild

        For Each oVehicleNode As XmlNode In oTempXML.DocumentElement.SelectSingleNode("VechileData").ChildNodes
            For Each oGridRow As GridViewRow In gwVehicles.Rows
                If CType(oGridRow.FindControl("radVehicle"), RadioButton).Checked _
                    AndAlso oGridRow.Cells(0).Text.Split("-"c)(0).Trim() = oVehicleNode.SelectSingleNode("CheckOut").InnerText.Split("-"c)(0).Trim() _
                    AndAlso oGridRow.Cells(0).Text.Split("-"c)(1).Trim() = oVehicleNode.SelectSingleNode("CheckOut").InnerText.Split("-"c)(1).Trim() _
                    AndAlso oGridRow.Cells(1).Text.Split("-"c)(0).Trim() = oVehicleNode.SelectSingleNode("CheckIn").InnerText.Split("-"c)(0).Trim() _
                    AndAlso oGridRow.Cells(1).Text.Split("-"c)(1).Trim() = oVehicleNode.SelectSingleNode("CheckIn").InnerText.Split("-"c)(1).Trim() _
                    AndAlso oGridRow.Cells(2).Text = oVehicleNode.SelectSingleNode("Vehicle").InnerText _
                    AndAlso oGridRow.Cells(3).Text = oVehicleNode.SelectSingleNode("UnitNo").InnerText _
                Then
                    oVehicleNode.SelectSingleNode("SelectedVechile").InnerText = 1
                    oVehicleData.DocumentElement.InnerXml = oVehicleNode.OuterXml
                    Return oVehicleData
                    Exit Function
                End If
            Next
        Next

        Return oVehicleData

    End Function

    Public Sub ShowConfirmationDialog(ByVal Message As String, ByVal Title As String)
        confimationBoxControl.Text = Message
        confimationBoxControl.Title = Title
        confimationBoxControl.Show()
    End Sub

    Protected Sub confimationBoxControl_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles confimationBoxControl.PostBack
        If leftButton Then
            'here is the logic
            Select Case ViewState(ViewState_ACTIONONCONFIRM)
                Case "REPAIRMAINTCREATE"
                    ViewState(ViewState_ACTIONONCONFIRM) = Nothing
                    ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, _
                    "window.location='../OpsAndLogs/FleetRepairManagement.aspx?UnitNo=" & CStr(ViewState(ViewState_UNITNO)) & _
                    Server.HtmlEncode("&BackPage=../Booking/IncidentManagement.aspx?RntId=" & CStr(ViewState(ViewState_RENTALID)) & "$BooId=" & CStr(ViewState(ViewState_BOOKINGID)) & "$IncidentId=" & CStr(ViewState(ViewState_NEWINCIDENTID))) & "';", True)
                Case "REMOVECAUSE"
                    ViewState(ViewState_ACTIONONCONFIRM) = Nothing
                    RemoveSubCauses()
                Case Else

            End Select
        Else
            'cancel was pressed!
            Select Case ViewState(ViewState_ACTIONONCONFIRM)
                Case "REMOVECAUSE"
                    'remove cause is cancelled
                    'clearing all selections and reloading grid
                    ViewState(ViewState_ACTIONONCONFIRM) = Nothing
                    ResetCauseGridSelection()
                Case Else
            End Select
        End If

    End Sub


    Public Sub ViewStateClearForNew()
        ViewState(ViewState_ACTIONONCONFIRM) = Nothing
        ViewState(ViewState_CAUSESTABLEDATA) = Nothing
        ViewState(ViewState_DETAILID) = Nothing
        ViewState(ViewState_DETAILSINTEGRITY) = Nothing
        ViewState(ViewState_INCIDENTDATA) = Nothing
        ViewState(ViewState_INCIDENTID) = Nothing
        ViewState(ViewState_INCIDENTINTEGRITY) = Nothing
        ViewState(ViewState_VECHICLETABLEDATA) = Nothing
        ViewState(ViewState_DUMMYCAUSEPRESENT) = Nothing
        FoundAMatchingRowInTheGrid = False
        UnitNumber = String.Empty
        RegistrationNumber = String.Empty
    End Sub

#End Region

    Protected Sub gwVehicles_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gwVehicles.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim radVehicle As RadioButton
            radVehicle = e.Row.Cells(4).FindControl("radVehicle")

            ' change for cell#0, Cell#1
            Dim dtTemp As Date
            If Not e.Row.Cells(0).Text.Trim().Equals(String.Empty) And Not e.Row.Cells(0).Text.Trim().Equals("&nbsp;") Then
                dtTemp = ParseDateTime(e.Row.Cells(0).Text.Split("-"c)(1).Trim(), SystemCulture)
                e.Row.Cells(0).Text = e.Row.Cells(0).Text.Split("-"c)(0) & " - " & dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If

            If Not e.Row.Cells(1).Text.Trim().Equals(String.Empty) And Not e.Row.Cells(1).Text.Trim().Equals("&nbsp;") Then
                dtTemp = ParseDateTime(e.Row.Cells(1).Text.Split("-"c)(1).Trim(), SystemCulture)
                e.Row.Cells(1).Text = e.Row.Cells(1).Text.Split("-"c)(0) & " - " & dtTemp.ToString(UserSettings.Current.ComDateFormat)
            End If
            ' change for cell#0, Cell#1


            ' addition for selection of row which corresponds to the exchange tab details
            Try
                Dim sUnitNo, sRegoNo As String
                sUnitNo = e.Row.Cells(3).Text.Trim().Split("-"c)(0).Trim()
                sRegoNo = e.Row.Cells(3).Text.Trim().Split("-"c)(1).Trim()

                If sUnitNo = UnitNumber And sRegoNo = RegistrationNumber Then
                    radVehicle.Checked = True
                    FoundAMatchingRowInTheGrid = True
                    radVehicle.Attributes.Add("onclick", "CheckVehicle(); ")
                Else
                    radVehicle.Attributes.Add("onclick", "CheckVehicle();")
                End If
            Catch
                ' do nothing
            End Try

            ' addition for selection of row which corresponds to the exchange tab details
        End If
    End Sub

#Region "rev:mia nov25"
    Protected Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If CStr(ViewState(ViewState_INCIDENTID)) = "" Then
            ''rev:mia nov25
            If Not IsSubCausesHasValue() Then
                Exit Sub
            End If
            'new record
            BuildCauseDetailsXML(False, True, False, True)
        Else

            'Mod: Raj Mar 26 2013
            Dim blnValIsBlank As Boolean = False
            For j As Int16 = 0 To gwCauses.Rows.Count - 1
                If gwCauses.Rows(j).Cells(0).Text = "&nbsp;" Then
                    blnValIsBlank = True                    
                End If
            Next

            If (blnValIsBlank = True) Then
                SetErrorMessage("Cause and Sub-Cause are not be added")
                Exit Sub
            Else
                If (gwCauses.Rows.Count = 1) Then
                    SetErrorMessage("At least one Cause and Sub-Cause must be added. Please add a new Cause and Sub-Cause before deleting this.")
                    Exit Sub
                End If
            End If

            Dim checkedRows As Integer = 0
            For Each oGridRow As GridViewRow In gwCauses.Rows
                If CType(oGridRow.FindControl("chkRemove"), CheckBox).Checked Then
                    checkedRows = checkedRows + 1
                End If
            Next

            If (gwCauses.Rows.Count = checkedRows) Then
                SetErrorMessage("At least one Cause and Sub-Cause must remain")
                Exit Sub
            End If
            'Mod: Raj Mar 26 2013


            'old record
            ViewState(ViewState_ACTIONONCONFIRM) = "REMOVECAUSE"
            ShowConfirmationDialog("Are you sure you wish to remove the selected subcauses from this incident?", "Confirmation Required")
        End If
    End Sub

    Function IsSubCausesHasValue() As Boolean
        If String.IsNullOrEmpty(ViewState(ViewState_CAUSESTABLEDATA)) Then
            Return False
        End If

        Dim temp As XmlDocument = New XmlDocument
        Try
            temp.LoadXml(ViewState(ViewState_CAUSESTABLEDATA))
            Return Not String.IsNullOrEmpty(temp.SelectSingleNode("IncidentSubCause/IncidentSubCause/SubCauseID").InnerText) AndAlso _
                   Not String.IsNullOrEmpty(temp.SelectSingleNode("IncidentSubCause/IncidentSubCause/CauseID").InnerText)
        Catch ex As Exception
            Return False
        End Try

    End Function

    Sub RemoveSubCauses()
        If CBool(ViewState(ViewState_DUMMYCAUSEPRESENT)) Then
            Exit Sub
        End If
        'each vehicle in vehicle table needs to be "marked in XML
        BuildCauseDetailsXML(False, True)

        Dim oReturnXml As XmlDocument
        Dim temp As XmlDocument = New XmlDocument
        temp.LoadXml(ViewState(ViewState_CAUSESTABLEDATA))
        oReturnXml = Aurora.Booking.Services.BookingIncidents.RemoveSubCauses(temp, ViewState(ViewState_BOOKINGID), ViewState(ViewState_RENTALID), ViewState(ViewState_INCIDENTID), "sp7", "Incidents")

        If oReturnXml.DocumentElement.SelectSingleNode("ErrorType").InnerText = "Application" Then
            SetErrorMessage(oReturnXml.DocumentElement.SelectSingleNode("ErrorMessage").InnerText)
        ElseIf oReturnXml.DocumentElement.SelectSingleNode("ErrorType").InnerText = "GEN046" Then
            SetInformationMessage(oReturnXml.DocumentElement.SelectSingleNode("ErrorMessage").InnerText)
        End If

        'done so far
        'now gotta reload
        LoadPage()

    End Sub

#End Region

#Region "rev:mia 20Aug2015- Changeover process upgrade -Logged By Nicky Bree"
    Protected Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel0.Click, btnCancel.Click
        Response.Redirect(Request.RawUrl)
    End Sub
#End Region
    
End Class
