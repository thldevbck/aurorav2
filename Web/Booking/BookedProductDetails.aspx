<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookedProductDetails.aspx.vb" Inherits="Booking_BookedProductDetails"
    Title="Untitled Page" %>

<%@ Register Src="Controls/BookingProductsReasons.ascx" TagName="BookingProductsReasons"
    TagPrefix="uc3" %>
<%@ Register Src="../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx"
    TagName="ConfimationBoxControl" TagPrefix="uc1" %>    

<%@ Register Src="../UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>

  
  
    
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
<script type="text/javascript" language="javascript" src="../OpsAndLogs/JS/OpsAndLogsCommon.js"></script>
<script language="javascript" type="text/javascript">
    function JSwarning(txtbox)
    {
        var txt = document.getElementById(txtbox.id)
            //alert(txt.id);
            if(txt.value.length>1000)
                {
                  setWarningShortMessage('Reasons limit is up to 1000 characters only!') ; 
                }
                else
                {
                 clearShortMessage();
                }
    }
                        var pbControl = null;
                        var prm = Sys.WebForms.PageRequestManager.getInstance();

                        prm.add_beginRequest(BeginRequestHandler);
                        prm.add_endRequest(EndRequestHandler);

                        function BeginRequestHandler(sender, args) {
                            pbControl = args.get_postBackElement();  //the control causing the postback
                           // alert(pbControl.id)
                            if(pbControl.id.indexOf('Next') > -1 || pbControl.id.indexOf('OK') > -1)
                            {
                                pbControl.disabled = true;
                            }
                        }

                        function EndRequestHandler(sender, args) {
                            if(pbControl.id.indexOf('Next') > -1 || pbControl.id.indexOf('OK') > -1)
                            {
                               pbControl.disabled = false;
                            }
                            pbControl = null;
                        }    
                        
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <br />
    <div>
        <asp:Panel runat="server" ID="pnlAgent"  Width="800" >
            <table>
                <tr>
                    <td width="100">
                        Agent:
                    </td>
                    <td width="400">
                        <b>
                            <asp:Label ID="lblAgent" runat="server" /></b>
                    </td>
                    <td width="100">
                        Hirer:
                    </td>
                    <td width="400">
                        <b>
                            <asp:Label ID="Lblhirer" runat="server" /></b>
                    </td>
                    <td width="100">
                        Status:
                    </td>
                    <td width="50">
                        <b>
                            <asp:Label ID="LblStatus" runat="server" /></b>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        
        
        <asp:UpdatePanel ID="UpdatePanelBookedProductDetails" runat="server">
            <ContentTemplate>
            
        <asp:Panel ID="pnlBookedPRoducts" runat="server" BackColor="lightgray" Width="800">
        <h5>
            Booked Product Details</h5>
            <asp:Repeater ID="repBookedProducts" runat="server">
                <HeaderTemplate>
                    <table width="100%" bordercolor="#C0C0C0" cellspacing="0" cellpadding="0" border="1" class="dataTable">
                    <thead>
                        <tr>
                        
                            <th width="50" style="border-color:Black;text-align: center;">
                                Rental
                            </th>
                            <th width="250" style="border-color:Black;text-align: center;">
                                Check-Out</th>
                            <th width="250" style="border-color:Black;text-align: center;">
                                Check-In</th>
                            <th width="150" style="border-color:Black;text-align: center;">
                                Hire Period</th>
                            <th width="150" style="border-color:Black;text-align: center;">
                                Package</th>
                            <th width="150" style="border-color:Black;text-align: center;">
                                Brand</th>
                            <th width="150" style="border-color:Black;text-align: center;">
                                Vehicle</th>
                            <th width="150" style="border-color:Black;text-align: center;">
                                Status</th>
                        </tr>
                    </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr align="center"  class="evenRow">
                        <td>
                            <%#Eval("Rental")%>
                        </td>
                        <td>
                            <%#Eval("Check-Out")%>
                        </td>
                        <td>
                            <%#Eval("Check-In")%>
                        </td>
                        <td>
                            <%#Eval("HirePd")%>
                        </td>
                        <td>
                            <%#Eval("Package")%>
                        </td>
                        <td>
                            <%#Eval("Brand")%>
                        </td>
                        <td>
                            <%#Eval("Vehicle")%>
                        </td>
                        <td>
                            <%#Eval("BookingSt")%>
                        </td>
                    </tr>
                </ItemTemplate>
                
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </asp:Panel>
        <br />
        <br />
        <asp:Panel runat="server" ID="pnlBookedProduct">
            <asp:GridView ID="gridBookedProduct" runat="server" 
                                                 DataKeyNames="SeqId" 
                                                 Width="800px" 
                                                 AutoGenerateColumns="False" CssClass="dataTableGrid">
                <RowStyle CssClass="evenRow" />
                <AlternatingRowStyle CssClass="oddRow" />
                <Columns>
                    <asp:TemplateField HeaderText="PRODUCT">
                        <ItemTemplate>
                            <%# Eval("PrdShortName") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FROM DATE">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtfromdate" Enabled='<%# eval("CkoDtMod") %>' Text='<%# eval("CkoDt") %>'
                                Width="100" visible="false" />
                                
                            <uc2:DateControl ID="frDateControl" runat="server" Date='<%# GetDate(eval("CkoDt")) %>'
                                Enabled='<%# eval("CkoDtMod") %>' />      
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="FROM LOCATION">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtfromlocation" Enabled='<%# eval("CkoLocMod") %>'
                                Text='<%# trim(eval("CkoLocCode")) %>' Width="100" MaxLength="3"/>
                                
                            <asp:RequiredFieldValidator 
                                            ID="RFV_txtfromlocation" 
                                            Enabled='<%# eval("CkoLocMod") %>'
                                            runat="server" 
                                            EnableClientScript="false"
                                            Display="Dynamic" 
                                            ControlToValidate="txtfromlocation"
                                            SetFocusOnError="True" 
                                            ErrorMessage="Check-Out Location is mandatory on existing booked products - please re-enter"
                                            ValidationGroup="btnNext">*</asp:RequiredFieldValidator>   
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TO DATE">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txttodate" Enabled='<%# eval("CkiDtMod") %>' Text='<%# eval("CkiDt") %>'
                                Width="100" visible="false" />
                                
                            <uc2:DateControl ID="toDateControl" runat="server" Date='<%# GetDate(eval("CkiDt")) %>'
                                Enabled='<%# eval("CkiDtMod") %>' />        
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TO LOCATION">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txttolocation" Enabled='<%# eval("CkiLocMod") %>'
                                Text='<%# trim(eval("CkiLocCode")) %>' Width="100" MaxLength="3"/>
                                <asp:RequiredFieldValidator 
                                            ID="RFV_txttolocation" 
                                            Enabled='<%# eval("CkiLocMod") %>'
                                            runat="server" 
                                            EnableClientScript="false"
                                            Display="Dynamic" 
                                            ControlToValidate="txttolocation"
                                            SetFocusOnError="True" 
                                            ErrorMessage="Check-In Location is mandatory on existing booked products - please re-enter"
                                            ValidationGroup="btnNext">*</asp:RequiredFieldValidator>   
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="RATE">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtRATE" Enabled='<%# eval("RateMod") %>' Text='<%# eval("Rate") %>'
                                Width="50" MaxLength="12" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="FLT_txtRATE" runat="server" FilterType="Numbers,Custom"
                                                TargetControlID="txtRATE"  ValidChars=".-" />
                                <asp:RequiredFieldValidator 
                                            ID="RFV_txtRATE" 
                                            Enabled='<%# eval("RateMod") %>'
                                            runat="server" 
                                            EnableClientScript="false"
                                            Display="Dynamic" 
                                            ControlToValidate="txtRATE"
                                            SetFocusOnError="True" 
                                            ErrorMessage="Rate is mandatory on existing booked products - please re-enter."
                                            ValidationGroup="btnNext">*</asp:RequiredFieldValidator>   
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UOM">
                        <ItemTemplate>
                            <%#Eval("UOM")%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="QTY">
                        <ItemTemplate>
                            <asp:TextBox runat="server" ID="txtQTY" Enabled='<%# eval("QtyMod") %>' Text='<%# eval("Qty") %>'
                                Width="50" MaxLength="5" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="FLT_txtQTY" runat="server" FilterType="Numbers"
                                                TargetControlID="txtQTY"  />
                                
                                <asp:RequiredFieldValidator 
                                            ID="RFV_txtQTY" 
                                            Enabled='<%# eval("QtyMod") %>'
                                            runat="server" 
                                            EnableClientScript="false"
                                            Display="Dynamic" 
                                            ControlToValidate="txtQTY"
                                            SetFocusOnError="True" 
                                            ValidationGroup="btnNext" errorMessage="Quantity is mandatory on existing booked products - please re-enter.">*</asp:RequiredFieldValidator>   
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="TOTAL OWING">
                        <ItemTemplate>
                            <%#Eval("PersonRate")%>
                            <asp:Label runat="server" ID="lblBookingRef" Text='<%# eval("BookingRef") %>' Width="50"
                                Visible="false" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Right" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            
            <br />
            <br />
            
            <%--Addition by Shoel - 16/10/8 - Fix for Person Dependent Rates--%>
        
            <div runat="server" style="display:none;width:800px" id="divPersonDependentRates" >
                <asp:Label ID = "lblPrrHead" runat="server" Text="Product Dependent Rates"></asp:Label>
                <asp:Repeater ID="rptPersonDependentRates" runat="server" >
                    <HeaderTemplate>
                    <table width="800px" border="0" cellpadding="1" cellspacing="0" class="dataTableGrid">
                        <THEAD>
                            <TR align="center">
                                <Th style="text-align: center" rowSpan="2">PRODUCT</Th>
                                <Th style="text-align: center" colSpan="2">ADULT* </Th>
                                <Th style="text-align: center" colSpan="2">CHILD* </Th>
                                <Th style="text-align: center" colSpan="2">INFANT* </Th>
                            </TR>
                            <TR align="center" >
                                <TH style="text-align: center">RATE</TH>
                                <TH style="text-align: center">QTY</TH>
                                <TH style="text-align: center">RATE</TH>
                                <TH style="text-align: center">QTY</TH>
                                <TH style="text-align: center">RATE</TH>
                                <TH style="text-align: center">QTY</TH>
                            </TR>
                        </THEAD>
                        <TBODY>
           </HeaderTemplate>
           
                    <ItemTemplate>
                            <tr align="center" class="evenRow">
                                <td nowrap="nowrap">
                                    <%#Container.DataItem("PrdShortName")%>
                                    <asp:HiddenField ID="hdnADULT_PrrId" runat="server" Value='<%#Container.DataItem("ADULT_PrrId")%>' />
                                    <asp:HiddenField ID="hdnCHILD_PrrId" runat="server" Value='<%#Container.DataItem("CHILD_PrrId")%>' />
                                    <asp:HiddenField ID="hdnINFANT_PrrId" runat="server" Value='<%#Container.DataItem("INFANT_PrrId")%>' />
                                    <asp:HiddenField ID="hdnSeqId" runat="server" Value='<%#Container.DataItem("SeqId")%>' />
                                </td>
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtADULT_Rate" runat="server" Text='<%#Container.DataItem("ADULT_Rate")%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeDecimal(event);" MaxLength="4" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("ADULT_RateMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnADULT_RateMod" runat="server" Value='<%#Container.DataItem("ADULT_RateMod")%>' />
                                    
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtADULT_Qty" runat="server" Text='<%#CINT(Container.DataItem("ADULT_Qty"))%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeInt(event);" MaxLength="2" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("ADULT_QtyMod"),False,True) %>'></asp:TextBox>
                                     <asp:HiddenField ID="hdnADULT_QtyMod" runat="server" Value='<%#Container.DataItem("ADULT_QtyMod")%>' />
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtCHILD_Rate" runat="server" Text='<%#Container.DataItem("CHILD_Rate")%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeDecimal(event);" MaxLength="4" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("CHILD_RateMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnCHILD_RateMod" runat="server" Value='<%#Container.DataItem("CHILD_RateMod")%>' />
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtCHILD_Qty" runat="server" Text='<%#CINT(Container.DataItem("CHILD_Qty"))%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeInt(event);" MaxLength="2" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("CHILD_QtyMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnCHILD_QtyMod" runat="server" Value='<%#Container.DataItem("CHILD_QtyMod")%>' />
                                </td>
                                
                               <td nowrap="nowrap">
                                    <asp:TextBox ID="txtINFANT_Rate" runat="server" Text='<%#Container.DataItem("INFANT_Rate")%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeDecimal(event);" MaxLength="4" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("INFANT_RateMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnINFANT_RateMod" runat="server" Value='<%#Container.DataItem("INFANT_RateMod")%>' />
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtINFANT_Qty" runat="server" Text='<%#CINT(Container.DataItem("INFANT_Qty"))%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeInt(event);" MaxLength="2" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("INFANT_QtyMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnINFANT_QtyMod" runat="server" Value='<%#Container.DataItem("INFANT_QtyMod")%>' />
                                </td>
                            </tr>
           
                     </ItemTemplate>
                     
                     <AlternatingItemTemplate>
                     <tr align="center" class="oddRow">
                                <td nowrap="nowrap">
                                    <%#Container.DataItem("PrdShortName")%>
                                    <asp:HiddenField ID="hdnADULT_PrrId" runat="server" Value='<%#Container.DataItem("ADULT_PrrId")%>' />
                                    <asp:HiddenField ID="hdnCHILD_PrrId" runat="server" Value='<%#Container.DataItem("CHILD_PrrId")%>' />
                                    <asp:HiddenField ID="hdnINFANT_PrrId" runat="server" Value='<%#Container.DataItem("INFANT_PrrId")%>' />
                                    <asp:HiddenField ID="hdnSeqId" runat="server" Value='<%#Container.DataItem("SeqId")%>' />
                                </td>
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtADULT_Rate" runat="server" Text='<%#Container.DataItem("ADULT_Rate")%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeDecimal(event);" MaxLength="4" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("ADULT_RateMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnADULT_RateMod" runat="server" Value='<%#Container.DataItem("ADULT_RateMod")%>' />
                                    
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtADULT_Qty" runat="server" Text='<%#CINT(Container.DataItem("ADULT_Qty"))%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeInt(event);" MaxLength="2" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("ADULT_QtyMod"),False,True) %>'></asp:TextBox>
                                     <asp:HiddenField ID="hdnADULT_QtyMod" runat="server" Value='<%#Container.DataItem("ADULT_QtyMod")%>' />
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtCHILD_Rate" runat="server" Text='<%#Container.DataItem("CHILD_Rate")%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeDecimal(event);" MaxLength="4" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("CHILD_RateMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnCHILD_RateMod" runat="server" Value='<%#Container.DataItem("CHILD_RateMod")%>' />
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtCHILD_Qty" runat="server" Text='<%#CINT(Container.DataItem("CHILD_Qty"))%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeInt(event);" MaxLength="2" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("CHILD_QtyMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnCHILD_QtyMod" runat="server" Value='<%#Container.DataItem("CHILD_QtyMod")%>' />
                                </td>
                                
                               <td nowrap="nowrap">
                                    <asp:TextBox ID="txtINFANT_Rate" runat="server" Text='<%#Container.DataItem("INFANT_Rate")%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeDecimal(event);" MaxLength="4" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("INFANT_RateMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnINFANT_RateMod" runat="server" Value='<%#Container.DataItem("INFANT_RateMod")%>' />
                                </td>
                                
                                <td nowrap="nowrap">
                                    <asp:TextBox ID="txtINFANT_Qty" runat="server" Text='<%#CINT(Container.DataItem("INFANT_Qty"))%>' 
                                    CssClass="TextBox_Standard" style="width:50px;text-align:right;"
                                    onkeypress="return keyStrokeInt(event);" MaxLength="2" OnBlur="CheckContents(this);"
                                    ReadOnly='<%# IIF(Eval("INFANT_QtyMod"),False,True) %>'></asp:TextBox>
                                    <asp:HiddenField ID="hdnINFANT_QtyMod" runat="server" Value='<%#Container.DataItem("INFANT_QtyMod")%>' />
                                </td>
                            </tr>
                     </AlternatingItemTemplate>
           
                    <FooterTemplate>
                        </tbody>
                </table>
                    
           </FooterTemplate>
           
           </asp:Repeater>
            </div>
        
            <%--Addition by Shoel - 16/10/8 - Fix for Person Dependent Rates--%>
        </asp:Panel>
        
        
        
        <asp:Panel ID="pnlAddReason" runat="server">
            <table width="100%">
                <tr>
                    <td >
                        <i>Please enter a reason for adding the following products:</i> <b> <asp:Label id="lblproductreason"  runat = "server" /></b>
                    </td>
                </tr>
                <tr>
                    <td style="height: 110px">
                        <asp:TextBox runat="server" ID="txtReasons" TextMode="MultiLine" Height="99px" Width="100%"  Visible="false"/>
                        <uc3:BookingProductsReasons ID="BookingProductsReasons1" runat="server" />
                        <br />
                        
                    </td>
                </tr>
                <tr width="100%" align="right">
                    <td>
                        <asp:Button ID="btnOK" Text="OK" runat="server" Width="100px" CssClass="Button_Standard Button_OK" />
                        <asp:Button ID="BtnCancel" Text="Reset" runat="server" Width="100px" CssClass="Button_Standard Button_Reset"/></td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>

        </asp:Panel>
        <table width="800">
            <tr width="100%" align="right">
                <td>
                    <asp:Button ID="btnNext" runat="server" Text="Next" Width="100" CssClass="Button_Standard Button_Next" ValidationGroup="btnNext"/>
                </td>
            </tr>
        </table>
                            
          </ContentTemplate>
        </asp:UpdatePanel>
     
        <br />

        <asp:Literal ID="litJS" runat="server" EnableViewState="true" Visible="false" />
        <asp:Literal ID="litAddRes" runat="server" EnableViewState="true" Visible="false" />
        <asp:Literal ID="LitJSuserRole" runat="server" EnableViewState="true" Visible="false" />
        <asp:Literal ID="LitxmlReceive" runat="server" EnableViewState="true" Visible="false" />
        <asp:Literal ID="Litxmldom" runat="server" EnableViewState="true" Visible="false" />
        <asp:UpdatePanel ID = "updConfirmationAndOverride" runat="server">
            <ContentTemplate>
                <uc1:SupervisorOverrideControl ID="SupervisorOverrideControl1" runat="server" btnCancel_AutoPostBack="true"  />
                <uc1:ConfimationBoxControl ID="cnfConfirmation" runat="server" LeftButton_Text="Enter Data"
                RightButton_Text="Skip Product" LeftButton_AutoPostBack="true" RightButton_AutoPostBack="true"
                Title="Confirmation Required" />        
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
