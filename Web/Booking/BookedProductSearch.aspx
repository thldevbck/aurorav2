<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	BookedProductSearch.aspx
''	Date Created	-	?.?.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	?.?.? / Base version
''                      18.7.8 / Shoel / fixed picker name issue (AKL - Auckland is now displayed, instead of just AKL).
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>
<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookedProductSearch.aspx.vb" Inherits="Booking_BookedProductSearch"
    Title="Untitled Page" %>

<%@ Register Src="~/UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="ucPickerControl" %>
<%@ Register Src="~/UserControls/DateControl/DateControl.ascx" TagName="DateControl"
    TagPrefix="ucDateControl" %>
<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <table align="left" border="0" width="100%" cellpadding="3">
        <tr>
            <td colspan="4">
                <b>Advanced Search Criteria</b>
            </td>
        </tr>
        <tr>
            <td width="150px">
                Status:</td>
            <td width="250px">
                <asp:DropDownList ID="cmbStatus" runat="server" CssClass="Dropdown_Large" Style="width: 240px">
                    <asp:ListItem Selected="true" Value="" Text="ALL">
                    </asp:ListItem>
                    <asp:ListItem Value="NC" Text="Non-Cancelled">
                    </asp:ListItem>
                    <asp:ListItem Value="IN" Text="IN - Inquiry">
                    </asp:ListItem>
                    <asp:ListItem Value="QN" Text="QN - Quotation">
                    </asp:ListItem>
                    <asp:ListItem Value="KB" Text="KB - Knock Back">
                    </asp:ListItem>
                    <asp:ListItem Value="WL" Text="WL - Wait Listed">
                    </asp:ListItem>
                    <asp:ListItem Value="NN" Text="NN - Provisional">
                    </asp:ListItem>
                    <asp:ListItem Value="KK" Text="KK - Confirmed">
                    </asp:ListItem>
                    <asp:ListItem Value="CO" Text="CO - Checked Out">
                    </asp:ListItem>
                    <asp:ListItem Value="CI" Text="CI - Checked In">
                    </asp:ListItem>
                    <asp:ListItem Value="XN" Text="XN - Cancelled Before Confirmation">
                    </asp:ListItem>
                    <asp:ListItem Value="XX" Text="XX - Cancelled After Confirmation">
                    </asp:ListItem>
                    <asp:ListItem Value="TK" Text="Ticketed"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td width="150px">
                Initial Status:
            </td>
            <td>
                <asp:DropDownList ID="cmbInitialStatus" runat="server" CssClass="Dropdown_Medium">
                    <asp:ListItem Selected="true" Value="" Text="ALL"></asp:ListItem>
                    <asp:ListItem Value="IN" Text="IN - Inquiry"></asp:ListItem>
                    <asp:ListItem Value="QN" Text="QN - Quotation"></asp:ListItem>
                    <asp:ListItem Value="KB" Text="KB - Knock Back"></asp:ListItem>
                    <asp:ListItem Value="WL" Text="WL - Wait Listed"></asp:ListItem>
                    <asp:ListItem Value="NN" Text="NN - Provisional"></asp:ListItem>
                    <asp:ListItem Value="KK" Text="KK - Confirmed"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Product:
            </td>
            <td>
                <ucPickerControl:PickerControl ID="pkrProduct" runat="server" PopupType="BookedProduct" />
            </td>
            <td>
                Product Class:
            </td>
            <td>
                <ucPickerControl:PickerControl ID="pkrProductClass" runat="server" PopupType="BookedProductClass" />
            </td>
        </tr>
        <tr>
            <td>
                Product Type:
            </td>
            <td>
                <ucPickerControl:PickerControl ID="pkrProductType" runat="server" PopupType="BookedProductType" />
            </td>
            <td>
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td>
                Check Out Date:
            </td>
            <td>
                <ucDateControl:DateControl ID="dtcCheckOutDate" runat="server" />
            </td>
            <td>
                Check Out Location:
            </td>
            <td>
                <ucPickerControl:PickerControl ID="pkrCheckOutLocation" runat="server" PopupType="CHKOLOC" AppendDescription="true" />
            </td>
        </tr>
        <tr>
            <td>
                Check In Date:
            </td>
            <td>
                <ucDateControl:DateControl ID="dtcCheckInDate" runat="server" />
            </td>
            <td>
                Check In Location:
            </td>
            <td>
                <ucPickerControl:PickerControl ID="pkrCheckInLocation" runat="server" PopupType="CHKILOC" AppendDescription="true" />
            </td>
        </tr>
        <tr>
            <td>
                Type:
            </td>
            <td>
                <asp:DropDownList CssClass="Dropdown_Large" runat="server" ID="cmbType" Style="width: 240px">
                </asp:DropDownList>
            </td>
            <td nowrap="nowrap">
                External Booking Reference:
            </td>
            <td>
                <asp:TextBox ID="txtExternalBookingRef" runat="server" CssClass="Textbox_Small" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>
                Charge to:
            </td>
            <td>
                <asp:DropDownList ID="cmbChargeTo" runat="server" CssClass="Dropdown_Small">
                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                    <asp:ListItem Value="0" Text="Agent"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Customer"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Current:
            </td>
            <td>
                <asp:DropDownList ID="cmbIsCurrent" runat="server" CssClass="Dropdown_Small">
                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Original:
            </td>
            <td>
                <asp:DropDownList ID="cmbIsOriginal" runat="server" CssClass="Dropdown_Small">
                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                FOC Products:
            </td>
            <td>
                <asp:DropDownList ID="cmbFOCProducts" runat="server" CssClass="Dropdown_Small">
                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Transfered to Accounts:
            </td>
            <td>
                <asp:DropDownList ID="cmbTransferredToAccounts" runat="server" CssClass="Dropdown_Small">
                    <asp:ListItem Value="" Text="ALL"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="0" Text="No"></asp:ListItem>
                </asp:DropDownList>
            </td>
            <td>
                Record Id:
            </td>
            <td>
                <asp:TextBox ID="txtRecordId" runat="server" CssClass="Textbox_Small" MaxLength="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="right">
                <asp:Button ID="btnSearch" runat="server" CssClass="Button_Standard Button_Search" Text="Search" />
                <asp:Button ID="btnCancel" runat="server" CssClass="Button_Standard Button_Reset" Text="Reset" />
            </td>
        </tr>
    </table>
</asp:Content>
