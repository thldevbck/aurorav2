<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="RentalEdit.aspx.vb" Inherits="Booking_RentalEdit" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Src="~\UserControls\PickerControl\PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<%@ Register Src="~\UserControls\DateControl\DateControl.ascx" TagName="DateControl"
    TagPrefix="uc2" %>
<%@ Register Src="~\UserControls\TimeControl\TimeControl.ascx" TagName="TimeControl"
    TagPrefix="uc3" %>
<%@ Register Src="~\Booking\Controls\ContactAgentMgtPopupUserControl.ascx" TagName="ContactAgentMgtPopupUserControl"
    TagPrefix="uc4" %>
<%@ Register Src="~/UserControls/CollapsiblePanel/CollapsiblePanel.ascx" TagName="CollapsiblePanel"
    TagPrefix="uc5" %>

<%--rev:mia 13jan2015-addition of supervisor override password--%>
<%@ Register Src="~/UserControls/SlotAvailableControl/SlotAvailableControl.ascx" TagName="SlotAvailableControl"   TagPrefix="uc6" %>
<%@ Register src="~/UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"     tagname="SupervisorOverrideControl" tagprefix="uc7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">

        

        var errorMessage = <%= MessageJSON %>;

        function ClientValidation()
        { 
            var pickerTextBox;
            pickerTextBox = document.getElementById('<%=packagePickerControl.FindControl( "pickerTextBox").ClientID%>');
    
            if (pickerTextBox.value == "")
            {
                setWarningShortMessage(getErrorMessage("GEN005"))
                return false
            } 
     
            var calendarTextBox;
    
            calendarTextBox = document.getElementById('<%=bookedDateControl.ClientID%>')
            if (calendarTextBox.value == "")
            {
                setWarningShortMessage(getErrorMessage("GEN005"))
                return false
            }    
        }

       function MarkOptionAsDisabledPageLoad()
       {
        
        clearMessages()
        var myselect = document.getElementById("ctl00_ContentPlaceHolder_ddlArrivalTime");
        var isDisabled = false
        var isSelected = false
        var lastindex = myselect.options[myselect.selectedIndex].value;
        var opts = myselect.options;
        var opt=0;
        var selectedindex =-1
        for(opt=0; opt<opts.length; opt++) 
        {
            
            if(opts[opt].getAttribute("default") != null)
            {
                if(opts[opt].getAttribute("default") == 'Y')
                {
                    selectedindex = opt;
                    break
                }
            }
        }

                for(opt=0; opt<opts.length; opt++) 
                {
                   
                    if(opts[opt].getAttribute("disabled") != null)
                    {
                       
                        isDisabled = opts[opt].getAttribute("disabled");
                        
                        if(isDisabled == 'True' || isDisabled == true)
                        {
                            if(lastindex == opts[opt].value)
                            {
                                myselect.selectedIndex = selectedindex;
                                
                                setWarningMessage("The slot " + opts[opt].text + " is not available.");
                                break;
                            }
                        }
                    }
                }
       }

//        function MarkOptionAsDisabled()
//        {
//            var myselect = document.getElementById("ctl00_ContentPlaceHolder_ddlArrivalTime");
//            var isdisabled = myselect.options[myselect.selectedIndex].getAttribute('disabled')
//            if(isdisabled != null)
//            {
//                alert(myselect.options[myselect.selectedIndex].getAttribute('disabled'));
//                myselect.options[myselect.selectedIndex].disabled = true
//            }
//        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <%--
    <div style="display: none">
        <asp:Label ID="knockBackLabel" runat="server" Text=""></asp:Label>
        <asp:Label ID="agentIdLabel" runat="server" Text=""></asp:Label>
    </div>
    --%>
    <asp:Panel ID="panel11" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td width="6%">
                    Agent:</td>
                <td width="45%">
                    <asp:TextBox ID="agentTextBox" runat="server" ReadOnly="true" Width="350"></asp:TextBox>
                </td>
                <td width="6%">
                    Hirer:</td>
                <td width="20%">
                    <asp:TextBox ID="hirerTextBox" runat="server" ReadOnly="true" Width="150"></asp:TextBox>
                </td>
                <td width="6%">
                    Status:</td>
                <td>
                    <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="130"></asp:TextBox>
                </td>
            </tr>
            
        </table>
        <br />
        <table width="100%">
            <tr>
                <td width="100%">
                    <asp:GridView ID="rentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                        CssClass="dataTable">
                        <AlternatingRowStyle CssClass="oddRow" />
                        <RowStyle CssClass="evenRow" />
                        <Columns>
                            <asp:BoundField DataField="RntNum" HeaderText="Rental" ReadOnly="True" />
                            <%--<asp:BoundField DataField="Check-Out" HeaderText="Check-out" ReadOnly="True" />
                            <asp:BoundField DataField="Check-In" HeaderText="Check-in" ReadOnly="True" />--%>
                            <asp:TemplateField HeaderText="Check-out" >
                                    <ItemTemplate>
                                        <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-Out")) + GetArrivalDepartureTime(eval("RntArrivalAtBranchDateTime")) %>'></asp:Label>
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Check-in" >
                                    <ItemTemplate>
                                        <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-In")) + GetArrivalDepartureTime(eval("RntDropOffAtBranchDateTime")) %>'></asp:Label>
                                    </ItemTemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                            <asp:BoundField DataField="PkgCode" HeaderText="Package" ReadOnly="True" />
                            <asp:BoundField DataField="BrdName" HeaderText="Brand" ReadOnly="True" />
                            <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                            <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
                            <asp:ButtonField Text="   " CommandName="Select" ItemStyle-Width="30"></asp:ButtonField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
        <br />
        <uc5:CollapsiblePanel ID="detailsCollapsiblePanel" runat="server" Title="List of Other Rentals"
            TargetControlID="detailsContentPanel" />
        <asp:Panel ID="detailsContentPanel" runat="server" Width="100%">
            <table width="100%">
                <tr>
                    <td>
                    </td>
                </tr>
                <asp:Label ID="otherRentalLabel" runat="server" Text=""></asp:Label>
                <tr>
                    <td width="100%">
                        <asp:GridView ID="otherRentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                            CssClass="dataTable">
                            <AlternatingRowStyle CssClass="oddRow" />
                            <RowStyle CssClass="evenRow" />
                            <Columns>
                                <asp:BoundField DataField="RentalNo" HeaderText="Rental" ReadOnly="True" />
                                <%--<asp:BoundField DataField="Check-Out" HeaderText="Check-out" ReadOnly="True" />
                                <asp:BoundField DataField="Check-In" HeaderText="Check-in" ReadOnly="True" />--%>
                                <asp:TemplateField HeaderText="Check-out" >
                                    <ItemTemplate>
                                        <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-Out")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Check-in" >
                                        <ItemTemplate>
                                            <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-In")) %>'></asp:Label>
                                        </ItemTemplate>
                                 </asp:TemplateField>
                                <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                                <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" />
                                <asp:BoundField DataField="Brand" HeaderText="Brand" ReadOnly="True" />
                                <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                                <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
                                <asp:TemplateField ItemStyle-Width="30">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl='<%# GetRentalUrl(Eval("RntId")) %>'>View</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <br />
        <table width="100%">
            <tr>
                <td width="10%">
                    Package:</td>
                <td width="57%">
                    <uc1:PickerControl ID="packagePickerControl" runat="server" PopupType="AGENTPACKAGE"
                        AppendDescription="true" Width="400" />&nbsp;*
                </td>
                <td width="10%">
                    Initial Status:</td>
                <td width="23%">
                    <asp:TextBox ID="initialStatusTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <%--rev:mia Oct 16 2014 - addition of PromoCode--%>
            <tr>
                <td>    
                     Promo Code:
                </td>
                <td>
                                        <asp:Textbox runat="server" ID="promocodeTextbox" Width="145"  MaxLength="30"></asp:Textbox>
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td colspan="6">
                    <b>Agent Details</b>
                </td>
            </tr>
            <tr>
                <td width="10%">
                    Contact:</td>
                <td width="24%">
                    <asp:DropDownList ID="agentContactDropDownList" runat="server" Width="80%">
                    </asp:DropDownList>
                    <asp:ImageButton ID="agentContactImageButton" runat="server" ImageUrl="~/Images/ShowDetail.gif" ImageAlign="AbsMiddle" />
                </td>
                <td width="10%">
                    Agent Ref:</td>
                <td width="23%">
                    <asp:TextBox ID="agentRefTextBox" runat="server" MaxLength="30"></asp:TextBox>
                </td>
                <td width="10%">
                    Voucher No:</td>
                <td width="23%">
                    <asp:TextBox ID="agentVoucherNoTextBox" runat="server" MaxLength="64"></asp:TextBox>
                </td>
            </tr>
            <%--rev:mia july 4 2012 - start-- addition of 3rd party field--%>
            <tr>
                <td>
                    3rd Party Ref:
                </td>
                <td>
                    <asp:TextBox ID="txtthirdParty"  runat="server" Width="140" MaxLength="64" />
                </td>
            </tr>
            <%--rev:mia july 4 2012 - end-- addition of 3rd party field--%>
        </table>
        <br />
       <table width="100%" >
         
         <tr runat="server" id="overrideSlotTR" visible="false">
                 <td>
                        Override Slot:   
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="overrideCheckbox" AutoPostBack="true" />
                </td>
         </tr>

         <tr>
                <td width="77px">
                        <label runat="server" id="arrivalslot">Arrival Time:</label>
                </td>
                <td>
                        <asp:DropDownList ID="ddlArrivalTime" runat="server" EnableViewState="true" Style="width: 147px" autopostback="false"></asp:DropDownList>
                        <label runat="server" id="arrivaltimeestimate" visible="false">(estimated)</label>
                </td>
            
            </tr>
            <tr>
                <td width="77px">
                        Depart Time:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDepartureTime" runat="server" EnableViewState="false" Style="width: 147px"></asp:DropDownList>
                       
                </td>
            </tr>
        </table>
        <br />
        <table width="100%">
            <tr>
                <td width="34%">
                    <b>Flight connections</b></td>
                <td>
                     <b>Customers</b></td>
                <td>
                </td>
            </tr>
            <tr>
                <td width="44%" rowspan="3">
                    <asp:Panel ID="panel2" runat="server" Width="95%">
                        <table width="100%" class="dataTable">
                            <thead>
                            <tr>
                                <th width="24%">
                                </th>
                                <th width="24%">
                                    Reference</th>
                                <th >
                                    Date</th>
                                <th ></th> </tr>
                            </thead>
                            <tr>
                                <td>
                                    Arrival:</td>
                                <td>
                                    <asp:TextBox ID="arrRefTextBox" runat="server" Width="90%" MaxLength="64"></asp:TextBox></td>
                                <td>
                                    <uc2:DateControl ID="arrDateControl" runat="server" Nullable="true" />
                                    </td>
                                    <td>
                                    <uc3:TimeControl ID="arrTimeControl" runat="server" Nullable="true" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Departure:</td>
                                <td>
                                    <asp:TextBox ID="depRefTextBox" runat="server" Width="90%" MaxLength="64"></asp:TextBox></td>
                                <td>
                                    <uc2:DateControl ID="depDateControl" runat="server" Nullable="true" /></td>
                                <td>    <uc3:TimeControl ID="depTimeControl" runat="server" Nullable="true" /></td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
                <td width="10%">
                    Adult:</td>
                <td width="13%">
                    <asp:TextBox ID="adultTextBox" runat="server" Width="70" onkeypress="keyStrokeInt();" style="text-align: right" MaxLength="5"></asp:TextBox>
  
                </td>
                <td width="10%">
                   </td>
                <td width="23%">
                    <asp:CheckBox ID="vipRentalCheckBox" runat="server" Text="VIP Rental" />
                </td>
            </tr>
            <tr>
                <td>
                    Children:</td>
                <td>
                    <asp:TextBox ID="childrenTextBox" runat="server" Width="70" onkeypress="keyStrokeInt();" style="text-align: right" MaxLength="5"></asp:TextBox>

                </td>
                <td>
                    </td>
                <td>
                    <asp:CheckBox ID="refCustomerCheckBox" runat="server" Text="Repeat Customer" /></td>
            </tr>
            <tr>
                <td>
                    Infants:</td>
                <td>
                    <asp:TextBox ID="infantsTextBox" runat="server" Width="70" onkeypress="keyStrokeInt();" style="text-align: right" MaxLength="5"></asp:TextBox>
    
                </td>
                <td>
                    </td>
                <td>
                    <asp:CheckBox ID="partOfConvoyCheckBox" runat="server" Text="Part of Convoy" /></td>
            </tr>
        </table>

        <br/>
        <br/>
        <%--Arrival-departure details section table--%>
        <table>
            <tbody>
                <tr>
                    <td colspan="2"><b>Customer Pickup Service Details</b></td>
                    <td colspan="2"><b>Customer Departure Service Details</b></td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        Pickup
                    </td>
                    <td>
                        <uc2:DateControl ID="dateArrivalPickup" runat="server" Nullable="false" />
                        <uc3:TimeControl ID="timeArrivalPickup" runat="server" Nullable="false" />
                    </td>
                    <td>
                        Pickup
                    </td>
                    <td>
                        <uc2:DateControl ID="dateDeparturePickup" runat="server" Nullable="false" />
                        <uc3:TimeControl ID="timeDeparturePickup" runat="server" Nullable="false" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Venue type
                    </td>
                    <td>
                        <asp:DropDownList ID="lstArrivalVenue" runat="server"></asp:DropDownList>
                    </td>
                    <td>
                        Venue type
                    </td>
                    <td>
                        <asp:DropDownList ID="lstDepartureVenue" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-right:20px; vertical-align:top">
                        <b>Address</b>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>Venue name:</td>
                                <td>
                                    <asp:TextBox ID="txtArrivalVenueName" MaxLength="40" CssClass="Textbox_Large" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Street:</td>
                                <td>
                                    <asp:TextBox ID="txtArrivalStreet" MaxLength="40" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Suburb:</td>
                                <td><asp:TextBox ID="txtArrivalSuburb" MaxLength="40" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>City / Town:</td>
                                <td>
                                    <asp:TextBox ID="txtArrivalCity" MaxLength="20" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>State:</td>
                                <td><asp:TextBox ID="txtArrivalState" MaxLength="10" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>PostCode:</td>
                                <td><asp:TextBox ID="txtArrivalPostCode" MaxLength="10" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Country:</td>
                                <td><uc1:PickerControl ID="pkrArrivalCountry" PopupType="POPUPCOUNTRY" AppendDescription="true" width="150" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Special request:</td>
                                <td>
                                    <asp:TextBox TextMode="MultiLine" Rows="10" Columns="40" ID="txtArrivalSpecialInstructions" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="padding-right:20px; vertical-align:top">
                        <b>Address</b>
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>Venue name:</td>
                                <td>
                                    <asp:TextBox ID="txtDepartureVenueName" MaxLength="40" CssClass="Textbox_Large" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Street:</td>
                                <td>
                                    <asp:TextBox ID="txtDepartureStreet" MaxLength="40" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>Suburb:</td>
                                <td><asp:TextBox ID="txtDepartureSuburb" MaxLength="40" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>City / Town:</td>
                                <td>
                                    <asp:TextBox ID="txtDepartureCity" MaxLength="20" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>State:</td>
                                <td><asp:TextBox ID="txtDepartureState" MaxLength="10" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>PostCode:</td>
                                <td><asp:TextBox ID="txtDeparturePostCode" MaxLength="10" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td>Country:</td>
                                <td><uc1:PickerControl ID="pkrDepartureCountry" PopupType="POPUPCOUNTRY" AppendDescription="true" width="150" runat="server" /></td>
                            </tr>
                            <tr>
                                <td>Special request:</td>
                                <td>
                                    <asp:TextBox TextMode="MultiLine" Rows="10" Columns="40" ID="txtDepartureSpecialInstructions" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
        <br />
          <asp:UpdatePanel ID="contentUpdatePanel1" runat="server">
        <ContentTemplate>
        <table width="100%">
            <tr>
                <td>
                    <b>Dates</b></td>
            </tr>
            <tr>
                <td width="10%">
                    Booked:</td>
                <td width="24%">
                    <uc2:DateControl ID="bookedDateControl" runat="server" Nullable="false" AutoPostBack="true" />&nbsp;*
                </td>
                <td width="10%">
                    Rental Source:</td>
                <td width="23%">
                    <asp:DropDownList ID="rentalSourceDropDownList" runat="server">
                    </asp:DropDownList></td>
                <td width="10%">
                    Follow up:</td>
                <td width="23%">
                    <asp:TextBox ID="followUpDateTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Trigger:</td>
                <td>
                    <asp:Label ID="triggerLabel" runat="server" Text=""></asp:Label>
                    <asp:TextBox ID="triggerTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    Old Rental:</td>
                <td>
                    <asp:TextBox ID="oldRentalNumTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
                <td>
                    Transferred:</td>
                <td>
                    <asp:Label ID="transferredLabel" runat="server" Text=""></asp:Label>
                    <asp:TextBox ID="transferredTextBox" runat="server" ReadOnly="true"></asp:TextBox>
                </td>
            </tr>
        </table>
        </ContentTemplate>
        </asp:UpdatePanel> 
        
        <br />
        
        <table width="100%">
            <tr>
                <td align="right">
                    <asp:Button ID="saveButton" runat="server" Text="Save" CssClass="Button_Standard Button_Save"
                        OnClientClick="return ClientValidation();" />
                    <asp:Button ID="backButton" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                </td>
            </tr>
        </table>
        <uc4:ContactAgentMgtPopupUserControl ID="ContactAgentMgtPopupUserControl1" runat="server" />
        <asp:DataGrid ID="datagrid1" runat="server">
        </asp:DataGrid>
        <asp:DataGrid ID="datagrid2" runat="server">
        </asp:DataGrid>
        <asp:DataGrid ID="datagrid3" runat="server">
        </asp:DataGrid>
        <br />

        <%--rev:mia 15jan2014-addition of supervisor override password--%>
        <uc6:SlotAvailableControl runat="server" ID="SlotAvailableControl" />
        <uc7:SupervisorOverrideControl ID="SlotSupervisorOverrideControl" runat="server"  btnCancel_AutoPostBack="true"  />
    </asp:Panel>
</asp:Content>
