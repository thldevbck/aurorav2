Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports system.Threading
Imports System.Globalization

<AuroraPageTitleAttribute("Customer Search")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingRequest)> _
<AuroraMessageAttribute("GEN022")> _
Partial Class Booking_CustomerFind
    Inherits AuroraPage

    Private bookingId As String = ""
    Private rentalId As String = ""
    Private isFromSearch As String = "0"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            'bookingId = "3285499"
            'rentalId = "3285499-2"

            'If Not Request.QueryString("sTxtSearch") Is Nothing Then
            '    textSearch = Request.QueryString("sTxtSearch").ToString()
            'End If
            If Not Request.QueryString("hdBookingId") Is Nothing Then
                bookingId = Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                rentalId = Request.QueryString("hdRentalId").ToString()
            End If
            If Not Request.QueryString("isFromSearch") Is Nothing Then
                isFromSearch = Request.QueryString("isFromSearch").ToString()
            End If

            'If Not Page.IsPostBack Then
            '    If Not Request.QueryString("activeTab") Is Nothing Then
            '        Dim activeTab As String
            '        activeTab = Request.QueryString("activeTab").ToString()
            '        Tabs.ActiveTabIndex = Utility.ParseInt(activeTab)
            '    End If

            '    loadTabControls()
            '    dataBindBookingDetail()
            'End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click
        search(1)
    End Sub

    Protected Sub soundexButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles soundexButton.Click
        search(0)
    End Sub

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        Response.Redirect("CustomerFind.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&isFromSearch=" & isFromSearch)
    End Sub

    Protected Sub newButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles newButton.Click
        ''rev:mia 591 Customer Search page - can't save customer details
        Dim lastname As String = "&lnm=" & lastNameTextBox.Text
        Dim firstname As String = "&fnm=" & firstNameTextBox.Text
        Dim dateofbirth As String = "&dob=" & dobDateControl.Text
        Dim vip As String = "&vip=" & vipCheckBox.Checked.ToString
        Dim fleet As String = "&flt=" & fleetDriverCheckBox.Checked.ToString
        Dim street As String = "&str=" & streetTextBox.Text
        Dim city As String = "&cty=" & cityTextBox.Text
        Dim state As String = "&sta=" & stateTextBox.Text
        Dim licensenumber As String = "&lic=" & licenceTextBox.Text
        Dim country As String = "&ctry=" & countryPickerControl.Text

        Dim strQS As String = String.Concat(lastname, firstname, dateofbirth, vip, fleet, street, city, state, licensenumber, country)

        Response.Redirect("CustomerEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&customerId=" & "&isFromSearch=" & isFromSearch & strQS)
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect("Booking.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=" & BookingTabIndex.Customer & "&isFromSearch=" & isFromSearch)
    End Sub

    Private Sub search(ByVal search As Integer)

        If Not dobDateControl.IsValid Then
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
            Return
        End If

        Dim dt As DataTable
        dt = BookingCustomer.FindCustomer(search, lastNameTextBox.Text, firstNameTextBox.Text, _
            dobDateControl.Date, vipCheckBox.Checked, streetTextBox.Text, cityTextBox.Text, _
            stateTextBox.Text, countryPickerControl.DataId, licenceTextBox.Text, _
            fleetDriverCheckBox.Checked, 0, UserCode)

        If dt IsNot Nothing Then
            If dt.Rows.Count = 0 Then
                SetShortMessage(AuroraHeaderMessageType.Warning, "No Customers Found !")
                resultLabel.Text = "No customers found"
                dt = Nothing
            ElseIf dt.Rows.Count = 1 Then
                Dim customerId As String
                customerId = dt.Rows(0)("CusId").ToString()
                If Not String.IsNullOrEmpty(customerId) Then
                    Response.Redirect("./CustomerMgt.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&customerId=" & customerId)
                    Return
                Else
                    SetShortMessage(AuroraHeaderMessageType.Warning, "No Customers Found !")
                    resultLabel.Text = "No customers found"
                    dt = Nothing
                End If
            ElseIf dt.Rows.Count < 1001 Then
                resultGridView.DataSource = dt
                resultGridView.DataBind()
                resultLabel.Text = resultGridView.Rows.Count & " customers found"
            Else
                dt.Columns.Add("NewColumn", GetType(Int32))
                For i As Integer = 0 To dt.Rows.Count - 1
                    dt.Rows(i)("NewColumn") = i
                Next i

                Dim dv As New DataView(dt)
                dv.RowFilter = ("NewColumn > 0 And NewColumn < 1001")

                resultGridView.DataSource = dv
                resultGridView.DataBind()
                resultLabel.Text = resultGridView.Rows.Count & " customers found"

                SetShortMessage(AuroraHeaderMessageType.Warning, "GEN022/GEN022 - Search Criteria has more than 1000 records")
                Return
            End If
        Else
            SetShortMessage(AuroraHeaderMessageType.Warning, "No Customers Found !")
            resultLabel.Text = "No customers found"
            dt = Nothing
        End If

        resultGridView.DataSource = dt
        resultGridView.DataBind()

    End Sub

    Protected Function GetCustomerUrl(ByVal customerId As Object) As String
        Dim customerIdString As String
        customerIdString = Convert.ToString(customerId)

        'Return "./CustomerMgt.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&customerId=" & customerIdString
        Return "./CustomerEdit.aspx?hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&customerId=" & customerIdString & "&isFromSearch=" & isFromSearch

    End Function

End Class
