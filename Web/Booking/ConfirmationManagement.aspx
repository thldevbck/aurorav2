<%--
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''
''	Screen Name	    -	ConfirmationManagement.aspx
''	Date Created	-	15.4.8
''	Author		    -	Shoel Palli 
''	Modified Hist	-	15.4.8 / Base version
''                       5.6.8 / Fixes for Issue 1 (http://akl-kxdev-001/IssueTracker/Issues/IssueDetail.aspx?id=1)
''                      13.1.9 / Fix for forgotten functionality + to add update panels as needed
''
''
''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
--%>

<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="ConfirmationManagement.aspx.vb" Inherits="Booking_ConfirmationManagement"
    Title="Untitled Page" %>

<asp:Content ID="cntStyle" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="cntScript" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">
    function checkMaxLengthForPersonalNote(obj)
    {
	    var slen
	    slen = (obj.value).length
	    if (slen > 1000)
	    {
		    obj.value = (obj.value).substring(0,1000)
		    setWarningShortMessage("Cannot Insert more then 1000 characters !")
		    //return
	    }
    }
    
    function SelectAllCheck()
    {
        var chkAll = event.srcElement;
        
                      //chkAll....The TD.....The TR........The Table                                        
        var tblMain = chkAll.parentElement.parentElement.parentElement;   
                    
        for (var i=0;i<tblMain.children.length;i++)
        {
            tblMain.children(i).children(0).children(0).checked = chkAll.checked;
        }            
        
    }
    
    function ShowConfirmationScreen(ConfirmationData,ConfirmationId)
    {
        //this function shows the confirmation for printing
        if ((ConfirmationData!="") && (ConfirmationId!=""))
        {
            var Win = window.open("ConfirmationViewScreen.aspx?CnfData=" + ConfirmationData, "", "top=0,left=0,menubar=yes, scrollbars=yes ,height=" + screen.availHeight + " ,width=" + screen.availWidth)
            Win.focus();
            Win.resizeTo(Win.screen.availWidth,Win.screen.availHeight);
            Win.moveTo(0,0);
            return false;
        }
        else
        {
            setWarningShortMessage("Please save the confirmation first");
            return false;
        }
    }
    
    function GoBackToBookingsPage(BooId,RntId)
    {
        window.location = "Booking.aspx?hdBookingId=" + BooId + "&hdRentalId=" + RntId + "&activeTab=3";//"&FromConfirmationView=1"
        return false;
    }


    //    rev:mia Sept 2 2013 -    this will allow confirmation email to be 
    //                             process by adding multiple checkboxes
    $(document).ready(function () {
      //  $('input[type=checkbox], label').click(function (e) {
        $('.cblRentals, label').click(function (e) {

            GetRentaIds()

            if (!e) var e = window.event;
            e.cancelBubble = true;
            if (e.stopPropagation) e.stopPropagation();
        });

    });

    function GetRentaIds() {

        var rentals = ""
        var count = 0
      
        $('.cblRentals :checked').each(function () {
        
            var value = $(this).prev().text()

            rentals = rentals + value + ','
            count = count + 1
        });

        if (count > 0) {
            var len = $('.cblRentals :checkbox').length
            //alert(len)
            if (len == count) {
                rentals = "All"
            }
            else {
                rentals = rentals.substring(0, rentals.length - 1)
            }

            $('#ctl00_ContentPlaceHolder_txtrentals').val(rentals);
        }
        else {
            $('#ctl00_ContentPlaceHolder_txtrentals').val("");
        }
        $('[id$=_hidRentalList]').val($('#ctl00_ContentPlaceHolder_txtrentals').val());


        $(".Button_Save").attr("disabled", (count > 0 ? false : true))
        $(".Button_Report").attr("disabled", (count > 0 ? false : true))
    }
    </script>

</asp:Content>
<asp:Content ID="cntContent" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
<script type="text/javascript">
    var pbControl = null;
    var prm = Sys.WebForms.PageRequestManager.getInstance();

    prm.add_beginRequest(BeginRequestHandler);
    prm.add_endRequest(EndRequestHandler);
    function BeginRequestHandler(sender, args) {
        pbControl = args.get_postBackElement();
        if (pbControl.id.indexOf('cblRentals') > 1) {
            $("ctl00_ContentPlaceHolder_cblRentals").attr("disabled", true)
        }
    }
    function EndRequestHandler(sender, args) {

        if (args.get_error() != undefined) {
            args.set_errorHandled(true);
        }

        if (pbControl.id.indexOf('cblRentals') > 1) {
            $("ctl00_ContentPlaceHolder_cblRentals").attr("disabled",false)
        }
    }
    
</script>
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <%-- <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>--%>
                <table cellspacing="0" cellpadding="0" width="100%" border="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="updMain" runat="server">
                                <ContentTemplate>
                                    <table border="0" width="100%">
                                        <tr>
                                            <td>
                                                <table cellspacing="0" cellpadding="4" width="100%" border="0">
                                                    <tr>
                                                        <td width="150px">
                                                            Booking:</td>
                                                        <td width="250px">
                                                            <asp:TextBox runat="server" ReadOnly="true" ID="lblBooking" CssClass="Textbox_Small"></asp:TextBox>
                                                        </td>
                                                        <td width="150px">
                                                            Rental:</td>
                                                        <td>

                                                         <%--rev:mia Sept 2 2013 -    this will allow confirmation email to be  process by adding multiple checkboxes--%>
                                                        <td align="left">
                                                            <input type="text" runat="server"  value="All" id="txtrentals" style="text-align: center; width:80px;"  readonly="readonly"/>
                                                             <ajaxToolkit:DropDownExtender runat="server" 
                                                                      ID="DDE"  
                                                                      TargetControlID="txtrentals"   
                                                                      DropDownControlID="pnlRentals"  
                                                                       />  
                                                            <asp:Panel ID="pnlRentals" 
                                                                   runat="server" 
                                                                   BorderColor="Gray" 
                                                                   BackColor="White"
                                                                   BorderWidth="1"
                                                                   style="margin-top: 8px; position: absolute; width: 35px;">
                                                                   
                                                                   <asp:CheckBoxList ID="cblRentals" runat="server" 
                                                                             RepeatColumns="1" 
                                                                             RepeatDirection="Vertical" 
                                                                             RepeatLayout="table" 
                                                                             CellPadding = "3" 
                                                                             CellSpacing = "3"
                                                                             TextAlign ="Left" 
                                                                             AutoPostBack="true" 
                                                                             OnSelectedIndexChanged="cblRentals_SelectedIndexChanged"
                                                                             CssClass="cblRentals"
                                                                              />
                                                                   </asp:Panel>
                                                            <asp:DropDownList ID="cmbRental" runat="server" CssClass="Dropdown_Small" AutoPostBack="true" Enabled="false" Visible ="false">
                                                            </asp:DropDownList>&nbsp;
                                                            *
                                                            &nbsp;
                                                            <asp:CheckBox ID="chkRentalCheckall" runat ="server" AutoPostBack="true" /> <asp:Label ID="labelRentalCheckall" Text="" runat="server" />
                                                        </td>
                                                        
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" width="150px">
                                                            Audience:</td>
                                                        <td width="250px">
                                                            <asp:DropDownList ID="cmbAudience" runat="server" CssClass="Dropdown_Small">
                                                            </asp:DropDownList>&nbsp;*
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td nowrap="nowrap" width="150px">
                                                            Country of Travel:</td>
                                                        <td width="250px">
                                                            <asp:DropDownList ID="cmbCountryOfTravel" runat="server" CssClass="Dropdown_Standard"
                                                                AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <%--<tr>
                                <td>
                                    &nbsp;&nbsp;
                                   <asp:Panel ID="pnlCountryOfTravel" runat="server">
                                    </asp:Panel>
                                </td>
                            </tr>--%>
                    <tr>
                        <td>
                            <table width="100%" cellspacing="0" cellpadding="4" border="0">
                                <tr>
                                    <td>
                                        Personal Note (Header):
                                    </td>
                                    <td align="right" style="visibility:hidden;">
                                        Highlight Note:
                                        <asp:CheckBox ID="chkHighlightNote" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <%--<textarea id="txaHeader" style="width: 100%" runat="server" onkeyup="checkMaxLengthForPersonalNote(this);"
                                                    onblur="checkMaxLengthForPersonalNote(this);" class="Textarea_Large"></textarea>--%>
                                        <asp:TextBox ID="txaHeader" runat="server" TextMode="MultiLine" MaxLength="1000"
                                            Style="width: 100%; height:150px;" onkeyup="checkMaxLengthForPersonalNote(this);" onblur="checkMaxLengthForPersonalNote(this);"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style=" visibility:hidden;">
                                    <td colspan="2">
                                        Personal Note (Footer):</td>
                                </tr>
                                <tr style="visibility:hidden;">
                                    <td colspan="2">
                                        <%--<textarea runat="server" style="width: 100%" id="txaFooter" onkeyup="checkMaxLengthForPersonalNote(this);"
                                                    onblur="checkMaxLengthForPersonalNote(this);" class="Textarea_Large"></textarea>--%>
                                        <asp:TextBox ID="txaFooter" runat="server" TextMode="MultiLine" MaxLength="1000"
                                            Style="width: 100%" onkeyup="checkMaxLengthForPersonalNote(this);" onblur="checkMaxLengthForPersonalNote(this);" Visible="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                <br>
            </td>
        </tr>
        <tr>
            <td align="right" nowrap="nowrap">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                <asp:Button ID="btnView" runat="server" Text="View" CssClass="Button_Standard Button_Report" />
                <asp:Button ID="btnCancel" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <asp:Button ID="btnBack" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
                <br>
            </td>
        </tr>
        <tr>
            <td>
                <b>Confirmation Text</b>
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>
                            <asp:UpdatePanel ID="updGrid" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gwMain" runat="server" AutoGenerateColumns="False" CssClass="dataTable"
                                        Width="100%" CellPadding="4">
                                        <RowStyle CssClass="evenRow" />
                                        <AlternatingRowStyle CssClass="oddRow" />
                                        <Columns>
                                            <asp:BoundField DataField="NoteId" HeaderText="NoteId" />
                                            <asp:TemplateField HeaderText="Check" HeaderStyle-Wrap="false">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkChecked" runat="server" Checked='<%# Eval("Selec") %>' />
                                                </ItemTemplate>
                                                <HeaderTemplate>
                                                    Check<br />
                                                    <asp:CheckBox ID="chkCheckAll" runat="server" />
                                                </HeaderTemplate>
                                                <HeaderStyle HorizontalAlign="Left" BorderStyle="none" CssClass="trHeader" VerticalAlign="top" />
                                                <ItemStyle HorizontalAlign="Left" BorderStyle="none" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NoteType" HeaderText="Type" >
                                                <HeaderStyle HorizontalAlign="Left" BorderStyle="none" CssClass="trHeader" VerticalAlign="top" />
                                                <ItemStyle HorizontalAlign="Left" BorderStyle="none" />
                                            </asp:BoundField>
                                            
                                            <asp:TemplateField HeaderText="Brand">
                                                <ItemTemplate>
                                                    <asp:Label ID="labelBrand" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" BorderStyle="none" CssClass="trHeader" VerticalAlign="top" />
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="NoteText" HeaderText="Text">
                                                <HeaderStyle HorizontalAlign="Left" BorderStyle="none" CssClass="trHeader" VerticalAlign="top" />
                                                <ItemStyle HorizontalAlign="Left" BorderStyle="none" VerticalAlign="top"/>
                                            </asp:BoundField>

                                            <asp:BoundField DataField="IsHeader" HeaderText="Header/Footer">
                                                <HeaderStyle HorizontalAlign="Center" BorderStyle="none" CssClass="trHeader" VerticalAlign="top" />
                                                <ItemStyle HorizontalAlign="Center" BorderStyle="none" />
                                            </asp:BoundField>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
                <br />
            </td>
        </tr>
        <tr>
            <td align="right" nowrap="nowrap">
                <asp:Button ID="btnSave2" runat="server" Text="Save" CssClass="Button_Standard Button_Save" />
                <asp:Button ID="btnView2" runat="server" Text="View" CssClass="Button_Standard Button_Report" />
                <asp:Button ID="btnCancel2" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" />
                <asp:Button ID="btnBack2" runat="server" Text="Back" CssClass="Button_Standard Button_Back" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hiddenFlagForPostBack" runat="server" />
    <%--rev:mia Sept 2 2013 -    this will allow confirmation email to be  process by adding multiple checkboxes--%>
    <asp:HiddenField runat="server" ID="hidRentalList" />
</asp:Content>
