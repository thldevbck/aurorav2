<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="SelectedVehicleList.aspx.vb" Inherits="Booking_SeletedVehicleList"
    Title="Untitled Page" %>

<%@ PreviousPageType VirtualPath="~/Booking/VehicleRequestResultList.aspx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

<asp:UpdatePanel ID="UpdatePanelForSelectedVehicle" runat="server">
<contenttemplate>
        
    <asp:Panel ID="pnlTitle" runat="server" Width="100%">
        <table width="100%">
            <tr>
                <td width="50%">
                    <b>
                        <asp:Label ID="lblselectedmsg" runat="server"></asp:Label>
                    </b>
                </td>
                <td width="50%" align="right">
                    <b>
                        <asp:CheckBox ID="chkconvoy" runat="server" AutoPostBack="true" TextAlign="Left"
                            Text="Is Convoy"></asp:CheckBox>
                    </b>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <table width="100%">
        <tbody>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" CssClass="dataTableGrid" Width="100%"
                        AutoGenerateColumns="False" ShowFooter="True" DataKeyNames="SlvId">
                        <RowStyle CssClass="evenRow"></RowStyle>
                        <HeaderStyle HorizontalAlign="Center" VerticalAlign="Bottom"></HeaderStyle>
                        <Columns>
                            <asp:TemplateField HeaderText="Check-Out">
                                <ItemTemplate>
                                    <%# eval("CoDw") %>
                                    <br />
                                    <b>
                                        <%# eval("CoDate") %>
                                    </b>
                                    <br />
                                    <%# eval("CoLoc") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Check-In">
                                <ItemTemplate>
                                    <%# eval("CiDw") %>
                                    <br />
                                    <b>
                                        <%# eval("CiDate") %>
                                    </b>
                                    <br />
                                    <%# eval("CiLoc") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Hire Period / Package">
                                <ItemTemplate>
                                    <%# eval("Hper") %>
                                    <b>/ </b>
                                    <%#Eval("Hpkg")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Selected">
                                <ItemTemplate>
                                    <%#Eval("Veh")%>
                                    <b>/ </b>
                                    <%#Eval("VehSel")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Bypass" FooterText="Check Total: ">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtByPassUnitNo" runat="server" Text='<%# eval("VehicleRanges") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Avail">
                                <ItemTemplate>
                                    <%#Eval("Avail")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Agent Ref &lt;br/&gt; Request Source">
                                <ItemTemplate>
                                    <%#Eval("AgnRef")%>
                                    <br />
                                    <asp:DropDownList ID="ddlRS" runat="server" AutoPostBack="true" Width="100%" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cost">
                                <FooterTemplate>
                                    <b>
                                        <asp:Label ID="lblfootercost" runat="server" Text='<%# eval("TotalCheckItems") %>'
                                            Width="30" /></b>
                                </FooterTemplate>
                                <ItemTemplate>
                                    <%#Eval("Cost")%>
                                </ItemTemplate>
                                <FooterStyle HorizontalAlign="left" Width="30px"></FooterStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Curr">
                                <ItemTemplate>
                                    <%#Eval("Curr")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <%#Eval("Status")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Check">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkCheck" AutoPostBack="true" OnCheckedChanged="CheckBox1_CheckedChanged" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="center" VerticalAlign="Middle" Width="70"></ItemStyle>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Part of &lt;br/&gt;Convoy" Visible="False">
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkPOC" Checked='<%# Eval("Poc") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <AlternatingRowStyle CssClass="oddRow"></AlternatingRowStyle>
                    </asp:GridView>
                </td>
            </tr>
        </tbody>
    </table>
    <br />
    <br />
    <asp:Panel ID="pnlButtonsconfirmation" runat="server" Width="100%">
        <table width="100%" border="0">
            <tbody>
                <tr width="100%">
                    <td align="right">
                        <asp:Button ID="BtnInquiry" runat="server" CssClass="Button_Standard" Width="110px"
                            Text="Inquiry (IN)"></asp:Button>
                        <asp:Button ID="BtnQuote" runat="server" CssClass="Button_Standard" Width="110px"
                            Text="Quote (QN)"></asp:Button>
                        <asp:Button ID="BtnKnockBack" runat="server" CssClass="Button_Standard" Width="110px"
                            Text="Knock-Back (KB)"></asp:Button>
                        <asp:Button ID="BtnWaitlist" runat="server" CssClass="Button_Standard" Width="110px"
                            Text="Waitlist (WL) "></asp:Button>
                        <asp:Button ID="BtnProvisional" runat="server" CssClass="Button_Standard" Width="110px"
                            Text="Provisional (NN)"></asp:Button>
                        <asp:Button ID="BtnConfirm" runat="server" CssClass="Button_Standard" Width="110px"
                            Text="Confirm (KK)"></asp:Button>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <br />
    <br />
    <asp:Panel ID="pnlNewBookinginformation" runat="server" Width="100%">
        <table width="100%" border="0">
            <tbody>
                <tr id="tr" runat="server" width="100%">
                    <td width="17%">
                        New Booking Number:
                    </td>
                    <td width="83%">
                        <asp:TextBox ID="txtNewBookingNumber" runat="server" Width="165"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Knock-Back:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlKnockBack" runat="server" Width="165px" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
            </tbody>
        </table>
    </asp:Panel>
    <br />
    <br />
    <asp:Panel ID="pnlSaving" runat="server" Width="100%">
        <table width="100%" border="0">
            <tr width="100%">
                <td align="right">
                    <asp:Button ID="BtnSave" runat="server" CssClass="Button_Standard" Text="Save"></asp:Button>
                    <asp:Button ID="BtnBack" runat="server" CssClass="Button_Standard" Text="Back"></asp:Button>
                    <asp:Button ID="BtnSaveNext" runat="server" CssClass="Button_Standard" Text="Save/Next">
                    </asp:Button>
                    <asp:Button ID="btnModifyBookingRequest" OnClick="btnModifyBookingRequest_Click"
                        runat="server" CssClass="Button_Standard" Width="150px" Text="Modify Booking Request">
                    </asp:Button>
                    <asp:Button ID="btnNewModifyBookingRequest" runat="server" CssClass="Button_Standard"
                        Width="150px" Text="New Booking Request"></asp:Button>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <ajaxToolkit:ConfirmButtonExtender ID="confirm_Inquiry" runat="server" TargetControlID="BtnInquiry">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="confirm_newbookingrequest" runat="server"
        TargetControlID="btnNewModifyBookingRequest" ConfirmText="Are you sure you want to start a new Booking Request?.">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="Confirm_Qoute" runat="server" TargetControlID="BtnQuote">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="Confirm_Knockback" runat="server" TargetControlID="BtnKnockBack">
    </ajaxToolkit:ConfirmButtonExtender>
    <ajaxToolkit:ConfirmButtonExtender ID="Confirm_Waitlisted" runat="server" TargetControlID="BtnWaitlist">
    </ajaxToolkit:ConfirmButtonExtender>
    
</contenttemplate>
</asp:UpdatePanel>
    <asp:HiddenField ID="HidBKRID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidBOOID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="Hidbuttonselection" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidBPDID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="HidVHRID" runat="server" EnableViewState="true" />
    <asp:HiddenField ID="hidContactID" runat="server" EnableViewState="true" />
</asp:Content>
