<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="PaymentMGT.aspx.vb" Inherits="Booking_PaymentMGT" Title="Untitled Page"  EnableEventValidation="false"%>

<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc1" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc2" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc3" %>
<%@ Register Src="../UserControls/ConfirmationBoxExtended/ConfirmationBoxControlExtended.ascx"
    TagName="ConfirmationBoxControlExtended" TagPrefix="uc4" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc5" %>
<%@ Register Src="../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc6" %>
<%@ Register Src="../UserControls/SupervisorOverrideControl/SupervisorOverrideControl.ascx"
    TagName="SupervisorOverrideControl" TagPrefix="uc10" %>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="ConfirmationBoxControl"
    TagPrefix="uc7" %>
<%@ Register Src="../UserControls/ConfirmationBoxExtended/ConfirmationBoxControlExtended.ascx"
    TagName="ConfirmationBoxControlEFTPOS" TagPrefix="uc8" %>

<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="cbBillingTokenWithNovalue"
    TagPrefix="uc9" %>

<%--rev:mia dec 2 2010--%>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="cbDPSPMTnoRoleWithDPZpayment"
    TagPrefix="uc11" %>


<%--
        rev:mia Aug 7 2012
        note: All payments/refund made against billing token regardless of any countries 
             will display a new popup message.
--%>
<%@ Register Src="../UserControls/ConfirmationBox/ConfirmationBoxControl.ascx" TagName="cbPaymentAgainstBillingToken" TagPrefix="uc12" %>

<%@ Register src="../Booking/Controls/BookingDetailsUserControl.ascx" tagname="BookingDetailsUserControl" tagprefix="uc13" %>

<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
  <object id="dpsEftX" classid="CLSID:92EDD80C-B50B-4B85-9AE9-9B3E6C63DED9" width="0"
        height="0">
    </object>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">
    <%--Feb 12 2010 - DPS eftpos integration - manny--%>
    
<%--
    <script src="Jquery/jquery-1.3.2.js" type="text/javascript"></script>
    <script src="Jquery/jquery.blockUI.js" type="text/javascript"></script>
--%>
    <script src="EftposJS/EFTPOSintegration.js" type="text/javascript"></script>
    <script src="EftposJS/jQuery.DynamicLoader.js" type="text/javascript"></script>

    <%--Feb 12 2010 - DPS eftpos integration - manny rocks--%>

    <script for="dpsEftX" event="AuthorizeEvent()">
           DisplayResult();
    </script>


    <script for="dpsEftX" event="CardReadEvent()">
          var typeofpayment    = $("[id$=_HiddenPurchase]").val()
          //var HiddenType        = $("[id$=_HiddenType]").val()
          
          
          if(typeofpayment == 'Refund' && !isNegativePayment)
          { 
            CardReadEventForRefund()
          }
          else
          {
            CardReadEventForRefundForNegative()
          }//if(typeofpayment == 'Refund' && !isNegativePayment)
          
    </script>

    <script for="dpsEftX" event="EnterDataEvent()">
		//alert('enterdataevent');
    </script>

    <script for="dpsEftX" event="OutputReceiptEvent()">
//	      PrintReceipt(dpsEftX.Receipt,$("[id$=HiddenUsercode]").val())      
          //alert(dpsEftX.EnablePrintReceipt + ' ' + dpsEftX.PrinterName )          
          PrintReceipt(dpsEftX.Receipt,'<%= Me.UserCode %>')                     
    </script>

    <script for="dpsEftX" event="OnlineEvent()">
          //alert(dpsEftX.EnablePrintReceipt + ' ' + dpsEftX.PrinterName )  
		  StatusChecking()
    </script>

    <script for="dpsEftX" event="StatusChangedEvent()">
		  StatusChecking();
		  NotReady();
    </script>

    <script for="dpsEftX" event="GetLastReceiptEvent()">
        GetLastReceipt()
    </script>

    <%---------------------------------------%>

    <script language="javascript" type="text/javascript">
                var returnvalue;
                var isvalid=false;

                function CvvInfo() {
                    window.open('http://www.paymentexpress.com/securitycode.htm', '', 'width=800,height=600');

                }


                function onCancel()
                {
                    var divFlagForBackAndCancel = document.getElementById('ctl00_ContentPlaceHolder_lblNoEntries')
                    if(divFlagForBackAndCancel.innerHTML != "(.....No recorded payment)") 
                        {
                            var bOk = window.confirm ("Selection of this command will remove the record completely.\nDo you wish to continue?") ; 
	                        return bOk;
	                        
                        }
                     return true        
                }
                
                function onGoback()
                {
                    var divFlagForBackAndCancel = document.getElementById('ctl00_ContentPlaceHolder_lblNoEntries')//document.getElementById('ctl00_ContentPlaceHolder_divFlagForBackAndCancel')
                    if(divFlagForBackAndCancel.innerHTML != "(.....No recorded payment)") 
                        {
                            var bOk = window.confirm ("Payment details have not been saved.\nSelect OK to exit without saving.\nSelect Cancel to continue payment process.?") ; 
	                        return bOk;
	                        
                        }
                     return true        
                }
                
                function doreverse(bDPSRole){
                if(bDPSRole=='True'){
                     returnvalue=window.showModalDialog('PaymentMGTConfirmation.aspx', 'Payment Reversal','status:no;dialogWidth:300px;dialogHeight:150px;dialogHide:true;help:no;scroll:no');
                    } 
                }
                
                function doreverseWithoutRole(roleboolean){
                    if(roleboolean == 'False'){
                            var bOk = window.confirm ('Are you sure you wish to reverse this payment?') 
                            return bOk
                    }
                    return true;
                }
                
               function doreverseWithRoleButNotTrue(roleboolean){
                    if(roleboolean == 'False'){
                            var bOk = window.confirm ('Are you sure you wish to reverse this payment?') 
                            return bOk
                    }
                    return true;
                }       
                          
                function TransferBondToOtherBooRnt(sExternalRef){
                        if(isvalid==false) return false
                        var bOk = window.confirm ('Are you sure you wish to Transfer this payment?') 
                        return bOk
	              }
                 
                function IsValidBookingRef (text) 
                        {  
                            var Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                            var Digits = "0123456789";

                            text = '' + text;
                            
                            if (text.length == 0) return false;
                            
                            var firstLetter = text.substring(0,1);
                            var isLetter = Alphabet.indexOf(firstLetter) != -1;
                            
                            var index = isLetter ? 9 : 7;
                            var length = isLetter ? 10 : 9;
                            
                           // if (text.length != length) return false;
                            if (text.indexOf("/") == -1) return false;
                            
                           // var strSlash = text.split("/");
                           // if (strSlash.length > 2) return false;

                            var indexChar = text.indexOf("/");
                            var indexAfter = text.substring(indexChar +1);
                            //return (indexChar == index) && (indexAfter != '') && (Digits.indexOf(indexAfter) != -1);
                            return true
                        }

                        function ClientSideValidation(sender, args)
                        {
                            args.IsValid = IsValidBookingRef (args.Value);
                            isvalid = args.IsValid
                            var btn = document.getElementById('ctl00_ContentPlaceHolder_btnTransferBond')
                        }               

                     
                     function CurrencyFormatted(amount)
                            {
	                            var i = parseFloat(amount);
	                            if(isNaN(i)) { i = 0.00; }
	                            var minus = '';
	                            if(i < 0) { minus = '-'; }
	                            i = Math.abs(i);
	                            i = parseInt((i + .005) * 100);
	                            i = i / 100;
	                            s = new String(i);
	                            if(s.indexOf('.') < 0) { s += '.00'; }
	                            if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	                            s = minus + s;
	                            return s;
                            }
                            
                            
                     function Cashdisplay()
                        {////debugger
                            var cash1 = document.getElementById('ctl00_ContentPlaceHolder_txt_Cash_amount1').value
                            var cash2 = document.getElementById('ctl00_ContentPlaceHolder_txt_Cash_amount2').value
                            var cash3 = document.getElementById('ctl00_ContentPlaceHolder_txt_Cash_amount3').value
                            
                            var txtcash1 = document.getElementById('ctl00_ContentPlaceHolder_txt_Cash_amount1')
                            var txtcash2 = document.getElementById('ctl00_ContentPlaceHolder_txt_Cash_amount2')
                            var txtcash3 = document.getElementById('ctl00_ContentPlaceHolder_txt_Cash_amount3')
                            
                            var lblcash1 = document.getElementById('ctl00_ContentPlaceHolder_lbl_Cash_amount1')
                            var lblcash2 = document.getElementById('ctl00_ContentPlaceHolder_lbl_Cash_amount2')
                            var lblcash3 = document.getElementById('ctl00_ContentPlaceHolder_lbl_Cash_amount3')
                            
                            if (cash1 == '' || cash1==null) 
                                {
                                  //  cash1=0
                                    lblcash1.innerHTML = "0.00"
                                
                                }else{
                                    lblcash1.innerHTML = CurrencyFormatted(cash1);
                                     txtcash1.value = CurrencyFormatted(cash1);
                                }
                            if (cash2 == '' || cash2==null) 
                                {
                                   // cash2=0
                                    lblcash2.innerHTML = "0.00"
                                
                                }else{
                                    lblcash2.innerHTML = CurrencyFormatted(cash2);
                                     txtcash2.value = CurrencyFormatted(cash2);
                                }   
                                 
                             if (cash3 == '' || cash3==null) 
                                 {
                                    //cash3=0 
                                    lblcash3.innerHTML = "0.00"              
                                    
                                 }else{
                                    lblcash3.innerHTML = CurrencyFormatted(cash3);
                                     txtcash3.value = CurrencyFormatted(cash3);
                                }   
                        }
                        
                        function RemoveCash()
                        {
                          var bOk = window.confirm ("Selection of this command will remove the record completely.\nDo you wish to continue?") ; 
	                      return bOk;
                        }
                        
                        function ConvertToDecimal(sender)
                        {
                            sender.value = CurrencyFormatted(sender.value);
                            
                        }
                        
                        function MsgRemoveRecord () 
                            { 
	                            var bOk = window.confirm ("Selection of this command will remove the record completely.\nDo you wish to continue?") ; 
	                            return bOk;
                            }
                            
                        var pbControl = null;
                        var prm = Sys.WebForms.PageRequestManager.getInstance();

                        prm.add_beginRequest(BeginRequestHandler);
                        prm.add_endRequest(EndRequestHandler);
                        
                        //Feb 12 2010 - DPS eftpos integration - manny rocks
                        Sys.Application.add_load(loadhandler);
                        function loadhandler() {


                            dpsEftX.EnableEOV    = '<%= ConfigurationManager.AppSettings("enableEOV").ToString %>'
                            dpsEftX.EmvVersionId = '<%= ConfigurationManager.AppSettings("emvVersionId").ToString %>'

                            
                            dpsEftX.ReceiptHeaderForPayment = '<%= ConfigurationManager.AppSettings("ReceiptHeaderForPayment").ToString %>'
                            dpsEftX.ReceiptHeaderForRefund = '<%= ConfigurationManager.AppSettings("ReceiptHeaderForRefund").ToString %>'
                            dpsEftX.PrinterName = $('[id$=_HiddenPrinterName]').val()  //  '<%= HttpUtility.UrlDecode(GetPrinterName) %>'
                            dpsEftX.EnablePrintReceipt      = '<%= ConfigurationManager.AppSettings("EnablePrintReceipt").ToString %>'
                            dpsEftX.ReceiptWidth            = '<%= ConfigurationManager.AppSettings("ReceiptWidth").ToString %>'
                            dpsEftX.ReceiptIsSeparate       = '<%= ConfigurationManager.AppSettings("ReceiptIsSeparate").ToString %>'
                            DisplayCardMismatch             = '<%= ConfigurationManager.AppSettings("DisplayCardMismatch").ToString %>'
                            basecard                        = $('[id$=_hiddenCardNumber]').val()   
                            cardVerificationFinished        = true;
                            isNegativePaymentRefunding      = false;
                             
                            $("[id$=_panelEFTPOS]").css("border","3px solid gray")
                            
                               
                            var typeofpayment = document.getElementById('ctl00_ContentPlaceHolder_HiddenPurchase');
                            
                            //|this first block of logic is applicable for the normal refund where amount is always positive
                            //|in some case, user want to refund a negative payment and it should be treated as a payment
                            if(typeofpayment.value == "Refund" && $("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val().indexOf("-") == -1)
                            {
                                 //alert('Normal refund')
                                 $("#VerifyCard").show()
                                 $("#PayButton").hide()
                                 $("#divVerification").hide()
                                 
                                 document.getElementById('creditcardSelect').disabled      = true;
                                 document.getElementById('nameText').disabled              = true;
                                 document.getElementById('ApprovalReferenceText').disabled = true;
                                 
                                 
                                 var paymentid = document.getElementById('ctl00_ContentPlaceHolder_HiddenPaymentID').value
                                 var url = "EFTPOSListener/EftposListener.aspx?mode=GetPayment"
                                 $.post(url, { GetPayment: escape(paymentid) }, function (result) {

                                     var info = result.split("@")
                                     var typeofpayment = $('[id$=_HiddenPurchase]').val()
                                     var HiddenType = $("[id$=_HiddenType]").val()

                                     $("#creditcardSelect").html("<option>" + info[0] + "</option>")
                                     $("#nameText").attr("value", info[1])
                                     $("#ApprovalReferenceText").attr("value", info[2])

                                     if (info[5] == 0) $("#AmountTextbox").attr("value", info[3])
                                     else $("#AmountTextbox").attr("value", info[5])

                                     $('[id$=_HiddenFieldAmount]').attr("value", $("#AmountTextbox").val())

                                     
                                     if (info[0].indexOf('Amex') != -1) {
                                         $("#SurchargeText").attr("value", "N/A")
                                         $("#TotalText").attr("value", $("#AmountTextbox").val())
                                     }
                                     else {
                                         if (info[0].indexOf('Amex') == -1 && HiddenType == 'R') {
                                             $("#SurchargeText").attr("value", "N/A")
                                             $("#TotalText").attr("value", $("#AmountTextbox").val())
                                         }
                                         else {
                                             
                                             $("#SurchargeText").attr("value", parseFloat(info[3] - info[5]).toFixed(2))
                                             $("#TotalText").attr("value", info[3])
                                         }
                                     }

                                 });
                                
                                 
                                 $("input:radio").attr("style","")
                                 $("input:radio:checked").attr("style","background-color:Gray")
                            }
                            else
                            {
                                
                                var AmountToBePaid = $("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val()
                                if(AmountToBePaid.indexOf("-") == 0 && typeofpayment.value == "Payment")
                                {
                                    //alert('Payment using a negative amount' )
                                    $("[id$=AmountTextbox]").attr("value",$("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val());      
                                    $("#VerifyCard").show()
                                    $("#PayButton").hide()
                                    $("#divVerification").hide()
                                }
                                else
                                {
                                    var Amount = $("#ctl00_ContentPlaceHolder_txtAmount").val()
                                    
                                    if(AmountToBePaid.indexOf("-") == -1 && typeofpayment.value == "Payment" && Amount > 0)
                                    {
                                        //alert('Payment using a positive amount' )
                                        $("div.modalPopup #creditcardSelect").focus();
                                        $("[id$=AmountTextbox]").attr("value",$("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val());      
                                        $("#VerifyCard").hide()
                                        $("#divVerification").hide()
                                    }
                                    else
                                    {
                                        
                                        if(AmountToBePaid.indexOf("-") == -1 && typeofpayment.value == "Payment" && Amount < 0)
                                        {
                                            //alert("Doing refund and with negative amount : " + Amount)
                                            isNegativePaymentRefunding = true
                                            $("#VerifyCard").hide()
                                            $("#PayButton").show()
                                            $("#divVerification").hide()
                                             
                                            document.getElementById('creditcardSelect').disabled      = true;
                                            document.getElementById('nameText').disabled              = true;
                                            document.getElementById('ApprovalReferenceText').disabled = true;
                                            document.getElementById('AmountTextbox').disabled         = true;
                                            RefundNegativePayment()
                                        }
                                    }
                                }//if($("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val().indexOf("-") == 0)
                                
                                
                            }//if(typeofpayment.value == "Refund")

                            if ($('[id$=_ddlMethod] option:selected').attr("text") == 'Debit Card') {

                             }
                            if ($('[id$=_ddlMethod] option:selected').attr("text") == 'Credit Card') {

                             }
                        }//function loadhandler
                        
                        
                        function BeginRequestHandler(sender, args) {
                            pbControl = args.get_postBackElement();  //the control causing the postback
                            if(pbControl.id.indexOf('Save') > -1 || pbControl.id.indexOf('Reverse') > -1 || pbControl.id.indexOf('leftButton') > -1 || pbControl.id.indexOf('centerButton') > -1 || pbControl.id.indexOf('rightButton') > -1) pbControl.disabled = true;
                            if(pbControl.id.indexOf('btnAddPayment') > -1 || pbControl.id.indexOf('btnSave') > -1)pbControl.disabled = true;
                            if(pbControl.id.indexOf('timerStatus') > -1)StatusChecking();
                            if(pbControl.id.indexOf('ctl00_ContentPlaceHolder_SupervisorOverrideControl1_okButton') > -1 )pbControl.disabled = true;
                            if (pbControl.id.indexOf('_ddlCardType') > -1) $('[id$=_btnEFTPOS]').attr("disabled", true)
                            isCardDefault = false

                            DisableButtonWhenItsImprint()
                        }

                        function EndRequestHandler(sender, args) {
                            
                           

                            if (pbControl.id.indexOf('_ddlCardType') > -1) {
                                var HiddenIsEFTPOSactive = $("[id$=_HiddenIsEFTPOSactive]").val()
                                var method = $("[id$=_ddlMethod] option:selected").attr("text")
                                var card = $("[id$=_ddlCardType] option:selected").attr("text")

                                if (method.indexOf("Credit") == 0) {
                                    if (card.length > 0 && $('[id=ctl00_ContentPlaceHolder_rdoCardPresent_0]').is(":checked") == true) {
                                        if (HiddenIsEFTPOSactive == "true") $('[id$=_btnEFTPOS]').attr("disabled", true)
                                        if (HiddenIsEFTPOSactive == "false") $('[id$=_btnEFTPOS]').attr("disabled", false)
                                    }
                                    else {

                                        if (card.length == 0 && $('[id=ctl00_ContentPlaceHolder_rdoCardPresent_0]').is(":checked") == true) {
                                            $('[id$=_btnEFTPOS]').attr("disabled", false)
                                        }
                                        else {
                                            if (card.length > 0 && $('[id=ctl00_ContentPlaceHolder_rdoCardPresent_0]').is(":visible") == false) {
                                                if (HiddenIsEFTPOSactive == "true") $('[id$=_btnEFTPOS]').attr("disabled", true)
                                                if (HiddenIsEFTPOSactive == "false") $('[id$=_btnEFTPOS]').attr("disabled", false)
                                            }
                                            else {
                                                $('[id$=_btnEFTPOS]').attr("disabled", true)
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                } else {
                                    if (method.indexOf("Debit") == 0) {
                                        if (card.length > 0) {
                                            if (HiddenIsEFTPOSactive == "true") $('[id$=_btnEFTPOS]').attr("disabled", true)
                                            if (HiddenIsEFTPOSactive == "false") $('[id$=_btnEFTPOS]').attr("disabled", false)
                                        }
                                        else {
                                            $('[id$=_btnEFTPOS]').attr("disabled", true)
                                        }
                                    }
                                }

                            }
                           if(pbControl.id.indexOf('ctl00_ContentPlaceHolder_SupervisorOverrideControl1_okButton') > -1 )
                           {
                                pbControl.disabled = false;
                                $('[id$=_ReasonOtherTextBox]').attr("disabled", false).focus();
                                
                           }
                           
                            if(pbControl.id.indexOf('Save') > -1 || pbControl.id.indexOf('Reverse') > -1 || pbControl.id.indexOf('leftButton') > -1 || pbControl.id.indexOf('centerButton') > -1 || pbControl.id.indexOf('rightButton') > -1 )
                            { 
                              $('[id$=_ReasonOtherTextBox]').attr("value", "")
                              
                              var other = $('[id$=_ddlReason] option:selected').attr("text")
                              if (other.indexOf('reason for this refund') != -1 || other.indexOf('reason for this payment') != -1) {
                                  $('[id$=_trReasonOther]').show()
                                  $('[id$=_okButton]').attr("disabled", true);
                              }
                              else {
                                  $('[id$=_trReasonOther]').hide()
                              }

                            }
                            
                            if(pbControl.id.indexOf('btnAddPayment') > -1 || pbControl.id.indexOf('btnSave') > -1)pbControl.disabled = false;
                            if(pbControl.id.indexOf('timerStatus') > -1)StatusChecking();
                           
                            //Feb 12 2010 - DPS eftpos integration - manny rocks
                            if(pbControl.id.indexOf('ddlMethod') > -1 )
                            {
                                var ddl     = document.getElementById(pbControl.id);
                                MethodValue = "<MethodValue>" + ddl.options[ddl.selectedIndex].value + "</MethodValue>" 
                                
                                //just incase user selected EFTPOS but use credit card.....wheeew
                                if (ddl.options[ddl.selectedIndex].text == "Debit Card") cardValueOfCreditCard = ddl.options[ddl.selectedIndex - 1].value
                                    
                                
                                if (ddl.options[ddl.selectedIndex].text == "Credit Card") cardValueOfCreditCard = ddl.options[ddl.selectedIndex + 1].value
                                                                    

                                if (ddl.options[ddl.selectedIndex].text == "Debit Card" || ddl.options[ddl.selectedIndex].text == "Credit Card" || ddl.options[ddl.selectedIndex].text == "Hold Number") {
                                }
                            }

                            //Auto populate card type from Maintain payment screen to Eftpos transaction screen
                            if (pbControl.id.indexOf('ddlCardType') > -1) {
                                isCardDefault = true
                                DisableButtonWhenItsImprint()

                            }
                            
                            //issue #113
                            var initbtnEftposState = $('[id$=_btnEFTPOS]').attr("disabled")
                            var initbtnRefundState = $('[id$=_btnReverse]').attr("disabled")
                            $('[id$=_HiddenFieldInitialStateOfEftposAndRefundIsEnabled]').attr("value", false)
                            if(initbtnEftposState == false && initbtnRefundState == false) $('[id$=_HiddenFieldInitialStateOfEftposAndRefundIsEnabled]').attr("value",true)

                            pbControl = null;

                             
                            //Feb 12 2010 - DPS eftpos integration - manny rocks
                            //------------------------------------------------------
                            //amount validation
                            //------------------------------------------------------
                            $('[id$=_btnEFTPOS]').click(function () {
                                //check the availability of the eftpos pinpad
                                if (!dpsEftX.ReadyPinPad) {
                                    DisplayGrowl("PinPad is not ready.", 0)
                                    return
                                }

                                if (!dpsEftX.ReadyLink) {
                                    DisplayGrowl("DPS link is not ready.", 0)
                                    return
                                }

                                if (!dpsEftX.Ready) {
                                    DisplayGrowl("DPS connectivity is not ready.", 0)
                                    return
                                }

                                if (overallstatus != true) {
                                    $("#VerifyCard").attr("disabled", true);
                                    $("#PayButton").attr("disabled", true);
                                    return
                                }
                                //start with a clean message
                                DisplayGrowl("", 0)
                                var amounttemp = $("#AmountTextbox").val();
                                
                                if (parseFloat(amounttemp) < 0 && amounttemp.indexOf('-') != -1 && !isNegativePaymentRefunding) {
                                    $("#VerifyCard").show()
                                    $("#PayButton").hide()
                                    $("#divVerification").hide()
                                    $("#VerifyCard").attr("disabled", false);
                                }
                                else {
                                    $("div.modalPopup #PayButton").attr('disabled', false);
                                } // if(parseFloat(amounttemp) < 0 || amounttemp.indexOf('-') != -1)

                                $("#divResult").html("");


                                if (cardValueOfCreditCard.length != 0) SetCreditCardWhenEFTPOS(cardValueOfCreditCard)
                                if (isNegativePaymentRefunding) {
                                    RefundNegativePayment()
                                    if ($("[id$=_ddlCardType] option:selected").attr("text").length != 0) $("#creditcardSelect").html("<option>" + $("[id$=_ddlCardType] option:selected").attr("text") + "</option>")
                                }


                                //-------------------------------------------
                                //Auto populate card type from Maintain payment screen to Eftpos transaction screen
                                $("[id$=nameText]").attr("value", $("[id$=_TxtName]").attr("value"))
                                $("[id$=ApprovalReferenceText]").attr("value", $("[id$=_txtApprovalReference]").attr("value"))
                                var typeofpayment = document.getElementById('ctl00_ContentPlaceHolder_HiddenPurchase');
                                var HiddenType = $("[id$=_HiddenType]").val()
                                if (typeofpayment.value == 'Refund' && HiddenType != 'B' ) {
                                    $("[id$=SurchargeText]").attr("value", "N/A")
                                    $("[id$=TotalText]").attr("value", "N/A")
                                }
                                else {
                                    Surcharge($("[id$=_ddlCardType]").attr("value"))
                                }
                                
                                $("[id$=creditcardSelect]").focus()

                                //-------------------------------------------
                            });   //$('[id$=_btnEFTPOS]').click(function()


                            $('[id$=_rightButton]').click(function () {
                                debugger;
                                $('[id=ctl00_ContentPlaceHolder_btnReverse]').attr('disabled', '').attr('class', 'Button_Standard')
                                if ($('[id$=_HiddenLocation]').val() == "NZ") {
                                    $('[id=ctl00_ContentPlaceHolder_btnEFTPOS]').attr('disabled', '').attr('class', 'Button_Standard')
                                }

                                DisableButtonWhenItsImprint()
                                if (this.id = 'ctl00_ContentPlaceHolder_ConfirmationBoxControl_DoReverseWithoutRole') {
                                    var disabled = $('[id$=_btnEFTPOS]').attr("disabled")
                                    if (disabled == true) {
                                        $('[id$=_btnEFTPOS]').attr("disabled", true).attr('class', '').attr('class', 'Button_Standard submitdisabled')
                                    }
                                }

                                var disabled = $('[id$=_btnEFTPOS]').attr("disabled")
                                if (this.id = 'ctl00_ContentPlaceHolder_ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue') {
                                    if (disabled == true) {
                                        $('[id$=_btnEFTPOS]').attr("disabled", true).attr('class', '').attr('class', 'Button_Standard submitdisabled')
                                    }
                                }

                            });   //$('[id$=_rightButton]').click(function()
                            
                            $('[id$=_ConfirmationBoxControlEFTPOS_leftButton]').click(function()
                            {
                                $('[id$=_btnEFTPOS]').trigger('click')
                                
                            });//$('[id$=_rightButton]').click(function()

                            
                            $('[id$=_cbPaymentAgainstBillingToken_rightButton]').click(function () {
                                $('[id$=_btnReverse]').attr('disabled', 'disabled').attr('class', 'Button_Standard submitdisabled')
                                $('[id$=_btnEFTPOS]').attr('disabled', 'disabled').attr('class', 'Button_Standard submitdisabled')
                            }); //$('[id$=_cbPaymentAgainstBillingToken_rightButton]').click(function () {


                            $('[id$=SupervisorOverrideControl1_cancelButton]').click(function() {
                                 //rev: issue#111
                                 $('[id$=_ddlReason] option:first-child').attr('selected', 'selected');
                                 var typeofpayment    = $("[id$=_HiddenPurchase]").val()
                                 if(typeofpayment == 'Refund') 
                                 { 
                                     $('[id=ctl00_ContentPlaceHolder_btnReverse]').attr('disabled','').attr('class','Button_Standard') 
                                     if($('[id=ctl00_ContentPlaceHolder_rdoCardPresent_0]').val() == 1)  $('[id=ctl00_ContentPlaceHolder_btnEFTPOS]').attr('disabled','').attr('class','Button_Standard') 
                                 }

                             }); //$('[id$=SupervisorOverrideControl1_cancelButton]').click(function() {


                             $('[id$=_ddlReason]').change(function () {
                                 var other = $('[id$=_ddlReason] option:selected').attr("text")
                                //if($(this).attr("value") == '12')
                                if (jQuery.trim(other) == 'Other' || jQuery.trim(other).indexOf('reason for this payment') != -1 || jQuery.trim(other).indexOf('reason for this refund') != -1)
                                {
                                    $('[id$=_trReasonOther]').show()
                                    $('[id$=_ReasonOtherTextBox]').attr("disabled", false);
                                    $('[id$=_ReasonOtherTextBox]').focus();
                                    $('[id$=_okButton]').attr("disabled", true);
                                    
                                }    
                                else 
                                {
                                    $('[id$=_ReasonOtherTextBox]').attr("disabled", true);
                                    $('[id$=_trReasonOther]').hide()
                                    $('[id$=_ReasonOtherTextBox]').attr("value", "")
                                    $('[id$=_okButton]').attr("disabled", false);
                                }    
                            });
                            
                            $('[id$=_ReasonOtherTextBox]').keyup(function() {
                                    if(this.value.length == 0)$('[id$=_okButton]').attr("disabled", true);
                                    else $('[id$=_okButton]').attr("disabled", false);  
                            });
                            
                            $('[id$=_ReasonOtherTextBox]').blur(function() {
                                    if(this.value.length == 0)$('[id$=_okButton]').attr("disabled", true);
                                    else $('[id$=_okButton]').attr("disabled", false);  
                            });

                            $("[id$=AmountTextbox]").change(function () {
                                $("#divVerification").hide()
                                //start with a clean message
                                DisplayGrowl("", 0)


                                var amt = 0; ;

                                if (!isNaN(this.value) && (this.value.length != 0)) {
                                    if (this.value.indexOf('-') == 0) {
                                        var tempvalue = $("[id$=TotalText]").attr("value")
                                        $("[id$=TotalText]").attr("value", ("-" + tempvalue))
                                        tempvalue = $("[id$=SurchargeText]").attr("value");
                                        $("[id$=SurchargeText]").attr("value", ("-" + tempvalue));

                                        $("[id$=_TypeText]").attr("value", "Refund")
                                        $("#VerifyCard").show()
                                        $("#VerifyCard").attr("disabled", false);
                                        $("#PayButton").hide()
                                        $("#divVerification").hide()
                                    }
                                    else {
                                        var mode = $("[id$=_HiddenMode]").attr("value")
                                        if (mode == "0") {
                                            $("[id$=_TypeText]").attr("value", "Payment")
                                            $("#VerifyCard").hide()
                                            $("#PayButton").show()
                                            $("#divVerification").hide()
                                        }
                                    }

                                    $("#PayButton").attr("disabled", false);

                                } else {
                                    $("#divResult").html("<label>ERROR: Enter amount to pay or refund. Characters are not allowed except for " - ".</label>")
                                    $("#PayButton").attr("disabled", true);
                                    $("[id$=SurchargeText]").attr("value", "");
                                    $("[id$=TotalText]").attr("value", "")
                                    $("#VerifyCard").attr("disabled", true);
                                }
                            });
                           
                                $("[id$=AmountTextbox]").keyup(function () {
                                    //start with a clean message
                                    DisplayGrowl("", 0) 
                                   var amt = 0; ;

                                   if (!isNaN(this.value) && (this.value.length != 0)) {
                                       $("#PayButton").attr("disabled", false);
                                       $("#VerifyCard").attr("disabled", false);

                                   } else {
                                       $("#divResult").html("<label>ERROR: Please enter amount to pay or refund. Characters are not allowed.</label>")
                                       $("#PayButton").attr("disabled", true);
                                       $("#VerifyCard").attr("disabled", true);
                                       $("[id$=SurchargeText]").attr("value","");
                                       $("[id$=TotalText]").attr("value","")
                                   }
                               });

                               $("[id$=AmountTextbox]").blur(function () {

                                   $("#VerifyCard").attr("disabled", false);
                                   var HiddenType = $("[id$=_HiddenType]").val()
                                   //start with a clean message
                                   DisplayGrowl("", 0)

                                   var amt = 0; ;
                                   if (!isNaN(this.value) && (this.value.length != 0)) {
                                       if (this.value.indexOf('.') == -1) this.value = this.value + ".00"
                                       else if (this.value.indexOf('.') != -1) this.value = CurrencyFormatted(this.value);
                                       $("#PayButton").attr("disabled", false);
                                       $("#VerifyCard").attr("disabled", false);


                                       //check if value is not greater than the actual amount
                                       var origAmt = $("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val()
                                       if (origAmt == "0.00" || origAmt == 0.00) {
                                           origAmt = $("#ctl00_ContentPlaceHolder_txtAmount").val()
                                       }
                                       var mode = $("[id$=_HiddenMode]").attr("value")
                                       if (mode != "0") {
                                           if (parseFloat(origAmt) < parseFloat(this.value) && parseFloat(origAmt) > 0) {
                                               isVerifyclick = true
                                               $("#VerifyCard").attr("disabled", true);
                                               if (HiddenType == 'R') {
                                                   $("[id$=SurchargeText]").attr("value", "N/A");
                                                   $("[id$=TotalText]").attr("value", "N/A")
                                               }
                                               else {
                                                   $("[id$=SurchargeText]").attr("value", "");
                                                   $("[id$=TotalText]").attr("value", "")
                                               }
                                               //return
                                           }


                                           if (parseFloat(origAmt) > 0 && parseFloat(this.value) < 0) {
                                               DisplayGrowl("ERROR: Please create a new payment if you want to refund amount less than zero.", 0)
                                               $("#VerifyCard").attr("disabled", true);
                                               if (HiddenType == 'R') {
                                                   $("[id$=SurchargeText]").attr("value", "N/A");
                                                   $("[id$=TotalText]").attr("value", "N/A")
                                               }
                                               else {
                                                   $("[id$=SurchargeText]").attr("value", "");
                                                   $("[id$=TotalText]").attr("value", "")
                                               }
                                               return
                                           }

                                           if (parseFloat(origAmt) > 0 && parseFloat(this.value) == 0) {
                                               DisplayGrowl("ERROR: You can't refund a zero amount ", 0)
                                               $("#VerifyCard").attr("disabled", true);
                                               if (HiddenType == 'R') {
                                                   $("[id$=SurchargeText]").attr("value", "N/A");
                                                   $("[id$=TotalText]").attr("value", "N/A")
                                               }
                                               else {
                                                   $("[id$=SurchargeText]").attr("value", "");
                                                   $("[id$=TotalText]").attr("value", "")
                                               }
                                               return
                                           }

                                           $("#VerifyCard").show()
                                           $("#VerifyCard").attr("disabled", false);
                                           $("#PayButton").hide()
                                           $("#PayButton").attr("disabled", true);


                                       }
                                       var typeofpayment = $("[id$=_HiddenPurchase]").val()
                                       if (HiddenType == 'B' && typeofpayment == 'Refund') {
                                           if (CheckLength($("div.modalPopup #creditcardSelect").attr("value"), 36) == true) {
                                               Surcharge($("div.modalPopup #creditcardSelect").attr("value"))
                                           }
                                           else {
                                               Surcharge($("[id$=_ddlCardType]").attr("value"))
                                           } //if (CheckLength($("div.modalPopup #creditcardSelect").attr("value"), 36) == true) 

                                       } //if (HiddenType == 'B' && typeofpayment == 'Refund')
                                       else {
                                           if (typeofpayment != 'Refund') {
                                               if (CheckLength($("div.modalPopup #creditcardSelect").attr("value"), 36) == true) {
                                                   Surcharge($("div.modalPopup #creditcardSelect").attr("value"))
                                               }
                                               else {
                                                   Surcharge($("[id$=_ddlCardType]").attr("value"))
                                               } //if (CheckLength($("div.modalPopup #creditcardSelect").attr("value"), 36) == true) 

                                           } //if (typeofpayment != 'Refund') {
                                       }

                                       if (parseFloat(this.value) < 0) {
                                           RetrieveEftposReasons('AUTHORITY')
                                           $('[id$=trReasonOtherVerified]').show()
                                           $('[id$=ReasonOtherTextBoxVerified]').attr("disabled", false);
                                           $('[id$=ReasonOtherTextBoxVerified]').focus();
                                       }
                                       else {
                                           RetrieveEftposReasons('REFUND')
                                       }


                                   }
                                   else {
                                       $("#PayButton").attr("disabled", true);
                                       $("#VerifyCard").attr("disabled", true);
                                       $("[id$=SurchargeText]").attr("value", "");
                                       $("[id$=TotalText]").attr("value", "")
                                   }

                                   //rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
                                   if (!AllowedToDoRefund()) {
                                       $("#PayButton").attr("disabled", true);
                                       $("#VerifyCard").attr("disabled", true);
                                       $("[id$=SurchargeText]").attr("value", "");
                                       $("[id$=TotalText]").attr("value", "")
                                       return;
                                   }


                               });        
                            
                            
                            //Name will come from DPS, so lets disbabled event for this control
                            //|----------------------------------------------------------------
                               $('#nameText').blur(function () {
                                   //start with a clean message
                                   DisplayGrowl("", 0) 
                                   if ($(this).val().length == 0) $("#divResult").html("<label>ERROR: Please enter Name</label>")
                                   else {
                                       var text = $(this).val();
                                       $("#divResult").html("<label></label>")
                                       $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                                   }
                               });

                            $('#nameText').keyup(function() {
                                var text=$(this).val();
                                $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                            });  
                            //|----------------------------------------------------------------


                            $('#ApprovalReferenceText').blur(function () {
                                //start with a clean message
                                DisplayGrowl("", 0)
                                if ($(this).val().length == 0) $("#divResult").html("<label>ERROR: Please enter Approval Reference</label>")
                                else {
                                    var text = $(this).val();
                                    $("#divResult").html("<label></label>")
                                    $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                                }
                            });
                            
                             $('#ApprovalReferenceText').keyup(function() {
                                var text=$(this).val();
                                $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                            });
                            
                            $("[id$=_TxtName]").keyup(function () {
                                var text = $(this).val();
                                $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                            });

                            $("[id$=_txtApprovalReference]").keyup(function () {
                                var text = $(this).val();
                                $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                            });

                            $("[id$=_TxtName]").blur(function () {
                                var text = $(this).val();
                                $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                            });

                            $("[id$=_txtApprovalReference]").blur(function () {
                                var text = $(this).val();
                                $(this).val(text.replace(/[^a-zA-Z 0-9]+/g, ''));
                            });

                            //issue #113
                            $("[id$=_txtAmount]").change(function () {
                                var text = $(this).val();
                                var origTxtamount = $("[id$=_HiddenFieldTxtAmount]").val()
                                var InitialStateOfEftposAndRefundIsEnabled = $('[id$=_HiddenFieldInitialStateOfEftposAndRefundIsEnabled]').val()

                                clearShortMessage()

                                if (parseFloat(text) == parseFloat(origTxtamount)) {
                                    if (InitialStateOfEftposAndRefundIsEnabled == "true") {
                                        $('[id$=_btnEFTPOS]').attr("disabled", false)
                                        $('[id$=_btnReverse]').attr("disabled", false)
                                    }
                                    return
                                }

                                if (parseFloat(text) > 0 && parseFloat(text) <= parseFloat(origTxtamount)) {
                                    if (InitialStateOfEftposAndRefundIsEnabled == "true") {
                                        $('[id$=_btnEFTPOS]').attr("disabled", false)
                                        $('[id$=_btnReverse]').attr("disabled", false)
                                    }
                                    return
                                }

                                if (parseFloat(text) <= 0) {
                                    setShortMessage("Warning", amountIsLessZero)
                                    if (InitialStateOfEftposAndRefundIsEnabled == "true") {
                                        $('[id$=_btnEFTPOS]').attr("disabled", true)
                                        $('[id$=_btnReverse]').attr("disabled", true)
                                    }
                                    return
                                }
                                else {
                                    if (InitialStateOfEftposAndRefundIsEnabled == "true") {
                                        $('[id=ctl00_ContentPlaceHolder_btnReverse]').attr('class', 'Button_Standard')
                                        $('[id$=_btnEFTPOS]').attr('class', 'Button_Standard')
                                    }
                                } //if (parseFloat(text) <= 0) {

                                if (parseFloat(text) > parseFloat(origTxtamount)) {
                                    setShortMessage("Warning", amountIsMorethanOriginal)
                                    if (InitialStateOfEftposAndRefundIsEnabled == "true") {
                                        $('[id$=_btnEFTPOS]').attr("disabled", true)
                                        $('[id$=_btnReverse]').attr("disabled", true)
                                    }
                                    return
                                } else {
                                    if (InitialStateOfEftposAndRefundIsEnabled == "true") {
                                        $('[id=ctl00_ContentPlaceHolder_btnReverse]').attr('class', 'Button_Standard')
                                        $('[id$=_btnEFTPOS]').attr('class', 'Button_Standard')
                                    }
                                } //if (parseFloat(text) > parseFloat(origTxtamount)) {

                            });

                            //------------------------------------------------------
                            //Getting all card type
                            //------------------------------------------------------
                            var typeofpayment = $("[id$=_HiddenPurchase]").val();
                            var ddl     = document.getElementById("ctl00_ContentPlaceHolder_ddlMethod");
                            
                            if(typeofpayment == "Payment" || $("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val().indexOf("-") == 0)
                            {
                                var url = "EFTPOSListener/EftposListener.aspx?mode=AllCards"
                                $.post(url, { AllCards: escape(ddl.options[ddl.selectedIndex].value) }, function (result) {
                                    //rev:mia june 29 2010
                                    if (!isCardDefault)
                                        $("div.modalPopup #creditcardSelect").html("<option value=''></option>" + result);
                                    else {
                                        $("div.modalPopup #creditcardSelect").html(result);
                                        $("[id$=creditcardSelect]").attr("value", $("[id$=_ddlCardType]").attr("value"))
                                    }
                                });   
                            }
                            else
                            { 
                                if($("#ctl00_ContentPlaceHolder_txtAmountToBePaid").val().indexOf("-") == 0)
                                {
                                }
                                
                            }//if(typeofpayment == "Payment")
                            
                            
                            
                            
                            //------------------------------------------------------
                            //clicking button
                            //------------------------------------------------------
                            $("div.modalPopup #PayButton").click(function () {
                                //start with a clean message
                                DisplayGrowl("", 0)
                                $("[id$=AmountTextbox]").attr("disabled", true)
                                if (typeofpayment == "Refund") return;

                                //rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
                                //rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
                                if (!AllowedToDoRefund()) return;
                                                               

                                $("#CancelButton").attr("disabled", false);


                                if ($("div.modalPopup #creditcardSelect").val().length == 0) {
                                    $("#divResult").html("<label>ERROR: Please select credit card</label>")
                                    return;
                                }
                                if ($('#nameText').val().length == 0) {
                                    $("#divResult").html("<label>ERROR: Please enter Name</label>")
                                    return;
                                }
                                if ($('#ApprovalReferenceText').val().length == 0) {
                                    $("#divResult").html("<label>ERROR: Please enter Approval Reference</label>")
                                    return;
                                }
                                if ($('#TotalText').val().length == 0) {
                                    $("#divResult").html("<label>ERROR: Total Payment is empty.</label>")
                                    return;
                                }

                                $("#PayButton").attr("disabled", true);
                                $("#CancelButton").attr("disabled", true);

                                Pay();

                            });

                            $("#CancelButton").click(function () {

                                var origAmt = $('[id$=_HiddenFieldAmount]').val() 

                                $("[id$=AmountTextbox]").attr("value", PadWithZeroes(origAmt))

                                var typeofpayment    = $("[id$=_HiddenPurchase]").val()
                                $("[id$=AmountTextbox]").attr("disabled", false)
                                if(typeofpayment == 'Payment') {
                                    $("#PayButton").show()
                                    $("#VerifyCard").hide()
                                    $("#CancelButton").attr("disabled", false);
                                    $("[id$=_TypeText]").attr("value", "Payment")
                                }
                                else {
                                  $("#PayButton").hide()
                                  $("#VerifyCard").show().attr('disabled',false);
                                  $("#CancelButton").attr("disabled", false);
                                  $("[id$=_TypeText]").attr("value", "Refund")
                                }

                              //rev:issue 111
                                  ///added defaulting text to the dropdown
                                $('[id$=_ddlReasonVerified] option:first-child').attr('selected','selected');

                            });
                            //-----------------------------------------------
                            //card vertification
                            //-----------------------------------------------
                          $('#VerifyCard').click(function () {
                              DisplayGrowl("", 40)
                                $(this).attr('disabled',true);
                                $("#CancelButton").attr("disabled", true);
                                $('[id=logonTextBoxVerified]').attr("value","")
                                $('[id=passwordTextBox]').attr("value","")
                                $('[id$=ReasonOtherTextBoxVerified]').attr("value","")
                                
                                $('[id$=okButtonVerified]').attr("disabled", false)
                                cardVerificationFinished = false;
                                
                                //check if its a negative payment
                                var amounttemp = $("#AmountTextbox").val(); 
                                if( amounttemp.indexOf('-') == 0)isNegativePayment = true
                                else isNegativePayment = false
                                isVerifyclick = true;

                                //rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232
                                //rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
                                if (!AllowedToDoRefund()) return;

                                ReadCard()
                            });

                            $('#cancelButtonVerified').click(function () {
                                $("#VerifyCard").attr('disabled', false)
                                $("#CancelButton").attr("disabled", false);
                                $("#divVerification").hide();
                                $("#divVerification input:text,#divVerification input:password").attr("value", "")
                                $("#divResult").html("")
                                cardVerificationFinished = false;
                                if (typeofpayment == 'Refund') $("[id$=AmountTextbox]").attr("disabled", false)
                                //rev:issue 111
                                $('[id$=_ddlReasonVerified] option:first-child').attr('selected','selected');

                            });

                            $('[id$=_ddlReasonVerified]').change(function () {
                                var other = $("[id$=_HiddenReasonOther]").val()
                                
                                if ($(this).attr("value") == other)
                                {
                                    $('[id$=trReasonOtherVerified]').show()
                                    $('[id$=ReasonOtherTextBoxVerified]').attr("disabled", false);
                                    $('[id$=ReasonOtherTextBoxVerified]').focus();
                                   if($('[id$=ReasonOtherTextBoxVerified]').val().length == 0) $('[id$=okButtonVerified]').attr("disabled", true);
                                }    
                                else 
                                {
                                    $('[id$=ReasonOtherTextBoxVerified]').attr("disabled", true);
                                    $('[id$=trReasonOtherVerified]').hide()
                                    $('[id$=ReasonOtherTextBoxVerified]').attr("value", "")
                                    $('[id$=okButtonVerified]').attr("disabled", false);
                                }    
                            });

                            $('[id$=okButtonVerified]').click(function () {
                                $("#divResult").html("<label></label>")
                                if ($('[id$=_ddlReasonVerified]').val() == 'Please select reason') {
                                    $("#divResult").html("<label>ERROR: Please select reason from the dropdown list</label>")
                                    return;
                                }

                                ///rev:mia november 29 2010
                                ///added another strict validation to make dropdown a very mandatory field
                                var defaulttext = $('[id$=_ddlReasonVerified] option:selected').text()
                                if (defaulttext == 'Please select reason') {
                                    $("#divResult").html("<label>ERROR: Please select reason from the dropdown list</label>")
                                    return;
                                }

                                ///rev:mia november 29 2010
                                ///refactor the wrong logic here
                                var reasontext = defaulttext
                                if (reasontext == 'Other' || reasontext.indexOf('refund') != -1 || reasontext.indexOf('payment') != -1) {
                                    if ($('[id$=ReasonOtherTextBoxVerified]').val().length == 0) {
                                        $("#divResult").html("<label>ERROR: Other field is mandatory</label>")
                                        return;
                                    }
                                }
                                cardVerificationFinished = true;

                                var userinfo = $('[id=logonTextBoxVerified]').val() + '|' + $('[id=passwordTextBox]').val() + '|VIEWPMT'
                                overridePassword(userinfo)

                            });

                            $('[id$=_ReasonOtherTextBoxVerified]').keyup(function () {
                                var other = $("[id$=_HiddenReasonOther]").val()
                                if ($('[id$=_ddlReasonVerified]').val() != other) return;
                                    if(this.value.length == 0)$('[id=okButtonVerified]').attr("disabled", true);
                                    else $('[id$=okButtonVerified]').attr("disabled", false);  
                            });

                            $('[id$=_ReasonOtherTextBoxVerified]').blur(function () {
                                var other = $("[id$=_HiddenReasonOther]").val()
                                if ($('[id$=_ddlReasonVerified]').val() != other) return;
                                    if(this.value.length == 0)$('[id=okButtonVerified]').attr("disabled", true);
                                    else $('[id$=okButtonVerified]').attr("disabled", false);  
                            });
                            //-----------------------------------------------


                            $("div.modalPopup #creditcardSelect").change(function () {
                               //start with a clean message
                                DisplayGrowl("", 0) 
                                 
                                 Surcharge($(this).attr("value"))
                                 $("#PayButton").attr("disabled", false);
                                 $("#divResult").html("<label></label>")
                                  if($(this).val().length == 0) 
                                  {
                                      $("#divResult").html("<label>ERROR: Please select credit card</label>")
                                     $("#PayButton").attr("disabled", true);
                                     $("[id$=SurchargeText]").attr("value","");
                                     $("[id$=TotalText]").attr("value","")
                                  }
                            });
                            

                            $('#ctl00_ContentPlaceHolder_BillingTokenSelect').click(function () {
                                var ddl = document.getElementById(this.id)
                                if (ddl.options.length - 1 == 0) {
                                    $("[id$=_btnHiddenBillingToken]").trigger("click")
                                }
                            })
                             
                           //-----------------------------------------------------------
                            var imgok    = "../IMAGES/tick_green.gif"
                            var imgerror = "../IMAGES/rental_error.gif"
                            var imgCard  = "../IMAGES/creditcards.ico"
                            
                            if(overallstatus == true)
                            {
                                    
                                    $("#imgCredit").css("border","1px solid white")
                                    if(dpsEftX.Ready) 
                                    {   
                                        if(dpsEftX.ReadyPinPad) $("#imgpinpad").attr("src",imgok)
                                        else $("#imgpinpad").attr("src",imgerror)
                                        
                                        if(dpsEftX.ReadyLink) $("#imglink").attr("src",imgok)
                                        else $("#imglink").attr("src",imgerror)
                                        $("div.modalPopup #PayButton").attr("disabled", false);    
                                        
                                        
                                        if(dpsEftX.ReadyLink) $("#imgCredit").attr("src",imgCard)
                                        
                                    }
                                    else
                                    {
                                      $("#imgpinpad").attr("src",imgerror);
                                      $("#imglink").attr("src",imgerror);  
                                      $("#imgCredit").attr("src",imgerror)
                                      
                                      $("div.modalPopup #PayButton").attr("disabled", true);     
                                      
                                    }
                                    
                                    $("#tdstatustext").html("<b>" + dpsEftX.StatusText + "</b>")
                                    
                            }
                            else 
                            {
                                $("#imgCredit").css("border", "1px solid white")
                                    $("#imgpinpad").attr("src",imgerror);
                                    $("#imglink").attr("src",imgerror);
                                    $("#imgCredit").attr("src", imgCard)
                                    $("#tdstatustext").html("<b>" + dpsEftX.StatusText + "</b>")
                                    $("div.modalPopup #PayButton").attr("disabled", true);
                                }
                            }


                        
                        
                        function DisplayWarningMessage()
                        {
                            var lblWarning = document.getElementById('ctl00_ContentPlaceHolder_lblWarning')
                             lblWarning.innerHTML='Please don�t click on the Save Or Reverse Payment button multiple times as this can result in multiple payments'
                        }  
                        
                        function TravellerscheckCurrency(txtbox)
                        {
                                var tbox = document.getElementById(txtbox.id)
                                tbox.value = CurrencyFormatted(tbox.value)
                        }             
                        
                        function Refund()
                        {
                            var typeofpayment = $("[id$=_HiddenPurchase]").val();
                            if(typeofpayment == "Refund")
                            {
                                $("#PayButton").attr("disabled", true);
                                RefundPayment();
                            }    
                        }
                        
                        function SetCreditCardWhenEFTPOS(cardValue)
                        {                                                
                            var url = "EFTPOSListener/EftposListener.aspx?mode=AllCards"
                            $.post(url, {AllCards : escape(cardValue)},function(result) {
                                $("#CardTest").html("<option value=''></option>" + result);
                            });        
                            
                        }

                        function CheckLength(value, mylength) {
                            return(value.length == mylength)
                        }

                        function PadWithZeroes(value) 
                        {
                            if (value.indexOf(".") == -1) 
                            {
                                return (value + ".00")
                            }
                            else
                             {
                                var temp = value.split(".")
                                if (temp[1].length == 1) return (value + "0")
                                else return(value)
                            }
                        }

                        //rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals
                        function AllowedToDoRefund() {
                            //ignore the rest of the logic 
                            return true;
                            var currencyType                                = $("[id$=HiddenCurrency]").val()
                            if (currencyType.indexOf("|AUD") != -1) return true

                            var typeofpayment                              = $("[id$=TypeText]").val()
                            var allowableamountforrefund                   = ''
                            var allowableAmountToRefundForBondNZ           = $('[id$=_HiddenFieldAllowableAmountToRefundForBondNZ]').val()
                            var allowableAmountToRefundReceiptAndDepositNZ = $('[id$=_HiddenFieldAllowableAmountToRefundReceiptAndDepositNZ]').val()
                            var pTypesPayment                              = $('[id$=_HiddenFieldpTypes]').val()
                            var amount                                     = $("[id$=AmountTextbox]").val()
                            var isnegativepayment                          = (amount.indexOf('-') != -1? true: false)

                            if (pTypesPayment.indexOf('B') != -1)
                                allowableamountforrefund = allowableAmountToRefundForBondNZ
                            else
                                allowableamountforrefund = allowableAmountToRefundReceiptAndDepositNZ

                            if (((Math.abs(amount) > allowableamountforrefund) && isnegativepayment == true)) {
                                if (pTypesPayment.indexOf('B') != -1)
                                    $("#divResult").html("<label>ERROR: The refund amount allowed for Bond type is only up to $" + allowableamountforrefund + "</label>")
                                else
                                    $("#divResult").html("<label>ERROR: The refund amount allowed for Receipt or Deposit type is only up to $" + allowableamountforrefund + "</label>")

                                return false;    
                            }

                            if (typeofpayment == 'Refund') {
                                if (Math.abs(amount) > allowableamountforrefund) {
                                    if (pTypesPayment.indexOf('B') != -1)
                                        $("#divResult").html("<label>ERROR: The refund amount allowed for Bond type is only up to $" + allowableamountforrefund + "</label>")
                                    else
                                        $("#divResult").html("<label>ERROR: The refund amount allowed for Receipt or Deposit type is only up to $" + allowableamountforrefund + "</label>")

                                    return false;    
                                }
                            }

                            return true;
                        }
    </script>

    <asp:ValidationSummary ID="ValidationSummaryPayment" DisplayMode="BulletList" EnableClientScript="true"
        ShowSummary="true" runat="server" ForeColor="Black" Enabled="false" />
    <asp:UpdatePanel runat="server" id="UpdatePaymentMGT">
        <ContentTemplate>
        
          <%--Feb 12 2010 - DPS eftpos integration - manny--%>    
            <asp:Panel id="panelEFTPOS" runat="server" cssclass="modalPopup" style="display: none; width:400px;"  >
                           <h3>EFTPOS Transaction</h3>
                           <table>
                                    <tr>
                                        <td width="200">
                                            Credit Card:
                                        </td>
                                        <td>
                                            <select id="creditcardSelect" name="creditcardSelect" style="width: 160px; "/>
                                            
                                        </td>
                                        <td><select id="CardTest" name="CardTest" style="width: 50px; visibility:hidden"/>   </td>
                                    </tr>
                                    <tr>
                                            <td>Type:</td>
                                            <td> <input id="TypeText" class="" name="TypeText" style="width: 150px; " readonly="readonly" runat="server" /></td>
                                            <td></td>
                                    </tr>
                                    <tr>
                                         <td >Name:</td>
                                         <td valign="top">
                                            <input id="nameText" class="" name="nameText" style="width: 250px;"/>
                                            <i>'Name' will be use if you prefer <b>manual entry</b></i>
                                        </td>
                                        <td>
                                            *
                                        </td>
                                    </tr>
                                    <tr>
                                         <td >Approval Reference:</td>
                                         <td>
                                            <input id="ApprovalReferenceText" class="" name="ApprovalReferenceText" style="width: 250px;" />
                                        </td>
                                        <td>
                                            *
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Payment:</td>
                                        <td>
                                            <input id="AmountTextbox" class="" name="AmountTextbox" style="width: 100px;" />
                                             &nbsp;* 
                                          <%--   <img src="../Images/information.jpg"  id="imageInformation" style="visibility:visible; width:25px;height:25px; "   name="imageInformation" alt="" />--%>
                                        </td>
                                        
                                    </tr>
                                    
                                      <tr>
                                         <td> Plus Surcharge:</td>   
                                        <td><input id="SurchargeText" class="" name="SurchargeText" style="width: 100px;" readonly="readonly" /></td>                
                                    </tr>
                                    <tr>
                                        <td>
                                            Total:
                                        </td>
                                        <td><input id="TotalText" class="" name="TotalText" style="width: 100px;" readonly="readonly" /></td>                
                                        <asp:HiddenField id="hiddenCardNumber" runat="server" />
                                    </tr>
                           </table>
                            <br />
                           <table width="100%">
                                <tr align="right">
                                    <td>
                                       <input type="button" value='Verify Card'   id="VerifyCard" onclick="" class="Button_Standard" style="width: 92px;"  >
                                       <input  type="button" value="Process"      id="PayButton" style="width: 92px;"  class="Button_Standard Button_Save" onclick="Refund();"/>
                                       <%--<input  type="button" value="Last Receipt" id="LastReceiptButton"  class="Button_Standard"  style="width: 92px;"/>--%>
                                       <input  type="button" value="Cancel"       id="CancelButton"       class="Button_Standard Button_Reset"  style="width: 92px;"/>
                                       
                                    </td>
                                </tr>
                            </table>
                            
                            <br />
                            
                           <div id="divVerification">
                            
                                <table>
                                     <tr>
                                        <td style="width:200px;" colspan="2"><b>Override</b></td>
                                        <td></td>
                                     </tr>
                                     
                                     <tr>
                                            <td style="width:110px;">Logon:</label></td>
                                            <td><input type="text" id="logonTextBoxVerified" style="width:200px" />&nbsp;*</td>
                                        </tr>
                                        <tr>
                                            <td>Password:</td>
                                            <td><input type="password" id="passwordTextBox" style="width:200px" />&nbsp;*</td>
                                        </tr>
                                        <tr id="trReasonVerified">
                                            <td>Reason:</td>
                                            <td>
                                                <asp:DropDownList ID="ddlReasonVerified" runat="server"  Width="250px" >
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr id="trReasonOtherVerified" visible="false" >
                                            <td>Other:</td>
                                            <td>
                                                <asp:TextBox ID="ReasonOtherTextBoxVerified" runat="Server" Width="245px" TextMode="MultiLine"  Enabled="false" />&nbsp;*
                                            </td>
                                        </tr>
                                </table>
                                <br />
                                <table>
                                    <tr align="right">
                                        <td style="width:305px;">
                                            <input  type="button" value="OK/Process" id="okButtonVerified"  class="Button_Standard"  style="width: 92px;"/>
                                        </td>
                                        <td>
                                        <input  type="button" value="Cancel"       id="cancelButtonVerified"  class="Button_Standard Button_Reset"  style="width: 92px;"/>
                                            
                                        </td>
                                    </tr>
                                    
                                </table>
                           </div> 
                           
                           <br />
                           
                           <div id="divResult" > </div>
                             <table id="tablestatus">
                                 <tr>
                                    <td> Pinpad: </td>
                                    <td  style="width:50px">  <img src="" id="imgpinpad" alt=""/> </td>
                                    <td> Link: </td>
                                    <td  style="width:50px"> <img src="" id="imglink" alt=""/></td>
                                    <td> Status:</td>
                                    <td id="tdstatustext" style="width:150px"> </td>
                                    <%--<td><a href="#" onclick="return LastTransactionReceipt()"><img src="" id="imgCredit" alt="Print Last Receipt" style=" visibility:hidden;"  /></a> </td>--%>
                                    <td> <asp:Timer runat="server" id="timerStatus" interval = "500" ></asp:Timer> </td>
                                 </tr>
                           </table>
            </asp:Panel>
            
            <ajaxToolkit:ModalPopupExtender 
                                BehaviorID="programmaticModalPopupBehavior"
                                ID="ModalPopupExtender1" 
                                runat="server" 
                                TargetControlID ="btnEFTPOS"
                                PopupControlID="panelEFTPOS"
                                BackgroundCssClass="modalBackground"
                                CancelControlID="CancelButton"
                                OnCancelScript = "CancelOK()">
                                
            </ajaxToolkit:ModalPopupExtender>  

    <uc13:BookingDetailsUserControl ID="BookingDetailsUserControl1"  runat="server" />
    <asp:Panel ID="pnlAmountInformation" runat="server" Width="100%">
        <table>
            <tr>
                <td width="140" align="left">
                    Booking Number:
                </td>
                <td align="left">
                    <b>
                        <asp:Label ID="lblBookingNumber" runat="server" Visible="false"></asp:Label></b>
                    <asp:TextBox ReadOnly="true" runat="server" Text="" ID="txtInfoBookingNumber" />
                </td>
                 <td style="width:50px;"></td>
                
            
            </tr>
            <tr>
            <%--<td width="140" align="left" style ="display:none">
                    Billing Token:
                </td>
                <td colspan="2">
                  
                  <asp:dropdownlist ID = "BillingTokenSelect" style="width:360px;" runat="server" />
                </td>
            </tr>
            <tr>
                <td><br />
                </td>
            </tr>--%>
            <tr runat="server" id="trPaymentEntered" visible="false">
                <td width="140" align="left">
                    Payment Entered by:
                </td>
                <td>
                    <%--<b>
                        <asp:Label ID="lblPaymentEntered" runat="server" Visible="false"></asp:Label></b>
                        --%>
                        <asp:TextBox ID="txtPaymentEntered" ReadOnly="true" runat="server" width="350"></asp:TextBox>
                </td>
            </tr>
            <tr runat="server" id="trPaymentTransferred" visible="false">
                <td width="140" align="left">
                    Transferred to SS:
                </td>
                <td>
                 <%--   <b>
                        <asp:Label ID="lblPaymentTransferred" runat="server" /></b>--%>
                        <asp:TextBox ID="txtPaymentTransferred" ReadOnly="true" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                <br />
                </td>
            </tr>
            <tr id="tdTansferBond" runat="server" visible="false">
                <td align="left">
                    Booking Number:
                </td>
                <td>
                    <asp:TextBox ID="txtBookingNumber" runat="server" AutoPostBack="false" MaxLength="10"></asp:TextBox>
                    <asp:CustomValidator ID="CustomValidatorBookinRef" runat="server" Text="Booking Ref is not in correct format. i.e '3000140/1"
                        SetFocusOnError="true" Display="Dynamic" ControlToValidate="txtBookingNumber"
                        ErrorMessage="Booking Ref is not in correct format. i.e '3000140/1' format."
                        ClientValidationFunction="ClientSideValidation" ValidationGroup="Bond"></asp:CustomValidator>
                    <asp:RequiredFieldValidator ID="RequiredBookingRef" runat="server" Text="Please enter Booking Reference Number!"
                        SetFocusOnError="true" Display="Dynamic" ControlToValidate="txtBookingNumber"
                        ErrorMessage="Please enter Booking Reference Number!" ValidationGroup="Bond"></asp:RequiredFieldValidator>
                </td>
                <td align="left">
                </td>
            </tr>
            <tr id="tdTansferBondLabel" runat="server" visible="false">
                <td align="left">
                </td>
                <td>
                    <i>Transfer Bond to other booking/rental</i>
                </td>
            </tr>
            <tr id="tdTansferBondButton" runat="server" visible="false">
                <td align="left">
                </td>
                <td>
                    <asp:Button ID="btnTransferBond" runat="server" Text="Transfer Bond" Width="150" CssClass="Button_Standard"
                        ValidationGroup="Bond" />
                </td>
                
            </tr>
            <tr>
                <td width="140" align="left">
                    Amount to be Paid:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtAmountToBePaid" ReadOnly="true"  style="text-align: right"></asp:TextBox>
                    <asp:Label ID="lblAmountToBePaidCurrency" runat="server"></asp:Label>
                </td>
                <td align="left">
                </td>
                
            </tr>
            <tr>
                <td width="140" align="left">
                    Running Payment Total:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRunningPaymentTotal" ReadOnly="true" style="text-align: right"></asp:TextBox>
                    <asp:Label ID="lblRunningPaymentTotalCurrency" runat="server" autopostback="true"></asp:Label>
                </td>
                <td align="left">
                </td>
                
            </tr>
            <tr>
                <td width="140" align="left">
                    Balance/Change:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtRunningBalanceChange" ReadOnly="true" style="text-align: right"></asp:TextBox>
                    <asp:Label ID="lblRunningBalanceChangeCurrency" runat="server"></asp:Label>
                </td>
                <td align="left">
                </td>
                
            </tr>
            <tr>
                <td width="140" align="left">
                    Method:
                </td>
                <td>
                    <asp:DropDownList ID="ddlMethod" runat="server" AutoPostBack="true" Width="150px">
                    </asp:DropDownList>
                </td>
            
            </tr>
            <tr runat="server" id="trdBillingToken">
               <td width="140" align="left">
                    Credit Card Authority:
                </td>
                <td>
                  <asp:dropdownlist ID = "BillingTokenSelect" style="width:350px;" runat="server" />
                </td>
            </tr>
        </table>
        
    </asp:Panel>
    <br />
    <%--style="display:none"--%>
    <asp:Panel ID="pnlPayment_CreditCardAndDebitCard" runat="server" Width="100%" >
        <table id="BillingToken">
            <tr>
                <td width="140" align="left">
                    <b>Credit Card, EFT POS</b>
                </td>
            </tr>
            <tr id="trCardPresent" runat="server">
                <td width="140" align="left">
                    Card Present?:
                </td>
                <td>
                    <asp:RadioButtonList ID="rdoCardPresent" runat="server" AutoPostBack="false" RepeatDirection="Horizontal">
                        <asp:ListItem Value="1">Yes</asp:ListItem>
                        <asp:ListItem Value="0">No</asp:ListItem>
                    </asp:RadioButtonList></td>
            </tr>
            <tr>
                <td width="140" align="left">
                    Card Type:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCardType" runat="server" AutoPostBack="false" Width="150px" />&nbsp;*
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfv_pnlPayment_CardType" runat="server" ErrorMessage="Card Type is required."
                        ControlToValidate="ddlCardType" Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreditCardAndDebitCard">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="140" align="left">
                    Card Number:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtCardNumber" MaxLength="25" />&nbsp;*
                    <ajaxToolkit:MaskedEditExtender ID="msk_txtCardNumber" TargetControlID="txtCardNumber"
                        Mask="9999-9999-9999-9999" MessageValidatorTip="true" MaskType="Number" ErrorTooltipEnabled="True"
                        runat="server" ClearMaskOnLostFocus="true" AutoComplete="false" />
                    <asp:Literal runat="server" ID="litCardNumber" EnableViewState="true" Visible="false" />
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfv_pnlPayment_CardNumber" runat="server" ErrorMessage="CardNumber is required."
                        ControlToValidate="txtCardNumber" Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreditCardAndDebitCard"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            
        <%--    <!--rev:mia march 12 2012 - added cvc2 START-->
            <tr runat="server" id="tr_cvv2">
                    <td width="140" align="left">
                        Card Security Code:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCVV2" MaxLength="3" width="50"/>&nbsp;<a href="http://www.paymentexpress.com/securitycode.htm" target="_blank"><u>What is this?</u></a>&nbsp;<asp:Label ID="labelCVV2" runat="server" Width="10">*</asp:Label>
                       
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="rfv_cvv2" 
                                                    runat="server" 
                                                    ErrorMessage="CVV2 is required."
                                                    ControlToValidate="txtCVV2" 
                                                    SetFocusOnError="True" 
                                                    ValidationGroup="CreditCardAndDebitCard"
                                                    EnableClientScript="false"
                                                    >*</asp:RequiredFieldValidator>

                    <ajaxToolkit:FilteredTextBoxExtender ID="flt_cvv2" 
                                                         runat="server" 
                                                         FilterType="Numbers"
                                                         TargetControlID="txtCVV2" 
                                                          />
                     <asp:CustomValidator ID="cv_cvv2" 
                                          runat="server" 
                                          ErrorMessage="Please check your CVV2." 
                                          ControlToValidate="txtCVV2" 
                                          SetFocusOnError="True" 
                                          ValidationGroup="CreditCardAndDebitCard"
                                          EnableClientScript="false"
                                          >*</asp:CustomValidator>
                                           
                    </td>
                   
                </tr>--%>
            </tr>
            <!--rev:mia march 12 2012 - added cvc2 END-->


            <tr>
                <td width="140" align="left">
                    Name:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="TxtName" MaxLength="60" />&nbsp;*
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfv_pnlPayment_Name" runat="server" ErrorMessage="Name is required."
                        ControlToValidate="TxtName" Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreditCardAndDebitCard"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="140" align="left">
                    Expiry Date:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtExpiryData" Width="50px" MaxLength="5" />
                    &nbsp;mm/yy&nbsp;*
                </td>
                <td>    
                    <asp:RequiredFieldValidator ID="rfv_pnlPayment_ExpiryData" runat="server" ErrorMessage="Expiry Date is required"
                        ControlToValidate="txtExpiryData" Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreditCardAndDebitCard"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ControlToValidate="txtExpiryData" runat="server"
                        EnableClientScript="false" SetFocusOnError="true" ID="REG_txtExpirydate" ValidationExpression="^((0[1-9])|(1[0-2]))\/(\d{2})$"
                        Display="Dynamic" ErrorMessage="Enter expiry date in mm/yy format." ValidationGroup="CreditCardAndDebitCard">*</asp:RegularExpressionValidator>
                </td>
            </tr>

                <!--rev:mia march 12 2012 - added cvc2 START-->
            <tr runat="server" id="tr_cvv2">
                    <td width="140" align="left">
                        Card Security Code:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCVV2" MaxLength="3" width="50"/>&nbsp;
<%--                        <a href="http://www.paymentexpress.com/securitycode.htm" target="_blank" ><u>What is this?</u></a>--%>
                        <%-- <asp:Label ID="lblCVVInfo" runat="server" Width="80"><u>What is this?</u></asp:Label>--%>
                        
                        <span onclick="CvvInfo();" onmouseover="this.style.cursor='hand'"><u>What is this?</u></span>
                        
                        &nbsp;<asp:Label ID="labelCVV2" runat="server" Width="10">*</asp:Label>
                       
                    </td>
                    <td align="left">
                        <asp:RequiredFieldValidator ID="rfv_cvv2" 
                                                    runat="server" 
                                                    ErrorMessage="CVV2 is required."
                                                    ControlToValidate="txtCVV2" 
                                                    SetFocusOnError="True" 
                                                    ValidationGroup="CreditCardAndDebitCard"
                                                    EnableClientScript="false"
                                                    >*</asp:RequiredFieldValidator>

                    <ajaxToolkit:FilteredTextBoxExtender ID="flt_cvv2" 
                                                         runat="server" 
                                                         FilterType="Numbers"
                                                         TargetControlID="txtCVV2" 
                                                          />
                     <asp:CustomValidator ID="cv_cvv2" 
                                          runat="server" 
                                          ErrorMessage="Please check your CVV2." 
                                          ControlToValidate="txtCVV2" 
                                          SetFocusOnError="True" 
                                          ValidationGroup="CreditCardAndDebitCard"
                                          EnableClientScript="false"
                                          >*</asp:CustomValidator>
                                           
                    </td>
                   
                </tr>

            <tr>
                <td width="140" align="left">
                    Approval Reference:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtApprovalReference" />&nbsp;*
                </td>
                <td>
                    <asp:RequiredFieldValidator ID="rfv_pnlPayment_ApprovalReference" runat="server"
                        ErrorMessage="Approval Reference is required." ControlToValidate="txtApprovalReference"
                        Display="Dynamic" SetFocusOnError="True" ValidationGroup="CreditCardAndDebitCard"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td width="140" align="left">
                    Amount:
                </td>
                <td>
                    <asp:TextBox runat="server" ID="txtAmount" style="text-align: right" />
                    *
                    <asp:Label ID="lblCreditCardcurrency" runat="server" Height="16px" Width="40px"></asp:Label>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FLT_txtAmount" runat="server" FilterType="Numbers,Custom"
                    TargetControlID="txtAmount" ValidChars=".-" />
                    </td>
                
                <td>
                    <asp:RequiredFieldValidator ID="rfv_pnlPayment_Amount" runat="server" ErrorMessage="Amount is required."
                        ControlToValidate="txtAmount" SetFocusOnError="True" ValidationGroup="CreditCardAndDebitCard"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlTravellersCheque" runat="server" Width="100%">
        <table Width="100%">
            <tr>
                <td>
                    <b>Traveller's Cheque</b>
                </td>
            </tr>
            <tr>
                <td width="140" align="left" valign="bottom">
                    Cheque
                </td>
                <td width="70" align="left" valign="bottom">
                    Bank
                </td>
                <td width="70" align="left" valign="bottom">
                    Branch
                </td>
                <td width="70" align="left" valign="bottom">
                    Account No.
                </td>
                <td width="70" align="left" valign="bottom">
                    Cheque No.
                </td>
                <td width="70" align="left" valign="bottom">
                    Currency
                </td>
                <td width="70" align="left" valign="bottom">
                    Amount
                </td>
                <td width="150" align="left" valign="bottom">
                    No. of Sequential Cheques
                </td>
                <td></td>
            </tr>
            <tr>
                <td width="140" align="left">
                    Number Details:*
                </td>
                <td width="70">
                    <asp:TextBox ID="txt_TravellersCheque_Bank" runat="server" Width="55px" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_TravellersCheque_Bank" runat="server" Display="Dynamic"
                        ErrorMessage="Bank is required." SetFocusOnError="True" ValidationGroup="TravellersCheque"
                        ControlToValidate="txt_TravellersCheque_Bank" EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
                <td width="70" >
                    <asp:TextBox ID="txt_TravellersCheque_Branch" runat="server" Width="55px" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_TravellersCheque_Branch" runat="server" Display="Dynamic"
                        ErrorMessage="Branch is required." SetFocusOnError="True" ValidationGroup="TravellersCheque"
                        ControlToValidate="txt_TravellersCheque_Branch" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
                <td width="70" >
                    <asp:TextBox ID="txt_TravellersCheque_AccountNo" runat="server" Width="55px" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_TravellersCheque_AccountNo" runat="server" Display="Dynamic"
                        ErrorMessage="Account Number is required." SetFocusOnError="True" ValidationGroup="TravellersCheque"
                        ControlToValidate="txt_TravellersCheque_AccountNo" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
                <td width="70">
                    <asp:TextBox ID="txt_TravellersCheque_ChequeNo" runat="server" Width="55px" MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_TravellersCheque_ChequeNo" runat="server" Display="Dynamic"
                        ErrorMessage="Cheque Number is required." SetFocusOnError="True" ValidationGroup="TravellersCheque"
                        ControlToValidate="txt_TravellersCheque_ChequeNo" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
                <td width="70">
                    <asp:DropDownList ID="ddl_TravellersCheque_Currency" runat="server" AutoPostBack="true"
                        Width="60px" />
                </td>
                <td width="70">
                    <asp:TextBox ID="txt_TravellersCheque_Amount" runat="server" Width="55px" AutoPostBack="false"
                        MaxLength="10"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_TravellersCheque_Amount" runat="server" Display="Dynamic"
                        ErrorMessage="Amount is required." SetFocusOnError="True" ValidationGroup="TravellersCheque"
                        ControlToValidate="txt_TravellersCheque_Amount" EnableClientScript="false">*</asp:RequiredFieldValidator></td>
                <ajaxToolkit:FilteredTextBoxExtender ID="flt_TravellersCheque_Amount" runat="server"
                    FilterType="Numbers,Custom" TargetControlID="txt_TravellersCheque_Amount" ValidChars=".-" />
                <td width="120">
                    <asp:TextBox ID="txt_TravellersCheque_SeqChequec" runat="server" Width="55px" Text="1"
                        MaxLength="3"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfv_TravellersCheque_SeqChequec" runat="server" Display="Dynamic"
                        ErrorMessage="Sequential Check Number is required." SetFocusOnError="True" ValidationGroup="TravellersCheque"
                        ControlToValidate="txt_TravellersCheque_SeqChequec" EnableClientScript="false">*</asp:RequiredFieldValidator>
                    <ajaxToolkit:FilteredTextBoxExtender ID="flt_TravellersCheque_SeqChequec" runat="server"
                        FilterType="Numbers" TargetControlID="txt_TravellersCheque_SeqChequec" />
                </td>
                <td>
                <asp:Button ID="btnAddCheck" runat="server" Text="Add Cheque" ValidationGroup="TravellersCheque"  CssClass="Button_Standard"/>
                </td>
            </tr>
            <tr>
                <td colspan=10 align=right > 
                <%--<asp:Button ID="btnAddCheck" runat="server" Text="Add Cheque" ValidationGroup="TravellersCheque"  CssClass="Button_Standard"/>--%>
                </td>
            </tr>
           
        </table>
        <br />
        <center>
            <asp:Repeater ID="repTravellersCheque" runat="server">
                <HeaderTemplate>
                    <table valign="top" width="100%" bordercolor="#C0C0C0" cellspacing="0" cellpadding="0"
                        border="1" class="dataTable">
                        <thead>
                            <tr>
                                <th width="75" style="border-color: Black; text-align: left;">
                                    Bank
                                </th>
                                <th width="75" style="border-color: Black; text-align: left;">
                                    Branch
                                </th>
                                <th width="75" style="border-color: Black; text-align: left;">
                                    Account No.
                                </th>
                                <th width="75" style="border-color: Black; text-align: left;">
                                    Cheque No.
                                </th>
                                <th width="75" style="border-color: Black; text-align: left;">
                                    Currency
                                </th>
                                <th width="50" style="border-color: Black; text-align: left;">
                                    Amount
                                </th>
                                <th style="border-color: Black; text-align: center; width:auto;">
                                    <asp:CheckBox runat="server" ID="chkRemoveRemoveTravellersCheque" AutoPostBack="true"
                                        OnCheckedChanged="chkRemoveTravellersCheque_CheckedChanged" Enabled="true" />
                                    <asp:Button ID="btnRemoveTravellersCheque" runat="server" Text="Remove" OnClick="btnRemoveTravellersCheque_Click" CssClass="Button_Standard"
                                        Width="50" Enabled="true" />
                                </th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr class="evenRow" align="center">
                        <td width="75">
                            <asp:TextBox ID="rep_Trav_Bank" runat="server" Text='<%# eval("Bank") %>' Width="75"></asp:TextBox>
                        </td>
                        <td width="75">
                            <asp:TextBox ID="rep_Trav_Branch" runat="server" Text='<%# eval("Branch") %>'></asp:TextBox>
                        </td>
                        <td width="75">
                            <asp:TextBox ID="rep_Trav_AccountNo" runat="server" Text='<%# eval("AccountNo") %>'></asp:TextBox>
                        </td>
                        <td width="75">
                            <asp:TextBox ID="rep_Trav_ChequeNo" runat="server" Text='<%# eval("ChequeNo") %>'></asp:TextBox>
                        </td>
                        <td width="100" align="left">
                            <asp:DropDownList ID="rep_TravellersCheque_Currency" runat="server" AutoPostBack="true"
                                Width="75" OnSelectedIndexChanged="rep_TravellersCheque_Currency_SelectedIndexChanged" />
                        </td>
                        <td width="50">
                            <asp:TextBox ID="rep_Trav_Amount" runat="server" Text='<%# eval("Amount") %>' Width="50"></asp:TextBox>
                            <ajaxToolkit:FilteredTextBoxExtender ID="FLT_rep_Trav_Amount" runat="server" FilterType="Numbers,Custom"
                                TargetControlID="rep_Trav_Amount" ValidChars=".-" />
                        </td>
                        <td width="80">
                            <asp:CheckBox runat="server" ID="chkTravellersCheckRowRemove" AutoPostBack="false"
                                OnCheckedChanged="chkTravellersCheckRowRemove_CheckedChanged" />
                            <asp:Literal ID="litTravellersCheckRowRemoveID" runat="server" EnableViewState="true"
                                Text='<%# eval("id") %>' Visible="false" />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    <tr>
                        <td width="75" align="left" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                        </td>
                        <td width="75" align="left" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                        </td>
                        <td width="75" align="left" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                        </td>
                        <td width="100" align="left" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                            Total by Currency:
                        </td>
                        <td width="100" align="left" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                            <asp:Literal ID="litTravellersFooterCurrency" runat="server" />
                        </td>
                        <td width="100" align="center" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                            <asp:Literal ID="litTravellersFooterAmount" runat="server" />
                        </td>
                        <td width="75" align="left" style="border-top: none; border-right: none; border-color: Black"
                            bgcolor="#dcdcdc">
                            <asp:Label ID="lblTotalRenderedCurrency" runat="server" />
                        </td>
                    </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </center>
    </asp:Panel>
    <asp:Panel ID="pnlPersonalCheque" runat="server" Width="100%">
        <table>
            <tr>
                <td>
                    <b>Personal Cheque</b>
                </td>
            </tr>
            <tr>
                <td width="140" align="left">
                </td>
                <td width="70" align="left">
                    Cheque No
                </td>
                <td>
                </td>
                <td width="70" align="left">
                    Bank
                </td>
                <td>
                </td>
                <td width="70" align="left">
                    Branch
                </td>
                <td>
                </td>
                <td width="70" align="left">
                    Account No.
                </td>
                <td>
                </td>
            </tr>
            <tr width="100%">
                <td width="140" align="left">
                    Cheque Details:*
                </td>
                <td width="70" align="left">
                    <asp:TextBox ID="txt_PersonalCheque_ChequeNo" runat="server" Width="60px" />
                    <asp:RequiredFieldValidator ID="rfv_PersonalCheque_ChequeNo" runat="server" Display="Dynamic"
                        ErrorMessage="Check Number is required." ValidationGroup="personalcheque" ControlToValidate="txt_PersonalCheque_ChequeNo"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
                <td align="center">
                    -</td>
                <td width="70" align="left">
                    <asp:TextBox ID="txt_PersonalCheque_Bank" runat="server" Width="60px" />
                    <asp:RequiredFieldValidator ID="rfv_PersonalCheque_Bank" runat="server" Display="Dynamic"
                        ErrorMessage="Bank is required." ValidationGroup="personalcheque" ControlToValidate="txt_PersonalCheque_Bank"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
                <td align="center">
                    -</td>
                <td width="70" align="left">
                    <asp:TextBox ID="txt_PersonalCheque_Branch" runat="server" Width="60px" />
                    <asp:RequiredFieldValidator ID="rfv_PersonalCheque_Branch" runat="server" Display="Dynamic"
                        ErrorMessage="Branch is required." ValidationGroup="personalcheque" ControlToValidate="txt_PersonalCheque_Branch"
                        EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
                <td align=center>
                    -</td>
                <td width="70" align="left">
                    <asp:TextBox ID="txt_PersonalCheque_AccountNo" runat="server" Width="60px" />
                    <asp:RequiredFieldValidator ID="rfv_PersonalCheque_AccountNo" runat="server" Display="Dynamic"
                        ErrorMessage="Account Number is required." SetFocusOnError="True" ValidationGroup="personalcheque"
                        ControlToValidate="txt_PersonalCheque_AccountNo" EnableClientScript="false">*</asp:RequiredFieldValidator>
                </td>
                <td align="left">
                    &nbsp;*</td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td width="140" align="left" style="height: 26px">
                    Account Name:
                </td>
                <td align="left" colspan="6" style="height: 26px">
                    <asp:TextBox ID="txt_PersonalCheque_AccountName" runat="server" Width="200px" />&nbsp;*
                    <asp:RequiredFieldValidator
                        ID="rfv_PersonalCheque_AccountName" runat="server" Display="Dynamic" ErrorMessage="Account Name is required."
                        SetFocusOnError="True" ValidationGroup="personalcheque" ControlToValidate="txt_PersonalCheque_AccountName"
                        EnableClientScript="false">*</asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td width="140" align="left" >
                    Amount:
                </td>
                <td align="left" style="height: 26px">
                    <asp:TextBox runat="server" ID="txt_PersonalCheque_Amount" />
                    <asp:Label ID="lbl_PersonalCheque_Currency" runat="server" Width="50px" />&nbsp;*
                    <asp:RequiredFieldValidator ID="rfv_PersonalCheque_Amount" runat="server" Display="Dynamic"
                        ErrorMessage="Amount is required." SetFocusOnError="True" ValidationGroup="personalcheque"
                        ControlToValidate="txt_PersonalCheque_Amount">*</asp:RequiredFieldValidator>
                    <ajaxToolkit:FilteredTextBoxExtender ID="flt_PersonalCheque_Amount" runat="server"
                        FilterType="Numbers,Custom" TargetControlID="txt_PersonalCheque_Amount" ValidChars=".-" />
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlCash" runat="server" Width="100%">
        <table>
            <tr>
                <td colspan="4" width=140>
                    <b>Cash</b>
                </td>
            </tr>
            <tr>
          
                <td>
                    <table valign="top" width="500" class="dataTable">
                        <thead>
                            <tr>
                                <th width="50">
                                    Amount*
                                </th>
                                <th width="85">
                                    Currency*
                                </th>
                                <th width="85">
                                    Amount (<asp:Label ID="lblCashCurrency" runat="server" />)
                                </th>
                                <th width="85">
                                    <asp:CheckBox runat="server" ID="chkCash" AutoPostBack="true" Text="Check" OnCheckedChanged="chkCash_CheckedChanged" />
                                </th>
                                <th width="85" >
                                    <asp:Button ID="btnRemoveCash" Text="Remove Cash" runat="server" CssClass="Button_Standard" />
                                </th>
                            </tr>
                        </thead>
                        <tr class="evenRow">
                            <td width="75">
                                <asp:TextBox ID="txt_Cash_amount1" runat="server" Width="70px" OnTextChanged="txt_Cash_amount1_TextChanged"
                                    AutoPostBack="true" style="text-align: right" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="flt_txt_Cash_amount1" runat="server" FilterType="Numbers,Custom"
                                    TargetControlID="txt_Cash_amount1" ValidChars=".-" />
                                <asp:RequiredFieldValidator ID="rfv_txt_Cash_amount1" runat="server" Display="Dynamic"
                                    ErrorMessage="At least one amount is required." ValidationGroup="cash" ControlToValidate="txt_Cash_amount1"
                                    EnableClientScript="false">*</asp:RequiredFieldValidator>
                            </td>
                            <td width="85">
                                <asp:DropDownList ID="ddl_Cash_Currency1" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_Cash_Currency1_SelectedIndexChanged" />
                            </td>
                            <td width="85" align=right>
                                <asp:Label ID="lbl_Cash_amount1" runat="server" Text="0.00" />
                            </td>
                            <td width="85">
                                <asp:CheckBox runat="server" ID="chk_Cash_Check1" AutoPostBack="false" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="oddRow" >
                            <td width="75">
                                <asp:TextBox ID="txt_Cash_amount2" runat="server" Width="70px" OnTextChanged="txt_Cash_amount2_TextChanged"
                                    AutoPostBack="true" style="text-align: right" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="lft_txt_Cash_amount2" runat="server" FilterType="Numbers,Custom"
                                    TargetControlID="txt_Cash_amount2" ValidChars=".-" />
                            </td>
                            <td width="85">
                                <asp:DropDownList ID="ddl_Cash_Currency2" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_Cash_Currency2_SelectedIndexChanged" />
                            </td>
                            <td width="85" align=right>
                                <asp:Label ID="lbl_Cash_amount2" runat="server" Text="0.00" />
                            </td>
                            <td width="85">
                                <asp:CheckBox runat="server" ID="chk_Cash_Check2" AutoPostBack="false" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="evenRow" >
                            <td width="75" >
                                <asp:TextBox ID="txt_Cash_amount3" runat="server" Width="70px" OnTextChanged="txt_Cash_amount3_TextChanged"
                                    AutoPostBack="true" style="text-align: right" />
                                <ajaxToolkit:FilteredTextBoxExtender ID="flt_txt_Cash_amount3" runat="server" FilterType="Numbers,Custom"
                                    TargetControlID="txt_Cash_amount3" ValidChars=".-" />
                            </td>
                            <td width="85">
                                <asp:DropDownList ID="ddl_Cash_Currency3" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddl_Cash_Currency3_SelectedIndexChanged" />
                            </td>
                            <td width="85" align=right>
                                <asp:Label ID="lbl_Cash_amount3" runat="server" Text="0.00" />
                            </td>
                            <td width="85">
                                <asp:CheckBox runat="server" ID="chk_Cash_Check3" AutoPostBack="false" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr class="oddRow" align="center">
                            <td width="100">
                                Total Tendered (Local)
                            </td>
                            <td width="20" >
                            </td>
                            <td width="85" align=right>
                                <asp:Label ID="lblTotal" runat="server" Text="0.00" />
                            </td>
                            <td width="85">
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlPaymentEntries" runat="server" Width="100%">
        <br />
        <b>Payment Entries</b>&nbsp;&nbsp;
        <asp:Label ID="lblNoEntries" runat="server" />
        <br />
        <br />
        <asp:Label ID="LabelWarningForEftpos" runat="server"  Text=""  Font-Bold="true" Font-Size="Larger" />
        <br />
        <center>
            <asp:Panel ID="pnlPaymentEntriesHeader" runat="server" Width="100%">
                <table class="dataTable" width="100%">
                    <thead>
                        <tr>
                            <th >
                                Method
                            </th>
                            <th >
                                Details
                            </th>
                            <th >
                                Currency
                            </th>
                            <th >
                                Amount
                            </th>
                            <th runat="server" id="thcurNoEntries">
                                        Amount <br />
                                        <asp:Literal id="litcurNoEntries" runat="server" />
                                        <%=GridCurrency%>
                            </th>
                            <th>
                                <asp:CheckBox runat="server" ID="chkRemoveHeader" AutoPostBack="false" OnCheckedChanged="chkRemove_CheckedChanged"
                                    Enabled="false" />
                                <asp:Button ID="btnRemoveHeader" runat="server" Text="Remove" OnClick="btnRemove_Click" CssClass="Button_Standard"
                                    Width="55" Enabled="false" />
                            </th>
                        </tr>
                    </thead>
                    <tr class="evenRow">
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td align=left >
                            <asp:CheckBox runat="server" ID="chkRemoveRowTest" AutoPostBack="true" OnCheckedChanged="chkRemoveRow_CheckedChanged"
                                Enabled="false" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </center>
        <table width="100%">
            <tr>
                <td colspan="5">
                    <asp:Repeater ID="RepPaymentEntries" runat="server">
                        <HeaderTemplate>
                            <table class="dataTable" width="100%">
                                <tr>
                                    <th>
                                        Method
                                    </th>
                                    <th>
                                        Details
                                    </th>
                                    <th>
                                        Currency
                                    </th>
                                    <th>
                                        Amount
                                    </th>
                                    <th runat="server" id="trcurrency">
                                        Amount <br />
                                        <asp:Literal id="litcur" runat="server" />
                                    </th>
                                    <th>
                                        <asp:CheckBox runat="server" ID="chkRemove" AutoPostBack="true" OnCheckedChanged="chkRemove_CheckedChanged" />
                                        <asp:Button ID="btnRemove" runat="server" Text="Remove" OnClick="btnRemove_Click" CssClass="Button_Standard"
                                            Width="55" />
                                    </th>
                                </tr>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class="evenRow" align="left">
                                <td>
                                    <%# Eval("Method") %>
                                </td>
                                <td>
                                    <%# Eval("Details") %>
                                </td>
                                <td>
                                    <%# Eval("CurrencyDesc") %>
                                </td>
                                <td>
                                    <%# Eval("Amount") %>
                                </td>
                                <td>
                                    <%# Eval("LocalAmount") %>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkRemoveRow" AutoPostBack="true" OnCheckedChanged="chkRemoveRow_CheckedChanged" />
                                    <asp:Literal ID="litID" runat="server" Text='<%# eval("id") %>' Visible="false" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                            <tr class="oddRow" align="left">
                                <td>
                                    <%# Eval("Method") %>
                                </td>
                                <td>
                                    <%# Eval("Details") %>
                                </td>
                                <td>
                                    <%# Eval("CurrencyDesc") %>
                                </td>
                                <td>
                                    <%# Eval("Amount") %>
                                </td>
                                <td>
                                    <%# Eval("LocalAmount") %>
                                </td>
                                <td>
                                    <asp:CheckBox runat="server" ID="chkRemoveRow" AutoPostBack="true" OnCheckedChanged="chkRemoveRow_CheckedChanged" />
                                    <asp:Literal ID="litID" runat="server" Text='<%# eval("id") %>' Visible="false" />
                                </td>
                            </tr>
                        </AlternatingItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
            <table width="100%">
                <tr align="right">
                    <td>
                        <asp:Button ID="BtnbackToBooking" runat="server" Text="Back" CssClass="Button_Standard Button_Back"   width="100px"/>
                        <asp:Button ID="BtnCancel" runat="server" Text="Reset" CssClass="Button_Standard Button_Reset" width="100px"/>
                        <asp:Button ID="btnAddPayment" runat="server" Text="Add Payment" CssClass="Button_Standard Button_Add" width="135px"/>
                        <asp:Button ID="btnReverse" runat="server" Text="Reverse Payment" CssClass="Button_Standard" width="135px"/>
                        <asp:Button ID="btnSave" runat="server" Text="Save Payment" CssClass="Button_Standard Button_Save" width="135px"/>
                    </td>
                    <td>
                        <%--Feb 12 2010 - DPS eftpos integration - manny--%>
                        <div id="divBtnEftpos">
                            <asp:Button ID="btnEFTPOS" runat="server" Text="EFTPOS" CssClass="Button_Standard Button_Save" width="142px"  />
                        </div>
                    </td>
                </tr>
            </table>
    
    </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:Label id="lblWarning" runat="server" text="" ForeColor="red" Font-Size="Medium" Font-Bold="true" />
    <asp:Literal ID="litPaymentRequest" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlPaymentmgt" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlPaymentmgtOrig" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlPaymentDetails" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="Litidtoremove" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlAmountOutStanding" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlPaymentDetailsOrig" runat="server" EnableViewState="true"
        Visible="false" />
    <asp:Literal ID="litpaymententrymgt" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlActivePaymentData" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlPaymentMethods" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litxmlTravellerChequeDetails" runat="server" EnableViewState="true"
        Visible="false" />
    <asp:Literal ID="litjs" runat="server" EnableViewState="true" />
    <asp:Literal ID="LitMode" runat="server" EnableViewState="true" Visible="false" />
    <asp:UpdatePanel runat="server" ID="UpdatePanel1">
        <ContentTemplate>
            <uc1:ConfirmationBoxControl ID="ConfirmationBoxControl_DoReverse" runat="server"
                Title="Reversal Confirmation" Text="Card Present for reversal?" />
            <uc1:ConfirmationBoxControl ID="ConfirmationBoxControl_DoReverseWithoutRole" runat="server"
                Title="Reversal Confirmation" Text="Are you sure you wish to reverse this payment?" />
            <uc1:ConfirmationBoxControl ID="ConfirmationBoxControl_DoReverseWithoutRoleButnotTrue"
                runat="server" Title="Reversal Confirmation" Text="Are you sure you wish to reverse this payment?" />
            <uc4:ConfirmationBoxControlExtended ID="ConfirmationBoxControlExtended1" runat="server"
                Title="Reversal Confirmation" Text="Card Present for reversal?" />
            <uc5:ConfirmationBoxControl ID="ConfirmationBoxControl_bondreverse" runat="server"
                Title="Bond Transfer" Text="Are you sure you wish to Transfer this payment?" />
                
            <uc6:SupervisorOverrideControl ID="SupervisorOverrideControl1" 
                                           runat="server"  
                                           ContinueButtonVisible="false" 
                                           ContinueButtonText="" 
                                           ContinueButtonWidth="200" />    
            
            <uc7:ConfirmationBoxControl ID="ConfirmationBoxControl_NoDPSTransaction" runat="server"
                Title="Reversal Confirmation" Text="Are you sure you wish to reverse this payment?" />


            <uc10:SupervisorOverrideControl ID="SupervisorOverrideControlTrigger" 
                                           runat="server"  
                                           ContinueButtonVisible="false" 
                                           ContinueButtonText="" 
                                           ContinueButtonWidth="200" />                                              
                                           
            <uc8:ConfirmationBoxControlEFTPOS ID="ConfirmationBoxControlEFTPOS" runat="server"
                Title="Reverse Using EFTPOS" Text="Do you want to use EFTPOS Pinpad?" />      
                
            <uc9:cbBillingTokenWithNovalue ID="cbBillingTokenWithNovalue" runat="server" 
                                          Title="Credit Card Authority" 
                                          Text="There is no Billing Token stored in this booking. Do you want to create new Billing Token?" />     
                                          
            <asp:Button ID="btnHiddenBillingToken" runat="server" style="display:none"  />       

            
            <uc11:cbDPSPMTnoRoleWithDPZpayment ID="cbDPSPMTnoRoleWithDPZpayment" runat="server" 
                                          Title="DPZ refund is not allowed"      
                                          LeftButton_Visible="false"                                      
                                          Text="You cannot refund this payment. Please click Cancel button." />     
            
            <%--
                rev:mia Aug 7 2012
                        note: All payments/refund made against billing token regardless of any countries 
                        will display a new popup message.
            --%>
            <uc12:cbPaymentAgainstBillingToken ID="cbPaymentAgainstBillingToken" 
                                          runat="server" 
                                          Title="Credit Card Authority Payment and Refund"      
                                           />     
            

        </ContentTemplate>
    </asp:UpdatePanel>
    <%--Feb 15 2010 - DPS eftpos integration - manny--%>
    <asp:HiddenField ID="hiddenPaymentxml" runat="server" />
    <asp:HiddenField ID="HiddenPaymentOutstanding" runat="server" />
    <asp:HiddenField ID="HiddenCurrency" runat="server" />
    <asp:HiddenField ID="HiddenRentalID" runat="server" />
    <asp:HiddenField ID="HiddenUsercode" runat="server" />
    <asp:HiddenField ID="HiddenPurchase" runat="server" />
    <asp:HiddenField ID="HiddenPaymentID" runat="server" />
    <asp:HiddenField ID="HiddenRefundData" runat="server" />
    <asp:HiddenField ID="HiddenType" runat="server" />
    <asp:HiddenField ID="HiddenLocation" runat="server" />
    <asp:HiddenField ID="HiddenCardType" runat="server" />
    <asp:HiddenField ID="HiddenMode" runat="server" />
    <asp:HiddenField ID="HiddenIsOverride" runat="server" />
    <asp:HiddenField ID="HiddenFieldUserPrinter" runat="server" />
    <asp:HiddenField ID="HiddenFieldAmount" runat="server" />
    <asp:HiddenField ID="HiddenFieldBillingTokenSelect" runat="server" />
    <asp:HiddenField ID="HiddenIsEFTPOSactive" runat="server" />
    <div id="divTest"    style="width:750px; height:auto;"></div>
    <asp:HiddenField ID="HiddenPrinterName" runat="server" value=""/>   
    <asp:HiddenField ID="HiddenReasonOverrideSaveValue" runat="server" value=""/>   
    <asp:HiddenField ID="HiddenReasonOverridePaymentValue" runat="server" value=""/>   
    <asp:HiddenField ID="HiddenReasonOther" runat="server" value=""/>   
    <asp:HiddenField ID="HiddenPdtPmtID" runat="server" value=""/>   
    <%--ISSUE #113--%>
    <asp:HiddenField ID="HiddenFieldTxtAmount" runat="server" />
    <asp:HiddenField ID="HiddenFieldInitialStateOfEftposAndRefundIsEnabled" runat="server" />
    <asp:HiddenField ID="HiddenFieldPdtEFTPOSCardMethodDontMatch" runat="server" />
    <asp:HiddenField ID="HiddenFieldPdtEFTPOSCardTypeSwiped" runat="server" />
    <asp:HiddenField ID="HiddenFieldPdtEFTPOSCardTypeSurchargeAdded" runat="server" />
     <%--Problem in EFTPOS currency--%>
     <asp:HiddenField ID="HiddenFieldEFTPOSCurrency" runat="server" />

     <!--rev:mia 02-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1232-->
     <!--rev:mia 20-Mar-2017 https://thlonline.atlassian.net/browse/AURORA-1038 Limit on single refund amount in Aurora for NZ Rentals-->
     <asp:HiddenField ID="HiddenFieldAllowableAmountToRefund" runat="server" />
     <asp:HiddenField ID="HiddenFieldAllowableAmountToRefundForBondNZ" runat="server" />
     <asp:HiddenField ID="HiddenFieldAllowableAmountToRefundReceiptAndDepositNZ" runat="server" />
     <asp:HiddenField ID="HiddenFieldpTypes" runat="server" />
</asp:Content>

