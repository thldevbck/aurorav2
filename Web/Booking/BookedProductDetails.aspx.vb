'' Change Log!
'' 22.9.8   -   Shoel   - Fixes for BPD issues

Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text
Imports System.Data.Common
Imports Aurora.ChkInOutRntCan.Services

<AuroraPageTitleAttribute("Booked Product Details : Booking  ")> _
<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookedProductDetails)> _
Partial Class Booking_BookedProductDetails
    Inherits AuroraPage

#Region " Constant"
    Const SESSION_saveForDetail As String = "saveForDetail"
    Const IntegrityErrorMsg As String = "This Rental has been modified by another user. Please refresh the screen."
    Const SESSION_xmlDso As String = "xmldso" ''converted to viewstate
    Const SESSION_xmlOrig As String = "xmlOrig" ''converted to viewstate
    Const SESSION_xmlpopup As String = "xmlPopup" ''converted to viewstate
    Const SESSION_hashproperties As String = "hashproperties"
    Private Const QS_BOOKING As String = "Booking.aspx?funCode=RS-BOKSUMMGT&activeTab=9"
#End Region

#Region " Variables"

    Protected XMLdom As New XmlDocument
    Protected XMLDSO As New XmlDocument
    Protected XMLOrig As New XmlDocument
    Protected XMLReceive As New XmlDocument
    Protected XMLPopupdata As New XmlDocument
    Protected BPDlistDetails As New XmlDocument
    Protected bValidIntNo As Boolean
    Protected hashproperties As New Hashtable
    Protected XMLaddRes As New XmlDocument
    Dim dict As New StringDictionary

    ''rev:mia oct.18
    Dim aryError() As String
#End Region

#Region " Properties"



    Protected Property HiddenRentalID() As String
        Get
            Return ViewState("_hiddenrentalid")
        End Get
        Set(ByVal value As String)
            ViewState("_hiddenrentalid") = value
        End Set
    End Property


    Public Property HiddenBookingID() As String
        Get
            Return ViewState("_hiddenBookingid")
        End Get
        Set(ByVal value As String)
            ViewState("_hiddenBookingid") = value
        End Set
    End Property

    Protected ReadOnly Property RentalNumber() As String
        Get

        End Get

    End Property



    Private Property RentalID() As String
        Get
            Return ViewState("mRentalID")
        End Get
        Set(ByVal value As String)
           ViewState("mRentalID")= value
        End Set
    End Property


    Private Property BookingID() As String
        Get
            Return ViewState("mbookingid")
        End Get
        Set(ByVal value As String)
            ViewState("mbookingid") = value
        End Set
    End Property


    Private Property Result() As Boolean
        Get
            Return ViewState("mResult")
        End Get
        Set(ByVal value As Boolean)
            ViewState("mResult") = value
        End Set
    End Property

    Protected ReadOnly Property Products() As String
        Get

            If Not String.IsNullOrEmpty(CType(ViewState("product"), String)) Then
                Return CType(ViewState("product"), String)
            End If

        End Get
    End Property

    'Private ToDelete As Integer = 0

    Property ToDelete() As Integer
        Get
            If ViewState("ToDelete") Is Nothing OrElse String.IsNullOrEmpty(CStr(ViewState("ToDelete"))) Then
                Return 0
            Else
                Return CInt(ViewState("ToDelete"))
            End If
        End Get
        Set(ByVal value As Integer)
            ViewState("ToDelete") = value
        End Set
    End Property

    Property DisputedProductSequenceId() As String
        Get
            Return CStr(ViewState("BPD_DP_SeqId"))
        End Get
        Set(ByVal value As String)
            ViewState("BPD_DP_SeqId") = value
        End Set
    End Property

    Property DisputedProductSapId() As String
        Get
            Return CStr(ViewState("BPD_DP_SapId"))
        End Get
        Set(ByVal value As String)
            ViewState("BPD_DP_SapId") = value
        End Set
    End Property

    Property DisputedProductPreviouslySelectedBooleanFlag()
        Get
            Return CBool(ViewState("BPD_DP_BoolFlag"))
        End Get
        Set(ByVal value)
            ViewState("BPD_DP_BoolFlag") = value
        End Set
    End Property

    Property BPDIdForIntegrityCheck() As String
        Get
            Return CStr(ViewState("BPD_DP_BPD4Integ"))
        End Get
        Set(ByVal value As String)
            ViewState("BPD_DP_BPD4Integ") = value
        End Set
    End Property

    Property SomethingIsDeleted() As Boolean
        Get
            If ViewState("BPD_DP_SomethingIsDeleted") Is Nothing Then
                Return False
            Else
                Return CBool(ViewState("BPD_DP_SomethingIsDeleted"))
            End If
        End Get
        Set(ByVal value As Boolean)
            ViewState("BPD_DP_SomethingIsDeleted") = value
        End Set
    End Property

#End Region

#Region " Function"

    Protected Function GetDate(ByVal d As Object) As Date
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Dim dDate As Date
            dDate = Utility.ParseDateTime(d, Utility.SystemCulture)
            Return dDate
        Else
            Return Date.MinValue
        End If
    End Function
    Private Function CheckRentalIntegrity(ByVal sRentalId As String, _
                ByVal sBPDId As String, _
                ByVal sBooID As String, _
                ByVal nScreenRentalIntegrityNo As String, _
                ByVal nScreenBookingIntegrityNo As String) As Boolean


        Dim bDataIsCurrent As Boolean = False


        Dim sDataFromDB As String
        Dim oXml As New XmlDocument
        Dim nDataBaseRentalIntegrityNo As String = "0"
        Dim nDataBaseBooIntNo As String = "0"



        sDataFromDB = Aurora.Common.Data.ExecuteScalarSP("RES_GetRentalIntegrity", sRentalId, sBPDId, sBooID)
        oXml.LoadXml(sDataFromDB)


        If Not oXml.SelectSingleNode("/Rental/IntegrityNo") Is Nothing Then
            nDataBaseRentalIntegrityNo = (oXml.SelectSingleNode("/Rental/IntegrityNo").InnerText)
        End If

        If nDataBaseRentalIntegrityNo.Equals(nScreenRentalIntegrityNo) Then

            If Not oXml.SelectSingleNode("/Rental/Booking/BooIntNo") Is Nothing Then
                nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("/Rental/Booking/BooIntNo").InnerText)
            Else

                If Not oXml.SelectSingleNode("/Booking/BooIntNo") Is Nothing Then
                    nDataBaseBooIntNo = CInt(oXml.SelectSingleNode("/Booking/BooIntNo").InnerText)
                End If
            End If


            If nDataBaseBooIntNo.Equals(nScreenBookingIntegrityNo) Then
                bDataIsCurrent = True
            Else
                bDataIsCurrent = False
            End If


        Else
            'failed very first check!
            bDataIsCurrent = False
        End If

        Return bDataIsCurrent

    End Function

    Private Function LoadForDetails() As String
        ''rev":mia dec23
        Dim hasRental As Boolean = False
        Dim xmlstring As String
        If Not XMLdom.SelectSingleNode("//Data/Rental/RntId") Is Nothing Then
            If Not XMLdom.SelectSingleNode("//Data/Rental/IntNo") Is Nothing Then
                If Not XMLdom.SelectSingleNode("//Data/Rental/BooIntNo") Is Nothing Then
                    hasRental = True
                    Dim sRntId As String
                    Dim nIntNo As String
                    Dim nBooIntNo As String

                    sRntId = XMLdom.SelectSingleNode("//Data/Rental/RntId").InnerText
                    nIntNo = CInt(XMLdom.SelectSingleNode("//Data/Rental/IntNo").InnerText)
                    nBooIntNo = CInt(XMLdom.SelectSingleNode("//Data/Rental/BooIntNo").InnerText)

                    bValidIntNo = Me.CheckRentalIntegrity(sRntId, String.Empty, String.Empty, nIntNo, nBooIntNo)
                End If
            End If
        End If

        Dim tempXML As String = XMLdom.OuterXml.ToString
        If bValidIntNo = True Then
            xmlstring = Aurora.ChkInOutRntCan.Services.ManageBPDList.manageBookedPrdList(Me.RentalID, tempXML, Me.Result, MyBase.UserCode)
        Else
            If hasRental = True Then
                xmlstring = IntegrityErrorMsg
                MyBase.AddErrorMessage(IntegrityErrorMsg)
            Else
                xmlstring = Aurora.ChkInOutRntCan.Services.ManageBPDList.manageBookedPrdList(Me.RentalID, tempXML, Me.Result, MyBase.UserCode)
            End If
        End If
        Return xmlstring
    End Function

    Private Function ParseBookedValue(ByVal doc As XmlDocument) As String
        Dim updatefleet As String = doc.DocumentElement.SelectSingleNode("Fleet").InnerText
        Dim intChild As Integer = doc.DocumentElement.ChildNodes(3).ChildNodes.Count - 1

        Dim bpdid As String = ""
        Dim sb As New StringBuilder
        For i As Integer = 0 To intChild
            If Not doc.DocumentElement.ChildNodes(2).ChildNodes(i) Is Nothing Then
                bpdid = doc.DocumentElement.ChildNodes(2).ChildNodes(i).InnerText
            Else
                bpdid = ""
            End If
            Dim SapId As String = doc.DocumentElement.ChildNodes(3).ChildNodes(i).InnerText
            Dim CkoDt As String = doc.DocumentElement.ChildNodes(4).ChildNodes(i).InnerText
            Dim CkoLocCode As String = doc.DocumentElement.ChildNodes(5).ChildNodes(i).InnerText
            Dim CkiDt As String = doc.DocumentElement.ChildNodes(6).ChildNodes(i).InnerText
            Dim CkiLocCode As String = doc.DocumentElement.ChildNodes(7).ChildNodes(i).InnerText
            Dim Rate As String = doc.DocumentElement.ChildNodes(8).ChildNodes(i).InnerText
            Dim Qty As String = doc.DocumentElement.ChildNodes(9).ChildNodes(i).InnerText
            Dim TotalOwing As String = doc.DocumentElement.ChildNodes(10).ChildNodes(i).InnerText
            Dim KmCharged As String = doc.DocumentElement.ChildNodes(11).ChildNodes(i).InnerText

            With sb
                .Append("<BookedProduct>")
                .AppendFormat("<BpdId>{0}</BpdId>", bpdid)
                .AppendFormat("<SapId>{0}</SapId>", SapId)
                .AppendFormat("<CkoDt>{0}</CkoDt>", CkoDt)
                .AppendFormat("<CkoLocCode>{0}</CkoLocCode>", CkoLocCode)
                .AppendFormat("<CkiDt>{0}</CkiDt>", CkiDt)
                .AppendFormat("<CkiLocCode>{0}</CkiLocCode>", CkiLocCode)
                .AppendFormat("<Rate>{0}</Rate>", Rate)
                .AppendFormat("<Qty>{0}</Qty>", Qty)
                .AppendFormat("<TotalOwing>{0}</TotalOwing>", TotalOwing)
                .AppendFormat("<KmCharged>{0}</KmCharged>", KmCharged)
                .Append("</BookedProduct>")
            End With
        Next

        sb.Insert(0, "<Data>")
        sb.Insert(sb.Length, "</Data>")
        Dim xmlstring As String = "<Data><BookedProduct><BpdId>HA2714502</BpdId><SapId>4BB-250</SapId><CkoDt>21/02/2008</CkoDt><CkoLocCode>AKL</CkoLocCode><CkiDt>21/03/2008</CkiDt><CkiLocCode>AKL</CkiLocCode><Rate>0.00</Rate><Qty>0</Qty><TotalOwing>4788.00</TotalOwing><KmCharged>0.00</KmCharged></BookedProduct><BookedProduct><BpdId></BpdId><SapId>94697627-AE78-459A-B756-B157D2D31DA7</SapId><CkoDt></CkoDt><CkoLocCode></CkoLocCode><CkiDt></CkiDt><CkiLocCode></CkiLocCode><Rate>0.00</Rate><Qty>0</Qty><TotalOwing>0.00</TotalOwing><KmCharged>0.00</KmCharged></BookedProduct></Data>"
        Dim bdpresult As String = Aurora.ChkInOutRntCan.Services.ManageBPDList.getBookedProductsDetail(Me.RentalID, sb.ToString, MyBase.UserCode, "booking.aspx")
        BPDlistDetails.LoadXml(bdpresult)
        XMLDSO.LoadXml(BPDlistDetails.OuterXml)
        XMLOrig.LoadXml(BPDlistDetails.OuterXml)

        If Not String.IsNullOrEmpty(Me.RentalID) AndAlso (Not String.IsNullOrEmpty(Me.BookingID)) Then
            ''   @BooId   VARCHAR  (64) = '',  
            ''   @Condition  VARCHAR  (8000) = '',  
            ''   @RntId   VARCHAR  (64) = '',  
            ''   @BooNum   VARCHAR  (64) = '',  
            ''   @sUsrCode  VARCHAR(64)  =   ''  

            Me.XMLPopupdata.LoadXml(Aurora.Common.Data.ExecuteScalarSP("cus_GetBookingDetail", _
                                        Me.HiddenBookingID, _
                                        "BookRntRec", _
                                        Me.RentalID, _
                                        String.Empty, _
                                        MyBase.UserCode))
        End If

    End Function


#End Region

#Region " Sub"

    Sub disablePanel(ByVal blnActive As Boolean)
        pnlAgent.Visible = blnActive
        Me.pnlBookedProduct.Visible = blnActive
        Me.pnlBookedPRoducts.Visible = blnActive
        Me.btnNext.Visible = blnActive
    End Sub

    Sub DisableButtons(ByVal blnActive As Boolean)
        Me.btnNext.Enabled = blnActive
        Me.btnNext.Visible = blnActive
        Me.BtnCancel.Enabled = Not blnActive

        Me.btnOK.Enabled = Not blnActive
        If btnNext.Enabled = False Then
            btnOK.Text = "Next"
        Else
            btnOK.Text = "Ok"
        End If

        Me.pnlAddReason.Visible = Not blnActive
        Me.pnlBookedProduct.Enabled = blnActive
    End Sub

    Function submitReasonData(ByVal blnSave As Boolean, ByRef xmlString As String, ByRef MessageError As String) As Boolean

        If blnSave = True Then
            If XMLaddRes Is Nothing Then
                XMLaddRes = New XmlDocument
                XMLaddRes.LoadXml(litJS.Text)
            End If

            Dim i As Integer = XMLaddRes.DocumentElement.ChildNodes.Count - 1
            For ii As Integer = 0 To i

                Dim Product As String = XMLaddRes.DocumentElement.ChildNodes(ii).SelectSingleNode("PrdShortName").InnerText
                ViewState("product") = Me.lblproductreason.Text
                Dim Reason As String = Me.txtReasons.Text
                If (Reason = "") Then
                    MessageError = ("Reason text for " & Me.lblproductreason.Text & " cannot be blank and should be more than 1 character!")
                    Return False

                End If
                Dim reasons() As String = Reason.Split(" ")

                If reasons.Length = 1 Then
                    MessageError = ("Please enter more than one word.")
                    Return False
                End If

                If reasons(0).Length = 1 Then
                    MessageError = ("Invalid text - please re-enter more than one letter.")
                    Return False
                End If

                If reasons(1).Length = 1 Then
                    MessageError = ("Invalid text - please re-enter than one letter.")
                    Return False
                End If

                If reasons(0).Equals(reasons(1)) Then
                    MessageError = ("Invalid text - no more occurence of word is allowed.")
                    Return False
                End If

                'If reasons(0).Length > 0 Then
                '    Dim retValidatingChar As Boolean = ValidateRepeatingCharacter(reasons(0))
                '    If retValidatingChar = False Then
                '        MessageError = ("Invalid text - no more occurence of character is allowed.")
                '        Return False
                '    End If
                'End If
                XMLaddRes.DocumentElement.ChildNodes(ii).SelectSingleNode("Reason").InnerText = Reason
            Next
        Else
            ''do nothing
        End If
        xmlString = XMLaddRes.OuterXml
        Return True

    End Function

    Private Function ValidateRepeatingCharacter(ByVal reason As String) As Boolean
        Try


            Dim firstChar As String = reason.Substring(0, 1)

            For i As Integer = 0 To reason.Length
                If firstChar <> "" Then
                    If (i + 1) <= reason.Length Then
                        If (reason.Substring(i + 1)) <> "" Then
                            If firstChar.Equals(reason.Substring(i + 1)) Then
                                Return False
                            End If
                        End If
                    End If
                End If
            Next
            Return True

        Catch ex As Exception

        End Try
    End Function

    Private Sub SaveBookedProductDetailsWithReasons(ByVal temp As String)

        If temp = "" Then
            DisableButtons(True)
            MyBase.SetErrorMessage("Reason is mandatory")
            Exit Sub
        End If

        Dim node As XmlNode = Me.XMLDSO.DocumentElement.SelectSingleNode("BookedProducts")

        Dim nodectr As Integer = 0
        Dim message As String = ""
        ''rev:mia oct.6-new code
        Dim tempDataFromControl As String = Me.BookingProductsReasons1.SubmitReasonData(temp)
        If tempDataFromControl.Contains("ERROR:") = False AndAlso Me.BookingProductsReasons1.StopProcessing = False Then
            Dim xmlAddReason As New XmlDocument
            xmlAddReason.LoadXml(tempDataFromControl)
            Dim ctrAddReason As Integer = xmlAddReason.DocumentElement.ChildNodes.Count - 1
            nodectr = 0

            For iii As Integer = 0 To ctrAddReason
                For Each row As GridViewRow In Me.gridBookedProduct.Rows
                    If (row.RowType = DataControlRowType.DataRow) AndAlso (Not String.IsNullOrEmpty(gridBookedProduct.DataKeys(row.DataItemIndex).Value)) Then
                        If node.ChildNodes(nodectr).SelectSingleNode("SeqId").InnerText = xmlAddReason.DocumentElement.ChildNodes(iii).SelectSingleNode("SeqId").InnerText Then
                            node.ChildNodes(nodectr).SelectSingleNode("Reason").InnerText = xmlAddReason.DocumentElement.ChildNodes(iii).SelectSingleNode("Reason").InnerText
                            Exit For
                        End If
                        nodectr = nodectr + 1
                    End If
                Next
            Next
        Else
            MyBase.SetErrorMessage(tempDataFromControl.Replace("ERROR:", ""))
            Exit Sub
        End If

        ''rev:mia oct.6-old code
        'If Me.submitReasonData(True, temp, message) = True Then
        '    Dim xmlAddReason As New XmlDocument
        '    xmlAddReason.LoadXml(temp)
        '    Dim ctrAddReason As Integer = xmlAddReason.DocumentElement.ChildNodes.Count - 1
        '    nodectr = 0

        '    For iii As Integer = 0 To ctrAddReason
        '        For Each row As GridViewRow In Me.gridBookedProduct.Rows
        '            If (row.RowType = DataControlRowType.DataRow) AndAlso (Not String.IsNullOrEmpty(gridBookedProduct.DataKeys(row.DataItemIndex).Value)) Then
        '                If node.ChildNodes(nodectr).SelectSingleNode("SeqId").InnerText = xmlAddReason.DocumentElement.ChildNodes(iii).SelectSingleNode("SeqId").InnerText Then
        '                    node.ChildNodes(nodectr).SelectSingleNode("Reason").InnerText = xmlAddReason.DocumentElement.ChildNodes(iii).SelectSingleNode("Reason").InnerText
        '                    Exit For
        '                End If
        '                nodectr = nodectr + 1
        '            End If
        '        Next
        '    Next

        'Else
        '    MyBase.SetErrorMessage(message)
        '    Exit Sub
        'End If

        For iii As Integer = 0 To gridBookedProduct.Rows.Count - 1
            If (node.ChildNodes(iii).SelectSingleNode("BpdId").InnerText = "" AndAlso _
                node.ChildNodes(iii).SelectSingleNode("AddReason").InnerText = "1" AndAlso _
                node.ChildNodes(iii).SelectSingleNode("Reason").InnerText = "") Then
                MyBase.SetErrorMessage("Reason text for " & node.ChildNodes(iii).SelectSingleNode("PrdShortName").InnerText & " cannot be blank")
            End If
        Next

        Call Me.SaveAllDetails(True)
    End Sub

    Private Sub SaveAllDetails(ByVal blnsave As Boolean)
        Try

            Dim paramRentalID As String = Me.RentalID
            Dim paramValue As String = IIf(blnsave = True, "YES", "NO")

            Dim noOfNode As Integer = Me.XMLOrig.DocumentElement("BookedProducts").ChildNodes.Count - 1
            Dim noOfElements As Integer
            Dim modified As Boolean = False

            Dim origValue As String
            Dim newvalue As String
            Dim sendxml As String = ""


            ' addition by Shoel for FERRPAX!!

            Dim iProductDependentRateRowCounter As Integer = 0

            For Each oNode As XmlNode In XMLDSO.DocumentElement.SelectNodes("BookedProducts/BookedProduct")
                If Not oNode.SelectSingleNode("PrrIdsList") Is Nothing AndAlso oNode.SelectSingleNode("PrrIdsList").InnerXml <> "*" Then
                    ' this is a prr node
                    Dim sbPrrXMLString As StringBuilder = New StringBuilder

                    For iPRRCount As Integer = 0 To rptPersonDependentRates.Items.Count - 1

                        If rptPersonDependentRates.Items(iPRRCount).ItemType = ListItemType.Item _
                           Or _
                           rptPersonDependentRates.Items(iPRRCount).ItemType = ListItemType.AlternatingItem _
                        Then

                            If iPRRCount = iProductDependentRateRowCounter Then
                                ' select subsequent rows
                                ' once a row has been looked at , stop using it!

                                '<PersonRate>
                                '  <PrrId>6E81C8F2-0CE1-410C-8C3A-59B8A30DA466</PrrId> 
                                '  <Code>ADULT</Code> 
                                '  <Rate>76.00</Rate> 
                                '  <RateMod>0</RateMod> 
                                '  <Qty>0.0000</Qty> 
                                '  <QtyMod>1</QtyMod> 
                                '</PersonRate>

                                Dim sSeqId As String
                                Dim sADULT_PrrId, sADULT_Rate, sADULT_RateMod, sADULT_Qty, sADULT_QtyMod As String
                                Dim sCHILD_PrrId, sCHILD_Rate, sCHILD_RateMod, sCHILD_Qty, sCHILD_QtyMod As String
                                Dim sINFANT_PrrId, sINFANT_Rate, sINFANT_RateMod, sINFANT_Qty, sINFANT_QtyMod As String

                                sADULT_PrrId = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnADULT_PrrId"), System.Web.UI.WebControls.HiddenField).Value
                                sADULT_Rate = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("txtADULT_Rate"), System.Web.UI.WebControls.TextBox).Text
                                sADULT_RateMod = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnADULT_RateMod"), System.Web.UI.WebControls.HiddenField).Value
                                sADULT_Qty = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("txtADULT_Qty"), System.Web.UI.WebControls.TextBox).Text
                                sADULT_QtyMod = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnADULT_QtyMod"), System.Web.UI.WebControls.HiddenField).Value

                                sCHILD_PrrId = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnCHILD_PrrId"), System.Web.UI.WebControls.HiddenField).Value
                                sCHILD_Rate = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("txtCHILD_Rate"), System.Web.UI.WebControls.TextBox).Text
                                sCHILD_RateMod = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnCHILD_RateMod"), System.Web.UI.WebControls.HiddenField).Value
                                sCHILD_Qty = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("txtCHILD_Qty"), System.Web.UI.WebControls.TextBox).Text
                                sCHILD_QtyMod = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnCHILD_QtyMod"), System.Web.UI.WebControls.HiddenField).Value

                                sINFANT_PrrId = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnINFANT_PrrId"), System.Web.UI.WebControls.HiddenField).Value
                                sINFANT_Rate = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("txtINFANT_Rate"), System.Web.UI.WebControls.TextBox).Text
                                sINFANT_RateMod = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnINFANT_RateMod"), System.Web.UI.WebControls.HiddenField).Value
                                sINFANT_Qty = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("txtINFANT_Qty"), System.Web.UI.WebControls.TextBox).Text
                                sINFANT_QtyMod = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnINFANT_QtyMod"), System.Web.UI.WebControls.HiddenField).Value

                                sSeqId = CType(rptPersonDependentRates.Items(iPRRCount).FindControl("hdnSeqId"), System.Web.UI.WebControls.HiddenField).Value


                                sbPrrXMLString.Append("<PersonRate>")
                                sbPrrXMLString.Append("<PrrId>" & sADULT_PrrId & "</PrrId>")
                                sbPrrXMLString.Append("<Code>ADULT</Code>")
                                sbPrrXMLString.Append("<Rate>" & String.Format("{0:f}", CDbl(sADULT_Rate)) & "</Rate>")
                                sbPrrXMLString.Append("<RateMod>" & sADULT_RateMod & "</RateMod>")
                                sbPrrXMLString.Append("<Qty>" & String.Format("{0:f4}", CDbl(sADULT_Qty)) & "</Qty>")
                                sbPrrXMLString.Append("<QtyMod>" & sADULT_QtyMod & "</QtyMod>")
                                sbPrrXMLString.Append("</PersonRate>")

                                sbPrrXMLString.Append("<PersonRate>")
                                sbPrrXMLString.Append("<PrrId>" & sCHILD_PrrId & "</PrrId>")
                                sbPrrXMLString.Append("<Code>CHILD</Code>")
                                sbPrrXMLString.Append("<Rate>" & String.Format("{0:f}", CDbl(sCHILD_Rate)) & "</Rate>")
                                sbPrrXMLString.Append("<RateMod>" & sCHILD_RateMod & "</RateMod>")
                                sbPrrXMLString.Append("<Qty>" & String.Format("{0:f4}", CDbl(sCHILD_Qty)) & "</Qty>")
                                sbPrrXMLString.Append("<QtyMod>" & sCHILD_QtyMod & "</QtyMod>")
                                sbPrrXMLString.Append("</PersonRate>")

                                sbPrrXMLString.Append("<PersonRate>")
                                sbPrrXMLString.Append("<PrrId>" & sINFANT_PrrId & "</PrrId>")
                                sbPrrXMLString.Append("<Code>INFANT</Code>")
                                sbPrrXMLString.Append("<Rate>" & String.Format("{0:f}", CDbl(sINFANT_Rate)) & "</Rate>")
                                sbPrrXMLString.Append("<RateMod>" & sINFANT_RateMod & "</RateMod>")
                                sbPrrXMLString.Append("<Qty>" & String.Format("{0:f4}", CDbl(sINFANT_Qty)) & "</Qty>")
                                sbPrrXMLString.Append("<QtyMod>" & sINFANT_QtyMod & "</QtyMod>")
                                sbPrrXMLString.Append("</PersonRate>")


                                ' increment the running counter for prr rows
                                iProductDependentRateRowCounter += 1
                                Exit For
                                ' break out of the for i loop of prr list
                            End If ' If iPRRCount = iProductDependentRateRowCounter Then
                        End If ' is not header / footer
                    Next ' prr rows loop

                    ' add this node to the xmldso

                    oNode.SelectSingleNode("PrrIdsList").InnerXml = sbPrrXMLString.ToString()

                End If ' xmldso node contains prr id list
            Next ' xml dso loop

            ' addition by Shoel for FERRPAX!!



            For i As Integer = 0 To noOfNode
                noOfElements = Me.XMLOrig.DocumentElement("BookedProducts").ChildNodes(i).ChildNodes.Count - 1

                ' set flag to false initially
                modified = False
                ' skip the disputed products that are specified as skip
                If Me.XMLDSO.DocumentElement("BookedProducts").ChildNodes(i).SelectSingleNode("SapId").InnerText.Trim() = DisputedProductSapId _
                   And _
                   Me.XMLDSO.DocumentElement("BookedProducts").ChildNodes(i).SelectSingleNode("SeqId").InnerText.Trim() = DisputedProductSequenceId _
                Then
                    Continue For
                End If

                ' if the BPD ID is empty, its new so please add it :)
                If Me.XMLOrig.DocumentElement("BookedProducts").ChildNodes(i).SelectSingleNode("BpdId").InnerText.Trim() = "" Then
                    modified = True
                Else
                    ' check each node contents
                    For ii As Integer = 0 To noOfElements

                        ' check each node against the original data
                        origValue = Me.XMLOrig.DocumentElement("BookedProducts").ChildNodes(i).ChildNodes(ii).InnerText
                        newvalue = Me.XMLDSO.DocumentElement("BookedProducts").ChildNodes(i).ChildNodes(ii).InnerText
                        If origValue.Equals(newvalue) = False Then
                            modified = True
                            Exit For
                        End If
                    Next
                End If

                ' collect a bpd id for integrity check if you havent done so already!
                If String.IsNullOrEmpty(BPDIdForIntegrityCheck) Then
                    BPDIdForIntegrityCheck = XMLDSO.DocumentElement("BookedProducts").ChildNodes(i).SelectSingleNode("BpdId").InnerText.Trim()
                End If

                If modified = True Then
                    sendxml = sendxml & XMLDSO.DocumentElement.SelectSingleNode("BookedProducts").ChildNodes(i).OuterXml
                End If
            Next

            Dim oSendXml As New XmlDocument
            If Not String.IsNullOrEmpty(sendxml) Then
                sendxml = "<Data><BookedProducts>" + sendxml + "</BookedProducts></Data>"
                oSendXml.LoadXml(sendxml)

                For i As Integer = (oSendXml.DocumentElement.ChildNodes(0).ChildNodes.Count - 1) To 0 Step -1
                    Dim CkoDtModStatus As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkoDtMod").InnerText
                    Dim CkoLocModStatus As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkoLocMod").InnerText
                    Dim CkiDtModStatus As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkiDtMod").InnerText
                    Dim CkiLocModStatus As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkiLocMod").InnerText
                    Dim RateModStatus As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("RateMod").InnerText
                    Dim QtyModStatus As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("QtyMod").InnerText
                    Dim BpdId As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("BpdId").InnerText
                    Dim CkoDt As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkoDt").InnerText
                    Dim CkoLoc As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkoLocCode").InnerText
                    Dim CkiDt As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkiDt").InnerText
                    Dim CkiLoc As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("CkiLocCode").InnerText
                    Dim Rate As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("Rate").InnerText
                    Dim Qty As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("Qty").InnerText

                    Dim sProductShortName As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("PrdShortName").InnerText
                    Dim sSapId As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("SapId").InnerText
                    Dim sSeqId As String = oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("SeqId").InnerText

                    If (Not String.IsNullOrEmpty(BpdId) And CkoDtModStatus = "1" And CkoDt = "") Then
                        MyBase.SetInformationShortMessage("Check-Out date is mandatory on existing booked products - please re-enter.")
                        Exit Sub
                    End If

                    If (Not String.IsNullOrEmpty(BpdId) And CkoLocModStatus = "1" And CkoLoc = "") Then
                        MyBase.SetInformationShortMessage("Check-Out Location is mandatory on existing booked products - please re-enter.")
                        Exit Sub
                    End If

                    If (Not String.IsNullOrEmpty(BpdId) And CkiDtModStatus = "1" And CkiDt = "") Then
                        MyBase.SetInformationShortMessage("Check-In Date is mandatory on existing booked products - please re-enter.")
                        Exit Sub
                    End If

                    If (Not String.IsNullOrEmpty(BpdId) And CkiLocModStatus = "1" And CkiLoc = "") Then
                        MyBase.SetInformationShortMessage("Check-In Location is mandatory on existing booked products - please re-enter.")
                        Exit Sub
                    End If


                    If (Not String.IsNullOrEmpty(BpdId) And RateModStatus = "1" And (Rate = "" OrElse CDbl(Rate) = 0.0)) Then
                        MyBase.SetInformationShortMessage("Rate is mandatory on existing booked products - please re-enter.")
                        Exit Sub
                    End If

                    If (Not String.IsNullOrEmpty(BpdId) And QtyModStatus = "1" And (Qty = "" OrElse CInt(Qty) = 0)) Then
                        MyBase.SetInformationShortMessage("Quantity is mandatory on existing booked products - please re-enter.")
                        Exit Sub
                    End If

                    Dim bProductHasMissingData As Boolean = False
                    Dim sbFieldsThatHaveDataMissing As StringBuilder = New StringBuilder

                    If oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("PrrIdsList").InnerXml <> "*" Then
                        ' this one has PRR Ids
                        ' so atleast one rate must be non zero

                        '<PersonRate>
                        '  <PrrId>6E81C8F2-0CE1-410C-8C3A-59B8A30DA466</PrrId> 
                        '  <Code>ADULT</Code> 
                        '  <Rate>76.00</Rate> 
                        '  <RateMod>0</RateMod> 
                        '  <Qty>0.0000</Qty> 
                        '  <QtyMod>1</QtyMod> 
                        '</PersonRate>
                        Dim nAdultQty, nChildQty, nInfantQty As Integer

                        For Each oPrrNode As XmlNode In oSendXml.DocumentElement.ChildNodes(0).ChildNodes(i).SelectSingleNode("PrrIdsList").ChildNodes
                            Select Case oPrrNode.SelectSingleNode("Code").InnerText.Trim().ToUpper()
                                Case "ADULT"
                                    nAdultQty = CInt(oPrrNode.SelectSingleNode("Qty").InnerText.Trim())
                                Case "CHILD"
                                    nChildQty = CInt(oPrrNode.SelectSingleNode("Qty").InnerText.Trim())
                                Case "INFANT"
                                    nInfantQty = CInt(oPrrNode.SelectSingleNode("Qty").InnerText.Trim())
                            End Select
                        Next

                        If Not String.IsNullOrEmpty(BpdId) And nAdultQty = 0 And nChildQty = 0 And nInfantQty = 0 Then
                            MyBase.SetInformationShortMessage("Quantity is mandatory on existing booked products - please re-enter valid quantity for person dependent rates")
                            Exit Sub
                        ElseIf String.IsNullOrEmpty(BpdId) And nAdultQty = 0 And nChildQty = 0 And nInfantQty = 0 Then
                            ' new product
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Quantity for person dependent rates, ")
                        End If


                    End If

                    ' Addition by Shoel : 17.10.8 : Check the new products for missing data!

                    If String.IsNullOrEmpty(Trim(BpdId)) Then

                        If (CkoDtModStatus = "1" And CkoDt = "") Then
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Check-out Date, ")
                        End If

                        If (CkoLocModStatus = "1" And CkoLoc = "") Then
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Check-out Location, ")
                        End If

                        If (CkiDtModStatus = "1" And CkiDt = "") Then
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Check-in Date, ")
                        End If

                        If (CkiLocModStatus = "1" And CkiLoc = "") Then
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Check-in Location, ")
                        End If


                        If (RateModStatus = "1" And (Rate = "" OrElse CDbl(Rate) = 0.0)) Then
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Rate, ")
                        End If

                        If (QtyModStatus = "1" And (Qty = "" OrElse CInt(Qty) = 0)) Then
                            bProductHasMissingData = True
                            sbFieldsThatHaveDataMissing.Append("Quantity, ")
                        End If

                        If bProductHasMissingData Then
                            DisputedProductSapId = Trim(sSapId)
                            DisputedProductSequenceId = Trim(sSeqId)
                            ShowConfirmationMessage(Trim(sProductShortName), sbFieldsThatHaveDataMissing.ToString())
                            Exit Sub
                        End If

                    End If

                    ' Addition by Shoel : 17.10.8 : Check the new products for missing data!

                Next

                Dim myArray() As String = {"IsVehicle", "number", "ascending"}
                Dim xmlstring As String = oSendXml.OuterXml.Replace("<Data><Data>", "<Data>")
                xmlstring = xmlstring.Replace("</Data></Data>", "</Data>")
                oSendXml.LoadXml(xmlstring)
                Call sortXMLforCustomer(oSendXml, "BookedProducts", "Data/BookedProducts/BookedProduct", myArray)

                If (Not Me.XMLPopupdata.SelectSingleNode("Rental/RntIntNo") Is Nothing) Then
                    oSendXml.LoadXml("<Data>" & oSendXml.OuterXml + "<RntIntNo>" & Me.XMLPopupdata.SelectSingleNode("Rental/RntIntNo").InnerText & "</RntIntNo>" & Me.XMLPopupdata.SelectSingleNode("Rental/BooIntNo").OuterXml & "</Data>")
                Else
                    oSendXml.LoadXml("<Data>" & oSendXml.OuterXml & "</Data>")
                End If

                Dim bValidIntNo As String = Nothing
                Dim sBPDId As String = Nothing
                Dim nIntegNo As Integer
                Dim oNodeCollection As XmlNodeList = Nothing
                Dim nBIntegNo As String = Nothing


                If Not oSendXml.SelectSingleNode("//Data/BookedProducts/BookedProduct/BpdId") Is Nothing Then
                    If String.IsNullOrEmpty(oSendXml.SelectSingleNode("//Data/BookedProducts/BookedProduct/BpdId").InnerText) = False Then
                        sBPDId = oSendXml.SelectSingleNode("//Data/BookedProducts/BookedProduct/BpdId").InnerText
                    End If
                End If


                If Not oSendXml.SelectSingleNode("//Data/RntIntNo") Is Nothing Then
                    If String.IsNullOrEmpty(oSendXml.SelectSingleNode("//Data/RntIntNo").InnerText) = False Then
                        nIntegNo = CInt(oSendXml.SelectSingleNode("//Data/RntIntNo").InnerText)
                    End If

                End If

                If Not oSendXml.SelectSingleNode("//Data/RntIntNo") Is Nothing Then
                    If String.IsNullOrEmpty(oSendXml.SelectSingleNode("//Data/BooIntNo").InnerText) = False Then
                        nBIntegNo = CInt(oSendXml.SelectSingleNode("//Data/BooIntNo").InnerText)
                    End If
                End If

                'If String.IsNullOrEmpty(sBPDId) Then
                '    oNodeCollection = oSendXml.SelectNodes("//Data/BookedProducts/BookedProduct")
                '    For i As Integer = 0 To oNodeCollection.Count - 1
                '        If Not oNodeCollection(i).SelectSingleNode("BpdId") Is Nothing Then
                '            If oNodeCollection(i).SelectSingleNode("BpdId").InnerText <> "" Then
                '                sBPDId = oNodeCollection(i).SelectSingleNode("BpdId").InnerText
                '                Exit For
                '            End If
                '        End If
                '    Next
                'End If

                ''rev:mia dec23-not all product has a bpdid
                Dim strResult As String
                If Not String.IsNullOrEmpty(BPDIdForIntegrityCheck) Then
                    bValidIntNo = CheckRentalIntegrity("", BPDIdForIntegrityCheck, "", nIntegNo, nBIntegNo)
                    If bValidIntNo = True Then
                        ''rev:mia oct.18 dont replace SaveDetailBPD.....work as expected...
                        ''dont modify this one...
                        strResult = Aurora.Booking.Services.BookingProducts.manageBookedProductList(paramRentalID, oSendXml.OuterXml, paramValue, MyBase.UserCode, "SaveDetailBPD", SomethingIsDeleted, "BookedProdDetails")
                    Else
                        MyBase.AddErrorMessage(IntegrityErrorMsg)
                        Exit Sub
                    End If
                Else
                    strResult = Aurora.Booking.Services.BookingProducts.manageBookedProductList(paramRentalID, oSendXml.OuterXml, paramValue, MyBase.UserCode, "SaveDetailBPD", SomethingIsDeleted, "BookedProdDetails")
                End If
                

                Dim tempdoc As New XmlDocument
                If String.IsNullOrEmpty(strResult) = False Then
                    tempdoc.LoadXml(strResult)
                End If
                If tempdoc.DocumentElement.Name.ToString.Equals("POPUp") = True Then
                    MyBase.AddErrorMessage(tempdoc.DocumentElement.InnerText & " - cannot be added until all data has been entered")
                    Me.SaveBookedProductDetails(False)
                    Exit Sub
                Else
                    If tempdoc.DocumentElement.Name.ToString.Equals("MATCH") Then
                        MyBase.AddErrorMessage(tempdoc.DocumentElement.InnerText)
                    ElseIf tempdoc.DocumentElement.Name.ToString.Equals("Root") Then
                        Dim qstring As String = QS_BOOKING '& "&hdBookingId=" & Request.QueryString("hdBookingId")
                        If tempdoc.DocumentElement.SelectSingleNode("Error").InnerText.Trim.Equals(String.Empty) Then
                            ' no error
                            If String.IsNullOrEmpty(Request.QueryString("sTxtSearch")) Then
                                qstring = qstring & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId") & "&msgToShow=" & tempdoc.DocumentElement.SelectSingleNode("Msg").InnerText & "&sTxtSearch="
                            Else
                                qstring = qstring & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId") & "&msgToShow=" & tempdoc.DocumentElement.SelectSingleNode("Msg").InnerText & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString()
                            End If

                        Else
                            ''qstring = qstring & "&hdRentalId=" & Request.QueryString("hdRentalId") & "&errToShow=" & tempdoc.DocumentElement.SelectSingleNode("Msg").InnerText & " " & tempdoc.DocumentElement.SelectSingleNode("Error").InnerText & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString()
                            If String.IsNullOrEmpty(Request.QueryString("sTxtSearch")) Then
                                qstring = qstring & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId") & _
                                        "&msgToShow=" & tempdoc.DocumentElement.SelectSingleNode("Msg").InnerText & tempdoc.DocumentElement.SelectSingleNode("Error").InnerText.Trim & "&sTxtSearch="
                            Else
                                qstring = qstring & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId") & _
                                        "&msgToShow=" & tempdoc.DocumentElement.SelectSingleNode("Msg").InnerText & tempdoc.DocumentElement.SelectSingleNode("Error").InnerText.Trim & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString()
                            End If

                        End If

                        Dim index As Integer = qstring.IndexOf("Source")
                        If index <> -1 Then
                            qstring = qstring.Substring(0, index)
                        End If
                        Response.Redirect(qstring.TrimEnd)
                    End If
                End If
            Else ' If Not String.IsNullOrEmpty(sendxml) Then
                ' nothing to save!
                ' just get back to product tab!!

                If String.IsNullOrEmpty(Request.QueryString("sTxtSearch")) Then
                    Response.Redirect(QS_BOOKING & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId") & "&sTxtSearch=")
                Else
                    Response.Redirect(QS_BOOKING & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId") & "&sTxtSearch=" & Request.QueryString("sTxtSearch").ToString())
                End If



            End If ' If Not String.IsNullOrEmpty(sendxml) Then

        Catch ex As System.Threading.ThreadAbortException
            ' do nothing
        Catch ex As Exception
            MyBase.AddErrorMessage(ex.Message)
        End Try

    End Sub

    Private Sub SaveBookedProductDetails(ByVal blnSave As Boolean)

        Try

            Dim node As XmlNode = Me.XMLDSO.DocumentElement.SelectSingleNode("BookedProducts")
            Dim gridctr As Integer = Me.gridBookedProduct.Rows.Count - 1
            Dim ctr As Integer = 0
            Dim txtbox As TextBox = Nothing
            Dim lblRate As Label = Nothing
            Dim sb As New StringBuilder
            Dim nodectr As Integer = 0


            Dim frDateControl As UserControls_DateControl
            Dim toDateControl As UserControls_DateControl

            For Each row As GridViewRow In Me.gridBookedProduct.Rows

                If (row.RowType = DataControlRowType.DataRow) AndAlso (Not String.IsNullOrEmpty(gridBookedProduct.DataKeys(row.DataItemIndex).Value)) Then

                    ''rev:mia 15july
                    ''-------------------------------------------------------------------------------------
                    txtbox = CType(row.FindControl("txtfromdate"), TextBox)
                    If txtbox IsNot Nothing Then
                        ''node.ChildNodes(nodectr).SelectSingleNode("CkoDt").InnerText = txtbox.Text
                    End If

                    frDateControl = CType(row.FindControl("frDateControl"), UserControls_DateControl)
                    toDateControl = CType(row.FindControl("toDateControl"), UserControls_DateControl)

                    If frDateControl.Date <> Date.MinValue AndAlso toDateControl.Date <> Date.MinValue Then
                        If Date.Compare(frDateControl.Date, toDateControl.Date) = 1 Then
                            frDateControl.Focus()
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Please check your Check-Out date!")
                            Exit Sub
                        End If
                    End If

                    If frDateControl IsNot Nothing Then
                        If frDateControl.Date = Date.MinValue Then
                            node.ChildNodes(nodectr).SelectSingleNode("CkoDt").InnerText = ""
                        Else
                            Dim s As String = Utility.DateDBToString(frDateControl.Date)
                            node.ChildNodes(nodectr).SelectSingleNode("CkoDt").InnerText = s ''.Substring(0, s.IndexOf(" "))
                        End If

                        ''node.ChildNodes(nodectr).SelectSingleNode("CkoDt").InnerText = Utility.DateTimeToString(frDateControl.Date, SystemCulture).Split(" "c)(0)
                    End If
                    ''-------------------------------------------------------------------------------------

                    txtbox = CType(row.FindControl("txtfromlocation"), TextBox)
                    If txtbox IsNot Nothing Then
                        node.ChildNodes(nodectr).SelectSingleNode("CkoLocCode").InnerText = txtbox.Text.ToUpper
                    End If

                    ''rev:mia 15july
                    txtbox = CType(row.FindControl("txttodate"), TextBox)
                    If txtbox IsNot Nothing Then
                        ''node.ChildNodes(nodectr).SelectSingleNode("CkiDt").InnerText = txtbox.Text
                    End If

                    If frDateControl IsNot Nothing Then

                        If toDateControl.Date = Date.MinValue Then
                            node.ChildNodes(nodectr).SelectSingleNode("CkiDt").InnerText = ""
                        Else
                            Dim s As String = Utility.DateDBToString(toDateControl.Date)
                            node.ChildNodes(nodectr).SelectSingleNode("CkiDt").InnerText = s ''.Substring(0, s.IndexOf(" "))
                        End If

                        ''node.ChildNodes(nodectr).SelectSingleNode("CkiDt").InnerText = Utility.DateTimeToString(toDateControl.Date, SystemCulture).Split(" "c)(0)
                    End If

                    ' 12.1.8 - Shoel - Fix for issue where FERRYVEH is not added with a checkin time thats before check out time (dates are correct)
                    ' if Cko Date and Cki Date are same, then set the times also equal
                    ''** commented because it's not required
                    '*** RKS: 20-APR-2011 @01:51pm - problem with 1 day Extension - changing hire period to zero and also making call DVASS.
                    'If node.ChildNodes(nodectr).SelectSingleNode("CkoDt").InnerText = node.ChildNodes(nodectr).SelectSingleNode("CkiDt").InnerText Then
                    'node.ChildNodes(nodectr).SelectSingleNode("CkiTm").InnerText = node.ChildNodes(nodectr).SelectSingleNode("CkoTm").InnerText
                    'End If

                    ' 12.1.8 - Shoel - Fix for issue where FERRYVEH is not added with a checkin time thats before check out time (dates are correct)


                    txtbox = CType(row.FindControl("txttolocation"), TextBox)
                    If txtbox IsNot Nothing Then
                        node.ChildNodes(nodectr).SelectSingleNode("CkiLocCode").InnerText = txtbox.Text.ToUpper
                    End If

                    txtbox = CType(row.FindControl("txtRATE"), TextBox)
                    If txtbox IsNot Nothing Then
                        node.ChildNodes(nodectr).SelectSingleNode("Rate").InnerText = txtbox.Text
                    End If

                    txtbox = CType(row.FindControl("txtQTY"), TextBox)
                    If txtbox IsNot Nothing Then
                        node.ChildNodes(nodectr).SelectSingleNode("Qty").InnerText = txtbox.Text
                    End If


                    lblRate = CType(row.FindControl("lblBookingRef"), Label)
                    If lblRate IsNot Nothing Then
                        If String.IsNullOrEmpty(lblRate.Text) = True Then
                            node.ChildNodes(nodectr).SelectSingleNode("BookingRef").InnerText = String.Empty
                        Else
                            node.ChildNodes(nodectr).SelectSingleNode("BookingRef").InnerText = lblRate.Text
                        End If
                    End If

                End If

                If (node.ChildNodes(nodectr).SelectSingleNode("BpdId").InnerText = "") AndAlso (node.ChildNodes(nodectr).SelectSingleNode("AddReason").InnerText = "1") Then
                    dict = CType(ViewState("oldentries"), StringDictionary)
                    If dict.ContainsKey(node.ChildNodes(nodectr).SelectSingleNode("PrdShortName").InnerText) = True Then
                        With sb
                            .Append("<Product>")
                            .AppendFormat("<PrdShortName>{0}</PrdShortName>", node.ChildNodes(nodectr).SelectSingleNode("PrdShortName").InnerText)
                            .AppendFormat("<SapId>{0}</SapId>", node.ChildNodes(nodectr).SelectSingleNode("SapId").InnerText)
                            .AppendFormat("<SeqId>{0}</SeqId>", node.ChildNodes(nodectr).SelectSingleNode("SeqId").InnerText)
                            .AppendFormat("<Reason>{0}</Reason>", node.ChildNodes(nodectr).SelectSingleNode("Reason").InnerText)
                            .Append("</Product>")
                        End With
                    End If

                End If
                nodectr = nodectr + 1
            Next


            If Not String.IsNullOrEmpty(sb.ToString) Then
                Dim temp As String = sb.ToString
                sb = Nothing : sb = New StringBuilder
                sb.AppendFormat("<AddRes>{0}</AddRes>", temp)

                temp = sb.ToString
                sb = Nothing : sb = New StringBuilder
                litJS.Text = temp
                DisableButtons(False)
                Dim xmltemp As New XmlDocument
                xmltemp.LoadXml(temp)

                ''rev:mia oct.3
                Dim tempnodes As XmlNodeList = xmltemp.SelectNodes("AddRes/Product")
                Dim productreason As String = ""
                For Each tempnode As XmlNode In tempnodes
                    If tempnode("PrdShortName") IsNot Nothing Then
                        If Not String.IsNullOrEmpty(tempnode("PrdShortName").InnerText) Then
                            productreason = productreason & tempnode("PrdShortName").InnerText & ", "
                        End If
                    End If
                Next
                If productreason.EndsWith(", ") Then
                    productreason = productreason.Remove(productreason.LastIndexOf(","))
                End If
                lblproductreason.Text = productreason

                ' shoel : 25.2.9 : Fix for products that have "Prompt for Rate" AND "Prompt for Reason"
                ' Save the current doc to viewstate for later use!
                Me.xmlDsoString = XMLDSO.OuterXml
                ' shoel : 25.2.9 : Fix for products that have "Prompt for Rate" AND "Prompt for Reason"

                Exit Sub
            End If

            Call Me.SaveAllDetails(blnSave)

        Catch ex As System.Threading.ThreadAbortException
            ' do nothing
        Catch ex As Exception
            MyBase.AddErrorMessage(ex.Message)
        End Try


    End Sub


    Private Sub sortXMLforCustomer(ByRef xSourceDSO As XmlDocument, ByVal sbasenodename As String, ByVal ssortnode As String, ByVal ssortdata As String())
        Dim xpath As XPath.XPathNavigator = xSourceDSO.CreateNavigator
        Dim transform As New Xsl.XslCompiledTransform
        Dim xslpath As String = Server.MapPath("~/Booking/xsl/BookedProductDetails.xsl")


        transform.Load(xslpath)
        Dim sb As New StringBuilder
        Dim writer As XmlWriter = XmlWriter.Create(sb, transform.OutputSettings)

        transform.Transform(xpath, Nothing, writer)
        Dim strText As String = sb.ToString
        strText = strText.Remove(0, strText.IndexOf(">") + 1)
        sb.Remove(0, sb.Length)
        sb.Append(strText)
        xSourceDSO.LoadXml(sb.ToString)
    End Sub

    Private Sub LoadData()
        'If Not Me.XMLDSO.DocumentElement.SelectSingleNode("DispFlag") Is Nothing Then
        '    If Me.XMLDSO.DocumentElement.SelectSingleNode("DispFlag").InnerText = "0" Then
        '        Response.Redirect(QS_BOOKING)
        '    End If
        'End If

        Me.lblAgent.Text = Me.XMLPopupdata.DocumentElement.SelectSingleNode("Hirer").InnerText
        Me.Lblhirer.Text = Me.XMLPopupdata.DocumentElement.ChildNodes(10).InnerText
        Me.LblStatus.Text = Me.XMLPopupdata.DocumentElement.ChildNodes(12).InnerText
        Dim ds As New DataSet
        Dim temp As String = XMLPopupdata.OuterXml
        temp = temp.Remove(0, 8)
        temp = temp.Remove(temp.Length - 9)
        temp = "<Rentals>" & temp & "</Rentals>"
        ds.ReadXml(New XmlTextReader(temp, System.Xml.XmlNodeType.Document, Nothing))
        repBookedProducts.DataSource = ds
        repBookedProducts.DataBind()


        Me.LoadGrid()

        If Not Me.XMLDSO.DocumentElement.SelectSingleNode("DispFlag") Is Nothing Then
            If Me.XMLDSO.DocumentElement.SelectSingleNode("DispFlag").InnerText = "0" Then
                Me.SaveBookedProductDetails(True)
                Response.Redirect(QS_BOOKING & "&hdBookingId=" & Request.QueryString("hdBookingId") & "&hdRentalId=" & Request.QueryString("hdRentalId"))
            End If
        End If


    End Sub

    Private Sub LoadGrid()
        Dim xmlstring As String = Me.XMLDSO.OuterXml
        Dim ds As New DataSet
        ds.ReadXml(New XmlTextReader(xmlstring, System.Xml.XmlNodeType.Document, Nothing))
        ''rev:mia dec23
        If ds.Tables("BookedProduct") Is Nothing Then Exit Sub
        gridBookedProduct.DataSource = ds.Tables("BookedProduct")
        gridBookedProduct.DataBind()
        ViewState("product") = ds.Tables("BookedProduct").Rows(ds.Tables("BookedProduct").Rows.Count - 1)("PrdShortName")

        
        ''rev:mia oct.6-new code
        Dim dview As DataView = ds.Tables("BookedProduct").DefaultView
        dview.RowFilter = "AddReason=1"

        Dim strReason As String = ""
        Dim intCtr As Integer = 0

        dict.Clear()
        For Each row As DataRowView In dview
            If String.IsNullOrEmpty(row("BpdId")) Then
                intCtr = intCtr + 1
                strReason = strReason & row("PrdShortName") & "|,"
                dict.Add(row("PrdShortName"), row("BpdId"))
            End If
        Next
        ViewState("oldentries") = dict

        If strReason.EndsWith(",") Then
            strReason = strReason.Remove(strReason.LastIndexOf(","))
        End If

        If intCtr > 0 Then
            Me.BookingProductsReasons1.NoOfLabelAndReasonsToDisplay = intCtr
            Me.BookingProductsReasons1.ProductAndReasonValues = strReason
        End If

        ' Added by Shoel : 16.10.8
        LoadProductDependentRatesGrid()
    End Sub

    Sub CheckForProductsBeingDeleted()
        For Each oNode As XmlNode In XMLdom.DocumentElement.ChildNodes
            If Not IsNothing(oNode.SelectSingleNode("Cancel")) AndAlso oNode.SelectSingleNode("Cancel").InnerText = 1 Then
                SomethingIsDeleted = True
                Exit Sub
            End If
        Next
    End Sub

#End Region

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim sURL As String = String.Empty
        Try

            Dim strParam() As String

            If Not Page.IsPostBack Then
                DisableButtons(True)
                If Not String.IsNullOrEmpty(Request.QueryString("funCode")) Then
                    Dim qs As String = Request.QueryString("funCode")

                    If qs.Equals("BPDDETAILLIS") Then

                        
                        strParam = CType(Session(SESSION_saveForDetail), String).Split("|")

                        Me.RentalID = strParam(0)
                        Me.HiddenRentalID = strParam(0)
                        Me.HiddenBookingID = strParam(4)

                        ''rev:mia 02Feb2015 - slot
                        Dim intParamlength As Integer = strParam.Length - 1
                        If intParamlength = 6 Then
                            SlotId = strParam(5)
                            SlotDescription = strParam(6)
                        End If


                        Me.Result = IIf(strParam(1) = 1, True, False) ''Boolean.Parse(strParam(1))
                        Dim paramXML As String = strParam(2)
                        Me.BookingID = strParam(3)
                        paramXML = paramXML.Replace("<data>", "<Data>")
                        paramXML = paramXML.Replace("</data>", "</Data>")
                        Me.XMLdom.LoadXml(paramXML)
                        Litxmldom.Text = Server.HtmlEncode(XMLdom.OuterXml)

                        ' Shoel - 3.2.9 - To ensure deletes trigger SSTransToFinance
                        SomethingIsDeleted = False
                        CheckForProductsBeingDeleted()
                        ' Shoel - 3.2.9 - To ensure deletes trigger SSTransToFinance

                        Dim tempxml As String = Me.LoadForDetails

                        Try
                            XMLReceive.LoadXml(tempxml)
                            LitxmlReceive.Text = Server.HtmlEncode(Me.XMLReceive.OuterXml)


                            sURL = QS_BOOKING & "&hdBookingId=" & Me.BookingID & "&hdRentalId=" & Me.RentalID

                            If Not XMLReceive.SelectSingleNode("Data/Errors") Is Nothing Then
                                If Not String.IsNullOrEmpty(XMLReceive.SelectSingleNode("Data/Errors").InnerText) Then

                                    Dim errValue As String = XMLReceive.DocumentElement.SelectSingleNode("Errors").InnerText
                                    ''reV:mia oct.18
                                    If errValue.IndexOf("OVABCDEF") <> -1 Then
                                        aryError = errValue.Split("*")
                                        ViewState("FlagToCancel") = True
                                        Me.SupervisorOverrideControl1.Show(Text:=aryError(1))

                                    Else
                                        Session("Err") = XMLReceive.SelectSingleNode("Data/Errors").InnerText
                                        sURL = sURL & "&errToShow=" & XMLReceive.SelectSingleNode("Data/Errors").InnerText
                                        Response.Redirect(sURL)
                                    End If
                                End If
                            End If

                            If Not XMLReceive.DocumentElement.SelectSingleNode("Error/ErrStatus") Is Nothing Then
                                If XMLReceive.DocumentElement.SelectSingleNode("Error/ErrStatus").InnerText.ToUpper().Equals("TRUE") Then
                                    ' theres some error
                                    sURL = sURL & "&errToShow=" & XMLReceive.DocumentElement.SelectSingleNode("Error/ErrDescription").InnerText
                                    Response.Redirect(sURL)
                                End If
                            End If

                        Catch ex As System.Threading.ThreadAbortException

                        Catch ex As Exception
                            Session("Err") = tempxml
                            Session(SESSION_saveForDetail) = paramXML
                            Response.Redirect(QS_BOOKING & "&hdBookingId=" & IIf(Request("hdBookingId") Is Nothing, String.Empty, Request("hdBookingId")) & "&hdRentalId=" & IIf(Request("hdRentalId") Is Nothing, String.Empty, Request("hdRentalId")))
                        End Try


                        If String.IsNullOrEmpty(tempxml) Then
                            MyBase.SetShortMessage(AuroraHeaderMessageType.Information, tempxml)
                            Exit Sub
                        End If

                        'If XMLReceive.DocumentElement.SelectSingleNode("Errors").HasChildNodes = True Then

                        If XMLReceive.DocumentElement.SelectSingleNode("Errors") IsNot Nothing Then
                            If XMLReceive.DocumentElement.SelectSingleNode("Errors").HasChildNodes = True Then
                                disablePanel(False)
                                Dim errValue As String = XMLReceive.DocumentElement.SelectSingleNode("Errors").InnerText
                                If errValue.IndexOf("OVABCDEF") <> -1 Then
                                    Dim aryError() As String = errValue.Split("*")
                                    Me.SupervisorOverrideControl1.Show(Text:=aryError(1))
                                End If
                            Else
                                ''rev:mia 02Feb2015 - slot
                                If (Not String.IsNullOrEmpty(SlotId) AndAlso Not String.IsNullOrEmpty(SlotDescription)) Then
                                    Aurora.Booking.Services.Rental.SaveRentalSlot(RentalID, SlotId, SlotDescription)
                                End If
                                Me.ParseBookedValue(XMLReceive)
                                Me.LoadData()

                                ''---------------------------------------------------------
                                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                                ''---------------------------------------------------------
                                'If Session(SESSION_xmlDso) IsNot Nothing Then
                                '    Session.Remove(SESSION_xmlDso)
                                'End If
                                ''Session(SESSION_xmlDso) = Me.XMLDSO
                                ''Session(SESSION_xmlOrig) = Me.XMLOrig
                                ''Session(SESSION_xmlpopup) = Me.XMLPopupdata
                                ''Session(SESSION_hashproperties) = Me.hashproperties

                                Me.xmlDsoString = Me.XMLDSO.OuterXml
                                Me.xmlOrigString = Me.XMLOrig.OuterXml
                                Me.xmlPopupString = Me.XMLPopupdata.OuterXml
                                ''---------------------------------------------------------

                                


                            End If

                        Else
                            ''rev:mia 02Feb2015 - slot
                            If (Not String.IsNullOrEmpty(SlotId) AndAlso Not String.IsNullOrEmpty(SlotDescription)) Then
                                Aurora.Booking.Services.Rental.SaveRentalSlot(RentalID, SlotId, SlotDescription)
                            End If
                            Me.ParseBookedValue(XMLReceive)
                            Me.LoadData()

                            ''---------------------------------------------------------
                            ''rev:mia jan 30 2009 -- Changes from session to viewstate
                            ''---------------------------------------------------------
                            'If Session(SESSION_xmlDso) IsNot Nothing Then
                            '    Session.Remove(SESSION_xmlDso)
                            'End If
                            ''Session(SESSION_xmlDso) = Me.XMLDSO
                            ''Session(SESSION_xmlOrig) = Me.XMLOrig
                            ''Session(SESSION_xmlpopup) = Me.XMLPopupdata
                            ''Session(SESSION_hashproperties) = Me.hashproperties

                            Me.xmlDsoString = Me.XMLDSO.OuterXml
                            Me.xmlOrigString = Me.XMLOrig.OuterXml
                            Me.xmlPopupString = Me.XMLPopupdata.OuterXml
                            ''---------------------------------------------------------

                            
                        End If

                    End If
                End If
            Else


                ''---------------------------------------------------------
                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                ''---------------------------------------------------------
                'If Session(SESSION_xmlDso) IsNot Nothing Then
                '    Me.XMLDSO = CType(Session(SESSION_xmlDso), XmlDocument)
                'End If
                If Not String.IsNullOrEmpty(Me.xmlDsoString) Then
                    Me.XMLDSO = New XmlDocument
                    Me.XMLDSO.LoadXml(Me.xmlDsoString)
                End If

                'If Session(SESSION_xmlOrig) IsNot Nothing Then
                '    Me.XMLOrig = CType(Session(SESSION_xmlOrig), XmlDocument)
                'End If
                If Not String.IsNullOrEmpty(Me.xmlOrigString) Then
                    Me.XMLOrig = New XmlDocument
                    Me.XMLOrig.LoadXml(Me.xmlOrigString)
                End If

                'If Session(SESSION_xmlpopup) IsNot Nothing Then
                '    Me.XMLPopupdata = CType(Session(SESSION_xmlpopup), XmlDocument)
                'End If
                If Not String.IsNullOrEmpty(Me.xmlPopupString) Then
                    Me.XMLPopupdata = New XmlDocument
                    Me.XMLPopupdata.LoadXml(Me.xmlPopupString)
                End If

                'If Session(SESSION_hashproperties) IsNot Nothing Then
                '    Me.hashproperties = CType(Session(SESSION_hashproperties), Hashtable)
                'End If
                ''---------------------------------------------------------

                If Not String.IsNullOrEmpty(litJS.Text) Then
                    XMLaddRes.LoadXml(litJS.Text)
                End If

            End If
            Me.SetValueLink(Me.BookingID, "")

        Catch exThread As System.Threading.ThreadAbortException
            'Response.Redirect(sURL)
            ' 13.2.9 - Shoel - Do nothing
        Catch ex As Exception
            ''MyBase.AddErrorMessage(ex.Message)
            Response.Redirect(sURL)
        End Try

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        '' clear the disputed product data - its as good as a fresh save
        'DisputedProductSapId = String.Empty
        'DisputedProductSequenceId = String.Empty
        'DisputedProductPreviouslySelectedBooleanFlag = False
        '' clear the disputed product data - its as good as a fresh save

        Dim validator As BaseValidator
        For Each validator In Page.GetValidators("btnNext")
            If Not validator.IsValid Then
                validator.FindControl(validator.ControlToValidate).Focus()
                If Not String.IsNullOrEmpty(validator.ErrorMessage) Then
                    MyBase.SetShortMessage(AuroraHeaderMessageType.Information, validator.ErrorMessage)
                End If
                Exit Sub
            End If
        Next
        Me.SaveBookedProductDetails(True)
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        SaveBookedProductDetailsWithReasons("")
    End Sub

    Protected Sub btnOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOK.Click
        SaveBookedProductDetailsWithReasons(litJS.Text)
    End Sub

    Public Sub supervisorOverrideControl_PostBack(ByVal sender As Object, ByVal IsOkButton As Boolean, ByVal param As String) Handles SupervisorOverrideControl1.PostBack
        Dim sReturnMsg As String = ""
        If IsOkButton Then

            sReturnMsg = Aurora.Common.Service.CheckUser(SupervisorOverrideControl1.Login, SupervisorOverrideControl1.Password, "ORIDCKOCKI")
            If (sReturnMsg) = "True" Then
                Me.Result = False

                XMLdom.LoadXml(Server.HtmlDecode(Litxmldom.Text))
                Dim tempxml As String = Me.LoadForDetails
                XMLReceive.LoadXml(tempxml)
                LitxmlReceive.Text = Server.HtmlEncode(Me.XMLReceive.OuterXml)
                Me.ParseBookedValue(XMLReceive)
                Me.LoadData()
                disablePanel(True)

                ''---------------------------------------------------------
                ''rev:mia jan 30 2009 -- Changes from session to viewstate
                ''---------------------------------------------------------
                'If Session(SESSION_xmlDso) IsNot Nothing Then
                '    Session.Remove(SESSION_xmlDso)
                'End If
                ''Session(SESSION_xmlDso) = Me.XMLDSO
                ''Session(SESSION_xmlOrig) = Me.XMLOrig
                ''Session(SESSION_xmlpopup) = Me.XMLPopupdata
                ''Session(SESSION_hashproperties) = Me.hashproperties

                Me.xmlDsoString = Me.XMLDSO.OuterXml
                Me.xmlOrigString = Me.XMLOrig.OuterXml
                Me.xmlPopupString = Me.XMLPopupdata.OuterXml
                ''---------------------------------------------------------

                ViewState("FlagToCancel") = False
            Else
                Dim qsBookingID As String = ""
                Dim qsRentalID As String = ""
                If Not Request.QueryString("hdBookingId") Is Nothing Then
                    qsBookingID = "&hdBookingId=" & Request.QueryString("hdBookingId").ToString()
                End If
                If Not Request.QueryString("hdRentalId") Is Nothing Then
                    qsRentalID = "&hdRentalId=" & Request.QueryString("hdRentalId").ToString()
                End If
                Session("Err") = sReturnMsg
                Response.Redirect(QS_BOOKING & qsBookingID & qsRentalID)
            End If
        Else

            ''rev: mia oct.18
            Dim qsBookingID As String = ""
            Dim qsRentalID As String = ""
            If Not Request.QueryString("hdBookingId") Is Nothing Then
                qsBookingID = "&hdBookingId=" & Request.QueryString("hdBookingId").ToString()
            End If
            If Not Request.QueryString("hdRentalId") Is Nothing Then
                qsRentalID = "&hdRentalId=" & Request.QueryString("hdRentalId").ToString()
            End If
            Session("Err") = aryError(1)
            Response.Redirect(QS_BOOKING & qsBookingID & qsRentalID)
        End If

    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim btn As Button = SupervisorOverrideControl1.FindControl("cancelButton")

        Dim qsBookingID As String = ""
        Dim qsRentalID As String = ""
        If Not Request.QueryString("hdBookingId") Is Nothing Then
            qsBookingID = "&hdBookingId=" & Request.QueryString("hdBookingId").ToString()
        End If
        If Not Request.QueryString("hdRentalId") Is Nothing Then
            qsRentalID = "&hdRentalId=" & Request.QueryString("hdRentalId").ToString()
        End If

        btn.OnClientClick = "window.location = '" & QS_BOOKING & qsBookingID & qsRentalID & "'"

    End Sub

    Public Sub LoadProductDependentRatesGrid()
        Dim sbProductDependentRateXML As StringBuilder = New StringBuilder



        For Each oNode As XmlNode In Me.XMLDSO.DocumentElement.SelectNodes("BookedProducts/BookedProduct")
            If oNode.SelectSingleNode("PrrIdsList").InnerXml <> "*" Then
                ' this has PRR info!

                sbProductDependentRateXML.Append("  <PrrProduct>")
                sbProductDependentRateXML.Append("      <PrdShortName>" & oNode.SelectSingleNode("PrdShortName").InnerText & "</PrdShortName>")
                sbProductDependentRateXML.Append("      <SeqId>" & oNode.SelectSingleNode("SeqId").InnerText & "</SeqId>")

                '<PersonRate>
                '  <PrrId>6E81C8F2-0CE1-410C-8C3A-59B8A30DA466</PrrId> 
                '  <Code>ADULT</Code> 
                '  <Rate>76.00</Rate> 
                '  <RateMod>0</RateMod> 
                '  <Qty>0.0000</Qty> 
                '  <QtyMod>1</QtyMod> 
                '</PersonRate>


                For Each oPrrNode As XmlNode In oNode.SelectNodes("PrrIdsList/PersonRate")


                    Dim sPrefix As String = oPrrNode.SelectSingleNode("Code").InnerText.ToUpper() ' ie ADULT, CHILD, INFANT

                    sbProductDependentRateXML.Append("  <" & sPrefix & "_PrrId>" & oPrrNode.SelectSingleNode("PrrId").InnerText.Trim() & "</" & sPrefix & "_PrrId>")
                    sbProductDependentRateXML.Append("  <" & sPrefix & "_Rate>" & oPrrNode.SelectSingleNode("Rate").InnerText.Trim() & "</" & sPrefix & "_Rate>")
                    sbProductDependentRateXML.Append("  <" & sPrefix & "_RateMod>" & oPrrNode.SelectSingleNode("RateMod").InnerText.Trim() & "</" & sPrefix & "_RateMod>")
                    sbProductDependentRateXML.Append("  <" & sPrefix & "_Qty>" & oPrrNode.SelectSingleNode("Qty").InnerText.Trim() & "</" & sPrefix & "_Qty>")
                    sbProductDependentRateXML.Append("  <" & sPrefix & "_QtyMod>" & oPrrNode.SelectSingleNode("QtyMod").InnerText.Trim() & "</" & sPrefix & "_QtyMod>")

                Next
                sbProductDependentRateXML.Append("  </PrrProduct>")
            End If
        Next

        If sbProductDependentRateXML.ToString().Trim().Equals(String.Empty) Then
            ' no Prrs :D
            Exit Sub
        End If

        sbProductDependentRateXML.Insert(0, "<PrrProducts>")
        sbProductDependentRateXML.Append("</PrrProducts>")

        Dim oTempXML As XmlDocument = New XmlDocument
        oTempXML.LoadXml(sbProductDependentRateXML.ToString())

        Dim dstProductDependentRates As DataSet = New DataSet
        dstProductDependentRates.ReadXml(New XmlNodeReader(oTempXML.DocumentElement))

        rptPersonDependentRates.DataSource = dstProductDependentRates.Tables(0)
        rptPersonDependentRates.DataBind()

        divPersonDependentRates.Style("display") = "block"

    End Sub

    Sub ShowConfirmationMessage(ByVal ProductName As String, ByVal FieldsThatAreMissingData As String)
        FieldsThatAreMissingData = FieldsThatAreMissingData.Substring(0, FieldsThatAreMissingData.Length - 2)
        cnfConfirmation.Text = "<b>" & ProductName & "</b> - cannot be added until all data has been entered. <br/> Please enter the " & FieldsThatAreMissingData & "."
        cnfConfirmation.Show()
    End Sub

    Protected Sub cnfConfirmation_PostBack(ByVal sender As Object, ByVal leftButton As Boolean, ByVal rightButton As Boolean, ByVal param As String) Handles cnfConfirmation.PostBack
        If leftButton Then
            ' enter data and then re-do
            DisputedProductSapId = String.Empty
            DisputedProductSequenceId = String.Empty
            DisputedProductPreviouslySelectedBooleanFlag = False
            Exit Sub
        End If
        If rightButton Then
            ' skip the product that has missing data!
            btnNext_Click(New Object, New EventArgs)
        End If
    End Sub

#Region "rev:mia jan 30 2009 -- Changes from session to viewstate"
    ''---------------------------------------------------------
    ''rev:mia jan 30 2009 -- Changes from session to viewstate
    ''-----------------------------------   ----------------------

    ''replacement for SESSION_xmlDso
    Private Property xmlDsoString() As String
        Get
            Return ViewState("xmlDsoString")
        End Get
        Set(ByVal value As String)
            ViewState("xmlDsoString") = value
        End Set
    End Property

    ''replacement for SESSION_xmlOrig
    Private Property xmlOrigString() As String
        Get
            Return ViewState("xmlOrigString")
        End Get
        Set(ByVal value As String)
            ViewState("xmlOrigString") = value
        End Set
    End Property

    ''replacement for SESSION_xmlpopup
    Private Property xmlPopupString()
        Get
            Return ViewState("xmlPopupString")
        End Get
        Set(ByVal value)
            ViewState("xmlPopupString") = value
        End Set
    End Property
#End Region

#Region "rev:mia 02Feb2015-addition of slot"
    Private Property SlotId As String
        Get
            Return ViewState("_SlotId")
        End Get
        Set(value As String)
            ViewState("_SlotId") = value
        End Set
    End Property

    Private Property SlotDescription As String
        Get
            Return ViewState("_SlotDescription")
        End Get
        Set(value As String)
            ViewState("_SlotDescription") = value
        End Set
    End Property

#End Region

End Class
