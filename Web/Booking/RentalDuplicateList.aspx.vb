Imports Aurora.Common
Imports System.xml
Imports System.Data.SqlClient
Imports System.Data
Imports System.Text



<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.DuplicateRentals)> _
Partial Class Booking_DuplicateRentals
    Inherits AuroraPage

#Region " Constant and Variables"
    ''rev:mia Feb.16
    Private Const QS_RS_RNTDUPLST As String = "RS-RNTDUPLST"
    Private Const QS_RS_VEHREQRES As String = "VehicleRequestResultList.aspx?funCode=RS-VEHREQRES"
    Private Const QS_RS_VEHREQMGT As String = "VehicleRequestMgt.aspx?funCode=RS-VEHREQMGT"
    Private Const QS_RS_VEHREQMGTNBR As String = "VehicleRequestMgt.aspx"
    Private SESSION_VehicleRequestMgt As String ' = "RS-VEHREQMGT" & MyBase.UserCode

    ''rev:mia feb.17
    Protected xmlHeader As New XmlDocument
    Protected xmlVhrHeader As New XmlDocument
    Protected xmlBooHeader As New XmlDocument
    Protected xmlDuplicateRental As New XmlDocument
    Protected xmlDetails As New XmlDocument
    Protected xmlDoc As New XmlDocument

    Protected SESSION_rentalduplicatelist As String '= "RS-RNTDUPLST" & MyBase.UserCode
    Protected SESSION_xmlHeader As String = "xmlHeader"
    Protected SESSION_xmlVhrHeader As String = "xmlVhrHeader"
    Protected SESSION_xmlBooHeader As String = "xmlBooHeader"
    Protected SESSION_xmlDuplicateRental As String = "xmlDuplicateRental"
    Protected SESSION_xmlDetails As String = "xmlDetails"
    Protected SESSION_xmlDoc As String = "xmlDoc"

    Protected hashXMLdocument As New Hashtable

    ' Private SESSION_VehicleRequestMgt As String = "RS-VEHREQMGT" & Me.UserCode
    Private dictObject As New StringDictionary
    Private unset As Boolean = False
#End Region

#Region " Sub"

    ''rev:mia Feb.17
    Sub SplitData()
        Me.LoadxmlVhrHeader()
        Me.LoadXMLBooHeader()
        Me.LoadXMLdetails()
    End Sub

    Sub LoadxmlVhrHeader()
        If Me.xmlVhrHeader Is Nothing Then
            Me.xmlVhrHeader = New XmlDocument
        End If
        Me.xmlVhrHeader.LoadXml(Me.xmlDuplicateRental.SelectSingleNode("//Data/VhrHeader").OuterXml)
    End Sub

    Sub LoadXMLBooHeader()
        If Me.xmlBooHeader Is Nothing Then
            Me.xmlBooHeader = New XmlDocument
        End If
        Me.xmlBooHeader.LoadXml(Me.xmlDuplicateRental.SelectSingleNode("//Data/BooHeader").OuterXml)

        Dim booid As String = ""
        If Not Me.xmlBooHeader.SelectSingleNode("BooId") Is Nothing Then
            booid = Me.xmlBooHeader.SelectSingleNode("BooId").InnerText
        End If

        If Not String.IsNullOrEmpty(booid) Then
            Me.LoadXMLheader(1)
        Else
            Me.LoadXMLheader(0)
        End If
    End Sub

    Sub LoadXMLheader(ByVal mode As Integer)
        If Me.xmlHeader Is Nothing Then
            Me.xmlHeader = New XmlDocument
        End If
        If mode = 1 Then
            Me.xmlHeader.LoadXml(Me.xmlBooHeader.OuterXml)
        Else
            Me.xmlHeader.LoadXml(Me.xmlVhrHeader.OuterXml)
        End If

    End Sub

    Sub LoadXMLdetails()
        If Me.xmlDetails Is Nothing Then
            Me.xmlDetails = New XmlDocument
        End If
        Me.xmlDetails.LoadXml(Me.xmlDuplicateRental.SelectSingleNode("//Data/Details").OuterXml)
    End Sub

    Sub AddToSession()
        If Session(SESSION_rentalduplicatelist) IsNot Nothing Then
            Session.Remove(SESSION_rentalduplicatelist)
        End If
        If hashXMLdocument IsNot Nothing Then
            hashXMLdocument.Clear()
        End If
        hashXMLdocument(SESSION_xmlHeader) = Me.xmlHeader
        hashXMLdocument(SESSION_xmlVhrHeader) = Me.xmlVhrHeader
        hashXMLdocument(SESSION_xmlBooHeader) = Me.xmlBooHeader
        hashXMLdocument(SESSION_xmlDuplicateRental) = Me.xmlDuplicateRental
        hashXMLdocument(SESSION_xmlDetails) = Me.xmlDetails
        hashXMLdocument(SESSION_xmlDoc) = Me.xmlDoc
        Session.Add(SESSION_rentalduplicatelist, hashXMLdocument)
    End Sub

    Sub RemoveSessionForNBR()
        If Session(SESSION_VehicleRequestMgt) IsNot Nothing Then
            Session.Remove(SESSION_VehicleRequestMgt)
        End If
    End Sub

    Sub BindData(ByVal xmldoc As XmlDocument)
        Dim ds As DataSet = New DataSet
        Try

        
            ds.ReadXml(New XmlNodeReader(xmldoc))
            GridView1.DataSource = ds.Tables("Rental")
            GridView1.DataBind()

            If ds.Tables IsNot Nothing Then
                If ds.Tables(0).Rows.Count = 0 Then
                    Response.Redirect(QS_RS_VEHREQRES)
                End If

                Me.txtHirer.Value = ds.Tables("VhrHeader").Rows(0).Item("Hirer").ToString.ToUpper
                Me.txtagent.Value = ds.Tables("VhrHeader").Rows(0).Item("Agent").ToString.ToUpper
            Else
                Response.Redirect(QS_RS_VEHREQRES)
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Sub GetFromSession()
        If Session(SESSION_rentalduplicatelist) IsNot Nothing Then
            hashXMLdocument = CType(Session(SESSION_rentalduplicatelist), Hashtable)
        Else
            Exit Sub
        End If
        Me.xmlHeader = CType(hashXMLdocument(SESSION_xmlHeader), XmlDocument)
        Me.xmlVhrHeader = CType(hashXMLdocument(SESSION_xmlVhrHeader), XmlDocument)
        Me.xmlBooHeader = CType(hashXMLdocument(SESSION_xmlBooHeader), XmlDocument)
        Me.xmlDuplicateRental = CType(hashXMLdocument(SESSION_xmlDuplicateRental), XmlDocument)
        Me.xmlDoc = CType(hashXMLdocument(SESSION_xmlDoc), XmlDocument)

        hashXMLdocument(SESSION_xmlDetails) = Me.xmlDetails
    End Sub

    
    Sub GetSessionForMBR()
        If Not String.IsNullOrEmpty(Request.QueryString("funcode")) Then
            If Request.QueryString("funcode").Equals("RS-RNTDUPLST") Then
                dictObject = CType(Session(SESSION_VehicleRequestMgt), StringDictionary)
                If dictObject IsNot Nothing Then
                    Me.BKRid = dictObject("bkrid")
                    Me.BooID = dictObject("booid")
                    lblbooid.Text = dictObject("booid")
                    Me.ButtonSelection = dictObject("butSel")
                    Me.VHRid = dictObject("vhrid")
                    Me.BPDid = dictObject("bpdId")
                    Me.hidContactID.Value = dictObject("Contact")
                End If
            End If
        End If
    End Sub

    Sub ModifySessionForMBR()

        dictObject = CType(Session(SESSION_VehicleRequestMgt), StringDictionary)
        dictObject = New StringDictionary

        If dictObject IsNot Nothing Then
            dictObject("bkrid") = Me.BKRid
            dictObject("booid") = Me.BooID ''IIf(String.IsNullOrEmpty(Me.BooID), Me.lblbooid.Text, Me.BooID)
            dictObject("butSel") = Me.ButtonSelection
            dictObject("vhrid") = Me.VHRid
            dictObject("bpdId") = Me.BPDid
            dictObject("Contact") = Me.hidContactID.Value
        End If
        Session.Remove(SESSION_VehicleRequestMgt)
        Session(SESSION_VehicleRequestMgt) = Me.dictObject
    End Sub

    Sub AddToBooking()

        Dim checked As Boolean = False
        Dim chkError As CheckBox = Nothing

        Try

        
            Dim i As Integer = 0
            Dim row As GridViewRow = Nothing
            Dim temp As String = String.Empty
            If Not Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum") Is Nothing Then
                temp = Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText
            End If
            For i = 0 To Me.xmlDoc.DocumentElement("Details").ChildNodes.Count - 1
                row = GridView1.Rows(i)
                chkError = CType(row.FindControl("chkError"), CheckBox)
                If chkError IsNot Nothing Then
                    If chkError.Checked = True Then
                        checked = True
                        Me.Hidbooid.Value = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooId").InnerText
                        Me.lblbooid.Text = Me.Hidbooid.Value
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooId").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooId").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("BooNum").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("AgnId").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("AgnId").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("Agent").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("Agent").InnerText
                        Me.xmlBooHeader.DocumentElement.SelectSingleNode("Hirer").InnerText = xmlDoc.DocumentElement("Details").ChildNodes(i).SelectSingleNode("Hirer").InnerText
                        Me.xmlHeader.LoadXml(Me.xmlBooHeader.OuterXml)

                        chkError.Checked = False
                        MyBase.SetValueLink((" :Booking :" & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText)
                    End If
                End If
            Next

            If checked = False Then
                MyBase.SetInformationShortMessage("Please select the booking")
            Else
                If Not String.IsNullOrEmpty(temp) Then
                    MyBase.SetInformationShortMessage("Booking # " & Me.BooID & " was linked in '" & temp & "'")
                End If
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try


    End Sub

    Sub UnsetToBooking()
        Try

        
            Dim temp As String = Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText
            Me.Hidbooid.Value = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooId").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("AgnId").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("Agent").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("Hirer").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText = ""
            Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooId").InnerText = ""
            Me.xmlHeader.LoadXml(Me.xmlBooHeader.OuterXml)

            Dim chkError As CheckBox = Nothing
            Dim row As GridViewRow = Nothing
            For i As Integer = 0 To Me.xmlDoc.DocumentElement("Details").ChildNodes.Count - 1
                row = GridView1.Rows(i)
                chkError = CType(row.FindControl("chkError"), CheckBox)
                If chkError IsNot Nothing Then
                    chkError.Checked = False
                End If
                MyBase.SetValueLink("", "")
            Next
            If Not String.IsNullOrEmpty(temp) Then
                MyBase.SetShortMessage(AuroraHeaderMessageType.Information, "Booking Request unlinked from " & temp)
            End If

        Catch ex As Exception
            MyBase.SetErrorMessage(ex.Message)
        End Try
    End Sub

    Sub ToggleButton(ByVal bln As Boolean)
        If bln = True Then
            Me.btnAddToBooking.Enabled = True
            Me.btnBrochure.Enabled = True
            Me.btnModifyBookingRequest.Enabled = True
            Me.btnNewBookingRequest.Enabled = True
            Me.btnNext.Enabled = True
            Me.btnUnset.Enabled = True
        Else
            Me.btnModifyBookingRequest.Enabled = True
            Me.btnNewBookingRequest.Enabled = True

            Me.btnAddToBooking.Enabled = False
            Me.btnBrochure.Enabled = False
            Me.btnNext.Enabled = False
            Me.btnUnset.Enabled = False
        End If
    End Sub

#End Region

#Region " Properties"

    Private Property BPDid() As String
        Get
            Return Me.Hidbpdid.Value
        End Get
        Set(ByVal value As String)
            Me.Hidbpdid.Value = value
        End Set
    End Property
    Private Property VHRid() As String
        Get
            Return Me.Hidvhrid.Value
        End Get
        Set(ByVal value As String)
            Me.Hidvhrid.Value = value
        End Set
    End Property
    Private Property ButtonSelection() As String
        Get
            Return Me.Hidbutsel.Value
        End Get
        Set(ByVal value As String)
            Me.Hidbutsel.Value = value
        End Set
    End Property
    Private Property BKRid() As String
        Get
            Return Me.Hidbkrid.Value
        End Get
        Set(ByVal value As String)
            Me.Hidbkrid.Value = value
        End Set
    End Property

    Private Property BooID() As String
        Get
            Return Me.Hidbooid.Value
        End Get
        Set(ByVal value As String)
            Me.Hidbooid.Value = value
        End Set
    End Property
#End Region

#Region " Methods"
    Protected Function ParseEvalItemToBoolean(ByVal objCurr As Object, Optional ByVal currentrequest As Object = Nothing) As Boolean
        If objCurr Is DBNull.Value Then
            Return False
        End If
        If objCurr Is Nothing Then
            Return False
        End If
        If objCurr = 0 Then
            If CType(currentrequest, String).StartsWith("Current") And objCurr = 0 Then
                Return False
            End If
            Return True
        End If
        If objCurr = 1 Then

            Return False
        End If

    End Function
#End Region

#Region " Events"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        SESSION_VehicleRequestMgt = "RS-VEHREQMGT" & MyBase.UserCode
        
        Try

        
            Me.ToggleButton(True)
            If Not Page.IsPostBack Then

                Me.GetSessionForMBR()
                unset = False

                Dim vhrId As String = Me.VHRid
                Dim booId As String = Me.BooID
                Dim bpdId As String = Me.BPDid

                Me.xmlDoc = Aurora.Common.Data.ExecuteSqlXmlSPDoc("RES_manageDupRentals", vhrId, booId, bpdId, "")
                Dim strXML As String = xmlDoc.OuterXml.Replace("^", " ")
                strXML = strXML.Replace("<data>", "<Data>")
                strXML = strXML.Replace("</data>", "</Data>")
                xmlDoc = Nothing
                xmlDoc = New XmlDocument
                xmlDoc.LoadXml(strXML)

                If Me.xmlDuplicateRental IsNot Nothing Then
                    Me.xmlDuplicateRental = New XmlDocument
                End If

                Me.xmlDuplicateRental.LoadXml(strXML)

                If (xmlDoc.SelectSingleNode("//Data/Details").ChildNodes.Count = 1) Then
                    Me.ModifySessionForMBR()
                    Response.Redirect(QS_RS_VEHREQRES)
                Else
                    Call BindData(xmlDoc)
                    Call Me.SplitData()
                    Call Me.AddToSession()

                End If

                If Not Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum") Is Nothing Then
                    If Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText <> "" Then
                        MyBase.SetValueLink((": Booking :" & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText)
                    Else
                        MyBase.SetValueLink("", "")
                    End If
                End If

            Else

                If unset = False Then
                    Me.Hidbooid.Value = Me.lblbooid.Text
                End If

                Me.GetFromSession()
                ''MyBase.SetValueLink(" : Booking " & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText, "")
                MyBase.SetValueLink((": Booking :" & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText).TrimStart, "~/Booking/Booking.aspx?activeTab=7&funcode=RS-BOKSUMMGT&hdBookingId=" & Me.xmlBooHeader.DocumentElement.SelectSingleNode("BooNum").InnerText)
            End If

        Catch ex As Exception
            MyBase.AddErrorMessage(ex.Message)
            Me.ToggleButton(False)
        End Try
    End Sub

    Protected Sub GridView1_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Try
                Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
                If rowview("BookingRental").ToString = "Current Request" Then
                    e.Row.BackColor = Drawing.Color.Ivory
                    e.Row.Font.Bold = True
                    '' e.Row.ForeColor = Drawing.Color.Blue
                End If
            Catch
            End Try

        End If


    End Sub

    Protected Sub GridView1_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView1.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim hplink As HyperLink
            If Not rowview("BookingRental").ToString = "Current Request" Then
                hplink = CType(e.Row.FindControl("hpLink"), HyperLink)
                If hplink IsNot Nothing Then
                    hplink.NavigateUrl = "Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & rowview("BooId") & "&hdRentalId=" & rowview("RntId")
                End If
                Dim rowAgentHirer As String = rowview("AgentHirer")
                rowAgentHirer = rowAgentHirer.Replace("^", " ")
                rowview("AgentHirer") = rowAgentHirer
            End If

        End If
    End Sub

    Protected Sub btnModifyBookingRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnModifyBookingRequest.Click
        ModifySessionForMBR()
        Response.Redirect(QS_RS_VEHREQMGT)
    End Sub

    Protected Sub btnNewBookingRequest_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNewBookingRequest.Click
        Me.RemoveSessionForNBR()
        Response.Redirect(QS_RS_VEHREQMGTNBR)
    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNext.Click
        Me.ModifySessionForMBR()
        Response.Redirect(QS_RS_VEHREQRES)
    End Sub

    Protected Sub btnUnset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnset.Click
        unset = True
        Me.UnsetToBooking()
        Me.AddToSession()
        ModifySessionForMBR()
    End Sub

    Protected Sub btnAddToBooking_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToBooking.Click
        Me.AddToBooking()
        Me.AddToSession()
        ModifySessionForMBR()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        'Dim chkCurrentCheckbox As CheckBox = Nothing
        'Dim checkbox As CheckBox = CType(sender, CheckBox)
        'Dim row As GridViewRow = CType(checkbox.NamingContainer, GridViewRow)
        'Dim gridid As String = GridView1.DataKeys(row.DataItemIndex).Value.ToString


        'For Each chkrow As GridViewRow In GridView1.Rows
        '    chkCurrentCheckbox = CType(chkrow.FindControl("chkError"), CheckBox)
        '    If chkCurrentCheckbox IsNot Nothing AndAlso chkCurrentCheckbox.Checked = True Then
        '        If GridView1.DataKeys(chkrow.DataItemIndex).Value.ToString.Equals(gridid) Then
        '            chkCurrentCheckbox.Checked = chkCurrentCheckbox.Checked
        '            If chkCurrentCheckbox.Checked = True Then
        '                AddToBooking()
        '            Else
        '                UnsetToBooking()
        '            End If
        '        Else
        '            chkCurrentCheckbox.Checked = False
        '        End If
        '    Else
        '        chkCurrentCheckbox.Checked = False
        '        If GridView1.DataKeys(chkrow.DataItemIndex).Value.ToString.Equals(gridid) Then
        '            UnsetToBooking()
        '        End If

        '    End If
        'Next
        'Me.AddToSession()
    End Sub

#End Region

End Class
