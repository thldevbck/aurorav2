﻿''Imports Aurora.Booking.Services
Imports Aurora.BookingExperiences.Service
Imports System.Data

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingExperience)> _
Partial Class Booking_BookingExperiences
    Inherits AuroraPage

#Region "Variables"

    Dim bookingId As String
    Dim rentalId As String

    Dim mode As String
    Dim dsStatic As DataSet

#End Region

#Region "Properties"

    Private Property ExpCallTktId As String
        Get
            Return CType(ViewState("ExpCallTktId"), String)
        End Get
        Set(value As String)
            ViewState("ExpCallTktId") = value
        End Set
    End Property

    Private Property StaticDataSet As DataSet
        Get
            Return CType(ViewState("StaticDataSet"), DataSet)
        End Get
        Set(value As DataSet)
            ViewState("StaticDataSet") = value
        End Set
    End Property

    Private Property ExperienceCallId As String
        Get
            Return CType(ViewState("ExpCallId"), String)
        End Get
        Set(value As String)
            ViewState("ExpCallId") = value
        End Set
    End Property

    Private Property ExpCallId As String
        Get
            Return ViewState("ExpCallId")
        End Get
        Set(value As String)
            ViewState("ExpCallId") = value
        End Set
    End Property

    Private Property IsFreeSale As Boolean
        Get
            Return CBool(ViewState("IsFreeSale"))
        End Get
        Set(value As Boolean)
            ViewState("IsFreeSale") = value
        End Set
    End Property
#End Region
    
#Region "Procedures"

    Sub LoadStaticData()
        Dim dsInfo As DataSet = BookingExperiences.GetCustomerInfo(bookingId, rentalId)
        bookingIdLabel.Text = rentalId

        If (Not dsInfo Is Nothing) Then
            If Not IsDBNull(dsInfo.Tables(0).Rows(0)("CName")) Then
                HirerLabel.Text = dsInfo.Tables(0).Rows(0)("CName")
            End If
            If Not IsDBNull(dsInfo.Tables(0).Rows(0)("EmailAddress")) Then
                EmailLabel.Text = dsInfo.Tables(0).Rows(0)("EmailAddress").ToString
            End If
        End If

        ReloadSuppliers(dsStatic)
        ReloadExperienceStatus(dsStatic)
        ReloadCallStatus(dsStatic)

    End Sub

    Sub LoadHistory()
        Dim dsHistory As DataSet = BookingExperiences.GetCallContactHistory(IIf(String.IsNullOrEmpty(expCallId), -1, expCallId), rentalId)
        If (dsHistory.Tables.Count - 1 = -1) Then

            dsHistory = New DataSet
            Dim dt As New DataTable

            dt.Columns.Add("ExpCallId", GetType(Integer))
            dt.Columns.Add("ExpCallStatusDesc", GetType(String))
            dt.Columns.Add("ExpCallOutboundCallDt", GetType(String))
            dt.Columns.Add("ExpCallNotes", GetType(String))
            dt.Columns.Add("AddUsrId", GetType(String))
            dt.Columns.Add("AddDateTime", GetType(String))
            dsHistory.Tables.Add(dt)

        End If

        gridviewHistory.DataSource = dsHistory
        gridviewHistory.DataBind()
    End Sub

    Sub LoadHistories(ExpCallId As Integer)
        Dim dsHistory As DataSet = BookingExperiences.GetCallContactHistories(ExpCallId)
        gridviewHistory.DataSource = dsHistory
        gridviewHistory.DataBind()
    End Sub

    Sub LoadPurchaseHistory(ExpCallId As Integer)
        Dim dsHistory As DataSet = BookingExperiences.GetExperienceSummary(ExpCallId) ''.GetPurchaseHistory(ExpCallId)
        If (dsHistory.Tables.Count - 1 = -1) Then

            Dim dt As New DataTable

            dt.Columns.Add("ExpCallId", GetType(Integer))
            dt.Columns.Add("ExpCallTktId", GetType(String))
            dt.Columns.Add("ExpItemId", GetType(String))
            dt.Columns.Add("ExpCallTktPrice", GetType(String))
            dt.Columns.Add("ExpCallTktQty", GetType(String))
            dt.Columns.Add("ExpCallTktTotal", GetType(String))
            dt.Columns.Add("ExpCallTktExpDateTime", GetType(String))
            dt.Columns.Add("ExpCallTktStatusDesc", GetType(String))
            dt.Columns.Add("ExpCallTktResRefNum", GetType(String))
            dt.Columns.Add("ExpCallTktPrdLinkId", GetType(String))
            dt.Columns.Add("AddUsrId", GetType(String))
            dt.Columns.Add("AddDateTime", GetType(String))
            dsHistory.Tables.Add(dt)

        End If

        addExperiencegridview.DataSource = dsHistory
        addExperiencegridview.DataBind()
    End Sub

    Sub LoadExperienceHistory(ExpCallId As Integer)
        addExperiencegridview.DataSource = BookingExperiences.GetExperienceSummary(ExpCallId)
        addExperiencegridview.DataBind()
    End Sub

    Sub ReloadSuppliers(_dsStatic As DataSet)
        SupplierDropDown.Items.Clear()
        SupplierDropDown.AppendDataBoundItems = True
        SupplierDropDown.Items.Add(New ListItem("Please Select"))
        SupplierDropDown.DataSource = _dsStatic.Tables(2)
        SupplierDropDown.DataTextField = "SupCode"
        SupplierDropDown.DataValueField = "SupId"
        SupplierDropDown.DataBind()
    End Sub

    Sub ReloadTagCategories(_dsStatic As DataSet)
        CategoryDropDownList.Items.Clear()
        CategoryDropDownList.AppendDataBoundItems = True
        CategoryDropDownList.Items.Add(New ListItem("Please Select"))
        CategoryDropDownList.DataSource = _dsStatic.Tables(5)
        CategoryDropDownList.DataTextField = "CodDesc"
        CategoryDropDownList.DataValueField = "CodId"
        CategoryDropDownList.DataBind()
    End Sub

    Sub ReloadExperienceStatus(_dsStatic As DataSet)
        StatusExDropDown.Items.Clear()
        StatusExDropDown.AppendDataBoundItems = True
        StatusExDropDown.Items.Add(New ListItem("Please Select"))
        StatusExDropDown.DataSource = _dsStatic.Tables(1)
        StatusExDropDown.DataTextField = "CodDesc"
        StatusExDropDown.DataValueField = "CodId"
        StatusExDropDown.DataBind()

    End Sub

    Sub ReloadCallStatus(_dsStatic As DataSet)
        StatusDropDown.Items.Clear()
        StatusDropDown.AppendDataBoundItems = True
        StatusDropDown.Items.Add(New ListItem("Please Select"))
        StatusDropDown.DataSource = _dsStatic.Tables(0)
        StatusDropDown.DataTextField = "CodDesc"
        StatusDropDown.DataValueField = "CodId"
        StatusDropDown.DataBind()
    End Sub

    Sub ReloadExperiences(_dsStatic As DataSet, supplierId As String, categoryId As String)
        Dim dview As DataView = dsStatic.Tables(3).DefaultView
        dview.RowFilter = "SupId = '" & supplierId & "' and Type='" & categoryId & "'"

        ExperienceDropDown.Items.Clear()
        ExperienceDropDown.AppendDataBoundItems = True
        ExperienceDropDown.Items.Add(New ListItem("Please Select"))
        ExperienceDropDown.DataTextField = "ExpName"
        ExperienceDropDown.DataValueField = "ExpId"
        ExperienceDropDown.DataSource = dview
        ExperienceDropDown.DataBind()
    End Sub

    Sub ReloadTickets(_dsStatic As DataSet, ExpItem As String)
        Dim dview As DataView = dsStatic.Tables(3).DefaultView
        dview.RowFilter = "ExpId = '" & ExpItem & "'"

        Dim sb As New StringBuilder
        For Each rowView As DataRowView In dview
            Dim row As DataRow = rowView.Row
            sb.AppendFormat("Conditions:{0}{1}{2}", row("ExpConditionText").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("Validity:{0}{1}{2}", row("ExpConditionValidity").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("Description:{0}{1}{2}", row("Description").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("Free Sale:{0}{1}{2}", row("ExpIsFreeSale").ToString, vbCrLf, vbCrLf)
        Next
        ExperienceDetailsTextBox.Text = sb.ToString

        dview = dsStatic.Tables(4).DefaultView
        dview.RowFilter = "ExpId = '" & ExpItem & "'"

        TicketDropDown.Items.Clear()
        TicketDropDown.AppendDataBoundItems = True
        TicketDropDown.Items.Add(New ListItem("Please Select"))
        TicketDropDown.DataTextField = "ExpItemDescription"
        TicketDropDown.DataValueField = "ExpItemId"
        TicketDropDown.DataSource = dview
        TicketDropDown.DataBind()

    End Sub

    Sub ReloadTagCategoriesPerSupplier(_dsTags As DataSet)
        CategoryDropDownList.Items.Clear()
        CategoryDropDownList.AppendDataBoundItems = True
        CategoryDropDownList.Items.Add(New ListItem("Please Select"))
        CategoryDropDownList.DataSource = _dsTags.Tables(0)
        CategoryDropDownList.DataTextField = "CodDesc"
        CategoryDropDownList.DataValueField = "CodId"
        CategoryDropDownList.DataBind()
    End Sub

    Sub SetExperienceDropDownToNil()
        ExperienceDropDown.Items.Clear()
        ExperienceDropDown.AppendDataBoundItems = True
        ExperienceDropDown.Items.Add(New ListItem("Please Select"))
        ExperienceDropDown.DataSource = Nothing
        ExperienceDropDown.DataBind()
    End Sub

    Sub SetTicketDropDownToNil()
        TicketDropDown.Items.Clear()
        TicketDropDown.AppendDataBoundItems = True
        TicketDropDown.Items.Add(New ListItem("Please Select"))
        TicketDropDown.DataSource = Nothing
        TicketDropDown.DataBind()
    End Sub

    Sub SetCategoryDropDownToNil()
        CategoryDropDownList.Items.Clear()
        CategoryDropDownList.AppendDataBoundItems = True
        CategoryDropDownList.Items.Add(New ListItem("Please Select"))
        CategoryDropDownList.DataSource = Nothing
        CategoryDropDownList.DataBind()
    End Sub

    Sub DisabledControls(active As Boolean)
        ExtraTextBox.Enabled = active
        priceTextBox.Enabled = active
        TicketDropDown.Enabled = active
        SupplierDropDown.Enabled = active
        ExperienceDropDown.Enabled = active
        CategoryDropDownList.Enabled = active
        ExperienceDetailsTextBox.Enabled = active
        QuantiytDropDown.Enabled = active
        ExtraTextBox.Enabled = True
    End Sub

    Sub DisabledAllControls(active As Boolean)
        ExtraTextBox.Enabled = active
        priceTextBox.Enabled = active
        dateDateControl.Enabled = active
        dateTimeControl.Enabled = active
        StatusExDropDown.Enabled = active
        TicketDropDown.Enabled = active
        SupplierDropDown.Enabled = active
        ExperienceDropDown.Enabled = active
        CategoryDropDownList.Enabled = active
        ExperienceDetailsTextBox.Enabled = active
        QuantiytDropDown.Enabled = active
        ExtraTextBox.Enabled = True
    End Sub

    Sub ReloadExperienceStatusWithFilter(_dsStatic As DataSet)
        Dim dview As DataView = dsStatic.Tables(1).DefaultView
        dview.RowFilter = "CodDesc IN ('Amended Confirmed','Cancelled')"

        StatusExDropDown.Items.Clear()
        StatusExDropDown.AppendDataBoundItems = True
        StatusExDropDown.Items.Add(New ListItem("Please Select"))
        StatusExDropDown.DataSource = dview
        StatusExDropDown.DataTextField = "CodDesc"
        StatusExDropDown.DataValueField = "CodId"
        StatusExDropDown.DataBind()
    End Sub

#End Region

#Region "Enum"
    Enum ControlsThatWillReset
        Supplier = 0
        Tag = 1
        Experience = 2
        Ticket = 3
        StatusExperience = 4
    End Enum
#End Region

#Region "Functions"

    Function ResetControlsWhenDropDownChange(resetcontrol As ControlsThatWillReset) As Boolean
        Select Case resetcontrol
            Case ControlsThatWillReset.Supplier
                SetCategoryDropDownToNil()
                SetExperienceDropDownToNil()
                SetTicketDropDownToNil()
                ExperienceDetailsTextBox.Text = ""
                tdExperienceDateTime.Visible = False
                tdExperienceDateTimeLabel.Visible = False
            Case ControlsThatWillReset.Tag
                SetExperienceDropDownToNil()
                SetTicketDropDownToNil()
                ExperienceDetailsTextBox.Text = ""
                tdExperienceDateTime.Visible = False
                tdExperienceDateTimeLabel.Visible = False
            Case ControlsThatWillReset.Experience
                tdExperienceDateTime.Visible = False
                tdExperienceDateTimeLabel.Visible = False
                SetTicketDropDownToNil()
                ExperienceDetailsTextBox.Text = ""
            Case ControlsThatWillReset.Ticket
            Case ControlsThatWillReset.StatusExperience
        End Select


        priceTextBox.Text = ""
        TotalTextBox.Text = ""
        dateDateControl.Text = ""
        dateTimeControl.Text = ""
        SupplierRefTextBox.Text = ""
        ExtraTextBox.Text = ""
        StatusExDropDown.SelectedIndex = -1
        QuantiytDropDown.SelectedIndex = -1


        Return True
    End Function

    Function DisabledDateTimeControl() As Boolean
        Dim disabled As Boolean = IIf(StatusDropDown.SelectedItem.Text.Contains("Call Back") = True, True, False)
        outboundDateControl.Enabled = disabled
        outboundTimeControl.Enabled = disabled
        tdtimeDate.Visible = disabled
        tdoutboundcall.Visible = disabled
        tdphonelabel.Visible = disabled
        tdphone.Visible = disabled
        If Not (disabled) Then
            phonenumbertext.Text = ""
            outboundDateControl.text = ""
            outboundTimeControl.text = ""
        End If
        Return disabled
    End Function

    Function IsValidToAddExperience(ByRef ExpCallOutboundCallDt As DateTime) As Boolean
        Dim statusID As String = StatusDropDown.SelectedValue
        If (String.IsNullOrEmpty(statusID)) Then
            SetErrorShortMessage("Please select status")
            Return False
        Else
            If (statusID.Contains("Select") = True) Then
                SetErrorShortMessage("Please select status")
                Return False
            End If
        End If

        If (DisabledDateTimeControl()) Then

            If (String.IsNullOrEmpty(outboundDateControl.Text)) Then
                SetErrorShortMessage("Please enter Outbound Call date")
                Return False
            End If

            If (String.IsNullOrEmpty(outboundTimeControl.Text)) Then
                SetErrorShortMessage("Please enter Outbound Call Time")
                Return False
            End If

            If (Not outboundDateControl.IsValid Or Not outboundTimeControl.IsValid) Then
                SetErrorShortMessage("Please enter Outbound Call date and Time")
                Return False
            End If
            ExpCallOutboundCallDt = outboundDateControl.Date.AddHours(outboundTimeControl.Time.Hours).AddMinutes(outboundTimeControl.Time.Minutes)
            If (ExpCallOutboundCallDt = DateTime.MinValue) Then
                SetErrorShortMessage("Please enter date and time")
                Return False
            End If


            If (ExpCallOutboundCallDt < DateTime.Now) Then
                SetErrorShortMessage("Please enter CallBack date and time in advance")
                Return False
            End If

            If (String.IsNullOrEmpty(phonenumbertext.Text)) Then
                SetErrorShortMessage("Please enter phone number")
                Return False
            End If
        Else
            ExpCallOutboundCallDt = DateTime.MinValue
        End If
        Return True
    End Function

    Function AddCall() As String
        Dim statusID As String = StatusDropDown.SelectedValue
        Dim ExpCallOutboundCallDt As DateTime
        Dim result As String = "ERROR"

        If (IsValidToAddExperience(ExpCallOutboundCallDt)) Then
            Dim ExpCallNotes As String = notesTextBox.Text
            Dim phonetext As String = phonenumbertext.Text

            If (Not String.IsNullOrEmpty(ExperienceCallId)) Then
                result = BookingExperiences.UpdateRecordAndCloneInHistory(ExperienceCallId, statusID, ExpCallOutboundCallDt, ExpCallNotes, UserCode, phonetext)
                ''result = BookingExperiences.UpdateRecordAndCloneInHistory(ExperienceCallId, statusID, ExpCallOutboundCallDt, ExpCallNotes, "NT5", phonetext)
                If (result.IndexOf("-") = -1) Then
                    If (result.Equals("OK")) Then
                        result = result & "-" & ExperienceCallId
                    End If
                End If
            Else
                result = BookingExperiences.InsertRecordInHistory(rentalId, statusID, ExpCallOutboundCallDt, ExpCallNotes, UserCode, phonetext)
                If (result.Contains("OK") = True) Then
                End If
            End If
        End If

        Return result
    End Function

  
#End Region

#Region "DropDowns"

    Protected Sub StatusDropDown_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles StatusDropDown.SelectedIndexChanged
        DisabledDateTimeControl()
    End Sub

    Protected Sub SupplierDropDown_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles SupplierDropDown.SelectedIndexChanged
        If (SupplierDropDown.SelectedValue = "Please Select") Then
            SetWarningShortMessage("Please select Supplier")
            ResetControlsWhenDropDownChange(ControlsThatWillReset.Supplier)
            Return
        End If

        Dim dsTags As DataSet = BookingExperiences.GetTagsForSupplierAndExperience(SupplierDropDown.SelectedValue)
        ReloadTagCategoriesPerSupplier(dsTags)
        ResetControlsWhenDropDownChange(ControlsThatWillReset.Tag)

    End Sub

    Protected Sub CategoryDropDownList_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles CategoryDropDownList.SelectedIndexChanged
        If dsStatic Is Nothing Then
            dsStatic = StaticDataSet
        End If
        If (SupplierDropDown.SelectedValue = "Please Select") Then
            SetWarningShortMessage("Please select Supplier")
            Return
        End If

        If (CategoryDropDownList.SelectedValue = "Please Select") Then
            ResetControlsWhenDropDownChange(ControlsThatWillReset.Tag)
            Return
        End If

        Dim dview As DataView = dsStatic.Tables(3).DefaultView
        dview.RowFilter = "SupId = '" & SupplierDropDown.SelectedValue & "' and Type='" & CategoryDropDownList.SelectedValue & "'"
        ExperienceDetailsTextBox.Text = ""

        ExperienceDropDown.Items.Clear()
        ExperienceDropDown.AppendDataBoundItems = True
        ExperienceDropDown.Items.Add(New ListItem("Please Select"))
        ExperienceDropDown.DataTextField = "ExpName"
        ExperienceDropDown.DataValueField = "ExpId"
        ExperienceDropDown.DataSource = dview
        ExperienceDropDown.DataBind()

        ResetControlsWhenDropDownChange(ControlsThatWillReset.Experience)
    End Sub

    Protected Sub ExperienceDropDown_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ExperienceDropDown.SelectedIndexChanged
        If dsStatic Is Nothing Then
            dsStatic = StaticDataSet
        End If
        If (SupplierDropDown.SelectedValue = "Please Select") Then
            SetWarningShortMessage("Please select Supplier")
            Return
        End If

        If (CategoryDropDownList.SelectedValue = "Please Select") Then
            ExperienceDetailsTextBox.Text = ""
            ExperienceDropDown.SelectedIndex = -1
            Return
        End If

        If (ExperienceDropDown.SelectedValue = "Please Select") Then
            ResetControlsWhenDropDownChange(ControlsThatWillReset.Experience)
            Return
        End If

        Dim dview As DataView = dsStatic.Tables(3).DefaultView
        dview.RowFilter = "ExpId = '" & ExperienceDropDown.SelectedValue & "'"

        Dim sb As New StringBuilder


        For Each rowView As DataRowView In dview
            Dim row As DataRow = rowView.Row
            'sb.AppendFormat("Conditions:{0}{1}{2}", row("ExpConditionText").ToString, vbCrLf, vbCrLf)
            'sb.AppendFormat("Validity:{0}{1}{2}", row("ExpConditionValidity").ToString, vbCrLf, vbCrLf)
            'sb.AppendFormat("Description:{0}{1}{2}", row("Description").ToString, vbCrLf, vbCrLf)


            sb.AppendFormat("{0}{1}{2}", row("ExpDescription").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("{0}{1}{2}", row("ExpCancellationPolicy").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("{0}{1}{2}", row("ExpCusNeedToKnow").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("{0}{1}{2}", row("ExpOpeningDays").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("{0}{1}{2}", row("ExpOpeningHours").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("{0}{1}{2}", row("ExpAddOnsInclusion").ToString, vbCrLf, vbCrLf)
            sb.AppendFormat("Free Sale:{0}{1}{2}", row("ExpIsFreeSale").ToString, vbCrLf, vbCrLf)

            IsFreeSale = CBool(row("ExpIsFreeSale").ToString)
        Next
        ExperienceDetailsTextBox.Text = sb.ToString

        dview = dsStatic.Tables(4).DefaultView
        dview.RowFilter = "ExpId = '" & ExperienceDropDown.SelectedValue & "'"

        TicketDropDown.Items.Clear()
        TicketDropDown.AppendDataBoundItems = True
        TicketDropDown.Items.Add(New ListItem("Please Select"))
        TicketDropDown.DataTextField = "ExpItemDescription"
        TicketDropDown.DataValueField = "ExpItemId"
        TicketDropDown.DataSource = dview
        TicketDropDown.DataBind()


        tdExperienceDateTime.Visible = Not IsFreeSale
        tdExperienceDateTimeLabel.Visible = Not IsFreeSale

    End Sub

    Protected Sub QuantiytDropDown_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles QuantiytDropDown.SelectedIndexChanged
        Dim price As Decimal
        If Not Decimal.TryParse(priceTextBox.Text.Replace("$", ""), price) Then
            priceTextBox.Text = ""
        Else
            Dim ExpCallTktPrice As Decimal = CDec(priceTextBox.Text)
            Dim ExpCallTktQty As Integer = QuantiytDropDown.SelectedValue
            Dim ExpCallTktTotal As Decimal = ExpCallTktPrice * ExpCallTktQty
            TotalTextBox.Text = ExpCallTktTotal.ToString("C")
            priceTextBox.Text = ExpCallTktPrice.ToString("C")
        End If
    End Sub

    Protected Sub TicketDropDown_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles TicketDropDown.SelectedIndexChanged
        If (TicketDropDown.Text = "Please Select") Then
            ResetControlsWhenDropDownChange(ControlsThatWillReset.Ticket)
            Return
        End If
        Dim text As String = TicketDropDown.SelectedItem.Text.Split("$")(1)
        If (Not String.IsNullOrEmpty(text)) Then
            text = text.Split(" ")(0)
            Try
                Dim price As Decimal
                If Not Decimal.TryParse(CDec(text), price) Then
                    priceTextBox.Text = ""
                Else
                    priceTextBox.Text = text
                    Dim ExpCallTktPrice As Decimal = CDec(priceTextBox.Text)
                    Dim ExpCallTktQty As Integer = QuantiytDropDown.SelectedValue
                    Dim ExpCallTktTotal As Decimal = ExpCallTktPrice * ExpCallTktQty
                    TotalTextBox.Text = ExpCallTktTotal.ToString("C")
                    priceTextBox.Text = ExpCallTktPrice.ToString("C")
                End If
            Catch ex As Exception
                priceTextBox.Text = ""
            End Try

        End If
    End Sub

#End Region

#Region "Buttons"

    Protected Sub CallContactButtonSave_Click(sender As Object, e As System.EventArgs) Handles CallContactButtonSave.Click
        Dim result As String = AddCall()
        If (result.Contains("OK")) Then
            Response.Redirect("Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=16")
        End If
    End Sub

    Protected Sub CallContactButtonSaveAndContinue_Click(sender As Object, e As System.EventArgs) Handles CallContactButtonSaveAndContinue.Click
        Dim result As String = AddCall()
        If (result.Contains("OK")) Then
            ExperienceCallId = result.Split("-")(1)
            expCallId = ExperienceCallId
            LoadHistories(CInt(ExperienceCallId))
            LoadPurchaseHistory(CInt(ExperienceCallId))
            ''reset after adding
            notesTextBox.Text = ""
            StatusDropDown.SelectedIndex = -1
            DisabledDateTimeControl()
            SetInformationMessage("New Call / Status was added successfully")
        End If

    End Sub

    Protected Sub CancelButton_Click(sender As Object, e As System.EventArgs) Handles CancelButton.Click
        ResetControlsWhenDropDownChange(ControlsThatWillReset.Supplier)
        SupplierDropDown.SelectedIndex = -1
        AddExperienceButton.Text = "Add Experience"
        DisabledAllControls(True)
    End Sub

    Protected Sub BackButton_Click(sender As Object, e As System.EventArgs) Handles BackButton.Click, BackButtonTop.Click
        Response.Redirect("Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & bookingId & "&hdRentalId=" & rentalId & "&activeTab=16")
    End Sub

    Protected Sub AddExperienceButton_Click(sender As Object, e As System.EventArgs) Handles AddExperienceButton.Click
        Const ERR_DATETIME As String = "Please provide Date And Time details"

        If (String.IsNullOrEmpty(ExperienceCallId)) Then
            SetErrorMessage("Please create first Call Status under the Call / Contact fields before create Experience Items")
            Return
        End If

        Dim SupplierDropDownID As String = SupplierDropDown.Text
        If (SupplierDropDownID.Contains("Select") = True) Then
            SetErrorShortMessage("Please provide Supplier detail")
            Exit Sub
        End If

        Dim categoryDropDownID As String = CategoryDropDownList.Text
        If (categoryDropDownID.Contains("Select") = True) Then
            SetErrorShortMessage("Please provide Tag/Category detail")
            Exit Sub
        End If

        Dim ExperienceDropDownID As String = ExperienceDropDown.Text
        If (ExperienceDropDownID.Contains("Select") = True) Then
            SetErrorShortMessage("Please provide Experience detail")
            Exit Sub
        End If

        Dim TicketDropDownID As String = TicketDropDown.Text
        If (TicketDropDownID.Contains("Select") = True) Then
            SetErrorShortMessage("Please provide Ticket detail")
            Exit Sub
        End If

        If (String.IsNullOrEmpty(priceTextBox.Text)) Then
            SetErrorShortMessage("Please provide Price detail")
            Exit Sub
        End If

        Dim StatusExDropDownID As String = StatusExDropDown.Text
        If (StatusExDropDownID.Contains("Select") = True) Then
            SetErrorShortMessage("Please provide Status detail")
            Exit Sub
        End If

        Dim ExpCallTktDateTime As DateTime
        ''rev:mia 24-nov-2015 AURORA-469 Experiences - cannot cancel a voucher
        Dim statusExDropDownText As String = StatusExDropDown.SelectedItem.Text
        If (Not IsFreeSale And statusExDropDownText <> "Cancelled") Then
            If (String.IsNullOrEmpty(dateDateControl.Text)) Then
                SetErrorShortMessage(ERR_DATETIME)
                Exit Sub
            End If

            If (String.IsNullOrEmpty(dateTimeControl.Text)) Then
                SetErrorShortMessage(ERR_DATETIME)
                Exit Sub
            End If

            If (Not dateDateControl.IsValid Or Not dateTimeControl.IsValid) Then
                SetErrorShortMessage(ERR_DATETIME)
                Exit Sub
            End If

            ExpCallTktDateTime = dateDateControl.Date.AddHours(dateTimeControl.Time.Hours).AddMinutes(dateTimeControl.Time.Minutes)

            If (ExpCallTktDateTime = DateTime.MinValue) Then
                SetErrorShortMessage(ERR_DATETIME)
                Exit Sub
            End If

            ''rev:mia 07-dec-2015 aurora-501 Unable to add an Experience to a booking with the same date as pick up date
            'If (ExpCallTktDateTime < DateTime.Now) Then
            '    SetErrorShortMessage("Please provide Date And Time detail ahead")
            '    Exit Sub
            'End If

        End If

        Dim ExpItemId As Integer = TicketDropDown.SelectedValue
        Dim ExpCallTktPrice As Decimal = CDec(priceTextBox.Text)
        Dim ExpCallTktQty As Integer = QuantiytDropDown.SelectedValue
        Dim ExpCallTktTotal As Decimal = ExpCallTktPrice * ExpCallTktQty

        Dim ExpCallTktSupInfo As String = ExtraTextBox.Text
        Dim ExpCallTktStatusId As String = StatusExDropDown.SelectedValue
        Dim ExpCallTktSupplierRefNum As String = SupplierRefTextBox.Text

        Dim ExpCallTktResRefNum As String = ""
        If (StatusExDropDown.SelectedItem.Text = "Confirmed") Then
            ExpCallTktResRefNum = rentalId.ToUpper
        End If



        Dim ExpCallTktPrdLinkId As Integer = -1
        Dim AddUsrId As String = UserCode.ToUpper

        Dim result As String = ""
        If (AddExperienceButton.Text = "Add Experience") Then
            result = BookingExperiences.InsertExperienceInHistory(ExperienceCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, AddUsrId, ExpCallTktSupplierRefNum)

            If (result.Contains("OK") And Not result.Contains("ERROR")) Then
                SetInformationMessage("Purchase Experience was added successfully")
            ElseIf (result.Contains("ERROR")) Then
                SetErrorMessage(result)
            Else
                SetErrorMessage("Purchase Experience Save Failed")
            End If
        Else
            If (Not String.IsNullOrEmpty(ExpCallTktId)) Then
                If (Not String.IsNullOrEmpty(HiddenVoucher.Value)) Then
                    ExpCallTktResRefNum = HiddenVoucher.Value
                End If
                result = BookingExperiences.UpdateSingleExperienceCallTickets(ExpCallId, ExpItemId, ExpCallTktPrice, ExpCallTktQty, ExpCallTktTotal, ExpCallTktDateTime, ExpCallTktSupInfo, ExpCallTktStatusId, ExpCallTktResRefNum, ExpCallTktPrdLinkId, AddUsrId, CInt(ExpCallTktId), ExpCallTktSupplierRefNum)
                If (result.Contains("OK") And Not result.Contains("ERROR")) Then
                    SetInformationMessage("Purchase Experience was updated successfully")
                    AddExperienceButton.Text = "Add Experience"
                ElseIf (result.Contains("ERROR")) Then
                    SetErrorMessage(result)
                Else
                    SetErrorMessage("Purchase Experience Update Failed")
                End If
                If dsStatic Is Nothing Then
                    dsStatic = StaticDataSet
                End If
                ReloadExperienceStatus(dsStatic)
            End If
        End If

        If (result.Contains("OK")) Then
            LoadExperienceHistory(ExperienceCallId)
        End If

        HiddenVoucher.Value = ""
        DisabledAllControls(True) ''active all
        SupplierDropDown.SelectedIndex = -1
        ResetControlsWhenDropDownChange(ControlsThatWillReset.Supplier)
    End Sub

#End Region

#Region "GridViews"

    Protected Sub addExperiencegridview_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles addExperiencegridview.RowCommand

        If e.CommandName = "Select" Then


            Dim index As Integer = Convert.ToInt32(e.CommandArgument)
            Dim selRow As GridViewRow = addExperiencegridview.Rows(index)
            ExpCallTktId = CType(selRow.FindControl("hiddenId"), HiddenField).Value



            DisabledControls(IIf(selRow.Cells(8).Text = "Confirmed", False, True))
            If (String.IsNullOrEmpty(ExpCallId)) Then
                ExpCallId = ExperienceCallId
            End If

            Dim dsExperience As DataSet = BookingExperiences.GetSingleExperienceCallTickets(CInt(ExpCallTktId))
            Dim dsTags As DataSet = BookingExperiences.GetTags(CInt(ExpCallTktId), CInt(expCallId))
            Dim ExpTypCodId As String = ""
            If (dsTags.Tables.Count - 1 <> -1) Then
                If dsTags.Tables(0).Rows.Count - 1 <> -1 Then
                    If (Not dsTags.Tables(0).Rows(0)("ExpTypCodId") Is Nothing) Then
                        ExpTypCodId = dsTags.Tables(0).Rows(0)("ExpTypCodId")
                    End If
                End If
            End If

            If dsExperience.Tables.Count - 1 <> -1 Then

                If dsStatic Is Nothing Then
                    dsStatic = StaticDataSet
                End If
              

                Dim dsSupplierAndItems As DataSet = BookingExperiences.GetExperienceSupplierAndItems(expCallId, ExpCallTktId)

                ReloadSuppliers(dsStatic)
                SupplierDropDown.SelectedValue = dsSupplierAndItems.Tables(0).Rows(0)("Supplier")

                If (Not String.IsNullOrEmpty(ExpTypCodId)) Then

                    dsTags = BookingExperiences.GetTagsForSupplierAndExperience(SupplierDropDown.SelectedValue)
                    ReloadTagCategoriesPerSupplier(dsTags)
                    CategoryDropDownList.SelectedValue = ExpTypCodId

                    ReloadExperiences(dsExperience, SupplierDropDown.SelectedValue, ExpTypCodId)
                    ExperienceDropDown.SelectedValue = dsSupplierAndItems.Tables(0).Rows(0)("Experience")

                    ReloadTickets(dsStatic, ExperienceDropDown.SelectedValue)
                    TicketDropDown.SelectedValue = dsExperience.Tables(0).Rows(0)("ExpItemId")
                End If

                Try
                    Dim dview As DataView = dsStatic.Tables(3).DefaultView
                    dview.RowFilter = "ExpId = '" & ExperienceDropDown.SelectedValue & "'"
                    Dim sb As New StringBuilder
                    For Each rowView As DataRowView In dview
                        Dim row As DataRow = rowView.Row
                        sb.AppendFormat("{0}{1}{2}", row("ExpDescription").ToString, vbCrLf, vbCrLf)
                        sb.AppendFormat("{0}{1}{2}", row("ExpCancellationPolicy").ToString, vbCrLf, vbCrLf)
                        sb.AppendFormat("{0}{1}{2}", row("ExpCusNeedToKnow").ToString, vbCrLf, vbCrLf)
                        sb.AppendFormat("{0}{1}{2}", row("ExpOpeningDays").ToString, vbCrLf, vbCrLf)
                        sb.AppendFormat("{0}{1}{2}", row("ExpOpeningHours").ToString, vbCrLf, vbCrLf)
                        sb.AppendFormat("{0}{1}{2}", row("ExpAddOnsInclusion").ToString, vbCrLf, vbCrLf)
                        sb.AppendFormat("Free Sale:{0}{1}{2}", row("ExpIsFreeSale").ToString, vbCrLf, vbCrLf)
                        IsFreeSale = CBool(row("ExpIsFreeSale").ToString)
                    Next
                    ExperienceDetailsTextBox.Text = sb.ToString
                Catch ex As Exception

                End Try

                priceTextBox.Text = CDec(dsExperience.Tables(0).Rows(0)("ExpCallTktPrice")).ToString("c")
                QuantiytDropDown.SelectedValue = CInt(dsExperience.Tables(0).Rows(0)("ExpCallTktQty"))
                TotalTextBox.Text = CDec(dsExperience.Tables(0).Rows(0)("ExpCallTktTotal")).ToString("c")
                ExtraTextBox.Text = dsExperience.Tables(0).Rows(0)("ExpCallTktSupInfo")
                SupplierRefTextBox.Text = dsExperience.Tables(0).Rows(0)("ExpCallTktSupplierRefNum")
                HiddenVoucher.Value = selRow.Cells(9).Text.Replace("&nbsp;", "")

                If (selRow.Cells(8).Text = "Confirmed") Then
                    ReloadExperienceStatusWithFilter(dsStatic)
                ElseIf (selRow.Cells(8).Text.Contains("Amended") = True) And Not String.IsNullOrEmpty(HiddenVoucher.Value) Then
                    ReloadExperienceStatusWithFilter(dsStatic)
                    StatusExDropDown.SelectedValue = dsExperience.Tables(0).Rows(0)("ExpCallTktStatusId")
                    DisabledControls(False)
                Else
                    ReloadExperienceStatus(dsStatic)
                    StatusExDropDown.SelectedValue = dsExperience.Tables(0).Rows(0)("ExpCallTktStatusId")
                End If

                If (Not IsDBNull(dsExperience.Tables(0).Rows(0)("ExpCallTktDateTime"))) Then
                    dateDateControl.Text = CDate(dsExperience.Tables(0).Rows(0)("ExpCallTktDateTime")).ToString("dd/MM/yyyy")
                    dateTimeControl.Text = CDate(dsExperience.Tables(0).Rows(0)("ExpCallTktDateTime")).ToString("hh:mm")

                    tdExperienceDateTimeLabel.Visible = True
                    tdExperienceDateTime.Visible = True
                Else
                    ''FREESALE = FALSE, make dateand time visible
                    ''rev:mia 24-nov-2015 AURORA-469 Experiences - cannot cancel a voucher
                    tdExperienceDateTimeLabel.Visible = Not IsFreeSale
                    tdExperienceDateTime.Visible = Not IsFreeSale
                End If
                
                AddExperienceButton.Text = "Update Experience"
            End If
        End If
    End Sub

    Protected Sub addExperiencegridview_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles addExperiencegridview.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim hiddenId As HiddenField = CType(e.Row.FindControl("hiddenId"), HiddenField)
            If (Not hiddenId Is Nothing) Then
                Dim rowview As DataRowView = CType(e.Row.DataItem, DataRowView)
                hiddenId.Value = rowview("Id").ToString
            End If
        End If
    End Sub

#End Region

#Region "Textbox"

    Protected Sub priceTextBox_TextChanged(sender As Object, e As System.EventArgs) Handles priceTextBox.TextChanged
        Try
            Dim price As Decimal
            If Not Decimal.TryParse(CDec(priceTextBox.Text), price) Then
                priceTextBox.Text = ""
            Else
                Dim ExpCallTktPrice As Decimal = CDec(priceTextBox.Text)
                Dim ExpCallTktQty As Integer = QuantiytDropDown.SelectedValue
                Dim ExpCallTktTotal As Decimal = ExpCallTktPrice * ExpCallTktQty
                TotalTextBox.Text = ExpCallTktTotal.ToString("C")
                priceTextBox.Text = ExpCallTktPrice.ToString("C")
            End If
        Catch ex As Exception
            SetErrorShortMessage("Invalid Price")
            priceTextBox.Text = ""
        End Try
    End Sub

#End Region

#Region "PageLoad"

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        bookingId = Request.QueryString("hdBookingId")
        rentalId = Request.QueryString("hdRentalId")
        mode = Request.QueryString("Mode")

        If Not IsPostBack Then
            expCallId = Request.QueryString("expCallId")

            Dim ctycode As String = BookingExperiences.GetCountry(rentalId)
            dsStatic = BookingExperiences.LoadStaticData(ctycode)
            StaticDataSet = dsStatic
            LoadStaticData()

            LoadHistories(expCallId)
            LoadPurchaseHistory(CInt(expCallId))

            outboundDateControl.Enabled = False
            outboundTimeControl.Enabled = False
            tdtimeDate.Visible = False
            tdoutboundcall.Visible = False
            tdphonelabel.Visible = False
            tdphone.Visible = False

            If (mode = "Edit") Then
                Dim dsSingleCall As DataSet = BookingExperiences.GetSingleExperienceCall(CInt(expCallId))
                StatusDropDown.SelectedValue = dsSingleCall.Tables(0).Rows(0)("ExpCallStatusId").ToString
                If (StatusDropDown.SelectedItem.Text.Equals("Call Back")) Then
                    outboundDateControl.Text = CDate(dsSingleCall.Tables(0).Rows(0)("ExpCallOutboundCallDt")).ToString("dd/MM/yyyy")
                    outboundTimeControl.Text = CDate(dsSingleCall.Tables(0).Rows(0)("ExpCallOutboundCallDt")).ToString("hh:mm")
                    phonenumbertext.Text = dsSingleCall.Tables(0).Rows(0)("ExpCallPhNumber").ToString()
                    DisabledDateTimeControl()
                End If
                ExperienceCallId = expCallId
            End If

            tdExperienceDateTime.Visible = False
            tdExperienceDateTimeLabel.Visible = False
        End If

    End Sub

#End Region
    


    



    
    

    

    

    
End Class
