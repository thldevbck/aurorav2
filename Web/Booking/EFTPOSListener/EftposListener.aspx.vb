Imports Aurora.Booking.Services
Imports Aurora.Common.Logging
Imports System.Data

Partial Class Booking_EFTPOSListener_EftposListener
    Inherits System.Web.UI.Page



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim qs As String = Request.QueryString("Mode")
        Dim returnedvalue As String = ""
        Dim encoded As String = ""

        Dim EFTPOS As New BookingPaymentEFTPOSMaintenance
        Dim TOKEN As New BookingEFTPOSBillingToken

        If Not String.IsNullOrEmpty(qs) Then
            If (qs.Equals("Payment")) Then
                encoded = EncodedString(Request("Payment"))
                LogDebug("EFTPOsListener  Payment", encoded)
                returnedvalue = EFTPOS.SaveEFTPOSPayment(encoded)
            ElseIf (qs.Equals("Refund")) Then
                ''encoded = EncodedString(Request("Refund"))
                encoded = Request("Refund")
                LogDebug("EFTPOsListener Refund", encoded)
                Dim isOverride As Boolean = CBool(IIf(String.IsNullOrEmpty(Request.QueryString("IsOveride")), 0, Request.QueryString("IsOveride")))
                Dim isPaymentChange As Boolean = CBool(Request.QueryString("isPaymentChange"))
                returnedvalue = EFTPOS.RefundEFTPOSpayment(encoded, isOverride, isPaymentChange)
            ElseIf (qs.Equals("AllCards")) Then
                encoded = EncodedString(Request("AllCards"))
                returnedvalue = EFTPOS.GetAllCardTypes(encoded)
            ElseIf (qs.Equals("Surcharge")) Then
                encoded = EncodedString(Request("Surcharge"))
                returnedvalue = EFTPOS.Surcharge(encoded)
                If (returnedvalue = CInt(0).ToString) Then returnedvalue = "0.00"

            ElseIf (qs.Equals("CreditCard")) Then
                Dim matchingError As String = ""
                encoded = EncodedString(Request("CreditCard"))
                Dim mixedResult As String = EFTPOS.IsCreditCard(encoded.Split("|")(0), encoded.Split("|")(1).Replace(";", "").Trim)
                If (mixedResult = "True") Then
                    returnedvalue = "True"
                Else
                    returnedvalue = "False"
                End If

            ElseIf (qs.Equals("GetPayment")) Then
                encoded = EncodedString(Request("GetPayment"))
                returnedvalue = EFTPOS.GetPayment(encoded)

            ElseIf (qs.Equals("Receipt")) Then
                ''dont encode this one so formatting of receipt 
                ''will not be lost
                encoded = Request("Receipt")
                Dim sep As String() = {"[eof]"}
                Dim temp As String() = encoded.Split(sep(0))
                Dim info As String = EncodedString(temp(0))
                Dim receipt As String = temp(1).Replace("eof]", "")
                returnedvalue = EFTPOS.SaveReceipt(info, receipt, CInt(ConfigurationManager.AppSettings("ReceiptWidth")))

            ElseIf (qs.Equals("PrintReceipt")) Then
                encoded = Request("PrintReceipt")
                Dim username As String = Request.QueryString("usercode")
                returnedvalue = EFTPOS.PrintIt(username, encoded, CInt(ConfigurationManager.AppSettings("ReceiptWidth")))

            ElseIf (qs.Equals("TestPrint")) Then
                encoded = Request("TestPrint")
                If (encoded.IndexOf("\") = 0) Then encoded = "\" + encoded
                encoded = "Star TSP100"
                returnedvalue = EFTPOS.PrintIt(encoded, "Aurora: Printing to " + encoded, CInt(ConfigurationManager.AppSettings("ReceiptWidth")))
            ElseIf (qs.Equals("GetPrinters")) Then
                ''encoded = Request.QueryString("UserCode")
                ''returnedvalue = EFTPOS.GetPrintersAssignedToBranchAndUser(encoded)
            ElseIf (qs.Equals("RefundOverridePassword")) Then
                encoded = Request("RefundOverridePassword")
                returnedvalue = EFTPOS.RefundOverridePassword(encoded)

            ElseIf (qs.Equals("GetCardNameForDebitIsNotEFTPOS")) Then
                encoded = Request("GetCardNameForDebitIsNotEFTPOS")
                returnedvalue = EFTPOS.GetCardNameForDebitIsNotEFTPOS(encoded)

            ElseIf (qs.Equals("IsCreditCardInBooking")) Then
                encoded = Request("IsCreditCardInBooking")
                encoded = encoded.TrimStart.TrimEnd
                returnedvalue = EFTPOS.IsCreditCardInBooking(encoded.Split("|")(0), encoded.Split("|")(1), encoded.Split("|")(2))

            ElseIf (qs.Equals("IsCreditCardIsFoundInBondPayment")) Then ''IsCreditCardIsFoundInBondPayment: CreditCardIsFoundInBondPayment
                encoded = Request("IsCreditCardIsFoundInBondPayment")
                encoded = encoded.TrimStart.TrimEnd
                returnedvalue = EFTPOS.IsCreditCardIsFoundInBondPayment(encoded.Split("|")(0), encoded.Split("|")(1), encoded.Split("|")(2))

            ElseIf (qs.Equals("logOverridePasswordToHistory")) Then
                encoded = Request("logOverridePasswordToHistory")
                encoded = encoded.TrimStart.TrimEnd
                returnedvalue = EFTPOS.AddEFTPOSHistory(encoded.Split("|")(0), encoded.Split("|")(1), encoded.Split("|")(2))
                If Not String.IsNullOrEmpty(returnedvalue) Then
                    ''DO NOTHING
                End If
            ElseIf (qs.Equals("IsAllowedToRefund")) Then
                encoded = Request("IsAllowedToRefund")
                encoded = encoded.TrimStart.TrimEnd
                Dim RptRntId As String = encoded.Split("|")(0)
                Dim cardNumber As String = encoded.Split("|")(1)
                Dim RptType As String = encoded.Split("|")(2)
                Dim amountToPay As Decimal = encoded.Split("|")(3)
                If (amountToPay.ToString.Contains("-")) Then
                    amountToPay = CDec(encoded.Split("|")(3).ToString.Replace("-", ""))
                End If
                Dim baseamt As String = encoded.Split("|")(4)
                If (baseamt.ToString.Contains("-")) Then
                    '' baseamt = CDec(encoded.Split("|")(4).ToString.Replace("-", ""))
                End If


                Dim message As String = encoded.Split("|")(5)
                Dim ischanged As Boolean = CBool(Request.QueryString("isChanged"))
                Dim manualRefund As String = Request.QueryString("manualRefund")
                Dim pmtId As String = encoded.Split("|")(5)
                Dim isAllowed As Boolean = Convert.ToBoolean(EFTPOS.IsAllowedToRefund(RptRntId.TrimEnd, _
                                                                                      cardNumber.TrimEnd, _
                                                                                      RptType.TrimEnd, _
                                                                                       amountToPay, _
                                                                                       message, _
                                                                                       ischanged, _
                                                                                       manualRefund, _
                                                                                       baseamt, pmtId))


                returnedvalue = message

            ElseIf (qs.Equals("CheckPrinter")) Then
                encoded = Request("CheckPrinter")
                returnedvalue = PrinterSelection.CheckPrinter(encoded)
                If Convert.ToBoolean(returnedvalue) = True Then
                    returnedvalue = "Proceed"
                Else
                    returnedvalue = "Stop"
                End If

                ''šapturing billing token
            ElseIf (qs.Equals("CardTypeList")) Then
                returnedvalue = TOKEN.CardTypeList

            ElseIf (qs.Equals("NewBillingToken")) Then
                encoded = Request("NewBillingToken")
                Dim CardHolderName As String = encoded.Split("|")(0).TrimEnd
                Dim CardNumber As String = encoded.Split("|")(1).TrimEnd.Replace("-", "")
                Dim Amount As String = 1
                Dim ExpiryData As String = encoded.Split("|")(2).TrimEnd.Replace("/", "")
                ExpiryData = ExpiryData.Replace("20", "")
                Dim DPSBillingId As String = ""
                Dim EnableAddBillCard As Boolean = True
                Dim countrycode As String = encoded.Split("|")(3).TrimEnd
                Dim merchant As String = encoded.Split("|")(4).TrimEnd
                Dim RentalIDBillingToken As String = encoded.Split("|")(5).TrimEnd
                Dim cardtype As String = encoded.Split("|")(6).TrimEnd
                Dim usercode As String = encoded.Split("|")(7).TrimEnd
                returnedvalue = TOKEN.NewBillingToken(CardHolderName, _
                                                      CardNumber, _
                                                      Amount, _
                                                      ExpiryData, _
                                                      DPSBillingId, _
                                                      EnableAddBillCard, _
                                                      countrycode, _
                                                      merchant, _
                                                      RentalIDBillingToken, _
                                                      cardtype, _
                                                      usercode)

            ElseIf (qs.Equals("GetAllToken")) Then
                encoded = Request("GetAllToken")
                returnedvalue = TOKEN.GetAllToken(encoded)
            ElseIf (qs.Equals("BillingTokenSelectForPayment")) Then
                encoded = Request("BillingTokenSelectForPayment")
                returnedvalue = TOKEN.GetBillingTokenForPayment(encoded)
            ElseIf (qs.Equals("BillingTokenSelectPopulateCardInfo")) Then
                encoded = Request("BillingTokenSelectPopulateCardInfo")
                returnedvalue = TOKEN.BillingTokenSelectPopulateCardInfo(encoded.Split("|")(0).TrimEnd, encoded.Split("|")(1).TrimEnd)
            ElseIf (qs.Equals("ACTIVEX")) Then
                encoded = Request("ACTIVEX")
                returnedvalue = EFTPOS.MessageInformationFormat(encoded.Split(","))

                ''rev:mia added to change the value of reasons in the dropdown
            ElseIf (qs.Equals("RETREIVEEFTPOSREASONS")) Then
                encoded = Request("RETREIVEEFTPOSREASONS")
                ''returnedvalue = EFTPOS.RetrieveEftposReasons(encoded)
                Dim ds As DataSet = EFTPOS.RetrieveEftposReasons(encoded)
                returnedvalue = ConvertToJS(ds)
            End If


        End If

        Response.Write(returnedvalue)
        Response.End()
    End Sub

    Function EncodedString(ByVal xml As String) As String
        Return HttpUtility.UrlDecode(xml)
    End Function

    Private Function ConvertToJS(ByVal ds As DataSet) As String
        If (ds.Tables(0).HasErrors) Then
            Return "ERROR"
        End If
        Dim sb As New StringBuilder
        If (ds.Tables(0).Rows.Count > 1) Then
            sb.AppendFormat("<option value={0}>{1}</option>", "", "Please select reason")
        End If
        For Each myrow As DataRow In ds.Tables(0).Rows
            sb.AppendFormat("<option value={0}>{1}</option>", myrow("PmtReasonId").ToString, myrow("PmtReasonText").ToString)
        Next
        Return sb.ToString
    End Function
End Class
