﻿Imports Aurora.Booking.Services
Imports System.Xml
Imports Aurora.Common.Logging

Partial Class Booking_EFTPOSListener_EftposPXPay
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim qs As String = Request.QueryString("Mode")
        Dim returnedvalue As String = ""
        Dim encoded As String = ""

        Dim PXPAY As BookingEFTPOSPxPay = Nothing
        Dim userCode As String = ""
        Dim rentalId As String = ""
        Dim country As String = ""
        If Not String.IsNullOrEmpty(qs) Then
            If qs.Equals("NewBillingToken") Then
                encoded = Request("NewBillingToken")
                Session("TokenBilling") = encoded

                userCode = encoded.Split("|")(2).TrimEnd
                rentalId = encoded.Split("|")(1).TrimEnd
                country = encoded.Split("|")(0).TrimEnd

                ''PXPAY = New BookingEFTPOSPxPay(encoded.Split("|")(0).TrimEnd, encoded.Split("|")(2).TrimEnd)
                ''PXPAY = New BookingEFTPOSPxPay(country, userCode)

                PXPAY = New BookingEFTPOSPxPay(userCode, rentalId, "NZ")
                LogDebug("EftposPXPayListener BookingEFTPOSPxPay(new): ", String.Concat(userCode, ",", rentalId, ",", country))
                LogDebug("EftposPXPayListener CaptureBillingToken: ", encoded.Split("|")(1).TrimEnd)
                returnedvalue = PXPAY.CaptureBillingToken(encoded.Split("|")(1).TrimEnd)
                If returnedvalue.IndexOf("ERROR") <> -1 Then
                    ''returnedvalue = returnedvalue.Replace("ERROR", "")
                Else
                    LogDebug("EftposPXPayListener ExractURI: ", returnedvalue)
                    returnedvalue = ExractURI(returnedvalue)
                End If

            ElseIf (qs.Equals("viewtoken")) Then
                encoded = Request("viewtoken")
                PXPAY = New BookingEFTPOSPxPay()
                returnedvalue = PXPAY.GetAllPXPAyToken(encoded)
            End If

        Else
            If Not (Request.QueryString("result") Is Nothing) Then
                encoded = Request.QueryString("result")
                Dim credentials As String = Session("TokenBilling")
                ''PXPAY = New BookingEFTPOSPxPay(credentials.Split("|")(0).TrimEnd, credentials.Split("|")(2).TrimEnd)

                userCode = credentials.Split("|")(2).TrimEnd
                rentalId = credentials.Split("|")(1).TrimEnd
                country = credentials.Split("|")(0).TrimEnd
                PXPAY = New BookingEFTPOSPxPay(userCode, rentalId, country)
                LogDebug("EftposPXPayListener BookingEFTPOSPxPay(new): ", String.Concat(userCode, ",", rentalId, ",", country))
                PXPAY.ProcessResponse(encoded)
                LogDebug("EftposPXPayListener ProcessResponse: ", encoded)

                ''returnedvalue = PXPAY.GetAllPXPAyToken(credentials.Split("|")(1).TrimEnd)
                returnedvalue = PXPAY.GetAllPXPAyToken(rentalId)
                LogDebug("EftposPXPayListener GetAllPXPAyToken: ", returnedvalue)
            End If
        End If

        Response.Write(returnedvalue)
        Response.End()
    End Sub


    Function EncodedString(ByVal xml As String) As String
        Return HttpUtility.UrlDecode(xml)
    End Function


    Function ExractURI(ByVal xml As String) As String
        Dim xmldoc As New XmlDocument
        xmldoc.LoadXml("<Data>" + xml + "</Data>")

        Dim tempValue As String = xmldoc.SelectSingleNode("Data/Request/URI").InnerText

        Return tempValue.Replace("&amp;", "&")
    End Function
End Class
