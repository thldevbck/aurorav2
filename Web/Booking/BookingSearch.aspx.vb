Imports Aurora.Common
Imports Aurora.Booking.Services
Imports System.Data
Imports system.Threading
Imports System.Globalization
Imports System.Drawing

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingSearch)> _
Partial Class Booking_BookingSearch
    Inherits AuroraPage

    Public Const BookingSearch_BookingStatus_SessionName As String = "BookingSearch_BookingStatus"
    Public Const BookingSearch_OldBookingNumber_SessionName As String = "BookingSearch_OldBookingNumber"
    Public Const BookingSearch_RentalStatus_SessionName As String = "BookingSearch_RentalStatus"
    Public Const BookingSearch_OldRentalNumber_SessionName As String = "BookingSearch_OldRentalNumber"
    Public Const BookingSearch_BrandCode_SessionName As String = "BookingSearch_BrandCode"
    Public Const BookingSearch_PackageId_SessionName As String = "BookingSearch_PackageId"
    Public Const BookingSearch_Package_SessionName As String = "BookingSearch_Package"
    Public Const BookingSearch_BookingCity_SessionName As String = "BookingSearch_BookingCity"
    Public Const BookingSearch_PickupFromDate_SessionName As String = "BookingSearch_PickupFromDate"
    Public Const BookingSearch_PickupToDate_SessionName As String = "BookingSearch_PickupToDate"
    Public Const BookingSearch_VehicleClass_SessionName As String = "BookingSearch_VehicleClass"
    Public Const BookingSearch_CheckOutLocationCodeId_SessionName As String = "BookingSearch_CheckOutLocationCodeId"
    Public Const BookingSearch_CheckOutLocationCode_SessionName As String = "BookingSearch_CheckOutLocationCode"
    Public Const BookingSearch_UnitNumber_SessionName As String = "BookingSearch_UnitNumber"
    Public Const BookingSearch_DropOffFromDate_SessionName As String = "BookingSearch_DropOffFromDate"
    Public Const BookingSearch_DropOffToDate_SessionName As String = "BookingSearch_DropOffToDate"
    Public Const BookingSearch_RegoNumber_SessionName As String = "BookingSearch_RegoNumber"
    Public Const BookingSearch_CheckInLocationCodeId_SessionName As String = "BookingSearch_CheckInLocationCodeId"
    Public Const BookingSearch_CheckInLocationCode_SessionName As String = "BookingSearch_CheckInLocationCode"
    Public Const BookingSearch_ArrivalConnectionRef_SessionName As String = "BookingSearch_ArrivalConnectionRef"
    Public Const BookingSearch_DepartureConnectionRef_SessionName As String = "BookingSearch_DepartureConnectionRef"
    Public Const BookingSearch_HirerLastName_SessionName As String = "BookingSearch_HirerLastName"
    Public Const BookingSearch_AgentCodeId_SessionName As String = "BookingSearch_AgentCodeId"
    Public Const BookingSearch_AgentCode_SessionName As String = "BookingSearch_AgentCode"
    Public Const BookingSearch_HirerAddress_SessionName As String = "BookingSearch_HirerAddress"
    Public Const BookingSearch_AgentRef_SessionName As String = "BookingSearch_AgentRef"
    Public Const BookingSearch_HirerState_SessionName As String = "BookingSearch_HirerState"
    Public Const BookingSearch_VoucherNumber_SessionName As String = "BookingSearch_VoucherNumber"
    Public Const BookingSearch_HirerTown_SessionName As String = "BookingSearch_HirerTown"
    Public Const BookingSearch_HirerCityId_SessionName As String = "BookingSearch_HirerCityId"
    Public Const BookingSearch_HirerCity_SessionName As String = "BookingSearch_HirerCity"
    Public Const BookingSearch_BookedFromDate_SessionName As String = "BookingSearch_BookedFromDate"
    Public Const BookingSearch_BookedToDate_SessionName As String = "BookingSearch_BookedToDate"
    Public Const BookingSearch_CustomerLastName_SessionName As String = "BookingSearch_CustomerLastName"
    Public Const BookingSearch_CustomerFirstName_SessionName As String = "BookingSearch_CustomerFirstName"
    Public Const BookingSearch_InError_SessionName As String = "BookingSearch_InError"
    Public Const BookingSearch_AgentType_SessionName As String = "BookingSearch_AgentType"
    Public Const BookingSearch_DebtorStatus_SessionName As String = "BookingSearch_DebtorStatus"
    Public Const BookingSearch_ProductShortNameId_SessionName As String = "BookingSearch_ProductShortNameId"
    Public Const BookingSearch_ProductShortName_SessionName As String = "BookingSearch_ProductShortName"
    Public Const BookingSearch_FleetModelId_SessionName As String = "BookingSearch_FleetModelId"
    Public Const BookingSearch_FleetModel_SessionName As String = "BookingSearch_FleetModel"
    Public Const BookingSearch_dvBookingSearch_SessionName As String = "BookingSearch_dvBookingSearch"

    Public Const BookingSearch_SearchNew_SessionName As String = "BookingSearch_SearchNew"

    'rev:mia july 4 2012 - start-- addition of 3rd party field
    Public Const BookingSearch_thirdPartyRef_SessionName As String = "BookingSearch_thirdPartyRef"

    Protected Overrides Sub OnInit(ByVal e As System.EventArgs)
        MyBase.OnInit(e)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not Page.IsPostBack Then

                typeDropDownList_dataBind()
                brandDropDownList_dataBind()
                bookingStatusDropDownList_dataBind()
                countryDropDownList_dataBind()
                'New search
                setDefaultValues()

                'If String.IsNullOrEmpty(Me.SearchParam) Then
                If Not Session.Item(BookingSearch_SearchNew_SessionName) Is Nothing And String.IsNullOrEmpty(Me.SearchParam) Then
                    setSessionValues()
                    bookingSearch()
                End If
                'End If

            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while loading the page.")
        End Try
    End Sub

    Private Sub setDefaultValues()
        Try
            pickUpFromDateControl.Date = DateTime.Today.AddYears(-1)
            pickUpToDateControl.Date = DateTime.Today.AddYears(1)
            countryDropDownList.SelectedValue = Utility.ParseString(CountryCode, "")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub setSessionValues()
        Try

            If Not Session.Item(BookingSearch_BookingStatus_SessionName) Is Nothing Then
                bookingStatusDropDownList.SelectedValue = Session.Item(BookingSearch_BookingStatus_SessionName)
            End If
            If Not Session.Item(BookingSearch_OldBookingNumber_SessionName) Is Nothing Then
                oldBookingNoTextBox.Text = Session.Item(BookingSearch_OldBookingNumber_SessionName)
            End If
            If Not Session.Item(BookingSearch_RentalStatus_SessionName) Is Nothing Then
                rentalStatusDropDownList.SelectedValue = Session.Item(BookingSearch_RentalStatus_SessionName)
            End If
            If Not Session.Item(BookingSearch_OldRentalNumber_SessionName) Is Nothing Then
                oldRentalNoTextBox.Text = Session.Item(BookingSearch_OldRentalNumber_SessionName)
            End If
            If Not Session.Item(BookingSearch_BrandCode_SessionName) Is Nothing Then
                brandDropDownList.SelectedValue = Session.Item(BookingSearch_BrandCode_SessionName)
            End If
            If Not Session.Item(BookingSearch_PackageId_SessionName) Is Nothing Then
                packagePickerControl.DataId = Session.Item(BookingSearch_PackageId_SessionName)
            End If
            If Not Session.Item(BookingSearch_Package_SessionName) Is Nothing Then
                packagePickerControl.Text = Session.Item(BookingSearch_Package_SessionName)
            End If
            If Not Session.Item(BookingSearch_BookingCity_SessionName) Is Nothing Then
                countryDropDownList.SelectedValue = Session.Item(BookingSearch_BookingCity_SessionName)
            End If
            If Not Session.Item(BookingSearch_PickupFromDate_SessionName) Is Nothing Then
                pickUpFromDateControl.Date = Session.Item(BookingSearch_PickupFromDate_SessionName)
            End If
            If Not Session.Item(BookingSearch_PickupToDate_SessionName) Is Nothing Then
                pickUpToDateControl.Date = Session.Item(BookingSearch_PickupToDate_SessionName)
            End If
            If Not Session.Item(BookingSearch_VehicleClass_SessionName) Is Nothing Then
                typeDropDownList.SelectedValue = Session.Item(BookingSearch_VehicleClass_SessionName)
            End If
            If Not Session.Item(BookingSearch_CheckOutLocationCodeId_SessionName) Is Nothing Then
                pickUpLocPickerControl.DataId = Session.Item(BookingSearch_CheckOutLocationCodeId_SessionName)
            End If
            If Not Session.Item(BookingSearch_CheckOutLocationCode_SessionName) Is Nothing Then
                pickUpLocPickerControl.Text = Session.Item(BookingSearch_CheckOutLocationCode_SessionName)
            End If
            If Not Session.Item(BookingSearch_UnitNumber_SessionName) Is Nothing Then
                unitTextBox.Text = Session.Item(BookingSearch_UnitNumber_SessionName)
            End If
            If Not Session.Item(BookingSearch_DropOffFromDate_SessionName) Is Nothing Then
                dropOffFromDateControl.Date = Session.Item(BookingSearch_DropOffFromDate_SessionName)
            End If
            If Not Session.Item(BookingSearch_DropOffToDate_SessionName) Is Nothing Then
                dropOffToDateControl.Date = Session.Item(BookingSearch_DropOffToDate_SessionName)
            End If
            If Not Session.Item(BookingSearch_RegoNumber_SessionName) Is Nothing Then
                regoTextBox.Text = Session.Item(BookingSearch_RegoNumber_SessionName)
            End If
            If Not Session.Item(BookingSearch_CheckInLocationCodeId_SessionName) Is Nothing Then
                dropOffLocPickerControl.DataId = Session.Item(BookingSearch_CheckInLocationCodeId_SessionName)
            End If
            If Not Session.Item(BookingSearch_CheckInLocationCode_SessionName) Is Nothing Then
                dropOffLocPickerControl.Text = Session.Item(BookingSearch_CheckInLocationCode_SessionName)
            End If
            If Not Session.Item(BookingSearch_ArrivalConnectionRef_SessionName) Is Nothing Then
                arrivalTextBox.Text = Session.Item(BookingSearch_ArrivalConnectionRef_SessionName)
            End If
            If Not Session.Item(BookingSearch_DepartureConnectionRef_SessionName) Is Nothing Then
                departureTextBox.Text = Session.Item(BookingSearch_DepartureConnectionRef_SessionName)
            End If
            If Not Session.Item(BookingSearch_HirerLastName_SessionName) Is Nothing Then
                hirerSurnameTextBox.Text = Session.Item(BookingSearch_HirerLastName_SessionName)
            End If
            If Not Session.Item(BookingSearch_AgentCodeId_SessionName) Is Nothing Then
                agentPickerControl.DataId = Session.Item(BookingSearch_AgentCodeId_SessionName)
            End If
            If Not Session.Item(BookingSearch_AgentCode_SessionName) Is Nothing Then
                agentPickerControl.Text = Session.Item(BookingSearch_AgentCode_SessionName)
            End If
            If Not Session.Item(BookingSearch_HirerAddress_SessionName) Is Nothing Then
                addressTextBox.Text = Session.Item(BookingSearch_HirerAddress_SessionName)
            End If
            If Not Session.Item(BookingSearch_AgentRef_SessionName) Is Nothing Then
                agentReferenceTextBox.Text = Session.Item(BookingSearch_AgentRef_SessionName)
            End If
            If Not Session.Item(BookingSearch_HirerState_SessionName) Is Nothing Then
                stateTextBox.Text = Session.Item(BookingSearch_HirerState_SessionName)
            End If
            If Not Session.Item(BookingSearch_VoucherNumber_SessionName) Is Nothing Then
                voucherNumberTextBox.Text = Session.Item(BookingSearch_VoucherNumber_SessionName)
            End If
            If Not Session.Item(BookingSearch_HirerTown_SessionName) Is Nothing Then
                cityTextBox.Text = Session.Item(BookingSearch_HirerTown_SessionName)
            End If
            If Not Session.Item(BookingSearch_HirerCityId_SessionName) Is Nothing Then
                countryPickerControl.DataId = Session.Item(BookingSearch_HirerCityId_SessionName)
            End If
            If Not Session.Item(BookingSearch_HirerCity_SessionName) Is Nothing Then
                countryPickerControl.Text = Session.Item(BookingSearch_HirerCity_SessionName)
            End If
            If Not Session.Item(BookingSearch_BookedFromDate_SessionName) Is Nothing Then
                bookedFromDateControl.Date = Session.Item(BookingSearch_BookedFromDate_SessionName)
            End If
            If Not Session.Item(BookingSearch_BookedToDate_SessionName) Is Nothing Then
                bookedToDateControl.Date = Session.Item(BookingSearch_BookedToDate_SessionName)
            End If
            If Not Session.Item(BookingSearch_CustomerLastName_SessionName) Is Nothing Then
                surnameTextBox.Text = Session.Item(BookingSearch_CustomerLastName_SessionName)
            End If
            If Not Session.Item(BookingSearch_CustomerFirstName_SessionName) Is Nothing Then
                firstNameTextBox.Text = Session.Item(BookingSearch_CustomerFirstName_SessionName)
            End If
            If Not Session.Item(BookingSearch_InError_SessionName) Is Nothing Then
                errorDropDownList.SelectedValue = Session.Item(BookingSearch_InError_SessionName)
            End If
            If Not Session.Item(BookingSearch_AgentType_SessionName) Is Nothing Then
                agentTypeDropDownList.SelectedValue = Session.Item(BookingSearch_AgentType_SessionName)
            End If
            If Not Session.Item(BookingSearch_DebtorStatus_SessionName) Is Nothing Then
                debtorStatusDropDownList.SelectedValue = Session.Item(BookingSearch_DebtorStatus_SessionName)
            End If
            If Not Session.Item(BookingSearch_ProductShortNameId_SessionName) Is Nothing Then
                bookedProductPickerControl.DataId = Session.Item(BookingSearch_ProductShortNameId_SessionName)
            End If
            If Not Session.Item(BookingSearch_ProductShortName_SessionName) Is Nothing Then
                bookedProductPickerControl.Text = Session.Item(BookingSearch_ProductShortName_SessionName)
            End If
            If Not Session.Item(BookingSearch_FleetModelId_SessionName) Is Nothing Then
                vehiclePickerControl.DataId = Session.Item(BookingSearch_FleetModelId_SessionName)
            End If
            If Not Session.Item(BookingSearch_FleetModel_SessionName) Is Nothing Then
                vehiclePickerControl.Text = Session.Item(BookingSearch_FleetModel_SessionName)
            End If

            'rev:mia july 4 2012 - start-- addition of 3rd party field
            If Not Session.Item(BookingSearch_thirdPartyRef_SessionName) Is Nothing Then
                txtthirdParty.Text = Session.Item(BookingSearch_thirdPartyRef_SessionName)
            End If


            'If Not Request.Cookies(BookingSearch_BookingStatus_SessionName) Is Nothing Then
            '    bookingStatusDropDownList.SelectedValue = Request.Cookies(BookingSearch_BookingStatus_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_OldBookingNumber_SessionName) Is Nothing Then
            '    oldBookingNoTextBox.Text = Request.Cookies(BookingSearch_OldBookingNumber_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_RentalStatus_SessionName) Is Nothing Then
            '    rentalStatusDropDownList.SelectedValue = Request.Cookies(BookingSearch_RentalStatus_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_OldRentalNumber_SessionName) Is Nothing Then
            '    oldRentalNoTextBox.Text = Request.Cookies(BookingSearch_OldRentalNumber_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_BrandCode_SessionName) Is Nothing Then
            '    brandDropDownList.SelectedValue = Request.Cookies(BookingSearch_BrandCode_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_PackageId_SessionName) Is Nothing Then
            '    packagePickerControl.DataId = Request.Cookies(BookingSearch_PackageId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_Package_SessionName) Is Nothing Then
            '    packagePickerControl.Text = Request.Cookies(BookingSearch_Package_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_BookingCity_SessionName) Is Nothing Then
            '    countryDropDownList.SelectedValue = Request.Cookies(BookingSearch_BookingCity_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_PickupFromDate_SessionName) Is Nothing Then
            '    pickUpFromDateControl.Date = Request.Cookies(BookingSearch_PickupFromDate_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_PickupToDate_SessionName) Is Nothing Then
            '    pickUpToDateControl.Date = Request.Cookies(BookingSearch_PickupToDate_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_VehicleClass_SessionName) Is Nothing Then
            '    typeDropDownList.SelectedValue = Request.Cookies(BookingSearch_VehicleClass_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_CheckOutLocationCodeId_SessionName) Is Nothing Then
            '    pickUpLocPickerControl.DataId = Request.Cookies(BookingSearch_CheckOutLocationCodeId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_CheckOutLocationCode_SessionName) Is Nothing Then
            '    pickUpLocPickerControl.Text = Request.Cookies(BookingSearch_CheckOutLocationCode_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_UnitNumber_SessionName) Is Nothing Then
            '    unitTextBox.Text = Request.Cookies(BookingSearch_UnitNumber_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_DropOffFromDate_SessionName) Is Nothing Then
            '    dropOffFromDateControl.Date = Request.Cookies(BookingSearch_DropOffFromDate_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_DropOffToDate_SessionName) Is Nothing Then
            '    dropOffToDateControl.Date = Request.Cookies(BookingSearch_DropOffToDate_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_RegoNumber_SessionName) Is Nothing Then
            '    regoTextBox.Text = Request.Cookies(BookingSearch_RegoNumber_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_CheckInLocationCodeId_SessionName) Is Nothing Then
            '    dropOffLocPickerControl.DataId = Request.Cookies(BookingSearch_CheckInLocationCodeId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_CheckInLocationCode_SessionName) Is Nothing Then
            '    dropOffLocPickerControl.Text = Request.Cookies(BookingSearch_CheckInLocationCode_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_ArrivalConnectionRef_SessionName) Is Nothing Then
            '    arrivalTextBox.Text = Request.Cookies(BookingSearch_ArrivalConnectionRef_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_DepartureConnectionRef_SessionName) Is Nothing Then
            '    departureTextBox.Text = Request.Cookies(BookingSearch_DepartureConnectionRef_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_HirerLastName_SessionName) Is Nothing Then
            '    hirerSurnameTextBox.Text = Request.Cookies(BookingSearch_HirerLastName_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_AgentCodeId_SessionName) Is Nothing Then
            '    agentPickerControl.DataId = Request.Cookies(BookingSearch_AgentCodeId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_AgentCode_SessionName) Is Nothing Then
            '    agentPickerControl.Text = Request.Cookies(BookingSearch_AgentCode_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_HirerAddress_SessionName) Is Nothing Then
            '    addressTextBox.Text = Request.Cookies(BookingSearch_HirerAddress_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_AgentRef_SessionName) Is Nothing Then
            '    agentReferenceTextBox.Text = Request.Cookies(BookingSearch_AgentRef_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_HirerState_SessionName) Is Nothing Then
            '    stateTextBox.Text = Request.Cookies(BookingSearch_HirerState_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_VoucherNumber_SessionName) Is Nothing Then
            '    voucherNumberTextBox.Text = Request.Cookies(BookingSearch_VoucherNumber_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_HirerTown_SessionName) Is Nothing Then
            '    cityTextBox.Text = Request.Cookies(BookingSearch_HirerTown_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_HirerCityId_SessionName) Is Nothing Then
            '    countryPickerControl.DataId = Request.Cookies(BookingSearch_HirerCityId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_HirerCity_SessionName) Is Nothing Then
            '    countryPickerControl.Text = Request.Cookies(BookingSearch_HirerCity_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_BookedFromDate_SessionName) Is Nothing Then
            '    bookedFromDateControl.Date = Request.Cookies(BookingSearch_BookedFromDate_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_BookedToDate_SessionName) Is Nothing Then
            '    bookedToDateControl.Date = Request.Cookies(BookingSearch_BookedToDate_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_CustomerLastName_SessionName) Is Nothing Then
            '    surnameTextBox.Text = Request.Cookies(BookingSearch_CustomerLastName_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_CustomerFirstName_SessionName) Is Nothing Then
            '    firstNameTextBox.Text = Request.Cookies(BookingSearch_CustomerFirstName_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_InError_SessionName) Is Nothing Then
            '    errorDropDownList.SelectedValue = Request.Cookies(BookingSearch_InError_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_AgentType_SessionName) Is Nothing Then
            '    agentTypeDropDownList.SelectedValue = Request.Cookies(BookingSearch_AgentType_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_DebtorStatus_SessionName) Is Nothing Then
            '    debtorStatusDropDownList.SelectedValue = Request.Cookies(BookingSearch_DebtorStatus_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_ProductShortNameId_SessionName) Is Nothing Then
            '    bookedProductPickerControl.DataId = Request.Cookies(BookingSearch_ProductShortNameId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_ProductShortName_SessionName) Is Nothing Then
            '    bookedProductPickerControl.Text = Request.Cookies(BookingSearch_ProductShortName_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_FleetModelId_SessionName) Is Nothing Then
            '    vehiclePickerControl.DataId = Request.Cookies(BookingSearch_FleetModelId_SessionName).Value
            'End If
            'If Not Request.Cookies(BookingSearch_FleetModel_SessionName) Is Nothing Then
            '    vehiclePickerControl.Text = Request.Cookies(BookingSearch_FleetModel_SessionName).Value
            'End If



        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub typeDropDownList_dataBind()
        Try
            Dim dt As DataTable
            dt = Booking.GetSrcClassComboData(UserCode)
            typeDropDownList.DataSource = dt
            typeDropDownList.DataTextField = "DESCRIPTION"
            typeDropDownList.DataValueField = "ID"
            typeDropDownList.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub brandDropDownList_dataBind()
        Try
           
            Dim dt As DataTable
            dt = Booking.GetBrandData(UserCode)
            brandDropDownList.DataSource = dt
            brandDropDownList.DataTextField = "BrdName"
            brandDropDownList.DataValueField = "BrdCode"
            brandDropDownList.DataBind()
            ''End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    ''-- RKS : MAC.COM : display all country
    Private Sub countryDropDownList_dataBind()
        Try


            
            Dim dt As DataTable
            dt = Booking.GetCountryData(UserCode)

            ''rev:mia Dec.7 2009 - code changes for this one is applicable only to 
            Dim item As New ListItem("", "")
            Me.countryDropDownList.Items.Add(item)
            countryDropDownList.AppendDataBoundItems = True

            countryDropDownList.DataSource = dt
            countryDropDownList.DataTextField = "CtyName"
            countryDropDownList.DataValueField = "CtyCode"
            countryDropDownList.DataBind()


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub bookingStatusDropDownList_dataBind()
        Try
            Dim dt As DataTable
            dt = Booking.GetBookingStatusList()

            dt.Columns.Add(New DataColumn("CodeName", GetType(String)))

            For Each r As DataRow In dt.Rows
                r.Item("CodeName") = r.Item("BookingStatusCode") & " - " & r.Item("BookingStatusName")
            Next

            dt.Rows.Add("0", "AL", "All Status", "All Status")

            bookingStatusDropDownList.DataSource = dt
            bookingStatusDropDownList.DataTextField = "CodeName"
            bookingStatusDropDownList.DataValueField = "BookingStatusCode"
            bookingStatusDropDownList.DataBind()

            rentalStatusDropDownList.DataSource = dt
            rentalStatusDropDownList.DataTextField = "CodeName"
            rentalStatusDropDownList.DataValueField = "BookingStatusCode"
            rentalStatusDropDownList.DataBind()

            SetDropDownListStatusColor(bookingStatusDropDownList)
            SetDropDownListStatusColor(rentalStatusDropDownList)

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Protected Sub searchButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles searchButton.Click

        bookingSearch()
    End Sub

    Private Sub bookingSearch()
        Try
            If (Not Page.IsPostBack) OrElse Page.IsValid Then
                Dim bookingStatus As String = bookingStatusDropDownList.SelectedValue
                Dim oldBookingNumber As String = oldBookingNoTextBox.Text
                Dim rentalStatus As String = rentalStatusDropDownList.SelectedValue
                Dim oldRentalNumber As String = oldRentalNoTextBox.Text
                Dim brandCode As String = brandDropDownList.SelectedValue
                Dim packageId As String = packagePickerControl.Text
                Dim bookingCity As String = countryDropDownList.SelectedValue
                'Dim pickupFromDate As DateTime
                'If String.IsNullOrEmpty(pickUpFromTextBox.Text) Then
                '    pickupFromDate = Nothing
                'Else
                '    pickupFromDate = Convert.ToDateTime(pickUpFromTextBox.Text)
                'End If

                'Dim pickupToDate As DateTime
                'If String.IsNullOrEmpty(pickUpToTextBox.Text) Then
                '    pickupToDate = Nothing
                'Else
                '    pickupToDate = Convert.ToDateTime(pickUpToTextBox.Text)
                'End If

                Dim vehicleClass As String = typeDropDownList.SelectedValue


                Dim checkOutLocationCode As String = pickUpLocPickerControl.Text
                If Not String.IsNullOrEmpty(pickUpLocPickerControl.DataId) Then
                    checkOutLocationCode = pickUpLocPickerControl.DataId
                    'ElseIf Not String.IsNullOrEmpty(pickUpLocPickerControl.Text) Then
                    '    'Valiate location code
                    '    Dim errorMessage As String = ""
                    '    checkOutLocationCode = getLocationCode(pickUpLocPickerControl.Text, errorMessage)
                    '    If Not String.IsNullOrEmpty(errorMessage) Then
                    '        AddErrorMessage(errorMessage)
                    '        Return
                    '    End If
                End If

                Dim unitNumber As String = unitTextBox.Text
                'Dim dropOffFromDate As DateTime
                'If String.IsNullOrEmpty(dropOffFromTextBox.Text) Then
                '    dropOffFromDate = Nothing
                'Else
                '    dropOffFromDate = Convert.ToDateTime(dropOffFromTextBox.Text)
                'End If
                'Dim dropOffToDate As DateTime
                'If String.IsNullOrEmpty(dropOffToTextBox.Text) Then
                '    dropOffToDate = Nothing
                'Else
                '    dropOffToDate = Convert.ToDateTime(dropOffToTextBox.Text)
                'End If
                Dim regoNumber As String = regoTextBox.Text


                Dim checkInLocationCode As String = dropOffLocPickerControl.Text

                If Not String.IsNullOrEmpty(dropOffLocPickerControl.DataId) Then
                    checkInLocationCode = dropOffLocPickerControl.DataId
                    'ElseIf Not String.IsNullOrEmpty(dropOffLocPickerControl.Text) Then
                    '    'Valiate location code
                    '    Dim errorMessage As String = ""
                    '    checkInLocationCode = getLocationCode(dropOffLocPickerControl.Text, errorMessage)
                    '    If Not String.IsNullOrEmpty(errorMessage) Then
                    '        AddErrorMessage(errorMessage)
                    '        Return
                    '    End If
                End If

                Dim arrivalConnectionRef As String = arrivalTextBox.Text
                Dim departureConnectionRef As String = departureTextBox.Text
                Dim hirerLastName As String = hirerSurnameTextBox.Text
                Dim agentCode As String = agentPickerControl.Text
                Dim hirerAddress As String = addressTextBox.Text
                Dim agentRef As String = agentReferenceTextBox.Text
                Dim hirerState As String = stateTextBox.Text
                Dim voucherNumber As String = voucherNumberTextBox.Text
                Dim hirerTown As String = cityTextBox.Text
                Dim hirerCity As String = countryPickerControl.Text
                If Not String.IsNullOrEmpty(countryPickerControl.DataId) Then
                    hirerCity = countryPickerControl.DataId
                End If

                'Dim bookedFromDate As DateTime
                'If String.IsNullOrEmpty(bookedFromTextBox.Text) Then
                '    bookedFromDate = Nothing
                'Else
                '    bookedFromDate = Convert.ToDateTime(bookedFromTextBox.Text)
                'End If
                'Dim bookedToDate As DateTime
                'If String.IsNullOrEmpty(bookedToTextBox.Text) Then
                '    bookedToDate = Nothing
                'Else
                '    bookedToDate = Convert.ToDateTime(bookedToTextBox.Text)
                'End If
                Dim customerLastName As String = surnameTextBox.Text
                Dim customerFirstName As String = firstNameTextBox.Text
                Dim inError As String = errorDropDownList.SelectedValue
                Dim agentType As String = agentTypeDropDownList.SelectedValue
                Dim debtorStatus As String = debtorStatusDropDownList.SelectedValue
                Dim productShortName As String = bookedProductPickerControl.Text
                If Not String.IsNullOrEmpty(bookedProductPickerControl.DataId) Then
                    productShortName = bookedProductPickerControl.DataId
                End If
                Dim fleetModel As String = vehiclePickerControl.Text
                If Not String.IsNullOrEmpty(vehiclePickerControl.DataId) Then
                    fleetModel = vehiclePickerControl.DataId
                End If

                'rev:mia july 4 2012 - start-- addition of 3rd party field
                Dim thirdparty As String = Me.txtthirdParty.Text

                Dim ds As DataSet
                ds = Booking.SearchBookingRental(bookingStatus, oldBookingNumber, rentalStatus, oldRentalNumber, _
                        brandCode, packageId, bookingCity, _
                        pickUpFromDateControl.Date, pickUpToDateControl.Date, vehicleClass, checkOutLocationCode, unitNumber, dropOffFromDateControl.Date, _
                        dropOffToDateControl.Date, regoNumber, checkInLocationCode, arrivalConnectionRef, departureConnectionRef, _
                        hirerLastName, agentCode, hirerAddress, agentRef, hirerState, voucherNumber, hirerTown, hirerCity, _
                        bookedFromDateControl.Date, bookedToDateControl.Date, customerLastName, customerFirstName, inError, agentType, debtorStatus, _
                        productShortName, fleetModel, UserCode, thirdparty) 'rev:mia july 4 2012 - start-- addition of 3rd party field

                setSessions(bookingStatus, oldBookingNumber, rentalStatus, oldRentalNumber, _
                        brandCode, packagePickerControl.DataId, packagePickerControl.Text, bookingCity, _
                        pickUpFromDateControl.Date, pickUpToDateControl.Date, vehicleClass, pickUpLocPickerControl.DataId, _
                        pickUpLocPickerControl.Text, unitNumber, dropOffFromDateControl.Date, _
                        dropOffToDateControl.Date, regoNumber, dropOffLocPickerControl.DataId, dropOffLocPickerControl.Text, arrivalConnectionRef, _
                        departureConnectionRef, hirerLastName, agentPickerControl.DataId, agentCode, hirerAddress, agentRef, hirerState, _
                        voucherNumber, hirerTown, countryPickerControl.DataId, countryPickerControl.Text, _
                        bookedFromDateControl.Date, bookedToDateControl.Date, customerLastName, _
                        customerFirstName, inError, agentType, debtorStatus, _
                        bookedProductPickerControl.DataId, bookedProductPickerControl.Text, _
                        vehiclePickerControl.DataId, vehiclePickerControl.Text, thirdparty) 'rev:mia july 4 2012 - start-- addition of 3rd party field

                'remove the existing session 
                Session.Remove(BookingSearch_dvBookingSearch_SessionName)

                If ds.Tables(0).Rows(0)(0) <> "" Then
                    SetShortMessage(AuroraHeaderMessageType.Warning, ds.Tables(0).Rows(0)(0))
                    'SetErrorMessage(ds.Tables(0).Rows(0)(0))

                End If

                If ds.Tables.Count < 2 Then ' ds.Tables(0).Rows(0)(0) = "GEN049/GEN049 - No Records Found" Then
                    '"GEN049/GEN049 - No Records Found"
                    'SetErrorMessage("No Records Found")

                    'Disable the sort radio button list
                    headerRadioButtonList.Enabled = False

                    'Clear gridview
                    resultGridView.DataSource = Nothing
                    resultGridView.DataBind()
                    resultLabel.Text = "No bookings found"

                Else

                    ' Create an Datetime typ  e col for sorting
                    Dim dt As DataTable
                    dt = ds.Tables(2)
                    dt.Columns.Add(New DataColumn("CKOWhenDate", GetType(DateTime)))

                    For Each dr As DataRow In dt.Rows
                        'Check out
                        'dr.Item("CKOWhenDate") = Convert.ToDateTime(dr.Item("CKOWhen")
                        Dim d As DateTime = Utility.ParseDateTime(dr.Item("CKOWhen"), Utility.SystemCulture)
                        dr.Item("CKOWhenDate") = d
                        dr.Item("CKOWhen") = d.ToShortDateString()

                        'Check in
                        dr.Item("CKIWhen") = Utility.ParseDateTime(dr.Item("CKIWhen"), Utility.SystemCulture).ToShortDateString()

                    Next

                    Dim dv As DataView
                    dv = New DataView(dt)

                    'Add the dataview to session
                    Session.Add(BookingSearch_dvBookingSearch_SessionName, dv)

                    'Sort the dv
                    dv.Sort = headerRadioButtonList.SelectedValue

                    If "CKOWhenDate" = headerRadioButtonList.SelectedValue Then
                        dv.Sort = dv.Sort & " desc"
                    End If

                    'Bind to gridview
                    resultGridView.DataSource = dv
                    resultGridView.DataBind()

                    headerRadioButtonList.Enabled = True

                    'If ds.Tables(0).Rows(0)(0) = "GEN022/GEN022 - Search Criteria  has more than 200 records" Then
                    '    SetWarningMessage("Search Criteria has more than 200 records")
                    'End If

                    resultLabel.Text = Utility.ParseString(dt.Rows.Count) & " bookings found"
                End If

                SetGridViewStatusColor()

                sortTable.Visible = True

            End If

        Catch ex As Exception
            LogException(ex)
            AddErrorMessage("An error occurred while searching bookings.")
        End Try
    End Sub

    Protected Sub customValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
        Try
            Dim pickUpFromDate As Date = pickUpFromDateControl.Date
            Dim pickUpToDate As Date = pickUpToDateControl.Date
            Dim dropOffFromDate As Date = dropOffFromDateControl.Date
            Dim dropOffToDate As Date = dropOffToDateControl.Date
            Dim bookedFromDate As Date = bookedFromDateControl.Date
            Dim bookedToDate As Date = bookedToDateControl.Date


            If Not (pickUpFromDateControl.IsValid And pickUpToDateControl.IsValid And dropOffFromDateControl.IsValid And _
                dropOffToDateControl.IsValid And bookedFromDateControl.IsValid And bookedToDateControl.IsValid) Then
                e.IsValid = False
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
                Return
            End If

            If String.IsNullOrEmpty(pickUpFromDateControl.Text) Or String.IsNullOrEmpty(pickUpToDateControl.Text) Then
                e.IsValid = True
            ElseIf pickUpFromDate <= pickUpToDate Then
                'If Not compareDates(pickUpFromDate, pickUpToDate) Then
                '    e.IsValid = False
                '    Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
                '    Return
                'Else
                '    e.IsValid = True

                'End If
            Else
                e.IsValid = False
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Check Out To date cannot be before the Check Out From date.")
                Return
            End If



            If String.IsNullOrEmpty(dropOffFromDateControl.Text) Or String.IsNullOrEmpty(dropOffToDateControl.Text) Then
                e.IsValid = True
            ElseIf dropOffFromDate <= dropOffToDate Then
                'If Not compareDates(dropOffFromDate, dropOffToDate) Then
                '    e.IsValid = False
                '    Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
                '    Return
                'Else

                '    e.IsValid = True
                'End If
            Else
                e.IsValid = False
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Check In To date cannot be before the Check Out From date.")
                Return
            End If



            If String.IsNullOrEmpty(bookedFromDateControl.Text) Or String.IsNullOrEmpty(bookedToDateControl.Text) Then
                e.IsValid = True
            ElseIf bookedFromDate <= bookedToDate Then
                'If Not compareDates(bookedFromDate, bookedToDate) Then
                '    e.IsValid = False
                '    Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
                '    Return
                'Else
                '    e.IsValid = True
                'End If
            Else
                e.IsValid = False
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Booked To date cannot be before the Booked From date.")
                Return
            End If

            'Search must contain at least one pair of dates.
            If Not (((Not String.IsNullOrEmpty(pickUpFromDateControl.Text)) And (Not String.IsNullOrEmpty(pickUpToDateControl.Text))) Or _
                ((Not String.IsNullOrEmpty(dropOffFromDateControl.Text)) And (Not String.IsNullOrEmpty(dropOffToDateControl.Text))) Or _
                ((Not String.IsNullOrEmpty(bookedFromDateControl.Text)) And (Not String.IsNullOrEmpty(bookedToDateControl.Text)))) Then

                e.IsValid = False
                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Search must contain at least one pair of dates.")
                Return
            End If


            'Must have at least one pair
            If compareDates(pickUpFromDate, pickUpToDate) And _
                pickUpFromDateControl.Date <> Date.MinValue And pickUpToDateControl.Date <> Date.MinValue Then
                e.IsValid = True
            Else
                If compareDates(dropOffFromDate, dropOffToDate) And _
                    dropOffFromDate.Date <> Date.MinValue And dropOffToDate.Date <> Date.MinValue Then
                    e.IsValid = True
                Else
                    If compareDates(bookedFromDate, bookedToDate) And _
                        bookedFromDate.Date <> Date.MinValue And bookedToDate.Date <> Date.MinValue Then
                        e.IsValid = True
                    Else
                        e.IsValid = False
                        Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
                        Return
                    End If
                End If
            End If


            'Dim pickUpFromString As String = pickUpFromTextBox.Text
            'Dim pickUpToString As String = pickUpToTextBox.Text

            'If Not (String.IsNullOrEmpty(pickUpFromString) And String.IsNullOrEmpty(pickUpToString)) Then
            '    Dim pickUpFrom As DateTime = Convert.ToDateTime(pickUpFromString)
            '    Dim pickUpTo As DateTime = Convert.ToDateTime(pickUpToString)

            '    If pickUpFrom <= pickUpTo Then
            '        e.IsValid = True
            '    Else
            '        e.IsValid = False
            '        SetErrorMessage("Check Out To date cannot be before the Check Out From date.")
            '    End If
            'Else
            '    e.IsValid = True
            'End If

        Catch ex As Exception
            e.IsValid = False
            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Date is invalid.")
        End Try
    End Sub

    'Protected Sub customValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
    '    Try
    '        Dim pickUpFromDate As Date = pickUpFromDateControl.Date
    '        Dim pickUpToDate As Date = pickUpToDateControl.Date
    '        Dim dropOffFromDate As Date = dropOffFromDateControl.Date
    '        Dim dropOffToDate As Date = dropOffToDateControl.Date
    '        Dim bookedFromDate As Date = bookedFromDateControl.Date
    '        Dim bookedToDate As Date = bookedToDateControl.Date


    '        If Not (pickUpFromDateControl.IsValid And pickUpToDateControl.IsValid And dropOffFromDateControl.IsValid And _
    '            dropOffToDateControl.IsValid And bookedFromDateControl.IsValid And bookedToDateControl.IsValid) Then
    '            e.IsValid = False
    '            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Enter date in " & Aurora.Common.UserSettings.Current.ComDateFormat & " format.")
    '            Return
    '        End If

    '        If String.IsNullOrEmpty(pickUpFromDateControl.Text) Or String.IsNullOrEmpty(pickUpToDateControl.Text) Then
    '            e.IsValid = True
    '        ElseIf pickUpFromDate <= pickUpToDate Then
    '            If Not compareDates(pickUpFromDate, pickUpToDate) Then
    '                e.IsValid = False
    '                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
    '                Return
    '            Else
    '                e.IsValid = True

    '            End If
    '        Else
    '            e.IsValid = False
    '            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Check Out To date cannot be before the Check Out From date.")
    '            Return
    '        End If



    '        If String.IsNullOrEmpty(dropOffFromDateControl.Text) Or String.IsNullOrEmpty(dropOffToDateControl.Text) Then
    '            e.IsValid = True
    '        ElseIf dropOffFromDate <= dropOffToDate Then
    '            If Not compareDates(dropOffFromDate, dropOffToDate) Then
    '                e.IsValid = False
    '                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
    '                Return
    '            Else

    '                e.IsValid = True
    '            End If
    '        Else
    '            e.IsValid = False
    '            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Check In To date cannot be before the Check Out From date.")
    '            Return
    '        End If



    '        If String.IsNullOrEmpty(bookedFromDateControl.Text) Or String.IsNullOrEmpty(bookedToDateControl.Text) Then
    '            e.IsValid = True
    '        ElseIf bookedFromDate <= bookedToDate Then
    '            If Not compareDates(bookedFromDate, bookedToDate) Then
    '                e.IsValid = False
    '                Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Maximum search period of two years exceeded. Please modify your search criteria.")
    '                Return
    '            Else
    '                e.IsValid = True
    '            End If
    '        Else
    '            e.IsValid = False
    '            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Booked To date cannot be before the Booked From date.")
    '            Return
    '        End If

    '        'Search must contain at least one pair of dates.
    '        If Not (((Not String.IsNullOrEmpty(pickUpFromDateControl.Text)) And (Not String.IsNullOrEmpty(pickUpToDateControl.Text))) Or _
    '            ((Not String.IsNullOrEmpty(dropOffFromDateControl.Text)) And (Not String.IsNullOrEmpty(dropOffToDateControl.Text))) Or _
    '            ((Not String.IsNullOrEmpty(bookedFromDateControl.Text)) And (Not String.IsNullOrEmpty(bookedToDateControl.Text)))) Then

    '            e.IsValid = False
    '            Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Search must contain at least one pair of dates.")
    '            Return
    '        End If


    '        'Dim pickUpFromString As String = pickUpFromTextBox.Text
    '        'Dim pickUpToString As String = pickUpToTextBox.Text

    '        'If Not (String.IsNullOrEmpty(pickUpFromString) And String.IsNullOrEmpty(pickUpToString)) Then
    '        '    Dim pickUpFrom As DateTime = Convert.ToDateTime(pickUpFromString)
    '        '    Dim pickUpTo As DateTime = Convert.ToDateTime(pickUpToString)

    '        '    If pickUpFrom <= pickUpTo Then
    '        '        e.IsValid = True
    '        '    Else
    '        '        e.IsValid = False
    '        '        SetErrorMessage("Check Out To date cannot be before the Check Out From date.")
    '        '    End If
    '        'Else
    '        '    e.IsValid = True
    '        'End If

    '    Catch ex As Exception
    '        e.IsValid = False
    '        Me.SetShortMessage(AuroraHeaderMessageType.Warning, "Date is invalid.")
    '    End Try
    'End Sub

    'Protected Sub dropOffCustomValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
    '    Try
    '        Dim dropOffFromDate As Date = dropOffFromDateControl.Date
    '        Dim dropOffToDate As Date = dropOffToDateControl.Date

    '        If String.IsNullOrEmpty(dropOffFromDateControl.Text) Or String.IsNullOrEmpty(dropOffToDateControl.Text) Then
    '            e.IsValid = True
    '        ElseIf dropOffFromDate <= dropOffToDate Then
    '            e.IsValid = True
    '        Else
    '            e.IsValid = False
    '            SetErrorMessage("Check In To date cannot be before the Check Out From date.")
    '        End If

    '        'Dim dropOffFromString As String = dropOffFromTextBox.Text
    '        'Dim dropOffToString As String = dropOffToTextBox.Text

    '        'If Not (String.IsNullOrEmpty(dropOffFromString) And String.IsNullOrEmpty(dropOffToString)) Then
    '        '    Dim dropOffFrom As DateTime = Convert.ToDateTime(dropOffFromString)
    '        '    Dim dropOffTo As DateTime = Convert.ToDateTime(dropOffToString)

    '        '    If dropOffFrom <= dropOffTo Then
    '        '        e.IsValid = True
    '        '    Else
    '        '        e.IsValid = False
    '        '        SetErrorMessage("Check In To date cannot be before the Check In From date.")
    '        '    End If
    '        'Else
    '        '    e.IsValid = True
    '        'End If
    '    Catch ex As Exception
    '        e.IsValid = False
    '        SetErrorMessage("Check In date is invalid.")
    '    End Try
    'End Sub

    'Protected Sub bookingCustomValidator_OnServerValidate(ByVal sender As Object, ByVal e As ServerValidateEventArgs)
    '    Try
    '        Dim bookedFromDate As Date = bookedFromDateControl.Date
    '        Dim bookedToDate As Date = bookedToDateControl.Date

    '        If String.IsNullOrEmpty(bookedFromDateControl.Text) Or String.IsNullOrEmpty(bookedToDateControl.Text) Then
    '            e.IsValid = True
    '        ElseIf bookedFromDate <= bookedToDate Then
    '            e.IsValid = True
    '        Else
    '            e.IsValid = False
    '            SetErrorMessage("Booked To date cannot be before the Booked From date.")
    '        End If
    '        'Dim bookedFromString As String = bookedFromTextBox.Text
    '        'Dim bookedToString As String = bookedToTextBox.Text

    '        'If Not (String.IsNullOrEmpty(bookedFromString) And String.IsNullOrEmpty(bookedToString)) Then
    '        '    Dim bookedFrom As DateTime = Convert.ToDateTime(bookedFromString)
    '        '    Dim bookedTo As DateTime = Convert.ToDateTime(bookedToString)

    '        '    If bookedFrom <= bookedTo Then
    '        '        e.IsValid = True
    '        '    Else
    '        '        e.IsValid = False
    '        '        SetErrorMessage("Booked To date cannot be before the Booked From date.")
    '        '    End If
    '        'Else
    '        '    e.IsValid = True
    '        'End If
    '    Catch ex As Exception
    '        e.IsValid = False
    '        SetErrorMessage("Booked date is invalid.")
    '    End Try
    'End Sub

    Protected Sub headerRadioButtonList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles headerRadioButtonList.SelectedIndexChanged
        Try
            If Not Session.Item(BookingSearch_dvBookingSearch_SessionName) Is Nothing Then
                Dim dv As DataView
                dv = Session.Item(BookingSearch_dvBookingSearch_SessionName)

                dv.Sort = headerRadioButtonList.SelectedValue

                If "CKOWhenDate" = headerRadioButtonList.SelectedValue Then
                    dv.Sort = dv.Sort & " desc"
                End If

                resultGridView.DataSource = dv
                resultGridView.DataBind()

                SetGridViewStatusColor()

            Else
                'session has been expired - research again
                'Me.SetShortMessage(AuroraHeaderMessageType.Error, "The session has been expired, please search again.")
                bookingSearch()
            End If
        Catch ex As Exception
            LogException(ex)
            Me.SetShortMessage(AuroraHeaderMessageType.Error, "An error occurred while sorting the bookings.")
        End Try
    End Sub

    Protected Function GetBookingUrl(ByVal bookingId As Object, ByVal rentalId As Object) As String

        Dim bookingIdString As String
        bookingIdString = Convert.ToString(bookingId)
        Dim rentalIdString As String
        rentalIdString = Convert.ToString(rentalId)
        'bookingIdString = bookingIdString.Replace("*", "")

        ' Return "./Booking.aspx?funCode=RS-BOKSUMMGT&sTxtSearch=" & bookingIdString
        Return "./Booking.aspx?hdBookingId=" & bookingIdString & "&hdRentalId=" & rentalId & "&isFromSearch=0"
    End Function

    Protected Sub cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancelButton.Click
        'Session.Remove(BookingSearch_SearchNew_SessionName)
        Response.Redirect("./BookingSearch.aspx?sTxtSearch=new")

    End Sub

    Private Function getLocationCode(ByVal text As String, ByRef errorMessage As String) As String

        Dim dr As DataRow = Aurora.Common.Service.searchPopupItem("LOCATION", "Location", text, errorMessage, UserCode)
        If Not dr Is Nothing Then
            Return dr("CODE")
        Else
            Return Nothing
        End If

    End Function

   
    Private Sub setSessions(ByVal bookingStatus As String, ByVal oldBookingNumber As String, _
         ByVal rentalStatus As String, ByVal oldRentalNumber As String, ByVal brandCode As String, _
         ByVal packageId As String, ByVal package As String, ByVal bookingCity As String, _
         ByVal pickupFromDate As DateTime, ByVal pickupToDate As DateTime, ByVal vehicleClass As String, _
         ByVal checkOutLocationCodeId As String, ByVal checkOutLocationCode As String, _
         ByVal unitNumber As String, ByVal dropOffFromDate As DateTime, ByVal dropOffToDate As DateTime, _
         ByVal regoNumber As String, ByVal checkInLocationCodeId As String, ByVal checkInLocationCode As String, _
         ByVal arrivalConnectionRef As String, ByVal departureConnectionRef As String, _
         ByVal hirerLastName As String, ByVal agentCodeId As String, ByVal agentCode As String, _
         ByVal hirerAddress As String, ByVal agentRef As String, ByVal hirerState As String, _
         ByVal voucherNumber As String, ByVal hirerTown As String, ByVal hirerCityId As String, _
         ByVal hirerCity As String, ByVal bookedFromDate As DateTime, ByVal bookedToDate As DateTime, _
         ByVal customerLastName As String, ByVal customerFirstName As String, ByVal inError As String, _
         ByVal agentType As String, ByVal debtorStatus As String, ByVal productShortNameId As String, _
         ByVal productShortName As String, ByVal fleetModelId As String, ByVal fleetModel As String, Optional ByVal thirdpartyref As String = "")

        Session.Remove(BookingSearch_BookingStatus_SessionName)
        Session.Remove(BookingSearch_OldBookingNumber_SessionName)
        Session.Remove(BookingSearch_RentalStatus_SessionName)
        Session.Remove(BookingSearch_OldRentalNumber_SessionName)
        Session.Remove(BookingSearch_BrandCode_SessionName)
        Session.Remove(BookingSearch_PackageId_SessionName)
        Session.Remove(BookingSearch_Package_SessionName)
        Session.Remove(BookingSearch_BookingCity_SessionName)
        Session.Remove(BookingSearch_PickupFromDate_SessionName)
        Session.Remove(BookingSearch_PickupToDate_SessionName)
        Session.Remove(BookingSearch_VehicleClass_SessionName)
        Session.Remove(BookingSearch_CheckOutLocationCodeId_SessionName)
        Session.Remove(BookingSearch_CheckOutLocationCode_SessionName)
        Session.Remove(BookingSearch_UnitNumber_SessionName)
        Session.Remove(BookingSearch_DropOffFromDate_SessionName)
        Session.Remove(BookingSearch_DropOffToDate_SessionName)
        Session.Remove(BookingSearch_RegoNumber_SessionName)
        Session.Remove(BookingSearch_CheckInLocationCodeId_SessionName)
        Session.Remove(BookingSearch_CheckInLocationCode_SessionName)
        Session.Remove(BookingSearch_ArrivalConnectionRef_SessionName)
        Session.Remove(BookingSearch_DepartureConnectionRef_SessionName)
        Session.Remove(BookingSearch_HirerLastName_SessionName)
        Session.Remove(BookingSearch_AgentCodeId_SessionName)
        Session.Remove(BookingSearch_AgentCode_SessionName)
        Session.Remove(BookingSearch_HirerAddress_SessionName)
        Session.Remove(BookingSearch_AgentRef_SessionName)
        Session.Remove(BookingSearch_HirerState_SessionName)
        Session.Remove(BookingSearch_VoucherNumber_SessionName)
        Session.Remove(BookingSearch_HirerTown_SessionName)
        Session.Remove(BookingSearch_HirerCityId_SessionName)
        Session.Remove(BookingSearch_HirerCity_SessionName)
        Session.Remove(BookingSearch_BookedFromDate_SessionName)
        Session.Remove(BookingSearch_BookedToDate_SessionName)
        Session.Remove(BookingSearch_CustomerLastName_SessionName)
        Session.Remove(BookingSearch_CustomerFirstName_SessionName)
        Session.Remove(BookingSearch_InError_SessionName)
        Session.Remove(BookingSearch_AgentType_SessionName)
        Session.Remove(BookingSearch_DebtorStatus_SessionName)
        Session.Remove(BookingSearch_ProductShortNameId_SessionName)
        Session.Remove(BookingSearch_ProductShortName_SessionName)
        Session.Remove(BookingSearch_FleetModelId_SessionName)
        Session.Remove(BookingSearch_FleetModel_SessionName)
        Session.Remove(BookingSearch_SearchNew_SessionName)
        'rev:mia july 4 2012 - start-- addition of 3rd party field
        Session.Remove(BookingSearch_thirdPartyRef_SessionName)

        Session.Add(BookingSearch_BookingStatus_SessionName, bookingStatus)
        Session.Add(BookingSearch_OldBookingNumber_SessionName, oldBookingNumber)
        Session.Add(BookingSearch_RentalStatus_SessionName, rentalStatus)
        Session.Add(BookingSearch_OldRentalNumber_SessionName, oldRentalNumber)
        Session.Add(BookingSearch_BrandCode_SessionName, brandCode)
        Session.Add(BookingSearch_PackageId_SessionName, packageId)
        Session.Add(BookingSearch_Package_SessionName, package)
        Session.Add(BookingSearch_BookingCity_SessionName, bookingCity)
        Session.Add(BookingSearch_PickupFromDate_SessionName, pickupFromDate)
        Session.Add(BookingSearch_PickupToDate_SessionName, pickupToDate)
        Session.Add(BookingSearch_VehicleClass_SessionName, vehicleClass)
        Session.Add(BookingSearch_CheckOutLocationCodeId_SessionName, checkOutLocationCodeId)
        Session.Add(BookingSearch_CheckOutLocationCode_SessionName, checkOutLocationCode)
        Session.Add(BookingSearch_UnitNumber_SessionName, unitNumber)
        Session.Add(BookingSearch_DropOffFromDate_SessionName, dropOffFromDate)
        Session.Add(BookingSearch_DropOffToDate_SessionName, dropOffToDate)
        Session.Add(BookingSearch_RegoNumber_SessionName, regoNumber)
        Session.Add(BookingSearch_CheckInLocationCodeId_SessionName, checkInLocationCodeId)
        Session.Add(BookingSearch_CheckInLocationCode_SessionName, checkInLocationCode)
        Session.Add(BookingSearch_ArrivalConnectionRef_SessionName, arrivalConnectionRef)
        Session.Add(BookingSearch_DepartureConnectionRef_SessionName, departureConnectionRef)
        Session.Add(BookingSearch_HirerLastName_SessionName, hirerLastName)
        Session.Add(BookingSearch_AgentCodeId_SessionName, agentCodeId)
        Session.Add(BookingSearch_AgentCode_SessionName, agentCode)
        Session.Add(BookingSearch_HirerAddress_SessionName, hirerAddress)
        Session.Add(BookingSearch_AgentRef_SessionName, agentRef)
        Session.Add(BookingSearch_HirerState_SessionName, hirerState)
        Session.Add(BookingSearch_VoucherNumber_SessionName, voucherNumber)
        Session.Add(BookingSearch_HirerTown_SessionName, hirerTown)
        Session.Add(BookingSearch_HirerCityId_SessionName, hirerCityId)
        Session.Add(BookingSearch_HirerCity_SessionName, hirerCity)
        Session.Add(BookingSearch_BookedFromDate_SessionName, bookedFromDate)
        Session.Add(BookingSearch_BookedToDate_SessionName, bookedToDate)
        Session.Add(BookingSearch_CustomerLastName_SessionName, customerLastName)
        Session.Add(BookingSearch_CustomerFirstName_SessionName, customerFirstName)
        Session.Add(BookingSearch_InError_SessionName, inError)
        Session.Add(BookingSearch_AgentType_SessionName, agentType)
        Session.Add(BookingSearch_DebtorStatus_SessionName, debtorStatus)
        Session.Add(BookingSearch_ProductShortNameId_SessionName, productShortNameId)
        Session.Add(BookingSearch_ProductShortName_SessionName, productShortName)
        Session.Add(BookingSearch_FleetModelId_SessionName, fleetModelId)
        Session.Add(BookingSearch_FleetModel_SessionName, fleetModel)
        Session.Add(BookingSearch_SearchNew_SessionName, "")

        'rev:mia july 4 2012 - start-- addition of 3rd party field
        Session.Add(BookingSearch_thirdPartyRef_SessionName, thirdpartyref)

        'Me.Response.SetCookie(New HttpCookie(BookingSearch_BookingStatus_SessionName, bookingStatus))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_OldBookingNumber_SessionName, oldBookingNumber))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_RentalStatus_SessionName, rentalStatus))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_OldRentalNumber_SessionName, oldRentalNumber))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_BrandCode_SessionName, brandCode))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_PackageId_SessionName, packageId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_Package_SessionName, package))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_BookingCity_SessionName, bookingCity))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_PickupFromDate_SessionName, pickupFromDate))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_PickupToDate_SessionName, pickupToDate))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_VehicleClass_SessionName, vehicleClass))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_CheckOutLocationCodeId_SessionName, checkOutLocationCodeId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_CheckOutLocationCode_SessionName, checkOutLocationCode))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_UnitNumber_SessionName, unitNumber))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_DropOffFromDate_SessionName, dropOffFromDate))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_DropOffToDate_SessionName, dropOffToDate))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_RegoNumber_SessionName, regoNumber))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_CheckInLocationCodeId_SessionName, checkInLocationCodeId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_CheckInLocationCode_SessionName, checkInLocationCode))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_ArrivalConnectionRef_SessionName, arrivalConnectionRef))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_DepartureConnectionRef_SessionName, departureConnectionRef))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_HirerLastName_SessionName, hirerLastName))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_AgentCodeId_SessionName, agentCodeId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_AgentCode_SessionName, agentCode))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_HirerAddress_SessionName, hirerAddress))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_AgentRef_SessionName, agentRef))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_HirerState_SessionName, hirerState))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_VoucherNumber_SessionName, voucherNumber))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_HirerTown_SessionName, hirerTown))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_HirerCityId_SessionName, hirerCityId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_HirerCity_SessionName, hirerCity))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_BookedFromDate_SessionName, bookedFromDate))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_BookedToDate_SessionName, bookedToDate))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_CustomerLastName_SessionName, customerLastName))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_CustomerFirstName_SessionName, customerFirstName))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_InError_SessionName, inError))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_AgentType_SessionName, agentType))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_DebtorStatus_SessionName, debtorStatus))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_ProductShortNameId_SessionName, productShortNameId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_ProductShortName_SessionName, productShortName))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_FleetModelId_SessionName, fleetModelId))
        'Me.Response.SetCookie(New HttpCookie(BookingSearch_FleetModel_SessionName, fleetModel))


    End Sub

    Private Sub SetGridViewStatusColor()
        'Add color for status
        For Each r As GridViewRow In resultGridView.Rows
            r.Attributes.Add("style", "background-color: " + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(r.Cells(4).Text)))
        Next
    End Sub

    Private Sub SetDropDownListStatusColor(ByVal ddl As DropDownList)
        'Add color for status
        For Each i As ListItem In ddl.Items
            If Not String.IsNullOrEmpty(i.Value) AndAlso i.Value <> "AL" Then
                i.Attributes.Add("style", "background-color:" + ColorTranslator.ToHtml(DataConstants.GetBookingStatusColor(i.Value)))
            End If
        Next
    End Sub

    Protected Sub backButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles backButton.Click
        Response.Redirect(Me.BackUrl)
    End Sub


    Protected Function GetDateFormat(ByVal d As Object) As String
        Dim dString As String
        dString = Convert.ToString(d)
        If Not String.IsNullOrEmpty(dString) Then
            Return Utility.DateDBUIConvert(dString)
        Else
            Return ""
        End If
    End Function

    Private Function compareDates(ByVal d1 As Date, ByVal d2 As Date) As Boolean

        If d1 = Date.MinValue Or d2 = Date.MinValue Then
            Return True
        End If
        Dim tempDate As Date
        tempDate = d1.AddYears(2)
        If d2 > tempDate Then
            Return False
        Else
            Return True
        End If
    End Function


End Class
