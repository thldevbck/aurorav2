﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
    xmlns:asp="remove">
    
    <xsl:output method="html" indent="yes"/>

  
    
    <!--<xsl:template match="@* | node()">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
    </xsl:template>-->
    <xsl:variable name="AddOn" select="AddOn"/>
   
    
    <xsl:template match="/">
        <xsl:variable name="Notes" select="data/BookedProduct/Notes"/>
        
                <table border="1" class="dataTableGrid" id="priceMatchingTable">
                    
                    <tr bgcolor="#9acd32" valign="bottom">
                        <th style="text-align:center;">Type</th>
                        <th style="text-align:center;">THL&lt;br/&gt;Product</th>
                        <th style="text-align:center;">Original&lt;br/&gt;Price</th>
                        <th style="text-align:center;">Offered&lt;br/&gt;Price</th>
                        <th style="text-align:center;">%</th>
                        <th style="text-align:center;">Competitor&lt;br/&gt;Price</th>
                        <th style="text-align:center;">Competitor&lt;br/&gt;Product</th>
                        <th style="text-align:center;">Price&lt;br/&gt;Variance</th>
                        <th style="text-align:center;">Approved&lt;br/&gt;Price</th>
                    </tr>
                    <xsl:for-each select="data/BookedProduct">
                            <xsl:choose>
                                <xsl:when test="PriceMatchType ='Daily Rates'">
                                    <tr>
                                        <td colspan="9" style="text-align:center;">
                                            <b>
                                                Daily Rates
                                            </b>
                                        </td>
                                    </tr>
                                </xsl:when>
                                <xsl:when test="PriceMatchType ='DailySubTotal'">
                                    <tr bgcolor="#FFFF00" class="rowGrouping">
                                        <td style="width:180px;">
                                            <!--type-->
                                            <b>
                                                SubTotal
                                            </b>
                                        </td>
                                        <td style="width:100px;" align="center">
                                        </td>
                                        
                                        <td style="width:75px; text-align:center;" >
                                            <!--Orig. Price-->
                                            <b>
                                                <xsl:value-of select="OriginalPrice"/>
                                            </b>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Offered Price uneditable-->
                                            <b>
                                                <xsl:value-of select="DiscountedPrice"/>
                                            </b>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Comp. Price-->
                                            <b>
                                                <label name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}"></label>
                                            </b>
                                        </td>
                                        <td>
                                            <!--Comp. Product-->
                                        </td>
                                        <td style="width:75px; text-align:center;">
                                            <!--Price Variance-->
                                            <b>
                                                <label name="price_VarianceFor_{PrdShortName}_{position()}" class="pricevarianceSubTotalRow" style="width:50px;"/>    
                                            </b>
                                            
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Approved Price-->
                                            <b>
                                                <label name="approved_PriceFor_{PrdShortName}_{position()}" class="approvedpriceSubTotalRow" style="width:50px;"/>
                                            </b>

                                        </td>
                                    </tr>
                                </xsl:when>
                                <xsl:when test="PriceMatchType ='DailyTotal'">
                                    <tr bgcolor="#FFFF00" class="rowGrouping">
                                        <td style="width:180px;">
                                            <!--type-->
                                            <b>
                                                Total
                                            </b>
                                        </td>
                                        <td style="width:100px; text-align:center;">
                                            <!--THL Product-->
                                            <label class="HirePeriod">
                                                <xsl:value-of select="PrdShortName"/>
                                            </label>
                                             &#xA0; Hire Days
                                        </td>

                                        <td style="width:75px; text-align:center;" >
                                            <!--Orig. Price-->
                                            <b>
                                                <label class="DailytotalOrigPrice">
                                                    <xsl:value-of select="OriginalPrice"/>
                                                </label>
                                            </b>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Offered Price uneditable-->
                                            <b>
                                                <label class="DailytotalOfferedPrice">
                                                    <xsl:value-of select="DiscountedPrice"/>
                                                </label>
                                                
                                            </b>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <b>
                                                <label class="DailytotalPercentage">
                                                </label>
                                            </b>
                                            
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Comp. Price-->
                                            <b>
                                                <label name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}"></label>
                                            </b>
                                        </td>
                                        <td>
                                            <!--Comp. Product-->
                                        </td>
                                        <td>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <b>
                                                <label name="approved_PriceFor_{PrdShortName}_{position()}" class="BookingDailyTotalApprovedEOD" style="width:50px;"/>
                                            </b>
                                        </td>
                                    </tr>
                                        
                                </xsl:when>
                                <xsl:when test="PriceMatchType ='AddOns'">
                                    <tr>
                                        <td colspan="9" style="text-align:center;">
                                            <!--type-->
                                            <b>
                                                <xsl:value-of select="PriceMatchType"/>
                                            </b>
                                            (Please enter total price)
                                        </td>
                                        
                                    </tr>
                                </xsl:when>
                                <xsl:when test="PriceMatchType ='BookingTotal'">
                                    <tr bgcolor="#FFFF00" class="rowGrouping">
                                        <td style="width:140px;">
                                            <!--type-->
                                            <b>
                                                Booking Total
                                            </b>
                                        </td>
                                        <td style="width:100px;" align="center">
                                            <!--THL Product-->
                                        </td>

                                        <td style="width:75px; text-align:center;" >
                                            <!--Orig. Price-->
                                            <b>
                                                <label class="BookingtotalOrigPrice">
                                                    <xsl:value-of select="OriginalPrice"/>
                                                </label>

                                            </b>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Offered Price uneditable-->
                                            <b>
                                                <label class="BookingtotalOfferedPrice">
                                                    <xsl:value-of select="DiscountedPrice"/>
                                                </label>

                                            </b>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <b>
                                                <label class="BookingtotalPercentage">
                                                    <xsl:value-of select="@totalPercentage"/>
                                                </label>
                                                
                                            </b> 
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Comp. Price-->
                                            <b>
                                                <label name="Competitor_Pricefor_{PrdShortName}_{position()}" class="BookingTotalEOD"></label>
                                            </b>
                                        </td>
                                        <td>
                                            <!--Comp. Product-->
                                        </td>
                                        <td style="width:75px; text-align:center;">
                                            <!--Price Variance-->
                                            <b>
                                                <label name="price_VarianceFor_{PrdShortName}_{position()}" class="BookingTotalPriceVarianceEOD" style="width:50px;" />
                                            </b>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Approved Price-->
                                            <b>
                                            <label name="approved_PriceFor_{PrdShortName}_{position()}" class="BookingTotalApprovedEOD" style="width:50px;"/>
                                            </b>
                                        </td>
                                    </tr>
                                </xsl:when>
                                <xsl:otherwise>
                                    <tr  class="productMatchingItem" id="{PriceMDSeqId}_{PriceMMDSeqID}" name="{UOM}">
                                        <td style="width:140px; text-align:center;">
                                            <!--type-->
                                            <xsl:value-of select="PriceMatchType"/>
                                        </td>
                                        <td style="width:100px; text-align:center;">
                                            <!--THL Product-->
                                            <xsl:value-of select="PrdShortName"/>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Orig. Price-->
                                            <xsl:value-of select="OriginalPrice"/>
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            <!--Offered Price editable-->
                                            <label name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}" style="width:70px;">
                                                <xsl:value-of select="DiscountedPrice"/>
                                            </label>
                                            
                                            <!--<xsl:value-of select="DiscountedPrice"/>-->
                                        </td>
                                        <td style="width:75px; text-align:center;" >
                                            
                                                <xsl:if test="@Percentage > 0">
                                                    <xsl:value-of select="@Percentage"/>
                                                </xsl:if>
                                            
                                        </td>
                                        <td style="width:75px;" >
                                            <!--Comp. Price-->
                                            <xsl:choose>
                                                <xsl:when test="PriceMatchType = 'Vehicle' ">
                                                    <input maxlength="8" type="text" name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}" style="width:60px;" value="{CompetitorPrice}"/>
                                                </xsl:when>
                                                <xsl:when test="PriceMatchType = 'Insurance' and @insOum &gt; 1">
                                                    <input maxlength="8" type="text" name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}" style="width:60px;" value="{CompetitorPrice}"/>
                                                </xsl:when>
                                                <xsl:when test="PriceMatchType = 'Insurance'  and @insOum = '1' or @insOum = 1">
                                                    <input maxlength="8" type="text" name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}_oum" style="width:60px;" value="{CompetitorPrice}"/>
                                                </xsl:when>
                                                <xsl:when test="PriceMatchType = ''">
                                                    <input maxlength="8" type="text" name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{@addons}" style="width:60px;"  value="{CompetitorPrice}"/>
                                                </xsl:when>
                                            </xsl:choose>
                                            
                                          
                                            
                                        </td>
                                        <td style="width:75px;" >
                                            <!--Comp. Product-->
                                            <input maxlength="64" type="text" name="Competitor_productFor_{PrdShortName}_{position()}" class="nonthlproduct" style="width:120px;" value="{CompetitorProduct}"/>
                                            
                                        </td>
                                        <td style="text-align:center;" >
                                            <!--Price Variance-->
                                            <xsl:if test="PriceMatchType != ''">
                                                <label id="Competitor_Pricefor_{PrdShortName}_{position()}" class="_{PriceMatchType}" style="width:40px;">
                                                    <xsl:value-of select="PriceVariance"/>
                                                </label>
                                            </xsl:if>
                                            <xsl:if test="PriceMatchType = ''">
                                                <label id="Competitor_Pricefor_{PrdShortName}_{position()}" class="_{@addons}" style="width:40px;">
                                                    <xsl:value-of select="PriceVariance"/>
                                                </label>
                                            </xsl:if>
                                        </td>
                                        <td style="text-align:center;" >
                                            <!--Approved Price-->
                                            <xsl:choose>
                                                <xsl:when test="PriceMatchType = 'Vehicle'">
                                                    <input maxlength="8" id="Competitor_PriceFor_{PrdShortName}_Vehicle" class="approvedpriceSubTotal" style="width:60px;" value="{ApprovedPrice}"/>
                                                </xsl:when>
                                                <xsl:when test="PriceMatchType = 'Insurance' and @insOum &gt; 1">
                                                    <input maxlength="8" id="Competitor_PriceFor_{PrdShortName}_Insurance" class="approvedpriceSubTotal" style="width:60px;" value="{ApprovedPrice}"/>
                                                </xsl:when>
                                                <xsl:when test="PriceMatchType = 'Insurance'  and @insOum = '1' or @insOum = 1">
                                                    <!--<input maxlength="5" type="text" name="Competitor_Pricefor_{PrdShortName}_{position()}" class="{PriceMatchType}_oum" style="width:60px;" value="{CompetitorPrice}"/>-->
                                                    <input maxlength="8" id="Competitor_PriceFor_{PrdShortName}_Insurance" class="approvedpriceSubTotal_oum" style="width:60px;" value="{ApprovedPrice}"/>
                                                </xsl:when>
                                                <!--<xsl:when test="PriceMatchType = 'Insurance'">
                                                    <input maxlength="5" id="Competitor_PriceFor_{PrdShortName}_Insurance" class="approvedpriceSubTotal" style="width:60px;" value="{ApprovedPrice}"/>
                                                </xsl:when>-->
                                                <xsl:when test="PriceMatchType = 'AddOns'">
                                                    <input maxlength="8" id="Competitor_PriceFor_{PrdShortName}_AddOns" class="approvedprice{@addons}" style="width:60px;" value="{ApprovedPrice}"/>
                                                </xsl:when>
                                                <xsl:when test="PriceMatchType = ''">
                                                    <input maxlength="8" id="Competitor_PriceFor_{PrdShortName}_AddOns" class="approvedprice{@addons}" style="width:60px;" value="{ApprovedPrice}"/>
                                                </xsl:when>
                                            </xsl:choose>
                                        </td>
                                    </tr>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                    <tr valign='top' class='rowNotes'>
                        <td>
                            Notes:
                        </td>
                        <td colspan='8' >
                            <textarea name='priceMatchNoteDesc' rows='2' cols='113' class='priceMatchNoteDesc'>
                                <xsl:value-of select="$Notes"/>
                            </textarea>
                        </td>
                        
                    </tr>
                </table>
        <br/>
    </xsl:template>
</xsl:stylesheet>
