<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt">
	<xsl:template match="/">
		<HTML>
			<LINK rel="stylesheet" type="text/css" href="../include/thlstyle.css"/>
			<HEAD>
				<style>
	H2{
		page-break-after: always;
	}
	TD{
		FONT-SIZE: 8pt;
		FONT-FAMILY:Courier New		
	}
	A{
		FONT-SIZE: 8pt;
		FONT-FAMILY:Courier New		
	}

</style>
			</HEAD>
			<!--BODY LEFTMARGIN='0' RIGHTMARGIN='0'-->
			<BODY>
				<xsl:for-each select="/data/doc">
						<H1/>
						<TABLE WIDTH="90%" align="center" BORDER="0" CELLSPACING="2" CELLPADDING="2">
							<TR>
								<TD>
									<IMG>
										<xsl:attribute name="SRC"><xsl:value-of select="Header/brandlogo"/></xsl:attribute>
									</IMG>
								</TD>
								<TD WIDTH="2%"/>
								<TD align="right" width="70%">
									<B style="FONT-SIZE: 12pt">
										<xsl:value-of select="Header/ConfirmationTitle"/>
									</B>
									<BR/>
									<xsl:value-of select="Header/CompanyName"/>
									<BR/>
									<xsl:if test="Header/ABNNumber !=''">
										<xsl:value-of select="Header/ABNNumber"/>
										<BR/>
									</xsl:if>
									<xsl:call-template name="lf2br">
										<xsl:with-param name="StringToTransform" select="Header/address"/>
									</xsl:call-template>
									<BR/>
									<xsl:value-of select="Header/phone"/>
									<xsl:if test="Header/Phone0800 !='' "> or <xsl:value-of select="Header/Phone0800"/>
									</xsl:if>
									<BR/>
									<u>
										<xsl:value-of select="Header/Web"/>
									</u>&#160;&#160;
									<xsl:value-of select="Header/GST"/>
								</TD>
							</TR>
						</TABLE>
						<BR/>
						<TABLE BORDER="0" WIDTH="90%" align="center">
							<TR>
								<TD ALIGN="left" nowrap="nowrap">
									<B>Vehicle Status</B>
								</TD>
								<td>
									<xsl:value-of select="Header/VehicleStatus"/>
								</td>
								<td>
									<B>Date</B>
								</td>
								<td>
									<xsl:value-of select="Header/CfnDate"/>
								</td>
							</TR>
							<TR>
								<TD ALIGN="left">
									<B>Our Ref</B>
								</TD>
								<td>
									<xsl:value-of select="Header/OurRef"/>
								</td>
								<td>
									<B>Our Consultant</B>
								</td>
								<td>
									<xsl:value-of select="Header/OurConsultant"/>
								</td>
							</TR>
						</TABLE>
						<BR/>
						<table width="90%" align="center">
							<tr>
								<td>
									<xsl:value-of select="Header/RentalStatusText"/>
								</td>
							</tr>
							<!-- Pre-reg stuff -->
							<tr>
								<td>
									<br/>
								</td>
							</tr>
							<tr>
								<td>
								<xsl:if test="Header/Cty='NZ'">
									<xsl:if test="Header/Brand='X'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/> 
										<A> 
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=kx
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=kx</A></td></tr>
									</xsl:if>
									<xsl:if test="Header/Brand='M'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/>
										<A>
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=ma
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=ma</A></td></tr>
									</xsl:if>
									<xsl:if test="Header/Brand='B'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/>
										<A>
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bz
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bz</A></td></tr>
									</xsl:if>
									<xsl:if test="Header/Brand='P'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/>
										<A>
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bk
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bk</A></td></tr>
									</xsl:if>
								</xsl:if>
								<xsl:if test="Header/Cty='AU'">
									<xsl:if test="Header/Brand='M'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/>
										<A>
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=ma
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=ma</A></td></tr>
									</xsl:if>
									<xsl:if test="Header/Brand='B'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/>
										<A>
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bz
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bz</A></td></tr>
									</xsl:if>
									<xsl:if test="Header/Brand='P'">
										<tr><td colspan="2">You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click on the link below now to complete and submit your information:<br/>
										<A>
											<xsl:attribute name="href">
												http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bk
											</xsl:attribute>http://secure-vehicle-bookings.com/PreReg/prereg.aspx?bookingnumber=<xsl:value-of select="Header/OurRef"/>&amp;vc=bk</A></td></tr>
									</xsl:if>
								</xsl:if>
							</td>
							</tr>
							<!-- Pre-reg stuff -->
							<tr>
								<td>
									<br/>
								</td>
							</tr>
						</table>
						<xsl:if test="Audience='Agent'">
							<Table width="90%" align="center">
								<tr>
									<td>
										<B>Agent name</B>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/AgnName"/>
									</td>
									<td>
										<b>Email</b>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/AgentEmail"/>
									</td>
								</tr>
								<tr>
									<td>
										<B>Contact</B>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/ContactName"/>
									</td>
									<td>
										<b>Agent Ref</b>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/AgentRef"/>
									</td>
								</tr>
								<tr>
									<td>
										<B>Phone</B>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/AgentPhone"/>
									</td>
									<td>
										<b>Fax</b>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/AgentFax"/>
									</td>
								</tr>
								<tr>
									<td>
										<B>Customer</B>
									</td>
									<td>
										<xsl:value-of select="AgentDetails/Customer"/>
									</td>
								</tr>
							</Table>
						</xsl:if>
						<xsl:if test="Audience='Customer'">
							<TABLE id="Table1" cellSpacing="1" cellPadding="1" width="90%" border="0" align="center">
								<TR>
									<TD width="20%">
										<b>Customer</b>
									</TD>
									<TD width="30%">
										<xsl:value-of select="CustomerDetails/CustomerName"/>
									</TD>
									<TD width="20%">
										<b>Email</b>
									</TD>
									<TD width="30%">
										<xsl:value-of select="CustomerDetails/CustomerEmail"/>
									</TD>
								</TR>
								<TR>
									<TD valign="top" rowspan="3">
										<b>Address</b>
									</TD>
									<TD rowspan="3" valign="top">
										<xsl:call-template name="lf2br">
											<xsl:with-param name="StringToTransform" select="CustomerDetails/Address"/>
										</xsl:call-template>
									</TD>
									<TD>
										<b>Fax</b>
									</TD>
									<TD>
										<xsl:value-of select="CustomerDetails/CustomerFax"/>
									</TD>
								</TR>
								<TR>
									<TD>
										<b>Phone</b>
									</TD>
									<TD>
										<xsl:value-of select="CustomerDetails/CustomerPhone"/>
									</TD>
								</TR>
								<TR>
									<TD>&#160;</TD>
									<TD/>
								</TR>
							</TABLE>
						</xsl:if>
						<xsl:if test="PersonalNote !=''">
							<TABLE WIDTH="90%" align="center">
								<tr>
									<td>
										<br/>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										<xsl:if test="//data/highlight =1">									
											<b><h5><xsl:value-of select="PersonalNote"/></h5></b>
										</xsl:if>
										<xsl:if test="//data/highlight !=1">									
											<xsl:value-of select="PersonalNote"/>
										</xsl:if>
									</td>
								</tr>
								<tr>
									<td>
										<br/>
									</td>
								</tr>
							</TABLE>
						</xsl:if>
						<TABLE width="90%" align="center">
							<xsl:for-each select="CfnTextNotes/Note">
								<TR>
									<!--TD nowrap="nowrap" valign="top"-->
									<TD width="25%" valign="top">
										<xsl:if test="NtsType!=preceding-sibling::node()[position()=1]/NtsType or position()=1">
											<b>
												<xsl:value-of select="NtsType"/>
											</b>
										</xsl:if>
									</TD>
									<TD width="75%">
										<xsl:call-template name="lf2br">
											<xsl:with-param name="StringToTransform" select="NtsText"/>
										</xsl:call-template>
									</TD>
								</TR>
							</xsl:for-each>
									<tr>
										<td colspan="3">
											<hr color="black" width="100%"/>
										</td>
									</tr>
						</TABLE>
						<table width="90%" align="center">
							<tr>
								<td colspan="3">
									<B>Booking Details</B> - <xsl:value-of select="BookingDetails/Prd"/>
								</td>
							</tr>
							<tr>
								<td>
									<br/>
								</td>
							</tr>
							<tr>
								<td width="20%">
									<b>Check Out</b>
								</td>
								<td width="50%">
									<b>Address</b>
								</td>
								<td width="30%">
									<b>Phone</b>
								</td>
							</tr>
							<tr>
								<td nowrap="nowrap">
									<xsl:value-of select="BookingDetails/CkoWhen"/>
								</td>
								<td nowrap="nowrap">
									<xsl:value-of select="BookingDetails/CkoAddress"/>
								</td>
								<td nowrap="nowrap">
									<xsl:value-of select="BookingDetails/CkoPhone"/>
								</td>
							</tr>
							<!--<xsl:if test="BookingDetails/CkoStart[.!=../CkiStart or (../CkiFinish!=../CkoFinish)]">-->
								<tr>
									<td colspan="4" align="center" width="100%">
										<b>Operating Hours :&#160;<xsl:value-of select="BookingDetails/CkoDayName"/>&#160;										
										<xsl:value-of select="BookingDetails/CkoStart"/>
										&#160;to&#160;
										<xsl:value-of select="BookingDetails/CkoFinish"/>
									</b>
									</td>
								</tr>
							<!--</xsl:if>-->
							<tr>
								<td>
									<b>Check In</b>
								</td>
								<td/>
								<td/>
							</tr>
							<tr>
								<td nowrap="nowrap">
									<xsl:value-of select="BookingDetails/CkiWhen"/>
								</td>
								<td nowrap="nowrap">
									<xsl:value-of select="BookingDetails/CkiAddress"/>
								</td>
								<td nowrap="nowrap">
									<xsl:value-of select="BookingDetails/CkiPhone"/>
								</td>
							</tr>
							<tr>
								<td colspan="4" align="center" width="100%">
									<b>Operating Hours :&#160;<xsl:value-of select="BookingDetails/CkiDayName"/>&#160;										
										<xsl:value-of select="BookingDetails/CkiStart"/>
										&#160;to&#160;
										<xsl:value-of select="BookingDetails/CkiFinish"/>	
									</b>
								</td>
							</tr>
						</table>
						<BR/>
						<table border="0" width="90%" align="center">
							<xsl:for-each select="Agent/Curr">
								<xsl:sort select="text()"/>
								<xsl:variable name="Curr" select="normalize-space(text())"/>
								<xsl:for-each select="DescList/Desc">
									<xsl:variable name="pos" select="position()"/>
									<tr>
										<td valign="top" nowrap="nowrap" width="33%">
											<xsl:if test="@Typ='Dis'">&#160;&#160;</xsl:if>
											<xsl:value-of select="."/>
										</td>
										<td valign="top" width="10%" align="right">
											<xsl:if test="../../QtyList/Qty[position()=$pos]!=0">
												<xsl:value-of select="../../QtyList/Qty[position()=$pos]"/>&#160;&#160;
											</xsl:if>
										</td>
										<td valign="top" width="7%">
											<xsl:value-of select="../../UomList/Uom[position()=$pos]"/>
										</td>
										<td valign="top" width="17%">
											<xsl:if test="../../PersonList/Person[position()=$pos]!=''">
												<xsl:value-of select="../../PersonList/Person[position()=$pos]"/>
												<br/>
											</xsl:if>
											<xsl:if test="../../TravellerList/Traveller[position()=$pos]!=''">
												<xsl:call-template name="lf2br">
													<xsl:with-param name="StringToTransform" select="../../TravellerList/Traveller[position()=$pos]"/>
												</xsl:call-template>
											</xsl:if>
										</td>
										<td valign="top" width="7%">
											<xsl:value-of select="Label"/>
										</td>
										<td valign="top" width="10%" nowrap="nowrap" align="right">
											<xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])!=0">
												<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
											</xsl:if>
											<xsl:if test="(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])=0) and (../../TotalList/Total[position()=$pos]=0) and (../../QtyList/Qty[position()=$pos]!=0)">
												<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
											</xsl:if>
										</td>
										<td valign="top" width="10%">
											<xsl:value-of select="$Curr"/>
										</td>
										<td valign="top" width="10%" nowrap="nowrap" align="right">
											<!--xsl:if test="../../TotalList/Total[position()=$pos]!=0"-->
												<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
											<!--/xsl:if-->
										</td>
									</tr>
								</xsl:for-each>
								<tr>
									<td colspan="6" align="right">Total Charged</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<b>
											<xsl:variable name="Charges" select="TotalChg"/>
											<xsl:value-of select="format-number($Charges,'#.00')"/>
										</b>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="right">Less Agent Discount</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<xsl:value-of select="format-number(AgentDiscount,'#.00')"/>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="right">Less Payment Received</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<!--xsl:variable name="PaymentRecvd" select="0"/-->
										<xsl:variable name="PaymentRecvd" select="lessPmtRcvd"/>
										<xsl:if test="$PaymentRecvd=''">
											<xsl:value-of select="format-number(0,'#.00')"/>
										</xsl:if>
										<xsl:if test="$PaymentRecvd!=''">	
											<xsl:value-of select="format-number($PaymentRecvd,'#.00')"/>
										</xsl:if>
									</td>
								</tr>
								<tr/>
								<tr>
									<td colspan="6" align="right">
										<b>Total Payable (Inclusive of GST)</b>
									</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<xsl:variable name="TotalPayable" select="0"/>
										<b>
											<xsl:variable name="PaymentRecvd" select="lessPmtRcvd"/>
										<xsl:if test="$PaymentRecvd=''">
											<xsl:variable name="Charges" select="TotalChg"/>
											<xsl:value-of select="format-number(($Charges - AgentDiscount),'#.00')"/>
										</xsl:if>
										<xsl:if test="$PaymentRecvd!=''">
											<xsl:variable name="Charges" select="TotalChg"/>
											<xsl:value-of select="format-number(($Charges - AgentDiscount - $PaymentRecvd),'#.00')"/>
										</xsl:if>	
										</b>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="right">GST Content</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<xsl:value-of select="TotalGst"/>
									</td>
								</tr>
								<tr>
									<td>
										<br/>
									</td>
								</tr>
							</xsl:for-each>
							<xsl:if test="count(Customer/Curr/*)!=0">
								<tr>
									<td colspan="8">
										<hr color="black" width="100%"/>
									</td>
								</tr>
								<tr>
									<td colspan="8">
										<b>Customer to pay</b>
									</td>
								</tr>
							</xsl:if>
							<xsl:for-each select="Customer/Curr">
								<xsl:sort select="text()"/>
								<xsl:variable name="Curr" select="normalize-space(text())"/>
								<xsl:for-each select="DescList/Desc">
								<xsl:variable name="isDesc" select="."/>
								<xsl:if test="$isDesc!=''">
									<xsl:variable name="pos" select="position()"/>
								
									<tr>
										<td valign="top" nowrap="nowrap" width="33%">
											<xsl:if test="@Typ='Dis'">&#160;&#160;</xsl:if>
											<xsl:value-of select="."/>
										</td>
										<td valign="top" width="10%" align="right">
											<xsl:if test="../../QtyList/Qty[position()=$pos]!=0">
												<xsl:value-of select="../../QtyList/Qty[position()=$pos]"/>&#160;&#160;
											</xsl:if>
										</td>
										<td valign="top" width="7%">
											<xsl:value-of select="../../UomList/Uom[position()=$pos]"/>
										</td>
										<td valign="top" width="17%">
											<xsl:if test="../../TravellerList/Traveller[position()=$pos]!=''">
												<xsl:call-template name="lf2br">
													<xsl:with-param name="StringToTransform" select="../../TravellerList/Traveller[position()=$pos]"/>
												</xsl:call-template>
											</xsl:if>
											<xsl:if test="../../TravellerList/Traveller[position()=$pos]=''">
												<xsl:value-of select="../../PersonList/Person[position()=$pos]"/>
											</xsl:if>
										</td>
										<td valign="top" width="7%">
											<xsl:value-of select="Label"/>
										</td>
										<td valign="top" width="10%" nowrap="nowrap" align="right">
											<xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])!=0">
												<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
											</xsl:if>
											<xsl:if test="(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])=0) and (../../TotalList/Total[position()=$pos]=0) and (../../QtyList/Qty[position()=$pos]!=0)">
												<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
											</xsl:if>
										</td>
										<td valign="top" width="10%">
										
											<xsl:value-of select="$Curr"/>
									
										</td>
										<td valign="top" width="10%" nowrap="nowrap" align="right">
											<!--xsl:if test="../../TotalList/Total[position()=$pos]!=0"-->
												<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
											<!--/xsl:if-->
										</td>
									</tr>
									</xsl:if>
								</xsl:for-each>
								<tr/>
								<tr>
									<td colspan="6" align="right">Total Charged</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<b>
											<xsl:variable name="Charges" select="TotalChg"/>
											<xsl:value-of select="format-number($Charges,'#.00')"/>
										</b>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="right">Less Payment Received</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<xsl:variable name="PaymentRecvd" select="sum(../../Payments/*/pmt[../curr=$Curr])"/>
										<xsl:value-of select="format-number($PaymentRecvd,'#.00')"/>
									</td>
								</tr>
								<tr/>
								<tr>
									<td colspan="6" align="right">
										<b>Total Payable (Inclusive of GST)</b>
									</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<xsl:variable name="TotalPayable" select="0"/>
										<b>
											<xsl:variable name="Charges" select="TotalChg"/>
											<xsl:variable name="PaymentRecvd" select="sum(../../Payments/*/pmt[../curr=$Curr])"/>
											<xsl:value-of select="format-number(($Charges)-$PaymentRecvd,'#.00')"/>
										</b>
									</td>
								</tr>
								<tr>
									<td colspan="6" align="right">GST Content</td>
									<td>
										<xsl:value-of select="$Curr"/>
									</td>
									<td nowrap="nowrap" align="right">
										<xsl:value-of select="TotalGst"/>
									</td>
								</tr>
								<tr>
									<td>
										<br/>
									</td>
								</tr>
							</xsl:for-each>
						</table>
						<TABLE width="90%" align="center">
							<tr>
								<td>
									<br/>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<hr width="100%" color="black"/>
								</td>
							</tr>
							<tr>
								<td colspan="2">
									<xsl:if test="//data/highlight = 1">
										<b><h5><xsl:value-of select="FooterNote"/></h5></b>
									</xsl:if>
									<xsl:if test="//data/highlight != 1">
										<xsl:value-of select="FooterNote"/>
									</xsl:if>
								</td>
							</tr>
							<xsl:if test="CfnFooterNotes/NewsFlash != ''">
								<TR>
									<TD colspan="2">
										<i>
											<xsl:value-of select="CfnFooterNotes/NewsFlash"/>
										</i>
									</TD>
								</TR>
								<tr>
									<td>
										<br/>
									</td>
								</tr>
							</xsl:if>
							<xsl:for-each select="CfnFooterNotes/Note">
								<TR>
									<!--TD valign="top" nowrap="nowrap" width="20%"-->
									<TD valign="top" width="25%">
										<xsl:if test="NtsType!=preceding-sibling::node()[position()=1]/NtsType or position()=1">
											<b>
												<xsl:value-of select="NtsType"/>
											</b>
										</xsl:if>
									</TD>
									<TD width="75%">
										<span>
											<xsl:value-of select="NtsText"/>
										</span>
									</TD>
								</TR>
								<tr>
									<td>
										<br/>
									</td>
								</tr>
							</xsl:for-each>
							<tr><td colspan="2">&#160;</td></tr>
							<!--<xsl:if test="Header/Cty='NZ'">
								<xsl:if test="Header/Brand='B'">
									<tr><td colspan="2"><B><font style="color:red; font-size:10pt" >You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click <A href="http://www.britz.com/rentquicknz">www.britz.com/rentquicknz</A> now to complete and submit your information</font></B></td></tr>
								</xsl:if>
								<xsl:if test="Header/Brand='M'">
									<tr><td colspan="2"><B><font style="color:red; font-size:10pt" >You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click <A href="http://maui.geni.gotdns.com/PreRegistrationNZ_Form.htm">www.maui-rentals.com/rentquicknz</A> now to complete and submit your information</font></B></td></tr>
								</xsl:if>
							</xsl:if>
							<xsl:if test="Header/Cty='AU'">
								<xsl:if test="Header/Brand='B'">
									<tr><td colspan="2"><B><font style="color:red; font-size:10pt" >You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click <A href="http://www.britz.com/rentquickau">www.britz.com/rentquickau</A> now to complete and submit your information</font></B></td></tr>
								</xsl:if>
								<xsl:if test="Header/Brand='M'">
									<tr><td colspan="2"><B><font style="color:red; font-size:10pt" >You can register your customer details online now to assist in ensuring a speedy service when you collect your vehicle. Click <A href="http://maui.geni.gotdns.com/PreRegistrationAUS_Form.htm">www.maui-rentals.com/rentquickau</A> now to complete and submit your information</font></B></td></tr>
								</xsl:if>
							</xsl:if>-->
						</TABLE>
					<xsl:if test="position()!=last()">
						<H2/>
					</xsl:if>
				</xsl:for-each>
			</BODY>
		</HTML>
	</xsl:template>
	<xsl:template name="lf2br">
		<xsl:param name="StringToTransform"/>
		<xsl:choose>
			<xsl:when test="contains($StringToTransform,'[NL]')">
				<xsl:value-of select="substring-before($StringToTransform,'[NL]')"/>
				<br/>
				<xsl:call-template name="lf2br">
					<xsl:with-param name="StringToTransform">
						<xsl:value-of select="substring-after($StringToTransform,'[NL]')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$StringToTransform"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
