<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version='1.0' xmlns:xsl='http://www.w3.org/1999/XSL/Transform'>
	<xsl:template match='/'>
		<BookedProducts>
			<xsl:for-each select='Data/BookedProducts/BookedProduct'>
				<xsl:sort select='IsVehicle' data-type='number' order='ascending'/>
				<xsl:copy>
					<xsl:apply-templates select='*'/>
				</xsl:copy>
			</xsl:for-each>
		</BookedProducts>
	</xsl:template>
	<xsl:template match='*'>
		<xsl:copy>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>