<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
  <xsl:template match="/">
    <style type="text/css">
      .rentalAgreement
      {
      background-color: white;
      }
      .rentalAgreement td
      {
      font-size: 8pt;
      font-family: Arial;
      }
      .rentalAgreement table.tableBlackBorder
      {
      width:100%;
      border:solid 2px black;
      }

    </style>

    <div class="rentalAgreement">
      <TABLE border="0" cellpadding="1" cellSpacing="0" width="100%" ID="Table10">
        <TR>
          <TD vAlign="top" width="50%" style="FONT-WEIGHT: bold; FONT-SIZE: 34pt; FONT-STYLE: italic; FONT-FAMILY: Arial">
            <i>Rental&#160;Agreement</i>
          </TD>
          <td width="50%">
            <table class="tableBlackBorder"  cellpadding="0"  rules="none" cellspacing="0">
              <tr>
                <td style="FONT-WEIGHT: bold">&#160;Licensee:</td>
                <td>
                  <xsl:value-of select="/Data/AgreementHeader/Licensee"/>
                </td>
              </tr>
              <!--Changed by Shoel-->
              <!-- displays data only if name is Tourism Holding Ltd and only for NZ -->
              <xsl:if test="/Data/AgreementHeader/Licensee = 'Tourism Holdings Limited' ">
                <xsl:if test="/Data/RentalDetails/Details/Country = 'NZ'" >
                  <tr>
                    <td></td>
                    <td>
                      Transport Service License Number 100 628 538
                    </td>
                  </tr>
                </xsl:if>
              </xsl:if>
              <!--End change-->
              <xsl:if test="/Data/AgreementHeader/ABNNumber !=''">
                <tr>
                  <td>
                    <b>&#160;ABN Number:</b>
                  </td>
                  <td>
                    <xsl:value-of select="/Data/AgreementHeader/ABNNumber"/>
                  </td>
                </tr>
              </xsl:if>
            </table>
          </td>
        </TR>
        <TR>
          <TD/>
        </TR>
      </TABLE>

      <BR/>

      <TABLE class="tableBlackBorder" cellSpacing="0"  ID="Table1">
        <TR>
          <TD bgColor="black" colSpan="5" style="height:105px">
            <FONT color="#ffffff" size="10px">&#160;Message</FONT>
          </TD>
        </TR>
        <TR>
          <TD nowrap="nowrap" style="height:55px; text-align:center">
            <b>Amazon Service not accessable</b>
          </TD>

        </TR>
      </TABLE>
    </div>
  </xsl:template>
</xsl:stylesheet>

