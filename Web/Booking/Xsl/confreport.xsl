<?xml version="1.0"?>
<!-- USED TO GENERATE 'QUOTE REQUEST CONFIRMATION' FILES(HTM) - EITHER BY CUSTOMER OR BY THE AGENT-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" extension-element-prefixes="msxsl">
  <xsl:template match="/">
    <!--  begin: variables to handle conditional styling -->

    <xsl:variable name="MACConfirm" select="MACConfirm.png"/>
    <xsl:variable name="MACqn" select="'http://www.thlonline.com/CentralLibraryImages/Aurora/MacConfirm.png'"/>

    <xsl:variable name="mauiQuotePhone" select="'#8b6f4d'"/>
    <xsl:variable name="mauiQuoteFreePhone" select="'#8b6f4d'"/>
    <xsl:variable name="mauiQuoteLinkInfooter" select=" '#006f7f'"/>

    <xsl:variable name="hrefColor" select="'#018401'"/>
    <xsl:variable name="footNoteBackColor" select="'#eeeeee'"/>
    <xsl:variable name="negativeValueColor" select="'#ff0000'"/>
    
    <!--  end: variables to handle conditional styling -->

    <html>
      <META http-equiv="content-type" content="text/html; charset=utf-8"/>

      <!--head element-->
      <head>
        <!--Something has to be in the title, otherwise the XSLT engine may make it a self-closing tag, thus essentially breaking the page
        (everything that appears after will be treated as the title)-->
        <title>Confirmation</title>
      </head>

      <body style="background: #f2f2f2; width: 100%; font-size: 11px; font-family: Arial;color: #000; margin: 0px">

        <xsl:for-each select="/data/doc">

          <xsl:variable name="confirmationOuterTableStyle">
            <xsl:choose>
              <xsl:when test="Header/Brand='Y'">
                <xsl:value-of select="'background:#524d02'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="''"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <xsl:variable name="confirmationTableStyle">
            <xsl:choose>
              <xsl:when test="Header/Brand='Y'">
                <xsl:value-of select="'font-size: 11px; background-color:#FFF;'"/>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="'font-size: 11px; border: 1px solid #cccccc; background-color:#FFF;'"/>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:variable>

          <!--Using table tag here instead of a div for better multiple mail client support.-->
          <table width="100%" style=" {$confirmationOuterTableStyle}">
            <tr>
              <td>

                <!-- Outer table container -->
                <table width="600" border="0" cellspacing="0" cellpadding="0" style=" {$confirmationTableStyle}" align="center">

                  <tr>

                    <td valign="top">
                      <!-- Header table container -->



                      <!--contactUs variables-->
                      <xsl:variable name="contactUSlink">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#006071'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#018401'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#588527'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#788d01'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#524d02'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='A' or Header/Brand='U'">
                            <xsl:value-of select="'#284a94'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='E'">
                            <xsl:value-of select="'#ef3e35'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#006071'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="summaryContactUsLinkColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#ee3124'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='A' or Header/Brand='U' or Header/Brand='E'">
                            <xsl:value-of select="'#000000'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="$contactUSlink"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--Alternate Text variables-->
                      <xsl:variable name="imgAlt">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'Maui'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'Britz'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'Explore More'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'Backpacker'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'Mighty'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Q'">
                            <xsl:value-of select="'KEA'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'MotorHomesAndCars'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--logobackground variables-->
                      <xsl:variable name="logoBackground">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#006071'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#d9761b'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#d9761b'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#0080b0'"/>
                          </xsl:when>
                          <!--rev:mia 07-april-2016-->
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#ee3124'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#006071'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--headerTablebackground variables-->
                      <xsl:variable name="headerTableBackColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#006071'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#f58220'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#fff334'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#0080b0'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#ee3124'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Q'">
                            <xsl:value-of select="'#0b5441'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='A' or Header/Brand='U'">
                            <xsl:value-of select="'#284a94'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='E'">
                            <xsl:value-of select="'#ef3e35'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#006071'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="headerTableForeColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#fff'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#fff'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#010101'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#fff'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#fff'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--linkcolor variables-->
                      <xsl:variable name="refColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#006071'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#d9761b'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#588527'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#0080b0'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#524d02'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='A' or Header/Brand='U'">
                            <xsl:value-of select="'#284a94'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='E'">
                            <xsl:value-of select="'#ef3e35'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#006071'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="depositValueColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#ee3124'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="$refColor"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="summaryCallNumberColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#ee3124'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="$refColor"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="referenceNumberColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#ee3124'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="$refColor"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="quoteTextColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#000'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#777'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--confirm variables-->
                      <xsl:variable name="confirmImg">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="mauiConfirm.png"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="britzConfirm.png"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="exploreConfirm.png"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="backpackerConfirm.png"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="$MACConfirm"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--QuoteOutercolr variables-->
                      <xsl:variable name="quoteOuterColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#c6d9dd'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#fae3d1'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#faf6d1'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#c7e7f4'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#c5c4a5'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Q'">
                            <xsl:value-of select="'#e3f2de'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='A' or Header/Brand='U'">
                            <xsl:value-of select="'#a9b7d4'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='E'">
                            <xsl:value-of select="'#dce590'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#c6d9dd'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--QuoteInnerColor variables-->
                      <xsl:variable name="quoteInnerColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='M'">
                            <xsl:value-of select="'#ccdfe3'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='B'">
                            <xsl:value-of select="'#ffe9d7'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='X'">
                            <xsl:value-of select="'#fffdd7'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='P'">
                            <xsl:value-of select="'#cdedfa'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#c5c4a5'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='A' or Header/Brand='U'">
                            <xsl:value-of select="'#a9b7d4'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='E'">
                            <xsl:value-of select="'#dce590'"/>
                          </xsl:when>
                          <xsl:when test="Header/Brand='Q'">
                            <xsl:value-of select="'#e3f2de'"/>
                          </xsl:when>
                          <xsl:otherwise>

                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="borderColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#524d02'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#cccccc'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="headerBgColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#e2e1d2'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#f3f3f3'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="headerTextColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#524d02'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#000'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="itemSummaryTextColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#fff'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#000'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <xsl:variable name="paymentSummaryHeaderBgColor">
                        <xsl:choose>
                          <xsl:when test="Header/Brand='Y'">
                            <xsl:value-of select="'#e2e1d2'"/>
                          </xsl:when>
                          <xsl:otherwise>
                            <xsl:value-of select="'#f2f2f2'"/>
                          </xsl:otherwise>
                        </xsl:choose>
                      </xsl:variable>

                      <!--Customer name variables-->
                      <xsl:variable name="customeName">
                        <xsl:if test="Audience='Agent'  ">
                          <xsl:value-of select="AgentDetails/Customer"/>
                        </xsl:if>
                        <xsl:if test="Audience='Customer'  ">
                          <xsl:value-of select="CustomerDetails/CustomerName"/>
                        </xsl:if>
                      </xsl:variable>

                      <!--highlight personal note variables-->
                      <xsl:variable name="highlightNote">
                        <xsl:if test="//data/highlight = 1">
                          <b>
                            <xsl:value-of select="PersonalNote"/>
                          </b>
                        </xsl:if>
                        <xsl:if test="//data/highlight = 0">
                          <xsl:value-of select="PersonalNote"/>
                        </xsl:if>
                      </xsl:variable>

                      <!--currency variables-->
                      <xsl:variable name="currencySymbol">
                        <xsl:if test="Audience='Customer'">
                          <xsl:value-of select="Customer/Curr/text()"/>
                        </xsl:if>
                        <xsl:if test="Audience='Agent'">
                          <xsl:value-of select="Agent/Curr/text()"/>
                        </xsl:if>
                      </xsl:variable>

                      <!--costlabel variables-->
                      <xsl:variable name="costLabel">
                        <xsl:if test="Header/VehicleStatus='Confirmed' and (Audience='Customer' or Audience='Agent')">
                          COST
                        </xsl:if>
                        <xsl:if test="Header/VehicleStatus!='Confirmed' ">
                          Cost
                        </xsl:if>
                      </xsl:variable>

                      <!--booking reference-->
                      <xsl:variable name="bookingreferencelabel">
                        <xsl:if test="Header/VehicleStatus='Confirmed' ">
                          YOUR BOOKING REFERENCE
                        </xsl:if>
                        <xsl:if test="Header/VehicleStatus!='Confirmed' ">
                          YOUR QUOTE REFERENCE
                        </xsl:if>
                      </xsl:variable>

                      <!--rev:mia 21-dec-2015 surveylinks email variables-->
                      <xsl:variable name="customeEmail">
                        <xsl:if test="Audience='Agent'  ">
                          <xsl:value-of select="AgentDetails/AgentEmail"/>
                        </xsl:if>
                        <xsl:if test="Audience='Customer'  ">
                          <xsl:value-of select="CustomerDetails/CustomerEmail"/>
                        </xsl:if>
                      </xsl:variable>
                      <xsl:variable name="referencenumberforsurvey" select="Header/OurRef"/>
                      
                      <!-- Header table container -->
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">

                        <tr>
                          <td width="145" style="padding: 8px;color:{$logoBackground}">
                            <xsl:variable name="currentlogo" select="Header/brandlogo"/>
                            <!--The logo for Mighty requires a bigger length, than currently set. However in order not to change the original behavior, a conditional statement is used.-->
                            <xsl:choose>
                              <xsl:when test="Header/Brand='Y'">
                                <img src="{$currentlogo}" alt="{$imgAlt}" width="133px" height="56px"/>
                              </xsl:when>
                              <xsl:when test="Header/Brand='Q'">
                                <img src="{$currentlogo}" alt="{$imgAlt}" width="115px" height="44px"/>
                              </xsl:when>
                              <xsl:when test="Header/Brand='U'">
                                <img src="{$currentlogo}" alt="{$imgAlt}" width="126px" height="43px"/>
                              </xsl:when>
                              <xsl:when test="Header/Brand='A'">
                                <img src="{$currentlogo}" alt="{$imgAlt}" width="86px" height="53px"/>
                              </xsl:when>
                              <xsl:when test="Header/Brand='E'">
                                <img src="{$currentlogo}" alt="{$imgAlt}" width="122px" height="64px"/>
                              </xsl:when>
                              <xsl:otherwise>
                                <!--Original-->
                                <img src="{$currentlogo}" alt="{$imgAlt}" width="102px" height="55px"/>
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>

                          <td colspan="3">
                            <!-- title container -->
                            <table width="100%" cellpadding="0" cellspacing="0" style="background:{$headerTableBackColor}; color: {$headerTableForeColor};">
                              <tr>
                                <td style="padding:5px 0 5px 10px;">
                                  <font style="font-size:18px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:{$headerTableForeColor};">
                                    <xsl:value-of select="Header/ConfirmationTitle"></xsl:value-of>
                                  </font>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>

                        <!--header label-->
                        <tr>
                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">CUSTOMER</font>
                          </td>
                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">REQUEST DATE</font>
                          </td>
                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">CONSULTANT</font>
                          </td>

                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                              <xsl:value-of select="$bookingreferencelabel"/>
                            </font>
                          </td>
                        </tr>

                        <!--header data-->
                        <tr>
                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                              <xsl:value-of select="$customeName"/>
                            </font>
                          </td>

                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                              <xsl:value-of select="Header/CfnDate"/>
                            </font>
                          </td>

                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                              <xsl:value-of select="Header/OurConsultant"/>
                            </font>
                          </td>

                          <td style="padding:5px 0 5px 10px;">
                            <font style="font-size:24px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$referenceNumberColor};">
                              <xsl:value-of select="Header/OurRef"/>
                            </font>
                          </td>

                        </tr>

                        <!--rev:mia 21-dec-2015 surveylinks email variables-->
                        <!--rev:mia 13-june-2016 https://thlonline.atlassian.net/browse/AURORA-901-->
                        <xsl:if test="((Header/Brand='M' or Header/Brand='Y' or Header/Brand='B') and Audience='Customer' and (Header/ConfirmationTitle != 'Booking Cancelled' and Header/ConfirmationTitle != 'Quote Request') )"> <!--rev:mia 07-april-2016-->
                          <tr>
                            <td colspan="4">
                              <table align="center" style="font-size: 11px;" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                  <tr>
                                    <td valign="top" style="padding: 15px 5px 5px 10px; width: 50%; background-color:{$quoteOuterColor};">
                                      <font style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal;">
                                        You must check in online prior to arrival.<br/>
                                        <a style="color:{$refColor};">
                                          <xsl:attribute name="href">
                                            <xsl:value-of select="BookingDetails/SelfCheckInURL"/>
                                          </xsl:attribute>
                                          Click here
                                        </a> to check in now.
                                      </font>
                                    </td>
                                    <td valign="top" style="padding: 15px 10px 5px 5px; background-color:{$quoteOuterColor};">
                                      <font style="color: rgb(0, 0, 0); font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: normal;">
                                        Help us make your holiday an experience to remember. Tell us more about you and your interests.
                                      </font>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td style="padding: 5px 5px 15px 10px; background-color:{$quoteOuterColor};">
                                      <font style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;">
                                        <a target="_blank">
                                          <xsl:attribute name="href">
                                            <xsl:value-of select="BookingDetails/SelfCheckInURL"/>
                                          </xsl:attribute>
                                          <xsl:choose>
                                            <xsl:when test="Header/Brand='M'">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/aurora/maui-btn.png" style="border:none;" alt="Check In Online"/>
                                            </xsl:when>
                                            <xsl:when test="Header/Brand='B'">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/aurora/britz-btn.png" style="border:none;" alt="Check In Online"/>
                                            </xsl:when>
                                            <xsl:when test="Header/Brand='Y'">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/aurora/mighty-btn.png" style="border:none;" alt="Check In Online"/>
                                            </xsl:when>
                                          </xsl:choose>
                                        </a>
                                      </font>
                                    </td>
                                    <td style="padding: 5px 10px 15px 5px; background-color:{$quoteOuterColor};">
                                      <font style="font-family: Arial, Helvetica, sans-serif; font-size: 11px; font-weight: bold;">
                                        <a href="https://www.surveymonkey.com/r/RPYCS3B?bID={$referencenumberforsurvey}&amp;email={$customeEmail}">
                                          <xsl:choose>
                                            <xsl:when test="Header/Brand='M'">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/aurora/maui-btn-blue.png" style="border:none;" alt="Take Survey"/>
                                            </xsl:when>
                                            <xsl:when test="Header/Brand='B'">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/aurora/britz-btn-orange.png" style="border:none;" alt="Take Survey"/>
                                            </xsl:when>
                                            <xsl:when test="Header/Brand='Y'">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/aurora/mighty-btn-green.png" style="border:none;" alt="Take Survey"/>
                                            </xsl:when>
                                          </xsl:choose>
                                        </a>
                                      </font>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </xsl:if>  
                       
                     

                        <!-- if quote request is by an agent the agent information is displayed -->
                        <!--Agent label and data-->
                        <xsl:if test="Audience='Agent'  ">
                          <tr>
                            <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">AGENT NAME</font>
                            </td>
                            <td colspan="2" style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">AGENT DETAILS</font>
                            </td>
                            <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">AGENT REFERENCE</font>
                            </td>
                          </tr>

                          <tr>
                            <td style="padding:5px 0 5px 10px;">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                <xsl:value-of select="AgentDetails/AgnName"/>
                              </font>
                            </td>
                            <td colspan="2" style="padding:5px 0 5px 10px;">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                <xsl:value-of select="AgentDetails/ContactName"/>
                              </font>
                            </td>
                            <td style="padding:5px 0 5px 10px;">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                <xsl:value-of select="AgentDetails/AgentRef"/>
                              </font>
                            </td>
                          </tr>

                          <tr>
                            <td colspan="2" style="padding:0 0 5px 10px;">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                <xsl:value-of select="AgentDetails/AgentEmail"/>
                              </font>
                            </td>

                            <td style="padding:0 0 5px 10px;">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                FX:<xsl:value-of select="AgentDetails/AgentFax"/>
                              </font>
                            </td>
                            <td style="padding:0 0 5px 10px;">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                PH:<xsl:value-of select="AgentDetails/AgentPhone"/>
                              </font>
                            </td>
                          </tr>

                        </xsl:if>


                        <!-- display rentalstatus note -->
                        <xsl:if test="Header/RentalStatusText != ''">
                          <tr>
                            <td colspan="4" style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};">
                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                <xsl:value-of select="Header/RentalStatusText"/>
                              </font>
                            </td>
                          </tr>
                        </xsl:if>

                        <!-- display personal note -->
                        <xsl:if test="PersonalNote != '' ">
                          <tr>
                            <td colspan="4" style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};">


                              <xsl:if test="//data/highlight = 1">
                                <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; color:#000; font-weight:bold;">
                                  <xsl:value-of select="$highlightNote"/>
                                </font>
                              </xsl:if>
                              <xsl:if test="//data/highlight = 0">
                                <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; color:#000; font-weight:normal;">
                                  <xsl:value-of select="PersonalNote"/>
                                </font>


                              </xsl:if>
                            </td>
                          </tr>
                        </xsl:if>




                        <!-- End Header container -->
                        <!-- Start two column layout headings -->
                        <!--travel details headers-->
                        <tr>

                          <td width="25%" style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$headerBgColor};" colspan="1">
                            <font style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:{$headerTextColor};">TRAVEL DETAILS</font>
                          </td>

                          <td width="40%" style="padding:5px 5px 5px 10px; border-top: 1px solid {$borderColor}; border-right: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$headerBgColor};" align="right" colspan="2">
                            <font style="font-size:12px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;color:{$refColor};">
                              <xsl:if test="BookingDetails/SelfCheckInURL != '' and Header/Brand!='Y'">
                                <a style="color:{$refColor}; float: right; font-size: 12px">
                                  <xsl:attribute name="href">
                                    <xsl:value-of select="BookingDetails/SelfCheckInURL"/>
                                  </xsl:attribute>
                                  Save time, check-in online
                                </a>
                              </xsl:if>
                              <xsl:if test="BookingDetails/ConfirmQuoteURL != ''">
                                <a style="color:{$refColor}; float: right; font-size: 12px">
                                  <xsl:attribute name="href">
                                    <xsl:value-of select="BookingDetails/ConfirmQuoteURL"/>
                                  </xsl:attribute>
                                  Save time, confirm quote online
                                </a>
                              </xsl:if>
                            </font>
                          </td>

                          <td width="35%" style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$quoteOuterColor};">
                            <font style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:{$itemSummaryTextColor};">
                              <xsl:choose>
                                <xsl:when test="Header/VehicleStatus='Quoted'">
                                  <xsl:if test="Audience='Agent'  ">
                                    CHARGE
                                  </xsl:if>
                                  <xsl:if test="Audience!='Agent'  ">
                                    QUOTE
                                  </xsl:if>
                                </xsl:when>
                                <xsl:when test="Header/VehicleStatus!='Quoted'">
                                  CHARGE
                                </xsl:when>
                              </xsl:choose>
                              SUMMARY
                            </font>
                          </td>

                        </tr>


                        <!-- End two column layout headings -->
                        <!-- Start two column layout container -->
                        <tr>

                          <td valign="top"  colspan="3" rowspan="3">
                            <xsl:attribute name="style">
                              <xsl:choose>
                                <xsl:when test="BookingDetails/SelfCheckInURL != '' and Header/Brand='Y'">
                                  padding:0px 0 5px 10px; border-right: 1px solid <xsl:value-of select="$borderColor"/>; background-color:#FFF;
                                </xsl:when>
                                <xsl:otherwise>
                                  padding:5px 0 5px 10px; border-right: 1px solid <xsl:value-of select="$borderColor"/>; background-color:#FFF;
                                </xsl:otherwise>
                              </xsl:choose>
                            </xsl:attribute>

                            <xsl:if test="BookingDetails/SelfCheckInURL != '' and Header/Brand='Y'">
                              <table width="371" height="58" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td colspan="3" valign="top">
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/checkinonline_topshadow.jpg" width="371" height="9" alt="" style="display:block;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td valign="top">
                                    <a href="{BookingDetails/SelfCheckInURL}" style="border:0px; display:block; text-decoration:none;">
                                      <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/checkinonline.jpg" width="135" height="36" alt="Check in online" style="display:block; border:0px;" />
                                    </a>
                                  </td>
                                  <td width="225" height="36" bgcolor="#da291c" style="text-align:center; line-height:12px;">


                                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:11px; font-weight:bold; color:#FFF;">
                                      You must check in online prior to arrival
                                    </span>
                                    <a href="{BookingDetails/SelfCheckInURL}" style="font-style:italic; text-decoration:underline; color:#FFF; font-size:11px; font-family:Arial, Helvetica, sans-serif;">CLICK HERE to check in now</a>

                                  </td>
                                  <td valign="top">
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/checkinonline_right.jpg" width="11" height="36" alt="" style="display:block;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="3" valign="top">
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/checkinonline_bottom.jpg" width="371" height="13" alt="" style="display:block;" />
                                  </td>
                                </tr>
                              </table>
                            </xsl:if>

                            <!-- Start left column - pick-up drop-off -->
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                              <!--VEHICLE-->
                              <tr>
                                <td width="18%" style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">VEHICLE</font>
                                </td>
                                <td style="padding:5px 0 5px 0px;" colspan="2">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                    <xsl:value-of select="BookingDetails/Prd"/>
                                  </font>
                                </td>
                              </tr>

                              <!--PICK UP-->
                              <tr>
                                <td style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">PICK UP</font>
                                </td>
                                <td style="padding:5px 0 5px 0px;" colspan="2">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$refColor};">
                                    <xsl:value-of select="BookingDetails/CkoWhen"/>
                                  </font>
                                </td>
                              </tr>

                              <!--Address-->
                              <tr>
                                <td>
                                  <![CDATA[&nbsp;]]>
                                </td>
                                <td valign="top" style="padding:5px 0 5px 0px;" colspan="2">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                    <xsl:value-of select="BookingDetails/CkoLocation"/>
                                    <br/>
                                    <xsl:value-of select="BookingDetails/CkoAddress"/>
                                  </font>
                                </td>
                              </tr>

                              <!--Branch Phone-->

                              <xsl:if test="Header/VehicleStatus!='Quoted'">
                                <tr>
                                  <td>
                                    <![CDATA[&nbsp;]]>
                                  </td>
                                  <td width="22%" style="padding:5px 0 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">Branch Phone:</font>
                                  </td>
                                  <td width="60%" style="padding:5px 0 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      <xsl:value-of select="BookingDetails/CkoPhone"/>
                                    </font>
                                  </td>

                                </tr>
                              </xsl:if>

                              <!--Opening Hours-->
                              <tr>
                                <td>
                                  <![CDATA[&nbsp;]]>
                                </td>
                                <td style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">Opening Hours:</font>
                                </td>
                                <td style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                    <xsl:value-of select="BookingDetails/CkoStart"/>
                                    to
                                    <xsl:value-of select="BookingDetails/CkoFinish"/>
                                  </font>
                                </td>
                              </tr>

                              <!--DROP OFF-->
                              <tr>
                                <td style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">DROP OFF</font>
                                </td>

                                <td style="padding:5px 0 5px 0px;" colspan="2">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$refColor};">
                                    <xsl:value-of select="BookingDetails/CkiWhen"/>
                                  </font>
                                </td>
                              </tr>

                              <!--Address-->
                              <tr>
                                <td>
                                  <![CDATA[&nbsp;]]>
                                </td>
                                <td valign="top" style="padding:5px 0 5px 0px;" colspan="2">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                    <xsl:value-of select="BookingDetails/CkiLocation"/>
                                    <br/>
                                    <xsl:value-of select="BookingDetails/CkiAddress"/>
                                  </font>
                                </td>
                              </tr>

                              <!--Branch Phone-->
                              <xsl:if test="Header/VehicleStatus!='Quoted'">
                                <tr>
                                  <td>
                                    <![CDATA[&nbsp;]]>
                                  </td>
                                  <td style="padding:5px 0 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">Branch Phone:</font>
                                  </td>
                                  <td style="padding:5px 0 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      <xsl:value-of select="BookingDetails/CkiPhone"/>
                                    </font>
                                  </td>
                                </tr>
                              </xsl:if>
                              <!--Opening Hours-->
                              <tr>
                                <td>
                                  <![CDATA[&nbsp;]]>
                                </td>
                                <td style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">Opening Hours:</font>
                                </td>

                                <td style="padding:5px 0 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                    <xsl:value-of select="BookingDetails/CkiStart"/>
                                    to
                                    <xsl:value-of select="BookingDetails/CkiFinish"/>
                                  </font>
                                </td>
                              </tr>

                            </table>
                            <!-- End left column - pick-up drop-off -->

                          </td>

                          <td valign="top" style="padding:5px 0 5px 10px; background-color:{$quoteInnerColor}">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <!--COST-->
                              <tr>
                                <td style="padding:5px 10px 5px 0px;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                    <xsl:value-of select="$costLabel"/>
                                  </font>

                                </td>

                                <td style="padding:5px 10px 5px 0px;" align="right">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                    <xsl:value-of select="$currencySymbol"/>
                                  </font>
                                </td>
                              </tr>

                              <!--Total Cost-->
                              <xsl:if test="Audience='Customer'">

                                <tr>
                                  <td style="padding:5px 10px 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">Total Cost</font>
                                  </td>
                                  <td style="padding:5px 10px 5px 0px;" align="right">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                      $<xsl:value-of select="format-number(Customer/Curr/TotalChg,'#.00')"/>
                                    </font>
                                  </td>
                                </tr>

                                <!--  add deposit for QN and Customer -->
                                <xsl:if test="Header/VehicleStatus='Quoted' ">
                                  <tr>
                                    <td valign="top" style="padding:5px 10px 5px 0px;" nowrap="true">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">Deposit Required</font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px; " align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:{$depositValueColor};">
                                        <xsl:if test="Header/DepositRequired != '' ">
                                          $<xsl:value-of select="format-number(Header/DepositRequired,'#.00')"/>
                                        </xsl:if>
                                        <xsl:if test="Header/DepositRequired = '' ">
                                          $<xsl:value-of select="format-number(0,'#.00')"/>
                                        </xsl:if>
                                      </font>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <!--display this block if its confirmed-->
                                <xsl:if test="Header/VehicleStatus='Confirmed' ">
                                  <tr>

                                    <td valign="top" style="padding:5px 10px 5px 0px;">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">Total Paid</font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px; border-bottom:2px solid #000;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                        <xsl:if test="Payments/payment/curr != '' ">
                                          $<xsl:value-of select="format-number(Payments/payment/pmt,'#.00')"/>
                                        </xsl:if>
                                        <xsl:if test="Payments/payment/curr = '' ">
                                          $<xsl:value-of select="format-number(0,'#.00')"/>
                                        </xsl:if>
                                      </font>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td valign="top" style="padding:5px 10px 5px 0px;">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        Final Balances
                                      </font>
                                      <br/>

                                      <!--rev:mia Oct.29 2012 - added-->
                                      <xsl:if test="Header/DispPmtTerm ='Y'    ">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; color:#000;">
                                          <xsl:choose>
                                            <xsl:when test="Header/SpecialDepTerm != ''">
                                              Full payment due <xsl:value-of select="Header/SpecialDepTerm"/> days before pick-up.
                                            </xsl:when>
                                            <!--<xsl:otherwise>
                                                      payable on pickup
                                                  </xsl:otherwise>-->
                                          </xsl:choose>

                                        </font>
                                      </xsl:if>
                                      <xsl:if test="Header/DispPmtTerm ='N'    ">
                                      </xsl:if>


                                    </td>
                                    <td valign="top" style="padding:5px 10px 5px 0px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold;color:{$refColor}">
                                        $<xsl:value-of select="format-number(Payments/Balance,'#.00')"/>
                                      </font>
                                    </td>

                                  </tr>
                                </xsl:if>


                              </xsl:if>

                              <xsl:if test="Audience='Agent'">
                                <tr>
                                  <td style="padding:5px 10px 5px 0px;" nowrap="true">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">Agent Charge (Nett)</font>
                                  </td>

                                  <td style="padding:5px 10px 5px 0px; " align="right">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      <xsl:variable name="totalpaid" select="(Agent/Curr/TotalChg) - (Agent/Curr/AgentDiscount)"/>
                                      $<xsl:value-of select="format-number($totalpaid,'#.00')"/>
                                    </font>
                                  </td>
                                </tr>

                                <tr>
                                  <td valign="top" style="padding:5px 10px 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">Customer Charge</font>
                                  </td>
                                  <td style="padding:5px 10px 5px 0px; " align="right">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      $<xsl:value-of select="format-number(Customer/Curr/TotalChg,'#.00')"/>
                                    </font>
                                  </td>

                                </tr>
                              </xsl:if>

                              <xsl:if test="Header/VehicleStatus='Quoted'">
                                <tr>
                                  <td valign="top" style="padding:5px 10px 5px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">Quote Expires</font>
                                  </td>

                                  <td style="padding:5px 10px 5px 0px; " align="right">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      <xsl:value-of select="BookingDetails/RentalExpiryDate"/>
                                    </font>
                                  </td>
                                </tr>

                                <!--rev:mia Nov.1 2012 - added. Today is All souls day.  -->
                                <xsl:if test="Header/SpecialDepTerm != ''">
                                  <tr>
                                    <td valign="top" style="padding:5px 10px 5px 0px;" colspan="2">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; color:#000;">
                                        Full payment due <xsl:value-of select="Header/SpecialDepTerm"/> days before pick-up.
                                      </font>
                                    </td>
                                    <td></td>
                                  </tr>
                                </xsl:if>


                              </xsl:if>


                            </table>
                            <!-- end right column - pricing table -->

                          </td>

                        </tr>


                        <tr>
                          <xsl:choose>
                            <xsl:when test="BookingDetails/ConfirmQuoteURL != ''">
                              <td align="center" style="border:1px solid {$headerTableBackColor};text-align:center; background:{$headerTableBackColor};padding:10px;">
                                <!--if ConfirmQuoteURL has a value then display the URL and button-->
                                <table width="100%" style="border-collapse:collapse;" border="0" cellspacing="0" cellpadding="0">
                                  <td width="10%" align="center" style="text-align:center;padding:0px;">
                                    &amp;nbsp;
                                  </td>
                                  <td width="80%" align="center" style="text-align:center;padding:0px;">
                                    <a href="{BookingDetails/ConfirmQuoteURL}" style="color: {$headerTableForeColor};text-decoration:none;font:normal bold 12px/20px Verdana, Arial;">CONFIRM&amp;nbsp;QUOTE</a>
                                    <br/>
                                    <a href="{BookingDetails/ConfirmQuoteURL}" style="color: {$headerTableForeColor};text-decoration:none;font:normal bold 12px/20px Verdana, Arial;">ONLINE&amp;nbsp;NOW</a>
                                  </td>
                                  <td width="10%" align="center" style="text-align:center;">
                                    <a href="{BookingDetails/ConfirmQuoteURL}" style="padding:0px; color: {$headerTableForeColor};text-decoration:none;font:normal bold 16px/20px Verdana, Arial;">
                                      <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/white_arrow.png" style="border:none;" alt=""/>
                                    </a>
                                  </td>
                                </table>
                              </td>
                            </xsl:when>
                            <xsl:otherwise>
                              <td></td>
                            </xsl:otherwise>
                          </xsl:choose>
                        </tr>

                        <tr>
                          <!--<td valign="top" style="padding:5px 0 5px 10px; border-bottom: 1px solid {$borderColor}; background-color:{$quoteInnerColor}">
                          Nimesh Removed border bottom because of additional row for self checkin text
                            -->
                          <td valign="top" style="padding:5px 0 5px 10px;  background-color:{$quoteInnerColor}">

                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <!--display confirm by phone-->
                              <xsl:if test="Header/VehicleStatus='Quoted' ">
                                <tr>
                                  <xsl:variable name="confirmTextPadding">
                                    <xsl:choose>
                                      <xsl:when test="Header/Brand = 'Y'">
                                        0px 0px 0px 0px
                                      </xsl:when>
                                      <xsl:otherwise>
                                        10px 10px 10px 0px
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </xsl:variable>
                                  <td colspan="2" align="center" style="padding: {$confirmTextPadding};">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      To confirm Quote by phone:
                                    </font>
                                    <br/>

                                    <!--  not maui -->
                                    <xsl:if test="Header/Brand='Q'">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$summaryCallNumberColor};">
                                        call <xsl:value-of select="Header/ConfQNNum"/>
                                      </font>
                                      <br />
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                        or
                                      </font>
                                      <br />

                                      <font style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$summaryContactUsLinkColor};">
                                        <a target="_blank" style="color:{$summaryContactUsLinkColor}; font-size: 11px">
                                          <xsl:attribute name="href">
                                            http://aurentals.keacampers.com/en/contact.aspx
                                          </xsl:attribute>  for freephone numbers   click here
                                        </a>
                                      </font>
                                    </xsl:if>
                                    <xsl:if test="Header/Brand!='M' and Header/Brand!='Q'">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$summaryCallNumberColor};">
                                        call <xsl:value-of select="Header/ConfQNNum"/>
                                      </font>
                                      <br />
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                        or
                                      </font>
                                      <br />

                                      <font style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$summaryContactUsLinkColor};">
                                        <a target="_blank" style="color:{$summaryContactUsLinkColor}; font-size: 11px">
                                          <xsl:attribute name="href">
                                            <xsl:value-of select="Header/Web"/>/contactUs
                                          </xsl:attribute>  for freephone numbers   click here
                                        </a>
                                      </font>
                                    </xsl:if>


                                    <!--  maui -->
                                    <xsl:if test="Header/Brand ='M'">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$mauiQuotePhone};">
                                        call <xsl:value-of select="Header/ConfQNNum"/>
                                      </font>
                                      <br />
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                        or
                                      </font>
                                      <br />
                                      <font style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$mauiQuoteFreePhone};">
                                        <a target="_blank" style="color:{$mauiQuoteFreePhone}; font-size: 11px">
                                          <xsl:attribute name="href">
                                            <xsl:value-of select="Header/Web"/>/contactUs
                                          </xsl:attribute>  for freephone numbers   click here
                                        </a>
                                      </font>
                                    </xsl:if>

                                  </td>
                                </tr>
                              </xsl:if>


                              <xsl:if test="BookingDetails/QuoteText != '' and BookingDetails/IsSurchargeAppl = '1'">
                                <tr>
                                  <td colspan="2" style="padding:10px 0px 10px 0px;">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$quoteTextColor};">
                                      <xsl:value-of select="BookingDetails/QuoteText"/>
                                    </font>
                                  </td>

                                </tr>
                              </xsl:if>


                              <xsl:if test="BookingDetails/SelfCheckInURL != ''  ">
                                <tr>
                                  <td colspan="2" style="padding:0px 0px 0px 0px;text-align:center;">

                                    <a>
                                      <xsl:attribute name="href">
                                        <xsl:value-of select="BookingDetails/SelfCheckInURL"/>
                                      </xsl:attribute>
                                      <xsl:choose>
                                        <xsl:when test="Header/Brand='M'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/maui_checkinonline.jpg" style="border:none;" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='B'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/britz_checkinonline.jpg" style="border:none;" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='X'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/exploremore_checkinonline.jpg" style="border:none;" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='P'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/backpacker_checkinonline.jpg" style="border:none;" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='Y'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/mighty_checkinonline.jpg" style="border:none;margin:0px;" width="154px" height="71px" align="middle" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='Q'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/keaCheckinOnline.jpg" style="border:none;margin:0px;" width="166px" height="60px" align="middle" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='E'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/econo_checkinonline.jpg" style="border:none;margin:0px;" width="148px" height="42px" align="middle" alt="Check In Online"/>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='A' or Header/Brand='U'">
                                          <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/united_checkinonline.jpg" style="border:none;margin:0px;" width="148px" height="42px" align="middle" alt="Check In Online"/>
                                        </xsl:when>
                                      </xsl:choose>
                                    </a>
                                  </td>
                                </tr>
                              </xsl:if>
                            </table>
                          </td>
                        </tr>

                        <tr>
                          <td valign="top" colspan="3" style="padding:0px; border-right: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:#FFF;"></td>
                          <td valign="top" style="padding:0px; border-bottom: 1px solid {$borderColor}; background-color:{$quoteInnerColor}">

                            <xsl:if test="BookingDetails/MustReadURL != ''  ">
                              <table width="210" height="89" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td colspan="3">
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/stuff_arrow.jpg" width="210" height="15" alt="" style="display:block;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/stuff_left.jpg" width="12" height="66" alt="" style="display:block;" />
                                  </td>
                                  <td width="186" height="66" bgcolor="#4d512d" style="text-align:center; line-height:14px;">
                                    <a href="{BookingDetails/MustReadURL}" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; font-weight:bold; color:#e2e1d1; text-decoration:none;">
                                      TO PREPARE FOR YOUR MIGHTY
                                      <br />
                                      EXPERIENCE YOU MUST READ THE:
                                    </a>
                                    <a href="{BookingDetails/MustReadURL}" style="line-height:18px; font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; color:#FFF; text-decoration:underline;">
                                      STUFF YOU'VE GOT TO KNOW
                                    </a>
                                  </td>
                                  <td>
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/stuff_right.jpg" width="12" height="66" alt="" style="display:block;" />
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="3">
                                    <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/Mighty_confirmation/stuff_bottom.jpg" width="210" height="8" alt="" style="display:block;" />
                                  </td>
                                </tr>
                              </table>
                            </xsl:if>
                            <!--<xsl:if test="Header/VehicleStatus='Confirmed' and BookingDetails/ConfirmQuoteURL != ''">-->
                            <xsl:if test="BookingDetails/SelfCheckInURL != ''  ">
                              <div align="center" style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:{$refColor};">
                                Or you can Check in by going to:
                                <a target="_blank" style="color:{$refColor};font-size: 11px" href="https://selfcheckin.thlonline.com">
                                  
                                  https://selfcheckin.thlonline.com
                                </a>
                                <!--<a target="_blank" style="color:{$refColor};font-size: 11px">
                                  <xsl:attribute name="href">
                                    https://selfcheckin.thlonline.com
                                  </xsl:attribute>
                                  https://selfcheckin.thlonline.com
                                </a>-->
                              </div>
                            
                            </xsl:if>

                          </td>
                        </tr>


                        <tr>
                          <td>
                            <![CDATA[&nbsp;]]>
                          </td>
                        </tr>

                        <tr>
                          <!-- Start RENTAL CHARGE DETAILS header -->
                          <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                              <tr>
                                <td width="60%" style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$headerBgColor};" colspan="4">

                                  <font style="font-size:14px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:{$headerTextColor};">RENTAL CHARGE DETAILS</font>
                                </td>
                                <td width="40%" style="padding:5px 10px 5px 0px; border-top: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$headerBgColor};" align="right">
                                  <font style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:{$contactUSlink};font-size: 11px">
                                    <a target="_blank" style="color:{$contactUSlink};float: right; font-size: 11px">
                                      <xsl:attribute name="href">
                                        <xsl:value-of select="Header/TandCLink"/>
                                      </xsl:attribute>
                                      Please read our Rental Agreement
                                    </a>
                                  </font>
                                </td>
                              </tr>
                            </table>
                            <!-- End RENTAL CHARGE DETAILS header -->

                          </td>
                        </tr>

                        <!-- Start RENTAL CHARGE DETAILS price table -->

                        <tr>
                          <td colspan="4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">

                              <!--header-->
                              <tr>
                                <td style="padding:5px 0 5px 10px; border-bottom:1px solid #ccc;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">BOOKING DETAILS</font>
                                </td>

                                <td colspan="2" align="center" style="padding:5px 10px 5px 10px; border-bottom:1px solid #ccc;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">NO. OF DAYS</font>
                                </td>
                                <td colspan="2" style="padding:5px 0 5px 10px; border-bottom:1px solid #ccc;">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">PRICE PER DAY</font>
                                </td>
                                <td style="padding:5px 10px 5px 0px; border-bottom:1px solid #ccc;" align="right">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">SUBTOTAL</font>
                                </td>
                                <td style="padding:5px 10px 5px 0px; border-bottom:1px solid #ccc;" align="right">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">TOTAL</font>
                                </td>
                              </tr>

                              <!--agent looping-->
                              <xsl:if test="Agent/Curr != '' ">
                                <xsl:for-each select="Agent/Curr">
                                  <xsl:sort select="text()"/>

                                  <xsl:variable name="Curr" select="normalize-space(text())"/>

                                  <xsl:for-each select="DescList/Desc">
                                    <xsl:variable name="pos" select="position()"/>

                                    <!--from and to-->
                                    <tr>
                                      <td  style="padding:5px 0 5px 10px;">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                          <xsl:if test="@F !=''">
                                            <b>
                                              <xsl:value-of select="@F"/>
                                              <xsl:text> - </xsl:text>
                                              <xsl:value-of select="@T"/>
                                            </b>
                                          </xsl:if>
                                        </font>
                                      </td>
                                    </tr>

                                    <tr>

                                      <td style="padding:5px 0 5px 10px;">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                          <xsl:if test="@Typ='Dis'">
                                            <xsl:text>  </xsl:text>
                                          </xsl:if>
                                          <xsl:value-of select="."/>
                                        </font>
                                      </td>

                                      <td style="padding:5px 0 5px 10px;" align="center">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                          <xsl:if test="../../QtyList/Qty[position()=$pos]!=0">
                                            <xsl:value-of select="../../QtyList/Qty[position()=$pos]"/>
                                            <xsl:text> </xsl:text>
                                          </xsl:if>
                                        </font>
                                      </td>

                                      <td style="padding:5px 0 5px 0px;" align="center">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">x</font>
                                      </td>

                                      <td style="padding:5px 10px 5px 0px;" align="right">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                          <xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])!=0">
                                            <xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]) &lt; 0">
                                              <span style="color:{$negativeValueColor};">
                                                $<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
                                              </span>
                                            </xsl:if>
                                            <xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]) &gt; 0">
                                              $<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
                                            </xsl:if>
                                          </xsl:if>
                                          <xsl:if test="(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])=0) and (../../TotalList/Total[position()=$pos]=0) and (../../QtyList/Qty[position()=$pos]!=0)">
                                            $<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
                                          </xsl:if>
                                        </font>
                                      </td>

                                      <td style="padding:5px 10px 5px 0px;" align="center">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">=</font>
                                      </td>

                                      <td style="padding:5px 10px 5px 0px;" align="right">
                                        <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                          <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &lt; 0">
                                            <span style="color:{$negativeValueColor};">
                                              $<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
                                            </span>
                                          </xsl:if>
                                          <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &gt; 0">
                                            $<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
                                          </xsl:if>
                                          <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') = 0">
                                            $<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
                                          </xsl:if>
                                        </font>
                                      </td>

                                      <xsl:choose>
                                        <xsl:when test="@Typ ='Dis' ">
                                          <xsl:if test="following-sibling::Desc[1]/@Typ !='Dis' or $pos = last() ">
                                            <xsl:variable name="path" select="preceding-sibling::*[@G][1]"/>
                                            <td style="padding:5px 10px 5px 0px;" align="right">
                                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                                <xsl:call-template name="NetTotalRow">
                                                  <xsl:with-param name="rows" select="$path"/>
                                                </xsl:call-template>
                                              </font>
                                            </td>
                                          </xsl:if>
                                        </xsl:when>
                                        <xsl:when test="@Typ ='Chg' ">
                                          <xsl:if test="following-sibling::Desc[1]/@Typ !='Dis'  or $pos = last()">
                                            <td style="padding:5px 10px 5px 0px;" align="right">
                                              <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                                <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &lt; 0">
                                                  <span style="color:{$negativeValueColor};">
                                                    $<xsl:value-of select="format-number(@G,'#.00')"/>
                                                  </span>
                                                </xsl:if>
                                                <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &gt; 0">
                                                  $<xsl:value-of select="format-number(@G,'#.00')"/>
                                                </xsl:if>
                                                <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') = 0">
                                                  $<xsl:value-of select="format-number(@G,'#.00')"/>
                                                </xsl:if>
                                              </font>
                                            </td>
                                          </xsl:if>
                                        </xsl:when>
                                      </xsl:choose>


                                    </tr>

                                  </xsl:for-each>
                                  <tr>
                                    <td></td>
                                    <td colspan="6" valign="top"  style="border-top: 2px solid #000; padding: 10px 0px 5px;" align="right"/>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:5px 10px 5px 10px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        TOTAL <xsl:value-of select="$Curr"/> (includes GST)
                                      </font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        <xsl:variable name="Charges" select="TotalChg"/>
                                        $<xsl:value-of select="format-number($Charges,'#.00')"/>
                                      </font>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td colspan="6" style="padding:5px 10px 5px 10px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        Less Agent Discount
                                      </font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        $<xsl:value-of select="format-number(AgentDiscount,'#.00')"/>
                                      </font>
                                    </td>
                                  </tr>

                                  <tr>
                                    <td colspan="6" style="padding:5px 10px 5px 10px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        GST Content

                                      </font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif;  color:#000;">
                                        <xsl:variable name="PaymentRecvd" select="sum(../../Payments/*/pmt[../curr=$Curr])"/>
                                        $<xsl:value-of select="TotalGst"/>
                                      </font>

                                    </td>
                                  </tr>

                                  <xsl:if test="Header/VehicleStatus='Quoted' ">
                                    <tr>
                                      <td align="right" style="width: 10px;">
                                        <xsl:text> </xsl:text>
                                      </td>
                                      <td colspan="6" valign="top" align="right" style="padding: 0px 0px 5px;font-family: Arial,sans-serif;">
                                        <b>
                                          <xsl:if test="/data/doc/Audience = 'Customer' ">
                                            Total Payable (Inclusive of GST)   (<xsl:value-of select="/data/doc/Customer/Curr/text()"/>)
                                          </xsl:if>
                                          <xsl:if test="/data/doc/Audience = 'Agent' ">
                                            Total Payable (Inclusive of GST)   (<xsl:value-of select="/data/doc/Agent/Curr/text()"/>)
                                          </xsl:if>
                                        </b>
                                        <br/>

                                      </td>
                                      <td valign="top" align="right" style="padding: 0px 10px 5px; color:{$refColor}; font-weight: bold;font-size: 12px;text-align:right;font-family: Arial,sans-serif;">
                                        <b>
                                          <xsl:variable name="TotalPayable" select="0"/>
                                          <xsl:variable name="Charges" select="TotalChg"/>
                                          <xsl:variable name="deposit" select="(.2 * $Charges)"/>
                                          $<xsl:value-of select="format-number($deposit,'#.00')"/>
                                        </b>
                                      </td>
                                    </tr>
                                  </xsl:if>

                                </xsl:for-each>

                              </xsl:if>



                              <!--customer looping-->
                              <xsl:if test="Customer/Curr != '' ">

                                <xsl:if test="Audience='Agent'  ">
                                  <tr>
                                    <td colspan="7" style="padding:5px 0 5px 10px;border-top: 1px solid {$borderColor};border-bottom:1px solid {$borderColor};">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">CUSTOMER TO PAY</font>
                                    </td>
                                  </tr>
                                </xsl:if>

                                <xsl:for-each select="Customer/Curr">
                                  <xsl:sort select="text()"/>
                                  <xsl:variable name="Curr" select="normalize-space(text())"/>

                                  <xsl:for-each select="DescList/Desc">
                                    <xsl:variable name="isDesc" select="."/>
                                    <xsl:if test="$isDesc!=''">
                                      <xsl:variable name="pos" select="position()"/>

                                      <!--from and to-->
                                      <tr>
                                        <td  style="padding:5px 0 5px 10px;">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                            <xsl:if test="@F !=''">
                                              <b>
                                                <xsl:value-of select="@F"/>
                                                <xsl:text> - </xsl:text>
                                                <xsl:value-of select="@T"/>
                                              </b>
                                            </xsl:if>
                                          </font>
                                        </td>
                                      </tr>


                                      <tr>

                                        <td style="padding:5px 0 5px 10px;">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                            <xsl:if test="@Typ='Dis'">
                                              <xsl:text>  </xsl:text>
                                            </xsl:if>
                                            <xsl:value-of select="."/>
                                          </font>
                                        </td>

                                        <td style="padding:5px 0 5px 10px;" align="center">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                            <xsl:if test="../../QtyList/Qty[position()=$pos]!=0">
                                              <xsl:value-of select="../../QtyList/Qty[position()=$pos]"/>
                                              <xsl:text> </xsl:text>
                                            </xsl:if>
                                          </font>
                                        </td>

                                        <td style="padding:5px 0 5px 0px;" align="center">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">x</font>
                                        </td>

                                        <td style="padding:5px 10px 5px 0px;" align="right">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                            <xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])!=0">
                                              <xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]) &lt; 0">
                                                <span style="color:{$negativeValueColor};">
                                                  $<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
                                                </span>
                                              </xsl:if>
                                              <xsl:if test="sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]) &gt; 0">
                                                $<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
                                              </xsl:if>
                                            </xsl:if>
                                            <xsl:if test="(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)])=0) and (../../TotalList/Total[position()=$pos]=0) and (../../QtyList/Qty[position()=$pos]!=0)">
                                              $<xsl:value-of select="format-number(sum(../../RateList/Rate[position()=$pos and sum(.) = sum(.)]),'#.00')"/>
                                            </xsl:if>
                                          </font>
                                        </td>

                                        <td style="padding:5px 10px 5px 0px;" align="center">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">=</font>
                                        </td>

                                        <td style="padding:5px 10px 5px 0px;" align="right">
                                          <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                            <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &lt; 0">
                                              <span style="color:{$negativeValueColor};">
                                                $<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
                                              </span>
                                            </xsl:if>
                                            <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &gt; 0">
                                              $<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
                                            </xsl:if>
                                            <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') = 0">
                                              $<xsl:value-of select="format-number(../../TotalList/Total[position()=$pos],'#.00')"/>
                                            </xsl:if>
                                          </font>
                                        </td>

                                        <xsl:choose>
                                          <xsl:when test="@Typ ='Dis' ">
                                            <xsl:if test="following-sibling::Desc[1]/@Typ !='Dis' or $pos = last() ">
                                              <xsl:variable name="path" select="preceding-sibling::*[@G][1]"/>
                                              <td style="padding:5px 10px 5px 0px;" align="right">
                                                <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                                  <xsl:call-template name="NetTotalRow">
                                                    <xsl:with-param name="rows" select="$path"/>
                                                  </xsl:call-template>
                                                </font>
                                              </td>
                                            </xsl:if>
                                          </xsl:when>

                                          <xsl:when test="@Typ ='Chg' ">
                                            <xsl:if test="following-sibling::Desc[1]/@Typ !='Dis'  or $pos = last()">

                                              <td style="padding:5px 10px 5px 0px;" align="right">
                                                <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                                  <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &lt; 0">
                                                    <span style="color:{$negativeValueColor};">
                                                      $<xsl:value-of select="format-number(@G,'#.00')"/>
                                                    </span>
                                                  </xsl:if>

                                                  <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') &gt; 0">
                                                    $<xsl:value-of select="format-number(@G,'#.00')"/>
                                                  </xsl:if>
                                                  <xsl:if test="format-number(../../TotalList/Total[position()=$pos],'#.00') = 0">
                                                    $<xsl:value-of select="format-number(@G,'#.00')"/>
                                                  </xsl:if>
                                                </font>
                                              </td>

                                            </xsl:if>
                                          </xsl:when>
                                        </xsl:choose>

                                      </tr>


                                    </xsl:if>
                                  </xsl:for-each>
                                  <tr/>
                                  <tr>
                                    <td></td>
                                    <td colspan="6" valign="top"  style="border-top: 2px solid #000; padding: 10px 0px 5px;" align="right"/>
                                  </tr>
                                  <tr>
                                    <td colspan="6" style="padding:5px 10px 5px 10px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        TOTAL <xsl:value-of select="$Curr"/> (includes GST)
                                      </font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        <xsl:variable name="Charges" select="TotalChg"/>
                                        $<xsl:value-of select="format-number($Charges,'#.00')"/>
                                      </font>
                                    </td>
                                  </tr>


                                  <tr>
                                    <td colspan="6" style="padding:5px 10px 5px 10px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                        GST Content

                                      </font>
                                    </td>
                                    <td style="padding:5px 10px 5px 0px;" align="right">
                                      <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif;  color:#000;">
                                        <xsl:variable name="PaymentRecvd" select="sum(../../Payments/*/pmt[../curr=$Curr])"/>
                                        $<xsl:value-of select="TotalGst"/>
                                      </font>

                                    </td>
                                  </tr>
                                </xsl:for-each>
                              </xsl:if>

                              <!--payment details-->
                              <xsl:if test="Payments/payment/curr != '' ">

                                <tr>
                                  <td style="font-size: 14px; background-color: {$paymentSummaryHeaderBgColor}; padding: 10px; font-weight: bold;
                                        border-bottom: 1px solid {$borderColor};font-family: Arial,sans-serif; border-top: 1px solid {$borderColor};" colspan="7">
                                    <b style="float: left;color:{$headerTextColor};">PAYMENT DETAILS</b>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="6" style="padding:5px 10px 5px 10px;" align="left">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif;  color:#000;">
                                      TOTAL PAID (includes cc fee if applicable)
                                    </font>
                                  </td>
                                  <td style="padding:5px 10px 5px 0px;" align="right">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif;  color:#000;">
                                      $<xsl:value-of select="Payments/payment/pmt"/>
                                    </font>
                                  </td>


                                </tr>
                                <tr>
                                  <td></td>
                                  <td colspan="6" valign="top"  style="border-top: 2px solid #000; padding: 10px 0px 5px;" align="right"/>
                                </tr>
                                <tr>

                                  <td colspan="6" style="padding:5px 10px 5px 10px;" align="left">
                                    <font style="font-family: Arial, Helvetica, sans-serif; color: #000; font-size: 11px; font-weight: bold;">
                                      <!--FINAL BALANCE (payable on pickup )-->
                                      <xsl:if test="Header/DispPmtTerm ='Y'    ">
                                        FINAL BALANCE
                                      </xsl:if>
                                      <xsl:if test="Header/DispPmtTerm ='N'    ">
                                        FINAL BALANCE
                                      </xsl:if>
                                    </font>
                                  </td>
                                  <td align="right" valign="top" style="padding: 10px 10px 5px;text-align:right;font-family: Arial,sans-serif;">
                                    <font style="font-family: Arial, Helvetica, sans-serif; color: #000; font-size: 11px; font-weight: bold;">
                                      $<xsl:value-of select="format-number(Payments/Balance,'#.00')"/>
                                    </font>
                                  </td>
                                </tr>


                              </xsl:if>


                              <xsl:if test="BookingDetails/ConfirmQuoteURL != ''  ">
                                <tr>
                                  <td colspan="7" align="right" style="padding-top:10px;">
                                    <table style="border-collapse:collapse;" border="0" cellspacing="0" cellpadding="0">
                                      <td width="65%">&amp;nbsp;</td>
                                      <td width="35%" align="center" style="border:1px solid {$headerTableBackColor};text-align:center; background:{$headerTableBackColor};padding:10px;">
                                        <table width="100%" style="border-collapse:collapse;" border="0" cellspacing="0" cellpadding="0">
                                          <td width="10%" align="center" style="text-align:center;padding:0px;">
                                            &amp;nbsp;
                                          </td>
                                          <td width="80%" align="center" style="text-align:center;padding:0px;">
                                            <a href="{BookingDetails/ConfirmQuoteURL}" style="color: {$headerTableForeColor};text-decoration:none;font:normal bold 12px/20px Verdana, Arial;">CONFIRM&amp;nbsp;QUOTE</a>
                                            <br/>
                                            <a href="{BookingDetails/ConfirmQuoteURL}" style="color: {$headerTableForeColor};text-decoration:none;font:normal bold 12px/20px Verdana, Arial;">ONLINE&amp;nbsp;NOW</a>
                                          </td>
                                          <td width="10%" align="center" style="text-align:center;">
                                            <a href="{BookingDetails/ConfirmQuoteURL}" style="padding:0px; color: {$headerTableForeColor};text-decoration:none;font:normal bold 16px/20px Verdana, Arial;">
                                              <img src="http://www.thlonline.com/CentralLibraryImages/Aurora/white_arrow.png" style="border:none;" alt=""/>
                                            </a>
                                          </td>
                                        </table>
                                      </td>
                                    </table>
                                  </td>
                                </tr>
                              </xsl:if>

                              <xsl:if test="Header/CountryCode='NZ' and (Header/Brand='Y' or Header/Brand='B' or Header/Brand='M')">
                                <xsl:variable name="itunesAppLink">
                                  <xsl:choose>
                                    <xsl:when test="Header/Brand='M'">https://itunes.apple.com/nz/app/maui-new-zealand-travel-guide/id583329177?mt=8</xsl:when>
                                    <xsl:when test="Header/Brand='B'">https://itunes.apple.com/nz/app/britz-new-zealand-travel-guide/id584729152?mt=8</xsl:when>
                                    <xsl:when test="Header/Brand='Y'">https://itunes.apple.com/nz/app/mighty-new-zealand-travel/id584734494?mt=8</xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                  </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="androidAppLink">
                                  <xsl:choose>
                                    <xsl:when test="Header/Brand='M'">https://play.google.com/store/apps/details?id=com.hummba.guide.maui</xsl:when>
                                    <xsl:when test="Header/Brand='B'">https://play.google.com/store/apps/details?id=com.hummba.guide.britz</xsl:when>
                                    <xsl:when test="Header/Brand='Y'">https://play.google.com/store/apps/details?id=com.hummba.guide.mighty</xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                  </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="appWebsiteLink">
                                  <xsl:choose>
                                    <xsl:when test="Header/Brand='M'">http://www.maui.co.nz/holiday-advice/tourism-radio</xsl:when>
                                    <xsl:when test="Header/Brand='B'">http://www.britz.co.nz/tourism-radio</xsl:when>
                                    <xsl:when test="Header/Brand='Y'">http://www.mightycampers.co.nz/cheap-campervan-hire-new-zealand/mighty-travel-app</xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                  </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="appWebsiteLinkImage">
                                  <xsl:choose>
                                    <xsl:when test="Header/Brand='M'">http://www.thlonline.com/centrallibraryimages/aurora/maui_phones.jpg</xsl:when>
                                    <xsl:when test="Header/Brand='B'">http://www.thlonline.com/centrallibraryimages/aurora/britz_phones.jpg</xsl:when>
                                    <xsl:when test="Header/Brand='Y'">http://www.thlonline.com/centrallibraryimages/aurora/mighty_phones.jpg</xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                  </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="itunesAppLinkImage">
                                  <xsl:choose>
                                    <xsl:when test="Header/Brand='M'">http://www.thlonline.com/centrallibraryimages/aurora/maui_itunes.jpg</xsl:when>
                                    <xsl:when test="Header/Brand='B'">http://www.thlonline.com/centrallibraryimages/aurora/britz_itunes.jpg</xsl:when>
                                    <xsl:when test="Header/Brand='Y'">http://www.thlonline.com/centrallibraryimages/aurora/mighty_itunes.jpg</xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                  </xsl:choose>
                                </xsl:variable>

                                <xsl:variable name="androidAppLinkImage">
                                  <xsl:choose>
                                    <xsl:when test="Header/Brand='M'">http://www.thlonline.com/centrallibraryimages/aurora/maui_play.jpg</xsl:when>
                                    <xsl:when test="Header/Brand='B'">http://www.thlonline.com/centrallibraryimages/aurora/britz_play.jpg</xsl:when>
                                    <xsl:when test="Header/Brand='Y'">http://www.thlonline.com/centrallibraryimages/aurora/mighty_play.jpg</xsl:when>
                                    <xsl:otherwise></xsl:otherwise>
                                  </xsl:choose>
                                </xsl:variable>


                                <tr>
                                  <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$headerBgColor};" colspan="7">
                                    <b style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:{$headerTextColor};">GET OUR FREE TRAVEL APP</b>
                                  </td>
                                </tr>
                                <tr>
                                  <td colspan="7" style="padding: 10px 0px 10px 5px;">
                                    <table>
                                      <tr>
                                        <td colspan="2" style="padding:0px 10px 10px 0px; font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                          The ultimate audio travel guide from
                                          <xsl:choose>
                                            <xsl:when test="Header/Brand='M'">
                                              Maui
                                            </xsl:when>
                                            <xsl:when test="Header/Brand='B'">
                                              Britz
                                            </xsl:when>
                                            <xsl:when test="Header/Brand='Y'">
                                              Mighty
                                            </xsl:when>
                                            <xsl:otherwise></xsl:otherwise>
                                          </xsl:choose>
                                          is crammed with well-researched,
                                          local information and it&#39;ll advise you about points of interest as you approach them. All right on your phone!
                                          <br/>
                                          <br/>
                                          Available for

                                          <a target="_blank" style="color:{$contactUSlink};font-size:11px;" href="{$itunesAppLink}">iPhone here</a>

                                          and

                                          <a target="_blank" style="color:{$contactUSlink};font-size:11px;" href="{$androidAppLink}">Android here.</a>

                                          Get more info

                                          <a target="_blank" style="color:{$contactUSlink};font-size:11px;" href="{$appWebsiteLink}">here.</a>
                                        </td>
                                        <td rowspan="2">
                                          <a target="_blank" style="text-decoration:none;border:none;" href="{$appWebsiteLink}">
                                            <img src="{$appWebsiteLinkImage}" style="text-decoration:none;border:none;" width="146" height="175" alt="" />
                                          </a>
                                        </td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <a target="_blank" style="text-decoration:none;border:none;" href="{$itunesAppLink}">
                                            <img src="{$itunesAppLinkImage}" style="text-decoration:none;border:none;" width="203" height="97" alt="" />
                                          </a>
                                        </td>
                                        <td>
                                          <a target="_blank" style="text-decoration:none;border:none;" href="{$androidAppLink}">
                                            <img src="{$androidAppLinkImage}" style="text-decoration:none;border:none;" width="203" height="97" alt="" />
                                          </a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </xsl:if>

                              <tr>
                                  <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor}; border-bottom: 1px solid {$borderColor}; background-color:{$headerBgColor};" colspan="7">
                                    <b style="font-size:14px; font-family:Arial, Helvetica, sans-serif; color:{$headerTextColor};">SAFE DRIVING</b>
                                  </td>
                                </tr>
                              <!--<xsl:if test="Header/CountryCode='NZ' ">-->
                                
                                <tr>
                                   <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor}; height:55px;" colspan="7" >
                                     
                                     <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                       <xsl:choose>
                                         <xsl:when test="Header/CountryCode='NZ'">
                                           Safe and enjoyable journeys driving in New Zealand, find out more here:
                                           <a target="_blank" style="color:{$contactUSlink};font-size: 11px"  href="http://www.drivesafe.org.nz">
                                             www.drivesafe.org.nz
                                           </a>
                                         </xsl:when>
                                       <xsl:when test="Header/CountryCode='AU'">
                                           Safe and enjoyable journeys driving in Australia, find out more here:
                                           <a target="_blank" style="color:{$contactUSlink};font-size: 11px"  href="http://www.ntc.gov.au">
                                            www.ntc.gov.au
                                           </a>
                                         </xsl:when>
                                       </xsl:choose>
                                     </font>
                                  
                                  </td>
                                
                                </tr>
                              
                              
                              <tr>
                                <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};  background-color:{$footNoteBackColor};" colspan="7">
                                  <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                    <!--<xsl:if test="Header/Brand!='U' and BookingDetails/ConfirmQuoteURL != ''">-->
                                     <!--Test[contains(Name,'Pre')-->
                                    <!--<xsl:if test="Header/VehicleStatus !='Concelled'">-->
                                    <xsl:if test="Header[not(contains(VehicleStatus,'Cancell'))] and BookingDetails/PRDType='AV'">
                                      For information on our Privacy Policy Collection Notice please click
                                      <xsl:choose>
                                        <xsl:when test="Header/Brand='Q' and Header/CountryCode='NZ'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px"  href="http://nzrentals.keacampers.com/Pages/disclaimer-and-privacy.aspx">
                                            
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='Q' and Header/CountryCode='AU'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://aurentals.keacampers.com/Pages/disclaimer-and-privacy.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='M' and Header/CountryCode='NZ'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.maui.co.nz/Pages/privacy-policy.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='M' and Header/CountryCode='AU'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.maui.com.au/Pages/privacy-policy.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='B'  and Header/CountryCode='NZ'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.britz.co.nz/contactus/Pages/privacy-statement.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='B'  and Header/CountryCode='AU'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.britz.com.au/contactus/Pages/privacy-statement.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='Y'  and Header/CountryCode='NZ'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.mightycampers.co.nz/Pages/privacy.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='Y'  and Header/CountryCode='AU'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.mightycampers.com.au/Pages/privacy-policy.aspx">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='A'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.alphacampervans.co.nz/about-us/privacypolicy">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:when test="Header/Brand='U'">
                                          <a target="_blank" style="color:{$contactUSlink};font-size: 11px" href="http://www.unitedcampervans.co.nz/about-us/privacy-policy">
                                            here.
                                          </a>
                                        </xsl:when>
                                        <xsl:otherwise></xsl:otherwise>
                                      </xsl:choose>
                                      <!--<xsl:if test="Header/Brand='Q'    ">
                                        <a target="_blank" style="color:{$contactUSlink};font-size: 11px">
                                          <xsl:attribute name="href">
                                            http://aurentals.keacampers.com/en/contact.aspx
                                          </xsl:attribute>
                                          website
                                        </a>
                                      </xsl:if >-->
                                      <!--</xsl:if>-->
                                    </xsl:if>
                                    <br/>

                                    For all enquiries, please visit our

                                    <xsl:if test="Header/Brand='Q'    ">
                                      <a target="_blank" style="color:{$contactUSlink};font-size: 11px">
                                        <xsl:attribute name="href">
                                          http://aurentals.keacampers.com/en/contact.aspx
                                        </xsl:attribute>
                                        website
                                      </a>
                                      or call our 24/7 Reservations Team on our
                                      <a target="_blank" style="color:{$contactUSlink}; font-size: 11px">
                                        <xsl:attribute name="href">
                                          http://aurentals.keacampers.com/en/contact.aspx
                                        </xsl:attribute>
                                        international freephone numbers
                                      </a>
                                    </xsl:if>

                                    <xsl:if test="Header/Brand!='M' and Header/Brand!='Q'">
                                      <a target="_blank" style="color:{$contactUSlink};font-size: 11px">
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="Header/Web"/>/contactUs
                                        </xsl:attribute>
                                        website
                                      </a>
                                      or call our 24/7 Reservations Team on our
                                      <a target="_blank" style="color:{$contactUSlink}; font-size: 11px">
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="Header/Web"/>/contactUs
                                        </xsl:attribute>
                                        international freephone numbers
                                      </a>
                                    </xsl:if>
                                    <xsl:if test="Header/Brand='M'    ">
                                      <a target="_blank" style="color:{$mauiQuoteLinkInfooter}; font-size: 11px">
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="Header/Web"/>/contactUs
                                        </xsl:attribute>
                                        website
                                      </a>
                                      or call our 24/7 Reservations Team on our
                                      <a target="_blank" style="color:{$mauiQuoteLinkInfooter}; font-size: 11px">
                                        <xsl:attribute name="href">
                                          <xsl:value-of select="Header/Web"/>/contactUs
                                        </xsl:attribute>
                                        international freephone numbers
                                      </a>
                                    </xsl:if>

                                    <br/>
                                    <br/>


                                    <b>
                                      <xsl:value-of select="Header/FooterText"/>
                                    </b> is a brand of <xsl:value-of select="Header/CompanyName"/> ,
                                    <xsl:call-template name="string-replace-all">
                                      <xsl:with-param name="text" select="Header/address"/>
                                      <xsl:with-param name="replace" select=" '[NL]' "/>
                                      <xsl:with-param name="by" select=" ', ' "/>
                                    </xsl:call-template>

                                    <xsl:if test="Header/Cty='AU'">
                                      <xsl:value-of select="Header/ABNNumber"/>
                                    </xsl:if>
                                    <xsl:if test="Header/Cty='NZ'">
                                      <xsl:value-of select="Header/GST"/>
                                    </xsl:if>
                                  </font>
                                </td>

                              </tr>


                              <xsl:if test="CfnFooterNotes/NewsFlash != ''">
                                <tr>
                                  <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};  background-color:{$footNoteBackColor};" colspan="7">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      <xsl:value-of select="CfnFooterNotes/NewsFlash" />
                                    </font>
                                  </td>
                                </tr>

                              </xsl:if>

                              <xsl:for-each select="CfnFooterNotes/Note">
                                <tr>
                                  <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};  background-color:{$footNoteBackColor};" width="20%">
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#000;">
                                      <xsl:if test="NtsType!=preceding-sibling::node()[position()=1]/NtsType or position()=1">
                                        <xsl:value-of select="NtsType"/>
                                      </xsl:if>
                                    </font>
                                  </td>
                                  <td style="padding:5px 0 5px 10px; border-top: 1px solid {$borderColor};  background-color:{$footNoteBackColor};" width="80%" colspan="6" >
                                    <font style="font-size:11px; font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000;">
                                      <xsl:value-of select="NtsText"/>
                                    </font>
                                  </td>
                                </tr>


                              </xsl:for-each>

                            </table>
                          </td>
                        </tr>
                      </table>

                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </xsl:for-each>
      </body>
    </html>
  </xsl:template>


  <xsl:template name="lf2br">
    <xsl:param name="StringToTransform"/>
    <xsl:choose>
      <xsl:when test="contains($StringToTransform,'[NL]')">
        <xsl:value-of select="substring-before($StringToTransform,'[NL]')"/>
        <br/>
        <xsl:call-template name="lf2br">
          <xsl:with-param name="StringToTransform">
            <xsl:value-of select="substring-after($StringToTransform,'[NL]')"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$StringToTransform"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="lineBreak">
    <xsl:param name="input"/>
    <xsl:choose>
      <xsl:when test="contains($input,'&#xA;')">
        <xsl:value-of select="substring-before($input,'&#xA;')"/>
        <br/>
        <xsl:call-template name="lineBreak">
          <xsl:with-param name="input">
            <xsl:value-of select="substring-after($input,'&#xA;')"/>
          </xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$input"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:template name="string-replace-all">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="by"/>
    <xsl:choose>
      <xsl:when test="contains($text, $replace)">
        <xsl:value-of select="substring-before($text,$replace)"/>
        <xsl:value-of select="$by"/>
        <xsl:call-template name="string-replace-all">
          <xsl:with-param name="text" select="substring-after($text,$replace)"/>
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="by" select="$by"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  <xsl:variable name="myVar">
    <xsl:call-template name="string-replace-all">
      <xsl:with-param name="text" select="'This is a sample text : {ReplaceMe} and {ReplaceMe}'"/>
      <xsl:with-param name="replace" select="'{ReplaceMe}'"/>
      <xsl:with-param name="by" select="'String.Replace() in XSLT'"/>
    </xsl:call-template>
  </xsl:variable>
  <xsl:param name="GlobalParam"/>
  <xsl:variable name="sibling-preceders" select="preceding-sibling::DescList/Desc/@G"/>

  <xsl:template name="NetTotalRow">
    <xsl:param name="locationcount" select="1"/>
    <xsl:param name="rows"/>
    <xsl:if test="$locationcount > 0 ">
      <xsl:if test="format-number($rows[position()][@Typ='Chg'][last()]/@G,'#.00') &lt; 0">
        <span style="color:'#ff0000;' ">
          $<xsl:value-of select="format-number($rows[position()][@Typ='Chg'][last()]/@G,'#.00')"/>
        </span>
      </xsl:if>
      <xsl:if test="format-number($rows[position()][@Typ='Chg'][last()]/@G,'#.00') &gt; 0">
        $<xsl:value-of select="format-number($rows[position()][@Typ='Chg'][last()]/@G,'#.00')"/>
      </xsl:if>
      <xsl:if test="format-number($rows[position()][@Typ='Chg'][last()]/@G,'#.00') = 0">
        $<xsl:value-of select="format-number($rows[position()][@Typ='Chg'][last()]/@G,'#.00')"/>
      </xsl:if>
    </xsl:if>
  </xsl:template>
</xsl:stylesheet>
