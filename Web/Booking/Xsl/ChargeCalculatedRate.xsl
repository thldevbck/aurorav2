<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <xsl:for-each select="/data">
      <table width="650" border="0" cellspacing="0" cellpadding="0" align="center">
        <tr>
          <td colspan="3">&#160;</td>
        </tr>
		<tr>
			  <td colspan="3">Calculated Rate for Booking: <b><xsl:value-of select="Rental/BooNum"/></b>
				  <br></br>
			  </td>
		</tr>
		  
        <!-- Agent Details -->
        <tr>
          <td colspan="3">
            <table width="100%" border="0">
				<tr >
					<td>
						Agent : <B>
							<xsl:value-of select="Rental/AgnName"/>
						</B>
					</td>
					<td align="RIGHT">
						Hirer : <B>
							<xsl:value-of select="Rental/Hirer"/>
						</B>
					</td>
					<td align="RIGHT">
						Status : <B>
							<xsl:value-of select="Rental/BookingSt"/>
						</B>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<!-- <tr><td colspan="4">&#160;</td></tr> -->
	<!-- Rental Details -->
	<tr>
		<td colspan="3">
			<table width="100%" class="dataTable">
				<th bgcolor="#A7A7A7"  >
				  <tr bgcolor="#A7A7A7" >
					  <td>
						  <B>Rental</B>
					  </td>
					  <td>
						  <B>Check-out</B>
					  </td>
					  <td>
						  <B>Check-in</B>
					  </td>
					  <td>
						  <B>Hire Pd</B>
					  </td>
					  <td>
						  <B>Package</B>
					  </td>
					  <td>
						  <B>Brand</B>
					  </td>
					  <td>
						  <B>Vehicle</B>
					  </td>
					  <td>
						  <B>Status</B>
					  </td>
				  </tr>
              </th>
              <tbody>
                <tr>
					<xsl:attribute name="bgcolor"><xsl:value-of select="ui/statusColor"/></xsl:attribute>
                  <td >
                    <a1>
                      <xsl:attribute name="href">#a</xsl:attribute>
                      <xsl:attribute name="onclick">
                        navigateScreen('<xsl:value-of select="Rental/RntId"/>')
                      </xsl:attribute>
                      <xsl:value-of select="Rental/Rental"/>
                    </a1>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/Check-Out"/>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/Check-In"/>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/HirePd"/>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/Package"/>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/Brand"/>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/Vehicle"/>
                  </td>
                  <td >
                    <xsl:value-of select="Rental/Status"/>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="3">&#160;</td>
        </tr>
        <!-- Product Details -->
        <tr>
          <td colspan="3">
            <table width="100%" border="0">
              <tr>
                <td>
                  Product : <B>
                    <xsl:value-of select="PrdDet/@Prd"/>
                  </B>
                </td>
                <td align="RIGHT">
                  Status Initial : <B>
                    <xsl:value-of select="PrdDet/@StatusI"/>
                  </B>
                </td>
                <td align="RIGHT">
                  Current Status : <B>
                    <xsl:value-of select="PrdDet/@StatusC"/>
                  </B>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <!-- <tr><td colspan="4">&#160;</td></tr> -->
        <!-- Actual Summary Details -->
        <tr>
          <td colspan="3">
            <table width="100%" border="0" bgcolor="gray" cellpadding="0" cellspacing="1">
              <thead>
                <tr align="center" bgcolor="#A7A7A7">
                  <td>
                    <B>Detail</B>
                  </td>
                  <td>
                    <B>Description</B>
                  </td>
                  <td align="RIGHT">
                    <B>%</B>
                  </td>
                  <td>
                    <B>
                      [<xsl:value-of select="PrdDet/@Curr"/>] Value
                    </B>
                  </td>
                </tr>
              </thead>
              <tbody>
                <xsl:for-each select="Detail/BaseRate">
                  <tr>
                    <td class="readonly" colspan="4">
                      <B>&#160;Base Rate</B>
                    </td>
                  </tr>
                  <tr>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Detail"/>&#160;
                    </td>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Desc"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@Per"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<B>
                        <xsl:value-of select="@DEMVal"/>
                      </B>&#160;
                    </td>
                  </tr>
                </xsl:for-each>
                <tr>
                  <td class="readonly" colspan="4">&#160;</td>
                </tr>

                <xsl:if test="Detail/Discounts[. = '']">
                  <tr>
                    <td colspan="4" class="readonly">
                      <B>&#160;Discounts</B>
                    </td>
                  </tr>
                  <!-- Discount Part -->
                  <xsl:for-each select="Detail/Discounts/Discount">
                    <xsl:if test="@Detail[. != '']">
                      <tr>
                        <td class="readonly" align="LEFT">
                          &#160;<xsl:value-of select="@Detail"/>&#160;
                        </td>
                        <td class="readonly" align="LEFT">
                          &#160;<xsl:value-of select="@Desc"/>&#160;
                        </td>
                        <td class="readonly" align="RIGHT">
                          &#160;<xsl:value-of select="@Per"/>&#160;
                        </td>
                        <td class="readonly" align="RIGHT">
                          &#160;<xsl:value-of select="@DEMVal"/>&#160;
                        </td>
                      </tr>
                    </xsl:if>
                  </xsl:for-each>

                  <!-- Total Part -->
                  <xsl:for-each select="Detail/TotDis">
                    <tr>
                      <td class="readonly" align="LEFT">
                        &#160;<B>
                          <xsl:value-of select="@Detail"/>
                        </B>&#160;
                      </td>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Desc"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@Per"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<B>
                          <xsl:value-of select="@DEMVal"/>
                        </B>&#160;
                      </td>
                    </tr>
                  </xsl:for-each>
                  <tr>
                    <td colspan="4" class="readonly">
                      <B>&#160;</B>
                    </td>
                  </tr>
                </xsl:if>

                <!-- RateLesDis Part -->
                <xsl:for-each select="Detail/RateLesDis">
                  <tr>
                    <td class="readonly" align="LEFT">
                      &#160;<B>
                        <xsl:value-of select="@Detail"/>
                      </B>&#160;
                    </td>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Desc"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@Per"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<B>
                        <xsl:value-of select="@DEMVal"/>
                      </B>&#160;
                    </td>
                  </tr>
                </xsl:for-each>
                <tr>
                  <td colspan="4" class="readonly">
                    <B>&#160;</B>
                  </td>
                </tr>

                <!-- Gross Part -->
                <xsl:for-each select="Detail/Gross">
                  <tr>
                    <td class="readonly" align="LEFT">
                      &#160;<B>
                        <xsl:value-of select="@Detail"/>
                      </B>&#160;
                    </td>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Desc"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@Per"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<B>
                        <xsl:value-of select="@DEMVal"/>
                      </B>&#160;
                    </td>
                  </tr>
                </xsl:for-each>
                <tr>
                  <td colspan="4" class="readonly">
                    <B>&#160;</B>
                  </td>
                </tr>

                <!-- AgnDis Part -->
                <xsl:for-each select="Detail/AgnDis">
                  <tr>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Detail"/>&#160;
                    </td>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Desc"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@Per"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@DEMVal"/>&#160;
                    </td>
                  </tr>
                </xsl:for-each>

                <!-- GST Part -->
                <xsl:for-each select="Detail/GST">
                  <tr>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Detail"/>&#160;
                    </td>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Desc"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@Per"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@DEMVal"/>&#160;
                    </td>
                  </tr>
                </xsl:for-each>

                <!-- FOC Part -->
                <xsl:if test="Detail/FOC/@Detail[. != '']">
                  <xsl:for-each select="Detail/FOC">
                    <tr>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Detail"/>&#160;
                      </td>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Desc"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@Per"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@DEMVal"/>&#160;
                      </td>
                    </tr>
                  </xsl:for-each>
                </xsl:if>

                <!-- MinCharge Part -->
                <xsl:if test="Detail/MinCharge/@Detail[. != '']">
                  <tr>
                    <td colspan="4" class="readonly">
                      <B>&#160;</B>
                    </td>
                  </tr>
                  <xsl:for-each select="Detail/MinCharge">
                    <tr>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Detail"/>&#160;
                      </td>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Desc"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@Per"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@DEMVal"/>&#160;
                      </td>
                    </tr>
                  </xsl:for-each>
                </xsl:if>
                <!-- MaxCharge Part -->
                <xsl:if test="Detail/MaxCharge/@Detail[. != '']">
                  <xsl:for-each select="Detail/MaxCharge">
                    <tr>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Detail"/>&#160;
                      </td>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Desc"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@Per"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@DEMVal"/>&#160;
                      </td>
                    </tr>
                  </xsl:for-each>
                </xsl:if>

                <!-- ZeroCost Part -->
                <xsl:if test="Detail/ZeroCost/@Detail[. != '']">
                  <xsl:for-each select="Detail/ZeroCost">
                    <tr>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Detail"/>&#160;
                      </td>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Desc"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@Per"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@DEMVal"/>&#160;
                      </td>
                    </tr>
                  </xsl:for-each>
                </xsl:if>

                <!-- Rounded Part -->
                <xsl:if test="Detail/Rounded/@Detail[. != '']">
                  <xsl:for-each select="Detail/Rounded">
                    <tr>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Detail"/>&#160;
                      </td>
                      <td class="readonly" align="LEFT">
                        &#160;<xsl:value-of select="@Desc"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@Per"/>&#160;
                      </td>
                      <td class="readonly" align="RIGHT">
                        &#160;<xsl:value-of select="@DEMVal"/>&#160;
                      </td>
                    </tr>
                  </xsl:for-each>
                </xsl:if>

                <tr>
                  <td colspan="4" class="readonly">
                    <B>&#160;</B>
                  </td>
                </tr>
                <!-- TotOwn Part -->
                <xsl:for-each select="Detail/TotOwn">
                  <tr>
                    <td class="readonly" align="LEFT">
                      &#160;<B>
                        <xsl:value-of select="@Detail"/>
                      </B>&#160;
                    </td>
                    <td class="readonly" align="LEFT">
                      &#160;<xsl:value-of select="@Desc"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<xsl:value-of select="@Per"/>&#160;
                    </td>
                    <td class="readonly" align="RIGHT">
                      &#160;<B>
                        <xsl:value-of select="@DEMVal"/>
                      </B>&#160;
                    </td>
                  </tr>
                </xsl:for-each>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="3">&#160;</td>
        </tr>
        <tr>
          <td colspan="3">
            <DIV class="linkfun">
              <table>
                <tr>
                  <!--<td>
										<a href="#" OnClick="window.print()">Print</a>
									</td>-->
                  <td>
                    <!--<a href="#" onclick="submitData('close')">Close</a>-->
                  </td>
                </tr>
              </table>
            </DIV>
          </td>
        </tr>
      </table>
    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>

