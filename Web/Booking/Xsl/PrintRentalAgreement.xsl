<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:template match="/">
		<style type="text/css">
			.rentalAgreement
			{
			background-color: white;
			}
			.rentalAgreement td
			{
			font-size: 8pt;
			font-family: Arial;
			}
			.rentalAgreement table.tableBlackBorder
			{
			width:100%;
			border:solid 2px black;
			}

		</style>

		<div class="rentalAgreement">
			<TABLE border="0" cellpadding="1" cellSpacing="0" width="100%" ID="Table10">
				<TR>
					<TD vAlign="top" width="50%" style="FONT-WEIGHT: bold; FONT-SIZE: 34pt; FONT-STYLE: italic; FONT-FAMILY: Arial">
						<i>Rental&#160;Agreement</i>
					</TD>
					<td width="50%">
						<table class="tableBlackBorder"  cellpadding="0"  rules="none" cellspacing="0">
							<tr>
								<td style="FONT-WEIGHT: bold">&#160;Licensee:</td>
								<td>
									<xsl:value-of select="/Data/AgreementHeader/Licensee"/>
								</td>
							</tr>
							<!--Changed by Shoel-->
							<!-- displays data only if name is Tourism Holding Ltd and only for NZ -->
							<xsl:if test="/Data/AgreementHeader/Licensee = 'Tourism Holdings Limited' ">
								<xsl:if test="/Data/RentalDetails/Details/Country = 'NZ'" >
									<tr>
										<td></td>
										<td>
											Transport Service License Number 100 628 538
										</td>
									</tr>
								</xsl:if>
							</xsl:if>
							<!--End change-->
							<xsl:if test="/Data/AgreementHeader/ABNNumber !=''">
								<tr>
									<td>
										<b>&#160;ABN Number:</b>
									</td>
									<td>
										<xsl:value-of select="/Data/AgreementHeader/ABNNumber"/>
									</td>
								</tr>
							</xsl:if>
						</table>
					</td>
				</TR>
				<TR>
					<TD/>
				</TR>
			</TABLE>

			<BR/>

			<TABLE border="0" cellSpacing="0" width="100%">
				<TR>
					<TD  width="20%" align="left">
						<b>Contract Number</b>
					</TD>
					<TD  width="44%">
						<xsl:value-of select="/Data/AgreementHeader/ContractNum"/>
					</TD>
					<TD  width="10%" align="right">
						<b>Status:</b>
					</TD>
					<TD  width="26%">
						&#160;&#160;<xsl:value-of select="/Data/AgreementHeader/RentalStatus"/>
					</TD>
				</TR>
				<TR>
					<TD  align="left">
						<b>Date:</b>
					</TD>
					<TD>
						<xsl:value-of select="/Data/AgreementHeader/Date"/>
					</TD>
					<TD  align="right">
						<b>Voucher No:</b>
					</TD>
					<td>
						&#160;&#160;<xsl:value-of select="/Data/AgreementHeader/VoucherNum"/>
					</td>
				</TR>
				<TR>
					<TD  align="left">
						<b>Agent:</b>
					</TD>
					<TD>
						<xsl:value-of select="/Data/AgreementHeader/Agent"/>
					</TD>
					<TD  align="right">
						<b>Agent Name:</b>
					</TD>
					<TD >
						&#160;&#160;<xsl:value-of select="/Data/AgreementHeader/AgentName"/>
					</TD>
				</TR>
			</TABLE>

			<br/>

			<TABLE class="tableBlackBorder" cellSpacing="0"  ID="Table1">
				<TR>
					<TD bgColor="black" colSpan="5">
						<FONT color="#ffffff">&#160;Hirer(s) Details</FONT>
					</TD>
				</TR>
				<TR>
					<TD  width="21%">
						<b>&#160;Name</b>
					</TD>
					<TD  width="31%">
						<b>Address</b>
					</TD>
					<TD  width="20%">
						<b>Country</b>
					</TD>
					<TD  width="17%">
						<b>Phone Number</b>
					</TD>
					<TD  width="12%">
						<b>Mobile</b>
					</TD>
				</TR>
				<xsl:for-each select="/Data/HirerDetails/Traveller">
					<TR>
						<TD vAlign="top">
							&#160;<xsl:value-of select="Name"/>
						</TD>
						<TD>
							<xsl:call-template name="lf2br">
								<xsl:with-param name="StringToTransform" select="Address"/>
							</xsl:call-template>
						</TD>
						<td valign="top">
							<xsl:value-of select="Country"/>
						</td>
						<td valign="top">
							<xsl:value-of select="Phone"/>
						</td>
						<td valign="top">
							<xsl:value-of select="Mobile"/>
						</td>
					</TR>
				</xsl:for-each>
			</TABLE>

			<BR/>

			<TABLE class="tableBlackBorder" cellSpacing="0" >
				<TR>
					<TD bgColor="black" colSpan="6">
						<FONT color="#ffffff">&#160;Driver(s)&#160;Details</FONT>
					</TD>
				</TR>
				<TR>
					<TD  width="21%">
						<b>&#160;Name</b>
					</TD>
					<TD  width="10%">
						<b>DOB</b>
					</TD>
					<TD  width="14%">
						<b>Type</b>
					</TD>
					<TD  width="17%">
						<b>Licence Number</b>
					</TD>
					<TD  width="15%">
						<b>Expiry Date</b>
					</TD>
					<TD  width="13%">
						<b>Signature</b>
					</TD>
				</TR>
				<xsl:for-each select="/Data/DriverDetails/Customer">
					<TR>
						<TD>
							&#160;<xsl:value-of select="Name"/>
						</TD>
						<TD>
							<xsl:value-of select="DOB"/>
						</TD>
						<TD>
							<xsl:value-of select="Type"/>
						</TD>
						<TD>
							<xsl:value-of select="LicenceNo"/>
						</TD>
						<TD>
							<xsl:value-of select="LicExpiryDate"/>
						</TD>
						<TD>____________________</TD>
					</TR>
				</xsl:for-each>
			</TABLE>

			<BR/>

			<TABLE class="tableBlackBorder" cellpadding="0" cellSpacing="0"  >
				<TR>
					<TD bgColor="black" colSpan="6">
						<FONT color="#ffffff">&#160;Travel&#160;Dates</FONT>
					</TD>
				</TR>
				<TR>
					<TD  width="10%">
						<b>&#160;From:</b>
					</TD>
					<TD  width="21%">
						<xsl:value-of select="/Data/TravelDates/Rental/LocFrom"/>
					</TD>
					<TD  width="8%">
						<b>To:</b>
					</TD>
					<TD  width="36%">
						<xsl:value-of select="/Data/TravelDates/Rental/LocTo"/>
					</TD>
					<TD  width="10%">
						<b>Hire:</b>
					</TD>
					<TD  width="15%">
						<xsl:value-of select="/Data/TravelDates/Rental/HIRE"/>&#160;
					</TD>
				</TR>
				<TR>
					<TD/>
					<TD>
						<xsl:value-of select="/Data/TravelDates/Rental/TDFrom"/>
					</TD>
					<TD/>
					<TD>
						<xsl:value-of select="/Data/TravelDates/Rental/TDTo"/>
					</TD>
					<TD>
						<b>Tel:</b>
					</TD>
					<TD>
						<xsl:value-of select="/Data/TravelDates/Rental/Tel"/>
					</TD>
				</TR>
				<TR>
					<TD/>
					<TD/>
					<TD/>
					<TD>
						<xsl:value-of select="/Data/TravelDates/Rental/Address"/>
					</TD>
					<TD/>
					<TD/>
				</TR>
			</TABLE>

			<BR/>
			<xsl:variable name="isExchange" select="/Data/isExchange"/>

			<TABLE class="tableBlackBorder" cellSpacing="0"  ID="Table4">
				<TR>
					<TD bgColor="black" colSpan="11">
						<FONT color="#ffffff">&#160;Vehicle Details</FONT>
					</TD>
				</TR>
				<xsl:for-each select="/Data/VehicleDetails/BookedProduct">
					<TR>
						<TD  width="8%">
							<b> Reg:</b>
						</TD>
						<TD  width="10%">
							<!--xsl:value-of select="/Data/VehicleDetails/BookedProduct/Reg"/-->
							<xsl:if test="$isExchange='true'">
								<xsl:if test="position()=1">
									<xsl:value-of select="Reg"/>&#160;*
								</xsl:if>
								<xsl:if test="position()!=1">
									<xsl:value-of select="Reg"/>
								</xsl:if>
							</xsl:if>
							<xsl:if test="$isExchange!='true'">
								<xsl:value-of select="Reg"/>
							</xsl:if>
						</TD>
						<TD  width="8%">
							<b>Unit No:</b>
						</TD>
						<TD  width="9%">
							<!--xsl:value-of select="/Data/VehicleDetails/BookedProduct/UnitNo"/-->
							<xsl:value-of select="UnitNo"/>
						</TD>
						<TD  width="9%">
							<b>Vehicle:</b>
						</TD>
						<TD  width="19%">
							<!--xsl:value-of select="/Data/VehicleDetails/BookedProduct/Vehicle"/-->
							<xsl:value-of select="Vehicle"/>
						</TD>
						<TD  width="14%">
							<b>Odometer:</b>
						</TD>
						<TD  width="7%">
							<!--xsl:value-of select="/Data/VehicleDetails/BookedProduct/OdometerOut"/-->
							<xsl:value-of select="OdometerOut"/>
						</TD>
						<TD  width="6%">
							<b>Out</b>
						</TD>
						<TD  width="6%">
							<!--xsl:value-of select="/Data/VehicleDetails/BookedProduct/OdometerIn"/-->
							<xsl:value-of select="OdometerIn"/>
						</TD>
						<TD  width="6%">
							<b>In</b>
						</TD>
					</TR>
				</xsl:for-each>
			</TABLE>

			<br/>

			<TABLE class="tableBlackBorder" cellSpacing="0"  ID="Table5">
				<TR>
					<TD bgColor="black" colSpan="7">
						<FONT color="#ffffff">&#160;Rental&#160;Details</FONT>
					</TD>
				</TR>
				<TR>
					<TD  width="10%">
						<b>&#160;Date</b>
					</TD>
					<TD  width="10%">
						<b>Product</b>
					</TD>
					<TD  width="33%">
						<b>Description</b>
					</TD>
					<TD  width="18%">
						<b>Hire Period</b>
					</TD>
					<TD  width="8%" align="right">
						<b>Qty</b>
					</TD>
					<TD  width="10%" align="right">
						<b>Currency</b>
					</TD>
					<TD  width="11%" align="right">
						<b>Amount</b>
					</TD>
				</TR>

				<xsl:for-each select="/Data/RentalDetails/Details">
					<TR>
						<TD>
							<xsl:value-of select="BpdDate"/>
						</TD>
						<TD>
							<xsl:if test="$isExchange='true'">
								<xsl:if test="position()=1">
									<xsl:value-of select="Product"/>&#160;*
								</xsl:if>
								<xsl:if test="position()!=1">
									<xsl:value-of select="Product"/>
								</xsl:if>
							</xsl:if>
							<xsl:if test="$isExchange!='true'">
								<xsl:value-of select="Product"/>
							</xsl:if>
						</TD>
						<TD>
							<xsl:value-of select="Description"/>
						</TD>
						<TD>
							<xsl:value-of select="HirePeriod"/>&#160;
						</TD>
						<xsl:variable name="quantity" select="Qty"/>
						<!--TD align="right"><xsl:value-of select="Qty"/></TD-->
						<xsl:if test="$quantity=0">
							<TD align="right">&#160;</TD>
						</xsl:if>
						<xsl:if test="$quantity!=0">
							<TD align="right">
								<xsl:value-of select="Qty"/>
							</TD>
						</xsl:if>
						<TD align="right">
							<xsl:value-of select="Currency"/>
						</TD>
						<TD align="right">
							<xsl:value-of select="Amount"/>
						</TD>
					</TR>
				</xsl:for-each>
			</TABLE>

			<br/>

			<TABLE class="tableBlackBorder" cellSpacing="0"  ID="Table6">
				<TR>
					<TD bgColor="black" colSpan="5">
						<FONT color="#ffffff">&#160;Payment/Refund Details</FONT>
					</TD>
				</TR>
				<TR>
					<TD  width="10%">
						<b>&#160;Date</b>
					</TD>
					<TD  width="21%">
						<b>Mode&#160;of&#160;Payment</b>
					</TD>
					<TD  width="49%">
						<b>CardHolder&#160;Name</b>
					</TD>
					<TD  width="10%" align="right">
						<b>&#160;Currency</b>
					</TD>
					<TD  width="10%" align="right">
						<b>&#160;Amount</b>
					</TD>
				</TR>
				<xsl:for-each select="/Data/PaymentRefund/PaymentMethod">
					<TR>
						<TD>
							&#160;<xsl:value-of select="PmtDate"/>
						</TD>
						<TD>
							<xsl:value-of select="ModeOfPayment"/>
						</TD>
						<TD>
							<xsl:value-of select="CardHolderName"/>
						</TD>
						<TD align="right">
							<xsl:value-of select="Currency"/>
						</TD>
						<TD align="right">
							<xsl:value-of select="format-number(Amount,'#.00')"/>
						</TD>
					</TR>
				</xsl:for-each>
			</TABLE>

			<BR/>

			<TABLE class="tableBlackBorder" cellSpacing="0" >
				<TR>
					<TD bgColor="black" colSpan="5">
						<FONT color="#ffffff"> Vehicle Security/Bond</FONT>
					</TD>
				</TR>
				<xsl:for-each select="/Data/VehicleSecurity/PaymentMethod">
					<TR>
						<TD  width="10%">
							&#160;	<xsl:value-of select="PmtDate"/>
						</TD>
						<TD  width="21%">
							<xsl:value-of select="ModeOfPayment"/>
						</TD>
						<TD  width="49%">
							<xsl:value-of select="CardHolderName"/>
						</TD>
						<TD  width="10%" align="right">
							<xsl:value-of select="Currency"/>
						</TD>
						<TD  width="10%" align="right">
							<xsl:value-of select="format-number(Amount,'#.00')"/>
						</TD>
					</TR>
				</xsl:for-each>
			</TABLE>
      
      <xsl:if test="/Data/BillingTokens/BillingToken !=''">
        <BR/>
			  <TABLE class="tableBlackBorder" cellSpacing="0"  ID="Table6">
				  <TR>
					  <TD bgColor="black" colSpan="5">
						  <FONT color="#ffffff">&#160;Customer acknowledgement/Credit Card Authority</FONT>
					  </TD>
				  </TR>
				  <TR>
					  <TD  width="20%">
						  &#160;<b>Card&#160;Type</b>
					  </TD>
					  <TD  width="30%">
						  <b>CardHolder&#160;Name</b>
					  </TD>
					  <TD  width="20%">
						  <b>Card&#160;Number</b>
					  </TD>
            <TD  width="20%">
              <b>Card&#160;Holder's&#160;Signature</b>
            </TD>
					  <TD  width="10%">
						  <b>Card&#160;Expiry&#160;Date</b>
					  </TD>
				  </TR>
				  <xsl:for-each select="/Data/BillingTokens/BillingToken">
					  <TR>
						  <TD>
							  &#160;<xsl:value-of select="CardType"/>
						  </TD>
						  <TD>
							  <xsl:value-of select="CardHolderName"/>
						  </TD>
						  <TD>
							  <xsl:value-of select="CardNumber"/>
						  </TD>
              <TD>
                &#160;
                <br/>
                ......................................................
              </TD>
						  <TD>
							  <xsl:value-of select="CardExpiryDate"/>
						  </TD>
					  </TR>
				  </xsl:for-each>
          <TR>
            <TD colspan="5">
              <br/>
              <!--I give permission for charges or refunds arising from this agreement that become known after vehicle return to be charged to the credit card signed for above.-->
              <xsl:value-of select="/Data/RntText/RText[Header='Customer Acknowledgement-Bill']/RanText"/>
              <br/>
              <br/>
            </TD>
          </TR>
          <TR>
            <TD  colspan="2">
              Hirer's Signature
              &#160;&#160;&#160;......................................................
            </TD>
            <TD  colspan="3">
              Branch Signature
              &#160;&#160;&#160;......................................................
            </TD>
          </TR>
			  </TABLE>
      </xsl:if >
			<xsl:if test="/Data/TravelDates/Rental/TCTimeStamp !=''">
				<br/>
				<table>
					<tr>
						<td>
							&#160;&#160;&#160;<xsl:value-of select="/Data/TravelDates/Rental/TCTimeStamp"/>&#160;(New Zealand Standard Time) - Time Stamp for acceptance of terms and conditions through Self Check In.
						</td>
					</tr>
				</table>
			
			</xsl:if>
			<br/>
		

			<xsl:for-each select="/Data/RntText/RText">
        <xsl:choose>
          <xsl:when test="Header='Customer Acknowledgement' and /Data/BillingTokens/BillingToken !='' ">
              <!--If header is not BILLing token and there are billing tokens, skip-->
          </xsl:when>
          <xsl:when test="Header='Customer Acknowledgement-Bill'">
            <!--If header is not BILLing token and there are billing tokens, skip-->
          </xsl:when>
          <xsl:otherwise>
            <div>
              <TABLE class="tableBlackBorder" cellpadding="4"  cellSpacing="0" >
                <TR>
                  <TD colspan="2">
                    <b>
                      <xsl:value-of select="Header"/>
                    </b>
                  </TD>
                </TR>
                <TR>
                  <TD colspan="2">
                    <xsl:value-of select="RanText"/>
                  </TD>
                </TR>
                <xsl:if test="optIn='1'">
                  <tr>
                    <td colspan="2">
                      <input id="Checkbox1" type="checkbox" />&#160;&#160;
                      I do wish to receive further information from
                      <xsl:if test="/Data/AgreementHeader/Company='COMPNAME_KXS_NZ'">
                        Explore More
                      </xsl:if>
                      <xsl:if test="/Data/AgreementHeader/Company!='COMPNAME_KXS_NZ'">
                        Maui/Britz/Backpacker
                      </xsl:if>
                      <br/>
                      <br/>
                    </td>
                  </tr>
                </xsl:if>
                <TR>
                  <TD >
                    <xsl:if test="HireSign!=''">
                      <xsl:value-of select="HireSign"/>&#160;&#160;&#160;......................................................
                    </xsl:if>
                  </TD>
                  <TD >
                    <xsl:if test="BranchSign!=''">
                      <xsl:value-of select="BranchSign"/>&#160;&#160;&#160;......................................................
                    </xsl:if>
                  </TD>
                </TR>
              </TABLE>

              <br/>
            </div>
          </xsl:otherwise>
        </xsl:choose>
			</xsl:for-each>
		</div>

	</xsl:template>
	<xsl:template name="lf2br">
		<xsl:param name="StringToTransform"/>
		<xsl:choose>
			<xsl:when test="contains($StringToTransform,'[NL]')">
				<xsl:value-of select="substring-before($StringToTransform,'[NL]')"/>
				<br/>
				<xsl:call-template name="lf2br">
					<xsl:with-param name="StringToTransform">
						<xsl:value-of select="substring-after($StringToTransform,'[NL]')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$StringToTransform"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
</xsl:stylesheet>
