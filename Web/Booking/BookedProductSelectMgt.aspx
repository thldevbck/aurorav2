<%@ Page Language="VB" MasterPageFile="~/Include/AuroraHeader.master" AutoEventWireup="false"
    CodeFile="BookedProductSelectMgt.aspx.vb" Inherits="Booking_BookedProductSelectMgt"
    Title="Untitled Page" %>

<%@ Register Src="../UserControls/PickerControl/PickerControl.ascx" TagName="PickerControl"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="StylePlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ScriptPlaceHolder" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder" runat="Server">

    <script language="javascript" type="text/javascript">
    function displayCompletion()
    {
      var hidFlag = document.getElementById('ctl00_ContentPlaceHolder_hidFlag');
      if(hidFlag.value == 'YES')
        {    
            return (confirm("You have not completed Adding or Removing items from the above lists.\nDo you want to complete this before exiting this screen?"))
	    }
    }
    
    </script>

    <div>
        <asp:Panel runat="server" ID="pnlAgent" Width="100%">
            <table width="100%">
                <tr>
                    <td width="6%">
                        Agent:</td>
                    <td width="45%">
                        <asp:TextBox ID="agentTextBox" runat="server" ReadOnly="true" Width="330">
                        </asp:TextBox>
                    </td>
                    <td width="6%">
                        Hirer:</td>
                    <td width="30%">
                        <asp:TextBox ID="hirerTextBox" runat="server" ReadOnly="true" Width="200">
                        </asp:TextBox>
                    </td>
                    <td width="6%">
                        Status:</td>
                    <td>
                        <asp:TextBox ID="statusTextBox" runat="server" ReadOnly="true" Width="50">
                        </asp:TextBox>
                    </td>
                </tr>
            </table>
            <%--<table>
                <tr>
                    <td width="100">
                        Agent:
                    </td>
                    <td width="400">
                        <b>
                            <asp:Label ID="lblAgent" runat="server" /></b>
                    </td>
                    <td width="100">
                        Hirer:
                    </td>
                    <td width="400">
                        <b>
                            <asp:Label ID="Lblhirer" runat="server" />
                        </b>
                    </td>
                    <td width="100">
                        Status:
                    </td>
                    <td width="50">
                        <b>
                            <asp:Label ID="LblStatus" runat="server" /></b>
                    </td>
                </tr>
            </table>--%>
            <br />
<%--            <asp:Repeater ID="repDisplayXMLPopupData" runat="server">
                <headertemplate>
                    <table width="100%" cellspacing="0" cellpadding="0" border="1" class="dataTable">
                        <thead>
                            <tr>
                                <th>
                                    Rental
                                </th>
                                <th>
                                    Check-Out
                                </th>
                                <th>
                                    Check-In
                                </th>
                                <th>
                                    Hire Prd
                                </th>
                                <th>
                                    Package
                                </th>
                                <th>
                                    Brand
                                </th>
                                <th>
                                    Vehicle
                                </th>
                                <th>
                                    Status
                                </th>
                            </tr>
                        </thead>
                </headertemplate>
                <itemtemplate>
                    <tr class="evenRow">
                        <td>
                            <%#Eval("Rental")%>
                        </td>
                        <td>
                            <%#Eval("Check-Out")%>
                        </td>
                        <td>
                            <%#Eval("Check-In")%>
                        </td>
                        <td>
                            <%#Eval("HirePd")%>
                        </td>
                        <td>
                            <%#Eval("Package")%>
                        </td>
                        <td>
                            <%#Eval("Brand")%>
                        </td>
                        <td>
                            <%#Eval("Vehicle")%>
                        </td>
                        <td>
                            <%#Eval("Status")%>
                        </td>
                    </tr>
                </itemtemplate>
                <footertemplate>
                    </table>
                </footertemplate>
            </asp:Repeater>--%>
            <asp:GridView ID="rentalGridView" runat="server" Width="100%" AutoGenerateColumns="False"
                        CssClass="dataTable">
                        <AlternatingRowStyle CssClass="oddRow" />
                        <RowStyle CssClass="evenRow" />
                        <Columns>
                            <asp:BoundField DataField="Rental" HeaderText="Check-out" ReadOnly="True" />
                            <%--<asp:BoundField DataField="Check-Out" HeaderText="Check-out" ReadOnly="True" />
                            <asp:BoundField DataField="Check-In" HeaderText="Check-in" ReadOnly="True" />--%>
                            <asp:TemplateField HeaderText="Check-Out" >
                                    <ItemTemplate>
                                        <asp:Label ID="cKOWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-out")) %>'></asp:Label>
                                    </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Check-In" >
                                    <ItemTemplate>
                                        <asp:Label ID="cKIWhenLabel" runat="server" Text='<%# GetDateFormat(eval("Check-in")) %>'></asp:Label>
                                    </ItemTemplate>
                             </asp:TemplateField>
                            <asp:BoundField DataField="HirePd" HeaderText="Hire Pd" ReadOnly="True" />
                            <asp:BoundField DataField="Package" HeaderText="Package" ReadOnly="True" />
                            <asp:BoundField DataField="Brand" HeaderText="Brand" ReadOnly="True" />
                            <asp:BoundField DataField="Vehicle" HeaderText="Vehicle" ReadOnly="True" />
                            <asp:BoundField DataField="Status" HeaderText="Status" ReadOnly="True" />
                        </Columns>
                    </asp:GridView>
                    
        </asp:Panel>
        <asp:Panel ID="pnlSelectionCriteria" runat="server" Width="100%">
            <br />
            <asp:UpdatePanel runat="server" id="UpdatePanelSelectionCriteria">
                <contenttemplate>
            <table width="100%">
                <tr width="100%">
                    <td colspan="3" height="20">
                       <b>Selection Criteria</b>
                    </td>
                </tr>
                
                <tr>
                    <td width="15%"></td>
                    <td colspan="2">
                        <asp:RadioButtonList ID="rdoPackage" runat="server" RepeatDirection="Horizontal" TextAlign="Left" autopostback="true" >
                            <asp:ListItem Selected="True">Package</asp:ListItem>
                            <asp:ListItem>Base</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td width="15%">
                        Product:
                    </td>
                    <td width="35%" >
                        <uc1:PickerControl ID="PickerControlProduct" 
                                           runat="server" 
                                           EnableViewState="false"
                                           AutoPostBack="false" 
                                           AppendDescription="true" 
                                           Width="256" 
                                           PopupType="SELBOOPRD"/>
                                          
                    </td>
                    <td>
                         <asp:Button ID="btnShowAvailable" Text="Show Available" runat="server" CssClass="Button_Standard"  />
                    </td>
                    
                </tr>
                <tr>
                    <td width="15%">
                        Class:
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlClass" runat="server" AutoPostBack="true" Width="280">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td width="15%">
                        Type:
                    </td>
                    <td colspan="2">
                        <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" Width="280">
                        </asp:DropDownList></td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr width="100%" valign="top">
                    <td width="50%" colspan="2">
                                
                                
                              <table valign="top" width="100%" cellspacing="0" cellpadding="1" border="1" class="dataTable" runat="server" id="tblAvailabilityNoRecord">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Available Products
                                                </th>
                                                <th width=100>
                                                    <input type="button" value="Add" disabled="disabled" class="Button_Standard" />
                                                </th>
                                            </tr>
                                            <tr>
                                                <th>
                                                    Product
                                                </th>
                                                <th>
                                                    <asp:CheckBox id="chkAllAvailabilityNoRecord" runat="server" autopostback="true" enabled="false"></asp:CheckBox> 
                                                </th>
                                            </tr>
                                        </thead>
                                            <tr>
                                                <td align="left">
                                                    
                                                </td>
                                                <td>
                                                    <asp:CheckBox id="chkAvailabilityNoRecord" runat="server" autopostback="true" enabled="false"></asp:CheckBox> 
                                                </td>
                                            </tr>
                                        </table>
                                
                                <asp:Repeater id="repAvailability" runat="server">
                                      <HeaderTemplate>
                                        <table valign="top" width="100%" cellspacing="0" cellpadding="1" border="1" class="dataTable">
                                              <thead>
                                              <tr>
                                                <th>
                                                    Available Products
                                                </th>
                                                <th width=50>
                                                    <asp:Button id="btnAdd" text="Add" runat="server"  class="Button_Standard Button_Add" onClick="btnAdd_Click"/>
                                                </th>
                                              </tr>
                                            <tr>
                                                <th>
                                                    Product
                                                </th>
                                                <th>
                                                    <asp:CheckBox id="chkAllAvailability" runat="server" autopostback="true" OnCheckedChanged="chkAllAvailability_CheckedChanged"></asp:CheckBox> 
                                                </th>
                                            </tr>
                                        </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class ="evenRow">
                                                <td align="left">
                                                    <%# eval("Desc") %>
                                                    <asp:literal id="litPrdId" runat="server" text='<%# eval("PrdId") %>' visible="false" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox id="chkAvailability" runat="server" autopostback="true" OnCheckedChanged="chkAvailability_CheckedChanged"></asp:CheckBox> 
                                                    
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                         <AlternatingItemTemplate>
                                        <tr class="oddRow">
                                                <td>
                                                    <%# eval("Desc") %>
                                                </td>
                                                <td>
                                                    <asp:CheckBox id="chkAvailability" runat="server" autopostback="true" OnCheckedChanged="chkAvailability_CheckedChanged"></asp:CheckBox> 
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                </asp:Repeater>
                    </td>
                    
                    <td  width="50%">
                            
                                 <table valign="top" width="100%" cellspacing="0" cellpadding="1" border="1" class="dataTable" runat="server" id="tblSelectedNoRecord" visible="false">
                                             <thead>
                                                <tr>
                                                    <th>
                                                        Selected Products
                                                    </th>
                                                    <th width=50>
                                                        <input type="button" value="Remove" disabled="disabled" class="Button_Standard"/>
                                                    </th>
                                                </tr>
                                              <tr>
                                                        <th>
                                                            Product
                                                        </th>
                                                        <th>
                                                            <asp:CheckBox id="chkAllselectedNoRecord" runat="server" autopostback="false" enabled="false"></asp:CheckBox> 
                                                        </th>
                                                    </tr>
                                                    <tr class="evenRow">
                                                        <td>
                                                        </td>
                                                        <td>
                                                            <asp:CheckBox id="chkselectedNoRecord" runat="server" autopostback="false"  enabled="false"></asp:CheckBox> 
                                                        </td>
                                                    </tr>
                                            </table>
                                 <asp:Repeater id="repSelected" runat="server">
                                 <HeaderTemplate>
                                        <table valign="top" width="100%" cellspacing="0" cellpadding="1" border="1" class="dataTable">
                                        <thead>
                                              <tr>
                                                    <th>
                                                        Selected Products
                                                    </th>
                                                    <th width=50>
                                                        <asp:Button id="btnRemove" text="Remove" runat="server" class="Button_Standard Button_Remove" 
                                                            onClick="btnRemove_Click"/>
                                                    </th>
                                                </tr>
                                            <tr>
                                                <th>
                                                    Product
                                                </th>
                                                <th>
                                                    <asp:CheckBox id="chkAllselected" runat="server" autopostback="true" OnCheckedChanged="chkAllselected_CheckedChanged"></asp:CheckBox> 
                                                </th>
                                            </tr>
                                        </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="evenRow">
                                                <td>
                                                    <%# eval("Desc") %>
                                                    <asp:literal id="litPrdId" runat="server" text='<%# eval("PrdId") %>' visible="false" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox id="chkselected" runat="server" autopostback="true" enabled='<%# eval("Rem") %>' OnCheckedChanged="chkselected_CheckedChanged"></asp:CheckBox> 
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        
                                        <AlternatingItemTemplate>
                                        <tr class="oddRow">
                                                <td>
                                                    <%# eval("Desc") %>
                                                    <asp:literal id="litPrdId" runat="server" text='<%# eval("PrdId") %>' visible="false" />
                                                </td>
                                                <td>
                                                    <asp:CheckBox id="chkselected" runat="server" autopostback="true" enabled='<%# eval("Rem") %>' OnCheckedChanged="chkselected_CheckedChanged"></asp:CheckBox> 
                                                </td>
                                            </tr>
                                        </AlternatingItemTemplate>
                                        
                                        <FooterTemplate>
                                            </table>
                                        </FooterTemplate>
                                </asp:Repeater>
                    </td>
                </tr>
               <tr width="100%" valign="top">
                    <td width="50%" colspan="2">
                    <td align="right">
                        <asp:Button id="btnSaveNext" text="Save/Next" runat="server" 
                            class="Button_Standard Button_Save" />
                        <asp:Button id="btnBack" text="Back" runat="server" 
                            class="Button_Standard Button_Back" />
                    </td>
                </tr>
            </table>
             <asp:HiddenField ID="hidFlag" runat="server"  EnableViewState="true"/>
            </contenttemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </div>
    <asp:Literal ID="litXMLDSO" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="LitXMLCLASS" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="LitXMLTYPE" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="LitXMLPOPUPDATA" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litXMLDSOavail" runat="server" EnableViewState="true" Visible="false" />
    <asp:Literal ID="litXMLDSOsel" runat="server" EnableViewState="true" Visible="false" />
</asp:Content>
