'' Change Log 
'' 18.7.8 - Shoel - date format done, full location name in picker, returned error message now shown properly

Imports System.Xml
Imports System.Data
Imports Aurora.Common
Imports Aurora.Booking.Services
Imports Aurora.Common.Utility

<AuroraFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookedProductSearch)> _
<AuroraMenuFunctionCodeAttribute(AuroraFunctionCodeAttribute.BookingSearch)> _
<AuroraMessageAttribute("GEN005,GEN062,GEN070")> _
Partial Class Booking_BookedProductSearch
    Inherits AuroraPage

    Const VIEWSTATE_BOOKINGID As String = "ViewState_BPDSearch_BookingId"
    Const VIEWSTATE_RENTALID As String = "ViewState_BPDSearch_RentalId"


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ' get the RentalId, BookingId
        If Not IsPostBack Then
            Dim sBookingId, sRentalId As String

            sBookingId = CStr(Request("BooId"))
            sRentalId = CStr(Request("RntId"))

            pkrProduct.Param2 = sRentalId
            pkrProductClass.Param2 = sRentalId
            pkrProductType.Param2 = sRentalId
            ViewState(VIEWSTATE_RENTALID) = sRentalId
            ViewState(VIEWSTATE_BOOKINGID) = sBookingId

            LoadProductTypeCombo()
            Session("BookedProductIdList_" & UserCode) = Nothing
            ' for cancel
            btnCancel.Attributes.Add("OnClick", "window.location='Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & ViewState(VIEWSTATE_BOOKINGID) & "&hdRentalId=" & ViewState(VIEWSTATE_RENTALID) & "&activeTab=9';return false;")
        End If


    End Sub

    Sub LoadProductTypeCombo()
        Dim dtProductTypes As DataTable
        dtProductTypes = Aurora.Booking.Services.BookedProductSearch.GetProductTypeData()

        cmbType.DataSource = dtProductTypes
        cmbType.DataTextField = "DESCRIPTION"
        cmbType.DataValueField = "CODE"
        cmbType.DataBind()

        Dim oListItem As ListItem = New ListItem("LER - Latest Exchange Records", "LER")
        cmbType.Items.Insert(0, oListItem)
        cmbType.Items.Insert(0, "")

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        GetSearchResults()
    End Sub

    Sub GetSearchResults()
        ' hit the db
        Dim xmlReturn As XmlDocument

        Dim sCKILocation, sCKOLocation, sCKIDate, sCKODate As String

        sCKILocation = IIf(pkrCheckInLocation.Text.Trim().IndexOf("-"c) < 0, pkrCheckInLocation.Text.Trim(), pkrCheckInLocation.Text.Trim().Split("-"c)(0))
        sCKOLocation = IIf(pkrCheckOutLocation.Text.Trim().IndexOf("-"c) < 0, pkrCheckOutLocation.Text.Trim(), pkrCheckOutLocation.Text.Trim().Split("-"c)(0))

        sCKIDate = IIf(dtcCheckInDate.Text.Trim().Equals(String.Empty), String.Empty, DateTimeToString(dtcCheckInDate.Date, SystemCulture).Split(" "c)(0))
        sCKODate = IIf(dtcCheckOutDate.Text.Trim().Equals(String.Empty), String.Empty, DateTimeToString(dtcCheckOutDate.Date, SystemCulture).Split(" "c)(0))

        xmlReturn = Aurora.Booking.Services.BookedProductSearch.DoBookedProductSearch(CStr(ViewState(VIEWSTATE_BOOKINGID)), _
        CStr(ViewState(VIEWSTATE_RENTALID)), cmbStatus.SelectedValue, cmbInitialStatus.SelectedValue, pkrProduct.Text, pkrProductClass.Text, _
        pkrProductType.Text, sCKODate, sCKOLocation, sCKIDate, sCKILocation, _
        cmbType.SelectedValue, txtExternalBookingRef.Text, cmbChargeTo.SelectedValue, cmbIsCurrent.SelectedValue, cmbIsOriginal.SelectedValue, _
        cmbFOCProducts.SelectedValue, cmbTransferredToAccounts.SelectedValue, txtRecordId.Text, UserCode)

        If (xmlReturn.DocumentElement.ChildNodes.Count = 0) _
            Or _
            (xmlReturn.DocumentElement.ChildNodes.Count = 1 And xmlReturn.DocumentElement.ChildNodes(0).Name = "#text") Then
            ' just a message
            SetWarningMessage(xmlReturn.DocumentElement.InnerText)
            Exit Sub
        Else
            ' some records? make a Comma seperated list and add to session
            Dim sBPDList As String = ""

            For Each oNode As XmlNode In xmlReturn.DocumentElement.ChildNodes
                sBPDList = sBPDList & oNode.Attributes("BpdId").InnerText & ","
            Next
            sBPDList = sBPDList.Substring(0, sBPDList.Length - 2)
            Session("BookedProductIdList_" & UserCode) = sBPDList
        End If

        ReturnToProductsTab()
    End Sub

    Sub ReturnToProductsTab()
        ScriptManager.RegisterStartupScript(Me, GetType(Page), UniqueID, "window.location='Booking.aspx?funCode=RS-BOKSUMMGT&hdBookingId=" & ViewState(VIEWSTATE_BOOKINGID) & "&hdRentalId=" & ViewState(VIEWSTATE_RENTALID) & "&activeTab=9';", True)
    End Sub
End Class
