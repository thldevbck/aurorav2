﻿// JScript File - for VehicleRequestMGT.aspx - rev:MIA - Feb24

   var requestURL =window.document.location.toString();
           var start=0
           var end=requestURL.lastIndexOf("/") + 1;
           var remotePage= 'GetVehicleRequestAjax.aspx?mode=';

           //for getting the current url dynamically 
           requestURL=requestURL.substring(start,end) + remotePage;       
           var xmlHttp;        
           var is_ie = (navigator.userAgent.indexOf('MSIE') >= 0) ? 1 : 0; 
           var is_ie5 = (navigator.appVersion.indexOf("MSIE 5.5")!=-1) ? 1 : 0; 
           var strGetpackagexml 
       
       
       //Compute date before Check-out date
       //----------------------------------------------------------------------------------------
       function setTillDays()
       {
	        var ckoDate = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckOut_dateTextBox').value
	        var gDdToday =  document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckIn_dateTextBox').value
	        
	        if (ckoDate != '' && gDdToday != '')
	            { 
	                    var date1 = new Date( gDdToday.split('/')[2], gDdToday.split('/')[1]-1, gDdToday.split('/')[0] );
	                    var date2 = new Date( ckoDate.split('/')[2], ckoDate.split('/')[1]-1, ckoDate.split('/')[0] );
	                    //JL 2008/06/18
	                    //remove 1 day
	                    //document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod').value = ( Math.round( ( date1.getTime() - date2.getTime() ) / 86400000 ) + 1 )	
	                    document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod').value = ( Math.round( ( date1.getTime() - date2.getTime() ) / 86400000 ) )	
             
                }
		        else
		        {
		                document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod').value = ""
		                return
		        }
	    } 
       
       //Validate date entered
       //----------------------------------------------------------------------------------------
       function validateDate(fld) {
           
            var RegExPattern =  /^\d{1,2}\/\d{1,2}\/\d{4}$/;
            if ((fld.value.match(RegExPattern)) && (fld.value!='')) {
                return true;
            } else {
                fld.focus();
                return false;
            } 
        }
       
       
       //Return date in word format
       //----------------------------------------------------------------------------------------
       function getDayofWeek(formate,lblindex){
                var strdate
                var cko
                var cki
                var myDate

	            if(lblindex == 1)
	            {
	            
	                    cko = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckOut_dateTextBox')
	                    myDate = cko.value 
	                    if(validateDate(cko)==false)
	                        {
	                            document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod').value = ""
	                            ctl00_ContentPlaceHolder_lbldayCKO.innerHTML = ""
	                            return
	                        }
	                    
	            }
	            else if(lblindex == 2)
	            {
	                    
	                    cki = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckIn_dateTextBox')
	                    myDate = cki.value 
	                    if(validateDate(cki)==false)
	                     {
	                            document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod').value = ""
	                            ctl00_ContentPlaceHolder_lblDayCKI.innerHTML = ""
	                            return
	                        }
	            }
	            
	            dt = new Date(myDate.split('/')[1]+'/'+myDate.split('/')[0]+'/'+myDate.split('/')[2])
	            if (formate == 1) //for cko
	                strdate = dt.getDay()
	            else{
		            var str = dt.toDateString()
		            strdate = str.substring(0,3)
	            }
	            
                
    	           //checkout date
	            if(lblindex==1)
	                {
	                     ctl00_ContentPlaceHolder_lbldayCKO.innerHTML=strdate
	                     document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckout').focus()
	                     
	                }
	                else if(lblindex==2)
	                {
	                     ctl00_ContentPlaceHolder_lblDayCKI.innerHTML=strdate
	                     document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckin').focus()
	                }         
	        
	            setTillDays(); 
	            
       }
       
      
         function GetPackageXML()
           {
                var agentid = document.getElementById('ctl00_ContentPlaceHolder_PickerControlForAgent_pickerHidden')
                if (agentid.value.length == 0)
                {
                        agentid=document.getElementById('ctl00_ContentPlaceHolder_hidAgentID')
                }
                
              //  alert(document.getElementById('ctl00_ContentPlaceHolder_PickerControlForCheckOut_pickerHidden').value);
                
                var productid= document.getElementById('ctl00_ContentPlaceHolder_hidPackageID')
                var hidCKOandCKIinfo = document.getElementById('ctl00_ContentPlaceHolder_hidcompanion')
                var cko = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckOut_dateTextBox')
                var ckoAMPM = document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckout')
                var ckobranch = document.getElementById('ctl00_ContentPlaceHolder_PickerControlForCheckOut_pickerTextBox')
                var cki = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckIn_dateTextBox')
                var ckiAMPM = document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckin')
                var ckibranch = document.getElementById('ctl00_ContentPlaceHolder_PickerControlForCheckIn_pickerTextBox')
                
                //var adult = document.getElementById('ctl00_ContentPlaceHolder_txtAdult')
                //var infants = document.getElementById('ctl00_ContentPlaceHolder_txtchildren')
                //var children = document.getElementById('ctl00_ContentPlaceHolder_txtinfants')
                
                var adult = document.getElementById('ctl00_ContentPlaceHolder_ddlAdultNO')
                var infants = document.getElementById('ctl00_ContentPlaceHolder_ddlChildNO')
                var children = document.getElementById('ctl00_ContentPlaceHolder_ddlInfantsNO')
                
                var packagexml
                var ckostr
                var ckistr
                
                strCko= ckobranch.value.split('-')
                strCki =ckibranch.value.split('-')     
                
                /*
                        if (ckoAMPM.value == 'AM')
                            {
                                ckostr = cko.value + ' ' + '07:00:00'
                            }
                            else
                            {
                                ckostr = cko.value + ' ' + '13:00:00'
                            }
                            
                         if (ckiAMPM.value == 'AM')
                            {
                                ckistr = cki.value + ' ' + '07:00:00'
                            }
                            else
                            {
                                ckistr = cki.value + ' ' + '13:00:00'
                            }
                 */      
                 
                        
                        if (ckoAMPM.value == 'AM')
                            {
                                ckostr = cko.value + ' ' + '07:00:00'
                            }
                            else
                            {
                                if (ckoAMPM.value == 'PM')
                                    {
                                        ckostr = cko.value + ' ' + '13:00:00'
                                    }
                                    else
                                    {
                                        ckostr = cko.value + ' ' + ckoAMPM.value
                                    }    
                            }
                            
                         if (ckiAMPM.value == 'AM')
                            {
                                ckistr = cki.value + ' ' + '07:00:00'
                            }
                            else
                            {
                                
                                if (ckiAMPM.value == 'PM')
                                    {
                                        ckistr = cki.value + ' ' + '13:00:00'
                                    }
                                    else
                                    {
                                        ckistr = cko.value + ' ' + ckiAMPM.value
                                    }    
                            }
                  
                        packagexml = agentid.value + ',' + productid.value + ',' + ckostr + ',' + strCko[0] + ',' + strCki[0] + ','  + adult.value + ',' + children.value + ',' + infants.value  + ',' + ckistr
                        strGetpackagexml =packagexml
                        document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo').value=packagexml
                        //document.getElementById('ctl00_ContentPlaceHolder_PickerControlForPackage_optionalParam').value = packagexml
                        //ctl00_ContentPlaceHolder_lbltest.innerHTML=document.getElementById('ctl00_ContentPlaceHolder_PickerControlForPackage_optionalParam').value
                        //packagexml
                        
           }

            
       
           function CalculateDays()
           {
            
                    var hirerdays = document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod')
                    var cko = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckOut_dateTextBox')
                    var cki = document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckIn_dateTextBox')
                    var ckiAMPM = document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckin')
                    var ckoAMPM = document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckout')
                    var days = document.getElementById('ctl00_ContentPlaceHolder_ddlDays')
                    var hidCKOandCKIinfo = document.getElementById('ctl00_ContentPlaceHolder_hidCKOandCKIinfo')
                    var ckoValue 
                        if(ckoAMPM.value.indexOf('AM'))
                            {
                                ckoValue = 'AM'
                            }else{
                                if(ckoAMPM.value.indexOf('PM'))
                                    {
                                        ckoValue = 'PM'
                                    }
                            }    
                    
                    var ckiValue
                    if(ckiAMPM.value.indexOf('AM'))
                            {
                                ckiValue = 'AM'
                            }else{
                                if(ckiAMPM.value.indexOf('PM'))
                                    {
                                        ckiValue = 'PM'
                                    }
                            }    
                    
                    //alert('ckoValue: ' + ckoValue + ' , ' + 'ckiValue: ' + ckoValue);
                    
                    var datehiredparam = cko.value + '|' + ckoValue + '|' + cki.value + '|' + ckiValue + '|' + hirerdays.value + '|' + days.value
                    
                    if (hirerdays.value.length > 0 && cko.value.length > 0 && cki.value.length > 0)
                        {
                          
                            hidCKOandCKIinfo.value = datehiredparam
                            var url=requestURL + 'getdatehired&paramdatehired=' + datehiredparam 
                            xmlHttp = GetXmlHttpObject(stateChangeHandler)
                            xmlHttp_Get(xmlHttp, url);     
                        }
                        
           }
            
           function GetProductID(userid)
           {
                    var productid= document.getElementById('ctl00_ContentPlaceHolder_PickerControlForProduct_pickerTextBox')
                    var url=requestURL + 'getproductid&paramProductid=' + productid.value + '&uid=' + userid
                    xmlHttp = GetXmlHttpObject(stateChangeHandler)
                    xmlHttp_Get(xmlHttp, url); 
               
           }
           
           //REV:MIA May-5
       
         
           
           function GetAvailableTimeCKO(userid)
           {  //alert('GetAvailableTimeCKO')
                var location= document.getElementById('ctl00_ContentPlaceHolder_PickerControlForCheckOut_pickerTextBox')
                
                   var url=requestURL + 'GetAvailableTimeCKO&paramcko=' + location.value + '&uid=' + userid + '&controltype=1' 
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
          function GetAvailableTimeCKI(userid)
           {  
                var location= document.getElementById('ctl00_ContentPlaceHolder_PickerControlForCheckIn_pickerTextBox')
                
                   var url=requestURL + 'GetAvailableTimeCKI&paramcki=' + location.value + '&uid=' + userid + '&controltype=2' 
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
          
          
            function GetAvailableRequestTimeCKO(userid)
           {  //alert('GetAvailableTimeCKO')
                var location= document.getElementById('ctl00_ContentPlaceHolder_PickerControlForBranchCKO_pickerTextBox')
                
                   var url=requestURL + 'GetAvailableTimeCKO&paramcko=' + location.value + '&uid=' + userid + '&controltype=1' 
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
           function GetAvailableRequestTimeCKI(userid)
           {  
                var location= document.getElementById('ctl00_ContentPlaceHolder_PickerControlForBranchCKI_pickerTextBox')
                
                   var url=requestURL + 'GetAvailableTimeCKI&paramcki=' + location.value + '&uid=' + userid + '&controltype=2' 
                   xmlHttp = GetXmlHttpObject(stateChangeHandler)
                   xmlHttp_Get(xmlHttp, url); 
            
           }
          
          
          
           function ClearLocationTimeAndSet(locationtime)
           {
                        var location
                        
                        //alert('mode: ' + locationtime.selectSingleNode('mode').text);
                        if(locationtime.selectSingleNode('mode')!= null)
                            {

                                if(locationtime.selectSingleNode('mode').text == '1') 
                                    {
                                        //alert('a');
                                        location= document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckout');
                                        if(location == null)
                                            {
                                                location= document.getElementById('ctl00_ContentPlaceHolder_ddlamppmCKO');
                                            }
                                           
                                    }
                                    else
                                    {
                                        //alert('B');
                                        location= document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcheckin');
                                        if(location == null)
                                            {
                                                location= document.getElementById('ctl00_ContentPlaceHolder_ddlAMPMcki');
                                            }
                                    }
                            } 	     	       
                        
                        //alert('mode: ' + location.id);
                        var locationNodes = locationtime.getElementsByTagName('ckocki');	     	       
                        
	                    var textValueName; 
	                    var optionItemName;
            	       
            	        
	                     //Clears the location dropdown list        	       
	                  
	                    for (var count = location.options.length-1; count >-1; count--)
	                    {
		                    location.options[count] = null;	        
            		        
	                    }        
	                    
	                    //optionItemName = new Option('','');
	                    //location.add(optionItemName);
	                    for (var count = 0; count < locationNodes.length; count++)
	                    {
                                           
                           
						    var textNamevalue = locationNodes[count].selectSingleNode("data").text; 
						    var textIDvalue = locationNodes[count].selectSingleNode("data").text; 
		                    optionItemName = new Option(textNamevalue, textIDvalue,  false, false);
		                    location.options[location.length] = optionItemName;  
            		        
	                    }	       
	                    
           }
         
           function getContacts(contact)
           {
                    var url
                    //alert('here');               
                    if (contact.length > 0)
                    { 
                        //Append the name to search for to the requestURL 
                        url = requestURL + 'getcontact&paramContactID=' + contact 
                    }
                    else
                    {
                        var ddl=document.getElementById("ctl00_ContentPlaceHolder_PickerControlForAgent_pickerTextBox"); 
                        url = requestURL + 'getcontact&paramContactName=' + ddl.value 
                    }
                    
                      
                        xmlHttp = GetXmlHttpObject(stateChangeHandler);
                        xmlHttp_Get(xmlHttp, url);     
        
           }
           
           function GetXmlHttpObject(handler)
                     { 
                        var objXmlHttp = null;    //Holds the local xmlHTTP object instance             
                        if (is_ie)
                        { 
                            //The object to create depends on version of IE 
                            //If it isn't ie5, then default to the Msxml2.XMLHTTP object           
                            var strObjName = (is_ie5) ? 'Microsoft.XMLHTTP' : 'Msxml2.XMLHTTP'; 
                         
                            //Attempt to create the object 
                            try
                            { 
                            objXmlHttp = new ActiveXObject(strObjName);                
                            objXmlHttp.onreadystatechange = handler; 
                            } 
                            catch(e)
                            { 
                            //Object creation errored 
                                alert('Object could not be created'); 
                                return; 
                            }    
                         } 
                         
                          return objXmlHttp;
                          alert(objXmlHttp);
                           
                     } 

                function xmlHttp_Get(xmlhttp, url)
                     {                     
                        xmlhttp.open('GET', url, true);             
                        xmlhttp.send(null); 
                     } 
    
    
                function GetHiredDate(datexml)
                {
                        
                        
                        var doc1 = new ActiveXObject("MSXML2.DOMDocument"); 
                        doc1.loadXML(datexml)
                        
                            var d1 = doc1.selectSingleNode('//Root/D1').text
                            var d2 = doc1.selectSingleNode('//Root/D2').text
                            var dif = doc1.selectSingleNode('//Root/Dif').text
                            
                            document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckOut_dateTextBox').value = d1
                            document.getElementById('ctl00_ContentPlaceHolder_DateControlForCheckIn_dateTextBox').value = d2
                            document.getElementById('ctl00_ContentPlaceHolder_txtHirePeriod').value = dif
                            //document.getElementById('ctl00_ContentPlaceHolder_hidDate').value=dif
                    
                }
                
                function ClearContactsAndSetContacts(userNodes)
                {
                        var usersList = document.getElementById("ctl00_ContentPlaceHolder_ddlContact");           
                        var userNameNodes = userNodes.getElementsByTagName('Contact');	     	       
	                    var textValueName; 
	                    var optionItemName;
            	       
            	        
	                     //Clears the ddl_users dropdown list        	       
	                    for (var count = usersList.options.length-1; count >-1; count--)
	                    {
		                    usersList.options[count] = null;	        
            		        
	                    }        
	                    //Add new users list to the users ddl_users
	                    optionItemName = new Option('','');
	                    usersList.add(optionItemName)
	                    for (var count = 0; count < userNameNodes.length; count++)
	                    {
                                           
                           
						    var textNamevalue = userNameNodes[count].selectSingleNode("ConLastName").text + ", " + userNameNodes[count].selectSingleNode("ConFirstName").text
						    var textIDvalue = userNameNodes[count].selectSingleNode("ConId").text 
		                    optionItemName = new Option(textNamevalue, textIDvalue,  false, false);
		                    usersList.options[usersList.length] = optionItemName;  
            		        
	                    }	       
	                    
            	        
                }

           function stateChangeHandler() 
                { 
                   //readyState of 4 or 'complete' represents that data has been returned            
                    if (xmlHttp.readyState == 4 || xmlHttp.readyState == 'complete')
                    { 
                        //Gather the results from the callback          
                        var str = xmlHttp.responseText; 
                        
                        
                        if(str.search('<AddAgentID>') != -1)
                            {
                                var agentid = str.substr(str.indexOf('<AddAgentID>'))
                                agentid = agentid.replace('<AddAgentID>','')
                                agentid = agentid.replace('</AddAgentID>','')
                                document.getElementById('ctl00_ContentPlaceHolder_hidAgentID').value = agentid
                                str  = str.substr(0,str.indexOf('<AddAgentID>'))
                            } 
                            
                        var doc = new ActiveXObject("MSXML2.DOMDocument"); 
                        doc.loadXML(str)
                        
                        // for populating  ddl_users                
                        if (doc.documentElement != null)
                        {
                          
                            if (doc.selectSingleNode('//Root/D1') != null)
                                {
                                    GetHiredDate(str);
                                }
                                else
                                {
                                    
                                    if(doc.selectSingleNode('//PRODUCT/Id') != null)
                                        {
                                            document.getElementById('ctl00_ContentPlaceHolder_hidPackageID').value = doc.selectSingleNode('//PRODUCT/Id').text
                                            document.getElementById('ctl00_ContentPlaceHolder_hidPackageID').value
                                        }
                                        else
                                        {
                                            
                                            if(doc.selectSingleNode('/root/ckocki') != null)
                                            {
                                               
                                                ClearLocationTimeAndSet(doc.documentElement);
                                            }
                                            else
                                            {
                                               
                                                ClearContactsAndSetContacts(doc.documentElement);
                                            }
                                            
                                        }
                                    
                                    
                                }    
                            
                        }
                        else
                        {
                            //alert("No Match");
                        
                        }                        
                   }   
               } 

                function Navigate()
                        {
                            if (confirm("Are you sure you want to start a new Booking Request?.")){			
			                    document.forms[0].action="VehicleRequestMgt.aspx"
			                    document.forms[0].method="POST"
			                    document.forms[0].submit()
		                    }
	                    }	
          
           
                 function IsNumberValid(Source,Args)
                        {
                            Args.IsValid = IsNumeric(Args.Value)
                
                        }
           
                function IsNumeric(sText)
                        {
                           var ValidChars = "0123456789";
                           var IsNumber=true;
                           var Char;

                         
                           for (i = 0; i < sText.length && IsNumber == true; i++) 
                              { 
                              Char = sText.charAt(i); 
                              if (ValidChars.indexOf(Char) == -1) 
                                 {
                                 IsNumber = false;
                                 }
                              }
                           return IsNumber;
                           
                        }


function PassHiddenValuesToParam()
{
  //document.getElementById('ctl00_ContentPlaceHolder_PickerControlForPackage_optionalParam').value = packagexml
  //document.forms[0].submit();
}

function EmptyAgentControl()
{
    //alert('EmptyAgentControl')
    debugger;
    var serverFlag = document.getElementById('ctl00_ContentPlaceHolder_HidFlag').value
    
    if(serverFlag=='POSTBACKCLEAR')
        {
            document.getElementById('ctl00_ContentPlaceHolder_PickerControlForAgent_pickerTextBox').value =''
            document.getElementById('ctl00_ContentPlaceHolder_ddlContact').value =''
        }
    
}

  function MessageOnchange()
    {
        setInformationMessage("Please 'Resubmit' to update table")
    }
