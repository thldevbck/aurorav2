<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ConfirmationViewScreen.aspx.vb" Inherits="Booking_ConfirmationViewScreen" MasterPageFile="~/Include/PopupHeader.master" validateRequest="false" %>

<%@ Register Src="..\UserControls\RegExTextBox\RegExTextBox.ascx" TagName="RegExTextBox" TagPrefix="uc1" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ScriptPlaceHolder">

<script language="javascript" type="text/javascript">

function RefreshBookingTab(CalledFrom,HiddenRefreshButtonId,BooId,RntId)
{
    if(CalledFrom == "ConfirmationManagement.aspx")
    {
        //have to change parent location to Booking.aspx
        window.opener.location = "Booking.aspx?hdBookingId=" + BooId + "&hdRentalId=" + RntId + "&activeTab=3";//"&FromConfirmationView=1"
        window.close()
        
    }
    else
    {
        //called from booking tab...
        window.opener.location = 'Booking.aspx?hdBookingId=' + BooId + '&hdRentalId=' +RntId  + '&activeTab=3'
        //window.opener.document.getElementById(HiddenRefreshButtonId).click();
        window.close();
    }
}

function PrintIfNeeded(PrintRadioButtonClientId)
{
    if(document.getElementById(PrintRadioButtonClientId).checked)
    {
        MessagePanelControl.clearMessagePanel(null);
        window.print();
    }
}
document.addEventListener("DOMContentLoaded", function (event) {
    document.body.style.backgroundColor = '#ffffff';
});
</script>
</asp:Content>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="StylePlaceHolder">

<style type="text/css">
@media print
{
    .pnlParams
    {
        display: none;
    }
}

.pnlMain
{
    background-color: white;
}
.pnlMain H2
{
	page-break-after: always;
}
.pnlMain TD
{
	font-size: 8pt;
	font-family: Courier New		
}
</style>

</asp:Content>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="ContentPlaceHolder">

    <asp:UpdatePanel ID="updParams" runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlParams" runat="server" CssClass="pnlParams">
        
                <table border="0" width="100%" cellpadding="2" cellspacing="0">
                    <tr>
                        <td width="150px" rowspan="3" valign="top" style="padding-top: 6px">Send By:</td>
                        <td width="150px">
                            <asp:RadioButton 
                                ID="radEmail" 
                                GroupName="rgSendMethod" 
                                runat="server" 
                                Checked="true" 
                                Text="Email Address:" />
                        </td>
                        <td style="padding-top: 4px">
                            <%--<uc1:RegExTextBox 
                                ID="txtEmailAddress" 
                                runat="server" 
                                Width="300px"
                                RegExPattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"
                                ErrorMessage="Enter a valid Email Address"
                                Nullable="false" />--%>
                                <asp:TextBox ID="txtEmailAddress" runat="server" CssClass="Textbox_Standard" style="width:300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton ID="radFax" GroupName="rgSendMethod" runat="server" Text="Fax Number:" />
                        </td>
                        <td style="padding-top: 4px">
                            
                            <%--<uc1:RegExTextBox ID="txtFaxNumber" runat="server" Width="150px"  RegExPattern="^\d+$" ErrorMessage="Enter a valid Fax Number" />--%>
                            <asp:TextBox ID="txtFaxNumber" runat="server" CssClass="Textbox_Standard" style="width:300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:RadioButton 
                                ID="radPrint" 
                                GroupName="rgSendMethod" 
                                runat="server" 
                                Text="Printer" />
                        </td>
                        <td colspan="2" align="right">
                            <asp:Button ID="btnSend" runat="server" Text="Send" CssClass="Button_Standard Button_OK" />
                        </td>
                    </tr>
                </table>
            
                <hr />
    
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel ID="pnlMain" runat="server" CssClass="pnlMain">
        <asp:Literal ID="confirmationXmlLiteral" runat="server" />
    </asp:Panel>
    
    <br />

    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button 
                    runat="server" 
                    ID="btnClose" 
                    OnClientClick="window.close(); return false;" 
                    Text="Close" 
                    CssClass="Button_Standard Button_Close" />
            </td>
        </tr>
    </table>
    
</asp:Content>